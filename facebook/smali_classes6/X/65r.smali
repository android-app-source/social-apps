.class public final LX/65r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65Z;


# instance fields
.field public final a:LX/65m;

.field public final b:LX/670;

.field private final c:Z

.field private final d:LX/672;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(LX/670;Z)V
    .locals 2

    .prologue
    .line 1049012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049013
    iput-object p1, p0, LX/65r;->b:LX/670;

    .line 1049014
    iput-boolean p2, p0, LX/65r;->c:Z

    .line 1049015
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/65r;->d:LX/672;

    .line 1049016
    new-instance v0, LX/65m;

    iget-object v1, p0, LX/65r;->d:LX/672;

    invoke-direct {v0, v1}, LX/65m;-><init>(LX/672;)V

    iput-object v0, p0, LX/65r;->a:LX/65m;

    .line 1049017
    const/16 v0, 0x4000

    iput v0, p0, LX/65r;->e:I

    .line 1049018
    return-void
.end method

.method public static a(LX/65r;IIBB)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1049019
    sget-object v0, LX/65t;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/65t;->a:Ljava/util/logging/Logger;

    invoke-static {v3, p1, p2, p3, p4}, LX/65p;->a(ZIIBB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 1049020
    :cond_0
    iget v0, p0, LX/65r;->e:I

    if-le p2, v0, :cond_1

    .line 1049021
    const-string v0, "FRAME_SIZE_ERROR length > %d: %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, LX/65r;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/65t;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 1049022
    :cond_1
    const/high16 v0, -0x80000000

    and-int/2addr v0, p1

    if-eqz v0, :cond_2

    const-string v0, "reserved bit set: %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/65t;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 1049023
    :cond_2
    iget-object v0, p0, LX/65r;->b:LX/670;

    .line 1049024
    ushr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1049025
    ushr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1049026
    and-int/lit16 v1, p2, 0xff

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1049027
    iget-object v0, p0, LX/65r;->b:LX/670;

    and-int/lit16 v1, p3, 0xff

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1049028
    iget-object v0, p0, LX/65r;->b:LX/670;

    and-int/lit16 v1, p4, 0xff

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1049029
    iget-object v0, p0, LX/65r;->b:LX/670;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049030
    return-void
.end method

.method private a(ZILjava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1048898
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048899
    :cond_0
    iget-object v0, p0, LX/65r;->a:LX/65m;

    invoke-virtual {v0, p3}, LX/65m;->a(Ljava/util/List;)V

    .line 1048900
    iget-object v0, p0, LX/65r;->d:LX/672;

    .line 1048901
    iget-wide v8, v0, LX/672;->b:J

    move-wide v2, v8

    .line 1048902
    iget v0, p0, LX/65r;->e:I

    int-to-long v0, v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v1, v0

    .line 1048903
    int-to-long v4, v1

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    const/4 v0, 0x4

    .line 1048904
    :goto_0
    if-eqz p1, :cond_1

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    .line 1048905
    :cond_1
    const/4 v4, 0x1

    invoke-static {p0, p2, v1, v4, v0}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048906
    iget-object v0, p0, LX/65r;->b:LX/670;

    iget-object v4, p0, LX/65r;->d:LX/672;

    int-to-long v6, v1

    invoke-interface {v0, v4, v6, v7}, LX/65J;->a_(LX/672;J)V

    .line 1048907
    int-to-long v4, v1

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    int-to-long v0, v1

    sub-long v0, v2, v0

    invoke-direct {p0, p2, v0, v1}, LX/65r;->b(IJ)V

    .line 1048908
    :cond_2
    return-void

    .line 1048909
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(IJ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1049005
    :goto_0
    cmp-long v0, p2, v6

    if-lez v0, :cond_1

    .line 1049006
    iget v0, p0, LX/65r;->e:I

    int-to-long v0, v0

    invoke-static {v0, v1, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v1, v0

    .line 1049007
    int-to-long v2, v1

    sub-long/2addr p2, v2

    .line 1049008
    const/16 v2, 0x9

    cmp-long v0, p2, v6

    if-nez v0, :cond_0

    const/4 v0, 0x4

    :goto_1
    invoke-static {p0, p1, v1, v2, v0}, LX/65r;->a(LX/65r;IIBB)V

    .line 1049009
    iget-object v0, p0, LX/65r;->b:LX/670;

    iget-object v2, p0, LX/65r;->d:LX/672;

    int-to-long v4, v1

    invoke-interface {v0, v2, v4, v5}, LX/65J;->a_(LX/672;J)V

    goto :goto_0

    .line 1049010
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1049011
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 1048998
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048999
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/65r;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 1049000
    :goto_0
    monitor-exit p0

    return-void

    .line 1049001
    :cond_1
    :try_start_2
    sget-object v0, LX/65t;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1049002
    sget-object v0, LX/65t;->a:Ljava/util/logging/Logger;

    const-string v1, ">> CONNECTION %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/65t;->b:LX/673;

    invoke-virtual {v4}, LX/673;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 1049003
    :cond_2
    iget-object v0, p0, LX/65r;->b:LX/670;

    sget-object v1, LX/65t;->b:LX/673;

    invoke-virtual {v1}, LX/673;->f()[B

    move-result-object v1

    invoke-interface {v0, v1}, LX/670;->c([B)LX/670;

    .line 1049004
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(IILjava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1048986
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048987
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/65r;->a:LX/65m;

    invoke-virtual {v0, p3}, LX/65m;->a(Ljava/util/List;)V

    .line 1048988
    iget-object v0, p0, LX/65r;->d:LX/672;

    .line 1048989
    iget-wide v8, v0, LX/672;->b:J

    move-wide v2, v8

    .line 1048990
    iget v0, p0, LX/65r;->e:I

    add-int/lit8 v0, v0, -0x4

    int-to-long v0, v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v1, v0

    .line 1048991
    int-to-long v4, v1

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v0, 0x4

    .line 1048992
    :goto_0
    add-int/lit8 v4, v1, 0x4

    const/4 v5, 0x5

    invoke-static {p0, p1, v4, v5, v0}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048993
    iget-object v0, p0, LX/65r;->b:LX/670;

    const v4, 0x7fffffff

    and-int/2addr v4, p2

    invoke-interface {v0, v4}, LX/670;->f(I)LX/670;

    .line 1048994
    iget-object v0, p0, LX/65r;->b:LX/670;

    iget-object v4, p0, LX/65r;->d:LX/672;

    int-to-long v6, v1

    invoke-interface {v0, v4, v6, v7}, LX/65J;->a_(LX/672;J)V

    .line 1048995
    int-to-long v4, v1

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    int-to-long v0, v1

    sub-long v0, v2, v0

    invoke-direct {p0, p1, v0, v1}, LX/65r;->b(IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048996
    :cond_1
    monitor-exit p0

    return-void

    .line 1048997
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(IJ)V
    .locals 4

    .prologue
    .line 1048977
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048978
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 1048979
    :cond_1
    :try_start_1
    const-string v0, "windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 1048980
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1048981
    invoke-static {v0, v1}, LX/65t;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 1048982
    :cond_2
    const/4 v0, 0x4

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048983
    iget-object v0, p0, LX/65r;->b:LX/670;

    long-to-int v1, p2

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1048984
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048985
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ILX/65X;)V
    .locals 3

    .prologue
    .line 1048971
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048972
    :cond_0
    :try_start_1
    iget v0, p2, LX/65X;->httpCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1048973
    :cond_1
    const/4 v0, 0x4

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048974
    iget-object v0, p0, LX/65r;->b:LX/670;

    iget v1, p2, LX/65X;->httpCode:I

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1048975
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048976
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ILX/65X;[B)V
    .locals 4

    .prologue
    .line 1049031
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049032
    :cond_0
    :try_start_1
    iget v0, p2, LX/65X;->httpCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string v0, "errorCode.httpCode == -1"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 1049033
    :cond_1
    array-length v0, p3

    add-int/lit8 v0, v0, 0x8

    .line 1049034
    const/4 v1, 0x0

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-static {p0, v1, v0, v2, v3}, LX/65r;->a(LX/65r;IIBB)V

    .line 1049035
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0, p1}, LX/670;->f(I)LX/670;

    .line 1049036
    iget-object v0, p0, LX/65r;->b:LX/670;

    iget v1, p2, LX/65X;->httpCode:I

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049037
    array-length v0, p3

    if-lez v0, :cond_2

    .line 1049038
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0, p3}, LX/670;->c([B)LX/670;

    .line 1049039
    :cond_2
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049040
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/663;)V
    .locals 4

    .prologue
    .line 1048950
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048951
    :cond_0
    :try_start_1
    iget v0, p0, LX/65r;->e:I

    .line 1048952
    iget v1, p1, LX/663;->a:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/663;->d:[I

    const/4 v2, 0x5

    aget v0, v1, v2

    :cond_1
    move v0, v0

    .line 1048953
    iput v0, p0, LX/65r;->e:I

    .line 1048954
    invoke-virtual {p1}, LX/663;->c()I

    move-result v0

    if-ltz v0, :cond_2

    .line 1048955
    iget-object v0, p0, LX/65r;->a:LX/65m;

    invoke-virtual {p1}, LX/663;->c()I

    move-result v1

    .line 1048956
    iput v1, v0, LX/65m;->a:I

    .line 1048957
    const/16 v2, 0x4000

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1048958
    iget v3, v0, LX/65m;->b:I

    if-ne v3, v2, :cond_3

    .line 1048959
    :cond_2
    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-static {p0, v0, v1, v2, v3}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048960
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048961
    monitor-exit p0

    return-void

    .line 1048962
    :cond_3
    iget v3, v0, LX/65m;->b:I

    if-ge v2, v3, :cond_4

    .line 1048963
    iget v3, v0, LX/65m;->h:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v0, LX/65m;->h:I

    .line 1048964
    :cond_4
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/65m;->i:Z

    .line 1048965
    iput v2, v0, LX/65m;->b:I

    .line 1048966
    iget v2, v0, LX/65m;->b:I

    iget v3, v0, LX/65m;->f:I

    if-ge v2, v3, :cond_5

    .line 1048967
    iget v2, v0, LX/65m;->b:I

    if-nez v2, :cond_6

    .line 1048968
    invoke-static {v0}, LX/65m;->a(LX/65m;)V

    .line 1048969
    :cond_5
    :goto_1
    goto :goto_0

    .line 1048970
    :cond_6
    iget v2, v0, LX/65m;->f:I

    iget v3, v0, LX/65m;->b:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, LX/65m;->b(LX/65m;I)I

    goto :goto_1
.end method

.method public final declared-synchronized a(ZII)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1048943
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/65r;->f:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048944
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 1048945
    :cond_1
    const/4 v1, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x6

    :try_start_1
    invoke-static {p0, v1, v2, v3, v0}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048946
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0, p2}, LX/670;->f(I)LX/670;

    .line 1048947
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0, p3}, LX/670;->f(I)LX/670;

    .line 1048948
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048949
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ZILX/672;I)V
    .locals 6

    .prologue
    .line 1048936
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048937
    :cond_0
    const/4 v0, 0x0

    .line 1048938
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 1048939
    :cond_1
    :try_start_1
    const/4 v2, 0x0

    invoke-static {p0, p2, p4, v2, v0}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048940
    if-lez p4, :cond_2

    .line 1048941
    iget-object v2, p0, LX/65r;->b:LX/670;

    int-to-long v4, p4

    invoke-interface {v2, p3, v4, v5}, LX/65J;->a_(LX/672;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048942
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ZZIILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZII",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1048932
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048933
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048934
    :cond_1
    invoke-direct {p0, p1, p3, p5}, LX/65r;->a(ZILjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048935
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1048929
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048930
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048931
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(LX/663;)V
    .locals 6

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 1048915
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65r;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048916
    :cond_0
    :try_start_1
    invoke-virtual {p1}, LX/663;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x6

    .line 1048917
    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static {p0, v3, v0, v4, v5}, LX/65r;->a(LX/65r;IIBB)V

    .line 1048918
    :goto_0
    const/16 v0, 0xa

    if-ge v2, v0, :cond_3

    .line 1048919
    invoke-virtual {p1, v2}, LX/663;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1048920
    if-ne v2, v1, :cond_2

    .line 1048921
    const/4 v0, 0x3

    .line 1048922
    :goto_1
    iget-object v3, p0, LX/65r;->b:LX/670;

    invoke-interface {v3, v0}, LX/670;->g(I)LX/670;

    .line 1048923
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-virtual {p1, v2}, LX/663;->b(I)I

    move-result v3

    invoke-interface {v0, v3}, LX/670;->f(I)LX/670;

    .line 1048924
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1048925
    :cond_2
    const/4 v0, 0x7

    if-ne v2, v0, :cond_4

    move v0, v1

    .line 1048926
    goto :goto_1

    .line 1048927
    :cond_3
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048928
    monitor-exit p0

    return-void

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1048914
    iget v0, p0, LX/65r;->e:I

    return v0
.end method

.method public final declared-synchronized close()V
    .locals 1

    .prologue
    .line 1048910
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/65r;->f:Z

    .line 1048911
    iget-object v0, p0, LX/65r;->b:LX/670;

    invoke-interface {v0}, LX/65J;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048912
    monitor-exit p0

    return-void

    .line 1048913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
