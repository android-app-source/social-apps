.class public final LX/6DQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLInterfaces$FbAutoFillQuery$;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6DJ;

.field public final synthetic b:LX/6DR;


# direct methods
.method public constructor <init>(LX/6DR;LX/6DJ;)V
    .locals 0

    .prologue
    .line 1064998
    iput-object p1, p0, LX/6DQ;->b:LX/6DR;

    iput-object p2, p0, LX/6DQ;->a:LX/6DJ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1065021
    iget-object v0, p0, LX/6DQ;->b:LX/6DR;

    invoke-static {v0, p1}, LX/6DR;->a$redex0(LX/6DR;Ljava/lang/Throwable;)V

    .line 1065022
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1064999
    check-cast p1, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    const/4 v7, 0x3

    const/4 v1, 0x0

    .line 1065000
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->a()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/6DQ;->a:LX/6DJ;

    if-eqz v0, :cond_9

    .line 1065001
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1065002
    invoke-virtual {p1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1065003
    invoke-virtual {v2, v5, v7}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1065004
    invoke-virtual {v2, v5, v7}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    .line 1065005
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_0

    .line 1065006
    invoke-virtual {v2, v5, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1065007
    invoke-virtual {v3, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1065008
    invoke-virtual {v2, v5, v7}, LX/15i;->o(II)LX/22e;

    move-result-object v2

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_4
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1065009
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1065010
    goto :goto_1

    .line 1065011
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1065012
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_3

    .line 1065013
    :cond_5
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1065014
    goto :goto_4

    .line 1065015
    :cond_6
    invoke-virtual {v2, v5, v7}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_5
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1065016
    :cond_7
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1065017
    goto :goto_5

    .line 1065018
    :cond_8
    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1065019
    iget-object v0, p0, LX/6DQ;->a:LX/6DJ;

    invoke-static {v3}, LX/6DS;->b(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, LX/6DQ;->b:LX/6DR;

    iget-object v2, v2, LX/6DR;->f:LX/6DS;

    invoke-virtual {v2, v3}, LX/6DS;->c(Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    invoke-static {v3}, LX/6DS;->d(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    invoke-static {v3}, LX/6DS;->e(Ljava/util/Map;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v4, v3}, LX/6DJ;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1065020
    :cond_9
    return-void
.end method
