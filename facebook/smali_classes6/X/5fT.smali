.class public final LX/5fT;
.super Landroid/view/OrientationEventListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 971660
    iput-object p1, p0, LX/5fT;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final onOrientationChanged(I)V
    .locals 2

    .prologue
    .line 971661
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971662
    invoke-virtual {v0, p1}, LX/5fQ;->a(I)V

    .line 971663
    iget-object v0, p0, LX/5fT;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-static {v0}, Lcom/facebook/optic/CameraPreviewView;->getDisplayRotation(Lcom/facebook/optic/CameraPreviewView;)I

    move-result v0

    .line 971664
    iget-object v1, p0, LX/5fT;->a:Lcom/facebook/optic/CameraPreviewView;

    iget v1, v1, Lcom/facebook/optic/CameraPreviewView;->h:I

    if-eq v0, v1, :cond_0

    .line 971665
    iget-object v1, p0, LX/5fT;->a:Lcom/facebook/optic/CameraPreviewView;

    .line 971666
    invoke-static {v1, v0}, Lcom/facebook/optic/CameraPreviewView;->a$redex0(Lcom/facebook/optic/CameraPreviewView;I)V

    .line 971667
    :cond_0
    return-void
.end method
