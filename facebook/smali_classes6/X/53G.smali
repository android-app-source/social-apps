.class public final LX/53G;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/53G;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public final b:LX/53J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53J",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:LX/53H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53H",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile d:I

.field public f:I

.field public final g:I

.field public final h:LX/53n;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826808
    const-class v0, LX/53G;

    const-string v1, "d"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53G;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/53J;LX/53H;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53J",
            "<TT;>;",
            "LX/53H",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826795
    invoke-direct {p0}, LX/0zZ;-><init>()V

    .line 826796
    invoke-static {}, LX/53n;->d()LX/53n;

    move-result-object v0

    iput-object v0, p0, LX/53G;->h:LX/53n;

    .line 826797
    const/4 v0, 0x0

    iput v0, p0, LX/53G;->f:I

    .line 826798
    iget-object v0, p0, LX/53G;->h:LX/53n;

    .line 826799
    iget v1, v0, LX/53n;->f:I

    move v0, v1

    .line 826800
    int-to-double v0, v0

    const-wide v2, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, LX/53G;->g:I

    .line 826801
    iput-object p1, p0, LX/53G;->b:LX/53J;

    .line 826802
    iput-object p2, p0, LX/53G;->c:LX/53H;

    .line 826803
    iget-object v0, p0, LX/53G;->h:LX/53n;

    invoke-virtual {p0, v0}, LX/0zZ;->a(LX/0za;)V

    .line 826804
    iget-object v0, p0, LX/53G;->h:LX/53n;

    .line 826805
    iget v1, v0, LX/53n;->f:I

    move v0, v1

    .line 826806
    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/0zZ;->a(J)V

    .line 826807
    return-void
.end method

.method private a(Ljava/lang/Object;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 826809
    iget-object v0, p0, LX/53G;->b:LX/53J;

    .line 826810
    invoke-static {v0}, LX/53J;->g$redex0(LX/53J;)Z

    move-result v3

    move v0, v3

    .line 826811
    if-eqz v0, :cond_7

    .line 826812
    :try_start_0
    iget v0, p0, LX/53G;->f:I

    invoke-static {p0}, LX/53G;->g(LX/53G;)I

    move-result v3

    add-int/2addr v0, v3

    iput v0, p0, LX/53G;->f:I

    .line 826813
    iget-object v0, p0, LX/53G;->c:LX/53H;

    if-nez v0, :cond_3

    .line 826814
    if-eqz p2, :cond_2

    .line 826815
    iget-object v0, p0, LX/53G;->b:LX/53J;

    invoke-virtual {v0, p0}, LX/53J;->a(LX/53G;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 826816
    :goto_0
    iget-object v3, p0, LX/53G;->b:LX/53J;

    invoke-static {v3}, LX/53J;->g(LX/53J;)Z

    move-result v3

    .line 826817
    iget v4, p0, LX/53G;->f:I

    iget v5, p0, LX/53G;->g:I

    if-le v4, v5, :cond_0

    .line 826818
    iget v4, p0, LX/53G;->f:I

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, LX/0zZ;->a(J)V

    .line 826819
    iput v1, p0, LX/53G;->f:I

    :cond_0
    move v1, v0

    move v0, v3

    .line 826820
    :goto_1
    if-eqz v1, :cond_6

    .line 826821
    if-eqz p2, :cond_8

    .line 826822
    :try_start_1
    iget-object v0, p0, LX/53G;->h:LX/53n;

    invoke-virtual {v0}, LX/53n;->e()V
    :try_end_1
    .catch LX/52z; {:try_start_1 .. :try_end_1} :catch_2

    .line 826823
    :goto_2
    if-eqz v2, :cond_1

    .line 826824
    iget-object v0, p0, LX/53G;->b:LX/53J;

    invoke-static {v0}, LX/53J;->h(LX/53J;)Z

    .line 826825
    :cond_1
    return-void

    .line 826826
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/53G;->b:LX/53J;

    iget-object v0, v0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 826827
    :goto_3
    :try_start_3
    iget v0, p0, LX/53G;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/53G;->f:I

    move v0, v1

    goto :goto_0

    .line 826828
    :catch_0
    move-exception v0

    .line 826829
    invoke-static {v0, p1}, LX/533;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/53G;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 826830
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/53G;->b:LX/53J;

    invoke-static {v1}, LX/53J;->g(LX/53J;)Z

    throw v0

    .line 826831
    :cond_3
    :try_start_4
    iget-object v0, p0, LX/53G;->c:LX/53H;

    iget-wide v4, v0, LX/53H;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    iget-object v0, p0, LX/53G;->h:LX/53n;

    .line 826832
    iget-object v3, v0, LX/53n;->e:Ljava/util/Queue;

    if-nez v3, :cond_9

    .line 826833
    const/4 v3, 0x0

    .line 826834
    :goto_4
    move v0, v3

    .line 826835
    if-nez v0, :cond_5

    .line 826836
    if-eqz p2, :cond_4

    .line 826837
    iget-object v0, p0, LX/53G;->b:LX/53J;

    invoke-virtual {v0, p0}, LX/53J;->a(LX/53G;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    goto :goto_0

    .line 826838
    :cond_4
    :try_start_5
    iget-object v0, p0, LX/53G;->b:LX/53J;

    iget-object v0, v0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 826839
    :goto_5
    :try_start_6
    iget v0, p0, LX/53G;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/53G;->f:I

    .line 826840
    sget-object v0, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    iget-object v3, p0, LX/53G;->c:LX/53H;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->decrementAndGet(Ljava/lang/Object;)J

    move v0, v1

    goto :goto_0

    .line 826841
    :catch_1
    move-exception v0

    .line 826842
    invoke-static {v0, p1}, LX/533;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/53G;->a(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    :cond_5
    move v0, v2

    .line 826843
    goto/16 :goto_0

    :cond_6
    move v2, v0

    goto :goto_2

    :cond_7
    move v0, v1

    move v1, v2

    goto :goto_1

    .line 826844
    :cond_8
    :try_start_7
    iget-object v0, p0, LX/53G;->h:LX/53n;

    invoke-virtual {v0, p1}, LX/53n;->a(Ljava/lang/Object;)V
    :try_end_7
    .catch LX/52z; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_2

    .line 826845
    :catch_2
    move-exception v0

    .line 826846
    invoke-virtual {p0, v0}, LX/53G;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_9
    iget-object v3, v0, LX/53n;->e:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    goto :goto_4
.end method

.method public static g(LX/53G;)I
    .locals 9

    .prologue
    .line 826769
    iget-object v0, p0, LX/53G;->c:LX/53H;

    if-eqz v0, :cond_3

    .line 826770
    const/4 v1, 0x0

    .line 826771
    iget-object v2, p0, LX/53G;->c:LX/53H;

    iget-wide v5, v2, LX/53H;->c:J

    move v3, v1

    .line 826772
    :goto_0
    int-to-long v7, v3

    cmp-long v2, v7, v5

    if-gez v2, :cond_2

    .line 826773
    iget-object v2, p0, LX/53G;->h:LX/53n;

    invoke-virtual {v2}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v4

    .line 826774
    if-eqz v4, :cond_2

    .line 826775
    invoke-static {v4}, LX/0vH;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 826776
    iget-object v2, p0, LX/53G;->b:LX/53J;

    invoke-virtual {v2, p0}, LX/53J;->a(LX/53G;)V

    .line 826777
    :cond_0
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 826778
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/53G;->b:LX/53J;

    iget-object v2, v2, LX/53J;->b:LX/0zZ;

    invoke-static {v4, v2}, LX/53n;->a(Ljava/lang/Object;LX/0vA;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 826779
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 826780
    :catch_0
    move-exception v2

    .line 826781
    invoke-static {v2, v4}, LX/533;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/53G;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 826782
    :cond_2
    sget-object v2, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    iget-object v3, p0, LX/53G;->c:LX/53H;

    neg-int v4, v1

    int-to-long v5, v4

    invoke-virtual {v2, v3, v5, v6}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 826783
    move v0, v1

    .line 826784
    :goto_2
    return v0

    .line 826785
    :cond_3
    const/4 v0, 0x0

    .line 826786
    :cond_4
    :goto_3
    iget-object v1, p0, LX/53G;->h:LX/53n;

    invoke-virtual {v1}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 826787
    invoke-static {v2}, LX/0vH;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 826788
    iget-object v1, p0, LX/53G;->b:LX/53J;

    invoke-virtual {v1, p0}, LX/53J;->a(LX/53G;)V

    goto :goto_3

    .line 826789
    :cond_5
    :try_start_1
    iget-object v1, p0, LX/53G;->b:LX/53J;

    iget-object v1, v1, LX/53J;->b:LX/0zZ;

    invoke-static {v2, v1}, LX/53n;->a(Ljava/lang/Object;LX/0vA;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-nez v1, :cond_4

    .line 826790
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 826791
    :catch_1
    move-exception v1

    .line 826792
    invoke-static {v1, v2}, LX/533;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/53G;->a(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 826793
    :cond_6
    move v0, v0

    .line 826794
    goto :goto_2
.end method


# virtual methods
.method public final R_()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 826766
    sget-object v0, LX/53G;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826767
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, LX/53G;->a(Ljava/lang/Object;Z)V

    .line 826768
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 826764
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/53G;->a(Ljava/lang/Object;Z)V

    .line 826765
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 826761
    sget-object v0, LX/53G;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v2, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826762
    iget-object v0, p0, LX/53G;->b:LX/53J;

    invoke-static {v0, p1, v2}, LX/53J;->a$redex0(LX/53J;Ljava/lang/Throwable;Z)V

    .line 826763
    :cond_0
    return-void
.end method
