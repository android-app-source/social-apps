.class public final LX/5pF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/ReadableMapKeySetIterator;


# instance fields
.field public a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/5pI;


# direct methods
.method public constructor <init>(LX/5pI;)V
    .locals 1

    .prologue
    .line 1007946
    iput-object p1, p0, LX/5pF;->b:LX/5pI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007947
    iget-object v0, p0, LX/5pF;->b:LX/5pI;

    iget-object v0, v0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/5pF;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNextKey()Z
    .locals 1

    .prologue
    .line 1007948
    iget-object v0, p0, LX/5pF;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final nextKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007949
    iget-object v0, p0, LX/5pF;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
