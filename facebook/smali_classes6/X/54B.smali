.class public abstract LX/54B;
.super LX/53v;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/53v",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/Integer;


# instance fields
.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 827774
    const-string v0, "jctoolts.spsc.max.lookahead.step"

    const/16 v1, 0x1000

    invoke-static {v0, v1}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/54B;->f:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 827775
    invoke-direct {p0, p1}, LX/53v;-><init>(I)V

    .line 827776
    div-int/lit8 v0, p1, 0x4

    sget-object v1, LX/54B;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/54B;->e:I

    .line 827777
    return-void
.end method
