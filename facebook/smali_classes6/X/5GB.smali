.class public final LX/5GB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 887295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 887296
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887297
    :goto_0
    return v1

    .line 887298
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887299
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 887300
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 887301
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 887302
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 887303
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 887304
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 887305
    :cond_2
    const-string v6, "location"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 887306
    invoke-static {p0, p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 887307
    :cond_3
    const-string v6, "map_bounding_box"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 887308
    invoke-static {p0, p1}, LX/5GA;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 887309
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 887310
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 887311
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 887312
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 887313
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 887314
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 887315
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 887316
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 887276
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887277
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887278
    if-eqz v0, :cond_0

    .line 887279
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887280
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887281
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887282
    if-eqz v0, :cond_1

    .line 887283
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887284
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 887285
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887286
    if-eqz v0, :cond_2

    .line 887287
    const-string v1, "map_bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887288
    invoke-static {p0, v0, p2}, LX/5GA;->a(LX/15i;ILX/0nX;)V

    .line 887289
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887290
    if-eqz v0, :cond_3

    .line 887291
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887292
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887293
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887294
    return-void
.end method
