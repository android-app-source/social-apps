.class public LX/6PT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1086166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086167
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6PT;->a:Ljava/lang/String;

    .line 1086168
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6PT;->b:Ljava/lang/String;

    .line 1086169
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/6PT;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1086170
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/6PT;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1086171
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086164
    iget-object v0, p0, LX/6PT;->a:Ljava/lang/String;

    iget-object v1, p0, LX/6PT;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086172
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/4Yr;

    .line 1086173
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/6PT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086174
    :cond_0
    :goto_0
    return-void

    .line 1086175
    :cond_1
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1086176
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/6PT;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086177
    iget-object v0, p0, LX/6PT;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1086178
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p1, "title"

    invoke-virtual {v1, p1, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086179
    iget-object v0, p0, LX/6PT;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1086180
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p1, "titleFromRenderLocation"

    invoke-virtual {v1, p1, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086181
    iget-object v0, p0, LX/6PT;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4Yr;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086165
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086163
    const-string v0, "LiveVideoMutatingVisitor"

    return-object v0
.end method
