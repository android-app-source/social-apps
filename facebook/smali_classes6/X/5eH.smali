.class public LX/5eH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1aD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/1aD",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Nt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nt",
            "<TP;TS;TE;TV;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5eH;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method public constructor <init>(LX/1Nt;ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Nt",
            "<TP;TS;TE;TV;>;ITP;)V"
        }
    .end annotation

    .prologue
    .line 966869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966870
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5eH;->c:Ljava/util/List;

    .line 966871
    iput-object p1, p0, LX/5eH;->a:LX/1Nt;

    .line 966872
    iput p2, p0, LX/5eH;->e:I

    .line 966873
    iput-object p3, p0, LX/5eH;->b:Ljava/lang/Object;

    .line 966874
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V2:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            ")TV2;"
        }
    .end annotation

    .prologue
    .line 966875
    iget v0, p0, LX/5eH;->e:I

    invoke-static {p1, v0}, LX/1aP;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILX/1Nt;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            ">(I",
            "LX/1Nt",
            "<TP2;*-TE;+",
            "Landroid/view/View;",
            ">;TP2;)V"
        }
    .end annotation

    .prologue
    .line 966876
    iget-object v0, p0, LX/5eH;->c:Ljava/util/List;

    new-instance v1, LX/5eH;

    invoke-direct {v1, p2, p1, p3}, LX/5eH;-><init>(LX/1Nt;ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 966877
    return-void
.end method

.method public final a(LX/1Nt;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1Nt",
            "<TP2;*-TE;+",
            "Landroid/view/View;",
            ">;TP2;)V"
        }
    .end annotation

    .prologue
    .line 966878
    iget-object v0, p0, LX/5eH;->c:Ljava/util/List;

    new-instance v1, LX/5eH;

    const/4 v2, -0x1

    invoke-direct {v1, p1, v2, p2}, LX/5eH;-><init>(LX/1Nt;ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 966879
    return-void
.end method
