.class public final enum LX/6FV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6FV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6FV;

.field public static final enum ADD_CONTACT:LX/6FV;

.field public static final enum CALL_TAB:LX/6FV;

.field public static final enum COMPOSE_MESSAGE_FLOW:LX/6FV;

.field public static final enum CREATE_GROUP_FLOW:LX/6FV;

.field public static final enum GROUPS_TAB:LX/6FV;

.field public static final enum GROUP_CALL:LX/6FV;

.field public static final enum INVITE_FLOW:LX/6FV;

.field public static final enum LIGHTWEIGHT_ACTIONS:LX/6FV;

.field public static final enum LIKE_BUTTON:LX/6FV;

.field public static final enum MEDIA_PICKER:LX/6FV;

.field public static final enum MEDIA_TRAY:LX/6FV;

.field public static final enum MUTE_ACTION:LX/6FV;

.field public static final enum NULL_CATEGORY:LX/6FV;

.field public static final enum P2P:LX/6FV;

.field public static final enum PEOPLE_TAB:LX/6FV;

.field public static final enum PLATFORM:LX/6FV;

.field public static final enum QUICK_CAM:LX/6FV;

.field public static final enum RECENTS_TAB:LX/6FV;

.field public static final enum ROOMS_TAB:LX/6FV;

.field public static final enum SEARCH:LX/6FV;

.field public static final enum SETTINGS_TAB:LX/6FV;

.field public static final enum STICKERS:LX/6FV;

.field public static final enum THREAD_SETTINGS:LX/6FV;

.field public static final enum VIDEO_CALL:LX/6FV;

.field public static final enum VOICE_CLIPS:LX/6FV;

.field public static final enum VOIP_CALL:LX/6FV;


# instance fields
.field private final mCategoryName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1068107
    new-instance v0, LX/6FV;

    const-string v1, "NULL_CATEGORY"

    const-string v2, "Not Inspected"

    invoke-direct {v0, v1, v4, v2}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->NULL_CATEGORY:LX/6FV;

    .line 1068108
    new-instance v0, LX/6FV;

    const-string v1, "RECENTS_TAB"

    const-string v2, "Recents Tab"

    invoke-direct {v0, v1, v5, v2}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->RECENTS_TAB:LX/6FV;

    .line 1068109
    new-instance v0, LX/6FV;

    const-string v1, "CALL_TAB"

    const-string v2, "Call Tab"

    invoke-direct {v0, v1, v6, v2}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->CALL_TAB:LX/6FV;

    .line 1068110
    new-instance v0, LX/6FV;

    const-string v1, "GROUPS_TAB"

    const-string v2, "Groups Tab"

    invoke-direct {v0, v1, v7, v2}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->GROUPS_TAB:LX/6FV;

    .line 1068111
    new-instance v0, LX/6FV;

    const-string v1, "PEOPLE_TAB"

    const-string v2, "People Tab"

    invoke-direct {v0, v1, v8, v2}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->PEOPLE_TAB:LX/6FV;

    .line 1068112
    new-instance v0, LX/6FV;

    const-string v1, "SETTINGS_TAB"

    const/4 v2, 0x5

    const-string v3, "Settings Tab"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->SETTINGS_TAB:LX/6FV;

    .line 1068113
    new-instance v0, LX/6FV;

    const-string v1, "ROOMS_TAB"

    const/4 v2, 0x6

    const-string v3, "Rooms Tab"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->ROOMS_TAB:LX/6FV;

    .line 1068114
    new-instance v0, LX/6FV;

    const-string v1, "COMPOSE_MESSAGE_FLOW"

    const/4 v2, 0x7

    const-string v3, "Compose Message Flow"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->COMPOSE_MESSAGE_FLOW:LX/6FV;

    .line 1068115
    new-instance v0, LX/6FV;

    const-string v1, "CREATE_GROUP_FLOW"

    const/16 v2, 0x8

    const-string v3, "Create Group Flow"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->CREATE_GROUP_FLOW:LX/6FV;

    .line 1068116
    new-instance v0, LX/6FV;

    const-string v1, "INVITE_FLOW"

    const/16 v2, 0x9

    const-string v3, "Invite Flow"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->INVITE_FLOW:LX/6FV;

    .line 1068117
    new-instance v0, LX/6FV;

    const-string v1, "QUICK_CAM"

    const/16 v2, 0xa

    const-string v3, "QuickCam"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->QUICK_CAM:LX/6FV;

    .line 1068118
    new-instance v0, LX/6FV;

    const-string v1, "MEDIA_TRAY"

    const/16 v2, 0xb

    const-string v3, "Media Tray"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->MEDIA_TRAY:LX/6FV;

    .line 1068119
    new-instance v0, LX/6FV;

    const-string v1, "MEDIA_PICKER"

    const/16 v2, 0xc

    const-string v3, "Media Picker"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->MEDIA_PICKER:LX/6FV;

    .line 1068120
    new-instance v0, LX/6FV;

    const-string v1, "STICKERS"

    const/16 v2, 0xd

    const-string v3, "Stickers"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->STICKERS:LX/6FV;

    .line 1068121
    new-instance v0, LX/6FV;

    const-string v1, "LIGHTWEIGHT_ACTIONS"

    const/16 v2, 0xe

    const-string v3, "Lightweight Actions"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->LIGHTWEIGHT_ACTIONS:LX/6FV;

    .line 1068122
    new-instance v0, LX/6FV;

    const-string v1, "VOICE_CLIPS"

    const/16 v2, 0xf

    const-string v3, "Voice Clips"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->VOICE_CLIPS:LX/6FV;

    .line 1068123
    new-instance v0, LX/6FV;

    const-string v1, "P2P"

    const/16 v2, 0x10

    const-string v3, "P2P"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->P2P:LX/6FV;

    .line 1068124
    new-instance v0, LX/6FV;

    const-string v1, "PLATFORM"

    const/16 v2, 0x11

    const-string v3, "Platform"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->PLATFORM:LX/6FV;

    .line 1068125
    new-instance v0, LX/6FV;

    const-string v1, "LIKE_BUTTON"

    const/16 v2, 0x12

    const-string v3, "Like Button"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->LIKE_BUTTON:LX/6FV;

    .line 1068126
    new-instance v0, LX/6FV;

    const-string v1, "VOIP_CALL"

    const/16 v2, 0x13

    const-string v3, "VoIP Call"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->VOIP_CALL:LX/6FV;

    .line 1068127
    new-instance v0, LX/6FV;

    const-string v1, "VIDEO_CALL"

    const/16 v2, 0x14

    const-string v3, "Video Call"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->VIDEO_CALL:LX/6FV;

    .line 1068128
    new-instance v0, LX/6FV;

    const-string v1, "GROUP_CALL"

    const/16 v2, 0x15

    const-string v3, "Group Call"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->GROUP_CALL:LX/6FV;

    .line 1068129
    new-instance v0, LX/6FV;

    const-string v1, "THREAD_SETTINGS"

    const/16 v2, 0x16

    const-string v3, "Thread Settings"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->THREAD_SETTINGS:LX/6FV;

    .line 1068130
    new-instance v0, LX/6FV;

    const-string v1, "MUTE_ACTION"

    const/16 v2, 0x17

    const-string v3, "Mute Action"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->MUTE_ACTION:LX/6FV;

    .line 1068131
    new-instance v0, LX/6FV;

    const-string v1, "ADD_CONTACT"

    const/16 v2, 0x18

    const-string v3, "Add Contact"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->ADD_CONTACT:LX/6FV;

    .line 1068132
    new-instance v0, LX/6FV;

    const-string v1, "SEARCH"

    const/16 v2, 0x19

    const-string v3, "Search"

    invoke-direct {v0, v1, v2, v3}, LX/6FV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6FV;->SEARCH:LX/6FV;

    .line 1068133
    const/16 v0, 0x1a

    new-array v0, v0, [LX/6FV;

    sget-object v1, LX/6FV;->NULL_CATEGORY:LX/6FV;

    aput-object v1, v0, v4

    sget-object v1, LX/6FV;->RECENTS_TAB:LX/6FV;

    aput-object v1, v0, v5

    sget-object v1, LX/6FV;->CALL_TAB:LX/6FV;

    aput-object v1, v0, v6

    sget-object v1, LX/6FV;->GROUPS_TAB:LX/6FV;

    aput-object v1, v0, v7

    sget-object v1, LX/6FV;->PEOPLE_TAB:LX/6FV;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6FV;->SETTINGS_TAB:LX/6FV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6FV;->ROOMS_TAB:LX/6FV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6FV;->COMPOSE_MESSAGE_FLOW:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6FV;->CREATE_GROUP_FLOW:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6FV;->INVITE_FLOW:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6FV;->QUICK_CAM:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6FV;->MEDIA_TRAY:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6FV;->MEDIA_PICKER:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6FV;->STICKERS:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6FV;->LIGHTWEIGHT_ACTIONS:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6FV;->VOICE_CLIPS:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6FV;->P2P:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/6FV;->PLATFORM:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/6FV;->LIKE_BUTTON:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/6FV;->VOIP_CALL:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/6FV;->VIDEO_CALL:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/6FV;->GROUP_CALL:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/6FV;->THREAD_SETTINGS:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/6FV;->MUTE_ACTION:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/6FV;->ADD_CONTACT:LX/6FV;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/6FV;->SEARCH:LX/6FV;

    aput-object v2, v0, v1

    sput-object v0, LX/6FV;->$VALUES:[LX/6FV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1068101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1068102
    iput-object p3, p0, LX/6FV;->mCategoryName:Ljava/lang/String;

    .line 1068103
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6FV;
    .locals 1

    .prologue
    .line 1068106
    const-class v0, LX/6FV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6FV;

    return-object v0
.end method

.method public static values()[LX/6FV;
    .locals 1

    .prologue
    .line 1068105
    sget-object v0, LX/6FV;->$VALUES:[LX/6FV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6FV;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068104
    iget-object v0, p0, LX/6FV;->mCategoryName:Ljava/lang/String;

    return-object v0
.end method
