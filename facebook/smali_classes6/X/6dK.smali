.class public final LX/6dK;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/6dL;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1115761
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1115762
    sget-object v0, LX/6cw;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->b:I

    .line 1115763
    sget-object v0, LX/6cw;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->c:I

    .line 1115764
    sget-object v0, LX/6do;->e:LX/0U1;

    .line 1115765
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115766
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->d:I

    .line 1115767
    sget-object v0, LX/6cw;->c:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->e:I

    .line 1115768
    sget-object v0, LX/6cw;->d:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->f:I

    .line 1115769
    sget-object v0, LX/6cw;->e:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->g:I

    .line 1115770
    sget-object v0, LX/6do;->i:LX/0U1;

    .line 1115771
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115772
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->h:I

    .line 1115773
    sget-object v0, LX/6cw;->f:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->i:I

    .line 1115774
    sget-object v0, LX/6cw;->g:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->j:I

    .line 1115775
    sget-object v0, LX/6cw;->h:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->k:I

    .line 1115776
    sget-object v0, LX/6cw;->i:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->l:I

    .line 1115777
    sget-object v0, LX/6cw;->j:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->m:I

    .line 1115778
    sget-object v0, LX/6cw;->k:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->n:I

    .line 1115779
    sget-object v0, LX/6cw;->l:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dK;->o:I

    .line 1115780
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1115728
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1115729
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v1, p0, LX/6dK;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v9

    .line 1115730
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v1, p0, LX/6dK;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 1115731
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v2, p0, LX/6dK;->i:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6cv;->fromDbValue(Ljava/lang/String;)LX/6cv;

    move-result-object v10

    .line 1115732
    sget-object v0, LX/6cv;->REQUEST:LX/6cv;

    if-ne v10, v0, :cond_0

    .line 1115733
    new-instance v0, LX/6dL;

    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->o:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {v0, v9, v1, v2, v3}, LX/6dL;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;J)V

    .line 1115734
    :goto_0
    return-object v0

    .line 1115735
    :cond_0
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v2, p0, LX/6dK;->d:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1115736
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->e:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1115737
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v4, p0, LX/6dK;->f:I

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1115738
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v5, p0, LX/6dK;->g:I

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115739
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v6, p0, LX/6dK;->h:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v6, v7

    .line 1115740
    :goto_1
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1115741
    new-instance v1, LX/6fz;

    invoke-direct {v1}, LX/6fz;-><init>()V

    .line 1115742
    iput-object v0, v1, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1115743
    move-object v0, v1

    .line 1115744
    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->m:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1115745
    iput-wide v2, v0, LX/6fz;->e:J

    .line 1115746
    move-object v0, v0

    .line 1115747
    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->k:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1115748
    iput-wide v2, v0, LX/6fz;->b:J

    .line 1115749
    move-object v0, v0

    .line 1115750
    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->l:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1115751
    iput-wide v2, v0, LX/6fz;->c:J

    .line 1115752
    move-object v0, v0

    .line 1115753
    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->n:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1115754
    iput-object v2, v0, LX/6fz;->d:Ljava/lang/String;

    .line 1115755
    move-object v0, v0

    .line 1115756
    iget-object v2, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v3, p0, LX/6dK;->j:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v7, :cond_2

    .line 1115757
    :goto_2
    iput-boolean v7, v0, LX/6fz;->f:Z

    .line 1115758
    new-instance v0, LX/6dL;

    invoke-virtual {v1}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v1

    invoke-direct {v0, v9, v1, v10}, LX/6dL;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadParticipant;LX/6cv;)V

    goto :goto_0

    :cond_1
    move v6, v8

    .line 1115759
    goto :goto_1

    :cond_2
    move v7, v8

    .line 1115760
    goto :goto_2
.end method
