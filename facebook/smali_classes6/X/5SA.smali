.class public final enum LX/5SA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5SA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5SA;

.field public static final enum PAGE:LX/5SA;

.field public static final enum PAGE_IDENTITY:LX/5SA;

.field public static final enum USER:LX/5SA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 917584
    new-instance v0, LX/5SA;

    const-string v1, "USER"

    invoke-direct {v0, v1, v2}, LX/5SA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SA;->USER:LX/5SA;

    .line 917585
    new-instance v0, LX/5SA;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v3}, LX/5SA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SA;->PAGE:LX/5SA;

    .line 917586
    new-instance v0, LX/5SA;

    const-string v1, "PAGE_IDENTITY"

    invoke-direct {v0, v1, v4}, LX/5SA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SA;->PAGE_IDENTITY:LX/5SA;

    .line 917587
    const/4 v0, 0x3

    new-array v0, v0, [LX/5SA;

    sget-object v1, LX/5SA;->USER:LX/5SA;

    aput-object v1, v0, v2

    sget-object v1, LX/5SA;->PAGE:LX/5SA;

    aput-object v1, v0, v3

    sget-object v1, LX/5SA;->PAGE_IDENTITY:LX/5SA;

    aput-object v1, v0, v4

    sput-object v0, LX/5SA;->$VALUES:[LX/5SA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 917588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5SA;
    .locals 1

    .prologue
    .line 917589
    const-class v0, LX/5SA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SA;

    return-object v0
.end method

.method public static values()[LX/5SA;
    .locals 1

    .prologue
    .line 917590
    sget-object v0, LX/5SA;->$VALUES:[LX/5SA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5SA;

    return-object v0
.end method


# virtual methods
.method public final isPageTimeline()Z
    .locals 1

    .prologue
    .line 917591
    sget-object v0, LX/5SA;->PAGE:LX/5SA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5SA;->PAGE_IDENTITY:LX/5SA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
