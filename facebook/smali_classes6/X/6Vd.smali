.class public LX/6Vd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1DZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1DZ",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0qn;


# direct methods
.method public constructor <init>(LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1104361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104362
    iput-object p1, p0, LX/6Vd;->a:LX/0qn;

    .line 1104363
    return-void
.end method

.method public static a(LX/0QB;)LX/6Vd;
    .locals 2

    .prologue
    .line 1104358
    new-instance v1, LX/6Vd;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-direct {v1, v0}, LX/6Vd;-><init>(LX/0qn;)V

    .line 1104359
    move-object v0, v1

    .line 1104360
    return-object v0
.end method

.method public static a(LX/6Vd;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1104364
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 1104365
    :cond_0
    :goto_0
    return v0

    .line 1104366
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1104367
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1104368
    iget-object v1, p0, LX/6Vd;->a:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/6Vd;->a:LX/0qn;

    invoke-virtual {v1, p2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    .line 1104369
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1104341
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1104342
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104343
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1104344
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1104345
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1104346
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1104347
    instance-of v4, v0, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    move v4, v4

    .line 1104348
    if-nez v4, :cond_0

    .line 1104349
    instance-of v4, v1, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    move v4, v4

    .line 1104350
    if-eqz v4, :cond_3

    .line 1104351
    :cond_0
    if-ne v1, v0, :cond_2

    move v0, v3

    .line 1104352
    :goto_0
    move v0, v0

    .line 1104353
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1104354
    goto :goto_0

    .line 1104355
    :cond_3
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1104356
    invoke-static {p0, v0, v1}, LX/6Vd;->a(LX/6Vd;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    goto :goto_0

    .line 1104357
    :cond_4
    if-eq v0, v1, :cond_5

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v3

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 5

    .prologue
    .line 1104326
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104327
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v2

    .line 1104328
    :goto_0
    return v0

    .line 1104329
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1104330
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1104331
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1104332
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1104333
    instance-of v4, v0, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    move v4, v4

    .line 1104334
    if-nez v4, :cond_2

    .line 1104335
    instance-of v4, v1, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    move v4, v4

    .line 1104336
    if-eqz v4, :cond_3

    :cond_2
    move v0, v3

    .line 1104337
    goto :goto_0

    .line 1104338
    :cond_3
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    move v0, v2

    .line 1104339
    goto :goto_0

    .line 1104340
    :cond_4
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-nez v0, :cond_5

    move v0, v3

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method
