.class public final LX/6Up;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6Uo;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6Uo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1102926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1102927
    iput-object p1, p0, LX/6Up;->a:LX/0QB;

    .line 1102928
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1102929
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6Up;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1102930
    packed-switch p2, :pswitch_data_0

    .line 1102931
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1102932
    :pswitch_0
    invoke-static {p1}, LX/6Ur;->a(LX/0QB;)LX/6Ur;

    move-result-object v0

    .line 1102933
    :goto_0
    return-object v0

    .line 1102934
    :pswitch_1
    invoke-static {p1}, LX/6Us;->a(LX/0QB;)LX/6Us;

    move-result-object v0

    goto :goto_0

    .line 1102935
    :pswitch_2
    invoke-static {p1}, LX/6Ut;->a(LX/0QB;)LX/6Ut;

    move-result-object v0

    goto :goto_0

    .line 1102936
    :pswitch_3
    invoke-static {p1}, LX/6Uu;->a(LX/0QB;)LX/6Uu;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1102937
    const/4 v0, 0x4

    return v0
.end method
