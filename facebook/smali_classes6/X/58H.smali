.class public final LX/58H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 846512
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 846513
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846514
    :goto_0
    return v1

    .line 846515
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846516
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 846517
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 846518
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 846519
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 846520
    const-string v3, "photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 846521
    const/4 v2, 0x0

    .line 846522
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 846523
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846524
    :goto_2
    move v0, v2

    .line 846525
    goto :goto_1

    .line 846526
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 846527
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 846528
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 846529
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846530
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 846531
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 846532
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 846533
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 846534
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 846535
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_3

    .line 846536
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 846537
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 846538
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 846539
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 846540
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 846541
    if-eqz v0, :cond_1

    .line 846542
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846543
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 846544
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 846545
    if-eqz v1, :cond_0

    .line 846546
    const-string p1, "image"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846547
    invoke-static {p0, v1, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 846548
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 846549
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 846550
    return-void
.end method
