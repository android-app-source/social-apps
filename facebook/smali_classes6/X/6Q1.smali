.class public LX/6Q1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0lC;

.field private final c:LX/6Qw;

.field private final d:Landroid/content/pm/PackageManager;

.field private final e:LX/0kb;

.field private final f:LX/1sd;

.field private final g:LX/2y8;

.field private final h:LX/6VX;

.field private final i:LX/0Uh;

.field public final j:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1086591
    const-class v0, LX/6Q1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Q1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/6Qw;Landroid/content/pm/PackageManager;LX/0kb;LX/1sd;LX/2y8;LX/6VX;LX/0Uh;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086593
    iput-object p1, p0, LX/6Q1;->b:LX/0lC;

    .line 1086594
    iput-object p2, p0, LX/6Q1;->c:LX/6Qw;

    .line 1086595
    iput-object p3, p0, LX/6Q1;->d:Landroid/content/pm/PackageManager;

    .line 1086596
    iput-object p4, p0, LX/6Q1;->e:LX/0kb;

    .line 1086597
    iput-object p5, p0, LX/6Q1;->f:LX/1sd;

    .line 1086598
    iput-object p6, p0, LX/6Q1;->g:LX/2y8;

    .line 1086599
    iput-object p7, p0, LX/6Q1;->h:LX/6VX;

    .line 1086600
    iput-object p8, p0, LX/6Q1;->i:LX/0Uh;

    .line 1086601
    iput-object p9, p0, LX/6Q1;->j:LX/0W3;

    .line 1086602
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/47F;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086603
    new-instance v0, LX/6Pz;

    invoke-direct {v0}, LX/6Pz;-><init>()V

    invoke-static {p0, v0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/6Q1;Landroid/content/Context;LX/47G;LX/0P1;)Landroid/content/Intent;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/47G;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 1086604
    iget-object v1, p0, LX/6Q1;->g:LX/2y8;

    invoke-virtual {v1}, LX/2y8;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6Q1;->f:LX/1sd;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, LX/1sd;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1086605
    :cond_0
    :goto_0
    return-object v0

    .line 1086606
    :cond_1
    invoke-virtual {p2}, LX/47G;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1086607
    iget-object v1, p0, LX/6Q1;->c:LX/6Qw;

    const-string v2, "application_data_not_valid"

    invoke-virtual {v1, p2, v2}, LX/6Qw;->a(LX/47G;Ljava/lang/String;)V

    goto :goto_0

    .line 1086608
    :cond_2
    iget-object v1, p2, LX/47G;->e:Ljava/lang/String;

    invoke-direct {p0, v1}, LX/6Q1;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1086609
    iget-object v1, p0, LX/6Q1;->c:LX/6Qw;

    const-string v2, "neko_di_app_already_installed"

    invoke-virtual {v1, v2, p2, v0}, LX/6Qw;->a(Ljava/lang/String;LX/47G;LX/0P1;)V

    goto :goto_0

    .line 1086610
    :cond_3
    iget-object v1, p0, LX/6Q1;->e:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1086611
    iget-object v1, p0, LX/6Q1;->c:LX/6Qw;

    const-string v2, "network_not_connected"

    invoke-virtual {v1, p2, v2}, LX/6Qw;->a(LX/47G;Ljava/lang/String;)V

    goto :goto_0

    .line 1086612
    :cond_4
    invoke-direct {p0, p2}, LX/6Q1;->c(LX/47G;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1086613
    const-string v0, "cta_click"

    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "cta_click"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1086614
    if-nez v0, :cond_5

    iget-object v0, p0, LX/6Q1;->i:LX/0Uh;

    const/16 v1, 0x329

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1086615
    :cond_5
    const-class v1, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, LX/6Q1;->a(Ljava/lang/Class;Landroid/content/Context;LX/47G;LX/0P1;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1086616
    :cond_6
    const-class v1, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, LX/6Q1;->a(Ljava/lang/Class;Landroid/content/Context;LX/47G;LX/0P1;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1086617
    :cond_7
    const-class v1, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, LX/6Q1;->a(Ljava/lang/Class;Landroid/content/Context;LX/47G;LX/0P1;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/47G;)Lcom/facebook/directinstall/intent/DirectInstallAppData;
    .locals 15
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1086618
    sget-object v0, LX/6Q0;->b:[I

    iget-object v1, p0, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1086619
    sget-object v0, LX/6Qo;->Any:LX/6Qo;

    :goto_0
    move-object v6, v0

    .line 1086620
    new-instance v0, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    iget-object v1, p0, LX/47G;->e:Ljava/lang/String;

    iget v2, p0, LX/47G;->k:I

    iget-object v3, p0, LX/47G;->g:Ljava/lang/String;

    iget-object v4, p0, LX/47G;->c:LX/0Px;

    iget-object v5, p0, LX/47G;->l:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;-><init>(Ljava/lang/String;ILjava/lang/String;LX/0Px;Ljava/lang/String;)V

    .line 1086621
    new-instance v1, LX/6Qr;

    invoke-direct {v1}, LX/6Qr;-><init>()V

    move-object v1, v1

    .line 1086622
    iget-object v2, p0, LX/47G;->a:Ljava/lang/String;

    .line 1086623
    iput-object v2, v1, LX/6Qr;->a:Ljava/lang/String;

    .line 1086624
    move-object v1, v1

    .line 1086625
    iget-object v2, p0, LX/47G;->b:Ljava/lang/String;

    .line 1086626
    iput-object v2, v1, LX/6Qr;->b:Ljava/lang/String;

    .line 1086627
    move-object v1, v1

    .line 1086628
    iget-object v2, p0, LX/47G;->m:Ljava/lang/String;

    .line 1086629
    iput-object v2, v1, LX/6Qr;->h:Ljava/lang/String;

    .line 1086630
    move-object v1, v1

    .line 1086631
    iget-object v2, p0, LX/47G;->h:Ljava/lang/String;

    .line 1086632
    iput-object v2, v1, LX/6Qr;->d:Ljava/lang/String;

    .line 1086633
    move-object v1, v1

    .line 1086634
    iget-object v2, p0, LX/47G;->f:Ljava/lang/String;

    .line 1086635
    iput-object v2, v1, LX/6Qr;->c:Ljava/lang/String;

    .line 1086636
    move-object v1, v1

    .line 1086637
    iget-object v2, p0, LX/47G;->p:Ljava/lang/String;

    .line 1086638
    iput-object v2, v1, LX/6Qr;->i:Ljava/lang/String;

    .line 1086639
    move-object v1, v1

    .line 1086640
    iget-object v2, p0, LX/47G;->q:Ljava/lang/String;

    .line 1086641
    iput-object v2, v1, LX/6Qr;->j:Ljava/lang/String;

    .line 1086642
    move-object v1, v1

    .line 1086643
    iget-object v2, p0, LX/47G;->j:Ljava/lang/String;

    .line 1086644
    iput-object v2, v1, LX/6Qr;->g:Ljava/lang/String;

    .line 1086645
    move-object v1, v1

    .line 1086646
    iget-object v2, p0, LX/47G;->u:LX/0Px;

    invoke-static {v2}, LX/6Q1;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1086647
    iput-object v2, v1, LX/6Qr;->m:LX/0Px;

    .line 1086648
    move-object v1, v1

    .line 1086649
    iget-object v2, p0, LX/47G;->v:LX/0Px;

    invoke-static {v2}, LX/6Q1;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1086650
    iput-object v2, v1, LX/6Qr;->n:LX/0Px;

    .line 1086651
    move-object v2, v1

    .line 1086652
    iget-object v1, p0, LX/47G;->w:Ljava/lang/Object;

    instance-of v1, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    if-eqz v1, :cond_0

    .line 1086653
    iget-object v1, p0, LX/47G;->w:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1086654
    iput-object v1, v2, LX/6Qr;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1086655
    :cond_0
    iget-object v1, p0, LX/47G;->x:Ljava/lang/Object;

    instance-of v1, v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    if-eqz v1, :cond_1

    .line 1086656
    iget-object v1, p0, LX/47G;->x:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1086657
    iput-object v1, v2, LX/6Qr;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1086658
    :cond_1
    invoke-virtual {v2}, LX/6Qr;->a()Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-result-object v1

    .line 1086659
    new-instance v2, LX/6Qn;

    invoke-direct {v2, v0, v1, v6}, LX/6Qn;-><init>(Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;Lcom/facebook/directinstall/intent/DirectInstallAppDetails;LX/6Qo;)V

    move-object v0, v2

    .line 1086660
    iget-object v1, p0, LX/47G;->o:Ljava/lang/String;

    .line 1086661
    iput-object v1, v0, LX/6Qn;->f:Ljava/lang/String;

    .line 1086662
    move-object v0, v0

    .line 1086663
    iget-object v1, p0, LX/47G;->r:Landroid/os/Bundle;

    .line 1086664
    iput-object v1, v0, LX/6Qn;->e:Landroid/os/Bundle;

    .line 1086665
    move-object v0, v0

    .line 1086666
    iget-object v1, p0, LX/47G;->i:Ljava/lang/String;

    .line 1086667
    iput-object v1, v0, LX/6Qn;->d:Ljava/lang/String;

    .line 1086668
    move-object v0, v0

    .line 1086669
    new-instance v7, Lcom/facebook/directinstall/intent/DirectInstallAppData;

    iget-object v8, v0, LX/6Qn;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    iget-object v9, v0, LX/6Qn;->c:Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    iget-object v10, v0, LX/6Qn;->a:LX/6Qo;

    iget-object v11, v0, LX/6Qn;->d:Ljava/lang/String;

    iget-object v12, v0, LX/6Qn;->e:Landroid/os/Bundle;

    iget-object v13, v0, LX/6Qn;->f:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct/range {v7 .. v14}, Lcom/facebook/directinstall/intent/DirectInstallAppData;-><init>(Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;Lcom/facebook/directinstall/intent/DirectInstallAppDetails;LX/6Qo;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;B)V

    move-object v0, v7

    .line 1086670
    return-object v0

    .line 1086671
    :pswitch_0
    sget-object v0, LX/6Qo;->WifiForce:LX/6Qo;

    goto/16 :goto_0

    .line 1086672
    :pswitch_1
    sget-object v0, LX/6Qo;->WifiOnly:LX/6Qo;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1086673
    const/4 v1, 0x0

    .line 1086674
    if-eqz p1, :cond_0

    .line 1086675
    :try_start_0
    iget-object v1, p0, LX/6Q1;->d:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1086676
    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    .line 1086677
    :catch_0
    goto :goto_0
.end method

.method private c(LX/47G;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1086678
    iget-object v1, p0, LX/6Q1;->i:LX/0Uh;

    const/16 v2, 0x32a

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6Q1;->i:LX/0Uh;

    const/16 v2, 0x32b

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 1086679
    iget-object v3, p0, LX/6Q1;->j:LX/0W3;

    sget-wide v5, LX/0X5;->dO:J

    invoke-interface {v3, v5, v6}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 1086680
    iget-object v4, p1, LX/47G;->e:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    move v1, v3

    .line 1086681
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1086682
    iget-object v0, p2, LX/47I;->d:LX/47G;

    move-object v0, v0

    .line 1086683
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1086684
    if-nez v0, :cond_0

    .line 1086685
    :goto_1
    return-object v4

    .line 1086686
    :cond_0
    iget-object v0, p2, LX/47I;->d:LX/47G;

    move-object v5, v0

    .line 1086687
    iget-object v0, p2, LX/47I;->c:LX/0P1;

    move-object v0, v0

    .line 1086688
    invoke-static {p0, p1, v5, v0}, LX/6Q1;->a(LX/6Q1;Landroid/content/Context;LX/47G;LX/0P1;)Landroid/content/Intent;

    move-result-object v6

    .line 1086689
    iget-object v0, p0, LX/6Q1;->h:LX/6VX;

    iget-object v1, v5, LX/47G;->e:Ljava/lang/String;

    iget-object v2, v5, LX/47G;->l:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v5, v5, LX/47G;->o:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/6VX;->a(Ljava/lang/String;JLX/162;Ljava/lang/String;)V

    move-object v4, v6

    .line 1086690
    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;Landroid/content/Context;LX/47G;LX/0P1;Z)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Landroid/content/Context;",
            "LX/47G;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1086691
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1086692
    invoke-static {p3}, LX/6Q1;->a(LX/47G;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v0

    invoke-static {v1, v0}, LX/6Qv;->a(Landroid/content/Intent;Lcom/facebook/directinstall/intent/DirectInstallAppData;)V

    .line 1086693
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1086694
    if-eqz p4, :cond_0

    .line 1086695
    invoke-interface {v2, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1086696
    :cond_0
    const-string v0, "tracking"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1086697
    if-eqz v0, :cond_1

    .line 1086698
    const-string v3, "tracking"

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086699
    :try_start_0
    iget-object v3, p0, LX/6Q1;->b:LX/0lC;

    invoke-virtual {v3, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1086700
    const-string v3, "tracking"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086701
    :cond_1
    :goto_0
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {v1, v0}, LX/6Qv;->a(Landroid/content/Intent;LX/0P1;)V

    .line 1086702
    const-string v0, "can_skip_permissions"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1086703
    return-object v1

    :catch_0
    goto :goto_0
.end method
