.class public LX/65f;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:LX/65f;


# instance fields
.field private a:Z

.field private c:J

.field private d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048204
    new-instance v0, LX/67I;

    invoke-direct {v0}, LX/67I;-><init>()V

    sput-object v0, LX/65f;->b:LX/65f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1048202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048203
    return-void
.end method


# virtual methods
.method public a(J)LX/65f;
    .locals 1

    .prologue
    .line 1048199
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/65f;->a:Z

    .line 1048200
    iput-wide p1, p0, LX/65f;->c:J

    .line 1048201
    return-object p0
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)LX/65f;
    .locals 3

    .prologue
    .line 1048182
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "timeout < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048183
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048184
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/65f;->d:J

    .line 1048185
    return-object p0
.end method

.method public bJ_()J
    .locals 2

    .prologue
    .line 1048198
    iget-wide v0, p0, LX/65f;->d:J

    return-wide v0
.end method

.method public bK_()Z
    .locals 1

    .prologue
    .line 1048197
    iget-boolean v0, p0, LX/65f;->a:Z

    return v0
.end method

.method public bL_()LX/65f;
    .locals 2

    .prologue
    .line 1048195
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/65f;->d:J

    .line 1048196
    return-object p0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 1048193
    iget-boolean v0, p0, LX/65f;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No deadline"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048194
    :cond_0
    iget-wide v0, p0, LX/65f;->c:J

    return-wide v0
.end method

.method public f()LX/65f;
    .locals 1

    .prologue
    .line 1048191
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/65f;->a:Z

    .line 1048192
    return-object p0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 1048186
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048187
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "thread interrupted"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048188
    :cond_0
    iget-boolean v0, p0, LX/65f;->a:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, LX/65f;->c:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 1048189
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "deadline reached"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048190
    :cond_1
    return-void
.end method
