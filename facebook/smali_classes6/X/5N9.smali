.class public final LX/5N9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/12L;


# direct methods
.method public constructor <init>(LX/12L;Z)V
    .locals 0

    .prologue
    .line 905940
    iput-object p1, p0, LX/5N9;->b:LX/12L;

    iput-boolean p2, p0, LX/5N9;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 905941
    iget-object v0, p0, LX/5N9;->b:LX/12L;

    const/4 v1, 0x0

    .line 905942
    iput-object v1, v0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 905943
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 905944
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 905945
    iget-object v0, p0, LX/5N9;->b:LX/12L;

    const/4 v1, 0x0

    .line 905946
    iput-object v1, v0, LX/12L;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 905947
    iget-object v0, p0, LX/5N9;->b:LX/12L;

    iget-boolean v1, p0, LX/5N9;->a:Z

    const/4 p0, 0x1

    const/4 v7, 0x0

    .line 905948
    if-eqz p1, :cond_0

    .line 905949
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 905950
    if-eqz v2, :cond_0

    .line 905951
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 905952
    check-cast v2, Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 905953
    :cond_0
    :goto_0
    return-void

    .line 905954
    :cond_1
    if-eqz v1, :cond_2

    const-string v2, "free_data_mode"

    move-object v3, v2

    .line 905955
    :goto_1
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 905956
    check-cast v2, Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/dialtone/protocol/ZeroToggleStickyModeGraphQLModels$SetStickyModeMutationFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 905957
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 905958
    sget-object v4, LX/12L;->a:Ljava/lang/String;

    const-string v5, "Tried to update lightswitch sticky mode to %s, but the server responded that it\'s currently %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v7

    aput-object v2, v6, p0

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 905959
    const-string v3, "free_data_mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 905960
    invoke-virtual {v0, p0}, LX/12L;->b(Z)V

    goto :goto_0

    .line 905961
    :cond_2
    const-string v2, "paid_data_mode"

    move-object v3, v2

    goto :goto_1

    .line 905962
    :cond_3
    const-string v3, "paid_data_mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 905963
    invoke-virtual {v0, v7}, LX/12L;->b(Z)V

    goto :goto_0
.end method
