.class public final LX/5r5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/5r6;


# direct methods
.method public constructor <init>(LX/5r6;)V
    .locals 0

    .prologue
    .line 1010875
    iput-object p1, p0, LX/5r5;->a:LX/5r6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/5r6;B)V
    .locals 0

    .prologue
    .line 1010862
    invoke-direct {p0, p1}, LX/5r5;-><init>(LX/5r6;)V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1010863
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    .line 1010864
    iput-boolean v2, v0, LX/5r6;->f:Z

    .line 1010865
    move v1, v2

    .line 1010866
    :goto_0
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    iget-object v0, v0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1010867
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    iget-object v0, v0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v4

    move v3, v2

    .line 1010868
    :goto_1
    if-ge v3, v4, :cond_0

    .line 1010869
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    iget-object v0, v0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Choreographer$FrameCallback;

    invoke-interface {v0, p1, p2}, Landroid/view/Choreographer$FrameCallback;->doFrame(J)V

    .line 1010870
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    invoke-static {v0}, LX/5r6;->b(LX/5r6;)I

    .line 1010871
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1010872
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1010873
    :cond_1
    iget-object v0, p0, LX/5r5;->a:LX/5r6;

    invoke-static {v0}, LX/5r6;->b(LX/5r6;)V

    .line 1010874
    return-void
.end method
