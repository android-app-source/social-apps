.class public final LX/6K5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JU;


# instance fields
.field public final synthetic a:LX/6K6;


# direct methods
.method public constructor <init>(LX/6K6;)V
    .locals 0

    .prologue
    .line 1076237
    iput-object p1, p0, LX/6K5;->a:LX/6K6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1076238
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v1, p0, LX/6K5;->a:LX/6K6;

    iget-object v1, v1, LX/6K6;->m:LX/6Jc;

    .line 1076239
    iget-object v2, v1, LX/6Jc;->h:LX/6LC;

    invoke-interface {v2}, LX/6LC;->a()Landroid/view/Surface;

    move-result-object v2

    move-object v1, v2

    .line 1076240
    iput-object v1, v0, LX/6K6;->l:Landroid/view/Surface;

    .line 1076241
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    iget-object v1, p0, LX/6K5;->a:LX/6K6;

    iget-object v1, v1, LX/6K6;->l:Landroid/view/Surface;

    iget-object v2, p0, LX/6K5;->a:LX/6K6;

    iget-object v2, v2, LX/6K6;->o:LX/6JR;

    .line 1076242
    new-instance v3, LX/6Ku;

    iget v4, v2, LX/6JR;->a:I

    iget v5, v2, LX/6JR;->b:I

    invoke-direct {v3, v1, v4, v5}, LX/6Ku;-><init>(Landroid/view/Surface;II)V

    .line 1076243
    iget-object v4, v0, LX/6Jt;->k:Ljava/util/Map;

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076244
    invoke-virtual {v0, v3}, LX/6Jt;->a(LX/6Kd;)V

    .line 1076245
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    .line 1076246
    iput-object v1, v0, LX/6K6;->q:LX/6K9;

    .line 1076247
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    invoke-interface {v0}, LX/6JG;->a()V

    .line 1076248
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076249
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076250
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1076251
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to start video recording"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6JG;->a(LX/6JJ;)V

    .line 1076252
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->d(I)V

    .line 1076253
    iget-object v0, p0, LX/6K5;->a:LX/6K6;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076254
    return-void
.end method
