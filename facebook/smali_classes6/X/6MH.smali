.class public final LX/6MH;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1079617
    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsSearchQueryModel;

    const v0, 0x67844245

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchPaymentEligibleContactsSearchQuery"

    const-string v6, "2953531be56124cddce9aee2176ee16c"

    const-string v7, "viewer"

    const-string v8, "10155069965636729"

    const-string v9, "10155259088016729"

    .line 1079618
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1079619
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1079620
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1079621
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1079622
    sparse-switch v0, :sswitch_data_0

    .line 1079623
    :goto_0
    return-object p1

    .line 1079624
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1079625
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1079626
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1079627
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1079628
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1079629
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_1
        -0x55d248cb -> :sswitch_4
        -0x4e92d738 -> :sswitch_5
        0x6234bbb -> :sswitch_2
        0x2956b75c -> :sswitch_0
        0x6e1e6cd4 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1079630
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1079631
    :goto_1
    return v0

    .line 1079632
    :pswitch_0
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1079633
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
