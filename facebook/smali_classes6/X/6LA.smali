.class public final LX/6LA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1078054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078055
    return-void
.end method

.method public static a(LX/6JU;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 1078048
    if-nez p0, :cond_0

    .line 1078049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "stateCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078050
    :cond_0
    if-nez p1, :cond_1

    .line 1078051
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "handler cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078052
    :cond_1
    new-instance v0, Lcom/facebook/cameracore/mediapipeline/recorder/StateCallbackNotifier$1;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/mediapipeline/recorder/StateCallbackNotifier$1;-><init>(LX/6JU;)V

    const v1, 0x17f154e0

    invoke-static {p1, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1078053
    return-void
.end method

.method public static a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1078042
    if-nez p0, :cond_0

    .line 1078043
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "stateCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078044
    :cond_0
    if-nez p1, :cond_1

    .line 1078045
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "handler cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078046
    :cond_1
    new-instance v0, Lcom/facebook/cameracore/mediapipeline/recorder/StateCallbackNotifier$2;

    invoke-direct {v0, p0, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/StateCallbackNotifier$2;-><init>(LX/6JU;Ljava/lang/Throwable;)V

    const v1, -0x1488a40c

    invoke-static {p1, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1078047
    return-void
.end method
