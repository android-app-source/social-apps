.class public LX/6F8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/6F6;

.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/6rt;

.field public final e:LX/03V;

.field public final f:LX/6rs;

.field public final g:LX/0lC;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6F6;LX/6rt;LX/03V;LX/6rs;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067100
    iput-object p1, p0, LX/6F8;->b:Landroid/content/Context;

    .line 1067101
    iput-object p2, p0, LX/6F8;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1067102
    iput-object p3, p0, LX/6F8;->a:LX/6F6;

    .line 1067103
    iput-object p4, p0, LX/6F8;->d:LX/6rt;

    .line 1067104
    iput-object p5, p0, LX/6F8;->e:LX/03V;

    .line 1067105
    iput-object p6, p0, LX/6F8;->f:LX/6rs;

    .line 1067106
    iput-object p7, p0, LX/6F8;->g:LX/0lC;

    .line 1067107
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067031
    const-string v0, "paymentsCheckout"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 6

    .prologue
    .line 1067032
    check-cast p1, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    .line 1067033
    iget-object v0, p0, LX/6F8;->a:LX/6F6;

    .line 1067034
    iput-object p1, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    .line 1067035
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1067036
    const-string v1, "JS_BRIDGE_ENABLE_PAYMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1067037
    iget-object v0, p0, LX/6F8;->a:LX/6F6;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_UNSUPPORTED_CALL:LX/6Cy;

    invoke-virtual {v0, v1}, LX/6F6;->a(LX/6Cy;)V

    .line 1067038
    :goto_0
    :pswitch_0
    return-void

    .line 1067039
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->h()LX/0FB;

    move-result-object v0

    .line 1067040
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067041
    sget-object v1, LX/6F7;->a:[I

    invoke-virtual {v0}, LX/0FB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1067042
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Payment Checkout Operation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1067043
    :pswitch_1
    const-string v0, "onConfirmationClose"

    invoke-virtual {p1, v0}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "handleChargeRequest"

    invoke-virtual {p1, v0}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1067044
    :cond_1
    iget-object v0, p0, LX/6F8;->a:LX/6F6;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CALLBACK:LX/6Cy;

    invoke-virtual {v0, v1}, LX/6F6;->a(LX/6Cy;)V

    goto :goto_0

    .line 1067045
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/6F8;->d:LX/6rt;

    .line 1067046
    const-string v1, "configuration"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1067047
    sget-object v2, LX/6qw;->JS_BASED:LX/6qw;

    invoke-static {v0, v1, v2}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6rt;Ljava/lang/String;LX/6qw;)Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    .line 1067048
    invoke-static {v0}, LX/6qZ;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;

    move-result-object v1

    const/4 v2, 0x1

    .line 1067049
    iput-boolean v2, v1, LX/6qZ;->D:Z

    .line 1067050
    move-object v1, v1

    .line 1067051
    invoke-virtual {v1}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    move-object v0, v1

    .line 1067052
    invoke-static {v0}, LX/6qZ;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;

    move-result-object v1

    const/4 v2, 0x1

    .line 1067053
    iput-boolean v2, v1, LX/6qZ;->E:Z

    .line 1067054
    move-object v1, v1

    .line 1067055
    invoke-virtual {v1}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    move-object v0, v1

    .line 1067056
    iget-object v1, p0, LX/6F8;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1067057
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1067058
    iget-object v1, p0, LX/6F8;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6F8;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1067059
    :goto_1
    goto :goto_0

    .line 1067060
    :pswitch_2
    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN:LX/0FB;

    invoke-virtual {v1, v0}, LX/0FB;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1067061
    iget-object v1, p0, LX/6F8;->a:LX/6F6;

    .line 1067062
    iget-object v2, v1, LX/6F6;->c:LX/6Ez;

    move-object v1, v2

    .line 1067063
    const-string v2, "paymentId"

    invoke-virtual {p1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v2, v2

    .line 1067064
    if-nez v1, :cond_3

    .line 1067065
    iget-object v1, p0, LX/6F8;->a:LX/6F6;

    sget-object v2, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION:LX/6Cy;

    invoke-virtual {v1, v2}, LX/6F6;->a(LX/6Cy;)V

    .line 1067066
    :goto_2
    goto/16 :goto_0

    .line 1067067
    :pswitch_3
    iget-object v0, p0, LX/6F8;->a:LX/6F6;

    .line 1067068
    iget-object v1, v0, LX/6F6;->d:LX/6Ew;

    move-object v0, v1

    .line 1067069
    if-nez v0, :cond_6

    .line 1067070
    iget-object v0, p0, LX/6F8;->a:LX/6F6;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION:LX/6Cy;

    invoke-virtual {v0, v1}, LX/6F6;->a(LX/6Cy;)V

    .line 1067071
    :goto_3
    goto/16 :goto_0

    .line 1067072
    :catch_0
    move-exception v0

    .line 1067073
    iget-object v1, p0, LX/6F8;->a:LX/6F6;

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG:LX/6Cy;

    invoke-virtual {v1, v2}, LX/6F6;->a(LX/6Cy;)V

    .line 1067074
    iget-object v1, p0, LX/6F8;->e:LX/03V;

    const-string v2, "paymentsCheckout"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1067075
    :cond_3
    if-eqz v2, :cond_4

    .line 1067076
    iget-object v3, v1, LX/6Ez;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    move v2, v3

    .line 1067077
    if-nez v2, :cond_5

    .line 1067078
    :cond_4
    iget-object v1, p0, LX/6F8;->a:LX/6F6;

    sget-object v2, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_UNAUTHORIZED_PAYMENT:LX/6Cy;

    invoke-virtual {v1, v2}, LX/6F6;->a(LX/6Cy;)V

    goto :goto_2

    .line 1067079
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->j()Ljava/lang/String;

    move-result-object v2

    .line 1067080
    iget-object v3, v1, LX/6Ez;->g:LX/6Ex;

    invoke-static {v3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067081
    iget-object v3, v1, LX/6Ez;->h:Ljava/lang/String;

    invoke-static {v3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067082
    iget-object v3, v1, LX/6Ez;->d:Ljava/util/concurrent/Executor;

    new-instance p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;-><init>(LX/6Ez;ZLjava/lang/String;)V

    const p1, 0x6b2e2606

    invoke-static {v3, p0, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1067083
    goto :goto_2

    .line 1067084
    :cond_6
    const-string v1, "contentConfiguration"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1067085
    const/4 v3, 0x0

    .line 1067086
    if-nez v1, :cond_8

    move-object v2, v3

    .line 1067087
    :goto_4
    move-object v1, v2

    .line 1067088
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->j()Ljava/lang/String;

    move-result-object v2

    .line 1067089
    iget-object v3, v0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067090
    iget-object v3, v0, LX/6Ew;->m:LX/0YG;

    invoke-static {v3}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1067091
    iget-object v3, v0, LX/6Ew;->m:LX/0YG;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/0YG;->cancel(Z)Z

    .line 1067092
    :cond_7
    iget-object v3, v0, LX/6Ew;->e:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;-><init>(LX/6Ew;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;Ljava/lang/String;)V

    const v5, -0x7716d067

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1067093
    goto :goto_3

    .line 1067094
    :cond_8
    :try_start_1
    iget-object v2, p0, LX/6F8;->f:LX/6rs;

    const-string v4, "1.1.2"

    invoke-virtual {v2, v4}, LX/6rs;->d(Ljava/lang/String;)LX/6rr;

    move-result-object v2

    const-string v4, "1.1.2"

    iget-object v5, p0, LX/6F8;->g:LX/0lC;

    invoke-virtual {v5, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-interface {v2, v4, v5}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 1067095
    :catch_1
    move-exception v2

    .line 1067096
    iget-object v4, p0, LX/6F8;->a:LX/6F6;

    sget-object v5, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG:LX/6Cy;

    invoke-virtual {v4, v5}, LX/6F6;->a(LX/6Cy;)V

    .line 1067097
    iget-object v4, p0, LX/6F8;->e:LX/03V;

    const-string v5, "paymentsCheckout"

    invoke-virtual {v4, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    .line 1067098
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
