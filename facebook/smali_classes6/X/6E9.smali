.class public LX/6E9;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;",
        "LX/6E4;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;)V
    .locals 0

    .prologue
    .line 1065849
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1065850
    return-void
.end method

.method private a(LX/6E4;)V
    .locals 3

    .prologue
    .line 1065845
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;

    .line 1065846
    iget-object v1, p0, LX/6E9;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1065847
    iget-object v1, p1, LX/6E4;->a:Ljava/lang/String;

    iget-object v2, p1, LX/6E4;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1065848
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 1065844
    check-cast p1, LX/6E4;

    invoke-direct {p0, p1}, LX/6E9;->a(LX/6E4;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1065842
    iput-object p1, p0, LX/6E9;->l:LX/6qh;

    .line 1065843
    return-void
.end method
