.class public final LX/5k9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 991380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;LX/1Fb;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 990850
    if-nez p1, :cond_0

    .line 990851
    :goto_0
    return v0

    .line 990852
    :cond_0
    invoke-interface {p1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990853
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 990854
    invoke-interface {p1}, LX/1Fb;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 990855
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 990856
    const/4 v1, 0x2

    invoke-interface {p1}, LX/1Fb;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 990857
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990858
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;LX/1f8;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 990859
    if-nez p1, :cond_0

    .line 990860
    :goto_0
    return v1

    .line 990861
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 990862
    invoke-interface {p1}, LX/1f8;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 990863
    const/4 v1, 0x1

    invoke-interface {p1}, LX/1f8;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 990864
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 990865
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 990866
    if-nez p1, :cond_0

    .line 990867
    :goto_0
    return v0

    .line 990868
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 990869
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 990870
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 990871
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990872
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990873
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990874
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 990875
    if-nez p1, :cond_0

    .line 990876
    :goto_0
    return v0

    .line 990877
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990878
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 990879
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    move-result-object v3

    const/4 v4, 0x0

    .line 990880
    if-nez v3, :cond_1

    .line 990881
    :goto_1
    move v3, v4

    .line 990882
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    const/4 v5, 0x0

    .line 990883
    if-nez v4, :cond_2

    .line 990884
    :goto_2
    move v4, v5

    .line 990885
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 990886
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990887
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990888
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 990889
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 990890
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990891
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 990892
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 990893
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 990894
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 990895
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 990896
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 990897
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 990898
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 990899
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 990900
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 990901
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 990902
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 990903
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 990904
    if-nez p1, :cond_0

    .line 990905
    :goto_0
    return v0

    .line 990906
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v2, 0x0

    .line 990907
    if-nez v1, :cond_1

    .line 990908
    :goto_1
    move v1, v2

    .line 990909
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 990910
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990911
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 990912
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 990913
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990914
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 990915
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 990916
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 990917
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 990918
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 990919
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 990920
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 990921
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 990922
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 990923
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 990924
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 990925
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 990926
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 990927
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 990928
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 990929
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 990930
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990931
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;)I
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 990932
    if-nez p1, :cond_0

    .line 990933
    :goto_0
    return v2

    .line 990934
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->a()LX/0Px;

    move-result-object v3

    .line 990935
    if-eqz v3, :cond_2

    .line 990936
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 990937
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990938
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;

    const/4 v6, 0x0

    .line 990939
    if-nez v0, :cond_3

    .line 990940
    :goto_2
    move v0, v6

    .line 990941
    aput v0, v4, v1

    .line 990942
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990943
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 990944
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 990945
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 990946
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990947
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 990948
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 990949
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    const/4 v9, 0x0

    .line 990950
    if-nez v8, :cond_4

    .line 990951
    :goto_4
    move v8, v9

    .line 990952
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 990953
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 990954
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 990955
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 990956
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 990957
    :cond_4
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 990958
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 990959
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 990960
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 990961
    const/4 v9, 0x1

    invoke-virtual {p0, v9, p1}, LX/186;->b(II)V

    .line 990962
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 990963
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 990964
    if-nez p1, :cond_0

    .line 990965
    :goto_0
    return v2

    .line 990966
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990967
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 990968
    if-nez v0, :cond_3

    .line 990969
    :goto_1
    move v4, v5

    .line 990970
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 990971
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    const/4 v1, 0x0

    .line 990972
    if-nez v0, :cond_7

    .line 990973
    :goto_2
    move v6, v1

    .line 990974
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    const/4 v1, 0x0

    .line 990975
    if-nez v0, :cond_8

    .line 990976
    :goto_3
    move v7, v1

    .line 990977
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 990978
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v9

    .line 990979
    if-eqz v9, :cond_2

    .line 990980
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    new-array v10, v0, [I

    move v1, v2

    .line 990981
    :goto_4
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990982
    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    const/4 v11, 0x0

    .line 990983
    if-nez v0, :cond_9

    .line 990984
    :goto_5
    move v0, v11

    .line 990985
    aput v0, v10, v1

    .line 990986
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 990987
    :cond_1
    invoke-virtual {p0, v10, v12}, LX/186;->a([IZ)I

    move-result v0

    .line 990988
    :goto_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    const/4 v9, 0x0

    .line 990989
    if-nez v1, :cond_a

    .line 990990
    :goto_7
    move v1, v9

    .line 990991
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v9

    invoke-static {p0, v9}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)I

    move-result v9

    .line 990992
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v10

    invoke-static {p0, v10}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I

    move-result v10

    .line 990993
    const/16 v11, 0x14

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 990994
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v11

    invoke-virtual {p0, v2, v11}, LX/186;->a(IZ)V

    .line 990995
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v11

    invoke-virtual {p0, v12, v11}, LX/186;->a(IZ)V

    .line 990996
    const/4 v11, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->d()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 990997
    const/4 v11, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->e()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 990998
    const/4 v11, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 990999
    const/4 v11, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 991000
    const/4 v11, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->p()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 991001
    const/4 v11, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v12

    invoke-virtual {p0, v11, v12}, LX/186;->a(IZ)V

    .line 991002
    const/16 v11, 0x8

    invoke-virtual {p0, v11, v3}, LX/186;->b(II)V

    .line 991003
    const/16 v3, 0x9

    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 991004
    const/16 v3, 0xa

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v4

    invoke-virtual {p0, v3, v4}, LX/186;->a(IZ)V

    .line 991005
    const/16 v3, 0xb

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 991006
    const/16 v3, 0xc

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 991007
    const/16 v3, 0xd

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 991008
    const/16 v3, 0xe

    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 991009
    const/16 v3, 0xf

    invoke-virtual {p0, v3, v0}, LX/186;->b(II)V

    .line 991010
    const/16 v0, 0x10

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991011
    const/16 v0, 0x11

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 991012
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 991013
    const/16 v0, 0x13

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v1

    invoke-virtual {p0, v0, v1, v2}, LX/186;->a(III)V

    .line 991014
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991015
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_6

    .line 991016
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;->a()LX/0Px;

    move-result-object v6

    .line 991017
    if-eqz v6, :cond_5

    .line 991018
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v4, v5

    .line 991019
    :goto_8
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    .line 991020
    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v9, 0x0

    .line 991021
    if-nez v1, :cond_6

    .line 991022
    :goto_9
    move v1, v9

    .line 991023
    aput v1, v7, v4

    .line 991024
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8

    .line 991025
    :cond_4
    invoke-virtual {p0, v7, v8}, LX/186;->a([IZ)I

    move-result v1

    .line 991026
    :goto_a
    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 991027
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 991028
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 991029
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_5
    move v1, v5

    goto :goto_a

    .line 991030
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 991031
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 991032
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 991033
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 991034
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 991035
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 991036
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_9

    .line 991037
    :cond_7
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 991038
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v6

    invoke-virtual {p0, v1, v6, v1}, LX/186;->a(III)V

    .line 991039
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 991040
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 991041
    :cond_8
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 991042
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v7

    invoke-virtual {p0, v1, v7, v1}, LX/186;->a(III)V

    .line 991043
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 991044
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 991045
    :cond_9
    const/4 v13, 0x1

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 991046
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->a()I

    move-result v13

    invoke-virtual {p0, v11, v13, v11}, LX/186;->a(III)V

    .line 991047
    invoke-virtual {p0}, LX/186;->d()I

    move-result v11

    .line 991048
    invoke-virtual {p0, v11}, LX/186;->d(I)V

    goto/16 :goto_5

    .line 991049
    :cond_a
    const/4 v10, 0x2

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 991050
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v10

    invoke-virtual {p0, v9, v10, v9}, LX/186;->a(III)V

    .line 991051
    const/4 v10, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v11

    invoke-virtual {p0, v10, v11, v9}, LX/186;->a(III)V

    .line 991052
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 991053
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto/16 :goto_7
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 991054
    if-nez p1, :cond_0

    .line 991055
    :goto_0
    return v0

    .line 991056
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 991057
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 991058
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    const/4 v4, 0x0

    .line 991059
    if-nez v3, :cond_1

    .line 991060
    :goto_1
    move v3, v4

    .line 991061
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 991062
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991063
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991064
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991065
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991066
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 991067
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991068
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 991069
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 991070
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 991071
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 991072
    if-nez p1, :cond_0

    .line 991073
    :goto_0
    return v2

    .line 991074
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v3

    .line 991075
    if-eqz v3, :cond_2

    .line 991076
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 991077
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 991078
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-static {p0, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I

    move-result v0

    aput v0, v4, v1

    .line 991079
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 991080
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 991081
    :goto_2
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991082
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 991083
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991084
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 991085
    if-nez p1, :cond_0

    .line 991086
    :goto_0
    return v0

    .line 991087
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 991088
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    const/4 v3, 0x0

    .line 991089
    if-nez v2, :cond_1

    .line 991090
    :goto_1
    move v2, v3

    .line 991091
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v3

    const/4 v4, 0x0

    .line 991092
    if-nez v3, :cond_2

    .line 991093
    :goto_2
    move v3, v4

    .line 991094
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v4

    const/4 v5, 0x0

    .line 991095
    if-nez v4, :cond_3

    .line 991096
    :goto_3
    move v4, v5

    .line 991097
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991098
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991099
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991100
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991101
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 991102
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991103
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 991104
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 991105
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991106
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 991107
    const/4 v7, 0x3

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 991108
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 991109
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 991110
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 991111
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 991112
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1

    .line 991113
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v5

    invoke-static {p0, v5}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v5

    .line 991114
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v6

    invoke-static {p0, v6}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v6

    .line 991115
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    invoke-static {p0, v7}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v7

    .line 991116
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v8

    invoke-static {p0, v8}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v8

    .line 991117
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v9

    invoke-static {p0, v9}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v9

    .line 991118
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v10

    invoke-static {p0, v10}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v10

    .line 991119
    const/4 v11, 0x6

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 991120
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 991121
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 991122
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 991123
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 991124
    const/4 v4, 0x4

    invoke-virtual {p0, v4, v9}, LX/186;->b(II)V

    .line 991125
    const/4 v4, 0x5

    invoke-virtual {p0, v4, v10}, LX/186;->b(II)V

    .line 991126
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 991127
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 991128
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 991129
    if-nez v6, :cond_4

    .line 991130
    :goto_4
    move v6, v7

    .line 991131
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 991132
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 991133
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 991134
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 991135
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 991136
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 991137
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 991138
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 991139
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLMedia;)I
    .locals 40

    .prologue
    .line 991140
    if-nez p1, :cond_0

    .line 991141
    const/4 v2, 0x0

    .line 991142
    :goto_0
    return v2

    .line 991143
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 991144
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991145
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v6

    .line 991146
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v7

    .line 991147
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 991148
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v10

    .line 991149
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v11

    .line 991150
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I

    move-result v12

    .line 991151
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;)I

    move-result v13

    .line 991152
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v14

    .line 991153
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v15

    .line 991154
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)I

    move-result v16

    .line 991155
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->R()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 991156
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 991157
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v19

    .line 991158
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v20

    .line 991159
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v21

    .line 991160
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v22

    .line 991161
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v23

    .line 991162
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v24

    .line 991163
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)I

    move-result v25

    .line 991164
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v26

    .line 991165
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v27

    .line 991166
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)I

    move-result v28

    .line 991167
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v29

    .line 991168
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v30

    .line 991169
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I

    move-result v31

    .line 991170
    const/4 v2, 0x0

    .line 991171
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v8

    .line 991172
    if-eqz v8, :cond_2

    .line 991173
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    new-array v0, v2, [I

    move-object/from16 v32, v0

    .line 991174
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 991175
    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoEncoding;)I

    move-result v2

    aput v2, v32, v3

    .line 991176
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 991177
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v2}, LX/186;->a([IZ)I

    move-result v2

    move v8, v2

    .line 991178
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 991179
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->c(LX/186;Lcom/facebook/graphql/model/GraphQLPrivacyScope;)I

    move-result v33

    .line 991180
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I

    move-result v34

    .line 991181
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ba()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 991182
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bl()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 991183
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bm()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 991184
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;)I

    move-result v38

    .line 991185
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)I

    move-result v39

    .line 991186
    const/16 v2, 0x40

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 991187
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 991188
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 991189
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 991190
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 991191
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 991192
    const/4 v2, 0x5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->t()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991193
    const/4 v2, 0x6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991194
    const/4 v2, 0x7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bP()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991195
    const/16 v2, 0x8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->v()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991196
    const/16 v2, 0x9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->w()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991197
    const/16 v2, 0xa

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->x()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991198
    const/16 v2, 0xb

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->y()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991199
    const/16 v2, 0xc

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991200
    const/16 v2, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->A()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991201
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bW()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991202
    const/16 v2, 0xf

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->B()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991203
    const/16 v2, 0x10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->C()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991204
    const/16 v2, 0x11

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 991205
    const/16 v3, 0x12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 991206
    const/16 v2, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 991207
    const/16 v2, 0x14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bM()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991208
    const/16 v2, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 991209
    const/16 v2, 0x16

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 991210
    const/16 v2, 0x17

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 991211
    const/16 v2, 0x18

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 991212
    const/16 v3, 0x19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bN()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 991213
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991214
    const/16 v2, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->M()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991215
    const/16 v2, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991216
    const/16 v2, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991217
    const/16 v2, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991218
    const/16 v2, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991219
    const/16 v2, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991220
    const/16 v2, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991221
    const/16 v2, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991222
    const/16 v2, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991223
    const/16 v2, 0x24

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->af()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991224
    const/16 v2, 0x25

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ag()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991225
    const/16 v2, 0x26

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ah()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991226
    const/16 v2, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991227
    const/16 v2, 0x28

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->al()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991228
    const/16 v2, 0x29

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991229
    const/16 v2, 0x2a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991230
    const/16 v2, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991231
    const/16 v2, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991232
    const/16 v2, 0x2d

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991233
    const/16 v2, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991234
    const/16 v3, 0x2f

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bO()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 991235
    const/16 v2, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991236
    const/16 v2, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991237
    const/16 v2, 0x32

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 991238
    const/16 v2, 0x33

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991239
    const/16 v2, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991240
    const/16 v2, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991241
    const/16 v2, 0x36

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991242
    const/16 v2, 0x37

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991243
    const/16 v2, 0x38

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991244
    const/16 v3, 0x39

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bj()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 991245
    const/16 v3, 0x3a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bk()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 991246
    const/16 v2, 0x3b

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991247
    const/16 v2, 0x3c

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991248
    const/16 v2, 0x3d

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bn()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991249
    const/16 v2, 0x3e

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991250
    const/16 v2, 0x3f

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991251
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 991252
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v8, v2

    goto/16 :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoto;)I
    .locals 35

    .prologue
    .line 991287
    if-nez p1, :cond_0

    .line 991288
    const/4 v2, 0x0

    .line 991289
    :goto_0
    return v2

    .line 991290
    :cond_0
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x4984e12

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 991291
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991292
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v6

    .line 991293
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v7

    .line 991294
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 991295
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v10

    .line 991296
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v11

    .line 991297
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I

    move-result v12

    .line 991298
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;)I

    move-result v13

    .line 991299
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v14

    .line 991300
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v15

    .line 991301
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 991302
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v17

    .line 991303
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v18

    .line 991304
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v19

    .line 991305
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v20

    .line 991306
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v21

    .line 991307
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v22

    .line 991308
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)I

    move-result v23

    .line 991309
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v24

    .line 991310
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v25

    .line 991311
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)I

    move-result v26

    .line 991312
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v27

    .line 991313
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v28

    .line 991314
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I

    move-result v29

    .line 991315
    const/4 v2, 0x0

    .line 991316
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v8

    .line 991317
    if-eqz v8, :cond_2

    .line 991318
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    new-array v0, v2, [I

    move-object/from16 v30, v0

    .line 991319
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 991320
    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoEncoding;)I

    move-result v2

    aput v2, v30, v3

    .line 991321
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 991322
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v2}, LX/186;->a([IZ)I

    move-result v2

    move v8, v2

    .line 991323
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->az()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 991324
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->c(LX/186;Lcom/facebook/graphql/model/GraphQLPrivacyScope;)I

    move-result v31

    .line 991325
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I

    move-result v32

    .line 991326
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;)I

    move-result v33

    .line 991327
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)I

    move-result v34

    .line 991328
    const/16 v2, 0x40

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 991329
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 991330
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 991331
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 991332
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 991333
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 991334
    const/4 v2, 0x5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->q()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991335
    const/4 v2, 0x6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->r()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991336
    const/4 v2, 0x7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aR()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991337
    const/16 v2, 0x8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->s()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991338
    const/16 v2, 0x9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->t()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991339
    const/16 v2, 0xa

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->u()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991340
    const/16 v2, 0xb

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->v()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991341
    const/16 v2, 0xc

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->w()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991342
    const/16 v2, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aU()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991343
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aV()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991344
    const/16 v2, 0xf

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->x()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991345
    const/16 v2, 0x10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->y()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991346
    const/16 v2, 0x11

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 991347
    const/16 v3, 0x12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->A()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 991348
    const/16 v2, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 991349
    const/16 v2, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 991350
    const/16 v2, 0x16

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 991351
    const/16 v2, 0x17

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 991352
    const/16 v2, 0x18

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 991353
    const/16 v2, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->H()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991354
    const/16 v2, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991355
    const/16 v2, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991356
    const/16 v2, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991357
    const/16 v2, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991358
    const/16 v2, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991359
    const/16 v2, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991360
    const/16 v2, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991361
    const/16 v2, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991362
    const/16 v2, 0x28

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->ab()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991363
    const/16 v2, 0x29

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->ad()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991364
    const/16 v2, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991365
    const/16 v2, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991366
    const/16 v2, 0x2d

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991367
    const/16 v2, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991368
    const/16 v2, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991369
    const/16 v2, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991370
    const/16 v2, 0x32

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 991371
    const/16 v2, 0x33

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->ay()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 991372
    const/16 v2, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991373
    const/16 v2, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991374
    const/16 v2, 0x36

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991375
    const/16 v2, 0x38

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->aH()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 991376
    const/16 v2, 0x3e

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991377
    const/16 v2, 0x3f

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 991378
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 991379
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v8, v2

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoEncoding;)I
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 991253
    if-nez p1, :cond_0

    .line 991254
    :goto_0
    return v2

    .line 991255
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 991256
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 991257
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991258
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v0

    invoke-static {p0, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;)I

    move-result v6

    .line 991259
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->o()LX/0Px;

    move-result-object v7

    .line 991260
    if-eqz v7, :cond_2

    .line 991261
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    new-array v8, v0, [I

    move v1, v2

    .line 991262
    :goto_1
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 991263
    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTile;

    const/4 v10, 0x0

    .line 991264
    if-nez v0, :cond_3

    .line 991265
    :goto_2
    move v0, v10

    .line 991266
    aput v0, v8, v1

    .line 991267
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 991268
    :cond_1
    invoke-virtual {p0, v8, v9}, LX/186;->a([IZ)I

    move-result v0

    .line 991269
    :goto_3
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 991270
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 991271
    invoke-virtual {p0, v9, v4}, LX/186;->b(II)V

    .line 991272
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v5}, LX/186;->b(II)V

    .line 991273
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v6}, LX/186;->b(II)V

    .line 991274
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 991275
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->n()I

    move-result v1

    invoke-virtual {p0, v0, v1, v2}, LX/186;->a(III)V

    .line 991276
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991277
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 991278
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->m()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 991279
    const/4 v12, 0x5

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 991280
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->a()I

    move-result v12

    invoke-virtual {p0, v10, v12, v10}, LX/186;->a(III)V

    .line 991281
    const/4 v12, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->j()I

    move-result v13

    invoke-virtual {p0, v12, v13, v10}, LX/186;->a(III)V

    .line 991282
    const/4 v12, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->k()I

    move-result v13

    invoke-virtual {p0, v12, v13, v10}, LX/186;->a(III)V

    .line 991283
    const/4 v12, 0x3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->l()I

    move-result v13

    invoke-virtual {p0, v12, v13, v10}, LX/186;->a(III)V

    .line 991284
    const/4 v10, 0x4

    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 991285
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 991286
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;)I
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 991561
    if-nez p1, :cond_0

    .line 991562
    :goto_0
    return v2

    .line 991563
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;->a()LX/0Px;

    move-result-object v3

    .line 991564
    if-eqz v3, :cond_2

    .line 991565
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 991566
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 991567
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFaceBox;

    const/4 v6, 0x0

    .line 991568
    if-nez v0, :cond_3

    .line 991569
    :goto_2
    move v0, v6

    .line 991570
    aput v0, v4, v1

    .line 991571
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 991572
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 991573
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991574
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 991575
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991576
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 991577
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v7

    invoke-static {p0, v7}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v7

    .line 991578
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v8

    invoke-static {p0, v8}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v8

    .line 991579
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->l()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 991580
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    move-result-object v10

    invoke-static {p0, v10}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;)I

    move-result v10

    .line 991581
    const/4 p1, 0x4

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 991582
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 991583
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 991584
    const/4 v6, 0x2

    invoke-virtual {p0, v6, v9}, LX/186;->b(II)V

    .line 991585
    const/4 v6, 0x3

    invoke-virtual {p0, v6, v10}, LX/186;->b(II)V

    .line 991586
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 991587
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 991548
    if-nez p1, :cond_0

    .line 991549
    :goto_0
    return v2

    .line 991550
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v3

    .line 991551
    if-eqz v3, :cond_2

    .line 991552
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 991553
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 991554
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    invoke-static {p0, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;)I

    move-result v0

    aput v0, v4, v1

    .line 991555
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 991556
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 991557
    :goto_2
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991558
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 991559
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991560
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 991492
    if-nez p1, :cond_0

    .line 991493
    :goto_0
    return v0

    .line 991494
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 991495
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    const/4 v3, 0x0

    .line 991496
    if-nez v2, :cond_1

    .line 991497
    :goto_1
    move v2, v3

    .line 991498
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v3

    const/4 v5, 0x0

    .line 991499
    if-nez v3, :cond_3

    .line 991500
    :goto_2
    move v3, v5

    .line 991501
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 991502
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991503
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991504
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991505
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991506
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 991507
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 991508
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991509
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 991510
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    const/4 v8, 0x0

    .line 991511
    if-nez v7, :cond_2

    .line 991512
    :goto_3
    move v7, v8

    .line 991513
    const/4 v8, 0x5

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 991514
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 991515
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 991516
    const/4 v3, 0x2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->T()Z

    move-result v4

    invoke-virtual {p0, v3, v4}, LX/186;->a(IZ)V

    .line 991517
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 991518
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 991519
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 991520
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1

    .line 991521
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 991522
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 991523
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 991524
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 991525
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_3

    .line 991526
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 991527
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v7

    invoke-static {p0, v7}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v7

    .line 991528
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 991529
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    const/4 v10, 0x0

    .line 991530
    if-nez v9, :cond_4

    .line 991531
    :goto_4
    move v9, v10

    .line 991532
    const/4 v10, 0x6

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 991533
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->a()Z

    move-result v10

    invoke-virtual {p0, v5, v10}, LX/186;->a(IZ)V

    .line 991534
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 991535
    const/4 v5, 0x2

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 991536
    const/4 v5, 0x3

    invoke-virtual {p0, v5, v8}, LX/186;->b(II)V

    .line 991537
    const/4 v5, 0x4

    invoke-virtual {p0, v5, v9}, LX/186;->b(II)V

    .line 991538
    const/4 v6, 0x5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->l()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    move-object v5, p0

    invoke-virtual/range {v5 .. v10}, LX/186;->a(IJJ)V

    .line 991539
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 991540
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 991541
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    .line 991542
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 991543
    const/4 v13, 0x2

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 991544
    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 991545
    const/4 v10, 0x1

    invoke-virtual {p0, v10, v12}, LX/186;->b(II)V

    .line 991546
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 991547
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 991477
    if-nez p1, :cond_0

    .line 991478
    :goto_0
    return v0

    .line 991479
    :cond_0
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 991480
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1, v0}, LX/186;->a(III)V

    .line 991481
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->j()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 991482
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->k()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 991483
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->l()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 991484
    const/4 v1, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->m()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 991485
    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->n()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 991486
    const/4 v1, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->o()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 991487
    const/4 v1, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->p()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 991488
    const/16 v1, 0x8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->q()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 991489
    const/16 v1, 0x9

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;->r()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 991490
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991491
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 991466
    if-nez p1, :cond_0

    .line 991467
    :goto_0
    return v0

    .line 991468
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 991469
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 991470
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 991471
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 991472
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991473
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991474
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991475
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991476
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 991435
    if-nez p1, :cond_0

    .line 991436
    :goto_0
    return v0

    .line 991437
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    const/4 v2, 0x0

    .line 991438
    if-nez v1, :cond_1

    .line 991439
    :goto_1
    move v1, v2

    .line 991440
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 991441
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->k()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 991442
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 991443
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991444
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991445
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991446
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991447
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 991448
    :cond_1
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x25d6af

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 991449
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 991450
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 991451
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 991452
    if-nez v6, :cond_2

    .line 991453
    :goto_2
    move v6, v7

    .line 991454
    const/4 v7, 0x4

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 991455
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 991456
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 991457
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 991458
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 991459
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991460
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 991461
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 991462
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 991463
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 991464
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 991465
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 991405
    if-nez p1, :cond_0

    .line 991406
    :goto_0
    return v0

    .line 991407
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 991408
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 991409
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 991410
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    const/4 v5, 0x0

    .line 991411
    if-nez v4, :cond_1

    .line 991412
    :goto_1
    move v4, v5

    .line 991413
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991414
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 991415
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 991416
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 991417
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 991418
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 991419
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 991420
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 991421
    if-nez v6, :cond_2

    .line 991422
    :goto_2
    move v6, v7

    .line 991423
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 991424
    const/4 v8, 0x3

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 991425
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a()Z

    move-result v8

    invoke-virtual {p0, v5, v8}, LX/186;->a(IZ)V

    .line 991426
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 991427
    const/4 v5, 0x2

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 991428
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 991429
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_1

    .line 991430
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 991431
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 991432
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 991433
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 991434
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 991381
    if-nez p1, :cond_0

    .line 991382
    :goto_0
    return v2

    .line 991383
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 991384
    if-eqz v3, :cond_2

    .line 991385
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 991386
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 991387
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    const/4 v6, 0x0

    .line 991388
    if-nez v0, :cond_3

    .line 991389
    :goto_2
    move v0, v6

    .line 991390
    aput v0, v4, v1

    .line 991391
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 991392
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 991393
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 991394
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 991395
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 991396
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 991397
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    .line 991398
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 991399
    const/4 v9, 0x3

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 991400
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 991401
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 991402
    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IJJ)V

    .line 991403
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 991404
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 990783
    if-nez p1, :cond_0

    .line 990784
    :goto_0
    return v2

    .line 990785
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990786
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v4

    .line 990787
    if-eqz v4, :cond_2

    .line 990788
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 990789
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990790
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    const/4 v7, 0x0

    .line 990791
    if-nez v0, :cond_3

    .line 990792
    :goto_2
    move v0, v7

    .line 990793
    aput v0, v5, v1

    .line 990794
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990795
    :cond_1
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 990796
    :goto_3
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 990797
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 990798
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 990799
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990800
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 990801
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 990802
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 990803
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->a()I

    move-result p1

    invoke-virtual {p0, v7, p1, v7}, LX/186;->a(III)V

    .line 990804
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 990805
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 990806
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 990807
    if-nez p1, :cond_0

    .line 990808
    :goto_0
    return v2

    .line 990809
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 990810
    if-eqz v3, :cond_2

    .line 990811
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 990812
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990813
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {p0, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v0

    aput v0, v4, v1

    .line 990814
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990815
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 990816
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990817
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 990818
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 990819
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 990820
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990821
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 990822
    if-nez p1, :cond_0

    .line 990823
    :goto_0
    return v2

    .line 990824
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v3

    .line 990825
    if-eqz v3, :cond_2

    .line 990826
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 990827
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990828
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    const/4 v6, 0x0

    .line 990829
    if-nez v0, :cond_3

    .line 990830
    :goto_2
    move v0, v6

    .line 990831
    aput v0, v4, v1

    .line 990832
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990833
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 990834
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 990835
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 990836
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990837
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 990838
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v7

    const/4 v8, 0x0

    .line 990839
    if-nez v7, :cond_4

    .line 990840
    :goto_4
    move v7, v8

    .line 990841
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 990842
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 990843
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 990844
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 990845
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 990846
    :cond_4
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 990847
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p1

    invoke-virtual {p0, v8, p1, v8}, LX/186;->a(III)V

    .line 990848
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 990849
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 989595
    if-nez p1, :cond_0

    .line 989596
    :goto_0
    return v0

    .line 989597
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 989598
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 989599
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 989600
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 989601
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 989602
    if-nez p1, :cond_0

    .line 989603
    :goto_0
    return v1

    .line 989604
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 989605
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 989606
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 989607
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 989608
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 989609
    if-nez p1, :cond_0

    .line 989610
    :goto_0
    return v2

    .line 989611
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;->a()LX/0Px;

    move-result-object v3

    .line 989612
    if-eqz v3, :cond_2

    .line 989613
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 989614
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 989615
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    const/4 v12, 0x0

    .line 989616
    if-nez v0, :cond_3

    move v6, v12

    .line 989617
    :goto_2
    move v0, v6

    .line 989618
    aput v0, v4, v1

    .line 989619
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 989620
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 989621
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 989622
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 989623
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 989624
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 989625
    :cond_3
    const/4 v6, 0x3

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 989626
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->a()I

    move-result v6

    invoke-virtual {p0, v12, v6, v12}, LX/186;->a(III)V

    .line 989627
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->j()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IJJ)V

    .line 989628
    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->k()I

    move-result v7

    invoke-virtual {p0, v6, v7, v12}, LX/186;->a(III)V

    .line 989629
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 989630
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)I
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 989631
    if-nez p1, :cond_0

    .line 989632
    :goto_0
    return v2

    .line 989633
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v3

    .line 989634
    if-eqz v3, :cond_2

    .line 989635
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 989636
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 989637
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v6, 0x0

    .line 989638
    if-nez v0, :cond_3

    .line 989639
    :goto_2
    move v0, v6

    .line 989640
    aput v0, v4, v1

    .line 989641
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 989642
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 989643
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 989644
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 989645
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 989646
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 989647
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    .line 989648
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 989649
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 989650
    const/4 p1, 0x3

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 989651
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 989652
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 989653
    const/4 v6, 0x2

    invoke-virtual {p0, v6, v9}, LX/186;->b(II)V

    .line 989654
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 989655
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(LX/1U8;)LX/5kD;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 989656
    if-nez p0, :cond_1

    .line 989657
    :cond_0
    :goto_0
    return-object v2

    .line 989658
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 989659
    const/4 v1, 0x0

    .line 989660
    if-nez p0, :cond_3

    .line 989661
    :goto_1
    move v1, v1

    .line 989662
    if-eqz v1, :cond_0

    .line 989663
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 989664
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 989665
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 989666
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 989667
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 989668
    const-string v1, "PhotosMetadataConversionHelper.getMediaMetadata"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 989669
    :cond_2
    new-instance v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v2, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 989670
    :cond_3
    invoke-interface {p0}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 989671
    invoke-interface {p0}, LX/1U8;->c()LX/1f8;

    move-result-object v4

    invoke-static {v0, v4}, LX/5k9;->a(LX/186;LX/1f8;)I

    move-result v4

    .line 989672
    invoke-interface {p0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 989673
    invoke-interface {p0}, LX/1U8;->e()LX/1Fb;

    move-result-object v6

    invoke-static {v0, v6}, LX/5k9;->a(LX/186;LX/1Fb;)I

    move-result v6

    .line 989674
    invoke-interface {p0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v7

    invoke-static {v0, v7}, LX/5k9;->a(LX/186;LX/1Fb;)I

    move-result v7

    .line 989675
    invoke-interface {p0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v8

    invoke-static {v0, v8}, LX/5k9;->a(LX/186;LX/1Fb;)I

    move-result v8

    .line 989676
    invoke-interface {p0}, LX/1U8;->j()LX/1Fb;

    move-result-object v9

    invoke-static {v0, v9}, LX/5k9;->a(LX/186;LX/1Fb;)I

    move-result v9

    .line 989677
    const/16 v10, 0x40

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 989678
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 989679
    const/16 v1, 0x18

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 989680
    const/16 v1, 0x1d

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 989681
    const/16 v1, 0x1e

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 989682
    const/16 v1, 0x1f

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 989683
    const/16 v1, 0x21

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 989684
    const/16 v1, 0x22

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 989685
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 989686
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1
.end method

.method public static a(LX/5sa;)LX/5kD;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 989687
    if-nez p0, :cond_1

    .line 989688
    :cond_0
    :goto_0
    return-object v2

    .line 989689
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 989690
    const/4 v1, 0x0

    .line 989691
    if-nez p0, :cond_3

    .line 989692
    :goto_1
    move v1, v1

    .line 989693
    if-eqz v1, :cond_0

    .line 989694
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 989695
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 989696
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 989697
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 989698
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 989699
    const-string v1, "PhotosMetadataConversionHelper.getMediaMetadata"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 989700
    :cond_2
    new-instance v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v2, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 989701
    :cond_3
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x4984e12

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 989702
    invoke-interface {p0}, LX/5sa;->c()LX/1f8;

    move-result-object v4

    invoke-static {v0, v4}, LX/5k9;->a(LX/186;LX/1f8;)I

    move-result v4

    .line 989703
    invoke-interface {p0}, LX/5sa;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 989704
    invoke-interface {p0}, LX/5sa;->aj_()LX/1Fb;

    move-result-object v6

    invoke-static {v0, v6}, LX/5k9;->a(LX/186;LX/1Fb;)I

    move-result v6

    .line 989705
    const/16 v7, 0x40

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 989706
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 989707
    const/16 v1, 0x18

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 989708
    const/16 v1, 0x1d

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 989709
    const/16 v1, 0x1f

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 989710
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 989711
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 989712
    if-nez p0, :cond_1

    .line 989713
    :cond_0
    :goto_0
    return-object v2

    .line 989714
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 989715
    invoke-static {v0, p0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v1

    .line 989716
    if-eqz v1, :cond_0

    .line 989717
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 989718
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 989719
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 989720
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 989721
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 989722
    const-string v1, "PhotosMetadataConversionHelper.getMediaMetadata"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 989723
    :cond_2
    new-instance v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v2, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 2

    .prologue
    .line 989724
    if-nez p0, :cond_0

    .line 989725
    const/4 v0, 0x0

    .line 989726
    :goto_0
    return-object v0

    .line 989727
    :cond_0
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    .line 989728
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 989729
    iput-object v1, v0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 989730
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 989731
    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 989732
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 989733
    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 989734
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 8

    .prologue
    .line 989735
    if-nez p0, :cond_0

    .line 989736
    const/4 v0, 0x0

    .line 989737
    :goto_0
    return-object v0

    .line 989738
    :cond_0
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 989739
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->b()Z

    move-result v0

    .line 989740
    iput-boolean v0, v2, LX/3dM;->e:Z

    .line 989741
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->c()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->c(Z)LX/3dM;

    .line 989742
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->d()Z

    move-result v0

    .line 989743
    iput-boolean v0, v2, LX/3dM;->g:Z

    .line 989744
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->e()Z

    move-result v0

    .line 989745
    iput-boolean v0, v2, LX/3dM;->i:Z

    .line 989746
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aC_()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->g(Z)LX/3dM;

    .line 989747
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aD_()Z

    move-result v0

    .line 989748
    iput-boolean v0, v2, LX/3dM;->k:Z

    .line 989749
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->j()Z

    move-result v0

    .line 989750
    iput-boolean v0, v2, LX/3dM;->l:Z

    .line 989751
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->k()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->j(Z)LX/3dM;

    .line 989752
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 989753
    iput-object v0, v2, LX/3dM;->y:Ljava/lang/String;

    .line 989754
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->m()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    .line 989755
    if-nez v0, :cond_3

    .line 989756
    const/4 v1, 0x0

    .line 989757
    :goto_1
    move-object v0, v1

    .line 989758
    iput-object v0, v2, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 989759
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->n()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->l(Z)LX/3dM;

    .line 989760
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->o()Ljava/lang/String;

    move-result-object v0

    .line 989761
    iput-object v0, v2, LX/3dM;->D:Ljava/lang/String;

    .line 989762
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;

    move-result-object v0

    .line 989763
    if-nez v0, :cond_7

    .line 989764
    const/4 v1, 0x0

    .line 989765
    :goto_2
    move-object v0, v1

    .line 989766
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 989767
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->q()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 989768
    if-nez v0, :cond_8

    .line 989769
    const/4 v1, 0x0

    .line 989770
    :goto_3
    move-object v0, v1

    .line 989771
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 989772
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->r()Ljava/lang/String;

    move-result-object v0

    .line 989773
    iput-object v0, v2, LX/3dM;->J:Ljava/lang/String;

    .line 989774
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 989775
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 989776
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->s()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 989777
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->s()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 989778
    if-nez v0, :cond_9

    .line 989779
    const/4 v4, 0x0

    .line 989780
    :goto_5
    move-object v0, v4

    .line 989781
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 989782
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 989783
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 989784
    iput-object v0, v2, LX/3dM;->N:LX/0Px;

    .line 989785
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->t()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 989786
    if-nez v0, :cond_a

    .line 989787
    const/4 v1, 0x0

    .line 989788
    :goto_6
    move-object v0, v1

    .line 989789
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 989790
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 989791
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    .line 989792
    if-nez v0, :cond_b

    .line 989793
    const/4 v1, 0x0

    .line 989794
    :goto_7
    move-object v0, v1

    .line 989795
    iput-object v0, v2, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 989796
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->w()I

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->b(I)LX/3dM;

    .line 989797
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0

    .line 989798
    :cond_3
    new-instance v4, LX/4Wx;

    invoke-direct {v4}, LX/4Wx;-><init>()V

    .line 989799
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 989800
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 989801
    const/4 v1, 0x0

    move v3, v1

    :goto_8
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 989802
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 989803
    if-nez v1, :cond_6

    .line 989804
    const/4 v6, 0x0

    .line 989805
    :goto_9
    move-object v1, v6

    .line 989806
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 989807
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 989808
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 989809
    iput-object v1, v4, LX/4Wx;->b:LX/0Px;

    .line 989810
    :cond_5
    invoke-virtual {v4}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v1

    goto/16 :goto_1

    .line 989811
    :cond_6
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 989812
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 989813
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 989814
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 989815
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 989816
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto :goto_9

    .line 989817
    :cond_7
    new-instance v1, LX/3dN;

    invoke-direct {v1}, LX/3dN;-><init>()V

    .line 989818
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;->a()I

    move-result v3

    .line 989819
    iput v3, v1, LX/3dN;->b:I

    .line 989820
    invoke-virtual {v1}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    goto/16 :goto_2

    .line 989821
    :cond_8
    new-instance v1, LX/3dO;

    invoke-direct {v1}, LX/3dO;-><init>()V

    .line 989822
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v3

    .line 989823
    iput v3, v1, LX/3dO;->b:I

    .line 989824
    invoke-virtual {v1}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    goto/16 :goto_3

    .line 989825
    :cond_9
    new-instance v4, LX/4WL;

    invoke-direct {v4}, LX/4WL;-><init>()V

    .line 989826
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v5

    .line 989827
    iput v5, v4, LX/4WL;->b:I

    .line 989828
    invoke-virtual {v4}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v4

    goto/16 :goto_5

    .line 989829
    :cond_a
    new-instance v1, LX/4ZH;

    invoke-direct {v1}, LX/4ZH;-><init>()V

    .line 989830
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v3

    .line 989831
    iput v3, v1, LX/4ZH;->b:I

    .line 989832
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v3

    .line 989833
    iput v3, v1, LX/4ZH;->e:I

    .line 989834
    invoke-virtual {v1}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    goto/16 :goto_6

    .line 989835
    :cond_b
    new-instance v1, LX/33O;

    invoke-direct {v1}, LX/33O;-><init>()V

    .line 989836
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 989837
    iput-object v3, v1, LX/33O;->aI:Ljava/lang/String;

    .line 989838
    invoke-virtual {v1}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    goto/16 :goto_7
.end method

.method private static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 989839
    if-nez p0, :cond_0

    .line 989840
    const/4 v0, 0x0

    .line 989841
    :goto_0
    return-object v0

    .line 989842
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 989843
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 989844
    iput v1, v0, LX/2dc;->c:I

    .line 989845
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 989846
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 989847
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 989848
    iput v1, v0, LX/2dc;->i:I

    .line 989849
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;)Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 8

    .prologue
    .line 989850
    if-nez p0, :cond_0

    .line 989851
    const/4 v0, 0x0

    .line 989852
    :goto_0
    return-object v0

    .line 989853
    :cond_0
    new-instance v2, LX/4Wz;

    invoke-direct {v2}, LX/4Wz;-><init>()V

    .line 989854
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 989855
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 989856
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 989857
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel;

    .line 989858
    if-nez v0, :cond_3

    .line 989859
    const/4 v4, 0x0

    .line 989860
    :goto_2
    move-object v0, v4

    .line 989861
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 989862
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 989863
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 989864
    iput-object v0, v2, LX/4Wz;->b:LX/0Px;

    .line 989865
    :cond_2
    invoke-virtual {v2}, LX/4Wz;->a()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    goto :goto_0

    .line 989866
    :cond_3
    new-instance v4, LX/4X0;

    invoke-direct {v4}, LX/4X0;-><init>()V

    .line 989867
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 989868
    iput-object v5, v4, LX/4X0;->b:Ljava/lang/String;

    .line 989869
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityObjectModel;

    move-result-object v5

    .line 989870
    if-nez v5, :cond_4

    .line 989871
    const/4 v6, 0x0

    .line 989872
    :goto_3
    move-object v5, v6

    .line 989873
    iput-object v5, v4, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 989874
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel;->d()LX/5LF;

    move-result-object v5

    .line 989875
    if-nez v5, :cond_5

    .line 989876
    const/4 v6, 0x0

    .line 989877
    :goto_4
    move-object v5, v6

    .line 989878
    iput-object v5, v4, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 989879
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;

    move-result-object v5

    .line 989880
    if-nez v5, :cond_6

    .line 989881
    const/4 v6, 0x0

    .line 989882
    :goto_5
    move-object v5, v6

    .line 989883
    iput-object v5, v4, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 989884
    invoke-virtual {v4}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v4

    goto :goto_2

    .line 989885
    :cond_4
    new-instance v6, LX/4XR;

    invoke-direct {v6}, LX/4XR;-><init>()V

    .line 989886
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 989887
    iput-object v7, v6, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 989888
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityObjectModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 989889
    iput-object v7, v6, LX/4XR;->fO:Ljava/lang/String;

    .line 989890
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityObjectModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 989891
    iput-object v7, v6, LX/4XR;->iB:Ljava/lang/String;

    .line 989892
    invoke-virtual {v6}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    goto :goto_3

    .line 989893
    :cond_5
    new-instance v6, LX/4Z3;

    invoke-direct {v6}, LX/4Z3;-><init>()V

    .line 989894
    invoke-interface {v5}, LX/5LF;->y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989895
    iput-object v7, v6, LX/4Z3;->j:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989896
    invoke-interface {v5}, LX/5LF;->x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989897
    iput-object v7, v6, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989898
    invoke-interface {v5}, LX/5LF;->w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989899
    iput-object v7, v6, LX/4Z3;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989900
    invoke-interface {v5}, LX/5LF;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989901
    iput-object v7, v6, LX/4Z3;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989902
    invoke-interface {v5}, LX/5LF;->u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989903
    iput-object v7, v6, LX/4Z3;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989904
    invoke-interface {v5}, LX/5LF;->t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    .line 989905
    iput-object v7, v6, LX/4Z3;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 989906
    invoke-virtual {v6}, LX/4Z3;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v6

    goto :goto_4

    .line 989907
    :cond_6
    new-instance v6, LX/4Z5;

    invoke-direct {v6}, LX/4Z5;-><init>()V

    .line 989908
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v7

    .line 989909
    if-nez v7, :cond_7

    .line 989910
    const/4 v0, 0x0

    .line 989911
    :goto_6
    move-object v7, v0

    .line 989912
    iput-object v7, v6, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 989913
    invoke-virtual {v6}, LX/4Z5;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v6

    goto :goto_5

    .line 989914
    :cond_7
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 989915
    invoke-virtual {v7}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 989916
    iput-object v5, v0, LX/2dc;->h:Ljava/lang/String;

    .line 989917
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_6
.end method

.method public static a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 15

    .prologue
    .line 989918
    if-nez p0, :cond_0

    .line 989919
    const/4 v0, 0x0

    .line 989920
    :goto_0
    return-object v0

    .line 989921
    :cond_0
    new-instance v2, LX/4XB;

    invoke-direct {v2}, LX/4XB;-><init>()V

    .line 989922
    invoke-interface {p0}, LX/5kD;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 989923
    iput-object v0, v2, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 989924
    invoke-interface {p0}, LX/5kD;->k()Ljava/lang/String;

    move-result-object v0

    .line 989925
    iput-object v0, v2, LX/4XB;->b:Ljava/lang/String;

    .line 989926
    invoke-interface {p0}, LX/5kD;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v0

    .line 989927
    if-nez v0, :cond_3

    .line 989928
    const/4 v1, 0x0

    .line 989929
    :goto_1
    move-object v0, v1

    .line 989930
    iput-object v0, v2, LX/4XB;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 989931
    invoke-interface {p0}, LX/5kD;->m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v0

    .line 989932
    if-nez v0, :cond_4

    .line 989933
    const/4 v1, 0x0

    .line 989934
    :goto_2
    move-object v0, v1

    .line 989935
    iput-object v0, v2, LX/4XB;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 989936
    invoke-interface {p0}, LX/5kD;->n()Ljava/lang/String;

    move-result-object v0

    .line 989937
    iput-object v0, v2, LX/4XB;->g:Ljava/lang/String;

    .line 989938
    invoke-interface {p0}, LX/5kD;->o()Z

    move-result v0

    .line 989939
    iput-boolean v0, v2, LX/4XB;->l:Z

    .line 989940
    invoke-interface {p0}, LX/5kD;->p()Z

    move-result v0

    .line 989941
    iput-boolean v0, v2, LX/4XB;->m:Z

    .line 989942
    invoke-interface {p0}, LX/5kD;->q()Z

    move-result v0

    .line 989943
    iput-boolean v0, v2, LX/4XB;->n:Z

    .line 989944
    invoke-interface {p0}, LX/5kD;->r()Z

    move-result v0

    .line 989945
    iput-boolean v0, v2, LX/4XB;->o:Z

    .line 989946
    invoke-interface {p0}, LX/5kD;->s()Z

    move-result v0

    .line 989947
    iput-boolean v0, v2, LX/4XB;->p:Z

    .line 989948
    invoke-interface {p0}, LX/5kD;->t()Z

    move-result v0

    .line 989949
    iput-boolean v0, v2, LX/4XB;->q:Z

    .line 989950
    invoke-interface {p0}, LX/5kD;->u()Z

    move-result v0

    .line 989951
    iput-boolean v0, v2, LX/4XB;->r:Z

    .line 989952
    invoke-interface {p0}, LX/5kD;->v()Z

    move-result v0

    .line 989953
    iput-boolean v0, v2, LX/4XB;->s:Z

    .line 989954
    invoke-interface {p0}, LX/5kD;->w()Z

    move-result v0

    .line 989955
    iput-boolean v0, v2, LX/4XB;->t:Z

    .line 989956
    invoke-interface {p0}, LX/5kD;->x()Z

    move-result v0

    .line 989957
    iput-boolean v0, v2, LX/4XB;->u:Z

    .line 989958
    invoke-interface {p0}, LX/5kD;->y()Z

    move-result v0

    .line 989959
    iput-boolean v0, v2, LX/4XB;->v:Z

    .line 989960
    invoke-interface {p0}, LX/5kD;->z()Z

    move-result v0

    .line 989961
    iput-boolean v0, v2, LX/4XB;->w:Z

    .line 989962
    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 989963
    iput-object v0, v2, LX/4XB;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 989964
    invoke-interface {p0}, LX/5kD;->B()J

    move-result-wide v0

    .line 989965
    iput-wide v0, v2, LX/4XB;->A:J

    .line 989966
    invoke-interface {p0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 989967
    iput-object v0, v2, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 989968
    invoke-interface {p0}, LX/5kD;->D()Z

    move-result v0

    .line 989969
    iput-boolean v0, v2, LX/4XB;->D:Z

    .line 989970
    invoke-interface {p0}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    .line 989971
    if-nez v0, :cond_7

    .line 989972
    const/4 v1, 0x0

    .line 989973
    :goto_3
    move-object v0, v1

    .line 989974
    iput-object v0, v2, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 989975
    invoke-interface {p0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    .line 989976
    if-nez v0, :cond_8

    .line 989977
    const/4 v1, 0x0

    .line 989978
    :goto_4
    move-object v0, v1

    .line 989979
    iput-object v0, v2, LX/4XB;->F:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 989980
    invoke-interface {p0}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 989981
    iput-object v0, v2, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 989982
    invoke-interface {p0}, LX/5kD;->c()LX/1f8;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1f8;)Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    .line 989983
    iput-object v0, v2, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 989984
    invoke-interface {p0}, LX/5kD;->H()D

    move-result-wide v0

    .line 989985
    iput-wide v0, v2, LX/4XB;->I:D

    .line 989986
    invoke-interface {p0}, LX/5kD;->I()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    move-result-object v0

    .line 989987
    if-nez v0, :cond_11

    .line 989988
    const/4 v4, 0x0

    .line 989989
    :goto_5
    move-object v0, v4

    .line 989990
    iput-object v0, v2, LX/4XB;->J:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 989991
    invoke-interface {p0}, LX/5kD;->J()Z

    move-result v0

    .line 989992
    iput-boolean v0, v2, LX/4XB;->K:Z

    .line 989993
    invoke-interface {p0}, LX/5kD;->K()Ljava/lang/String;

    move-result-object v0

    .line 989994
    iput-object v0, v2, LX/4XB;->R:Ljava/lang/String;

    .line 989995
    invoke-interface {p0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    .line 989996
    iput-object v0, v2, LX/4XB;->T:Ljava/lang/String;

    .line 989997
    invoke-interface {p0}, LX/5kD;->e()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 989998
    iput-object v0, v2, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 989999
    invoke-interface {p0}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990000
    iput-object v0, v2, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990001
    invoke-interface {p0}, LX/5kD;->L()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990002
    iput-object v0, v2, LX/4XB;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990003
    invoke-interface {p0}, LX/5kD;->ai_()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990004
    iput-object v0, v2, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990005
    invoke-interface {p0}, LX/5kD;->j()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990006
    iput-object v0, v2, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990007
    invoke-interface {p0}, LX/5kD;->M()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990008
    iput-object v0, v2, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990009
    invoke-interface {p0}, LX/5kD;->N()I

    move-result v0

    .line 990010
    iput v0, v2, LX/4XB;->ah:I

    .line 990011
    invoke-interface {p0}, LX/5kD;->O()I

    move-result v0

    .line 990012
    iput v0, v2, LX/4XB;->ai:I

    .line 990013
    invoke-interface {p0}, LX/5kD;->P()I

    move-result v0

    .line 990014
    iput v0, v2, LX/4XB;->aj:I

    .line 990015
    invoke-interface {p0}, LX/5kD;->Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;)Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    .line 990016
    iput-object v0, v2, LX/4XB;->ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 990017
    invoke-interface {p0}, LX/5kD;->R()Z

    move-result v0

    .line 990018
    iput-boolean v0, v2, LX/4XB;->an:Z

    .line 990019
    invoke-interface {p0}, LX/5kD;->S()Z

    move-result v0

    .line 990020
    iput-boolean v0, v2, LX/4XB;->at:Z

    .line 990021
    invoke-interface {p0}, LX/5kD;->T()Z

    move-result v0

    .line 990022
    iput-boolean v0, v2, LX/4XB;->ax:Z

    .line 990023
    invoke-interface {p0}, LX/5kD;->U()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 990024
    iput-object v0, v2, LX/4XB;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990025
    invoke-interface {p0}, LX/5kD;->V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v0

    .line 990026
    if-nez v0, :cond_15

    .line 990027
    const/4 v1, 0x0

    .line 990028
    :goto_6
    move-object v0, v1

    .line 990029
    iput-object v0, v2, LX/4XB;->aD:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 990030
    invoke-interface {p0}, LX/5kD;->W()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-result-object v0

    .line 990031
    if-nez v0, :cond_17

    .line 990032
    const/4 v1, 0x0

    .line 990033
    :goto_7
    move-object v0, v1

    .line 990034
    iput-object v0, v2, LX/4XB;->aG:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 990035
    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    .line 990036
    if-nez v0, :cond_1b

    .line 990037
    const/4 v1, 0x0

    .line 990038
    :goto_8
    move-object v0, v1

    .line 990039
    iput-object v0, v2, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 990040
    invoke-interface {p0}, LX/5kD;->Y()D

    move-result-wide v0

    .line 990041
    iput-wide v0, v2, LX/4XB;->aR:D

    .line 990042
    invoke-interface {p0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 990043
    iput-object v0, v2, LX/4XB;->aS:Lcom/facebook/graphql/model/GraphQLActor;

    .line 990044
    invoke-interface {p0}, LX/5kD;->aa()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v0

    .line 990045
    if-nez v0, :cond_20

    .line 990046
    const/4 v1, 0x0

    .line 990047
    :goto_9
    move-object v0, v1

    .line 990048
    iput-object v0, v2, LX/4XB;->aV:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 990049
    invoke-interface {p0}, LX/5kD;->ab()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 990050
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 990051
    const/4 v0, 0x0

    move v1, v0

    :goto_a
    invoke-interface {p0}, LX/5kD;->ab()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990052
    invoke-interface {p0}, LX/5kD;->ab()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;

    .line 990053
    if-nez v0, :cond_21

    .line 990054
    const/4 v4, 0x0

    .line 990055
    :goto_b
    move-object v0, v4

    .line 990056
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990057
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 990058
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 990059
    iput-object v0, v2, LX/4XB;->aW:LX/0Px;

    .line 990060
    :cond_2
    invoke-interface {p0}, LX/5kD;->ac()I

    move-result v0

    .line 990061
    iput v0, v2, LX/4XB;->bb:I

    .line 990062
    invoke-interface {p0}, LX/5kD;->ad()Ljava/lang/String;

    move-result-object v0

    .line 990063
    iput-object v0, v2, LX/4XB;->bc:Ljava/lang/String;

    .line 990064
    invoke-interface {p0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    .line 990065
    if-nez v0, :cond_26

    .line 990066
    const/4 v1, 0x0

    .line 990067
    :goto_c
    move-object v0, v1

    .line 990068
    iput-object v0, v2, LX/4XB;->bh:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 990069
    invoke-interface {p0}, LX/5kD;->af()LX/5QV;

    move-result-object v0

    .line 990070
    if-nez v0, :cond_28

    .line 990071
    const/4 v1, 0x0

    .line 990072
    :goto_d
    move-object v0, v1

    .line 990073
    iput-object v0, v2, LX/4XB;->bj:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 990074
    invoke-interface {p0}, LX/5kD;->ag()Ljava/lang/String;

    move-result-object v0

    .line 990075
    iput-object v0, v2, LX/4XB;->bl:Ljava/lang/String;

    .line 990076
    invoke-interface {p0}, LX/5kD;->ah()Z

    move-result v0

    .line 990077
    iput-boolean v0, v2, LX/4XB;->bs:Z

    .line 990078
    invoke-interface {p0}, LX/5kD;->ai()D

    move-result-wide v0

    .line 990079
    iput-wide v0, v2, LX/4XB;->bv:D

    .line 990080
    invoke-interface {p0}, LX/5kD;->aj()D

    move-result-wide v0

    .line 990081
    iput-wide v0, v2, LX/4XB;->bw:D

    .line 990082
    invoke-interface {p0}, LX/5kD;->ak()Ljava/lang/String;

    move-result-object v0

    .line 990083
    iput-object v0, v2, LX/4XB;->bx:Ljava/lang/String;

    .line 990084
    invoke-interface {p0}, LX/5kD;->al()Ljava/lang/String;

    move-result-object v0

    .line 990085
    iput-object v0, v2, LX/4XB;->by:Ljava/lang/String;

    .line 990086
    invoke-interface {p0}, LX/5kD;->am()I

    move-result v0

    .line 990087
    iput v0, v2, LX/4XB;->bz:I

    .line 990088
    invoke-interface {p0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    .line 990089
    if-nez v0, :cond_2a

    .line 990090
    const/4 v4, 0x0

    .line 990091
    :goto_e
    move-object v0, v4

    .line 990092
    iput-object v0, v2, LX/4XB;->bG:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 990093
    invoke-interface {p0}, LX/5kD;->ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    .line 990094
    if-nez v0, :cond_32

    .line 990095
    const/4 v1, 0x0

    .line 990096
    :goto_f
    move-object v0, v1

    .line 990097
    iput-object v0, v2, LX/4XB;->bP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 990098
    invoke-virtual {v2}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto/16 :goto_0

    .line 990099
    :cond_3
    new-instance v1, LX/4Vp;

    invoke-direct {v1}, LX/4Vp;-><init>()V

    .line 990100
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;->b()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v3

    .line 990101
    iput-object v3, v1, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 990102
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990103
    iput-object v3, v1, LX/4Vp;->m:Ljava/lang/String;

    .line 990104
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    goto/16 :goto_1

    .line 990105
    :cond_4
    new-instance v1, LX/4Vr;

    invoke-direct {v1}, LX/4Vr;-><init>()V

    .line 990106
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 990107
    iput-object v3, v1, LX/4Vr;->m:Ljava/lang/String;

    .line 990108
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990109
    iput-object v3, v1, LX/4Vr;->r:Ljava/lang/String;

    .line 990110
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->d()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v3

    .line 990111
    if-nez v3, :cond_5

    .line 990112
    const/4 v4, 0x0

    .line 990113
    :goto_10
    move-object v3, v4

    .line 990114
    iput-object v3, v1, LX/4Vr;->t:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    .line 990115
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v3

    .line 990116
    if-nez v3, :cond_6

    .line 990117
    const/4 v4, 0x0

    .line 990118
    :goto_11
    move-object v3, v4

    .line 990119
    iput-object v3, v1, LX/4Vr;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990120
    invoke-virtual {v1}, LX/4Vr;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    goto/16 :goto_2

    .line 990121
    :cond_5
    new-instance v4, LX/4XK;

    invoke-direct {v4}, LX/4XK;-><init>()V

    .line 990122
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 990123
    iput-object v5, v4, LX/4XK;->c:Ljava/lang/String;

    .line 990124
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 990125
    iput-object v5, v4, LX/4XK;->d:Ljava/lang/String;

    .line 990126
    new-instance v5, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    invoke-direct {v5, v4}, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;-><init>(LX/4XK;)V

    .line 990127
    move-object v4, v5

    .line 990128
    goto :goto_10

    .line 990129
    :cond_6
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 990130
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 990131
    iput-object v5, v4, LX/2dc;->h:Ljava/lang/String;

    .line 990132
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_11

    .line 990133
    :cond_7
    new-instance v1, LX/4Y6;

    invoke-direct {v1}, LX/4Y6;-><init>()V

    .line 990134
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 990135
    iput-object v3, v1, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990136
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990137
    iput-object v3, v1, LX/4Y6;->n:Ljava/lang/String;

    .line 990138
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 990139
    iput-object v3, v1, LX/4Y6;->r:Ljava/lang/String;

    .line 990140
    invoke-virtual {v1}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    goto/16 :goto_3

    .line 990141
    :cond_8
    new-instance v4, LX/4Y0;

    invoke-direct {v4}, LX/4Y0;-><init>()V

    .line 990142
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 990143
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 990144
    const/4 v1, 0x0

    move v3, v1

    :goto_12
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_9

    .line 990145
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 990146
    if-nez v1, :cond_b

    .line 990147
    const/4 v6, 0x0

    .line 990148
    :goto_13
    move-object v1, v6

    .line 990149
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990150
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_12

    .line 990151
    :cond_9
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 990152
    iput-object v1, v4, LX/4Y0;->b:LX/0Px;

    .line 990153
    :cond_a
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    invoke-direct {v1, v4}, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;-><init>(LX/4Y0;)V

    .line 990154
    move-object v1, v1

    .line 990155
    goto/16 :goto_4

    .line 990156
    :cond_b
    new-instance v6, LX/4WG;

    invoke-direct {v6}, LX/4WG;-><init>()V

    .line 990157
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(LX/1f8;)Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v7

    .line 990158
    iput-object v7, v6, LX/4WG;->b:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 990159
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v7

    invoke-static {v7}, LX/5k9;->a(LX/1f8;)Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v7

    .line 990160
    iput-object v7, v6, LX/4WG;->c:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 990161
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 990162
    iput-object v7, v6, LX/4WG;->d:Ljava/lang/String;

    .line 990163
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v7

    .line 990164
    if-nez v7, :cond_c

    .line 990165
    const/4 v8, 0x0

    .line 990166
    :goto_14
    move-object v7, v8

    .line 990167
    iput-object v7, v6, LX/4WG;->e:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 990168
    new-instance v7, Lcom/facebook/graphql/model/GraphQLFaceBox;

    invoke-direct {v7, v6}, Lcom/facebook/graphql/model/GraphQLFaceBox;-><init>(LX/4WG;)V

    .line 990169
    move-object v6, v7

    .line 990170
    goto :goto_13

    .line 990171
    :cond_c
    new-instance v10, LX/4WH;

    invoke-direct {v10}, LX/4WH;-><init>()V

    .line 990172
    invoke-virtual {v7}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_e

    .line 990173
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 990174
    const/4 v8, 0x0

    move v9, v8

    :goto_15
    invoke-virtual {v7}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_d

    .line 990175
    invoke-virtual {v7}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel$EdgesModel;

    .line 990176
    if-nez v8, :cond_f

    .line 990177
    const/4 v12, 0x0

    .line 990178
    :goto_16
    move-object v8, v12

    .line 990179
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990180
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_15

    .line 990181
    :cond_d
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 990182
    iput-object v8, v10, LX/4WH;->b:LX/0Px;

    .line 990183
    :cond_e
    new-instance v8, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    invoke-direct {v8, v10}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;-><init>(LX/4WH;)V

    .line 990184
    move-object v8, v8

    .line 990185
    goto :goto_14

    .line 990186
    :cond_f
    new-instance v12, LX/4WI;

    invoke-direct {v12}, LX/4WI;-><init>()V

    .line 990187
    invoke-virtual {v8}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v13

    .line 990188
    iput-object v13, v12, LX/4WI;->b:Ljava/lang/String;

    .line 990189
    invoke-virtual {v8}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxUserModel;

    move-result-object v13

    .line 990190
    if-nez v13, :cond_10

    .line 990191
    const/4 v1, 0x0

    .line 990192
    :goto_17
    move-object v13, v1

    .line 990193
    iput-object v13, v12, LX/4WI;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 990194
    new-instance v13, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;

    invoke-direct {v13, v12}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;-><init>(LX/4WI;)V

    .line 990195
    move-object v12, v13

    .line 990196
    goto :goto_16

    .line 990197
    :cond_10
    new-instance v1, LX/25F;

    invoke-direct {v1}, LX/25F;-><init>()V

    .line 990198
    invoke-virtual {v13}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxUserModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 990199
    iput-object v8, v1, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990200
    invoke-virtual {v13}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxUserModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 990201
    iput-object v8, v1, LX/25F;->C:Ljava/lang/String;

    .line 990202
    invoke-virtual {v1}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    goto :goto_17

    .line 990203
    :cond_11
    new-instance v6, LX/4ZS;

    invoke-direct {v6}, LX/4ZS;-><init>()V

    .line 990204
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 990205
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 990206
    const/4 v4, 0x0

    move v5, v4

    :goto_18
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_12

    .line 990207
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;

    .line 990208
    if-nez v4, :cond_14

    .line 990209
    const/4 v8, 0x0

    .line 990210
    :goto_19
    move-object v4, v8

    .line 990211
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990212
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_18

    .line 990213
    :cond_12
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 990214
    iput-object v4, v6, LX/4ZS;->b:LX/0Px;

    .line 990215
    :cond_13
    invoke-virtual {v6}, LX/4ZS;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v4

    goto/16 :goto_5

    .line 990216
    :cond_14
    new-instance v8, LX/4ZT;

    invoke-direct {v8}, LX/4ZT;-><init>()V

    .line 990217
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->a()I

    move-result v9

    .line 990218
    iput v9, v8, LX/4ZT;->b:I

    .line 990219
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->b()J

    move-result-wide v10

    .line 990220
    iput-wide v10, v8, LX/4ZT;->c:J

    .line 990221
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->c()I

    move-result v9

    .line 990222
    iput v9, v8, LX/4ZT;->d:I

    .line 990223
    invoke-virtual {v8}, LX/4ZT;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    move-result-object v8

    goto :goto_19

    .line 990224
    :cond_15
    new-instance v1, LX/4Vp;

    invoke-direct {v1}, LX/4Vp;-><init>()V

    .line 990225
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 990226
    iput-object v3, v1, LX/4Vp;->m:Ljava/lang/String;

    .line 990227
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v3

    .line 990228
    if-nez v3, :cond_16

    .line 990229
    const/4 v4, 0x0

    .line 990230
    :goto_1a
    move-object v3, v4

    .line 990231
    iput-object v3, v1, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 990232
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    goto/16 :goto_6

    .line 990233
    :cond_16
    new-instance v4, LX/4YL;

    invoke-direct {v4}, LX/4YL;-><init>()V

    .line 990234
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 990235
    iput-object v0, v4, LX/4YL;->h:Ljava/lang/String;

    .line 990236
    invoke-virtual {v4}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    goto :goto_1a

    .line 990237
    :cond_17
    new-instance v1, LX/4YF;

    invoke-direct {v1}, LX/4YF;-><init>()V

    .line 990238
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v3

    const/4 v4, 0x0

    .line 990239
    if-nez v3, :cond_19

    .line 990240
    :cond_18
    :goto_1b
    move-object v3, v4

    .line 990241
    iput-object v3, v1, LX/4YF;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 990242
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 990243
    iput-object v3, v1, LX/4YF;->c:Ljava/lang/String;

    .line 990244
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->c()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v3

    .line 990245
    iput-object v3, v1, LX/4YF;->d:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    .line 990246
    new-instance v3, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    invoke-direct {v3, v1}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;-><init>(LX/4YF;)V

    .line 990247
    move-object v1, v3

    .line 990248
    goto/16 :goto_7

    .line 990249
    :cond_19
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 990250
    if-eqz v5, :cond_18

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x25d6af

    if-ne v5, v6, :cond_18

    .line 990251
    new-instance v4, LX/4XY;

    invoke-direct {v4}, LX/4XY;-><init>()V

    .line 990252
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 990253
    iput-object v5, v4, LX/4XY;->ag:Ljava/lang/String;

    .line 990254
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 990255
    iput-object v5, v4, LX/4XY;->aT:Ljava/lang/String;

    .line 990256
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v5

    .line 990257
    if-nez v5, :cond_1a

    .line 990258
    const/4 v6, 0x0

    .line 990259
    :goto_1c
    move-object v5, v6

    .line 990260
    iput-object v5, v4, LX/4XY;->bp:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990261
    invoke-virtual {v4}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    goto :goto_1b

    .line 990262
    :cond_1a
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 990263
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 990264
    iput-object v3, v6, LX/2dc;->h:Ljava/lang/String;

    .line 990265
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_1c

    .line 990266
    :cond_1b
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 990267
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1d

    .line 990268
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 990269
    const/4 v1, 0x0

    move v3, v1

    :goto_1d
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_1c

    .line 990270
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1W5;

    .line 990271
    if-nez v1, :cond_1e

    .line 990272
    const/4 v6, 0x0

    .line 990273
    :goto_1e
    move-object v1, v6

    .line 990274
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990275
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1d

    .line 990276
    :cond_1c
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 990277
    iput-object v1, v4, LX/173;->e:LX/0Px;

    .line 990278
    :cond_1d
    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    .line 990279
    iput-object v1, v4, LX/173;->f:Ljava/lang/String;

    .line 990280
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    goto/16 :goto_8

    .line 990281
    :cond_1e
    new-instance v6, LX/4W6;

    invoke-direct {v6}, LX/4W6;-><init>()V

    .line 990282
    invoke-interface {v1}, LX/1W5;->a()LX/171;

    move-result-object v7

    .line 990283
    if-nez v7, :cond_1f

    .line 990284
    const/4 v8, 0x0

    .line 990285
    :goto_1f
    move-object v7, v8

    .line 990286
    iput-object v7, v6, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 990287
    invoke-interface {v1}, LX/1W5;->b()I

    move-result v7

    .line 990288
    iput v7, v6, LX/4W6;->c:I

    .line 990289
    invoke-interface {v1}, LX/1W5;->c()I

    move-result v7

    .line 990290
    iput v7, v6, LX/4W6;->d:I

    .line 990291
    invoke-virtual {v6}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v6

    goto :goto_1e

    .line 990292
    :cond_1f
    new-instance v8, LX/170;

    invoke-direct {v8}, LX/170;-><init>()V

    .line 990293
    invoke-interface {v7}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 990294
    iput-object v9, v8, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990295
    invoke-interface {v7}, LX/171;->c()LX/0Px;

    move-result-object v9

    .line 990296
    iput-object v9, v8, LX/170;->b:LX/0Px;

    .line 990297
    invoke-interface {v7}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v9

    .line 990298
    iput-object v9, v8, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 990299
    invoke-interface {v7}, LX/171;->e()Ljava/lang/String;

    move-result-object v9

    .line 990300
    iput-object v9, v8, LX/170;->o:Ljava/lang/String;

    .line 990301
    invoke-interface {v7}, LX/171;->v_()Ljava/lang/String;

    move-result-object v9

    .line 990302
    iput-object v9, v8, LX/170;->A:Ljava/lang/String;

    .line 990303
    invoke-interface {v7}, LX/171;->w_()Ljava/lang/String;

    move-result-object v9

    .line 990304
    iput-object v9, v8, LX/170;->X:Ljava/lang/String;

    .line 990305
    invoke-interface {v7}, LX/171;->j()Ljava/lang/String;

    move-result-object v9

    .line 990306
    iput-object v9, v8, LX/170;->Y:Ljava/lang/String;

    .line 990307
    invoke-virtual {v8}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    goto :goto_1f

    .line 990308
    :cond_20
    new-instance v1, LX/4Y6;

    invoke-direct {v1}, LX/4Y6;-><init>()V

    .line 990309
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 990310
    iput-object v3, v1, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990311
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990312
    iput-object v3, v1, LX/4Y6;->n:Ljava/lang/String;

    .line 990313
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 990314
    iput-object v3, v1, LX/4Y6;->r:Ljava/lang/String;

    .line 990315
    invoke-virtual {v1}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    goto/16 :goto_9

    .line 990316
    :cond_21
    new-instance v6, LX/4Xz;

    invoke-direct {v6}, LX/4Xz;-><init>()V

    .line 990317
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 990318
    iput-object v4, v6, LX/4Xz;->b:Ljava/lang/String;

    .line 990319
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 990320
    iput-object v4, v6, LX/4Xz;->c:Ljava/lang/String;

    .line 990321
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 990322
    iput-object v4, v6, LX/4Xz;->d:Ljava/lang/String;

    .line 990323
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;

    move-result-object v4

    .line 990324
    if-nez v4, :cond_24

    .line 990325
    const/4 v8, 0x0

    .line 990326
    :goto_20
    move-object v4, v8

    .line 990327
    iput-object v4, v6, LX/4Xz;->e:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 990328
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->ay_()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_23

    .line 990329
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 990330
    const/4 v4, 0x0

    move v5, v4

    :goto_21
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->ay_()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_22

    .line 990331
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->ay_()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;

    .line 990332
    if-nez v4, :cond_25

    .line 990333
    const/4 v8, 0x0

    .line 990334
    :goto_22
    move-object v4, v8

    .line 990335
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990336
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_21

    .line 990337
    :cond_22
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 990338
    iput-object v4, v6, LX/4Xz;->f:LX/0Px;

    .line 990339
    :cond_23
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->az_()I

    move-result v4

    .line 990340
    iput v4, v6, LX/4Xz;->h:I

    .line 990341
    invoke-virtual {v6}, LX/4Xz;->a()Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    move-result-object v4

    goto/16 :goto_b

    .line 990342
    :cond_24
    new-instance v8, LX/4Y5;

    invoke-direct {v8}, LX/4Y5;-><init>()V

    .line 990343
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->a()I

    move-result v9

    .line 990344
    iput v9, v8, LX/4Y5;->b:I

    .line 990345
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->b()I

    move-result v9

    .line 990346
    iput v9, v8, LX/4Y5;->c:I

    .line 990347
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->c()I

    move-result v9

    .line 990348
    iput v9, v8, LX/4Y5;->d:I

    .line 990349
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->d()I

    move-result v9

    .line 990350
    iput v9, v8, LX/4Y5;->e:I

    .line 990351
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->e()I

    move-result v9

    .line 990352
    iput v9, v8, LX/4Y5;->f:I

    .line 990353
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->aA_()I

    move-result v9

    .line 990354
    iput v9, v8, LX/4Y5;->g:I

    .line 990355
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->aB_()D

    move-result-wide v10

    .line 990356
    iput-wide v10, v8, LX/4Y5;->h:D

    .line 990357
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->j()D

    move-result-wide v10

    .line 990358
    iput-wide v10, v8, LX/4Y5;->i:D

    .line 990359
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->k()D

    move-result-wide v10

    .line 990360
    iput-wide v10, v8, LX/4Y5;->j:D

    .line 990361
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$SphericalMetadataModel;->l()D

    move-result-wide v10

    .line 990362
    iput-wide v10, v8, LX/4Y5;->k:D

    .line 990363
    new-instance v9, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-direct {v9, v8}, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;-><init>(LX/4Y5;)V

    .line 990364
    move-object v8, v9

    .line 990365
    goto/16 :goto_20

    .line 990366
    :cond_25
    new-instance v8, LX/4Y4;

    invoke-direct {v8}, LX/4Y4;-><init>()V

    .line 990367
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->a()I

    move-result v9

    .line 990368
    iput v9, v8, LX/4Y4;->b:I

    .line 990369
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->b()I

    move-result v9

    .line 990370
    iput v9, v8, LX/4Y4;->c:I

    .line 990371
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->c()I

    move-result v9

    .line 990372
    iput v9, v8, LX/4Y4;->d:I

    .line 990373
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->d()I

    move-result v9

    .line 990374
    iput v9, v8, LX/4Y4;->e:I

    .line 990375
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->e()Ljava/lang/String;

    move-result-object v9

    .line 990376
    iput-object v9, v8, LX/4Y4;->f:Ljava/lang/String;

    .line 990377
    new-instance v9, Lcom/facebook/graphql/model/GraphQLPhotoTile;

    invoke-direct {v9, v8}, Lcom/facebook/graphql/model/GraphQLPhotoTile;-><init>(LX/4Y4;)V

    .line 990378
    move-object v8, v9

    .line 990379
    goto/16 :goto_22

    .line 990380
    :cond_26
    new-instance v1, LX/4YL;

    invoke-direct {v1}, LX/4YL;-><init>()V

    .line 990381
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v3

    .line 990382
    iput-boolean v3, v1, LX/4YL;->b:Z

    .line 990383
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v3

    .line 990384
    if-nez v3, :cond_27

    .line 990385
    const/4 v4, 0x0

    .line 990386
    :goto_23
    move-object v3, v4

    .line 990387
    iput-object v3, v1, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990388
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990389
    iput-object v3, v1, LX/4YL;->h:Ljava/lang/String;

    .line 990390
    invoke-virtual {v1}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    goto/16 :goto_c

    .line 990391
    :cond_27
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 990392
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 990393
    iput-object v5, v4, LX/2dc;->h:Ljava/lang/String;

    .line 990394
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_23

    .line 990395
    :cond_28
    new-instance v1, LX/4Ww;

    invoke-direct {v1}, LX/4Ww;-><init>()V

    .line 990396
    invoke-interface {v0}, LX/5QV;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 990397
    iput-object v3, v1, LX/4Ww;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990398
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v3

    .line 990399
    iput-object v3, v1, LX/4Ww;->c:Ljava/lang/String;

    .line 990400
    invoke-interface {v0}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v3

    .line 990401
    if-nez v3, :cond_29

    .line 990402
    const/4 v4, 0x0

    .line 990403
    :goto_24
    move-object v3, v4

    .line 990404
    iput-object v3, v1, LX/4Ww;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990405
    new-instance v3, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-direct {v3, v1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;-><init>(LX/4Ww;)V

    .line 990406
    move-object v1, v3

    .line 990407
    goto/16 :goto_d

    .line 990408
    :cond_29
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 990409
    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 990410
    iput-object v0, v4, LX/2dc;->h:Ljava/lang/String;

    .line 990411
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_24

    .line 990412
    :cond_2a
    new-instance v6, LX/4Y2;

    invoke-direct {v6}, LX/4Y2;-><init>()V

    .line 990413
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_2c

    .line 990414
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 990415
    const/4 v4, 0x0

    move v5, v4

    :goto_25
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2b

    .line 990416
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 990417
    if-nez v4, :cond_2d

    .line 990418
    const/4 v8, 0x0

    .line 990419
    :goto_26
    move-object v4, v8

    .line 990420
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990421
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_25

    .line 990422
    :cond_2b
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 990423
    iput-object v4, v6, LX/4Y2;->b:LX/0Px;

    .line 990424
    :cond_2c
    new-instance v4, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    invoke-direct {v4, v6}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;-><init>(LX/4Y2;)V

    .line 990425
    move-object v4, v4

    .line 990426
    goto/16 :goto_e

    .line 990427
    :cond_2d
    new-instance v8, LX/4Y3;

    invoke-direct {v8}, LX/4Y3;-><init>()V

    .line 990428
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v9

    .line 990429
    iput-object v9, v8, LX/4Y3;->b:Ljava/lang/String;

    .line 990430
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v9

    .line 990431
    if-nez v9, :cond_2e

    .line 990432
    const/4 v10, 0x0

    .line 990433
    :goto_27
    move-object v9, v10

    .line 990434
    iput-object v9, v8, LX/4Y3;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 990435
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v9

    .line 990436
    if-nez v9, :cond_30

    .line 990437
    const/4 v10, 0x0

    .line 990438
    :goto_28
    move-object v9, v10

    .line 990439
    iput-object v9, v8, LX/4Y3;->d:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 990440
    new-instance v9, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    invoke-direct {v9, v8}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;-><init>(LX/4Y3;)V

    .line 990441
    move-object v8, v9

    .line 990442
    goto :goto_26

    .line 990443
    :cond_2e
    new-instance v10, LX/25F;

    invoke-direct {v10}, LX/25F;-><init>()V

    .line 990444
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 990445
    iput-object v11, v10, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990446
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v11

    .line 990447
    iput-object v11, v10, LX/25F;->C:Ljava/lang/String;

    .line 990448
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->d()Z

    move-result v11

    .line 990449
    iput-boolean v11, v10, LX/25F;->M:Z

    .line 990450
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v11

    .line 990451
    iput-object v11, v10, LX/25F;->T:Ljava/lang/String;

    .line 990452
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->aF_()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;

    move-result-object v11

    .line 990453
    if-nez v11, :cond_2f

    .line 990454
    const/4 v12, 0x0

    .line 990455
    :goto_29
    move-object v11, v12

    .line 990456
    iput-object v11, v10, LX/25F;->W:Lcom/facebook/graphql/model/GraphQLPage;

    .line 990457
    invoke-virtual {v10}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v10

    goto :goto_27

    .line 990458
    :cond_2f
    new-instance v12, LX/4XY;

    invoke-direct {v12}, LX/4XY;-><init>()V

    .line 990459
    invoke-virtual {v11}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 990460
    iput-object v9, v12, LX/4XY;->ag:Ljava/lang/String;

    .line 990461
    invoke-virtual {v12}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v12

    goto :goto_29

    .line 990462
    :cond_30
    new-instance v10, LX/4Y1;

    invoke-direct {v10}, LX/4Y1;-><init>()V

    .line 990463
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->a()Z

    move-result v11

    .line 990464
    iput-boolean v11, v10, LX/4Y1;->b:Z

    .line 990465
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->b()Ljava/lang/String;

    move-result-object v11

    .line 990466
    iput-object v11, v10, LX/4Y1;->c:Ljava/lang/String;

    .line 990467
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->c()LX/1f8;

    move-result-object v11

    invoke-static {v11}, LX/5k9;->a(LX/1f8;)Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v11

    .line 990468
    iput-object v11, v10, LX/4Y1;->d:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 990469
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->d()Ljava/lang/String;

    move-result-object v11

    .line 990470
    iput-object v11, v10, LX/4Y1;->e:Ljava/lang/String;

    .line 990471
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;

    move-result-object v11

    .line 990472
    if-nez v11, :cond_31

    .line 990473
    const/4 v12, 0x0

    .line 990474
    :goto_2a
    move-object v11, v12

    .line 990475
    iput-object v11, v10, LX/4Y1;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 990476
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->aE_()J

    move-result-wide v12

    .line 990477
    iput-wide v12, v10, LX/4Y1;->g:J

    .line 990478
    new-instance v11, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    invoke-direct {v11, v10}, Lcom/facebook/graphql/model/GraphQLPhotoTag;-><init>(LX/4Y1;)V

    .line 990479
    move-object v10, v11

    .line 990480
    goto :goto_28

    .line 990481
    :cond_31
    new-instance v12, LX/3dL;

    invoke-direct {v12}, LX/3dL;-><init>()V

    .line 990482
    invoke-virtual {v11}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 990483
    iput-object v13, v12, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990484
    invoke-virtual {v11}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;->b()Ljava/lang/String;

    move-result-object v13

    .line 990485
    iput-object v13, v12, LX/3dL;->ag:Ljava/lang/String;

    .line 990486
    invoke-virtual {v12}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v12

    goto :goto_2a

    .line 990487
    :cond_32
    new-instance v4, LX/4ZV;

    invoke-direct {v4}, LX/4ZV;-><init>()V

    .line 990488
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_34

    .line 990489
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 990490
    const/4 v1, 0x0

    move v3, v1

    :goto_2b
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_33

    .line 990491
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel$NodesModel;

    .line 990492
    if-nez v1, :cond_35

    .line 990493
    const/4 v6, 0x0

    .line 990494
    :goto_2c
    move-object v1, v6

    .line 990495
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990496
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2b

    .line 990497
    :cond_33
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 990498
    iput-object v1, v4, LX/4ZV;->b:LX/0Px;

    .line 990499
    :cond_34
    invoke-virtual {v4}, LX/4ZV;->a()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    goto/16 :goto_f

    .line 990500
    :cond_35
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 990501
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 990502
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990503
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 990504
    iput-object v7, v6, LX/3dL;->E:Ljava/lang/String;

    .line 990505
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 990506
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 990507
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto :goto_2c
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 13

    .prologue
    .line 990508
    if-nez p0, :cond_0

    .line 990509
    const/4 v0, 0x0

    .line 990510
    :goto_0
    return-object v0

    .line 990511
    :cond_0
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    .line 990512
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$ApplicationModel;

    move-result-object v0

    .line 990513
    if-nez v0, :cond_3

    .line 990514
    const/4 v1, 0x0

    .line 990515
    :goto_1
    move-object v0, v1

    .line 990516
    iput-object v0, v2, LX/23u;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 990517
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 990518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 990519
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990520
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel;

    .line 990521
    if-nez v0, :cond_4

    .line 990522
    const/4 v4, 0x0

    .line 990523
    :goto_3
    move-object v0, v4

    .line 990524
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990525
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 990526
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 990527
    iput-object v0, v2, LX/23u;->k:LX/0Px;

    .line 990528
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 990529
    iput-object v0, v2, LX/23u;->m:Ljava/lang/String;

    .line 990530
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 990531
    iput-object v0, v2, LX/23u;->N:Ljava/lang/String;

    .line 990532
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->aw_()Ljava/lang/String;

    move-result-object v0

    .line 990533
    iput-object v0, v2, LX/23u;->T:Ljava/lang/String;

    .line 990534
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->ax_()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$SponsoredDataModel;

    move-result-object v0

    .line 990535
    if-nez v0, :cond_8

    .line 990536
    const/4 v1, 0x0

    .line 990537
    :goto_4
    move-object v0, v1

    .line 990538
    iput-object v0, v2, LX/23u;->aw:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 990539
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 990540
    iput-object v0, v2, LX/23u;->aM:Ljava/lang/String;

    .line 990541
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 990542
    :cond_3
    new-instance v1, LX/4Vr;

    invoke-direct {v1}, LX/4Vr;-><init>()V

    .line 990543
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 990544
    iput-object v3, v1, LX/4Vr;->m:Ljava/lang/String;

    .line 990545
    invoke-virtual {v1}, LX/4Vr;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    goto :goto_1

    .line 990546
    :cond_4
    new-instance v6, LX/39x;

    invoke-direct {v6}, LX/39x;-><init>()V

    .line 990547
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 990548
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 990549
    const/4 v4, 0x0

    move v5, v4

    :goto_5
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 990550
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel$ActionLinksModel;

    .line 990551
    if-nez v4, :cond_7

    .line 990552
    const/4 v8, 0x0

    .line 990553
    :goto_6
    move-object v4, v8

    .line 990554
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990555
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 990556
    :cond_5
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 990557
    iput-object v4, v6, LX/39x;->b:LX/0Px;

    .line 990558
    :cond_6
    invoke-virtual {v6}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto :goto_3

    .line 990559
    :cond_7
    new-instance v8, LX/4Ys;

    invoke-direct {v8}, LX/4Ys;-><init>()V

    .line 990560
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 990561
    iput-object v9, v8, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 990562
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel$ActionLinksModel;->b()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v9

    .line 990563
    iput-object v9, v8, LX/4Ys;->b:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 990564
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel$ActionLinksModel;->c()J

    move-result-wide v10

    .line 990565
    iput-wide v10, v8, LX/4Ys;->x:J

    .line 990566
    invoke-virtual {v8}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v8

    goto :goto_6

    .line 990567
    :cond_8
    new-instance v1, LX/4Ym;

    invoke-direct {v1}, LX/4Ym;-><init>()V

    .line 990568
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$SponsoredDataModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$SponsoredDataModel$UserModel;

    move-result-object v3

    .line 990569
    if-nez v3, :cond_9

    .line 990570
    const/4 v4, 0x0

    .line 990571
    :goto_7
    move-object v3, v4

    .line 990572
    iput-object v3, v1, LX/4Ym;->p:Lcom/facebook/graphql/model/GraphQLUser;

    .line 990573
    new-instance v3, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-direct {v3, v1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;-><init>(LX/4Ym;)V

    .line 990574
    move-object v1, v3

    .line 990575
    goto/16 :goto_4

    .line 990576
    :cond_9
    new-instance v4, LX/33O;

    invoke-direct {v4}, LX/33O;-><init>()V

    .line 990577
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$SponsoredDataModel$UserModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 990578
    iput-object v0, v4, LX/33O;->U:Ljava/lang/String;

    .line 990579
    invoke-virtual {v4}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    goto :goto_7
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 990580
    if-nez p0, :cond_0

    .line 990581
    const/4 v0, 0x0

    .line 990582
    :goto_0
    return-object v0

    .line 990583
    :cond_0
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    .line 990584
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 990585
    iput-object v1, v0, LX/23u;->m:Ljava/lang/String;

    .line 990586
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 990587
    iput-object v1, v0, LX/23u;->N:Ljava/lang/String;

    .line 990588
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 990589
    iput-object v1, v0, LX/23u;->T:Ljava/lang/String;

    .line 990590
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    .line 990591
    if-nez v1, :cond_1

    .line 990592
    const/4 v2, 0x0

    .line 990593
    :goto_1
    move-object v1, v2

    .line 990594
    iput-object v1, v0, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 990595
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 990596
    :cond_1
    new-instance v2, LX/4YL;

    invoke-direct {v2}, LX/4YL;-><init>()V

    .line 990597
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v3

    .line 990598
    iput-boolean v3, v2, LX/4YL;->b:Z

    .line 990599
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v3

    .line 990600
    if-nez v3, :cond_2

    .line 990601
    const/4 v4, 0x0

    .line 990602
    :goto_2
    move-object v3, v4

    .line 990603
    iput-object v3, v2, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 990604
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 990605
    iput-object v3, v2, LX/4YL;->h:Ljava/lang/String;

    .line 990606
    invoke-virtual {v2}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    goto :goto_1

    .line 990607
    :cond_2
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 990608
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 990609
    iput-object p0, v4, LX/2dc;->h:Ljava/lang/String;

    .line 990610
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 6

    .prologue
    .line 990611
    if-nez p0, :cond_0

    .line 990612
    const/4 v0, 0x0

    .line 990613
    :goto_0
    return-object v0

    .line 990614
    :cond_0
    new-instance v2, LX/4Z6;

    invoke-direct {v2}, LX/4Z6;-><init>()V

    .line 990615
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 990616
    iput-object v0, v2, LX/4Z6;->b:Ljava/lang/String;

    .line 990617
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 990618
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 990619
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990620
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 990621
    if-nez v0, :cond_3

    .line 990622
    const/4 v4, 0x0

    .line 990623
    :goto_2
    move-object v0, v4

    .line 990624
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990625
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990626
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 990627
    iput-object v0, v2, LX/4Z6;->c:LX/0Px;

    .line 990628
    :cond_2
    invoke-virtual {v2}, LX/4Z6;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    goto :goto_0

    .line 990629
    :cond_3
    new-instance v4, LX/4Vm;

    invoke-direct {v4}, LX/4Vm;-><init>()V

    .line 990630
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    .line 990631
    iput v5, v4, LX/4Vm;->b:I

    .line 990632
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v5

    .line 990633
    iput-object v5, v4, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 990634
    invoke-virtual {v4}, LX/4Vm;->a()Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .locals 8

    .prologue
    .line 990635
    if-nez p0, :cond_0

    .line 990636
    const/4 v0, 0x0

    .line 990637
    :goto_0
    return-object v0

    .line 990638
    :cond_0
    new-instance v2, LX/3dQ;

    invoke-direct {v2}, LX/3dQ;-><init>()V

    .line 990639
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 990640
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 990641
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990642
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 990643
    if-nez v0, :cond_3

    .line 990644
    const/4 v4, 0x0

    .line 990645
    :goto_2
    move-object v0, v4

    .line 990646
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 990647
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990648
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 990649
    iput-object v0, v2, LX/3dQ;->b:LX/0Px;

    .line 990650
    :cond_2
    invoke-virtual {v2}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    goto :goto_0

    .line 990651
    :cond_3
    new-instance v4, LX/4ZJ;

    invoke-direct {v4}, LX/4ZJ;-><init>()V

    .line 990652
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 990653
    if-nez v5, :cond_4

    .line 990654
    const/4 v6, 0x0

    .line 990655
    :goto_3
    move-object v5, v6

    .line 990656
    iput-object v5, v4, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 990657
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v5

    .line 990658
    iput v5, v4, LX/4ZJ;->c:I

    .line 990659
    invoke-virtual {v4}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v4

    goto :goto_2

    .line 990660
    :cond_4
    new-instance v6, LX/4WM;

    invoke-direct {v6}, LX/4WM;-><init>()V

    .line 990661
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v7

    .line 990662
    iput v7, v6, LX/4WM;->f:I

    .line 990663
    invoke-virtual {v6}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v6

    goto :goto_3
.end method

.method public static a(LX/1f8;)Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 5

    .prologue
    .line 990664
    if-nez p0, :cond_0

    .line 990665
    const/4 v0, 0x0

    .line 990666
    :goto_0
    return-object v0

    .line 990667
    :cond_0
    new-instance v0, LX/4ZN;

    invoke-direct {v0}, LX/4ZN;-><init>()V

    .line 990668
    invoke-interface {p0}, LX/1f8;->a()D

    move-result-wide v2

    .line 990669
    iput-wide v2, v0, LX/4ZN;->b:D

    .line 990670
    invoke-interface {p0}, LX/1f8;->b()D

    move-result-wide v2

    .line 990671
    iput-wide v2, v0, LX/4ZN;->c:D

    .line 990672
    invoke-virtual {v0}, LX/4ZN;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 990673
    if-nez p1, :cond_0

    .line 990674
    :goto_0
    return v0

    .line 990675
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 990676
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 990677
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990678
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 990679
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990680
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990681
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 990682
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990683
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 990684
    if-nez p1, :cond_0

    .line 990685
    :goto_0
    return v0

    .line 990686
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990687
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    const/4 v3, 0x0

    .line 990688
    if-nez v2, :cond_1

    .line 990689
    :goto_1
    move v2, v3

    .line 990690
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 990691
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990692
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990693
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990694
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 990695
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 990696
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 990697
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 990698
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 990699
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 990700
    if-nez p1, :cond_0

    .line 990701
    :goto_0
    return v0

    .line 990702
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 990703
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 990704
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990705
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 990706
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990707
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990708
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 990709
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990710
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 990711
    if-nez p1, :cond_0

    .line 990712
    :goto_0
    return v2

    .line 990713
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    const/4 v1, 0x0

    .line 990714
    if-nez v0, :cond_3

    .line 990715
    :goto_1
    move v3, v1

    .line 990716
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    .line 990717
    if-eqz v4, :cond_2

    .line 990718
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 990719
    :goto_2
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 990720
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    aput v0, v5, v1

    .line 990721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 990722
    :cond_1
    invoke-virtual {p0, v5, v9}, LX/186;->a([IZ)I

    move-result v0

    .line 990723
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990724
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 990725
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 990726
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v6

    const/4 v7, 0x0

    .line 990727
    if-nez v6, :cond_4

    .line 990728
    :goto_4
    move v6, v7

    .line 990729
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 990730
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 990731
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 990732
    invoke-virtual {p0, v9, v0}, LX/186;->b(II)V

    .line 990733
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990734
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 990735
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 990736
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 990737
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 990738
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990739
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 990740
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990741
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 990742
    invoke-virtual {p0, v1, v3}, LX/186;->b(II)V

    .line 990743
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 990744
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 990745
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v8

    const/4 v10, 0x0

    .line 990746
    if-nez v8, :cond_5

    .line 990747
    :goto_5
    move v8, v10

    .line 990748
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 990749
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 990750
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 990751
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_4

    .line 990752
    :cond_5
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 990753
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 990754
    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 990755
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 990756
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto :goto_5
.end method

.method private static c(LX/186;Lcom/facebook/graphql/model/GraphQLPrivacyScope;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 990757
    if-nez p1, :cond_0

    .line 990758
    :goto_0
    return v0

    .line 990759
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    const/4 v2, 0x0

    .line 990760
    if-nez v1, :cond_1

    .line 990761
    :goto_1
    move v1, v2

    .line 990762
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 990763
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 990764
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a()Z

    move-result v3

    invoke-virtual {p0, v0, v3}, LX/186;->a(IZ)V

    .line 990765
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 990766
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 990767
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990768
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 990769
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 990770
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 990771
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 990772
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 990773
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 990774
    if-nez p1, :cond_0

    .line 990775
    :goto_0
    return v0

    .line 990776
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 990777
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 990778
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 990779
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 990780
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 990781
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 990782
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
