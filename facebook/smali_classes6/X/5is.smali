.class public final LX/5is;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 985018
    const/4 v5, 0x0

    .line 985019
    const-wide/16 v6, 0x0

    .line 985020
    const/4 v4, 0x0

    .line 985021
    const-wide/16 v2, 0x0

    .line 985022
    const/4 v1, 0x0

    .line 985023
    const/4 v0, 0x0

    .line 985024
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 985025
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 985026
    const/4 v0, 0x0

    .line 985027
    :goto_0
    return v0

    .line 985028
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_5

    .line 985029
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 985030
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 985031
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 985032
    const-string v4, "horizontal_alignment"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 985033
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 985034
    :cond_1
    const-string v4, "horizontal_margin"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 985035
    const/4 v0, 0x1

    .line 985036
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 985037
    :cond_2
    const-string v4, "vertical_alignment"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 985038
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 985039
    :cond_3
    const-string v4, "vertical_margin"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 985040
    const/4 v0, 0x1

    .line 985041
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 985042
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 985043
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 985044
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 985045
    if-eqz v1, :cond_6

    .line 985046
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985047
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 985048
    if-eqz v6, :cond_7

    .line 985049
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985050
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move-wide v8, v2

    move v10, v5

    move-wide v2, v6

    move v7, v4

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 985051
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985052
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 985053
    if-eqz v0, :cond_0

    .line 985054
    const-string v0, "horizontal_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985055
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985056
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985057
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 985058
    const-string v2, "horizontal_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985059
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985060
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 985061
    if-eqz v0, :cond_2

    .line 985062
    const-string v0, "vertical_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985063
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985064
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985065
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 985066
    const-string v2, "vertical_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985067
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985068
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985069
    return-void
.end method
