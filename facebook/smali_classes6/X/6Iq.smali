.class public final LX/6Iq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;


# instance fields
.field public final synthetic a:LX/6Iu;


# direct methods
.method public constructor <init>(LX/6Iu;)V
    .locals 0

    .prologue
    .line 1074624
    iput-object p1, p0, LX/6Iq;->a:LX/6Iu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onImageAvailable(Landroid/media/ImageReader;)V
    .locals 6

    .prologue
    .line 1074625
    iget-object v0, p0, LX/6Iq;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;

    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireNextImage()Landroid/media/Image;

    move-result-object v2

    iget-object v3, p0, LX/6Iq;->a:LX/6Iu;

    iget-object v3, v3, LX/6Iu;->m:Ljava/io/File;

    iget-object v4, p0, LX/6Iq;->a:LX/6Iu;

    iget-object v4, v4, LX/6Iu;->e:LX/6Iv;

    iget-object v5, p0, LX/6Iq;->a:LX/6Iu;

    iget-object v5, v5, LX/6Iu;->M:LX/6JG;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;-><init>(Landroid/media/Image;Ljava/io/File;LX/6Iv;LX/6JG;)V

    .line 1074626
    iget-object v2, v0, LX/6Iv;->d:Landroid/os/Handler;

    const v3, -0x589a01a2

    invoke-static {v2, v1, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1074627
    return-void
.end method
