.class public final LX/5B9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 45

    .prologue
    .line 861616
    const/16 v41, 0x0

    .line 861617
    const/16 v40, 0x0

    .line 861618
    const/16 v39, 0x0

    .line 861619
    const/16 v38, 0x0

    .line 861620
    const/16 v37, 0x0

    .line 861621
    const/16 v36, 0x0

    .line 861622
    const/16 v35, 0x0

    .line 861623
    const/16 v34, 0x0

    .line 861624
    const/16 v33, 0x0

    .line 861625
    const/16 v32, 0x0

    .line 861626
    const/16 v31, 0x0

    .line 861627
    const/16 v30, 0x0

    .line 861628
    const/16 v29, 0x0

    .line 861629
    const/16 v28, 0x0

    .line 861630
    const/16 v27, 0x0

    .line 861631
    const/16 v26, 0x0

    .line 861632
    const/16 v25, 0x0

    .line 861633
    const/16 v24, 0x0

    .line 861634
    const/16 v23, 0x0

    .line 861635
    const/16 v22, 0x0

    .line 861636
    const/16 v21, 0x0

    .line 861637
    const/16 v20, 0x0

    .line 861638
    const/16 v19, 0x0

    .line 861639
    const/16 v18, 0x0

    .line 861640
    const/16 v17, 0x0

    .line 861641
    const/16 v16, 0x0

    .line 861642
    const/4 v15, 0x0

    .line 861643
    const/4 v14, 0x0

    .line 861644
    const/4 v13, 0x0

    .line 861645
    const/4 v12, 0x0

    .line 861646
    const/4 v11, 0x0

    .line 861647
    const/4 v10, 0x0

    .line 861648
    const/4 v9, 0x0

    .line 861649
    const/4 v8, 0x0

    .line 861650
    const/4 v7, 0x0

    .line 861651
    const/4 v6, 0x0

    .line 861652
    const/4 v5, 0x0

    .line 861653
    const/4 v4, 0x0

    .line 861654
    const/4 v3, 0x0

    .line 861655
    const/4 v2, 0x0

    .line 861656
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    .line 861657
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861658
    const/4 v2, 0x0

    .line 861659
    :goto_0
    return v2

    .line 861660
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861661
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1d

    .line 861662
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v42

    .line 861663
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 861664
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1

    if-eqz v42, :cond_1

    .line 861665
    const-string v43, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_2

    .line 861666
    const/4 v13, 0x1

    .line 861667
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 861668
    :cond_2
    const-string v43, "can_see_voice_switcher"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_3

    .line 861669
    const/4 v12, 0x1

    .line 861670
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 861671
    :cond_3
    const-string v43, "can_viewer_comment"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_4

    .line 861672
    const/4 v11, 0x1

    .line 861673
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 861674
    :cond_4
    const-string v43, "can_viewer_comment_with_photo"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_5

    .line 861675
    const/4 v10, 0x1

    .line 861676
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 861677
    :cond_5
    const-string v43, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_6

    .line 861678
    const/4 v9, 0x1

    .line 861679
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 861680
    :cond_6
    const-string v43, "can_viewer_comment_with_video"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_7

    .line 861681
    const/4 v8, 0x1

    .line 861682
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 861683
    :cond_7
    const-string v43, "can_viewer_like"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_8

    .line 861684
    const/4 v7, 0x1

    .line 861685
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 861686
    :cond_8
    const-string v43, "can_viewer_react"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_9

    .line 861687
    const/4 v6, 0x1

    .line 861688
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 861689
    :cond_9
    const-string v43, "can_viewer_subscribe"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_a

    .line 861690
    const/4 v5, 0x1

    .line 861691
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 861692
    :cond_a
    const-string v43, "comments_mirroring_domain"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_b

    .line 861693
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 861694
    :cond_b
    const-string v43, "default_comment_ordering"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_c

    .line 861695
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 861696
    :cond_c
    const-string v43, "does_viewer_like"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_d

    .line 861697
    const/4 v4, 0x1

    .line 861698
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 861699
    :cond_d
    const-string v43, "id"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_e

    .line 861700
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 861701
    :cond_e
    const-string v43, "important_reactors"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_f

    .line 861702
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 861703
    :cond_f
    const-string v43, "is_viewer_subscribed"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_10

    .line 861704
    const/4 v3, 0x1

    .line 861705
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 861706
    :cond_10
    const-string v43, "legacy_api_post_id"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_11

    .line 861707
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto/16 :goto_1

    .line 861708
    :cond_11
    const-string v43, "like_sentence"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_12

    .line 861709
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 861710
    :cond_12
    const-string v43, "likers"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_13

    .line 861711
    invoke-static/range {p0 .. p1}, LX/5B6;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 861712
    :cond_13
    const-string v43, "reactors"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_14

    .line 861713
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 861714
    :cond_14
    const-string v43, "remixable_photo_uri"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_15

    .line 861715
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 861716
    :cond_15
    const-string v43, "seen_by"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_16

    .line 861717
    invoke-static/range {p0 .. p1}, LX/5B7;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 861718
    :cond_16
    const-string v43, "supported_reactions"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_17

    .line 861719
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 861720
    :cond_17
    const-string v43, "top_reactions"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_18

    .line 861721
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 861722
    :cond_18
    const-string v43, "viewer_acts_as_page"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_19

    .line 861723
    invoke-static/range {p0 .. p1}, LX/5B8;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 861724
    :cond_19
    const-string v43, "viewer_acts_as_person"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1a

    .line 861725
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 861726
    :cond_1a
    const-string v43, "viewer_does_not_like_sentence"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1b

    .line 861727
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 861728
    :cond_1b
    const-string v43, "viewer_feedback_reaction_key"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1c

    .line 861729
    const/4 v2, 0x1

    .line 861730
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 861731
    :cond_1c
    const-string v43, "viewer_likes_sentence"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_0

    .line 861732
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 861733
    :cond_1d
    const/16 v42, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 861734
    if-eqz v13, :cond_1e

    .line 861735
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 861736
    :cond_1e
    if-eqz v12, :cond_1f

    .line 861737
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 861738
    :cond_1f
    if-eqz v11, :cond_20

    .line 861739
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 861740
    :cond_20
    if-eqz v10, :cond_21

    .line 861741
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 861742
    :cond_21
    if-eqz v9, :cond_22

    .line 861743
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 861744
    :cond_22
    if-eqz v8, :cond_23

    .line 861745
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 861746
    :cond_23
    if-eqz v7, :cond_24

    .line 861747
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 861748
    :cond_24
    if-eqz v6, :cond_25

    .line 861749
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 861750
    :cond_25
    if-eqz v5, :cond_26

    .line 861751
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 861752
    :cond_26
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 861753
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 861754
    if-eqz v4, :cond_27

    .line 861755
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 861756
    :cond_27
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 861757
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 861758
    if-eqz v3, :cond_28

    .line 861759
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 861760
    :cond_28
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861761
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861762
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861763
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861764
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861765
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861766
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861767
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861768
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861769
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861770
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861771
    if-eqz v2, :cond_29

    .line 861772
    const/16 v2, 0x1a

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 861773
    :cond_29
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 861774
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
