.class public LX/5qw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/5om;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/util/SparseBooleanArray;

.field private final f:LX/5rq;

.field private final g:LX/5qe;

.field private final h:Lcom/facebook/react/uimanager/RootViewManager;

.field private final i:LX/5sO;

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1010492
    const-class v0, LX/5qw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/5qw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/5rq;)V
    .locals 1

    .prologue
    .line 1010493
    new-instance v0, Lcom/facebook/react/uimanager/RootViewManager;

    invoke-direct {v0}, Lcom/facebook/react/uimanager/RootViewManager;-><init>()V

    invoke-direct {p0, p1, v0}, LX/5qw;-><init>(LX/5rq;Lcom/facebook/react/uimanager/RootViewManager;)V

    .line 1010494
    return-void
.end method

.method public constructor <init>(LX/5rq;Lcom/facebook/react/uimanager/RootViewManager;)V
    .locals 1

    .prologue
    .line 1010495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010496
    new-instance v0, LX/5qe;

    invoke-direct {v0}, LX/5qe;-><init>()V

    iput-object v0, p0, LX/5qw;->g:LX/5qe;

    .line 1010497
    new-instance v0, LX/5sO;

    invoke-direct {v0}, LX/5sO;-><init>()V

    iput-object v0, p0, LX/5qw;->i:LX/5sO;

    .line 1010498
    new-instance v0, LX/5om;

    invoke-direct {v0}, LX/5om;-><init>()V

    iput-object v0, p0, LX/5qw;->b:LX/5om;

    .line 1010499
    iput-object p1, p0, LX/5qw;->f:LX/5rq;

    .line 1010500
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    .line 1010501
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    .line 1010502
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    .line 1010503
    iput-object p2, p0, LX/5qw;->h:Lcom/facebook/react/uimanager/RootViewManager;

    .line 1010504
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;
    .locals 7
    .param p2    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [LX/5rn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x10

    const/4 v1, 0x0

    .line 1010505
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1010506
    if-eqz p0, :cond_2

    .line 1010507
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "View tag:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010508
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  children("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): [\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1010509
    :goto_0
    invoke-virtual {p1, p0}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v2

    if-ge v0, v2, :cond_1

    move v2, v1

    .line 1010510
    :goto_1
    add-int v4, v0, v2

    invoke-virtual {p1, p0}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v5

    if-ge v4, v5, :cond_0

    if-ge v2, v6, :cond_0

    .line 1010511
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int v5, v0, v2

    invoke-virtual {p1, p0, v5}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010512
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1010513
    :cond_0
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010514
    add-int/lit8 v0, v0, 0x10

    goto :goto_0

    .line 1010515
    :cond_1
    const-string v0, " ],\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010516
    :cond_2
    if-eqz p2, :cond_5

    .line 1010517
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  indicesToRemove("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): [\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1010518
    :goto_2
    array-length v2, p2

    if-ge v0, v2, :cond_4

    move v2, v1

    .line 1010519
    :goto_3
    add-int v4, v0, v2

    array-length v5, p2

    if-ge v4, v5, :cond_3

    if-ge v2, v6, :cond_3

    .line 1010520
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int v5, v0, v2

    aget v5, p2, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010521
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1010522
    :cond_3
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010523
    add-int/lit8 v0, v0, 0x10

    goto :goto_2

    .line 1010524
    :cond_4
    const-string v0, " ],\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010525
    :cond_5
    if-eqz p3, :cond_8

    .line 1010526
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  viewsToAdd("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): [\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1010527
    :goto_4
    array-length v2, p3

    if-ge v0, v2, :cond_7

    move v2, v1

    .line 1010528
    :goto_5
    add-int v4, v0, v2

    array-length v5, p3

    if-ge v4, v5, :cond_6

    if-ge v2, v6, :cond_6

    .line 1010529
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int v5, v0, v2

    aget-object v5, p3, v5

    iget v5, v5, LX/5rn;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int v5, v0, v2

    aget-object v5, p3, v5

    iget v5, v5, LX/5rn;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "],"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010530
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1010531
    :cond_6
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010532
    add-int/lit8 v0, v0, 0x10

    goto :goto_4

    .line 1010533
    :cond_7
    const-string v0, " ],\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010534
    :cond_8
    if-eqz p4, :cond_b

    .line 1010535
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  tagsToDelete("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): [\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1010536
    :goto_6
    array-length v2, p4

    if-ge v0, v2, :cond_a

    move v2, v1

    .line 1010537
    :goto_7
    add-int v4, v0, v2

    array-length v5, p4

    if-ge v4, v5, :cond_9

    if-ge v2, v6, :cond_9

    .line 1010538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int v5, v0, v2

    aget v5, p4, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010539
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1010540
    :cond_9
    const-string v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010541
    add-int/lit8 v0, v0, 0x10

    goto :goto_6

    .line 1010542
    :cond_a
    const-string v0, " ]\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010543
    :cond_b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 1010544
    iget-boolean v0, p0, LX/5qw;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5qw;->i:LX/5sO;

    invoke-virtual {v0, p1}, LX/5sO;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010545
    iget-object v0, p0, LX/5qw;->i:LX/5sO;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/5sO;->a(Landroid/view/View;IIII)V

    .line 1010546
    :goto_0
    return-void

    .line 1010547
    :cond_0
    add-int v0, p2, p4

    add-int v1, p3, p5

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private static a([II)Z
    .locals 4
    .param p0    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1010548
    if-nez p0, :cond_1

    .line 1010549
    :cond_0
    :goto_0
    return v0

    .line 1010550
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 1010551
    if-ne v3, p1, :cond_2

    .line 1010552
    const/4 v0, 0x1

    goto :goto_0

    .line 1010553
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private d(I)LX/5rJ;
    .locals 3

    .prologue
    .line 1010554
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010555
    if-nez v0, :cond_0

    .line 1010556
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010557
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5rJ;

    return-object v0
.end method


# virtual methods
.method public final a(IFF)I
    .locals 3

    .prologue
    .line 1010558
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010559
    if-nez v0, :cond_0

    .line 1010560
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010561
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p2, p3, v0}, LX/5rK;->a(FFLandroid/view/ViewGroup;)I

    move-result v0

    return v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1010562
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010563
    if-nez v0, :cond_0

    .line 1010564
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to resolve view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " which doesn\'t exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010565
    :cond_0
    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1010566
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010567
    if-nez v0, :cond_0

    .line 1010568
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010569
    :cond_0
    invoke-virtual {v0, p2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1010570
    return-void
.end method

.method public final a(IIIIII)V
    .locals 8

    .prologue
    const-wide/32 v6, 0x2000000

    .line 1010571
    invoke-static {}, LX/5pe;->b()V

    .line 1010572
    const-string v0, "NativeViewHierarchyManager_updateLayout"

    invoke-static {v6, v7, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v1, "parentTag"

    invoke-virtual {v0, v1, p1}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v0

    const-string v1, "tag"

    invoke-virtual {v0, v1, p2}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1010573
    :try_start_0
    invoke-virtual {p0, p2}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1010574
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p6, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 1010575
    iget-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1010576
    iget-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ViewManager;

    .line 1010577
    instance-of v2, v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    if-eqz v2, :cond_1

    .line 1010578
    check-cast v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 1010579
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->t()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1010580
    invoke-direct/range {v0 .. v5}, LX/5qw;->a(Landroid/view/View;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010581
    :cond_0
    :goto_0
    invoke-static {v6, v7}, LX/018;->a(J)V

    .line 1010582
    return-void

    .line 1010583
    :cond_1
    :try_start_1
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to use view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as a parent, but its Manager doesn\'t extends ViewGroupManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1010584
    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, LX/018;->a(J)V

    throw v0

    :cond_2
    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1010585
    :try_start_2
    invoke-direct/range {v0 .. v5}, LX/5qw;->a(Landroid/view/View;IIII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(IILX/5pC;)V
    .locals 3
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1010586
    invoke-static {}, LX/5pe;->b()V

    .line 1010587
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010588
    if-nez v0, :cond_0

    .line 1010589
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to send command to a non-existing view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010590
    :cond_0
    invoke-virtual {p0, p1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v1

    .line 1010591
    invoke-virtual {v1, v0, p2, p3}, Lcom/facebook/react/uimanager/ViewManager;->a(Landroid/view/View;ILX/5pC;)V

    .line 1010592
    return-void
.end method

.method public final a(IIZ)V
    .locals 3

    .prologue
    .line 1010593
    if-nez p3, :cond_0

    .line 1010594
    iget-object v0, p0, LX/5qw;->g:LX/5qe;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LX/5qe;->a(ILandroid/view/ViewParent;)V

    .line 1010595
    :goto_0
    return-void

    .line 1010596
    :cond_0
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010597
    if-eq p2, p1, :cond_1

    instance-of v1, v0, Landroid/view/ViewParent;

    if-eqz v1, :cond_1

    .line 1010598
    iget-object v1, p0, LX/5qw;->g:LX/5qe;

    check-cast v0, Landroid/view/ViewParent;

    invoke-virtual {v1, p2, v0}, LX/5qe;->a(ILandroid/view/ViewParent;)V

    goto :goto_0

    .line 1010599
    :cond_1
    iget-object v1, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1010600
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot block native responder on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that is a root view"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/5pd;->a(Ljava/lang/String;)V

    .line 1010601
    :cond_2
    iget-object v1, p0, LX/5qw;->g:LX/5qe;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, LX/5qe;->a(ILandroid/view/ViewParent;)V

    goto :goto_0
.end method

.method public final a(ILX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1010471
    invoke-static {}, LX/5pe;->b()V

    .line 1010472
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010473
    if-nez v0, :cond_0

    .line 1010474
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010475
    :cond_0
    new-instance v2, Landroid/widget/PopupMenu;

    invoke-direct {p0, p1}, LX/5qw;->d(I)LX/5rJ;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 1010476
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    move v0, v1

    .line 1010477
    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 1010478
    invoke-interface {p2, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v1, v0, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1010479
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1010480
    :cond_1
    new-instance v0, LX/5qv;

    invoke-direct {v0, p3}, LX/5qv;-><init>(Lcom/facebook/react/bridge/Callback;)V

    .line 1010481
    invoke-virtual {v2, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1010482
    invoke-virtual {v2, v0}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 1010483
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    .line 1010484
    return-void
.end method

.method public final a(ILX/5rC;)V
    .locals 4

    .prologue
    .line 1010485
    invoke-static {}, LX/5pe;->b()V

    .line 1010486
    :try_start_0
    invoke-virtual {p0, p1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    .line 1010487
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1010488
    invoke-virtual {v0, v1, p2}, Lcom/facebook/react/uimanager/ViewManager;->a(Landroid/view/View;LX/5rC;)V
    :try_end_0
    .catch LX/5qo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1010489
    :goto_0
    return-void

    .line 1010490
    :catch_0
    move-exception v0

    .line 1010491
    sget-object v1, LX/5qw;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to update properties for view tag "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(ILX/5rH;LX/5rJ;)V
    .locals 0

    .prologue
    .line 1010345
    invoke-virtual {p0, p1, p2}, LX/5qw;->a(ILandroid/view/ViewGroup;)V

    .line 1010346
    return-void
.end method

.method public final a(ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1010347
    invoke-static {}, LX/5pe;->b()V

    .line 1010348
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1010349
    new-instance v0, LX/5qo;

    const-string v1, "Trying to add a root view with an explicit id already set. React Native uses the id field to track react tags and will overwrite this field. If that is fine, explicitly overwrite the id field to View.NO_ID before calling addMeasuredRootView."

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010350
    :cond_0
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1010351
    iget-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    iget-object v1, p0, LX/5qw;->h:Lcom/facebook/react/uimanager/RootViewManager;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1010352
    iget-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1010353
    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->setId(I)V

    .line 1010354
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1010355
    invoke-static {}, LX/5pe;->b()V

    .line 1010356
    invoke-virtual {p0, p1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    .line 1010357
    invoke-virtual {p0, p1}, LX/5qw;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1010358
    invoke-virtual {v0, v1, p2}, Lcom/facebook/react/uimanager/ViewManager;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 1010359
    return-void
.end method

.method public final a(I[I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1010360
    invoke-static {}, LX/5pe;->b()V

    .line 1010361
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010362
    if-nez v0, :cond_0

    .line 1010363
    new-instance v0, LX/5qz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No native view for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currently exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010364
    :cond_0
    invoke-static {v0}, LX/5rE;->a(Landroid/view/View;)LX/5rD;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1010365
    if-nez v1, :cond_1

    .line 1010366
    new-instance v0, LX/5qz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Native view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is no longer on screen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010367
    :cond_1
    invoke-virtual {v1, p2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1010368
    aget v1, p2, v4

    .line 1010369
    aget v2, p2, v5

    .line 1010370
    invoke-virtual {v0, p2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1010371
    aget v3, p2, v4

    sub-int v1, v3, v1

    aput v1, p2, v4

    .line 1010372
    aget v1, p2, v5

    sub-int/2addr v1, v2

    aput v1, p2, v5

    .line 1010373
    const/4 v1, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    aput v2, p2, v1

    .line 1010374
    const/4 v1, 0x3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    aput v0, p2, v1

    .line 1010375
    return-void
.end method

.method public final a(I[I[LX/5rn;[I)V
    .locals 7
    .param p2    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [LX/5rn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1010376
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1010377
    invoke-virtual {p0, p1}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 1010378
    if-nez v0, :cond_0

    .line 1010379
    new-instance v2, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to manageChildren view with tag "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " which doesn\'t exist\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010380
    :cond_0
    invoke-virtual {v1, v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v3

    .line 1010381
    if-eqz p2, :cond_6

    .line 1010382
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_6

    .line 1010383
    aget v5, p2, v2

    .line 1010384
    if-gez v5, :cond_1

    .line 1010385
    new-instance v2, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to remove a negative view index:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " view tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010386
    :cond_1
    invoke-virtual {v1, v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v6

    if-lt v5, v6, :cond_2

    .line 1010387
    new-instance v2, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to remove a view index above child count "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " view tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010388
    :cond_2
    if-lt v5, v3, :cond_3

    .line 1010389
    new-instance v2, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to remove an out of order view index:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " view tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010390
    :cond_3
    invoke-virtual {v1, v0, v5}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    .line 1010391
    iget-boolean v6, p0, LX/5qw;->j:Z

    if-eqz v6, :cond_4

    iget-object v6, p0, LX/5qw;->i:LX/5sO;

    invoke-virtual {v6, v3}, LX/5sO;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {p4, v3}, LX/5qw;->a([II)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1010392
    :cond_4
    invoke-virtual {v1, v0, v5}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;I)V

    .line 1010393
    :cond_5
    add-int/lit8 v2, v2, -0x1

    move v3, v5

    goto/16 :goto_0

    .line 1010394
    :cond_6
    if-eqz p3, :cond_8

    move v3, v4

    .line 1010395
    :goto_1
    array-length v2, p3

    if-ge v3, v2, :cond_8

    .line 1010396
    aget-object v5, p3, v3

    .line 1010397
    iget-object v2, p0, LX/5qw;->c:Landroid/util/SparseArray;

    iget v6, v5, LX/5rn;->b:I

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1010398
    if-nez v2, :cond_7

    .line 1010399
    new-instance v2, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to add unknown view tag: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v5, LX/5rn;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010400
    :cond_7
    iget v5, v5, LX/5rn;->c:I

    invoke-virtual {v1, v0, v2, v5}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;Landroid/view/View;I)V

    .line 1010401
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1010402
    :cond_8
    if-eqz p4, :cond_b

    .line 1010403
    :goto_2
    array-length v2, p4

    if-ge v4, v2, :cond_b

    .line 1010404
    aget v3, p4, v4

    .line 1010405
    iget-object v2, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1010406
    if-nez v2, :cond_9

    .line 1010407
    new-instance v2, LX/5qo;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying to destroy unknown view tag: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n detail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1, p2, p3, p4}, LX/5qw;->a(Landroid/view/ViewGroup;Lcom/facebook/react/uimanager/ViewGroupManager;[I[LX/5rn;[I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1010408
    :cond_9
    iget-boolean v3, p0, LX/5qw;->j:Z

    if-eqz v3, :cond_a

    iget-object v3, p0, LX/5qw;->i:LX/5sO;

    invoke-virtual {v3, v2}, LX/5sO;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1010409
    iget-object v3, p0, LX/5qw;->i:LX/5sO;

    new-instance v5, LX/5qu;

    invoke-direct {v5, p0, v1, v0, v2}, LX/5qu;-><init>(LX/5qw;Lcom/facebook/react/uimanager/ViewGroupManager;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v3, v2, v5}, LX/5sO;->a(Landroid/view/View;LX/5qt;)V

    .line 1010410
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1010411
    :cond_a
    invoke-virtual {p0, v2}, LX/5qw;->a(Landroid/view/View;)V

    goto :goto_3

    .line 1010412
    :cond_b
    return-void
.end method

.method public final a(LX/5pG;)V
    .locals 1

    .prologue
    .line 1010413
    iget-object v0, p0, LX/5qw;->i:LX/5sO;

    invoke-virtual {v0, p1}, LX/5sO;->a(LX/5pG;)V

    .line 1010414
    return-void
.end method

.method public final a(LX/5rJ;ILjava/lang/String;LX/5rC;)V
    .locals 6
    .param p4    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/32 v4, 0x2000000

    .line 1010415
    invoke-static {}, LX/5pe;->b()V

    .line 1010416
    const-string v0, "NativeViewHierarchyManager_createView"

    invoke-static {v4, v5, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v1, "tag"

    invoke-virtual {v0, v1, p2}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v0

    const-string v1, "className"

    invoke-virtual {v0, v1, p3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1010417
    :try_start_0
    iget-object v0, p0, LX/5qw;->f:LX/5rq;

    invoke-virtual {v0, p3}, LX/5rq;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    .line 1010418
    iget-object v1, p0, LX/5qw;->g:LX/5qe;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/react/uimanager/ViewManager;->a(LX/5rJ;LX/5qe;)Landroid/view/View;

    move-result-object v1

    .line 1010419
    iget-object v2, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1010420
    iget-object v2, p0, LX/5qw;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1010421
    invoke-virtual {v1, p2}, Landroid/view/View;->setId(I)V

    .line 1010422
    if-eqz p4, :cond_0

    .line 1010423
    invoke-virtual {v0, v1, p4}, Lcom/facebook/react/uimanager/ViewManager;->a(Landroid/view/View;LX/5rC;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010424
    :cond_0
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1010425
    return-void

    .line 1010426
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method

.method public a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1010427
    invoke-static {}, LX/5pe;->b()V

    .line 1010428
    iget-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1010429
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, LX/5qw;->b(I)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ViewManager;->a(Landroid/view/View;)V

    .line 1010430
    :cond_0
    iget-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ViewManager;

    .line 1010431
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    instance-of v1, v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    if-eqz v1, :cond_3

    move-object v1, p1

    .line 1010432
    check-cast v1, Landroid/view/ViewGroup;

    .line 1010433
    check-cast v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 1010434
    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/ViewGroup;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_2

    .line 1010435
    invoke-virtual {v0, v1, v2}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    .line 1010436
    iget-object v4, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1010437
    invoke-virtual {p0, v3}, LX/5qw;->a(Landroid/view/View;)V

    .line 1010438
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1010439
    :cond_2
    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ViewGroupManager;->c(Landroid/view/ViewGroup;)V

    .line 1010440
    :cond_3
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 1010441
    iget-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 1010442
    return-void
.end method

.method public final b(I)Lcom/facebook/react/uimanager/ViewManager;
    .locals 3

    .prologue
    .line 1010443
    iget-object v0, p0, LX/5qw;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ViewManager;

    .line 1010444
    if-nez v0, :cond_0

    .line 1010445
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewManager for tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " could not be found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010446
    :cond_0
    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1010447
    iget-object v0, p0, LX/5qw;->g:LX/5qe;

    invoke-virtual {v0}, LX/5qe;->a()V

    .line 1010448
    return-void
.end method

.method public final b(I[I)V
    .locals 5

    .prologue
    .line 1010449
    invoke-static {}, LX/5pe;->b()V

    .line 1010450
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010451
    if-nez v0, :cond_0

    .line 1010452
    new-instance v0, LX/5qz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No native view for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currently exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010453
    :cond_0
    invoke-virtual {v0, p2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1010454
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1010455
    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1010456
    if-lez v2, :cond_1

    .line 1010457
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1010458
    const/4 v2, 0x1

    aget v3, p2, v2

    sub-int v1, v3, v1

    aput v1, p2, v2

    .line 1010459
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    aput v2, p2, v1

    .line 1010460
    const/4 v1, 0x3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    aput v0, p2, v1

    .line 1010461
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1010462
    iget-object v0, p0, LX/5qw;->i:LX/5sO;

    invoke-virtual {v0}, LX/5sO;->a()V

    .line 1010463
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1010464
    invoke-static {}, LX/5pe;->b()V

    .line 1010465
    iget-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1010466
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "View with tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not registered as a root view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5pd;->a(Ljava/lang/String;)V

    .line 1010467
    :cond_0
    iget-object v0, p0, LX/5qw;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1010468
    invoke-virtual {p0, v0}, LX/5qw;->a(Landroid/view/View;)V

    .line 1010469
    iget-object v0, p0, LX/5qw;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 1010470
    return-void
.end method
