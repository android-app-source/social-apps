.class public final LX/5GC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 887336
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 887337
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887338
    :goto_0
    return v1

    .line 887339
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_5

    .line 887340
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 887341
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 887342
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 887343
    const-string v10, "confidence"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 887344
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 887345
    :cond_1
    const-string v10, "funnel_logging_tags"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 887346
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 887347
    :cond_2
    const-string v10, "place_entities"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 887348
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 887349
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_3

    .line 887350
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_3

    .line 887351
    invoke-static {p0, p1}, LX/5GB;->b(LX/15w;LX/186;)I

    move-result v9

    .line 887352
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 887353
    :cond_3
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 887354
    goto :goto_1

    .line 887355
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 887356
    :cond_5
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 887357
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 887358
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 887359
    :cond_6
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 887360
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 887361
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 887317
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887318
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887319
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 887320
    const-string v2, "confidence"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887321
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887322
    :cond_0
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 887323
    if-eqz v0, :cond_1

    .line 887324
    const-string v0, "funnel_logging_tags"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887325
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 887326
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887327
    if-eqz v0, :cond_3

    .line 887328
    const-string v1, "place_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887329
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 887330
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 887331
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5GB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 887332
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 887333
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 887334
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887335
    return-void
.end method
