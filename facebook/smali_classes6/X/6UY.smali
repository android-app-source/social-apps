.class public LX/6UY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1101796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101797
    const/16 v0, 0xff

    iput v0, p0, LX/6UY;->f:I

    .line 1101798
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1101794
    const/4 v2, 0x0

    const/16 v5, 0xff

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;III)V

    .line 1101795
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;II)V
    .locals 6

    .prologue
    .line 1101792
    const/4 v2, 0x0

    const/16 v5, 0xff

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;III)V

    .line 1101793
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;III)V
    .locals 1

    .prologue
    .line 1101784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101785
    const/16 v0, 0xff

    iput v0, p0, LX/6UY;->f:I

    .line 1101786
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    .line 1101787
    iput-object p2, p0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    .line 1101788
    iput p3, p0, LX/6UY;->e:I

    .line 1101789
    iput p4, p0, LX/6UY;->d:I

    .line 1101790
    iput p5, p0, LX/6UY;->f:I

    .line 1101791
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1101782
    const/4 v2, 0x0

    const/16 v5, 0xff

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;III)V

    .line 1101783
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;II)V
    .locals 6

    .prologue
    .line 1101770
    const/4 v2, 0x0

    const/16 v5, 0xff

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;III)V

    .line 1101771
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1101780
    const/16 v5, 0xff

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;III)V

    .line 1101781
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;III)V
    .locals 1

    .prologue
    .line 1101772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101773
    const/16 v0, 0xff

    iput v0, p0, LX/6UY;->f:I

    .line 1101774
    iput-object p1, p0, LX/6UY;->a:Landroid/net/Uri;

    .line 1101775
    iput-object p2, p0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    .line 1101776
    iput p3, p0, LX/6UY;->e:I

    .line 1101777
    iput p4, p0, LX/6UY;->d:I

    .line 1101778
    iput p5, p0, LX/6UY;->f:I

    .line 1101779
    return-void
.end method
