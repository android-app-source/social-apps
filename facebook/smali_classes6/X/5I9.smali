.class public final LX/5I9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 893404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 893196
    if-nez p1, :cond_1

    .line 893197
    :cond_0
    :goto_0
    return v0

    .line 893198
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 893199
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-ne v1, v2, :cond_0

    .line 893200
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, 0x0

    .line 893201
    if-nez v1, :cond_2

    .line 893202
    :goto_1
    move v1, v2

    .line 893203
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    const/4 v3, 0x0

    .line 893204
    if-nez v2, :cond_3

    .line 893205
    :goto_2
    move v2, v3

    .line 893206
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893207
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 893208
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p0, v5}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v5

    .line 893209
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p0, v6}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v6

    .line 893210
    const/4 v7, 0x6

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 893211
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893212
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 893213
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 893214
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 893215
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 893216
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 893217
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893218
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893219
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893220
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 893221
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 893222
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893223
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 893224
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 893225
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 893226
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 893227
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 893228
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLComment;)I
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 893229
    if-nez p1, :cond_0

    .line 893230
    :goto_0
    return v2

    .line 893231
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v3

    .line 893232
    if-eqz v3, :cond_2

    .line 893233
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 893234
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 893235
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0, v0}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    aput v0, v4, v1

    .line 893236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 893237
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 893238
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893239
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    const/4 v4, 0x0

    .line 893240
    if-nez v3, :cond_3

    .line 893241
    :goto_3
    move v3, v4

    .line 893242
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 893243
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 893244
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 893245
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 893246
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893247
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 893248
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 893249
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 893250
    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 893251
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 893252
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 893253
    if-nez p1, :cond_0

    .line 893254
    :goto_0
    return v0

    .line 893255
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v2, 0x0

    .line 893256
    if-nez v1, :cond_1

    .line 893257
    :goto_1
    move v1, v2

    .line 893258
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893259
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893260
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 893261
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 893262
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893263
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893264
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 893265
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 893266
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 893267
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 893268
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 893269
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 893270
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 893271
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 893272
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 893273
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 893274
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 893275
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 893276
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 893277
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 893278
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 893279
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893280
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 893281
    if-nez p1, :cond_0

    .line 893282
    :goto_0
    return v0

    .line 893283
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893284
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893285
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 893286
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 893287
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 893288
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893289
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLLocation;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 893290
    if-nez p1, :cond_0

    .line 893291
    :goto_0
    return v1

    .line 893292
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 893293
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893294
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893295
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 893296
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 893297
    if-nez p1, :cond_0

    .line 893298
    :goto_0
    return v0

    .line 893299
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893300
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893301
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893302
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893303
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 893304
    if-nez p1, :cond_0

    .line 893305
    :goto_0
    return v0

    .line 893306
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 893307
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1, v0}, LX/186;->a(III)V

    .line 893308
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893309
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;)I
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 893336
    if-nez p1, :cond_0

    .line 893337
    :goto_0
    return v2

    .line 893338
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893339
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->k()LX/0Px;

    move-result-object v4

    .line 893340
    if-eqz v4, :cond_2

    .line 893341
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 893342
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 893343
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v7, 0x0

    .line 893344
    if-nez v0, :cond_3

    .line 893345
    :goto_2
    move v0, v7

    .line 893346
    aput v0, v5, v1

    .line 893347
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 893348
    :cond_1
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 893349
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893350
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 893351
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 893352
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 893353
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893354
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893355
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 893356
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 893357
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v9

    invoke-static {p0, v9}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLLocation;)I

    move-result v9

    .line 893358
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 893359
    const/4 v11, 0x3

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 893360
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 893361
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v9}, LX/186;->b(II)V

    .line 893362
    const/4 v7, 0x2

    invoke-virtual {p0, v7, v10}, LX/186;->b(II)V

    .line 893363
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 893364
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 893310
    if-nez p1, :cond_0

    .line 893311
    :goto_0
    return v0

    .line 893312
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893313
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    const/4 v3, 0x0

    .line 893314
    if-nez v2, :cond_1

    .line 893315
    :goto_1
    move v2, v3

    .line 893316
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893317
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 893318
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 893319
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 893320
    const/4 v7, 0x6

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 893321
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893322
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 893323
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 893324
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 893325
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 893326
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 893327
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893328
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893329
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 893330
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 893331
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 893332
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 893333
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 893334
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 893335
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLRating;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 893405
    if-nez p1, :cond_0

    .line 893406
    :goto_0
    return v0

    .line 893407
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 893408
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLRating;->j()I

    move-result v1

    invoke-virtual {p0, v0, v1, v0}, LX/186;->a(III)V

    .line 893409
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893410
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893411
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 893412
    if-nez p1, :cond_0

    .line 893413
    :goto_0
    return v0

    .line 893414
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    const/4 p1, 0x1

    const/4 v4, 0x0

    .line 893415
    if-nez v1, :cond_1

    .line 893416
    :goto_1
    move v1, v4

    .line 893417
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893418
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893419
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893420
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893421
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 893422
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v5

    .line 893423
    if-eqz v5, :cond_d

    .line 893424
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    new-array v6, v2, [I

    move v3, v4

    .line 893425
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 893426
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-static {p0, v2}, LX/5I9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v2

    aput v2, v6, v3

    .line 893427
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 893428
    :cond_2
    invoke-virtual {p0, v6, p1}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 893429
    :goto_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v6

    .line 893430
    if-eqz v6, :cond_c

    .line 893431
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    move v5, v4

    .line 893432
    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_3

    .line 893433
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v2

    aput v2, v7, v5

    .line 893434
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 893435
    :cond_3
    invoke-virtual {p0, v7, p1}, LX/186;->a([IZ)I

    move-result v2

    move v5, v2

    .line 893436
    :goto_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 893437
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v7

    .line 893438
    if-eqz v7, :cond_b

    .line 893439
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    new-array v8, v2, [I

    move v6, v4

    .line 893440
    :goto_6
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    if-ge v6, v2, :cond_4

    .line 893441
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)I

    move-result v2

    aput v2, v8, v6

    .line 893442
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_6

    .line 893443
    :cond_4
    invoke-virtual {p0, v8, p1}, LX/186;->a([IZ)I

    move-result v2

    move v6, v2

    .line 893444
    :goto_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v8

    .line 893445
    if-eqz v8, :cond_a

    .line 893446
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    new-array v9, v2, [I

    move v7, v4

    .line 893447
    :goto_8
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    if-ge v7, v2, :cond_5

    .line 893448
    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;)I

    move-result v2

    aput v2, v9, v7

    .line 893449
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_8

    .line 893450
    :cond_5
    invoke-virtual {p0, v9, p1}, LX/186;->a([IZ)I

    move-result v2

    move v7, v2

    .line 893451
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v9

    .line 893452
    if-eqz v9, :cond_9

    .line 893453
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    new-array v12, v2, [I

    move v8, v4

    .line 893454
    :goto_a
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-ge v8, v2, :cond_6

    .line 893455
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-static {p0, v2}, LX/5I9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v2

    aput v2, v12, v8

    .line 893456
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_a

    .line 893457
    :cond_6
    invoke-virtual {p0, v12, p1}, LX/186;->a([IZ)I

    move-result v2

    move v8, v2

    .line 893458
    :goto_b
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v12

    .line 893459
    if-eqz v12, :cond_8

    .line 893460
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    new-array v13, v2, [I

    move v9, v4

    .line 893461
    :goto_c
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    if-ge v9, v2, :cond_7

    .line 893462
    invoke-virtual {v12, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v2

    aput v2, v13, v9

    .line 893463
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_c

    .line 893464
    :cond_7
    invoke-virtual {p0, v13, p1}, LX/186;->a([IZ)I

    move-result v2

    .line 893465
    :goto_d
    const/16 v9, 0xa

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 893466
    invoke-virtual {p0, v4, v10}, LX/186;->b(II)V

    .line 893467
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v4

    invoke-virtual {p0, p1, v4}, LX/186;->a(IZ)V

    .line 893468
    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v9

    invoke-virtual {p0, v4, v9}, LX/186;->a(IZ)V

    .line 893469
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v3}, LX/186;->b(II)V

    .line 893470
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 893471
    const/4 v3, 0x5

    invoke-virtual {p0, v3, v11}, LX/186;->b(II)V

    .line 893472
    const/4 v3, 0x6

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 893473
    const/4 v3, 0x7

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 893474
    const/16 v3, 0x8

    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 893475
    const/16 v3, 0x9

    invoke-virtual {p0, v3, v2}, LX/186;->b(II)V

    .line 893476
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 893477
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_8
    move v2, v4

    goto :goto_d

    :cond_9
    move v8, v4

    goto :goto_b

    :cond_a
    move v7, v4

    goto/16 :goto_9

    :cond_b
    move v6, v4

    goto/16 :goto_7

    :cond_c
    move v5, v4

    goto/16 :goto_5

    :cond_d
    move v3, v4

    goto/16 :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStreetAddress;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 893397
    if-nez p1, :cond_0

    .line 893398
    :goto_0
    return v0

    .line 893399
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 893400
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893401
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893402
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893403
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 893365
    if-nez p1, :cond_0

    .line 893366
    :goto_0
    return v2

    .line 893367
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v3

    .line 893368
    if-eqz v3, :cond_4

    .line 893369
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 893370
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 893371
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    const/4 v5, 0x0

    .line 893372
    if-nez v0, :cond_5

    .line 893373
    :goto_2
    move v0, v5

    .line 893374
    aput v0, v4, v1

    .line 893375
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 893376
    :cond_1
    invoke-virtual {p0, v4, v6}, LX/186;->a([IZ)I

    move-result v0

    move v1, v0

    .line 893377
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v4

    .line 893378
    if-eqz v4, :cond_3

    .line 893379
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v3, v2

    .line 893380
    :goto_4
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 893381
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {p0, v0}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v0

    aput v0, v5, v3

    .line 893382
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 893383
    :cond_2
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 893384
    :goto_5
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893385
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 893386
    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 893387
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 893388
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 893389
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893390
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_5

    :cond_4
    move v1, v2

    goto :goto_3

    .line 893391
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 893392
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->a()I

    move-result v7

    invoke-virtual {p0, v5, v7, v5}, LX/186;->a(III)V

    .line 893393
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v8

    invoke-virtual {p0, v7, v8, v5}, LX/186;->a(III)V

    .line 893394
    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v8

    invoke-virtual {p0, v7, v8, v5}, LX/186;->a(III)V

    .line 893395
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 893396
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 893137
    if-nez p1, :cond_0

    .line 893138
    :goto_0
    return v0

    .line 893139
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v1

    const/4 v2, 0x0

    .line 893140
    if-nez v1, :cond_1

    .line 893141
    :goto_1
    move v1, v2

    .line 893142
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v2

    const/4 v3, 0x0

    .line 893143
    if-nez v2, :cond_2

    .line 893144
    :goto_2
    move v2, v3

    .line 893145
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893146
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 893147
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    const/4 v6, 0x0

    .line 893148
    if-nez v5, :cond_3

    .line 893149
    :goto_3
    move v5, v6

    .line 893150
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v6

    const/4 v7, 0x0

    .line 893151
    if-nez v6, :cond_4

    .line 893152
    :goto_4
    move v6, v7

    .line 893153
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 893154
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 893155
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893156
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 893157
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 893158
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 893159
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 893160
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 893161
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 893162
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893163
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893164
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893165
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p0, v4}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v4

    .line 893166
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 893167
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 893168
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 893169
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 893170
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 893171
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 893172
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p0, v5}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v5

    .line 893173
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 893174
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 893175
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 893176
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 893177
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 893178
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 893179
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 893180
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 893181
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 893182
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 893183
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 893184
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 893185
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 893186
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 893187
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 893188
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 893189
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 893190
    if-nez p1, :cond_0

    .line 893191
    :goto_0
    return v0

    .line 893192
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 893193
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1, v0}, LX/186;->a(III)V

    .line 893194
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893195
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 892501
    if-nez p0, :cond_1

    .line 892502
    :cond_0
    :goto_0
    return-object v2

    .line 892503
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 892504
    invoke-static {v0, p0}, LX/5I9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v1

    .line 892505
    if-eqz v1, :cond_0

    .line 892506
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 892507
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 892508
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 892509
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 892510
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 892511
    const-string v1, "SocialSearchConversionHelper.getCommentPlaceInfoPageFields"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 892512
    :cond_2
    new-instance v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-direct {v2, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommentFieldsForPlaceMutation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 892590
    if-nez p0, :cond_1

    .line 892591
    :cond_0
    :goto_0
    return-object v2

    .line 892592
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 892593
    invoke-static {v0, p0}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLComment;)I

    move-result v1

    .line 892594
    if-eqz v1, :cond_0

    .line 892595
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 892596
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 892597
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 892598
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 892599
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 892600
    const-string v1, "SocialSearchConversionHelper.getCommentFieldsForPlaceMutation"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 892601
    :cond_2
    new-instance v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    invoke-direct {v2, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 892602
    if-nez p0, :cond_1

    .line 892603
    :cond_0
    :goto_0
    return-object v2

    .line 892604
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 892605
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 892606
    if-nez p0, :cond_3

    .line 892607
    :goto_1
    move v1, v4

    .line 892608
    if-eqz v1, :cond_0

    .line 892609
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 892610
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 892611
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 892612
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 892613
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 892614
    const-string v1, "SocialSearchConversionHelper.getStoryFieldsForPlaceMutation"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 892615
    :cond_2
    new-instance v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    invoke-direct {v2, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 892616
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v5

    .line 892617
    if-eqz v5, :cond_5

    .line 892618
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 892619
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 892620
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v8, 0x0

    .line 892621
    if-nez v1, :cond_6

    .line 892622
    :goto_3
    move v1, v8

    .line 892623
    aput v1, v6, v3

    .line 892624
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 892625
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 892626
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 892627
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 892628
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 892629
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 892630
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 892631
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    .line 892632
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    const/4 v10, 0x0

    .line 892633
    if-nez v9, :cond_7

    .line 892634
    :goto_5
    move v9, v10

    .line 892635
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 892636
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 892637
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 892638
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_3

    .line 892639
    :cond_7
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    .line 892640
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 892641
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 892642
    invoke-virtual {v0, v10, v11}, LX/186;->b(II)V

    .line 892643
    const/4 v11, 0x1

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->lX()I

    move-result v1

    invoke-virtual {v0, v11, v1, v10}, LX/186;->a(III)V

    .line 892644
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v12}, LX/186;->b(II)V

    .line 892645
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    .line 892646
    invoke-virtual {v0, v10}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 4

    .prologue
    .line 892647
    if-nez p0, :cond_0

    .line 892648
    const/4 v0, 0x0

    .line 892649
    :goto_0
    return-object v0

    .line 892650
    :cond_0
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    .line 892651
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 892652
    iput-object v1, v0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 892653
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;

    move-result-object v1

    .line 892654
    if-nez v1, :cond_1

    .line 892655
    const/4 v2, 0x0

    .line 892656
    :goto_1
    move-object v1, v2

    .line 892657
    iput-object v1, v0, LX/3dL;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 892658
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->c()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;

    move-result-object v1

    .line 892659
    if-nez v1, :cond_2

    .line 892660
    const/4 v2, 0x0

    .line 892661
    :goto_2
    move-object v1, v2

    .line 892662
    iput-object v1, v0, LX/3dL;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 892663
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 892664
    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 892665
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 892666
    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 892667
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->aP_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/5I9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 892668
    iput-object v1, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 892669
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;->aO_()LX/176;

    move-result-object v1

    invoke-static {v1}, LX/5I9;->a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 892670
    iput-object v1, v0, LX/3dL;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 892671
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_0

    .line 892672
    :cond_1
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 892673
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 892674
    iput-object v3, v2, LX/173;->f:Ljava/lang/String;

    .line 892675
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_1

    .line 892676
    :cond_2
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    .line 892677
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 892678
    iput-object v3, v2, LX/4XY;->N:Ljava/lang/String;

    .line 892679
    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLComment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 892513
    if-nez p0, :cond_0

    .line 892514
    const/4 v0, 0x0

    .line 892515
    :goto_0
    return-object v0

    .line 892516
    :cond_0
    new-instance v2, LX/4Vu;

    invoke-direct {v2}, LX/4Vu;-><init>()V

    .line 892517
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 892518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 892519
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 892520
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    .line 892521
    if-nez v0, :cond_3

    .line 892522
    const/4 v4, 0x0

    .line 892523
    :goto_2
    move-object v0, v4

    .line 892524
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892525
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 892526
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 892527
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 892528
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 892529
    iput-object v0, v2, LX/4Vu;->q:Ljava/lang/String;

    .line 892530
    invoke-virtual {v2}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto :goto_0

    .line 892531
    :cond_3
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 892532
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v5

    const/4 v8, 0x0

    .line 892533
    if-nez v5, :cond_4

    .line 892534
    const/4 v6, 0x0

    .line 892535
    :goto_3
    move-object v5, v6

    .line 892536
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 892537
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto :goto_2

    .line 892538
    :cond_4
    new-instance v9, LX/4XR;

    invoke-direct {v9}, LX/4XR;-><init>()V

    .line 892539
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 892540
    iput-object v6, v9, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 892541
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->e()Z

    move-result v6

    .line 892542
    iput-boolean v6, v9, LX/4XR;->bc:Z

    .line 892543
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->bC_()Z

    move-result v6

    .line 892544
    iput-boolean v6, v9, LX/4XR;->bA:Z

    .line 892545
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 892546
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move v7, v8

    .line 892547
    :goto_4
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_5

    .line 892548
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892549
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_4

    .line 892550
    :cond_5
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892551
    iput-object v6, v9, LX/4XR;->cm:LX/0Px;

    .line 892552
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->bD_()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 892553
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move v7, v8

    .line 892554
    :goto_5
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->bD_()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_7

    .line 892555
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->bD_()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892556
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_5

    .line 892557
    :cond_7
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892558
    iput-object v6, v9, LX/4XR;->cn:LX/0Px;

    .line 892559
    :cond_8
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v6

    .line 892560
    iput-object v6, v9, LX/4XR;->fO:Ljava/lang/String;

    .line 892561
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 892562
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move v7, v8

    .line 892563
    :goto_6
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_9

    .line 892564
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->k()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892565
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_6

    .line 892566
    :cond_9
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892567
    iput-object v6, v9, LX/4XR;->hE:LX/0Px;

    .line 892568
    :cond_a
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 892569
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move v7, v8

    .line 892570
    :goto_7
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_b

    .line 892571
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->l()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;)Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892572
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_7

    .line 892573
    :cond_b
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892574
    iput-object v6, v9, LX/4XR;->jA:LX/0Px;

    .line 892575
    :cond_c
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 892576
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move v7, v8

    .line 892577
    :goto_8
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_d

    .line 892578
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892579
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_8

    .line 892580
    :cond_d
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892581
    iput-object v6, v9, LX/4XR;->jB:LX/0Px;

    .line 892582
    :cond_e
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 892583
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 892584
    :goto_9
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_f

    .line 892585
    invoke-virtual {v5}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-static {v6}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892586
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 892587
    :cond_f
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 892588
    iput-object v6, v9, LX/4XR;->jC:LX/0Px;

    .line 892589
    :cond_10
    invoke-virtual {v9}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    goto/16 :goto_3
.end method

.method private static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 892680
    if-nez p0, :cond_0

    .line 892681
    const/4 v0, 0x0

    .line 892682
    :goto_0
    return-object v0

    .line 892683
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 892684
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 892685
    iput v1, v0, LX/2dc;->c:I

    .line 892686
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 892687
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 892688
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 892689
    iput v1, v0, LX/2dc;->i:I

    .line 892690
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 5

    .prologue
    .line 892691
    if-nez p0, :cond_0

    .line 892692
    const/4 v0, 0x0

    .line 892693
    :goto_0
    return-object v0

    .line 892694
    :cond_0
    new-instance v0, LX/4X8;

    invoke-direct {v0}, LX/4X8;-><init>()V

    .line 892695
    invoke-interface {p0}, LX/1k1;->a()D

    move-result-wide v2

    .line 892696
    iput-wide v2, v0, LX/4X8;->b:D

    .line 892697
    invoke-interface {p0}, LX/1k1;->b()D

    move-result-wide v2

    .line 892698
    iput-wide v2, v0, LX/4X8;->c:D

    .line 892699
    invoke-virtual {v0}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 8

    .prologue
    .line 892700
    if-nez p0, :cond_0

    .line 892701
    const/4 v0, 0x0

    .line 892702
    :goto_0
    return-object v0

    .line 892703
    :cond_0
    new-instance v0, LX/4XY;

    invoke-direct {v0}, LX/4XY;-><init>()V

    .line 892704
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->x()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 892705
    if-nez v1, :cond_1

    .line 892706
    const/4 v3, 0x0

    .line 892707
    :goto_1
    move-object v1, v3

    .line 892708
    iput-object v1, v0, LX/4XY;->j:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 892709
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->u()Ljava/lang/String;

    move-result-object v1

    .line 892710
    iput-object v1, v0, LX/4XY;->F:Ljava/lang/String;

    .line 892711
    invoke-interface {p0}, LX/5CH;->c()LX/0Px;

    move-result-object v1

    .line 892712
    iput-object v1, v0, LX/4XY;->G:LX/0Px;

    .line 892713
    invoke-interface {p0}, LX/5CH;->d()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v1

    .line 892714
    if-nez v1, :cond_2

    .line 892715
    const/4 v2, 0x0

    .line 892716
    :goto_2
    move-object v1, v2

    .line 892717
    iput-object v1, v0, LX/4XY;->J:Lcom/facebook/graphql/model/GraphQLPage;

    .line 892718
    invoke-interface {p0}, LX/5CH;->e()Ljava/lang/String;

    move-result-object v1

    .line 892719
    iput-object v1, v0, LX/4XY;->ag:Ljava/lang/String;

    .line 892720
    invoke-interface {p0}, LX/5CH;->ae_()Z

    move-result v1

    .line 892721
    iput-boolean v1, v0, LX/4XY;->aw:Z

    .line 892722
    invoke-interface {p0}, LX/5CH;->af_()LX/1k1;

    move-result-object v1

    invoke-static {v1}, LX/5I9;->a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 892723
    iput-object v1, v0, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 892724
    invoke-interface {p0}, LX/5CH;->j()Ljava/lang/String;

    move-result-object v1

    .line 892725
    iput-object v1, v0, LX/4XY;->aT:Ljava/lang/String;

    .line 892726
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->r()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 892727
    if-nez v1, :cond_3

    .line 892728
    const/4 v3, 0x0

    .line 892729
    :goto_3
    move-object v1, v3

    .line 892730
    iput-object v1, v0, LX/4XY;->ba:Lcom/facebook/graphql/model/GraphQLRating;

    .line 892731
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->s()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 892732
    if-nez v1, :cond_4

    .line 892733
    const/4 v3, 0x0

    .line 892734
    :goto_4
    move-object v1, v3

    .line 892735
    iput-object v1, v0, LX/4XY;->bk:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 892736
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->v()Ljava/lang/String;

    move-result-object v1

    .line 892737
    iput-object v1, v0, LX/4XY;->br:Ljava/lang/String;

    .line 892738
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->w()Ljava/lang/String;

    move-result-object v1

    .line 892739
    iput-object v1, v0, LX/4XY;->bx:Ljava/lang/String;

    .line 892740
    invoke-interface {p0}, LX/5CH;->k()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/5I9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 892741
    iput-object v1, v0, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 892742
    invoke-interface {p0}, LX/5CH;->l()Z

    move-result v1

    .line 892743
    iput-boolean v1, v0, LX/4XY;->bR:Z

    .line 892744
    invoke-interface {p0}, LX/5CH;->m()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v1

    .line 892745
    if-nez v1, :cond_5

    .line 892746
    const/4 v2, 0x0

    .line 892747
    :goto_5
    move-object v1, v2

    .line 892748
    iput-object v1, v0, LX/4XY;->ce:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 892749
    invoke-interface {p0}, LX/5CH;->n()Z

    move-result v1

    .line 892750
    iput-boolean v1, v0, LX/4XY;->co:Z

    .line 892751
    invoke-interface {p0}, LX/5CH;->o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v1

    .line 892752
    iput-object v1, v0, LX/4XY;->cA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 892753
    invoke-interface {p0}, LX/5CH;->p()Ljava/lang/String;

    move-result-object v1

    .line 892754
    iput-object v1, v0, LX/4XY;->cR:Ljava/lang/String;

    .line 892755
    invoke-interface {p0}, LX/5CH;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 892756
    iput-object v1, v0, LX/4XY;->dj:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 892757
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->t()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 892758
    if-nez v1, :cond_a

    .line 892759
    const/4 v3, 0x0

    .line 892760
    :goto_6
    move-object v1, v3

    .line 892761
    iput-object v1, v0, LX/4XY;->dn:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 892762
    invoke-virtual {v0}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    goto/16 :goto_0

    .line 892763
    :cond_1
    new-instance v3, LX/4Z0;

    invoke-direct {v3}, LX/4Z0;-><init>()V

    .line 892764
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 892765
    iput-object v4, v3, LX/4Z0;->h:Ljava/lang/String;

    .line 892766
    invoke-virtual {v3}, LX/4Z0;->a()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v3

    goto/16 :goto_1

    .line 892767
    :cond_2
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    .line 892768
    invoke-virtual {v1}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 892769
    iput-object v3, v2, LX/4XY;->P:Ljava/lang/String;

    .line 892770
    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    goto/16 :goto_2

    .line 892771
    :cond_3
    new-instance v3, LX/4YW;

    invoke-direct {v3}, LX/4YW;-><init>()V

    .line 892772
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 892773
    iput v4, v3, LX/4YW;->c:I

    .line 892774
    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 892775
    iput-wide v5, v3, LX/4YW;->d:D

    .line 892776
    invoke-virtual {v3}, LX/4YW;->a()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v3

    goto/16 :goto_3

    .line 892777
    :cond_4
    new-instance v3, LX/4Xc;

    invoke-direct {v3}, LX/4Xc;-><init>()V

    .line 892778
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 892779
    iput v4, v3, LX/4Xc;->b:I

    .line 892780
    invoke-virtual {v3}, LX/4Xc;->a()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v3

    goto/16 :goto_4

    .line 892781
    :cond_5
    new-instance v2, LX/4Z9;

    invoke-direct {v2}, LX/4Z9;-><init>()V

    .line 892782
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->e()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;

    move-result-object v3

    .line 892783
    if-nez v3, :cond_6

    .line 892784
    const/4 v4, 0x0

    .line 892785
    :goto_7
    move-object v3, v4

    .line 892786
    iput-object v3, v2, LX/4Z9;->b:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 892787
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->A_()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;

    move-result-object v3

    .line 892788
    if-nez v3, :cond_7

    .line 892789
    const/4 v4, 0x0

    .line 892790
    :goto_8
    move-object v3, v4

    .line 892791
    iput-object v3, v2, LX/4Z9;->c:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 892792
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 892793
    iput-object v3, v2, LX/4Z9;->h:Ljava/lang/String;

    .line 892794
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 892795
    iput-object v3, v2, LX/4Z9;->j:Ljava/lang/String;

    .line 892796
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->d()LX/1oQ;

    move-result-object v3

    .line 892797
    if-nez v3, :cond_8

    .line 892798
    const/4 v4, 0x0

    .line 892799
    :goto_9
    move-object v3, v4

    .line 892800
    iput-object v3, v2, LX/4Z9;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 892801
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->z_()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$SavedDashboardSectionModel;

    move-result-object v3

    .line 892802
    if-nez v3, :cond_9

    .line 892803
    const/4 v4, 0x0

    .line 892804
    :goto_a
    move-object v3, v4

    .line 892805
    iput-object v3, v2, LX/4Z9;->n:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 892806
    invoke-virtual {v1}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 892807
    iput-object v3, v2, LX/4Z9;->r:Ljava/lang/String;

    .line 892808
    invoke-virtual {v2}, LX/4Z9;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    goto/16 :goto_5

    .line 892809
    :cond_6
    new-instance v4, LX/4ZB;

    invoke-direct {v4}, LX/4ZB;-><init>()V

    .line 892810
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 892811
    iput-object v5, v4, LX/4ZB;->b:Ljava/lang/String;

    .line 892812
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddItemActionInfoModel;->b()LX/176;

    move-result-object v5

    invoke-static {v5}, LX/5I9;->a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 892813
    iput-object v5, v4, LX/4ZB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 892814
    invoke-virtual {v4}, LX/4ZB;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v4

    goto :goto_7

    .line 892815
    :cond_7
    new-instance v4, LX/4ZB;

    invoke-direct {v4}, LX/4ZB;-><init>()V

    .line 892816
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 892817
    iput-object v5, v4, LX/4ZB;->b:Ljava/lang/String;

    .line 892818
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$AddedItemStateInfoModel;->b()LX/176;

    move-result-object v5

    invoke-static {v5}, LX/5I9;->a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 892819
    iput-object v5, v4, LX/4ZB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 892820
    invoke-virtual {v4}, LX/4ZB;->a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v4

    goto :goto_8

    .line 892821
    :cond_8
    new-instance v4, LX/4YH;

    invoke-direct {v4}, LX/4YH;-><init>()V

    .line 892822
    invoke-interface {v3}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v5

    .line 892823
    iput-object v5, v4, LX/4YH;->h:Ljava/lang/String;

    .line 892824
    invoke-interface {v3}, LX/1oQ;->m()Ljava/lang/String;

    move-result-object v5

    .line 892825
    iput-object v5, v4, LX/4YH;->l:Ljava/lang/String;

    .line 892826
    invoke-virtual {v4}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    goto :goto_9

    .line 892827
    :cond_9
    new-instance v4, LX/4Yi;

    invoke-direct {v4}, LX/4Yi;-><init>()V

    .line 892828
    invoke-virtual {v3}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel$SavedDashboardSectionModel;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v5

    .line 892829
    iput-object v5, v4, LX/4Yi;->b:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 892830
    invoke-virtual {v4}, LX/4Yi;->a()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v4

    goto :goto_a

    .line 892831
    :cond_a
    new-instance v3, LX/4ZU;

    invoke-direct {v3}, LX/4ZU;-><init>()V

    .line 892832
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v4

    .line 892833
    iput v4, v3, LX/4ZU;->b:I

    .line 892834
    invoke-virtual {v3}, LX/4ZU;->a()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v3

    goto/16 :goto_6
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;)Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;
    .locals 6

    .prologue
    .line 892835
    if-nez p0, :cond_0

    .line 892836
    const/4 v0, 0x0

    .line 892837
    :goto_0
    return-object v0

    .line 892838
    :cond_0
    new-instance v2, LX/4Xt;

    invoke-direct {v2}, LX/4Xt;-><init>()V

    .line 892839
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 892840
    iput-object v0, v2, LX/4Xt;->b:Ljava/lang/String;

    .line 892841
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 892842
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 892843
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 892844
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5DV;

    .line 892845
    if-nez v0, :cond_3

    .line 892846
    const/4 v4, 0x0

    .line 892847
    :goto_2
    move-object v0, v4

    .line 892848
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892849
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 892850
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 892851
    iput-object v0, v2, LX/4Xt;->c:LX/0Px;

    .line 892852
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 892853
    iput-object v0, v2, LX/4Xt;->d:Ljava/lang/String;

    .line 892854
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-direct {v0, v2}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;-><init>(LX/4Xt;)V

    .line 892855
    move-object v0, v0

    .line 892856
    goto :goto_0

    .line 892857
    :cond_3
    new-instance v4, LX/4XY;

    invoke-direct {v4}, LX/4XY;-><init>()V

    .line 892858
    invoke-interface {v0}, LX/5DV;->b()Ljava/lang/String;

    move-result-object v5

    .line 892859
    iput-object v5, v4, LX/4XY;->ag:Ljava/lang/String;

    .line 892860
    invoke-interface {v0}, LX/5DV;->c()LX/1k1;

    move-result-object v5

    invoke-static {v5}, LX/5I9;->a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v5

    .line 892861
    iput-object v5, v4, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 892862
    invoke-interface {v0}, LX/5DV;->d()Ljava/lang/String;

    move-result-object v5

    .line 892863
    iput-object v5, v4, LX/4XY;->aT:Ljava/lang/String;

    .line 892864
    invoke-virtual {v4}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;
    .locals 4

    .prologue
    .line 892865
    if-nez p0, :cond_0

    .line 892866
    const/4 v0, 0x0

    .line 892867
    :goto_0
    return-object v0

    .line 892868
    :cond_0
    new-instance v0, LX/4YC;

    invoke-direct {v0}, LX/4YC;-><init>()V

    .line 892869
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 892870
    iput-object v1, v0, LX/4YC;->b:Ljava/lang/String;

    .line 892871
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->c()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel$CreatorModel;

    move-result-object v1

    .line 892872
    if-nez v1, :cond_1

    .line 892873
    const/4 v2, 0x0

    .line 892874
    :goto_1
    move-object v1, v2

    .line 892875
    iput-object v1, v0, LX/4YC;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 892876
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 892877
    iput-object v1, v0, LX/4YC;->d:Ljava/lang/String;

    .line 892878
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 892879
    iput-object v1, v0, LX/4YC;->e:Ljava/lang/String;

    .line 892880
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->aM_()Ljava/lang/String;

    move-result-object v1

    .line 892881
    iput-object v1, v0, LX/4YC;->f:Ljava/lang/String;

    .line 892882
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;->aN_()Ljava/lang/String;

    move-result-object v1

    .line 892883
    iput-object v1, v0, LX/4YC;->h:Ljava/lang/String;

    .line 892884
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;-><init>(LX/4YC;)V

    .line 892885
    move-object v0, v1

    .line 892886
    goto :goto_0

    .line 892887
    :cond_1
    new-instance v2, LX/3dL;

    invoke-direct {v2}, LX/3dL;-><init>()V

    .line 892888
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel$CreatorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 892889
    iput-object v3, v2, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 892890
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel$CreatorModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 892891
    iput-object v3, v2, LX/3dL;->E:Ljava/lang/String;

    .line 892892
    invoke-virtual {v2}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    goto :goto_1
.end method

.method public static a(LX/176;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 892893
    if-nez p0, :cond_0

    .line 892894
    const/4 v0, 0x0

    .line 892895
    :goto_0
    return-object v0

    .line 892896
    :cond_0
    new-instance v3, LX/173;

    invoke-direct {v3}, LX/173;-><init>()V

    .line 892897
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 892898
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 892899
    :goto_1
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 892900
    invoke-interface {p0}, LX/176;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jz;

    .line 892901
    if-nez v0, :cond_5

    .line 892902
    const/4 v5, 0x0

    .line 892903
    :goto_2
    move-object v0, v5

    .line 892904
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892905
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 892906
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 892907
    iput-object v0, v3, LX/173;->b:LX/0Px;

    .line 892908
    :cond_2
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 892909
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 892910
    :goto_3
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 892911
    invoke-interface {p0}, LX/176;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 892912
    if-nez v0, :cond_6

    .line 892913
    const/4 v4, 0x0

    .line 892914
    :goto_4
    move-object v0, v4

    .line 892915
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 892916
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 892917
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 892918
    iput-object v0, v3, LX/173;->e:LX/0Px;

    .line 892919
    :cond_4
    invoke-interface {p0}, LX/176;->a()Ljava/lang/String;

    move-result-object v0

    .line 892920
    iput-object v0, v3, LX/173;->f:Ljava/lang/String;

    .line 892921
    invoke-virtual {v3}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 892922
    :cond_5
    new-instance v5, LX/4Vo;

    invoke-direct {v5}, LX/4Vo;-><init>()V

    .line 892923
    invoke-interface {v0}, LX/1jz;->a()I

    move-result v6

    .line 892924
    iput v6, v5, LX/4Vo;->b:I

    .line 892925
    invoke-interface {v0}, LX/1jz;->b()I

    move-result v6

    .line 892926
    iput v6, v5, LX/4Vo;->c:I

    .line 892927
    invoke-interface {v0}, LX/1jz;->c()I

    move-result v6

    .line 892928
    iput v6, v5, LX/4Vo;->d:I

    .line 892929
    invoke-virtual {v5}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v5

    goto :goto_2

    .line 892930
    :cond_6
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 892931
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v5

    .line 892932
    if-nez v5, :cond_7

    .line 892933
    const/4 v6, 0x0

    .line 892934
    :goto_5
    move-object v5, v6

    .line 892935
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 892936
    invoke-interface {v0}, LX/1W5;->b()I

    move-result v5

    .line 892937
    iput v5, v4, LX/4W6;->c:I

    .line 892938
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    .line 892939
    iput v5, v4, LX/4W6;->d:I

    .line 892940
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto :goto_4

    .line 892941
    :cond_7
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 892942
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 892943
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 892944
    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v7

    .line 892945
    iput-object v7, v6, LX/170;->b:LX/0Px;

    .line 892946
    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v7

    .line 892947
    iput-object v7, v6, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 892948
    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v7

    .line 892949
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 892950
    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v7

    .line 892951
    iput-object v7, v6, LX/170;->A:Ljava/lang/String;

    .line 892952
    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v7

    .line 892953
    iput-object v7, v6, LX/170;->X:Ljava/lang/String;

    .line 892954
    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    .line 892955
    iput-object v7, v6, LX/170;->Y:Ljava/lang/String;

    .line 892956
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_5
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 20

    .prologue
    .line 892957
    if-nez p1, :cond_0

    .line 892958
    const/4 v2, 0x0

    .line 892959
    :goto_0
    return v2

    .line 892960
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStreetAddress;)I

    move-result v2

    .line 892961
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 892962
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 892963
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v5

    .line 892964
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 892965
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLLocation;)I

    move-result v7

    .line 892966
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 892967
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->U()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRating;)I

    move-result v9

    .line 892968
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->W()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;)I

    move-result v10

    .line 892969
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aW()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 892970
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->ab()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 892971
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v13

    .line 892972
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->ao()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)I

    move-result v14

    .line 892973
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->ax()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 892974
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 892975
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aP()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 892976
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aQ()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;)I

    move-result v18

    .line 892977
    const/16 v19, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 892978
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 892979
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 892980
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 892981
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 892982
    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 892983
    const/4 v2, 0x6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->G()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 892984
    const/4 v2, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 892985
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 892986
    const/16 v2, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 892987
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 892988
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 892989
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 892990
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 892991
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aj()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 892992
    const/16 v2, 0xf

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 892993
    const/16 v2, 0x10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPage;->aq()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 892994
    const/16 v2, 0x11

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 892995
    const/16 v2, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 892996
    const/16 v2, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 892997
    const/16 v2, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 892998
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 892999
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 893000
    if-nez p0, :cond_1

    .line 893001
    :cond_0
    :goto_0
    return-object v2

    .line 893002
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 893003
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 893004
    if-nez p0, :cond_3

    .line 893005
    :goto_1
    move v1, v4

    .line 893006
    if-eqz v1, :cond_0

    .line 893007
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 893008
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 893009
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 893010
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 893011
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 893012
    const-string v1, "SocialSearchConversionHelper.getCommentFieldsLightweightRecMutation"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 893013
    :cond_2
    new-instance v2, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;

    invoke-direct {v2, v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 893014
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v5

    .line 893015
    if-eqz v5, :cond_5

    .line 893016
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 893017
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 893018
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v8, 0x0

    .line 893019
    if-nez v1, :cond_6

    .line 893020
    :goto_3
    move v1, v8

    .line 893021
    aput v1, v6, v3

    .line 893022
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 893023
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 893024
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893025
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 893026
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 893027
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 893028
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 893029
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    .line 893030
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    const/4 v10, 0x0

    .line 893031
    if-nez v9, :cond_7

    .line 893032
    :goto_5
    move v9, v10

    .line 893033
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 893034
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 893035
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 893036
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_3

    .line 893037
    :cond_7
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    .line 893038
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 893039
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 893040
    invoke-virtual {v0, v10, v11}, LX/186;->b(II)V

    .line 893041
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v12}, LX/186;->b(II)V

    .line 893042
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    .line 893043
    invoke-virtual {v0, v10}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static c(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 893044
    if-nez p1, :cond_0

    .line 893045
    :goto_0
    return v0

    .line 893046
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    const/4 p1, 0x1

    const/4 v4, 0x0

    .line 893047
    if-nez v1, :cond_1

    .line 893048
    :goto_1
    move v1, v4

    .line 893049
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 893050
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 893051
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 893052
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 893053
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 893054
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v5

    .line 893055
    if-eqz v5, :cond_d

    .line 893056
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    new-array v6, v2, [I

    move v3, v4

    .line 893057
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 893058
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-static {p0, v2}, LX/5I9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v2

    aput v2, v6, v3

    .line 893059
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 893060
    :cond_2
    invoke-virtual {p0, v6, p1}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 893061
    :goto_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v6

    .line 893062
    if-eqz v6, :cond_c

    .line 893063
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    move v5, v4

    .line 893064
    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_3

    .line 893065
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v2

    aput v2, v7, v5

    .line 893066
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 893067
    :cond_3
    invoke-virtual {p0, v7, p1}, LX/186;->a([IZ)I

    move-result v2

    move v5, v2

    .line 893068
    :goto_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 893069
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v7

    .line 893070
    if-eqz v7, :cond_b

    .line 893071
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    new-array v8, v2, [I

    move v6, v4

    .line 893072
    :goto_6
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    if-ge v6, v2, :cond_4

    .line 893073
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)I

    move-result v2

    aput v2, v8, v6

    .line 893074
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_6

    .line 893075
    :cond_4
    invoke-virtual {p0, v8, p1}, LX/186;->a([IZ)I

    move-result v2

    move v6, v2

    .line 893076
    :goto_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v8

    .line 893077
    if-eqz v8, :cond_a

    .line 893078
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    new-array v9, v2, [I

    move v7, v4

    .line 893079
    :goto_8
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v2

    if-ge v7, v2, :cond_5

    .line 893080
    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;)I

    move-result v2

    aput v2, v9, v7

    .line 893081
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_8

    .line 893082
    :cond_5
    invoke-virtual {p0, v9, p1}, LX/186;->a([IZ)I

    move-result v2

    move v7, v2

    .line 893083
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v9

    .line 893084
    if-eqz v9, :cond_9

    .line 893085
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    new-array v12, v2, [I

    move v8, v4

    .line 893086
    :goto_a
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-ge v8, v2, :cond_6

    .line 893087
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-static {p0, v2}, LX/5I9;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v2

    aput v2, v12, v8

    .line 893088
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_a

    .line 893089
    :cond_6
    invoke-virtual {p0, v12, p1}, LX/186;->a([IZ)I

    move-result v2

    move v8, v2

    .line 893090
    :goto_b
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v12

    .line 893091
    if-eqz v12, :cond_8

    .line 893092
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    new-array v13, v2, [I

    move v9, v4

    .line 893093
    :goto_c
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    if-ge v9, v2, :cond_7

    .line 893094
    invoke-virtual {v12, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p0, v2}, LX/5I9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v2

    aput v2, v13, v9

    .line 893095
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_c

    .line 893096
    :cond_7
    invoke-virtual {p0, v13, p1}, LX/186;->a([IZ)I

    move-result v2

    .line 893097
    :goto_d
    const/16 v9, 0xa

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 893098
    invoke-virtual {p0, v4, v10}, LX/186;->b(II)V

    .line 893099
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v4

    invoke-virtual {p0, p1, v4}, LX/186;->a(IZ)V

    .line 893100
    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v9

    invoke-virtual {p0, v4, v9}, LX/186;->a(IZ)V

    .line 893101
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v3}, LX/186;->b(II)V

    .line 893102
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 893103
    const/4 v3, 0x5

    invoke-virtual {p0, v3, v11}, LX/186;->b(II)V

    .line 893104
    const/4 v3, 0x6

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 893105
    const/4 v3, 0x7

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 893106
    const/16 v3, 0x8

    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 893107
    const/16 v3, 0x9

    invoke-virtual {p0, v3, v2}, LX/186;->b(II)V

    .line 893108
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 893109
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_8
    move v2, v4

    goto :goto_d

    :cond_9
    move v8, v4

    goto :goto_b

    :cond_a
    move v7, v4

    goto/16 :goto_9

    :cond_b
    move v6, v4

    goto/16 :goto_7

    :cond_c
    move v5, v4

    goto/16 :goto_5

    :cond_d
    move v3, v4

    goto/16 :goto_3
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommentFieldsProfileRecommendationMutation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 893110
    if-nez p0, :cond_1

    .line 893111
    :cond_0
    :goto_0
    return-object v2

    .line 893112
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 893113
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 893114
    if-nez p0, :cond_3

    .line 893115
    :goto_1
    move v1, v4

    .line 893116
    if-eqz v1, :cond_0

    .line 893117
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 893118
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 893119
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 893120
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 893121
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 893122
    const-string v1, "SocialSearchConversionHelper.getCommentFieldsProfileRecommendationMutation"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 893123
    :cond_2
    new-instance v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;

    invoke-direct {v2, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 893124
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v5

    .line 893125
    if-eqz v5, :cond_5

    .line 893126
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 893127
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 893128
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1}, LX/5I9;->c(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v1

    aput v1, v6, v3

    .line 893129
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 893130
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 893131
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 893132
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 893133
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 893134
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 893135
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 893136
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_3
.end method
