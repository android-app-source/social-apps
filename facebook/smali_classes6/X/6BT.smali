.class public final LX/6BT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:I

.field public d:LX/6BU;

.field public e:LX/6BV;

.field public f:Ljava/lang/String;

.field public g:Ljava/io/File;

.field public h:Ljava/lang/String;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1062680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062681
    sget-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/6BT;->b:Landroid/net/Uri;

    .line 1062682
    const/4 v0, 0x0

    iput v0, p0, LX/6BT;->c:I

    .line 1062683
    sget-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->b:LX/6BU;

    iput-object v0, p0, LX/6BT;->d:LX/6BU;

    .line 1062684
    sget-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->c:LX/6BV;

    iput-object v0, p0, LX/6BT;->e:LX/6BV;

    .line 1062685
    const-string v0, "default_asset_download"

    iput-object v0, p0, LX/6BT;->f:Ljava/lang/String;

    .line 1062686
    sget-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->d:Ljava/io/File;

    iput-object v0, p0, LX/6BT;->g:Ljava/io/File;

    .line 1062687
    sget-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e:Ljava/lang/String;

    iput-object v0, p0, LX/6BT;->h:Ljava/lang/String;

    .line 1062688
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6BT;->i:Ljava/util/Map;

    .line 1062689
    iput-object p1, p0, LX/6BT;->a:Ljava/lang/String;

    .line 1062690
    return-void
.end method
