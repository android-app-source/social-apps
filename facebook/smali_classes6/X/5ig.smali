.class public final LX/5ig;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:D

.field public c:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 984105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v13, 0x0

    const-wide/16 v4, 0x0

    const/4 v12, 0x0

    .line 984106
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 984107
    iget-object v2, p0, LX/5ig;->a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 984108
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 984109
    invoke-virtual {v0, v13, v2}, LX/186;->b(II)V

    .line 984110
    iget-wide v2, p0, LX/5ig;->b:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 984111
    const/4 v7, 0x2

    iget-wide v8, p0, LX/5ig;->c:D

    move-object v6, v0

    move-wide v10, v4

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 984112
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 984113
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 984114
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 984115
    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 984116
    new-instance v2, LX/15i;

    move-object v4, v12

    move-object v5, v12

    move v6, v1

    move-object v7, v12

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 984117
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;

    invoke-direct {v0, v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;-><init>(LX/15i;)V

    .line 984118
    return-object v0
.end method
