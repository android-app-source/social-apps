.class public LX/637;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/636;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/graphics/Paint;

.field private final c:LX/635;


# direct methods
.method public constructor <init>(Landroid/view/View;IILX/635;)V
    .locals 2

    .prologue
    .line 1042786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1042787
    iput-object p1, p0, LX/637;->a:Landroid/view/View;

    .line 1042788
    iput-object p4, p0, LX/637;->c:LX/635;

    .line 1042789
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/637;->b:Landroid/graphics/Paint;

    .line 1042790
    iget-object v0, p0, LX/637;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1042791
    iget-object v0, p0, LX/637;->b:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1042792
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1042793
    sget-object v1, LX/634;->a:[I

    iget-object v2, p0, LX/637;->c:LX/635;

    invoke-virtual {v2}, LX/635;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move v3, v0

    move v2, v0

    move v1, v0

    .line 1042794
    :goto_0
    int-to-float v1, v1

    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/637;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1042795
    return-void

    .line 1042796
    :pswitch_0
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    move v3, v1

    move v2, v0

    move v1, v0

    .line 1042797
    goto :goto_0

    .line 1042798
    :pswitch_1
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    move v3, v0

    move v2, v0

    move v6, v1

    move v1, v0

    move v0, v6

    .line 1042799
    goto :goto_0

    .line 1042800
    :pswitch_2
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1042801
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v2

    move v2, v1

    move v6, v1

    move v1, v0

    move v0, v6

    .line 1042802
    goto :goto_0

    .line 1042803
    :pswitch_3
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 1042804
    iget-object v1, p0, LX/637;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    move v3, v2

    move v6, v1

    move v1, v2

    move v2, v0

    move v0, v6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1042805
    const/4 v0, 0x0

    return v0
.end method
