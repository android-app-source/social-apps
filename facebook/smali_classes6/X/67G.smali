.class public final LX/67G;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/67F;

.field public static b:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1052642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052643
    return-void
.end method

.method public static a()LX/67F;
    .locals 6

    .prologue
    .line 1052644
    const-class v1, LX/67G;

    monitor-enter v1

    .line 1052645
    :try_start_0
    sget-object v0, LX/67G;->a:LX/67F;

    if-eqz v0, :cond_0

    .line 1052646
    sget-object v0, LX/67G;->a:LX/67F;

    .line 1052647
    iget-object v2, v0, LX/67F;->f:LX/67F;

    sput-object v2, LX/67G;->a:LX/67F;

    .line 1052648
    const/4 v2, 0x0

    iput-object v2, v0, LX/67F;->f:LX/67F;

    .line 1052649
    sget-wide v2, LX/67G;->b:J

    const-wide/16 v4, 0x2000

    sub-long/2addr v2, v4

    sput-wide v2, LX/67G;->b:J

    .line 1052650
    monitor-exit v1

    .line 1052651
    :goto_0
    return-object v0

    .line 1052652
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1052653
    new-instance v0, LX/67F;

    invoke-direct {v0}, LX/67F;-><init>()V

    goto :goto_0

    .line 1052654
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/67F;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x2000

    .line 1052655
    iget-object v0, p0, LX/67F;->f:LX/67F;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/67F;->g:LX/67F;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1052656
    :cond_1
    iget-boolean v0, p0, LX/67F;->d:Z

    if-eqz v0, :cond_2

    .line 1052657
    :goto_0
    return-void

    .line 1052658
    :cond_2
    const-class v1, LX/67G;

    monitor-enter v1

    .line 1052659
    :try_start_0
    sget-wide v2, LX/67G;->b:J

    add-long/2addr v2, v6

    const-wide/32 v4, 0x10000

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    monitor-exit v1

    goto :goto_0

    .line 1052660
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1052661
    :cond_3
    :try_start_1
    sget-wide v2, LX/67G;->b:J

    add-long/2addr v2, v6

    sput-wide v2, LX/67G;->b:J

    .line 1052662
    sget-object v0, LX/67G;->a:LX/67F;

    iput-object v0, p0, LX/67F;->f:LX/67F;

    .line 1052663
    const/4 v0, 0x0

    iput v0, p0, LX/67F;->c:I

    iput v0, p0, LX/67F;->b:I

    .line 1052664
    sput-object p0, LX/67G;->a:LX/67F;

    .line 1052665
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
