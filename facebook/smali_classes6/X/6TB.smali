.class public final LX/6TB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1097485
    const/16 v17, 0x0

    .line 1097486
    const/16 v16, 0x0

    .line 1097487
    const/4 v15, 0x0

    .line 1097488
    const/4 v14, 0x0

    .line 1097489
    const/4 v13, 0x0

    .line 1097490
    const/4 v12, 0x0

    .line 1097491
    const/4 v11, 0x0

    .line 1097492
    const/4 v10, 0x0

    .line 1097493
    const/4 v9, 0x0

    .line 1097494
    const/4 v8, 0x0

    .line 1097495
    const/4 v7, 0x0

    .line 1097496
    const/4 v6, 0x0

    .line 1097497
    const/4 v5, 0x0

    .line 1097498
    const/4 v4, 0x0

    .line 1097499
    const/4 v3, 0x0

    .line 1097500
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1097501
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1097502
    const/4 v3, 0x0

    .line 1097503
    :goto_0
    return v3

    .line 1097504
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1097505
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_c

    .line 1097506
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1097507
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1097508
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1097509
    const-string v19, "author"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1097510
    invoke-static/range {p0 .. p1}, LX/6T0;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1097511
    :cond_2
    const-string v19, "body"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1097512
    invoke-static/range {p0 .. p1}, LX/4ar;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1097513
    :cond_3
    const-string v19, "feedback"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1097514
    invoke-static/range {p0 .. p1}, LX/6Sg;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1097515
    :cond_4
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1097516
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 1097517
    :cond_5
    const-string v19, "is_featured"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1097518
    const/4 v6, 0x1

    .line 1097519
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 1097520
    :cond_6
    const-string v19, "live_streaming_comment_priority"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1097521
    const/4 v5, 0x1

    .line 1097522
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1097523
    :cond_7
    const-string v19, "notable_likers"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1097524
    invoke-static/range {p0 .. p1}, LX/6Si;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1097525
    :cond_8
    const-string v19, "timestamp_in_video"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1097526
    const/4 v4, 0x1

    .line 1097527
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1097528
    :cond_9
    const-string v19, "translatability_for_viewer"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1097529
    invoke-static/range {p0 .. p1}, LX/6Sj;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1097530
    :cond_a
    const-string v19, "translated_body_for_viewer"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1097531
    invoke-static/range {p0 .. p1}, LX/6Sk;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1097532
    :cond_b
    const-string v19, "written_while_video_was_live"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1097533
    const/4 v3, 0x1

    .line 1097534
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1097535
    :cond_c
    const/16 v18, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1097536
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1097537
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1097538
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1097539
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1097540
    if-eqz v6, :cond_d

    .line 1097541
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 1097542
    :cond_d
    if-eqz v5, :cond_e

    .line 1097543
    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v6}, LX/186;->a(III)V

    .line 1097544
    :cond_e
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 1097545
    if-eqz v4, :cond_f

    .line 1097546
    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1097547
    :cond_f
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1097548
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1097549
    if-eqz v3, :cond_10

    .line 1097550
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 1097551
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1097552
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1097553
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1097554
    if-eqz v0, :cond_0

    .line 1097555
    const-string v1, "author"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097556
    invoke-static {p0, v0, p2, p3}, LX/6T0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097557
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097558
    if-eqz v0, :cond_1

    .line 1097559
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097560
    invoke-static {p0, v0, p2, p3}, LX/4ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097561
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097562
    if-eqz v0, :cond_2

    .line 1097563
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097564
    invoke-static {p0, v0, p2}, LX/6Sg;->a(LX/15i;ILX/0nX;)V

    .line 1097565
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1097566
    if-eqz v0, :cond_3

    .line 1097567
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097568
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1097569
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1097570
    if-eqz v0, :cond_4

    .line 1097571
    const-string v1, "is_featured"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097572
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1097573
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1097574
    if-eqz v0, :cond_5

    .line 1097575
    const-string v1, "live_streaming_comment_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097576
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1097577
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097578
    if-eqz v0, :cond_6

    .line 1097579
    const-string v1, "notable_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097580
    invoke-static {p0, v0, p2, p3}, LX/6Si;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097581
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1097582
    if-eqz v0, :cond_7

    .line 1097583
    const-string v1, "timestamp_in_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097584
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1097585
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097586
    if-eqz v0, :cond_8

    .line 1097587
    const-string v1, "translatability_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097588
    invoke-static {p0, v0, p2, p3}, LX/6Sj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097589
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097590
    if-eqz v0, :cond_9

    .line 1097591
    const-string v1, "translated_body_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097592
    invoke-static {p0, v0, p2}, LX/6Sk;->a(LX/15i;ILX/0nX;)V

    .line 1097593
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1097594
    if-eqz v0, :cond_a

    .line 1097595
    const-string v1, "written_while_video_was_live"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097596
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1097597
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1097598
    return-void
.end method
