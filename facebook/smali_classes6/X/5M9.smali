.class public final LX/5M9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

.field public J:Ljava/lang/String;

.field public K:Z

.field public L:Lcom/facebook/ipc/composer/model/ComposerLocation;

.field public M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field public N:J

.field public O:Lcom/facebook/greetingcards/model/GreetingCard;

.field public P:Z

.field public Q:Z

.field public R:Ljava/lang/String;

.field public S:Z

.field public T:Ljava/lang/String;

.field public U:Ljava/lang/String;

.field public V:Z

.field public W:Z

.field public X:Ljava/lang/String;

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public aa:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ab:Ljava/lang/String;

.field public ac:Z

.field public ad:Z

.field public ae:Lcom/facebook/composer/publish/common/PollUploadParams;

.field public af:Ljava/lang/String;

.field public ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Z

.field public ai:Z

.field public aj:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

.field public ak:Z

.field public al:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/publish/common/MediaAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public am:Ljava/lang/String;

.field public an:Ljava/lang/String;

.field public ao:Z

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public j:Ljava/lang/String;

.field public k:J

.field public l:Lcom/facebook/graphql/model/GraphQLEntity;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:J

.field public p:LX/5Rn;

.field public q:LX/2rt;

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:Z

.field public u:J

.field public v:Z

.field public w:Z

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 905003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905004
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 905005
    iput-object v0, p0, LX/5M9;->e:LX/0Px;

    .line 905006
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/5M9;->p:LX/5Rn;

    .line 905007
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 2

    .prologue
    .line 904933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904934
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 904935
    iput-object v0, p0, LX/5M9;->e:LX/0Px;

    .line 904936
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/5M9;->p:LX/5Rn;

    .line 904937
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    iput-wide v0, p0, LX/5M9;->b:J

    .line 904938
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->c:Ljava/lang/String;

    .line 904939
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->d:Ljava/lang/String;

    .line 904940
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    iput-object v0, p0, LX/5M9;->e:LX/0Px;

    .line 904941
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->g:Ljava/lang/String;

    .line 904942
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->h:Ljava/lang/String;

    .line 904943
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, LX/5M9;->i:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 904944
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->j:Ljava/lang/String;

    .line 904945
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    iput-wide v0, p0, LX/5M9;->k:J

    .line 904946
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 904947
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->m:Ljava/lang/String;

    .line 904948
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->n:Ljava/lang/String;

    .line 904949
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    iput-object v0, p0, LX/5M9;->q:LX/2rt;

    .line 904950
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    iput-wide v0, p0, LX/5M9;->o:J

    .line 904951
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    iput-object v0, p0, LX/5M9;->p:LX/5Rn;

    .line 904952
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    iput-boolean v0, p0, LX/5M9;->w:Z

    .line 904953
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->x:Ljava/lang/String;

    .line 904954
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->y:Ljava/lang/String;

    .line 904955
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->z:Ljava/lang/String;

    .line 904956
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->A:Ljava/lang/String;

    .line 904957
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->B:Ljava/lang/String;

    .line 904958
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->C:Ljava/lang/String;

    .line 904959
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->D:Ljava/lang/String;

    .line 904960
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->E:Ljava/lang/String;

    .line 904961
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->F:Ljava/lang/String;

    .line 904962
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    iput-boolean v0, p0, LX/5M9;->r:Z

    .line 904963
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->G:Ljava/lang/String;

    .line 904964
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->s:Ljava/lang/String;

    .line 904965
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    iput-boolean v0, p0, LX/5M9;->t:Z

    .line 904966
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    iput-wide v0, p0, LX/5M9;->u:J

    .line 904967
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, LX/5M9;->H:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 904968
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    iput-boolean v0, p0, LX/5M9;->K:Z

    .line 904969
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, LX/5M9;->L:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 904970
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    iput-object v0, p0, LX/5M9;->I:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 904971
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, LX/5M9;->M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 904972
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    iput-wide v0, p0, LX/5M9;->N:J

    .line 904973
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iput-object v0, p0, LX/5M9;->O:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 904974
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    iput-boolean v0, p0, LX/5M9;->P:Z

    .line 904975
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    iput-boolean v0, p0, LX/5M9;->Q:Z

    .line 904976
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->R:Ljava/lang/String;

    .line 904977
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    iput-boolean v0, p0, LX/5M9;->S:Z

    .line 904978
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->T:Ljava/lang/String;

    .line 904979
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->U:Ljava/lang/String;

    .line 904980
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->a:Ljava/lang/String;

    .line 904981
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->J:Ljava/lang/String;

    .line 904982
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    iput-boolean v0, p0, LX/5M9;->v:Z

    .line 904983
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    iput-boolean v0, p0, LX/5M9;->W:Z

    .line 904984
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    iput-boolean v0, p0, LX/5M9;->V:Z

    .line 904985
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->X:Ljava/lang/String;

    .line 904986
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    iput-object v0, p0, LX/5M9;->Z:LX/0Px;

    .line 904987
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    iput-object v0, p0, LX/5M9;->aa:LX/0Px;

    .line 904988
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    iput-object v0, p0, LX/5M9;->Y:LX/0Px;

    .line 904989
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->ab:Ljava/lang/String;

    .line 904990
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    iput-boolean v0, p0, LX/5M9;->ac:Z

    .line 904991
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    iput-boolean v0, p0, LX/5M9;->ad:Z

    .line 904992
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    iput-object v0, p0, LX/5M9;->ae:Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 904993
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->af:Ljava/lang/String;

    .line 904994
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, LX/5M9;->ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 904995
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    iput-boolean v0, p0, LX/5M9;->ah:Z

    .line 904996
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    iput-boolean v0, p0, LX/5M9;->ai:Z

    .line 904997
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, LX/5M9;->aj:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 904998
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    iput-boolean v0, p0, LX/5M9;->ak:Z

    .line 904999
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->am:Ljava/lang/String;

    .line 905000
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    iput-object v0, p0, LX/5M9;->an:Ljava/lang/String;

    .line 905001
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    iput-boolean v0, p0, LX/5M9;->ao:Z

    .line 905002
    return-void
.end method


# virtual methods
.method public final a(J)LX/5M9;
    .locals 1

    .prologue
    .line 904931
    iput-wide p1, p0, LX/5M9;->b:J

    .line 904932
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)LX/5M9;
    .locals 2

    .prologue
    .line 904929
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/5M9;->o:J

    .line 904930
    return-object p0
.end method

.method public final a()Lcom/facebook/composer/publish/common/PublishPostParams;
    .locals 2

    .prologue
    .line 904920
    iget-object v0, p0, LX/5M9;->q:LX/2rt;

    if-nez v0, :cond_0

    .line 904921
    iget-object v0, p0, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_1

    .line 904922
    sget-object v0, LX/2rt;->SHARE:LX/2rt;

    .line 904923
    :goto_0
    move-object v0, v0

    .line 904924
    iput-object v0, p0, LX/5M9;->q:LX/2rt;

    .line 904925
    :cond_0
    new-instance v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/PublishPostParams;-><init>(LX/5M9;)V

    return-object v0

    .line 904926
    :cond_1
    iget-object v0, p0, LX/5M9;->M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-eqz v0, :cond_2

    .line 904927
    sget-object v0, LX/2rt;->SELL:LX/2rt;

    goto :goto_0

    .line 904928
    :cond_2
    sget-object v0, LX/2rt;->STATUS:LX/2rt;

    goto :goto_0
.end method

.method public final c(J)LX/5M9;
    .locals 1

    .prologue
    .line 904918
    iput-wide p1, p0, LX/5M9;->k:J

    .line 904919
    return-object p0
.end method

.method public final c(Z)LX/5M9;
    .locals 0

    .prologue
    .line 904916
    iput-boolean p1, p0, LX/5M9;->r:Z

    .line 904917
    return-object p0
.end method

.method public final d(J)LX/5M9;
    .locals 1

    .prologue
    .line 904914
    iput-wide p1, p0, LX/5M9;->u:J

    .line 904915
    return-object p0
.end method

.method public final d(LX/0Px;)LX/5M9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/5M9;"
        }
    .end annotation

    .prologue
    .line 904908
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/5M9;->e:LX/0Px;

    .line 904909
    return-object p0
.end method

.method public final e(LX/0Px;)LX/5M9;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/publish/common/MediaAttachment;",
            ">;)",
            "LX/5M9;"
        }
    .end annotation

    .prologue
    .line 904912
    iput-object p1, p0, LX/5M9;->al:LX/0Px;

    .line 904913
    return-object p0
.end method

.method public final r(Ljava/lang/String;)LX/5M9;
    .locals 0

    .prologue
    .line 904910
    iput-object p1, p0, LX/5M9;->G:Ljava/lang/String;

    .line 904911
    return-object p0
.end method
