.class public final LX/66u;
.super LX/66m;
.source ""


# instance fields
.field private final c:LX/65W;

.field private final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>(LX/65W;Ljava/util/Random;Ljava/util/concurrent/ExecutorService;LX/K09;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1051272
    const/4 v1, 0x1

    invoke-virtual {p1}, LX/65W;->b()LX/65S;

    move-result-object v0

    iget-object v2, v0, LX/65S;->d:LX/671;

    .line 1051273
    invoke-virtual {p1}, LX/65W;->b()LX/65S;

    move-result-object v0

    iget-object v3, v0, LX/65S;->e:LX/670;

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 1051274
    invoke-direct/range {v0 .. v7}, LX/66m;-><init>(ZLX/671;LX/670;Ljava/util/Random;Ljava/util/concurrent/Executor;LX/K09;Ljava/lang/String;)V

    .line 1051275
    iput-object p1, p0, LX/66u;->c:LX/65W;

    .line 1051276
    iput-object p3, p0, LX/66u;->d:Ljava/util/concurrent/ExecutorService;

    .line 1051277
    return-void
.end method

.method public static a(LX/65W;LX/655;Ljava/util/Random;LX/K09;)LX/66m;
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 1051278
    iget-object v0, p1, LX/655;->a:LX/650;

    move-object v0, v0

    .line 1051279
    iget-object v1, v0, LX/650;->a:LX/64q;

    move-object v0, v1

    .line 1051280
    invoke-virtual {v0}, LX/64q;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1051281
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    const-string v3, "OkHttp %s WebSocket"

    new-array v8, v2, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    .line 1051282
    invoke-static {v3, v8}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, LX/65A;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    move v3, v2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 1051283
    invoke-virtual {v1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 1051284
    new-instance v2, LX/66u;

    move-object v3, p0

    move-object v4, p2

    move-object v5, v1

    move-object v6, p3

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, LX/66u;-><init>(LX/65W;Ljava/util/Random;Ljava/util/concurrent/ExecutorService;LX/K09;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 1051285
    iget-object v0, p0, LX/66u;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 1051286
    iget-object v0, p0, LX/66u;->c:LX/65W;

    invoke-virtual {v0}, LX/65W;->d()V

    .line 1051287
    iget-object v0, p0, LX/66u;->c:LX/65W;

    const/4 v1, 0x1

    iget-object v2, p0, LX/66u;->c:LX/65W;

    invoke-virtual {v2}, LX/65W;->a()LX/66G;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/65W;->a(ZLX/66G;)V

    .line 1051288
    return-void
.end method
