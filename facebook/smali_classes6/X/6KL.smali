.class public LX/6KL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zr;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:Landroid/content/res/Resources;

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/content/Context;

.field private final f:LX/6KT;

.field private final g:LX/6KB;


# direct methods
.method public constructor <init>(LX/0Zr;Ljava/util/concurrent/ExecutorService;Landroid/content/res/Resources;Landroid/os/Handler;Landroid/content/Context;LX/6KT;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1076564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076565
    iput-object p1, p0, LX/6KL;->a:LX/0Zr;

    .line 1076566
    iput-object p2, p0, LX/6KL;->b:Ljava/util/concurrent/ExecutorService;

    .line 1076567
    iput-object p3, p0, LX/6KL;->c:Landroid/content/res/Resources;

    .line 1076568
    iput-object p4, p0, LX/6KL;->d:Landroid/os/Handler;

    .line 1076569
    iput-object p5, p0, LX/6KL;->e:Landroid/content/Context;

    .line 1076570
    iput-object p6, p0, LX/6KL;->f:LX/6KT;

    .line 1076571
    iget-object v0, p0, LX/6KL;->f:LX/6KT;

    invoke-static {v0}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v0

    invoke-virtual {v0}, LX/6KD;->a()LX/6KB;

    move-result-object v0

    iput-object v0, p0, LX/6KL;->g:LX/6KB;

    .line 1076572
    return-void
.end method
