.class public final LX/5tP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1015946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1015947
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1015948
    :goto_0
    return v1

    .line 1015949
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 1015950
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1015951
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1015952
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1015953
    const-string v7, "has_photo_menus"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1015954
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 1015955
    :cond_1
    const-string v7, "has_structured_menu"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1015956
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1015957
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1015958
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1015959
    if-eqz v3, :cond_4

    .line 1015960
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1015961
    :cond_4
    if-eqz v0, :cond_5

    .line 1015962
    invoke-virtual {p1, v2, v4}, LX/186;->a(IZ)V

    .line 1015963
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1015964
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1015965
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1015966
    if-eqz v0, :cond_0

    .line 1015967
    const-string v1, "has_photo_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015968
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1015969
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1015970
    if-eqz v0, :cond_1

    .line 1015971
    const-string v1, "has_structured_menu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015972
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1015973
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1015974
    return-void
.end method
