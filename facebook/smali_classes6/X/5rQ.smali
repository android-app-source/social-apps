.class public LX/5rQ;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;
.implements LX/5pV;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKUIManager"
.end annotation


# instance fields
.field public final a:LX/5s9;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/5rN;

.field private final d:LX/5rP;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(LX/5pY;Ljava/util/List;LX/344;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;",
            "LX/344;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1011710
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1011711
    new-instance v0, LX/5rP;

    invoke-direct {v0, p0}, LX/5rP;-><init>(LX/5rQ;)V

    iput-object v0, p0, LX/5rQ;->d:LX/5rP;

    .line 1011712
    const/4 v0, 0x1

    iput v0, p0, LX/5rQ;->e:I

    .line 1011713
    iput v1, p0, LX/5rQ;->f:I

    .line 1011714
    invoke-static {p1}, LX/5ql;->a(Landroid/content/Context;)V

    .line 1011715
    new-instance v0, LX/5s9;

    invoke-direct {v0, p1}, LX/5s9;-><init>(LX/5pY;)V

    iput-object v0, p0, LX/5rQ;->a:LX/5s9;

    .line 1011716
    invoke-static {p2}, LX/5rQ;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/5rQ;->b:Ljava/util/Map;

    .line 1011717
    iget-object v0, p0, LX/5rQ;->a:LX/5s9;

    invoke-virtual {p3, p1, p2, v0}, LX/344;->a(LX/5pY;Ljava/util/List;LX/5s9;)LX/5rN;

    move-result-object v0

    iput-object v0, p0, LX/5rQ;->c:LX/5rN;

    .line 1011718
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 1011719
    return-void
.end method

.method public static synthetic a(LX/5rQ;)LX/5pY;
    .locals 1

    .prologue
    .line 1011720
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1011721
    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x2000

    .line 1011722
    const-string v0, "CREATE_UI_MANAGER_MODULE_CONSTANTS_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1011723
    const-string v0, "CreateUIManagerConstants"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1011724
    :try_start_0
    invoke-static {p0}, LX/5rS;->a(Ljava/util/List;)Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1011725
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 1011726
    const-string v1, "CREATE_UI_MANAGER_MODULE_CONSTANTS_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    return-object v0

    .line 1011727
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 1011728
    const-string v1, "CREATE_UI_MANAGER_MODULE_CONSTANTS_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1011729
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1}, LX/5rN;->e(I)I

    move-result v0

    return v0
.end method

.method public final a(LX/5rH;)I
    .locals 6

    .prologue
    .line 1011730
    iget v2, p0, LX/5rQ;->e:I

    .line 1011731
    iget v0, p0, LX/5rQ;->e:I

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, LX/5rQ;->e:I

    .line 1011732
    invoke-virtual {p1}, LX/5rH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/5rH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, LX/5rH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v0, :cond_0

    .line 1011733
    invoke-virtual {p1}, LX/5rH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1011734
    invoke-virtual {p1}, LX/5rH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1011735
    :goto_0
    new-instance v5, LX/5rJ;

    .line 1011736
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1011737
    invoke-virtual {p1}, LX/5rH;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v0, v1}, LX/5rJ;-><init>(LX/5pY;Landroid/content/Context;)V

    .line 1011738
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/5rN;->a(LX/5rH;IIILX/5rJ;)V

    .line 1011739
    new-instance v0, LX/5rO;

    invoke-direct {v0, p0, v2}, LX/5rO;-><init>(LX/5rQ;I)V

    .line 1011740
    iput-object v0, p1, LX/5rH;->a:LX/5rG;

    .line 1011741
    return v2

    .line 1011742
    :cond_0
    invoke-virtual {p1}, LX/5rH;->getWidth()I

    move-result v3

    .line 1011743
    invoke-virtual {p1}, LX/5rH;->getHeight()I

    move-result v4

    goto :goto_0
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 1011744
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1011745
    invoke-virtual {v0}, LX/5pX;->g()V

    .line 1011746
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3}, LX/5rN;->a(III)V

    .line 1011747
    return-void
.end method

.method public final a(LX/5qU;)V
    .locals 1
    .param p1    # LX/5qU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1011748
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1}, LX/5rN;->a(LX/5qU;)V

    .line 1011749
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1011750
    iget-object v0, p0, LX/5rQ;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final bM_()V
    .locals 1

    .prologue
    .line 1011765
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0}, LX/5rN;->d()V

    .line 1011766
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 1011751
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0}, LX/5rN;->e()V

    .line 1011752
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 1011753
    return-void
.end method

.method public clearJSResponder()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011754
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0}, LX/5rN;->c()V

    .line 1011755
    return-void
.end method

.method public configureNextLayoutAnimation(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011756
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3}, LX/5rN;->a(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 1011757
    return-void
.end method

.method public createView(ILjava/lang/String;ILX/5pG;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011758
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/5rN;->a(ILjava/lang/String;ILX/5pG;)V

    .line 1011759
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1011760
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1011761
    iget-object v1, p0, LX/5rQ;->d:LX/5rP;

    invoke-virtual {v0, v1}, LX/5pY;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 1011762
    return-void
.end method

.method public dispatchViewManagerCommand(IILX/5pC;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011763
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3}, LX/5rN;->a(IILX/5pC;)V

    .line 1011764
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1011701
    invoke-super {p0}, LX/5pb;->f()V

    .line 1011702
    iget-object v0, p0, LX/5rQ;->a:LX/5s9;

    .line 1011703
    invoke-static {v0}, LX/5s9;->f(LX/5s9;)V

    .line 1011704
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1011705
    iget-object v1, p0, LX/5rQ;->d:LX/5rP;

    invoke-virtual {v0, v1}, LX/5pY;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 1011706
    invoke-static {}, LX/5s3;->a()LX/5po;

    move-result-object v0

    invoke-virtual {v0}, LX/5po;->b()V

    .line 1011707
    return-void
.end method

.method public findSubviewIn(ILX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011708
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-interface {p2, v2}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2, p3}, LX/5rN;->a(IFFLcom/facebook/react/bridge/Callback;)V

    .line 1011709
    return-void
.end method

.method public final g()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    .line 1011666
    iget v0, p0, LX/5rQ;->f:I

    .line 1011667
    iget v1, p0, LX/5rQ;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/5rQ;->f:I

    .line 1011668
    const-string v1, "onBatchCompleteUI"

    invoke-static {v4, v5, v1}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v1

    const-string v2, "BatchId"

    invoke-virtual {v1, v2, v0}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    .line 1011669
    :try_start_0
    iget-object v1, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v1, v0}, LX/5rN;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011670
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1011671
    return-void

    .line 1011672
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1011682
    const-string v0, "RKUIManager"

    return-object v0
.end method

.method public final i()LX/5s9;
    .locals 1

    .prologue
    .line 1011681
    iget-object v0, p0, LX/5rQ;->a:LX/5s9;

    return-object v0
.end method

.method public manageChildren(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V
    .locals 7
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011679
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/5rN;->a(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V

    .line 1011680
    return-void
.end method

.method public measure(ILcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011677
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->a(ILcom/facebook/react/bridge/Callback;)V

    .line 1011678
    return-void
.end method

.method public measureInWindow(ILcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011675
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->b(ILcom/facebook/react/bridge/Callback;)V

    .line 1011676
    return-void
.end method

.method public measureLayout(IILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011673
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/5rN;->a(IILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 1011674
    return-void
.end method

.method public measureLayoutRelativeToParent(ILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011664
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3}, LX/5rN;->a(ILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 1011665
    return-void
.end method

.method public removeRootView(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011685
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1}, LX/5rN;->b(I)V

    .line 1011686
    return-void
.end method

.method public removeSubviewsFromContainerWithID(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011687
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1}, LX/5rN;->c(I)V

    .line 1011688
    return-void
.end method

.method public replaceExistingNonRootView(II)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011689
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->a(II)V

    .line 1011690
    return-void
.end method

.method public sendAccessibilityEvent(II)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011691
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->b(II)V

    .line 1011692
    return-void
.end method

.method public setChildren(ILX/5pC;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011693
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->a(ILX/5pC;)V

    .line 1011694
    return-void
.end method

.method public setJSResponder(IZ)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011695
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2}, LX/5rN;->a(IZ)V

    .line 1011696
    return-void
.end method

.method public setLayoutAnimationEnabledExperimental(Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011683
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1}, LX/5rN;->a(Z)V

    .line 1011684
    return-void
.end method

.method public showPopupMenu(ILX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011697
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/5rN;->a(ILX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 1011698
    return-void
.end method

.method public updateView(ILjava/lang/String;LX/5pG;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1011699
    iget-object v0, p0, LX/5rQ;->c:LX/5rN;

    invoke-virtual {v0, p1, p2, p3}, LX/5rN;->a(ILjava/lang/String;LX/5pG;)V

    .line 1011700
    return-void
.end method
