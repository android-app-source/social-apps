.class public final LX/5hV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:D

.field public C:D

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:I

.field public G:Z

.field public H:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:I

.field public a:Z

.field public b:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:D

.field public h:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:I

.field public s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:D

.field public u:I

.field public v:I

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 978979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .locals 22

    .prologue
    .line 978980
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 978981
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5hV;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 978982
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 978983
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->h:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 978984
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->j:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 978985
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 978986
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->w:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 978987
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->x:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 978988
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->y:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 978989
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->z:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 978990
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->A:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 978991
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->D:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 978992
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->E:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 978993
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->H:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 978994
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 978995
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5hV;->J:LX/0Px;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v21

    .line 978996
    const/16 v4, 0x25

    invoke-virtual {v2, v4}, LX/186;->c(I)V

    .line 978997
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/5hV;->a:Z

    invoke-virtual {v2, v4, v5}, LX/186;->a(IZ)V

    .line 978998
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/5hV;->b:Z

    invoke-virtual {v2, v4, v5}, LX/186;->a(IZ)V

    .line 978999
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, LX/186;->b(II)V

    .line 979000
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5hV;->d:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 979001
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 979002
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979003
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5hV;->g:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 979004
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 979005
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->i:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979006
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 979007
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->k:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979008
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->l:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979009
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->m:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979010
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->n:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979011
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979012
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->p:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979013
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979014
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->r:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979015
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 979016
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5hV;->t:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 979017
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->u:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979018
    const/16 v3, 0x15

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->v:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979019
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 979020
    const/16 v3, 0x17

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 979021
    const/16 v3, 0x18

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 979022
    const/16 v3, 0x19

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 979023
    const/16 v3, 0x1a

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979024
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5hV;->B:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 979025
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5hV;->C:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 979026
    const/16 v3, 0x1d

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979027
    const/16 v3, 0x1e

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979028
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->F:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979029
    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5hV;->G:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 979030
    const/16 v3, 0x21

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979031
    const/16 v3, 0x22

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979032
    const/16 v3, 0x23

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 979033
    const/16 v3, 0x24

    move-object/from16 v0, p0

    iget v4, v0, LX/5hV;->K:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 979034
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 979035
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 979036
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 979037
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 979038
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 979039
    new-instance v3, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;-><init>(LX/15i;)V

    .line 979040
    return-object v3
.end method
