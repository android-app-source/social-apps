.class public LX/5oh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# instance fields
.field private final a:LX/0oc;

.field private final b:LX/0ej;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(LX/0oc;LX/0ej;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oc;",
            "LX/0ej;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1007435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007436
    iput-object p1, p0, LX/5oh;->a:LX/0oc;

    .line 1007437
    iput-object p2, p0, LX/5oh;->b:LX/0ej;

    .line 1007438
    iput-object p3, p0, LX/5oh;->c:Ljava/lang/String;

    .line 1007439
    iput-object p4, p0, LX/5oh;->d:Ljava/util/Map;

    .line 1007440
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1007441
    iget-object v0, p0, LX/5oh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/5oh;->e:Z

    .line 1007442
    return-void
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 4

    .prologue
    .line 1007443
    if-eqz p4, :cond_1

    .line 1007444
    :cond_0
    :goto_0
    return-void

    .line 1007445
    :cond_1
    iget-boolean v0, p0, LX/5oh;->e:Z

    if-eqz v0, :cond_0

    .line 1007446
    iget-object v0, p0, LX/5oh;->b:LX/0ej;

    iget-object v1, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v0, v1, p2}, LX/0ej;->f(LX/0oc;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007447
    packed-switch p3, :pswitch_data_0

    .line 1007448
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1007449
    :pswitch_0
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->e(LX/0oc;I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1007450
    :pswitch_1
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->b(LX/0oc;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1007451
    :pswitch_2
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->c(LX/0oc;I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1007452
    :pswitch_3
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->d(LX/0oc;I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1007453
    :pswitch_4
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->a(LX/0oc;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1007454
    :pswitch_5
    iget-object v0, p0, LX/5oh;->d:Ljava/util/Map;

    iget-object v1, p0, LX/5oh;->b:LX/0ej;

    iget-object v2, p0, LX/5oh;->a:LX/0oc;

    invoke-virtual {v1, v2, p2}, LX/0ej;->a(LX/0oc;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
