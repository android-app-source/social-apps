.class public final LX/693;
.super LX/68R;
.source ""


# instance fields
.field public o:Lcom/facebook/android/maps/model/LatLng;

.field private p:I

.field public q:D

.field private r:I

.field private s:F

.field private final t:Landroid/graphics/Paint;

.field private final u:[F

.field private final v:LX/31i;

.field private w:F

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(LX/680;LX/694;)V
    .locals 4

    .prologue
    .line 1057267
    invoke-direct {p0, p1}, LX/68R;-><init>(LX/680;)V

    .line 1057268
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    .line 1057269
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/693;->u:[F

    .line 1057270
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/693;->v:LX/31i;

    .line 1057271
    iget-object v0, p2, LX/694;->a:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v0

    .line 1057272
    iput-object v0, p0, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    .line 1057273
    iget v0, p2, LX/694;->b:I

    move v0, v0

    .line 1057274
    iput v0, p0, LX/693;->p:I

    .line 1057275
    iget-wide v2, p2, LX/694;->c:D

    move-wide v0, v2

    .line 1057276
    iput-wide v0, p0, LX/693;->q:D

    .line 1057277
    iget v0, p2, LX/694;->d:I

    move v0, v0

    .line 1057278
    iput v0, p0, LX/693;->r:I

    .line 1057279
    iget v0, p2, LX/694;->e:F

    move v0, v0

    .line 1057280
    iput v0, p0, LX/693;->s:F

    .line 1057281
    iget v0, p2, LX/694;->g:F

    move v0, v0

    .line 1057282
    iput v0, p0, LX/693;->k:F

    .line 1057283
    iget-boolean v0, p2, LX/694;->f:Z

    move v0, v0

    .line 1057284
    iput-boolean v0, p0, LX/693;->i:Z

    .line 1057285
    invoke-static {p0}, LX/693;->q(LX/693;)V

    .line 1057286
    return-void
.end method

.method public static q(LX/693;)V
    .locals 6

    .prologue
    .line 1057259
    iget-object v0, p0, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, LX/31h;->d(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/693;->m:D

    .line 1057260
    iget-object v0, p0, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/693;->n:D

    .line 1057261
    iget-object v0, p0, LX/693;->v:LX/31i;

    iget-object v1, p0, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v4, p0, LX/693;->q:D

    invoke-static {v2, v3, v4, v5}, LX/31h;->a(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/31h;->b(D)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, v0, LX/31i;->a:D

    .line 1057262
    iget-wide v0, p0, LX/67m;->n:D

    iget-object v2, p0, LX/693;->v:LX/31i;

    iget-wide v2, v2, LX/31i;->a:D

    sub-double/2addr v0, v2

    .line 1057263
    iget-object v2, p0, LX/693;->v:LX/31i;

    iget-wide v4, p0, LX/67m;->n:D

    add-double/2addr v4, v0

    iput-wide v4, v2, LX/31i;->b:D

    .line 1057264
    iget-object v2, p0, LX/693;->v:LX/31i;

    iget-wide v4, p0, LX/67m;->m:D

    sub-double/2addr v4, v0

    iput-wide v4, v2, LX/31i;->c:D

    .line 1057265
    iget-object v2, p0, LX/693;->v:LX/31i;

    iget-wide v4, p0, LX/67m;->m:D

    add-double/2addr v0, v4

    iput-wide v0, v2, LX/31i;->d:D

    .line 1057266
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/android/maps/model/LatLng;
    .locals 1

    .prologue
    .line 1057258
    iget-object v0, p0, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    return-object v0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1057249
    iget-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1057250
    iget-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    iget v1, p0, LX/693;->p:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057251
    iget v0, p0, LX/693;->w:F

    iget v1, p0, LX/693;->x:F

    iget v2, p0, LX/693;->y:F

    iget-object v3, p0, LX/693;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1057252
    iget v0, p0, LX/693;->s:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1057253
    iget-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1057254
    iget-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    iget v1, p0, LX/693;->r:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057255
    iget-object v0, p0, LX/693;->t:Landroid/graphics/Paint;

    iget v1, p0, LX/693;->s:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1057256
    iget v0, p0, LX/693;->w:F

    iget v1, p0, LX/693;->x:F

    iget v2, p0, LX/693;->y:F

    iget-object v3, p0, LX/693;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1057257
    :cond_0
    return-void
.end method

.method public final c()LX/31i;
    .locals 1

    .prologue
    .line 1057248
    iget-object v0, p0, LX/693;->v:LX/31i;

    return-object v0
.end method

.method public final p()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1057240
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-wide v2, p0, LX/67m;->m:D

    iget-wide v4, p0, LX/67m;->n:D

    iget-object v6, p0, LX/693;->u:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->a(DD[F)V

    .line 1057241
    iget-object v0, p0, LX/693;->u:[F

    aget v0, v0, v8

    iput v0, p0, LX/693;->w:F

    .line 1057242
    iget-object v0, p0, LX/693;->u:[F

    aget v0, v0, v7

    iput v0, p0, LX/693;->x:F

    .line 1057243
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-wide v2, p0, LX/67m;->m:D

    iget-object v0, p0, LX/693;->v:LX/31i;

    iget-wide v4, v0, LX/31i;->a:D

    iget-object v6, p0, LX/693;->u:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->a(DD[F)V

    .line 1057244
    iget-object v0, p0, LX/693;->u:[F

    aget v0, v0, v8

    .line 1057245
    iget-object v1, p0, LX/693;->u:[F

    aget v1, v1, v7

    .line 1057246
    iget v2, p0, LX/693;->w:F

    sub-float/2addr v0, v2

    float-to-double v2, v0

    iget v0, p0, LX/693;->x:F

    sub-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/693;->y:F

    .line 1057247
    return v7
.end method
