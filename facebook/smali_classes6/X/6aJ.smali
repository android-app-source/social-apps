.class public LX/6aJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6aJ;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0Zm;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1112321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112322
    iput-object p1, p0, LX/6aJ;->a:LX/0Zb;

    .line 1112323
    iput-object p2, p0, LX/6aJ;->b:LX/0Zm;

    .line 1112324
    return-void
.end method

.method public static a(LX/0QB;)LX/6aJ;
    .locals 5

    .prologue
    .line 1112280
    sget-object v0, LX/6aJ;->c:LX/6aJ;

    if-nez v0, :cond_1

    .line 1112281
    const-class v1, LX/6aJ;

    monitor-enter v1

    .line 1112282
    :try_start_0
    sget-object v0, LX/6aJ;->c:LX/6aJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1112283
    if-eqz v2, :cond_0

    .line 1112284
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1112285
    new-instance p0, LX/6aJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v4

    check-cast v4, LX/0Zm;

    invoke-direct {p0, v3, v4}, LX/6aJ;-><init>(LX/0Zb;LX/0Zm;)V

    .line 1112286
    move-object v0, p0

    .line 1112287
    sput-object v0, LX/6aJ;->c:LX/6aJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1112288
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1112289
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1112290
    :cond_1
    sget-object v0, LX/6aJ;->c:LX/6aJ;

    return-object v0

    .line 1112291
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1112292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1112314
    const-string v0, "oxygen_map_here_upsell_dialog_impression"

    .line 1112315
    iget-object v1, p0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v1, v0}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1112316
    :cond_0
    :goto_0
    return-void

    .line 1112317
    :cond_1
    iget-object v1, p0, LX/6aJ;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1112318
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112319
    const-string v1, "oxygen_map"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1112320
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1112307
    const-string v0, "oxygen_map_here_upsell_dialog_clicked"

    .line 1112308
    iget-object v1, p0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v1, v0}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1112309
    :cond_0
    :goto_0
    return-void

    .line 1112310
    :cond_1
    iget-object v1, p0, LX/6aJ;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1112311
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112312
    const-string v1, "oxygen_map"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "action_name"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1112313
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1112300
    const-string v0, "oxygen_map_fullscreen_maps_launched"

    .line 1112301
    iget-object v1, p0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v1, v0}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1112302
    :cond_0
    :goto_0
    return-void

    .line 1112303
    :cond_1
    iget-object v1, p0, LX/6aJ;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1112304
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112305
    const-string v1, "oxygen_map"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "surface"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "query_type"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1112306
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1112293
    const-string v0, "oxygen_map_external_map_app_launched"

    .line 1112294
    iget-object v1, p0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v1, v0}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1112295
    :cond_0
    :goto_0
    return-void

    .line 1112296
    :cond_1
    iget-object v1, p0, LX/6aJ;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1112297
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112298
    const-string v1, "oxygen_map"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "surface"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "query_type"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1112299
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
