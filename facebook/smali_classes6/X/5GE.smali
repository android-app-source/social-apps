.class public final LX/5GE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 887396
    const-wide/16 v12, 0x0

    .line 887397
    const-wide/16 v10, 0x0

    .line 887398
    const-wide/16 v8, 0x0

    .line 887399
    const-wide/16 v6, 0x0

    .line 887400
    const/4 v5, 0x0

    .line 887401
    const/4 v4, 0x0

    .line 887402
    const/4 v3, 0x0

    .line 887403
    const/4 v2, 0x0

    .line 887404
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 887405
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 887406
    const/4 v2, 0x0

    .line 887407
    :goto_0
    return v2

    .line 887408
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 887409
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 887410
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 887411
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 887412
    const-string v6, "east"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 887413
    const/4 v2, 0x1

    .line 887414
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 887415
    :cond_1
    const-string v6, "north"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 887416
    const/4 v2, 0x1

    .line 887417
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 887418
    :cond_2
    const-string v6, "south"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 887419
    const/4 v2, 0x1

    .line 887420
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 887421
    :cond_3
    const-string v6, "west"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 887422
    const/4 v2, 0x1

    .line 887423
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 887424
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 887425
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 887426
    if-eqz v3, :cond_6

    .line 887427
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887428
    :cond_6
    if-eqz v10, :cond_7

    .line 887429
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887430
    :cond_7
    if-eqz v9, :cond_8

    .line 887431
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887432
    :cond_8
    if-eqz v8, :cond_9

    .line 887433
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887434
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method
