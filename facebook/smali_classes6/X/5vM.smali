.class public final LX/5vM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/5vL;

.field public d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public g:Ljava/lang/String;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1021795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021796
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5vM;->j:Z

    .line 1021797
    return-void
.end method

.method public constructor <init>(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V
    .locals 1

    .prologue
    .line 1021798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021799
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5vM;->j:Z

    .line 1021800
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->a:Ljava/lang/String;

    .line 1021801
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->b:Ljava/lang/String;

    .line 1021802
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    iput-object v0, p0, LX/5vM;->c:LX/5vL;

    .line 1021803
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->d:Ljava/lang/String;

    .line 1021804
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->e:Ljava/lang/String;

    .line 1021805
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, LX/5vM;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1021806
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->g:Ljava/lang/String;

    .line 1021807
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    iput-object v0, p0, LX/5vM;->h:LX/0Px;

    .line 1021808
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->i:Ljava/lang/String;

    .line 1021809
    iget-boolean v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->j:Z

    iput-boolean v0, p0, LX/5vM;->j:Z

    .line 1021810
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->k:Ljava/lang/String;

    .line 1021811
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    iput-object v0, p0, LX/5vM;->l:Ljava/lang/String;

    .line 1021812
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;
    .locals 1

    .prologue
    .line 1021813
    new-instance v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    invoke-direct {v0, p0}, Lcom/facebook/story/UpdateTimelineAppCollectionParams;-><init>(LX/5vM;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/5vM;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 1021814
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5vM;->d:Ljava/lang/String;

    .line 1021815
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/5vM;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1021816
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/5vM;->e:Ljava/lang/String;

    .line 1021817
    return-object p0

    .line 1021818
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
