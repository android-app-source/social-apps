.class public final enum LX/62r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/62r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/62r;

.field public static final enum BUFFERING:LX/62r;

.field public static final enum COLLAPSING_AFTER_REFRESH:LX/62r;

.field public static final enum FAILED:LX/62r;

.field public static final enum FINISHED:LX/62r;

.field public static final enum LOADING:LX/62r;

.field public static final enum NORMAL:LX/62r;

.field public static final enum POPUP:LX/62r;

.field public static final enum PULL_TO_REFRESH:LX/62r;

.field public static final enum PUSH_TO_REFRESH:LX/62r;

.field public static final enum RELEASE_TO_REFRESH:LX/62r;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1042684
    new-instance v0, LX/62r;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->NORMAL:LX/62r;

    .line 1042685
    new-instance v0, LX/62r;

    const-string v1, "PULL_TO_REFRESH"

    invoke-direct {v0, v1, v4}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->PULL_TO_REFRESH:LX/62r;

    .line 1042686
    new-instance v0, LX/62r;

    const-string v1, "PUSH_TO_REFRESH"

    invoke-direct {v0, v1, v5}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->PUSH_TO_REFRESH:LX/62r;

    .line 1042687
    new-instance v0, LX/62r;

    const-string v1, "RELEASE_TO_REFRESH"

    invoke-direct {v0, v1, v6}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->RELEASE_TO_REFRESH:LX/62r;

    .line 1042688
    new-instance v0, LX/62r;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v7}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->LOADING:LX/62r;

    .line 1042689
    new-instance v0, LX/62r;

    const-string v1, "BUFFERING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->BUFFERING:LX/62r;

    .line 1042690
    new-instance v0, LX/62r;

    const-string v1, "POPUP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->POPUP:LX/62r;

    .line 1042691
    new-instance v0, LX/62r;

    const-string v1, "COLLAPSING_AFTER_REFRESH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    .line 1042692
    new-instance v0, LX/62r;

    const-string v1, "FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->FAILED:LX/62r;

    .line 1042693
    new-instance v0, LX/62r;

    const-string v1, "FINISHED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/62r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62r;->FINISHED:LX/62r;

    .line 1042694
    const/16 v0, 0xa

    new-array v0, v0, [LX/62r;

    sget-object v1, LX/62r;->NORMAL:LX/62r;

    aput-object v1, v0, v3

    sget-object v1, LX/62r;->PULL_TO_REFRESH:LX/62r;

    aput-object v1, v0, v4

    sget-object v1, LX/62r;->PUSH_TO_REFRESH:LX/62r;

    aput-object v1, v0, v5

    sget-object v1, LX/62r;->RELEASE_TO_REFRESH:LX/62r;

    aput-object v1, v0, v6

    sget-object v1, LX/62r;->LOADING:LX/62r;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/62r;->BUFFERING:LX/62r;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/62r;->POPUP:LX/62r;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/62r;->FAILED:LX/62r;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/62r;->FINISHED:LX/62r;

    aput-object v2, v0, v1

    sput-object v0, LX/62r;->$VALUES:[LX/62r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1042695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/62r;
    .locals 1

    .prologue
    .line 1042696
    const-class v0, LX/62r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/62r;

    return-object v0
.end method

.method public static values()[LX/62r;
    .locals 1

    .prologue
    .line 1042697
    sget-object v0, LX/62r;->$VALUES:[LX/62r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/62r;

    return-object v0
.end method
