.class public LX/5zl;
.super LX/4cO;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/ContentResolver;

.field private final d:Lcom/facebook/ui/media/attachments/MediaResource;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1035706
    const-class v0, LX/5zl;

    sput-object v0, LX/5zl;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0SG;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 1

    .prologue
    .line 1035665
    invoke-static {p1, p3}, LX/5zl;->a(Landroid/content/ContentResolver;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4cO;-><init>(Ljava/lang/String;)V

    .line 1035666
    iput-object p1, p0, LX/5zl;->c:Landroid/content/ContentResolver;

    .line 1035667
    iput-object p3, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035668
    invoke-static {p2, p3}, LX/5zl;->a(LX/0SG;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5zl;->e:Ljava/lang/String;

    .line 1035669
    return-void
.end method

.method private static a(LX/0SG;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1035697
    sget-object v0, LX/5zk;->b:[I

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1035698
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected attachment type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035699
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1035700
    :goto_0
    return-object v0

    .line 1035701
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1035702
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1035703
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audioclip-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1035704
    :pswitch_3
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1035705
    :pswitch_4
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Landroid/content/ContentResolver;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1035687
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1035688
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1035689
    :cond_0
    :goto_0
    return-object v0

    .line 1035690
    :cond_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1035691
    if-nez v0, :cond_0

    .line 1035692
    sget-object v0, LX/5zk;->b:[I

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1035693
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected attachment type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035694
    :pswitch_0
    const-string v0, "image/jpeg"

    goto :goto_0

    .line 1035695
    :pswitch_1
    const-string v0, "video/mp4"

    goto :goto_0

    .line 1035696
    :pswitch_2
    const-string v0, "audio/MPEG"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035686
    iget-object v0, p0, LX/5zl;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 1035677
    iget-object v0, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/5zs;->from(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v0

    .line 1035678
    sget-object v1, LX/5zk;->a:[I

    invoke-virtual {v0}, LX/5zs;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1035679
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported scheme"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035680
    :pswitch_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1035681
    invoke-static {v0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1vI;->a(Ljava/io/OutputStream;)J

    .line 1035682
    :goto_0
    return-void

    .line 1035683
    :pswitch_1
    iget-object v0, p0, LX/5zl;->c:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 1035684
    :try_start_0
    invoke-static {v0, p1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1035685
    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035676
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035675
    const-string v0, "binary"

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1035670
    iget-object v0, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/5zs;->from(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v0

    .line 1035671
    sget-object v1, LX/5zk;->a:[I

    invoke-virtual {v0}, LX/5zs;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1035672
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    .line 1035673
    :pswitch_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/5zl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1035674
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
