.class public final LX/53N;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/53N;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/53N;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<-TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile d:J

.field private final f:LX/52r;

.field public final g:LX/53P;

.field public final h:LX/53n;

.field private i:Z

.field public j:Z

.field private volatile k:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 827103
    const-class v0, LX/53N;

    const-string v1, "k"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53N;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 827104
    const-class v0, LX/53N;

    const-string v1, "d"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53N;->e:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/52s;LX/0zZ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/52s;",
            "LX/0zZ",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 827124
    invoke-direct {p0}, LX/0zZ;-><init>()V

    .line 827125
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 827126
    iput-object v0, p0, LX/53N;->b:LX/0vH;

    .line 827127
    invoke-static {}, LX/54I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827128
    new-instance v0, LX/53n;

    sget-object v2, LX/53n;->i:LX/53d;

    sget v3, LX/53n;->c:I

    invoke-direct {v0, v2, v3}, LX/53n;-><init>(LX/53d;I)V

    .line 827129
    :goto_0
    move-object v0, v0

    .line 827130
    iput-object v0, p0, LX/53N;->h:LX/53n;

    .line 827131
    iput-boolean v1, p0, LX/53N;->i:Z

    .line 827132
    iput-boolean v1, p0, LX/53N;->j:Z

    .line 827133
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/53N;->k:J

    .line 827134
    iput-object p2, p0, LX/53N;->a:LX/0zZ;

    .line 827135
    invoke-virtual {p1}, LX/52s;->a()LX/52r;

    move-result-object v0

    iput-object v0, p0, LX/53N;->f:LX/52r;

    .line 827136
    new-instance v0, LX/53P;

    iget-object v1, p0, LX/53N;->f:LX/52r;

    iget-object v2, p0, LX/53N;->h:LX/53n;

    invoke-direct {v0, v1, v2}, LX/53P;-><init>(LX/52r;LX/53n;)V

    iput-object v0, p0, LX/53N;->g:LX/53P;

    .line 827137
    iget-object v0, p0, LX/53N;->g:LX/53P;

    invoke-virtual {p2, v0}, LX/0zZ;->a(LX/0za;)V

    .line 827138
    new-instance v0, LX/53L;

    invoke-direct {v0, p0}, LX/53L;-><init>(LX/53N;)V

    invoke-virtual {p2, v0}, LX/0zZ;->a(LX/52p;)V

    .line 827139
    iget-object v0, p0, LX/53N;->f:LX/52r;

    invoke-virtual {p2, v0}, LX/0zZ;->a(LX/0za;)V

    .line 827140
    invoke-virtual {p2, p0}, LX/0zZ;->a(LX/0za;)V

    .line 827141
    return-void

    :cond_0
    new-instance v0, LX/53n;

    invoke-direct {v0}, LX/53n;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final R_()V
    .locals 1

    .prologue
    .line 827119
    invoke-virtual {p0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/53N;->i:Z

    if-eqz v0, :cond_1

    .line 827120
    :cond_0
    :goto_0
    return-void

    .line 827121
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/53N;->i:Z

    .line 827122
    iget-object v0, p0, LX/53N;->h:LX/53n;

    invoke-virtual {v0}, LX/53n;->e()V

    .line 827123
    invoke-virtual {p0}, LX/53N;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 827142
    invoke-virtual {p0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/53N;->i:Z

    if-eqz v0, :cond_1

    .line 827143
    :cond_0
    :goto_0
    return-void

    .line 827144
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/53N;->h:LX/53n;

    invoke-virtual {v0, p1}, LX/53n;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch LX/52z; {:try_start_0 .. :try_end_0} :catch_0

    .line 827145
    invoke-virtual {p0}, LX/53N;->e()V

    goto :goto_0

    .line 827146
    :catch_0
    move-exception v0

    .line 827147
    invoke-virtual {p0, v0}, LX/53N;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 827110
    invoke-virtual {p0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/53N;->i:Z

    if-eqz v0, :cond_1

    .line 827111
    :cond_0
    :goto_0
    return-void

    .line 827112
    :cond_1
    invoke-virtual {p0}, LX/0zZ;->b()V

    .line 827113
    iput-boolean v1, p0, LX/53N;->i:Z

    .line 827114
    iput-boolean v1, p0, LX/53N;->j:Z

    .line 827115
    iget-object v0, p0, LX/53N;->h:LX/53n;

    .line 827116
    iget-object v1, v0, LX/53n;->a:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 827117
    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, LX/53n;->a:Ljava/lang/Object;

    .line 827118
    :cond_2
    invoke-virtual {p0}, LX/53N;->e()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 827108
    sget v0, LX/53n;->c:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/0zZ;->a(J)V

    .line 827109
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 827105
    sget-object v0, LX/53N;->e:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndIncrement(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 827106
    iget-object v0, p0, LX/53N;->f:LX/52r;

    new-instance v1, LX/53M;

    invoke-direct {v1, p0}, LX/53M;-><init>(LX/53N;)V

    invoke-virtual {v0, v1}, LX/52r;->a(LX/0vR;)LX/0za;

    .line 827107
    :cond_0
    return-void
.end method
