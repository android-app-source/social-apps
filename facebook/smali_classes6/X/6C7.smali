.class public final LX/6C7;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/6C8;

.field public b:LX/2h0;


# direct methods
.method public constructor <init>(LX/6C8;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1063476
    iput-object p1, p0, LX/6C7;->a:LX/6C8;

    .line 1063477
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1063478
    new-instance v0, LX/6C6;

    invoke-direct {v0, p0, p1}, LX/6C6;-><init>(LX/6C7;LX/6C8;)V

    iput-object v0, p0, LX/6C7;->b:LX/2h0;

    .line 1063479
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1063480
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1063481
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1063482
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Illegal action specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1063483
    :pswitch_0
    iget-object v1, p0, LX/6C7;->a:LX/6C8;

    iget-object v1, v1, LX/6C8;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5up;

    const-string v2, "native_web_view"

    const-string v3, "saved_add"

    iget-object p1, p0, LX/6C7;->b:LX/2h0;

    invoke-virtual {v1, v0, v2, v3, p1}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 1063484
    :goto_0
    return-void

    .line 1063485
    :pswitch_1
    iget-object v1, p0, LX/6C7;->a:LX/6C8;

    iget-object v1, v1, LX/6C8;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1063486
    iget-object v1, p0, LX/6C7;->a:LX/6C8;

    iget-object v1, v1, LX/6C8;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f081cf5

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1063487
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
