.class public final LX/6df;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0U1;

.field public static final B:LX/0U1;

.field public static final C:LX/0U1;

.field public static final D:LX/0U1;

.field public static final E:LX/0U1;

.field public static final F:LX/0U1;

.field public static final G:LX/0U1;

.field public static final H:LX/0U1;

.field public static final I:LX/0U1;

.field public static final J:LX/0U1;

.field public static final K:LX/0U1;

.field public static final L:LX/0U1;

.field public static final M:LX/0U1;

.field public static final N:LX/0U1;

.field public static final O:LX/0U1;

.field public static final P:LX/0U1;

.field public static final Q:LX/0U1;

.field public static final R:LX/0U1;

.field public static final S:LX/0U1;

.field public static final T:LX/0U1;

.field public static final U:LX/0U1;

.field public static final V:LX/0U1;

.field public static final W:LX/0U1;

.field public static final X:LX/0U1;

.field public static final Y:LX/0U1;

.field public static final Z:LX/0U1;

.field public static final a:LX/0U1;

.field public static final aa:LX/0U1;

.field public static final ab:LX/0U1;

.field public static final ac:LX/0U1;

.field public static final ad:LX/0U1;

.field public static final ae:LX/0U1;

.field public static final af:LX/0U1;

.field public static final ag:LX/0U1;

.field public static final ah:LX/0U1;

.field public static final ai:LX/0U1;

.field public static final aj:LX/0U1;

.field public static final ak:LX/0U1;

.field public static final al:LX/0U1;

.field public static final am:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1116270
    new-instance v0, LX/0U1;

    const-string v1, "msg_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->a:LX/0U1;

    .line 1116271
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->b:LX/0U1;

    .line 1116272
    new-instance v0, LX/0U1;

    const-string v1, "action_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->c:LX/0U1;

    .line 1116273
    new-instance v0, LX/0U1;

    const-string v1, "text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->d:LX/0U1;

    .line 1116274
    new-instance v0, LX/0U1;

    const-string v1, "sender"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->e:LX/0U1;

    .line 1116275
    new-instance v0, LX/0U1;

    const-string v1, "is_not_forwardable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->f:LX/0U1;

    .line 1116276
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->g:LX/0U1;

    .line 1116277
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_sent_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->h:LX/0U1;

    .line 1116278
    new-instance v0, LX/0U1;

    const-string v1, "attachments"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->i:LX/0U1;

    .line 1116279
    new-instance v0, LX/0U1;

    const-string v1, "shares"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->j:LX/0U1;

    .line 1116280
    new-instance v0, LX/0U1;

    const-string v1, "sticker_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->k:LX/0U1;

    .line 1116281
    new-instance v0, LX/0U1;

    const-string v1, "msg_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->l:LX/0U1;

    .line 1116282
    new-instance v0, LX/0U1;

    const-string v1, "affected_users"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->m:LX/0U1;

    .line 1116283
    new-instance v0, LX/0U1;

    const-string v1, "coordinates"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->n:LX/0U1;

    .line 1116284
    new-instance v0, LX/0U1;

    const-string v1, "offline_threading_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->o:LX/0U1;

    .line 1116285
    new-instance v0, LX/0U1;

    const-string v1, "source"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->p:LX/0U1;

    .line 1116286
    new-instance v0, LX/0U1;

    const-string v1, "channel_source"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->q:LX/0U1;

    .line 1116287
    new-instance v0, LX/0U1;

    const-string v1, "send_channel"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->r:LX/0U1;

    .line 1116288
    new-instance v0, LX/0U1;

    const-string v1, "is_non_authoritative"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->s:LX/0U1;

    .line 1116289
    new-instance v0, LX/0U1;

    const-string v1, "pending_send_media_attachment"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->t:LX/0U1;

    .line 1116290
    new-instance v0, LX/0U1;

    const-string v1, "sent_share_attachment"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->u:LX/0U1;

    .line 1116291
    new-instance v0, LX/0U1;

    const-string v1, "client_tags"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->v:LX/0U1;

    .line 1116292
    new-instance v0, LX/0U1;

    const-string v1, "send_error"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->w:LX/0U1;

    .line 1116293
    new-instance v0, LX/0U1;

    const-string v1, "send_error_message"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->x:LX/0U1;

    .line 1116294
    new-instance v0, LX/0U1;

    const-string v1, "send_error_number"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->y:LX/0U1;

    .line 1116295
    new-instance v0, LX/0U1;

    const-string v1, "send_error_timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->z:LX/0U1;

    .line 1116296
    new-instance v0, LX/0U1;

    const-string v1, "send_error_error_url"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->A:LX/0U1;

    .line 1116297
    new-instance v0, LX/0U1;

    const-string v1, "publicity"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->B:LX/0U1;

    .line 1116298
    new-instance v0, LX/0U1;

    const-string v1, "send_queue_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->C:LX/0U1;

    .line 1116299
    new-instance v0, LX/0U1;

    const-string v1, "payment_transaction"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->D:LX/0U1;

    .line 1116300
    new-instance v0, LX/0U1;

    const-string v1, "payment_request"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->E:LX/0U1;

    .line 1116301
    new-instance v0, LX/0U1;

    const-string v1, "has_unavailable_attachment"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->F:LX/0U1;

    .line 1116302
    new-instance v0, LX/0U1;

    const-string v1, "app_attribution"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->G:LX/0U1;

    .line 1116303
    new-instance v0, LX/0U1;

    const-string v1, "content_app_attribution"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->H:LX/0U1;

    .line 1116304
    new-instance v0, LX/0U1;

    const-string v1, "xma"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->I:LX/0U1;

    .line 1116305
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->J:LX/0U1;

    .line 1116306
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_theme_color"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->K:LX/0U1;

    .line 1116307
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_icon_emoji"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->L:LX/0U1;

    .line 1116308
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_nickname"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->M:LX/0U1;

    .line 1116309
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_target_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->N:LX/0U1;

    .line 1116310
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_message_lifetime"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->O:LX/0U1;

    .line 1116311
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_journey_color_choices"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->P:LX/0U1;

    .line 1116312
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_journey_emoji_choices"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->Q:LX/0U1;

    .line 1116313
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_journey_nickname_choices"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->R:LX/0U1;

    .line 1116314
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_journey_bot_choices"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->S:LX/0U1;

    .line 1116315
    new-instance v0, LX/0U1;

    const-string v1, "message_lifetime"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->T:LX/0U1;

    .line 1116316
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_rtc_event"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->U:LX/0U1;

    .line 1116317
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_rtc_server_info_data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->V:LX/0U1;

    .line 1116318
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_rtc_is_video_call"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->W:LX/0U1;

    .line 1116319
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_ride_provider_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->X:LX/0U1;

    .line 1116320
    new-instance v0, LX/0U1;

    const-string v1, "is_sponsored"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->Y:LX/0U1;

    .line 1116321
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_ad_properties"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->Z:LX/0U1;

    .line 1116322
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_game_score_data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->aa:LX/0U1;

    .line 1116323
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_thread_event_reminder_properties"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ab:LX/0U1;

    .line 1116324
    new-instance v0, LX/0U1;

    const-string v1, "commerce_message_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ac:LX/0U1;

    .line 1116325
    new-instance v0, LX/0U1;

    const-string v1, "customizations"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ad:LX/0U1;

    .line 1116326
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_joinable_event_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ae:LX/0U1;

    .line 1116327
    new-instance v0, LX/0U1;

    const-string v1, "metadata_at_text_ranges"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->af:LX/0U1;

    .line 1116328
    new-instance v0, LX/0U1;

    const-string v1, "platform_metadata"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ag:LX/0U1;

    .line 1116329
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_is_joinable_promo"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ah:LX/0U1;

    .line 1116330
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_agent_intent_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ai:LX/0U1;

    .line 1116331
    new-instance v0, LX/0U1;

    const-string v1, "montage_reply_message_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->aj:LX/0U1;

    .line 1116332
    new-instance v0, LX/0U1;

    const-string v1, "admin_text_booking_request_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->ak:LX/0U1;

    .line 1116333
    new-instance v0, LX/0U1;

    const-string v1, "generic_admin_message_extensible_data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->al:LX/0U1;

    .line 1116334
    new-instance v0, LX/0U1;

    const-string v1, "profile_ranges"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6df;->am:LX/0U1;

    return-void
.end method
