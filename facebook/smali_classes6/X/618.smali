.class public LX/618;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private a:LX/612;

.field private b:I

.field private c:I

.field private d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Z

.field public i:I

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>(LX/612;III)V
    .locals 1

    .prologue
    .line 1039726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039727
    const v0, 0x5dc00

    iput v0, p0, LX/618;->e:I

    .line 1039728
    const/16 v0, 0xf

    iput v0, p0, LX/618;->f:I

    .line 1039729
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/618;->h:Z

    .line 1039730
    const/4 v0, 0x1

    iput v0, p0, LX/618;->i:I

    .line 1039731
    const/16 v0, 0x100

    iput v0, p0, LX/618;->j:I

    .line 1039732
    const/4 v0, -0x1

    iput v0, p0, LX/618;->k:I

    .line 1039733
    iput-object p1, p0, LX/618;->a:LX/612;

    .line 1039734
    iput p2, p0, LX/618;->b:I

    .line 1039735
    iput p3, p0, LX/618;->c:I

    .line 1039736
    iput p4, p0, LX/618;->d:I

    .line 1039737
    return-void
.end method


# virtual methods
.method public final a()Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 1039738
    iget-object v0, p0, LX/618;->a:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    iget v1, p0, LX/618;->b:I

    iget v2, p0, LX/618;->c:I

    invoke-static {v0, v1, v2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 1039739
    const-string v1, "color-format"

    iget v2, p0, LX/618;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039740
    iget v1, p0, LX/618;->e:I

    if-lez v1, :cond_0

    .line 1039741
    const-string v1, "bitrate"

    iget v2, p0, LX/618;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039742
    :cond_0
    iget v1, p0, LX/618;->f:I

    if-lez v1, :cond_1

    .line 1039743
    const-string v1, "frame-rate"

    iget v2, p0, LX/618;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039744
    :cond_1
    iget v1, p0, LX/618;->g:I

    if-lez v1, :cond_2

    .line 1039745
    const-string v1, "i-frame-interval"

    iget v2, p0, LX/618;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039746
    :cond_2
    iget-boolean v1, p0, LX/618;->h:Z

    if-eqz v1, :cond_3

    .line 1039747
    const-string v1, "profile"

    iget v2, p0, LX/618;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039748
    const-string v1, "level"

    iget v2, p0, LX/618;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039749
    :cond_3
    iget v1, p0, LX/618;->k:I

    if-ltz v1, :cond_4

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_4

    .line 1039750
    const-string v1, "bitrate-mode"

    iget v2, p0, LX/618;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1039751
    :cond_4
    return-object v0
.end method
