.class public final LX/56Y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 838134
    const/4 v12, 0x0

    .line 838135
    const/4 v11, 0x0

    .line 838136
    const/4 v10, 0x0

    .line 838137
    const/4 v9, 0x0

    .line 838138
    const/4 v8, 0x0

    .line 838139
    const/4 v7, 0x0

    .line 838140
    const/4 v6, 0x0

    .line 838141
    const/4 v5, 0x0

    .line 838142
    const/4 v4, 0x0

    .line 838143
    const/4 v3, 0x0

    .line 838144
    const/4 v2, 0x0

    .line 838145
    const/4 v1, 0x0

    .line 838146
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 838147
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 838148
    const/4 v1, 0x0

    .line 838149
    :goto_0
    return v1

    .line 838150
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 838151
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 838152
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 838153
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 838154
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 838155
    const-string v14, "context_page"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 838156
    invoke-static/range {p0 .. p1}, LX/56g;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 838157
    :cond_2
    const-string v14, "follow_up_title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 838158
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 838159
    :cond_3
    const-string v14, "info_fields_data"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 838160
    invoke-static/range {p0 .. p1}, LX/56j;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 838161
    :cond_4
    const-string v14, "legal_content"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 838162
    invoke-static/range {p0 .. p1}, LX/56p;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 838163
    :cond_5
    const-string v14, "need_split_flow"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 838164
    const/4 v2, 0x1

    .line 838165
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 838166
    :cond_6
    const-string v14, "pages"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 838167
    invoke-static/range {p0 .. p1}, LX/56X;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 838168
    :cond_7
    const-string v14, "policy_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 838169
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 838170
    :cond_8
    const-string v14, "split_flow_request_method"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 838171
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 838172
    :cond_9
    const-string v14, "split_flow_use_post"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 838173
    const/4 v1, 0x1

    .line 838174
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 838175
    :cond_a
    const-string v14, "thank_you_page"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 838176
    invoke-static/range {p0 .. p1}, LX/56q;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 838177
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 838178
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 838179
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 838180
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 838181
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 838182
    if-eqz v2, :cond_c

    .line 838183
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 838184
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 838185
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 838186
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 838187
    if-eqz v1, :cond_d

    .line 838188
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 838189
    :cond_d
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 838190
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 838191
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 838192
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838193
    if-eqz v0, :cond_0

    .line 838194
    const-string v1, "context_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838195
    invoke-static {p0, v0, p2, p3}, LX/56g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 838196
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838197
    if-eqz v0, :cond_1

    .line 838198
    const-string v1, "follow_up_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838199
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838200
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838201
    if-eqz v0, :cond_2

    .line 838202
    const-string v1, "info_fields_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838203
    invoke-static {p0, v0, p2, p3}, LX/56j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 838204
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838205
    if-eqz v0, :cond_3

    .line 838206
    const-string v1, "legal_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838207
    invoke-static {p0, v0, p2, p3}, LX/56p;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 838208
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 838209
    if-eqz v0, :cond_4

    .line 838210
    const-string v1, "need_split_flow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838211
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 838212
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838213
    if-eqz v0, :cond_6

    .line 838214
    const-string v1, "pages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838215
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 838216
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 838217
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/63w;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 838218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 838219
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 838220
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838221
    if-eqz v0, :cond_7

    .line 838222
    const-string v1, "policy_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838223
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838224
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838225
    if-eqz v0, :cond_8

    .line 838226
    const-string v1, "split_flow_request_method"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838227
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838228
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 838229
    if-eqz v0, :cond_9

    .line 838230
    const-string v1, "split_flow_use_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838231
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 838232
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838233
    if-eqz v0, :cond_a

    .line 838234
    const-string v1, "thank_you_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838235
    invoke-static {p0, v0, p2}, LX/56q;->a(LX/15i;ILX/0nX;)V

    .line 838236
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 838237
    return-void
.end method
