.class public LX/5uY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public final d:[I

.field public final e:[F

.field public final f:[F

.field public final g:[F

.field public final h:[F

.field public i:LX/5uZ;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1019408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1019409
    new-array v0, v1, [I

    iput-object v0, p0, LX/5uY;->d:[I

    .line 1019410
    new-array v0, v1, [F

    iput-object v0, p0, LX/5uY;->e:[F

    .line 1019411
    new-array v0, v1, [F

    iput-object v0, p0, LX/5uY;->f:[F

    .line 1019412
    new-array v0, v1, [F

    iput-object v0, p0, LX/5uY;->g:[F

    .line 1019413
    new-array v0, v1, [F

    iput-object v0, p0, LX/5uY;->h:[F

    .line 1019414
    const/4 v0, 0x0

    iput-object v0, p0, LX/5uY;->i:LX/5uZ;

    .line 1019415
    invoke-virtual {p0}, LX/5uY;->b()V

    .line 1019416
    return-void
.end method

.method public static a(Landroid/view/MotionEvent;I)I
    .locals 4

    .prologue
    .line 1019417
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 1019418
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1019419
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    .line 1019420
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_2

    .line 1019421
    :cond_0
    if-lt p1, v2, :cond_2

    .line 1019422
    add-int/lit8 p1, p1, 0x1

    move v0, p1

    .line 1019423
    :goto_0
    if-ge v0, v1, :cond_1

    :goto_1
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :cond_2
    move v0, p1

    goto :goto_0
.end method

.method public static j(LX/5uY;)V
    .locals 2

    .prologue
    .line 1019424
    iget-boolean v0, p0, LX/5uY;->a:Z

    if-nez v0, :cond_1

    .line 1019425
    iget-object v0, p0, LX/5uY;->i:LX/5uZ;

    if-eqz v0, :cond_0

    .line 1019426
    iget-object v0, p0, LX/5uY;->i:LX/5uZ;

    .line 1019427
    iget-object v1, v0, LX/5uZ;->b:LX/5ua;

    if-eqz v1, :cond_0

    .line 1019428
    iget-object v1, v0, LX/5uZ;->b:LX/5ua;

    invoke-virtual {v1, v0}, LX/5ua;->a(LX/5uZ;)V

    .line 1019429
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5uY;->a:Z

    .line 1019430
    :cond_1
    return-void
.end method

.method public static k(LX/5uY;)V
    .locals 1

    .prologue
    .line 1019431
    iget-boolean v0, p0, LX/5uY;->a:Z

    if-eqz v0, :cond_0

    .line 1019432
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5uY;->a:Z

    .line 1019433
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1019434
    iput-boolean v0, p0, LX/5uY;->a:Z

    .line 1019435
    iput v0, p0, LX/5uY;->b:I

    .line 1019436
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1019437
    iget-object v1, p0, LX/5uY;->d:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 1019438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1019439
    :cond_0
    return-void
.end method
