.class public LX/66c;
.super LX/66Y;
.source ""


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:Ljava/lang/reflect/Method;

.field private final c:Ljava/lang/reflect/Method;

.field private final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1050559
    invoke-direct {p0}, LX/66Y;-><init>()V

    .line 1050560
    iput-object p1, p0, LX/66c;->a:Ljava/lang/reflect/Method;

    .line 1050561
    iput-object p2, p0, LX/66c;->b:Ljava/lang/reflect/Method;

    .line 1050562
    iput-object p3, p0, LX/66c;->c:Ljava/lang/reflect/Method;

    .line 1050563
    iput-object p4, p0, LX/66c;->d:Ljava/lang/Class;

    .line 1050564
    iput-object p5, p0, LX/66c;->e:Ljava/lang/Class;

    .line 1050565
    return-void
.end method


# virtual methods
.method public final a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1050566
    :try_start_0
    iget-object v0, p0, LX/66c;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 1050567
    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v0

    check-cast v0, LX/66b;

    .line 1050568
    iget-boolean v2, v0, LX/66b;->b:Z

    if-nez v2, :cond_0

    iget-object v2, v0, LX/66b;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1050569
    sget-object v0, LX/66Y;->a:LX/66Y;

    move-object v0, v0

    .line 1050570
    const/4 v2, 0x4

    const-string v3, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, LX/66Y;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1050571
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v2, v0, LX/66b;->b:Z

    if-eqz v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v0, LX/66b;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1050572
    :catch_0
    :goto_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catch_1
    goto :goto_1
.end method

.method public final a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/net/ssl/SSLSocket;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1050573
    invoke-static {p3}, LX/66Y;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1050574
    :try_start_0
    const-class v1, LX/66Y;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v4, p0, LX/66c;->d:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/66c;->e:Ljava/lang/Class;

    aput-object v4, v2, v3

    new-instance v3, LX/66b;

    invoke-direct {v3, v0}, LX/66b;-><init>(Ljava/util/List;)V

    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 1050575
    iget-object v1, p0, LX/66c;->a:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050576
    return-void

    .line 1050577
    :catch_0
    move-exception v0

    .line 1050578
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 1050579
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljavax/net/ssl/SSLSocket;)V
    .locals 4

    .prologue
    .line 1050580
    :try_start_0
    iget-object v0, p0, LX/66c;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1050581
    return-void

    .line 1050582
    :catch_0
    :goto_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catch_1
    goto :goto_0
.end method
