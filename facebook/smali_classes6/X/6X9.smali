.class public LX/6X9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V
    .locals 1

    .prologue
    .line 1107863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1107864
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    iput-object v0, p0, LX/6X9;->a:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 1107865
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/6X9;
    .locals 1

    .prologue
    .line 1107866
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107867
    new-instance v0, LX/6X9;

    invoke-direct {v0, p0}, LX/6X9;-><init>(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)LX/6X9;
    .locals 1

    .prologue
    .line 1107868
    iget-object v0, p0, LX/6X9;->a:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 1107869
    invoke-virtual {v0, p1}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a(I)V

    .line 1107870
    return-object p0
.end method
