.class public final LX/5Up;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 926685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel;
    .locals 15

    .prologue
    .line 926686
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 926687
    iget-object v1, p0, LX/5Up;->a:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 926688
    iget-object v2, p0, LX/5Up;->b:LX/0Px;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 926689
    iget-object v3, p0, LX/5Up;->c:Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 926690
    iget-object v4, p0, LX/5Up;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 926691
    iget-object v5, p0, LX/5Up;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 926692
    iget-object v6, p0, LX/5Up;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 926693
    iget-object v7, p0, LX/5Up;->g:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 926694
    iget-object v8, p0, LX/5Up;->h:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 926695
    iget-object v9, p0, LX/5Up;->i:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 926696
    iget-object v10, p0, LX/5Up;->j:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 926697
    iget-object v11, p0, LX/5Up;->k:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 926698
    iget-object v12, p0, LX/5Up;->l:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 926699
    iget-object v13, p0, LX/5Up;->m:Ljava/lang/String;

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 926700
    const/16 v14, 0xd

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 926701
    const/4 v14, 0x0

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 926702
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 926703
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 926704
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 926705
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 926706
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 926707
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 926708
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 926709
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 926710
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 926711
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 926712
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 926713
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 926714
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 926715
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 926716
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 926717
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 926718
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 926719
    new-instance v1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel;-><init>(LX/15i;)V

    .line 926720
    return-object v1
.end method
