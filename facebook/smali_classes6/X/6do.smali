.class public final LX/6do;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0U1;

.field public static final B:LX/0U1;

.field public static final C:LX/0U1;

.field public static final D:LX/0U1;

.field public static final E:LX/0U1;

.field public static final F:LX/0U1;

.field public static final G:LX/0U1;

.field public static final H:LX/0U1;

.field public static final I:LX/0U1;

.field public static final J:LX/0U1;

.field public static final K:LX/0U1;

.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1116386
    new-instance v0, LX/0U1;

    const-string v1, "user_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->a:LX/0U1;

    .line 1116387
    new-instance v0, LX/0U1;

    const-string v1, "first_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->b:LX/0U1;

    .line 1116388
    new-instance v0, LX/0U1;

    const-string v1, "last_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->c:LX/0U1;

    .line 1116389
    new-instance v0, LX/0U1;

    const-string v1, "username"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->d:LX/0U1;

    .line 1116390
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->e:LX/0U1;

    .line 1116391
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_user"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->f:LX/0U1;

    .line 1116392
    new-instance v0, LX/0U1;

    const-string v1, "profile_pic_square"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->g:LX/0U1;

    .line 1116393
    new-instance v0, LX/0U1;

    const-string v1, "profile_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->h:LX/0U1;

    .line 1116394
    new-instance v0, LX/0U1;

    const-string v1, "is_commerce"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->i:LX/0U1;

    .line 1116395
    new-instance v0, LX/0U1;

    const-string v1, "commerce_page_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->j:LX/0U1;

    .line 1116396
    new-instance v0, LX/0U1;

    const-string v1, "is_partial"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->k:LX/0U1;

    .line 1116397
    new-instance v0, LX/0U1;

    const-string v1, "user_rank"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->l:LX/0U1;

    .line 1116398
    new-instance v0, LX/0U1;

    const-string v1, "is_blocked_by_viewer"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->m:LX/0U1;

    .line 1116399
    new-instance v0, LX/0U1;

    const-string v1, "is_message_blocked_by_viewer"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->n:LX/0U1;

    .line 1116400
    new-instance v0, LX/0U1;

    const-string v1, "can_viewer_message"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->o:LX/0U1;

    .line 1116401
    new-instance v0, LX/0U1;

    const-string v1, "commerce_page_settings"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->p:LX/0U1;

    .line 1116402
    new-instance v0, LX/0U1;

    const-string v1, "is_friend"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->q:LX/0U1;

    .line 1116403
    new-instance v0, LX/0U1;

    const-string v1, "last_fetch_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->r:LX/0U1;

    .line 1116404
    new-instance v0, LX/0U1;

    const-string v1, "montage_thread_fbid"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->s:LX/0U1;

    .line 1116405
    new-instance v0, LX/0U1;

    const-string v1, "can_see_viewer_montage_thread"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->t:LX/0U1;

    .line 1116406
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_bot"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->u:LX/0U1;

    .line 1116407
    new-instance v0, LX/0U1;

    const-string v1, "is_vc_endpoint"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->v:LX/0U1;

    .line 1116408
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_promotion_blocked_by_viewer"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->w:LX/0U1;

    .line 1116409
    new-instance v0, LX/0U1;

    const-string v1, "user_custom_tags"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->x:LX/0U1;

    .line 1116410
    new-instance v0, LX/0U1;

    const-string v1, "is_receiving_subscription_messages"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->y:LX/0U1;

    .line 1116411
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_platform_bot"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->z:LX/0U1;

    .line 1116412
    new-instance v0, LX/0U1;

    const-string v1, "user_call_to_actions"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->A:LX/0U1;

    .line 1116413
    new-instance v0, LX/0U1;

    const-string v1, "structured_menu_call_to_actions"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->B:LX/0U1;

    .line 1116414
    new-instance v0, LX/0U1;

    const-string v1, "current_country_code"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->C:LX/0U1;

    .line 1116415
    new-instance v0, LX/0U1;

    const-string v1, "home_country_code"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->D:LX/0U1;

    .line 1116416
    new-instance v0, LX/0U1;

    const-string v1, "extension_resume_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->E:LX/0U1;

    .line 1116417
    new-instance v0, LX/0U1;

    const-string v1, "extension_resume_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->F:LX/0U1;

    .line 1116418
    new-instance v0, LX/0U1;

    const-string v1, "extension_payment_policy"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->G:LX/0U1;

    .line 1116419
    new-instance v0, LX/0U1;

    const-string v1, "structured_menu_badge_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->H:LX/0U1;

    .line 1116420
    new-instance v0, LX/0U1;

    const-string v1, "does_accept_user_feedback"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->I:LX/0U1;

    .line 1116421
    new-instance v0, LX/0U1;

    const-string v1, "extension_properties"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->J:LX/0U1;

    .line 1116422
    new-instance v0, LX/0U1;

    const-string v1, "viewer_connection_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6do;->K:LX/0U1;

    return-void
.end method
