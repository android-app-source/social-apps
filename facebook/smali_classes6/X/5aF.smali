.class public final LX/5aF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 954179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 954180
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 954181
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 954182
    const/4 v2, 0x0

    .line 954183
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 954184
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954185
    :goto_1
    move v1, v2

    .line 954186
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 954187
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 954188
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954189
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 954190
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 954191
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954192
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 954193
    const-string v7, "bot_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 954194
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 954195
    :cond_3
    const-string v7, "description"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 954196
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 954197
    :cond_4
    const-string v7, "icon"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 954198
    const/4 v6, 0x0

    .line 954199
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_b

    .line 954200
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954201
    :goto_3
    move v3, v6

    .line 954202
    goto :goto_2

    .line 954203
    :cond_5
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 954204
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 954205
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 954206
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 954207
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 954208
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 954209
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 954210
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_2

    .line 954211
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954212
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 954213
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 954214
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954215
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 954216
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 954217
    const/4 v7, 0x0

    .line 954218
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v8, :cond_f

    .line 954219
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954220
    :goto_5
    move v3, v7

    .line 954221
    goto :goto_4

    .line 954222
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 954223
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 954224
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_b
    move v3, v6

    goto :goto_4

    .line 954225
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954226
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 954227
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 954228
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954229
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 954230
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 954231
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_6

    .line 954232
    :cond_e
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 954233
    invoke-virtual {p1, v7, v3}, LX/186;->b(II)V

    .line 954234
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_f
    move v3, v7

    goto :goto_6
.end method
