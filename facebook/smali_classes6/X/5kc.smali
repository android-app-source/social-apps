.class public final LX/5kc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 995695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;
    .locals 13

    .prologue
    .line 995696
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 995697
    iget-object v1, p0, LX/5kc;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 995698
    iget-object v2, p0, LX/5kc;->j:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 995699
    iget-object v3, p0, LX/5kc;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 995700
    iget-object v4, p0, LX/5kc;->m:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 995701
    iget-object v5, p0, LX/5kc;->n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 995702
    iget-object v6, p0, LX/5kc;->o:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 995703
    iget-object v7, p0, LX/5kc;->p:LX/0Px;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 995704
    iget-object v8, p0, LX/5kc;->q:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 995705
    iget-object v9, p0, LX/5kc;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 995706
    iget-object v10, p0, LX/5kc;->s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 995707
    const/16 v11, 0x14

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 995708
    const/4 v11, 0x0

    iget-boolean v12, p0, LX/5kc;->a:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995709
    const/4 v11, 0x1

    iget-boolean v12, p0, LX/5kc;->b:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995710
    const/4 v11, 0x2

    iget-boolean v12, p0, LX/5kc;->c:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995711
    const/4 v11, 0x3

    iget-boolean v12, p0, LX/5kc;->d:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995712
    const/4 v11, 0x4

    iget-boolean v12, p0, LX/5kc;->e:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995713
    const/4 v11, 0x5

    iget-boolean v12, p0, LX/5kc;->f:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995714
    const/4 v11, 0x6

    iget-boolean v12, p0, LX/5kc;->g:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995715
    const/4 v11, 0x7

    iget-boolean v12, p0, LX/5kc;->h:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 995716
    const/16 v11, 0x8

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 995717
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 995718
    const/16 v1, 0xa

    iget-boolean v2, p0, LX/5kc;->k:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 995719
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 995720
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 995721
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 995722
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 995723
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 995724
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 995725
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 995726
    const/16 v1, 0x12

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 995727
    const/16 v1, 0x13

    iget v2, p0, LX/5kc;->t:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 995728
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 995729
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 995730
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 995731
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 995732
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 995733
    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;-><init>(LX/15i;)V

    .line 995734
    return-object v1
.end method
