.class public LX/6C4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6C3;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0So;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063401
    iput-object p1, p0, LX/6C4;->a:LX/0Zb;

    .line 1063402
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6C4;->c:Ljava/util/Map;

    .line 1063403
    sget-object v0, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v0, v0

    .line 1063404
    iput-object v0, p0, LX/6C4;->d:LX/0So;

    .line 1063405
    return-void
.end method

.method public static a(LX/0QB;)LX/6C4;
    .locals 2

    .prologue
    .line 1063406
    new-instance v1, LX/6C4;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/6C4;-><init>(LX/0Zb;)V

    .line 1063407
    move-object v0, v1

    .line 1063408
    return-object v0
.end method

.method public static a(LX/6C4;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/6C3;)V
    .locals 6

    .prologue
    .line 1063409
    const-string v0, "article_chaining_id"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063410
    const-string v0, "time_delta"

    iget-object v1, p0, LX/6C4;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p3, LX/6C3;->a:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063411
    const-string v0, "secondary_navigation"

    iget-boolean v1, p3, LX/6C3;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063412
    return-void
.end method

.method public static g(LX/6C4;)Z
    .locals 2

    .prologue
    .line 1063413
    iget-object v0, p0, LX/6C4;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6C4;->c:Ljava/util/Map;

    iget-object v1, p0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
