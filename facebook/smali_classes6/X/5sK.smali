.class public abstract LX/5sK;
.super LX/5sH;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1012751
    invoke-direct {p0}, LX/5sH;-><init>()V

    .line 1012752
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;IIII)Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 1012753
    invoke-virtual {p0}, LX/5sK;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 1012754
    :goto_0
    invoke-virtual {p0}, LX/5sK;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1012755
    :goto_1
    iget-object v0, p0, LX/5sH;->a:LX/5sI;

    if-eqz v0, :cond_2

    .line 1012756
    sget-object v0, LX/5sJ;->a:[I

    iget-object v3, p0, LX/5sH;->a:LX/5sI;

    invoke-virtual {v3}, LX/5sI;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1012757
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing animation for property : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5sH;->a:LX/5sI;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v2

    .line 1012758
    goto :goto_0

    :cond_1
    move v2, v0

    .line 1012759
    goto :goto_1

    .line 1012760
    :pswitch_0
    new-instance v0, LX/5sU;

    invoke-direct {v0, p1, v1, v2}, LX/5sU;-><init>(Landroid/view/View;FF)V

    .line 1012761
    :goto_2
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    goto :goto_2

    .line 1012762
    :cond_2
    new-instance v0, LX/5qo;

    const-string v1, "Missing animated property from animation config"

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1012763
    iget v0, p0, LX/5sH;->b:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/5sH;->a:LX/5sI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()Z
.end method
