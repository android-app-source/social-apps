.class public LX/6al;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/680;

.field public b:LX/7an;

.field private c:LX/6az;


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 0

    .prologue
    .line 1112611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112612
    iput-object p1, p0, LX/6al;->a:LX/680;

    .line 1112613
    return-void
.end method

.method public constructor <init>(LX/7an;)V
    .locals 0

    .prologue
    .line 1112608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112609
    iput-object p1, p0, LX/6al;->b:LX/7an;

    .line 1112610
    return-void
.end method


# virtual methods
.method public final a(LX/694;)LX/6aO;
    .locals 9

    .prologue
    .line 1112581
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_0

    .line 1112582
    new-instance v0, LX/6aO;

    iget-object v1, p0, LX/6al;->a:LX/680;

    .line 1112583
    new-instance v2, LX/693;

    invoke-direct {v2, v1, p1}, LX/693;-><init>(LX/680;LX/694;)V

    invoke-virtual {v1, v2}, LX/680;->a(LX/67m;)LX/67m;

    move-result-object v2

    check-cast v2, LX/693;

    move-object v1, v2

    .line 1112584
    invoke-direct {v0, v1}, LX/6aO;-><init>(LX/693;)V

    .line 1112585
    :goto_0
    return-object v0

    .line 1112586
    :cond_0
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_1

    .line 1112587
    new-instance v0, LX/6aO;

    iget-object v1, p0, LX/6al;->b:LX/7an;

    .line 1112588
    if-nez p1, :cond_2

    .line 1112589
    const/4 v3, 0x0

    .line 1112590
    :goto_1
    move-object v2, v3

    .line 1112591
    invoke-virtual {v1, v2}, LX/7an;->a(Lcom/google/android/gms/maps/model/CircleOptions;)LX/7c8;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6aO;-><init>(LX/7c8;)V

    goto :goto_0

    .line 1112592
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    .line 1112593
    iget-object v4, p1, LX/694;->a:Lcom/facebook/android/maps/model/LatLng;

    move-object v4, v4

    .line 1112594
    invoke-static {v4}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->b:Lcom/google/android/gms/maps/model/LatLng;

    move-object v3, v3

    .line 1112595
    iget v4, p1, LX/694;->b:I

    move v4, v4

    .line 1112596
    iput v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->f:I

    move-object v3, v3

    .line 1112597
    iget-wide v7, p1, LX/694;->c:D

    move-wide v5, v7

    .line 1112598
    iput-wide v5, v3, Lcom/google/android/gms/maps/model/CircleOptions;->c:D

    move-object v3, v3

    .line 1112599
    iget v4, p1, LX/694;->d:I

    move v4, v4

    .line 1112600
    iput v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->e:I

    move-object v3, v3

    .line 1112601
    iget v4, p1, LX/694;->e:F

    move v4, v4

    .line 1112602
    iput v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->d:F

    move-object v3, v3

    .line 1112603
    iget-boolean v4, p1, LX/694;->f:Z

    move v4, v4

    .line 1112604
    iput-boolean v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->h:Z

    move-object v3, v3

    .line 1112605
    iget v4, p1, LX/694;->g:F

    move v4, v4

    .line 1112606
    iput v4, v3, Lcom/google/android/gms/maps/model/CircleOptions;->g:F

    move-object v3, v3

    .line 1112607
    goto :goto_1
.end method

.method public final a(LX/699;)LX/6ax;
    .locals 3

    .prologue
    .line 1112550
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_0

    .line 1112551
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0, p1}, LX/680;->a(LX/699;)LX/698;

    move-result-object v0

    invoke-static {v0}, LX/6ax;->a(LX/698;)LX/6ax;

    move-result-object v0

    .line 1112552
    :goto_0
    return-object v0

    .line 1112553
    :cond_0
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_1

    .line 1112554
    iget-object v0, p0, LX/6al;->b:LX/7an;

    .line 1112555
    if-nez p1, :cond_2

    .line 1112556
    const/4 v1, 0x0

    .line 1112557
    :goto_1
    move-object v1, v1

    .line 1112558
    invoke-virtual {v0, v1}, LX/7an;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)LX/7cA;

    move-result-object v0

    invoke-static {v0}, LX/6ax;->a(LX/7cA;)LX/6ax;

    move-result-object v0

    goto :goto_0

    .line 1112559
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 1112560
    iget v2, p1, LX/699;->d:F

    move v2, v2

    .line 1112561
    iput v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->n:F

    move-object v1, v1

    .line 1112562
    invoke-virtual {p1}, LX/699;->b()F

    move-result v2

    invoke-virtual {p1}, LX/699;->c()F

    move-result p0

    iput v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->f:F

    iput p0, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->g:F

    move-object v1, v1

    .line 1112563
    iget-boolean v2, p1, LX/699;->e:Z

    move v2, v2

    .line 1112564
    iput-boolean v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->h:Z

    move-object v1, v1

    .line 1112565
    iget-boolean v2, p1, LX/699;->f:Z

    move v2, v2

    .line 1112566
    iput-boolean v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->j:Z

    move-object v1, v1

    .line 1112567
    iget-object v2, p1, LX/699;->c:LX/68w;

    move-object v2, v2

    .line 1112568
    invoke-static {v2}, LX/6as;->a(LX/68w;)LX/7c6;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->e:LX/7c6;

    move-object v1, v1

    .line 1112569
    invoke-virtual {p1}, LX/699;->e()F

    move-result v2

    invoke-virtual {p1}, LX/699;->f()F

    move-result p0

    iput v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->l:F

    iput p0, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->m:F

    move-object v1, v1

    .line 1112570
    iget-object v2, p1, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    move-object v2, v2

    .line 1112571
    invoke-static {v2}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 1112572
    iget v2, p1, LX/699;->g:F

    move v2, v2

    .line 1112573
    iput v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->k:F

    move-object v1, v1

    .line 1112574
    iget-object v2, p1, LX/699;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1112575
    iput-object v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1112576
    iget-object v2, p1, LX/699;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1112577
    iput-object v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1112578
    iget-boolean v2, p1, LX/699;->k:Z

    move v2, v2

    .line 1112579
    iput-boolean v2, v1, Lcom/google/android/gms/maps/model/MarkerOptions;->i:Z

    move-object v1, v1

    .line 1112580
    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1112545
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112546
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0}, LX/680;->b()V

    .line 1112547
    :cond_0
    :goto_0
    return-void

    .line 1112548
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112549
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v0}, LX/7an;->b()V

    goto :goto_0
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 1112540
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112541
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/680;->a(IIII)V

    .line 1112542
    :cond_0
    :goto_0
    return-void

    .line 1112543
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112544
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/7an;->a(IIII)V

    goto :goto_0
.end method

.method public final a(LX/67n;)V
    .locals 2

    .prologue
    .line 1112534
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112535
    iget-object v0, p0, LX/6al;->a:LX/680;

    .line 1112536
    iput-object p1, v0, LX/680;->g:LX/67n;

    .line 1112537
    :cond_0
    :goto_0
    return-void

    .line 1112538
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112539
    iget-object v0, p0, LX/6al;->b:LX/7an;

    new-instance v1, LX/6ah;

    invoke-direct {v1, p0, p1}, LX/6ah;-><init>(LX/6al;LX/67n;)V

    invoke-virtual {v0, v1}, LX/7an;->a(LX/6ag;)V

    goto :goto_0
.end method

.method public final a(LX/6Zu;)V
    .locals 2

    .prologue
    .line 1112462
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112463
    iget-object v0, p0, LX/6al;->a:LX/680;

    new-instance v1, LX/6ad;

    invoke-direct {v1, p0, p1}, LX/6ad;-><init>(LX/6al;LX/6Zu;)V

    .line 1112464
    iput-object v1, v0, LX/680;->p:LX/6ad;

    .line 1112465
    :cond_0
    :goto_0
    return-void

    .line 1112466
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112467
    iget-object v0, p0, LX/6al;->b:LX/7an;

    new-instance v1, LX/6af;

    invoke-direct {v1, p0, p1}, LX/6af;-><init>(LX/6al;LX/6Zu;)V

    invoke-virtual {v0, v1}, LX/7an;->a(LX/6ae;)V

    goto :goto_0
.end method

.method public final a(LX/6a3;)V
    .locals 2

    .prologue
    .line 1112520
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112521
    iget-object v0, p0, LX/6al;->a:LX/680;

    new-instance v1, LX/6aY;

    invoke-direct {v1, p0, p1}, LX/6aY;-><init>(LX/6al;LX/6a3;)V

    .line 1112522
    iput-object v1, v0, LX/680;->O:LX/67t;

    .line 1112523
    iget-object p0, v0, LX/680;->O:LX/67t;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/680;->j:LX/3BW;

    .line 1112524
    iget-object p1, p0, LX/3BW;->d:Landroid/location/Location;

    move-object p0, p1

    .line 1112525
    if-eqz p0, :cond_0

    iget-object p0, v0, LX/680;->j:LX/3BW;

    .line 1112526
    iget-boolean p1, p0, LX/3BW;->e:Z

    move p0, p1

    .line 1112527
    if-eqz p0, :cond_0

    .line 1112528
    iget-object p0, v0, LX/680;->O:LX/67t;

    iget-object p1, v0, LX/680;->j:LX/3BW;

    .line 1112529
    iget-object v0, p1, LX/3BW;->d:Landroid/location/Location;

    move-object p1, v0

    .line 1112530
    invoke-interface {p0, p1}, LX/67t;->a(Landroid/location/Location;)V

    .line 1112531
    :cond_0
    :goto_0
    return-void

    .line 1112532
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112533
    iget-object v0, p0, LX/6al;->b:LX/7an;

    new-instance v1, LX/6ac;

    invoke-direct {v1, p0, p1}, LX/6ac;-><init>(LX/6al;LX/6a3;)V

    invoke-virtual {v0, v1}, LX/7an;->a(LX/6ab;)V

    goto :goto_0
.end method

.method public final a(LX/6aM;)V
    .locals 2

    .prologue
    .line 1112515
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112516
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {p1}, LX/6aM;->a()LX/67d;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/680;->b(LX/67d;)V

    .line 1112517
    :cond_0
    :goto_0
    return-void

    .line 1112518
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112519
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {p1}, LX/6aM;->b()LX/7aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7an;->a(LX/7aQ;)V

    goto :goto_0
.end method

.method public final a(LX/6aM;ILX/6aj;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1112506
    iget-object v1, p0, LX/6al;->a:LX/680;

    if-eqz v1, :cond_2

    .line 1112507
    if-eqz p3, :cond_0

    .line 1112508
    new-instance v0, LX/6aX;

    invoke-direct {v0, p0, p3}, LX/6aX;-><init>(LX/6al;LX/6aj;)V

    .line 1112509
    :cond_0
    iget-object v1, p0, LX/6al;->a:LX/680;

    invoke-virtual {p1}, LX/6aM;->a()LX/67d;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1112510
    :cond_1
    :goto_0
    return-void

    .line 1112511
    :cond_2
    iget-object v1, p0, LX/6al;->b:LX/7an;

    if-eqz v1, :cond_1

    .line 1112512
    if-eqz p3, :cond_3

    .line 1112513
    new-instance v0, LX/6aa;

    invoke-direct {v0, p0, p3}, LX/6aa;-><init>(LX/6al;LX/6aj;)V

    .line 1112514
    :cond_3
    iget-object v1, p0, LX/6al;->b:LX/7an;

    invoke-virtual {p1}, LX/6aM;->b()LX/7aQ;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, LX/7an;->a(LX/7aQ;ILX/6aZ;)V

    goto :goto_0
.end method

.method public final a(LX/6ak;)V
    .locals 2

    .prologue
    .line 1112500
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112501
    iget-object v0, p0, LX/6al;->a:LX/680;

    new-instance v1, LX/6ai;

    invoke-direct {v1, p0, p1}, LX/6ai;-><init>(LX/6al;LX/6ak;)V

    .line 1112502
    iput-object v1, v0, LX/680;->m:LX/67y;

    .line 1112503
    :cond_0
    :goto_0
    return-void

    .line 1112504
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112505
    iget-object v0, p0, LX/6al;->b:LX/7an;

    new-instance v1, LX/6aQ;

    invoke-direct {v1, p0, p1}, LX/6aQ;-><init>(LX/6al;LX/6ak;)V

    invoke-virtual {v0, v1}, LX/7an;->a(LX/6aP;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1112495
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    .line 1112496
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0, p1}, LX/680;->a(Z)V

    .line 1112497
    :cond_0
    :goto_0
    return-void

    .line 1112498
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    .line 1112499
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v0, p1}, LX/7an;->a(Z)V

    goto :goto_0
.end method

.method public final b()LX/6ay;
    .locals 2

    .prologue
    .line 1112487
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_0

    .line 1112488
    new-instance v0, LX/6ay;

    iget-object v1, p0, LX/6al;->a:LX/680;

    .line 1112489
    iget-object p0, v1, LX/680;->k:LX/31h;

    move-object v1, p0

    .line 1112490
    invoke-direct {v0, v1}, LX/6ay;-><init>(LX/31h;)V

    .line 1112491
    :goto_0
    return-object v0

    .line 1112492
    :cond_0
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_1

    .line 1112493
    new-instance v0, LX/6ay;

    iget-object v1, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v1}, LX/7an;->e()LX/7aw;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6ay;-><init>(LX/7aw;)V

    goto :goto_0

    .line 1112494
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/6az;
    .locals 3

    .prologue
    .line 1112480
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6al;->c:LX/6az;

    if-nez v0, :cond_1

    .line 1112481
    new-instance v0, LX/6az;

    iget-object v1, p0, LX/6al;->a:LX/680;

    .line 1112482
    iget-object v2, v1, LX/680;->E:LX/68Q;

    move-object v1, v2

    .line 1112483
    invoke-direct {v0, v1}, LX/6az;-><init>(LX/68Q;)V

    iput-object v0, p0, LX/6al;->c:LX/6az;

    .line 1112484
    :cond_0
    :goto_0
    iget-object v0, p0, LX/6al;->c:LX/6az;

    return-object v0

    .line 1112485
    :cond_1
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6al;->c:LX/6az;

    if-nez v0, :cond_0

    .line 1112486
    new-instance v0, LX/6az;

    iget-object v1, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v1}, LX/7an;->d()LX/7ax;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6az;-><init>(LX/7ax;)V

    iput-object v0, p0, LX/6al;->c:LX/6az;

    goto :goto_0
.end method

.method public final d()Landroid/location/Location;
    .locals 1

    .prologue
    .line 1112474
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_0

    .line 1112475
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0}, LX/680;->e()Landroid/location/Location;

    move-result-object v0

    .line 1112476
    :goto_0
    return-object v0

    .line 1112477
    :cond_0
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_1

    .line 1112478
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v0}, LX/7an;->c()Landroid/location/Location;

    move-result-object v0

    goto :goto_0

    .line 1112479
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/692;
    .locals 1

    .prologue
    .line 1112468
    iget-object v0, p0, LX/6al;->a:LX/680;

    if-eqz v0, :cond_0

    .line 1112469
    iget-object v0, p0, LX/6al;->a:LX/680;

    invoke-virtual {v0}, LX/680;->c()LX/692;

    move-result-object v0

    .line 1112470
    :goto_0
    return-object v0

    .line 1112471
    :cond_0
    iget-object v0, p0, LX/6al;->b:LX/7an;

    if-eqz v0, :cond_1

    .line 1112472
    iget-object v0, p0, LX/6al;->b:LX/7an;

    invoke-virtual {v0}, LX/7an;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, LX/6as;->a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/692;

    move-result-object v0

    goto :goto_0

    .line 1112473
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
