.class public final LX/5np;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004726
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1004727
    check-cast p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;

    .line 1004728
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1004729
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004730
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "client_time"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004731
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "product"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004732
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1004733
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "from_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004734
    :cond_0
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1004735
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "to_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004736
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004737
    new-instance v0, LX/14N;

    const-string v1, "reportStickyUpsellAction"

    const-string v2, "POST"

    const-string v3, "me/sticky_upsell_events"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004738
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004739
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
