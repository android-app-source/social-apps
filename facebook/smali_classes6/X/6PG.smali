.class public final LX/6PG;
.super LX/3dG;
.source ""


# instance fields
.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic e:LX/3iV;


# direct methods
.method public varargs constructor <init>(LX/3iV;LX/2lk;[Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 1086101
    iput-object p1, p0, LX/6PG;->e:LX/3iV;

    iput-object p4, p0, LX/6PG;->b:Ljava/lang/String;

    iput-boolean p5, p0, LX/6PG;->c:Z

    iput-object p6, p0, LX/6PG;->d:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0, p2, p3}, LX/3dG;-><init>(LX/2lk;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 1086102
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6PG;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6PG;->c:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1086103
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v0, p0, LX/6PG;->e:LX/3iV;

    iget-object v0, v0, LX/3iV;->c:LX/20j;

    iget-object v1, p0, LX/6PG;->d:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0, p1, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086104
    const-string v0, "FeedbackGraphQLGenerator.toggleLikeVisitor"

    return-object v0
.end method
