.class public LX/6cm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Mk;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2No;


# direct methods
.method public constructor <init>(LX/2Mk;LX/0Or;LX/2No;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Mk;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2No;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114605
    iput-object p1, p0, LX/6cm;->a:LX/2Mk;

    .line 1114606
    iput-object p2, p0, LX/6cm;->b:LX/0Or;

    .line 1114607
    iput-object p3, p0, LX/6cm;->c:LX/2No;

    .line 1114608
    return-void
.end method

.method private static a(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 1114609
    if-nez p0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1114580
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6cm;
    .locals 4

    .prologue
    .line 1114602
    new-instance v2, LX/6cm;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v0

    check-cast v0, LX/2Mk;

    const/16 v1, 0x15e8

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/2No;->b(LX/0QB;)LX/2No;

    move-result-object v1

    check-cast v1, LX/2No;

    invoke-direct {v2, v0, v3, v1}, LX/6cm;-><init>(LX/2Mk;LX/0Or;LX/2No;)V

    .line 1114603
    return-object v2
.end method

.method private static c(Lcom/facebook/messaging/model/messages/Message;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1114594
    invoke-static {p0}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1114595
    const/4 v0, -0x1

    .line 1114596
    :goto_0
    return v0

    .line 1114597
    :cond_0
    iget-object v3, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 1114598
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1114599
    int-to-long v6, v1

    iget-wide v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    add-long/2addr v0, v6

    long-to-int v1, v0

    .line 1114600
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1114601
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;
    .locals 8
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1114581
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1114582
    :cond_0
    const/4 v0, 0x0

    .line 1114583
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "timeStampMs"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "thread_key"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "msgType"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sentTimestampMs"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v2, v3}, LX/6cm;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "senderInfo"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "actionId"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "attachments.count"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-static {v2}, LX/6cm;->a(Ljava/util/List;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "shares.count"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-static {v2}, LX/6cm;->a(Ljava/util/List;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "offlineThreadingId"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isNonAuthoritative"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "source"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "channelSource"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v2}, LX/6f2;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sendChannel"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    invoke-virtual {v2}, LX/6f3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sentByUser"

    .line 1114584
    iget-object v2, p0, LX/6cm;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 1114585
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "sentByDevice"

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sendError.type"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v2, v2, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sendError.errorMessage"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sendError.errorNumber"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget v2, v2, Lcom/facebook/messaging/model/send/SendError;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "sendError.timeStamp"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-wide v2, v2, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-static {v2, v3}, LX/6cm;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "mediaAttachments.type"

    .line 1114586
    invoke-static {p1}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1114587
    const-string v2, "none"

    .line 1114588
    :goto_2
    move-object v2, v2

    .line 1114589
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "mediaAttachments.totalSize"

    invoke-static {p1}, LX/6cm;->c(Lcom/facebook/messaging/model/messages/Message;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "mediaAttachments.count"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-static {v2}, LX/6cm;->a(Ljava/util/List;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "hasUnavailableAttachment"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->D:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "publicity"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1114590
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1114591
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "clientTags"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-static {v2}, LX/2No;->a(LX/0P1;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "sendQueueType"

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "sentShareAttachments.type"

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_4
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "composerAppAttribution.appId"

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_5
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "contentAppAttribution.appId"

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_6
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "montage_reply_message_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    iget-object v0, v0, LX/6fM;->serializedValue:Ljava/lang/String;

    goto :goto_3

    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    iget-object v0, v0, LX/6fR;->DBSerialValue:Ljava/lang/String;

    goto :goto_4

    :cond_5
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_6
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v0, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    goto :goto_6

    .line 1114592
    :cond_7
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 1114593
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2
.end method
