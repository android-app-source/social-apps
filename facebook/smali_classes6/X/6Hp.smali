.class public final enum LX/6Hp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Hp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Hp;

.field public static final enum EXTERNAL:LX/6Hp;

.field public static final enum INTERNAL:LX/6Hp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1072831
    new-instance v0, LX/6Hp;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v2}, LX/6Hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Hp;->INTERNAL:LX/6Hp;

    .line 1072832
    new-instance v0, LX/6Hp;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v3}, LX/6Hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Hp;->EXTERNAL:LX/6Hp;

    .line 1072833
    const/4 v0, 0x2

    new-array v0, v0, [LX/6Hp;

    sget-object v1, LX/6Hp;->INTERNAL:LX/6Hp;

    aput-object v1, v0, v2

    sget-object v1, LX/6Hp;->EXTERNAL:LX/6Hp;

    aput-object v1, v0, v3

    sput-object v0, LX/6Hp;->$VALUES:[LX/6Hp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1072830
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Hp;
    .locals 1

    .prologue
    .line 1072834
    const-class v0, LX/6Hp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Hp;

    return-object v0
.end method

.method public static values()[LX/6Hp;
    .locals 1

    .prologue
    .line 1072829
    sget-object v0, LX/6Hp;->$VALUES:[LX/6Hp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Hp;

    return-object v0
.end method
