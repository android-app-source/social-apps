.class public final LX/5BM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 48

    .prologue
    .line 863247
    const/16 v44, 0x0

    .line 863248
    const/16 v43, 0x0

    .line 863249
    const/16 v42, 0x0

    .line 863250
    const/16 v41, 0x0

    .line 863251
    const/16 v40, 0x0

    .line 863252
    const/16 v39, 0x0

    .line 863253
    const/16 v38, 0x0

    .line 863254
    const/16 v37, 0x0

    .line 863255
    const/16 v36, 0x0

    .line 863256
    const/16 v35, 0x0

    .line 863257
    const/16 v34, 0x0

    .line 863258
    const/16 v33, 0x0

    .line 863259
    const/16 v32, 0x0

    .line 863260
    const/16 v31, 0x0

    .line 863261
    const/16 v30, 0x0

    .line 863262
    const/16 v29, 0x0

    .line 863263
    const/16 v28, 0x0

    .line 863264
    const/16 v27, 0x0

    .line 863265
    const/16 v26, 0x0

    .line 863266
    const/16 v25, 0x0

    .line 863267
    const/16 v24, 0x0

    .line 863268
    const/16 v23, 0x0

    .line 863269
    const/16 v22, 0x0

    .line 863270
    const/16 v21, 0x0

    .line 863271
    const/16 v20, 0x0

    .line 863272
    const/16 v19, 0x0

    .line 863273
    const/16 v18, 0x0

    .line 863274
    const/16 v17, 0x0

    .line 863275
    const/16 v16, 0x0

    .line 863276
    const/4 v15, 0x0

    .line 863277
    const/4 v14, 0x0

    .line 863278
    const/4 v13, 0x0

    .line 863279
    const/4 v12, 0x0

    .line 863280
    const/4 v11, 0x0

    .line 863281
    const/4 v10, 0x0

    .line 863282
    const/4 v9, 0x0

    .line 863283
    const/4 v8, 0x0

    .line 863284
    const/4 v7, 0x0

    .line 863285
    const/4 v6, 0x0

    .line 863286
    const/4 v5, 0x0

    .line 863287
    const/4 v4, 0x0

    .line 863288
    const/4 v3, 0x0

    .line 863289
    const/4 v2, 0x0

    .line 863290
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1

    .line 863291
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 863292
    const/4 v2, 0x0

    .line 863293
    :goto_0
    return v2

    .line 863294
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 863295
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1f

    .line 863296
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v45

    .line 863297
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 863298
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    if-eqz v45, :cond_1

    .line 863299
    const-string v46, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_2

    .line 863300
    const/4 v14, 0x1

    .line 863301
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v44

    goto :goto_1

    .line 863302
    :cond_2
    const-string v46, "can_see_voice_switcher"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_3

    .line 863303
    const/4 v13, 0x1

    .line 863304
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v43

    goto :goto_1

    .line 863305
    :cond_3
    const-string v46, "can_viewer_comment"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_4

    .line 863306
    const/4 v12, 0x1

    .line 863307
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 863308
    :cond_4
    const-string v46, "can_viewer_comment_with_photo"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_5

    .line 863309
    const/4 v11, 0x1

    .line 863310
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 863311
    :cond_5
    const-string v46, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_6

    .line 863312
    const/4 v10, 0x1

    .line 863313
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 863314
    :cond_6
    const-string v46, "can_viewer_comment_with_video"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_7

    .line 863315
    const/4 v9, 0x1

    .line 863316
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 863317
    :cond_7
    const-string v46, "can_viewer_like"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_8

    .line 863318
    const/4 v8, 0x1

    .line 863319
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto/16 :goto_1

    .line 863320
    :cond_8
    const-string v46, "can_viewer_react"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_9

    .line 863321
    const/4 v7, 0x1

    .line 863322
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 863323
    :cond_9
    const-string v46, "can_viewer_subscribe"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_a

    .line 863324
    const/4 v6, 0x1

    .line 863325
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 863326
    :cond_a
    const-string v46, "comments_disabled_notice"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_b

    .line 863327
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v35

    goto/16 :goto_1

    .line 863328
    :cond_b
    const-string v46, "comments_mirroring_domain"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_c

    .line 863329
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    goto/16 :goto_1

    .line 863330
    :cond_c
    const-string v46, "does_viewer_like"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_d

    .line 863331
    const/4 v5, 0x1

    .line 863332
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 863333
    :cond_d
    const-string v46, "have_comments_been_disabled"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_e

    .line 863334
    const/4 v4, 0x1

    .line 863335
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 863336
    :cond_e
    const-string v46, "id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_f

    .line 863337
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 863338
    :cond_f
    const-string v46, "important_reactors"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_10

    .line 863339
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 863340
    :cond_10
    const-string v46, "is_viewer_subscribed"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_11

    .line 863341
    const/4 v3, 0x1

    .line 863342
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 863343
    :cond_11
    const-string v46, "legacy_api_post_id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_12

    .line 863344
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 863345
    :cond_12
    const-string v46, "like_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_13

    .line 863346
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 863347
    :cond_13
    const-string v46, "likers"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_14

    .line 863348
    invoke-static/range {p0 .. p1}, LX/5BJ;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 863349
    :cond_14
    const-string v46, "reactors"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_15

    .line 863350
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 863351
    :cond_15
    const-string v46, "remixable_photo_uri"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_16

    .line 863352
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 863353
    :cond_16
    const-string v46, "seen_by"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_17

    .line 863354
    invoke-static/range {p0 .. p1}, LX/5BK;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 863355
    :cond_17
    const-string v46, "supported_reactions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_18

    .line 863356
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 863357
    :cond_18
    const-string v46, "top_level_comments"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_19

    .line 863358
    invoke-static/range {p0 .. p1}, LX/59L;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 863359
    :cond_19
    const-string v46, "top_reactions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1a

    .line 863360
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 863361
    :cond_1a
    const-string v46, "viewer_acts_as_page"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1b

    .line 863362
    invoke-static/range {p0 .. p1}, LX/5BL;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 863363
    :cond_1b
    const-string v46, "viewer_acts_as_person"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1c

    .line 863364
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 863365
    :cond_1c
    const-string v46, "viewer_does_not_like_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1d

    .line 863366
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 863367
    :cond_1d
    const-string v46, "viewer_feedback_reaction_key"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1e

    .line 863368
    const/4 v2, 0x1

    .line 863369
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 863370
    :cond_1e
    const-string v46, "viewer_likes_sentence"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_0

    .line 863371
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 863372
    :cond_1f
    const/16 v45, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 863373
    if-eqz v14, :cond_20

    .line 863374
    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 863375
    :cond_20
    if-eqz v13, :cond_21

    .line 863376
    const/4 v13, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 863377
    :cond_21
    if-eqz v12, :cond_22

    .line 863378
    const/4 v12, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 863379
    :cond_22
    if-eqz v11, :cond_23

    .line 863380
    const/4 v11, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 863381
    :cond_23
    if-eqz v10, :cond_24

    .line 863382
    const/4 v10, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 863383
    :cond_24
    if-eqz v9, :cond_25

    .line 863384
    const/4 v9, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 863385
    :cond_25
    if-eqz v8, :cond_26

    .line 863386
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 863387
    :cond_26
    if-eqz v7, :cond_27

    .line 863388
    const/4 v7, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 863389
    :cond_27
    if-eqz v6, :cond_28

    .line 863390
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 863391
    :cond_28
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 863392
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 863393
    if-eqz v5, :cond_29

    .line 863394
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 863395
    :cond_29
    if-eqz v4, :cond_2a

    .line 863396
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 863397
    :cond_2a
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 863398
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 863399
    if-eqz v3, :cond_2b

    .line 863400
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 863401
    :cond_2b
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863402
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863403
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863404
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863405
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863406
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863407
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863408
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863409
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863410
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863411
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863412
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 863413
    if-eqz v2, :cond_2c

    .line 863414
    const/16 v2, 0x1c

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 863415
    :cond_2c
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 863416
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 863417
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 863418
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 863419
    if-eqz v0, :cond_0

    .line 863420
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863421
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863422
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863423
    if-eqz v0, :cond_1

    .line 863424
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863425
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863426
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863427
    if-eqz v0, :cond_2

    .line 863428
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863429
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863430
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863431
    if-eqz v0, :cond_3

    .line 863432
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863433
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863434
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863435
    if-eqz v0, :cond_4

    .line 863436
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863437
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863438
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863439
    if-eqz v0, :cond_5

    .line 863440
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863441
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863442
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863443
    if-eqz v0, :cond_6

    .line 863444
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863445
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863446
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863447
    if-eqz v0, :cond_7

    .line 863448
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863449
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863450
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863451
    if-eqz v0, :cond_8

    .line 863452
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863453
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863454
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863455
    if-eqz v0, :cond_9

    .line 863456
    const-string v1, "comments_disabled_notice"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863457
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 863458
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 863459
    if-eqz v0, :cond_a

    .line 863460
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863461
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863462
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863463
    if-eqz v0, :cond_b

    .line 863464
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863465
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863466
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863467
    if-eqz v0, :cond_c

    .line 863468
    const-string v1, "have_comments_been_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863469
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863470
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 863471
    if-eqz v0, :cond_d

    .line 863472
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863474
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863475
    if-eqz v0, :cond_e

    .line 863476
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863477
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863478
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 863479
    if-eqz v0, :cond_f

    .line 863480
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863481
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 863482
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 863483
    if-eqz v0, :cond_10

    .line 863484
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863485
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863486
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863487
    if-eqz v0, :cond_11

    .line 863488
    const-string v1, "like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863489
    invoke-static {p0, v0, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863490
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863491
    if-eqz v0, :cond_12

    .line 863492
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863493
    invoke-static {p0, v0, p2}, LX/5BJ;->a(LX/15i;ILX/0nX;)V

    .line 863494
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863495
    if-eqz v0, :cond_13

    .line 863496
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863497
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 863498
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 863499
    if-eqz v0, :cond_14

    .line 863500
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863501
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863502
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863503
    if-eqz v0, :cond_15

    .line 863504
    const-string v1, "seen_by"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863505
    invoke-static {p0, v0, p2}, LX/5BK;->a(LX/15i;ILX/0nX;)V

    .line 863506
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863507
    if-eqz v0, :cond_16

    .line 863508
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863509
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863510
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863511
    if-eqz v0, :cond_17

    .line 863512
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863513
    invoke-static {p0, v0, p2, p3}, LX/59L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863514
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863515
    if-eqz v0, :cond_18

    .line 863516
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863517
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863518
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863519
    if-eqz v0, :cond_19

    .line 863520
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863521
    invoke-static {p0, v0, p2, p3}, LX/5BL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863522
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863523
    if-eqz v0, :cond_1a

    .line 863524
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863525
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 863526
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863527
    if-eqz v0, :cond_1b

    .line 863528
    const-string v1, "viewer_does_not_like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863529
    invoke-static {p0, v0, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863530
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 863531
    if-eqz v0, :cond_1c

    .line 863532
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863533
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 863534
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 863535
    if-eqz v0, :cond_1d

    .line 863536
    const-string v1, "viewer_likes_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863537
    invoke-static {p0, v0, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863538
    :cond_1d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 863539
    return-void
.end method
