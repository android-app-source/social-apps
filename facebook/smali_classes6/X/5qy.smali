.class public LX/5qy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5rl;

.field private final b:LX/5rF;

.field private final c:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(LX/5rl;LX/5rF;)V
    .locals 1

    .prologue
    .line 1010694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010695
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    .line 1010696
    iput-object p1, p0, LX/5qy;->a:LX/5rl;

    .line 1010697
    iput-object p2, p0, LX/5qy;->b:LX/5rF;

    .line 1010698
    return-void
.end method

.method private static a(Lcom/facebook/react/uimanager/ReactShadowNode;I)LX/5qx;
    .locals 2

    .prologue
    .line 1010755
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v0, v0

    .line 1010756
    if-eqz v0, :cond_1

    .line 1010757
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1010758
    if-nez v0, :cond_0

    .line 1010759
    const/4 v0, 0x0

    .line 1010760
    :goto_1
    return-object v0

    .line 1010761
    :cond_0
    invoke-virtual {v0, p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(Lcom/facebook/react/uimanager/ReactShadowNode;)I

    move-result v1

    add-int/2addr p1, v1

    move-object p0, v0

    .line 1010762
    goto :goto_0

    .line 1010763
    :cond_1
    new-instance v0, LX/5qx;

    invoke-direct {v0, p0, p1}, LX/5qx;-><init>(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;II)V
    .locals 7

    .prologue
    .line 1010734
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v0, v0

    .line 1010735
    if-nez v0, :cond_1

    .line 1010736
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1010737
    if-eqz v0, :cond_1

    .line 1010738
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v0

    .line 1010739
    iget-object v0, p0, LX/5qy;->a:LX/5rl;

    .line 1010740
    iget-object v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v1, v1

    .line 1010741
    iget v3, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v3

    .line 1010742
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->D()I

    move-result v5

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->E()I

    move-result v6

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/5rl;->a(IIIIII)V

    .line 1010743
    :cond_0
    return-void

    .line 1010744
    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1010745
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1010746
    iget v2, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v2

    .line 1010747
    iget-object v3, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1010748
    iget-object v3, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1010749
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->B()I

    move-result v2

    .line 1010750
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->C()I

    move-result v3

    .line 1010751
    add-int/2addr v2, p2

    .line 1010752
    add-int/2addr v3, p3

    .line 1010753
    invoke-direct {p0, v1, v2, v3}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;II)V

    .line 1010754
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V
    .locals 7
    .param p2    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1010711
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1010712
    if-nez v0, :cond_0

    .line 1010713
    invoke-virtual {p1, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Z)V

    .line 1010714
    :goto_0
    return-void

    .line 1010715
    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;)I

    move-result v2

    .line 1010716
    invoke-virtual {v0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1010717
    invoke-direct {p0, p1, v1}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Z)V

    .line 1010718
    invoke-virtual {p1, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Z)V

    .line 1010719
    iget-object v3, p0, LX/5qy;->a:LX/5rl;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->m()Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v4

    .line 1010720
    iget v5, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v5

    .line 1010721
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, p2}, LX/5rl;->a(LX/5rJ;ILjava/lang/String;LX/5rC;)V

    .line 1010722
    invoke-virtual {v0, p1, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010723
    invoke-direct {p0, v0, p1, v2}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    move v0, v1

    .line 1010724
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1010725
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    invoke-direct {p0, p1, v2, v0}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010726
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1010727
    :cond_1
    iget-object v0, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1010728
    invoke-static {p0, p1}, LX/5qy;->c(LX/5qy;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1010729
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1010730
    invoke-virtual {p1, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    invoke-static {p0, v0}, LX/5qy;->c(LX/5qy;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1010731
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v0, v1

    .line 1010732
    goto :goto_2

    .line 1010733
    :cond_3
    iget-object v0, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    goto :goto_0
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 2

    .prologue
    .line 1010699
    invoke-virtual {p1, p3}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(Lcom/facebook/react/uimanager/ReactShadowNode;)I

    move-result v0

    .line 1010700
    iget-boolean v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v1, v1

    .line 1010701
    if-eqz v1, :cond_1

    .line 1010702
    invoke-static {p1, v0}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)LX/5qx;

    move-result-object v0

    .line 1010703
    if-nez v0, :cond_0

    .line 1010704
    :goto_0
    return-void

    .line 1010705
    :cond_0
    iget-object p1, v0, LX/5qx;->a:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1010706
    iget v0, v0, LX/5qx;->b:I

    .line 1010707
    :cond_1
    iget-boolean v1, p2, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v1, v1

    .line 1010708
    if-nez v1, :cond_2

    .line 1010709
    invoke-direct {p0, p1, p2, v0}, LX/5qy;->c(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    goto :goto_0

    .line 1010710
    :cond_2
    invoke-direct {p0, p1, p2, v0}, LX/5qy;->d(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1010680
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1010681
    if-eqz v0, :cond_2

    .line 1010682
    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(Lcom/facebook/react/uimanager/ReactShadowNode;)I

    move-result v2

    .line 1010683
    invoke-virtual {v0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1010684
    iget-object v3, p0, LX/5qy;->a:LX/5rl;

    .line 1010685
    iget v4, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v4, v4

    .line 1010686
    new-array v5, v7, [I

    aput v2, v5, v6

    if-eqz p2, :cond_1

    new-array v0, v7, [I

    .line 1010687
    iget v2, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v2

    .line 1010688
    aput v2, v0, v6

    :goto_0
    invoke-virtual {v3, v4, v5, v1, v0}, LX/5rl;->a(I[I[LX/5rn;[I)V

    .line 1010689
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    .line 1010690
    goto :goto_0

    .line 1010691
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_0

    .line 1010692
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    invoke-direct {p0, v1, p2}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Z)V

    .line 1010693
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private static a(LX/5rC;)Z
    .locals 4
    .param p0    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1010672
    if-nez p0, :cond_1

    .line 1010673
    :cond_0
    :goto_0
    return v0

    .line 1010674
    :cond_1
    const-string v2, "collapsable"

    invoke-virtual {p0, v2}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "collapsable"

    invoke-virtual {p0, v2, v0}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 1010675
    goto :goto_0

    .line 1010676
    :cond_2
    iget-object v2, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v2}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v2

    .line 1010677
    :cond_3
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1010678
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/5s2;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    .line 1010679
    goto :goto_0
.end method

.method public static c(LX/5qy;Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 4

    .prologue
    .line 1010764
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 1010765
    iget-object v1, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1010766
    :goto_0
    return-void

    .line 1010767
    :cond_0
    iget-object v1, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1010768
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v2, v0

    .line 1010769
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->B()I

    move-result v1

    .line 1010770
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->C()I

    move-result v0

    .line 1010771
    :goto_1
    if-eqz v2, :cond_1

    .line 1010772
    iget-boolean v3, v2, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v3, v3

    .line 1010773
    if-eqz v3, :cond_1

    .line 1010774
    invoke-virtual {v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v1, v3

    .line 1010775
    invoke-virtual {v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v0, v3

    .line 1010776
    iget-object v3, v2, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v2, v3

    .line 1010777
    goto :goto_1

    .line 1010778
    :cond_1
    invoke-direct {p0, p1, v1, v0}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;II)V

    goto :goto_0
.end method

.method private c(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1010665
    invoke-virtual {p1, p2, p3}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010666
    iget-object v0, p0, LX/5qy;->a:LX/5rl;

    .line 1010667
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 1010668
    const/4 v2, 0x1

    new-array v2, v2, [LX/5rn;

    const/4 v3, 0x0

    new-instance v4, LX/5rn;

    .line 1010669
    iget v5, p2, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v5

    .line 1010670
    invoke-direct {v4, v5, p3}, LX/5rn;-><init>(II)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v6, v2, v6}, LX/5rl;->a(I[I[LX/5rn;[I)V

    .line 1010671
    return-void
.end method

.method private d(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1010647
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v0, v0

    .line 1010648
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    move v0, v2

    .line 1010649
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1010650
    invoke-virtual {p2, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v4

    .line 1010651
    iget-object v3, v4, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v3, v3

    .line 1010652
    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    invoke-static {v3}, LX/0nE;->a(Z)V

    .line 1010653
    iget-boolean v3, v4, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v3, v3

    .line 1010654
    if-eqz v3, :cond_2

    .line 1010655
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->u()I

    move-result v3

    .line 1010656
    invoke-direct {p0, p1, v4, p3}, LX/5qy;->d(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010657
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->u()I

    move-result v4

    .line 1010658
    sub-int v3, v4, v3

    add-int/2addr p3, v3

    .line 1010659
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1010660
    goto :goto_0

    :cond_1
    move v3, v2

    .line 1010661
    goto :goto_2

    .line 1010662
    :cond_2
    invoke-direct {p0, p1, v4, p3}, LX/5qy;->c(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010663
    add-int/lit8 p3, p3, 0x1

    goto :goto_3

    .line 1010664
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1010606
    iget-object v0, p0, LX/5qy;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1010607
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;)V
    .locals 3

    .prologue
    .line 1010642
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1010643
    iget-object v1, p0, LX/5qy;->b:LX/5rF;

    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1010644
    invoke-direct {p0, p1, v1, v0}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010645
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1010646
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rJ;LX/5rC;)V
    .locals 3
    .param p3    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1010634
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RCTView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p3}, LX/5qy;->a(LX/5rC;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1010635
    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Z)V

    .line 1010636
    if-nez v0, :cond_0

    .line 1010637
    iget-object v0, p0, LX/5qy;->a:LX/5rl;

    .line 1010638
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 1010639
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2, p3}, LX/5rl;->a(LX/5rJ;ILjava/lang/String;LX/5rC;)V

    .line 1010640
    :cond_0
    return-void

    .line 1010641
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 2

    .prologue
    .line 1010623
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v0, v0

    .line 1010624
    if-eqz v0, :cond_1

    invoke-static {p3}, LX/5qy;->a(LX/5rC;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1010625
    :goto_0
    if-eqz v0, :cond_2

    .line 1010626
    invoke-direct {p0, p1, p3}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V

    .line 1010627
    :cond_0
    :goto_1
    return-void

    .line 1010628
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1010629
    :cond_2
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v0, v0

    .line 1010630
    if-nez v0, :cond_0

    .line 1010631
    iget-object v0, p0, LX/5qy;->a:LX/5rl;

    .line 1010632
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 1010633
    invoke-virtual {v0, v1, p3}, LX/5rl;->a(ILX/5rC;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;[I[LX/5rn;[I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1010608
    move v0, v1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 1010609
    aget v3, p2, v0

    move v2, v1

    .line 1010610
    :goto_1
    array-length v4, p4

    if-ge v2, v4, :cond_3

    .line 1010611
    aget v4, p4, v2

    if-ne v4, v3, :cond_0

    .line 1010612
    const/4 v2, 0x1

    .line 1010613
    :goto_2
    iget-object v4, p0, LX/5qy;->b:LX/5rF;

    invoke-virtual {v4, v3}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v3

    .line 1010614
    invoke-direct {p0, v3, v2}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Z)V

    .line 1010615
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1010616
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1010617
    :cond_1
    :goto_3
    array-length v0, p3

    if-ge v1, v0, :cond_2

    .line 1010618
    aget-object v0, p3, v1

    .line 1010619
    iget-object v2, p0, LX/5qy;->b:LX/5rF;

    iget v3, v0, LX/5rn;->b:I

    invoke-virtual {v2, v3}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    .line 1010620
    iget v0, v0, LX/5rn;->c:I

    invoke-direct {p0, p1, v2, v0}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1010621
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1010622
    :cond_2
    return-void

    :cond_3
    move v2, v1

    goto :goto_2
.end method
