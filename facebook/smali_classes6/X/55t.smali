.class public interface abstract LX/55t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/55s;


# virtual methods
.method public abstract aG_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aH_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Z
.end method

.method public abstract c()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()J
.end method

.method public abstract e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract p()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()LX/586;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()J
.end method

.method public abstract s()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Z
.end method

.method public abstract w()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
