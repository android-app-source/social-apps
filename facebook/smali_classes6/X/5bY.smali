.class public final LX/5bY;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 958636
    const-class v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    const v0, 0x21d23644

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "UsersQuery"

    const-string v6, "68a0ff6382db8e69ade52863b8afe442"

    const-string v7, "messaging_actors"

    const-string v8, "10155256719241729"

    const/4 v9, 0x0

    .line 958637
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 958638
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 958639
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 958625
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 958626
    sparse-switch v0, :sswitch_data_0

    .line 958627
    :goto_0
    return-object p1

    .line 958628
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 958629
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 958630
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 958631
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 958632
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 958633
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 958634
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 958635
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1b236af7 -> :sswitch_7
        -0x179abbec -> :sswitch_6
        -0x132889c -> :sswitch_4
        0xe0e2e5a -> :sswitch_1
        0x2f1911b0 -> :sswitch_2
        0x3349e8c0 -> :sswitch_3
        0x69308369 -> :sswitch_0
        0x72938b08 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 958621
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 958622
    :goto_1
    return v0

    .line 958623
    :pswitch_0
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 958624
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x36
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 958620
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfo$UsersQueryString$1;

    const-class v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/messaging/graphql/threads/UserInfo$UsersQueryString$1;-><init>(LX/5bY;Ljava/lang/Class;)V

    return-object v0
.end method
