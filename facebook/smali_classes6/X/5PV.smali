.class public abstract LX/5PV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5PS;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field public a:Landroid/opengl/EGLSurface;

.field public b:LX/5PM;

.field private c:Z


# direct methods
.method public constructor <init>(LX/5PM;)V
    .locals 1

    .prologue
    .line 910787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910788
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    .line 910789
    iput-object p1, p0, LX/5PV;->b:LX/5PM;

    .line 910790
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/5PV;->c:Z

    .line 910791
    if-nez p1, :cond_0

    .line 910792
    new-instance v0, LX/5PM;

    invoke-direct {v0}, LX/5PM;-><init>()V

    iput-object v0, p0, LX/5PV;->b:LX/5PM;

    .line 910793
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    invoke-virtual {v0}, LX/5PM;->e()LX/5PM;

    .line 910794
    :cond_0
    return-void

    .line 910795
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 910796
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    .line 910797
    iget-object v1, v0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move-object v0, v1

    .line 910798
    iget-object v1, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    iget-object v2, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    iget-object v3, p0, LX/5PV;->b:LX/5PM;

    .line 910799
    iget-object p0, v3, LX/5PM;->b:Landroid/opengl/EGLContext;

    move-object v3, p0

    .line 910800
    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 910801
    const-string v0, "eglMakeCurrent"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910802
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910803
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 910804
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    .line 910805
    iget-object v1, v0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move-object v0, v1

    .line 910806
    iget-object v1, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 910807
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 910808
    iget-object v0, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 910809
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    .line 910810
    iget-object v1, v0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move-object v0, v1

    .line 910811
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iget-object v3, p0, LX/5PV;->b:LX/5PM;

    .line 910812
    iget-object v4, v3, LX/5PM;->b:Landroid/opengl/EGLContext;

    move-object v3, v4

    .line 910813
    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 910814
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    .line 910815
    iget-object v1, v0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move-object v0, v1

    .line 910816
    iget-object v1, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 910817
    :cond_0
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    .line 910818
    iget-boolean v0, p0, LX/5PV;->c:Z

    if-eqz v0, :cond_1

    .line 910819
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    invoke-virtual {v0}, LX/5PM;->a()V

    .line 910820
    :cond_1
    return-void
.end method
