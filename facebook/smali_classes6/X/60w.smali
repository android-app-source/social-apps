.class public final enum LX/60w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/60w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/60w;

.field public static final enum BGRA:LX/60w;

.field public static final enum RGBA:LX/60w;


# instance fields
.field public final openGlConstant:I

.field public final openGlString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1039464
    new-instance v0, LX/60w;

    const-string v1, "RGBA"

    const-string v2, "rgba"

    const/16 v3, 0x1908

    invoke-direct {v0, v1, v4, v2, v3}, LX/60w;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/60w;->RGBA:LX/60w;

    .line 1039465
    new-instance v0, LX/60w;

    const-string v1, "BGRA"

    const-string v2, "bgra"

    const v3, 0x80e1

    invoke-direct {v0, v1, v5, v2, v3}, LX/60w;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/60w;->BGRA:LX/60w;

    .line 1039466
    const/4 v0, 0x2

    new-array v0, v0, [LX/60w;

    sget-object v1, LX/60w;->RGBA:LX/60w;

    aput-object v1, v0, v4

    sget-object v1, LX/60w;->BGRA:LX/60w;

    aput-object v1, v0, v5

    sput-object v0, LX/60w;->$VALUES:[LX/60w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1039467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1039468
    iput-object p3, p0, LX/60w;->openGlString:Ljava/lang/String;

    .line 1039469
    iput p4, p0, LX/60w;->openGlConstant:I

    .line 1039470
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/60w;
    .locals 1

    .prologue
    .line 1039471
    const-class v0, LX/60w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/60w;

    return-object v0
.end method

.method public static values()[LX/60w;
    .locals 1

    .prologue
    .line 1039472
    sget-object v0, LX/60w;->$VALUES:[LX/60w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/60w;

    return-object v0
.end method
