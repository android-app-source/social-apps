.class public LX/6CW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6CP;


# direct methods
.method public constructor <init>(LX/6CP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063877
    iput-object p1, p0, LX/6CW;->a:LX/6CP;

    .line 1063878
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063879
    const-string v0, "updateCart"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 5

    .prologue
    .line 1063880
    check-cast p1, Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    .line 1063881
    iget-object v0, p0, LX/6CW;->a:LX/6CP;

    .line 1063882
    const-string v1, "itemCount"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1063883
    const-string v2, "cartURL"

    invoke-virtual {p1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v2, v2

    .line 1063884
    const-string v3, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v3, v3

    .line 1063885
    new-instance v4, LX/6CV;

    invoke-direct {v4, p0, p1}, LX/6CV;-><init>(LX/6CW;Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;)V

    .line 1063886
    new-instance p0, LX/4HH;

    invoke-direct {p0}, LX/4HH;-><init>()V

    const-string p1, "UPDATE_CART"

    invoke-virtual {p0, p1}, LX/4HH;->a(Ljava/lang/String;)LX/4HH;

    move-result-object p0

    .line 1063887
    const-string p1, "cart_item_count"

    invoke-virtual {p0, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063888
    move-object p0, p0

    .line 1063889
    const-string p1, "cart_url"

    invoke-virtual {p0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063890
    move-object p0, p0

    .line 1063891
    invoke-virtual {p0, v3}, LX/4HH;->b(Ljava/lang/String;)LX/4HH;

    move-result-object p0

    .line 1063892
    invoke-static {v0, p0, v4, v3}, LX/6CP;->a(LX/6CP;LX/4HH;LX/6CO;Ljava/lang/String;)V

    .line 1063893
    return-void
.end method
