.class public LX/6JB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIIIILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/util/List",
            "<",
            "LX/6JA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1075375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075376
    iput p1, p0, LX/6JB;->a:I

    .line 1075377
    iput p2, p0, LX/6JB;->b:I

    .line 1075378
    iput p3, p0, LX/6JB;->c:I

    .line 1075379
    iput p4, p0, LX/6JB;->d:I

    .line 1075380
    iput p5, p0, LX/6JB;->e:I

    .line 1075381
    iput-object p6, p0, LX/6JB;->f:Ljava/util/List;

    .line 1075382
    return-void
.end method

.method private constructor <init>(LX/6JB;)V
    .locals 6

    .prologue
    .line 1075383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075384
    iget v0, p1, LX/6JB;->a:I

    iput v0, p0, LX/6JB;->a:I

    .line 1075385
    iget v0, p1, LX/6JB;->b:I

    iput v0, p0, LX/6JB;->b:I

    .line 1075386
    iget v0, p1, LX/6JB;->c:I

    iput v0, p0, LX/6JB;->c:I

    .line 1075387
    iget v0, p1, LX/6JB;->d:I

    iput v0, p0, LX/6JB;->d:I

    .line 1075388
    iget v0, p1, LX/6JB;->e:I

    iput v0, p0, LX/6JB;->e:I

    .line 1075389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6JB;->f:Ljava/util/List;

    .line 1075390
    iget-object v0, p1, LX/6JB;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1075391
    iget-object v0, p1, LX/6JB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JA;

    .line 1075392
    iget-object v2, p0, LX/6JB;->f:Ljava/util/List;

    new-instance v3, LX/6JA;

    iget-object v4, v0, LX/6JA;->a:Landroid/graphics/SurfaceTexture;

    iget v5, v0, LX/6JA;->b:I

    iget v0, v0, LX/6JA;->c:I

    invoke-direct {v3, v4, v5, v0}, LX/6JA;-><init>(Landroid/graphics/SurfaceTexture;II)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1075393
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/6JB;
    .locals 1

    .prologue
    .line 1075394
    new-instance v0, LX/6JB;

    invoke-direct {v0, p0}, LX/6JB;-><init>(LX/6JB;)V

    return-object v0
.end method
