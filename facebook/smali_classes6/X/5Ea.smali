.class public final LX/5Ea;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 883120
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883121
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883122
    if-eqz v0, :cond_0

    .line 883123
    const-string v1, "layout_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883124
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883125
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883126
    if-eqz v0, :cond_1

    .line 883127
    const-string v1, "layout_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883128
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883129
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883130
    if-eqz v0, :cond_2

    .line 883131
    const-string v1, "layout_x"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883132
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883133
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883134
    if-eqz v0, :cond_3

    .line 883135
    const-string v1, "layout_y"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883136
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883137
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883138
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 883139
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 883140
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 883141
    :goto_0
    return v1

    .line 883142
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 883143
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 883144
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 883145
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 883146
    const-string v11, "layout_height"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 883147
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v2

    goto :goto_1

    .line 883148
    :cond_1
    const-string v11, "layout_width"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 883149
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 883150
    :cond_2
    const-string v11, "layout_x"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 883151
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 883152
    :cond_3
    const-string v11, "layout_y"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 883153
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 883154
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 883155
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 883156
    if-eqz v5, :cond_6

    .line 883157
    invoke-virtual {p1, v1, v9, v1}, LX/186;->a(III)V

    .line 883158
    :cond_6
    if-eqz v4, :cond_7

    .line 883159
    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 883160
    :cond_7
    if-eqz v3, :cond_8

    .line 883161
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 883162
    :cond_8
    if-eqz v0, :cond_9

    .line 883163
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 883164
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto :goto_1
.end method
