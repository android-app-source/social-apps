.class public final LX/6Gu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/6Gv;


# direct methods
.method public constructor <init>(LX/6Gv;)V
    .locals 0

    .prologue
    .line 1071028
    iput-object p1, p0, LX/6Gu;->a:LX/6Gv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x2c286f17

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071029
    iget-object v1, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v1, v1, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 1071030
    const v1, 0x31334604

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1071031
    :goto_0
    return-void

    .line 1071032
    :cond_0
    iget-object v1, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v1, v1, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget-object v2, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v2, v2, LX/6Gv;->a:Landroid/net/Uri;

    .line 1071033
    new-instance v3, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-direct {v3}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;-><init>()V

    .line 1071034
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1071035
    const-string p1, "arg_screenshot_bitmap_uri"

    invoke-virtual {v4, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1071036
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1071037
    move-object v2, v3

    .line 1071038
    iput-object v2, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    .line 1071039
    iget-object v1, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v1, v1, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v2, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v2, v2, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget-object v2, v2, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->e:LX/6Gq;

    .line 1071040
    iput-object v2, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->v:LX/6Gq;

    .line 1071041
    iget-object v1, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v1, v1, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v2, p0, LX/6Gu;->a:LX/6Gv;

    iget-object v2, v2, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const-class v3, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1071042
    const v1, -0x26568422

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
