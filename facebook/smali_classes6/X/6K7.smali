.class public final LX/6K7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JG;


# instance fields
.field public final synthetic a:LX/6K8;


# direct methods
.method public constructor <init>(LX/6K8;)V
    .locals 0

    .prologue
    .line 1076385
    iput-object p1, p0, LX/6K7;->a:LX/6K8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1076386
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    .line 1076387
    iput-object v1, v0, LX/6K8;->d:LX/6K9;

    .line 1076388
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->c:LX/6JG;

    invoke-interface {v0}, LX/6JG;->a()V

    .line 1076389
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076390
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076391
    return-void
.end method

.method public final a(LX/6JJ;)V
    .locals 3

    .prologue
    .line 1076392
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    .line 1076393
    iput-object v1, v0, LX/6K8;->d:LX/6K9;

    .line 1076394
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->c:LX/6JG;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to start video recording"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6JG;->a(LX/6JJ;)V

    .line 1076395
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->d(I)V

    .line 1076396
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076397
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1076398
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    .line 1076399
    iput-object v1, v0, LX/6K8;->d:LX/6K9;

    .line 1076400
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->c:LX/6JG;

    invoke-interface {v0}, LX/6JG;->b()V

    .line 1076401
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    iget-object v0, v0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076402
    iget-object v0, p0, LX/6K7;->a:LX/6K8;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076403
    return-void
.end method
