.class public LX/6RS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1089751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 1089752
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;IIZ)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;Ljava/util/TimeZone;IIZ)Ljava/util/Calendar;
    .locals 6

    .prologue
    .line 1089753
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1, p1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1089754
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1089755
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2, p1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1089756
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 1089757
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1089758
    add-int/lit8 v5, p3, -0x1

    .line 1089759
    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/util/Calendar;->get(I)I

    move-result p0

    .line 1089760
    if-le p0, v5, :cond_2

    .line 1089761
    :cond_0
    :goto_0
    move v1, v3

    .line 1089762
    if-eqz v1, :cond_1

    if-nez p4, :cond_1

    .line 1089763
    add-int/lit8 v0, v0, 0x1

    .line 1089764
    :cond_1
    new-instance v1, Ljava/util/Date;

    const-wide/16 v4, 0x0

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 1089765
    add-int/lit8 v1, p3, -0x1

    invoke-virtual {v2, v0, v1, p2}, Ljava/util/GregorianCalendar;->set(III)V

    .line 1089766
    return-object v2

    .line 1089767
    :cond_2
    if-ge p0, v5, :cond_3

    move v3, v4

    .line 1089768
    goto :goto_0

    .line 1089769
    :cond_3
    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-gt v5, p2, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private static a(Ljava/util/Calendar;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1089770
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1089771
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1089772
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1089773
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1089774
    return-void
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1089775
    const/16 v0, 0xe

    invoke-static {p0, p1, v0}, LX/6RS;->a(Ljava/util/Calendar;Ljava/util/Calendar;I)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1089776
    invoke-static {p0}, LX/6RS;->a(Ljava/util/Calendar;)V

    .line 1089777
    invoke-static {p1}, LX/6RS;->a(Ljava/util/Calendar;)V

    .line 1089778
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 1089779
    invoke-virtual {p1, p0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089780
    invoke-static {p0, p1, p2}, LX/6RS;->b(Ljava/util/Calendar;Ljava/util/Calendar;I)Z

    move-result v0

    .line 1089781
    :goto_0
    return v0

    .line 1089782
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->roll(II)V

    .line 1089783
    invoke-static {p0, p1, p2}, LX/6RS;->b(Ljava/util/Calendar;Ljava/util/Calendar;I)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Ljava/util/Calendar;Ljava/util/Calendar;I)Z
    .locals 6

    .prologue
    .line 1089784
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1089785
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p2

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
