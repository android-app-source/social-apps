.class public LX/6PZ;
.super LX/6PU;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public constructor <init>(LX/0Ot;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/enums/GraphQLSavedState;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086233
    invoke-direct {p0, p2}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086234
    iput-object p1, p0, LX/6PZ;->a:LX/0Ot;

    .line 1086235
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/6PZ;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086236
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 2

    .prologue
    .line 1086237
    iget-object v0, p0, LX/6PZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    iget-object v1, p0, LX/6PZ;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/40T;->a(LX/16f;)V

    .line 1086238
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086239
    const-string v0, "SaveStoryMutatingVisitor"

    return-object v0
.end method
