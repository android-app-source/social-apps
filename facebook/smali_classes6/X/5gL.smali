.class public final LX/5gL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 974465
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 974466
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974467
    :goto_0
    return v1

    .line 974468
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974469
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 974470
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 974471
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 974472
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 974473
    const-string v4, "media"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 974474
    invoke-static {p0, p1}, LX/5gK;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 974475
    :cond_2
    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 974476
    const/4 v3, 0x0

    .line 974477
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 974478
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974479
    :goto_2
    move v0, v3

    .line 974480
    goto :goto_1

    .line 974481
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 974482
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 974483
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 974484
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 974485
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974486
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 974487
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 974488
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 974489
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 974490
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 974491
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 974492
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 974493
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 974494
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 974449
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974450
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974451
    if-eqz v0, :cond_0

    .line 974452
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974453
    invoke-static {p0, v0, p2, p3}, LX/5gK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 974454
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974455
    if-eqz v0, :cond_2

    .line 974456
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974457
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974458
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 974459
    if-eqz v1, :cond_1

    .line 974460
    const-string p1, "text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974461
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974462
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974463
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974464
    return-void
.end method
