.class public LX/61o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61h;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private a:Landroid/media/MediaMuxer;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040671
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1040672
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->start()V

    .line 1040673
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1040674
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->setOrientationHint(I)V

    .line 1040675
    return-void
.end method

.method public final a(LX/60z;)V
    .locals 4

    .prologue
    .line 1040676
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    iget v1, p0, LX/61o;->b:I

    invoke-interface {p1}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-interface {p1}, LX/60z;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1040677
    return-void
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 1040678
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v0

    iput v0, p0, LX/61o;->b:I

    .line 1040679
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1040680
    new-instance v0, Landroid/media/MediaMuxer;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/media/MediaMuxer;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    .line 1040681
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1040682
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->stop()V

    .line 1040683
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->release()V

    .line 1040684
    return-void
.end method

.method public final b(LX/60z;)V
    .locals 4

    .prologue
    .line 1040685
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    iget v1, p0, LX/61o;->c:I

    invoke-interface {p1}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-interface {p1}, LX/60z;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1040686
    return-void
.end method

.method public final b(Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 1040687
    iget-object v0, p0, LX/61o;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v0

    iput v0, p0, LX/61o;->c:I

    .line 1040688
    return-void
.end method
