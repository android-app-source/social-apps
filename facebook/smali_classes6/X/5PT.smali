.class public LX/5PT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5PS;


# instance fields
.field public a:Ljavax/microedition/khronos/egl/EGLSurface;

.field public b:LX/5PL;

.field private c:Z

.field private final d:Ljavax/microedition/khronos/egl/EGL10;


# direct methods
.method public constructor <init>(LX/5PL;)V
    .locals 1

    .prologue
    .line 910742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910743
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 910744
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, LX/5PT;->d:Ljavax/microedition/khronos/egl/EGL10;

    .line 910745
    iput-object p1, p0, LX/5PT;->b:LX/5PL;

    .line 910746
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/5PT;->c:Z

    .line 910747
    if-nez p1, :cond_0

    .line 910748
    new-instance v0, LX/5PL;

    invoke-direct {v0}, LX/5PL;-><init>()V

    iput-object v0, p0, LX/5PT;->b:LX/5PL;

    .line 910749
    iget-object v0, p0, LX/5PT;->b:LX/5PL;

    .line 910750
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/5PL;->b(LX/5PL;I)LX/5PL;

    .line 910751
    :cond_0
    return-void

    .line 910752
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 910753
    iget-object v0, p0, LX/5PT;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PT;->b:LX/5PL;

    .line 910754
    iget-object v2, v1, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object v1, v2

    .line 910755
    iget-object v2, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, LX/5PT;->b:LX/5PL;

    .line 910756
    iget-object p0, v4, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    move-object v4, p0

    .line 910757
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 910758
    const-string v0, "eglMakeCurrent"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910759
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910760
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 910761
    iget-object v0, p0, LX/5PT;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PT;->b:LX/5PL;

    .line 910762
    iget-object v2, v1, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object v1, v2

    .line 910763
    iget-object v2, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 910764
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 910765
    iget-object v0, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 910766
    iget-object v0, p0, LX/5PT;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PT;->b:LX/5PL;

    .line 910767
    iget-object v2, v1, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object v1, v2

    .line 910768
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, LX/5PT;->b:LX/5PL;

    .line 910769
    iget-object v5, v4, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    move-object v4, v5

    .line 910770
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 910771
    iget-object v0, p0, LX/5PT;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PT;->b:LX/5PL;

    .line 910772
    iget-object v2, v1, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object v1, v2

    .line 910773
    iget-object v2, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 910774
    :cond_0
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, LX/5PT;->a:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 910775
    iget-boolean v0, p0, LX/5PT;->c:Z

    if-eqz v0, :cond_1

    .line 910776
    iget-object v0, p0, LX/5PT;->b:LX/5PL;

    invoke-virtual {v0}, LX/5PL;->a()V

    .line 910777
    :cond_1
    return-void
.end method
