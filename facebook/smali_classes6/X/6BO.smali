.class public final LX/6BO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1kt",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMedia;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;

.field public b:LX/1ks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ks",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/3HP;

.field public d:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic e:LX/3HD;


# direct methods
.method public constructor <init>(LX/3HD;Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;LX/1ks;LX/3HP;)V
    .locals 0
    .param p4    # LX/3HP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;",
            "LX/1ks",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;",
            "Lcom/facebook/api/ufiservices/common/FeedbackCacheProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1061900
    iput-object p1, p0, LX/6BO;->e:LX/3HD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061901
    iput-object p2, p0, LX/6BO;->a:Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;

    .line 1061902
    iput-object p3, p0, LX/6BO;->b:LX/1ks;

    .line 1061903
    iput-object p4, p0, LX/6BO;->c:LX/3HP;

    .line 1061904
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1061905
    iget-object v0, p0, LX/6BO;->b:LX/1ks;

    invoke-virtual {v0}, LX/1ks;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1061906
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061907
    if-eqz v0, :cond_0

    .line 1061908
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061909
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1061910
    iget-object v0, p0, LX/6BO;->e:LX/3HD;

    iget-object v1, v0, LX/3HD;->e:LX/3HE;

    .line 1061911
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061912
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3HE;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1061913
    :cond_0
    iget-object v0, p0, LX/6BO;->b:LX/1ks;

    invoke-virtual {v0, p1}, LX/1ks;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    .line 1061914
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061915
    if-eqz v0, :cond_1

    .line 1061916
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061917
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1061918
    const/4 v0, 0x1

    .line 1061919
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1061920
    iget-object v0, p0, LX/6BO;->b:LX/1ks;

    invoke-virtual {v0}, LX/1ks;->b()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1061921
    iget-object v1, p0, LX/6BO;->e:LX/3HD;

    .line 1061922
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061923
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1061924
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1061925
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061926
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1061927
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061928
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1061929
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1061930
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061931
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1lO;->a(Ljava/util/Collection;)LX/1lO;

    move-result-object v0

    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    .line 1061932
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1061933
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/6BO;->a:Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;

    .line 1061934
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    move-object v0, v1

    .line 1061935
    sget-object v1, LX/5H2;->COMPLETE:LX/5H2;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/6BO;->c:LX/3HP;

    if-eqz v0, :cond_1

    .line 1061936
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061937
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1061938
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1061939
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, LX/6BO;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1061940
    iget-object v0, p0, LX/6BO;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, LX/6BO;->e:LX/3HD;

    iget-object v1, v1, LX/3HD;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 1061941
    iget-object v0, p0, LX/6BO;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_3

    .line 1061942
    :cond_1
    :goto_1
    return-object p1

    .line 1061943
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    iget-object v6, v1, LX/3HD;->c:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    goto/16 :goto_0

    .line 1061944
    :cond_3
    iget-object v0, p0, LX/6BO;->c:LX/3HP;

    iget-object v1, p0, LX/6BO;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3HP;->a(Ljava/lang/String;)LX/1kt;

    move-result-object v0

    .line 1061945
    new-instance v1, LX/1lO;

    invoke-direct {v1}, LX/1lO;-><init>()V

    iget-object v2, p0, LX/6BO;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1061946
    iput-object v2, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 1061947
    move-object v1, v1

    .line 1061948
    invoke-virtual {v1}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 1061949
    if-eqz v4, :cond_1

    .line 1061950
    invoke-interface {v0, v1}, LX/1kt;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    goto :goto_1
.end method
