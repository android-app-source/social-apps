.class public LX/67r;
.super LX/67m;
.source ""

# interfaces
.implements LX/67n;
.implements LX/67o;
.implements LX/67p;
.implements LX/67q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/android/maps/ClusterItem;",
        ">",
        "LX/67m;",
        "LX/67n;",
        "LX/67o;",
        "LX/67p;",
        "LX/67q;"
    }
.end annotation


# static fields
.field private static final D:LX/31i;


# instance fields
.field public final A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/67h",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public E:F

.field public final F:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67i;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field private H:LX/692;

.field public o:LX/67j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/67j",
            "<TT;>;"
        }
    .end annotation
.end field

.field public p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/67i;",
            "LX/67h",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/67h",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private r:LX/67i;

.field private s:LX/67i;

.field private t:LX/67k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/67k",
            "<TT;>;"
        }
    .end annotation
.end field

.field private u:LX/67l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/67l",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final v:LX/31i;

.field private final w:LX/31i;

.field private final x:[D

.field public y:LX/68u;

.field public final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/67h",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1053666
    new-instance v1, LX/31i;

    move-wide v4, v2

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, LX/31i;-><init>(DDDD)V

    sput-object v1, LX/67r;->D:LX/31i;

    return-void
.end method

.method private a(LX/67i;)V
    .locals 1

    .prologue
    .line 1053662
    iget-object v0, p0, LX/67r;->r:LX/67i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/67r;->r:LX/67i;

    if-eq v0, p1, :cond_0

    .line 1053663
    iget-object v0, p0, LX/67r;->r:LX/67i;

    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0}, LX/67m;->e()V

    .line 1053664
    :cond_0
    iput-object p1, p0, LX/67r;->r:LX/67i;

    .line 1053665
    return-void
.end method

.method public static a$redex0(LX/67r;Ljava/util/Set;)Ljava/util/Set;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/67h",
            "<TT;>;>;)",
            "Ljava/util/Set",
            "<",
            "LX/67h",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1053622
    iget-object v0, p0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1053623
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1053624
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1053625
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/67i;

    .line 1053626
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053627
    iget-object v4, p0, LX/67r;->x:[D

    invoke-virtual {v0, v4}, LX/67h;->a([D)V

    .line 1053628
    iget-object v4, p0, LX/67r;->x:[D

    aget-wide v4, v4, v10

    .line 1053629
    iget-object v6, p0, LX/67r;->x:[D

    aget-wide v6, v6, v11

    .line 1053630
    iget-object v8, p0, LX/67r;->v:LX/31i;

    invoke-virtual {v8, v4, v5, v6, v7}, LX/31i;->a(DD)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1053631
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1053632
    iget-object v0, p0, LX/67r;->r:LX/67i;

    if-ne v1, v0, :cond_2

    .line 1053633
    invoke-direct {p0, v2}, LX/67r;->a(LX/67i;)V

    .line 1053634
    :cond_2
    iget-object v0, v1, LX/67i;->a:LX/67m;

    instance-of v0, v0, LX/698;

    if-eqz v0, :cond_3

    .line 1053635
    iget-object v0, v1, LX/67i;->a:LX/67m;

    check-cast v0, LX/698;

    .line 1053636
    iput-object v2, v0, LX/698;->H:LX/67o;

    .line 1053637
    :cond_3
    iget-boolean v0, v1, LX/67i;->c:Z

    if-eqz v0, :cond_0

    .line 1053638
    iget-object v0, p0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1053639
    :cond_4
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053640
    iget-object v1, p0, LX/67r;->x:[D

    invoke-virtual {v0, v1}, LX/67h;->a([D)V

    .line 1053641
    iget-object v1, p0, LX/67r;->x:[D

    aget-wide v6, v1, v10

    .line 1053642
    iget-object v1, p0, LX/67r;->x:[D

    aget-wide v8, v1, v11

    .line 1053643
    iget v1, v0, LX/67h;->b:I

    move v1, v1

    .line 1053644
    if-eqz v1, :cond_5

    iget-object v1, p0, LX/67r;->v:LX/31i;

    invoke-virtual {v1, v6, v7, v8, v9}, LX/31i;->a(DD)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1053645
    iget-object v1, p0, LX/67r;->o:LX/67j;

    invoke-interface {v1}, LX/67j;->a()I

    move-result v5

    .line 1053646
    iget-object v1, p0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1053647
    iget-object v1, p0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_2
    if-ltz v3, :cond_a

    .line 1053648
    iget-object v1, p0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/67i;

    .line 1053649
    iget v6, v1, LX/67i;->b:I

    if-ne v6, v5, :cond_8

    .line 1053650
    iget-object v5, p0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1053651
    :goto_3
    if-nez v1, :cond_6

    .line 1053652
    iget-object v1, p0, LX/67r;->o:LX/67j;

    invoke-interface {v1}, LX/67j;->b()LX/67i;

    move-result-object v1

    .line 1053653
    :cond_6
    iget-object v3, v1, LX/67i;->a:LX/67m;

    .line 1053654
    iput-object v3, v0, LX/67h;->q:LX/67m;

    .line 1053655
    iget-object v3, p0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1053656
    iget-object v0, v1, LX/67i;->a:LX/67m;

    instance-of v0, v0, LX/698;

    if-eqz v0, :cond_7

    .line 1053657
    iget-object v0, v1, LX/67i;->a:LX/67m;

    check-cast v0, LX/698;

    .line 1053658
    iput-object p0, v0, LX/698;->H:LX/67o;

    .line 1053659
    :cond_7
    iget-object v0, v1, LX/67i;->a:LX/67m;

    invoke-virtual {v0}, LX/67m;->b()V

    goto :goto_1

    .line 1053660
    :cond_8
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_2

    .line 1053661
    :cond_9
    return-object p1

    :cond_a
    move-object v1, v2

    goto :goto_3
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1053612
    iget-object v0, p0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67i;

    .line 1053613
    iget-object v2, p0, LX/67r;->r:LX/67i;

    if-eq v0, v2, :cond_0

    iget-object v2, v0, LX/67i;->a:LX/67m;

    .line 1053614
    iget-boolean v3, v2, LX/67m;->i:Z

    move v2, v3

    .line 1053615
    if-eqz v2, :cond_0

    .line 1053616
    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0, p1}, LX/67m;->a(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1053617
    :cond_1
    iget-object v0, p0, LX/67r;->r:LX/67i;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/67r;->r:LX/67i;

    iget-object v0, v0, LX/67i;->a:LX/67m;

    .line 1053618
    iget-boolean v1, v0, LX/67m;->i:Z

    move v0, v1

    .line 1053619
    if-eqz v0, :cond_2

    .line 1053620
    iget-object v0, p0, LX/67r;->r:LX/67i;

    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0, p1}, LX/67m;->a(Landroid/graphics/Canvas;)V

    .line 1053621
    :cond_2
    return-void
.end method

.method private d(LX/68u;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1053599
    const/4 v0, 0x0

    iget-object v1, p0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1053600
    iget-object v0, p0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053601
    iget-object v1, v0, LX/67h;->q:LX/67m;

    move-object v1, v1

    .line 1053602
    check-cast v1, LX/698;

    .line 1053603
    invoke-virtual {v0}, LX/67h;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v4

    .line 1053604
    invoke-virtual {v1, v4}, LX/698;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1053605
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, v4}, LX/698;->a(F)V

    .line 1053606
    iput-object v5, v0, LX/67h;->p:LX/67h;

    .line 1053607
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1053608
    :cond_0
    iget-object v0, p0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1053609
    invoke-virtual {p1}, LX/68u;->a()V

    .line 1053610
    iput-object v5, p0, LX/67r;->y:LX/68u;

    .line 1053611
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 1053586
    const/4 v0, 0x0

    iput-object v0, p0, LX/67r;->s:LX/67i;

    .line 1053587
    const/4 v0, 0x0

    .line 1053588
    iget-object v1, p0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67i;

    .line 1053589
    iget-object v2, v0, LX/67i;->a:LX/67m;

    .line 1053590
    iget-boolean v5, v2, LX/67m;->i:Z

    move v2, v5

    .line 1053591
    if-eqz v2, :cond_2

    .line 1053592
    iget-object v2, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v2, p1, p2}, LX/67m;->a(FF)I

    move-result v2

    .line 1053593
    if-ne v2, v3, :cond_1

    .line 1053594
    iput-object v0, p0, LX/67r;->s:LX/67i;

    move v1, v3

    .line 1053595
    :cond_0
    return v1

    .line 1053596
    :cond_1
    if-le v2, v1, :cond_2

    .line 1053597
    iput-object v0, p0, LX/67r;->s:LX/67i;

    move v0, v2

    :goto_1
    move v1, v0

    .line 1053598
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(LX/68u;)V
    .locals 12

    .prologue
    .line 1053571
    const/4 v0, 0x0

    iget-object v1, p0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1053572
    iget-object v0, p0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053573
    iget-object v1, v0, LX/67h;->q:LX/67m;

    move-object v1, v1

    .line 1053574
    check-cast v1, LX/698;

    .line 1053575
    iget-object v4, v0, LX/67h;->p:LX/67h;

    move-object v4, v4

    .line 1053576
    invoke-virtual {v4}, LX/67h;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v4

    .line 1053577
    invoke-virtual {v0}, LX/67h;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 1053578
    iget v5, p1, LX/68u;->C:F

    move v5, v5

    .line 1053579
    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v8, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double/2addr v6, v8

    float-to-double v8, v5

    mul-double/2addr v6, v8

    .line 1053580
    iget-wide v8, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v10, v4, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, LX/67h;->b(D)D

    move-result-wide v8

    float-to-double v10, v5

    mul-double/2addr v8, v10

    .line 1053581
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v10, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    add-double/2addr v6, v10

    iget-wide v10, v4, Lcom/facebook/android/maps/model/LatLng;->b:D

    add-double/2addr v8, v10

    invoke-static {v8, v9}, LX/67h;->b(D)D

    move-result-wide v8

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1053582
    invoke-virtual {v1, v0}, LX/698;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1053583
    invoke-virtual {v1, v5}, LX/698;->a(F)V

    .line 1053584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1053585
    :cond_0
    return-void
.end method

.method public final a(LX/692;)V
    .locals 1

    .prologue
    .line 1053567
    iget-object v0, p0, LX/67r;->H:LX/692;

    invoke-virtual {p1, v0}, LX/692;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1053568
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67r;->G:Z

    .line 1053569
    :cond_0
    iput-object p1, p0, LX/67r;->H:LX/692;

    .line 1053570
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 1053527
    iget-boolean v0, p0, LX/67r;->G:Z

    if-nez v0, :cond_0

    .line 1053528
    invoke-direct {p0, p1}, LX/67r;->b(Landroid/graphics/Canvas;)V

    .line 1053529
    :goto_0
    return-void

    .line 1053530
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/67r;->G:Z

    .line 1053531
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->c()LX/692;

    move-result-object v0

    iget v0, v0, LX/692;->b:F

    .line 1053532
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-object v2, p0, LX/67r;->w:LX/31i;

    invoke-virtual {v1, v2}, LX/31h;->a(LX/31i;)V

    .line 1053533
    iget v1, p0, LX/67r;->E:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_1

    iget-object v1, p0, LX/67r;->v:LX/31i;

    iget-object v2, p0, LX/67r;->w:LX/31i;

    invoke-virtual {v1, v2}, LX/31i;->e(LX/31i;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1053534
    invoke-direct {p0, p1}, LX/67r;->b(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1053535
    :cond_1
    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_6

    .line 1053536
    iget-object v1, p0, LX/67r;->v:LX/31i;

    sget-object v2, LX/67r;->D:LX/31i;

    iget-wide v2, v2, LX/31i;->b:D

    iput-wide v2, v1, LX/31i;->b:D

    .line 1053537
    iget-object v1, p0, LX/67r;->v:LX/31i;

    sget-object v2, LX/67r;->D:LX/31i;

    iget-wide v2, v2, LX/31i;->a:D

    iput-wide v2, v1, LX/31i;->a:D

    .line 1053538
    iget-object v1, p0, LX/67r;->v:LX/31i;

    sget-object v2, LX/67r;->D:LX/31i;

    iget-wide v2, v2, LX/31i;->c:D

    iput-wide v2, v1, LX/31i;->c:D

    .line 1053539
    iget-object v1, p0, LX/67r;->v:LX/31i;

    sget-object v2, LX/67r;->D:LX/31i;

    iget-wide v2, v2, LX/31i;->d:D

    iput-wide v2, v1, LX/31i;->d:D

    .line 1053540
    :goto_1
    iget v1, p0, LX/67r;->E:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    iget v1, p0, LX/67r;->E:F

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_8

    .line 1053541
    :cond_2
    iget-object v1, p0, LX/67r;->y:LX/68u;

    if-eqz v1, :cond_3

    .line 1053542
    iget-object v1, p0, LX/67r;->y:LX/68u;

    invoke-virtual {v1}, LX/68u;->g()V

    .line 1053543
    :cond_3
    iget-object v1, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-eqz v1, :cond_4

    .line 1053544
    iget-object v1, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-static {v1}, LX/31l;->d(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1053545
    const/4 v1, 0x0

    iput-object v1, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1053546
    :cond_4
    iget-object v1, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-nez v1, :cond_5

    .line 1053547
    new-instance v1, Lcom/facebook/android/maps/ClusterOverlay$1;

    invoke-direct {v1, p0, v0}, Lcom/facebook/android/maps/ClusterOverlay$1;-><init>(LX/67r;F)V

    iput-object v1, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1053548
    iget-object v0, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    const-wide/16 v2, 0x96

    invoke-static {v0, v2, v3}, LX/31l;->b(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;J)V

    .line 1053549
    :cond_5
    :goto_2
    invoke-direct {p0, p1}, LX/67r;->b(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 1053550
    :cond_6
    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v2, v1, LX/31i;->d:D

    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v4, v1, LX/31i;->c:D

    sub-double/2addr v2, v4

    .line 1053551
    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v4, v1, LX/31i;->b:D

    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v6, v1, LX/31i;->a:D

    sub-double/2addr v4, v6

    .line 1053552
    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v6, v1, LX/31i;->c:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v8, v2, v8

    sub-double/2addr v6, v8

    .line 1053553
    iget-object v1, p0, LX/67r;->w:LX/31i;

    iget-wide v8, v1, LX/31i;->d:D

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v10

    add-double/2addr v2, v8

    .line 1053554
    sub-double v8, v2, v6

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v8, v10

    if-ltz v1, :cond_7

    .line 1053555
    iget-object v1, p0, LX/67r;->v:LX/31i;

    const-wide/16 v2, 0x0

    iput-wide v2, v1, LX/31i;->c:D

    .line 1053556
    iget-object v1, p0, LX/67r;->v:LX/31i;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, v1, LX/31i;->d:D

    .line 1053557
    :goto_3
    iget-object v1, p0, LX/67r;->v:LX/31i;

    const-wide/16 v2, 0x0

    iget-object v6, p0, LX/67r;->w:LX/31i;

    iget-wide v6, v6, LX/31i;->a:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v8, v4, v8

    sub-double/2addr v6, v8

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    iput-wide v2, v1, LX/31i;->a:D

    .line 1053558
    iget-object v1, p0, LX/67r;->v:LX/31i;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-object v6, p0, LX/67r;->w:LX/31i;

    iget-wide v6, v6, LX/31i;->b:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v8

    add-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    iput-wide v2, v1, LX/31i;->b:D

    goto/16 :goto_1

    .line 1053559
    :cond_7
    iget-object v1, p0, LX/67r;->v:LX/31i;

    invoke-static {v6, v7}, LX/67h;->a(D)D

    move-result-wide v6

    iput-wide v6, v1, LX/31i;->c:D

    .line 1053560
    iget-object v1, p0, LX/67r;->v:LX/31i;

    invoke-static {v2, v3}, LX/67h;->a(D)D

    move-result-wide v2

    iput-wide v2, v1, LX/31i;->d:D

    goto :goto_3

    .line 1053561
    :cond_8
    iget-object v0, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-eqz v0, :cond_9

    .line 1053562
    iget-object v0, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-static {v0}, LX/31l;->d(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1053563
    const/4 v0, 0x0

    iput-object v0, p0, LX/67r;->C:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1053564
    :cond_9
    iget-object v0, p0, LX/67r;->y:LX/68u;

    if-nez v0, :cond_5

    iget-object v0, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-nez v0, :cond_5

    .line 1053565
    new-instance v0, Lcom/facebook/android/maps/ClusterOverlay$2;

    invoke-direct {v0, p0}, Lcom/facebook/android/maps/ClusterOverlay$2;-><init>(LX/67r;)V

    iput-object v0, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1053566
    iget-object v0, p0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    const-wide/16 v2, 0x190

    invoke-static {v0, v2, v3}, LX/31l;->b(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;J)V

    goto/16 :goto_2
.end method

.method public final a(LX/698;)Z
    .locals 1

    .prologue
    .line 1053526
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1053505
    iget-object v0, p0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67i;

    .line 1053506
    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0}, LX/67m;->b()V

    goto :goto_0

    .line 1053507
    :cond_0
    return-void
.end method

.method public final b(LX/68u;)V
    .locals 0

    .prologue
    .line 1053524
    invoke-direct {p0, p1}, LX/67r;->d(LX/68u;)V

    .line 1053525
    return-void
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1053520
    iget-object v0, p0, LX/67r;->s:LX/67i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/67r;->s:LX/67i;

    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1053521
    iget-object v0, p0, LX/67r;->s:LX/67i;

    invoke-direct {p0, v0}, LX/67r;->a(LX/67i;)V

    .line 1053522
    const/4 v0, 0x1

    .line 1053523
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/698;)Z
    .locals 3

    .prologue
    .line 1053519
    iget-object v0, p0, LX/67r;->t:LX/67k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/67r;->t:LX/67k;

    iget-object v1, p0, LX/67r;->p:Ljava/util/Map;

    iget-object v2, p0, LX/67r;->s:LX/67i;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, LX/67k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/68u;)V
    .locals 0

    .prologue
    .line 1053517
    invoke-direct {p0, p1}, LX/67r;->d(LX/68u;)V

    .line 1053518
    return-void
.end method

.method public final c(FF)Z
    .locals 1

    .prologue
    .line 1053516
    iget-object v0, p0, LX/67r;->s:LX/67i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/67r;->s:LX/67i;

    iget-object v0, v0, LX/67i;->a:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->c(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/698;)Z
    .locals 3

    .prologue
    .line 1053515
    iget-object v0, p0, LX/67r;->u:LX/67l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/67r;->u:LX/67l;

    iget-object v1, p0, LX/67r;->p:Ljava/util/Map;

    iget-object v2, p0, LX/67r;->s:LX/67i;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, LX/67l;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1053512
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1053513
    iget-object v1, v0, LX/680;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1053514
    return-void
.end method

.method public final d(LX/698;)V
    .locals 0

    .prologue
    .line 1053510
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1053511
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1053508
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/67r;->a(LX/67i;)V

    .line 1053509
    return-void
.end method
