.class public final LX/6Ji;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Jg;
.implements LX/6Jh;


# instance fields
.field public final synthetic a:LX/6Jk;

.field public volatile b:Landroid/graphics/SurfaceTexture;

.field public c:J

.field public d:Z


# direct methods
.method public constructor <init>(LX/6Jk;)V
    .locals 1

    .prologue
    .line 1075640
    iput-object p1, p0, LX/6Ji;->a:LX/6Jk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075641
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Ji;->b:Landroid/graphics/SurfaceTexture;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/SurfaceTexture;LX/6Km;)V
    .locals 3

    .prologue
    .line 1075626
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1075627
    iput-object p1, p0, LX/6Ji;->b:Landroid/graphics/SurfaceTexture;

    .line 1075628
    new-instance v1, Lcom/facebook/cameracore/capturecoordinator/CameraCaptureCoordinator$CameraVideoInputDelegate$1;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/capturecoordinator/CameraCaptureCoordinator$CameraVideoInputDelegate$1;-><init>(LX/6Ji;)V

    const v2, 0x32da4a5e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1075629
    return-void
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 1075652
    iget-boolean v0, p0, LX/6Ji;->d:Z

    if-eqz v0, :cond_0

    .line 1075653
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, LX/6Ji;->c:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1075654
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1075642
    invoke-virtual {p0}, LX/6Ji;->dE_()V

    .line 1075643
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075644
    iput-object v1, v0, LX/6Jk;->k:LX/6Ik;

    .line 1075645
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075646
    iput-object v1, v0, LX/6Jk;->h:LX/6JC;

    .line 1075647
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075648
    iput-object v1, v0, LX/6Jk;->i:LX/6JB;

    .line 1075649
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075650
    iput-object v1, v0, LX/6Jk;->b:LX/6Ia;

    .line 1075651
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1075630
    const/4 v0, 0x1

    return v0
.end method

.method public final dE_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1075631
    :try_start_0
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->l:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x9c4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075632
    :goto_0
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075633
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->j()V

    .line 1075634
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Ji;->b:Landroid/graphics/SurfaceTexture;

    .line 1075635
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075636
    iput-boolean v4, v0, LX/6Jk;->f:Z

    .line 1075637
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    .line 1075638
    iput-boolean v4, v0, LX/6Jk;->g:Z

    .line 1075639
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1075623
    const/4 v0, 0x1

    return v0
.end method

.method public final getClock()LX/6Jg;
    .locals 0

    .prologue
    .line 1075622
    return-object p0
.end method

.method public final getInputHeight()I
    .locals 1

    .prologue
    .line 1075621
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->j:LX/6JR;

    iget v0, v0, LX/6JR;->b:I

    return v0
.end method

.method public final getInputWidth()I
    .locals 1

    .prologue
    .line 1075624
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->j:LX/6JR;

    iget v0, v0, LX/6JR;->a:I

    return v0
.end method

.method public final getRotationDegrees$134621()I
    .locals 1

    .prologue
    .line 1075625
    iget-object v0, p0, LX/6Ji;->a:LX/6Jk;

    iget-object v0, v0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->h()I

    move-result v0

    return v0
.end method
