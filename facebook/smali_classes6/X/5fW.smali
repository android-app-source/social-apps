.class public final LX/5fW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5f5;

.field public final synthetic b:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;LX/5f5;)V
    .locals 0

    .prologue
    .line 971693
    iput-object p1, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iput-object p2, p0, LX/5fW;->a:LX/5f5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/hardware/Camera$Size;)V
    .locals 5

    .prologue
    .line 971694
    iget-object v0, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iget-object v1, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iget v1, v1, Lcom/facebook/optic/CameraPreviewView;->b:I

    iget-object v2, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iget v2, v2, Lcom/facebook/optic/CameraPreviewView;->c:I

    iget v3, p1, Landroid/hardware/Camera$Size;->width:I

    iget v4, p1, Landroid/hardware/Camera$Size;->height:I

    .line 971695
    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/optic/CameraPreviewView;->a$redex0(Lcom/facebook/optic/CameraPreviewView;IIII)V

    .line 971696
    iget-object v0, p0, LX/5fW;->a:LX/5f5;

    .line 971697
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 971698
    iget-object v2, v1, LX/5fQ;->i:LX/5fM;

    move-object v1, v2

    .line 971699
    invoke-interface {v0, v1}, LX/5f5;->a(Ljava/lang/Object;)V

    .line 971700
    monitor-enter p0

    .line 971701
    :try_start_0
    iget-object v0, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    if-eqz v0, :cond_0

    .line 971702
    iget-object v0, p0, LX/5fW;->b:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    invoke-interface {v0}, LX/5fb;->a()V

    .line 971703
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 971704
    iget-object v0, p0, LX/5fW;->a:LX/5f5;

    invoke-interface {v0, p1}, LX/5f5;->a(Ljava/lang/Exception;)V

    .line 971705
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 971706
    check-cast p1, Landroid/hardware/Camera$Size;

    invoke-direct {p0, p1}, LX/5fW;->a(Landroid/hardware/Camera$Size;)V

    return-void
.end method
