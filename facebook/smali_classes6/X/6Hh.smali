.class public LX/6Hh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/6HZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/6He;

.field public volatile d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/6Hi;",
            ">;"
        }
    .end annotation
.end field

.field public e:[Landroid/hardware/Camera$Face;

.field public f:Z

.field public g:Landroid/os/HandlerThread;

.field public h:Landroid/os/Handler;

.field public i:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072722
    const-class v0, LX/6Hh;

    sput-object v0, LX/6Hh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6He;LX/0Zr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1072702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072703
    iput-boolean v2, p0, LX/6Hh;->f:Z

    .line 1072704
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Hh;->g:Landroid/os/HandlerThread;

    .line 1072705
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6Hh;->b:Ljava/util/ArrayList;

    .line 1072706
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6Hh;->d:Ljava/util/ArrayList;

    .line 1072707
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".background"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {p2, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/6Hh;->g:Landroid/os/HandlerThread;

    .line 1072708
    iget-object v0, p0, LX/6Hh;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1072709
    iget-object v0, p0, LX/6Hh;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1072710
    if-nez v0, :cond_0

    .line 1072711
    sget-object v0, LX/6Hh;->a:Ljava/lang/Class;

    const-string v1, "Error creating FaceTracker -- no looper could be retrieved frombackground thread"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072712
    :goto_0
    return-void

    .line 1072713
    :cond_0
    new-instance v1, LX/6Hg;

    invoke-direct {v1, p0, v0}, LX/6Hg;-><init>(LX/6Hh;Landroid/os/Looper;)V

    iput-object v1, p0, LX/6Hh;->h:Landroid/os/Handler;

    .line 1072714
    new-instance v0, LX/6Hf;

    invoke-direct {v0, p0}, LX/6Hf;-><init>(LX/6Hh;)V

    iput-object v0, p0, LX/6Hh;->i:Landroid/os/Handler;

    .line 1072715
    iput-object p1, p0, LX/6Hh;->c:LX/6He;

    .line 1072716
    iget-object v0, p0, LX/6Hh;->c:LX/6He;

    .line 1072717
    iget-object v1, v0, LX/6He;->g:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072718
    goto :goto_0
.end method

.method public static a(FFFF)D
    .locals 2

    .prologue
    .line 1072719
    sub-float v0, p2, p0

    .line 1072720
    sub-float v1, p3, p1

    .line 1072721
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a$redex0(LX/6Hh;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6Hi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1072698
    iget-object v0, p0, LX/6Hh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/6Hh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6HZ;

    .line 1072699
    invoke-interface {v0, p1}, LX/6HZ;->a(Ljava/util/List;)V

    .line 1072700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1072701
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(LX/6HZ;)V
    .locals 1

    .prologue
    .line 1072695
    if-eqz p1, :cond_0

    .line 1072696
    iget-object v0, p0, LX/6Hh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1072697
    :cond_0
    return-void
.end method
