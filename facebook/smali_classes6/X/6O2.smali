.class public LX/6O2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;",
        "Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:Ljava/text/SimpleDateFormat;

.field private static final d:Ljava/util/Calendar;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:LX/6Or;

.field public final g:LX/0lp;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/telephony/TelephonyManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1084714
    const-class v0, LX/6O2;

    sput-object v0, LX/6O2;->a:Ljava/lang/Class;

    .line 1084715
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, LX/6O2;->b:Ljava/text/SimpleDateFormat;

    .line 1084716
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "--MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, LX/6O2;->c:Ljava/text/SimpleDateFormat;

    .line 1084717
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, LX/6O2;->d:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6Or;LX/0lp;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/protocol/annotations/IsContactExtendedFieldsUploadEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/annotations/IsPhoneEmailSourcesUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6Or;",
            "LX/0lp;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084707
    iput-object p1, p0, LX/6O2;->e:Landroid/content/Context;

    .line 1084708
    iput-object p2, p0, LX/6O2;->f:LX/6Or;

    .line 1084709
    iput-object p3, p0, LX/6O2;->g:LX/0lp;

    .line 1084710
    iput-object p4, p0, LX/6O2;->h:LX/0Or;

    .line 1084711
    iput-object p5, p0, LX/6O2;->i:LX/0Or;

    .line 1084712
    iput-object p6, p0, LX/6O2;->j:Landroid/telephony/TelephonyManager;

    .line 1084713
    return-void
.end method

.method public static a(Lcom/facebook/contacts/model/PhonebookEmailAddress;)LX/0m9;
    .locals 4

    .prologue
    .line 1084697
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "label"

    .line 1084698
    iget v2, p0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084699
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1084700
    const-string v2, "home"

    .line 1084701
    :goto_0
    move-object v2, v2

    .line 1084702
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v1, "raw"

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookEmailAddress;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    return-object v0

    .line 1084703
    :cond_0
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1084704
    const-string v2, "work"

    goto :goto_0

    .line 1084705
    :cond_1
    const-string v2, "other"

    goto :goto_0
.end method

.method public static a(Lcom/facebook/contacts/model/PhonebookPhoneNumber;)LX/0m9;
    .locals 4

    .prologue
    .line 1084678
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "label"

    .line 1084679
    iget v2, p0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084680
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1084681
    const-string v2, "home"

    .line 1084682
    :goto_0
    move-object v2, v2

    .line 1084683
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v1, "raw"

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    return-object v0

    .line 1084684
    :cond_0
    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 1084685
    const-string v2, "work"

    goto :goto_0

    .line 1084686
    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1084687
    const-string v2, "mobile"

    goto :goto_0

    .line 1084688
    :cond_2
    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    .line 1084689
    const-string v2, "fax_home"

    goto :goto_0

    .line 1084690
    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 1084691
    const-string v2, "fax_work"

    goto :goto_0

    .line 1084692
    :cond_4
    const/16 v3, 0xd

    if-ne v2, v3, :cond_5

    .line 1084693
    const-string v2, "fax_other"

    goto :goto_0

    .line 1084694
    :cond_5
    const/4 v3, 0x6

    if-ne v2, v3, :cond_6

    .line 1084695
    const-string v2, "pager"

    goto :goto_0

    .line 1084696
    :cond_6
    const-string v2, "other"

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/6O2;
    .locals 8

    .prologue
    .line 1084675
    new-instance v1, LX/6O2;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/6Or;->b(LX/0QB;)LX/6Or;

    move-result-object v3

    check-cast v3, LX/6Or;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v4

    check-cast v4, LX/0lp;

    const/16 v5, 0x1479

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x309

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-direct/range {v1 .. v7}, LX/6O2;-><init>(Landroid/content/Context;LX/6Or;LX/0lp;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;)V

    .line 1084676
    move-object v0, v1

    .line 1084677
    return-object v0
.end method

.method public static a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1084674
    if-eqz p0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public static a(LX/0Px;LX/162;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/contacts/model/PhonebookContactField;",
            ">;",
            "LX/162;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1084718
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v2

    .line 1084719
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContactField;

    .line 1084720
    instance-of v4, v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    if-eqz v4, :cond_1

    .line 1084721
    check-cast v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1084722
    invoke-static {v0}, LX/6O2;->a(Lcom/facebook/contacts/model/PhonebookPhoneNumber;)LX/0m9;

    move-result-object v4

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->b:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1084723
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1084724
    :cond_1
    instance-of v4, v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    if-eqz v4, :cond_0

    .line 1084725
    check-cast v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    .line 1084726
    invoke-static {v0}, LX/6O2;->a(Lcom/facebook/contacts/model/PhonebookEmailAddress;)LX/0m9;

    move-result-object v4

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;->b:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 1084727
    :cond_2
    invoke-interface {v2}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0m9;

    .line 1084728
    const-string v3, "sources"

    const-string v4, ";"

    invoke-interface {v2, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    .line 1084729
    :cond_3
    return-void
.end method

.method public static a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1084671
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084672
    :goto_0
    return-void

    .line 1084673
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1084292
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084293
    :goto_0
    return-void

    .line 1084294
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1084295
    :catch_0
    move-exception v0

    .line 1084296
    sget-object v1, LX/6O2;->a:Ljava/lang/Class;

    const-string v2, "Got IOException when adding contact field key %s value %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/6O2;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V
    .locals 12

    .prologue
    .line 1084411
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    .line 1084412
    if-eqz v0, :cond_0

    .line 1084413
    const-string v1, "name"

    invoke-static {p2, v1, v0}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084414
    :cond_0
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1084415
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1084416
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1084417
    :goto_0
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1084418
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    .line 1084419
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1084420
    :goto_1
    iget-object v0, p0, LX/6O2;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1084421
    const-string v0, "photo"

    iget-boolean v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->k:Z

    invoke-static {v1}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084422
    const-string v0, "note"

    iget-boolean v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->l:Z

    invoke-static {v1}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084423
    const-string v0, "first_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084424
    const-string v0, "last_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084425
    const-string v0, "prefix"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->e:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084426
    const-string v0, "middle_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->f:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084427
    const-string v0, "suffix"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->g:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084428
    const-string v0, "phonetic_first_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->h:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084429
    const-string v0, "phonetic_last_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->j:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084430
    const-string v0, "phonetic_middle_name"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->i:Ljava/lang/String;

    invoke-static {p2, v0, v1}, LX/6O2;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084431
    const/4 v2, 0x0

    .line 1084432
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->o:LX/0Px;

    .line 1084433
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1084434
    :goto_2
    const/4 v2, 0x0

    .line 1084435
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->p:LX/0Px;

    .line 1084436
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1084437
    :goto_3
    const/4 v2, 0x0

    .line 1084438
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->q:LX/0Px;

    .line 1084439
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1084440
    :goto_4
    const/4 v2, 0x0

    .line 1084441
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->r:LX/0Px;

    .line 1084442
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1084443
    :goto_5
    const/4 v2, 0x0

    .line 1084444
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->s:LX/0Px;

    .line 1084445
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1084446
    :goto_6
    const/4 v2, 0x0

    .line 1084447
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->t:LX/0Px;

    .line 1084448
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1084449
    :goto_7
    const/4 v4, 0x0

    .line 1084450
    iget-object v5, p1, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    .line 1084451
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_31

    .line 1084452
    :goto_8
    invoke-static {p1, p2}, LX/6O2;->m(Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V

    .line 1084453
    const/4 v2, 0x0

    .line 1084454
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->w:LX/0Px;

    .line 1084455
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1084456
    :cond_1
    :goto_9
    return-void

    .line 1084457
    :cond_2
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084458
    iget-object v0, p0, LX/6O2;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v6}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1084459
    invoke-static {v3, v4}, LX/6O2;->a(LX/0Px;LX/162;)V

    .line 1084460
    :cond_3
    :try_start_0
    const-string v0, "phones"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1084461
    :catch_0
    move-exception v0

    .line 1084462
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookPhoneNumber"

    aput-object v3, v1, v2

    .line 1084463
    iget-object v2, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1084464
    aput-object v2, v1, v6

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1084465
    :cond_4
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_a
    if-ge v1, v5, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1084466
    invoke-static {v0}, LX/6O2;->a(Lcom/facebook/contacts/model/PhonebookPhoneNumber;)LX/0m9;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084467
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1084468
    :cond_5
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084469
    iget-object v0, p0, LX/6O2;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v6}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1084470
    invoke-static {v3, v4}, LX/6O2;->a(LX/0Px;LX/162;)V

    .line 1084471
    :cond_6
    :try_start_1
    const-string v0, "emails"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 1084472
    :catch_1
    move-exception v0

    .line 1084473
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookEmailAddress"

    aput-object v3, v1, v2

    .line 1084474
    iget-object v2, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1084475
    aput-object v2, v1, v6

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1084476
    :cond_7
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_b
    if-ge v1, v5, :cond_6

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    .line 1084477
    invoke-static {v0}, LX/6O2;->a(Lcom/facebook/contacts/model/PhonebookEmailAddress;)LX/0m9;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084478
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1084479
    :cond_8
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084480
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_c
    if-ge v1, v5, :cond_9

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;

    .line 1084481
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084482
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084483
    const/4 p0, 0x1

    if-ne v8, p0, :cond_a

    .line 1084484
    const-string v8, "home"

    .line 1084485
    :goto_d
    move-object v8, v8

    .line 1084486
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084487
    const-string v7, "data"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084488
    const-string v7, "label"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084489
    const-string v7, "protocol"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;->b:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084490
    const-string v7, "custom_protocol"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;->c:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084491
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1084493
    :cond_9
    :try_start_2
    const-string v0, "instant_messaging"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 1084494
    :catch_2
    move-exception v0

    .line 1084495
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookInstantMessaging"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084496
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084497
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 1084498
    :cond_a
    const/4 p0, 0x2

    if-ne v8, p0, :cond_b

    .line 1084499
    const-string v8, "work"

    goto :goto_d

    .line 1084500
    :cond_b
    const-string v8, "other"

    goto :goto_d

    .line 1084501
    :cond_c
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084502
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_e
    if-ge v1, v5, :cond_d

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookNickname;

    .line 1084503
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084504
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084505
    const/4 p0, 0x1

    if-ne v8, p0, :cond_e

    .line 1084506
    const-string v8, "default"

    .line 1084507
    :goto_f
    move-object v8, v8

    .line 1084508
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084509
    const-string v7, "name"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookNickname;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084510
    const-string v7, "label"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084511
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084512
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 1084513
    :cond_d
    :try_start_3
    const-string v0, "nick_names"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_3

    .line 1084514
    :catch_3
    move-exception v0

    .line 1084515
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookNickname"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084516
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084517
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 1084518
    :cond_e
    const/4 p0, 0x3

    if-ne v8, p0, :cond_f

    .line 1084519
    const-string v8, "maiden"

    goto :goto_f

    .line 1084520
    :cond_f
    const/4 p0, 0x5

    if-ne v8, p0, :cond_10

    .line 1084521
    const-string v8, "initials"

    goto :goto_f

    .line 1084522
    :cond_10
    const/4 p0, 0x4

    if-ne v8, p0, :cond_11

    .line 1084523
    const-string v8, "short"

    goto :goto_f

    .line 1084524
    :cond_11
    const-string v8, "other"

    goto :goto_f

    .line 1084525
    :cond_12
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084526
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_10
    if-ge v1, v5, :cond_13

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookAddress;

    .line 1084527
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084528
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084529
    const/4 p0, 0x1

    if-ne v8, p0, :cond_14

    .line 1084530
    const-string v8, "home"

    .line 1084531
    :goto_11
    move-object v8, v8

    .line 1084532
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084533
    const-string v7, "label"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084534
    const-string v7, "formatted_address"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084535
    const-string v7, "street"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->b:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084536
    const-string v7, "po_box"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->c:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084537
    const-string v7, "neighborhood"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->d:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084538
    const-string v7, "city"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->e:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084539
    const-string v7, "region"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->f:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084540
    const-string v7, "post_code"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookAddress;->g:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084541
    const-string v7, "country"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookAddress;->h:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084542
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084543
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 1084544
    :cond_13
    :try_start_4
    const-string v0, "address"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_4

    .line 1084545
    :catch_4
    move-exception v0

    .line 1084546
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookAddress"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084547
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084548
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 1084549
    :cond_14
    const/4 p0, 0x2

    if-ne v8, p0, :cond_15

    .line 1084550
    const-string v8, "work"

    goto :goto_11

    .line 1084551
    :cond_15
    const-string v8, "other"

    goto :goto_11

    .line 1084552
    :cond_16
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084553
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_12
    if-ge v1, v5, :cond_17

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookWebsite;

    .line 1084554
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084555
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084556
    const/4 p0, 0x1

    if-ne v8, p0, :cond_18

    .line 1084557
    const-string v8, "homepage"

    .line 1084558
    :goto_13
    move-object v8, v8

    .line 1084559
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084560
    const-string v7, "URL"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookWebsite;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084561
    const-string v7, "label"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084562
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084563
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 1084564
    :cond_17
    :try_start_5
    const-string v0, "website"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_5

    .line 1084565
    :catch_5
    move-exception v0

    .line 1084566
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookWebsite"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084567
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084568
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 1084569
    :cond_18
    const/4 p0, 0x2

    if-ne v8, p0, :cond_19

    .line 1084570
    const-string v8, "blog"

    goto :goto_13

    .line 1084571
    :cond_19
    const/4 p0, 0x3

    if-ne v8, p0, :cond_1a

    .line 1084572
    const-string v8, "profile"

    goto :goto_13

    .line 1084573
    :cond_1a
    const/4 p0, 0x4

    if-ne v8, p0, :cond_1b

    .line 1084574
    const-string v8, "home"

    goto :goto_13

    .line 1084575
    :cond_1b
    const/4 p0, 0x5

    if-ne v8, p0, :cond_1c

    .line 1084576
    const-string v8, "work"

    goto :goto_13

    .line 1084577
    :cond_1c
    const/4 p0, 0x6

    if-ne v8, p0, :cond_1d

    .line 1084578
    const-string v8, "ftp"

    goto :goto_13

    .line 1084579
    :cond_1d
    const-string v8, "other"

    goto :goto_13

    .line 1084580
    :cond_1e
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084581
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_14
    if-ge v1, v5, :cond_1f

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookRelation;

    .line 1084582
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084583
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084584
    const/4 p0, 0x1

    if-ne v8, p0, :cond_20

    .line 1084585
    const-string v8, "assistant"

    .line 1084586
    :goto_15
    move-object v8, v8

    .line 1084587
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084588
    const-string v7, "name"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookRelation;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084589
    const-string v7, "label"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084590
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084591
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 1084592
    :cond_1f
    :try_start_6
    const-string v0, "relation"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_6

    .line 1084593
    :catch_6
    move-exception v0

    .line 1084594
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookRelation"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084595
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084596
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 1084597
    :cond_20
    const/4 p0, 0x2

    if-ne v8, p0, :cond_21

    .line 1084598
    const-string v8, "brother"

    goto :goto_15

    .line 1084599
    :cond_21
    const/4 p0, 0x3

    if-ne v8, p0, :cond_22

    .line 1084600
    const-string v8, "child"

    goto :goto_15

    .line 1084601
    :cond_22
    const/4 p0, 0x4

    if-ne v8, p0, :cond_23

    .line 1084602
    const-string v8, "domestic_partner"

    goto :goto_15

    .line 1084603
    :cond_23
    const/4 p0, 0x5

    if-ne v8, p0, :cond_24

    .line 1084604
    const-string v8, "father"

    goto :goto_15

    .line 1084605
    :cond_24
    const/4 p0, 0x6

    if-ne v8, p0, :cond_25

    .line 1084606
    const-string v8, "friend"

    goto :goto_15

    .line 1084607
    :cond_25
    const/4 p0, 0x7

    if-ne v8, p0, :cond_26

    .line 1084608
    const-string v8, "manager"

    goto :goto_15

    .line 1084609
    :cond_26
    const/16 p0, 0x8

    if-ne v8, p0, :cond_27

    .line 1084610
    const-string v8, "mother"

    goto :goto_15

    .line 1084611
    :cond_27
    const/16 p0, 0x9

    if-ne v8, p0, :cond_28

    .line 1084612
    const-string v8, "parent"

    goto :goto_15

    .line 1084613
    :cond_28
    const/16 p0, 0xa

    if-ne v8, p0, :cond_29

    .line 1084614
    const-string v8, "partner"

    goto :goto_15

    .line 1084615
    :cond_29
    const/16 p0, 0xb

    if-ne v8, p0, :cond_2a

    .line 1084616
    const-string v8, "referred_by"

    goto :goto_15

    .line 1084617
    :cond_2a
    const/16 p0, 0xc

    if-ne v8, p0, :cond_2b

    .line 1084618
    const-string v8, "relative"

    goto :goto_15

    .line 1084619
    :cond_2b
    const/16 p0, 0xd

    if-ne v8, p0, :cond_2c

    .line 1084620
    const-string v8, "sister"

    goto/16 :goto_15

    .line 1084621
    :cond_2c
    const/16 p0, 0xe

    if-ne v8, p0, :cond_2d

    .line 1084622
    const-string v8, "spouse"

    goto/16 :goto_15

    .line 1084623
    :cond_2d
    const-string v8, "other"

    goto/16 :goto_15

    .line 1084624
    :cond_2e
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084625
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_16
    if-ge v1, v5, :cond_2f

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    .line 1084626
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084627
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084628
    const/4 p0, 0x1

    if-ne v8, p0, :cond_30

    .line 1084629
    const-string v8, "work"

    .line 1084630
    :goto_17
    move-object v8, v8

    .line 1084631
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084632
    const-string v7, "label"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084633
    const-string v7, "company"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084634
    const-string v7, "department"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084635
    const-string v7, "job_title"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084636
    const-string v7, "job_description"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084637
    const-string v7, "symbol"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084638
    const-string v7, "phonetic_name"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084639
    const-string v7, "office_location"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    invoke-static {v6, v7, v0}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084640
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084641
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 1084642
    :cond_2f
    :try_start_7
    const-string v0, "organization"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_7

    .line 1084643
    :catch_7
    move-exception v0

    .line 1084644
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookOrganization"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084645
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084646
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_7

    :cond_30
    const-string v8, "other"

    goto :goto_17

    .line 1084647
    :cond_31
    new-instance v6, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v2}, LX/162;-><init>(LX/0mC;)V

    .line 1084648
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    :goto_18
    if-ge v3, v7, :cond_32

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;

    .line 1084649
    new-instance v8, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v9}, LX/0m9;-><init>(LX/0mC;)V

    const-string v9, "number_times_contacted"

    iget v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->b:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "starred"

    iget-boolean v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->c:Z

    invoke-static {v10}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "last_time_contacted"

    iget-wide v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->d:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "custom_ringtone"

    iget-boolean v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->e:Z

    invoke-static {v10}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "in_visible_group"

    iget-boolean v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->f:Z

    invoke-static {v10}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "send_to_voicemail"

    iget-boolean v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->g:Z

    invoke-static {v10}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "is_user_profile"

    iget-boolean v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->h:Z

    invoke-static {v10}, LX/6O2;->a(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    .line 1084650
    const-string v9, "contact_id"

    iget-object v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->a:Ljava/lang/String;

    invoke-static {v8, v9, v10}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084651
    const-string v9, "account_type"

    .line 1084652
    iget-object v10, v2, Lcom/facebook/contacts/model/PhonebookContactMetadata;->i:Ljava/lang/String;

    move-object v2, v10

    .line 1084653
    invoke-static {v8, v9, v2}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084654
    invoke-virtual {v6, v8}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084655
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_18

    .line 1084656
    :cond_32
    :try_start_8
    const-string v2, "meta_data"

    invoke-virtual {p2, v2, v6}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    goto/16 :goto_8

    .line 1084657
    :catch_8
    move-exception v2

    .line 1084658
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "appendPhonebookContactMetadata"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 1084659
    iget-object v5, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1084660
    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_8

    .line 1084661
    :cond_33
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084662
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_19
    if-ge v1, v5, :cond_34

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;

    .line 1084663
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "username"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    const-string v7, "phone_number"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 1084664
    invoke-virtual {v4, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084665
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_19

    .line 1084666
    :cond_34
    :try_start_9
    const-string v0, "whatsapp_profile"

    invoke-virtual {p2, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    goto/16 :goto_9

    .line 1084667
    :catch_9
    move-exception v0

    .line 1084668
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "appendPhonebookWhatsappProfile"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 1084669
    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1084670
    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_9
.end method

.method public static varargs a(Ljava/lang/Throwable;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1084409
    sget-object v0, LX/6O2;->a:Ljava/lang/Class;

    const-string v1, "Got Exception when %s for contact %s"

    invoke-static {v0, p0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1084410
    return-void
.end method

.method private static m(Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 1084373
    iget-object v3, p0, Lcom/facebook/contacts/model/PhonebookContact;->u:LX/0Px;

    .line 1084374
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084375
    :goto_0
    return-void

    .line 1084376
    :cond_0
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1084377
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookEvent;

    .line 1084378
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    const-string v7, "type"

    .line 1084379
    iget v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    .line 1084380
    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    .line 1084381
    const-string v8, "anniversary"

    .line 1084382
    :goto_2
    move-object v8, v8

    .line 1084383
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    .line 1084384
    const-string v7, "label"

    iget-object v8, v0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    invoke-static {v6, v7, v8}, LX/6O2;->a(LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    .line 1084385
    :try_start_0
    iget-object v7, v0, Lcom/facebook/contacts/model/PhonebookEvent;->a:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_2

    .line 1084386
    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    sget-object v8, LX/6O2;->c:Ljava/text/SimpleDateFormat;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookEvent;->a:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1084387
    :cond_1
    :goto_3
    const-string v0, "month"

    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1084388
    const-string v0, "day"

    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1084389
    :goto_4
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    .line 1084390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1084391
    :cond_2
    :try_start_1
    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    sget-object v8, LX/6O2;->b:Ljava/text/SimpleDateFormat;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookEvent;->a:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1084392
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    const/4 v7, 0x0

    invoke-static {v0, v7}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContactMetadata;

    .line 1084393
    if-eqz v0, :cond_6

    iget-boolean v0, v0, Lcom/facebook/contacts/model/PhonebookContactMetadata;->h:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 1084394
    if-nez v0, :cond_1

    .line 1084395
    const-string v0, "date"

    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1084396
    const-string v0, "year"

    sget-object v7, LX/6O2;->d:Ljava/util/Calendar;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 1084397
    :catch_0
    move-exception v0

    .line 1084398
    new-array v7, v13, [Ljava/lang/Object;

    const-string v8, "appendPhonebookEvent"

    aput-object v8, v7, v2

    .line 1084399
    iget-object v8, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1084400
    aput-object v8, v7, v12

    invoke-static {v0, v7}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_4

    .line 1084401
    :cond_3
    :try_start_2
    const-string v0, "event"

    invoke-virtual {p1, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1084402
    :catch_1
    move-exception v0

    .line 1084403
    new-array v1, v13, [Ljava/lang/Object;

    const-string v3, "appendPhonebookEvent"

    aput-object v3, v1, v2

    .line 1084404
    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1084405
    aput-object v2, v1, v12

    invoke-static {v0, v1}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1084406
    :cond_4
    const/4 v9, 0x3

    if-ne v8, v9, :cond_5

    .line 1084407
    const-string v8, "birthday"

    goto/16 :goto_2

    .line 1084408
    :cond_5
    const-string v8, "other"

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 1084306
    check-cast p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;

    .line 1084307
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1084308
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084309
    iget-object v0, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1084310
    if-eqz v0, :cond_0

    .line 1084311
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "import_id"

    .line 1084312
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1084313
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084314
    :cond_0
    iget-object v0, p0, LX/6O2;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 1084315
    iget-object v1, p0, LX/6O2;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 1084316
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1084317
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "sim_country"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084318
    :cond_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1084319
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "network_country"

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084320
    :cond_2
    iget-object v0, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->a:LX/6OZ;

    move-object v0, v0

    .line 1084321
    if-eqz v0, :cond_3

    .line 1084322
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "flow"

    .line 1084323
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->a:LX/6OZ;

    move-object v2, v2

    .line 1084324
    invoke-virtual {v2}, LX/6OZ;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084325
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_full_upload"

    .line 1084326
    iget-boolean v2, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->d:Z

    move v2, v2

    .line 1084327
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084328
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "contacts"

    .line 1084329
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->c:LX/0Px;

    move-object v2, v2

    .line 1084330
    new-instance v6, Ljava/io/StringWriter;

    invoke-direct {v6}, Ljava/io/StringWriter;-><init>()V

    .line 1084331
    iget-object v3, p0, LX/6O2;->g:LX/0lp;

    invoke-virtual {v3, v6}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v7

    .line 1084332
    invoke-virtual {v7}, LX/0nX;->d()V

    .line 1084333
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_4

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/server/UploadBulkContactChange;

    .line 1084334
    :try_start_0
    invoke-virtual {v7}, LX/0nX;->f()V

    .line 1084335
    const-string v9, "record_id"

    .line 1084336
    iget-object v10, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    move-object v10, v10

    .line 1084337
    invoke-virtual {v7, v9, v10}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084338
    const-string v9, "modifier"

    .line 1084339
    sget-object v10, LX/6O1;->a:[I

    .line 1084340
    iget-object v11, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    move-object v11, v11

    .line 1084341
    invoke-virtual {v11}, LX/6ON;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 1084342
    const/4 v10, 0x0

    :goto_1
    move-object v10, v10

    .line 1084343
    invoke-virtual {v7, v9, v10}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084344
    const-string v9, "signature"

    .line 1084345
    iget-object v10, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->b:Ljava/lang/String;

    move-object v10, v10

    .line 1084346
    invoke-virtual {v7, v9, v10}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084347
    iget-object v9, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    move-object v9, v9

    .line 1084348
    sget-object v10, LX/6ON;->DELETE:LX/6ON;

    if-eq v9, v10, :cond_5

    .line 1084349
    iget-object v9, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object v9, v9

    .line 1084350
    invoke-static {p0, v9, v7}, LX/6O2;->a(LX/6O2;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V

    .line 1084351
    :goto_2
    invoke-virtual {v7}, LX/0nX;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1084352
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1084353
    :cond_4
    invoke-virtual {v7}, LX/0nX;->e()V

    .line 1084354
    invoke-virtual {v7}, LX/0nX;->flush()V

    .line 1084355
    invoke-virtual {v6}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1084356
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084357
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "android_id"

    iget-object v2, p0, LX/6O2;->e:Landroid/content/Context;

    invoke-static {v2}, LX/6Or;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084358
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "phone_id"

    iget-object v2, p0, LX/6O2;->f:LX/6Or;

    invoke-virtual {v2}, LX/6Or;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084359
    new-instance v0, LX/14N;

    const-string v1, "FriendFinderMobileContinuousSync"

    const-string v2, "POST"

    const-string v3, "method/friendFinder.mobilecontinuoussync"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1084360
    :cond_5
    :try_start_1
    new-instance v9, LX/6NG;

    .line 1084361
    iget-object v10, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    move-object v10, v10

    .line 1084362
    invoke-direct {v9, v10}, LX/6NG;-><init>(Ljava/lang/String;)V

    const-string v10, "None"

    .line 1084363
    iput-object v10, v9, LX/6NG;->b:Ljava/lang/String;

    .line 1084364
    move-object v9, v9

    .line 1084365
    invoke-virtual {v9}, LX/6NG;->c()Lcom/facebook/contacts/model/PhonebookContact;

    move-result-object v9

    invoke-static {p0, v9, v7}, LX/6O2;->a(LX/6O2;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1084366
    :catch_0
    move-exception v9

    .line 1084367
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string p1, "appendContactChange"

    aput-object p1, v10, v11

    const/4 v11, 0x1

    .line 1084368
    iget-object p1, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1084369
    aput-object p1, v10, v11

    invoke-static {v9, v10}, LX/6O2;->a(Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_3

    .line 1084370
    :pswitch_0
    const-string v10, "0"

    goto/16 :goto_1

    .line 1084371
    :pswitch_1
    const-string v10, "1"

    goto/16 :goto_1

    .line 1084372
    :pswitch_2
    const-string v10, "2"

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1084297
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1084298
    const-string v1, "import_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1084299
    const-string v2, "server_status"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 1084300
    new-instance v2, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;

    .line 1084301
    sget-object p0, LX/6Ob;->UNKNOWN:LX/6Ob;

    .line 1084302
    invoke-static {}, LX/6Ob;->values()[LX/6Ob;

    move-result-object p1

    array-length p1, p1

    if-ge v0, p1, :cond_0

    .line 1084303
    invoke-static {}, LX/6Ob;->values()[LX/6Ob;

    move-result-object p0

    aget-object p0, p0, v0

    .line 1084304
    :cond_0
    move-object v0, p0

    .line 1084305
    invoke-direct {v2, v1, v0}, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;-><init>(Ljava/lang/String;LX/6Ob;)V

    return-object v2
.end method
