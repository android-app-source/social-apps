.class public final LX/66R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64r;


# instance fields
.field private final a:LX/64w;

.field public b:LX/65W;

.field public c:Z

.field public volatile d:Z


# direct methods
.method public constructor <init>(LX/64w;)V
    .locals 0

    .prologue
    .line 1050120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050121
    iput-object p1, p0, LX/66R;->a:LX/64w;

    .line 1050122
    return-void
.end method

.method private a(LX/64q;)LX/64R;
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 1050123
    invoke-virtual {p1}, LX/64q;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1050124
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050125
    iget-object v1, v0, LX/64w;->l:Ljavax/net/ssl/SSLSocketFactory;

    move-object v5, v1

    .line 1050126
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050127
    iget-object v1, v0, LX/64w;->n:Ljavax/net/ssl/HostnameVerifier;

    move-object v6, v1

    .line 1050128
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050129
    iget-object v1, v0, LX/64w;->o:LX/64a;

    move-object v7, v1

    .line 1050130
    :goto_0
    new-instance v0, LX/64R;

    .line 1050131
    iget-object v1, p1, LX/64q;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1050132
    iget v2, p1, LX/64q;->f:I

    move v2, v2

    .line 1050133
    iget-object v3, p0, LX/66R;->a:LX/64w;

    .line 1050134
    iget-object v4, v3, LX/64w;->s:LX/64j;

    move-object v3, v4

    .line 1050135
    iget-object v4, p0, LX/66R;->a:LX/64w;

    .line 1050136
    iget-object v8, v4, LX/64w;->k:Ljavax/net/SocketFactory;

    move-object v4, v8

    .line 1050137
    iget-object v8, p0, LX/66R;->a:LX/64w;

    .line 1050138
    iget-object v9, v8, LX/64w;->p:LX/64S;

    move-object v8, v9

    .line 1050139
    iget-object v9, p0, LX/66R;->a:LX/64w;

    .line 1050140
    iget-object v10, v9, LX/64w;->b:Ljava/net/Proxy;

    move-object v9, v10

    .line 1050141
    iget-object v10, p0, LX/66R;->a:LX/64w;

    .line 1050142
    iget-object v11, v10, LX/64w;->c:Ljava/util/List;

    move-object v10, v11

    .line 1050143
    iget-object v11, p0, LX/66R;->a:LX/64w;

    .line 1050144
    iget-object v12, v11, LX/64w;->d:Ljava/util/List;

    move-object v11, v12

    .line 1050145
    iget-object v12, p0, LX/66R;->a:LX/64w;

    .line 1050146
    iget-object p0, v12, LX/64w;->g:Ljava/net/ProxySelector;

    move-object v12, p0

    .line 1050147
    invoke-direct/range {v0 .. v12}, LX/64R;-><init>(Ljava/lang/String;ILX/64j;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;LX/64a;LX/64S;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V

    .line 1050148
    return-object v0

    :cond_0
    move-object v6, v7

    move-object v5, v7

    goto :goto_0
.end method

.method private a(LX/655;)LX/650;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1050149
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1050150
    :cond_0
    iget-object v0, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v0}, LX/65W;->b()LX/65S;

    move-result-object v0

    .line 1050151
    if-eqz v0, :cond_2

    .line 1050152
    iget-object v2, v0, LX/65S;->k:LX/657;

    move-object v0, v2

    .line 1050153
    :goto_0
    iget v2, p1, LX/655;->c:I

    move v2, v2

    .line 1050154
    iget-object v3, p1, LX/655;->a:LX/650;

    move-object v3, v3

    .line 1050155
    iget-object v4, v3, LX/650;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1050156
    sparse-switch v2, :sswitch_data_0

    .line 1050157
    :cond_1
    :goto_1
    return-object v1

    :cond_2
    move-object v0, v1

    .line 1050158
    goto :goto_0

    .line 1050159
    :sswitch_0
    if-eqz v0, :cond_3

    .line 1050160
    iget-object v1, v0, LX/657;->b:Ljava/net/Proxy;

    move-object v0, v1

    .line 1050161
    :goto_2
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_4

    .line 1050162
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050163
    :cond_3
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050164
    iget-object v1, v0, LX/64w;->b:Ljava/net/Proxy;

    move-object v0, v1

    .line 1050165
    goto :goto_2

    .line 1050166
    :cond_4
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050167
    iget-object v1, v0, LX/64w;->p:LX/64S;

    move-object v0, v1

    .line 1050168
    invoke-interface {v0}, LX/64S;->a()LX/650;

    move-result-object v1

    goto :goto_1

    .line 1050169
    :sswitch_1
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050170
    iget-object v1, v0, LX/64w;->q:LX/64S;

    move-object v0, v1

    .line 1050171
    invoke-interface {v0}, LX/64S;->a()LX/650;

    move-result-object v1

    goto :goto_1

    .line 1050172
    :sswitch_2
    const-string v0, "GET"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "HEAD"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1050173
    :cond_5
    :sswitch_3
    iget-object v0, p0, LX/66R;->a:LX/64w;

    .line 1050174
    iget-boolean v2, v0, LX/64w;->u:Z

    move v0, v2

    .line 1050175
    if-eqz v0, :cond_1

    .line 1050176
    const-string v0, "Location"

    invoke-virtual {p1, v0}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1050177
    if-eqz v0, :cond_1

    .line 1050178
    iget-object v2, p1, LX/655;->a:LX/650;

    move-object v2, v2

    .line 1050179
    iget-object v4, v2, LX/650;->a:LX/64q;

    move-object v2, v4

    .line 1050180
    invoke-virtual {v2, v0}, LX/64q;->c(Ljava/lang/String;)LX/64q;

    move-result-object v0

    .line 1050181
    if-eqz v0, :cond_1

    .line 1050182
    iget-object v2, v0, LX/64q;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1050183
    iget-object v4, p1, LX/655;->a:LX/650;

    move-object v4, v4

    .line 1050184
    iget-object v5, v4, LX/650;->a:LX/64q;

    move-object v4, v5

    .line 1050185
    iget-object v5, v4, LX/64q;->b:Ljava/lang/String;

    move-object v4, v5

    .line 1050186
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1050187
    if-nez v2, :cond_6

    iget-object v2, p0, LX/66R;->a:LX/64w;

    .line 1050188
    iget-boolean v4, v2, LX/64w;->t:Z

    move v2, v4

    .line 1050189
    if-eqz v2, :cond_1

    .line 1050190
    :cond_6
    iget-object v2, p1, LX/655;->a:LX/650;

    move-object v2, v2

    .line 1050191
    invoke-virtual {v2}, LX/650;->newBuilder()LX/64z;

    move-result-object v2

    .line 1050192
    invoke-static {v3}, LX/66N;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1050193
    const-string v4, "PROPFIND"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 1050194
    if-eqz v4, :cond_9

    .line 1050195
    const-string v3, "GET"

    invoke-virtual {v2, v3, v1}, LX/64z;->a(Ljava/lang/String;LX/651;)LX/64z;

    .line 1050196
    :goto_4
    const-string v1, "Transfer-Encoding"

    invoke-virtual {v2, v1}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    .line 1050197
    const-string v1, "Content-Length"

    invoke-virtual {v2, v1}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    .line 1050198
    const-string v1, "Content-Type"

    invoke-virtual {v2, v1}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    .line 1050199
    :cond_7
    invoke-static {p1, v0}, LX/66R;->a(LX/655;LX/64q;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1050200
    const-string v1, "Authorization"

    invoke-virtual {v2, v1}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    .line 1050201
    :cond_8
    invoke-virtual {v2, v0}, LX/64z;->a(LX/64q;)LX/64z;

    move-result-object v0

    invoke-virtual {v0}, LX/64z;->b()LX/650;

    move-result-object v1

    goto/16 :goto_1

    .line 1050202
    :cond_9
    invoke-virtual {v2, v3, v1}, LX/64z;->a(Ljava/lang/String;LX/651;)LX/64z;

    goto :goto_4

    .line 1050203
    :sswitch_4
    iget-object v0, p1, LX/655;->a:LX/650;

    move-object v0, v0

    .line 1050204
    iget-object v2, v0, LX/650;->d:LX/651;

    move-object v0, v2

    .line 1050205
    instance-of v0, v0, LX/66T;

    if-nez v0, :cond_1

    .line 1050206
    iget-object v0, p1, LX/655;->a:LX/650;

    move-object v1, v0

    .line 1050207
    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_3
        0x12d -> :sswitch_3
        0x12e -> :sswitch_3
        0x12f -> :sswitch_3
        0x133 -> :sswitch_2
        0x134 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
        0x198 -> :sswitch_4
    .end sparse-switch
.end method

.method private static a(LX/655;LX/64q;)Z
    .locals 3

    .prologue
    .line 1050208
    iget-object v0, p0, LX/655;->a:LX/650;

    move-object v0, v0

    .line 1050209
    iget-object v1, v0, LX/650;->a:LX/64q;

    move-object v0, v1

    .line 1050210
    iget-object v1, v0, LX/64q;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1050211
    iget-object v2, p1, LX/64q;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1050212
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1050213
    iget v1, v0, LX/64q;->f:I

    move v1, v1

    .line 1050214
    iget v2, p1, LX/64q;->f:I

    move v2, v2

    .line 1050215
    if-ne v1, v2, :cond_0

    .line 1050216
    iget-object v1, v0, LX/64q;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1050217
    iget-object v1, p1, LX/64q;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1050218
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1050219
    goto :goto_0
.end method

.method private a(Ljava/io/IOException;ZLX/650;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1050220
    iget-object v1, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v1, p1}, LX/65W;->a(Ljava/io/IOException;)V

    .line 1050221
    iget-object v1, p0, LX/66R;->a:LX/64w;

    .line 1050222
    iget-boolean v2, v1, LX/64w;->v:Z

    move v1, v2

    .line 1050223
    if-nez v1, :cond_1

    .line 1050224
    :cond_0
    :goto_0
    return v0

    .line 1050225
    :cond_1
    if-nez p2, :cond_2

    .line 1050226
    iget-object v1, p3, LX/650;->d:LX/651;

    move-object v1, v1

    .line 1050227
    instance-of v1, v1, LX/66T;

    if-nez v1, :cond_0

    .line 1050228
    :cond_2
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1050229
    instance-of p3, p1, Ljava/net/ProtocolException;

    if-eqz p3, :cond_6

    .line 1050230
    :cond_3
    :goto_1
    move v1, v1

    .line 1050231
    if-eqz v1, :cond_0

    .line 1050232
    iget-object v1, p0, LX/66R;->b:LX/65W;

    .line 1050233
    iget-object v2, v1, LX/65W;->b:LX/657;

    if-nez v2, :cond_5

    iget-object v2, v1, LX/65W;->d:LX/65V;

    .line 1050234
    invoke-static {v2}, LX/65V;->e(LX/65V;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1050235
    invoke-static {v2}, LX/65V;->c(LX/65V;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1050236
    invoke-static {v2}, LX/65V;->g(LX/65V;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_4
    const/4 v1, 0x1

    :goto_2
    move v2, v1

    .line 1050237
    if-eqz v2, :cond_9

    :cond_5
    const/4 v2, 0x1

    :goto_3
    move v1, v2

    .line 1050238
    if-eqz v1, :cond_0

    .line 1050239
    const/4 v0, 0x1

    goto :goto_0

    .line 1050240
    :cond_6
    instance-of p3, p1, Ljava/io/InterruptedIOException;

    if-eqz p3, :cond_7

    .line 1050241
    instance-of p3, p1, Ljava/net/SocketTimeoutException;

    if-eqz p3, :cond_3

    if-eqz p2, :cond_3

    move v1, v2

    goto :goto_1

    .line 1050242
    :cond_7
    instance-of p3, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz p3, :cond_8

    .line 1050243
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p3

    instance-of p3, p3, Ljava/security/cert/CertificateException;

    if-nez p3, :cond_3

    .line 1050244
    :cond_8
    instance-of p3, p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-nez p3, :cond_3

    move v1, v2

    .line 1050245
    goto :goto_1

    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    .line 1050246
    goto :goto_2
.end method


# virtual methods
.method public final a(LX/66O;)LX/655;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1050247
    iget-object v0, p1, LX/66O;->f:LX/650;

    move-object v2, v0

    .line 1050248
    new-instance v4, LX/65W;

    iget-object v5, p0, LX/66R;->a:LX/64w;

    .line 1050249
    iget-object v0, v5, LX/64w;->r:LX/64c;

    move-object v5, v0

    .line 1050250
    iget-object v0, v2, LX/650;->a:LX/64q;

    move-object v6, v0

    .line 1050251
    invoke-direct {p0, v6}, LX/66R;->a(LX/64q;)LX/64R;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/65W;-><init>(LX/64c;LX/64R;)V

    iput-object v4, p0, LX/66R;->b:LX/65W;

    move v4, v1

    move-object v5, v2

    move-object v2, v3

    .line 1050252
    :cond_0
    :goto_0
    iget-boolean v1, p0, LX/66R;->d:Z

    if-eqz v1, :cond_1

    .line 1050253
    iget-object v1, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v1}, LX/65W;->c()V

    .line 1050254
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1050255
    :cond_1
    :try_start_0
    move-object v0, p1

    check-cast v0, LX/66O;

    move-object v1, v0

    iget-object v6, p0, LX/66R;->b:LX/65W;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v6, v7, v8}, LX/66O;->a(LX/650;LX/65W;LX/66G;LX/65S;)LX/655;
    :try_end_0
    .catch LX/65U; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1050256
    if-eqz v2, :cond_3

    .line 1050257
    invoke-virtual {v1}, LX/655;->newBuilder()LX/654;

    move-result-object v1

    .line 1050258
    invoke-virtual {v2}, LX/655;->newBuilder()LX/654;

    move-result-object v2

    .line 1050259
    iput-object v3, v2, LX/654;->g:LX/656;

    .line 1050260
    move-object v2, v2

    .line 1050261
    invoke-virtual {v2}, LX/654;->a()LX/655;

    move-result-object v2

    .line 1050262
    if-eqz v2, :cond_2

    invoke-static {v2}, LX/654;->d(LX/655;)V

    .line 1050263
    :cond_2
    iput-object v2, v1, LX/654;->j:LX/655;

    .line 1050264
    move-object v1, v1

    .line 1050265
    invoke-virtual {v1}, LX/654;->a()LX/655;

    move-result-object v1

    .line 1050266
    :cond_3
    invoke-direct {p0, v1}, LX/66R;->a(LX/655;)LX/650;

    move-result-object v5

    .line 1050267
    if-nez v5, :cond_5

    .line 1050268
    iget-boolean v2, p0, LX/66R;->c:Z

    if-nez v2, :cond_4

    .line 1050269
    iget-object v2, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v2}, LX/65W;->c()V

    .line 1050270
    :cond_4
    return-object v1

    .line 1050271
    :catch_0
    move-exception v1

    .line 1050272
    :try_start_1
    iget-object v0, v1, LX/65U;->lastException:Ljava/io/IOException;

    move-object v6, v0

    .line 1050273
    const/4 v7, 0x1

    invoke-direct {p0, v6, v7, v5}, LX/66R;->a(Ljava/io/IOException;ZLX/650;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1050274
    iget-object v0, v1, LX/65U;->lastException:Ljava/io/IOException;

    move-object v1, v0

    .line 1050275
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1050276
    :catchall_0
    move-exception v1

    .line 1050277
    iget-object v2, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v2, v3}, LX/65W;->a(Ljava/io/IOException;)V

    .line 1050278
    iget-object v2, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v2}, LX/65W;->c()V

    throw v1

    .line 1050279
    :catch_1
    move-exception v1

    .line 1050280
    const/4 v6, 0x0

    :try_start_2
    invoke-direct {p0, v1, v6, v5}, LX/66R;->a(Ljava/io/IOException;ZLX/650;)Z

    move-result v6

    if-nez v6, :cond_0

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1050281
    :cond_5
    iget-object v0, v1, LX/655;->g:LX/656;

    move-object v2, v0

    .line 1050282
    invoke-static {v2}, LX/65A;->a(Ljava/io/Closeable;)V

    .line 1050283
    add-int/lit8 v2, v4, 0x1

    const/16 v4, 0x14

    if-le v2, v4, :cond_6

    .line 1050284
    iget-object v1, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v1}, LX/65W;->c()V

    .line 1050285
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Too many follow-up requests: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1050286
    :cond_6
    iget-object v0, v5, LX/650;->d:LX/651;

    move-object v4, v0

    .line 1050287
    instance-of v4, v4, LX/66T;

    if-eqz v4, :cond_7

    .line 1050288
    new-instance v2, Ljava/net/HttpRetryException;

    const-string v3, "Cannot retry streamed HTTP body"

    .line 1050289
    iget v0, v1, LX/655;->c:I

    move v1, v0

    .line 1050290
    invoke-direct {v2, v3, v1}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 1050291
    :cond_7
    iget-object v0, v5, LX/650;->a:LX/64q;

    move-object v4, v0

    .line 1050292
    invoke-static {v1, v4}, LX/66R;->a(LX/655;LX/64q;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1050293
    iget-object v4, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v4}, LX/65W;->c()V

    .line 1050294
    new-instance v4, LX/65W;

    iget-object v6, p0, LX/66R;->a:LX/64w;

    .line 1050295
    iget-object v0, v6, LX/64w;->r:LX/64c;

    move-object v6, v0

    .line 1050296
    iget-object v0, v5, LX/650;->a:LX/64q;

    move-object v7, v0

    .line 1050297
    invoke-direct {p0, v7}, LX/66R;->a(LX/64q;)LX/64R;

    move-result-object v7

    invoke-direct {v4, v6, v7}, LX/65W;-><init>(LX/64c;LX/64R;)V

    iput-object v4, p0, LX/66R;->b:LX/65W;

    :cond_8
    move v4, v2

    move-object v2, v1

    .line 1050298
    goto/16 :goto_0

    .line 1050299
    :cond_9
    iget-object v4, p0, LX/66R;->b:LX/65W;

    invoke-virtual {v4}, LX/65W;->a()LX/66G;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1050300
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Closing the body of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " didn\'t close its backing stream. Bad interceptor?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
