.class public LX/6Fd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/6Fg;",
        "LX/6Fh;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0W9;

.field private final c:LX/0Zh;

.field private final d:LX/0W3;

.field private final e:LX/0tK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1068571
    const-class v0, LX/6Fd;

    sput-object v0, LX/6Fd;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W9;LX/0W3;LX/0tK;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1068572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068573
    iput-object p1, p0, LX/6Fd;->b:LX/0W9;

    .line 1068574
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    iput-object v0, p0, LX/6Fd;->c:LX/0Zh;

    .line 1068575
    iput-object p2, p0, LX/6Fd;->d:LX/0W3;

    .line 1068576
    iput-object p3, p0, LX/6Fd;->e:LX/0tK;

    .line 1068577
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 13

    .prologue
    .line 1068578
    check-cast p1, LX/6Fg;

    .line 1068579
    iget-object v0, p0, LX/6Fd;->c:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v8

    .line 1068580
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0nA;->a(LX/0nD;)V

    .line 1068581
    const-string v0, "desc"

    .line 1068582
    iget-object v1, p1, LX/6Fg;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1068583
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068584
    const-string v0, "log"

    .line 1068585
    iget-object v1, p1, LX/6Fg;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1068586
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068587
    const-string v0, "format"

    const-string v1, "json-strings"

    .line 1068588
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068589
    const-string v0, "network_type"

    .line 1068590
    iget-object v1, p1, LX/6Fg;->n:Ljava/lang/String;

    move-object v1, v1

    .line 1068591
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068592
    const-string v0, "network_subtype"

    .line 1068593
    iget-object v1, p1, LX/6Fg;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1068594
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068595
    const-string v0, "build_num"

    .line 1068596
    iget-object v1, p1, LX/6Fg;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1068597
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068598
    iget-object v0, p1, LX/6Fg;->q:LX/6Fb;

    move-object v0, v0

    .line 1068599
    if-eqz v0, :cond_0

    .line 1068600
    iget-object v0, p1, LX/6Fg;->q:LX/6Fb;

    move-object v0, v0

    .line 1068601
    sget-object v1, LX/6Fb;->DEFAULT:LX/6Fb;

    if-eq v0, v1, :cond_0

    .line 1068602
    const-string v0, "source"

    .line 1068603
    iget-object v1, p1, LX/6Fg;->q:LX/6Fb;

    move-object v1, v1

    .line 1068604
    invoke-virtual {v1}, LX/6Fb;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1068605
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068606
    :cond_0
    iget-object v0, p1, LX/6Fg;->f:LX/0P1;

    move-object v2, v0

    .line 1068607
    if-eqz v2, :cond_2

    .line 1068608
    invoke-virtual {v2}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068609
    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1068610
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1068611
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068612
    goto :goto_0

    .line 1068613
    :cond_2
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1068614
    const-string v0, "Git_Hash"

    .line 1068615
    iget-object v2, p1, LX/6Fg;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1068616
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068617
    iget-object v0, p1, LX/6Fg;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1068618
    if-eqz v0, :cond_3

    .line 1068619
    const-string v2, "Git_Branch"

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068620
    :cond_3
    iget-object v0, p1, LX/6Fg;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1068621
    if-eqz v0, :cond_4

    .line 1068622
    const-string v2, "Build_Time"

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068623
    :cond_4
    const-string v0, "Report_ID"

    .line 1068624
    iget-object v2, p1, LX/6Fg;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1068625
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068626
    const-string v0, "Build_Num"

    .line 1068627
    iget-object v2, p1, LX/6Fg;->k:Ljava/lang/String;

    move-object v2, v2

    .line 1068628
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068629
    const-string v0, "OS_Version"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068630
    const-string v0, "Manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068631
    const-string v0, "Model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068632
    const-string v0, "Device Locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068633
    const-string v0, "App Locale"

    iget-object v2, p0, LX/6Fd;->b:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068634
    const-string v0, "Zombie(s)"

    .line 1068635
    iget-object v2, p1, LX/6Fg;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1068636
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068637
    const-string v2, "Sent_On_Retry"

    .line 1068638
    iget-boolean v0, p1, LX/6Fg;->r:Z

    move v0, v0

    .line 1068639
    if-eqz v0, :cond_8

    const-string v0, "True"

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068640
    const-string v0, "Creation_Time"

    .line 1068641
    iget-object v2, p1, LX/6Fg;->s:Ljava/lang/String;

    move-object v2, v2

    .line 1068642
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068643
    const-string v0, "Send_Time"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068644
    const-string v0, "Timed_Out_Attachments"

    .line 1068645
    iget-object v2, p1, LX/6Fg;->t:Ljava/lang/String;

    move-object v2, v2

    .line 1068646
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068647
    iget-object v0, p0, LX/6Fd;->e:LX/0tK;

    invoke-virtual {v0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1068648
    iget-object v0, p0, LX/6Fd;->e:LX/0tK;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0tK;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1068649
    iget-object v0, p0, LX/6Fd;->e:LX/0tK;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1068650
    const-string v0, "data_saver"

    const-string v2, "active"

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068651
    :cond_5
    :goto_2
    const-string v0, "info"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1068652
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068653
    iget-object v0, p1, LX/6Fg;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1068654
    if-eqz v0, :cond_6

    .line 1068655
    const-string v0, "category_id"

    .line 1068656
    iget-object v1, p1, LX/6Fg;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1068657
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068658
    :cond_6
    iget-object v0, p1, LX/6Fg;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1068659
    if-eqz v0, :cond_7

    .line 1068660
    const-string v0, "duplicate_bug_id"

    .line 1068661
    iget-object v1, p1, LX/6Fg;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1068662
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068663
    :cond_7
    iget-object v0, p0, LX/6Fd;->d:LX/0W3;

    sget-wide v2, LX/0X5;->aY:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v3

    .line 1068664
    new-instance v4, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1068665
    iget-object v0, p1, LX/6Fg;->e:LX/0P1;

    move-object v0, v0

    .line 1068666
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068667
    const/4 v2, 0x1

    invoke-virtual {v4, v0, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_3

    .line 1068668
    :cond_8
    const-string v0, "False"

    goto/16 :goto_1

    .line 1068669
    :cond_9
    const-string v0, "data_saver"

    const-string v2, "not_active"

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_2

    .line 1068670
    :cond_a
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1068671
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1068672
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v0, v0

    .line 1068673
    if-eqz v0, :cond_e

    .line 1068674
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v0, v0

    .line 1068675
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1068676
    const/4 v1, 0x0

    .line 1068677
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v5, v0

    .line 1068678
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v6, :cond_e

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1068679
    :try_start_0
    new-instance v7, Ljava/io/File;

    new-instance v11, Ljava/net/URI;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v11}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1068680
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v7}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1068681
    :cond_b
    sget-object v0, LX/6Fd;->a:Ljava/lang/Class;

    const-string v7, "Ignoring invalid screen shot file"

    invoke-static {v0, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 1068682
    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_4

    .line 1068683
    :cond_c
    const-string v0, "screenshot-%d.png"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v0, v11}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1068684
    if-eqz v3, :cond_d

    .line 1068685
    const/4 v7, 0x1

    invoke-virtual {v4, v0, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1068686
    :goto_6
    add-int/lit8 v0, v1, 0x1

    goto :goto_5

    .line 1068687
    :cond_d
    new-instance v11, LX/4ct;

    const-string v12, "image/png"

    invoke-direct {v11, v7, v12, v0}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068688
    new-instance v7, LX/4cQ;

    invoke-direct {v7, v0, v11}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 1068689
    :catch_0
    move-exception v0

    .line 1068690
    sget-object v7, LX/6Fd;->a:Ljava/lang/Class;

    const-string v11, "Ignoring invalid screen shot"

    invoke-static {v7, v11, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068691
    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_5

    .line 1068692
    :cond_e
    const-string v0, "attachment_file_names"

    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1068693
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068694
    iget-object v0, p1, LX/6Fg;->d:LX/0P1;

    move-object v0, v0

    .line 1068695
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1068696
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1068697
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068698
    :try_start_1
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1068699
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1068700
    sget-object v0, LX/6Fd;->a:Ljava/lang/Class;

    const-string v1, "Ignoring invalid debug attachment"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1068701
    const-string v0, "Attachment file not found, skipping: "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    .line 1068702
    :catch_1
    move-exception v0

    .line 1068703
    sget-object v1, LX/6Fd;->a:Ljava/lang/Class;

    const-string v2, "Ignoring invalid debug attachment: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v1, v0, v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1068704
    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1068705
    :cond_f
    :try_start_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1068706
    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, ".jpeg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    const-string v2, "image/jpeg"

    .line 1068707
    :goto_8
    new-instance v0, LX/4cu;

    const-wide/16 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 1068708
    new-instance v1, LX/4cQ;

    invoke-direct {v1, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 1068709
    :cond_11
    const-string v2, "text/plain"
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_8

    .line 1068710
    :cond_12
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_13

    .line 1068711
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1068712
    new-instance v1, LX/4cq;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "text/plain"

    const-string v3, "missing_attachment_report.txt"

    invoke-direct {v1, v0, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1068713
    new-instance v0, LX/4cQ;

    const-string v2, "missing_attachment_report.txt"

    invoke-direct {v0, v2, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1068714
    :cond_13
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "bugReportUpload"

    .line 1068715
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1068716
    move-object v0, v0

    .line 1068717
    const-string v1, "POST"

    .line 1068718
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1068719
    move-object v0, v0

    .line 1068720
    const-string v1, "method/bug.create"

    .line 1068721
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1068722
    move-object v0, v0

    .line 1068723
    iput-object v8, v0, LX/14O;->h:LX/0n9;

    .line 1068724
    move-object v0, v0

    .line 1068725
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1068726
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1068727
    move-object v0, v0

    .line 1068728
    iput-object v9, v0, LX/14O;->l:Ljava/util/List;

    .line 1068729
    move-object v0, v0

    .line 1068730
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1068731
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1068732
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 1068733
    sget-object v0, LX/6Fd;->a:Ljava/lang/Class;

    const-string v1, "Bug report upload failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "error code: %d, msg: %s"

    .line 1068734
    iget v5, p2, LX/1pN;->b:I

    move v5, v5

    .line 1068735
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1068736
    iget-object v6, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 1068737
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1068738
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1068739
    iget-object v1, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1068740
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Fh;->a(ILjava/lang/String;)LX/6Fh;

    move-result-object v0

    .line 1068741
    :goto_0
    return-object v0

    .line 1068742
    :cond_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "bug_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 1068743
    invoke-static {v0}, LX/6Fh;->a(Ljava/lang/String;)LX/6Fh;

    move-result-object v0

    goto :goto_0
.end method
