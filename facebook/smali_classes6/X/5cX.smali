.class public final LX/5cX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 963034
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 963035
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 963036
    :goto_0
    return v1

    .line 963037
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 963038
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 963039
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 963040
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 963041
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 963042
    const-string v9, "aircraft_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 963043
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 963044
    :cond_2
    const-string v9, "arrival_airport"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 963045
    invoke-static {p0, p1}, LX/5cc;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 963046
    :cond_3
    const-string v9, "arrival_time_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 963047
    invoke-static {p0, p1}, LX/5cY;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 963048
    :cond_4
    const-string v9, "boarding_time_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 963049
    invoke-static {p0, p1}, LX/5cY;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 963050
    :cond_5
    const-string v9, "departure_airport"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 963051
    invoke-static {p0, p1}, LX/5cc;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 963052
    :cond_6
    const-string v9, "departure_time_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 963053
    invoke-static {p0, p1}, LX/5cY;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 963054
    :cond_7
    const-string v9, "flight_number"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 963055
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 963056
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 963057
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 963058
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 963059
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 963060
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 963061
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 963062
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 963063
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 963064
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 963065
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 963066
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963067
    if-eqz v0, :cond_0

    .line 963068
    const-string v1, "aircraft_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963069
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963070
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963071
    if-eqz v0, :cond_1

    .line 963072
    const-string v1, "arrival_airport"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963073
    invoke-static {p0, v0, p2}, LX/5cc;->a(LX/15i;ILX/0nX;)V

    .line 963074
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963075
    if-eqz v0, :cond_2

    .line 963076
    const-string v1, "arrival_time_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963077
    invoke-static {p0, v0, p2}, LX/5cY;->a(LX/15i;ILX/0nX;)V

    .line 963078
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963079
    if-eqz v0, :cond_3

    .line 963080
    const-string v1, "boarding_time_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963081
    invoke-static {p0, v0, p2}, LX/5cY;->a(LX/15i;ILX/0nX;)V

    .line 963082
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963083
    if-eqz v0, :cond_4

    .line 963084
    const-string v1, "departure_airport"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963085
    invoke-static {p0, v0, p2}, LX/5cc;->a(LX/15i;ILX/0nX;)V

    .line 963086
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963087
    if-eqz v0, :cond_5

    .line 963088
    const-string v1, "departure_time_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963089
    invoke-static {p0, v0, p2}, LX/5cY;->a(LX/15i;ILX/0nX;)V

    .line 963090
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963091
    if-eqz v0, :cond_6

    .line 963092
    const-string v1, "flight_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963093
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963094
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 963095
    return-void
.end method
