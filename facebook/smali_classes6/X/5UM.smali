.class public final LX/5UM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 924710
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 924711
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 924712
    :goto_0
    return v1

    .line 924713
    :cond_0
    const-string v11, "runtime_minutes"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 924714
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 924715
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 924716
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 924717
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 924718
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 924719
    const-string v11, "action_links"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 924720
    invoke-static {p0, p1}, LX/5UI;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 924721
    :cond_2
    const-string v11, "movie_cast"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 924722
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 924723
    :cond_3
    const-string v11, "movie_directors"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 924724
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 924725
    :cond_4
    const-string v11, "movie_poster_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 924726
    invoke-static {p0, p1}, LX/5UN;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 924727
    :cond_5
    const-string v11, "movie_rating"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 924728
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 924729
    :cond_6
    const-string v11, "movie_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 924730
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 924731
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 924732
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 924733
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 924734
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 924735
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 924736
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 924737
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 924738
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 924739
    if-eqz v0, :cond_9

    .line 924740
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 924741
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 924742
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 924743
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 924744
    if-eqz v0, :cond_0

    .line 924745
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924746
    invoke-static {p0, v0, p2, p3}, LX/5UI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 924747
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 924748
    if-eqz v0, :cond_1

    .line 924749
    const-string v0, "movie_cast"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924750
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 924751
    :cond_1
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 924752
    if-eqz v0, :cond_2

    .line 924753
    const-string v0, "movie_directors"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924754
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 924755
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 924756
    if-eqz v0, :cond_3

    .line 924757
    const-string v1, "movie_poster_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924758
    invoke-static {p0, v0, p2}, LX/5UN;->a(LX/15i;ILX/0nX;)V

    .line 924759
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 924760
    if-eqz v0, :cond_4

    .line 924761
    const-string v1, "movie_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924762
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 924763
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 924764
    if-eqz v0, :cond_5

    .line 924765
    const-string v1, "movie_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924766
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 924767
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 924768
    if-eqz v0, :cond_6

    .line 924769
    const-string v1, "runtime_minutes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924770
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 924771
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 924772
    return-void
.end method
