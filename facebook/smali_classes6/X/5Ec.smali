.class public final LX/5Ec;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 883178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 883179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 883180
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 883181
    invoke-static {p0, p1}, LX/5Ec;->b(LX/15w;LX/186;)I

    move-result v1

    .line 883182
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 883183
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 883184
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 883185
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 883186
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5Ec;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 883187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 883188
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 883189
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 883190
    const/16 v26, 0x0

    .line 883191
    const/16 v25, 0x0

    .line 883192
    const/16 v24, 0x0

    .line 883193
    const/16 v23, 0x0

    .line 883194
    const/16 v22, 0x0

    .line 883195
    const/16 v21, 0x0

    .line 883196
    const/16 v20, 0x0

    .line 883197
    const/16 v19, 0x0

    .line 883198
    const/16 v18, 0x0

    .line 883199
    const/16 v17, 0x0

    .line 883200
    const/16 v16, 0x0

    .line 883201
    const/4 v15, 0x0

    .line 883202
    const/4 v14, 0x0

    .line 883203
    const/4 v13, 0x0

    .line 883204
    const/4 v12, 0x0

    .line 883205
    const/4 v11, 0x0

    .line 883206
    const/4 v10, 0x0

    .line 883207
    const/4 v9, 0x0

    .line 883208
    const/4 v8, 0x0

    .line 883209
    const/4 v7, 0x0

    .line 883210
    const/4 v6, 0x0

    .line 883211
    const/4 v5, 0x0

    .line 883212
    const/4 v4, 0x0

    .line 883213
    const/4 v3, 0x0

    .line 883214
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 883215
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 883216
    const/4 v3, 0x0

    .line 883217
    :goto_0
    return v3

    .line 883218
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 883219
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_17

    .line 883220
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 883221
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 883222
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 883223
    const-string v28, "__type__"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_2

    const-string v28, "__typename"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 883224
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v26

    goto :goto_1

    .line 883225
    :cond_3
    const-string v28, "address"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 883226
    invoke-static/range {p0 .. p1}, LX/5Eb;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 883227
    :cond_4
    const-string v28, "category_icon_name"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 883228
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto :goto_1

    .line 883229
    :cond_5
    const-string v28, "category_names"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 883230
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 883231
    :cond_6
    const-string v28, "city"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 883232
    invoke-static/range {p0 .. p1}, LX/5CP;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 883233
    :cond_7
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 883234
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 883235
    :cond_8
    const-string v28, "is_owned"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 883236
    const/4 v5, 0x1

    .line 883237
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 883238
    :cond_9
    const-string v28, "location"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 883239
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 883240
    :cond_a
    const-string v28, "name"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 883241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 883242
    :cond_b
    const-string v28, "overall_star_rating"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 883243
    invoke-static/range {p0 .. p1}, LX/5CQ;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 883244
    :cond_c
    const-string v28, "page_visits"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 883245
    invoke-static/range {p0 .. p1}, LX/5CR;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 883246
    :cond_d
    const-string v28, "place_list_icon_path"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 883247
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 883248
    :cond_e
    const-string v28, "price_range_description"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 883249
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 883250
    :cond_f
    const-string v28, "profile_picture"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 883251
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 883252
    :cond_10
    const-string v28, "profile_picture_is_silhouette"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 883253
    const/4 v4, 0x1

    .line 883254
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 883255
    :cond_11
    const-string v28, "saved_collection"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 883256
    invoke-static/range {p0 .. p1}, LX/40i;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 883257
    :cond_12
    const-string v28, "should_show_reviews_on_profile"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_13

    .line 883258
    const/4 v3, 0x1

    .line 883259
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 883260
    :cond_13
    const-string v28, "super_category_type"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_14

    .line 883261
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 883262
    :cond_14
    const-string v28, "url"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_15

    .line 883263
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 883264
    :cond_15
    const-string v28, "viewer_saved_state"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_16

    .line 883265
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 883266
    :cond_16
    const-string v28, "viewer_visits"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 883267
    invoke-static/range {p0 .. p1}, LX/5CS;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 883268
    :cond_17
    const/16 v27, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 883269
    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883270
    const/16 v26, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883271
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883272
    const/16 v24, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883273
    const/16 v23, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883274
    const/16 v22, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 883275
    if-eqz v5, :cond_18

    .line 883276
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 883277
    :cond_18
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 883278
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 883279
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 883280
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 883281
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 883282
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 883283
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 883284
    if-eqz v4, :cond_19

    .line 883285
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 883286
    :cond_19
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 883287
    if-eqz v3, :cond_1a

    .line 883288
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 883289
    :cond_1a
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 883290
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 883291
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 883292
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 883293
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v4, 0x13

    const/16 v3, 0x11

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 883294
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883295
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 883296
    if-eqz v0, :cond_0

    .line 883297
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883298
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 883299
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883300
    if-eqz v0, :cond_2

    .line 883301
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883302
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883303
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 883304
    if-eqz v1, :cond_1

    .line 883305
    const-string v5, "single_line_full_address"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883306
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883307
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883308
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883309
    if-eqz v0, :cond_3

    .line 883310
    const-string v1, "category_icon_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883311
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883312
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 883313
    if-eqz v0, :cond_4

    .line 883314
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883315
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 883316
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883317
    if-eqz v0, :cond_5

    .line 883318
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883319
    invoke-static {p0, v0, p2}, LX/5CP;->a(LX/15i;ILX/0nX;)V

    .line 883320
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883321
    if-eqz v0, :cond_6

    .line 883322
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883323
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883324
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 883325
    if-eqz v0, :cond_7

    .line 883326
    const-string v1, "is_owned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883327
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 883328
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883329
    if-eqz v0, :cond_8

    .line 883330
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883331
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 883332
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883333
    if-eqz v0, :cond_9

    .line 883334
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883335
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883336
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883337
    if-eqz v0, :cond_a

    .line 883338
    const-string v1, "overall_star_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883339
    invoke-static {p0, v0, p2}, LX/5CQ;->a(LX/15i;ILX/0nX;)V

    .line 883340
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883341
    if-eqz v0, :cond_b

    .line 883342
    const-string v1, "page_visits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883343
    invoke-static {p0, v0, p2}, LX/5CR;->a(LX/15i;ILX/0nX;)V

    .line 883344
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883345
    if-eqz v0, :cond_c

    .line 883346
    const-string v1, "place_list_icon_path"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883347
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883348
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883349
    if-eqz v0, :cond_d

    .line 883350
    const-string v1, "price_range_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883351
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883352
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883353
    if-eqz v0, :cond_e

    .line 883354
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883355
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 883356
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 883357
    if-eqz v0, :cond_f

    .line 883358
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883359
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 883360
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883361
    if-eqz v0, :cond_10

    .line 883362
    const-string v1, "saved_collection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883363
    invoke-static {p0, v0, p2, p3}, LX/40i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 883364
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 883365
    if-eqz v0, :cond_11

    .line 883366
    const-string v1, "should_show_reviews_on_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883367
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 883368
    :cond_11
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 883369
    if-eqz v0, :cond_12

    .line 883370
    const-string v0, "super_category_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883371
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883372
    :cond_12
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883373
    if-eqz v0, :cond_13

    .line 883374
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883375
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883376
    :cond_13
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 883377
    if-eqz v0, :cond_14

    .line 883378
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883379
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883380
    :cond_14
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883381
    if-eqz v0, :cond_15

    .line 883382
    const-string v1, "viewer_visits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883383
    invoke-static {p0, v0, p2}, LX/5CS;->a(LX/15i;ILX/0nX;)V

    .line 883384
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883385
    return-void
.end method
