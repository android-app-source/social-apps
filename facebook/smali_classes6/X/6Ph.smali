.class public LX/6Ph;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "LX/4WJ;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)V
    .locals 1
    .param p2    # Lcom/facebook/graphql/model/GraphQLActor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086313
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Ph;->a:Ljava/lang/String;

    .line 1086314
    iput-object p2, p0, LX/6Ph;->b:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1086315
    iput-boolean p3, p0, LX/6Ph;->c:Z

    .line 1086316
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086311
    iget-object v0, p0, LX/6Ph;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 3

    .prologue
    .line 1086299
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    check-cast p2, LX/4WJ;

    .line 1086300
    iget-object v0, p0, LX/6Ph;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086301
    :cond_0
    :goto_0
    return-void

    .line 1086302
    :cond_1
    iget-boolean v0, p0, LX/6Ph;->c:Z

    invoke-virtual {p2, v0}, LX/4WJ;->a(Z)V

    .line 1086303
    iget-object v0, p0, LX/6Ph;->b:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    .line 1086304
    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    .line 1086305
    iget-boolean v1, p0, LX/6Ph;->c:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/6Ph;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1086306
    iget-object v1, p0, LX/6Ph;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {v0, v1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4WJ;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V

    goto :goto_0

    .line 1086307
    :cond_2
    iget-boolean v1, p0, LX/6Ph;->c:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1086308
    iget-object v1, p0, LX/6Ph;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {v0, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4WJ;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086310
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086309
    const-string v0, "ToggleLikeMutatingVisitor"

    return-object v0
.end method
