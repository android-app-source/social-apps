.class public LX/6Z3;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/1qw;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6Z3;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1qw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110849
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1110850
    return-void
.end method

.method public static a(LX/0QB;)LX/6Z3;
    .locals 4

    .prologue
    .line 1110836
    sget-object v0, LX/6Z3;->a:LX/6Z3;

    if-nez v0, :cond_1

    .line 1110837
    const-class v1, LX/6Z3;

    monitor-enter v1

    .line 1110838
    :try_start_0
    sget-object v0, LX/6Z3;->a:LX/6Z3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1110839
    if-eqz v2, :cond_0

    .line 1110840
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1110841
    new-instance v3, LX/6Z3;

    const/16 p0, 0xc6b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/6Z3;-><init>(LX/0Ot;)V

    .line 1110842
    move-object v0, v3

    .line 1110843
    sput-object v0, LX/6Z3;->a:LX/6Z3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1110844
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1110845
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1110846
    :cond_1
    sget-object v0, LX/6Z3;->a:LX/6Z3;

    return-object v0

    .line 1110847
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1110848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1110851
    check-cast p3, LX/1qw;

    .line 1110852
    iget-object p0, p3, LX/1qw;->b:LX/0eF;

    invoke-interface {p0}, LX/0eF;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1110853
    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object p0

    invoke-static {p3, p0}, LX/1qw;->a(LX/1qw;Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1110854
    :cond_0
    return-void
.end method
