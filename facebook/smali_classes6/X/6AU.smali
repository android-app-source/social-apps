.class public final LX/6AU;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1059302
    const-class v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;

    const v0, 0x590b2815

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "StaticSimpleFeedbackMediaQuery"

    const-string v6, "658e5c4383a22363c8a13527ebd5e3fd"

    const-string v7, "node"

    const-string v8, "10155207369021729"

    const-string v9, "10155259090151729"

    .line 1059303
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1059304
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1059305
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1059306
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1059307
    sparse-switch v0, :sswitch_data_0

    .line 1059308
    :goto_0
    return-object p1

    .line 1059309
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1059310
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1059311
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1059312
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1059313
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1059314
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_3
        -0x3c54de38 -> :sswitch_1
        -0x3b85b241 -> :sswitch_5
        -0x35b0b8aa -> :sswitch_2
        -0x30b65c8f -> :sswitch_0
        0x7506f93c -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1059315
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1059316
    :goto_1
    return v0

    .line 1059317
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1059318
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1059319
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x35 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
