.class public final LX/5Ku;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Ku;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Ks;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Kv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899239
    const/4 v0, 0x0

    sput-object v0, LX/5Ku;->a:LX/5Ku;

    .line 899240
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Ku;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 899241
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 899242
    new-instance v0, LX/5Kv;

    invoke-direct {v0}, LX/5Kv;-><init>()V

    iput-object v0, p0, LX/5Ku;->c:LX/5Kv;

    .line 899243
    return-void
.end method

.method public static declared-synchronized q()LX/5Ku;
    .locals 2

    .prologue
    .line 899244
    const-class v1, LX/5Ku;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Ku;->a:LX/5Ku;

    if-nez v0, :cond_0

    .line 899245
    new-instance v0, LX/5Ku;

    invoke-direct {v0}, LX/5Ku;-><init>()V

    sput-object v0, LX/5Ku;->a:LX/5Ku;

    .line 899246
    :cond_0
    sget-object v0, LX/5Ku;->a:LX/5Ku;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 899247
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 899248
    check-cast p2, LX/5Kt;

    .line 899249
    iget-object v0, p2, LX/5Kt;->a:Lcom/facebook/java2js/JSValue;

    .line 899250
    const-string v1, "text"

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    invoke-static {p1, v1}, LX/5Kv;->b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;

    move-result-object v1

    .line 899251
    const-string v2, "maximumNumberOfLines"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v3

    .line 899252
    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_0

    .line 899253
    double-to-int v2, v3

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    .line 899254
    :cond_0
    const-string v2, "truncationMode"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 899255
    if-eqz v2, :cond_1

    .line 899256
    invoke-static {v2}, LX/5Kv;->b(Ljava/lang/String;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    .line 899257
    :cond_1
    invoke-virtual {v1}, LX/1X5;->b()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 899258
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 899259
    invoke-static {}, LX/1dS;->b()V

    .line 899260
    const/4 v0, 0x0

    return-object v0
.end method
