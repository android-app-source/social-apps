.class public final LX/5nX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004349
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1004350
    check-cast p1, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;

    .line 1004351
    const/4 v0, 0x6

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1004352
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1004353
    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1004354
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "events"

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004355
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "client_time"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004356
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "surface"

    const-string v3, "fb4atux"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004357
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "log_exposure"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004358
    iget-object v1, p1, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1004359
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004360
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004361
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "reportAAATuxAction"

    .line 1004362
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1004363
    move-object v1, v1

    .line 1004364
    const-string v2, "POST"

    .line 1004365
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1004366
    move-object v1, v1

    .line 1004367
    const-string v2, "me/audience_alignment_info"

    .line 1004368
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1004369
    move-object v1, v1

    .line 1004370
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1004371
    move-object v0, v1

    .line 1004372
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1004373
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1004374
    move-object v0, v0

    .line 1004375
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004376
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004377
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
