.class public LX/6Pc;
.super LX/6PU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V
    .locals 0
    .param p2    # Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086268
    invoke-direct {p0, p1}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086269
    iput-object p2, p0, LX/6Pc;->a:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 1086270
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 1

    .prologue
    .line 1086264
    iget-object v0, p0, LX/6Pc;->a:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Pc;->a:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Pc;->a:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v0

    .line 1086265
    :goto_0
    iget-object p0, p2, LX/40T;->a:LX/4VK;

    const-string p1, "local_last_negative_feedback_action_type"

    invoke-virtual {p0, p1, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086266
    return-void

    .line 1086267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086271
    const-string v0, "SetStoryNegativeFeedbackMutatingVisitor"

    return-object v0
.end method
