.class public LX/5jE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

.field public final b:F

.field public final c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public final d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public final e:F

.field public final f:F


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;)V
    .locals 2

    .prologue
    .line 986031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986032
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;->b()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->a:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 986033
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;->a()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->b:F

    .line 986034
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 986035
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 986036
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->e:F

    .line 986037
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;->d()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->f:F

    .line 986038
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;)V
    .locals 2

    .prologue
    .line 986039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986040
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;->b()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->a:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 986041
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;->a()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->b:F

    .line 986042
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 986043
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jE;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 986044
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->e:F

    .line 986045
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->d()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jE;->f:F

    .line 986046
    return-void
.end method
