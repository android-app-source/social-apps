.class public final LX/6GA;
.super LX/6G9;
.source ""


# instance fields
.field public final synthetic a:LX/0kK;


# direct methods
.method public constructor <init>(LX/0kK;)V
    .locals 0

    .prologue
    .line 1069930
    iput-object p1, p0, LX/6GA;->a:LX/0kK;

    invoke-direct {p0}, LX/6G9;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1069903
    iget-object v0, p0, LX/6GA;->a:LX/0kK;

    .line 1069904
    iget-object v1, v0, LX/0kK;->g:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1069905
    invoke-static {v0}, LX/0kK;->f(LX/0kK;)Lcom/facebook/bugreporter/RageShakeDialogFragment;

    move-result-object v1

    .line 1069906
    if-eqz v1, :cond_3

    .line 1069907
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    .line 1069908
    :goto_0
    move v1, v1

    .line 1069909
    if-nez v1, :cond_0

    iget-object v1, v0, LX/0kK;->g:Landroid/content/Context;

    instance-of v1, v1, Lcom/facebook/bugreporter/activity/BugReportActivity;

    if-nez v1, :cond_0

    .line 1069910
    iget-object v1, v0, LX/0kK;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1069911
    if-nez v1, :cond_1

    .line 1069912
    iget-object v1, v0, LX/0kK;->f:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0818ea

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1069913
    :cond_0
    :goto_2
    return-void

    .line 1069914
    :cond_1
    iget-object v1, v0, LX/0kK;->g:Landroid/content/Context;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/0kK;->c:LX/0kN;

    if-eqz v1, :cond_2

    .line 1069915
    iget-object v1, v0, LX/0kK;->h:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1069916
    invoke-static {v0}, LX/0kK;->f(LX/0kK;)Lcom/facebook/bugreporter/RageShakeDialogFragment;

    move-result-object v1

    .line 1069917
    if-nez v1, :cond_2

    .line 1069918
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, v0, LX/0kK;->g:Landroid/content/Context;

    const v4, 0x7f0818f1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, v0, LX/0kK;->g:Landroid/content/Context;

    const v4, 0x7f0818f2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, v0, LX/0kK;->g:Landroid/content/Context;

    const v4, 0x7f0818f3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1069919
    iget-object v2, v0, LX/0kK;->g:Landroid/content/Context;

    const v3, 0x7f0818d9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1069920
    new-instance v3, Lcom/facebook/bugreporter/RageShakeDialogFragment;

    invoke-direct {v3}, Lcom/facebook/bugreporter/RageShakeDialogFragment;-><init>()V

    .line 1069921
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1069922
    const-string v5, "title"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069923
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1069924
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1069925
    const-string p0, "items"

    invoke-virtual {v4, p0, v5}, Landroid/os/Bundle;->putCharSequenceArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1069926
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1069927
    move-object v1, v3

    .line 1069928
    iget-object v2, v0, LX/0kK;->h:LX/0gc;

    const-string v3, "BugReportFragmentDialog"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1069929
    :cond_2
    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1
.end method
