.class public final LX/6Ss;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1096906
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1096907
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096908
    :goto_0
    return v1

    .line 1096909
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096910
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1096911
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1096912
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096913
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1096914
    const-string v3, "top_level_comments"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1096915
    invoke-static {p0, p1}, LX/6TA;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1096916
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1096917
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1096918
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1096919
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096920
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096921
    if-eqz v0, :cond_0

    .line 1096922
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096923
    invoke-static {p0, v0, p2, p3}, LX/6TA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096924
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096925
    return-void
.end method
