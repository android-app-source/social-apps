.class public LX/5r6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/5r6;


# instance fields
.field private final b:Landroid/view/Choreographer;

.field private final c:LX/5r5;

.field public final d:[Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayDeque",
            "<",
            "Landroid/view/Choreographer$FrameCallback;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field public f:Z


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1010876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010877
    iput v0, p0, LX/5r6;->e:I

    .line 1010878
    iput-boolean v0, p0, LX/5r6;->f:Z

    .line 1010879
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v1

    iput-object v1, p0, LX/5r6;->b:Landroid/view/Choreographer;

    .line 1010880
    new-instance v1, LX/5r5;

    invoke-direct {v1, p0}, LX/5r5;-><init>(LX/5r6;)V

    iput-object v1, p0, LX/5r6;->c:LX/5r5;

    .line 1010881
    invoke-static {}, LX/5r4;->values()[LX/5r4;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Ljava/util/ArrayDeque;

    iput-object v1, p0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    .line 1010882
    :goto_0
    iget-object v1, p0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1010883
    iget-object v1, p0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    new-instance v2, Ljava/util/ArrayDeque;

    invoke-direct {v2}, Ljava/util/ArrayDeque;-><init>()V

    aput-object v2, v1, v0

    .line 1010884
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1010885
    :cond_0
    return-void
.end method

.method public static a()LX/5r6;
    .locals 1

    .prologue
    .line 1010886
    invoke-static {}, LX/5pe;->b()V

    .line 1010887
    sget-object v0, LX/5r6;->a:LX/5r6;

    if-nez v0, :cond_0

    .line 1010888
    new-instance v0, LX/5r6;

    invoke-direct {v0}, LX/5r6;-><init>()V

    sput-object v0, LX/5r6;->a:LX/5r6;

    .line 1010889
    :cond_0
    sget-object v0, LX/5r6;->a:LX/5r6;

    return-object v0
.end method

.method public static synthetic b(LX/5r6;)I
    .locals 2

    .prologue
    .line 1010890
    iget v0, p0, LX/5r6;->e:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LX/5r6;->e:I

    return v0
.end method

.method public static b(LX/5r6;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1010891
    iget v0, p0, LX/5r6;->e:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1010892
    iget v0, p0, LX/5r6;->e:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/5r6;->f:Z

    if-eqz v0, :cond_0

    .line 1010893
    iget-object v0, p0, LX/5r6;->b:Landroid/view/Choreographer;

    iget-object v2, p0, LX/5r6;->c:LX/5r5;

    invoke-virtual {v0, v2}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1010894
    iput-boolean v1, p0, LX/5r6;->f:Z

    .line 1010895
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1010896
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1010897
    invoke-static {}, LX/5pe;->b()V

    .line 1010898
    iget-object v0, p0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    invoke-virtual {p1}, LX/5r4;->getOrder()I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v0, p2}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 1010899
    iget v0, p0, LX/5r6;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/5r6;->e:I

    .line 1010900
    iget v0, p0, LX/5r6;->e:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1010901
    iget-boolean v0, p0, LX/5r6;->f:Z

    if-nez v0, :cond_0

    .line 1010902
    iget-object v0, p0, LX/5r6;->b:Landroid/view/Choreographer;

    iget-object v2, p0, LX/5r6;->c:LX/5r5;

    invoke-virtual {v0, v2}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1010903
    iput-boolean v1, p0, LX/5r6;->f:Z

    .line 1010904
    :cond_0
    return-void

    .line 1010905
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V
    .locals 2

    .prologue
    .line 1010906
    invoke-static {}, LX/5pe;->b()V

    .line 1010907
    iget-object v0, p0, LX/5r6;->d:[Ljava/util/ArrayDeque;

    invoke-virtual {p1}, LX/5r4;->getOrder()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Ljava/util/ArrayDeque;->removeFirstOccurrence(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010908
    iget v0, p0, LX/5r6;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/5r6;->e:I

    .line 1010909
    invoke-static {p0}, LX/5r6;->b(LX/5r6;)V

    .line 1010910
    :goto_0
    return-void

    .line 1010911
    :cond_0
    const-string v0, "React"

    const-string v1, "Tried to remove non-existent frame callback"

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
