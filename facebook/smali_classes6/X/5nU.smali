.class public final LX/5nU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004285
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1004286
    check-cast p1, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;

    .line 1004287
    const/4 v0, 0x6

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1004288
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1004289
    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1004290
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "events"

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004291
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "client_time"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004292
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "surface"

    const-string v3, "onlyme-fb4atux"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004293
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "log_exposure"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004294
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004295
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004296
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "reportAAAOnlyMeAction"

    .line 1004297
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1004298
    move-object v1, v1

    .line 1004299
    const-string v2, "POST"

    .line 1004300
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1004301
    move-object v1, v1

    .line 1004302
    const-string v2, "me/audience_alignment_info"

    .line 1004303
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1004304
    move-object v1, v1

    .line 1004305
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1004306
    move-object v0, v1

    .line 1004307
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1004308
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1004309
    move-object v0, v0

    .line 1004310
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004282
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004283
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
