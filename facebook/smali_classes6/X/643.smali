.class public final LX/643;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1044090
    iput-object p1, p0, LX/643;->c:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    iput-object p2, p0, LX/643;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/643;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1044091
    iget-object v0, p0, LX/643;->c:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    iget-object v0, v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->g:LX/640;

    iget-object v1, p0, LX/643;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/643;->b:Z

    .line 1044092
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1044093
    const-string v3, "User-Agent"

    const-string p0, "FB-ZeroBalance"

    invoke-interface {v4, v3, p0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044094
    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const/4 p0, 0x0

    invoke-static {v3, p0}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 1044095
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v3

    sget-object p0, LX/640;->b:Lorg/apache/http/client/ResponseHandler;

    .line 1044096
    iput-object p0, v3, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1044097
    move-object p0, v3

    .line 1044098
    if-eqz v2, :cond_0

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1044099
    :goto_0
    iput-object v3, p0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1044100
    move-object v3, p0

    .line 1044101
    iput-object v4, v3, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1044102
    move-object v3, v3

    .line 1044103
    const-string v4, "zero_balance_ping"

    .line 1044104
    iput-object v4, v3, LX/15E;->c:Ljava/lang/String;

    .line 1044105
    move-object v3, v3

    .line 1044106
    iget-object v4, v0, LX/640;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v3}, LX/15E;->a()LX/15D;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/http/HttpResponse;

    move-object v0, v3

    .line 1044107
    return-object v0

    .line 1044108
    :cond_0
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
