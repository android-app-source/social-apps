.class public final LX/5CT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 867825
    const/16 v20, 0x0

    .line 867826
    const/16 v19, 0x0

    .line 867827
    const/16 v18, 0x0

    .line 867828
    const/16 v17, 0x0

    .line 867829
    const/16 v16, 0x0

    .line 867830
    const/4 v15, 0x0

    .line 867831
    const/4 v14, 0x0

    .line 867832
    const/4 v13, 0x0

    .line 867833
    const/4 v12, 0x0

    .line 867834
    const/4 v11, 0x0

    .line 867835
    const/4 v10, 0x0

    .line 867836
    const/4 v9, 0x0

    .line 867837
    const/4 v8, 0x0

    .line 867838
    const/4 v7, 0x0

    .line 867839
    const/4 v6, 0x0

    .line 867840
    const/4 v5, 0x0

    .line 867841
    const/4 v4, 0x0

    .line 867842
    const/4 v3, 0x0

    .line 867843
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 867844
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 867845
    const/4 v3, 0x0

    .line 867846
    :goto_0
    return v3

    .line 867847
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 867848
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_11

    .line 867849
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 867850
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 867851
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 867852
    const-string v22, "__type__"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "__typename"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 867853
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v20

    goto :goto_1

    .line 867854
    :cond_3
    const-string v22, "city"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 867855
    invoke-static/range {p0 .. p1}, LX/5CP;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 867856
    :cond_4
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 867857
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto :goto_1

    .line 867858
    :cond_5
    const-string v22, "is_owned"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 867859
    const/4 v5, 0x1

    .line 867860
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto :goto_1

    .line 867861
    :cond_6
    const-string v22, "location"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 867862
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 867863
    :cond_7
    const-string v22, "name"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 867864
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 867865
    :cond_8
    const-string v22, "overall_star_rating"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 867866
    invoke-static/range {p0 .. p1}, LX/5CQ;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 867867
    :cond_9
    const-string v22, "page_visits"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 867868
    invoke-static/range {p0 .. p1}, LX/5CR;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 867869
    :cond_a
    const-string v22, "profile_picture_is_silhouette"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 867870
    const/4 v4, 0x1

    .line 867871
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 867872
    :cond_b
    const-string v22, "saved_collection"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 867873
    invoke-static/range {p0 .. p1}, LX/40i;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 867874
    :cond_c
    const-string v22, "should_show_reviews_on_profile"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 867875
    const/4 v3, 0x1

    .line 867876
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 867877
    :cond_d
    const-string v22, "super_category_type"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 867878
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 867879
    :cond_e
    const-string v22, "url"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 867880
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 867881
    :cond_f
    const-string v22, "viewer_saved_state"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 867882
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 867883
    :cond_10
    const-string v22, "viewer_visits"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 867884
    invoke-static/range {p0 .. p1}, LX/5CS;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 867885
    :cond_11
    const/16 v21, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 867886
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867887
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867888
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867889
    if-eqz v5, :cond_12

    .line 867890
    const/4 v5, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 867891
    :cond_12
    const/4 v5, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 867892
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 867893
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 867894
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 867895
    if-eqz v4, :cond_13

    .line 867896
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 867897
    :cond_13
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 867898
    if-eqz v3, :cond_14

    .line 867899
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 867900
    :cond_14
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 867901
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 867902
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 867903
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 867904
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
