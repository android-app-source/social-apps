.class public final enum LX/5dc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5dc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5dc;

.field public static final enum APP:LX/5dc;

.field public static final enum GAME:LX/5dc;

.field public static final enum PAGE:LX/5dc;

.field public static final enum UNRECOGNIZED:LX/5dc;


# instance fields
.field private final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 965871
    new-instance v0, LX/5dc;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, LX/5dc;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    .line 965872
    new-instance v0, LX/5dc;

    const-string v1, "APP"

    invoke-direct {v0, v1, v4, v3}, LX/5dc;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5dc;->APP:LX/5dc;

    .line 965873
    new-instance v0, LX/5dc;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v5, v4}, LX/5dc;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5dc;->PAGE:LX/5dc;

    .line 965874
    new-instance v0, LX/5dc;

    const-string v1, "GAME"

    invoke-direct {v0, v1, v6, v5}, LX/5dc;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5dc;->GAME:LX/5dc;

    .line 965875
    const/4 v0, 0x4

    new-array v0, v0, [LX/5dc;

    sget-object v1, LX/5dc;->UNRECOGNIZED:LX/5dc;

    aput-object v1, v0, v3

    sget-object v1, LX/5dc;->APP:LX/5dc;

    aput-object v1, v0, v4

    sget-object v1, LX/5dc;->PAGE:LX/5dc;

    aput-object v1, v0, v5

    sget-object v1, LX/5dc;->GAME:LX/5dc;

    aput-object v1, v0, v6

    sput-object v0, LX/5dc;->$VALUES:[LX/5dc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 965876
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 965877
    iput p3, p0, LX/5dc;->mValue:I

    .line 965878
    return-void
.end method

.method public static fromGraphQLMessageAttributionType(Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;)LX/5dc;
    .locals 2

    .prologue
    .line 965879
    sget-object v0, LX/5db;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 965880
    sget-object v0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    :goto_0
    return-object v0

    .line 965881
    :pswitch_0
    sget-object v0, LX/5dc;->APP:LX/5dc;

    goto :goto_0

    .line 965882
    :pswitch_1
    sget-object v0, LX/5dc;->PAGE:LX/5dc;

    goto :goto_0

    .line 965883
    :pswitch_2
    sget-object v0, LX/5dc;->GAME:LX/5dc;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static fromValue(I)LX/5dc;
    .locals 1

    .prologue
    .line 965884
    packed-switch p0, :pswitch_data_0

    .line 965885
    sget-object v0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    :goto_0
    return-object v0

    .line 965886
    :pswitch_0
    sget-object v0, LX/5dc;->APP:LX/5dc;

    goto :goto_0

    .line 965887
    :pswitch_1
    sget-object v0, LX/5dc;->PAGE:LX/5dc;

    goto :goto_0

    .line 965888
    :pswitch_2
    sget-object v0, LX/5dc;->GAME:LX/5dc;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/5dc;
    .locals 1

    .prologue
    .line 965889
    const-class v0, LX/5dc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5dc;

    return-object v0
.end method

.method public static values()[LX/5dc;
    .locals 1

    .prologue
    .line 965890
    sget-object v0, LX/5dc;->$VALUES:[LX/5dc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5dc;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 965891
    iget v0, p0, LX/5dc;->mValue:I

    return v0
.end method
