.class public LX/6FJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field public final b:LX/6sW;

.field public final c:LX/5fv;


# direct methods
.method public constructor <init>(LX/03V;LX/6sW;LX/5fv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067641
    iput-object p1, p0, LX/6FJ;->a:LX/03V;

    .line 1067642
    iput-object p2, p0, LX/6FJ;->b:LX/6sW;

    .line 1067643
    iput-object p3, p0, LX/6FJ;->c:LX/5fv;

    .line 1067644
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .locals 2

    .prologue
    .line 1067636
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v1

    .line 1067637
    invoke-static {v1}, LX/47j;->a(LX/0am;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 1067638
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    return-object v0

    .line 1067639
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6FJ;
    .locals 4

    .prologue
    .line 1067645
    new-instance v3, LX/6FJ;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/6sW;->b(LX/0QB;)LX/6sW;

    move-result-object v1

    check-cast v1, LX/6sW;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v2

    check-cast v2, LX/5fv;

    invoke-direct {v3, v0, v1, v2}, LX/6FJ;-><init>(LX/03V;LX/6sW;LX/5fv;)V

    .line 1067646
    return-object v3
.end method

.method public static b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1067635
    invoke-static {p0}, LX/6FJ;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6xg;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/String;LX/0m9;LX/6FF;)V
    .locals 7
    .param p5    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1067595
    const/4 v0, 0x0

    .line 1067596
    :try_start_0
    iget-object v1, p0, LX/6FJ;->c:LX/5fv;

    const-string v2, "USD"

    invoke-virtual {v1, v2, p3}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1067597
    :goto_0
    move-object v3, v0

    .line 1067598
    if-nez v3, :cond_0

    .line 1067599
    invoke-interface {p6}, LX/6FF;->a()V

    .line 1067600
    :goto_1
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 1067601
    if-eqz v6, :cond_1

    const/4 p0, 0x1

    :goto_2
    invoke-static {p0}, LX/0Tp;->a(Z)V

    .line 1067602
    new-instance p0, LX/6FI;

    invoke-direct {p0, v0, v6}, LX/6FI;-><init>(LX/6FJ;LX/6FF;)V

    .line 1067603
    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object p1

    iget-object p1, p1, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {p1, v1}, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)LX/6sd;

    move-result-object p1

    .line 1067604
    iput-object v3, p1, LX/6sd;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1067605
    move-object p1, p1

    .line 1067606
    invoke-static {v2}, LX/6FJ;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object p2

    .line 1067607
    iput-object p2, p1, LX/6sd;->i:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1067608
    move-object p1, p1

    .line 1067609
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1067610
    iput-object p2, p1, LX/6sd;->f:Ljava/lang/String;

    .line 1067611
    move-object p1, p1

    .line 1067612
    iput-object v4, p1, LX/6sd;->d:Ljava/lang/String;

    .line 1067613
    move-object p1, p1

    .line 1067614
    iput-object v5, p1, LX/6sd;->h:LX/0m9;

    .line 1067615
    move-object p1, p1

    .line 1067616
    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object p2

    .line 1067617
    invoke-static {p2}, LX/47j;->a(LX/0am;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1067618
    const/4 p2, 0x0

    .line 1067619
    :goto_3
    move-object p2, p2

    .line 1067620
    iput-object p2, p1, LX/6sd;->n:Ljava/lang/String;

    .line 1067621
    move-object p1, p1

    .line 1067622
    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object p2

    .line 1067623
    invoke-static {p2}, LX/47j;->a(LX/0am;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1067624
    const/4 p2, 0x0

    .line 1067625
    :goto_4
    move-object p2, p2

    .line 1067626
    iput-object p2, p1, LX/6sd;->k:Ljava/lang/String;

    .line 1067627
    move-object p1, p1

    .line 1067628
    invoke-virtual {p1}, LX/6sd;->a()Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    move-result-object p1

    .line 1067629
    iget-object p2, v0, LX/6FJ;->b:LX/6sW;

    invoke-virtual {p2, p1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 1067630
    sget-object p2, LX/131;->INSTANCE:LX/131;

    move-object p2, p2

    .line 1067631
    invoke-static {p1, p0, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1067632
    goto :goto_1

    .line 1067633
    :catch_0
    iget-object v1, p0, LX/6FJ;->a:LX/03V;

    const-string v2, "ProcessPaymentsHelper"

    const-string v3, "Amount passed via payments api in javascript SDK cannot be parsed."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1067634
    :cond_1
    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {p2}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_3
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {p2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object p2

    goto :goto_4
.end method
