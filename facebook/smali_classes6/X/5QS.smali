.class public final enum LX/5QS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5QS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5QS;

.field public static final enum ALL_CHILD_GROUPS:LX/5QS;

.field public static final enum ALL_SUB_GROUPS:LX/5QS;

.field public static final enum FORUM_GROUPS:LX/5QS;

.field public static final enum SUGGESTED_GROUPS:LX/5QS;

.field public static final enum VIEWER_CHILD_GROUPS:LX/5QS;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 913208
    new-instance v0, LX/5QS;

    const-string v1, "ALL_CHILD_GROUPS"

    invoke-direct {v0, v1, v2}, LX/5QS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QS;->ALL_CHILD_GROUPS:LX/5QS;

    .line 913209
    new-instance v0, LX/5QS;

    const-string v1, "SUGGESTED_GROUPS"

    invoke-direct {v0, v1, v3}, LX/5QS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QS;->SUGGESTED_GROUPS:LX/5QS;

    .line 913210
    new-instance v0, LX/5QS;

    const-string v1, "VIEWER_CHILD_GROUPS"

    invoke-direct {v0, v1, v4}, LX/5QS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QS;->VIEWER_CHILD_GROUPS:LX/5QS;

    .line 913211
    new-instance v0, LX/5QS;

    const-string v1, "FORUM_GROUPS"

    invoke-direct {v0, v1, v5}, LX/5QS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QS;->FORUM_GROUPS:LX/5QS;

    .line 913212
    new-instance v0, LX/5QS;

    const-string v1, "ALL_SUB_GROUPS"

    invoke-direct {v0, v1, v6}, LX/5QS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QS;->ALL_SUB_GROUPS:LX/5QS;

    .line 913213
    const/4 v0, 0x5

    new-array v0, v0, [LX/5QS;

    sget-object v1, LX/5QS;->ALL_CHILD_GROUPS:LX/5QS;

    aput-object v1, v0, v2

    sget-object v1, LX/5QS;->SUGGESTED_GROUPS:LX/5QS;

    aput-object v1, v0, v3

    sget-object v1, LX/5QS;->VIEWER_CHILD_GROUPS:LX/5QS;

    aput-object v1, v0, v4

    sget-object v1, LX/5QS;->FORUM_GROUPS:LX/5QS;

    aput-object v1, v0, v5

    sget-object v1, LX/5QS;->ALL_SUB_GROUPS:LX/5QS;

    aput-object v1, v0, v6

    sput-object v0, LX/5QS;->$VALUES:[LX/5QS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 913214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5QS;
    .locals 1

    .prologue
    .line 913215
    const-class v0, LX/5QS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5QS;

    return-object v0
.end method

.method public static values()[LX/5QS;
    .locals 1

    .prologue
    .line 913216
    sget-object v0, LX/5QS;->$VALUES:[LX/5QS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5QS;

    return-object v0
.end method
