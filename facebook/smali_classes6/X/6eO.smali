.class public LX/6eO;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:LX/6eN;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1117876
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1117877
    invoke-direct {p0}, LX/6eO;->a()V

    .line 1117878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1117872
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1117873
    invoke-direct {p0}, LX/6eO;->a()V

    .line 1117874
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1117879
    const v0, 0x7f03149d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1117880
    const v0, 0x7f0d2ed4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/6eO;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1117881
    const v0, 0x7f0d2ed5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/6eO;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1117882
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6eO;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1117883
    const v0, 0x7f0d2ed6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/6eM;

    invoke-direct {v1, p0}, LX/6eM;-><init>(LX/6eO;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1117884
    return-void
.end method


# virtual methods
.method public getTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1117875
    iget-object v0, p0, LX/6eO;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public setListener(LX/6eN;)V
    .locals 0

    .prologue
    .line 1117870
    iput-object p1, p0, LX/6eO;->c:LX/6eN;

    .line 1117871
    return-void
.end method
