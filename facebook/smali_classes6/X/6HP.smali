.class public final LX/6HP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/6HU;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/6HU;)V
    .locals 1

    .prologue
    .line 1071723
    iput-object p1, p0, LX/6HP;->a:LX/6HU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071724
    const/16 v0, 0x3e8

    iput v0, p0, LX/6HP;->b:I

    .line 1071725
    const/4 v0, 0x0

    iput v0, p0, LX/6HP;->c:I

    .line 1071726
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    const/4 v3, 0x0

    .line 1071727
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1071728
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->B:LX/6HF;

    invoke-interface {v0}, LX/6HF;->c()V

    .line 1071729
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v1, p0, LX/6HP;->a:LX/6HU;

    iget-object v2, p0, LX/6HP;->a:LX/6HU;

    iget v2, v2, LX/6HU;->x:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1071730
    iget-object v6, v1, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v6, :cond_b

    .line 1071731
    iget-object v4, v1, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1071732
    :cond_0
    :goto_0
    move-object v1, v4

    .line 1071733
    iput-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1071734
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1071735
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1071736
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_f

    .line 1071737
    :cond_1
    const/4 v1, 0x0

    .line 1071738
    :goto_1
    move v0, v1

    .line 1071739
    if-nez v0, :cond_5

    .line 1071740
    :cond_2
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_3

    .line 1071741
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->d:Landroid/hardware/Camera;

    const v1, 0x42a41817

    invoke-static {v0, v1}, LX/0J2;->a(Landroid/hardware/Camera;I)V

    .line 1071742
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    .line 1071743
    iput-object v3, v0, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1071744
    :cond_3
    iget v0, p0, LX/6HP;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/6HP;->c:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_4

    .line 1071745
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->B:LX/6HF;

    const-string v1, "CameraLoader failed 3 times"

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "getCameraInstance failed"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1071746
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->a:LX/6HO;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/6HO;->a(Z)V

    .line 1071747
    :goto_2
    return-void

    .line 1071748
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed at: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/6HP;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1071749
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1071750
    new-instance v1, Lcom/facebook/camera/device/CameraHolder$CameraLoader$1;

    invoke-direct {v1, p0}, Lcom/facebook/camera/device/CameraHolder$CameraLoader$1;-><init>(LX/6HP;)V

    iget v2, p0, LX/6HP;->b:I

    int-to-long v2, v2

    const v4, 0x1e4c5730    # 1.08177E-20f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_2

    .line 1071751
    :cond_5
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    .line 1071752
    iget-object v5, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071753
    iget-object v5, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->a()V

    .line 1071754
    iget-object v5, v0, LX/6HU;->f:LX/6HI;

    iget-object v6, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v5, v6}, LX/6HI;->a(Landroid/hardware/Camera;)V

    .line 1071755
    iget-object v5, v0, LX/6HU;->a:LX/6HO;

    iget-object v6, v0, LX/6HU;->f:LX/6HI;

    .line 1071756
    sget-object v7, LX/6HI;->a:Ljava/util/HashMap;

    iget-object v8, v6, LX/6HI;->g:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move v6, v7

    .line 1071757
    invoke-interface {v5, v6}, LX/6HO;->a(I)V

    .line 1071758
    iget-object v5, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v5}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    .line 1071759
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v7

    .line 1071760
    if-eqz v7, :cond_10

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    .line 1071761
    :goto_3
    if-eqz v7, :cond_6

    .line 1071762
    invoke-interface {v7, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1071763
    :cond_6
    new-instance v7, LX/6Ha;

    iget-object v8, v0, LX/6HU;->B:LX/6HF;

    invoke-direct {v7, v5, v8}, LX/6Ha;-><init>([Ljava/lang/String;LX/6HF;)V

    iput-object v7, v0, LX/6HU;->n:LX/6Ha;

    .line 1071764
    invoke-static {v6}, LX/6Ha;->a(Landroid/hardware/Camera$Parameters;)Z

    move-result v5

    if-eqz v5, :cond_11

    iget-object v5, v0, LX/6HU;->C:LX/6Hj;

    .line 1071765
    iget-object v6, v5, LX/6Hj;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move v5, v6

    .line 1071766
    if-eqz v5, :cond_11

    .line 1071767
    new-instance v5, LX/6He;

    iget-object v6, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v7, v0, LX/6HU;->G:LX/03V;

    invoke-direct {v5, v6, v7}, LX/6He;-><init>(Landroid/hardware/Camera;LX/03V;)V

    iput-object v5, v0, LX/6HU;->o:LX/6He;

    .line 1071768
    new-instance v5, LX/6Hh;

    iget-object v6, v0, LX/6HU;->o:LX/6He;

    iget-object v7, v0, LX/6HU;->F:LX/0Zr;

    invoke-direct {v5, v6, v7}, LX/6Hh;-><init>(LX/6He;LX/0Zr;)V

    iput-object v5, v0, LX/6HU;->p:LX/6Hh;

    .line 1071769
    iget-object v5, v0, LX/6HU;->p:LX/6Hh;

    iget-object v6, v0, LX/6HU;->n:LX/6Ha;

    .line 1071770
    if-eqz v6, :cond_7

    .line 1071771
    iget-object v7, v5, LX/6Hh;->b:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1071772
    :cond_7
    :goto_4
    iget-object v9, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v9}, LX/0Sh;->a()V

    .line 1071773
    new-instance v9, LX/6HV;

    iget-object v10, v0, LX/6HU;->g:Landroid/content/Context;

    iget-object v11, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v12, v0, LX/6HU;->n:LX/6Ha;

    iget-object v13, v0, LX/6HU;->o:LX/6He;

    iget-object v14, v0, LX/6HU;->B:LX/6HF;

    invoke-direct/range {v9 .. v14}, LX/6HV;-><init>(Landroid/content/Context;Landroid/hardware/Camera;LX/6Ha;LX/6He;LX/6HF;)V

    iput-object v9, v0, LX/6HU;->e:LX/6HV;

    .line 1071774
    iget-object v9, v0, LX/6HU;->e:LX/6HV;

    iget-object v10, v0, LX/6HU;->h:LX/6HT;

    .line 1071775
    iput-object v10, v9, LX/6HV;->d:LX/6HS;

    .line 1071776
    iget-object v9, v0, LX/6HU;->e:LX/6HV;

    invoke-virtual {v9, v0}, LX/6HV;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1071777
    iget-object v9, v0, LX/6HU;->a:LX/6HO;

    iget-object v10, v0, LX/6HU;->e:LX/6HV;

    invoke-interface {v9, v10}, LX/6HO;->a(LX/6HV;)V

    .line 1071778
    invoke-static {v0}, LX/6HU;->L(LX/6HU;)V

    .line 1071779
    iget-object v5, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v5}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    .line 1071780
    new-instance v6, LX/6HR;

    invoke-direct {v6, v0}, LX/6HR;-><init>(LX/6HU;)V

    .line 1071781
    iget-object v7, v0, LX/6HU;->a:LX/6HO;

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v7, v8, v9, v6}, LX/6HO;->a(Ljava/util/List;Ljava/util/List;LX/6HR;)V

    .line 1071782
    iget-object v7, v6, LX/6HR;->b:Landroid/hardware/Camera$Size;

    if-eqz v7, :cond_8

    .line 1071783
    iget-object v7, v6, LX/6HR;->b:Landroid/hardware/Camera$Size;

    iget v7, v7, Landroid/hardware/Camera$Size;->width:I

    iget-object v8, v6, LX/6HR;->b:Landroid/hardware/Camera$Size;

    iget v8, v8, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v7, v8}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 1071784
    :cond_8
    const/16 v7, 0x55

    invoke-virtual {v5, v7}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    .line 1071785
    iget-object v7, v6, LX/6HR;->a:Landroid/hardware/Camera$Size;

    if-eqz v7, :cond_9

    .line 1071786
    iget-object v7, v6, LX/6HR;->a:Landroid/hardware/Camera$Size;

    iget v7, v7, Landroid/hardware/Camera$Size;->width:I

    iget-object v6, v6, LX/6HR;->a:Landroid/hardware/Camera$Size;

    iget v6, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v7, v6}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1071787
    :cond_9
    :try_start_0
    iget-object v6, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v6, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1071788
    :goto_5
    :try_start_1
    sget-boolean v5, LX/6Hs;->c:Z

    move v5, v5

    .line 1071789
    if-eqz v5, :cond_12

    .line 1071790
    iget-object v5, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v6, v0, LX/6HU;->a:LX/6HO;

    invoke-interface {v6}, LX/6HO;->f()LX/6IG;

    move-result-object v6

    iget v6, v6, LX/6IG;->mReverseRotation:I

    add-int/lit16 v6, v6, 0xb4

    invoke-virtual {v5, v6}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1071791
    :goto_6
    iget-object v5, v0, LX/6HU;->o:LX/6He;

    if-eqz v5, :cond_a

    .line 1071792
    iget-object v5, v0, LX/6HU;->o:LX/6He;

    const/4 v6, 0x0

    .line 1071793
    iget-object v7, v5, LX/6He;->b:Landroid/hardware/Camera;

    invoke-virtual {v7}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getMaxNumDetectedFaces()I

    move-result v7

    if-gtz v7, :cond_13

    .line 1071794
    iput-boolean v6, v5, LX/6He;->e:Z

    .line 1071795
    :goto_7
    move v5, v6

    .line 1071796
    if-eqz v5, :cond_a

    .line 1071797
    iget-object v5, v0, LX/6HU;->o:LX/6He;

    invoke-static {v0}, LX/6HU;->E(LX/6HU;)I

    move-result v6

    .line 1071798
    iput v6, v5, LX/6He;->d:I

    .line 1071799
    :cond_a
    invoke-static {v0}, LX/6HU;->x(LX/6HU;)V

    .line 1071800
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    .line 1071801
    iput-object v3, v0, LX/6HU;->r:LX/6HP;

    .line 1071802
    iget-object v0, p0, LX/6HP;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->B:LX/6HF;

    invoke-interface {v0}, LX/6HF;->d()V

    goto/16 :goto_2

    .line 1071803
    :cond_b
    iget-object v6, v1, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "android.hardware.camera"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_c

    iget-object v6, v1, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "android.hardware.camera.front"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    :cond_c
    const/4 v5, 0x1

    .line 1071804
    :cond_d
    if-eqz v5, :cond_0

    .line 1071805
    :try_start_2
    invoke-virtual {v1}, LX/6HU;->b()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1071806
    const v5, 0x3f9e7166

    invoke-static {v2, v5}, LX/0J2;->a(II)Landroid/hardware/Camera;

    move-result-object v4

    goto/16 :goto_0

    .line 1071807
    :cond_e
    const/4 v5, 0x0

    const v6, 0x68152171

    invoke-static {v5, v6}, LX/0J2;->a(II)Landroid/hardware/Camera;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v4

    goto/16 :goto_0

    .line 1071808
    :catch_0
    goto/16 :goto_0

    :cond_f
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 1071809
    :cond_10
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    goto/16 :goto_3

    .line 1071810
    :cond_11
    const/4 v5, 0x0

    iput-object v5, v0, LX/6HU;->o:LX/6He;

    goto/16 :goto_4

    .line 1071811
    :catch_1
    move-exception v5

    .line 1071812
    iget-object v6, v0, LX/6HU;->B:LX/6HF;

    const-string v7, "initCameraSettings/setParameters failed"

    invoke-interface {v6, v7, v5}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 1071813
    :cond_12
    :try_start_3
    iget-object v5, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v6, v0, LX/6HU;->a:LX/6HO;

    invoke-interface {v6}, LX/6HO;->f()LX/6IG;

    move-result-object v6

    iget v6, v6, LX/6IG;->mReverseRotation:I

    invoke-virtual {v5, v6}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_6

    .line 1071814
    :catch_2
    move-exception v5

    .line 1071815
    iget-object v6, v0, LX/6HU;->B:LX/6HF;

    const-string v7, "initCameraSettings/setDisplayOrientation failed"

    invoke-interface {v6, v7, v5}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_6

    .line 1071816
    :cond_13
    iget-boolean v7, v5, LX/6He;->e:Z

    if-eqz v7, :cond_14

    .line 1071817
    iget-object v6, v5, LX/6He;->b:Landroid/hardware/Camera;

    invoke-virtual {v6, v5}, Landroid/hardware/Camera;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    .line 1071818
    const/4 v6, 0x1

    goto/16 :goto_7

    .line 1071819
    :cond_14
    iput-boolean v6, v5, LX/6He;->e:Z

    goto/16 :goto_7
.end method
