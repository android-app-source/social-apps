.class public final LX/5Fd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 885900
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 885901
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885902
    :goto_0
    return v1

    .line 885903
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885904
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 885905
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 885906
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 885908
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 885909
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 885910
    :cond_2
    const-string v5, "latest_version"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 885911
    const/4 v4, 0x0

    .line 885912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_a

    .line 885913
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885914
    :goto_2
    move v2, v4

    .line 885915
    goto :goto_1

    .line 885916
    :cond_3
    const-string v5, "owner_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 885917
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 885918
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 885919
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 885920
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 885921
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 885922
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 885923
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885924
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 885925
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 885926
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_7

    if-eqz v6, :cond_7

    .line 885928
    const-string v7, "article_canonical_url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 885929
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 885930
    :cond_8
    const-string v7, "feed_cover_config"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 885931
    const/4 v6, 0x0

    .line 885932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_f

    .line 885933
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885934
    :goto_4
    move v2, v6

    .line 885935
    goto :goto_3

    .line 885936
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 885937
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 885938
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 885939
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v2, v4

    move v5, v4

    goto :goto_3

    .line 885940
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885941
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 885942
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 885943
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885944
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_c

    if-eqz v8, :cond_c

    .line 885945
    const-string v9, "config_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 885946
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 885947
    :cond_d
    const-string v9, "config_version"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 885948
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_5

    .line 885949
    :cond_e
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 885950
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 885951
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 885952
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_4

    :cond_f
    move v2, v6

    move v7, v6

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 885953
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885954
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885955
    if-eqz v0, :cond_0

    .line 885956
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885957
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885958
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885959
    if-eqz v0, :cond_5

    .line 885960
    const-string v1, "latest_version"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885961
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885962
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 885963
    if-eqz v1, :cond_1

    .line 885964
    const-string v2, "article_canonical_url"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885965
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885966
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 885967
    if-eqz v1, :cond_4

    .line 885968
    const-string v2, "feed_cover_config"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885969
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885970
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 885971
    if-eqz v2, :cond_2

    .line 885972
    const-string v0, "config_name"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885973
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885974
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 885975
    if-eqz v2, :cond_3

    .line 885976
    const-string v0, "config_version"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885977
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885978
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885979
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885980
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885981
    if-eqz v0, :cond_6

    .line 885982
    const-string v1, "owner_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885983
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885984
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885985
    return-void
.end method
