.class public LX/6V7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6V2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6V2",
        "<",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103497
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1103498
    const-class v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1103499
    check-cast p1, Landroid/widget/TextView;

    .line 1103500
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1103501
    const/4 v0, 0x0

    .line 1103502
    if-eqz v1, :cond_0

    .line 1103503
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1103504
    :cond_0
    const-string v1, "textview_text"

    if-nez v0, :cond_1

    const-string v0, "null"

    :cond_1
    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103505
    return-void
.end method
