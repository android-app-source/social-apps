.class public abstract LX/6Lb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<TPARAMS;TRESU",
        "LT;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/6LZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6LZ",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field public c:LX/1Mv;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public d:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<TPARAMS;TRESU",
            "LT;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1078589
    new-instance v0, LX/6LZ;

    const/4 v1, 0x0

    sget-object v2, LX/6La;->NOT_AVAILABLE:LX/6La;

    invoke-direct {v0, v1, v2}, LX/6LZ;-><init>(Ljava/lang/Object;LX/6La;)V

    sput-object v0, LX/6Lb;->a:LX/6LZ;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 1078599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078600
    invoke-static {}, LX/6Lc;->a()LX/3Mb;

    move-result-object v0

    iput-object v0, p0, LX/6Lb;->d:LX/3Mb;

    .line 1078601
    iput-object p1, p0, LX/6Lb;->b:Ljava/util/concurrent/Executor;

    .line 1078602
    return-void
.end method

.method public static a$redex0(LX/6Lb;Ljava/lang/Object;LX/6LZ;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1078604
    invoke-virtual {p0, p1, p2}, LX/6Lb;->b(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1078605
    invoke-virtual {p0}, LX/6Lb;->b()V

    .line 1078606
    iget-object v1, p0, LX/6Lb;->d:LX/3Mb;

    invoke-interface {v1, p1, v0}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1078607
    new-instance v1, LX/6LX;

    invoke-direct {v1, p0, p1, p3}, LX/6LX;-><init>(LX/6Lb;Ljava/lang/Object;I)V

    .line 1078608
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    iput-object v2, p0, LX/6Lb;->c:LX/1Mv;

    .line 1078609
    iget-object v2, p0, LX/6Lb;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1078610
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078603
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1078590
    iget-object v0, p0, LX/6Lb;->c:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1078591
    iget-object v0, p0, LX/6Lb;->c:LX/1Mv;

    .line 1078592
    const/4 v1, 0x0

    iput-object v1, p0, LX/6Lb;->c:LX/1Mv;

    .line 1078593
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1078594
    :cond_0
    return-void
.end method

.method public final a(LX/3Mb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Mb",
            "<TPARAMS;TRESU",
            "LT;",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1078595
    if-nez p1, :cond_0

    .line 1078596
    invoke-static {}, LX/6Lc;->a()LX/3Mb;

    move-result-object v0

    iput-object v0, p0, LX/6Lb;->d:LX/3Mb;

    .line 1078597
    :goto_0
    return-void

    .line 1078598
    :cond_0
    iput-object p1, p0, LX/6Lb;->d:LX/3Mb;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)V"
        }
    .end annotation

    .prologue
    .line 1078576
    invoke-virtual {p0}, LX/6Lb;->a()V

    .line 1078577
    invoke-virtual {p0, p1}, LX/6Lb;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    .line 1078578
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1078579
    invoke-virtual {v0}, LX/6LZ;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1078580
    iget-object v1, v0, LX/6LZ;->a:Ljava/lang/Object;

    .line 1078581
    iget-object v2, p0, LX/6Lb;->d:LX/3Mb;

    invoke-interface {v2, p1, v1}, LX/3Mb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1078582
    iget-object v2, v0, LX/6LZ;->b:LX/6La;

    sget-object v3, LX/6La;->FINAL:LX/6La;

    if-ne v2, v3, :cond_0

    .line 1078583
    iget-object v0, p0, LX/6Lb;->d:LX/3Mb;

    invoke-interface {v0, p1, v1}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1078584
    :goto_0
    return-void

    .line 1078585
    :cond_0
    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, LX/6Lb;->a$redex0(LX/6Lb;Ljava/lang/Object;LX/6LZ;I)V

    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)LX/6LZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1078586
    invoke-virtual {p0, p1, p2}, LX/6Lb;->a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1078587
    new-instance v1, LX/6LY;

    invoke-direct {v1, p0}, LX/6LY;-><init>(LX/6Lb;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 1078588
    return-void
.end method
