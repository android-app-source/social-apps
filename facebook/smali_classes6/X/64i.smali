.class public final LX/64i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field private c:Ljava/lang/Runnable;

.field private d:Ljava/util/concurrent/ExecutorService;

.field public final e:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lokhttp3/RealCall$AsyncCall;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lokhttp3/RealCall$AsyncCall;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "LX/64y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1045383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045384
    const/16 v0, 0x40

    iput v0, p0, LX/64i;->a:I

    .line 1045385
    const/4 v0, 0x5

    iput v0, p0, LX/64i;->b:I

    .line 1045386
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/64i;->e:Ljava/util/Deque;

    .line 1045387
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    .line 1045388
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/64i;->g:Ljava/util/Deque;

    .line 1045389
    return-void
.end method

.method private a(Ljava/util/Deque;Ljava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Deque",
            "<TT;>;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 1045345
    monitor-enter p0

    .line 1045346
    :try_start_0
    invoke-interface {p1, p2}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Call wasn\'t in-flight!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1045347
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1045348
    :cond_0
    if-eqz p3, :cond_1

    .line 1045349
    :try_start_1
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    iget v1, p0, LX/64i;->a:I

    if-lt v0, v1, :cond_3

    .line 1045350
    :cond_1
    :goto_0
    invoke-direct {p0}, LX/64i;->c()I

    move-result v0

    .line 1045351
    iget-object v1, p0, LX/64i;->c:Ljava/lang/Runnable;

    .line 1045352
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1045353
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 1045354
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1045355
    :cond_2
    return-void

    .line 1045356
    :cond_3
    iget-object v0, p0, LX/64i;->e:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1045357
    iget-object v0, p0, LX/64i;->e:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1045358
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/RealCall$AsyncCall;

    .line 1045359
    invoke-static {p0, v0}, LX/64i;->c(LX/64i;Lokhttp3/RealCall$AsyncCall;)I

    move-result p1

    iget p2, p0, LX/64i;->b:I

    if-ge p1, p2, :cond_5

    .line 1045360
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1045361
    iget-object p1, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {p1, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 1045362
    invoke-virtual {p0}, LX/64i;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    const p2, 0x44df88a0

    invoke-static {p1, v0, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1045363
    :cond_5
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    iget p1, p0, LX/64i;->a:I

    if-lt v0, p1, :cond_4

    goto :goto_0
.end method

.method private declared-synchronized c()I
    .locals 2

    .prologue
    .line 1045382
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    iget-object v1, p0, LX/64i;->g:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/64i;Lokhttp3/RealCall$AsyncCall;)I
    .locals 4

    .prologue
    .line 1045377
    const/4 v0, 0x0

    .line 1045378
    iget-object v1, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/RealCall$AsyncCall;

    .line 1045379
    invoke-virtual {v0}, Lokhttp3/RealCall$AsyncCall;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lokhttp3/RealCall$AsyncCall;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1045380
    goto :goto_0

    .line 1045381
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/concurrent/ExecutorService;
    .locals 9

    .prologue
    .line 1045372
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/64i;->d:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 1045373
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp Dispatcher"

    const/4 v8, 0x0

    .line 1045374
    invoke-static {v0, v8}, LX/65A;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, LX/64i;->d:Ljava/util/concurrent/ExecutorService;

    .line 1045375
    :cond_0
    iget-object v0, p0, LX/64i;->d:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1045376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lokhttp3/RealCall$AsyncCall;)V
    .locals 2

    .prologue
    .line 1045366
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    iget v1, p0, LX/64i;->a:I

    if-ge v0, v1, :cond_0

    invoke-static {p0, p1}, LX/64i;->c(LX/64i;Lokhttp3/RealCall$AsyncCall;)I

    move-result v0

    iget v1, p0, LX/64i;->b:I

    if-ge v0, v1, :cond_0

    .line 1045367
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 1045368
    invoke-virtual {p0}, LX/64i;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const v1, -0x31b34da9

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1045369
    :goto_0
    monitor-exit p0

    return-void

    .line 1045370
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/64i;->e:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1045371
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lokhttp3/RealCall$AsyncCall;)V
    .locals 2

    .prologue
    .line 1045364
    iget-object v0, p0, LX/64i;->f:Ljava/util/Deque;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, LX/64i;->a(Ljava/util/Deque;Ljava/lang/Object;Z)V

    .line 1045365
    return-void
.end method
