.class public final LX/65E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public a:Z

.field public final synthetic b:LX/671;

.field public final synthetic c:LX/65G;

.field public final synthetic d:LX/670;

.field public final synthetic e:LX/65F;


# direct methods
.method public constructor <init>(LX/65F;LX/671;LX/65G;LX/670;)V
    .locals 0

    .prologue
    .line 1046635
    iput-object p1, p0, LX/65E;->e:LX/65F;

    iput-object p2, p0, LX/65E;->b:LX/671;

    iput-object p3, p0, LX/65E;->c:LX/65G;

    iput-object p4, p0, LX/65E;->d:LX/670;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    const/4 v3, 0x1

    .line 1046636
    :try_start_0
    iget-object v2, p0, LX/65E;->b:LX/671;

    invoke-interface {v2, p1, p2, p3}, LX/65D;->a(LX/672;J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 1046637
    cmp-long v2, v4, v0

    if-nez v2, :cond_2

    .line 1046638
    iget-boolean v2, p0, LX/65E;->a:Z

    if-nez v2, :cond_0

    .line 1046639
    iput-boolean v3, p0, LX/65E;->a:Z

    .line 1046640
    iget-object v2, p0, LX/65E;->d:LX/670;

    invoke-interface {v2}, LX/65J;->close()V

    :cond_0
    move-wide v4, v0

    .line 1046641
    :goto_0
    return-wide v4

    .line 1046642
    :catch_0
    move-exception v0

    .line 1046643
    iget-boolean v1, p0, LX/65E;->a:Z

    if-nez v1, :cond_1

    .line 1046644
    iput-boolean v3, p0, LX/65E;->a:Z

    .line 1046645
    :cond_1
    throw v0

    .line 1046646
    :cond_2
    iget-object v0, p0, LX/65E;->d:LX/670;

    invoke-interface {v0}, LX/670;->c()LX/672;

    move-result-object v1

    .line 1046647
    iget-wide v6, p1, LX/672;->b:J

    move-wide v2, v6

    .line 1046648
    sub-long/2addr v2, v4

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/672;->a(LX/672;JJ)LX/672;

    .line 1046649
    iget-object v0, p0, LX/65E;->d:LX/670;

    invoke-interface {v0}, LX/670;->t()LX/670;

    goto :goto_0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1046634
    iget-object v0, p0, LX/65E;->b:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 1046629
    iget-boolean v0, p0, LX/65E;->a:Z

    if-nez v0, :cond_0

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1046630
    invoke-static {p0, v0, v1}, LX/65A;->a(LX/65D;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1046631
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/65E;->a:Z

    .line 1046632
    :cond_0
    iget-object v0, p0, LX/65E;->b:LX/671;

    invoke-interface {v0}, LX/65D;->close()V

    .line 1046633
    return-void
.end method
