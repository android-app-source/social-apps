.class public LX/6ED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6EC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065945
    const-class v0, LX/6ED;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6ED;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065944
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065923
    const-string v0, "hasCapability"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 6

    .prologue
    .line 1065924
    check-cast p1, Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;

    .line 1065925
    iget-object v0, p0, LX/6ED;->c:LX/6EC;

    .line 1065926
    iput-object p1, v0, LX/6EC;->a:Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;

    .line 1065927
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1065928
    const-string v0, "capabilities"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v0, v0

    .line 1065929
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1065930
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1065931
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "hasCamera"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1065932
    iget-object v3, p0, LX/6ED;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1065933
    const-string v4, "hasCamera"

    const-string v5, "android.hardware.camera.any"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1065934
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1065935
    :cond_1
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "supportsPayments"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1065936
    const-string v3, "supportsPayments"

    .line 1065937
    const-string v4, "JS_BRIDGE_PAGE_POLICY_URL"

    invoke-virtual {p1, v4}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object v4, v4

    .line 1065938
    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1065939
    :catch_0
    move-exception v0

    .line 1065940
    iget-object v1, p0, LX/6ED;->d:LX/03V;

    sget-object v2, LX/6ED;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1065941
    :goto_3
    return-void

    .line 1065942
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/6ED;->c:LX/6EC;

    invoke-virtual {v0, v1}, LX/6EC;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method
