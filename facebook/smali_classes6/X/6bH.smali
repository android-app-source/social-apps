.class public LX/6bH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0TD;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6bK;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6bK;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/6b8;


# direct methods
.method public constructor <init>(LX/0TD;LX/0Or;LX/6b8;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "LX/6bK;",
            ">;",
            "LX/6b8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113171
    iput-object p1, p0, LX/6bH;->a:LX/0TD;

    .line 1113172
    iput-object p2, p0, LX/6bH;->b:LX/0Or;

    .line 1113173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6bH;->c:Ljava/util/Map;

    .line 1113174
    iput-object p3, p0, LX/6bH;->d:LX/6b8;

    .line 1113175
    return-void
.end method

.method private static a(LX/6bH;Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2Mf;",
            "Lcom/facebook/photos/base/media/VideoItem;",
            "LX/6bJ;",
            "Lcom/facebook/media/transcode/MediaTranscodeParameters;",
            "LX/6b7;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6bE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113200
    iget-object v8, p0, LX/6bH;->a:LX/0TD;

    new-instance v0, LX/6bF;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/6bF;-><init>(LX/6bH;Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)V

    invoke-interface {v8, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1113201
    new-instance v1, LX/6bG;

    invoke-direct {v1, p0, p1}, LX/6bG;-><init>(LX/6bH;Ljava/lang/String;)V

    iget-object v2, p0, LX/6bH;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1113202
    return-object v0
.end method

.method public static b(LX/6bH;Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)LX/6bE;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1113203
    if-nez p3, :cond_0

    .line 1113204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must provide non null item to transcode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1113205
    :cond_0
    iget-object v0, p0, LX/6bH;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bK;

    .line 1113206
    iget-object v1, p0, LX/6bH;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, p5

    .line 1113207
    check-cast v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;

    move-object v1, p3

    move-object v2, p2

    move-object v3, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, LX/6bK;->a(Lcom/facebook/photos/base/media/VideoItem;LX/2Mf;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)LX/6bA;

    move-result-object v0

    .line 1113208
    iget-object v1, p0, LX/6bH;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113209
    new-instance v1, LX/6bE;

    .line 1113210
    iget-boolean v2, v0, LX/6bA;->a:Z

    move v2, v2

    .line 1113211
    iget-object v3, v0, LX/6bA;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v0, v3

    .line 1113212
    invoke-direct {v1, v2, v0, p4}, LX/6bE;-><init>(ZLcom/facebook/ipc/media/MediaItem;LX/6bJ;)V

    .line 1113213
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p2    # LX/2Mf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2Mf;",
            "Lcom/facebook/photos/base/media/VideoItem;",
            "LX/6bJ;",
            "Lcom/facebook/media/transcode/MediaTranscodeParameters;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6bE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113198
    iget-object v0, p0, LX/6bH;->d:LX/6b8;

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    invoke-virtual {v0, v1, p1, p6}, LX/6b8;->a(LX/4gF;Ljava/lang/String;Ljava/lang/String;)LX/6b7;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1113199
    invoke-static/range {v0 .. v6}, LX/6bH;->a(LX/6bH;Ljava/lang/String;LX/2Mf;Lcom/facebook/photos/base/media/VideoItem;LX/6bJ;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/base/media/VideoItem;",
            "Lcom/facebook/media/transcode/video/VideoTranscodeParameters;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/6bJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113176
    if-nez p1, :cond_0

    .line 1113177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must provide non null item to transcode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1113178
    :cond_0
    iget-object v0, p0, LX/6bH;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bK;

    .line 1113179
    const/4 v3, 0x0

    .line 1113180
    if-eqz p2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1113181
    const/4 v4, -0x1

    .line 1113182
    const/4 v2, -0x2

    .line 1113183
    iget-object v5, p2, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    if-eqz v5, :cond_3

    .line 1113184
    iget-object v5, p2, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1113185
    iget-boolean v3, v5, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    if-eqz v3, :cond_2

    .line 1113186
    iget v3, v5, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    .line 1113187
    iget v2, v5, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    .line 1113188
    :goto_1
    iget-boolean v4, v5, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    .line 1113189
    :goto_2
    const/4 v7, 0x0

    .line 1113190
    :try_start_0
    iget-object v8, v0, LX/6bK;->a:LX/2MV;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v9

    invoke-interface {v8, v9}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    move-object v9, v8

    .line 1113191
    :goto_3
    if-nez v9, :cond_4

    .line 1113192
    :goto_4
    move-object v2, v7

    .line 1113193
    move-object v0, v2

    .line 1113194
    return-object v0

    :cond_1
    move v2, v3

    .line 1113195
    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    :cond_3
    move v6, v4

    move v4, v3

    move v3, v6

    goto :goto_2

    .line 1113196
    :catch_0
    move-exception v8

    move-object v9, v7

    goto :goto_3

    .line 1113197
    :cond_4
    iget-wide v7, v9, LX/60x;->a:J

    iget v9, v9, LX/60x;->g:I

    move v10, v4

    move v11, v3

    move v12, v2

    invoke-static/range {v7 .. v12}, LX/6bK;->a(JIZII)Ljava/util/List;

    move-result-object v7

    goto :goto_4
.end method
