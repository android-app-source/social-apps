.class public final LX/6Rx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1091321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1091322
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1091323
    :goto_0
    return v1

    .line 1091324
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 1091325
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1091326
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1091327
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1091328
    const-string v12, "length"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1091329
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1091330
    :cond_1
    const-string v12, "reactions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1091331
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1091332
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_2

    .line 1091333
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_2

    .line 1091334
    invoke-static {p0, p1}, LX/6Rw;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1091335
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1091336
    :cond_2
    invoke-static {v10, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v10

    move v10, v10

    .line 1091337
    goto :goto_1

    .line 1091338
    :cond_3
    const-string v12, "start_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1091339
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v8

    move v6, v7

    goto :goto_1

    .line 1091340
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1091341
    :cond_5
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1091342
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1091343
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1091344
    :cond_6
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 1091345
    if-eqz v6, :cond_7

    .line 1091346
    const/4 v1, 0x2

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1091347
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v6, v1

    move v0, v1

    move-wide v8, v4

    move v10, v1

    move-wide v2, v4

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1091348
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1091349
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1091350
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1091351
    const-string v2, "length"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091352
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1091353
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091354
    if-eqz v0, :cond_2

    .line 1091355
    const-string v1, "reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091356
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1091357
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1091358
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/6Rw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091359
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1091360
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1091361
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1091362
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 1091363
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091364
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1091365
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1091366
    return-void
.end method
