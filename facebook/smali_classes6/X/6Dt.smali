.class public LX/6Dt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ds;


# instance fields
.field public final a:LX/6tk;

.field private final b:LX/6tK;


# direct methods
.method public constructor <init>(LX/6tk;LX/6tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065683
    iput-object p1, p0, LX/6Dt;->a:LX/6tk;

    .line 1065684
    iput-object p2, p0, LX/6Dt;->b:LX/6tK;

    .line 1065685
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1065686
    iget-object v0, p0, LX/6Dt;->b:LX/6tK;

    invoke-virtual {v0, p1}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1065687
    iget-object v1, p0, LX/6Dt;->b:LX/6tK;

    .line 1065688
    iget-object v0, p0, LX/6Dt;->a:LX/6tk;

    const v2, 0x7f08002a

    invoke-virtual {v0, p1, v2}, LX/6tk;->a(Lcom/facebook/payments/checkout/model/CheckoutData;I)LX/6sw;

    move-result-object v0

    move-object v0, v0

    .line 1065689
    check-cast v0, LX/6sw;

    invoke-virtual {v1, p1, p2, v0}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;LX/6sw;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1065690
    sget-object v0, LX/6Dr;->a:[I

    invoke-virtual {p1}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1065691
    iget-object v0, p0, LX/6Dt;->b:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1065692
    :pswitch_0
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;

    .line 1065693
    new-instance v1, LX/6E4;

    invoke-direct {v1, v0}, LX/6E4;-><init>(Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;)V

    move-object v0, v1

    .line 1065694
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
