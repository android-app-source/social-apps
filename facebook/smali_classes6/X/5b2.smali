.class public final LX/5b2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 956632
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 956633
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956634
    :goto_0
    return v1

    .line 956635
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956636
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 956637
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 956638
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956639
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 956640
    const-string v7, "custom_like_emoji"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 956641
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 956642
    :cond_2
    const-string v7, "incoming_bubble_color"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 956643
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 956644
    :cond_3
    const-string v7, "outgoing_bubble_color"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 956645
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 956646
    :cond_4
    const-string v7, "participant_customizations"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 956647
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 956648
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_5

    .line 956649
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_5

    .line 956650
    const/4 v7, 0x0

    .line 956651
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_d

    .line 956652
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956653
    :goto_3
    move v6, v7

    .line 956654
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 956655
    :cond_5
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 956656
    goto/16 :goto_1

    .line 956657
    :cond_6
    const-string v7, "thread_background_color"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 956658
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 956659
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 956660
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 956661
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 956662
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 956663
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 956664
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 956665
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 956666
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956667
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 956668
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 956669
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956670
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_a

    if-eqz v9, :cond_a

    .line 956671
    const-string v10, "nickname"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 956672
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_4

    .line 956673
    :cond_b
    const-string v10, "participant_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 956674
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 956675
    :cond_c
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 956676
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 956677
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 956678
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_3

    :cond_d
    move v6, v7

    move v8, v7

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 956679
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956680
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956681
    if-eqz v0, :cond_0

    .line 956682
    const-string v1, "custom_like_emoji"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956683
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956684
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956685
    if-eqz v0, :cond_1

    .line 956686
    const-string v1, "incoming_bubble_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956687
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956688
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956689
    if-eqz v0, :cond_2

    .line 956690
    const-string v1, "outgoing_bubble_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956691
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956692
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956693
    if-eqz v0, :cond_6

    .line 956694
    const-string v1, "participant_customizations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956695
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 956696
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 956697
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 956698
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956699
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 956700
    if-eqz v3, :cond_3

    .line 956701
    const-string p3, "nickname"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956702
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956703
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 956704
    if-eqz v3, :cond_4

    .line 956705
    const-string p3, "participant_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956706
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956707
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956708
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 956709
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 956710
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956711
    if-eqz v0, :cond_7

    .line 956712
    const-string v1, "thread_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956713
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956714
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956715
    return-void
.end method
