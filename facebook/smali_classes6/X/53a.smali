.class public LX/53a;
.super LX/52r;
.source ""

# interfaces
.implements LX/0za;


# instance fields
.field public volatile a:Z

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private final c:LX/54N;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ThreadFactory;)V
    .locals 1

    .prologue
    .line 827234
    invoke-direct {p0}, LX/52r;-><init>()V

    .line 827235
    const/4 v0, 0x1

    invoke-static {v0, p1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, LX/53a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 827236
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 827237
    invoke-virtual {v0}, LX/0vB;->d()LX/54N;

    move-result-object v0

    iput-object v0, p0, LX/53a;->c:LX/54N;

    .line 827238
    return-void
.end method


# virtual methods
.method public final a(LX/0vR;)LX/0za;
    .locals 3

    .prologue
    .line 827239
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, LX/52r;->a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 2

    .prologue
    .line 827240
    iget-boolean v0, p0, LX/53a;->a:Z

    if-eqz v0, :cond_0

    .line 827241
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 827242
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/53a;->b(LX/0vR;JLjava/util/concurrent/TimeUnit;)Lrx/internal/schedulers/ScheduledAction;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/0vR;JLjava/util/concurrent/TimeUnit;)Lrx/internal/schedulers/ScheduledAction;
    .locals 4

    .prologue
    .line 827243
    move-object v0, p1

    .line 827244
    new-instance v1, Lrx/internal/schedulers/ScheduledAction;

    invoke-direct {v1, v0}, Lrx/internal/schedulers/ScheduledAction;-><init>(LX/0vR;)V

    .line 827245
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-gtz v0, :cond_0

    .line 827246
    iget-object v0, p0, LX/53a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    const v2, 0xe394133

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 827247
    :goto_0
    iget-object v2, v1, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    new-instance v3, LX/53b;

    invoke-direct {v3, v1, v0}, LX/53b;-><init>(Lrx/internal/schedulers/ScheduledAction;Ljava/util/concurrent/Future;)V

    invoke-virtual {v2, v3}, LX/54i;->a(LX/0za;)V

    .line 827248
    return-object v1

    .line 827249
    :cond_0
    iget-object v0, p0, LX/53a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, v1, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 827250
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/53a;->a:Z

    .line 827251
    iget-object v0, p0, LX/53a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 827252
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827253
    iget-boolean v0, p0, LX/53a;->a:Z

    return v0
.end method
