.class public LX/6dM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "LX/6dL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1115795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115796
    iput-object p1, p0, LX/6dM;->a:Landroid/database/Cursor;

    .line 1115797
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1115798
    iget-object v0, p0, LX/6dM;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1115799
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/6dL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1115800
    new-instance v0, LX/6dK;

    iget-object v1, p0, LX/6dM;->a:Landroid/database/Cursor;

    invoke-direct {v0, v1}, LX/6dK;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method
