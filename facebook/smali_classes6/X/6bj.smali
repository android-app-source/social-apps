.class public LX/6bj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113845
    const-class v0, LX/6bj;

    sput-object v0, LX/6bj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1113846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 1113847
    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1113848
    array-length v3, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1113849
    sget-object v2, LX/6bj;->a:Ljava/lang/Class;

    const-string v3, "Invalid fileName does not contain duration."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1113850
    :goto_0
    return-wide v0

    .line 1113851
    :cond_0
    const/4 v3, 0x2

    aget-object v2, v2, v3

    .line 1113852
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1113853
    if-gez v3, :cond_1

    .line 1113854
    sget-object v2, LX/6bj;->a:Ljava/lang/Class;

    const-string v3, "Invalid fileName does not contain duration."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 1113855
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1113856
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 1113857
    :catch_0
    sget-object v2, LX/6bj;->a:Ljava/lang/Class;

    const-string v3, "Invalid fileName does not contain duration."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method
