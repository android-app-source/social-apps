.class public LX/6Dv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Du;


# instance fields
.field private final a:LX/6FE;


# direct methods
.method public constructor <init>(LX/6FE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065696
    iput-object p1, p0, LX/6Dv;->a:LX/6FE;

    .line 1065697
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1065704
    iget-object v0, p0, LX/6Dv;->a:LX/6FE;

    .line 1065705
    iget-object v1, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065706
    iput-object p1, v0, LX/6FE;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1065707
    invoke-static {p1}, LX/6FJ;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v1

    .line 1065708
    new-instance v2, LX/0Ew;

    invoke-direct {v2}, LX/0Ew;-><init>()V

    .line 1065709
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1065710
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1065711
    sget-object p0, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v3, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1065712
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object p0

    .line 1065713
    if-eqz p0, :cond_4

    invoke-interface {p0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object p0

    :goto_0
    move-object p0, p0

    .line 1065714
    iput-object p0, v2, LX/0Ew;->a:Ljava/lang/String;

    .line 1065715
    :cond_0
    sget-object p0, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v3, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v4, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1065716
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object p0

    .line 1065717
    invoke-static {p0}, LX/47j;->a(LX/0am;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1065718
    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v4}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1065719
    iput-object v4, v2, LX/0Ew;->b:Ljava/lang/String;

    .line 1065720
    :cond_1
    sget-object v4, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1065721
    new-instance v4, LX/0Eh;

    invoke-direct {v4}, LX/0Eh;-><init>()V

    .line 1065722
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v3

    .line 1065723
    invoke-static {v3}, LX/47j;->a(LX/0am;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1065724
    invoke-virtual {v4}, LX/0Eh;->h()Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    move-result-object v3

    .line 1065725
    :goto_2
    move-object v3, v3

    .line 1065726
    iput-object v3, v2, LX/0Ew;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    .line 1065727
    :cond_2
    if-eqz v1, :cond_3

    .line 1065728
    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v3

    .line 1065729
    iput-object v3, v2, LX/0Ew;->c:Ljava/lang/String;

    .line 1065730
    move-object v3, v2

    .line 1065731
    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f()Ljava/lang/String;

    move-result-object v1

    .line 1065732
    iput-object v1, v3, LX/0Ew;->d:Ljava/lang/String;

    .line 1065733
    :cond_3
    iget-object v1, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    iget-object v3, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    invoke-virtual {v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v3

    .line 1065734
    new-instance v4, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;

    invoke-direct {v4, v2}, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;-><init>(LX/0Ew;)V

    move-object v2, v4

    .line 1065735
    invoke-static {v3, v2}, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->a(Ljava/lang/String;Lcom/facebook/browserextensions/ipc/UserCredentialInfo;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1065736
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_4
    const-string p0, ""

    goto :goto_0

    .line 1065737
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 1065738
    :cond_6
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1065739
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->b()Ljava/lang/String;

    move-result-object p0

    .line 1065740
    iput-object p0, v4, LX/0Eh;->a:Ljava/lang/String;

    .line 1065741
    move-object v4, v4

    .line 1065742
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->c()Ljava/lang/String;

    move-result-object p0

    .line 1065743
    iput-object p0, v4, LX/0Eh;->b:Ljava/lang/String;

    .line 1065744
    move-object v4, v4

    .line 1065745
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->h()Ljava/lang/String;

    move-result-object p0

    .line 1065746
    iput-object p0, v4, LX/0Eh;->d:Ljava/lang/String;

    .line 1065747
    move-object v4, v4

    .line 1065748
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->i()Ljava/lang/String;

    move-result-object p0

    .line 1065749
    iput-object p0, v4, LX/0Eh;->e:Ljava/lang/String;

    .line 1065750
    move-object v4, v4

    .line 1065751
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->e()Ljava/lang/String;

    move-result-object p0

    .line 1065752
    iput-object p0, v4, LX/0Eh;->f:Ljava/lang/String;

    .line 1065753
    move-object v4, v4

    .line 1065754
    invoke-interface {v3}, Lcom/facebook/payments/shipping/model/MailingAddress;->f()Lcom/facebook/common/locale/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v3

    .line 1065755
    iput-object v3, v4, LX/0Eh;->g:Ljava/lang/String;

    .line 1065756
    move-object v3, v4

    .line 1065757
    invoke-virtual {v3}, LX/0Eh;->h()Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    move-result-object v3

    goto :goto_2
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1065703
    return-void
.end method

.method public final a(LX/6Ex;)V
    .locals 0

    .prologue
    .line 1065702
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1065701
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1065700
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1065699
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1065698
    return-void
.end method
