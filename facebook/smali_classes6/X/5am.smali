.class public final LX/5am;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 956145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 956146
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 956147
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 956148
    const/4 v2, 0x0

    .line 956149
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 956150
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956151
    :goto_1
    move v1, v2

    .line 956152
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 956153
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 956154
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956155
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 956156
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 956157
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956158
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 956159
    const-string v5, "reaction"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 956160
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 956161
    :cond_3
    const-string v5, "user"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 956162
    invoke-static {p0, p1}, LX/5al;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 956163
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 956164
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 956165
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 956166
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 956129
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 956130
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 956131
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 956132
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956133
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 956134
    if-eqz v2, :cond_0

    .line 956135
    const-string v3, "reaction"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956136
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956137
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 956138
    if-eqz v2, :cond_1

    .line 956139
    const-string v3, "user"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956140
    invoke-static {p0, v2, p2}, LX/5al;->a(LX/15i;ILX/0nX;)V

    .line 956141
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 956143
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 956144
    return-void
.end method
