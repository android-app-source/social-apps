.class public final LX/6HT;
.super LX/6HS;
.source ""


# instance fields
.field public final synthetic a:LX/6HU;


# direct methods
.method public constructor <init>(LX/6HU;)V
    .locals 0

    .prologue
    .line 1071840
    iput-object p1, p0, LX/6HT;->a:LX/6HU;

    invoke-direct {p0}, LX/6HS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1071841
    iget-object v0, p0, LX/6HT;->a:LX/6HU;

    const/4 v1, 0x0

    .line 1071842
    iput-boolean v1, v0, LX/6HU;->s:Z

    .line 1071843
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1071844
    if-eqz p1, :cond_2

    .line 1071845
    iget-object v0, p0, LX/6HT;->a:LX/6HU;

    const/4 p1, 0x1

    .line 1071846
    iget-object v1, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1071847
    iput-boolean p1, v0, LX/6HU;->s:Z

    .line 1071848
    iget-object v1, v0, LX/6HU;->a:LX/6HO;

    invoke-interface {v1, p1}, LX/6HO;->a(Z)V

    .line 1071849
    iget-object v1, v0, LX/6HU;->a:LX/6HO;

    iget-object p0, v0, LX/6HU;->f:LX/6HI;

    invoke-virtual {p0}, LX/6HI;->a()Z

    move-result p0

    invoke-interface {v1, p0}, LX/6HO;->b(Z)V

    .line 1071850
    iget-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1071851
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1071852
    sget-boolean v1, LX/6Hs;->a:Z

    move v1, v1

    .line 1071853
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/6HU;->E(LX/6HU;)I

    move-result v1

    if-eq v1, p1, :cond_1

    .line 1071854
    :cond_0
    new-instance v1, LX/6Hb;

    iget-object p0, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object p1, v0, LX/6HU;->B:LX/6HF;

    invoke-direct {v1, p0, p1}, LX/6Hb;-><init>(Landroid/hardware/Camera;LX/6HF;)V

    iput-object v1, v0, LX/6HU;->m:LX/6Hb;

    .line 1071855
    new-instance v1, Landroid/view/ScaleGestureDetector;

    iget-object p0, v0, LX/6HU;->g:Landroid/content/Context;

    iget-object p1, v0, LX/6HU;->m:LX/6Hb;

    invoke-direct {v1, p0, p1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, v0, LX/6HU;->l:Landroid/view/ScaleGestureDetector;

    .line 1071856
    :cond_1
    :goto_0
    return-void

    .line 1071857
    :cond_2
    iget-object v0, p0, LX/6HT;->a:LX/6HU;

    .line 1071858
    iget-object v1, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1071859
    iget-object v1, v0, LX/6HU;->a:LX/6HO;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, LX/6HO;->a(Z)V

    .line 1071860
    goto :goto_0
.end method
