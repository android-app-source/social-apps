.class public final LX/5SS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/java2js/Invokable;


# instance fields
.field private final mJSContext:Lcom/facebook/java2js/JSContext;

.field private final mMethod:Ljava/lang/reflect/Method;

.field private final mObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;Ljava/lang/reflect/Method;)V
    .locals 0

    .prologue
    .line 918028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 918029
    iput-object p1, p0, LX/5SS;->mJSContext:Lcom/facebook/java2js/JSContext;

    .line 918030
    iput-object p2, p0, LX/5SS;->mObject:Ljava/lang/Object;

    .line 918031
    iput-object p3, p0, LX/5SS;->mMethod:Ljava/lang/reflect/Method;

    .line 918032
    return-void
.end method


# virtual methods
.method public invoke([Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
    .locals 5

    .prologue
    .line 918033
    iget-object v0, p0, LX/5SS;->mMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    .line 918034
    array-length v0, p1

    new-array v2, v0, [Ljava/lang/Object;

    .line 918035
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 918036
    aget-object v3, p1, v0

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 918037
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918038
    :cond_0
    iget-object v0, p0, LX/5SS;->mJSContext:Lcom/facebook/java2js/JSContext;

    iget-object v1, p0, LX/5SS;->mMethod:Ljava/lang/reflect/Method;

    iget-object v3, p0, LX/5SS;->mObject:Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method
