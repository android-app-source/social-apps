.class public abstract LX/66A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public final a:LX/675;

.field public b:Z

.field public final synthetic c:LX/66H;


# direct methods
.method public constructor <init>(LX/66H;)V
    .locals 2

    .prologue
    .line 1049542
    iput-object p1, p0, LX/66A;->c:LX/66H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049543
    new-instance v0, LX/675;

    iget-object v1, p0, LX/66A;->c:LX/66H;

    iget-object v1, v1, LX/66H;->c:LX/671;

    invoke-interface {v1}, LX/65D;->a()LX/65f;

    move-result-object v1

    invoke-direct {v0, v1}, LX/675;-><init>(LX/65f;)V

    iput-object v0, p0, LX/66A;->a:LX/675;

    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1049544
    iget-object v0, p0, LX/66A;->a:LX/675;

    return-object v0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x6

    .line 1049545
    iget-object v0, p0, LX/66A;->c:LX/66H;

    iget v0, v0, LX/66H;->e:I

    if-ne v0, v2, :cond_1

    .line 1049546
    :cond_0
    :goto_0
    return-void

    .line 1049547
    :cond_1
    iget-object v0, p0, LX/66A;->c:LX/66H;

    iget v0, v0, LX/66H;->e:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66A;->c:LX/66H;

    iget v2, v2, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049548
    :cond_2
    iget-object v1, p0, LX/66A;->a:LX/675;

    .line 1049549
    iget-object v3, v1, LX/675;->a:LX/65f;

    move-object v3, v3

    .line 1049550
    sget-object v0, LX/65f;->b:LX/65f;

    invoke-virtual {v1, v0}, LX/675;->a(LX/65f;)LX/675;

    .line 1049551
    invoke-virtual {v3}, LX/65f;->f()LX/65f;

    .line 1049552
    invoke-virtual {v3}, LX/65f;->bL_()LX/65f;

    .line 1049553
    iget-object v0, p0, LX/66A;->c:LX/66H;

    .line 1049554
    iput v2, v0, LX/66H;->e:I

    .line 1049555
    iget-object v0, p0, LX/66A;->c:LX/66H;

    iget-object v0, v0, LX/66H;->b:LX/65W;

    if-eqz v0, :cond_0

    .line 1049556
    iget-object v0, p0, LX/66A;->c:LX/66H;

    iget-object v1, v0, LX/66H;->b:LX/65W;

    if-nez p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, LX/66A;->c:LX/66H;

    invoke-virtual {v1, v0, v2}, LX/65W;->a(ZLX/66G;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
