.class public LX/675;
.super LX/65f;
.source ""


# instance fields
.field public a:LX/65f;


# direct methods
.method public constructor <init>(LX/65f;)V
    .locals 2

    .prologue
    .line 1052177
    invoke-direct {p0}, LX/65f;-><init>()V

    .line 1052178
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052179
    :cond_0
    iput-object p1, p0, LX/675;->a:LX/65f;

    .line 1052180
    return-void
.end method


# virtual methods
.method public final a(J)LX/65f;
    .locals 1

    .prologue
    .line 1052176
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0, p1, p2}, LX/65f;->a(J)LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)LX/65f;
    .locals 1

    .prologue
    .line 1052175
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0, p1, p2, p3}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/65f;)LX/675;
    .locals 2

    .prologue
    .line 1052172
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052173
    :cond_0
    iput-object p1, p0, LX/675;->a:LX/65f;

    .line 1052174
    return-object p0
.end method

.method public final bJ_()J
    .locals 2

    .prologue
    .line 1052171
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->bJ_()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bK_()Z
    .locals 1

    .prologue
    .line 1052170
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->bK_()Z

    move-result v0

    return v0
.end method

.method public final bL_()LX/65f;
    .locals 1

    .prologue
    .line 1052165
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->bL_()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1052169
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()LX/65f;
    .locals 1

    .prologue
    .line 1052168
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->f()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1052166
    iget-object v0, p0, LX/675;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->g()V

    .line 1052167
    return-void
.end method
