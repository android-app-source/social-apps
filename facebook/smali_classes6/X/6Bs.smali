.class public LX/6Bs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/0TD;

.field private final b:LX/6Bo;


# direct methods
.method public constructor <init>(LX/0TD;LX/6Bo;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063228
    iput-object p1, p0, LX/6Bs;->a:LX/0TD;

    .line 1063229
    iput-object p2, p0, LX/6Bs;->b:LX/6Bo;

    .line 1063230
    return-void
.end method


# virtual methods
.method public final a(LX/6Bp;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Bp;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6Bq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1063231
    iget-object v0, p0, LX/6Bs;->a:LX/0TD;

    new-instance v1, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;

    iget-object v2, p0, LX/6Bs;->b:LX/6Bo;

    invoke-direct {v1, p1, v2}, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;-><init>(LX/6Bp;LX/6Bo;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
