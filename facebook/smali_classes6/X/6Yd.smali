.class public LX/6Yd;
.super LX/6Ya;
.source ""


# instance fields
.field private final c:LX/7Y8;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/4g9;

.field public final f:LX/7Y7;

.field public final g:LX/7YP;


# direct methods
.method public constructor <init>(LX/7Y8;LX/0Or;LX/4g9;LX/7Y7;LX/7YP;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroBroadcastManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroAnalyticsLogger;",
            "Lcom/facebook/iorg/common/zero/interfaces/IorgAndroidThreadUtil;",
            "Lcom/facebook/iorg/common/upsell/server/UpsellApiRequestManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110316
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110317
    iput-object p1, p0, LX/6Yd;->c:LX/7Y8;

    .line 1110318
    iput-object p2, p0, LX/6Yd;->d:LX/0Or;

    .line 1110319
    iput-object p3, p0, LX/6Yd;->e:LX/4g9;

    .line 1110320
    iput-object p4, p0, LX/6Yd;->f:LX/7Y7;

    .line 1110321
    iput-object p5, p0, LX/6Yd;->g:LX/7YP;

    .line 1110322
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 10

    .prologue
    .line 1110296
    iget-object v0, p0, LX/6Yd;->e:LX/4g9;

    sget-object v1, LX/4g6;->j:LX/4g6;

    invoke-virtual {p0}, LX/6Ya;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110297
    iget-object v0, p0, LX/6Yd;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110298
    iget-object v0, p0, LX/6Yd;->c:LX/7Y8;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->UPSELL:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Y8;->a(Landroid/content/Intent;)V

    .line 1110299
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110300
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    move-object v0, v1

    .line 1110301
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    move-object v0, v1

    .line 1110302
    invoke-virtual {p0}, LX/6Ya;->f()LX/6Y5;

    move-result-object v1

    .line 1110303
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1110304
    invoke-virtual {v1, v2}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v1

    .line 1110305
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1110306
    iput-object v2, v1, LX/6Y5;->c:Ljava/lang/String;

    .line 1110307
    move-object v1, v1

    .line 1110308
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1110309
    new-instance v4, LX/6Yc;

    iget-object v6, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    iget-object v7, p0, LX/6Yd;->f:LX/7Y7;

    iget-object v8, p0, LX/6Yd;->e:LX/4g9;

    iget-object v9, p0, LX/6Yd;->g:LX/7YP;

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, LX/6Yc;-><init>(LX/6Yd;Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/7Y7;LX/4g9;LX/7YP;)V

    move-object v3, v4

    .line 1110310
    invoke-virtual {v1, v2, v3}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v1

    .line 1110311
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1110312
    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    .line 1110313
    new-instance v1, LX/6YP;

    invoke-direct {v1, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110314
    invoke-virtual {v1, v0}, LX/6YP;->a(LX/6Y5;)V

    .line 1110315
    return-object v1
.end method
