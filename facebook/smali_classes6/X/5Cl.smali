.class public final LX/5Cl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 869348
    const/4 v12, 0x0

    .line 869349
    const/4 v11, 0x0

    .line 869350
    const/4 v10, 0x0

    .line 869351
    const/4 v9, 0x0

    .line 869352
    const/4 v8, 0x0

    .line 869353
    const/4 v7, 0x0

    .line 869354
    const/4 v6, 0x0

    .line 869355
    const/4 v5, 0x0

    .line 869356
    const/4 v4, 0x0

    .line 869357
    const/4 v3, 0x0

    .line 869358
    const/4 v2, 0x0

    .line 869359
    const/4 v1, 0x0

    .line 869360
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 869361
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 869362
    const/4 v1, 0x0

    .line 869363
    :goto_0
    return v1

    .line 869364
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 869365
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 869366
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 869367
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 869368
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 869369
    const-string v14, "__type__"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "__typename"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 869370
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    goto :goto_1

    .line 869371
    :cond_3
    const-string v14, "friendship_status"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 869372
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    goto :goto_1

    .line 869373
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 869374
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 869375
    :cond_5
    const-string v14, "invite_status_in_feedback"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 869376
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 869377
    :cond_6
    const-string v14, "is_banned_by_page_viewer"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 869378
    const/4 v2, 0x1

    .line 869379
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 869380
    :cond_7
    const-string v14, "mutual_friends"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 869381
    invoke-static/range {p0 .. p1}, LX/5Cn;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 869382
    :cond_8
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 869383
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 869384
    :cond_9
    const-string v14, "profile_picture"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 869385
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 869386
    :cond_a
    const-string v14, "unread_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 869387
    const/4 v1, 0x1

    .line 869388
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v4

    goto/16 :goto_1

    .line 869389
    :cond_b
    const-string v14, "unseen_stories"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 869390
    invoke-static/range {p0 .. p1}, LX/5Co;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 869391
    :cond_c
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 869392
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 869393
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 869394
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 869395
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 869396
    if-eqz v2, :cond_d

    .line 869397
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 869398
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 869399
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 869400
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 869401
    if-eqz v1, :cond_e

    .line 869402
    const/16 v1, 0x8

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4, v2}, LX/186;->a(III)V

    .line 869403
    :cond_e
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 869404
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 869405
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 869406
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 869407
    if-eqz v0, :cond_0

    .line 869408
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869409
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 869410
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 869411
    if-eqz v0, :cond_1

    .line 869412
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869413
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 869414
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 869415
    if-eqz v0, :cond_2

    .line 869416
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869417
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 869418
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 869419
    if-eqz v0, :cond_3

    .line 869420
    const-string v0, "invite_status_in_feedback"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869421
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 869422
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 869423
    if-eqz v0, :cond_4

    .line 869424
    const-string v1, "is_banned_by_page_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869425
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 869426
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 869427
    if-eqz v0, :cond_5

    .line 869428
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869429
    invoke-static {p0, v0, p2}, LX/5Cn;->a(LX/15i;ILX/0nX;)V

    .line 869430
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 869431
    if-eqz v0, :cond_6

    .line 869432
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869433
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 869434
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 869435
    if-eqz v0, :cond_7

    .line 869436
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869437
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 869438
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 869439
    if-eqz v0, :cond_8

    .line 869440
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869441
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 869442
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 869443
    if-eqz v0, :cond_9

    .line 869444
    const-string v1, "unseen_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869445
    invoke-static {p0, v0, p2}, LX/5Co;->a(LX/15i;ILX/0nX;)V

    .line 869446
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 869447
    return-void
.end method
