.class public LX/5Kj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Ki;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/componentscript/components/CSImageSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899029
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Kj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/componentscript/components/CSImageSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 899030
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 899031
    iput-object p1, p0, LX/5Kj;->b:LX/0Ot;

    .line 899032
    return-void
.end method

.method public static a(LX/0QB;)LX/5Kj;
    .locals 4

    .prologue
    .line 899033
    const-class v1, LX/5Kj;

    monitor-enter v1

    .line 899034
    :try_start_0
    sget-object v0, LX/5Kj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 899035
    sput-object v2, LX/5Kj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 899036
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899037
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 899038
    new-instance v3, LX/5Kj;

    const/16 p0, 0x194f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/5Kj;-><init>(LX/0Ot;)V

    .line 899039
    move-object v0, v3

    .line 899040
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 899041
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/5Kj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 899042
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 899043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 899044
    check-cast p2, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;

    .line 899045
    iget-object v0, p0, LX/5Kj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/components/CSImageSpec;

    iget-object v1, p2, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    iget-object v2, p2, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 899046
    const-string v3, "size"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    const-class v4, Ljava/lang/Double;

    invoke-virtual {v3, v4}, Lcom/facebook/java2js/JSValue;->asMap(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object p0

    .line 899047
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/componentscript/components/CSImageSpec;->b:LX/1Ad;

    const-string p2, "image"

    invoke-virtual {v1, p2}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, -0x777778

    invoke-interface {v3, v4}, LX/1Di;->y(I)LX/1Di;

    move-result-object v4

    .line 899048
    if-eqz p0, :cond_0

    .line 899049
    const-string v3, "width"

    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v3

    invoke-interface {v4, v3}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    const-string v3, "height"

    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v3

    invoke-interface {v4, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    .line 899050
    :goto_0
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 899051
    return-object v0

    :cond_0
    move-object v3, v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 899052
    invoke-static {}, LX/1dS;->b()V

    .line 899053
    const/4 v0, 0x0

    return-object v0
.end method
