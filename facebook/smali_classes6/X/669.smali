.class public final LX/669;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64r;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1049486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049487
    iput-boolean p1, p0, LX/669;->a:Z

    .line 1049488
    return-void
.end method


# virtual methods
.method public final a(LX/66O;)LX/655;
    .locals 9

    .prologue
    .line 1049489
    move-object v0, p1

    check-cast v0, LX/66O;

    .line 1049490
    iget-object v1, v0, LX/66O;->c:LX/66G;

    move-object v1, v1

    .line 1049491
    move-object v0, p1

    .line 1049492
    check-cast v0, LX/66O;

    .line 1049493
    iget-object v2, v0, LX/66O;->b:LX/65W;

    move-object v2, v2

    .line 1049494
    iget-object v0, p1, LX/66O;->f:LX/650;

    move-object v0, v0

    .line 1049495
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1049496
    invoke-interface {v1, v0}, LX/66G;->a(LX/650;)V

    .line 1049497
    iget-object v3, v0, LX/650;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1049498
    invoke-static {v3}, LX/66N;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1049499
    iget-object v3, v0, LX/650;->d:LX/651;

    move-object v3, v3

    .line 1049500
    if-eqz v3, :cond_0

    .line 1049501
    iget-object v3, v0, LX/650;->d:LX/651;

    move-object v3, v3

    .line 1049502
    invoke-virtual {v3}, LX/651;->b()J

    move-result-wide v6

    invoke-interface {v1, v0, v6, v7}, LX/66G;->a(LX/650;J)LX/65J;

    move-result-object v3

    .line 1049503
    invoke-static {v3}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v3

    .line 1049504
    iget-object v6, v0, LX/650;->d:LX/651;

    move-object v6, v6

    .line 1049505
    invoke-virtual {v6, v3}, LX/651;->a(LX/670;)V

    .line 1049506
    invoke-interface {v3}, LX/65J;->close()V

    .line 1049507
    :cond_0
    invoke-interface {v1}, LX/66G;->c()V

    .line 1049508
    invoke-interface {v1}, LX/66G;->b()LX/654;

    move-result-object v3

    .line 1049509
    iput-object v0, v3, LX/654;->a:LX/650;

    .line 1049510
    move-object v0, v3

    .line 1049511
    invoke-virtual {v2}, LX/65W;->b()LX/65S;

    move-result-object v3

    .line 1049512
    iget-object v6, v3, LX/65S;->m:LX/64l;

    move-object v3, v6

    .line 1049513
    iput-object v3, v0, LX/654;->e:LX/64l;

    .line 1049514
    move-object v0, v0

    .line 1049515
    iput-wide v4, v0, LX/654;->k:J

    .line 1049516
    move-object v0, v0

    .line 1049517
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1049518
    iput-wide v4, v0, LX/654;->l:J

    .line 1049519
    move-object v0, v0

    .line 1049520
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    .line 1049521
    iget-boolean v3, p0, LX/669;->a:Z

    if-eqz v3, :cond_1

    .line 1049522
    iget v3, v0, LX/655;->c:I

    move v3, v3

    .line 1049523
    const/16 v4, 0x65

    if-eq v3, v4, :cond_2

    .line 1049524
    :cond_1
    invoke-virtual {v0}, LX/655;->newBuilder()LX/654;

    move-result-object v3

    .line 1049525
    invoke-interface {v1, v0}, LX/66G;->a(LX/655;)LX/656;

    move-result-object v0

    .line 1049526
    iput-object v0, v3, LX/654;->g:LX/656;

    .line 1049527
    move-object v0, v3

    .line 1049528
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    .line 1049529
    :cond_2
    const-string v1, "close"

    .line 1049530
    iget-object v3, v0, LX/655;->a:LX/650;

    move-object v3, v3

    .line 1049531
    const-string v4, "Connection"

    invoke-virtual {v3, v4}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "close"

    const-string v3, "Connection"

    .line 1049532
    invoke-virtual {v0, v3}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1049533
    :cond_3
    invoke-virtual {v2}, LX/65W;->d()V

    .line 1049534
    :cond_4
    iget v1, v0, LX/655;->c:I

    move v1, v1

    .line 1049535
    const/16 v2, 0xcc

    if-eq v1, v2, :cond_5

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_6

    .line 1049536
    :cond_5
    iget-object v2, v0, LX/655;->g:LX/656;

    move-object v2, v2

    .line 1049537
    invoke-virtual {v2}, LX/656;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    .line 1049538
    new-instance v2, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HTTP "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " had non-zero Content-Length: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1049539
    iget-object v3, v0, LX/655;->g:LX/656;

    move-object v0, v3

    .line 1049540
    invoke-virtual {v0}, LX/656;->b()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1049541
    :cond_6
    return-object v0
.end method
