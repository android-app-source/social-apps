.class public final LX/5tX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1016229
    const/4 v7, 0x0

    .line 1016230
    const/4 v6, 0x0

    .line 1016231
    const-wide/16 v4, 0x0

    .line 1016232
    const/4 v3, 0x0

    .line 1016233
    const/4 v2, 0x0

    .line 1016234
    const/4 v1, 0x0

    .line 1016235
    const/4 v0, 0x0

    .line 1016236
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 1016237
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1016238
    const/4 v0, 0x0

    .line 1016239
    :goto_0
    return v0

    .line 1016240
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v9, :cond_8

    .line 1016241
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1016242
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1016243
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v1, :cond_0

    .line 1016244
    const-string v9, "actors"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1016245
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1016246
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v9, :cond_1

    .line 1016247
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v9, :cond_1

    .line 1016248
    invoke-static {p0, p1}, LX/5tV;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1016249
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1016250
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1016251
    move v5, v1

    goto :goto_1

    .line 1016252
    :cond_2
    const-string v9, "cache_id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1016253
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 1016254
    :cond_3
    const-string v9, "creation_time"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1016255
    const/4 v0, 0x1

    .line 1016256
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1016257
    :cond_4
    const-string v9, "feedback"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1016258
    invoke-static {p0, p1}, LX/5tW;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1016259
    :cond_5
    const-string v9, "id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1016260
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 1016261
    :cond_6
    const-string v9, "message"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1016262
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1016263
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1016264
    :cond_8
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1016265
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1016266
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1016267
    if-eqz v0, :cond_9

    .line 1016268
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1016269
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1016270
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1016271
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1016272
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v8, v3

    move v11, v2

    move-wide v2, v4

    move v4, v6

    move v5, v7

    move v6, v1

    move v7, v11

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1016273
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1016274
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1016275
    if-eqz v0, :cond_1

    .line 1016276
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016277
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1016278
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1016279
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/5tV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1016280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1016281
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1016282
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1016283
    if-eqz v0, :cond_2

    .line 1016284
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016285
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1016286
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1016287
    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 1016288
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016289
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1016290
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1016291
    if-eqz v0, :cond_4

    .line 1016292
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016293
    invoke-static {p0, v0, p2}, LX/5tW;->a(LX/15i;ILX/0nX;)V

    .line 1016294
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1016295
    if-eqz v0, :cond_5

    .line 1016296
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016297
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1016298
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1016299
    if-eqz v0, :cond_6

    .line 1016300
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016301
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1016302
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1016303
    return-void
.end method
