.class public LX/6Ln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/3fr;

.field public final b:LX/0tX;

.field public final c:LX/3fb;

.field public final d:LX/6Lo;

.field public final e:LX/03V;

.field private final f:LX/0ka;

.field public final g:LX/3fc;

.field private final h:LX/2RQ;

.field public final i:I

.field private final j:Z


# direct methods
.method public constructor <init>(LX/3fr;LX/0tX;LX/3fb;LX/6Lo;LX/0W3;LX/03V;LX/0ka;LX/3fc;LX/2RQ;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1078900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078901
    iput-object p1, p0, LX/6Ln;->a:LX/3fr;

    .line 1078902
    iput-object p2, p0, LX/6Ln;->b:LX/0tX;

    .line 1078903
    iput-object p3, p0, LX/6Ln;->c:LX/3fb;

    .line 1078904
    iput-object p4, p0, LX/6Ln;->d:LX/6Lo;

    .line 1078905
    iput-object p6, p0, LX/6Ln;->e:LX/03V;

    .line 1078906
    iput-object p7, p0, LX/6Ln;->f:LX/0ka;

    .line 1078907
    iput-object p8, p0, LX/6Ln;->g:LX/3fc;

    .line 1078908
    iput-object p9, p0, LX/6Ln;->h:LX/2RQ;

    .line 1078909
    sget-wide v0, LX/0X5;->fE:J

    const/4 v2, 0x5

    invoke-interface {p5, v0, v1, v2}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/6Ln;->i:I

    .line 1078910
    sget-wide v0, LX/0X5;->fF:J

    const/4 v2, 0x0

    invoke-interface {p5, v0, v1, v2}, LX/0W4;->a(JZ)Z

    move-result v0

    iput-boolean v0, p0, LX/6Ln;->j:Z

    .line 1078911
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 13

    .prologue
    const/4 v3, 0x1

    .line 1078833
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1078834
    :cond_0
    :goto_0
    return v3

    .line 1078835
    :cond_1
    iget-boolean v0, p0, LX/6Ln;->j:Z

    if-eqz v0, :cond_0

    .line 1078836
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1078837
    iget-object v1, p0, LX/6Ln;->a:LX/3fr;

    iget-object v2, p0, LX/6Ln;->h:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 1078838
    :goto_1
    :try_start_0
    invoke-interface {v1}, LX/6N1;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1078839
    invoke-interface {v1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1078840
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/6N1;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, LX/6N1;->close()V

    .line 1078841
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1078842
    const/4 v4, 0x0

    .line 1078843
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 1078844
    iget v1, p0, LX/6Ln;->i:I

    if-le v1, v7, :cond_3

    move v2, v7

    .line 1078845
    :goto_2
    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    .line 1078846
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    move v5, v4

    .line 1078847
    :goto_3
    if-ge v5, v2, :cond_4

    .line 1078848
    sub-int v1, v7, v5

    invoke-virtual {v6, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 1078849
    sub-int v9, v7, v5

    add-int/lit8 v9, v9, -0x1

    invoke-static {v0, v1, v9}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1078850
    sub-int v1, v7, v5

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1078851
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    .line 1078852
    :cond_3
    iget v2, p0, LX/6Ln;->i:I

    goto :goto_2

    .line 1078853
    :cond_4
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 1078854
    :try_start_1
    new-instance v1, LX/0v6;

    const-string v5, "ContactComparison"

    invoke-direct {v1, v5}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1078855
    new-instance v5, LX/6MJ;

    invoke-direct {v5}, LX/6MJ;-><init>()V

    move-object v5, v5

    .line 1078856
    iget-object v6, p0, LX/6Ln;->g:LX/3fc;

    invoke-virtual {v6, v5}, LX/3fc;->b(LX/0gW;)V

    .line 1078857
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 1078858
    invoke-static {}, LX/3gr;->c()LX/6MB;

    move-result-object v5

    .line 1078859
    const-string v6, "contact_ids"

    invoke-virtual {v5, v6, v9}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1078860
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    move-object v5, v5

    .line 1078861
    invoke-virtual {v1, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1078862
    iget-object v6, p0, LX/6Ln;->b:LX/0tX;

    invoke-virtual {v6, v1}, LX/0tX;->a(LX/0v6;)V

    .line 1078863
    const v1, 0x61a81767

    invoke-static {v5, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v1

    .line 1078864
    new-instance v5, LX/0P2;

    invoke-direct {v5}, LX/0P2;-><init>()V

    .line 1078865
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 1078866
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->d()Ljava/lang/String;

    move-result-object v8

    .line 1078867
    if-eqz v8, :cond_5

    .line 1078868
    iget-object v11, p0, LX/6Ln;->c:LX/3fb;

    invoke-virtual {v11, v1}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v1

    invoke-virtual {v1}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v1

    invoke-virtual {v5, v8, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 1078869
    :catch_0
    move-exception v1

    .line 1078870
    iget-object v2, p0, LX/6Ln;->e:LX/03V;

    const-string v4, "ContactsReliabilityCheckConditionalWorker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to fetch following contacts from server : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    .line 1078871
    iput-object v1, v4, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1078872
    move-object v1, v4

    .line 1078873
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/03V;->a(LX/0VG;)V

    .line 1078874
    :goto_5
    goto/16 :goto_0

    .line 1078875
    :cond_6
    :try_start_2
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v11

    move v8, v4

    move v5, v4

    .line 1078876
    :goto_6
    if-ge v8, v2, :cond_9

    .line 1078877
    sub-int v1, v7, v8

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/Contact;

    .line 1078878
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/contacts/graphql/Contact;

    .line 1078879
    if-nez v6, :cond_8

    .line 1078880
    add-int/lit8 v5, v5, 0x1

    .line 1078881
    :cond_7
    :goto_7
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_6

    .line 1078882
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object p1

    invoke-static {v12, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_b

    const/4 v12, 0x1

    :goto_8
    move v1, v12

    .line 1078883
    if-eqz v1, :cond_7

    .line 1078884
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 1078885
    :cond_9
    iget-object v1, p0, LX/6Ln;->d:LX/6Lo;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    const v7, 0x10c94397

    invoke-static {v10, v7}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1078886
    iget-object v8, v7, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v7, v8

    .line 1078887
    check-cast v7, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel;

    invoke-virtual {v7}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    move-object v8, v0

    .line 1078888
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v11, "contacts_reliability_difference"

    invoke-direct {v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v11, "contacts_reliability"

    .line 1078889
    iput-object v11, v10, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1078890
    move-object v10, v10

    .line 1078891
    const-string v11, "num_checked"

    invoke-virtual {v10, v11, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "num_differences"

    invoke-virtual {v10, v11, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "num_missing"

    invoke-virtual {v10, v11, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "num_db_contacts"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "num_server_contacts"

    invoke-virtual {v10, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v11

    const-string v12, "omnistore_contacts_enabled"

    iget-object v10, v1, LX/6Lo;->b:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    sget-object p1, LX/2Jp;->OMNISTORE_CONTACTS_COLLECTION:LX/2Jp;

    if-ne v10, p1, :cond_c

    const/4 v10, 0x1

    :goto_9
    invoke-virtual {v11, v12, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "db_contacts"

    .line 1078892
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1078893
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/contacts/graphql/Contact;

    .line 1078894
    invoke-virtual {v12}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v4, 0x2c

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 1078895
    :cond_a
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object v12, v12

    .line 1078896
    invoke-virtual {v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    .line 1078897
    iget-object v11, v1, LX/6Lo;->a:LX/0Zb;

    invoke-interface {v11, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1078898
    goto/16 :goto_5

    :cond_b
    :try_start_3
    const/4 v12, 0x0

    goto/16 :goto_8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 1078899
    :cond_c
    const/4 v10, 0x0

    goto :goto_9
.end method
