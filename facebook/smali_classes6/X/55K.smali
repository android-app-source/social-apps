.class public final enum LX/55K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/55K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/55K;

.field public static final enum ARTICLE_CHAINING:LX/55K;

.field public static final enum GROUP_RELATED_STORIES:LX/55K;

.field public static final enum INSTAGRAM_PHOTO_CHAINING:LX/55K;

.field public static final enum JOBSEARCH_PIVOT:LX/55K;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS:LX/55K;

.field public static final enum PEOPLE_YOU_MAY_KNOW:LX/55K;

.field public static final enum PHOTO_CHAINING:LX/55K;

.field public static final enum POLITICAL_ISSUE_PIVOT:LX/55K;

.field public static final enum RELATED_EVENTS_CHAINING:LX/55K;

.field public static final enum TRENDING_STORIES:LX/55K;

.field public static final enum UNSEEN_STORIES_CHAINING:LX/55K;

.field public static final enum VIDEO_CHAINING:LX/55K;

.field private static final defaultList:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 829159
    new-instance v0, LX/55K;

    const-string v1, "ARTICLE_CHAINING"

    invoke-direct {v0, v1, v3}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->ARTICLE_CHAINING:LX/55K;

    .line 829160
    new-instance v0, LX/55K;

    const-string v1, "INSTAGRAM_PHOTO_CHAINING"

    invoke-direct {v0, v1, v4}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->INSTAGRAM_PHOTO_CHAINING:LX/55K;

    .line 829161
    new-instance v0, LX/55K;

    const-string v1, "PEOPLE_YOU_MAY_KNOW"

    invoke-direct {v0, v1, v5}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->PEOPLE_YOU_MAY_KNOW:LX/55K;

    .line 829162
    new-instance v0, LX/55K;

    const-string v1, "PHOTO_CHAINING"

    invoke-direct {v0, v1, v6}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->PHOTO_CHAINING:LX/55K;

    .line 829163
    new-instance v0, LX/55K;

    const-string v1, "TRENDING_STORIES"

    invoke-direct {v0, v1, v7}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->TRENDING_STORIES:LX/55K;

    .line 829164
    new-instance v0, LX/55K;

    const-string v1, "VIDEO_CHAINING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->VIDEO_CHAINING:LX/55K;

    .line 829165
    new-instance v0, LX/55K;

    const-string v1, "RELATED_EVENTS_CHAINING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->RELATED_EVENTS_CHAINING:LX/55K;

    .line 829166
    new-instance v0, LX/55K;

    const-string v1, "UNSEEN_STORIES_CHAINING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->UNSEEN_STORIES_CHAINING:LX/55K;

    .line 829167
    new-instance v0, LX/55K;

    const-string v1, "GROUP_RELATED_STORIES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->GROUP_RELATED_STORIES:LX/55K;

    .line 829168
    new-instance v0, LX/55K;

    const-string v1, "POLITICAL_ISSUE_PIVOT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->POLITICAL_ISSUE_PIVOT:LX/55K;

    .line 829169
    new-instance v0, LX/55K;

    const-string v1, "JOBSEARCH_PIVOT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->JOBSEARCH_PIVOT:LX/55K;

    .line 829170
    new-instance v0, LX/55K;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/55K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55K;->PAGE_CONTEXTUAL_RECOMMENDATIONS:LX/55K;

    .line 829171
    const/16 v0, 0xc

    new-array v0, v0, [LX/55K;

    sget-object v1, LX/55K;->ARTICLE_CHAINING:LX/55K;

    aput-object v1, v0, v3

    sget-object v1, LX/55K;->INSTAGRAM_PHOTO_CHAINING:LX/55K;

    aput-object v1, v0, v4

    sget-object v1, LX/55K;->PEOPLE_YOU_MAY_KNOW:LX/55K;

    aput-object v1, v0, v5

    sget-object v1, LX/55K;->PHOTO_CHAINING:LX/55K;

    aput-object v1, v0, v6

    sget-object v1, LX/55K;->TRENDING_STORIES:LX/55K;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/55K;->VIDEO_CHAINING:LX/55K;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/55K;->RELATED_EVENTS_CHAINING:LX/55K;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/55K;->UNSEEN_STORIES_CHAINING:LX/55K;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/55K;->GROUP_RELATED_STORIES:LX/55K;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/55K;->POLITICAL_ISSUE_PIVOT:LX/55K;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/55K;->JOBSEARCH_PIVOT:LX/55K;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/55K;->PAGE_CONTEXTUAL_RECOMMENDATIONS:LX/55K;

    aput-object v2, v0, v1

    sput-object v0, LX/55K;->$VALUES:[LX/55K;

    .line 829172
    invoke-static {}, LX/55K;->values()[LX/55K;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/55K;->defaultList:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 829158
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getAllSupportedUnitTypes()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 829173
    sget-object v0, LX/55K;->defaultList:LX/0Px;

    return-object v0
.end method

.method public static getSupportedUnitTypes(Ljava/util/List;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/55K;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 829149
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 829150
    :cond_0
    sget-object v0, LX/55K;->defaultList:LX/0Px;

    .line 829151
    :goto_0
    return-object v0

    .line 829152
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 829153
    sget-object v0, LX/55K;->defaultList:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    sget-object v0, LX/55K;->defaultList:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/55K;

    .line 829154
    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 829155
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 829156
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 829157
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/55K;
    .locals 1

    .prologue
    .line 829147
    const-class v0, LX/55K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/55K;

    return-object v0
.end method

.method public static values()[LX/55K;
    .locals 1

    .prologue
    .line 829148
    sget-object v0, LX/55K;->$VALUES:[LX/55K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/55K;

    return-object v0
.end method
