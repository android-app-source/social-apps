.class public LX/6Ha;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6HZ;


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field public a:LX/6HU;

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Landroid/graphics/Matrix;

.field public i:Landroid/view/View;

.field public j:Lcom/facebook/camera/views/FocusIndicatorView;

.field public k:Landroid/view/View;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:[Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Landroid/os/Handler;

.field public r:Z

.field public s:I

.field private t:LX/6HF;

.field public u:Landroid/hardware/Camera;

.field public v:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/6Hi;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field public x:I

.field public y:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072451
    const-class v0, LX/6Ha;

    sput-object v0, LX/6Ha;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;LX/6HF;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1072440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072441
    iput v2, p0, LX/6Ha;->c:I

    .line 1072442
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Ha;->h:Landroid/graphics/Matrix;

    .line 1072443
    iput v2, p0, LX/6Ha;->w:I

    .line 1072444
    iput v2, p0, LX/6Ha;->x:I

    .line 1072445
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ha;->z:J

    .line 1072446
    iput v2, p0, LX/6Ha;->A:I

    .line 1072447
    iput-object p1, p0, LX/6Ha;->o:[Ljava/lang/String;

    .line 1072448
    new-instance v0, LX/6HY;

    invoke-direct {v0, p0}, LX/6HY;-><init>(LX/6Ha;)V

    iput-object v0, p0, LX/6Ha;->q:Landroid/os/Handler;

    .line 1072449
    iput-object p2, p0, LX/6Ha;->t:LX/6HF;

    .line 1072450
    return-void
.end method

.method private static a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 9

    .prologue
    const/16 v4, 0x3e8

    const/16 v8, -0x3e8

    const-wide v6, 0x3ff3333333333333L    # 1.2

    .line 1072435
    iget v0, p0, Landroid/graphics/Rect;->left:I

    int-to-double v0, v0

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1072436
    iget v1, p0, Landroid/graphics/Rect;->top:I

    int-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1072437
    iget v2, p0, Landroid/graphics/Rect;->right:I

    int-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1072438
    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    int-to-double v4, v3

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1072439
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method private a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1072423
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1072424
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1072425
    iget-object v0, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1072426
    iget-object v0, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 1072427
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1072428
    div-int/lit8 v5, v1, 0x2

    sub-int v5, p1, v5

    sub-int v1, v3, v1

    invoke-static {v5, v6, v1}, LX/6IF;->a(III)I

    move-result v1

    .line 1072429
    div-int/lit8 v3, v2, 0x2

    sub-int v3, p2, v3

    sub-int v2, v4, v2

    invoke-static {v3, v6, v2}, LX/6IF;->a(III)I

    move-result v2

    .line 1072430
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1072431
    invoke-virtual {v0}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v0

    .line 1072432
    const/16 v1, 0xd

    aput v6, v0, v1

    .line 1072433
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1072434
    return-void
.end method

.method private a(IIFIIIILandroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1072402
    int-to-float v0, p1

    mul-float/2addr v0, p3

    float-to-int v0, v0

    .line 1072403
    int-to-float v1, p2

    mul-float/2addr v1, p3

    float-to-int v1, v1

    .line 1072404
    div-int/lit8 v2, v0, 0x2

    sub-int v2, p4, v2

    sub-int v3, p6, v0

    invoke-static {v2, v5, v3}, LX/6IF;->a(III)I

    move-result v2

    .line 1072405
    div-int/lit8 v3, v1, 0x2

    sub-int v3, p5, v3

    sub-int v4, p7, v1

    invoke-static {v3, v5, v4}, LX/6IF;->a(III)I

    move-result v3

    .line 1072406
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v2

    int-to-float v6, v3

    add-int/2addr v0, v2

    int-to-float v0, v0

    add-int/2addr v1, v3

    int-to-float v1, v1

    invoke-direct {v4, v5, v6, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1072407
    iget-object v0, p0, LX/6Ha;->h:Landroid/graphics/Matrix;

    if-nez v0, :cond_0

    .line 1072408
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/6Ha;->h:Landroid/graphics/Matrix;

    .line 1072409
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1072410
    iget-boolean v1, p0, LX/6Ha;->r:Z

    iget v2, p0, LX/6Ha;->s:I

    iget-object v3, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v5, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    const/high16 p3, 0x44fa0000    # 2000.0f

    const/high16 p2, 0x40000000    # 2.0f

    const/high16 p1, 0x3f800000    # 1.0f

    .line 1072411
    if-eqz v1, :cond_1

    const/high16 v6, -0x40800000    # -1.0f

    :goto_0
    invoke-virtual {v0, v6, p1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1072412
    int-to-float v6, v2

    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1072413
    int-to-float v6, v3

    div-float/2addr v6, p3

    int-to-float p1, v5

    div-float/2addr p1, p3

    invoke-virtual {v0, v6, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1072414
    int-to-float v6, v3

    div-float/2addr v6, p2

    int-to-float p1, v5

    div-float/2addr p1, p2

    invoke-virtual {v0, v6, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1072415
    iget-object v1, p0, LX/6Ha;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1072416
    :cond_0
    iget-object v0, p0, LX/6Ha;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1072417
    iget v0, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->left:I

    .line 1072418
    iget v0, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->top:I

    .line 1072419
    iget v0, v4, Landroid/graphics/RectF;->right:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->right:I

    .line 1072420
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->bottom:I

    .line 1072421
    return-void

    :cond_1
    move v6, p1

    .line 1072422
    goto :goto_0
.end method

.method public static a(LX/6Ha;I)V
    .locals 4

    .prologue
    .line 1072397
    invoke-static {p0}, LX/6Ha;->l(LX/6Ha;)V

    .line 1072398
    invoke-direct {p0}, LX/6Ha;->m()V

    .line 1072399
    iget-object v0, p0, LX/6Ha;->q:Landroid/os/Handler;

    const/4 v1, 0x0

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1072400
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/6Ha;->z:J

    .line 1072401
    return-void
.end method

.method public static a(LX/6Ha;LX/6HX;I)V
    .locals 1

    .prologue
    .line 1072391
    iget-object v0, p0, LX/6Ha;->a:LX/6HU;

    invoke-virtual {v0, p1}, LX/6HU;->a(LX/6HX;)V

    .line 1072392
    iput p2, p0, LX/6Ha;->A:I

    .line 1072393
    const/4 v0, 0x1

    iput v0, p0, LX/6Ha;->c:I

    .line 1072394
    invoke-static {p0}, LX/6Ha;->l(LX/6Ha;)V

    .line 1072395
    invoke-direct {p0}, LX/6Ha;->m()V

    .line 1072396
    return-void
.end method

.method public static a(Landroid/hardware/Camera$Parameters;)Z
    .locals 1

    .prologue
    .line 1072390
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1072225
    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1072382
    :try_start_0
    iget-object v1, p0, LX/6Ha;->u:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1072383
    invoke-virtual {v1, p1}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1072384
    invoke-virtual {v1, p2}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    .line 1072385
    iget-object v2, p0, LX/6Ha;->u:Landroid/hardware/Camera;

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072386
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1072387
    :catch_0
    move-exception v1

    .line 1072388
    iget-object v2, p0, LX/6Ha;->t:LX/6HF;

    const-string v3, "setFocusAndMeteringAreas/setParameters failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1072389
    iput-boolean v0, p0, LX/6Ha;->f:Z

    goto :goto_0
.end method

.method public static i(LX/6Ha;)V
    .locals 4

    .prologue
    .line 1072371
    invoke-virtual {p0}, LX/6Ha;->g()V

    .line 1072372
    iget-object v0, p0, LX/6Ha;->a:LX/6HU;

    .line 1072373
    iget-object v1, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1072374
    iget-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    .line 1072375
    :try_start_0
    iget-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->cancelAutoFocus()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072376
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/6Ha;->c:I

    .line 1072377
    invoke-static {p0}, LX/6Ha;->l(LX/6Ha;)V

    .line 1072378
    invoke-direct {p0}, LX/6Ha;->m()V

    .line 1072379
    return-void

    .line 1072380
    :catch_0
    move-exception v1

    .line 1072381
    iget-object v2, v0, LX/6HU;->B:LX/6HF;

    const-string v3, "cancelAutoFocus failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static j(LX/6Ha;)V
    .locals 5

    .prologue
    .line 1072355
    iget-object v0, p0, LX/6Ha;->j:Lcom/facebook/camera/views/FocusIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/camera/views/FocusIndicatorView;->d()V

    .line 1072356
    iget-object v0, p0, LX/6Ha;->a:LX/6HU;

    .line 1072357
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/6HU;->s:Z

    .line 1072358
    iget-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1072359
    invoke-static {v0}, LX/6HU;->D(LX/6HU;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 1072360
    :try_start_0
    iget-object v2, v0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072361
    :goto_0
    :try_start_1
    iget-object v1, v0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v2, v0, LX/6HU;->V:Landroid/hardware/Camera$ShutterCallback;

    const/4 v3, 0x0

    iget-object v4, v0, LX/6HU;->W:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1072362
    :goto_1
    const/4 v1, 0x1

    move v0, v1

    .line 1072363
    if-eqz v0, :cond_0

    .line 1072364
    invoke-direct {p0}, LX/6Ha;->m()V

    .line 1072365
    :goto_2
    return-void

    .line 1072366
    :cond_0
    sget-object v0, LX/6Ha;->b:Ljava/lang/Class;

    const-string v1, "FocusManager.capture rejected"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_2

    .line 1072367
    :catch_0
    move-exception v1

    .line 1072368
    iget-object v2, v0, LX/6HU;->B:LX/6HF;

    const-string v3, "capture/setParameters failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1072369
    :catch_1
    move-exception v1

    .line 1072370
    iget-object v2, v0, LX/6HU;->B:LX/6HF;

    const-string v3, "capture/takePicture failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public static l(LX/6Ha;)V
    .locals 3

    .prologue
    .line 1072336
    iget-boolean v0, p0, LX/6Ha;->d:Z

    if-nez v0, :cond_1

    .line 1072337
    :cond_0
    :goto_0
    return-void

    .line 1072338
    :cond_1
    iget-object v0, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    .line 1072339
    iget-object v1, p0, LX/6Ha;->j:Lcom/facebook/camera/views/FocusIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/camera/views/FocusIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1072340
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1072341
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1072342
    iget-object v0, p0, LX/6Ha;->j:Lcom/facebook/camera/views/FocusIndicatorView;

    .line 1072343
    iget v1, p0, LX/6Ha;->c:I

    if-nez v1, :cond_3

    .line 1072344
    iget-object v1, p0, LX/6Ha;->l:Ljava/util/List;

    if-nez v1, :cond_2

    .line 1072345
    invoke-interface {v0}, LX/6IM;->d()V

    goto :goto_0

    .line 1072346
    :cond_2
    invoke-interface {v0}, LX/6IM;->a()V

    goto :goto_0

    .line 1072347
    :cond_3
    iget v1, p0, LX/6Ha;->c:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    iget v1, p0, LX/6Ha;->c:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 1072348
    :cond_4
    invoke-interface {v0}, LX/6IM;->a()V

    goto :goto_0

    .line 1072349
    :cond_5
    const-string v1, "continuous-picture"

    iget-object v2, p0, LX/6Ha;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1072350
    invoke-interface {v0}, LX/6IM;->a()V

    goto :goto_0

    .line 1072351
    :cond_6
    iget v1, p0, LX/6Ha;->c:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_7

    .line 1072352
    invoke-interface {v0}, LX/6IM;->b()V

    goto :goto_0

    .line 1072353
    :cond_7
    iget v1, p0, LX/6Ha;->c:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 1072354
    invoke-interface {v0}, LX/6IM;->c()V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1072333
    iget-object v0, p0, LX/6Ha;->q:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1072334
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ha;->z:J

    .line 1072335
    return-void
.end method

.method public static n(LX/6Ha;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1072315
    iget-object v1, p0, LX/6Ha;->p:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/6Ha;->p:Ljava/lang/String;

    .line 1072316
    :goto_0
    move-object v1, v1

    .line 1072317
    if-nez v1, :cond_1

    .line 1072318
    :cond_0
    :goto_1
    return v0

    :cond_1
    const-string v2, "infinity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "fixed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "edof"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 1072319
    :cond_2
    iget-object v1, p0, LX/6Ha;->u:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 1072320
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    .line 1072321
    iget-boolean v1, p0, LX/6Ha;->e:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/6Ha;->l:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 1072322
    const-string v1, "auto"

    iput-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    .line 1072323
    :cond_3
    :goto_2
    iget-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    invoke-static {v1, v3}, LX/6Ha;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1072324
    const-string v1, "auto"

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v3}, LX/6Ha;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1072325
    const-string v1, "auto"

    iput-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    .line 1072326
    :cond_4
    :goto_3
    iget-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    goto :goto_0

    .line 1072327
    :cond_5
    const/4 v1, 0x0

    iput-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    .line 1072328
    iget-object v4, p0, LX/6Ha;->o:[Ljava/lang/String;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    .line 1072329
    invoke-static {v6, v3}, LX/6Ha;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1072330
    iput-object v6, p0, LX/6Ha;->n:Ljava/lang/String;

    goto :goto_2

    .line 1072331
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1072332
    :cond_7
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/6Ha;->n:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6Hi;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x1388

    .line 1072269
    iget-boolean v0, p0, LX/6Ha;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6Ha;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1072270
    :cond_0
    :goto_0
    return-void

    .line 1072271
    :cond_1
    if-nez p1, :cond_2

    .line 1072272
    iget-object v0, p0, LX/6Ha;->y:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1072273
    iget-object v0, p0, LX/6Ha;->y:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1072274
    invoke-static {p0}, LX/6Ha;->i(LX/6Ha;)V

    goto :goto_0

    .line 1072275
    :cond_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1072276
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072277
    iget-boolean v4, v0, LX/6Hi;->h:Z

    if-eqz v4, :cond_3

    invoke-virtual {v0}, LX/6Hi;->a()I

    move-result v4

    const v5, 0x13880

    if-le v4, v5, :cond_3

    iget v4, v0, LX/6Hi;->k:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_3

    .line 1072278
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1072279
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072280
    iget-object v5, p0, LX/6Ha;->y:Ljava/util/HashSet;

    iget-wide v6, v0, LX/6Hi;->l:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1072281
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 1072282
    goto :goto_2

    .line 1072283
    :cond_5
    iget-object v0, p0, LX/6Ha;->y:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-ne v1, v0, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_6

    .line 1072284
    invoke-static {p0, v8}, LX/6Ha;->a(LX/6Ha;I)V

    goto :goto_0

    .line 1072285
    :cond_6
    iget-object v0, p0, LX/6Ha;->y:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1072286
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1072287
    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1072288
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    .line 1072289
    :goto_4
    iget-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    if-nez v0, :cond_8

    .line 1072290
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    .line 1072291
    :goto_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [LX/6Hi;

    .line 1072292
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072293
    add-int/lit8 v1, v2, 0x1

    aput-object v0, v4, v2

    .line 1072294
    iget-object v2, p0, LX/6Ha;->y:Ljava/util/HashSet;

    iget-wide v6, v0, LX/6Hi;->l:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 1072295
    goto :goto_6

    .line 1072296
    :cond_7
    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_4

    .line 1072297
    :cond_8
    iget-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_5

    .line 1072298
    :cond_9
    iget-object v0, p0, LX/6Ha;->v:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1072299
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_7
    if-ltz v0, :cond_c

    .line 1072300
    aget-object v1, v4, v0

    .line 1072301
    iget-object v1, v1, LX/6Hi;->f:Landroid/graphics/Rect;

    invoke-static {v1}, LX/6Ha;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1072302
    new-instance v2, Landroid/hardware/Camera$Area;

    const/16 v3, 0x3e8

    invoke-direct {v2, v1, v3}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    .line 1072303
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v0

    iget v3, p0, LX/6Ha;->w:I

    if-ge v1, v3, :cond_a

    .line 1072304
    iget-object v1, p0, LX/6Ha;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072305
    :cond_a
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v0

    iget v3, p0, LX/6Ha;->x:I

    if-ge v1, v3, :cond_b

    .line 1072306
    iget-object v1, p0, LX/6Ha;->m:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072307
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 1072308
    :cond_c
    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    iget-object v1, p0, LX/6Ha;->m:Ljava/util/List;

    invoke-direct {p0, v0, v1}, LX/6Ha;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1072309
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v4, v0

    .line 1072310
    iget-object v1, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, LX/6Hd;->a(LX/6Hi;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1072311
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    invoke-direct {p0, v1, v0}, LX/6Ha;->a(II)V

    .line 1072312
    iget-boolean v0, p0, LX/6Ha;->e:Z

    if-eqz v0, :cond_d

    .line 1072313
    sget-object v0, LX/6HX;->FACE_DETECTION_AUTOFOCUS:LX/6HX;

    invoke-static {p0, v0, v8}, LX/6Ha;->a(LX/6Ha;LX/6HX;I)V

    goto/16 :goto_0

    .line 1072314
    :cond_d
    invoke-static {p0, v8}, LX/6Ha;->a(LX/6Ha;I)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_3
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/16 v11, 0x2710

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1072245
    iget-boolean v0, p0, LX/6Ha;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v10

    .line 1072246
    :goto_0
    return v0

    .line 1072247
    :cond_1
    iget v0, p0, LX/6Ha;->c:I

    if-eq v0, v9, :cond_2

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p0, LX/6Ha;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1072248
    :cond_2
    invoke-static {p0}, LX/6Ha;->i(LX/6Ha;)V

    .line 1072249
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1072250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1072251
    iget-boolean v0, p0, LX/6Ha;->f:Z

    if-eqz v0, :cond_4

    .line 1072252
    invoke-direct {p0, v4, v5}, LX/6Ha;->a(II)V

    .line 1072253
    :cond_4
    iget-boolean v0, p0, LX/6Ha;->e:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_6

    .line 1072254
    iget-boolean v0, p0, LX/6Ha;->f:Z

    if-eqz v0, :cond_5

    .line 1072255
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1072256
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1072257
    iget-object v0, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 1072258
    iget-object v0, p0, LX/6Ha;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 1072259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    .line 1072260
    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    new-instance v3, Landroid/hardware/Camera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {v3, v8, v9}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    .line 1072262
    iget-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    new-instance v3, Landroid/hardware/Camera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {v3, v8, v9}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072263
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Area;

    iget-object v8, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, LX/6Ha;->a(IIFIIIILandroid/graphics/Rect;)V

    .line 1072264
    const/high16 v3, 0x3fc00000    # 1.5f

    iget-object v0, p0, LX/6Ha;->m:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Area;

    iget-object v8, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, LX/6Ha;->a(IIFIIIILandroid/graphics/Rect;)V

    .line 1072265
    iget-object v0, p0, LX/6Ha;->l:Ljava/util/List;

    iget-object v1, p0, LX/6Ha;->m:Ljava/util/List;

    invoke-direct {p0, v0, v1}, LX/6Ha;->a(Ljava/util/List;Ljava/util/List;)Z

    .line 1072266
    :cond_5
    sget-object v0, LX/6HX;->TOUCH_TO_FOCUS:LX/6HX;

    invoke-static {p0, v0, v11}, LX/6Ha;->a(LX/6Ha;LX/6HX;I)V

    :goto_1
    move v0, v9

    .line 1072267
    goto/16 :goto_0

    .line 1072268
    :cond_6
    invoke-static {p0, v11}, LX/6Ha;->a(LX/6Ha;I)V

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1072241
    const/4 v0, 0x0

    iput v0, p0, LX/6Ha;->c:I

    .line 1072242
    invoke-direct {p0}, LX/6Ha;->m()V

    .line 1072243
    invoke-static {p0}, LX/6Ha;->l(LX/6Ha;)V

    .line 1072244
    return-void
.end method

.method public final g()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1072226
    iget-boolean v0, p0, LX/6Ha;->d:Z

    if-nez v0, :cond_1

    .line 1072227
    :cond_0
    :goto_0
    return-void

    .line 1072228
    :cond_1
    iget-object v0, p0, LX/6Ha;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1072229
    invoke-virtual {v0}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v1

    .line 1072230
    const/16 v2, 0xd

    const/4 v3, -0x1

    aput v3, v1, v2

    .line 1072231
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1072232
    iget-boolean v0, p0, LX/6Ha;->f:Z

    if-eqz v0, :cond_0

    .line 1072233
    iput-object v5, p0, LX/6Ha;->l:Ljava/util/List;

    .line 1072234
    iput-object v5, p0, LX/6Ha;->m:Ljava/util/List;

    .line 1072235
    iget-object v0, p0, LX/6Ha;->u:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 1072236
    invoke-virtual {v0, v5}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    .line 1072237
    invoke-virtual {v0, v5}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    .line 1072238
    :try_start_0
    iget-object v1, p0, LX/6Ha;->u:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1072239
    :catch_0
    move-exception v0

    .line 1072240
    iget-object v1, p0, LX/6Ha;->t:LX/6HF;

    const-string v2, "resetFocus/setParameters failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
