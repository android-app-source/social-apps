.class public final enum LX/6Xh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Xh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Xh;

.field public static final enum APP_FALLBACK_INSTALL:LX/6Xh;

.field public static final enum APP_FALLBACK_WEB:LX/6Xh;

.field public static final enum INTERSTITIAL_ALWAYS:LX/6Xh;

.field public static final enum INTERSTITIAL_CONTENT_ONLY:LX/6Xh;

.field public static final enum INTERSTITIAL_INSTALL_ONLY:LX/6Xh;

.field public static final enum LEGACY:LX/6Xh;

.field public static final enum WEB_ALWAYS:LX/6Xh;


# instance fields
.field private allowOpenInNativeAppIfInstalled:Z

.field private goToMarketIfAppNotInstalled:Z

.field private showInterstitialForInstall:Z

.field private showInterstitialForOpen:Z

.field private string:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x4

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1108740
    new-instance v0, LX/6Xh;

    const-string v1, "LEGACY"

    const-string v3, ""

    move v5, v2

    move v6, v4

    move v7, v2

    invoke-direct/range {v0 .. v7}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v0, LX/6Xh;->LEGACY:LX/6Xh;

    .line 1108741
    new-instance v5, LX/6Xh;

    const-string v6, "INTERSTITIAL_ALWAYS"

    const-string v8, "interstitial_always"

    move v7, v4

    move v9, v4

    move v10, v4

    move v11, v4

    move v12, v4

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->INTERSTITIAL_ALWAYS:LX/6Xh;

    .line 1108742
    new-instance v5, LX/6Xh;

    const-string v6, "INTERSTITIAL_CONTENT_ONLY"

    const-string v8, "interstitial_content_only"

    move v7, v13

    move v9, v4

    move v10, v4

    move v11, v2

    move v12, v2

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->INTERSTITIAL_CONTENT_ONLY:LX/6Xh;

    .line 1108743
    new-instance v5, LX/6Xh;

    const-string v6, "WEB_ALWAYS"

    const-string v8, "web_always"

    move v7, v14

    move v9, v2

    move v10, v2

    move v11, v2

    move v12, v2

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->WEB_ALWAYS:LX/6Xh;

    .line 1108744
    new-instance v5, LX/6Xh;

    const-string v6, "APP_FALLBACK_WEB"

    const-string v8, "app_fallback_web"

    move v7, v15

    move v9, v4

    move v10, v2

    move v11, v2

    move v12, v2

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->APP_FALLBACK_WEB:LX/6Xh;

    .line 1108745
    new-instance v5, LX/6Xh;

    const-string v6, "INTERSTITIAL_INSTALL_ONLY"

    const/4 v7, 0x5

    const-string v8, "interstitial_install_only"

    move v9, v4

    move v10, v2

    move v11, v4

    move v12, v4

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->INTERSTITIAL_INSTALL_ONLY:LX/6Xh;

    .line 1108746
    new-instance v5, LX/6Xh;

    const-string v6, "APP_FALLBACK_INSTALL"

    const/4 v7, 0x6

    const-string v8, "app_fallback_install"

    move v9, v4

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v5 .. v12}, LX/6Xh;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V

    sput-object v5, LX/6Xh;->APP_FALLBACK_INSTALL:LX/6Xh;

    .line 1108747
    const/4 v0, 0x7

    new-array v0, v0, [LX/6Xh;

    sget-object v1, LX/6Xh;->LEGACY:LX/6Xh;

    aput-object v1, v0, v2

    sget-object v1, LX/6Xh;->INTERSTITIAL_ALWAYS:LX/6Xh;

    aput-object v1, v0, v4

    sget-object v1, LX/6Xh;->INTERSTITIAL_CONTENT_ONLY:LX/6Xh;

    aput-object v1, v0, v13

    sget-object v1, LX/6Xh;->WEB_ALWAYS:LX/6Xh;

    aput-object v1, v0, v14

    sget-object v1, LX/6Xh;->APP_FALLBACK_WEB:LX/6Xh;

    aput-object v1, v0, v15

    const/4 v1, 0x5

    sget-object v2, LX/6Xh;->INTERSTITIAL_INSTALL_ONLY:LX/6Xh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6Xh;->APP_FALLBACK_INSTALL:LX/6Xh;

    aput-object v2, v0, v1

    sput-object v0, LX/6Xh;->$VALUES:[LX/6Xh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZZZ)V"
        }
    .end annotation

    .prologue
    .line 1108721
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1108722
    iput-object p3, p0, LX/6Xh;->string:Ljava/lang/String;

    .line 1108723
    iput-boolean p4, p0, LX/6Xh;->allowOpenInNativeAppIfInstalled:Z

    .line 1108724
    iput-boolean p5, p0, LX/6Xh;->showInterstitialForOpen:Z

    .line 1108725
    iput-boolean p6, p0, LX/6Xh;->goToMarketIfAppNotInstalled:Z

    .line 1108726
    iput-boolean p7, p0, LX/6Xh;->showInterstitialForInstall:Z

    .line 1108727
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/6Xh;
    .locals 5

    .prologue
    .line 1108728
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1108729
    invoke-static {}, LX/6Xh;->values()[LX/6Xh;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1108730
    iget-object v4, v0, LX/6Xh;->string:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1108731
    :goto_1
    return-object v0

    .line 1108732
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1108733
    :cond_1
    sget-object v0, LX/6Xh;->LEGACY:LX/6Xh;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Xh;
    .locals 1

    .prologue
    .line 1108734
    const-class v0, LX/6Xh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Xh;

    return-object v0
.end method

.method public static values()[LX/6Xh;
    .locals 1

    .prologue
    .line 1108735
    sget-object v0, LX/6Xh;->$VALUES:[LX/6Xh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Xh;

    return-object v0
.end method


# virtual methods
.method public final shouldAllowOpenInNativeAppIfInstalled()Z
    .locals 1

    .prologue
    .line 1108736
    iget-boolean v0, p0, LX/6Xh;->allowOpenInNativeAppIfInstalled:Z

    return v0
.end method

.method public final shouldGoToMarketIfAppNotInstalled()Z
    .locals 1

    .prologue
    .line 1108737
    iget-boolean v0, p0, LX/6Xh;->goToMarketIfAppNotInstalled:Z

    return v0
.end method

.method public final shouldShowInterstitialForInstall()Z
    .locals 1

    .prologue
    .line 1108738
    iget-boolean v0, p0, LX/6Xh;->showInterstitialForInstall:Z

    return v0
.end method

.method public final shouldShowInterstitialForOpen()Z
    .locals 1

    .prologue
    .line 1108739
    iget-boolean v0, p0, LX/6Xh;->showInterstitialForOpen:Z

    return v0
.end method
