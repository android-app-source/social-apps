.class public final LX/5Ry;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/photos/creativeediting/model/StickerParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:F

.field public d:F

.field public e:LX/434;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 916918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916919
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, LX/5Ry;->c:F

    .line 916920
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/5Ry;->d:F

    .line 916921
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    iput-object v0, p0, LX/5Ry;->e:LX/434;

    .line 916922
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;)V
    .locals 1

    .prologue
    .line 916923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916924
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, LX/5Ry;->c:F

    .line 916925
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/5Ry;->d:F

    .line 916926
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    iput-object v0, p0, LX/5Ry;->e:LX/434;

    .line 916927
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->c:Landroid/graphics/RectF;

    move-object v0, v0

    .line 916928
    iput-object v0, p0, LX/5Ry;->a:Landroid/graphics/RectF;

    .line 916929
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-object v0, v0

    .line 916930
    iput-object v0, p0, LX/5Ry;->b:Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 916931
    iget v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    move v0, v0

    .line 916932
    iput v0, p0, LX/5Ry;->c:F

    .line 916933
    iget v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    move v0, v0

    .line 916934
    iput v0, p0, LX/5Ry;->d:F

    .line 916935
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    move-object v0, v0

    .line 916936
    iput-object v0, p0, LX/5Ry;->e:LX/434;

    .line 916937
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;
    .locals 2

    .prologue
    .line 916938
    new-instance v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;-><init>(LX/5Ry;)V

    return-object v0
.end method
