.class public final synthetic LX/6QQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1087402
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->values()[Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6QQ;->b:[I

    :try_start_0
    sget-object v0, LX/6QQ;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ANY:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    .line 1087403
    :goto_0
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->values()[Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6QQ;->a:[I

    :try_start_1
    sget-object v0, LX/6QQ;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v0, LX/6QQ;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, LX/6QQ;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_2

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_0
.end method
