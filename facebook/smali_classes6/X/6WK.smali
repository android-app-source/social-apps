.class public LX/6WK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1105332
    const/4 v0, 0x5

    new-array v0, v0, [I

    const v1, 0x7f0b0f8f

    aput v1, v0, v3

    const v1, 0x7f0b0f90

    aput v1, v0, v4

    const v1, 0x7f0b0f91

    aput v1, v0, v5

    const v1, 0x7f0b0f92

    aput v1, v0, v6

    const v1, 0x7f0b0f8e

    aput v1, v0, v7

    sput-object v0, LX/6WK;->a:[I

    .line 1105333
    const/4 v0, 0x7

    new-array v0, v0, [I

    const v1, 0x7f0e011e

    aput v1, v0, v3

    const v1, 0x7f0e011f

    aput v1, v0, v4

    const v1, 0x7f0e011c

    aput v1, v0, v5

    const v1, 0x7f0e011d

    aput v1, v0, v6

    const v1, 0x7f0e0120

    aput v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f0e0121

    aput v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f0e0124

    aput v2, v0, v1

    sput-object v0, LX/6WK;->b:[I

    .line 1105334
    const/16 v0, 0xf

    new-array v0, v0, [I

    const v1, 0x7f0e0120

    aput v1, v0, v3

    const v1, 0x7f0e0121

    aput v1, v0, v4

    const v1, 0x7f0e0122

    aput v1, v0, v5

    const v1, 0x7f0e0123

    aput v1, v0, v6

    const v1, 0x7f0e0125

    aput v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f0e0127

    aput v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f0e0126

    aput v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f0e0129

    aput v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f0e012a

    aput v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f0e011e

    aput v2, v0, v1

    const/16 v1, 0xa

    const v2, 0x7f0e011f

    aput v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x7f0e012b

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, 0x7f0e012c

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, 0x7f0e012f

    aput v2, v0, v1

    const/16 v1, 0xe

    const v2, 0x7f0e0130

    aput v2, v0, v1

    sput-object v0, LX/6WK;->c:[I

    .line 1105335
    new-array v0, v5, [I

    const v1, 0x7f0e012d

    aput v1, v0, v3

    const v1, 0x7f0e012e

    aput v1, v0, v4

    sput-object v0, LX/6WK;->d:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1105329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1105330
    return-void
.end method

.method public static a(I)I
    .locals 1
    .param p0    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/TitleTextAppearenceType;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation

    .prologue
    .line 1105331
    sget-object v0, LX/6WK;->b:[I

    aget v0, v0, p0

    return v0
.end method

.method public static b(I)I
    .locals 1
    .param p0    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/BodyTextAppearenceType;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation

    .prologue
    .line 1105336
    sget-object v0, LX/6WK;->c:[I

    aget v0, v0, p0

    return v0
.end method

.method public static c(I)I
    .locals 1
    .param p0    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/MetaTextAppearenceType;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation

    .prologue
    .line 1105327
    sget-object v0, LX/6WK;->d:[I

    aget v0, v0, p0

    return v0
.end method

.method public static d(I)I
    .locals 1
    .param p0    # I
        .annotation build Lcom/facebook/fig/listitem/Const$ThumbnailSize;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation

    .prologue
    .line 1105328
    sget-object v0, LX/6WK;->a:[I

    aget v0, v0, p0

    return v0
.end method
