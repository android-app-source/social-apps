.class public final LX/64v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/64i;

.field public b:Ljava/net/Proxy;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/net/ProxySelector;

.field public h:LX/64g;

.field public i:LX/64U;

.field public j:LX/65O;

.field public k:Ljavax/net/SocketFactory;

.field public l:Ljavax/net/ssl/SSLSocketFactory;

.field public m:LX/66W;

.field public n:Ljavax/net/ssl/HostnameVerifier;

.field public o:LX/64a;

.field public p:LX/64S;

.field public q:LX/64S;

.field public r:LX/64c;

.field public s:LX/64j;

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:I

.field public x:I

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x2710

    const/4 v1, 0x1

    .line 1046097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046098
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/64v;->e:Ljava/util/List;

    .line 1046099
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/64v;->f:Ljava/util/List;

    .line 1046100
    new-instance v0, LX/64i;

    invoke-direct {v0}, LX/64i;-><init>()V

    iput-object v0, p0, LX/64v;->a:LX/64i;

    .line 1046101
    sget-object v0, LX/64w;->z:Ljava/util/List;

    iput-object v0, p0, LX/64v;->c:Ljava/util/List;

    .line 1046102
    sget-object v0, LX/64w;->A:Ljava/util/List;

    iput-object v0, p0, LX/64v;->d:Ljava/util/List;

    .line 1046103
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    iput-object v0, p0, LX/64v;->g:Ljava/net/ProxySelector;

    .line 1046104
    sget-object v0, LX/64g;->a:LX/64g;

    iput-object v0, p0, LX/64v;->h:LX/64g;

    .line 1046105
    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    iput-object v0, p0, LX/64v;->k:Ljavax/net/SocketFactory;

    .line 1046106
    sget-object v0, LX/66g;->a:LX/66g;

    iput-object v0, p0, LX/64v;->n:Ljavax/net/ssl/HostnameVerifier;

    .line 1046107
    sget-object v0, LX/64a;->a:LX/64a;

    iput-object v0, p0, LX/64v;->o:LX/64a;

    .line 1046108
    sget-object v0, LX/64S;->a:LX/64S;

    iput-object v0, p0, LX/64v;->p:LX/64S;

    .line 1046109
    sget-object v0, LX/64S;->a:LX/64S;

    iput-object v0, p0, LX/64v;->q:LX/64S;

    .line 1046110
    new-instance v0, LX/64c;

    invoke-direct {v0}, LX/64c;-><init>()V

    iput-object v0, p0, LX/64v;->r:LX/64c;

    .line 1046111
    sget-object v0, LX/64j;->a:LX/64j;

    iput-object v0, p0, LX/64v;->s:LX/64j;

    .line 1046112
    iput-boolean v1, p0, LX/64v;->t:Z

    .line 1046113
    iput-boolean v1, p0, LX/64v;->u:Z

    .line 1046114
    iput-boolean v1, p0, LX/64v;->v:Z

    .line 1046115
    iput v2, p0, LX/64v;->w:I

    .line 1046116
    iput v2, p0, LX/64v;->x:I

    .line 1046117
    iput v2, p0, LX/64v;->y:I

    .line 1046118
    return-void
.end method

.method public constructor <init>(LX/64w;)V
    .locals 2

    .prologue
    .line 1046068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046069
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/64v;->e:Ljava/util/List;

    .line 1046070
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/64v;->f:Ljava/util/List;

    .line 1046071
    iget-object v0, p1, LX/64w;->a:LX/64i;

    iput-object v0, p0, LX/64v;->a:LX/64i;

    .line 1046072
    iget-object v0, p1, LX/64w;->b:Ljava/net/Proxy;

    iput-object v0, p0, LX/64v;->b:Ljava/net/Proxy;

    .line 1046073
    iget-object v0, p1, LX/64w;->c:Ljava/util/List;

    iput-object v0, p0, LX/64v;->c:Ljava/util/List;

    .line 1046074
    iget-object v0, p1, LX/64w;->d:Ljava/util/List;

    iput-object v0, p0, LX/64v;->d:Ljava/util/List;

    .line 1046075
    iget-object v0, p0, LX/64v;->e:Ljava/util/List;

    iget-object v1, p1, LX/64w;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1046076
    iget-object v0, p0, LX/64v;->f:Ljava/util/List;

    iget-object v1, p1, LX/64w;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1046077
    iget-object v0, p1, LX/64w;->g:Ljava/net/ProxySelector;

    iput-object v0, p0, LX/64v;->g:Ljava/net/ProxySelector;

    .line 1046078
    iget-object v0, p1, LX/64w;->h:LX/64g;

    iput-object v0, p0, LX/64v;->h:LX/64g;

    .line 1046079
    iget-object v0, p1, LX/64w;->j:LX/65O;

    iput-object v0, p0, LX/64v;->j:LX/65O;

    .line 1046080
    iget-object v0, p1, LX/64w;->i:LX/64U;

    iput-object v0, p0, LX/64v;->i:LX/64U;

    .line 1046081
    iget-object v0, p1, LX/64w;->k:Ljavax/net/SocketFactory;

    iput-object v0, p0, LX/64v;->k:Ljavax/net/SocketFactory;

    .line 1046082
    iget-object v0, p1, LX/64w;->l:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, LX/64v;->l:Ljavax/net/ssl/SSLSocketFactory;

    .line 1046083
    iget-object v0, p1, LX/64w;->m:LX/66W;

    iput-object v0, p0, LX/64v;->m:LX/66W;

    .line 1046084
    iget-object v0, p1, LX/64w;->n:Ljavax/net/ssl/HostnameVerifier;

    iput-object v0, p0, LX/64v;->n:Ljavax/net/ssl/HostnameVerifier;

    .line 1046085
    iget-object v0, p1, LX/64w;->o:LX/64a;

    iput-object v0, p0, LX/64v;->o:LX/64a;

    .line 1046086
    iget-object v0, p1, LX/64w;->p:LX/64S;

    iput-object v0, p0, LX/64v;->p:LX/64S;

    .line 1046087
    iget-object v0, p1, LX/64w;->q:LX/64S;

    iput-object v0, p0, LX/64v;->q:LX/64S;

    .line 1046088
    iget-object v0, p1, LX/64w;->r:LX/64c;

    iput-object v0, p0, LX/64v;->r:LX/64c;

    .line 1046089
    iget-object v0, p1, LX/64w;->s:LX/64j;

    iput-object v0, p0, LX/64v;->s:LX/64j;

    .line 1046090
    iget-boolean v0, p1, LX/64w;->t:Z

    iput-boolean v0, p0, LX/64v;->t:Z

    .line 1046091
    iget-boolean v0, p1, LX/64w;->u:Z

    iput-boolean v0, p0, LX/64v;->u:Z

    .line 1046092
    iget-boolean v0, p1, LX/64w;->v:Z

    iput-boolean v0, p0, LX/64v;->v:Z

    .line 1046093
    iget v0, p1, LX/64w;->w:I

    iput v0, p0, LX/64v;->w:I

    .line 1046094
    iget v0, p1, LX/64w;->x:I

    iput v0, p0, LX/64v;->x:I

    .line 1046095
    iget v0, p1, LX/64w;->y:I

    iput v0, p0, LX/64v;->y:I

    .line 1046096
    return-void
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)LX/64v;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1046061
    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046062
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046063
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1046064
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046065
    :cond_2
    cmp-long v2, v0, v4

    if-nez v2, :cond_3

    cmp-long v2, p1, v4

    if-lez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too small."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046066
    :cond_3
    long-to-int v0, v0

    iput v0, p0, LX/64v;->w:I

    .line 1046067
    return-object p0
.end method

.method public final a(LX/64g;)LX/64v;
    .locals 2

    .prologue
    .line 1046023
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "cookieJar == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046024
    :cond_0
    iput-object p1, p0, LX/64v;->h:LX/64g;

    .line 1046025
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/64v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;)",
            "LX/64v;"
        }
    .end annotation

    .prologue
    .line 1046052
    invoke-static {p1}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1046053
    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1046054
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "protocols doesn\'t contain http/1.1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1046055
    :cond_0
    sget-object v1, LX/64x;->HTTP_1_0:LX/64x;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1046056
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "protocols must not contain http/1.0: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1046057
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1046058
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "protocols must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046059
    :cond_2
    invoke-static {v0}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64v;->c:Ljava/util/List;

    .line 1046060
    return-object p0
.end method

.method public final a(Ljavax/net/ssl/SSLSocketFactory;)LX/64v;
    .locals 3

    .prologue
    .line 1046041
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "sslSocketFactory == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046042
    :cond_0
    sget-object v0, LX/66Y;->a:LX/66Y;

    move-object v0, v0

    .line 1046043
    invoke-virtual {v0, p1}, LX/66Y;->a(Ljavax/net/ssl/SSLSocketFactory;)Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    .line 1046044
    if-nez v0, :cond_1

    .line 1046045
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to extract the trust manager on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1046046
    sget-object v2, LX/66Y;->a:LX/66Y;

    move-object v2, v2

    .line 1046047
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sslSocketFactory is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1046048
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046049
    :cond_1
    iput-object p1, p0, LX/64v;->l:Ljavax/net/ssl/SSLSocketFactory;

    .line 1046050
    invoke-static {v0}, LX/66W;->a(Ljavax/net/ssl/X509TrustManager;)LX/66W;

    move-result-object v0

    iput-object v0, p0, LX/64v;->m:LX/66W;

    .line 1046051
    return-object p0
.end method

.method public final a()LX/64w;
    .locals 2

    .prologue
    .line 1046040
    new-instance v0, LX/64w;

    invoke-direct {v0, p0}, LX/64w;-><init>(LX/64v;)V

    return-object v0
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)LX/64v;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1046033
    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046034
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046035
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1046036
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046037
    :cond_2
    cmp-long v2, v0, v4

    if-nez v2, :cond_3

    cmp-long v2, p1, v4

    if-lez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too small."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046038
    :cond_3
    long-to-int v0, v0

    iput v0, p0, LX/64v;->x:I

    .line 1046039
    return-object p0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)LX/64v;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1046026
    cmp-long v0, p1, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046027
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046028
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1046029
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046030
    :cond_2
    cmp-long v2, v0, v4

    if-nez v2, :cond_3

    cmp-long v2, p1, v4

    if-lez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too small."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046031
    :cond_3
    long-to-int v0, v0

    iput v0, p0, LX/64v;->y:I

    .line 1046032
    return-object p0
.end method
