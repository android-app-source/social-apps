.class public final LX/6DA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/browserextensions/common/BrowserExtensionsExternalUrlHandler;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/browserextensions/common/BrowserExtensionsExternalUrlHandler;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1064671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064672
    iput-object p1, p0, LX/6DA;->a:LX/0QB;

    .line 1064673
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1064674
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6DA;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1064675
    packed-switch p2, :pswitch_data_0

    .line 1064676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064677
    :pswitch_0
    new-instance p2, LX/6Er;

    invoke-static {p1}, LX/6D1;->b(LX/0QB;)LX/6D1;

    move-result-object v0

    check-cast v0, LX/6D1;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-direct {p2, v0, v1, p0}, LX/6Er;-><init>(LX/6D1;Lcom/facebook/content/SecureContextHelper;LX/0Uh;)V

    .line 1064678
    move-object v0, p2

    .line 1064679
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1064680
    const/4 v0, 0x1

    return v0
.end method
