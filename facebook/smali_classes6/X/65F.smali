.class public final LX/65F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64r;


# static fields
.field private static final b:LX/656;


# instance fields
.field public final a:LX/65O;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1046790
    new-instance v0, LX/65C;

    invoke-direct {v0}, LX/65C;-><init>()V

    sput-object v0, LX/65F;->b:LX/656;

    return-void
.end method

.method public constructor <init>(LX/65O;)V
    .locals 0

    .prologue
    .line 1046650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046651
    iput-object p1, p0, LX/65F;->a:LX/65O;

    .line 1046652
    return-void
.end method

.method private static a(LX/655;)LX/655;
    .locals 2

    .prologue
    .line 1046653
    if-eqz p0, :cond_0

    .line 1046654
    iget-object v0, p0, LX/655;->g:LX/656;

    move-object v0, v0

    .line 1046655
    if-eqz v0, :cond_0

    .line 1046656
    invoke-virtual {p0}, LX/655;->newBuilder()LX/654;

    move-result-object v0

    const/4 v1, 0x0

    .line 1046657
    iput-object v1, v0, LX/654;->g:LX/656;

    .line 1046658
    move-object v0, v0

    .line 1046659
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1046660
    const-string v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Keep-Alive"

    .line 1046661
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authenticate"

    .line 1046662
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authorization"

    .line 1046663
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TE"

    .line 1046664
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Trailers"

    .line 1046665
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Transfer-Encoding"

    .line 1046666
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Upgrade"

    .line 1046667
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1046668
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/66O;)LX/655;
    .locals 11

    .prologue
    .line 1046669
    iget-object v0, p0, LX/65F;->a:LX/65O;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/65F;->a:LX/65O;

    .line 1046670
    invoke-interface {v0}, LX/65O;->a()LX/655;

    move-result-object v0

    .line 1046671
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1046672
    new-instance v1, LX/65H;

    .line 1046673
    iget-object v4, p1, LX/66O;->f:LX/650;

    move-object v4, v4

    .line 1046674
    invoke-direct {v1, v2, v3, v4, v0}, LX/65H;-><init>(JLX/650;LX/655;)V

    const/4 v4, 0x0

    .line 1046675
    invoke-static {v1}, LX/65H;->b(LX/65H;)LX/65I;

    move-result-object v2

    .line 1046676
    iget-object v3, v2, LX/65I;->a:LX/650;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/65H;->b:LX/650;

    invoke-virtual {v3}, LX/650;->e()LX/64W;

    move-result-object v3

    .line 1046677
    iget-boolean v1, v3, LX/64W;->m:Z

    move v3, v1

    .line 1046678
    if-eqz v3, :cond_0

    .line 1046679
    new-instance v2, LX/65I;

    invoke-direct {v2, v4, v4}, LX/65I;-><init>(LX/650;LX/655;)V

    .line 1046680
    :cond_0
    move-object v1, v2

    .line 1046681
    iget-object v2, v1, LX/65I;->a:LX/650;

    .line 1046682
    iget-object v1, v1, LX/65I;->b:LX/655;

    .line 1046683
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 1046684
    iget-object v3, v0, LX/655;->g:LX/656;

    move-object v3, v3

    .line 1046685
    invoke-static {v3}, LX/65A;->a(Ljava/io/Closeable;)V

    .line 1046686
    :cond_1
    if-nez v2, :cond_4

    if-nez v1, :cond_4

    .line 1046687
    new-instance v0, LX/654;

    invoke-direct {v0}, LX/654;-><init>()V

    .line 1046688
    iget-object v1, p1, LX/66O;->f:LX/650;

    move-object v1, v1

    .line 1046689
    iput-object v1, v0, LX/654;->a:LX/650;

    .line 1046690
    move-object v0, v0

    .line 1046691
    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;

    .line 1046692
    iput-object v1, v0, LX/654;->b:LX/64x;

    .line 1046693
    move-object v0, v0

    .line 1046694
    const/16 v1, 0x1f8

    .line 1046695
    iput v1, v0, LX/654;->c:I

    .line 1046696
    move-object v0, v0

    .line 1046697
    const-string v1, "Unsatisfiable Request (only-if-cached)"

    .line 1046698
    iput-object v1, v0, LX/654;->d:Ljava/lang/String;

    .line 1046699
    move-object v0, v0

    .line 1046700
    sget-object v1, LX/65F;->b:LX/656;

    .line 1046701
    iput-object v1, v0, LX/654;->g:LX/656;

    .line 1046702
    move-object v0, v0

    .line 1046703
    const-wide/16 v2, -0x1

    .line 1046704
    iput-wide v2, v0, LX/654;->k:J

    .line 1046705
    move-object v0, v0

    .line 1046706
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1046707
    iput-wide v2, v0, LX/654;->l:J

    .line 1046708
    move-object v0, v0

    .line 1046709
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    .line 1046710
    :cond_2
    :goto_1
    return-object v0

    .line 1046711
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1046712
    :cond_4
    if-nez v2, :cond_5

    .line 1046713
    invoke-virtual {v1}, LX/655;->newBuilder()LX/654;

    move-result-object v0

    .line 1046714
    invoke-static {v1}, LX/65F;->a(LX/655;)LX/655;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->b(LX/655;)LX/654;

    move-result-object v0

    .line 1046715
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    goto :goto_1

    .line 1046716
    :cond_5
    :try_start_0
    invoke-virtual {p1, v2}, LX/66O;->a(LX/650;)LX/655;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1046717
    if-nez v2, :cond_6

    if-eqz v0, :cond_6

    .line 1046718
    iget-object v3, v0, LX/655;->g:LX/656;

    move-object v0, v3

    .line 1046719
    invoke-static {v0}, LX/65A;->a(Ljava/io/Closeable;)V

    .line 1046720
    :cond_6
    if-eqz v1, :cond_10

    .line 1046721
    const/4 v5, 0x1

    .line 1046722
    iget v6, v2, LX/655;->c:I

    move v6, v6

    .line 1046723
    const/16 v7, 0x130

    if-ne v6, v7, :cond_13

    .line 1046724
    :cond_7
    :goto_2
    move v0, v5

    .line 1046725
    if-eqz v0, :cond_f

    .line 1046726
    invoke-virtual {v1}, LX/655;->newBuilder()LX/654;

    move-result-object v0

    .line 1046727
    iget-object v3, v1, LX/655;->f:LX/64n;

    move-object v3, v3

    .line 1046728
    iget-object v4, v2, LX/655;->f:LX/64n;

    move-object v4, v4

    .line 1046729
    const/4 v5, 0x0

    .line 1046730
    new-instance v7, LX/64m;

    invoke-direct {v7}, LX/64m;-><init>()V

    .line 1046731
    invoke-virtual {v3}, LX/64n;->a()I

    move-result v8

    move v6, v5

    :goto_3
    if-ge v6, v8, :cond_b

    .line 1046732
    invoke-virtual {v3, v6}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v9

    .line 1046733
    invoke-virtual {v3, v6}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v10

    .line 1046734
    const-string p0, "Warning"

    invoke-virtual {p0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_8

    const-string p0, "1"

    invoke-virtual {v10, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_a

    .line 1046735
    :cond_8
    invoke-static {v9}, LX/65F;->a(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_9

    invoke-virtual {v4, v9}, LX/64n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_a

    .line 1046736
    :cond_9
    sget-object p0, LX/64t;->a:LX/64t;

    invoke-virtual {p0, v7, v9, v10}, LX/64t;->a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046737
    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1046738
    :cond_b
    invoke-virtual {v4}, LX/64n;->a()I

    move-result v6

    :goto_4
    if-ge v5, v6, :cond_d

    .line 1046739
    invoke-virtual {v4, v5}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v8

    .line 1046740
    const-string v9, "Content-Length"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 1046741
    invoke-static {v8}, LX/65F;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1046742
    sget-object v9, LX/64t;->a:LX/64t;

    invoke-virtual {v4, v5}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v7, v8, v10}, LX/64t;->a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046743
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1046744
    :cond_d
    invoke-virtual {v7}, LX/64m;->a()LX/64n;

    move-result-object v5

    move-object v3, v5

    .line 1046745
    invoke-virtual {v0, v3}, LX/654;->a(LX/64n;)LX/654;

    move-result-object v0

    .line 1046746
    invoke-static {v1}, LX/65F;->a(LX/655;)LX/655;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->b(LX/655;)LX/654;

    move-result-object v0

    .line 1046747
    invoke-static {v2}, LX/65F;->a(LX/655;)LX/655;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->a(LX/655;)LX/654;

    move-result-object v0

    .line 1046748
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    .line 1046749
    iget-object v1, v2, LX/655;->g:LX/656;

    move-object v1, v1

    .line 1046750
    invoke-virtual {v1}, LX/656;->close()V

    goto/16 :goto_1

    .line 1046751
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_e

    .line 1046752
    iget-object v2, v0, LX/655;->g:LX/656;

    move-object v0, v2

    .line 1046753
    invoke-static {v0}, LX/65A;->a(Ljava/io/Closeable;)V

    :cond_e
    throw v1

    .line 1046754
    :cond_f
    iget-object v0, v1, LX/655;->g:LX/656;

    move-object v0, v0

    .line 1046755
    invoke-static {v0}, LX/65A;->a(Ljava/io/Closeable;)V

    .line 1046756
    :cond_10
    invoke-virtual {v2}, LX/655;->newBuilder()LX/654;

    move-result-object v0

    .line 1046757
    invoke-static {v1}, LX/65F;->a(LX/655;)LX/655;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->b(LX/655;)LX/654;

    move-result-object v0

    .line 1046758
    invoke-static {v2}, LX/65F;->a(LX/655;)LX/655;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->a(LX/655;)LX/654;

    move-result-object v0

    .line 1046759
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v0

    .line 1046760
    invoke-static {v0}, LX/66M;->b(LX/655;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1046761
    iget-object v1, v2, LX/655;->a:LX/650;

    move-object v1, v1

    .line 1046762
    iget-object v2, p0, LX/65F;->a:LX/65O;

    const/4 v3, 0x0

    .line 1046763
    if-nez v2, :cond_15

    .line 1046764
    :cond_11
    :goto_5
    move-object v1, v3

    .line 1046765
    if-nez v1, :cond_16

    .line 1046766
    :cond_12
    :goto_6
    move-object v0, v0

    .line 1046767
    goto/16 :goto_1

    .line 1046768
    :cond_13
    iget-object v6, v1, LX/655;->f:LX/64n;

    move-object v6, v6

    .line 1046769
    const-string v7, "Last-Modified"

    invoke-virtual {v6, v7}, LX/64n;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    .line 1046770
    if-eqz v6, :cond_14

    .line 1046771
    iget-object v7, v2, LX/655;->f:LX/64n;

    move-object v7, v7

    .line 1046772
    const-string v8, "Last-Modified"

    invoke-virtual {v7, v8}, LX/64n;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    .line 1046773
    if-eqz v7, :cond_14

    .line 1046774
    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    cmp-long v6, v7, v9

    if-ltz v6, :cond_7

    .line 1046775
    :cond_14
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 1046776
    :cond_15
    invoke-static {v0, v1}, LX/65I;->a(LX/655;LX/650;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1046777
    invoke-interface {v2}, LX/65O;->b()LX/65G;

    move-result-object v3

    goto :goto_5

    .line 1046778
    :cond_16
    invoke-interface {v1}, LX/65G;->a()LX/65J;

    move-result-object v2

    .line 1046779
    if-eqz v2, :cond_12

    .line 1046780
    iget-object v3, v0, LX/655;->g:LX/656;

    move-object v3, v3

    .line 1046781
    invoke-virtual {v3}, LX/656;->d()LX/671;

    move-result-object v3

    .line 1046782
    invoke-static {v2}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v2

    .line 1046783
    new-instance v4, LX/65E;

    invoke-direct {v4, p0, v3, v1, v2}, LX/65E;-><init>(LX/65F;LX/671;LX/65G;LX/670;)V

    .line 1046784
    invoke-virtual {v0}, LX/655;->newBuilder()LX/654;

    move-result-object v2

    new-instance v3, LX/66P;

    .line 1046785
    iget-object v5, v0, LX/655;->f:LX/64n;

    move-object v5, v5

    .line 1046786
    invoke-static {v4}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v4

    invoke-direct {v3, v5, v4}, LX/66P;-><init>(LX/64n;LX/671;)V

    .line 1046787
    iput-object v3, v2, LX/654;->g:LX/656;

    .line 1046788
    move-object v2, v2

    .line 1046789
    invoke-virtual {v2}, LX/654;->a()LX/655;

    move-result-object v0

    goto :goto_6
.end method
