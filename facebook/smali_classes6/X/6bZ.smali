.class public LX/6bZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6bX;


# instance fields
.field private final a:LX/0Xp;

.field public final b:LX/3Ec;


# direct methods
.method public constructor <init>(LX/0Xp;LX/3Ec;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113687
    iput-object p1, p0, LX/6bZ;->a:LX/0Xp;

    .line 1113688
    iput-object p2, p0, LX/6bZ;->b:LX/3Ec;

    .line 1113689
    return-void
.end method

.method public static b(LX/0QB;)LX/6bZ;
    .locals 3

    .prologue
    .line 1113690
    new-instance v2, LX/6bZ;

    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v0

    check-cast v0, LX/0Xp;

    invoke-static {p0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v1

    check-cast v1, LX/3Ec;

    invoke-direct {v2, v0, v1}, LX/6bZ;-><init>(LX/0Xp;LX/3Ec;)V

    .line 1113691
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1113692
    sget-object v1, LX/007;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1113693
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1113694
    sget-object v1, LX/007;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1113695
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1113696
    :cond_0
    :goto_0
    return v0

    .line 1113697
    :cond_1
    const-string v1, "autocompose"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "compose"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1113698
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1113699
    const-string v3, "tid"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1113700
    const-string v3, "ttype"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1113701
    if-eqz v6, :cond_3

    if-nez v7, :cond_4

    .line 1113702
    :cond_3
    sget-object v3, LX/0aY;->y:Ljava/lang/String;

    .line 1113703
    :goto_1
    move-object v1, v3

    .line 1113704
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1113705
    const-string v1, "send"

    .line 1113706
    const-string v2, "s"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1113707
    if-eqz v2, :cond_6

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1113708
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1113709
    const-string v1, "text"

    .line 1113710
    const-string v2, "m"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1113711
    if-nez v2, :cond_7

    const-string v2, ""

    :goto_3
    move-object v2, v2

    .line 1113712
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1113713
    iget-object v1, p0, LX/6bZ;->a:LX/0Xp;

    invoke-virtual {v1, v0}, LX/0Xp;->a(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 1113714
    :cond_4
    const/4 v3, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_5
    :goto_4
    packed-switch v3, :pswitch_data_1

    .line 1113715
    sget-object v3, LX/0aY;->y:Ljava/lang/String;

    goto :goto_1

    .line 1113716
    :pswitch_0
    const-string v8, "1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v3, v4

    goto :goto_4

    :pswitch_1
    const-string v8, "2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v3, v5

    goto :goto_4

    .line 1113717
    :pswitch_2
    iget-object v3, p0, LX/6bZ;->b:LX/3Ec;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    .line 1113718
    sget-object v6, LX/0aY;->z:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v4

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 1113719
    :pswitch_3
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    .line 1113720
    sget-object v6, LX/0aY;->z:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v4

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
