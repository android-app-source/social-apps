.class public final LX/5BQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 863640
    const/16 v30, 0x0

    .line 863641
    const/16 v29, 0x0

    .line 863642
    const/16 v28, 0x0

    .line 863643
    const/16 v27, 0x0

    .line 863644
    const/16 v26, 0x0

    .line 863645
    const/16 v25, 0x0

    .line 863646
    const/16 v24, 0x0

    .line 863647
    const/16 v23, 0x0

    .line 863648
    const/16 v22, 0x0

    .line 863649
    const/16 v19, 0x0

    .line 863650
    const-wide/16 v20, 0x0

    .line 863651
    const/16 v18, 0x0

    .line 863652
    const/16 v17, 0x0

    .line 863653
    const/16 v16, 0x0

    .line 863654
    const/4 v15, 0x0

    .line 863655
    const/4 v14, 0x0

    .line 863656
    const/4 v13, 0x0

    .line 863657
    const/4 v12, 0x0

    .line 863658
    const/4 v11, 0x0

    .line 863659
    const/4 v10, 0x0

    .line 863660
    const/4 v9, 0x0

    .line 863661
    const/4 v8, 0x0

    .line 863662
    const/4 v7, 0x0

    .line 863663
    const/4 v6, 0x0

    .line 863664
    const/4 v5, 0x0

    .line 863665
    const/4 v4, 0x0

    .line 863666
    const/4 v3, 0x0

    .line 863667
    const/4 v2, 0x0

    .line 863668
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1f

    .line 863669
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 863670
    const/4 v2, 0x0

    .line 863671
    :goto_0
    return v2

    .line 863672
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v32, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v32

    if-eq v2, v0, :cond_16

    .line 863673
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 863674
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 863675
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 863676
    const-string v32, "__type__"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-nez v32, :cond_1

    const-string v32, "__typename"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 863677
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v31, v2

    goto :goto_1

    .line 863678
    :cond_2
    const-string v32, "attachments"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_3

    .line 863679
    invoke-static/range {p0 .. p1}, LX/59I;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 863680
    :cond_3
    const-string v32, "author"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 863681
    invoke-static/range {p0 .. p1}, LX/59B;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 863682
    :cond_4
    const-string v32, "body"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 863683
    invoke-static/range {p0 .. p1}, LX/59F;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 863684
    :cond_5
    const-string v32, "body_markdown_html"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_6

    .line 863685
    invoke-static/range {p0 .. p1}, LX/59C;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 863686
    :cond_6
    const-string v32, "can_edit_constituent_title"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 863687
    const/4 v2, 0x1

    .line 863688
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v26, v12

    move v12, v2

    goto/16 :goto_1

    .line 863689
    :cond_7
    const-string v32, "can_viewer_delete"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_8

    .line 863690
    const/4 v2, 0x1

    .line 863691
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v25, v11

    move v11, v2

    goto/16 :goto_1

    .line 863692
    :cond_8
    const-string v32, "can_viewer_edit"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 863693
    const/4 v2, 0x1

    .line 863694
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v24, v7

    move v7, v2

    goto/16 :goto_1

    .line 863695
    :cond_9
    const-string v32, "can_viewer_share"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 863696
    const/4 v2, 0x1

    .line 863697
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v23, v6

    move v6, v2

    goto/16 :goto_1

    .line 863698
    :cond_a
    const-string v32, "constituent_title"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 863699
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 863700
    :cond_b
    const-string v32, "created_time"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_c

    .line 863701
    const/4 v2, 0x1

    .line 863702
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 863703
    :cond_c
    const-string v32, "edit_history"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_d

    .line 863704
    invoke-static/range {p0 .. p1}, LX/59G;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 863705
    :cond_d
    const-string v32, "feedback"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 863706
    invoke-static/range {p0 .. p1}, LX/5BM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 863707
    :cond_e
    const-string v32, "id"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 863708
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 863709
    :cond_f
    const-string v32, "is_featured"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_10

    .line 863710
    const/4 v2, 0x1

    .line 863711
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v18, v10

    move v10, v2

    goto/16 :goto_1

    .line 863712
    :cond_10
    const-string v32, "is_marked_as_spam"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_11

    .line 863713
    const/4 v2, 0x1

    .line 863714
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v17, v9

    move v9, v2

    goto/16 :goto_1

    .line 863715
    :cond_11
    const-string v32, "is_pinned"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_12

    .line 863716
    const/4 v2, 0x1

    .line 863717
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v16, v8

    move v8, v2

    goto/16 :goto_1

    .line 863718
    :cond_12
    const-string v32, "permalink_title"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_13

    .line 863719
    invoke-static/range {p0 .. p1}, LX/5BP;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 863720
    :cond_13
    const-string v32, "spam_display_mode"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_14

    .line 863721
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 863722
    :cond_14
    const-string v32, "translatability_for_viewer"

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 863723
    invoke-static/range {p0 .. p1}, LX/59K;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 863724
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 863725
    :cond_16
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 863726
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863727
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863728
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863729
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863730
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863731
    if-eqz v12, :cond_17

    .line 863732
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863733
    :cond_17
    if-eqz v11, :cond_18

    .line 863734
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863735
    :cond_18
    if-eqz v7, :cond_19

    .line 863736
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863737
    :cond_19
    if-eqz v6, :cond_1a

    .line 863738
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863739
    :cond_1a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863740
    if-eqz v3, :cond_1b

    .line 863741
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 863742
    :cond_1b
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863743
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863744
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 863745
    if-eqz v10, :cond_1c

    .line 863746
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863747
    :cond_1c
    if-eqz v9, :cond_1d

    .line 863748
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863749
    :cond_1d
    if-eqz v8, :cond_1e

    .line 863750
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 863751
    :cond_1e
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 863752
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 863753
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 863754
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1f
    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move/from16 v34, v18

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v20

    move/from16 v21, v34

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v11

    move v11, v8

    move v8, v2

    goto/16 :goto_1
.end method
