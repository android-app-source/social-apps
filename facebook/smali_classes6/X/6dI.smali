.class public final LX/6dI;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/6dH;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1115675
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1115676
    sget-object v0, LX/6dV;->a:LX/0U1;

    .line 1115677
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115678
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->b:I

    .line 1115679
    sget-object v0, LX/6dV;->b:LX/0U1;

    .line 1115680
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115681
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->c:I

    .line 1115682
    sget-object v0, LX/6dV;->c:LX/0U1;

    .line 1115683
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115684
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->d:I

    .line 1115685
    sget-object v0, LX/6dV;->d:LX/0U1;

    .line 1115686
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115687
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->e:I

    .line 1115688
    sget-object v0, LX/6dV;->e:LX/0U1;

    .line 1115689
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115690
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->f:I

    .line 1115691
    sget-object v0, LX/6dV;->f:LX/0U1;

    .line 1115692
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115693
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->g:I

    .line 1115694
    sget-object v0, LX/6dV;->g:LX/0U1;

    .line 1115695
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1115696
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dI;->h:I

    .line 1115697
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1115698
    const/4 v1, 0x1

    .line 1115699
    iget-object v0, p0, LX/2TZ;->a:Landroid/database/Cursor;

    iget v2, p0, LX/6dI;->c:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 1115700
    iget v0, p0, LX/6dI;->b:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v0

    .line 1115701
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v0, v3, :cond_0

    .line 1115702
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1115703
    :cond_0
    new-instance v3, LX/6ft;

    invoke-direct {v3}, LX/6ft;-><init>()V

    iget v4, p0, LX/6dI;->d:I

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1115704
    iput-object v4, v3, LX/6ft;->a:Ljava/lang/String;

    .line 1115705
    move-object v3, v3

    .line 1115706
    iput-object v0, v3, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1115707
    move-object v0, v3

    .line 1115708
    iget v3, p0, LX/6dI;->e:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1115709
    iput-wide v4, v0, LX/6ft;->c:J

    .line 1115710
    move-object v0, v0

    .line 1115711
    iget v3, p0, LX/6dI;->f:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1115712
    iput-object v3, v0, LX/6ft;->d:Ljava/lang/String;

    .line 1115713
    move-object v3, v0

    .line 1115714
    iget v0, p0, LX/6dI;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 1115715
    :goto_0
    iput-boolean v0, v3, LX/6ft;->g:Z

    .line 1115716
    move-object v0, v3

    .line 1115717
    iget v1, p0, LX/6dI;->h:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115718
    iput-object v1, v0, LX/6ft;->h:Ljava/lang/String;

    .line 1115719
    move-object v0, v0

    .line 1115720
    new-instance v1, LX/6dH;

    invoke-virtual {v0}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/6dH;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    return-object v1

    .line 1115721
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
