.class public LX/5pE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pC;
.implements LX/5pD;


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1007924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007925
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5pE;->a:Ljava/util/List;

    .line 1007926
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 1007927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007928
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/5pE;->a:Ljava/util/List;

    .line 1007929
    return-void
.end method

.method public static a(Ljava/util/List;)LX/5pE;
    .locals 1

    .prologue
    .line 1007894
    new-instance v0, LX/5pE;

    invoke-direct {v0, p0}, LX/5pE;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private c(I)LX/5pE;
    .locals 1

    .prologue
    .line 1007930
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pE;

    return-object v0
.end method

.method private d(I)LX/5pI;
    .locals 1

    .prologue
    .line 1007931
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pI;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(I)LX/5pG;
    .locals 1

    .prologue
    .line 1007932
    invoke-direct {p0, p1}, LX/5pE;->d(I)LX/5pI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5pD;)V
    .locals 1

    .prologue
    .line 1007933
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007934
    return-void
.end method

.method public final a(LX/5pH;)V
    .locals 1

    .prologue
    .line 1007935
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007936
    return-void
.end method

.method public final synthetic b(I)LX/5pC;
    .locals 1

    .prologue
    .line 1007937
    invoke-direct {p0, p1}, LX/5pE;->c(I)LX/5pE;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1007938
    if-ne p0, p1, :cond_1

    .line 1007939
    :cond_0
    :goto_0
    return v0

    .line 1007940
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1007941
    :cond_3
    check-cast p1, LX/5pE;

    .line 1007942
    iget-object v2, p0, LX/5pE;->a:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/5pE;->a:Ljava/util/List;

    iget-object v3, p1, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1007943
    goto :goto_0

    .line 1007944
    :cond_4
    iget-object v2, p1, LX/5pE;->a:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final getBoolean(I)Z
    .locals 1

    .prologue
    .line 1007923
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 1007945
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getInt(I)I
    .locals 1

    .prologue
    .line 1007922
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007921
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getType(I)Lcom/facebook/react/bridge/ReadableType;
    .locals 2

    .prologue
    .line 1007906
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1007907
    if-nez v0, :cond_0

    .line 1007908
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Null:Lcom/facebook/react/bridge/ReadableType;

    .line 1007909
    :goto_0
    return-object v0

    .line 1007910
    :cond_0
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1007911
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Boolean:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007912
    :cond_1
    instance-of v1, v0, Ljava/lang/Double;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Float;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1007913
    :cond_2
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007914
    :cond_3
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1007915
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007916
    :cond_4
    instance-of v1, v0, LX/5pC;

    if-eqz v1, :cond_5

    .line 1007917
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Array:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007918
    :cond_5
    instance-of v0, v0, LX/5pG;

    if-eqz v0, :cond_6

    .line 1007919
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Map:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007920
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1007905
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pushBoolean(Z)V
    .locals 2

    .prologue
    .line 1007903
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007904
    return-void
.end method

.method public final pushDouble(D)V
    .locals 3

    .prologue
    .line 1007901
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007902
    return-void
.end method

.method public final pushInt(I)V
    .locals 2

    .prologue
    .line 1007899
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007900
    return-void
.end method

.method public final pushNull()V
    .locals 2

    .prologue
    .line 1007897
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007898
    return-void
.end method

.method public final pushString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1007895
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007896
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1007893
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007892
    iget-object v0, p0, LX/5pE;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
