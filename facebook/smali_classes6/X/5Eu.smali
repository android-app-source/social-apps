.class public final LX/5Eu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 884198
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 884199
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884200
    :goto_0
    return v1

    .line 884201
    :cond_0
    const-string v6, "viewer_friend_count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 884202
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 884203
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 884204
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 884205
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884206
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 884207
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 884208
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 884209
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 884210
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 884211
    invoke-static {p0, p1}, LX/5Et;->b(LX/15w;LX/186;)I

    move-result v5

    .line 884212
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 884213
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 884214
    goto :goto_1

    .line 884215
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 884216
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 884217
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 884218
    if-eqz v0, :cond_5

    .line 884219
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 884220
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 884221
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884222
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 884223
    if-eqz v0, :cond_1

    .line 884224
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884225
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 884226
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 884227
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/5Et;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 884228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 884229
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 884230
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 884231
    if-eqz v0, :cond_2

    .line 884232
    const-string v1, "viewer_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884233
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 884234
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884235
    return-void
.end method
