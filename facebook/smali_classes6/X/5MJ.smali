.class public LX/5MJ;
.super LX/22N;
.source ""


# direct methods
.method public constructor <init>(LX/1jk;LX/5MV;LX/0ue;LX/0u0;LX/0uc;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 905364
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/22N;-><init>(LX/1jk;LX/1w1;ILX/0ue;LX/0u0;LX/0uc;)V

    .line 905365
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5MJ;->d:Ljava/util/Map;

    .line 905366
    iget-object v0, p2, LX/5MV;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v1

    .line 905367
    if-nez v1, :cond_0

    .line 905368
    new-instance v0, LX/5MH;

    const-string v1, "Missing output type definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905369
    :cond_0
    iget-object v0, p2, LX/5MV;->i:Ljava/util/List;

    if-nez v0, :cond_1

    .line 905370
    new-instance v0, LX/5MH;

    const-string v1, "Missing table"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905371
    :cond_1
    iget-object v0, p2, LX/5MV;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pv;

    .line 905372
    iget-object v4, v0, LX/1pv;->a:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 905373
    new-instance v0, LX/5MH;

    const-string v1, "Missing table item bucket"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905374
    :cond_2
    new-array v4, v3, [LX/0uE;

    .line 905375
    new-instance v5, LX/0uE;

    iget-object v6, v0, LX/1pv;->b:Ljava/lang/String;

    invoke-direct {v5, v1, v6}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    aput-object v5, v4, v7

    .line 905376
    iget-object v5, p0, LX/22N;->d:Ljava/util/Map;

    iget-object v0, v0, LX/1pv;->a:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 905377
    :cond_3
    new-array v0, v3, [LX/0uE;

    iput-object v0, p0, LX/5MJ;->e:[LX/0uE;

    .line 905378
    iget-object v0, p0, LX/22N;->e:[LX/0uE;

    new-instance v2, LX/0uE;

    iget-object v3, p2, LX/5MV;->j:Ljava/lang/String;

    invoke-direct {v2, v1, v3}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    aput-object v2, v0, v7

    .line 905379
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 905380
    const/4 v0, -0x1

    return v0
.end method
