.class public final LX/6J5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ik;


# instance fields
.field public final synthetic a:LX/6Ik;

.field public final synthetic b:LX/6J6;


# direct methods
.method public constructor <init>(LX/6J6;LX/6Ik;)V
    .locals 0

    .prologue
    .line 1075273
    iput-object p1, p0, LX/6J5;->b:LX/6J6;

    iput-object p2, p0, LX/6J5;->a:LX/6Ik;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1075274
    iget-object v0, p0, LX/6J5;->a:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->a()V

    .line 1075275
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1075265
    sget-object v0, LX/6J6;->a:Ljava/lang/String;

    const-string v1, "startPreview onError"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1075266
    iget-object v0, p0, LX/6J5;->b:LX/6J6;

    iget-object v0, v0, LX/6J6;->c:LX/6J2;

    sget-object v1, LX/6J0;->CLOSED:LX/6J0;

    invoke-virtual {v0, v1}, LX/6J2;->a(LX/6J0;)V

    .line 1075267
    iget-object v0, p0, LX/6J5;->b:LX/6J6;

    iget-object v0, v0, LX/6J6;->c:LX/6J2;

    .line 1075268
    invoke-static {}, LX/6J2;->d()V

    .line 1075269
    invoke-static {v0}, LX/6J2;->h(LX/6J2;)V

    .line 1075270
    sget-object v1, LX/6J1;->NONE:LX/6J1;

    iput-object v1, v0, LX/6J2;->e:LX/6J1;

    .line 1075271
    iget-object v0, p0, LX/6J5;->a:LX/6Ik;

    invoke-interface {v0, p1}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    .line 1075272
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1075260
    iget-object v0, p0, LX/6J5;->b:LX/6J6;

    iget-object v0, v0, LX/6J6;->c:LX/6J2;

    sget-object v1, LX/6J0;->PREVIEW:LX/6J0;

    invoke-virtual {v0, v1}, LX/6J2;->a(LX/6J0;)V

    .line 1075261
    iget-object v0, p0, LX/6J5;->b:LX/6J6;

    iget-object v0, v0, LX/6J6;->c:LX/6J2;

    invoke-virtual {v0}, LX/6J2;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075262
    invoke-virtual {p0}, LX/6J5;->a()V

    .line 1075263
    :goto_0
    return-void

    .line 1075264
    :cond_0
    iget-object v0, p0, LX/6J5;->a:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->b()V

    goto :goto_0
.end method
