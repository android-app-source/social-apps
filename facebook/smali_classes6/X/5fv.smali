.class public LX/5fv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0W9;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 972446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972447
    iput-object p1, p0, LX/5fv;->a:LX/0W9;

    .line 972448
    return-void
.end method

.method public static a(LX/0QB;)LX/5fv;
    .locals 1

    .prologue
    .line 972449
    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/5fv;
    .locals 2

    .prologue
    .line 972450
    new-instance v1, LX/5fv;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-direct {v1, v0}, LX/5fv;-><init>(LX/0W9;)V

    .line 972451
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1

    .prologue
    .line 972452
    invoke-static {p1}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 972453
    iget-object p1, p0, LX/5fv;->a:LX/0W9;

    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1, v0, p2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;Ljava/util/Currency;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object p1

    move-object v0, p1

    .line 972454
    return-object v0
.end method

.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 972455
    iget-object v0, p0, LX/5fv;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 972456
    sget-object p0, LX/5fu;->DEFAULT:LX/5fu;

    invoke-virtual {p1, v0, p0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;LX/5fu;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 972457
    return-object v0
.end method

.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 972458
    iget-object v0, p0, LX/5fv;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;LX/5fu;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
