.class public final LX/6MA;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1079507
    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    const v0, -0x44500ed8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchContactQuery"

    const-string v6, "1705d9f920e7ec7961fb899392016dd6"

    const-string v7, "node"

    const-string v8, "10155069964801729"

    const-string v9, "10155259087261729"

    .line 1079508
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1079509
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1079510
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1079511
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1079512
    sparse-switch v0, :sswitch_data_0

    .line 1079513
    :goto_0
    return-object p1

    .line 1079514
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1079515
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1079516
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1079517
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1079518
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_2
        -0x55d248cb -> :sswitch_3
        -0x4e92d738 -> :sswitch_4
        0x856599a -> :sswitch_1
        0x2956b75c -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1079519
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1079520
    :goto_1
    return v0

    .line 1079521
    :pswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1079522
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
