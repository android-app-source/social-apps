.class public final LX/5QA;
.super Landroid/text/style/StyleSpan;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/3Q9;


# direct methods
.method public constructor <init>(LX/3Q9;II)V
    .locals 0

    .prologue
    .line 912368
    iput-object p1, p0, LX/5QA;->b:LX/3Q9;

    iput p3, p0, LX/5QA;->a:I

    invoke-direct {p0, p2}, Landroid/text/style/StyleSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 912369
    iget v1, p0, LX/5QA;->a:I

    .line 912370
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 912371
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 912372
    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 912373
    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 912374
    return-void
.end method
