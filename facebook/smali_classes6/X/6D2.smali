.class public final LX/6D2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0DZ;


# instance fields
.field public final synthetic a:LX/0Da;

.field public final synthetic b:LX/6D3;


# direct methods
.method public constructor <init>(LX/6D3;LX/0Da;)V
    .locals 0

    .prologue
    .line 1064573
    iput-object p1, p0, LX/6D2;->b:LX/6D3;

    iput-object p2, p0, LX/6D2;->a:LX/0Da;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILandroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1064574
    :try_start_0
    iget-object v0, p0, LX/6D2;->a:LX/0Da;

    invoke-interface {v0, p1, p2, p3}, LX/0Da;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILandroid/os/Bundle;)V

    .line 1064575
    iget-object v0, p0, LX/6D2;->b:LX/6D3;

    iget-object v0, v0, LX/6D3;->e:LX/6D0;

    .line 1064576
    iget-object v1, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v1, v1

    .line 1064577
    invoke-virtual {v0, v1}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v0

    .line 1064578
    iget-object v1, v0, LX/6Cz;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CG;

    .line 1064579
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v2, v2

    .line 1064580
    invoke-interface {v1, v2}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064581
    sget-object v2, LX/6Cy;->SUCCESS:LX/6Cy;

    invoke-virtual {v2}, LX/6Cy;->getValue()I

    move-result v2

    if-ne p2, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v1, p1, p2, v2}, LX/6CG;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;IZ)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064582
    :cond_2
    :goto_2
    return-void

    .line 1064583
    :catch_0
    move-exception v0

    .line 1064584
    iget-object v1, p0, LX/6D2;->b:LX/6D3;

    iget-object v1, v1, LX/6D3;->c:LX/03V;

    const-string v2, "BrowserExtensionsJSBridge"

    const-string v3, "Exception %s when handling call %s"

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1064585
    iget-object v4, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1064586
    invoke-static {v3, v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
