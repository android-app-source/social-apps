.class public final LX/58T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 44

    .prologue
    .line 846826
    const/16 v40, 0x0

    .line 846827
    const/16 v39, 0x0

    .line 846828
    const/16 v38, 0x0

    .line 846829
    const/16 v37, 0x0

    .line 846830
    const/16 v36, 0x0

    .line 846831
    const/16 v35, 0x0

    .line 846832
    const/16 v34, 0x0

    .line 846833
    const/16 v33, 0x0

    .line 846834
    const/16 v32, 0x0

    .line 846835
    const/16 v31, 0x0

    .line 846836
    const/16 v30, 0x0

    .line 846837
    const/16 v29, 0x0

    .line 846838
    const/16 v28, 0x0

    .line 846839
    const/16 v27, 0x0

    .line 846840
    const/16 v26, 0x0

    .line 846841
    const/16 v25, 0x0

    .line 846842
    const/16 v24, 0x0

    .line 846843
    const/16 v23, 0x0

    .line 846844
    const/16 v22, 0x0

    .line 846845
    const/16 v21, 0x0

    .line 846846
    const/16 v20, 0x0

    .line 846847
    const/16 v19, 0x0

    .line 846848
    const/16 v18, 0x0

    .line 846849
    const/16 v17, 0x0

    .line 846850
    const/16 v16, 0x0

    .line 846851
    const/4 v15, 0x0

    .line 846852
    const/4 v14, 0x0

    .line 846853
    const/4 v13, 0x0

    .line 846854
    const/4 v12, 0x0

    .line 846855
    const/4 v11, 0x0

    .line 846856
    const/4 v10, 0x0

    .line 846857
    const/4 v9, 0x0

    .line 846858
    const/4 v8, 0x0

    .line 846859
    const/4 v7, 0x0

    .line 846860
    const/4 v6, 0x0

    .line 846861
    const/4 v5, 0x0

    .line 846862
    const/4 v4, 0x0

    .line 846863
    const/4 v3, 0x0

    .line 846864
    const/4 v2, 0x0

    .line 846865
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    .line 846866
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 846867
    const/4 v2, 0x0

    .line 846868
    :goto_0
    return v2

    .line 846869
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 846870
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1c

    .line 846871
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v41

    .line 846872
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 846873
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    if-eqz v41, :cond_1

    .line 846874
    const-string v42, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_2

    .line 846875
    const/4 v13, 0x1

    .line 846876
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 846877
    :cond_2
    const-string v42, "can_see_voice_switcher"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_3

    .line 846878
    const/4 v12, 0x1

    .line 846879
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 846880
    :cond_3
    const-string v42, "can_viewer_comment"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 846881
    const/4 v11, 0x1

    .line 846882
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 846883
    :cond_4
    const-string v42, "can_viewer_comment_with_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 846884
    const/4 v10, 0x1

    .line 846885
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 846886
    :cond_5
    const-string v42, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_6

    .line 846887
    const/4 v9, 0x1

    .line 846888
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 846889
    :cond_6
    const-string v42, "can_viewer_comment_with_video"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 846890
    const/4 v8, 0x1

    .line 846891
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 846892
    :cond_7
    const-string v42, "can_viewer_like"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_8

    .line 846893
    const/4 v7, 0x1

    .line 846894
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 846895
    :cond_8
    const-string v42, "can_viewer_react"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_9

    .line 846896
    const/4 v6, 0x1

    .line 846897
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 846898
    :cond_9
    const-string v42, "can_viewer_subscribe"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 846899
    const/4 v5, 0x1

    .line 846900
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 846901
    :cond_a
    const-string v42, "comments_mirroring_domain"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_b

    .line 846902
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 846903
    :cond_b
    const-string v42, "default_comment_ordering"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_c

    .line 846904
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 846905
    :cond_c
    const-string v42, "does_viewer_like"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_d

    .line 846906
    const/4 v4, 0x1

    .line 846907
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 846908
    :cond_d
    const-string v42, "id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 846909
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 846910
    :cond_e
    const-string v42, "important_reactors"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_f

    .line 846911
    invoke-static/range {p0 .. p1}, LX/58M;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 846912
    :cond_f
    const-string v42, "is_viewer_subscribed"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_10

    .line 846913
    const/4 v3, 0x1

    .line 846914
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 846915
    :cond_10
    const-string v42, "legacy_api_post_id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 846916
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 846917
    :cond_11
    const-string v42, "likers"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 846918
    invoke-static/range {p0 .. p1}, LX/58N;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 846919
    :cond_12
    const-string v42, "reactors"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_13

    .line 846920
    invoke-static/range {p0 .. p1}, LX/58O;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 846921
    :cond_13
    const-string v42, "real_time_activity_info"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_14

    .line 846922
    invoke-static/range {p0 .. p1}, LX/5Ab;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 846923
    :cond_14
    const-string v42, "remixable_photo_uri"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_15

    .line 846924
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 846925
    :cond_15
    const-string v42, "reshares"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_16

    .line 846926
    invoke-static/range {p0 .. p1}, LX/58P;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 846927
    :cond_16
    const-string v42, "supported_reactions"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 846928
    invoke-static/range {p0 .. p1}, LX/58Q;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 846929
    :cond_17
    const-string v42, "top_level_comments"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 846930
    invoke-static/range {p0 .. p1}, LX/58R;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 846931
    :cond_18
    const-string v42, "top_reactions"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_19

    .line 846932
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 846933
    :cond_19
    const-string v42, "viewer_acts_as_page"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 846934
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 846935
    :cond_1a
    const-string v42, "viewer_acts_as_person"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1b

    .line 846936
    invoke-static/range {p0 .. p1}, LX/58S;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 846937
    :cond_1b
    const-string v42, "viewer_feedback_reaction_key"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_0

    .line 846938
    const/4 v2, 0x1

    .line 846939
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 846940
    :cond_1c
    const/16 v41, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 846941
    if-eqz v13, :cond_1d

    .line 846942
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 846943
    :cond_1d
    if-eqz v12, :cond_1e

    .line 846944
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 846945
    :cond_1e
    if-eqz v11, :cond_1f

    .line 846946
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 846947
    :cond_1f
    if-eqz v10, :cond_20

    .line 846948
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 846949
    :cond_20
    if-eqz v9, :cond_21

    .line 846950
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 846951
    :cond_21
    if-eqz v8, :cond_22

    .line 846952
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 846953
    :cond_22
    if-eqz v7, :cond_23

    .line 846954
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 846955
    :cond_23
    if-eqz v6, :cond_24

    .line 846956
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 846957
    :cond_24
    if-eqz v5, :cond_25

    .line 846958
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 846959
    :cond_25
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 846960
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 846961
    if-eqz v4, :cond_26

    .line 846962
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 846963
    :cond_26
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 846964
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 846965
    if-eqz v3, :cond_27

    .line 846966
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 846967
    :cond_27
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846968
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846969
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846970
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846971
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846972
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846973
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846974
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846975
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846976
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 846977
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 846978
    if-eqz v2, :cond_28

    .line 846979
    const/16 v2, 0x1a

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 846980
    :cond_28
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 846981
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 846982
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 846983
    if-eqz v0, :cond_0

    .line 846984
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846985
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 846986
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 846987
    if-eqz v0, :cond_1

    .line 846988
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846989
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 846990
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 846991
    if-eqz v0, :cond_2

    .line 846992
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846993
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 846994
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 846995
    if-eqz v0, :cond_3

    .line 846996
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846997
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 846998
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 846999
    if-eqz v0, :cond_4

    .line 847000
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847001
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847002
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847003
    if-eqz v0, :cond_5

    .line 847004
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847005
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847006
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847007
    if-eqz v0, :cond_6

    .line 847008
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847009
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847010
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847011
    if-eqz v0, :cond_7

    .line 847012
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847013
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847014
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847015
    if-eqz v0, :cond_8

    .line 847016
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847017
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847018
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847019
    if-eqz v0, :cond_9

    .line 847020
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847021
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847022
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847023
    if-eqz v0, :cond_a

    .line 847024
    const-string v1, "default_comment_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847025
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847026
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847027
    if-eqz v0, :cond_b

    .line 847028
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847029
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847030
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847031
    if-eqz v0, :cond_c

    .line 847032
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847033
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847034
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847035
    if-eqz v0, :cond_d

    .line 847036
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847037
    invoke-static {p0, v0, p2, p3}, LX/58M;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 847038
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847039
    if-eqz v0, :cond_e

    .line 847040
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847041
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847042
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847043
    if-eqz v0, :cond_f

    .line 847044
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847045
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847046
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847047
    if-eqz v0, :cond_10

    .line 847048
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847049
    invoke-static {p0, v0, p2}, LX/58N;->a(LX/15i;ILX/0nX;)V

    .line 847050
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847051
    if-eqz v0, :cond_11

    .line 847052
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847053
    invoke-static {p0, v0, p2}, LX/58O;->a(LX/15i;ILX/0nX;)V

    .line 847054
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847055
    if-eqz v0, :cond_12

    .line 847056
    const-string v1, "real_time_activity_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847057
    invoke-static {p0, v0, p2, p3}, LX/5Ab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 847058
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847059
    if-eqz v0, :cond_13

    .line 847060
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847061
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847062
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847063
    if-eqz v0, :cond_14

    .line 847064
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847065
    invoke-static {p0, v0, p2}, LX/58P;->a(LX/15i;ILX/0nX;)V

    .line 847066
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847067
    if-eqz v0, :cond_16

    .line 847068
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847069
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 847070
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_15

    .line 847071
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/58Q;->a(LX/15i;ILX/0nX;)V

    .line 847072
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 847073
    :cond_15
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 847074
    :cond_16
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847075
    if-eqz v0, :cond_17

    .line 847076
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847077
    invoke-static {p0, v0, p2}, LX/58R;->a(LX/15i;ILX/0nX;)V

    .line 847078
    :cond_17
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847079
    if-eqz v0, :cond_18

    .line 847080
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847081
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 847082
    :cond_18
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847083
    if-eqz v0, :cond_19

    .line 847084
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847085
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 847086
    :cond_19
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 847087
    if-eqz v0, :cond_1a

    .line 847088
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847089
    invoke-static {p0, v0, p2}, LX/58S;->a(LX/15i;ILX/0nX;)V

    .line 847090
    :cond_1a
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 847091
    if-eqz v0, :cond_1b

    .line 847092
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847093
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 847094
    :cond_1b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 847095
    return-void
.end method
