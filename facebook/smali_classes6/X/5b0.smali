.class public final LX/5b0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 956553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 956554
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956555
    :goto_0
    return v1

    .line 956556
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956557
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 956558
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 956559
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956560
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 956561
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 956562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 956563
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 956564
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 956565
    const/4 v3, 0x0

    .line 956566
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 956567
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956568
    :goto_3
    move v2, v3

    .line 956569
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 956570
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 956571
    goto :goto_1

    .line 956572
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 956573
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 956574
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 956575
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956576
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 956577
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 956578
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956579
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 956580
    const-string v5, "messaging_actor_id_only"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 956581
    invoke-static {p0, p1}, LX/5bm;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_4

    .line 956582
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 956583
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 956584
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 956585
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956586
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956587
    if-eqz v0, :cond_2

    .line 956588
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956589
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 956590
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 956591
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 956592
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956593
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 956594
    if-eqz v3, :cond_0

    .line 956595
    const-string p1, "messaging_actor_id_only"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956596
    invoke-static {p0, v3, p2}, LX/5bm;->a(LX/15i;ILX/0nX;)V

    .line 956597
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956598
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 956599
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 956600
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956601
    return-void
.end method
