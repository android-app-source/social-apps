.class public final LX/6UN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1101461
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1101462
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1101463
    :goto_0
    return v1

    .line 1101464
    :cond_0
    const-string v9, "video_time_offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1101465
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1101466
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1101467
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1101468
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1101469
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1101470
    const-string v9, "amount_received"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1101471
    invoke-static {p0, p1}, LX/6UK;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1101472
    :cond_2
    const-string v9, "comment"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1101473
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1101474
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1101475
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1101476
    :cond_4
    const-string v9, "tip_giver"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1101477
    invoke-static {p0, p1}, LX/6UM;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1101478
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1101479
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1101480
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1101481
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1101482
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1101483
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1101484
    if-eqz v0, :cond_7

    .line 1101485
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1101486
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1101487
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101488
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1101489
    if-eqz v0, :cond_0

    .line 1101490
    const-string v1, "amount_received"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101491
    invoke-static {p0, v0, p2}, LX/6UK;->a(LX/15i;ILX/0nX;)V

    .line 1101492
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1101493
    if-eqz v0, :cond_1

    .line 1101494
    const-string v1, "comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101495
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101496
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1101497
    if-eqz v0, :cond_2

    .line 1101498
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101499
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101500
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1101501
    if-eqz v0, :cond_3

    .line 1101502
    const-string v1, "tip_giver"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101503
    invoke-static {p0, v0, p2, p3}, LX/6UM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1101504
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1101505
    if-eqz v0, :cond_4

    .line 1101506
    const-string v1, "video_time_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101507
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1101508
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101509
    return-void
.end method
