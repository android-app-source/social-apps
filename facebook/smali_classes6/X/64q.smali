.class public final LX/64q;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[C


# instance fields
.field public final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1045869
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, LX/64q;->a:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>(LX/64p;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1045870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045871
    iget-object v0, p1, LX/64p;->a:Ljava/lang/String;

    iput-object v0, p0, LX/64q;->b:Ljava/lang/String;

    .line 1045872
    iget-object v0, p1, LX/64p;->b:Ljava/lang/String;

    invoke-static {v0, v3}, LX/64q;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64q;->c:Ljava/lang/String;

    .line 1045873
    iget-object v0, p1, LX/64p;->c:Ljava/lang/String;

    invoke-static {v0, v3}, LX/64q;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64q;->d:Ljava/lang/String;

    .line 1045874
    iget-object v0, p1, LX/64p;->d:Ljava/lang/String;

    iput-object v0, p0, LX/64q;->e:Ljava/lang/String;

    .line 1045875
    invoke-virtual {p1}, LX/64p;->a()I

    move-result v0

    iput v0, p0, LX/64q;->f:I

    .line 1045876
    iget-object v0, p1, LX/64p;->f:Ljava/util/List;

    invoke-static {v0, v3}, LX/64q;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64q;->g:Ljava/util/List;

    .line 1045877
    iget-object v0, p1, LX/64p;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/64p;->g:Ljava/util/List;

    const/4 v2, 0x1

    .line 1045878
    invoke-static {v0, v2}, LX/64q;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/64q;->h:Ljava/util/List;

    .line 1045879
    iget-object v0, p1, LX/64p;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/64p;->h:Ljava/lang/String;

    .line 1045880
    invoke-static {v0, v3}, LX/64q;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/64q;->i:Ljava/lang/String;

    .line 1045881
    invoke-virtual {p1}, LX/64p;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64q;->j:Ljava/lang/String;

    .line 1045882
    return-void

    :cond_1
    move-object v0, v1

    .line 1045883
    goto :goto_0
.end method

.method public static a(C)I
    .locals 1

    .prologue
    .line 1045884
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    add-int/lit8 v0, p0, -0x30

    .line 1045885
    :goto_0
    return v0

    .line 1045886
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1045887
    :cond_1
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 1045888
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1045889
    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045890
    const/16 v0, 0x50

    .line 1045891
    :goto_0
    return v0

    .line 1045892
    :cond_0
    const-string v0, "https"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1045893
    const/16 v0, 0x1bb

    goto :goto_0

    .line 1045894
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 1045895
    move v2, p1

    :goto_0
    if-ge v2, p2, :cond_4

    .line 1045896
    invoke-virtual {p0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 1045897
    const/16 v1, 0x20

    if-lt v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-lt v0, v1, :cond_0

    if-nez p7, :cond_2

    .line 1045898
    :cond_0
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    const/16 v1, 0x25

    if-ne v0, v1, :cond_1

    if-eqz p4, :cond_2

    if-eqz p5, :cond_1

    .line 1045899
    invoke-static {p0, v2, p2}, LX/64q;->a(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/16 v1, 0x2b

    if-ne v0, v1, :cond_3

    if-eqz p6, :cond_3

    .line 1045900
    :cond_2
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    .line 1045901
    invoke-virtual {v0, p0, p1, v2}, LX/672;->a(Ljava/lang/String;II)LX/672;

    move-object v1, p0

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    .line 1045902
    invoke-static/range {v0 .. v8}, LX/64q;->a(LX/672;Ljava/lang/String;IILjava/lang/String;ZZZZ)V

    .line 1045903
    invoke-virtual {v0}, LX/672;->p()Ljava/lang/String;

    move-result-object v0

    .line 1045904
    :goto_1
    return-object v0

    .line 1045905
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_0

    .line 1045906
    :cond_4
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;IIZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1045907
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_5

    .line 1045908
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1045909
    const/16 v2, 0x25

    if-eq v1, v2, :cond_0

    const/16 v2, 0x2b

    if-ne v1, v2, :cond_4

    if-eqz p3, :cond_4

    .line 1045910
    :cond_0
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    .line 1045911
    invoke-virtual {v1, p0, p1, v0}, LX/672;->a(Ljava/lang/String;II)LX/672;

    .line 1045912
    const/4 p1, -0x1

    .line 1045913
    move v2, v0

    :goto_1
    if-ge v2, p2, :cond_3

    .line 1045914
    invoke-virtual {p0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    .line 1045915
    const/16 v4, 0x25

    if-ne v3, v4, :cond_1

    add-int/lit8 v4, v2, 0x2

    if-ge v4, p2, :cond_1

    .line 1045916
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, LX/64q;->a(C)I

    move-result v4

    .line 1045917
    add-int/lit8 v5, v2, 0x2

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, LX/64q;->a(C)I

    move-result v5

    .line 1045918
    if-eq v4, p1, :cond_2

    if-eq v5, p1, :cond_2

    .line 1045919
    shl-int/lit8 v4, v4, 0x4

    add-int/2addr v4, v5

    invoke-virtual {v1, v4}, LX/672;->b(I)LX/672;

    .line 1045920
    add-int/lit8 v2, v2, 0x2

    .line 1045921
    :goto_2
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    .line 1045922
    :cond_1
    const/16 v4, 0x2b

    if-ne v3, v4, :cond_2

    if-eqz p3, :cond_2

    .line 1045923
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, LX/672;->b(I)LX/672;

    goto :goto_2

    .line 1045924
    :cond_2
    invoke-virtual {v1, v3}, LX/672;->a(I)LX/672;

    goto :goto_2

    .line 1045925
    :cond_3
    invoke-virtual {v1}, LX/672;->p()Ljava/lang/String;

    move-result-object v0

    .line 1045926
    :goto_3
    return-object v0

    .line 1045927
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1045928
    :cond_5
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1045929
    const/4 v1, 0x0

    .line 1045930
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    .line 1045931
    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1045932
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p0, v0, v1, p1}, LX/64q;->a(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1045933
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1045934
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1045935
    if-eqz v0, :cond_0

    invoke-static {v0, p1}, LX/64q;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1045936
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/672;Ljava/lang/String;IILjava/lang/String;ZZZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x25

    .line 1045937
    const/4 v0, 0x0

    .line 1045938
    :goto_0
    if-ge p2, p3, :cond_8

    .line 1045939
    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 1045940
    if-eqz p5, :cond_0

    const/16 v1, 0x9

    if-eq v2, v1, :cond_1

    const/16 v1, 0xa

    if-eq v2, v1, :cond_1

    const/16 v1, 0xc

    if-eq v2, v1, :cond_1

    const/16 v1, 0xd

    if-eq v2, v1, :cond_1

    .line 1045941
    :cond_0
    const/16 v1, 0x2b

    if-ne v2, v1, :cond_3

    if-eqz p7, :cond_3

    .line 1045942
    if-eqz p5, :cond_2

    const-string v1, "+"

    :goto_1
    invoke-virtual {p0, v1}, LX/672;->a(Ljava/lang/String;)LX/672;

    .line 1045943
    :cond_1
    :goto_2
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr p2, v1

    goto :goto_0

    .line 1045944
    :cond_2
    const-string v1, "%2B"

    goto :goto_1

    .line 1045945
    :cond_3
    const/16 v1, 0x20

    if-lt v2, v1, :cond_5

    const/16 v1, 0x7f

    if-eq v2, v1, :cond_5

    const/16 v1, 0x80

    if-lt v2, v1, :cond_4

    if-nez p8, :cond_5

    .line 1045946
    :cond_4
    invoke-virtual {p4, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_5

    if-ne v2, v5, :cond_7

    if-eqz p5, :cond_5

    if-eqz p6, :cond_7

    .line 1045947
    invoke-static {p1, p2, p3}, LX/64q;->a(Ljava/lang/String;II)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1045948
    :cond_5
    if-nez v0, :cond_6

    .line 1045949
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    .line 1045950
    :cond_6
    invoke-virtual {v0, v2}, LX/672;->a(I)LX/672;

    .line 1045951
    :goto_3
    invoke-virtual {v0}, LX/672;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1045952
    invoke-virtual {v0}, LX/672;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 1045953
    invoke-virtual {p0, v5}, LX/672;->b(I)LX/672;

    .line 1045954
    sget-object v3, LX/64q;->a:[C

    shr-int/lit8 v4, v1, 0x4

    and-int/lit8 v4, v4, 0xf

    aget-char v3, v3, v4

    invoke-virtual {p0, v3}, LX/672;->b(I)LX/672;

    .line 1045955
    sget-object v3, LX/64q;->a:[C

    and-int/lit8 v1, v1, 0xf

    aget-char v1, v3, v1

    invoke-virtual {p0, v1}, LX/672;->b(I)LX/672;

    goto :goto_3

    .line 1045956
    :cond_7
    invoke-virtual {p0, v2}, LX/672;->a(I)LX/672;

    goto :goto_2

    .line 1045957
    :cond_8
    return-void
.end method

.method private static a(Ljava/lang/String;II)Z
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1045958
    add-int/lit8 v0, p1, 0x2

    if-ge v0, p2, :cond_0

    .line 1045959
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x25

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    .line 1045960
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, LX/64q;->a(C)I

    move-result v0

    if-eq v0, v2, :cond_0

    add-int/lit8 v0, p1, 0x2

    .line 1045961
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, LX/64q;->a(C)I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1045962
    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 1045846
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1045847
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 1045848
    const/16 v1, 0x26

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 1045849
    if-ne v1, v4, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1045850
    :cond_0
    const/16 v3, 0x3d

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 1045851
    if-eq v3, v4, :cond_1

    if-le v3, v1, :cond_2

    .line 1045852
    :cond_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045853
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045854
    :goto_1
    add-int/lit8 v0, v1, 0x1

    .line 1045855
    goto :goto_0

    .line 1045856
    :cond_2
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045857
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1045858
    :cond_3
    return-object v2
.end method

.method public static b(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1045859
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 1045860
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1045861
    add-int/lit8 v1, v2, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1045862
    if-lez v2, :cond_0

    const/16 v4, 0x26

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045863
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045864
    if-eqz v1, :cond_1

    .line 1045865
    const/16 v0, 0x3d

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045866
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045867
    :cond_1
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 1045868
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/net/URI;
    .locals 15

    .prologue
    .line 1045770
    new-instance v1, LX/64p;

    invoke-direct {v1}, LX/64p;-><init>()V

    .line 1045771
    iget-object v0, p0, LX/64q;->b:Ljava/lang/String;

    iput-object v0, v1, LX/64p;->a:Ljava/lang/String;

    .line 1045772
    invoke-virtual {p0}, LX/64q;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/64p;->b:Ljava/lang/String;

    .line 1045773
    invoke-virtual {p0}, LX/64q;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/64p;->c:Ljava/lang/String;

    .line 1045774
    iget-object v0, p0, LX/64q;->e:Ljava/lang/String;

    iput-object v0, v1, LX/64p;->d:Ljava/lang/String;

    .line 1045775
    iget v0, p0, LX/64q;->f:I

    iget-object v2, p0, LX/64q;->b:Ljava/lang/String;

    invoke-static {v2}, LX/64q;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v0, v2, :cond_4

    iget v0, p0, LX/64q;->f:I

    :goto_0
    iput v0, v1, LX/64p;->e:I

    .line 1045776
    iget-object v0, v1, LX/64p;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1045777
    iget-object v0, v1, LX/64p;->f:Ljava/util/List;

    invoke-virtual {p0}, LX/64q;->i()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1045778
    invoke-virtual {p0}, LX/64q;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/64p;->c(Ljava/lang/String;)LX/64p;

    .line 1045779
    iget-object v0, p0, LX/64q;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 1045780
    :goto_1
    move-object v0, v0

    .line 1045781
    iput-object v0, v1, LX/64p;->h:Ljava/lang/String;

    .line 1045782
    move-object v0, v1

    .line 1045783
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 1045784
    iget-object v4, v0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    move v10, v8

    :goto_2
    if-ge v10, v11, :cond_0

    .line 1045785
    iget-object v4, v0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1045786
    iget-object v12, v0, LX/64p;->f:Ljava/util/List;

    const-string v5, "[]"

    move v7, v6

    move v9, v6

    .line 1045787
    invoke-static/range {v4 .. v9}, LX/64q;->a(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v4

    .line 1045788
    invoke-interface {v12, v10, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1045789
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_2

    .line 1045790
    :cond_0
    iget-object v4, v0, LX/64p;->g:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 1045791
    iget-object v4, v0, LX/64p;->g:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v4, v8

    :goto_3
    if-ge v4, v5, :cond_2

    .line 1045792
    iget-object v7, v0, LX/64p;->g:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1045793
    if-eqz v9, :cond_1

    .line 1045794
    iget-object v7, v0, LX/64p;->g:Ljava/util/List;

    const-string v10, "\\^`{|}"

    move v11, v6

    move v12, v6

    move v13, v6

    move v14, v6

    .line 1045795
    invoke-static/range {v9 .. v14}, LX/64q;->a(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v9

    .line 1045796
    invoke-interface {v7, v4, v9}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1045797
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1045798
    :cond_2
    iget-object v4, v0, LX/64p;->h:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1045799
    iget-object v4, v0, LX/64p;->h:Ljava/lang/String;

    const-string v5, " \"#<>\\^`{|}"

    move v7, v6

    move v9, v8

    invoke-static/range {v4 .. v9}, LX/64q;->a(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/64p;->h:Ljava/lang/String;

    .line 1045800
    :cond_3
    move-object v0, v0

    .line 1045801
    invoke-virtual {v0}, LX/64p;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1045802
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1045803
    :goto_4
    return-object v0

    .line 1045804
    :catch_0
    move-exception v0

    .line 1045805
    :try_start_1
    const-string v2, "[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1045806
    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_4

    .line 1045807
    :catch_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1045808
    :cond_4
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 1045809
    :cond_5
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    const/16 v2, 0x23

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1045810
    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public final c(Ljava/lang/String;)LX/64q;
    .locals 3

    .prologue
    .line 1045811
    new-instance v0, LX/64p;

    invoke-direct {v0}, LX/64p;-><init>()V

    .line 1045812
    invoke-virtual {v0, p0, p1}, LX/64p;->a(LX/64q;Ljava/lang/String;)LX/64o;

    move-result-object v1

    .line 1045813
    sget-object v2, LX/64o;->SUCCESS:LX/64o;

    if-ne v1, v2, :cond_1

    :goto_0
    move-object v0, v0

    .line 1045814
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/64p;->c()LX/64q;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1045815
    iget-object v0, p0, LX/64q;->b:Ljava/lang/String;

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1045816
    iget-object v0, p0, LX/64q;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 1045817
    :goto_0
    return-object v0

    .line 1045818
    :cond_0
    iget-object v0, p0, LX/64q;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    .line 1045819
    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const-string v3, ":@"

    invoke-static {v1, v0, v2, v3}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v1

    .line 1045820
    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1045821
    iget-object v0, p0, LX/64q;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 1045822
    :goto_0
    return-object v0

    .line 1045823
    :cond_0
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    const/16 v1, 0x3a

    iget-object v2, p0, LX/64q;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1045824
    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1045825
    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1045826
    instance-of v0, p1, LX/64q;

    if-eqz v0, :cond_0

    check-cast p1, LX/64q;

    iget-object v0, p1, LX/64q;->j:Ljava/lang/String;

    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1045827
    iget-object v0, p0, LX/64q;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1045828
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    const/16 v1, 0x2f

    iget-object v2, p0, LX/64q;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1045829
    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const-string v3, "?#"

    invoke-static {v1, v0, v2, v3}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v1

    .line 1045830
    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1045831
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x2f

    .line 1045838
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    iget-object v1, p0, LX/64q;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1045839
    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const-string v3, "?#"

    invoke-static {v1, v0, v2, v3}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v1

    .line 1045840
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1045841
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1045842
    add-int/lit8 v3, v0, 0x1

    .line 1045843
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    invoke-static {v0, v3, v1, v5}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v0

    .line 1045844
    iget-object v4, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v4, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1045845
    :cond_0
    return-object v2
.end method

.method public final j()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1045832
    iget-object v0, p0, LX/64q;->h:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1045833
    :goto_0
    return-object v0

    .line 1045834
    :cond_0
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1045835
    iget-object v1, p0, LX/64q;->j:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x23

    invoke-static {v1, v2, v3, v4}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v1

    .line 1045836
    iget-object v2, p0, LX/64q;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1045837
    iget-object v0, p0, LX/64q;->j:Ljava/lang/String;

    return-object v0
.end method
