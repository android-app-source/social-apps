.class public LX/5zn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public A:Z

.field public B:J

.field public C:Z

.field public D:Z

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/net/Uri;

.field public c:LX/2MK;

.field public d:LX/5zj;

.field public e:LX/5zi;

.field public f:Landroid/net/Uri;

.field public g:Lcom/facebook/ui/media/attachments/MediaResource;

.field public h:J

.field public i:J

.field public j:I

.field public k:I

.field public l:LX/47d;

.field public m:Z

.field public n:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public q:Ljava/lang/String;

.field public r:J

.field public s:Landroid/graphics/RectF;

.field public t:Z

.field public u:I

.field public v:I

.field public w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

.field public x:Z

.field public y:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

.field public z:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1035850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035851
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5zn;->a:Ljava/util/Map;

    .line 1035852
    sget-object v0, LX/5zj;->UNSPECIFIED:LX/5zj;

    iput-object v0, p0, LX/5zn;->d:LX/5zj;

    .line 1035853
    sget-object v0, LX/5zi;->OTHER:LX/5zi;

    iput-object v0, p0, LX/5zn;->e:LX/5zi;

    .line 1035854
    sget-object v0, LX/47d;->UNDEFINED:LX/47d;

    iput-object v0, p0, LX/5zn;->l:LX/47d;

    .line 1035855
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    iput-object v0, p0, LX/5zn;->s:Landroid/graphics/RectF;

    .line 1035856
    const/4 v0, -0x1

    iput v0, p0, LX/5zn;->u:I

    .line 1035857
    const/4 v0, -0x2

    iput v0, p0, LX/5zn;->v:I

    .line 1035858
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/5zn;->B:J

    .line 1035859
    const-string v0, ""

    iput-object v0, p0, LX/5zn;->E:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final G()Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 1

    .prologue
    .line 1035849
    new-instance v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {v0, p0}, Lcom/facebook/ui/media/attachments/MediaResource;-><init>(LX/5zn;)V

    return-object v0
.end method

.method public final a(I)LX/5zn;
    .locals 0

    .prologue
    .line 1035847
    iput p1, p0, LX/5zn;->j:I

    .line 1035848
    return-object p0
.end method

.method public final a(LX/47d;)LX/5zn;
    .locals 0

    .prologue
    .line 1035845
    iput-object p1, p0, LX/5zn;->l:LX/47d;

    .line 1035846
    return-object p0
.end method

.method public final a(Landroid/graphics/RectF;)LX/5zn;
    .locals 0

    .prologue
    .line 1035843
    iput-object p1, p0, LX/5zn;->s:Landroid/graphics/RectF;

    .line 1035844
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/5zn;
    .locals 0

    .prologue
    .line 1035725
    iput-object p1, p0, LX/5zn;->b:Landroid/net/Uri;

    .line 1035726
    return-object p0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;
    .locals 5

    .prologue
    .line 1035745
    iget-wide v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    .line 1035746
    iput-wide v0, p0, LX/5zn;->h:J

    .line 1035747
    move-object v0, p0

    .line 1035748
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1035749
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 1035750
    move-object v0, v0

    .line 1035751
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1035752
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 1035753
    move-object v0, v0

    .line 1035754
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035755
    iput-object v1, v0, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035756
    move-object v0, v0

    .line 1035757
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 1035758
    iput-wide v2, v0, LX/5zn;->i:J

    .line 1035759
    move-object v0, v0

    .line 1035760
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 1035761
    iput v1, v0, LX/5zn;->j:I

    .line 1035762
    move-object v0, v0

    .line 1035763
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 1035764
    iput v1, v0, LX/5zn;->k:I

    .line 1035765
    move-object v0, v0

    .line 1035766
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    .line 1035767
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 1035768
    move-object v0, v0

    .line 1035769
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    .line 1035770
    iput-object v1, v0, LX/5zn;->e:LX/5zi;

    .line 1035771
    move-object v0, v0

    .line 1035772
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    .line 1035773
    iput-object v1, v0, LX/5zn;->l:LX/47d;

    .line 1035774
    move-object v0, v0

    .line 1035775
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    .line 1035776
    iput-boolean v1, v0, LX/5zn;->m:Z

    .line 1035777
    move-object v0, v0

    .line 1035778
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    .line 1035779
    iput-object v1, v0, LX/5zn;->n:Landroid/net/Uri;

    .line 1035780
    move-object v0, v0

    .line 1035781
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 1035782
    iput-object v1, v0, LX/5zn;->o:Ljava/lang/String;

    .line 1035783
    move-object v0, v0

    .line 1035784
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1035785
    iput-object v1, v0, LX/5zn;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1035786
    move-object v0, v0

    .line 1035787
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1035788
    iput-object v1, v0, LX/5zn;->q:Ljava/lang/String;

    .line 1035789
    move-object v0, v0

    .line 1035790
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 1035791
    iput-wide v2, v0, LX/5zn;->r:J

    .line 1035792
    move-object v0, v0

    .line 1035793
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    .line 1035794
    iput-object v1, v0, LX/5zn;->f:Landroid/net/Uri;

    .line 1035795
    move-object v0, v0

    .line 1035796
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 1035797
    iput-object v1, v0, LX/5zn;->s:Landroid/graphics/RectF;

    .line 1035798
    move-object v0, v0

    .line 1035799
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    .line 1035800
    iput-boolean v1, v0, LX/5zn;->t:Z

    .line 1035801
    move-object v0, v0

    .line 1035802
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 1035803
    iput v1, v0, LX/5zn;->u:I

    .line 1035804
    move-object v0, v0

    .line 1035805
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 1035806
    iput v1, v0, LX/5zn;->v:I

    .line 1035807
    move-object v0, v0

    .line 1035808
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 1035809
    iput-object v1, v0, LX/5zn;->w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 1035810
    move-object v0, v0

    .line 1035811
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    .line 1035812
    iput-boolean v1, v0, LX/5zn;->x:Z

    .line 1035813
    move-object v0, v0

    .line 1035814
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    .line 1035815
    iget-object v2, v0, LX/5zn;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 1035816
    iget-object v2, v0, LX/5zn;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1035817
    move-object v0, v0

    .line 1035818
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1035819
    iput-object v1, v0, LX/5zn;->y:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1035820
    move-object v0, v0

    .line 1035821
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    .line 1035822
    iput-object v1, v0, LX/5zn;->z:Landroid/net/Uri;

    .line 1035823
    move-object v0, v0

    .line 1035824
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    .line 1035825
    iput-boolean v1, v0, LX/5zn;->A:Z

    .line 1035826
    move-object v0, v0

    .line 1035827
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    .line 1035828
    iput-wide v2, v0, LX/5zn;->B:J

    .line 1035829
    move-object v0, v0

    .line 1035830
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    .line 1035831
    iput-boolean v1, v0, LX/5zn;->C:Z

    .line 1035832
    move-object v0, v0

    .line 1035833
    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    .line 1035834
    iput-boolean v1, v0, LX/5zn;->D:Z

    .line 1035835
    move-object v0, v0

    .line 1035836
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    .line 1035837
    iput-object v1, v0, LX/5zn;->E:Ljava/lang/String;

    .line 1035838
    move-object v0, v0

    .line 1035839
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    .line 1035840
    iput-object v1, v0, LX/5zn;->F:Ljava/lang/String;

    .line 1035841
    move-object v0, v0

    .line 1035842
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/5zn;
    .locals 1

    .prologue
    .line 1035743
    iget-object v0, p0, LX/5zn;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1035744
    return-object p0
.end method

.method public final b(I)LX/5zn;
    .locals 0

    .prologue
    .line 1035860
    iput p1, p0, LX/5zn;->k:I

    .line 1035861
    return-object p0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;
    .locals 0

    .prologue
    .line 1035741
    iput-object p1, p0, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035742
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5zn;
    .locals 0

    .prologue
    .line 1035739
    iput-object p1, p0, LX/5zn;->q:Ljava/lang/String;

    .line 1035740
    return-object p0
.end method

.method public final b(Z)LX/5zn;
    .locals 0

    .prologue
    .line 1035737
    iput-boolean p1, p0, LX/5zn;->m:Z

    .line 1035738
    return-object p0
.end method

.method public final c(I)LX/5zn;
    .locals 0

    .prologue
    .line 1035735
    iput p1, p0, LX/5zn;->u:I

    .line 1035736
    return-object p0
.end method

.method public final c(J)LX/5zn;
    .locals 1

    .prologue
    .line 1035733
    iput-wide p1, p0, LX/5zn;->r:J

    .line 1035734
    return-object p0
.end method

.method public final c(Landroid/net/Uri;)LX/5zn;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1035731
    iput-object p1, p0, LX/5zn;->n:Landroid/net/Uri;

    .line 1035732
    return-object p0
.end method

.method public final c(Z)LX/5zn;
    .locals 0

    .prologue
    .line 1035729
    iput-boolean p1, p0, LX/5zn;->x:Z

    .line 1035730
    return-object p0
.end method

.method public final d(I)LX/5zn;
    .locals 0

    .prologue
    .line 1035727
    iput p1, p0, LX/5zn;->v:I

    .line 1035728
    return-object p0
.end method
