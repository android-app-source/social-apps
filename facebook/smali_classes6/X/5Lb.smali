.class public final LX/5Lb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 902096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;)LX/5Lb;
    .locals 2

    .prologue
    .line 902076
    new-instance v0, LX/5Lb;

    invoke-direct {v0}, LX/5Lb;-><init>()V

    .line 902077
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 902078
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 902079
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 902080
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->d:Ljava/lang/String;

    .line 902081
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/5Lb;->e:Z

    .line 902082
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->f:Ljava/lang/String;

    .line 902083
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->m()I

    move-result v1

    iput v1, v0, LX/5Lb;->g:I

    .line 902084
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->h:Ljava/lang/String;

    .line 902085
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902086
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902087
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902088
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902089
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902090
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 902091
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Lb;->o:Ljava/lang/String;

    .line 902092
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/5Lb;->p:Z

    .line 902093
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/5Lb;->q:Z

    .line 902094
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/5Lb;->r:Z

    .line 902095
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .locals 15

    .prologue
    .line 902036
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 902037
    iget-object v1, p0, LX/5Lb;->a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 902038
    iget-object v2, p0, LX/5Lb;->b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 902039
    iget-object v3, p0, LX/5Lb;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 902040
    iget-object v4, p0, LX/5Lb;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 902041
    iget-object v5, p0, LX/5Lb;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 902042
    iget-object v6, p0, LX/5Lb;->h:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 902043
    iget-object v7, p0, LX/5Lb;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 902044
    iget-object v8, p0, LX/5Lb;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 902045
    iget-object v9, p0, LX/5Lb;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 902046
    iget-object v10, p0, LX/5Lb;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 902047
    iget-object v11, p0, LX/5Lb;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 902048
    iget-object v12, p0, LX/5Lb;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 902049
    iget-object v13, p0, LX/5Lb;->o:Ljava/lang/String;

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 902050
    const/16 v14, 0x12

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 902051
    const/4 v14, 0x0

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 902052
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902053
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 902054
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 902055
    const/4 v1, 0x4

    iget-boolean v2, p0, LX/5Lb;->e:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 902056
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 902057
    const/4 v1, 0x6

    iget v2, p0, LX/5Lb;->g:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 902058
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 902059
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 902060
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 902061
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 902062
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 902063
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 902064
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 902065
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 902066
    const/16 v1, 0xf

    iget-boolean v2, p0, LX/5Lb;->p:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 902067
    const/16 v1, 0x10

    iget-boolean v2, p0, LX/5Lb;->q:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 902068
    const/16 v1, 0x11

    iget-boolean v2, p0, LX/5Lb;->r:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 902069
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 902070
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 902071
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 902072
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 902073
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 902074
    new-instance v1, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-direct {v1, v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;-><init>(LX/15i;)V

    .line 902075
    return-object v1
.end method
