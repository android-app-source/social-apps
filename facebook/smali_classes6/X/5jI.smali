.class public final enum LX/5jI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5jI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5jI;

.field public static final enum FILTER:LX/5jI;

.field public static final enum FRAME:LX/5jI;

.field public static final enum MASK:LX/5jI;

.field public static final enum PARTICLE_EFFECT:LX/5jI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 986277
    new-instance v0, LX/5jI;

    const-string v1, "FILTER"

    invoke-direct {v0, v1, v2}, LX/5jI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jI;->FILTER:LX/5jI;

    .line 986278
    new-instance v0, LX/5jI;

    const-string v1, "FRAME"

    invoke-direct {v0, v1, v3}, LX/5jI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jI;->FRAME:LX/5jI;

    .line 986279
    new-instance v0, LX/5jI;

    const-string v1, "MASK"

    invoke-direct {v0, v1, v4}, LX/5jI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jI;->MASK:LX/5jI;

    .line 986280
    new-instance v0, LX/5jI;

    const-string v1, "PARTICLE_EFFECT"

    invoke-direct {v0, v1, v5}, LX/5jI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jI;->PARTICLE_EFFECT:LX/5jI;

    .line 986281
    const/4 v0, 0x4

    new-array v0, v0, [LX/5jI;

    sget-object v1, LX/5jI;->FILTER:LX/5jI;

    aput-object v1, v0, v2

    sget-object v1, LX/5jI;->FRAME:LX/5jI;

    aput-object v1, v0, v3

    sget-object v1, LX/5jI;->MASK:LX/5jI;

    aput-object v1, v0, v4

    sget-object v1, LX/5jI;->PARTICLE_EFFECT:LX/5jI;

    aput-object v1, v0, v5

    sput-object v0, LX/5jI;->$VALUES:[LX/5jI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 986276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5jI;
    .locals 1

    .prologue
    .line 986274
    const-class v0, LX/5jI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5jI;

    return-object v0
.end method

.method public static values()[LX/5jI;
    .locals 1

    .prologue
    .line 986275
    sget-object v0, LX/5jI;->$VALUES:[LX/5jI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5jI;

    return-object v0
.end method
