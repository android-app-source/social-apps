.class public final LX/5io;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 984841
    const-wide/16 v16, 0x0

    .line 984842
    const-wide/16 v14, 0x0

    .line 984843
    const/4 v11, 0x0

    .line 984844
    const-wide/16 v12, 0x0

    .line 984845
    const/4 v10, 0x0

    .line 984846
    const/4 v7, 0x0

    .line 984847
    const-wide/16 v8, 0x0

    .line 984848
    const/4 v6, 0x0

    .line 984849
    const/4 v5, 0x0

    .line 984850
    const/4 v4, 0x0

    .line 984851
    const/4 v3, 0x0

    .line 984852
    const/4 v2, 0x0

    .line 984853
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 984854
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 984855
    const/4 v2, 0x0

    .line 984856
    :goto_0
    return v2

    .line 984857
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 984858
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 984859
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 984860
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 984861
    const-string v6, "brightness"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 984862
    const/4 v2, 0x1

    .line 984863
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 984864
    :cond_1
    const-string v6, "contrast"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 984865
    const/4 v2, 0x1

    .line 984866
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 984867
    :cond_2
    const-string v6, "display_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 984868
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 984869
    :cond_3
    const-string v6, "hue"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 984870
    const/4 v2, 0x1

    .line 984871
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 984872
    :cond_4
    const-string v6, "hue_colorize"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 984873
    const/4 v2, 0x1

    .line 984874
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v15, v6

    goto :goto_1

    .line 984875
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 984876
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 984877
    :cond_6
    const-string v6, "saturation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 984878
    const/4 v2, 0x1

    .line 984879
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto/16 :goto_1

    .line 984880
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 984881
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 984882
    if-eqz v3, :cond_9

    .line 984883
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 984884
    :cond_9
    if-eqz v11, :cond_a

    .line 984885
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 984886
    :cond_a
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 984887
    if-eqz v10, :cond_b

    .line 984888
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 984889
    :cond_b
    if-eqz v9, :cond_c

    .line 984890
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 984891
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 984892
    if-eqz v8, :cond_d

    .line 984893
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 984894
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move/from16 v18, v11

    move-wide/from16 v20, v14

    move v11, v5

    move v14, v7

    move v15, v10

    move v10, v4

    move-wide/from16 v4, v16

    move-wide/from16 v16, v12

    move-wide v12, v8

    move v8, v2

    move v9, v3

    move v3, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 984895
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 984896
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 984897
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 984898
    const-string v2, "brightness"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984899
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 984900
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 984901
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 984902
    const-string v2, "contrast"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984903
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 984904
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 984905
    if-eqz v0, :cond_2

    .line 984906
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984907
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 984908
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 984909
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 984910
    const-string v2, "hue"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984911
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 984912
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 984913
    if-eqz v0, :cond_4

    .line 984914
    const-string v1, "hue_colorize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984915
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 984916
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 984917
    if-eqz v0, :cond_5

    .line 984918
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984919
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 984920
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 984921
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 984922
    const-string v2, "saturation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984923
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 984924
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 984925
    return-void
.end method
