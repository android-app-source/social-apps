.class public final LX/5Zx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 953443
    const/16 v17, 0x0

    .line 953444
    const/16 v16, 0x0

    .line 953445
    const/4 v15, 0x0

    .line 953446
    const/4 v14, 0x0

    .line 953447
    const/4 v13, 0x0

    .line 953448
    const/4 v12, 0x0

    .line 953449
    const/4 v11, 0x0

    .line 953450
    const/4 v10, 0x0

    .line 953451
    const/4 v9, 0x0

    .line 953452
    const/4 v8, 0x0

    .line 953453
    const/4 v7, 0x0

    .line 953454
    const/4 v6, 0x0

    .line 953455
    const/4 v5, 0x0

    .line 953456
    const/4 v4, 0x0

    .line 953457
    const/4 v3, 0x0

    .line 953458
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 953459
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 953460
    const/4 v3, 0x0

    .line 953461
    :goto_0
    return v3

    .line 953462
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 953463
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 953464
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 953465
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 953466
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 953467
    const-string v19, "__type__"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const-string v19, "__typename"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 953468
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v17

    goto :goto_1

    .line 953469
    :cond_3
    const-string v19, "best_description"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 953470
    invoke-static/range {p0 .. p1}, LX/5a0;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 953471
    :cond_4
    const-string v19, "commerce_page_type"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 953472
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    goto :goto_1

    .line 953473
    :cond_5
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 953474
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 953475
    :cond_6
    const-string v19, "instant_game_info"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 953476
    invoke-static/range {p0 .. p1}, LX/5aa;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 953477
    :cond_7
    const-string v19, "is_messenger_user"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 953478
    const/4 v3, 0x1

    .line 953479
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 953480
    :cond_8
    const-string v19, "messenger_welcome_page_context_banner"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 953481
    invoke-static/range {p0 .. p1}, LX/5a1;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 953482
    :cond_9
    const-string v19, "name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 953483
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 953484
    :cond_a
    const-string v19, "profile_pic_large"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 953485
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 953486
    :cond_b
    const-string v19, "profile_pic_medium"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 953487
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 953488
    :cond_c
    const-string v19, "profile_pic_small"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 953489
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 953490
    :cond_d
    const-string v19, "responsiveness_context"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 953491
    invoke-static/range {p0 .. p1}, LX/5a2;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 953492
    :cond_e
    const-string v19, "square_logo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 953493
    invoke-static/range {p0 .. p1}, LX/5ab;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 953494
    :cond_f
    const-string v19, "username"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 953495
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 953496
    :cond_10
    const/16 v18, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 953497
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 953498
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 953499
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 953500
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 953501
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 953502
    if-eqz v3, :cond_11

    .line 953503
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 953504
    :cond_11
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 953505
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 953506
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 953507
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 953508
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 953509
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 953510
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 953511
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 953512
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 953513
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 953514
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 953515
    if-eqz v0, :cond_0

    .line 953516
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953517
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 953518
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953519
    if-eqz v0, :cond_1

    .line 953520
    const-string v1, "best_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953521
    invoke-static {p0, v0, p2}, LX/5a0;->a(LX/15i;ILX/0nX;)V

    .line 953522
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 953523
    if-eqz v0, :cond_2

    .line 953524
    const-string v0, "commerce_page_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953525
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953526
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 953527
    if-eqz v0, :cond_3

    .line 953528
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953529
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953530
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953531
    if-eqz v0, :cond_4

    .line 953532
    const-string v1, "instant_game_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953533
    invoke-static {p0, v0, p2, p3}, LX/5aa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 953534
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 953535
    if-eqz v0, :cond_5

    .line 953536
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953537
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 953538
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953539
    if-eqz v0, :cond_6

    .line 953540
    const-string v1, "messenger_welcome_page_context_banner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953541
    invoke-static {p0, v0, p2, p3}, LX/5a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 953542
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 953543
    if-eqz v0, :cond_7

    .line 953544
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953545
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953546
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953547
    if-eqz v0, :cond_8

    .line 953548
    const-string v1, "profile_pic_large"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953549
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 953550
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953551
    if-eqz v0, :cond_9

    .line 953552
    const-string v1, "profile_pic_medium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953553
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 953554
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953555
    if-eqz v0, :cond_a

    .line 953556
    const-string v1, "profile_pic_small"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953557
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 953558
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953559
    if-eqz v0, :cond_b

    .line 953560
    const-string v1, "responsiveness_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953561
    invoke-static {p0, v0, p2}, LX/5a2;->a(LX/15i;ILX/0nX;)V

    .line 953562
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953563
    if-eqz v0, :cond_c

    .line 953564
    const-string v1, "square_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953565
    invoke-static {p0, v0, p2}, LX/5ab;->a(LX/15i;ILX/0nX;)V

    .line 953566
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 953567
    if-eqz v0, :cond_d

    .line 953568
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953569
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953570
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 953571
    return-void
.end method
