.class public final LX/65t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65s;


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:LX/673;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1049054
    const-class v0, LX/65p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/65t;->a:Ljava/util/logging/Logger;

    .line 1049055
    const-string v0, "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    .line 1049056
    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65t;->b:LX/673;

    .line 1049057
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1049053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(IBS)I
    .locals 4

    .prologue
    .line 1049041
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_0

    add-int/lit8 p0, p0, -0x1

    .line 1049042
    :cond_0
    if-le p2, p0, :cond_1

    .line 1049043
    const-string v0, "PROTOCOL_ERROR padding %s > remaining length %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049044
    :cond_1
    sub-int v0, p0, p2

    int-to-short v0, v0

    return v0
.end method

.method public static b(LX/671;)I
    .locals 2

    .prologue
    .line 1049049
    invoke-interface {p0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    .line 1049050
    invoke-interface {p0}, LX/671;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 1049051
    invoke-interface {p0}, LX/671;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 1049052
    return v0
.end method

.method public static varargs c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 2

    .prologue
    .line 1049048
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0, p1}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static varargs d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 1049047
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/671;Z)LX/65Y;
    .locals 2

    .prologue
    .line 1049046
    new-instance v0, LX/65q;

    const/16 v1, 0x1000

    invoke-direct {v0, p1, v1, p2}, LX/65q;-><init>(LX/671;IZ)V

    return-object v0
.end method

.method public final a(LX/670;Z)LX/65Z;
    .locals 1

    .prologue
    .line 1049045
    new-instance v0, LX/65r;

    invoke-direct {v0, p1, p2}, LX/65r;-><init>(LX/670;Z)V

    return-object v0
.end method
