.class public final enum LX/6CE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6CE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6CE;

.field public static final enum NEWSFEED_AD:LX/6CE;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1063608
    new-instance v0, LX/6CE;

    const-string v1, "NEWSFEED_AD"

    const-string v2, "newsfeed_ad"

    invoke-direct {v0, v1, v3, v2}, LX/6CE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CE;->NEWSFEED_AD:LX/6CE;

    .line 1063609
    const/4 v0, 0x1

    new-array v0, v0, [LX/6CE;

    sget-object v1, LX/6CE;->NEWSFEED_AD:LX/6CE;

    aput-object v1, v0, v3

    sput-object v0, LX/6CE;->$VALUES:[LX/6CE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063610
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1063611
    iput-object p3, p0, LX/6CE;->value:Ljava/lang/String;

    .line 1063612
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6CE;
    .locals 1

    .prologue
    .line 1063613
    const-class v0, LX/6CE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6CE;

    return-object v0
.end method

.method public static values()[LX/6CE;
    .locals 1

    .prologue
    .line 1063614
    sget-object v0, LX/6CE;->$VALUES:[LX/6CE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6CE;

    return-object v0
.end method
