.class public LX/5jK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5jJ;

.field public b:F

.field public c:F


# direct methods
.method public constructor <init>(LX/5jJ;)V
    .locals 0

    .prologue
    .line 986342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986343
    iput-object p1, p0, LX/5jK;->a:LX/5jJ;

    .line 986344
    return-void
.end method

.method public constructor <init>(LX/5jK;)V
    .locals 1

    .prologue
    .line 986354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986355
    iget-object v0, p1, LX/5jK;->a:LX/5jJ;

    iput-object v0, p0, LX/5jK;->a:LX/5jJ;

    .line 986356
    iget v0, p1, LX/5jK;->b:F

    iput v0, p0, LX/5jK;->b:F

    .line 986357
    iget v0, p1, LX/5jK;->c:F

    iput v0, p0, LX/5jK;->c:F

    .line 986358
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 2

    .prologue
    .line 986353
    iget v0, p0, LX/5jK;->c:F

    iget v1, p0, LX/5jK;->b:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 986352
    iget-object v0, p0, LX/5jK;->a:LX/5jJ;

    sget-object v1, LX/5jJ;->ON_DOWN:LX/5jJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 986351
    invoke-virtual {p0}, LX/5jK;->a()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 986350
    invoke-virtual {p0}, LX/5jK;->a()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 986349
    iget-object v0, p0, LX/5jK;->a:LX/5jJ;

    sget-object v1, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 986348
    iget-object v0, p0, LX/5jK;->a:LX/5jJ;

    sget-object v1, LX/5jJ;->FINISHING:LX/5jJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 986347
    iget-object v0, p0, LX/5jK;->a:LX/5jJ;

    sget-object v1, LX/5jJ;->SWIPING:LX/5jJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 986346
    invoke-virtual {p0}, LX/5jK;->a()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 986345
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "direction: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "right/up"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/5jK;->a:LX/5jJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startingPosition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/5jK;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentPosition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/5jK;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delta: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/5jK;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "left/down"

    goto :goto_0
.end method
