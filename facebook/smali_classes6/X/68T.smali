.class public final LX/68T;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/68W;

.field public final synthetic val$surface:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/68W;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1055330
    iput-object p1, p0, LX/68T;->this$0:LX/68W;

    iput-object p2, p0, LX/68T;->val$surface:Ljava/lang/String;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1055331
    const-string v0, "surface"

    iget-object v1, p0, LX/68T;->val$surface:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/68T;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055332
    const-string v0, "bytes_downloaded"

    iget-object v1, p0, LX/68T;->this$0:LX/68W;

    .line 1055333
    iget-object v6, v1, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    move-wide v4, v6

    .line 1055334
    move-wide v2, v4

    .line 1055335
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/68T;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055336
    const-string v0, "cache_hit_count"

    iget-object v1, p0, LX/68T;->this$0:LX/68W;

    iget-object v1, v1, LX/68W;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/68T;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055337
    const-string v0, "cache_miss_count"

    iget-object v1, p0, LX/68T;->this$0:LX/68W;

    iget-object v1, v1, LX/68W;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/68T;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055338
    return-void
.end method
