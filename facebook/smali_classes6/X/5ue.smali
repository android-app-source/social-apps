.class public LX/5ue;
.super LX/5ub;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/animation/ValueAnimator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1019800
    const-class v0, LX/5ue;

    sput-object v0, LX/5ue;->a:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/5uZ;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1019830
    invoke-direct {p0, p1}, LX/5ub;-><init>(LX/5uZ;)V

    .line 1019831
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    .line 1019832
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1019833
    return-void

    .line 1019834
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic a(LX/5ue;Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 1019829
    invoke-super {p0, p1}, LX/5ua;->b(Landroid/graphics/Matrix;)V

    return-void
.end method

.method public static i()LX/5ue;
    .locals 3

    .prologue
    .line 1019824
    new-instance v0, LX/5ue;

    .line 1019825
    new-instance v1, LX/5uZ;

    .line 1019826
    new-instance v2, LX/5uY;

    invoke-direct {v2}, LX/5uY;-><init>()V

    move-object v2, v2

    .line 1019827
    invoke-direct {v1, v2}, LX/5uZ;-><init>(LX/5uY;)V

    move-object v1, v1

    .line 1019828
    invoke-direct {v0, v1}, LX/5ue;-><init>(LX/5uZ;)V

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V
    .locals 6
    .param p4    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1019808
    invoke-virtual {p0}, LX/5ue;->g()V

    .line 1019809
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 1019810
    iget-boolean v0, p0, LX/5ub;->a:Z

    move v0, v0

    .line 1019811
    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/03g;->b(Z)V

    .line 1019812
    iput-boolean v1, p0, LX/5ub;->a:Z

    .line 1019813
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1019814
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    move-object v0, v0

    .line 1019815
    iget-object v1, p0, LX/5ub;->b:[F

    move-object v1, v1

    .line 1019816
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1019817
    iget-object v0, p0, LX/5ub;->c:[F

    move-object v0, v0

    .line 1019818
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1019819
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/5uc;

    invoke-direct {v1, p0}, LX/5uc;-><init>(LX/5ue;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1019820
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/5ud;

    invoke-direct {v1, p0, p4}, LX/5ud;-><init>(LX/5ue;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1019821
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1019822
    return-void

    :cond_1
    move v0, v2

    .line 1019823
    goto :goto_0
.end method

.method public final g()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1019802
    iget-boolean v0, p0, LX/5ub;->a:Z

    move v0, v0

    .line 1019803
    if-nez v0, :cond_0

    .line 1019804
    :goto_0
    return-void

    .line 1019805
    :cond_0
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1019806
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1019807
    iget-object v0, p0, LX/5ue;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0
.end method

.method public final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1019801
    sget-object v0, LX/5ue;->a:Ljava/lang/Class;

    return-object v0
.end method
