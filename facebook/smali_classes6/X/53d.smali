.class public abstract LX/53d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:I

.field private c:LX/52r;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 827294
    const-wide/16 v0, 0x43

    invoke-direct {p0, v2, v2, v0, v1}, LX/53d;-><init>(IIJ)V

    .line 827295
    return-void
.end method

.method private constructor <init>(IIJ)V
    .locals 7

    .prologue
    .line 827296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827297
    iput p2, p0, LX/53d;->b:I

    .line 827298
    invoke-static {}, LX/54I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827299
    new-instance v0, LX/541;

    iget v1, p0, LX/53d;->b:I

    const/16 v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, v1}, LX/541;-><init>(I)V

    iput-object v0, p0, LX/53d;->a:Ljava/util/Queue;

    .line 827300
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_1

    .line 827301
    iget-object v1, p0, LX/53d;->a:Ljava/util/Queue;

    invoke-virtual {p0}, LX/53d;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 827302
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 827303
    :cond_0
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/53d;->a:Ljava/util/Queue;

    goto :goto_0

    .line 827304
    :cond_1
    sget-object v0, LX/54b;->d:LX/54b;

    iget-object v0, v0, LX/54b;->a:LX/52s;

    move-object v0, v0

    .line 827305
    invoke-virtual {v0}, LX/52s;->a()LX/52r;

    move-result-object v0

    iput-object v0, p0, LX/53d;->c:LX/52r;

    .line 827306
    iget-object v0, p0, LX/53d;->c:LX/52r;

    new-instance v1, LX/53i;

    invoke-direct {v1, p0, p1, p2}, LX/53i;-><init>(LX/53d;II)V

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v2, p3

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, LX/52r;->a(LX/0vR;JJLjava/util/concurrent/TimeUnit;)LX/0za;

    .line 827307
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 827308
    if-nez p1, :cond_0

    .line 827309
    :goto_0
    return-void

    .line 827310
    :cond_0
    iget-object v0, p0, LX/53d;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 827311
    iget-object v0, p0, LX/53d;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 827312
    invoke-virtual {p0}, LX/53d;->a()Ljava/lang/Object;

    move-result-object v0

    .line 827313
    :cond_0
    return-object v0
.end method
