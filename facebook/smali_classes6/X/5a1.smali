.class public final LX/5a1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 953649
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 953650
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953651
    :goto_0
    return v1

    .line 953652
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953653
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 953654
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 953655
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 953656
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 953657
    const-string v4, "subtitles"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 953658
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 953659
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 953660
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 953661
    const/4 v4, 0x0

    .line 953662
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_9

    .line 953663
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953664
    :goto_3
    move v3, v4

    .line 953665
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 953666
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 953667
    goto :goto_1

    .line 953668
    :cond_3
    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 953669
    const/4 v3, 0x0

    .line 953670
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_d

    .line 953671
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953672
    :goto_4
    move v0, v3

    .line 953673
    goto :goto_1

    .line 953674
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 953675
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 953676
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 953677
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 953678
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953679
    :cond_7
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 953680
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 953681
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 953682
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 953683
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 953684
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_5

    .line 953685
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 953686
    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 953687
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_9
    move v3, v4

    goto :goto_5

    .line 953688
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953689
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_c

    .line 953690
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 953691
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 953692
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_b

    if-eqz v4, :cond_b

    .line 953693
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 953694
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_6

    .line 953695
    :cond_c
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 953696
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 953697
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_4

    :cond_d
    move v0, v3

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 953698
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 953699
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953700
    if-eqz v0, :cond_2

    .line 953701
    const-string v1, "subtitles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953702
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 953703
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 953704
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 953705
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 953706
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 953707
    if-eqz v3, :cond_0

    .line 953708
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953709
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953710
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 953711
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 953712
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 953713
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953714
    if-eqz v0, :cond_4

    .line 953715
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953716
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 953717
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 953718
    if-eqz v1, :cond_3

    .line 953719
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953720
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953721
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 953722
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 953723
    return-void
.end method
