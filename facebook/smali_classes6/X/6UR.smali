.class public LX/6UR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1101597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101598
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1101574
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;

    .line 1101575
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1101576
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101577
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "video_id"

    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101578
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "timespent_watching_video"

    iget v3, p1, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;->b:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101579
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "live_videos_watching_events"

    .line 1101580
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1101581
    move-object v1, v1

    .line 1101582
    const-string v2, "POST"

    .line 1101583
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1101584
    move-object v1, v1

    .line 1101585
    const-string v2, "live_videos_watching_events"

    .line 1101586
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1101587
    move-object v1, v1

    .line 1101588
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1101589
    move-object v0, v1

    .line 1101590
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1101591
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1101592
    move-object v0, v0

    .line 1101593
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 1101594
    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1101595
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1101596
    const/4 v0, 0x0

    return-object v0
.end method
