.class public LX/6Yg;
.super LX/6Ya;
.source ""


# instance fields
.field private final c:LX/7Y8;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/4g9;

.field private final f:LX/0yH;


# direct methods
.method public constructor <init>(LX/7Y8;LX/0yH;LX/0Or;LX/4g9;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroBroadcastManager;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroAnalyticsLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110369
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110370
    iput-object p1, p0, LX/6Yg;->c:LX/7Y8;

    .line 1110371
    iput-object p2, p0, LX/6Yg;->f:LX/0yH;

    .line 1110372
    iput-object p3, p0, LX/6Yg;->d:LX/0Or;

    .line 1110373
    iput-object p4, p0, LX/6Yg;->e:LX/4g9;

    .line 1110374
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1110375
    iget-object v0, p0, LX/6Yg;->e:LX/4g9;

    sget-object v1, LX/4g6;->f:LX/4g6;

    invoke-virtual {p0}, LX/6Ya;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110376
    iget-object v0, p0, LX/6Yg;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110377
    iget-object v0, p0, LX/6Yg;->c:LX/7Y8;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->UPSELL:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Y8;->a(Landroid/content/Intent;)V

    .line 1110378
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110379
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    move-object v0, v1

    .line 1110380
    invoke-virtual {p0}, LX/6Ya;->f()LX/6Y5;

    move-result-object v1

    .line 1110381
    if-nez v0, :cond_1

    .line 1110382
    iget-object v0, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110383
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1110384
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1110385
    iget-object v0, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110386
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1110387
    :goto_0
    move-object v0, v0

    .line 1110388
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/6Y5;->a(Ljava/lang/String;Z)LX/6Y5;

    move-result-object v0

    .line 1110389
    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110390
    iget-object v3, v2, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1110391
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1110392
    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110393
    iget-object v3, v2, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1110394
    :goto_1
    move-object v2, v2

    .line 1110395
    iput-object v2, v0, LX/6Y5;->c:Ljava/lang/String;

    .line 1110396
    move-object v0, v0

    .line 1110397
    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110398
    iget-object v3, v2, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    move-object v2, v3

    .line 1110399
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1110400
    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110401
    iget-object v3, v2, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    move-object v2, v3

    .line 1110402
    :goto_2
    move-object v2, v2

    .line 1110403
    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    .line 1110404
    :goto_3
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110405
    invoke-virtual {v0, v1}, LX/6YP;->a(LX/6Y5;)V

    .line 1110406
    return-object v0

    .line 1110407
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c()Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    move-result-object v0

    .line 1110408
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1110409
    invoke-virtual {v1, v2}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v2

    .line 1110410
    iget-object v3, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1110411
    iput-object v3, v2, LX/6Y5;->c:Ljava/lang/String;

    .line 1110412
    move-object v2, v2

    .line 1110413
    iget-object v3, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1110414
    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v2

    .line 1110415
    iget-object v3, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    move-object v0, v3

    .line 1110416
    invoke-virtual {p0}, LX/6Ya;->d()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    goto :goto_3

    :cond_2
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v2, 0x7f080e05

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v3, 0x7f080e06

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v3, 0x7f080e07

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method
