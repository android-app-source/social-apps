.class public final LX/66M;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1050050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050051
    return-void
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1050045
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1050046
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1050047
    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 1050048
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1050049
    :cond_1
    return p1
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 1050041
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1050042
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1050043
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1050044
    :cond_0
    return p1
.end method

.method public static a(LX/64n;)J
    .locals 5

    .prologue
    .line 1049990
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, LX/64n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    .line 1049991
    if-nez v0, :cond_0

    .line 1049992
    :goto_0
    move-wide v0, v2

    .line 1049993
    return-wide v0

    .line 1049994
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 1049995
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/655;)J
    .locals 2

    .prologue
    .line 1050039
    iget-object v0, p0, LX/655;->f:LX/64n;

    move-object v0, v0

    .line 1050040
    invoke-static {v0}, LX/66M;->a(LX/64n;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/64g;LX/64q;LX/64n;)V
    .locals 10

    .prologue
    .line 1050014
    sget-object v0, LX/64g;->a:LX/64g;

    if-ne p0, v0, :cond_1

    .line 1050015
    :cond_0
    :goto_0
    return-void

    .line 1050016
    :cond_1
    const-string v2, "Set-Cookie"

    .line 1050017
    const/4 v4, 0x0

    .line 1050018
    const/4 v3, 0x0

    invoke-virtual {p2}, LX/64n;->a()I

    move-result v5

    move v7, v3

    move-object v3, v4

    move v4, v7

    :goto_1
    if-ge v4, v5, :cond_4

    .line 1050019
    invoke-virtual {p2, v4}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1050020
    if-nez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1050021
    :cond_2
    invoke-virtual {p2, v4}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1050022
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1050023
    :cond_4
    if-eqz v3, :cond_9

    .line 1050024
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    :goto_2
    move-object v5, v3

    .line 1050025
    const/4 v3, 0x0

    .line 1050026
    const/4 v2, 0x0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v4, v2

    :goto_3
    if-ge v4, v6, :cond_5

    .line 1050027
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1050028
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9, p1, v2}, LX/64f;->a(JLX/64q;Ljava/lang/String;)LX/64f;

    move-result-object v8

    move-object v7, v8

    .line 1050029
    if-eqz v7, :cond_8

    .line 1050030
    if-nez v3, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1050031
    :goto_4
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1050032
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_3

    .line 1050033
    :cond_5
    if-eqz v3, :cond_6

    .line 1050034
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_6
    move-object v0, v2

    .line 1050035
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1050036
    invoke-interface {p0, p1, v0}, LX/64g;->a(LX/64q;Ljava/util/List;)V

    goto :goto_0

    .line 1050037
    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_6

    :cond_7
    move-object v2, v3

    goto :goto_4

    :cond_8
    move-object v2, v3

    goto :goto_5

    .line 1050038
    :cond_9
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    goto :goto_2
.end method

.method public static b(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 1050006
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1050007
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1050008
    const p1, 0x7fffffff

    .line 1050009
    :goto_0
    return p1

    .line 1050010
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 1050011
    const/4 p1, 0x0

    goto :goto_0

    .line 1050012
    :cond_1
    long-to-int p1, v0

    goto :goto_0

    .line 1050013
    :catch_0
    goto :goto_0
.end method

.method public static b(LX/655;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1049996
    iget-object v2, p0, LX/655;->a:LX/650;

    move-object v2, v2

    .line 1049997
    iget-object v3, v2, LX/650;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1049998
    const-string v3, "HEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1049999
    :cond_0
    :goto_0
    return v0

    .line 1050000
    :cond_1
    iget v2, p0, LX/655;->c:I

    move v2, v2

    .line 1050001
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1050002
    goto :goto_0

    .line 1050003
    :cond_3
    invoke-static {p0}, LX/66M;->a(LX/655;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    const-string v2, "chunked"

    const-string v3, "Transfer-Encoding"

    .line 1050004
    invoke-virtual {p0, v3}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 1050005
    goto :goto_0
.end method
