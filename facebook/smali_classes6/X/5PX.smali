.class public LX/5PX;
.super LX/5PV;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 910833
    new-instance v0, LX/5PM;

    invoke-direct {v0}, LX/5PM;-><init>()V

    invoke-virtual {v0}, LX/5PM;->e()LX/5PM;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, LX/5PX;-><init>(LX/5PM;II)V

    .line 910834
    return-void
.end method

.method private constructor <init>(LX/5PM;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 910835
    invoke-direct {p0, p1}, LX/5PV;-><init>(LX/5PM;)V

    .line 910836
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/16 v1, 0x3057

    aput v1, v0, v3

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3056

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 910837
    iget-object v1, p0, LX/5PV;->b:LX/5PM;

    .line 910838
    iget-object v2, v1, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move-object v1, v2

    .line 910839
    iget-object v2, p0, LX/5PV;->b:LX/5PM;

    .line 910840
    iget-object p1, v2, LX/5PM;->c:Landroid/opengl/EGLConfig;

    move-object v2, p1

    .line 910841
    invoke-static {v1, v2, v0, v3}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/5PX;->a:Landroid/opengl/EGLSurface;

    .line 910842
    const-string v0, "eglCreatePbufferSurface"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910843
    iget-object v0, p0, LX/5PV;->a:Landroid/opengl/EGLSurface;

    invoke-static {v0}, LX/64O;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910844
    return-void
.end method
