.class public final LX/57B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 840983
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_7

    .line 840984
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 840985
    :goto_0
    return v0

    .line 840986
    :cond_0
    const-string v11, "expiration"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 840987
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v7, v1

    .line 840988
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 840989
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 840990
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 840991
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 840992
    const-string v11, "destination_link"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 840993
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 840994
    :cond_2
    const-string v11, "is_messenger"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 840995
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v8, v6

    move v6, v1

    goto :goto_1

    .line 840996
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 840997
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 840998
    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 840999
    if-eqz v7, :cond_5

    move-object v0, p1

    .line 841000
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 841001
    :cond_5
    if-eqz v6, :cond_6

    .line 841002
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 841003
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_7
    move v6, v0

    move v7, v0

    move v8, v0

    move-wide v2, v4

    move v9, v0

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 840968
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 840969
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 840970
    if-eqz v0, :cond_0

    .line 840971
    const-string v1, "destination_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 840972
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 840973
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 840974
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 840975
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 840976
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 840977
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 840978
    if-eqz v0, :cond_2

    .line 840979
    const-string v1, "is_messenger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 840980
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 840981
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 840982
    return-void
.end method
