.class public final LX/6Z7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field public final synthetic a:LX/6Z8;


# direct methods
.method public constructor <init>(LX/6Z8;)V
    .locals 0

    .prologue
    .line 1110946
    iput-object p1, p0, LX/6Z7;->a:LX/6Z8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 1110947
    invoke-static {p1}, LX/6Z8;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 1110948
    if-eqz v0, :cond_0

    .line 1110949
    iget-object v1, p0, LX/6Z7;->a:LX/6Z8;

    invoke-virtual {v1, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 1110950
    :cond_0
    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1110951
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1110952
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1110953
    return-void
.end method
