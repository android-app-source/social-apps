.class public LX/6Ee;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;",
        ">;"
    }
.end annotation


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6EF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/6EV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6EW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/6D0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/6Cz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066358
    const-class v0, LX/6Ee;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Ee;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066360
    return-void
.end method

.method public static a$redex0(LX/6Ee;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1066361
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1066362
    :try_start_0
    const-string v0, "error"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066363
    :goto_0
    iget-object v0, p0, LX/6Ee;->h:LX/6EW;

    invoke-virtual {v0, v1}, LX/6EW;->a(Lorg/json/JSONObject;)V

    .line 1066364
    return-void

    .line 1066365
    :catch_0
    move-exception v0

    .line 1066366
    iget-object v2, p0, LX/6Ee;->d:LX/03V;

    sget-object v3, LX/6Ee;->k:Ljava/lang/String;

    const-string v4, "Failed to create error result"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1066367
    const-string v0, "requestUserInfoField"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 6

    .prologue
    .line 1066368
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    .line 1066369
    :try_start_0
    const-string v0, "field"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v0, v0

    .line 1066370
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6D6;->valueOf(Ljava/lang/String;)LX/6D6;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1066371
    sget-object v1, LX/6D6;->USER_MOBILE_PHONE:LX/6D6;

    if-ne v0, v1, :cond_0

    .line 1066372
    iget-object v1, p0, LX/6Ee;->i:LX/0Uh;

    const/16 v2, 0x50f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1066373
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_INVALID_PARAM:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1066374
    :goto_0
    return-void

    .line 1066375
    :catch_0
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_INVALID_PARAM:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    goto :goto_0

    .line 1066376
    :cond_0
    iget-object v1, p0, LX/6Ee;->h:LX/6EW;

    .line 1066377
    iput-object p1, v1, LX/6EW;->a:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    .line 1066378
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1066379
    new-instance v2, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;

    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;-><init>(Ljava/lang/String;)V

    .line 1066380
    const-string v3, "query_permissions_operation_param"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1066381
    iget-object v2, p0, LX/6Ee;->g:LX/6EV;

    const-string v3, "query_permissions_operation_type"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, LX/6EV;->a(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    move-object v1, v1

    .line 1066382
    new-instance v2, LX/6Eb;

    invoke-direct {v2, p0, v0, p1}, LX/6Eb;-><init>(LX/6Ee;LX/6D6;Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;)V

    iget-object v0, p0, LX/6Ee;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
