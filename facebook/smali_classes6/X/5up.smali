.class public LX/5up;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/5up;


# instance fields
.field private final b:LX/0aG;

.field private final c:LX/1Ck;

.field private final d:LX/0tX;

.field private final e:LX/2s3;

.field private final f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1020199
    const-class v0, LX/5up;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/5up;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/1Ck;LX/0tX;LX/2s3;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1020257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020258
    iput-object p1, p0, LX/5up;->b:LX/0aG;

    .line 1020259
    iput-object p2, p0, LX/5up;->c:LX/1Ck;

    .line 1020260
    iput-object p3, p0, LX/5up;->d:LX/0tX;

    .line 1020261
    iput-object p4, p0, LX/5up;->e:LX/2s3;

    .line 1020262
    iput-object p5, p0, LX/5up;->f:LX/03V;

    .line 1020263
    return-void
.end method

.method public static a(LX/0QB;)LX/5up;
    .locals 9

    .prologue
    .line 1020264
    sget-object v0, LX/5up;->g:LX/5up;

    if-nez v0, :cond_1

    .line 1020265
    const-class v1, LX/5up;

    monitor-enter v1

    .line 1020266
    :try_start_0
    sget-object v0, LX/5up;->g:LX/5up;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1020267
    if-eqz v2, :cond_0

    .line 1020268
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1020269
    new-instance v3, LX/5up;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    const-class v7, LX/2s3;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2s3;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/5up;-><init>(LX/0aG;LX/1Ck;LX/0tX;LX/2s3;LX/03V;)V

    .line 1020270
    move-object v0, v3

    .line 1020271
    sput-object v0, LX/5up;->g:LX/5up;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1020272
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1020273
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1020274
    :cond_1
    sget-object v0, LX/5up;->g:LX/5up;

    return-object v0

    .line 1020275
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1020276
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V
    .locals 4

    .prologue
    .line 1020284
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1020285
    const-string v1, "update_saved_state_params"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1020286
    iget-object v1, p0, LX/5up;->b:LX/0aG;

    const-string v2, "update_saved_state"

    const v3, 0x5e4ac8b2

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1020287
    iget-object v1, p0, LX/5up;->c:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_update_item_saved_state"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1020288
    iget-object v3, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    move-object v3, v3

    .line 1020289
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/52F;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1020290
    return-void
.end method


# virtual methods
.method public final a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020277
    new-instance v0, LX/5un;

    .line 1020278
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1020279
    invoke-direct {v0, p1, p3, p4, v1}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020280
    iput-object v1, v0, LX/5un;->d:LX/0am;

    .line 1020281
    move-object v0, v0

    .line 1020282
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p5}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020283
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ZLjava/lang/String;Ljava/lang/String;LX/2h1;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/2h1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1020235
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1020236
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1020237
    const v1, -0x3625f733

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1020238
    if-nez v1, :cond_1

    .line 1020239
    const/4 v1, 0x0

    .line 1020240
    :goto_0
    move-object v1, v1

    .line 1020241
    if-nez v1, :cond_0

    .line 1020242
    iget-object v1, p0, LX/5up;->f:LX/03V;

    sget-object v2, LX/5up;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "There is no actionLink in story "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1020243
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1020244
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020245
    :goto_1
    return-void

    .line 1020246
    :cond_0
    iget-object v6, p0, LX/5up;->d:LX/0tX;

    iget-object v0, p0, LX/5up;->e:LX/2s3;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/2s3;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/saved/server/SaveStoryLegacyMutation;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0tX;->a(LX/4V2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1020247
    iget-object v2, p0, LX/5up;->c:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "task_key_update_item_saved_state"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1020248
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1020249
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, LX/52F;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-virtual {v2, v0, v1, p6}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020250
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->ARCHIVE:LX/5uo;

    .line 1020251
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020252
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020253
    iput-object v1, v0, LX/5un;->d:LX/0am;

    .line 1020254
    move-object v0, v0

    .line 1020255
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020256
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020228
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->UNARCHIVE:LX/5uo;

    .line 1020229
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020230
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020231
    iput-object v1, v0, LX/5un;->d:LX/0am;

    .line 1020232
    move-object v0, v0

    .line 1020233
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020234
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020221
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    .line 1020222
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020223
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020224
    iput-object v1, v0, LX/5un;->d:LX/0am;

    .line 1020225
    move-object v0, v0

    .line 1020226
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020227
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020214
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->REMOVE_FROM_ARCHIVE:LX/5uo;

    .line 1020215
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020216
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020217
    iput-object v1, v0, LX/5un;->d:LX/0am;

    .line 1020218
    move-object v0, v0

    .line 1020219
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020220
    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020207
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    .line 1020208
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020209
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020210
    iput-object v1, v0, LX/5un;->c:LX/0am;

    .line 1020211
    move-object v0, v0

    .line 1020212
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020213
    return-void
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1020200
    new-instance v0, LX/5un;

    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    .line 1020201
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1020202
    invoke-direct {v0, v1, p2, p3, v2}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1020203
    iput-object v1, v0, LX/5un;->c:LX/0am;

    .line 1020204
    move-object v0, v0

    .line 1020205
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LX/5up;->a(Lcom/facebook/saved/server/UpdateSavedStateParams;LX/2h0;)V

    .line 1020206
    return-void
.end method
