.class public LX/6AC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058703
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6AC;->a:Ljava/lang/String;

    .line 1058704
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6AC;->b:LX/0Px;

    .line 1058705
    iget-object v0, p0, LX/6AC;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1058706
    return-void

    .line 1058707
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058708
    iget-object v0, p0, LX/6AC;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 6

    .prologue
    .line 1058709
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    const/4 v2, 0x0

    .line 1058710
    iget-object v0, p0, LX/6AC;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1058711
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1058712
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v1

    .line 1058713
    :goto_1
    if-nez v0, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v3, v1

    :cond_1
    move v0, v3

    .line 1058714
    if-nez v0, :cond_3

    .line 1058715
    :cond_2
    :goto_2
    return-void

    .line 1058716
    :cond_3
    iget-object v0, p0, LX/6AC;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_3
    if-ge v1, v3, :cond_4

    iget-object v0, p0, LX/6AC;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 1058717
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {p1, v4, v5}, LX/6Ov;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 1058718
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v2}, LX/6Ov;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    .line 1058719
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1058720
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4XS;->a(LX/0Px;)V

    .line 1058721
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4XS;->e(LX/0Px;)V

    goto :goto_2

    :cond_5
    move v0, v3

    .line 1058722
    goto :goto_0

    :cond_6
    move v4, v3

    .line 1058723
    goto :goto_1
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058724
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058725
    const-string v0, "SocialSearchDeletePlaceRecommendationCommentUpdateMutatingVisitor"

    return-object v0
.end method
