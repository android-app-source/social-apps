.class public final LX/53P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static final c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/53P;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/52r;

.field public volatile b:I

.field public final d:LX/53n;

.field public volatile e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 827152
    const-class v0, LX/53P;

    const-string v1, "b"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53P;->c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/52r;LX/53n;)V
    .locals 1

    .prologue
    .line 827153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827154
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/53P;->e:Z

    .line 827155
    iput-object p1, p0, LX/53P;->a:LX/52r;

    .line 827156
    iput-object p2, p0, LX/53P;->d:LX/53n;

    .line 827157
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 827158
    sget-object v0, LX/53P;->c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndSet(Ljava/lang/Object;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 827159
    iget-object v0, p0, LX/53P;->a:LX/52r;

    new-instance v1, LX/53O;

    invoke-direct {v1, p0}, LX/53O;-><init>(LX/53P;)V

    invoke-virtual {v0, v1}, LX/52r;->a(LX/0vR;)LX/0za;

    .line 827160
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827161
    iget-boolean v0, p0, LX/53P;->e:Z

    return v0
.end method
