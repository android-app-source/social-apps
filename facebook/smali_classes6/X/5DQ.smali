.class public final LX/5DQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 871602
    const/4 v9, 0x0

    .line 871603
    const/4 v8, 0x0

    .line 871604
    const/4 v7, 0x0

    .line 871605
    const/4 v6, 0x0

    .line 871606
    const/4 v5, 0x0

    .line 871607
    const/4 v4, 0x0

    .line 871608
    const/4 v3, 0x0

    .line 871609
    const/4 v2, 0x0

    .line 871610
    const/4 v1, 0x0

    .line 871611
    const/4 v0, 0x0

    .line 871612
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 871613
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 871614
    const/4 v0, 0x0

    .line 871615
    :goto_0
    return v0

    .line 871616
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 871617
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 871618
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 871619
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 871620
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 871621
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 871622
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 871623
    :cond_2
    const-string v11, "important_reactors"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 871624
    invoke-static {p0, p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 871625
    :cond_3
    const-string v11, "legacy_api_post_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 871626
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 871627
    :cond_4
    const-string v11, "likers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 871628
    invoke-static {p0, p1}, LX/5DN;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 871629
    :cond_5
    const-string v11, "reactors"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 871630
    invoke-static {p0, p1}, LX/5DO;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 871631
    :cond_6
    const-string v11, "top_reactions"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 871632
    invoke-static {p0, p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 871633
    :cond_7
    const-string v11, "viewer_acts_as_person"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 871634
    invoke-static {p0, p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 871635
    :cond_8
    const-string v11, "viewer_feedback_reaction"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 871636
    invoke-static {p0, p1}, LX/5DP;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 871637
    :cond_9
    const-string v11, "viewer_feedback_reaction_key"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 871638
    const/4 v0, 0x1

    .line 871639
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    goto/16 :goto_1

    .line 871640
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 871641
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 871642
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 871643
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 871644
    const/4 v7, 0x3

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 871645
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 871646
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 871647
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 871648
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 871649
    if-eqz v0, :cond_b

    .line 871650
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 871651
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 871652
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 871653
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 871654
    if-eqz v0, :cond_0

    .line 871655
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871656
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 871657
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871658
    if-eqz v0, :cond_1

    .line 871659
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871660
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 871661
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 871662
    if-eqz v0, :cond_2

    .line 871663
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871664
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 871665
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871666
    if-eqz v0, :cond_3

    .line 871667
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871668
    invoke-static {p0, v0, p2}, LX/5DN;->a(LX/15i;ILX/0nX;)V

    .line 871669
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871670
    if-eqz v0, :cond_4

    .line 871671
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871672
    invoke-static {p0, v0, p2}, LX/5DO;->a(LX/15i;ILX/0nX;)V

    .line 871673
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871674
    if-eqz v0, :cond_5

    .line 871675
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871676
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 871677
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871678
    if-eqz v0, :cond_6

    .line 871679
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871680
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 871681
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 871682
    if-eqz v0, :cond_7

    .line 871683
    const-string v1, "viewer_feedback_reaction"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871684
    invoke-static {p0, v0, p2}, LX/5DP;->a(LX/15i;ILX/0nX;)V

    .line 871685
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 871686
    if-eqz v0, :cond_8

    .line 871687
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871688
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 871689
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 871690
    return-void
.end method
