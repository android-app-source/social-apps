.class public LX/6Bf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field public final b:LX/0SG;

.field private final c:LX/0So;

.field private final d:LX/6Bh;

.field private final e:LX/6Bs;

.field private final f:LX/2IT;

.field private final g:Landroid/net/ConnectivityManager;

.field private final h:LX/1Er;

.field private final i:LX/30H;

.field private final j:LX/03V;

.field private final k:LX/0Sh;

.field private final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/6Bn;

.field public final n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/6BZ;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/6Be;

.field public p:LX/6Bc;

.field public q:LX/6Bc;

.field private r:LX/6Bd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1063021
    const-class v0, LX/6Bf;

    sput-object v0, LX/6Bf;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0So;LX/6Bh;LX/6Bs;LX/2IT;Landroid/net/ConnectivityManager;LX/1Er;LX/30H;LX/03V;LX/0Sh;Ljava/util/Set;LX/6Bn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0So;",
            "LX/6Bh;",
            "LX/6Bs;",
            "LX/2IT;",
            "Landroid/net/ConnectivityManager;",
            "LX/1Er;",
            "LX/30H;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadHandler;",
            ">;",
            "LX/6Bn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1062903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062904
    iput-object p1, p0, LX/6Bf;->b:LX/0SG;

    .line 1062905
    iput-object p2, p0, LX/6Bf;->c:LX/0So;

    .line 1062906
    iput-object p3, p0, LX/6Bf;->d:LX/6Bh;

    .line 1062907
    iput-object p4, p0, LX/6Bf;->e:LX/6Bs;

    .line 1062908
    iput-object p5, p0, LX/6Bf;->f:LX/2IT;

    .line 1062909
    iput-object p6, p0, LX/6Bf;->g:Landroid/net/ConnectivityManager;

    .line 1062910
    iput-object p7, p0, LX/6Bf;->h:LX/1Er;

    .line 1062911
    iput-object p8, p0, LX/6Bf;->i:LX/30H;

    .line 1062912
    iput-object p9, p0, LX/6Bf;->j:LX/03V;

    .line 1062913
    iput-object p10, p0, LX/6Bf;->k:LX/0Sh;

    .line 1062914
    iput-object p11, p0, LX/6Bf;->l:Ljava/util/Set;

    .line 1062915
    iput-object p12, p0, LX/6Bf;->m:LX/6Bn;

    .line 1062916
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    .line 1062917
    invoke-virtual {p0}, LX/6Bf;->b()V

    .line 1062918
    return-void
.end method

.method private static a(LX/6Bf;Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Ljava/io/File;
    .locals 4

    .prologue
    .line 1063018
    iget-object v0, p0, LX/6Bf;->h:LX/1Er;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "asset_tmp_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1063019
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v2, v2

    .line 1063020
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".tmp"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/6Bf;Ljava/lang/String;Ljava/lang/Exception;LX/6Bp;)V
    .locals 4

    .prologue
    .line 1063009
    iget-object v0, p0, LX/6Bf;->j:LX/03V;

    const-string v1, "currentRequest=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1063010
    iget-object v0, p0, LX/6Bf;->f:LX/2IT;

    .line 1063011
    iget-object v1, p3, LX/6Bp;->a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-object v1, v1

    .line 1063012
    iget-object v2, v1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v1, v2

    .line 1063013
    iget-object v2, p0, LX/6Bf;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/2IT;->b(Ljava/lang/String;J)Z

    .line 1063014
    iget-object v0, p0, LX/6Bf;->r:LX/6Bd;

    if-eqz v0, :cond_0

    .line 1063015
    iget-object v0, p0, LX/6Bf;->r:LX/6Bd;

    .line 1063016
    iget v1, v0, LX/6Bd;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/6Bd;->b:I

    .line 1063017
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 1063003
    iget-object v0, p0, LX/6Bf;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yK;

    .line 1063004
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v2, v2

    .line 1063005
    const-string p0, "FaceDetectionAssets"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 1063006
    if-eqz v2, :cond_0

    .line 1063007
    invoke-virtual {v0, p2}, LX/7yK;->a(Ljava/io/File;)V

    goto :goto_0

    .line 1063008
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/6Bf;
    .locals 14

    .prologue
    .line 1062993
    new-instance v0, LX/6Bf;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    .line 1062994
    new-instance v5, LX/6Bh;

    invoke-static {p0}, LX/2IT;->b(LX/0QB;)LX/2IT;

    move-result-object v3

    check-cast v3, LX/2IT;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v3, v4}, LX/6Bh;-><init>(LX/2IT;LX/0SG;)V

    .line 1062995
    move-object v3, v5

    .line 1062996
    check-cast v3, LX/6Bh;

    .line 1062997
    new-instance v6, LX/6Bs;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {p0}, LX/6Bo;->a(LX/0QB;)LX/6Bo;

    move-result-object v5

    check-cast v5, LX/6Bo;

    invoke-direct {v6, v4, v5}, LX/6Bs;-><init>(LX/0TD;LX/6Bo;)V

    .line 1062998
    move-object v4, v6

    .line 1062999
    check-cast v4, LX/6Bs;

    invoke-static {p0}, LX/2IT;->b(LX/0QB;)LX/2IT;

    move-result-object v5

    check-cast v5, LX/2IT;

    invoke-static {p0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v7

    check-cast v7, LX/1Er;

    invoke-static {p0}, LX/30H;->b(LX/0QB;)LX/30H;

    move-result-object v8

    check-cast v8, LX/30H;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    .line 1063000
    new-instance v11, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v12

    new-instance v13, LX/6BY;

    invoke-direct {v13, p0}, LX/6BY;-><init>(LX/0QB;)V

    invoke-direct {v11, v12, v13}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v11, v11

    .line 1063001
    invoke-static {p0}, LX/6Bn;->a(LX/0QB;)LX/6Bn;

    move-result-object v12

    check-cast v12, LX/6Bn;

    invoke-direct/range {v0 .. v12}, LX/6Bf;-><init>(LX/0SG;LX/0So;LX/6Bh;LX/6Bs;LX/2IT;Landroid/net/ConnectivityManager;LX/1Er;LX/30H;LX/03V;LX/0Sh;Ljava/util/Set;LX/6Bn;)V

    .line 1063002
    return-object v0
.end method

.method private static c(LX/6Bf;)V
    .locals 2

    .prologue
    .line 1063022
    iget-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6BZ;

    .line 1063023
    invoke-interface {v0}, LX/6BZ;->b()V

    goto :goto_0

    .line 1063024
    :cond_0
    return-void
.end method

.method private static d(LX/6Bf;)Z
    .locals 2

    .prologue
    .line 1062989
    iget-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6BZ;

    .line 1062990
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/6BZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1062991
    const/4 v0, 0x0

    .line 1062992
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static e(LX/6Bf;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1062987
    iget-object v1, p0, LX/6Bf;->g:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1062988
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1062982
    iget-object v0, p0, LX/6Bf;->r:LX/6Bd;

    if-eqz v0, :cond_0

    .line 1062983
    iget-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    iget-object v1, p0, LX/6Bf;->r:LX/6Bd;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1062984
    :cond_0
    new-instance v0, LX/6Bd;

    invoke-direct {v0, p1}, LX/6Bd;-><init>(I)V

    iput-object v0, p0, LX/6Bf;->r:LX/6Bd;

    .line 1062985
    iget-object v0, p0, LX/6Bf;->r:LX/6Bd;

    invoke-virtual {p0, v0}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062986
    return-void
.end method

.method public final a(LX/6BZ;)V
    .locals 1

    .prologue
    .line 1062980
    iget-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1062981
    return-void
.end method

.method public final a()Z
    .locals 22

    .prologue
    .line 1062919
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->k:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 1062920
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->c:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v16

    .line 1062921
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->i:LX/30H;

    invoke-virtual {v4}, LX/30H;->a()LX/0Px;

    .line 1062922
    invoke-static/range {p0 .. p0}, LX/6Bf;->c(LX/6Bf;)V

    .line 1062923
    const/4 v4, 0x0

    .line 1062924
    const/4 v9, 0x0

    .line 1062925
    const-wide/16 v10, 0x0

    move-wide v14, v10

    .line 1062926
    :goto_0
    invoke-static/range {p0 .. p0}, LX/6Bf;->d(LX/6Bf;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1062927
    move-object/from16 v0, p0

    iget-object v5, v0, LX/6Bf;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    .line 1062928
    invoke-static/range {p0 .. p0}, LX/6Bf;->e(LX/6Bf;)Z

    move-result v10

    .line 1062929
    move-object/from16 v0, p0

    iget-object v5, v0, LX/6Bf;->d:LX/6Bh;

    invoke-static/range {p0 .. p0}, LX/6Bf;->e(LX/6Bf;)Z

    move-result v8

    invoke-virtual {v5, v8}, LX/6Bh;->a(Z)Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-result-object v5

    .line 1062930
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1062931
    if-eqz v5, :cond_7

    .line 1062932
    invoke-virtual {v5}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a()Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_0

    .line 1062933
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Bf;->j:LX/03V;

    const-string v7, "assetdownload_runner_source_null"

    const-string v8, "currentConfig: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-static {v8, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062934
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Bf;->f:LX/2IT;

    invoke-virtual {v5}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Bf;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v10

    invoke-virtual {v6, v7, v10, v11}, LX/2IT;->b(Ljava/lang/String;J)Z

    .line 1062935
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Bf;->d:LX/6Bh;

    invoke-virtual {v6, v5}, LX/6Bh;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    goto :goto_0

    .line 1062936
    :cond_0
    add-int/lit8 v13, v4, 0x1

    .line 1062937
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->i:LX/30H;

    invoke-virtual {v4, v5}, LX/30H;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1062938
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->f:LX/2IT;

    invoke-virtual {v5}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/6Bf;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v10

    invoke-virtual {v4, v6, v10, v11}, LX/2IT;->a(Ljava/lang/String;J)Z

    .line 1062939
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->d:LX/6Bh;

    invoke-virtual {v4, v5}, LX/6Bh;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    move v4, v13

    .line 1062940
    goto :goto_0

    .line 1062941
    :cond_1
    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/6Bf;->a(LX/6Bf;Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Ljava/io/File;

    move-result-object v11

    .line 1062942
    new-instance v12, LX/6Bp;

    invoke-direct {v12, v5, v11}, LX/6Bp;-><init>(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;)V

    .line 1062943
    if-nez v11, :cond_2

    .line 1062944
    const-string v4, "assetdownload_runner_tempfile_null"

    new-instance v5, Ljava/io/IOException;

    const-string v6, "tempFile is null"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v12}, LX/6Bf;->a(LX/6Bf;Ljava/lang/String;Ljava/lang/Exception;LX/6Bp;)V

    move v8, v13

    .line 1062945
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-static/range {p0 .. p0}, LX/6Bf;->e(LX/6Bf;)Z

    move-result v12

    move-wide/from16 v5, v16

    move-wide v10, v14

    invoke-virtual/range {v4 .. v12}, LX/6Bn;->a(JLjava/util/HashSet;IIJZ)V

    .line 1062946
    const/4 v4, 0x1

    :goto_2
    return v4

    .line 1062947
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->e:LX/6Bs;

    invoke-virtual {v4, v12}, LX/6Bs;->a(LX/6Bp;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1062948
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Bf;->d:LX/6Bh;

    invoke-virtual {v8, v5}, LX/6Bh;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    .line 1062949
    const-wide/16 v18, 0x7530

    :try_start_0
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v20, -0x640f7c6a

    move-wide/from16 v0, v18

    move/from16 v2, v20

    invoke-static {v4, v0, v1, v8, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, LX/6Bq;

    move-object v8, v0

    .line 1062950
    invoke-static/range {p0 .. p0}, LX/6Bf;->e(LX/6Bf;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->p:LX/6Bc;

    if-eqz v4, :cond_4

    .line 1062951
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->p:LX/6Bc;

    invoke-virtual {v8}, LX/6Bq;->a()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, LX/6Bc;->a(J)V

    .line 1062952
    :cond_3
    :goto_3
    invoke-virtual {v8}, LX/6Bq;->a()J

    move-result-wide v18

    add-long v14, v14, v18

    .line 1062953
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->i:LX/30H;

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v4, v5, v11, v0}, LX/30H;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;Z)Ljava/io/File;

    move-result-object v4

    .line 1062954
    if-nez v4, :cond_5

    .line 1062955
    new-instance v4, Ljava/util/concurrent/ExecutionException;

    new-instance v8, Ljava/io/IOException;

    const-string v11, "copyToLocalFile() was not successful"

    invoke-direct {v8, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v8}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1062956
    :catch_0
    move-exception v8

    move v11, v9

    .line 1062957
    :goto_4
    const-string v4, "assetdownload_runner_interrupted_exception"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v8, v12}, LX/6Bf;->a(LX/6Bf;Ljava/lang/String;Ljava/lang/Exception;LX/6Bp;)V

    .line 1062958
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    move v9, v10

    invoke-virtual/range {v4 .. v9}, LX/6Bn;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JLjava/lang/Exception;Z)V

    move v9, v11

    move-wide v10, v14

    .line 1062959
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->r:LX/6Bd;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->r:LX/6Bd;

    invoke-virtual {v4}, LX/6Bd;->a()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1062960
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-static/range {p0 .. p0}, LX/6Bf;->e(LX/6Bf;)Z

    move-result v12

    move-wide/from16 v5, v16

    move v8, v13

    invoke-virtual/range {v4 .. v12}, LX/6Bn;->a(JLjava/util/HashSet;IIJZ)V

    .line 1062961
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1062962
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->q:LX/6Bc;

    if-eqz v4, :cond_3

    .line 1062963
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->q:LX/6Bc;

    invoke-virtual {v8}, LX/6Bq;->a()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, LX/6Bc;->a(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_3

    .line 1062964
    :catch_1
    move-exception v8

    move v11, v9

    .line 1062965
    :goto_6
    const-string v4, "assetdownload_runner_execution_exception"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v8, v12}, LX/6Bf;->a(LX/6Bf;Ljava/lang/String;Ljava/lang/Exception;LX/6Bp;)V

    .line 1062966
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    move v9, v10

    invoke-virtual/range {v4 .. v9}, LX/6Bn;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JLjava/lang/Exception;Z)V

    move v9, v11

    move-wide v10, v14

    .line 1062967
    goto :goto_5

    .line 1062968
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, LX/6Bf;->f:LX/2IT;

    invoke-virtual {v5}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, LX/6Bf;->b:LX/0SG;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, LX/0SG;->a()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v11, v0, v1, v2}, LX/2IT;->a(Ljava/lang/String;J)Z

    .line 1062969
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, LX/6Bf;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1062970
    add-int/lit8 v11, v9, 0x1

    .line 1062971
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    invoke-virtual {v8}, LX/6Bq;->a()J

    move-result-wide v8

    invoke-virtual/range {v4 .. v10}, LX/6Bn;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JJZ)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_3

    move v9, v11

    move-wide v10, v14

    .line 1062972
    goto :goto_5

    .line 1062973
    :catch_2
    move-exception v8

    move v11, v9

    .line 1062974
    :goto_7
    const-string v4, "assetdownload_runner_timeout_exception"

    move-object/from16 v0, p0

    invoke-static {v0, v4, v8, v12}, LX/6Bf;->a(LX/6Bf;Ljava/lang/String;Ljava/lang/Exception;LX/6Bp;)V

    .line 1062975
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Bf;->m:LX/6Bn;

    move v9, v10

    invoke-virtual/range {v4 .. v9}, LX/6Bn;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JLjava/lang/Exception;Z)V

    move v9, v11

    move-wide v10, v14

    goto/16 :goto_5

    :cond_6
    move-wide v14, v10

    move v4, v13

    .line 1062976
    goto/16 :goto_0

    .line 1062977
    :catch_3
    move-exception v8

    goto :goto_7

    .line 1062978
    :catch_4
    move-exception v8

    goto :goto_6

    .line 1062979
    :catch_5
    move-exception v8

    goto/16 :goto_4

    :cond_7
    move v8, v4

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1062896
    iget-object v0, p0, LX/6Bf;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1062897
    iput-object v1, p0, LX/6Bf;->o:LX/6Be;

    .line 1062898
    iput-object v1, p0, LX/6Bf;->p:LX/6Bc;

    .line 1062899
    iput-object v1, p0, LX/6Bf;->q:LX/6Bc;

    .line 1062900
    iput-object v1, p0, LX/6Bf;->r:LX/6Bd;

    .line 1062901
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6Bf;->a(I)V

    .line 1062902
    return-void
.end method
