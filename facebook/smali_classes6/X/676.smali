.class public final LX/676;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field private a:I

.field public final b:LX/671;

.field public final c:Ljava/util/zip/Inflater;

.field private final d:LX/677;

.field public final e:Ljava/util/zip/CRC32;


# direct methods
.method public constructor <init>(LX/65D;)V
    .locals 3

    .prologue
    .line 1052248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052249
    const/4 v0, 0x0

    iput v0, p0, LX/676;->a:I

    .line 1052250
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, LX/676;->e:Ljava/util/zip/CRC32;

    .line 1052251
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052252
    :cond_0
    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, p0, LX/676;->c:Ljava/util/zip/Inflater;

    .line 1052253
    invoke-static {p1}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    iput-object v0, p0, LX/676;->b:LX/671;

    .line 1052254
    new-instance v0, LX/677;

    iget-object v1, p0, LX/676;->b:LX/671;

    iget-object v2, p0, LX/676;->c:Ljava/util/zip/Inflater;

    invoke-direct {v0, v1, v2}, LX/677;-><init>(LX/671;Ljava/util/zip/Inflater;)V

    iput-object v0, p0, LX/676;->d:LX/677;

    .line 1052255
    return-void
.end method

.method private static a(LX/676;LX/672;JJ)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1052237
    iget-object v0, p1, LX/672;->a:LX/67F;

    .line 1052238
    :goto_0
    iget v1, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    cmp-long v1, p2, v4

    if-ltz v1, :cond_0

    .line 1052239
    iget v1, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    sub-long/2addr p2, v4

    .line 1052240
    iget-object v0, v0, LX/67F;->f:LX/67F;

    goto :goto_0

    .line 1052241
    :cond_0
    :goto_1
    cmp-long v1, p4, v2

    if-lez v1, :cond_1

    .line 1052242
    iget v1, v0, LX/67F;->b:I

    int-to-long v4, v1

    add-long/2addr v4, p2

    long-to-int v1, v4

    .line 1052243
    iget v4, v0, LX/67F;->c:I

    sub-int/2addr v4, v1

    int-to-long v4, v4

    invoke-static {v4, v5, p4, p5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    .line 1052244
    iget-object v5, p0, LX/676;->e:Ljava/util/zip/CRC32;

    iget-object v6, v0, LX/67F;->a:[B

    invoke-virtual {v5, v6, v1, v4}, Ljava/util/zip/CRC32;->update([BII)V

    .line 1052245
    int-to-long v4, v4

    sub-long/2addr p4, v4

    .line 1052246
    iget-object v0, v0, LX/67F;->f:LX/67F;

    move-wide p2, v2

    goto :goto_1

    .line 1052247
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/String;II)V
    .locals 5

    .prologue
    .line 1052232
    if-eq p2, p1, :cond_0

    .line 1052233
    new-instance v0, Ljava/io/IOException;

    const-string v1, "%s: actual 0x%08x != expected 0x%08x"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    .line 1052234
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1052235
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052236
    :cond_0
    return-void
.end method

.method private b()V
    .locals 10

    .prologue
    .line 1052203
    iget-object v0, p0, LX/676;->b:LX/671;

    const-wide/16 v2, 0xa

    invoke-interface {v0, v2, v3}, LX/671;->a(J)V

    .line 1052204
    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v0

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/672;->b(J)B

    move-result v7

    .line 1052205
    shr-int/lit8 v0, v7, 0x1

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    move v6, v0

    .line 1052206
    :goto_0
    if-eqz v6, :cond_0

    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    .line 1052207
    :cond_0
    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->i()S

    move-result v0

    .line 1052208
    const-string v1, "ID1ID2"

    const/16 v2, 0x1f8b

    invoke-static {v1, v2, v0}, LX/676;->a(Ljava/lang/String;II)V

    .line 1052209
    iget-object v0, p0, LX/676;->b:LX/671;

    const-wide/16 v2, 0x8

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    .line 1052210
    shr-int/lit8 v0, v7, 0x2

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1052211
    iget-object v0, p0, LX/676;->b:LX/671;

    const-wide/16 v2, 0x2

    invoke-interface {v0, v2, v3}, LX/671;->a(J)V

    .line 1052212
    if-eqz v6, :cond_1

    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    .line 1052213
    :cond_1
    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v0

    invoke-virtual {v0}, LX/672;->l()S

    move-result v8

    .line 1052214
    iget-object v0, p0, LX/676;->b:LX/671;

    int-to-long v2, v8

    invoke-interface {v0, v2, v3}, LX/671;->a(J)V

    .line 1052215
    if-eqz v6, :cond_2

    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v1

    const-wide/16 v2, 0x0

    int-to-long v4, v8

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    .line 1052216
    :cond_2
    iget-object v0, p0, LX/676;->b:LX/671;

    int-to-long v2, v8

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    .line 1052217
    :cond_3
    shr-int/lit8 v0, v7, 0x3

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 1052218
    iget-object v0, p0, LX/676;->b:LX/671;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/671;->a(B)J

    move-result-wide v8

    .line 1052219
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1052220
    :cond_4
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    .line 1052221
    :cond_5
    if-eqz v6, :cond_6

    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v8

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    .line 1052222
    :cond_6
    iget-object v0, p0, LX/676;->b:LX/671;

    const-wide/16 v2, 0x1

    add-long/2addr v2, v8

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    .line 1052223
    :cond_7
    shr-int/lit8 v0, v7, 0x4

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 1052224
    iget-object v0, p0, LX/676;->b:LX/671;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/671;->a(B)J

    move-result-wide v8

    .line 1052225
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1052226
    :cond_8
    if-eqz v6, :cond_9

    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v8

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    .line 1052227
    :cond_9
    iget-object v0, p0, LX/676;->b:LX/671;

    const-wide/16 v2, 0x1

    add-long/2addr v2, v8

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    .line 1052228
    :cond_a
    if-eqz v6, :cond_b

    .line 1052229
    const-string v0, "FHCRC"

    iget-object v1, p0, LX/676;->b:LX/671;

    invoke-interface {v1}, LX/671;->l()S

    move-result v1

    iget-object v2, p0, LX/676;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, LX/676;->a(Ljava/lang/String;II)V

    .line 1052230
    iget-object v0, p0, LX/676;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->reset()V

    .line 1052231
    :cond_b
    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 12

    .prologue
    const-wide/16 v0, -0x1

    const/4 v7, 0x2

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    .line 1052184
    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052185
    :cond_0
    cmp-long v2, p2, v4

    if-nez v2, :cond_1

    .line 1052186
    :goto_0
    return-wide v4

    .line 1052187
    :cond_1
    iget v2, p0, LX/676;->a:I

    if-nez v2, :cond_2

    .line 1052188
    invoke-direct {p0}, LX/676;->b()V

    .line 1052189
    iput v3, p0, LX/676;->a:I

    .line 1052190
    :cond_2
    iget v2, p0, LX/676;->a:I

    if-ne v2, v3, :cond_4

    .line 1052191
    iget-wide v2, p1, LX/672;->b:J

    .line 1052192
    iget-object v4, p0, LX/676;->d:LX/677;

    invoke-virtual {v4, p1, p2, p3}, LX/677;->a(LX/672;J)J

    move-result-wide v4

    .line 1052193
    cmp-long v6, v4, v0

    if-eqz v6, :cond_3

    move-object v0, p0

    move-object v1, p1

    .line 1052194
    invoke-static/range {v0 .. v5}, LX/676;->a(LX/676;LX/672;JJ)V

    goto :goto_0

    .line 1052195
    :cond_3
    iput v7, p0, LX/676;->a:I

    .line 1052196
    :cond_4
    iget v2, p0, LX/676;->a:I

    if-ne v2, v7, :cond_5

    .line 1052197
    const-string v8, "CRC"

    iget-object v9, p0, LX/676;->b:LX/671;

    invoke-interface {v9}, LX/671;->m()I

    move-result v9

    iget-object v10, p0, LX/676;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v10}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v10

    long-to-int v10, v10

    invoke-static {v8, v9, v10}, LX/676;->a(Ljava/lang/String;II)V

    .line 1052198
    const-string v8, "ISIZE"

    iget-object v9, p0, LX/676;->b:LX/671;

    invoke-interface {v9}, LX/671;->m()I

    move-result v9

    iget-object v10, p0, LX/676;->c:Ljava/util/zip/Inflater;

    invoke-virtual {v10}, Ljava/util/zip/Inflater;->getTotalOut()I

    move-result v10

    invoke-static {v8, v9, v10}, LX/676;->a(Ljava/lang/String;II)V

    .line 1052199
    const/4 v2, 0x3

    iput v2, p0, LX/676;->a:I

    .line 1052200
    iget-object v2, p0, LX/676;->b:LX/671;

    invoke-interface {v2}, LX/671;->e()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1052201
    new-instance v0, Ljava/io/IOException;

    const-string v1, "gzip finished without exhausting source"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-wide v4, v0

    .line 1052202
    goto :goto_0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052183
    iget-object v0, p0, LX/676;->b:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052181
    iget-object v0, p0, LX/676;->d:LX/677;

    invoke-virtual {v0}, LX/677;->close()V

    .line 1052182
    return-void
.end method
