.class public final LX/5dr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5di",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;
    .locals 8

    .prologue
    .line 966196
    new-instance v1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;

    const-string v0, "name"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "confidence"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->f(LX/0lF;)F

    move-result v3

    const-string v0, "amount"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v4

    const-string v0, "currency"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;-><init>(Ljava/lang/String;FJLjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966197
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966198
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;

    return-object v0
.end method
