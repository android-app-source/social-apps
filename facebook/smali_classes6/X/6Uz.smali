.class public final enum LX/6Uz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Uz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Uz;

.field public static final enum ALL:LX/6Uz;

.field public static final enum IMMEDIATE_CHILDREN_ONLY:LX/6Uz;

.field public static final enum NONE:LX/6Uz;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103265
    new-instance v0, LX/6Uz;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/6Uz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Uz;->NONE:LX/6Uz;

    .line 1103266
    new-instance v0, LX/6Uz;

    const-string v1, "IMMEDIATE_CHILDREN_ONLY"

    invoke-direct {v0, v1, v3}, LX/6Uz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Uz;->IMMEDIATE_CHILDREN_ONLY:LX/6Uz;

    .line 1103267
    new-instance v0, LX/6Uz;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, LX/6Uz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Uz;->ALL:LX/6Uz;

    .line 1103268
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Uz;

    sget-object v1, LX/6Uz;->NONE:LX/6Uz;

    aput-object v1, v0, v2

    sget-object v1, LX/6Uz;->IMMEDIATE_CHILDREN_ONLY:LX/6Uz;

    aput-object v1, v0, v3

    sget-object v1, LX/6Uz;->ALL:LX/6Uz;

    aput-object v1, v0, v4

    sput-object v0, LX/6Uz;->$VALUES:[LX/6Uz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103269
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Uz;
    .locals 1

    .prologue
    .line 1103270
    const-class v0, LX/6Uz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Uz;

    return-object v0
.end method

.method public static values()[LX/6Uz;
    .locals 1

    .prologue
    .line 1103271
    sget-object v0, LX/6Uz;->$VALUES:[LX/6Uz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Uz;

    return-object v0
.end method
