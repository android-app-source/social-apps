.class public final LX/5GM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 887770
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 887771
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887772
    :goto_0
    return v1

    .line 887773
    :cond_0
    const-string v8, "viewer_has_voted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 887774
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    .line 887775
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 887776
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 887777
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 887778
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 887779
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 887780
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 887781
    :cond_2
    const-string v8, "text_with_entities"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 887782
    const/4 v7, 0x0

    .line 887783
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_b

    .line 887784
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887785
    :goto_2
    move v5, v7

    .line 887786
    goto :goto_1

    .line 887787
    :cond_3
    const-string v8, "voters"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 887788
    invoke-static {p0, p1}, LX/5GL;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 887789
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 887790
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 887791
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 887792
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 887793
    if-eqz v0, :cond_6

    .line 887794
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 887795
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 887796
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1

    .line 887797
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887798
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 887799
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 887800
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 887801
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_9

    if-eqz v8, :cond_9

    .line 887802
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 887803
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 887804
    :cond_a
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 887805
    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 887806
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_2

    :cond_b
    move v5, v7

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 887807
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887808
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887809
    if-eqz v0, :cond_0

    .line 887810
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887811
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887812
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887813
    if-eqz v0, :cond_2

    .line 887814
    const-string v1, "text_with_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887815
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887816
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 887817
    if-eqz v1, :cond_1

    .line 887818
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887819
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887820
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887821
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 887822
    if-eqz v0, :cond_3

    .line 887823
    const-string v1, "viewer_has_voted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887824
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 887825
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887826
    if-eqz v0, :cond_4

    .line 887827
    const-string v1, "voters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887828
    invoke-static {p0, v0, p2, p3}, LX/5GL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 887829
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887830
    return-void
.end method
