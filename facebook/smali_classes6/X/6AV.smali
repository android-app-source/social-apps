.class public final LX/6AV;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1059335
    const-class v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;

    const v0, 0x52e91c9a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "StaticSimpleFeedbackMediaQueryWithAttribution"

    const-string v6, "1ffd7130a9f5ffd15eaca64f1ae65ac1"

    const-string v7, "node"

    const-string v8, "10155207369131729"

    const-string v9, "10155259090221729"

    .line 1059336
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1059337
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1059338
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1059320
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1059321
    sparse-switch v0, :sswitch_data_0

    .line 1059322
    :goto_0
    return-object p1

    .line 1059323
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1059324
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1059325
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1059326
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1059327
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1059328
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1059329
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69f19a9a -> :sswitch_1
        -0x41a91745 -> :sswitch_4
        -0x3c54de38 -> :sswitch_2
        -0x3b85b241 -> :sswitch_6
        -0x35b0b8aa -> :sswitch_3
        -0x30b65c8f -> :sswitch_0
        0x7506f93c -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1059330
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1059331
    :goto_1
    return v0

    .line 1059332
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1059333
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1059334
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x36 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
