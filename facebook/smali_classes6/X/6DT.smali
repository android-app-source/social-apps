.class public final LX/6DT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

.field public final synthetic c:LX/6DU;


# direct methods
.method public constructor <init>(LX/6DU;Ljava/util/ArrayList;Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1065159
    iput-object p1, p0, LX/6DT;->c:LX/6DU;

    iput-object p2, p0, LX/6DT;->a:Ljava/util/ArrayList;

    iput-object p3, p0, LX/6DT;->b:Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1065160
    iget-object v0, p0, LX/6DT;->c:LX/6DU;

    iget-object v0, v0, LX/6DU;->i:LX/03V;

    sget-object v1, LX/6DU;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1065161
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065162
    check-cast p1, Ljava/util/List;

    .line 1065163
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065164
    iget-object v0, p0, LX/6DT;->c:LX/6DU;

    iget-object v0, v0, LX/6DU;->c:LX/6Cz;

    iget-object v1, p0, LX/6DT;->a:Ljava/util/ArrayList;

    iget-object v2, p0, LX/6DT;->b:Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

    const-string v3, "browser_extensions_autofill_no_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6Cz;->b(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065165
    :cond_0
    iget-object v1, p0, LX/6DT;->c:LX/6DU;

    iget-object v2, p0, LX/6DT;->b:Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

    iget-object v0, p0, LX/6DT;->c:LX/6DU;

    iget-object v0, v0, LX/6DU;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    .line 1065166
    iget-object v3, v1, LX/6DU;->c:LX/6Cz;

    if-eqz v3, :cond_1

    .line 1065167
    iget-object v3, v1, LX/6DU;->c:LX/6Cz;

    const-string v4, "browser_extensions_autofill_dialog_shown"

    invoke-virtual {v3, p1, v2, v4}, LX/6Cz;->a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065168
    :cond_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1065169
    const-string v4, "EXTRA_AUTO_FILL_JS_BRIDGE"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1065170
    const-string v4, "EXTRA_AUTO_FILL_FIELDS"

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v4, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1065171
    const-string v4, "EXTRA_AUTO_FILL_FIELD_FULL_NAME"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1065172
    move-object v3, v3

    .line 1065173
    iget-object v4, v1, LX/6DU;->e:LX/1Bf;

    iget-object p0, v1, LX/6DU;->b:Landroid/content/Context;

    .line 1065174
    invoke-static {v4}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v1

    invoke-static {p0, v1, v3}, LX/049;->a(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 1065175
    return-void
.end method
