.class public final LX/5BV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 864244
    const/16 v39, 0x0

    .line 864245
    const/16 v38, 0x0

    .line 864246
    const/16 v37, 0x0

    .line 864247
    const/16 v36, 0x0

    .line 864248
    const/16 v35, 0x0

    .line 864249
    const/16 v34, 0x0

    .line 864250
    const/16 v33, 0x0

    .line 864251
    const/16 v32, 0x0

    .line 864252
    const/16 v31, 0x0

    .line 864253
    const/16 v30, 0x0

    .line 864254
    const/16 v29, 0x0

    .line 864255
    const/16 v28, 0x0

    .line 864256
    const/16 v27, 0x0

    .line 864257
    const/16 v26, 0x0

    .line 864258
    const/16 v25, 0x0

    .line 864259
    const/16 v24, 0x0

    .line 864260
    const/16 v23, 0x0

    .line 864261
    const/16 v22, 0x0

    .line 864262
    const/16 v21, 0x0

    .line 864263
    const/16 v20, 0x0

    .line 864264
    const/16 v19, 0x0

    .line 864265
    const/16 v18, 0x0

    .line 864266
    const/16 v17, 0x0

    .line 864267
    const/16 v16, 0x0

    .line 864268
    const/4 v15, 0x0

    .line 864269
    const/4 v14, 0x0

    .line 864270
    const/4 v13, 0x0

    .line 864271
    const/4 v12, 0x0

    .line 864272
    const/4 v11, 0x0

    .line 864273
    const/4 v10, 0x0

    .line 864274
    const/4 v9, 0x0

    .line 864275
    const/4 v8, 0x0

    .line 864276
    const/4 v7, 0x0

    .line 864277
    const/4 v6, 0x0

    .line 864278
    const/4 v5, 0x0

    .line 864279
    const/4 v4, 0x0

    .line 864280
    const/4 v3, 0x0

    .line 864281
    const/4 v2, 0x0

    .line 864282
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1

    .line 864283
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 864284
    const/4 v2, 0x0

    .line 864285
    :goto_0
    return v2

    .line 864286
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 864287
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1b

    .line 864288
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v40

    .line 864289
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 864290
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    if-eqz v40, :cond_1

    .line 864291
    const-string v41, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_2

    .line 864292
    const/4 v13, 0x1

    .line 864293
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 864294
    :cond_2
    const-string v41, "can_see_voice_switcher"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_3

    .line 864295
    const/4 v12, 0x1

    .line 864296
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 864297
    :cond_3
    const-string v41, "can_viewer_comment"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_4

    .line 864298
    const/4 v11, 0x1

    .line 864299
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 864300
    :cond_4
    const-string v41, "can_viewer_comment_with_photo"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_5

    .line 864301
    const/4 v10, 0x1

    .line 864302
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 864303
    :cond_5
    const-string v41, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_6

    .line 864304
    const/4 v9, 0x1

    .line 864305
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 864306
    :cond_6
    const-string v41, "can_viewer_comment_with_video"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_7

    .line 864307
    const/4 v8, 0x1

    .line 864308
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto :goto_1

    .line 864309
    :cond_7
    const-string v41, "can_viewer_like"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_8

    .line 864310
    const/4 v7, 0x1

    .line 864311
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 864312
    :cond_8
    const-string v41, "can_viewer_react"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 864313
    const/4 v6, 0x1

    .line 864314
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 864315
    :cond_9
    const-string v41, "can_viewer_subscribe"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_a

    .line 864316
    const/4 v5, 0x1

    .line 864317
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 864318
    :cond_a
    const-string v41, "comments_mirroring_domain"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 864319
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 864320
    :cond_b
    const-string v41, "does_viewer_like"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 864321
    const/4 v4, 0x1

    .line 864322
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 864323
    :cond_c
    const-string v41, "id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_d

    .line 864324
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 864325
    :cond_d
    const-string v41, "important_reactors"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_e

    .line 864326
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 864327
    :cond_e
    const-string v41, "is_viewer_subscribed"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_f

    .line 864328
    const/4 v3, 0x1

    .line 864329
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 864330
    :cond_f
    const-string v41, "legacy_api_post_id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_10

    .line 864331
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 864332
    :cond_10
    const-string v41, "likers"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_11

    .line 864333
    invoke-static/range {p0 .. p1}, LX/5Ac;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 864334
    :cond_11
    const-string v41, "reactors"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_12

    .line 864335
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 864336
    :cond_12
    const-string v41, "real_time_activity_info"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_13

    .line 864337
    invoke-static/range {p0 .. p1}, LX/5Ab;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 864338
    :cond_13
    const-string v41, "remixable_photo_uri"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_14

    .line 864339
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 864340
    :cond_14
    const-string v41, "reshares"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_15

    .line 864341
    invoke-static/range {p0 .. p1}, LX/5Ad;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 864342
    :cond_15
    const-string v41, "supported_reactions"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_16

    .line 864343
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 864344
    :cond_16
    const-string v41, "top_level_comments"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_17

    .line 864345
    invoke-static/range {p0 .. p1}, LX/5Ae;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 864346
    :cond_17
    const-string v41, "top_reactions"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_18

    .line 864347
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 864348
    :cond_18
    const-string v41, "viewer_acts_as_page"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_19

    .line 864349
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 864350
    :cond_19
    const-string v41, "viewer_acts_as_person"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1a

    .line 864351
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 864352
    :cond_1a
    const-string v41, "viewer_feedback_reaction_key"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_0

    .line 864353
    const/4 v2, 0x1

    .line 864354
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 864355
    :cond_1b
    const/16 v40, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 864356
    if-eqz v13, :cond_1c

    .line 864357
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 864358
    :cond_1c
    if-eqz v12, :cond_1d

    .line 864359
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 864360
    :cond_1d
    if-eqz v11, :cond_1e

    .line 864361
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 864362
    :cond_1e
    if-eqz v10, :cond_1f

    .line 864363
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 864364
    :cond_1f
    if-eqz v9, :cond_20

    .line 864365
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 864366
    :cond_20
    if-eqz v8, :cond_21

    .line 864367
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 864368
    :cond_21
    if-eqz v7, :cond_22

    .line 864369
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 864370
    :cond_22
    if-eqz v6, :cond_23

    .line 864371
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 864372
    :cond_23
    if-eqz v5, :cond_24

    .line 864373
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 864374
    :cond_24
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 864375
    if-eqz v4, :cond_25

    .line 864376
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 864377
    :cond_25
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 864378
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 864379
    if-eqz v3, :cond_26

    .line 864380
    const/16 v3, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 864381
    :cond_26
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864382
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864383
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864384
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864385
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864386
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864387
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864388
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864389
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864390
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 864391
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 864392
    if-eqz v2, :cond_27

    .line 864393
    const/16 v2, 0x19

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 864394
    :cond_27
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
