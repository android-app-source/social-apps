.class public LX/5K9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:LX/1Of;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 898208
    new-instance v0, LX/1Oe;

    invoke-direct {v0}, LX/1Oe;-><init>()V

    sput-object v0, LX/5K9;->a:LX/1Of;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 898209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 898210
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recycler must have sizes spec set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/5KB;LX/1Of;LX/5Je;LX/5K7;LX/3x6;LX/1OX;ZLX/1PH;LX/1np;)V
    .locals 2
    .param p1    # LX/1Of;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/5Je;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/5K7;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/3x6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1OX;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation build Lcom/facebook/components/annotations/OnBind;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5KB;",
            "LX/1Of;",
            "LX/5Je;",
            "LX/5K7;",
            "LX/3x6;",
            "LX/1OX;",
            "Z",
            "LX/1PH;",
            "LX/1np",
            "<",
            "LX/1Of;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 898211
    invoke-virtual {p0, p6}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 898212
    if-eqz p7, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/5KB;->setEnabled(Z)V

    .line 898213
    invoke-virtual {p0, p7}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 898214
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 898215
    if-nez v0, :cond_1

    .line 898216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView not found, it should not be removed from SwipeRefreshLayout before unmounting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 898218
    :cond_1
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v1, v1

    .line 898219
    iput-object v1, p8, LX/1np;->a:Ljava/lang/Object;

    .line 898220
    sget-object v1, LX/5K9;->a:LX/1Of;

    if-eq p1, v1, :cond_2

    .line 898221
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 898222
    :cond_2
    if-eqz p4, :cond_3

    .line 898223
    invoke-virtual {v0, p4}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 898224
    :cond_3
    if-eqz p5, :cond_4

    .line 898225
    invoke-virtual {v0, p5}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 898226
    :cond_4
    invoke-virtual {p2, v0}, LX/3mY;->b(Landroid/view/ViewGroup;)V

    .line 898227
    if-eqz p3, :cond_5

    .line 898228
    iput-object v0, p3, LX/5K7;->a:Landroid/support/v7/widget/RecyclerView;

    .line 898229
    :cond_5
    return-void
.end method

.method public static a(LX/5KB;LX/5Je;)V
    .locals 2

    .prologue
    .line 898230
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 898231
    if-nez v0, :cond_0

    .line 898232
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView not found, it should not be removed from SwipeRefreshLayout before unmounting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898233
    :cond_0
    invoke-virtual {p1, v0}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 898234
    return-void
.end method

.method public static a(LX/5KB;LX/5Je;LX/5K7;LX/3x6;LX/1OX;LX/1Of;)V
    .locals 2
    .param p1    # LX/5Je;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/5K7;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/3x6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1OX;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 898235
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 898236
    if-nez v0, :cond_0

    .line 898237
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView not found, it should not be removed from SwipeRefreshLayout before unmounting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898238
    :cond_0
    invoke-virtual {v0, p5}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 898239
    invoke-virtual {p1, v0}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 898240
    if-eqz p2, :cond_1

    .line 898241
    iput-object v1, p2, LX/5K7;->a:Landroid/support/v7/widget/RecyclerView;

    .line 898242
    :cond_1
    if-eqz p3, :cond_2

    .line 898243
    invoke-virtual {v0, p3}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 898244
    :cond_2
    if-eqz p4, :cond_3

    .line 898245
    invoke-virtual {v0, p4}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 898246
    :cond_3
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 898247
    return-void
.end method

.method public static a(LX/5KB;LX/5Je;ZZI)V
    .locals 2
    .param p1    # LX/5Je;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 898248
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 898249
    if-nez v0, :cond_0

    .line 898250
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView not found, it should not be removed from SwipeRefreshLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898251
    :cond_0
    iput-boolean p2, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 898252
    invoke-virtual {v0, p3}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 898253
    invoke-virtual {v0, p4}, Landroid/support/v7/widget/RecyclerView;->setScrollBarStyle(I)V

    .line 898254
    invoke-virtual {p1, v0}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 898255
    return-void
.end method
