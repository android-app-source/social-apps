.class public final LX/5fX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f4",
        "<[B",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5f4;

.field public final synthetic b:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;LX/5f4;)V
    .locals 0

    .prologue
    .line 971707
    iput-object p1, p0, LX/5fX;->b:Lcom/facebook/optic/CameraPreviewView;

    iput-object p2, p0, LX/5fX;->a:LX/5f4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([BLjava/lang/Integer;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 971708
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971709
    invoke-virtual {v0}, LX/5fQ;->o()Landroid/graphics/Rect;

    move-result-object v1

    .line 971710
    invoke-virtual {v0}, LX/5fQ;->n()Landroid/graphics/Rect;

    move-result-object v0

    .line 971711
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, LX/5fX;->b:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v3}, Lcom/facebook/optic/CameraPreviewView;->getWidth()I

    move-result v3

    iget-object v4, p0, LX/5fX;->b:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v4}, Lcom/facebook/optic/CameraPreviewView;->getHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 971712
    new-instance v3, LX/5fk;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v3, v1, v0, v2, v4}, LX/5fk;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 971713
    iget-object v0, p0, LX/5fX;->a:LX/5f4;

    invoke-interface {v0, p1, v3}, LX/5f4;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 971714
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 971715
    iget-object v0, p0, LX/5fX;->a:LX/5f4;

    invoke-interface {v0, p1}, LX/5f4;->a(Ljava/lang/Exception;)V

    .line 971716
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 971717
    check-cast p1, [B

    check-cast p2, Ljava/lang/Integer;

    invoke-direct {p0, p1, p2}, LX/5fX;->a([BLjava/lang/Integer;)V

    return-void
.end method
