.class public LX/53n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static b:I

.field public static final c:I

.field private static final d:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/53d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53d",
            "<",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static i:LX/53d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53d",
            "<",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:Ljava/lang/Object;

.field public e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:I

.field public final g:LX/53d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53d",
            "<",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 827457
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 827458
    sput-object v0, LX/53n;->d:LX/0vH;

    .line 827459
    const/16 v0, 0x80

    sput v0, LX/53n;->b:I

    .line 827460
    sget-boolean v0, LX/53k;->a:Z

    move v0, v0

    .line 827461
    if-eqz v0, :cond_0

    .line 827462
    const/16 v0, 0x10

    sput v0, LX/53n;->b:I

    .line 827463
    :cond_0
    const-string v0, "rx.ring-buffer.size"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 827464
    if-eqz v1, :cond_1

    .line 827465
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/53n;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 827466
    :cond_1
    :goto_0
    sget v0, LX/53n;->b:I

    sput v0, LX/53n;->c:I

    .line 827467
    new-instance v0, LX/53l;

    invoke-direct {v0}, LX/53l;-><init>()V

    sput-object v0, LX/53n;->h:LX/53d;

    .line 827468
    new-instance v0, LX/53m;

    invoke-direct {v0}, LX/53m;-><init>()V

    sput-object v0, LX/53n;->i:LX/53d;

    return-void

    .line 827469
    :catch_0
    move-exception v0

    .line 827470
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to set \'rx.buffer.size\' with value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " => "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 827510
    new-instance v0, LX/53t;

    sget v1, LX/53n;->c:I

    invoke-direct {v0, v1}, LX/53t;-><init>(I)V

    sget v1, LX/53n;->c:I

    invoke-direct {p0, v0, v1}, LX/53n;-><init>(Ljava/util/Queue;I)V

    .line 827511
    return-void
.end method

.method public constructor <init>(LX/53d;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53d",
            "<",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;>;I)V"
        }
    .end annotation

    .prologue
    .line 827505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827506
    iput-object p1, p0, LX/53n;->g:LX/53d;

    .line 827507
    invoke-virtual {p1}, LX/53d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    iput-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    .line 827508
    iput p2, p0, LX/53n;->f:I

    .line 827509
    return-void
.end method

.method private constructor <init>(Ljava/util/Queue;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 827500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827501
    iput-object p1, p0, LX/53n;->e:Ljava/util/Queue;

    .line 827502
    const/4 v0, 0x0

    iput-object v0, p0, LX/53n;->g:LX/53d;

    .line 827503
    iput p2, p0, LX/53n;->f:I

    .line 827504
    return-void
.end method

.method public static a(Ljava/lang/Object;LX/0vA;)Z
    .locals 1

    .prologue
    .line 827499
    invoke-static {p1, p0}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static d()LX/53n;
    .locals 3

    .prologue
    .line 827496
    invoke-static {}, LX/54I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827497
    new-instance v0, LX/53n;

    sget-object v1, LX/53n;->i:LX/53d;

    sget v2, LX/53n;->c:I

    invoke-direct {v0, v1, v2}, LX/53n;-><init>(LX/53d;I)V

    .line 827498
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/53n;

    invoke-direct {v0}, LX/53n;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 827491
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 827492
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This instance has been unsubscribed and the queue is no longer usable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827493
    :cond_0
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 827494
    new-instance v0, LX/52z;

    invoke-direct {v0}, LX/52z;-><init>()V

    throw v0

    .line 827495
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 827485
    iget-object v0, p0, LX/53n;->g:LX/53d;

    if-eqz v0, :cond_0

    .line 827486
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    .line 827487
    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 827488
    const/4 v1, 0x0

    iput-object v1, p0, LX/53n;->e:Ljava/util/Queue;

    .line 827489
    iget-object v1, p0, LX/53n;->g:LX/53d;

    invoke-virtual {v1, v0}, LX/53d;->a(Ljava/lang/Object;)V

    .line 827490
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827484
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 827480
    iget-object v0, p0, LX/53n;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 827481
    sget-object v0, LX/0vH;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 827482
    iput-object v0, p0, LX/53n;->a:Ljava/lang/Object;

    .line 827483
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 827477
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 827478
    const/4 v0, 0x1

    .line 827479
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 827471
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 827472
    :cond_0
    :goto_0
    return-object v0

    .line 827473
    :cond_1
    iget-object v0, p0, LX/53n;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 827474
    if-nez v0, :cond_0

    iget-object v2, p0, LX/53n;->a:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/53n;->e:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 827475
    iget-object v0, p0, LX/53n;->a:Ljava/lang/Object;

    .line 827476
    iput-object v1, p0, LX/53n;->a:Ljava/lang/Object;

    goto :goto_0
.end method
