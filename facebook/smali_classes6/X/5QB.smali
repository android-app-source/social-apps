.class public final LX/5QB;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/text/Spannable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Q9;

.field private final b:I

.field private final c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(LX/3Q9;Landroid/widget/TextView;Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)V
    .locals 0

    .prologue
    .line 912375
    iput-object p1, p0, LX/5QB;->a:LX/3Q9;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 912376
    iput-object p2, p0, LX/5QB;->d:Landroid/widget/TextView;

    .line 912377
    iput-object p3, p0, LX/5QB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 912378
    iput p4, p0, LX/5QB;->b:I

    .line 912379
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 912380
    iget-object v0, p0, LX/5QB;->a:LX/3Q9;

    iget-object v1, p0, LX/5QB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget v2, p0, LX/5QB;->b:I

    invoke-virtual {v0, v1, v2}, LX/3Q9;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 912381
    check-cast p1, Landroid/text/Spannable;

    .line 912382
    iget-object v0, p0, LX/5QB;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 912383
    return-void
.end method

.method public final onPreExecute()V
    .locals 2

    .prologue
    .line 912384
    iget-object v0, p0, LX/5QB;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/5QB;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 912385
    return-void
.end method
