.class public final LX/6Uu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uo;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6Uu;


# instance fields
.field public final b:LX/6V0;

.field public final c:LX/0lC;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/6V0;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103138
    iput-object p1, p0, LX/6Uu;->b:LX/6V0;

    .line 1103139
    iput-object p2, p0, LX/6Uu;->c:LX/0lC;

    .line 1103140
    iput-object p3, p0, LX/6Uu;->d:LX/03V;

    .line 1103141
    return-void
.end method

.method public static a(LX/0QB;)LX/6Uu;
    .locals 6

    .prologue
    .line 1103142
    sget-object v0, LX/6Uu;->e:LX/6Uu;

    if-nez v0, :cond_1

    .line 1103143
    const-class v1, LX/6Uu;

    monitor-enter v1

    .line 1103144
    :try_start_0
    sget-object v0, LX/6Uu;->e:LX/6Uu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1103145
    if-eqz v2, :cond_0

    .line 1103146
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1103147
    new-instance p0, LX/6Uu;

    invoke-static {v0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v3

    check-cast v3, LX/6V0;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/6Uu;-><init>(LX/6V0;LX/0lC;LX/03V;)V

    .line 1103148
    move-object v0, p0

    .line 1103149
    sput-object v0, LX/6Uu;->e:LX/6Uu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103150
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103151
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103152
    :cond_1
    sget-object v0, LX/6Uu;->e:LX/6Uu;

    return-object v0

    .line 1103153
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/ViewGroup;LX/162;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1103155
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 1103156
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1103157
    :goto_0
    iget-object v2, p0, LX/6Uu;->c:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->e()LX/0m9;

    move-result-object v2

    .line 1103158
    const-string v3, "offending_view_group"

    iget-object v4, p0, LX/6Uu;->b:LX/6V0;

    sget-object v5, LX/6Uz;->NONE:LX/6Uz;

    invoke-virtual {v4, p1, v5}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103159
    if-eqz v0, :cond_0

    .line 1103160
    const-string v3, "offending_view_group_child"

    iget-object v4, p0, LX/6Uu;->b:LX/6V0;

    sget-object v5, LX/6Uz;->NONE:LX/6Uz;

    invoke-virtual {v4, v0, v5}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103161
    :cond_0
    move-object v0, v2

    .line 1103162
    invoke-virtual {p2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1103163
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1103164
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1103165
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 1103166
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p2}, LX/6Uu;->a(Landroid/view/ViewGroup;LX/162;)V

    .line 1103167
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1103168
    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1103169
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103170
    const-string v0, "ViewGroups that have less than two children are often unnecessary. Merge the important properties to neighboring Views and remove this ViewGroup."

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1103171
    iget-object v0, p0, LX/6Uu;->c:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v0

    .line 1103172
    invoke-direct {p0, p1, v0}, LX/6Uu;->a(Landroid/view/ViewGroup;LX/162;)V

    .line 1103173
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    .line 1103174
    if-nez v1, :cond_0

    .line 1103175
    const/4 v0, 0x0

    .line 1103176
    :goto_0
    return v0

    .line 1103177
    :cond_0
    const-string v2, "num_rule_breakers"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103178
    :try_start_0
    iget-object v1, p0, LX/6Uu;->c:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1103179
    :goto_1
    const-string v1, "rule_breakers"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103180
    const/4 v0, 0x1

    goto :goto_0

    .line 1103181
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1103182
    const-string v2, "Error serializing rule breakers: "

    .line 1103183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1103184
    iget-object v3, p0, LX/6Uu;->d:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1103185
    sget-object v3, LX/6Uu;->a:Ljava/lang/Class;

    invoke-static {v3, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103186
    const-string v0, "Unnecessary ViewGroup"

    return-object v0
.end method
