.class public final enum LX/6NY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6NY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6NY;

.field public static final enum COMMUNICATION_RANK:LX/6NY;

.field public static final enum FBID:LX/6NY;

.field public static final enum IS_IN_CONTACT_LIST:LX/6NY;

.field public static final enum IS_MEMORIALIZED:LX/6NY;

.field public static final enum IS_MESSENGER_USER:LX/6NY;

.field public static final enum IS_PUSHABLE_TRISTATE:LX/6NY;

.field public static final enum IS_ZERO_COMMUNICATION_RANK:LX/6NY;

.field public static final enum LINK_TYPE:LX/6NY;

.field public static final enum NAME:LX/6NY;

.field public static final enum PHONE_E164:LX/6NY;

.field public static final enum PHONE_LOCAL:LX/6NY;

.field public static final enum PHONE_NATIONAL:LX/6NY;

.field public static final enum PHONE_VERIFIED:LX/6NY;

.field public static final enum PROFILE_TYPE:LX/6NY;

.field public static final enum SORT_NAME_KEY:LX/6NY;

.field public static final enum USERNAME_KEY:LX/6NY;

.field public static final enum VIEWER_CONNECTION_STATUS:LX/6NY;

.field public static final enum WITH_TAGGING_RANK:LX/6NY;


# instance fields
.field private final mDbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1083640
    new-instance v0, LX/6NY;

    const-string v1, "NAME"

    const-string v2, "name"

    invoke-direct {v0, v1, v4, v2}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->NAME:LX/6NY;

    .line 1083641
    new-instance v0, LX/6NY;

    const-string v1, "COMMUNICATION_RANK"

    const-string v2, "communication_rank"

    invoke-direct {v0, v1, v5, v2}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->COMMUNICATION_RANK:LX/6NY;

    .line 1083642
    new-instance v0, LX/6NY;

    const-string v1, "WITH_TAGGING_RANK"

    const-string v2, "with_tagging_rank"

    invoke-direct {v0, v1, v6, v2}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->WITH_TAGGING_RANK:LX/6NY;

    .line 1083643
    new-instance v0, LX/6NY;

    const-string v1, "PHONE_E164"

    const-string v2, "phone_e164"

    invoke-direct {v0, v1, v7, v2}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->PHONE_E164:LX/6NY;

    .line 1083644
    new-instance v0, LX/6NY;

    const-string v1, "PHONE_NATIONAL"

    const-string v2, "phone_national"

    invoke-direct {v0, v1, v8, v2}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->PHONE_NATIONAL:LX/6NY;

    .line 1083645
    new-instance v0, LX/6NY;

    const-string v1, "PHONE_LOCAL"

    const/4 v2, 0x5

    const-string v3, "phone_local"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->PHONE_LOCAL:LX/6NY;

    .line 1083646
    new-instance v0, LX/6NY;

    const-string v1, "PHONE_VERIFIED"

    const/4 v2, 0x6

    const-string v3, "phone_verified"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->PHONE_VERIFIED:LX/6NY;

    .line 1083647
    new-instance v0, LX/6NY;

    const-string v1, "SORT_NAME_KEY"

    const/4 v2, 0x7

    const-string v3, "sort_name_key"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->SORT_NAME_KEY:LX/6NY;

    .line 1083648
    new-instance v0, LX/6NY;

    const-string v1, "IS_MEMORIALIZED"

    const/16 v2, 0x8

    const-string v3, "is_memorialized"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->IS_MEMORIALIZED:LX/6NY;

    .line 1083649
    new-instance v0, LX/6NY;

    const-string v1, "VIEWER_CONNECTION_STATUS"

    const/16 v2, 0x9

    const-string v3, "viewer_connection_status"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->VIEWER_CONNECTION_STATUS:LX/6NY;

    .line 1083650
    new-instance v0, LX/6NY;

    const-string v1, "PROFILE_TYPE"

    const/16 v2, 0xa

    const-string v3, "profile_type"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->PROFILE_TYPE:LX/6NY;

    .line 1083651
    new-instance v0, LX/6NY;

    const-string v1, "LINK_TYPE"

    const/16 v2, 0xb

    const-string v3, "link_type"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->LINK_TYPE:LX/6NY;

    .line 1083652
    new-instance v0, LX/6NY;

    const-string v1, "FBID"

    const/16 v2, 0xc

    const-string v3, "fbid"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->FBID:LX/6NY;

    .line 1083653
    new-instance v0, LX/6NY;

    const-string v1, "IS_PUSHABLE_TRISTATE"

    const/16 v2, 0xd

    const-string v3, "pushable_tristate"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->IS_PUSHABLE_TRISTATE:LX/6NY;

    .line 1083654
    new-instance v0, LX/6NY;

    const-string v1, "IS_MESSENGER_USER"

    const/16 v2, 0xe

    const-string v3, "messenger_user"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->IS_MESSENGER_USER:LX/6NY;

    .line 1083655
    new-instance v0, LX/6NY;

    const-string v1, "IS_IN_CONTACT_LIST"

    const/16 v2, 0xf

    const-string v3, "in_contact_list"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->IS_IN_CONTACT_LIST:LX/6NY;

    .line 1083656
    new-instance v0, LX/6NY;

    const-string v1, "IS_ZERO_COMMUNICATION_RANK"

    const/16 v2, 0x10

    const-string v3, "zero_communication_rank"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->IS_ZERO_COMMUNICATION_RANK:LX/6NY;

    .line 1083657
    new-instance v0, LX/6NY;

    const-string v1, "USERNAME_KEY"

    const/16 v2, 0x11

    const-string v3, "username"

    invoke-direct {v0, v1, v2, v3}, LX/6NY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6NY;->USERNAME_KEY:LX/6NY;

    .line 1083658
    const/16 v0, 0x12

    new-array v0, v0, [LX/6NY;

    sget-object v1, LX/6NY;->NAME:LX/6NY;

    aput-object v1, v0, v4

    sget-object v1, LX/6NY;->COMMUNICATION_RANK:LX/6NY;

    aput-object v1, v0, v5

    sget-object v1, LX/6NY;->WITH_TAGGING_RANK:LX/6NY;

    aput-object v1, v0, v6

    sget-object v1, LX/6NY;->PHONE_E164:LX/6NY;

    aput-object v1, v0, v7

    sget-object v1, LX/6NY;->PHONE_NATIONAL:LX/6NY;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6NY;->PHONE_LOCAL:LX/6NY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6NY;->PHONE_VERIFIED:LX/6NY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6NY;->SORT_NAME_KEY:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6NY;->IS_MEMORIALIZED:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6NY;->VIEWER_CONNECTION_STATUS:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6NY;->PROFILE_TYPE:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6NY;->LINK_TYPE:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6NY;->FBID:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6NY;->IS_PUSHABLE_TRISTATE:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6NY;->IS_MESSENGER_USER:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6NY;->IS_IN_CONTACT_LIST:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6NY;->IS_ZERO_COMMUNICATION_RANK:LX/6NY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/6NY;->USERNAME_KEY:LX/6NY;

    aput-object v2, v0, v1

    sput-object v0, LX/6NY;->$VALUES:[LX/6NY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1083635
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1083636
    iput-object p3, p0, LX/6NY;->mDbValue:Ljava/lang/String;

    .line 1083637
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6NY;
    .locals 1

    .prologue
    .line 1083639
    const-class v0, LX/6NY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6NY;

    return-object v0
.end method

.method public static values()[LX/6NY;
    .locals 1

    .prologue
    .line 1083638
    sget-object v0, LX/6NY;->$VALUES:[LX/6NY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6NY;

    return-object v0
.end method


# virtual methods
.method public final getDbValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1083634
    iget-object v0, p0, LX/6NY;->mDbValue:Ljava/lang/String;

    return-object v0
.end method
