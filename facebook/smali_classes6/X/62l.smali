.class public abstract LX/62l;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/62k;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Landroid/view/VelocityTracker;

.field public B:Landroid/os/Handler;

.field private C:LX/1DI;

.field private D:Ljava/lang/Runnable;

.field private E:Landroid/graphics/Rect;

.field private F:Z

.field public G:Z

.field public a:Landroid/widget/Scroller;

.field public b:LX/62r;

.field public c:I

.field public d:F

.field public e:LX/0Sy;

.field public g:I

.field public h:F

.field private i:LX/62n;

.field private j:I

.field private k:I

.field public l:I

.field public m:I

.field private n:D

.field public o:F

.field public p:F

.field public q:F

.field public r:F

.field public s:F

.field public t:F

.field public u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field public y:LX/0So;

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1042170
    const-class v0, LX/62l;

    sput-object v0, LX/62l;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v2, 0x0

    .line 1042171
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 1042172
    sget-object v0, LX/62r;->NORMAL:LX/62r;

    iput-object v0, p0, LX/62l;->b:LX/62r;

    .line 1042173
    iput v2, p0, LX/62l;->c:I

    .line 1042174
    iput v2, p0, LX/62l;->k:I

    .line 1042175
    iput v2, p0, LX/62l;->l:I

    .line 1042176
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/62l;->n:D

    .line 1042177
    iput v4, p0, LX/62l;->o:F

    .line 1042178
    iput v4, p0, LX/62l;->d:F

    .line 1042179
    iput v4, p0, LX/62l;->p:F

    .line 1042180
    iput v3, p0, LX/62l;->r:F

    .line 1042181
    iput v3, p0, LX/62l;->s:F

    .line 1042182
    iput v3, p0, LX/62l;->t:F

    .line 1042183
    iput-boolean v5, p0, LX/62l;->v:Z

    .line 1042184
    iput-boolean v2, p0, LX/62l;->w:Z

    .line 1042185
    iput-boolean v2, p0, LX/62l;->x:Z

    .line 1042186
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/62l;->B:Landroid/os/Handler;

    .line 1042187
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/62l;->E:Landroid/graphics/Rect;

    .line 1042188
    iput-boolean v2, p0, LX/62l;->F:Z

    .line 1042189
    iput-boolean v5, p0, LX/62l;->G:Z

    .line 1042190
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/62l;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042191
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v2, 0x0

    .line 1042192
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042193
    sget-object v0, LX/62r;->NORMAL:LX/62r;

    iput-object v0, p0, LX/62l;->b:LX/62r;

    .line 1042194
    iput v2, p0, LX/62l;->c:I

    .line 1042195
    iput v2, p0, LX/62l;->k:I

    .line 1042196
    iput v2, p0, LX/62l;->l:I

    .line 1042197
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/62l;->n:D

    .line 1042198
    iput v4, p0, LX/62l;->o:F

    .line 1042199
    iput v4, p0, LX/62l;->d:F

    .line 1042200
    iput v4, p0, LX/62l;->p:F

    .line 1042201
    iput v3, p0, LX/62l;->r:F

    .line 1042202
    iput v3, p0, LX/62l;->s:F

    .line 1042203
    iput v3, p0, LX/62l;->t:F

    .line 1042204
    iput-boolean v5, p0, LX/62l;->v:Z

    .line 1042205
    iput-boolean v2, p0, LX/62l;->w:Z

    .line 1042206
    iput-boolean v2, p0, LX/62l;->x:Z

    .line 1042207
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/62l;->B:Landroid/os/Handler;

    .line 1042208
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/62l;->E:Landroid/graphics/Rect;

    .line 1042209
    iput-boolean v2, p0, LX/62l;->F:Z

    .line 1042210
    iput-boolean v5, p0, LX/62l;->G:Z

    .line 1042211
    invoke-direct {p0, p1, p2}, LX/62l;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v2, 0x0

    .line 1042213
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042214
    sget-object v0, LX/62r;->NORMAL:LX/62r;

    iput-object v0, p0, LX/62l;->b:LX/62r;

    .line 1042215
    iput v2, p0, LX/62l;->c:I

    .line 1042216
    iput v2, p0, LX/62l;->k:I

    .line 1042217
    iput v2, p0, LX/62l;->l:I

    .line 1042218
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/62l;->n:D

    .line 1042219
    iput v4, p0, LX/62l;->o:F

    .line 1042220
    iput v4, p0, LX/62l;->d:F

    .line 1042221
    iput v4, p0, LX/62l;->p:F

    .line 1042222
    iput v3, p0, LX/62l;->r:F

    .line 1042223
    iput v3, p0, LX/62l;->s:F

    .line 1042224
    iput v3, p0, LX/62l;->t:F

    .line 1042225
    iput-boolean v5, p0, LX/62l;->v:Z

    .line 1042226
    iput-boolean v2, p0, LX/62l;->w:Z

    .line 1042227
    iput-boolean v2, p0, LX/62l;->x:Z

    .line 1042228
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/62l;->B:Landroid/os/Handler;

    .line 1042229
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/62l;->E:Landroid/graphics/Rect;

    .line 1042230
    iput-boolean v2, p0, LX/62l;->F:Z

    .line 1042231
    iput-boolean v5, p0, LX/62l;->G:Z

    .line 1042232
    invoke-direct {p0, p1, p2}, LX/62l;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042233
    return-void
.end method

.method public static a(I)LX/62r;
    .locals 3

    .prologue
    .line 1042234
    if-nez p0, :cond_0

    .line 1042235
    sget-object v0, LX/62r;->PULL_TO_REFRESH:LX/62r;

    .line 1042236
    :goto_0
    return-object v0

    .line 1042237
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 1042238
    sget-object v0, LX/62r;->PUSH_TO_REFRESH:LX/62r;

    goto :goto_0

    .line 1042239
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown direction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1042240
    const-class v0, LX/62l;

    invoke-static {v0, p0}, LX/62l;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1042241
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    .line 1042242
    sget-object v0, LX/03r;->PullToRefreshListView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1042243
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/62l;->c:I

    .line 1042244
    invoke-virtual {p0}, LX/62l;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/62l;->g:I

    .line 1042245
    invoke-virtual {p0}, LX/62l;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b06d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/62l;->h:F

    .line 1042246
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1042247
    sget-object v0, LX/03r;->RefreshableListViewContainer:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1042248
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/62l;->k:I

    .line 1042249
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1042250
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 1042251
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/62l;->m:I

    .line 1042252
    new-instance v0, LX/62m;

    invoke-direct {v0, p0}, LX/62m;-><init>(LX/62l;)V

    iput-object v0, p0, LX/62l;->C:LX/1DI;

    .line 1042253
    new-instance v0, Lcom/facebook/widget/refreshableview/RefreshableViewContainer$2;

    invoke-direct {v0, p0}, Lcom/facebook/widget/refreshableview/RefreshableViewContainer$2;-><init>(LX/62l;)V

    iput-object v0, p0, LX/62l;->D:Ljava/lang/Runnable;

    .line 1042254
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1042255
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    .line 1042256
    if-nez v0, :cond_0

    .line 1042257
    :goto_0
    return-void

    .line 1042258
    :cond_0
    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/62l;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {p0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object p0

    check-cast p0, LX/0Sy;

    iput-object v1, p1, LX/62l;->y:LX/0So;

    iput-object p0, p1, LX/62l;->e:LX/0Sy;

    return-void
.end method

.method public static a(LX/62l;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1042259
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    .line 1042260
    if-nez v0, :cond_0

    .line 1042261
    const/4 v0, 0x0

    .line 1042262
    :goto_0
    return v0

    .line 1042263
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1042264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1042265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1042266
    invoke-virtual {p0}, LX/62l;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    .line 1042267
    invoke-virtual {p0}, LX/62l;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 1042268
    float-to-int v3, v1

    .line 1042269
    float-to-int v4, v2

    .line 1042270
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    if-nez v5, :cond_1

    .line 1042271
    iget-object v5, p0, LX/62l;->E:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1042272
    iget-object v5, p0, LX/62l;->E:Landroid/graphics/Rect;

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    iput-boolean v3, p0, LX/62l;->w:Z

    .line 1042273
    :cond_1
    iget-boolean v3, p0, LX/62l;->w:Z

    if-eqz v3, :cond_2

    .line 1042274
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v1, v3

    .line 1042275
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 1042276
    iput v1, p0, LX/62l;->s:F

    .line 1042277
    iput v2, p0, LX/62l;->t:F

    .line 1042278
    invoke-virtual {p1, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1042279
    invoke-static {v0, p1}, LX/62l;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 1042280
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1042281
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1042282
    :goto_0
    return v0

    .line 1042283
    :catch_0
    move-exception v0

    .line 1042284
    sget-object v1, LX/62l;->f:Ljava/lang/Class;

    const-string v2, "Caught and ignoring ArrayIndexOutOfBoundsException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1042285
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(LX/62l;LX/62r;Z)V
    .locals 3

    .prologue
    .line 1042286
    iget-boolean v0, p0, LX/62l;->x:Z

    if-eqz v0, :cond_1

    .line 1042287
    :cond_0
    :goto_0
    return-void

    .line 1042288
    :cond_1
    iget-object v0, p0, LX/62l;->b:LX/62r;

    if-eq p1, v0, :cond_0

    .line 1042289
    iget-object v0, p0, LX/62l;->b:LX/62r;

    .line 1042290
    iput-object p1, p0, LX/62l;->b:LX/62r;

    .line 1042291
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v1

    .line 1042292
    iget v2, p0, LX/62l;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setDirection(I)V

    .line 1042293
    sget-object v2, LX/62r;->NORMAL:LX/62r;

    if-eq p1, v2, :cond_2

    sget-object v2, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    if-ne p1, v2, :cond_4

    .line 1042294
    :cond_2
    iget v2, p0, LX/62l;->c:I

    invoke-static {v2}, LX/62l;->a(I)LX/62r;

    move-result-object v2

    .line 1042295
    iput-object v2, v1, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->o:LX/62r;

    .line 1042296
    :goto_1
    iget-object v1, p0, LX/62l;->i:LX/62n;

    if-eqz v1, :cond_0

    .line 1042297
    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-ne p1, v1, :cond_5

    .line 1042298
    iget-object v0, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_8

    .line 1042299
    iget-object v0, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 1042300
    iget-object v0, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    .line 1042301
    :goto_2
    move v0, v0

    .line 1042302
    iget-object v1, p0, LX/62l;->i:LX/62n;

    .line 1042303
    invoke-virtual {v1, p2}, LX/62n;->b(Z)V

    .line 1042304
    iget-object v1, p0, LX/62l;->b:LX/62r;

    sget-object v2, LX/62r;->FAILED:LX/62r;

    if-eq v1, v2, :cond_3

    .line 1042305
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(IF)V

    .line 1042306
    :cond_3
    iget-object v0, p0, LX/62l;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/62l;->z:J

    goto :goto_0

    .line 1042307
    :cond_4
    iput-object p1, v1, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->o:LX/62r;

    .line 1042308
    goto :goto_1

    .line 1042309
    :cond_5
    sget-object v1, LX/62r;->NORMAL:LX/62r;

    if-ne p1, v1, :cond_6

    .line 1042310
    iget-object v0, p0, LX/62l;->i:LX/62n;

    invoke-virtual {v0}, LX/62n;->a()V

    .line 1042311
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b()V

    goto :goto_0

    .line 1042312
    :cond_6
    sget-object v1, LX/62r;->FAILED:LX/62r;

    if-ne p1, v1, :cond_7

    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-ne v0, v1, :cond_7

    .line 1042313
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a()V

    goto :goto_0

    .line 1042314
    :cond_7
    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static b(LX/62l;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1042315
    iget-boolean v0, p0, LX/62l;->x:Z

    if-eqz v0, :cond_1

    .line 1042316
    :cond_0
    :goto_0
    return-void

    .line 1042317
    :cond_1
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v2, LX/62r;->POPUP:LX/62r;

    if-ne v0, v2, :cond_2

    .line 1042318
    iget v0, p0, LX/62l;->d:F

    float-to-int v2, v0

    .line 1042319
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v3

    sub-int v4, v3, v2

    const/16 v5, 0x1f4

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0

    .line 1042320
    :cond_2
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v2, LX/62r;->NORMAL:LX/62r;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v2, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    if-eq v0, v2, :cond_3

    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, LX/62l;->p:F

    iget v2, p0, LX/62l;->l:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_5

    .line 1042321
    :cond_3
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v0

    iget v2, p0, LX/62l;->j:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    .line 1042322
    iget v2, p0, LX/62l;->d:F

    div-float/2addr v2, v0

    const/high16 v3, 0x44960000    # 1200.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const/16 v3, 0x12c

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1042323
    iget v2, p0, LX/62l;->d:F

    float-to-int v2, v2

    .line 1042324
    const v3, 0x3d4ccccd    # 0.05f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 1042325
    if-ge v2, v0, :cond_4

    .line 1042326
    mul-int v3, v5, v2

    div-int v5, v3, v0

    .line 1042327
    :cond_4
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    neg-int v4, v2

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1042328
    invoke-virtual {p0}, LX/62l;->invalidate()V

    goto :goto_0

    .line 1042329
    :cond_5
    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, LX/62l;->p:F

    iget v1, p0, LX/62l;->l:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_7

    if-nez p1, :cond_7

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1042330
    if-eqz v0, :cond_6

    .line 1042331
    invoke-virtual {p0}, LX/62l;->h()V

    goto/16 :goto_0

    .line 1042332
    :cond_6
    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1042333
    invoke-virtual {p0}, LX/62l;->invalidate()V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c(LX/62l;F)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1042334
    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/62l;->c:I

    if-nez v0, :cond_0

    cmpg-float v0, p1, v2

    if-ltz v0, :cond_1

    :cond_0
    iget v0, p0, LX/62l;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    cmpl-float v0, p1, v2

    if-lez v0, :cond_2

    .line 1042335
    :cond_1
    iget v0, p0, LX/62l;->d:F

    add-float/2addr v0, p1

    iput v0, p0, LX/62l;->d:F

    .line 1042336
    :goto_0
    iget v0, p0, LX/62l;->c:I

    if-nez v0, :cond_4

    .line 1042337
    iget v0, p0, LX/62l;->d:F

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/62l;->d:F

    .line 1042338
    :goto_1
    return-void

    .line 1042339
    :cond_2
    iget v0, p0, LX/62l;->d:F

    .line 1042340
    iget v3, p0, LX/62l;->c:I

    if-nez v3, :cond_5

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1042341
    :goto_2
    const/4 v4, 0x0

    mul-float v5, p1, v3

    iget v6, p0, LX/62l;->h:F

    mul-float/2addr v5, v6

    mul-float v6, v0, v0

    add-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 1042342
    float-to-double v5, v4

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-float v4, v5

    mul-float/2addr v3, v4

    .line 1042343
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, LX/62l;->h:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1042344
    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v3, p1

    add-float/2addr v3, v0

    .line 1042345
    :cond_3
    move v0, v3

    .line 1042346
    iput v0, p0, LX/62l;->d:F

    goto :goto_0

    .line 1042347
    :cond_4
    iget v0, p0, LX/62l;->d:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/62l;->d:F

    goto :goto_1

    .line 1042348
    :cond_5
    const/high16 v3, -0x40800000    # -1.0f

    goto :goto_2
.end method

.method private getOverScrollHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1042352
    invoke-virtual {p0}, LX/62l;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1042353
    const/4 v0, 0x0

    .line 1042354
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/62l;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTriggerHeight(LX/62l;)I
    .locals 6

    .prologue
    .line 1042349
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v0

    int-to-double v0, v0

    const v2, 0x3cf5c28f    # 0.03f

    invoke-virtual {p0}, LX/62l;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    iget-wide v4, p0, LX/62l;->n:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 1042350
    iget v1, p0, LX/62l;->l:I

    sub-int/2addr v0, v1

    .line 1042351
    return v0
.end method

.method private i()V
    .locals 5

    .prologue
    const v2, 0x38d1b717    # 1.0E-4f

    .line 1042376
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042377
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/62l;->d:F

    .line 1042378
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1042379
    invoke-virtual {p0}, LX/62l;->invalidate()V

    .line 1042380
    :cond_0
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/62l;->p:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget v0, p0, LX/62l;->d:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 1042381
    sget-object v0, LX/62r;->NORMAL:LX/62r;

    invoke-virtual {p0, v0}, LX/62l;->a(LX/62r;)V

    .line 1042382
    :cond_1
    iget v0, p0, LX/62l;->p:F

    iget v1, p0, LX/62l;->d:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 1042383
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->POPUP:LX/62r;

    if-ne v0, v1, :cond_2

    iget v0, p0, LX/62l;->p:F

    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 1042384
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/62l;->b(LX/62l;Z)V

    .line 1042385
    invoke-virtual {p0}, LX/62l;->invalidate()V

    .line 1042386
    :cond_2
    :goto_0
    return-void

    .line 1042387
    :cond_3
    iget v0, p0, LX/62l;->d:F

    iget v1, p0, LX/62l;->p:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 1042388
    if-eqz v0, :cond_2

    .line 1042389
    iget v1, p0, LX/62l;->p:F

    int-to-float v2, v0

    add-float/2addr v1, v2

    invoke-direct {p0, v1}, LX/62l;->setCurrentHeaderHeightExposed(F)V

    .line 1042390
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1042391
    invoke-direct {p0}, LX/62l;->getOverScrollHeaderView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1042392
    invoke-direct {p0}, LX/62l;->getOverScrollHeaderView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 1042393
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getBottom()I

    move-result v2

    .line 1042394
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    const v3, 0x7f0d2113

    invoke-virtual {v0, v3}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1042395
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 1042396
    iget v4, p0, LX/62l;->l:I

    add-int/2addr v4, v1

    if-lt v4, v0, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1042397
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    const v3, 0x7f0d2114

    invoke-virtual {v0, v3}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1042398
    iget v0, p0, LX/62l;->l:I

    add-int/2addr v0, v1

    if-lt v0, v2, :cond_5

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, v2, v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1042399
    invoke-virtual {p0}, LX/62l;->invalidate()V

    goto :goto_0

    .line 1042400
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    iget v4, p0, LX/62l;->l:I

    add-int/2addr v0, v4

    goto :goto_1

    .line 1042401
    :cond_5
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LX/62l;->l:I

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method public static j(LX/62l;)Z
    .locals 2

    .prologue
    .line 1042375
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->FAILED:LX/62r;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setCurrentHeaderHeightExposed(F)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const v4, 0x38d1b717    # 1.0E-4f

    .line 1042370
    iget v2, p0, LX/62l;->p:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    move v2, v1

    :goto_0
    cmpg-float v3, p1, v4

    if-gez v3, :cond_3

    :goto_1
    if-eq v2, v1, :cond_1

    .line 1042371
    cmpg-float v1, p1, v4

    if-gez v1, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {p0, v0}, LX/62l;->setHeaderVisibility(I)V

    .line 1042372
    :cond_1
    iput p1, p0, LX/62l;->p:F

    .line 1042373
    return-void

    :cond_2
    move v2, v0

    .line 1042374
    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/62r;)V
    .locals 1

    .prologue
    .line 1042368
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/62l;->a$redex0(LX/62l;LX/62r;Z)V

    .line 1042369
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1042364
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-ne v0, v1, :cond_0

    .line 1042365
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    iget-object v1, p0, LX/62l;->C:LX/1DI;

    iget-object v2, p0, LX/62l;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V

    .line 1042366
    sget-object v0, LX/62r;->FAILED:LX/62r;

    invoke-virtual {p0, v0}, LX/62l;->a(LX/62r;)V

    .line 1042367
    :cond_0
    return-void
.end method

.method public abstract a()Z
.end method

.method public abstract a(F)Z
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1041976
    invoke-virtual {p0}, LX/62l;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/62l;->a(Ljava/lang/String;)V

    .line 1041977
    return-void
.end method

.method public abstract b()Z
.end method

.method public final c()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 1042363
    return-object p0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1042362
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1042358
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-eq v0, v1, :cond_0

    .line 1042359
    sget-object v0, LX/62r;->LOADING:LX/62r;

    invoke-virtual {p0, v0}, LX/62l;->a(LX/62r;)V

    .line 1042360
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/62l;->b(LX/62l;Z)V

    .line 1042361
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1042355
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1042356
    invoke-direct {p0}, LX/62l;->i()V

    .line 1042357
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1042159
    iget-object v0, p0, LX/62l;->b:LX/62r;

    sget-object v1, LX/62r;->LOADING:LX/62r;

    if-eq v0, v1, :cond_0

    .line 1042160
    sget-object v0, LX/62r;->LOADING:LX/62r;

    invoke-virtual {p0, v0}, LX/62l;->a(LX/62r;)V

    .line 1042161
    invoke-virtual {p0}, LX/62l;->h()V

    .line 1042162
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 8

    .prologue
    .line 1042163
    const/4 v0, 0x1

    .line 1042164
    const/4 v1, 0x0

    const-wide/16 v6, 0x7d0

    .line 1042165
    iget-object v2, p0, LX/62l;->y:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/62l;->z:J

    sub-long/2addr v2, v4

    .line 1042166
    cmp-long v4, v2, v6

    if-gez v4, :cond_0

    if-nez v0, :cond_0

    sub-long v2, v6, v2

    .line 1042167
    :goto_0
    iget-object v4, p0, LX/62l;->B:Landroid/os/Handler;

    new-instance v5, Lcom/facebook/widget/refreshableview/RefreshableViewContainer$3;

    invoke-direct {v5, p0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewContainer$3;-><init>(LX/62l;Z)V

    const v6, 0x152d3807

    invoke-static {v4, v5, v2, v3, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1042168
    return-void

    .line 1042169
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1041957
    invoke-virtual {p0}, LX/62l;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/62l;->a(Ljava/lang/String;)V

    .line 1041958
    return-void
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1041986
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1041985
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, LX/62l;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1041984
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getDirection()I
    .locals 1

    .prologue
    .line 1041983
    iget v0, p0, LX/62l;->c:I

    return v0
.end method

.method public getHeaderHeight()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1041978
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getMeasuredHeight()I

    move-result v0

    .line 1041979
    if-lez v0, :cond_0

    .line 1041980
    :goto_0
    return v0

    .line 1041981
    :cond_0
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->measure(II)V

    .line 1041982
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getHeaderHeightExposed()F
    .locals 1

    .prologue
    .line 1041975
    iget v0, p0, LX/62l;->d:F

    return v0
.end method

.method public getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;
    .locals 1

    .prologue
    .line 1041972
    invoke-virtual {p0}, LX/62l;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1041973
    const/4 v0, 0x0

    .line 1041974
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/62l;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    goto :goto_0
.end method

.method public getOnRefreshListener()LX/62n;
    .locals 1

    .prologue
    .line 1041971
    iget-object v0, p0, LX/62l;->i:LX/62n;

    return-object v0
.end method

.method public getState()LX/62r;
    .locals 1

    .prologue
    .line 1041970
    iget-object v0, p0, LX/62l;->b:LX/62r;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1041967
    invoke-virtual {p0}, LX/62l;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1041968
    const/4 v0, 0x0

    .line 1041969
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/62l;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1041959
    iget-boolean v0, p0, LX/62l;->x:Z

    if-eqz v0, :cond_0

    .line 1041960
    :goto_0
    return-void

    .line 1041961
    :cond_0
    iget v0, p0, LX/62l;->d:F

    float-to-int v2, v0

    .line 1041962
    iget v0, p0, LX/62l;->c:I

    if-nez v0, :cond_1

    .line 1041963
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v0

    sub-int/2addr v0, v2

    iget v3, p0, LX/62l;->l:I

    sub-int v4, v0, v3

    .line 1041964
    :goto_1
    iget-object v0, p0, LX/62l;->a:Landroid/widget/Scroller;

    const/16 v5, 0x1f4

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1041965
    invoke-virtual {p0}, LX/62l;->invalidate()V

    goto :goto_0

    .line 1041966
    :cond_1
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v0

    neg-int v0, v0

    sub-int/2addr v0, v2

    iget v3, p0, LX/62l;->l:I

    sub-int v4, v0, v3

    goto :goto_1
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1041956
    iget-boolean v0, p0, LX/62l;->x:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1041987
    invoke-virtual {p0}, LX/62l;->getChildCount()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 1041988
    :goto_0
    return-void

    .line 1041989
    :cond_0
    const-string v1, "RefreshableListViewContainer.onLayout"

    const v2, 0x2a8876b6

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1041990
    :try_start_0
    iget v1, p0, LX/62l;->c:I

    if-nez v1, :cond_2

    .line 1041991
    iget-boolean v1, p0, LX/62l;->F:Z

    if-eqz v1, :cond_1

    iget v0, p0, LX/62l;->k:I

    neg-int v0, v0

    .line 1041992
    :cond_1
    sub-int v1, p5, p3

    .line 1041993
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/view/View;->layout(IIII)V

    .line 1041994
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    .line 1041995
    iget v1, p0, LX/62l;->k:I

    add-int/2addr v0, v1

    .line 1041996
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 1041997
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v2

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v2, v3, v0, v4, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->layout(IIII)V

    .line 1041998
    iget v1, p0, LX/62l;->j:I

    sub-int v1, v0, v1

    .line 1041999
    invoke-direct {p0}, LX/62l;->getOverScrollHeaderView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 1042000
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/62l;->setCurrentHeaderHeightExposed(F)V

    .line 1042001
    invoke-direct {p0}, LX/62l;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1042002
    const v0, 0x209e671

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 1042003
    :cond_2
    :try_start_1
    iget v0, p0, LX/62l;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1042004
    sub-int v0, p5, p3

    .line 1042005
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 1042006
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    .line 1042007
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 1042008
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v2

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v2, v3, v1, v4, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->layout(IIII)V

    .line 1042009
    iget v1, p0, LX/62l;->j:I

    add-int/2addr v1, v0

    .line 1042010
    invoke-direct {p0}, LX/62l;->getOverScrollHeaderView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    sub-int v4, p4, p2

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/view/View;->layout(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1042011
    :catchall_0
    move-exception v0

    const v1, 0x4a10b2fd    # 2370751.2f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1042012
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown direction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/62l;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1042013
    const-string v1, "RefreshableListViewContainer.onMeasure"

    const v2, -0x29f863ab

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1042014
    :try_start_0
    invoke-virtual {p0}, LX/62l;->getChildCount()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 1042015
    :goto_0
    if-ge v0, v3, :cond_0

    .line 1042016
    invoke-virtual {p0, v0}, LX/62l;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1042017
    invoke-virtual {p0, v4, p1, p2}, LX/62l;->measureChild(Landroid/view/View;II)V

    .line 1042018
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1042019
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1042020
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1042021
    :cond_0
    invoke-virtual {p0}, LX/62l;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1042022
    invoke-virtual {p0}, LX/62l;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1042023
    invoke-static {v1, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/62l;->setMeasuredDimension(II)V

    .line 1042024
    invoke-direct {p0}, LX/62l;->getOverScrollHeaderView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, LX/62l;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1042025
    const v0, 0x1bf897e7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1042026
    return-void

    .line 1042027
    :catchall_0
    move-exception v0

    const v1, 0x50bad678

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    const v1, -0x1701fed9

    invoke-static {v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1042028
    iget-boolean v2, p0, LX/62l;->x:Z

    if-eqz v2, :cond_0

    .line 1042029
    const v2, 0x199655b9

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1042030
    :goto_0
    return v0

    .line 1042031
    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1042032
    const v2, 0xb32eaba

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1042033
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1042034
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1042035
    const/4 v5, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1042036
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1042037
    packed-switch v0, :pswitch_data_0

    .line 1042038
    invoke-static {p0, p1}, LX/62l;->a(LX/62l;Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1042039
    :cond_2
    :goto_1
    move v0, v4

    .line 1042040
    iput v2, p0, LX/62l;->q:F

    .line 1042041
    invoke-direct {p0}, LX/62l;->i()V

    .line 1042042
    const v2, 0x39434acc

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1042043
    :pswitch_0
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    .line 1042044
    iget-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1042045
    iput v2, p0, LX/62l;->r:F

    .line 1042046
    iput-boolean v6, p0, LX/62l;->u:Z

    .line 1042047
    iget-object v3, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1042048
    iget-object v3, p0, LX/62l;->a:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1042049
    :cond_3
    invoke-static {p0, p1}, LX/62l;->a(LX/62l;Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_1

    .line 1042050
    :pswitch_1
    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->PULL_TO_REFRESH:LX/62r;

    if-eq v3, v5, :cond_4

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->PUSH_TO_REFRESH:LX/62r;

    if-eq v3, v5, :cond_4

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->BUFFERING:LX/62r;

    if-eq v3, v5, :cond_4

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->FINISHED:LX/62r;

    if-ne v3, v5, :cond_6

    .line 1042051
    :cond_4
    sget-object v3, LX/62r;->NORMAL:LX/62r;

    invoke-virtual {p0, v3}, LX/62l;->a(LX/62r;)V

    .line 1042052
    invoke-static {p0, v6}, LX/62l;->b(LX/62l;Z)V

    .line 1042053
    :cond_5
    :goto_2
    iput v10, p0, LX/62l;->o:F

    .line 1042054
    invoke-static {p0, p1}, LX/62l;->a(LX/62l;Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1042055
    iget-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_2

    .line 1042056
    iget-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->recycle()V

    .line 1042057
    const/4 v3, 0x0

    iput-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    goto :goto_1

    .line 1042058
    :cond_6
    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->LOADING:LX/62r;

    if-ne v3, v5, :cond_7

    .line 1042059
    invoke-static {p0, v6}, LX/62l;->b(LX/62l;Z)V

    .line 1042060
    iget v3, p0, LX/62l;->d:F

    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v3, v3, v5

    if-lez v3, :cond_5

    .line 1042061
    invoke-virtual {p0}, LX/62l;->getHeaderHeight()I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, LX/62l;->d:F

    goto :goto_2

    .line 1042062
    :cond_7
    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->NORMAL:LX/62r;

    if-eq v3, v5, :cond_8

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    if-ne v3, v5, :cond_9

    .line 1042063
    :cond_8
    iput v10, p0, LX/62l;->d:F

    goto :goto_2

    .line 1042064
    :cond_9
    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v5, LX/62r;->FAILED:LX/62r;

    if-ne v3, v5, :cond_5

    .line 1042065
    invoke-static {p0, v6}, LX/62l;->b(LX/62l;Z)V

    goto :goto_2

    .line 1042066
    :pswitch_2
    iget v3, p0, LX/62l;->q:F

    sub-float v7, v2, v3

    .line 1042067
    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v8, LX/62r;->NORMAL:LX/62r;

    if-eq v3, v8, :cond_a

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v8, LX/62r;->POPUP:LX/62r;

    if-eq v3, v8, :cond_a

    iget-object v3, p0, LX/62l;->b:LX/62r;

    sget-object v8, LX/62r;->COLLAPSING_AFTER_REFRESH:LX/62r;

    if-ne v3, v8, :cond_10

    .line 1042068
    :cond_a
    invoke-virtual {p0}, LX/62l;->a()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1042069
    invoke-static {p0, p1}, LX/62l;->a(LX/62l;Landroid/view/MotionEvent;)Z

    move-result v4

    move v3, v6

    .line 1042070
    :goto_3
    iget-object v8, p0, LX/62l;->b:LX/62r;

    sget-object v9, LX/62r;->BUFFERING:LX/62r;

    if-ne v8, v9, :cond_1a

    .line 1042071
    const/4 v3, 0x0

    .line 1042072
    iget v8, p0, LX/62l;->c:I

    if-nez v8, :cond_1d

    .line 1042073
    cmpl-float v8, v7, v3

    if-lez v8, :cond_1c

    .line 1042074
    iget v8, p0, LX/62l;->o:F

    add-float/2addr v8, v7

    iput v8, p0, LX/62l;->o:F

    .line 1042075
    iget v8, p0, LX/62l;->o:F

    iget v9, p0, LX/62l;->g:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1b

    .line 1042076
    iget v3, p0, LX/62l;->o:F

    iget v8, p0, LX/62l;->g:I

    int-to-float v8, v8

    sub-float v7, v3, v8

    .line 1042077
    iget v3, p0, LX/62l;->g:I

    int-to-float v3, v3

    iput v3, p0, LX/62l;->o:F

    .line 1042078
    :goto_4
    move v3, v7

    .line 1042079
    iget v7, p0, LX/62l;->o:F

    iget v8, p0, LX/62l;->g:I

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-ltz v7, :cond_11

    .line 1042080
    iget v7, p0, LX/62l;->c:I

    invoke-static {v7}, LX/62l;->a(I)LX/62r;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/62l;->a(LX/62r;)V

    :cond_b
    :goto_5
    move v7, v6

    .line 1042081
    :goto_6
    iget-object v8, p0, LX/62l;->b:LX/62r;

    sget-object v9, LX/62r;->PULL_TO_REFRESH:LX/62r;

    if-eq v8, v9, :cond_c

    iget-object v8, p0, LX/62l;->b:LX/62r;

    sget-object v9, LX/62r;->PUSH_TO_REFRESH:LX/62r;

    if-ne v8, v9, :cond_15

    .line 1042082
    :cond_c
    invoke-static {p0, v3}, LX/62l;->c(LX/62l;F)V

    .line 1042083
    iget v3, p0, LX/62l;->d:F

    cmpl-float v3, v3, v10

    if-nez v3, :cond_12

    .line 1042084
    sget-object v3, LX/62r;->NORMAL:LX/62r;

    invoke-virtual {p0, v3}, LX/62l;->a(LX/62r;)V

    .line 1042085
    :cond_d
    :goto_7
    if-eqz v7, :cond_17

    .line 1042086
    invoke-static {p0, p1}, LX/62l;->a(LX/62l;Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1042087
    :cond_e
    :goto_8
    iget-boolean v3, p0, LX/62l;->G:Z

    if-eqz v3, :cond_2

    .line 1042088
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v3

    iget v7, p0, LX/62l;->d:F

    cmpl-float v7, v7, v10

    if-nez v7, :cond_19

    :goto_9
    invoke-virtual {v3, v5}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    goto/16 :goto_1

    .line 1042089
    :cond_f
    invoke-virtual {p0}, LX/62l;->b()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {p0, v7}, LX/62l;->a(F)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1042090
    sget-object v3, LX/62r;->BUFFERING:LX/62r;

    invoke-virtual {p0, v3}, LX/62l;->a(LX/62r;)V

    :cond_10
    move v3, v5

    goto/16 :goto_3

    .line 1042091
    :cond_11
    iget v7, p0, LX/62l;->o:F

    cmpl-float v7, v7, v10

    if-nez v7, :cond_b

    .line 1042092
    sget-object v7, LX/62r;->NORMAL:LX/62r;

    invoke-virtual {p0, v7}, LX/62l;->a(LX/62r;)V

    goto :goto_5

    .line 1042093
    :cond_12
    invoke-static {p0}, LX/62l;->getTriggerHeight(LX/62l;)I

    move-result v3

    .line 1042094
    iget v7, p0, LX/62l;->d:F

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    int-to-float v8, v3

    cmpl-float v7, v7, v8

    if-ltz v7, :cond_14

    .line 1042095
    iget-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_13

    .line 1042096
    iget-object v3, p0, LX/62l;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1042097
    :cond_13
    sget-object v3, LX/62r;->LOADING:LX/62r;

    invoke-static {p0, v3, v5}, LX/62l;->a$redex0(LX/62l;LX/62r;Z)V

    :goto_a
    move v7, v6

    .line 1042098
    goto :goto_7

    .line 1042099
    :cond_14
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v7

    const/high16 v8, 0x42c80000    # 100.0f

    iget v9, p0, LX/62l;->d:F

    mul-float/2addr v8, v9

    int-to-float v3, v3

    div-float v3, v8, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v7, v3, v10}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(IF)V

    goto :goto_a

    .line 1042100
    :cond_15
    invoke-virtual {p0, v3}, LX/62l;->a(F)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, LX/62l;->b:LX/62r;

    sget-object v9, LX/62r;->FINISHED:LX/62r;

    if-eq v8, v9, :cond_16

    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1042101
    :cond_16
    invoke-static {p0, v3}, LX/62l;->c(LX/62l;F)V

    move v7, v6

    .line 1042102
    goto/16 :goto_7

    .line 1042103
    :cond_17
    invoke-static {p0}, LX/62l;->j(LX/62l;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1042104
    const/4 v0, 0x1

    .line 1042105
    iget-boolean v3, p0, LX/62l;->u:Z

    if-eqz v3, :cond_20

    .line 1042106
    :cond_18
    :goto_b
    goto/16 :goto_8

    :cond_19
    move v5, v6

    .line 1042107
    goto :goto_9

    :cond_1a
    move v11, v7

    move v7, v3

    move v3, v11

    goto/16 :goto_6

    :cond_1b
    move v7, v3

    .line 1042108
    goto/16 :goto_4

    .line 1042109
    :cond_1c
    iput v3, p0, LX/62l;->o:F

    goto/16 :goto_4

    .line 1042110
    :cond_1d
    cmpg-float v8, v7, v3

    if-gez v8, :cond_1f

    .line 1042111
    iget v8, p0, LX/62l;->o:F

    sub-float/2addr v8, v7

    iput v8, p0, LX/62l;->o:F

    .line 1042112
    iget v8, p0, LX/62l;->o:F

    iget v9, p0, LX/62l;->g:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1e

    .line 1042113
    iget v3, p0, LX/62l;->o:F

    iget v8, p0, LX/62l;->g:I

    int-to-float v8, v8

    sub-float/2addr v3, v8

    neg-float v7, v3

    .line 1042114
    iget v3, p0, LX/62l;->g:I

    int-to-float v3, v3

    iput v3, p0, LX/62l;->o:F

    goto/16 :goto_4

    :cond_1e
    move v7, v3

    .line 1042115
    goto/16 :goto_4

    .line 1042116
    :cond_1f
    iput v3, p0, LX/62l;->o:F

    goto/16 :goto_4

    .line 1042117
    :cond_20
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v7, p0, LX/62l;->r:F

    sub-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1042118
    iget v7, p0, LX/62l;->m:I

    int-to-float v7, v7

    cmpg-float v3, v3, v7

    if-ltz v3, :cond_18

    .line 1042119
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v7

    .line 1042120
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_18

    invoke-virtual {v7}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    if-nez v3, :cond_18

    .line 1042121
    iget v3, p0, LX/62l;->m:I

    add-int/lit8 v3, v3, 0x1

    .line 1042122
    iget v8, p0, LX/62l;->c:I

    if-ne v8, v0, :cond_21

    .line 1042123
    neg-int v3, v3

    .line 1042124
    :cond_21
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v8

    .line 1042125
    iget v9, p0, LX/62l;->s:F

    iget v11, p0, LX/62l;->t:F

    int-to-float v3, v3

    sub-float v3, v11, v3

    invoke-virtual {v8, v9, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1042126
    invoke-static {v7, v8}, LX/62l;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1042127
    iget v3, p0, LX/62l;->s:F

    iget v9, p0, LX/62l;->t:F

    invoke-virtual {v8, v3, v9}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1042128
    invoke-static {v7, v8}, LX/62l;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1042129
    iput-boolean v0, p0, LX/62l;->u:Z

    goto :goto_b

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1042130
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1042131
    invoke-direct {p0, p1}, LX/62l;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1042132
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1042133
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1042134
    invoke-direct {p0, p1}, LX/62l;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1042135
    return-void
.end method

.method public setCustomTriggerMultiplier(D)V
    .locals 1

    .prologue
    .line 1042136
    iput-wide p1, p0, LX/62l;->n:D

    .line 1042137
    return-void
.end method

.method public setDirection(I)V
    .locals 0

    .prologue
    .line 1042138
    iput p1, p0, LX/62l;->c:I

    .line 1042139
    return-void
.end method

.method public setDisabled(Z)V
    .locals 0

    .prologue
    .line 1042140
    iput-boolean p1, p0, LX/62l;->x:Z

    .line 1042141
    return-void
.end method

.method public setErrorVerticalPadding(I)V
    .locals 1

    .prologue
    .line 1042142
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setErrorVerticalPadding(I)V

    .line 1042143
    return-void
.end method

.method public setHeaderVisibility(I)V
    .locals 1

    .prologue
    .line 1042144
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setVisibility(I)V

    .line 1042145
    return-void
.end method

.method public setOnRefreshListener(LX/62n;)V
    .locals 0

    .prologue
    .line 1042146
    iput-object p1, p0, LX/62l;->i:LX/62n;

    .line 1042147
    return-void
.end method

.method public setOverflowListOverlap(I)V
    .locals 1

    .prologue
    .line 1042148
    invoke-virtual {p0}, LX/62l;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/62l;->k:I

    .line 1042149
    return-void
.end method

.method public setOverlapOnBottom(Z)V
    .locals 0

    .prologue
    .line 1042150
    iput-boolean p1, p0, LX/62l;->F:Z

    .line 1042151
    return-void
.end method

.method public setTopMargin(I)V
    .locals 2

    .prologue
    .line 1042152
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v0

    const v1, 0x7f0d2110

    invoke-virtual {v0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1042153
    invoke-virtual {p0}, LX/62l;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1042154
    invoke-virtual {p0}, LX/62l;->getHeaderView()Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1042155
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v0, p0, LX/62l;->l:I

    .line 1042156
    return-void
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 0

    .prologue
    .line 1042157
    iput-boolean p1, p0, LX/62l;->G:Z

    .line 1042158
    return-void
.end method
