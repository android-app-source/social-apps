.class public LX/6cp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6co;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/6co;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6co;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1114620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114621
    iput-object p1, p0, LX/6cp;->a:LX/0Px;

    .line 1114622
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1114623
    iget-object v0, p0, LX/6cp;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/6cp;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6co;

    .line 1114624
    iget-object v4, v0, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1114625
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1114626
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/6cp;->b:LX/0P1;

    .line 1114627
    return-void
.end method

.method public static newBuilder()LX/6cn;
    .locals 1

    .prologue
    .line 1114628
    new-instance v0, LX/6cn;

    invoke-direct {v0}, LX/6cn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1114629
    iget-object v0, p0, LX/6cp;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/6co;
    .locals 1

    .prologue
    .line 1114630
    iget-object v0, p0, LX/6cp;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6co;

    return-object v0
.end method
