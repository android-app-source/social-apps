.class public LX/5OD;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final j:[I

.field private static final k:[I


# instance fields
.field public l:Lcom/facebook/fbui/glyph/GlyphView;

.field public m:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Lcom/facebook/resources/ui/FbCheckedTextView;

.field private p:Z

.field private q:Z

.field private r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 908134
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/5OD;->j:[I

    .line 908135
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009f

    aput v2, v0, v1

    sput-object v0, LX/5OD;->k:[I

    return-void

    .line 908136
    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 908132
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5OD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 908133
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 908130
    const v0, 0x7f01024c

    invoke-direct {p0, p1, p2, v0}, LX/5OD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908131
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 908050
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908051
    iput-boolean v0, p0, LX/5OD;->p:Z

    .line 908052
    iput-boolean v0, p0, LX/5OD;->q:Z

    .line 908053
    iput-boolean v0, p0, LX/5OD;->r:Z

    .line 908054
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 908055
    const v0, 0x7f030611

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 908056
    const v0, 0x7f0d10ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 908057
    const v0, 0x7f0d10ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/5OD;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 908058
    const v0, 0x7f0d10eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    .line 908059
    const v0, 0x7f0d10ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckedTextView;

    iput-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 908060
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 908125
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908126
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 908127
    :goto_0
    return-void

    .line 908128
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 908129
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 908123
    iget-object v0, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 908124
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 908091
    if-nez p1, :cond_0

    .line 908092
    :goto_0
    return-void

    .line 908093
    :cond_0
    iget-object v0, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 908094
    instance-of v0, p1, LX/3Ai;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 908095
    check-cast v0, LX/3Ai;

    .line 908096
    iget-boolean v1, v0, LX/3Ai;->j:Z

    move v1, v1

    .line 908097
    if-eqz v1, :cond_3

    .line 908098
    iget-object v1, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 908099
    iget-object v1, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    const v2, 0x7f0209a0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setCheckMarkDrawable(I)V

    .line 908100
    iget-object v1, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {v1, v4}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 908101
    iget-boolean v1, v0, LX/3Ai;->k:Z

    move v1, v1

    .line 908102
    if-nez v1, :cond_1

    .line 908103
    iget-object v1, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v1, v3, v3, v3, v3}, Lcom/facebook/resources/ui/FbCheckedTextView;->setPadding(IIII)V

    .line 908104
    :cond_1
    :goto_1
    iget-object v1, p0, LX/5OD;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 908105
    iget-object v2, v0, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v2, v2

    .line 908106
    invoke-static {v1, v2}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 908107
    iget-object v1, v0, LX/3Ai;->e:Ljava/lang/CharSequence;

    move-object v1, v1

    .line 908108
    iget-boolean v2, v0, LX/3Ai;->j:Z

    move v0, v2

    .line 908109
    if-nez v0, :cond_2

    .line 908110
    iget-object v0, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 908111
    :cond_2
    :goto_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 908112
    if-nez v0, :cond_5

    .line 908113
    iget-object v0, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 908114
    iget-object v0, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 908115
    :goto_3
    invoke-interface {p1}, Landroid/view/MenuItem;->isCheckable()Z

    move-result v0

    iput-boolean v0, p0, LX/5OD;->p:Z

    .line 908116
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/5OD;->setChecked(Z)V

    .line 908117
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/5OD;->setEnabled(Z)V

    goto :goto_0

    .line 908118
    :cond_3
    iget-object v1, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    goto :goto_1

    .line 908119
    :cond_4
    iget-object v0, p0, LX/5OD;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, v4}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 908120
    iget-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-static {v0, v4}, LX/5OD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 908121
    :cond_5
    iget-object v1, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 908122
    iget-object v1, p0, LX/5OD;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 908084
    iget-boolean v0, p0, LX/5OD;->r:Z

    if-eq v0, p1, :cond_0

    .line 908085
    iput-boolean p1, p0, LX/5OD;->r:Z

    .line 908086
    iget-object v1, p0, LX/5OD;->n:Lcom/facebook/resources/ui/FbTextView;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setSingleLine(Z)V

    .line 908087
    invoke-virtual {p0}, LX/5OD;->requestLayout()V

    .line 908088
    invoke-virtual {p0}, LX/5OD;->invalidate()V

    .line 908089
    :cond_0
    return-void

    .line 908090
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 908083
    iget-boolean v0, p0, LX/5OD;->q:Z

    return v0
.end method

.method public final onCreateDrawableState(I)[I
    .locals 3

    .prologue
    .line 908074
    iget-boolean v0, p0, LX/5OD;->p:Z

    if-nez v0, :cond_0

    .line 908075
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 908076
    :goto_0
    return-object v0

    .line 908077
    :cond_0
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 908078
    iget-boolean v0, p0, LX/5OD;->q:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/5OD;->j:[I

    :goto_1
    invoke-static {v1, v0}, LX/5OD;->mergeDrawableStates([I[I)[I

    .line 908079
    iget-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 908080
    iget-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    iget-boolean v2, p0, LX/5OD;->q:Z

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    :cond_1
    move-object v0, v1

    .line 908081
    goto :goto_0

    .line 908082
    :cond_2
    sget-object v0, LX/5OD;->k:[I

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 908068
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 908069
    iget-boolean v0, p0, LX/5OD;->r:Z

    if-nez v0, :cond_2

    .line 908070
    iget-object v0, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/5OD;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    if-gt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/5OD;->o:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 908071
    :cond_1
    iget-object v0, p0, LX/5OD;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 908072
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 908073
    :cond_2
    return-void
.end method

.method public final setChecked(Z)V
    .locals 0

    .prologue
    .line 908065
    iput-boolean p1, p0, LX/5OD;->q:Z

    .line 908066
    invoke-virtual {p0}, LX/5OD;->refreshDrawableState()V

    .line 908067
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 908061
    iget-boolean v0, p0, LX/5OD;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/5OD;->q:Z

    .line 908062
    invoke-virtual {p0}, LX/5OD;->refreshDrawableState()V

    .line 908063
    return-void

    .line 908064
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
