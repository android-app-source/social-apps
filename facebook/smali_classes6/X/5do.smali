.class public final enum LX/5do;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5do;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5do;

.field public static final enum CONCEPT:LX/5do;

.field public static final enum INTENT:LX/5do;

.field public static final enum NONE:LX/5do;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 966113
    new-instance v0, LX/5do;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, LX/5do;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5do;->NONE:LX/5do;

    .line 966114
    new-instance v0, LX/5do;

    const-string v1, "CONCEPT"

    invoke-direct {v0, v1, v3, v3}, LX/5do;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5do;->CONCEPT:LX/5do;

    .line 966115
    new-instance v0, LX/5do;

    const-string v1, "INTENT"

    invoke-direct {v0, v1, v4, v4}, LX/5do;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5do;->INTENT:LX/5do;

    .line 966116
    const/4 v0, 0x3

    new-array v0, v0, [LX/5do;

    sget-object v1, LX/5do;->NONE:LX/5do;

    aput-object v1, v0, v2

    sget-object v1, LX/5do;->CONCEPT:LX/5do;

    aput-object v1, v0, v3

    sget-object v1, LX/5do;->INTENT:LX/5do;

    aput-object v1, v0, v4

    sput-object v0, LX/5do;->$VALUES:[LX/5do;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 966117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 966118
    iput p3, p0, LX/5do;->value:I

    .line 966119
    return-void
.end method

.method public static fromRawValue(I)LX/5do;
    .locals 6

    .prologue
    .line 966120
    invoke-static {}, LX/5do;->values()[LX/5do;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 966121
    iget v4, v0, LX/5do;->value:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 966122
    :goto_1
    return-object v0

    .line 966123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 966124
    :cond_1
    sget-object v0, LX/5do;->NONE:LX/5do;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5do;
    .locals 1

    .prologue
    .line 966125
    const-class v0, LX/5do;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5do;

    return-object v0
.end method

.method public static values()[LX/5do;
    .locals 1

    .prologue
    .line 966126
    sget-object v0, LX/5do;->$VALUES:[LX/5do;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5do;

    return-object v0
.end method
