.class public LX/6eC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6eC;


# instance fields
.field private final a:LX/6e6;

.field private final b:LX/6e8;


# direct methods
.method public constructor <init>(LX/6e6;LX/6e8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117635
    iput-object p1, p0, LX/6eC;->a:LX/6e6;

    .line 1117636
    iput-object p2, p0, LX/6eC;->b:LX/6e8;

    .line 1117637
    return-void
.end method

.method public static a(LX/0QB;)LX/6eC;
    .locals 5

    .prologue
    .line 1117621
    sget-object v0, LX/6eC;->c:LX/6eC;

    if-nez v0, :cond_1

    .line 1117622
    const-class v1, LX/6eC;

    monitor-enter v1

    .line 1117623
    :try_start_0
    sget-object v0, LX/6eC;->c:LX/6eC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1117624
    if-eqz v2, :cond_0

    .line 1117625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1117626
    new-instance p0, LX/6eC;

    invoke-static {v0}, LX/6e6;->a(LX/0QB;)LX/6e6;

    move-result-object v3

    check-cast v3, LX/6e6;

    invoke-static {v0}, LX/6e8;->a(LX/0QB;)LX/6e8;

    move-result-object v4

    check-cast v4, LX/6e8;

    invoke-direct {p0, v3, v4}, LX/6eC;-><init>(LX/6e6;LX/6e8;)V

    .line 1117627
    move-object v0, p0

    .line 1117628
    sput-object v0, LX/6eC;->c:LX/6eC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1117629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1117630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1117631
    :cond_1
    sget-object v0, LX/6eC;->c:LX/6eC;

    return-object v0

    .line 1117632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1117633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1117582
    iget-object v0, p0, LX/6eC;->b:LX/6e8;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1117583
    const v1, 0x7fc7143

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1117584
    const-string v1, "recent_emoji"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1117585
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1117586
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1117587
    sget-object v1, LX/6e9;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1117588
    iget-object v3, p0, LX/6eC;->a:LX/6e6;

    .line 1117589
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1117590
    const/4 v4, 0x0

    .line 1117591
    :goto_0
    move-object v1, v4

    .line 1117592
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1117593
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1117594
    const v2, -0x5713a237

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1117595
    return-object v1

    .line 1117596
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1117597
    const v2, 0x5a4c923f

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 1117598
    :cond_1
    iget-object v4, v3, LX/6e6;->b:LX/0lC;

    invoke-virtual {v4, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1117599
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1117600
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v5}, LX/0lF;->e()I

    move-result v7

    if-ge v4, v7, :cond_5

    .line 1117601
    invoke-virtual {v5, v4}, LX/0lF;->a(I)LX/0lF;

    move-result-object v7

    .line 1117602
    const-string v8, "firstCodePoint"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-static {v8}, LX/16N;->d(LX/0lF;)I

    move-result v10

    .line 1117603
    const-string v8, "secondCodePoint"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-static {v8}, LX/16N;->d(LX/0lF;)I

    move-result p0

    .line 1117604
    const/4 v8, 0x0

    .line 1117605
    const-string v9, "remainingCodePoints"

    invoke-virtual {v7, v9}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1117606
    const-string v8, "remainingCodePoints"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 1117607
    invoke-virtual {v8}, LX/0lF;->k()LX/0nH;

    move-result-object v9

    sget-object v1, LX/0nH;->ARRAY:LX/0nH;

    if-ne v9, v1, :cond_6

    const/4 v9, 0x1

    :goto_2
    invoke-static {v9}, LX/0PB;->checkArgument(Z)V

    .line 1117608
    check-cast v8, LX/162;

    .line 1117609
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1117610
    const/4 v9, 0x0

    :goto_3
    invoke-virtual {v8}, LX/0lF;->e()I

    move-result v7

    if-ge v9, v7, :cond_2

    .line 1117611
    invoke-virtual {v8, v9}, LX/0lF;->a(I)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->d(LX/0lF;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1117612
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1117613
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    move-object v8, v9

    .line 1117614
    :cond_3
    if-nez v8, :cond_7

    iget-object v8, v3, LX/6e6;->a:LX/1zU;

    invoke-virtual {v8, v10, p0}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v8

    :goto_4
    move-object v7, v8

    .line 1117615
    if-eqz v7, :cond_4

    .line 1117616
    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1117617
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1117618
    :cond_5
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_0

    .line 1117619
    :cond_6
    const/4 v9, 0x0

    goto :goto_2

    .line 1117620
    :cond_7
    iget-object v9, v3, LX/6e6;->a:LX/1zU;

    invoke-virtual {v9, v10, p0, v8}, LX/1zU;->a(IILjava/util/List;)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v8

    goto :goto_4
.end method

.method public final a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1117549
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1117550
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1117551
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1117552
    new-instance v6, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 1117553
    const-string v4, "firstCodePoint"

    .line 1117554
    iget v5, v1, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v5, v5

    .line 1117555
    invoke-virtual {v6, v4, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1117556
    const-string v4, "secondCodePoint"

    .line 1117557
    iget v5, v1, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v5, v5

    .line 1117558
    invoke-virtual {v6, v4, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1117559
    iget-object v4, v1, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    move-object v4, v4

    .line 1117560
    if-eqz v4, :cond_0

    .line 1117561
    const-string v4, "remainingCodePoints"

    invoke-virtual {v6, v4}, LX/0m9;->i(Ljava/lang/String;)LX/162;

    move-result-object v7

    .line 1117562
    iget-object v4, v1, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    move-object p1, v4

    .line 1117563
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v0, :cond_0

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 1117564
    invoke-virtual {v7, v4}, LX/162;->a(Ljava/lang/Integer;)LX/162;

    .line 1117565
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1117566
    :cond_0
    move-object v1, v6

    .line 1117567
    invoke-virtual {v2, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 1117568
    :cond_1
    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1117569
    iget-object v1, p0, LX/6eC;->b:LX/6e8;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1117570
    const v2, -0x34633e40    # -2.0546432E7f

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1117571
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1117572
    sget-object v3, LX/6e9;->a:LX/0U1;

    .line 1117573
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117574
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117575
    const-string v0, "recent_emoji"

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1117576
    if-nez v0, :cond_2

    .line 1117577
    const-string v0, "recent_emoji"

    const/4 v3, 0x0

    const v4, -0x5d29fdc4

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x45c8405d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117578
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1117579
    const v0, -0x267f08e7

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1117580
    return-void

    .line 1117581
    :catchall_0
    move-exception v0

    const v2, -0x7258ed2b

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
