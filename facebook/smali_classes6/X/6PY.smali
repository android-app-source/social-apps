.class public LX/6PY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlace;",
        "LX/4Y7;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 1

    .prologue
    .line 1086220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086221
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6PY;->a:Ljava/lang/String;

    .line 1086222
    iput-object p2, p0, LX/6PY;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086223
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086224
    iget-object v0, p0, LX/6PY;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086225
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlace;

    check-cast p2, LX/4Y7;

    .line 1086226
    iget-object v0, p0, LX/6PY;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086227
    :goto_0
    return-void

    .line 1086228
    :cond_0
    iget-object v0, p0, LX/6PY;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086229
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "viewer_saved_state"

    invoke-virtual {v1, p0, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086230
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086231
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086232
    const-string v0, "SavePlaceMutatingVisitor"

    return-object v0
.end method
