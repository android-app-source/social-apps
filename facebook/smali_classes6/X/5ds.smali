.class public final enum LX/5ds;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5ds;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5ds;

.field public static final enum AGENT_SUGGESTIONS:LX/5ds;

.field public static final enum BROADCAST_UNIT_ID:LX/5ds;

.field public static final enum IGNORE_FOR_WEBHOOK:LX/5ds;

.field public static final enum MARKETPLACE_TAB_MESSAGE:LX/5ds;

.field public static final enum NONE:LX/5ds;

.field public static final enum QUICK_REPLIES:LX/5ds;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 966235
    new-instance v0, LX/5ds;

    const-string v1, "NONE"

    const-string v2, ""

    invoke-direct {v0, v1, v4, v2}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->NONE:LX/5ds;

    .line 966236
    new-instance v0, LX/5ds;

    const-string v1, "IGNORE_FOR_WEBHOOK"

    const-string v2, "ignore_for_webhook"

    invoke-direct {v0, v1, v5, v2}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->IGNORE_FOR_WEBHOOK:LX/5ds;

    .line 966237
    new-instance v0, LX/5ds;

    const-string v1, "QUICK_REPLIES"

    const-string v2, "quick_replies"

    invoke-direct {v0, v1, v6, v2}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->QUICK_REPLIES:LX/5ds;

    .line 966238
    new-instance v0, LX/5ds;

    const-string v1, "MARKETPLACE_TAB_MESSAGE"

    const-string v2, "marketplace_tab_message"

    invoke-direct {v0, v1, v7, v2}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->MARKETPLACE_TAB_MESSAGE:LX/5ds;

    .line 966239
    new-instance v0, LX/5ds;

    const-string v1, "BROADCAST_UNIT_ID"

    const-string v2, "broadcast_unit_id"

    invoke-direct {v0, v1, v8, v2}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->BROADCAST_UNIT_ID:LX/5ds;

    .line 966240
    new-instance v0, LX/5ds;

    const-string v1, "AGENT_SUGGESTIONS"

    const/4 v2, 0x5

    const-string v3, "agent_suggestions"

    invoke-direct {v0, v1, v2, v3}, LX/5ds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ds;->AGENT_SUGGESTIONS:LX/5ds;

    .line 966241
    const/4 v0, 0x6

    new-array v0, v0, [LX/5ds;

    sget-object v1, LX/5ds;->NONE:LX/5ds;

    aput-object v1, v0, v4

    sget-object v1, LX/5ds;->IGNORE_FOR_WEBHOOK:LX/5ds;

    aput-object v1, v0, v5

    sget-object v1, LX/5ds;->QUICK_REPLIES:LX/5ds;

    aput-object v1, v0, v6

    sget-object v1, LX/5ds;->MARKETPLACE_TAB_MESSAGE:LX/5ds;

    aput-object v1, v0, v7

    sget-object v1, LX/5ds;->BROADCAST_UNIT_ID:LX/5ds;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5ds;->AGENT_SUGGESTIONS:LX/5ds;

    aput-object v2, v0, v1

    sput-object v0, LX/5ds;->$VALUES:[LX/5ds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 966242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 966243
    iput-object p3, p0, LX/5ds;->value:Ljava/lang/String;

    .line 966244
    return-void
.end method

.method public static fromRawValue(Ljava/lang/String;)LX/5ds;
    .locals 5

    .prologue
    .line 966245
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966246
    sget-object v0, LX/5ds;->NONE:LX/5ds;

    .line 966247
    :cond_0
    :goto_0
    return-object v0

    .line 966248
    :cond_1
    invoke-static {}, LX/5ds;->values()[LX/5ds;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 966249
    iget-object v4, v0, LX/5ds;->value:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 966250
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 966251
    :cond_2
    sget-object v0, LX/5ds;->NONE:LX/5ds;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5ds;
    .locals 1

    .prologue
    .line 966252
    const-class v0, LX/5ds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5ds;

    return-object v0
.end method

.method public static values()[LX/5ds;
    .locals 1

    .prologue
    .line 966253
    sget-object v0, LX/5ds;->$VALUES:[LX/5ds;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5ds;

    return-object v0
.end method
