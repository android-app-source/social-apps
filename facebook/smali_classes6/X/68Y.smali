.class public LX/68Y;
.super LX/67m;
.source ""


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field public o:I

.field private final p:F

.field private final q:F

.field private final r:F

.field private final s:F

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:F

.field private final w:Landroid/graphics/Paint;

.field public x:Landroid/graphics/Bitmap;

.field public y:Z

.field public z:J


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 2

    .prologue
    .line 1055659
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1055660
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    .line 1055661
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const-string v0, "https://www.facebook.com/images/here_maps/here_maps_logo_64px.png"

    :goto_0
    iput-object v0, p0, LX/68Y;->t:Ljava/lang/String;

    .line 1055662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/67m;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "copyright_logo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/68Y;->u:Ljava/lang/String;

    .line 1055663
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68Y;->p:F

    .line 1055664
    const/4 v0, 0x3

    iput v0, p0, LX/68Y;->j:I

    .line 1055665
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, LX/68Y;->k:F

    .line 1055666
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x41100000    # 9.0f

    mul-float/2addr v0, v1

    iput v0, p0, LX/68Y;->q:F

    .line 1055667
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    iput v0, p0, LX/68Y;->r:F

    .line 1055668
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    iget v1, p0, LX/68Y;->q:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1055669
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iput v0, p0, LX/68Y;->s:F

    .line 1055670
    return-void

    .line 1055671
    :cond_0
    const-string v0, "https://www.facebook.com/images/here_maps/here_maps_logo_32px.png"

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1055672
    iget v0, p0, LX/68Y;->o:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1055673
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    iget v1, p0, LX/68Y;->q:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1055674
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    const v1, -0x40000001    # -1.9999999f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055675
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055676
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    iget v1, p0, LX/68Y;->r:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1055677
    const-string v0, "\u00a9 OpenStreetMap"

    iget v1, p0, LX/68Y;->A:F

    iget v2, p0, LX/68Y;->C:F

    iget-object v3, p0, LX/68Y;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1055678
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    const/high16 v1, -0x67000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055679
    iget-object v0, p0, LX/68Y;->w:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055680
    const-string v0, "\u00a9 OpenStreetMap"

    iget v1, p0, LX/68Y;->A:F

    iget v2, p0, LX/68Y;->C:F

    iget-object v3, p0, LX/68Y;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1055681
    :cond_0
    :goto_0
    return-void

    .line 1055682
    :cond_1
    iget v0, p0, LX/68Y;->o:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1055683
    iget-object v0, p0, LX/68Y;->x:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1055684
    iget-object v0, p0, LX/68Y;->x:Landroid/graphics/Bitmap;

    iget v1, p0, LX/68Y;->A:F

    iget v2, p0, LX/68Y;->B:F

    iget-object v3, p0, LX/68Y;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1055685
    :cond_2
    iget-boolean v0, p0, LX/68Y;->y:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/68Y;->z:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1055686
    const/4 v4, 0x1

    iput-boolean v4, p0, LX/68Y;->y:Z

    .line 1055687
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, LX/68Y;->z:J

    .line 1055688
    new-instance v4, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;

    invoke-direct {v4, p0}, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;-><init>(LX/68Y;)V

    invoke-static {v4}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1055689
    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1055690
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1055691
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1055692
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    .line 1055693
    iget v1, p0, LX/68Y;->p:F

    iget-object v2, p0, LX/67m;->e:LX/680;

    iget v2, v2, LX/680;->c:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, LX/68Y;->A:F

    .line 1055694
    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->f:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    iget v2, p0, LX/68Y;->v:F

    sub-float/2addr v1, v2

    iget v2, p0, LX/68Y;->p:F

    sub-float/2addr v1, v2

    iput v1, p0, LX/68Y;->B:F

    .line 1055695
    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->f:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/68Y;->s:F

    sub-float/2addr v0, v1

    iget v1, p0, LX/68Y;->p:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/68Y;->C:F

    .line 1055696
    return-void
.end method
