.class public final LX/54X;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/53o;

.field public static final b:LX/54X;


# instance fields
.field public final c:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828118
    new-instance v0, LX/53o;

    const-string v1, "RxScheduledExecutorPool-"

    invoke-direct {v0, v1}, LX/53o;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/54X;->a:LX/53o;

    .line 828119
    new-instance v0, LX/54X;

    invoke-direct {v0}, LX/54X;-><init>()V

    sput-object v0, LX/54X;->b:LX/54X;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/16 v0, 0x8

    .line 828120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828121
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    .line 828122
    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    .line 828123
    div-int/lit8 v1, v1, 0x2

    .line 828124
    :cond_0
    if-le v1, v0, :cond_1

    .line 828125
    :goto_0
    sget-object v1, LX/54X;->a:LX/53o;

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, LX/54X;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 828126
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
