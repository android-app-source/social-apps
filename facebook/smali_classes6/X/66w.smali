.class public final LX/66w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field public final synthetic a:LX/65J;

.field public final synthetic b:LX/65g;


# direct methods
.method public constructor <init>(LX/65g;LX/65J;)V
    .locals 0

    .prologue
    .line 1051381
    iput-object p1, p0, LX/66w;->b:LX/65g;

    iput-object p2, p0, LX/66w;->a:LX/65J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1051380
    iget-object v0, p0, LX/66w;->b:LX/65g;

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1051366
    iget-wide v0, p1, LX/672;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    move-wide v4, p2

    .line 1051367
    :goto_0
    cmp-long v0, v4, v2

    if-lez v0, :cond_2

    .line 1051368
    move-wide v0, v2

    :cond_0
    const-wide/32 v6, 0x10000

    cmp-long v6, v0, v6

    if-gez v6, :cond_1

    .line 1051369
    iget-object v6, p1, LX/672;->a:LX/67F;

    iget v6, v6, LX/67F;->c:I

    iget-object v7, p1, LX/672;->a:LX/67F;

    iget v7, v7, LX/67F;->b:I

    sub-int/2addr v6, v7

    .line 1051370
    int-to-long v6, v6

    add-long/2addr v0, v6

    .line 1051371
    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    move-wide v0, v4

    .line 1051372
    :cond_1
    iget-object v6, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v6}, LX/65g;->c()V

    .line 1051373
    :try_start_0
    iget-object v6, p0, LX/66w;->a:LX/65J;

    invoke-interface {v6, p1, v0, v1}, LX/65J;->a_(LX/672;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051374
    sub-long/2addr v4, v0

    .line 1051375
    iget-object v0, p0, LX/66w;->b:LX/65g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/65g;->a(Z)V

    goto :goto_0

    .line 1051376
    :catch_0
    move-exception v0

    .line 1051377
    :try_start_1
    iget-object v1, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v1, v0}, LX/65g;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1051378
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/66w;->b:LX/65g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/65g;->a(Z)V

    throw v0

    .line 1051379
    :cond_2
    return-void
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 1051359
    iget-object v0, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v0}, LX/65g;->c()V

    .line 1051360
    :try_start_0
    iget-object v0, p0, LX/66w;->a:LX/65J;

    invoke-interface {v0}, LX/65J;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051361
    iget-object v0, p0, LX/66w;->b:LX/65g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/65g;->a(Z)V

    .line 1051362
    return-void

    .line 1051363
    :catch_0
    move-exception v0

    .line 1051364
    :try_start_1
    iget-object v1, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v1, v0}, LX/65g;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1051365
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/66w;->b:LX/65g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/65g;->a(Z)V

    throw v0
.end method

.method public final flush()V
    .locals 3

    .prologue
    .line 1051351
    iget-object v0, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v0}, LX/65g;->c()V

    .line 1051352
    :try_start_0
    iget-object v0, p0, LX/66w;->a:LX/65J;

    invoke-interface {v0}, LX/65J;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051353
    iget-object v0, p0, LX/66w;->b:LX/65g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/65g;->a(Z)V

    .line 1051354
    return-void

    .line 1051355
    :catch_0
    move-exception v0

    .line 1051356
    :try_start_1
    iget-object v1, p0, LX/66w;->b:LX/65g;

    invoke-virtual {v1, v0}, LX/65g;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1051357
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/66w;->b:LX/65g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/65g;->a(Z)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1051358
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AsyncTimeout.sink("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/66w;->a:LX/65J;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
