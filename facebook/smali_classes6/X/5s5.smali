.class public LX/5s5;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "DebugComponentOwnershipModule"
.end annotation


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/5s4;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 1012402
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1012403
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/5s5;->a:Landroid/util/SparseArray;

    .line 1012404
    const/4 v0, 0x0

    iput v0, p0, LX/5s5;->c:I

    .line 1012405
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 1012406
    invoke-super {p0}, LX/5pb;->d()V

    .line 1012407
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1012408
    const-class v1, Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;

    iput-object v0, p0, LX/5s5;->b:Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;

    .line 1012409
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1012410
    invoke-super {p0}, LX/5pb;->f()V

    .line 1012411
    const/4 v0, 0x0

    iput-object v0, p0, LX/5s5;->b:Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;

    .line 1012412
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012413
    const-string v0, "DebugComponentOwnershipModule"

    return-object v0
.end method

.method public declared-synchronized receiveOwnershipHierarchy(IILX/5pC;)V
    .locals 3
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1012414
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5s5;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5s4;

    .line 1012415
    if-nez v0, :cond_0

    .line 1012416
    new-instance v0, LX/5p9;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got receiveOwnershipHierarchy for invalid request id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1012417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1012418
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/5s5;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1012419
    monitor-exit p0

    return-void
.end method
