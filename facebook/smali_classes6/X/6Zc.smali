.class public LX/6Zc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zm;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zm;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111297
    iput-object p1, p0, LX/6Zc;->a:LX/0Zm;

    .line 1111298
    iput-object p2, p0, LX/6Zc;->b:LX/0Zb;

    .line 1111299
    return-void
.end method

.method public static a(LX/6Zc;Ljava/lang/String;)LX/0oG;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1111300
    iget-object v1, p0, LX/6Zc;->a:LX/0Zm;

    invoke-virtual {v1, p1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1111301
    :cond_0
    :goto_0
    return-object v0

    .line 1111302
    :cond_1
    iget-object v1, p0, LX/6Zc;->b:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1111303
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1111304
    const-string v0, "gms_ls_upsell"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-object v0, v1

    .line 1111305
    goto :goto_0
.end method
