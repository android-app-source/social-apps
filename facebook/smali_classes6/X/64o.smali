.class public final enum LX/64o;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/64o;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/64o;

.field public static final enum INVALID_HOST:LX/64o;

.field public static final enum INVALID_PORT:LX/64o;

.field public static final enum MISSING_SCHEME:LX/64o;

.field public static final enum SUCCESS:LX/64o;

.field public static final enum UNSUPPORTED_SCHEME:LX/64o;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1045493
    new-instance v0, LX/64o;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/64o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/64o;->SUCCESS:LX/64o;

    .line 1045494
    new-instance v0, LX/64o;

    const-string v1, "MISSING_SCHEME"

    invoke-direct {v0, v1, v3}, LX/64o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/64o;->MISSING_SCHEME:LX/64o;

    .line 1045495
    new-instance v0, LX/64o;

    const-string v1, "UNSUPPORTED_SCHEME"

    invoke-direct {v0, v1, v4}, LX/64o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/64o;->UNSUPPORTED_SCHEME:LX/64o;

    .line 1045496
    new-instance v0, LX/64o;

    const-string v1, "INVALID_PORT"

    invoke-direct {v0, v1, v5}, LX/64o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/64o;->INVALID_PORT:LX/64o;

    .line 1045497
    new-instance v0, LX/64o;

    const-string v1, "INVALID_HOST"

    invoke-direct {v0, v1, v6}, LX/64o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/64o;->INVALID_HOST:LX/64o;

    .line 1045498
    const/4 v0, 0x5

    new-array v0, v0, [LX/64o;

    sget-object v1, LX/64o;->SUCCESS:LX/64o;

    aput-object v1, v0, v2

    sget-object v1, LX/64o;->MISSING_SCHEME:LX/64o;

    aput-object v1, v0, v3

    sget-object v1, LX/64o;->UNSUPPORTED_SCHEME:LX/64o;

    aput-object v1, v0, v4

    sget-object v1, LX/64o;->INVALID_PORT:LX/64o;

    aput-object v1, v0, v5

    sget-object v1, LX/64o;->INVALID_HOST:LX/64o;

    aput-object v1, v0, v6

    sput-object v0, LX/64o;->$VALUES:[LX/64o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1045490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/64o;
    .locals 1

    .prologue
    .line 1045492
    const-class v0, LX/64o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/64o;

    return-object v0
.end method

.method public static values()[LX/64o;
    .locals 1

    .prologue
    .line 1045491
    sget-object v0, LX/64o;->$VALUES:[LX/64o;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/64o;

    return-object v0
.end method
