.class public LX/6AA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1058678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058679
    iput-object p1, p0, LX/6AA;->a:Ljava/lang/String;

    .line 1058680
    iput-object p2, p0, LX/6AA;->b:Ljava/lang/String;

    .line 1058681
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058682
    iget-object v0, p0, LX/6AA;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1058683
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1058684
    iget-object v0, p0, LX/6AA;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058685
    :goto_0
    return-void

    .line 1058686
    :cond_0
    iget-object v0, p0, LX/6AA;->b:Ljava/lang/String;

    invoke-static {p1, v0}, LX/6Ov;->c(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1058687
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4XS;->c(LX/0Px;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058688
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058689
    const-string v0, "SocialSearchDeleteLightweightRecMutatingVisitor"

    return-object v0
.end method
