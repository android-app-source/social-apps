.class public LX/6A8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/model/GraphQLComment;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/0Px;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058641
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6A8;->a:Ljava/lang/String;

    .line 1058642
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, LX/6A8;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1058643
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6A8;->c:LX/0Px;

    .line 1058644
    iget-object v0, p0, LX/6A8;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1058645
    return-void

    .line 1058646
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058647
    iget-object v0, p0, LX/6A8;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 5

    .prologue
    .line 1058648
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1058649
    iget-object v0, p0, LX/6A8;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058650
    :goto_0
    return-void

    .line 1058651
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v1

    .line 1058652
    iget-object v0, p0, LX/6A8;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/6A8;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1058653
    iget-object v4, p0, LX/6A8;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v2, v0, v4}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    .line 1058654
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1058655
    :cond_1
    invoke-virtual {p2, v2}, LX/4XS;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058656
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058657
    const-string v0, "SocialSearchAddPlaceListItemStoryUpdateMutatingVisitor"

    return-object v0
.end method
