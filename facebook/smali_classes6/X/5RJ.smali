.class public final LX/5RJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 914870
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-direct {v0, p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 914871
    new-array v0, p1, [Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    return-object v0
.end method
