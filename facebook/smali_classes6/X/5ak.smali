.class public final LX/5ak;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 55

    .prologue
    .line 955756
    const/16 v49, 0x0

    .line 955757
    const/16 v48, 0x0

    .line 955758
    const/16 v47, 0x0

    .line 955759
    const/16 v46, 0x0

    .line 955760
    const/16 v45, 0x0

    .line 955761
    const/16 v44, 0x0

    .line 955762
    const/16 v43, 0x0

    .line 955763
    const/16 v42, 0x0

    .line 955764
    const/16 v41, 0x0

    .line 955765
    const/16 v40, 0x0

    .line 955766
    const/16 v39, 0x0

    .line 955767
    const/16 v38, 0x0

    .line 955768
    const/16 v37, 0x0

    .line 955769
    const/16 v36, 0x0

    .line 955770
    const/16 v35, 0x0

    .line 955771
    const/16 v34, 0x0

    .line 955772
    const/16 v33, 0x0

    .line 955773
    const/16 v32, 0x0

    .line 955774
    const/16 v31, 0x0

    .line 955775
    const/16 v30, 0x0

    .line 955776
    const/16 v29, 0x0

    .line 955777
    const/16 v28, 0x0

    .line 955778
    const/16 v27, 0x0

    .line 955779
    const/16 v26, 0x0

    .line 955780
    const/16 v25, 0x0

    .line 955781
    const/16 v24, 0x0

    .line 955782
    const/16 v19, 0x0

    .line 955783
    const-wide/16 v22, 0x0

    .line 955784
    const-wide/16 v20, 0x0

    .line 955785
    const/16 v18, 0x0

    .line 955786
    const/16 v17, 0x0

    .line 955787
    const/16 v16, 0x0

    .line 955788
    const/4 v15, 0x0

    .line 955789
    const/4 v14, 0x0

    .line 955790
    const/4 v13, 0x0

    .line 955791
    const/4 v12, 0x0

    .line 955792
    const/4 v11, 0x0

    .line 955793
    const/4 v10, 0x0

    .line 955794
    const/4 v9, 0x0

    .line 955795
    const/4 v8, 0x0

    .line 955796
    const/4 v7, 0x0

    .line 955797
    const/4 v6, 0x0

    .line 955798
    const/4 v5, 0x0

    .line 955799
    const/4 v4, 0x0

    .line 955800
    const/4 v3, 0x0

    .line 955801
    const/4 v2, 0x0

    .line 955802
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v50

    sget-object v51, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_31

    .line 955803
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 955804
    const/4 v2, 0x0

    .line 955805
    :goto_0
    return v2

    .line 955806
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v51, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v51

    if-eq v2, v0, :cond_26

    .line 955807
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 955808
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 955809
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v51

    sget-object v52, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 955810
    const-string v51, "__type__"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-nez v51, :cond_1

    const-string v51, "__typename"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_2

    .line 955811
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v50, v2

    goto :goto_1

    .line 955812
    :cond_2
    const-string v51, "answered_video_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_3

    .line 955813
    const/4 v2, 0x1

    .line 955814
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v49, v14

    move v14, v2

    goto :goto_1

    .line 955815
    :cond_3
    const-string v51, "answered_voice_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_4

    .line 955816
    const/4 v2, 0x1

    .line 955817
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v48, v13

    move v13, v2

    goto :goto_1

    .line 955818
    :cond_4
    const-string v51, "blob_attachments"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_5

    .line 955819
    invoke-static/range {p0 .. p1}, LX/5ae;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 955820
    :cond_5
    const-string v51, "commerce_message_type"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_6

    .line 955821
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v46, v2

    goto/16 :goto_1

    .line 955822
    :cond_6
    const-string v51, "customizations"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_7

    .line 955823
    invoke-static/range {p0 .. p1}, LX/5af;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v45, v2

    goto/16 :goto_1

    .line 955824
    :cond_7
    const-string v51, "duration_video_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_8

    .line 955825
    const/4 v2, 0x1

    .line 955826
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v44, v12

    move v12, v2

    goto/16 :goto_1

    .line 955827
    :cond_8
    const-string v51, "duration_voice_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_9

    .line 955828
    const/4 v2, 0x1

    .line 955829
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v43, v11

    move v11, v2

    goto/16 :goto_1

    .line 955830
    :cond_9
    const-string v51, "extensible_attachment"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_a

    .line 955831
    invoke-static/range {p0 .. p1}, LX/5bW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 955832
    :cond_a
    const-string v51, "extensible_message_admin_text"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_b

    .line 955833
    invoke-static/range {p0 .. p1}, LX/5aT;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 955834
    :cond_b
    const-string v51, "extensible_message_admin_text_type"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_c

    .line 955835
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 955836
    :cond_c
    const-string v51, "is_sponsored"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_d

    .line 955837
    const/4 v2, 0x1

    .line 955838
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v39, v7

    move v7, v2

    goto/16 :goto_1

    .line 955839
    :cond_d
    const-string v51, "is_user_generated"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_e

    .line 955840
    const/4 v2, 0x1

    .line 955841
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v38, v6

    move v6, v2

    goto/16 :goto_1

    .line 955842
    :cond_e
    const-string v51, "message"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_f

    .line 955843
    invoke-static/range {p0 .. p1}, LX/5ag;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 955844
    :cond_f
    const-string v51, "message_id"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_10

    .line 955845
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 955846
    :cond_10
    const-string v51, "message_reactions"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_11

    .line 955847
    invoke-static/range {p0 .. p1}, LX/5am;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 955848
    :cond_11
    const-string v51, "message_sender"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_12

    .line 955849
    invoke-static/range {p0 .. p1}, LX/5bu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 955850
    :cond_12
    const-string v51, "message_source_data"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_13

    .line 955851
    invoke-static/range {p0 .. p1}, LX/5ah;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 955852
    :cond_13
    const-string v51, "montage_reply_data"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_14

    .line 955853
    invoke-static/range {p0 .. p1}, LX/5ai;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 955854
    :cond_14
    const-string v51, "offline_threading_id"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_15

    .line 955855
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 955856
    :cond_15
    const-string v51, "p2p_log_message_type"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_16

    .line 955857
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 955858
    :cond_16
    const-string v51, "p2p_transfer_id"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_17

    .line 955859
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 955860
    :cond_17
    const-string v51, "participants_added"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_18

    .line 955861
    invoke-static/range {p0 .. p1}, LX/5bu;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 955862
    :cond_18
    const-string v51, "participants_removed"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_19

    .line 955863
    invoke-static/range {p0 .. p1}, LX/5bu;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 955864
    :cond_19
    const-string v51, "platform_xmd_encoded"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1a

    .line 955865
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 955866
    :cond_1a
    const-string v51, "reply_type"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1b

    .line 955867
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 955868
    :cond_1b
    const-string v51, "snippet"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1c

    .line 955869
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 955870
    :cond_1c
    const-string v51, "start_time_video_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1d

    .line 955871
    const/4 v2, 0x1

    .line 955872
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 955873
    :cond_1d
    const-string v51, "start_time_voice_call"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1e

    .line 955874
    const/4 v2, 0x1

    .line 955875
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v22

    move v10, v2

    goto/16 :goto_1

    .line 955876
    :cond_1e
    const-string v51, "sticker"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1f

    .line 955877
    invoke-static/range {p0 .. p1}, LX/5aj;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 955878
    :cond_1f
    const-string v51, "tags_list"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_20

    .line 955879
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 955880
    :cond_20
    const-string v51, "thread_name"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_21

    .line 955881
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 955882
    :cond_21
    const-string v51, "thread_pic_url"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_22

    .line 955883
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 955884
    :cond_22
    const-string v51, "timestamp_precise"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_23

    .line 955885
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 955886
    :cond_23
    const-string v51, "ttl"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_24

    .line 955887
    const/4 v2, 0x1

    .line 955888
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v16, v9

    move v9, v2

    goto/16 :goto_1

    .line 955889
    :cond_24
    const-string v51, "unread"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 955890
    const/4 v2, 0x1

    .line 955891
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v15, v8

    move v8, v2

    goto/16 :goto_1

    .line 955892
    :cond_25
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 955893
    :cond_26
    const/16 v2, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 955894
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955895
    if-eqz v14, :cond_27

    .line 955896
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 955897
    :cond_27
    if-eqz v13, :cond_28

    .line 955898
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 955899
    :cond_28
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955900
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955901
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955902
    if-eqz v12, :cond_29

    .line 955903
    const/4 v2, 0x6

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 955904
    :cond_29
    if-eqz v11, :cond_2a

    .line 955905
    const/4 v2, 0x7

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 955906
    :cond_2a
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955907
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955908
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955909
    if-eqz v7, :cond_2b

    .line 955910
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 955911
    :cond_2b
    if-eqz v6, :cond_2c

    .line 955912
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 955913
    :cond_2c
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955914
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955915
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955916
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955917
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955918
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955919
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955920
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955921
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955922
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955923
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955924
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955925
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955926
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955927
    if-eqz v3, :cond_2d

    .line 955928
    const/16 v3, 0x1b

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 955929
    :cond_2d
    if-eqz v10, :cond_2e

    .line 955930
    const/16 v3, 0x1c

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 955931
    :cond_2e
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955932
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955933
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955934
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955935
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 955936
    if-eqz v9, :cond_2f

    .line 955937
    const/16 v2, 0x22

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 955938
    :cond_2f
    if-eqz v8, :cond_30

    .line 955939
    const/16 v2, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 955940
    :cond_30
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_31
    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v37

    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v53, v22

    move-wide/from16 v22, v20

    move/from16 v21, v18

    move/from16 v20, v17

    move/from16 v18, v15

    move/from16 v17, v14

    move v14, v11

    move v15, v12

    move v12, v9

    move v11, v8

    move v9, v3

    move v8, v2

    move v3, v5

    move-wide/from16 v4, v53

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v7, 0x14

    const/16 v6, 0xa

    const/4 v2, 0x4

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 955941
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955942
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 955943
    if-eqz v0, :cond_0

    .line 955944
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955945
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 955946
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955947
    if-eqz v0, :cond_1

    .line 955948
    const-string v1, "answered_video_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955949
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955950
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955951
    if-eqz v0, :cond_2

    .line 955952
    const-string v1, "answered_voice_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955953
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955954
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955955
    if-eqz v0, :cond_4

    .line 955956
    const-string v1, "blob_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955957
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 955958
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v8

    if-ge v1, v8, :cond_3

    .line 955959
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v8

    invoke-static {p0, v8, p2, p3}, LX/5ae;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 955960
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 955961
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 955962
    :cond_4
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 955963
    if-eqz v0, :cond_5

    .line 955964
    const-string v0, "commerce_message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955965
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955966
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955967
    if-eqz v0, :cond_6

    .line 955968
    const-string v1, "customizations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955969
    invoke-static {p0, v0, p2, p3}, LX/5af;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955970
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 955971
    if-eqz v0, :cond_7

    .line 955972
    const-string v1, "duration_video_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955973
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955974
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 955975
    if-eqz v0, :cond_8

    .line 955976
    const-string v1, "duration_voice_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955977
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955978
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955979
    if-eqz v0, :cond_9

    .line 955980
    const-string v1, "extensible_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955981
    invoke-static {p0, v0, p2, p3}, LX/5bW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955982
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955983
    if-eqz v0, :cond_a

    .line 955984
    const-string v1, "extensible_message_admin_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955985
    invoke-static {p0, v0, p2, p3}, LX/5aT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955986
    :cond_a
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 955987
    if-eqz v0, :cond_b

    .line 955988
    const-string v0, "extensible_message_admin_text_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955989
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955990
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955991
    if-eqz v0, :cond_c

    .line 955992
    const-string v1, "is_sponsored"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955993
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955994
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955995
    if-eqz v0, :cond_d

    .line 955996
    const-string v1, "is_user_generated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955997
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955998
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955999
    if-eqz v0, :cond_e

    .line 956000
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956001
    invoke-static {p0, v0, p2, p3}, LX/5ag;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 956002
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956003
    if-eqz v0, :cond_f

    .line 956004
    const-string v1, "message_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956005
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956006
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956007
    if-eqz v0, :cond_10

    .line 956008
    const-string v1, "message_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956009
    invoke-static {p0, v0, p2, p3}, LX/5am;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 956010
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956011
    if-eqz v0, :cond_11

    .line 956012
    const-string v1, "message_sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956013
    invoke-static {p0, v0, p2, p3}, LX/5bu;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 956014
    :cond_11
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956015
    if-eqz v0, :cond_13

    .line 956016
    const-string v1, "message_source_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956017
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956018
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 956019
    if-eqz v1, :cond_12

    .line 956020
    const-string v2, "message_source"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956021
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956022
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956023
    :cond_13
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956024
    if-eqz v0, :cond_15

    .line 956025
    const-string v1, "montage_reply_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956026
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956027
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 956028
    if-eqz v1, :cond_14

    .line 956029
    const-string v2, "message_id"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956030
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956031
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956032
    :cond_15
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956033
    if-eqz v0, :cond_16

    .line 956034
    const-string v1, "offline_threading_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956035
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956036
    :cond_16
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 956037
    if-eqz v0, :cond_17

    .line 956038
    const-string v0, "p2p_log_message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956039
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956040
    :cond_17
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956041
    if-eqz v0, :cond_18

    .line 956042
    const-string v1, "p2p_transfer_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956043
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956044
    :cond_18
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956045
    if-eqz v0, :cond_19

    .line 956046
    const-string v1, "participants_added"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956047
    invoke-static {p0, v0, p2, p3}, LX/5bu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 956048
    :cond_19
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956049
    if-eqz v0, :cond_1a

    .line 956050
    const-string v1, "participants_removed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956051
    invoke-static {p0, v0, p2, p3}, LX/5bu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 956052
    :cond_1a
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956053
    if-eqz v0, :cond_1b

    .line 956054
    const-string v1, "platform_xmd_encoded"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956055
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956056
    :cond_1b
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956057
    if-eqz v0, :cond_1c

    .line 956058
    const-string v0, "reply_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956059
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956060
    :cond_1c
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956061
    if-eqz v0, :cond_1d

    .line 956062
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956063
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956064
    :cond_1d
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 956065
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1e

    .line 956066
    const-string v2, "start_time_video_call"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956067
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 956068
    :cond_1e
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 956069
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1f

    .line 956070
    const-string v2, "start_time_voice_call"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956071
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 956072
    :cond_1f
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956073
    if-eqz v0, :cond_20

    .line 956074
    const-string v1, "sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956075
    invoke-static {p0, v0, p2}, LX/5aj;->a(LX/15i;ILX/0nX;)V

    .line 956076
    :cond_20
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956077
    if-eqz v0, :cond_21

    .line 956078
    const-string v0, "tags_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956079
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 956080
    :cond_21
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956081
    if-eqz v0, :cond_22

    .line 956082
    const-string v1, "thread_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956083
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956084
    :cond_22
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956085
    if-eqz v0, :cond_23

    .line 956086
    const-string v1, "thread_pic_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956087
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956088
    :cond_23
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956089
    if-eqz v0, :cond_24

    .line 956090
    const-string v1, "timestamp_precise"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956091
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956092
    :cond_24
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 956093
    if-eqz v0, :cond_25

    .line 956094
    const-string v1, "ttl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956095
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956096
    :cond_25
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 956097
    if-eqz v0, :cond_26

    .line 956098
    const-string v1, "unread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956099
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 956100
    :cond_26
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956101
    return-void
.end method
