.class public LX/54L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0vA",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/0vA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vA",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field private d:LX/54K;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 827951
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/54L;->e:Ljava/lang/Object;

    .line 827952
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/54L;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0vA;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vA",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 827934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827935
    iput-boolean v0, p0, LX/54L;->b:Z

    .line 827936
    iput-boolean v0, p0, LX/54L;->c:Z

    .line 827937
    iput-object p1, p0, LX/54L;->a:LX/0vA;

    .line 827938
    return-void
.end method

.method private a(LX/54K;)V
    .locals 6

    .prologue
    .line 827939
    if-eqz p1, :cond_0

    iget v0, p1, LX/54K;->b:I

    if-nez v0, :cond_1

    .line 827940
    :cond_0
    return-void

    .line 827941
    :cond_1
    iget-object v2, p1, LX/54K;->a:[Ljava/lang/Object;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 827942
    if-eqz v0, :cond_0

    .line 827943
    sget-object v4, LX/54L;->e:Ljava/lang/Object;

    if-ne v0, v4, :cond_2

    .line 827944
    iget-object v0, p0, LX/54L;->a:LX/0vA;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, LX/0vA;->a(Ljava/lang/Object;)V

    .line 827945
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 827946
    :cond_2
    sget-object v4, LX/54L;->f:Ljava/lang/Object;

    if-ne v0, v4, :cond_3

    .line 827947
    iget-object v0, p0, LX/54L;->a:LX/0vA;

    invoke-interface {v0}, LX/0vA;->R_()V

    goto :goto_1

    .line 827948
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, LX/54J;

    if-ne v4, v5, :cond_4

    .line 827949
    iget-object v4, p0, LX/54L;->a:LX/0vA;

    check-cast v0, LX/54J;

    iget-object v0, v0, LX/54J;->a:Ljava/lang/Throwable;

    invoke-interface {v4, v0}, LX/0vA;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 827950
    :cond_4
    iget-object v4, p0, LX/54L;->a:LX/0vA;

    invoke-interface {v4, v0}, LX/0vA;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public final R_()V
    .locals 2

    .prologue
    .line 827851
    monitor-enter p0

    .line 827852
    :try_start_0
    iget-boolean v0, p0, LX/54L;->c:Z

    if-eqz v0, :cond_0

    .line 827853
    monitor-exit p0

    .line 827854
    :goto_0
    return-void

    .line 827855
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/54L;->c:Z

    .line 827856
    iget-boolean v0, p0, LX/54L;->b:Z

    if-eqz v0, :cond_2

    .line 827857
    iget-object v0, p0, LX/54L;->d:LX/54K;

    if-nez v0, :cond_1

    .line 827858
    new-instance v0, LX/54K;

    invoke-direct {v0}, LX/54K;-><init>()V

    iput-object v0, p0, LX/54L;->d:LX/54K;

    .line 827859
    :cond_1
    iget-object v0, p0, LX/54L;->d:LX/54K;

    sget-object v1, LX/54L;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/54K;->a(Ljava/lang/Object;)V

    .line 827860
    monitor-exit p0

    goto :goto_0

    .line 827861
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 827862
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/54L;->b:Z

    .line 827863
    iget-object v0, p0, LX/54L;->d:LX/54K;

    .line 827864
    const/4 v1, 0x0

    iput-object v1, p0, LX/54L;->d:LX/54K;

    .line 827865
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 827866
    invoke-direct {p0, v0}, LX/54L;->a(LX/54K;)V

    .line 827867
    iget-object v0, p0, LX/54L;->a:LX/0vA;

    invoke-interface {v0}, LX/0vA;->R_()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const v5, 0x7fffffff

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 827868
    monitor-enter p0

    .line 827869
    :try_start_0
    iget-boolean v3, p0, LX/54L;->c:Z

    if-eqz v3, :cond_0

    .line 827870
    monitor-exit p0

    .line 827871
    :goto_0
    return-void

    .line 827872
    :cond_0
    iget-boolean v3, p0, LX/54L;->b:Z

    if-eqz v3, :cond_3

    .line 827873
    iget-object v0, p0, LX/54L;->d:LX/54K;

    if-nez v0, :cond_1

    .line 827874
    new-instance v0, LX/54K;

    invoke-direct {v0}, LX/54K;-><init>()V

    iput-object v0, p0, LX/54L;->d:LX/54K;

    .line 827875
    :cond_1
    iget-object v0, p0, LX/54L;->d:LX/54K;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, p1}, LX/54K;->a(Ljava/lang/Object;)V

    .line 827876
    monitor-exit p0

    goto :goto_0

    .line 827877
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 827878
    :cond_2
    :try_start_1
    sget-object p1, LX/54L;->e:Ljava/lang/Object;

    goto :goto_1

    .line 827879
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/54L;->b:Z

    .line 827880
    iget-object v3, p0, LX/54L;->d:LX/54K;

    .line 827881
    const/4 v4, 0x0

    iput-object v4, p0, LX/54L;->d:LX/54K;

    .line 827882
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v4, v5

    .line 827883
    :cond_4
    :try_start_2
    invoke-direct {p0, v3}, LX/54L;->a(LX/54K;)V

    .line 827884
    if-ne v4, v5, :cond_5

    .line 827885
    iget-object v6, p0, LX/54L;->a:LX/0vA;

    invoke-interface {v6, p1}, LX/0vA;->a(Ljava/lang/Object;)V

    .line 827886
    :cond_5
    add-int/lit8 v4, v4, -0x1

    .line 827887
    if-lez v4, :cond_8

    .line 827888
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 827889
    :try_start_3
    iget-object v3, p0, LX/54L;->d:LX/54K;

    .line 827890
    const/4 v6, 0x0

    iput-object v6, p0, LX/54L;->d:LX/54K;

    .line 827891
    if-nez v3, :cond_7

    .line 827892
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/54L;->b:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 827893
    :try_start_4
    monitor-exit p0

    goto :goto_0

    .line 827894
    :catchall_1
    move-exception v0

    :goto_2
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 827895
    :catchall_2
    move-exception v0

    move v2, v1

    :goto_3
    if-nez v2, :cond_6

    .line 827896
    monitor-enter p0

    .line 827897
    :try_start_6
    iget-boolean v1, p0, LX/54L;->c:Z

    if-eqz v1, :cond_a

    .line 827898
    const/4 v1, 0x0

    iput-object v1, p0, LX/54L;->d:LX/54K;

    .line 827899
    :goto_4
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :cond_6
    throw v0

    .line 827900
    :cond_7
    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 827901
    :cond_8
    if-gtz v4, :cond_4

    .line 827902
    monitor-enter p0

    .line 827903
    :try_start_8
    iget-boolean v1, p0, LX/54L;->c:Z

    if-eqz v1, :cond_9

    .line 827904
    iget-object v0, p0, LX/54L;->d:LX/54K;

    .line 827905
    const/4 v1, 0x0

    iput-object v1, p0, LX/54L;->d:LX/54K;

    .line 827906
    :goto_5
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 827907
    invoke-direct {p0, v0}, LX/54L;->a(LX/54K;)V

    goto :goto_0

    .line 827908
    :cond_9
    const/4 v1, 0x0

    :try_start_9
    iput-boolean v1, p0, LX/54L;->b:Z

    goto :goto_5

    .line 827909
    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 827910
    :cond_a
    const/4 v1, 0x0

    :try_start_a
    iput-boolean v1, p0, LX/54L;->b:Z

    goto :goto_4

    .line 827911
    :catchall_4
    move-exception v0

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v0

    .line 827912
    :catchall_5
    move-exception v0

    goto :goto_3

    .line 827913
    :catchall_6
    move-exception v0

    move v1, v2

    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 827914
    invoke-static {p1}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 827915
    monitor-enter p0

    .line 827916
    :try_start_0
    iget-boolean v0, p0, LX/54L;->c:Z

    if-eqz v0, :cond_0

    .line 827917
    monitor-exit p0

    .line 827918
    :goto_0
    return-void

    .line 827919
    :cond_0
    iget-boolean v0, p0, LX/54L;->b:Z

    if-eqz v0, :cond_2

    .line 827920
    iget-object v0, p0, LX/54L;->d:LX/54K;

    if-nez v0, :cond_1

    .line 827921
    new-instance v0, LX/54K;

    invoke-direct {v0}, LX/54K;-><init>()V

    iput-object v0, p0, LX/54L;->d:LX/54K;

    .line 827922
    :cond_1
    iget-object v0, p0, LX/54L;->d:LX/54K;

    new-instance v1, LX/54J;

    invoke-direct {v1, p1}, LX/54J;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/54K;->a(Ljava/lang/Object;)V

    .line 827923
    monitor-exit p0

    goto :goto_0

    .line 827924
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 827925
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/54L;->b:Z

    .line 827926
    iget-object v0, p0, LX/54L;->d:LX/54K;

    .line 827927
    const/4 v1, 0x0

    iput-object v1, p0, LX/54L;->d:LX/54K;

    .line 827928
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 827929
    invoke-direct {p0, v0}, LX/54L;->a(LX/54K;)V

    .line 827930
    iget-object v0, p0, LX/54L;->a:LX/0vA;

    invoke-interface {v0, p1}, LX/0vA;->a(Ljava/lang/Throwable;)V

    .line 827931
    monitor-enter p0

    .line 827932
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, LX/54L;->b:Z

    .line 827933
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
