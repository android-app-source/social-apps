.class public final LX/6Q4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V
    .locals 0

    .prologue
    .line 1086778
    iput-object p1, p0, LX/6Q4;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x10d3aaf7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1086779
    iget-object v1, p0, LX/6Q4;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v1, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->c:LX/0s6;

    iget-object v2, p0, LX/6Q4;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v2, v2, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086780
    iget-object p1, v2, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v2, p1

    .line 1086781
    iget-object p1, v2, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1086782
    invoke-virtual {v1, v2}, LX/01H;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1086783
    iget-object v1, p0, LX/6Q4;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    .line 1086784
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->l:Landroid/content/pm/PackageManager;

    iget-object v5, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086785
    iget-object p0, v5, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v5, p0

    .line 1086786
    iget-object p0, v5, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v5, p0

    .line 1086787
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1086788
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->j:LX/6QI;

    iget-object v4, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086789
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v4, v5

    .line 1086790
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1086791
    iget-object v5, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086792
    iget-object p0, v5, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v5, p0

    .line 1086793
    iget-object p0, v5, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    move-object v5, p0

    .line 1086794
    iget-object p0, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->o:Ljava/util/Map;

    .line 1086795
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 1086796
    if-eqz p0, :cond_0

    .line 1086797
    invoke-interface {p1, p0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1086798
    :cond_0
    const-string v1, "neko_di_app_details_open_application"

    invoke-virtual {v2, v1, v4, v5, p1}, LX/6QI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1086799
    const v1, 0x3d51b20e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1086800
    :goto_0
    return-void

    .line 1086801
    :cond_1
    iget-object v1, p0, LX/6Q4;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1086802
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->d:LX/6Q9;

    if-nez v2, :cond_3

    .line 1086803
    :cond_2
    :goto_1
    const v1, 0x2ef7ead1

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1086804
    :cond_3
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->d:LX/6Q9;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    iget-object v5, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->o:Ljava/util/Map;

    invoke-interface {v2, v3, v4, v5, v7}, LX/6Q9;->a(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->p:Z

    .line 1086805
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->j:LX/6QI;

    iget-object v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086806
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v3, v4

    .line 1086807
    iget-object v4, v3, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1086808
    iget-object v4, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->n:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1086809
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v4, v5

    .line 1086810
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    move-object v4, v5

    .line 1086811
    iget-object v5, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->o:Ljava/util/Map;

    .line 1086812
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1086813
    if-eqz v5, :cond_4

    .line 1086814
    invoke-interface {p0, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1086815
    :cond_4
    const-string p1, "neko_di_app_details_accept"

    invoke-virtual {v2, p1, v3, v4, p0}, LX/6QI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1086816
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 1086817
    if-eqz v2, :cond_5

    iget-boolean v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->r:Z

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->k:LX/0Uh;

    const/16 v4, 0x32c

    invoke-virtual {v3, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->k:LX/0Uh;

    const/16 v4, 0x32d

    invoke-virtual {v3, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1086818
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1

    .line 1086819
    :cond_5
    if-eqz v2, :cond_2

    iget-object v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->k:LX/0Uh;

    const/16 v4, 0x32d

    invoke-virtual {v3, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1086820
    iget-boolean v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->q:Z

    if-nez v3, :cond_6

    .line 1086821
    new-instance v3, LX/6Q8;

    invoke-direct {v3, v1}, LX/6Q8;-><init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V

    iput-object v3, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->t:Landroid/content/ServiceConnection;

    .line 1086822
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/facebook/directinstall/feed/progressservice/ProgressService;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v4, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->t:Landroid/content/ServiceConnection;

    const v5, 0x41050375

    invoke-static {v2, v3, v4, v7, v5}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v2

    iput-boolean v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->q:Z

    .line 1086823
    :cond_6
    iget-boolean v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->q:Z

    if-eqz v2, :cond_2

    .line 1086824
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->F:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1
.end method
