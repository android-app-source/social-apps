.class public final LX/6PA;
.super LX/6P1;
.source ""


# instance fields
.field public final synthetic b:Z

.field public final synthetic c:LX/3iW;


# direct methods
.method public constructor <init>(LX/3iW;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1086049
    iput-object p1, p0, LX/6PA;->c:LX/3iW;

    iput-boolean p3, p0, LX/6PA;->b:Z

    invoke-direct {p0, p2}, LX/6P1;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3

    .prologue
    .line 1086050
    invoke-static {p1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1086051
    if-nez v1, :cond_1

    .line 1086052
    :cond_0
    :goto_0
    return-object p1

    .line 1086053
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1086054
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    iget-boolean v2, p0, LX/6PA;->b:Z

    if-eq v0, v2, :cond_0

    .line 1086055
    iget-object v0, p0, LX/6PA;->c:LX/3iW;

    iget-object v0, v0, LX/3iW;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1086056
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1086057
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object p1, v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086058
    const-string v0, "FeedStoryCacheAdapter.togglePageLike"

    return-object v0
.end method
