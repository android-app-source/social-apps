.class public final LX/5FC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 884805
    const/4 v10, 0x0

    .line 884806
    const/4 v9, 0x0

    .line 884807
    const/4 v8, 0x0

    .line 884808
    const/4 v7, 0x0

    .line 884809
    const/4 v6, 0x0

    .line 884810
    const/4 v5, 0x0

    .line 884811
    const/4 v4, 0x0

    .line 884812
    const-wide/16 v2, 0x0

    .line 884813
    const/4 v1, 0x0

    .line 884814
    const/4 v0, 0x0

    .line 884815
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 884816
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884817
    const/4 v0, 0x0

    .line 884818
    :goto_0
    return v0

    .line 884819
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884820
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 884821
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 884822
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884823
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 884824
    const-string v12, "campaign_title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 884825
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 884826
    :cond_2
    const-string v12, "charity_interface"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 884827
    invoke-static {p0, p1}, LX/5FA;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 884828
    :cond_3
    const-string v12, "fundraiser_for_charity_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 884829
    invoke-static {p0, p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 884830
    :cond_4
    const-string v12, "fundraiser_progress_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 884831
    invoke-static {p0, p1}, LX/5FB;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 884832
    :cond_5
    const-string v12, "has_goal_amount"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 884833
    const/4 v1, 0x1

    .line 884834
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 884835
    :cond_6
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 884836
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 884837
    :cond_7
    const-string v12, "logo_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 884838
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 884839
    :cond_8
    const-string v12, "percent_of_goal_reached"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 884840
    const/4 v0, 0x1

    .line 884841
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    goto/16 :goto_1

    .line 884842
    :cond_9
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 884843
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 884844
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 884845
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 884846
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 884847
    if-eqz v1, :cond_a

    .line 884848
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 884849
    :cond_a
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 884850
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 884851
    if-eqz v0, :cond_b

    .line 884852
    const/4 v1, 0x7

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 884853
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 884854
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884855
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884856
    if-eqz v0, :cond_0

    .line 884857
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884859
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884860
    if-eqz v0, :cond_1

    .line 884861
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884862
    invoke-static {p0, v0, p2, p3}, LX/5FA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884863
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884864
    if-eqz v0, :cond_2

    .line 884865
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884866
    invoke-static {p0, v0, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884867
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884868
    if-eqz v0, :cond_4

    .line 884869
    const-string v1, "fundraiser_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884870
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884871
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 884872
    if-eqz v1, :cond_3

    .line 884873
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884874
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884875
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884876
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 884877
    if-eqz v0, :cond_5

    .line 884878
    const-string v1, "has_goal_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884879
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 884880
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884881
    if-eqz v0, :cond_6

    .line 884882
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884883
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884884
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884885
    if-eqz v0, :cond_7

    .line 884886
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884887
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 884888
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 884889
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_8

    .line 884890
    const-string v2, "percent_of_goal_reached"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884891
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 884892
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884893
    return-void
.end method
