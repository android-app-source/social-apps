.class public final LX/5V4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 927907
    const/4 v13, 0x0

    .line 927908
    const/4 v12, 0x0

    .line 927909
    const/4 v11, 0x0

    .line 927910
    const/4 v10, 0x0

    .line 927911
    const/4 v9, 0x0

    .line 927912
    const/4 v8, 0x0

    .line 927913
    const/4 v7, 0x0

    .line 927914
    const/4 v6, 0x0

    .line 927915
    const/4 v5, 0x0

    .line 927916
    const/4 v4, 0x0

    .line 927917
    const/4 v3, 0x0

    .line 927918
    const/4 v2, 0x0

    .line 927919
    const/4 v1, 0x0

    .line 927920
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 927921
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 927922
    const/4 v1, 0x0

    .line 927923
    :goto_0
    return v1

    .line 927924
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 927925
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 927926
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 927927
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 927928
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 927929
    const-string v15, "application"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 927930
    invoke-static/range {p0 .. p1}, LX/5V8;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 927931
    :cond_2
    const-string v15, "call_to_actions"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 927932
    invoke-static/range {p0 .. p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 927933
    :cond_3
    const-string v15, "default_action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 927934
    invoke-static/range {p0 .. p1}, LX/5To;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 927935
    :cond_4
    const-string v15, "first_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 927936
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 927937
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 927938
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 927939
    :cond_6
    const-string v15, "image_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 927940
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 927941
    :cond_7
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 927942
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 927943
    :cond_8
    const-string v15, "native_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 927944
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 927945
    :cond_9
    const-string v15, "second_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 927946
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 927947
    :cond_a
    const-string v15, "source_name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 927948
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 927949
    :cond_b
    const-string v15, "status_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 927950
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 927951
    :cond_c
    const-string v15, "target_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 927952
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 927953
    :cond_d
    const-string v15, "third_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 927954
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 927955
    :cond_e
    const/16 v14, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 927956
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 927957
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 927958
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 927959
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 927960
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 927961
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 927962
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 927963
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 927964
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 927965
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 927966
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 927967
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 927968
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 927969
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 927970
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 927971
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 927972
    if-eqz v0, :cond_0

    .line 927973
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927974
    invoke-static {p0, v0, p2, p3}, LX/5V8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 927975
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 927976
    if-eqz v0, :cond_1

    .line 927977
    const-string v1, "call_to_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927978
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 927979
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 927980
    if-eqz v0, :cond_2

    .line 927981
    const-string v1, "default_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927982
    invoke-static {p0, v0, p2, p3}, LX/5To;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 927983
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927984
    if-eqz v0, :cond_3

    .line 927985
    const-string v1, "first_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927986
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927987
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927988
    if-eqz v0, :cond_4

    .line 927989
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927990
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927991
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927992
    if-eqz v0, :cond_5

    .line 927993
    const-string v1, "image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927994
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927995
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927996
    if-eqz v0, :cond_6

    .line 927997
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927998
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927999
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928000
    if-eqz v0, :cond_7

    .line 928001
    const-string v1, "native_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928002
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928003
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928004
    if-eqz v0, :cond_8

    .line 928005
    const-string v1, "second_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928006
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928007
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928008
    if-eqz v0, :cond_9

    .line 928009
    const-string v1, "source_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928010
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928011
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 928012
    if-eqz v0, :cond_a

    .line 928013
    const-string v0, "status_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928014
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928015
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928016
    if-eqz v0, :cond_b

    .line 928017
    const-string v1, "target_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928018
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928019
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928020
    if-eqz v0, :cond_c

    .line 928021
    const-string v1, "third_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928022
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928023
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 928024
    return-void
.end method
