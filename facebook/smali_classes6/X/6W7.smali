.class public LX/6W7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6W7;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1104604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104605
    iput-object p1, p0, LX/6W7;->a:LX/0Zb;

    .line 1104606
    return-void
.end method

.method public static a(LX/0QB;)LX/6W7;
    .locals 4

    .prologue
    .line 1104591
    sget-object v0, LX/6W7;->b:LX/6W7;

    if-nez v0, :cond_1

    .line 1104592
    const-class v1, LX/6W7;

    monitor-enter v1

    .line 1104593
    :try_start_0
    sget-object v0, LX/6W7;->b:LX/6W7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1104594
    if-eqz v2, :cond_0

    .line 1104595
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1104596
    new-instance p0, LX/6W7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/6W7;-><init>(LX/0Zb;)V

    .line 1104597
    move-object v0, p0

    .line 1104598
    sput-object v0, LX/6W7;->b:LX/6W7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104599
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1104600
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1104601
    :cond_1
    sget-object v0, LX/6W7;->b:LX/6W7;

    return-object v0

    .line 1104602
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1104603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1104587
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "single_creator_set"

    .line 1104588
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1104589
    move-object v0, v0

    .line 1104590
    return-object v0
.end method

.method public static a(LX/6W7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1104585
    iget-object v0, p0, LX/6W7;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1104586
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1104584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->B()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1104579
    invoke-static {p0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1104580
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStorySet;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1104581
    const-string v0, "single_creator_set_like_click"

    invoke-static {v0}, LX/6W7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "story_set_id"

    invoke-static {p1}, LX/6W7;->c(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "creator_id"

    invoke-static {p1}, LX/6W7;->d(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1104582
    invoke-static {p0, v0}, LX/6W7;->a(LX/6W7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1104583
    return-void
.end method
