.class public final LX/672;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;
.implements LX/670;
.implements LX/671;


# static fields
.field private static final c:[B


# instance fields
.field public a:LX/67F;

.field public b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1051860
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/672;->c:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1051858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1051859
    return-void
.end method

.method private a(JLjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 1051843
    iget-wide v0, p0, LX/672;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051844
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051845
    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 1051846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051847
    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_3

    const-string v0, ""

    .line 1051848
    :cond_2
    :goto_0
    return-object v0

    .line 1051849
    :cond_3
    iget-object v1, p0, LX/672;->a:LX/67F;

    .line 1051850
    iget v0, v1, LX/67F;->b:I

    int-to-long v2, v0

    add-long/2addr v2, p1

    iget v0, v1, LX/67F;->c:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    .line 1051851
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, LX/672;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0

    .line 1051852
    :cond_4
    new-instance v0, Ljava/lang/String;

    iget-object v2, v1, LX/67F;->a:[B

    iget v3, v1, LX/67F;->b:I

    long-to-int v4, p1

    invoke-direct {v0, v2, v3, v4, p3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 1051853
    iget v2, v1, LX/67F;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p1

    long-to-int v2, v2

    iput v2, v1, LX/67F;->b:I

    .line 1051854
    iget-wide v2, p0, LX/672;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, LX/672;->b:J

    .line 1051855
    iget v2, v1, LX/67F;->b:I

    iget v3, v1, LX/67F;->c:I

    if-ne v2, v3, :cond_2

    .line 1051856
    invoke-virtual {v1}, LX/67F;->a()LX/67F;

    move-result-object v2

    iput-object v2, p0, LX/672;->a:LX/67F;

    .line 1051857
    invoke-static {v1}, LX/67G;->a(LX/67F;)V

    goto :goto_0
.end method

.method private m(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051842
    sget-object v0, LX/67J;->a:Ljava/nio/charset/Charset;

    invoke-direct {p0, p1, p2, v0}, LX/672;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/673;
    .locals 4

    .prologue
    .line 1051836
    iget-wide v0, p0, LX/672;->b:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1051837
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/672;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051838
    :cond_0
    iget-wide v0, p0, LX/672;->b:J

    long-to-int v0, v0

    .line 1051839
    if-nez v0, :cond_1

    sget-object v1, LX/673;->b:LX/673;

    .line 1051840
    :goto_0
    move-object v0, v1

    .line 1051841
    return-object v0

    :cond_1
    new-instance v1, LX/67H;

    invoke-direct {v1, p0, v0}, LX/67H;-><init>(LX/672;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 1051825
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051826
    iget-object v1, p0, LX/672;->a:LX/67F;

    .line 1051827
    if-nez v1, :cond_1

    const/4 v0, -0x1

    .line 1051828
    :cond_0
    :goto_0
    return v0

    .line 1051829
    :cond_1
    iget v0, v1, LX/67F;->c:I

    iget v2, v1, LX/67F;->b:I

    sub-int/2addr v0, v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1051830
    iget-object v2, v1, LX/67F;->a:[B

    iget v3, v1, LX/67F;->b:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1051831
    iget v2, v1, LX/67F;->b:I

    add-int/2addr v2, v0

    iput v2, v1, LX/67F;->b:I

    .line 1051832
    iget-wide v2, p0, LX/672;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/672;->b:J

    .line 1051833
    iget v2, v1, LX/67F;->b:I

    iget v3, v1, LX/67F;->c:I

    if-ne v2, v3, :cond_0

    .line 1051834
    invoke-virtual {v1}, LX/67F;->a()LX/67F;

    move-result-object v2

    iput-object v2, p0, LX/672;->a:LX/67F;

    .line 1051835
    invoke-static {v1}, LX/67G;->a(LX/67F;)V

    goto :goto_0
.end method

.method public final a(B)J
    .locals 2

    .prologue
    .line 1051824
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/672;->a(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(BJ)J
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const-wide/16 v6, -0x1

    .line 1051629
    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fromIndex < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051630
    :cond_0
    iget-object v2, p0, LX/672;->a:LX/67F;

    .line 1051631
    if-nez v2, :cond_1

    move-wide v0, v6

    .line 1051632
    :goto_0
    return-wide v0

    .line 1051633
    :cond_1
    iget-wide v4, p0, LX/672;->b:J

    sub-long/2addr v4, p2

    cmp-long v3, v4, p2

    if-gez v3, :cond_2

    .line 1051634
    iget-wide v0, p0, LX/672;->b:J

    move-object v4, v2

    .line 1051635
    :goto_1
    cmp-long v2, v0, p2

    if-lez v2, :cond_4

    .line 1051636
    iget-object v4, v4, LX/67F;->g:LX/67F;

    .line 1051637
    iget v2, v4, LX/67F;->c:I

    iget v3, v4, LX/67F;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_1

    :cond_2
    move-object v4, v2

    .line 1051638
    :goto_2
    iget v2, v4, LX/67F;->c:I

    iget v3, v4, LX/67F;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v2, v0

    cmp-long v5, v2, p2

    if-gez v5, :cond_4

    .line 1051639
    iget-object v0, v4, LX/67F;->f:LX/67F;

    move-object v4, v0

    move-wide v0, v2

    .line 1051640
    goto :goto_2

    .line 1051641
    :cond_3
    iget v2, v4, LX/67F;->c:I

    iget v3, v4, LX/67F;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 1051642
    iget-object v4, v4, LX/67F;->f:LX/67F;

    move-wide p2, v0

    .line 1051643
    :cond_4
    iget-wide v2, p0, LX/672;->b:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_6

    .line 1051644
    iget-object v3, v4, LX/67F;->a:[B

    .line 1051645
    iget v2, v4, LX/67F;->b:I

    int-to-long v8, v2

    add-long/2addr v8, p2

    sub-long/2addr v8, v0

    long-to-int v2, v8

    iget v5, v4, LX/67F;->c:I

    :goto_3
    if-ge v2, v5, :cond_3

    .line 1051646
    aget-byte v8, v3, v2

    if-ne v8, p1, :cond_5

    .line 1051647
    iget v3, v4, LX/67F;->b:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 1051648
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move-wide v0, v6

    .line 1051649
    goto :goto_0
.end method

.method public final a(LX/65D;)J
    .locals 6

    .prologue
    .line 1051722
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051723
    :cond_0
    const-wide/16 v0, 0x0

    .line 1051724
    :goto_0
    const-wide/16 v2, 0x2000

    invoke-interface {p1, p0, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1051725
    add-long/2addr v0, v2

    goto :goto_0

    .line 1051726
    :cond_1
    return-wide v0
.end method

.method public final a(LX/672;J)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1051817
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051818
    :cond_0
    cmp-long v0, p2, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051819
    :cond_1
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const-wide/16 p2, -0x1

    .line 1051820
    :goto_0
    return-wide p2

    .line 1051821
    :cond_2
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_3

    iget-wide p2, p0, LX/672;->b:J

    .line 1051822
    :cond_3
    invoke-virtual {p1, p0, p2, p3}, LX/672;->a_(LX/672;J)V

    goto :goto_0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1051816
    sget-object v0, LX/65f;->b:LX/65f;

    return-object v0
.end method

.method public final a(I)LX/672;
    .locals 3

    .prologue
    .line 1051796
    const/16 v0, 0x80

    if-ge p1, v0, :cond_0

    .line 1051797
    invoke-virtual {p0, p1}, LX/672;->b(I)LX/672;

    .line 1051798
    :goto_0
    return-object p0

    .line 1051799
    :cond_0
    const/16 v0, 0x800

    if-ge p1, v0, :cond_1

    .line 1051800
    shr-int/lit8 v0, p1, 0x6

    or-int/lit16 v0, v0, 0xc0

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051801
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    goto :goto_0

    .line 1051802
    :cond_1
    const/high16 v0, 0x10000

    if-ge p1, v0, :cond_3

    .line 1051803
    const v0, 0xd800

    if-lt p1, v0, :cond_2

    const v0, 0xdfff

    if-gt p1, v0, :cond_2

    .line 1051804
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected code point: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051805
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051806
    :cond_2
    shr-int/lit8 v0, p1, 0xc

    or-int/lit16 v0, v0, 0xe0

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051807
    shr-int/lit8 v0, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051808
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    goto :goto_0

    .line 1051809
    :cond_3
    const v0, 0x10ffff

    if-gt p1, v0, :cond_4

    .line 1051810
    shr-int/lit8 v0, p1, 0x12

    or-int/lit16 v0, v0, 0xf0

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051811
    shr-int/lit8 v0, p1, 0xc

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051812
    shr-int/lit8 v0, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051813
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    goto :goto_0

    .line 1051814
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected code point: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051815
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/672;JJ)LX/672;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1051778
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "out == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051779
    :cond_0
    iget-wide v0, p0, LX/672;->b:J

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051780
    cmp-long v0, p4, v6

    if-nez v0, :cond_2

    .line 1051781
    :cond_1
    return-object p0

    .line 1051782
    :cond_2
    iget-wide v0, p1, LX/672;->b:J

    add-long/2addr v0, p4

    iput-wide v0, p1, LX/672;->b:J

    .line 1051783
    iget-object v0, p0, LX/672;->a:LX/67F;

    .line 1051784
    :goto_0
    iget v1, v0, LX/67F;->c:I

    iget v2, v0, LX/67F;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, p2, v2

    if-ltz v1, :cond_3

    .line 1051785
    iget v1, v0, LX/67F;->c:I

    iget v2, v0, LX/67F;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    sub-long/2addr p2, v2

    .line 1051786
    iget-object v0, v0, LX/67F;->f:LX/67F;

    goto :goto_0

    .line 1051787
    :cond_3
    :goto_1
    cmp-long v1, p4, v6

    if-lez v1, :cond_1

    .line 1051788
    new-instance v1, LX/67F;

    invoke-direct {v1, v0}, LX/67F;-><init>(LX/67F;)V

    .line 1051789
    iget v2, v1, LX/67F;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p2

    long-to-int v2, v2

    iput v2, v1, LX/67F;->b:I

    .line 1051790
    iget v2, v1, LX/67F;->b:I

    long-to-int v3, p4

    add-int/2addr v2, v3

    iget v3, v1, LX/67F;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v1, LX/67F;->c:I

    .line 1051791
    iget-object v2, p1, LX/672;->a:LX/67F;

    if-nez v2, :cond_4

    .line 1051792
    iput-object v1, v1, LX/67F;->g:LX/67F;

    iput-object v1, v1, LX/67F;->f:LX/67F;

    iput-object v1, p1, LX/672;->a:LX/67F;

    .line 1051793
    :goto_2
    iget v2, v1, LX/67F;->c:I

    iget v1, v1, LX/67F;->b:I

    sub-int v1, v2, v1

    int-to-long v2, v1

    sub-long/2addr p4, v2

    .line 1051794
    iget-object v0, v0, LX/67F;->f:LX/67F;

    move-wide p2, v6

    goto :goto_1

    .line 1051795
    :cond_4
    iget-object v2, p1, LX/672;->a:LX/67F;

    iget-object v2, v2, LX/67F;->g:LX/67F;

    invoke-virtual {v2, v1}, LX/67F;->a(LX/67F;)LX/67F;

    goto :goto_2
.end method

.method public final a(LX/673;)LX/672;
    .locals 2

    .prologue
    .line 1051775
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051776
    :cond_0
    invoke-virtual {p1, p0}, LX/673;->a(LX/672;)V

    .line 1051777
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/672;
    .locals 2

    .prologue
    .line 1051774
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, LX/672;->a(Ljava/lang/String;II)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;II)LX/672;
    .locals 9

    .prologue
    const v8, 0xdfff

    const/16 v7, 0x80

    .line 1051729
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051730
    :cond_0
    if-gez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalAccessError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "beginIndex < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051731
    :cond_1
    if-ge p3, p2, :cond_2

    .line 1051732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex < beginIndex: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051733
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p3, v0, :cond_4

    .line 1051734
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex > string.length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1051735
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051736
    :cond_3
    add-int v1, v0, v4

    iget v3, v2, LX/67F;->c:I

    sub-int/2addr v1, v3

    .line 1051737
    iget v3, v2, LX/67F;->c:I

    add-int/2addr v3, v1

    iput v3, v2, LX/67F;->c:I

    .line 1051738
    iget-wide v2, p0, LX/672;->b:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/672;->b:J

    move p2, v0

    .line 1051739
    :cond_4
    :goto_0
    if-ge p2, p3, :cond_c

    .line 1051740
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1051741
    if-ge v1, v7, :cond_5

    .line 1051742
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/672;->e(I)LX/67F;

    move-result-object v2

    .line 1051743
    iget-object v3, v2, LX/67F;->a:[B

    .line 1051744
    iget v0, v2, LX/67F;->c:I

    sub-int v4, v0, p2

    .line 1051745
    rsub-int v0, v4, 0x2000

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1051746
    add-int/lit8 v0, p2, 0x1

    add-int v6, v4, p2

    int-to-byte v1, v1

    aput-byte v1, v3, v6

    .line 1051747
    :goto_1
    if-ge v0, v5, :cond_3

    .line 1051748
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1051749
    if-ge v6, v7, :cond_3

    .line 1051750
    add-int/lit8 v1, v0, 0x1

    add-int/2addr v0, v4

    int-to-byte v6, v6

    aput-byte v6, v3, v0

    move v0, v1

    goto :goto_1

    .line 1051751
    :cond_5
    const/16 v0, 0x800

    if-ge v1, v0, :cond_6

    .line 1051752
    shr-int/lit8 v0, v1, 0x6

    or-int/lit16 v0, v0, 0xc0

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051753
    and-int/lit8 v0, v1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051754
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1051755
    :cond_6
    const v0, 0xd800

    if-lt v1, v0, :cond_7

    if-le v1, v8, :cond_8

    .line 1051756
    :cond_7
    shr-int/lit8 v0, v1, 0xc

    or-int/lit16 v0, v0, 0xe0

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051757
    shr-int/lit8 v0, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051758
    and-int/lit8 v0, v1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051759
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1051760
    :cond_8
    add-int/lit8 v0, p2, 0x1

    if-ge v0, p3, :cond_a

    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1051761
    :goto_2
    const v2, 0xdbff

    if-gt v1, v2, :cond_9

    const v2, 0xdc00

    if-lt v0, v2, :cond_9

    if-le v0, v8, :cond_b

    .line 1051762
    :cond_9
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051763
    add-int/lit8 p2, p2, 0x1

    .line 1051764
    goto/16 :goto_0

    .line 1051765
    :cond_a
    const/4 v0, 0x0

    goto :goto_2

    .line 1051766
    :cond_b
    const/high16 v2, 0x10000

    const v3, -0xd801

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0xa

    const v3, -0xdc01

    and-int/2addr v0, v3

    or-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 1051767
    shr-int/lit8 v1, v0, 0x12

    or-int/lit16 v1, v1, 0xf0

    invoke-virtual {p0, v1}, LX/672;->b(I)LX/672;

    .line 1051768
    shr-int/lit8 v1, v0, 0xc

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, LX/672;->b(I)LX/672;

    .line 1051769
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, LX/672;->b(I)LX/672;

    .line 1051770
    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    .line 1051771
    add-int/lit8 p2, p2, 0x2

    .line 1051772
    goto/16 :goto_0

    .line 1051773
    :cond_c
    return-object p0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1051727
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051728
    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 1051889
    const/4 v0, 0x0

    .line 1051890
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 1051891
    array-length v1, p1

    sub-int/2addr v1, v0

    invoke-virtual {p0, p1, v0, v1}, LX/672;->a([BII)I

    move-result v1

    .line 1051892
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051893
    :cond_0
    add-int/2addr v0, v1

    .line 1051894
    goto :goto_0

    .line 1051895
    :cond_1
    return-void
.end method

.method public final a_(LX/672;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1051862
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051863
    :cond_0
    if-ne p1, p0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051864
    :cond_1
    iget-wide v0, p1, LX/672;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051865
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    .line 1051866
    iget-object v0, p1, LX/672;->a:LX/67F;

    iget v0, v0, LX/67F;->c:I

    iget-object v1, p1, LX/672;->a:LX/67F;

    iget v1, v1, LX/67F;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_6

    .line 1051867
    iget-object v0, p0, LX/672;->a:LX/67F;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/672;->a:LX/67F;

    iget-object v0, v0, LX/67F;->g:LX/67F;

    move-object v1, v0

    .line 1051868
    :goto_1
    if-eqz v1, :cond_5

    iget-boolean v0, v1, LX/67F;->e:Z

    if-eqz v0, :cond_5

    iget v0, v1, LX/67F;->c:I

    int-to-long v4, v0

    add-long/2addr v4, p2

    iget-boolean v0, v1, LX/67F;->d:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    int-to-long v6, v0

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x2000

    cmp-long v0, v4, v6

    if-gtz v0, :cond_5

    .line 1051869
    iget-object v0, p1, LX/672;->a:LX/67F;

    long-to-int v2, p2

    invoke-virtual {v0, v1, v2}, LX/67F;->a(LX/67F;I)V

    .line 1051870
    iget-wide v0, p1, LX/672;->b:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, LX/672;->b:J

    .line 1051871
    iget-wide v0, p0, LX/672;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051872
    :cond_2
    return-void

    .line 1051873
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 1051874
    :cond_4
    iget v0, v1, LX/67F;->b:I

    goto :goto_2

    .line 1051875
    :cond_5
    iget-object v0, p1, LX/672;->a:LX/67F;

    long-to-int v1, p2

    invoke-virtual {v0, v1}, LX/67F;->a(I)LX/67F;

    move-result-object v0

    iput-object v0, p1, LX/672;->a:LX/67F;

    .line 1051876
    :cond_6
    iget-object v0, p1, LX/672;->a:LX/67F;

    .line 1051877
    iget v1, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    .line 1051878
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v1

    iput-object v1, p1, LX/672;->a:LX/67F;

    .line 1051879
    iget-object v1, p0, LX/672;->a:LX/67F;

    if-nez v1, :cond_7

    .line 1051880
    iput-object v0, p0, LX/672;->a:LX/67F;

    .line 1051881
    iget-object v0, p0, LX/672;->a:LX/67F;

    iget-object v1, p0, LX/672;->a:LX/67F;

    iget-object v6, p0, LX/672;->a:LX/67F;

    iput-object v6, v1, LX/67F;->g:LX/67F;

    iput-object v6, v0, LX/67F;->f:LX/67F;

    .line 1051882
    :goto_3
    iget-wide v0, p1, LX/672;->b:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, LX/672;->b:J

    .line 1051883
    iget-wide v0, p0, LX/672;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/672;->b:J

    .line 1051884
    sub-long/2addr p2, v4

    .line 1051885
    goto :goto_0

    .line 1051886
    :cond_7
    iget-object v1, p0, LX/672;->a:LX/67F;

    iget-object v1, v1, LX/67F;->g:LX/67F;

    .line 1051887
    invoke-virtual {v1, v0}, LX/67F;->a(LX/67F;)LX/67F;

    move-result-object v0

    .line 1051888
    invoke-virtual {v0}, LX/67F;->b()V

    goto :goto_3
.end method

.method public final b(J)B
    .locals 7

    .prologue
    .line 1051932
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051933
    iget-object v0, p0, LX/672;->a:LX/67F;

    .line 1051934
    :goto_0
    iget v1, v0, LX/67F;->c:I

    iget v2, v0, LX/67F;->b:I

    sub-int/2addr v1, v2

    .line 1051935
    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    iget-object v1, v0, LX/67F;->a:[B

    iget v0, v0, LX/67F;->b:I

    long-to-int v2, p1

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    .line 1051936
    :cond_0
    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 1051937
    iget-object v0, v0, LX/67F;->f:LX/67F;

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1051931
    iget-wide v0, p0, LX/672;->b:J

    return-wide v0
.end method

.method public final synthetic b(LX/673;)LX/670;
    .locals 1

    .prologue
    .line 1051930
    invoke-virtual {p0, p1}, LX/672;->a(LX/673;)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;)LX/670;
    .locals 1

    .prologue
    .line 1051929
    invoke-virtual {p0, p1}, LX/672;->a(Ljava/lang/String;)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/672;
    .locals 4

    .prologue
    .line 1051925
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/672;->e(I)LX/67F;

    move-result-object v0

    .line 1051926
    iget-object v1, v0, LX/67F;->a:[B

    iget v2, v0, LX/67F;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, LX/67F;->c:I

    int-to-byte v0, p1

    aput-byte v0, v1, v2

    .line 1051927
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051928
    return-object p0
.end method

.method public final b([B)LX/672;
    .locals 2

    .prologue
    .line 1051923
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051924
    :cond_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/672;->b([BII)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final b([BII)LX/672;
    .locals 6

    .prologue
    .line 1051938
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051939
    :cond_0
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051940
    add-int v0, p2, p3

    .line 1051941
    :goto_0
    if-ge p2, v0, :cond_1

    .line 1051942
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/672;->e(I)LX/67F;

    move-result-object v1

    .line 1051943
    sub-int v2, v0, p2

    iget v3, v1, LX/67F;->c:I

    rsub-int v3, v3, 0x2000

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1051944
    iget-object v3, v1, LX/67F;->a:[B

    iget v4, v1, LX/67F;->c:I

    invoke-static {p1, p2, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1051945
    add-int/2addr p2, v2

    .line 1051946
    iget v3, v1, LX/67F;->c:I

    add-int/2addr v2, v3

    iput v2, v1, LX/67F;->c:I

    goto :goto_0

    .line 1051947
    :cond_1
    iget-wide v0, p0, LX/672;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051948
    return-object p0
.end method

.method public final b(LX/672;J)V
    .locals 2

    .prologue
    .line 1051918
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, v0, p2

    if-gez v0, :cond_0

    .line 1051919
    iget-wide v0, p0, LX/672;->b:J

    invoke-virtual {p1, p0, v0, v1}, LX/672;->a_(LX/672;J)V

    .line 1051920
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051921
    :cond_0
    invoke-virtual {p1, p0, p2, p3}, LX/672;->a_(LX/672;J)V

    .line 1051922
    return-void
.end method

.method public final synthetic c([B)LX/670;
    .locals 1

    .prologue
    .line 1051917
    invoke-virtual {p0, p1}, LX/672;->b([B)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([BII)LX/670;
    .locals 1

    .prologue
    .line 1051916
    invoke-virtual {p0, p1, p2, p3}, LX/672;->b([BII)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/672;
    .locals 0

    .prologue
    .line 1051915
    return-object p0
.end method

.method public final c(I)LX/672;
    .locals 5

    .prologue
    .line 1051907
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/672;->e(I)LX/67F;

    move-result-object v0

    .line 1051908
    iget-object v1, v0, LX/67F;->a:[B

    .line 1051909
    iget v2, v0, LX/67F;->c:I

    .line 1051910
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051911
    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051912
    iput v2, v0, LX/67F;->c:I

    .line 1051913
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051914
    return-object p0
.end method

.method public final c(J)LX/673;
    .locals 3

    .prologue
    .line 1051906
    new-instance v0, LX/673;

    invoke-virtual {p0, p1, p2}, LX/672;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    return-object v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1051896
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    .line 1051897
    iget-wide v2, p0, LX/672;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1051898
    :goto_0
    return-object v0

    .line 1051899
    :cond_0
    new-instance v0, LX/67F;

    iget-object v2, p0, LX/672;->a:LX/67F;

    invoke-direct {v0, v2}, LX/67F;-><init>(LX/67F;)V

    iput-object v0, v1, LX/672;->a:LX/67F;

    .line 1051900
    iget-object v0, v1, LX/672;->a:LX/67F;

    iget-object v2, v1, LX/672;->a:LX/67F;

    iget-object v3, v1, LX/672;->a:LX/67F;

    iput-object v3, v2, LX/67F;->g:LX/67F;

    iput-object v3, v0, LX/67F;->f:LX/67F;

    .line 1051901
    iget-object v0, p0, LX/672;->a:LX/67F;

    iget-object v0, v0, LX/67F;->f:LX/67F;

    :goto_1
    iget-object v2, p0, LX/672;->a:LX/67F;

    if-eq v0, v2, :cond_1

    .line 1051902
    iget-object v2, v1, LX/672;->a:LX/67F;

    iget-object v2, v2, LX/67F;->g:LX/67F;

    new-instance v3, LX/67F;

    invoke-direct {v3, v0}, LX/67F;-><init>(LX/67F;)V

    invoke-virtual {v2, v3}, LX/67F;->a(LX/67F;)LX/67F;

    .line 1051903
    iget-object v0, v0, LX/67F;->f:LX/67F;

    goto :goto_1

    .line 1051904
    :cond_1
    iget-wide v2, p0, LX/672;->b:J

    iput-wide v2, v1, LX/672;->b:J

    move-object v0, v1

    .line 1051905
    goto :goto_0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 1051823
    return-void
.end method

.method public final d()LX/670;
    .locals 0

    .prologue
    .line 1051861
    return-object p0
.end method

.method public final d(I)LX/672;
    .locals 5

    .prologue
    .line 1051564
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/672;->e(I)LX/67F;

    move-result-object v0

    .line 1051565
    iget-object v1, v0, LX/67F;->a:[B

    .line 1051566
    iget v2, v0, LX/67F;->c:I

    .line 1051567
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051568
    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051569
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051570
    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051571
    iput v2, v0, LX/67F;->c:I

    .line 1051572
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051573
    return-object p0
.end method

.method public final d(J)Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/16 v2, 0x1

    .line 1051558
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    sub-long v0, p1, v2

    invoke-virtual {p0, v0, v1}, LX/672;->b(J)B

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 1051559
    sub-long v0, p1, v2

    invoke-direct {p0, v0, v1}, LX/672;->m(J)Ljava/lang/String;

    move-result-object v0

    .line 1051560
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, LX/672;->f(J)V

    .line 1051561
    :goto_0
    return-object v0

    .line 1051562
    :cond_0
    invoke-direct {p0, p1, p2}, LX/672;->m(J)Ljava/lang/String;

    move-result-object v0

    .line 1051563
    invoke-virtual {p0, v2, v3}, LX/672;->f(J)V

    goto :goto_0
.end method

.method public final e(I)LX/67F;
    .locals 3

    .prologue
    const/16 v2, 0x2000

    .line 1051550
    if-lez p1, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1051551
    :cond_1
    iget-object v0, p0, LX/672;->a:LX/67F;

    if-nez v0, :cond_3

    .line 1051552
    invoke-static {}, LX/67G;->a()LX/67F;

    move-result-object v0

    iput-object v0, p0, LX/672;->a:LX/67F;

    .line 1051553
    iget-object v1, p0, LX/672;->a:LX/67F;

    iget-object v2, p0, LX/672;->a:LX/67F;

    iget-object v0, p0, LX/672;->a:LX/67F;

    iput-object v0, v2, LX/67F;->g:LX/67F;

    iput-object v0, v1, LX/67F;->f:LX/67F;

    .line 1051554
    :cond_2
    :goto_0
    return-object v0

    .line 1051555
    :cond_3
    iget-object v0, p0, LX/672;->a:LX/67F;

    iget-object v0, v0, LX/67F;->g:LX/67F;

    .line 1051556
    iget v1, v0, LX/67F;->c:I

    add-int/2addr v1, p1

    if-gt v1, v2, :cond_4

    iget-boolean v1, v0, LX/67F;->e:Z

    if-nez v1, :cond_2

    .line 1051557
    :cond_4
    invoke-static {}, LX/67G;->a()LX/67F;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/67F;->a(LX/67F;)LX/67F;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 1051549
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(J)[B
    .locals 7

    .prologue
    .line 1051543
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1051544
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 1051545
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051546
    :cond_0
    long-to-int v0, p1

    new-array v0, v0, [B

    .line 1051547
    invoke-virtual {p0, v0}, LX/672;->a([B)V

    .line 1051548
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    const-wide/16 v0, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1051520
    if-ne p0, p1, :cond_0

    move v0, v6

    .line 1051521
    :goto_0
    return v0

    .line 1051522
    :cond_0
    instance-of v2, p1, LX/672;

    if-nez v2, :cond_1

    move v0, v7

    goto :goto_0

    .line 1051523
    :cond_1
    check-cast p1, LX/672;

    .line 1051524
    iget-wide v2, p0, LX/672;->b:J

    iget-wide v4, p1, LX/672;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move v0, v7

    goto :goto_0

    .line 1051525
    :cond_2
    iget-wide v2, p0, LX/672;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    move v0, v6

    goto :goto_0

    .line 1051526
    :cond_3
    iget-object v5, p0, LX/672;->a:LX/67F;

    .line 1051527
    iget-object v4, p1, LX/672;->a:LX/67F;

    .line 1051528
    iget v3, v5, LX/67F;->b:I

    .line 1051529
    iget v2, v4, LX/67F;->b:I

    .line 1051530
    :goto_1
    iget-wide v8, p0, LX/672;->b:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_8

    .line 1051531
    iget v8, v5, LX/67F;->c:I

    sub-int/2addr v8, v3

    iget v9, v4, LX/67F;->c:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-long v10, v8

    move v8, v7

    .line 1051532
    :goto_2
    int-to-long v12, v8

    cmp-long v9, v12, v10

    if-gez v9, :cond_5

    .line 1051533
    iget-object v12, v5, LX/67F;->a:[B

    add-int/lit8 v9, v3, 0x1

    aget-byte v12, v12, v3

    iget-object v13, v4, LX/67F;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v13, v2

    if-eq v12, v2, :cond_4

    move v0, v7

    goto :goto_0

    .line 1051534
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v3

    move v3, v9

    goto :goto_2

    .line 1051535
    :cond_5
    iget v8, v5, LX/67F;->c:I

    if-ne v3, v8, :cond_6

    .line 1051536
    iget-object v5, v5, LX/67F;->f:LX/67F;

    .line 1051537
    iget v3, v5, LX/67F;->b:I

    .line 1051538
    :cond_6
    iget v8, v4, LX/67F;->c:I

    if-ne v2, v8, :cond_7

    .line 1051539
    iget-object v4, v4, LX/67F;->f:LX/67F;

    .line 1051540
    iget v2, v4, LX/67F;->b:I

    .line 1051541
    :cond_7
    add-long/2addr v0, v10

    goto :goto_1

    :cond_8
    move v0, v6

    .line 1051542
    goto :goto_0
.end method

.method public final synthetic f(I)LX/670;
    .locals 1

    .prologue
    .line 1051519
    invoke-virtual {p0, p1}, LX/672;->d(I)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 1051518
    new-instance v0, LX/66z;

    invoke-direct {v0, p0}, LX/66z;-><init>(LX/672;)V

    return-object v0
.end method

.method public final f(J)V
    .locals 7

    .prologue
    .line 1051507
    :cond_0
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 1051508
    iget-object v0, p0, LX/672;->a:LX/67F;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051509
    :cond_1
    iget-object v0, p0, LX/672;->a:LX/67F;

    iget v0, v0, LX/67F;->c:I

    iget-object v1, p0, LX/672;->a:LX/67F;

    iget v1, v1, LX/67F;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1051510
    iget-wide v2, p0, LX/672;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/672;->b:J

    .line 1051511
    int-to-long v2, v0

    sub-long/2addr p1, v2

    .line 1051512
    iget-object v1, p0, LX/672;->a:LX/67F;

    iget v2, v1, LX/67F;->b:I

    add-int/2addr v0, v2

    iput v0, v1, LX/67F;->b:I

    .line 1051513
    iget-object v0, p0, LX/672;->a:LX/67F;

    iget v0, v0, LX/67F;->b:I

    iget-object v1, p0, LX/672;->a:LX/67F;

    iget v1, v1, LX/67F;->c:I

    if-ne v0, v1, :cond_0

    .line 1051514
    iget-object v0, p0, LX/672;->a:LX/67F;

    .line 1051515
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v1

    iput-object v1, p0, LX/672;->a:LX/67F;

    .line 1051516
    invoke-static {v0}, LX/67G;->a(LX/67F;)V

    goto :goto_0

    .line 1051517
    :cond_2
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 1051506
    return-void
.end method

.method public final g()J
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1051500
    iget-wide v0, p0, LX/672;->b:J

    .line 1051501
    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 1051502
    :cond_0
    :goto_0
    return-wide v0

    .line 1051503
    :cond_1
    iget-object v2, p0, LX/672;->a:LX/67F;

    iget-object v2, v2, LX/67F;->g:LX/67F;

    .line 1051504
    iget v3, v2, LX/67F;->c:I

    const/16 v4, 0x2000

    if-ge v3, v4, :cond_0

    iget-boolean v3, v2, LX/67F;->e:Z

    if-eqz v3, :cond_0

    .line 1051505
    iget v3, v2, LX/67F;->c:I

    iget v2, v2, LX/67F;->b:I

    sub-int v2, v3, v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public final synthetic g(I)LX/670;
    .locals 1

    .prologue
    .line 1051499
    invoke-virtual {p0, p1}, LX/672;->c(I)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final g(J)LX/672;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const-wide/16 v6, 0xff

    .line 1051485
    invoke-virtual {p0, v8}, LX/672;->e(I)LX/67F;

    move-result-object v0

    .line 1051486
    iget-object v1, v0, LX/67F;->a:[B

    .line 1051487
    iget v2, v0, LX/67F;->c:I

    .line 1051488
    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x38

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051489
    add-int/lit8 v2, v3, 0x1

    const/16 v4, 0x30

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051490
    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x28

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051491
    add-int/lit8 v2, v3, 0x1

    const/16 v4, 0x20

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051492
    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x18

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051493
    add-int/lit8 v2, v3, 0x1

    const/16 v4, 0x10

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051494
    add-int/lit8 v3, v2, 0x1

    ushr-long v4, p1, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 1051495
    add-int/lit8 v2, v3, 0x1

    and-long v4, p1, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 1051496
    iput v2, v0, LX/67F;->c:I

    .line 1051497
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x8

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    .line 1051498
    return-object p0
.end method

.method public final h()B
    .locals 10

    .prologue
    .line 1051473
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051474
    :cond_0
    iget-object v0, p0, LX/672;->a:LX/67F;

    .line 1051475
    iget v1, v0, LX/67F;->b:I

    .line 1051476
    iget v2, v0, LX/67F;->c:I

    .line 1051477
    iget-object v3, v0, LX/67F;->a:[B

    .line 1051478
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    .line 1051479
    iget-wide v6, p0, LX/672;->b:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    iput-wide v6, p0, LX/672;->b:J

    .line 1051480
    if-ne v4, v2, :cond_1

    .line 1051481
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v2

    iput-object v2, p0, LX/672;->a:LX/67F;

    .line 1051482
    invoke-static {v0}, LX/67G;->a(LX/67F;)V

    .line 1051483
    :goto_0
    return v1

    .line 1051484
    :cond_1
    iput v4, v0, LX/67F;->b:I

    goto :goto_0
.end method

.method public final synthetic h(I)LX/670;
    .locals 1

    .prologue
    .line 1051472
    invoke-virtual {p0, p1}, LX/672;->b(I)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final h(J)LX/672;
    .locals 11

    .prologue
    .line 1051448
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1051449
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    move-result-object p0

    .line 1051450
    :goto_0
    return-object p0

    .line 1051451
    :cond_0
    const/4 v0, 0x0

    .line 1051452
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_17

    .line 1051453
    neg-long v2, p1

    .line 1051454
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 1051455
    const-string v0, "-9223372036854775808"

    invoke-virtual {p0, v0}, LX/672;->a(Ljava/lang/String;)LX/672;

    move-result-object p0

    goto :goto_0

    .line 1051456
    :cond_1
    const/4 v0, 0x1

    move v4, v0

    .line 1051457
    :goto_1
    const-wide/32 v0, 0x5f5e100

    cmp-long v0, v2, v0

    if-gez v0, :cond_a

    const-wide/16 v0, 0x2710

    cmp-long v0, v2, v0

    if-gez v0, :cond_6

    const-wide/16 v0, 0x64

    cmp-long v0, v2, v0

    if-gez v0, :cond_4

    const-wide/16 v0, 0xa

    cmp-long v0, v2, v0

    if-gez v0, :cond_3

    const/4 v0, 0x1

    .line 1051458
    :goto_2
    if-eqz v4, :cond_2

    .line 1051459
    add-int/lit8 v0, v0, 0x1

    .line 1051460
    :cond_2
    invoke-virtual {p0, v0}, LX/672;->e(I)LX/67F;

    move-result-object v5

    .line 1051461
    iget-object v6, v5, LX/67F;->a:[B

    .line 1051462
    iget v1, v5, LX/67F;->c:I

    add-int/2addr v1, v0

    .line 1051463
    :goto_3
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-eqz v7, :cond_15

    .line 1051464
    const-wide/16 v8, 0xa

    rem-long v8, v2, v8

    long-to-int v7, v8

    .line 1051465
    add-int/lit8 v1, v1, -0x1

    sget-object v8, LX/672;->c:[B

    aget-byte v7, v8, v7

    aput-byte v7, v6, v1

    .line 1051466
    const-wide/16 v8, 0xa

    div-long/2addr v2, v8

    goto :goto_3

    .line 1051467
    :cond_3
    const/4 v0, 0x2

    goto :goto_2

    :cond_4
    const-wide/16 v0, 0x3e8

    cmp-long v0, v2, v0

    if-gez v0, :cond_5

    const/4 v0, 0x3

    goto :goto_2

    :cond_5
    const/4 v0, 0x4

    goto :goto_2

    :cond_6
    const-wide/32 v0, 0xf4240

    cmp-long v0, v2, v0

    if-gez v0, :cond_8

    const-wide/32 v0, 0x186a0

    cmp-long v0, v2, v0

    if-gez v0, :cond_7

    const/4 v0, 0x5

    goto :goto_2

    :cond_7
    const/4 v0, 0x6

    goto :goto_2

    :cond_8
    const-wide/32 v0, 0x989680

    cmp-long v0, v2, v0

    if-gez v0, :cond_9

    const/4 v0, 0x7

    goto :goto_2

    :cond_9
    const/16 v0, 0x8

    goto :goto_2

    :cond_a
    const-wide v0, 0xe8d4a51000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_e

    const-wide v0, 0x2540be400L

    cmp-long v0, v2, v0

    if-gez v0, :cond_c

    const-wide/32 v0, 0x3b9aca00

    cmp-long v0, v2, v0

    if-gez v0, :cond_b

    const/16 v0, 0x9

    goto :goto_2

    :cond_b
    const/16 v0, 0xa

    goto :goto_2

    :cond_c
    const-wide v0, 0x174876e800L

    cmp-long v0, v2, v0

    if-gez v0, :cond_d

    const/16 v0, 0xb

    goto :goto_2

    :cond_d
    const/16 v0, 0xc

    goto :goto_2

    :cond_e
    const-wide v0, 0x38d7ea4c68000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_11

    const-wide v0, 0x9184e72a000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_f

    const/16 v0, 0xd

    goto/16 :goto_2

    :cond_f
    const-wide v0, 0x5af3107a4000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_10

    const/16 v0, 0xe

    goto/16 :goto_2

    :cond_10
    const/16 v0, 0xf

    goto/16 :goto_2

    :cond_11
    const-wide v0, 0x16345785d8a0000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_13

    const-wide v0, 0x2386f26fc10000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_12

    const/16 v0, 0x10

    goto/16 :goto_2

    :cond_12
    const/16 v0, 0x11

    goto/16 :goto_2

    :cond_13
    const-wide v0, 0xde0b6b3a7640000L

    cmp-long v0, v2, v0

    if-gez v0, :cond_14

    const/16 v0, 0x12

    goto/16 :goto_2

    :cond_14
    const/16 v0, 0x13

    goto/16 :goto_2

    .line 1051468
    :cond_15
    if-eqz v4, :cond_16

    .line 1051469
    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x2d

    aput-byte v2, v6, v1

    .line 1051470
    :cond_16
    iget v1, v5, LX/67F;->c:I

    add-int/2addr v1, v0

    iput v1, v5, LX/67F;->c:I

    .line 1051471
    iget-wide v2, p0, LX/672;->b:J

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    goto/16 :goto_0

    :cond_17
    move v4, v0

    move-wide v2, p1

    goto/16 :goto_1
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 1051439
    iget-object v1, p0, LX/672;->a:LX/67F;

    .line 1051440
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1051441
    :goto_0
    return v0

    .line 1051442
    :cond_0
    const/4 v0, 0x1

    .line 1051443
    :cond_1
    iget v2, v1, LX/67F;->b:I

    iget v4, v1, LX/67F;->c:I

    :goto_1
    if-ge v2, v4, :cond_2

    .line 1051444
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, v1, LX/67F;->a:[B

    aget-byte v3, v3, v2

    add-int/2addr v3, v0

    .line 1051445
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    .line 1051446
    :cond_2
    iget-object v1, v1, LX/67F;->f:LX/67F;

    .line 1051447
    iget-object v2, p0, LX/672;->a:LX/67F;

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method public final i(J)LX/672;
    .locals 9

    .prologue
    .line 1051650
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1051651
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, LX/672;->b(I)LX/672;

    move-result-object p0

    .line 1051652
    :goto_0
    return-object p0

    .line 1051653
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->highestOneBit(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    add-int/lit8 v1, v0, 0x1

    .line 1051654
    invoke-virtual {p0, v1}, LX/672;->e(I)LX/67F;

    move-result-object v2

    .line 1051655
    iget-object v3, v2, LX/67F;->a:[B

    .line 1051656
    iget v0, v2, LX/67F;->c:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v4, v2, LX/67F;->c:I

    :goto_1
    if-lt v0, v4, :cond_1

    .line 1051657
    sget-object v5, LX/672;->c:[B

    const-wide/16 v6, 0xf

    and-long/2addr v6, p1

    long-to-int v6, v6

    aget-byte v5, v5, v6

    aput-byte v5, v3, v0

    .line 1051658
    const/4 v5, 0x4

    ushr-long/2addr p1, v5

    .line 1051659
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1051660
    :cond_1
    iget v0, v2, LX/67F;->c:I

    add-int/2addr v0, v1

    iput v0, v2, LX/67F;->c:I

    .line 1051661
    iget-wide v2, p0, LX/672;->b:J

    int-to-long v0, v1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/672;->b:J

    goto :goto_0
.end method

.method public final i()S
    .locals 10

    .prologue
    const-wide/16 v8, 0x2

    .line 1051705
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 2: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/672;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051706
    :cond_0
    iget-object v0, p0, LX/672;->a:LX/67F;

    .line 1051707
    iget v1, v0, LX/67F;->b:I

    .line 1051708
    iget v2, v0, LX/67F;->c:I

    .line 1051709
    sub-int v3, v2, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 1051710
    invoke-virtual {p0}, LX/672;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 1051711
    invoke-virtual {p0}, LX/672;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 1051712
    int-to-short v0, v0

    .line 1051713
    :goto_0
    return v0

    .line 1051714
    :cond_1
    iget-object v3, v0, LX/67F;->a:[B

    .line 1051715
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    .line 1051716
    iget-wide v6, p0, LX/672;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, LX/672;->b:J

    .line 1051717
    if-ne v5, v2, :cond_2

    .line 1051718
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v2

    iput-object v2, p0, LX/672;->a:LX/67F;

    .line 1051719
    invoke-static {v0}, LX/67G;->a(LX/67F;)V

    .line 1051720
    :goto_1
    int-to-short v0, v1

    goto :goto_0

    .line 1051721
    :cond_2
    iput v5, v0, LX/67F;->b:I

    goto :goto_1
.end method

.method public final j()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x4

    .line 1051688
    iget-wide v0, p0, LX/672;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 4: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/672;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051689
    :cond_0
    iget-object v1, p0, LX/672;->a:LX/67F;

    .line 1051690
    iget v0, v1, LX/67F;->b:I

    .line 1051691
    iget v2, v1, LX/67F;->c:I

    .line 1051692
    sub-int v3, v2, v0

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 1051693
    invoke-virtual {p0}, LX/672;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 1051694
    invoke-virtual {p0}, LX/672;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 1051695
    invoke-virtual {p0}, LX/672;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 1051696
    invoke-virtual {p0}, LX/672;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 1051697
    :goto_0
    return v0

    .line 1051698
    :cond_1
    iget-object v3, v1, LX/67F;->a:[B

    .line 1051699
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    add-int/lit8 v4, v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v0, v5

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 1051700
    iget-wide v6, p0, LX/672;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, LX/672;->b:J

    .line 1051701
    if-ne v5, v2, :cond_2

    .line 1051702
    invoke-virtual {v1}, LX/67F;->a()LX/67F;

    move-result-object v2

    iput-object v2, p0, LX/672;->a:LX/67F;

    .line 1051703
    invoke-static {v1}, LX/67G;->a(LX/67F;)V

    goto :goto_0

    .line 1051704
    :cond_2
    iput v5, v1, LX/67F;->b:I

    goto :goto_0
.end method

.method public final synthetic j(J)LX/670;
    .locals 1

    .prologue
    .line 1051687
    invoke-virtual {p0, p1, p2}, LX/672;->i(J)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final k()J
    .locals 12

    .prologue
    .line 1051672
    iget-wide v0, p0, LX/672;->b:J

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 8: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/672;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051673
    :cond_0
    iget-object v2, p0, LX/672;->a:LX/67F;

    .line 1051674
    iget v0, v2, LX/67F;->b:I

    .line 1051675
    iget v3, v2, LX/67F;->c:I

    .line 1051676
    sub-int v1, v3, v0

    const/16 v4, 0x8

    if-ge v1, v4, :cond_1

    .line 1051677
    invoke-virtual {p0}, LX/672;->j()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    .line 1051678
    invoke-virtual {p0}, LX/672;->j()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 1051679
    :goto_0
    return-wide v0

    .line 1051680
    :cond_1
    iget-object v1, v2, LX/67F;->a:[B

    .line 1051681
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v1, v0

    int-to-long v6, v0

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    const/16 v0, 0x38

    shl-long/2addr v6, v0

    add-int/lit8 v0, v4, 0x1

    aget-byte v4, v1, v4

    int-to-long v4, v4

    const-wide/16 v8, 0xff

    and-long/2addr v4, v8

    const/16 v8, 0x30

    shl-long/2addr v4, v8

    or-long/2addr v4, v6

    add-int/lit8 v6, v0, 0x1

    aget-byte v0, v1, v0

    int-to-long v8, v0

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v0, 0x28

    shl-long/2addr v8, v0

    or-long/2addr v4, v8

    add-int/lit8 v0, v6, 0x1

    aget-byte v6, v1, v6

    int-to-long v6, v6

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    const/16 v8, 0x20

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    add-int/lit8 v6, v0, 0x1

    aget-byte v0, v1, v0

    int-to-long v8, v0

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v0, 0x18

    shl-long/2addr v8, v0

    or-long/2addr v4, v8

    add-int/lit8 v0, v6, 0x1

    aget-byte v6, v1, v6

    int-to-long v6, v6

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    const/16 v8, 0x10

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    add-int/lit8 v6, v0, 0x1

    aget-byte v0, v1, v0

    int-to-long v8, v0

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v0, 0x8

    shl-long/2addr v8, v0

    or-long/2addr v4, v8

    add-int/lit8 v7, v6, 0x1

    aget-byte v0, v1, v6

    int-to-long v0, v0

    const-wide/16 v8, 0xff

    and-long/2addr v0, v8

    or-long/2addr v0, v4

    .line 1051682
    iget-wide v4, p0, LX/672;->b:J

    const-wide/16 v8, 0x8

    sub-long/2addr v4, v8

    iput-wide v4, p0, LX/672;->b:J

    .line 1051683
    if-ne v7, v3, :cond_2

    .line 1051684
    invoke-virtual {v2}, LX/67F;->a()LX/67F;

    move-result-object v3

    iput-object v3, p0, LX/672;->a:LX/67F;

    .line 1051685
    invoke-static {v2}, LX/67G;->a(LX/67F;)V

    goto :goto_0

    .line 1051686
    :cond_2
    iput v7, v2, LX/67F;->b:I

    goto :goto_0
.end method

.method public final synthetic k(J)LX/670;
    .locals 1

    .prologue
    .line 1051671
    invoke-virtual {p0, p1, p2}, LX/672;->h(J)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l(J)LX/670;
    .locals 1

    .prologue
    .line 1051670
    invoke-virtual {p0, p1, p2}, LX/672;->g(J)LX/672;

    move-result-object v0

    return-object v0
.end method

.method public final l()S
    .locals 2

    .prologue
    .line 1051665
    invoke-virtual {p0}, LX/672;->i()S

    move-result v0

    .line 1051666
    const v1, 0xffff

    and-int/2addr v1, v0

    .line 1051667
    const p0, 0xff00

    and-int/2addr p0, v1

    ushr-int/lit8 p0, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v1, p0

    .line 1051668
    int-to-short v1, v1

    move v0, v1

    .line 1051669
    return v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 1051662
    invoke-virtual {p0}, LX/672;->j()I

    move-result v0

    .line 1051663
    const/high16 v1, -0x1000000

    and-int/2addr v1, v0

    ushr-int/lit8 v1, v1, 0x18

    const/high16 p0, 0xff0000

    and-int/2addr p0, v0

    ushr-int/lit8 p0, p0, 0x8

    or-int/2addr v1, p0

    const p0, 0xff00

    and-int/2addr p0, v0

    shl-int/lit8 p0, p0, 0x8

    or-int/2addr v1, p0

    and-int/lit16 p0, v0, 0xff

    shl-int/lit8 p0, p0, 0x18

    or-int/2addr v1, p0

    move v0, v1

    .line 1051664
    return v0
.end method

.method public final n()J
    .locals 18

    .prologue
    .line 1051574
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/672;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "size == 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1051575
    :cond_0
    const-wide/16 v4, 0x0

    .line 1051576
    const/4 v3, 0x0

    .line 1051577
    const/4 v2, 0x0

    .line 1051578
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, LX/672;->a:LX/67F;

    .line 1051579
    iget-object v11, v10, LX/67F;->a:[B

    .line 1051580
    iget v6, v10, LX/67F;->b:I

    .line 1051581
    iget v12, v10, LX/67F;->c:I

    move v7, v6

    .line 1051582
    :goto_0
    if-ge v7, v12, :cond_6

    .line 1051583
    aget-byte v8, v11, v7

    .line 1051584
    const/16 v6, 0x30

    if-lt v8, v6, :cond_2

    const/16 v6, 0x39

    if-gt v8, v6, :cond_2

    .line 1051585
    add-int/lit8 v6, v8, -0x30

    .line 1051586
    :goto_1
    const-wide/high16 v14, -0x1000000000000000L    # -3.105036184601418E231

    and-long/2addr v14, v4

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-eqz v9, :cond_8

    .line 1051587
    new-instance v2, LX/672;

    invoke-direct {v2}, LX/672;-><init>()V

    invoke-virtual {v2, v4, v5}, LX/672;->i(J)LX/672;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/672;->b(I)LX/672;

    move-result-object v2

    .line 1051588
    new-instance v3, Ljava/lang/NumberFormatException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Number too large: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/672;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1051589
    :cond_2
    const/16 v6, 0x61

    if-lt v8, v6, :cond_3

    const/16 v6, 0x66

    if-gt v8, v6, :cond_3

    .line 1051590
    add-int/lit8 v6, v8, -0x61

    add-int/lit8 v6, v6, 0xa

    goto :goto_1

    .line 1051591
    :cond_3
    const/16 v6, 0x41

    if-lt v8, v6, :cond_4

    const/16 v6, 0x46

    if-gt v8, v6, :cond_4

    .line 1051592
    add-int/lit8 v6, v8, -0x41

    add-int/lit8 v6, v6, 0xa

    goto :goto_1

    .line 1051593
    :cond_4
    if-nez v3, :cond_5

    .line 1051594
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected leading [0-9a-fA-F] character but was 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051595
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1051596
    :cond_5
    const/4 v2, 0x1

    .line 1051597
    :cond_6
    if-ne v7, v12, :cond_9

    .line 1051598
    invoke-virtual {v10}, LX/67F;->a()LX/67F;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, LX/672;->a:LX/67F;

    .line 1051599
    invoke-static {v10}, LX/67G;->a(LX/67F;)V

    .line 1051600
    :goto_2
    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v6, v0, LX/672;->a:LX/67F;

    if-nez v6, :cond_1

    .line 1051601
    :cond_7
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/672;->b:J

    int-to-long v2, v3

    sub-long v2, v6, v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/672;->b:J

    .line 1051602
    return-wide v4

    .line 1051603
    :cond_8
    const/4 v8, 0x4

    shl-long/2addr v4, v8

    .line 1051604
    int-to-long v8, v6

    or-long/2addr v8, v4

    .line 1051605
    add-int/lit8 v4, v7, 0x1

    add-int/lit8 v3, v3, 0x1

    move v7, v4

    move-wide v4, v8

    goto/16 :goto_0

    .line 1051606
    :cond_9
    iput v7, v10, LX/67F;->b:I

    goto :goto_2
.end method

.method public final o()LX/673;
    .locals 2

    .prologue
    .line 1051628
    new-instance v0, LX/673;

    invoke-virtual {p0}, LX/672;->r()[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1051625
    :try_start_0
    iget-wide v0, p0, LX/672;->b:J

    sget-object v2, LX/67J;->a:Ljava/nio/charset/Charset;

    invoke-direct {p0, v0, v1, v2}, LX/672;->a(JLjava/nio/charset/Charset;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1051626
    :catch_0
    move-exception v0

    .line 1051627
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final q()Ljava/lang/String;
    .locals 10

    .prologue
    .line 1051616
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/672;->a(B)J

    move-result-wide v0

    .line 1051617
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1051618
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    .line 1051619
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x20

    iget-wide v6, p0, LX/672;->b:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/672;->a(LX/672;JJ)LX/672;

    .line 1051620
    new-instance v0, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\\n not found: size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051621
    iget-wide v8, p0, LX/672;->b:J

    move-wide v4, v8

    .line 1051622
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " content="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1051623
    invoke-virtual {v1}, LX/672;->o()LX/673;

    move-result-object v1

    invoke-virtual {v1}, LX/673;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u2026"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051624
    :cond_0
    invoke-virtual {p0, v0, v1}, LX/672;->d(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()[B
    .locals 2

    .prologue
    .line 1051613
    :try_start_0
    iget-wide v0, p0, LX/672;->b:J

    invoke-virtual {p0, v0, v1}, LX/672;->e(J)[B
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1051614
    :catch_0
    move-exception v0

    .line 1051615
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 1051609
    :try_start_0
    iget-wide v0, p0, LX/672;->b:J

    invoke-virtual {p0, v0, v1}, LX/672;->f(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1051610
    return-void

    .line 1051611
    :catch_0
    move-exception v0

    .line 1051612
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final t()LX/670;
    .locals 1

    .prologue
    .line 1051608
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051607
    invoke-direct {p0}, LX/672;->w()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
