.class public LX/6N3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082250
    iput-object p1, p0, LX/6N3;->a:LX/0Zb;

    .line 1082251
    return-void
.end method

.method public static a(LX/0QB;)LX/6N3;
    .locals 4

    .prologue
    .line 1082252
    const-class v1, LX/6N3;

    monitor-enter v1

    .line 1082253
    :try_start_0
    sget-object v0, LX/6N3;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1082254
    sput-object v2, LX/6N3;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1082255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1082257
    new-instance p0, LX/6N3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/6N3;-><init>(LX/0Zb;)V

    .line 1082258
    move-object v0, p0

    .line 1082259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1082260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6N3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1082261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1082262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1082263
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "invalid_contact_field"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1082264
    const-string v1, "contact_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "contact_field"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1082265
    const-string v2, "contacts_iterator"

    move-object v2, v2

    .line 1082266
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1082267
    iget-object v1, p0, LX/6N3;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1082268
    return-void
.end method
