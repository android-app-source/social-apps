.class public final LX/66f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:[C


# direct methods
.method public constructor <init>(Ljavax/security/auth/x500/X500Principal;)V
    .locals 1

    .prologue
    .line 1050657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050658
    const-string v0, "RFC2253"

    invoke-virtual {p1, v0}, Ljavax/security/auth/x500/X500Principal;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/66f;->a:Ljava/lang/String;

    .line 1050659
    iget-object v0, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, LX/66f;->b:I

    .line 1050660
    return-void
.end method

.method public static a(LX/66f;I)I
    .locals 8

    .prologue
    const/16 v7, 0x61

    const/16 v6, 0x46

    const/16 v5, 0x41

    const/16 v4, 0x39

    const/16 v3, 0x30

    .line 1050661
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, LX/66f;->b:I

    if-lt v0, v1, :cond_0

    .line 1050662
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050663
    :cond_0
    iget-object v0, p0, LX/66f;->g:[C

    aget-char v0, v0, p1

    .line 1050664
    if-lt v0, v3, :cond_1

    if-gt v0, v4, :cond_1

    .line 1050665
    add-int/lit8 v0, v0, -0x30

    .line 1050666
    :goto_0
    iget-object v1, p0, LX/66f;->g:[C

    add-int/lit8 v2, p1, 0x1

    aget-char v1, v1, v2

    .line 1050667
    if-lt v1, v3, :cond_4

    if-gt v1, v4, :cond_4

    .line 1050668
    add-int/lit8 v1, v1, -0x30

    .line 1050669
    :goto_1
    shl-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v1

    return v0

    .line 1050670
    :cond_1
    if-lt v0, v7, :cond_2

    const/16 v1, 0x66

    if-gt v0, v1, :cond_2

    .line 1050671
    add-int/lit8 v0, v0, -0x57

    goto :goto_0

    .line 1050672
    :cond_2
    if-lt v0, v5, :cond_3

    if-gt v0, v6, :cond_3

    .line 1050673
    add-int/lit8 v0, v0, -0x37

    goto :goto_0

    .line 1050674
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050675
    :cond_4
    if-lt v1, v7, :cond_5

    const/16 v2, 0x66

    if-gt v1, v2, :cond_5

    .line 1050676
    add-int/lit8 v1, v1, -0x57

    goto :goto_1

    .line 1050677
    :cond_5
    if-lt v1, v5, :cond_6

    if-gt v1, v6, :cond_6

    .line 1050678
    add-int/lit8 v1, v1, -0x37

    goto :goto_1

    .line 1050679
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/66f;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x3d

    const/16 v2, 0x20

    .line 1050680
    :goto_0
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto :goto_0

    .line 1050681
    :cond_0
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ne v0, v1, :cond_1

    .line 1050682
    const/4 v0, 0x0

    .line 1050683
    :goto_1
    return-object v0

    .line 1050684
    :cond_1
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->d:I

    .line 1050685
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050686
    :goto_2
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-eq v0, v3, :cond_2

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-eq v0, v2, :cond_2

    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto :goto_2

    .line 1050687
    :cond_2
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-lt v0, v1, :cond_3

    .line 1050688
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050689
    :cond_3
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->e:I

    .line 1050690
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_6

    .line 1050691
    :goto_3
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-eq v0, v3, :cond_4

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_4

    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto :goto_3

    .line 1050692
    :cond_4
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_5

    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ne v0, v1, :cond_6

    .line 1050693
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050694
    :cond_6
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050695
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_7

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-eq v0, v2, :cond_6

    .line 1050696
    :cond_7
    iget v0, p0, LX/66f;->e:I

    iget v1, p0, LX/66f;->d:I

    sub-int/2addr v0, v1

    const/4 v1, 0x4

    if-le v0, v1, :cond_b

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    add-int/lit8 v1, v1, 0x3

    aget-char v0, v0, v1

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_b

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_8

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_b

    :cond_8
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    add-int/lit8 v1, v1, 0x1

    aget-char v0, v0, v1

    const/16 v1, 0x49

    if-eq v0, v1, :cond_9

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    add-int/lit8 v1, v1, 0x1

    aget-char v0, v0, v1

    const/16 v1, 0x69

    if-ne v0, v1, :cond_b

    :cond_9
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    add-int/lit8 v1, v1, 0x2

    aget-char v0, v0, v1

    const/16 v1, 0x44

    if-eq v0, v1, :cond_a

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->d:I

    add-int/lit8 v1, v1, 0x2

    aget-char v0, v0, v1

    const/16 v1, 0x64

    if-ne v0, v1, :cond_b

    .line 1050697
    :cond_a
    iget v0, p0, LX/66f;->d:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/66f;->d:I

    .line 1050698
    :cond_b
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/66f;->g:[C

    iget v2, p0, LX/66f;->d:I

    iget v3, p0, LX/66f;->e:I

    iget v4, p0, LX/66f;->d:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_1
.end method

.method private static b(LX/66f;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1050699
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050700
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->d:I

    .line 1050701
    iget v0, p0, LX/66f;->d:I

    iput v0, p0, LX/66f;->e:I

    .line 1050702
    :goto_0
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ne v0, v1, :cond_0

    .line 1050703
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050704
    :cond_0
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    .line 1050705
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050706
    :goto_1
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_3

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto :goto_1

    .line 1050707
    :cond_1
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_2

    .line 1050708
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->e:I

    invoke-static {p0}, LX/66f;->e(LX/66f;)C

    move-result v2

    aput-char v2, v0, v1

    .line 1050709
    :goto_2
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050710
    iget v0, p0, LX/66f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->e:I

    goto :goto_0

    .line 1050711
    :cond_2
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->e:I

    iget-object v2, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v2, v2, v3

    aput-char v2, v0, v1

    goto :goto_2

    .line 1050712
    :cond_3
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/66f;->g:[C

    iget v2, p0, LX/66f;->d:I

    iget v3, p0, LX/66f;->e:I

    iget v4, p0, LX/66f;->d:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method private static c(LX/66f;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x20

    .line 1050713
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LX/66f;->b:I

    if-lt v0, v1, :cond_0

    .line 1050714
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050715
    :cond_0
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->d:I

    .line 1050716
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050717
    :goto_0
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_4

    .line 1050718
    :cond_1
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->e:I

    .line 1050719
    :cond_2
    iget v0, p0, LX/66f;->e:I

    iget v1, p0, LX/66f;->d:I

    sub-int v2, v0, v1

    .line 1050720
    const/4 v0, 0x5

    if-lt v2, v0, :cond_3

    and-int/lit8 v0, v2, 0x1

    if-nez v0, :cond_7

    .line 1050721
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050722
    :cond_4
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_5

    .line 1050723
    iget v0, p0, LX/66f;->c:I

    iput v0, p0, LX/66f;->e:I

    .line 1050724
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050725
    :goto_1
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_2

    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto :goto_1

    .line 1050726
    :cond_5
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x41

    if-lt v0, v1, :cond_6

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x46

    if-gt v0, v1, :cond_6

    .line 1050727
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v2, v0, v1

    add-int/lit8 v2, v2, 0x20

    int-to-char v2, v2

    aput-char v2, v0, v1

    .line 1050728
    :cond_6
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    goto/16 :goto_0

    .line 1050729
    :cond_7
    div-int/lit8 v0, v2, 0x2

    new-array v3, v0, [B

    .line 1050730
    const/4 v1, 0x0

    iget v0, p0, LX/66f;->d:I

    add-int/lit8 v0, v0, 0x1

    :goto_2
    array-length v4, v3

    if-ge v1, v4, :cond_8

    .line 1050731
    invoke-static {p0, v0}, LX/66f;->a(LX/66f;I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 1050732
    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1050733
    :cond_8
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->d:I

    invoke-direct {v0, v1, v3, v2}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public static e(LX/66f;)C
    .locals 8

    .prologue
    .line 1050734
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050735
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-ne v0, v1, :cond_0

    .line 1050736
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050737
    :cond_0
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 1050738
    const/16 v6, 0x80

    const/16 v2, 0x3f

    .line 1050739
    iget v0, p0, LX/66f;->c:I

    invoke-static {p0, v0}, LX/66f;->a(LX/66f;I)I

    move-result v1

    .line 1050740
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050741
    if-ge v1, v6, :cond_1

    .line 1050742
    int-to-char v0, v1

    .line 1050743
    :goto_0
    move v0, v0

    .line 1050744
    :goto_1
    return v0

    .line 1050745
    :sswitch_0
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    goto :goto_1

    .line 1050746
    :cond_1
    const/16 v0, 0xc0

    if-lt v1, v0, :cond_8

    const/16 v0, 0xf7

    if-gt v1, v0, :cond_8

    .line 1050747
    const/16 v0, 0xdf

    if-gt v1, v0, :cond_3

    .line 1050748
    const/4 v0, 0x1

    .line 1050749
    and-int/lit8 v1, v1, 0x1f

    .line 1050750
    :goto_2
    const/4 v3, 0x0

    move v7, v3

    move v3, v1

    move v1, v7

    :goto_3
    if-ge v1, v0, :cond_7

    .line 1050751
    iget v4, p0, LX/66f;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/66f;->c:I

    .line 1050752
    iget v4, p0, LX/66f;->c:I

    iget v5, p0, LX/66f;->b:I

    if-eq v4, v5, :cond_2

    iget-object v4, p0, LX/66f;->g:[C

    iget v5, p0, LX/66f;->c:I

    aget-char v4, v4, v5

    const/16 v5, 0x5c

    if-eq v4, v5, :cond_5

    :cond_2
    move v0, v2

    .line 1050753
    goto :goto_0

    .line 1050754
    :cond_3
    const/16 v0, 0xef

    if-gt v1, v0, :cond_4

    .line 1050755
    const/4 v0, 0x2

    .line 1050756
    and-int/lit8 v1, v1, 0xf

    goto :goto_2

    .line 1050757
    :cond_4
    const/4 v0, 0x3

    .line 1050758
    and-int/lit8 v1, v1, 0x7

    goto :goto_2

    .line 1050759
    :cond_5
    iget v4, p0, LX/66f;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/66f;->c:I

    .line 1050760
    iget v4, p0, LX/66f;->c:I

    invoke-static {p0, v4}, LX/66f;->a(LX/66f;I)I

    move-result v4

    .line 1050761
    iget v5, p0, LX/66f;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, LX/66f;->c:I

    .line 1050762
    and-int/lit16 v5, v4, 0xc0

    if-eq v5, v6, :cond_6

    move v0, v2

    .line 1050763
    goto :goto_0

    .line 1050764
    :cond_6
    shl-int/lit8 v3, v3, 0x6

    and-int/lit8 v4, v4, 0x3f

    add-int/2addr v3, v4

    .line 1050765
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1050766
    :cond_7
    int-to-char v0, v3

    goto :goto_0

    :cond_8
    move v0, v2

    .line 1050767
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x22 -> :sswitch_0
        0x23 -> :sswitch_0
        0x25 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2b -> :sswitch_0
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x3c -> :sswitch_0
        0x3d -> :sswitch_0
        0x3e -> :sswitch_0
        0x5c -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1050768
    iput v0, p0, LX/66f;->c:I

    .line 1050769
    iput v0, p0, LX/66f;->d:I

    .line 1050770
    iput v0, p0, LX/66f;->e:I

    .line 1050771
    iput v0, p0, LX/66f;->f:I

    .line 1050772
    iget-object v0, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, LX/66f;->g:[C

    .line 1050773
    invoke-static {p0}, LX/66f;->a(LX/66f;)Ljava/lang/String;

    move-result-object v0

    .line 1050774
    if-nez v0, :cond_1

    move-object v1, v2

    .line 1050775
    :cond_0
    :goto_0
    return-object v1

    .line 1050776
    :cond_1
    const-string v1, ""

    .line 1050777
    iget v3, p0, LX/66f;->c:I

    iget v4, p0, LX/66f;->b:I

    if-ne v3, v4, :cond_2

    move-object v1, v2

    .line 1050778
    goto :goto_0

    .line 1050779
    :cond_2
    iget-object v3, p0, LX/66f;->g:[C

    iget v4, p0, LX/66f;->c:I

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    .line 1050780
    const/16 v6, 0x20

    .line 1050781
    iget v1, p0, LX/66f;->c:I

    iput v1, p0, LX/66f;->d:I

    .line 1050782
    iget v1, p0, LX/66f;->c:I

    iput v1, p0, LX/66f;->e:I

    .line 1050783
    :cond_3
    :goto_1
    iget v1, p0, LX/66f;->c:I

    iget v3, p0, LX/66f;->b:I

    if-lt v1, v3, :cond_6

    .line 1050784
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/66f;->g:[C

    iget v4, p0, LX/66f;->d:I

    iget v5, p0, LX/66f;->e:I

    iget v6, p0, LX/66f;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    .line 1050785
    :goto_2
    move-object v1, v1

    .line 1050786
    :goto_3
    :sswitch_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1050787
    iget v0, p0, LX/66f;->c:I

    iget v1, p0, LX/66f;->b:I

    if-lt v0, v1, :cond_4

    move-object v1, v2

    .line 1050788
    goto :goto_0

    .line 1050789
    :sswitch_1
    invoke-static {p0}, LX/66f;->b(LX/66f;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 1050790
    :sswitch_2
    invoke-static {p0}, LX/66f;->c(LX/66f;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 1050791
    :cond_4
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_5

    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_5

    .line 1050792
    iget-object v0, p0, LX/66f;->g:[C

    iget v1, p0, LX/66f;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_5

    .line 1050793
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050794
    :cond_5
    iget v0, p0, LX/66f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66f;->c:I

    .line 1050795
    invoke-static {p0}, LX/66f;->a(LX/66f;)Ljava/lang/String;

    move-result-object v0

    .line 1050796
    if-nez v0, :cond_1

    .line 1050797
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050798
    :cond_6
    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v1, v1, v3

    sparse-switch v1, :sswitch_data_1

    .line 1050799
    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/66f;->e:I

    iget-object v4, p0, LX/66f;->g:[C

    iget v5, p0, LX/66f;->c:I

    aget-char v4, v4, v5

    aput-char v4, v1, v3

    .line 1050800
    iget v1, p0, LX/66f;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/66f;->c:I

    goto/16 :goto_1

    .line 1050801
    :sswitch_3
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/66f;->g:[C

    iget v4, p0, LX/66f;->d:I

    iget v5, p0, LX/66f;->e:I

    iget v6, p0, LX/66f;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_2

    .line 1050802
    :sswitch_4
    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/66f;->e:I

    invoke-static {p0}, LX/66f;->e(LX/66f;)C

    move-result v4

    aput-char v4, v1, v3

    .line 1050803
    iget v1, p0, LX/66f;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/66f;->c:I

    goto/16 :goto_1

    .line 1050804
    :sswitch_5
    iget v1, p0, LX/66f;->e:I

    iput v1, p0, LX/66f;->f:I

    .line 1050805
    iget v1, p0, LX/66f;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/66f;->c:I

    .line 1050806
    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/66f;->e:I

    aput-char v6, v1, v3

    .line 1050807
    :goto_4
    iget v1, p0, LX/66f;->c:I

    iget v3, p0, LX/66f;->b:I

    if-ge v1, v3, :cond_7

    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v1, v1, v3

    if-ne v1, v6, :cond_7

    .line 1050808
    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/66f;->e:I

    aput-char v6, v1, v3

    .line 1050809
    iget v1, p0, LX/66f;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/66f;->c:I

    goto :goto_4

    .line 1050810
    :cond_7
    iget v1, p0, LX/66f;->c:I

    iget v3, p0, LX/66f;->b:I

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x2c

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x2b

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/66f;->g:[C

    iget v3, p0, LX/66f;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x3b

    if-ne v1, v3, :cond_3

    .line 1050811
    :cond_8
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/66f;->g:[C

    iget v4, p0, LX/66f;->d:I

    iget v5, p0, LX/66f;->f:I

    iget v6, p0, LX/66f;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_1
        0x23 -> :sswitch_2
        0x2b -> :sswitch_0
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x20 -> :sswitch_5
        0x2b -> :sswitch_3
        0x2c -> :sswitch_3
        0x3b -> :sswitch_3
        0x5c -> :sswitch_4
    .end sparse-switch
.end method
