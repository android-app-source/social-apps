.class public final LX/57a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 841681
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 841682
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 841683
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 841684
    invoke-static {p0, p1}, LX/57a;->b(LX/15w;LX/186;)I

    move-result v1

    .line 841685
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 841686
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 841687
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 841688
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 841689
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/57a;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 841690
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 841691
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 841692
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 841693
    const-wide/16 v10, 0x0

    .line 841694
    const/4 v8, 0x0

    .line 841695
    const/4 v7, 0x0

    .line 841696
    const/4 v6, 0x0

    .line 841697
    const/4 v5, 0x0

    .line 841698
    const/4 v4, 0x0

    .line 841699
    const/4 v3, 0x0

    .line 841700
    const/4 v2, 0x0

    .line 841701
    const/4 v1, 0x0

    .line 841702
    const/4 v0, 0x0

    .line 841703
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v12, :cond_d

    .line 841704
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841705
    const/4 v0, 0x0

    .line 841706
    :goto_0
    return v0

    .line 841707
    :cond_0
    const-string v4, "description_font"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 841708
    invoke-static {p0, p1}, LX/57f;->a(LX/15w;LX/186;)I

    move-result v1

    move v13, v1

    .line 841709
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 841710
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 841711
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 841712
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v1, :cond_1

    .line 841713
    const-string v4, "creation_time"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 841714
    const/4 v0, 0x1

    .line 841715
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 841716
    :cond_2
    const-string v4, "description_font_name"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 841717
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v12, v1

    goto :goto_1

    .line 841718
    :cond_3
    const-string v4, "digest_cards"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 841719
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 841720
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 841721
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 841722
    invoke-static {p0, p1}, LX/57W;->b(LX/15w;LX/186;)I

    move-result v4

    .line 841723
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 841724
    :cond_4
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 841725
    move v11, v1

    goto :goto_1

    .line 841726
    :cond_5
    const-string v4, "digest_owner"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 841727
    invoke-static {p0, p1}, LX/57Z;->a(LX/15w;LX/186;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 841728
    :cond_6
    const-string v4, "digest_title"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 841729
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto/16 :goto_1

    .line 841730
    :cond_7
    const-string v4, "id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 841731
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto/16 :goto_1

    .line 841732
    :cond_8
    const-string v4, "title_font"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 841733
    invoke-static {p0, p1}, LX/57f;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 841734
    :cond_9
    const-string v4, "title_font_name"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 841735
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 841736
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 841737
    :cond_b
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 841738
    if-eqz v0, :cond_c

    .line 841739
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 841740
    :cond_c
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 841741
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 841742
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 841743
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 841744
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 841745
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 841746
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 841747
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 841748
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_d
    move v9, v4

    move v12, v7

    move v13, v8

    move v7, v2

    move v8, v3

    move-wide v2, v10

    move v10, v5

    move v11, v6

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 841749
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 841750
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 841751
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 841752
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841753
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 841754
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841755
    if-eqz v0, :cond_1

    .line 841756
    const-string v1, "description_font"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841757
    invoke-static {p0, v0, p2}, LX/57f;->a(LX/15i;ILX/0nX;)V

    .line 841758
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841759
    if-eqz v0, :cond_2

    .line 841760
    const-string v1, "description_font_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841761
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841762
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841763
    if-eqz v0, :cond_4

    .line 841764
    const-string v1, "digest_cards"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841765
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 841766
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 841767
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/57W;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 841768
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 841769
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 841770
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841771
    if-eqz v0, :cond_5

    .line 841772
    const-string v1, "digest_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841773
    invoke-static {p0, v0, p2, p3}, LX/57Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 841774
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841775
    if-eqz v0, :cond_6

    .line 841776
    const-string v1, "digest_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841777
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841778
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841779
    if-eqz v0, :cond_7

    .line 841780
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841781
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841782
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841783
    if-eqz v0, :cond_8

    .line 841784
    const-string v1, "title_font"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841785
    invoke-static {p0, v0, p2}, LX/57f;->a(LX/15i;ILX/0nX;)V

    .line 841786
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841787
    if-eqz v0, :cond_9

    .line 841788
    const-string v1, "title_font_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841789
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841790
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 841791
    return-void
.end method
