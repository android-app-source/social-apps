.class public LX/6Hd;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/6HZ;


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/animation/AnimatorSet;

.field private i:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072494
    new-instance v0, LX/6Hc;

    invoke-direct {v0}, LX/6Hc;-><init>()V

    sput-object v0, LX/6Hd;->d:Ljava/util/ArrayList;

    .line 1072495
    const-class v0, LX/6Hd;

    sput-object v0, LX/6Hd;->c:Ljava/lang/Class;

    return-void
.end method

.method private static a(FI)I
    .locals 2

    .prologue
    .line 1072496
    const/high16 v0, 0x447a0000    # 1000.0f

    add-float/2addr v0, p0

    int-to-float v1, p1

    mul-float/2addr v0, v1

    const/high16 v1, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(LX/6Hi;II)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1072497
    iget-object v0, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072498
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    invoke-static {v1, p1}, LX/6Hd;->a(FI)I

    move-result v1

    .line 1072499
    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-static {v2, p2}, LX/6Hd;->a(FI)I

    move-result v2

    .line 1072500
    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    invoke-static {v3, p1}, LX/6Hd;->a(FI)I

    move-result v3

    .line 1072501
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-static {v0, p2}, LX/6Hd;->a(FI)I

    move-result v0

    .line 1072502
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v1, v2, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1072503
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Hd;->j:Z

    .line 1072504
    invoke-virtual {p0}, LX/6Hd;->invalidate()V

    .line 1072505
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6Hi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1072506
    if-nez p1, :cond_2

    .line 1072507
    iget-object v0, p0, LX/6Hd;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1072508
    iget-object v0, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1072509
    sget-object v2, LX/6Hd;->d:Ljava/util/ArrayList;

    iget-object v3, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1072510
    :cond_0
    iget-object v0, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1072511
    :cond_1
    invoke-virtual {p0}, LX/6Hd;->invalidate()V

    .line 1072512
    return-void

    .line 1072513
    :cond_2
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1072514
    iget-object v0, p0, LX/6Hd;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1072515
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1072516
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072517
    iget-object v3, p0, LX/6Hd;->e:Ljava/util/Map;

    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1072518
    iget-object v3, p0, LX/6Hd;->e:Ljava/util/Map;

    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1072519
    sget-object v8, LX/6Hd;->d:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_7

    .line 1072520
    const v8, -0x333334

    .line 1072521
    :goto_3
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 1072522
    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 1072523
    const/high16 v10, 0x40400000    # 3.0f

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1072524
    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1072525
    move-object v8, v9

    .line 1072526
    move-object v5, v8

    .line 1072527
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072528
    :cond_4
    iget-object v3, p0, LX/6Hd;->f:Ljava/util/Map;

    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1072529
    iget-object v3, p0, LX/6Hd;->g:Ljava/util/Map;

    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, LX/6Hd;->f:Ljava/util/Map;

    iget-wide v6, v0, LX/6Hi;->l:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072530
    :cond_5
    iget-object v3, p0, LX/6Hd;->f:Ljava/util/Map;

    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1072531
    iget v5, p0, LX/6Hd;->a:I

    iget v6, p0, LX/6Hd;->b:I

    invoke-static {v0, v5, v6}, LX/6Hd;->a(LX/6Hi;II)Landroid/graphics/Rect;

    move-result-object v5

    move-object v5, v5

    .line 1072532
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072533
    iget-wide v4, v0, LX/6Hi;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1072534
    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1072535
    sget-object v3, LX/6Hd;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072536
    iget-object v1, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072537
    iget-object v1, p0, LX/6Hd;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072538
    iget-object v1, p0, LX/6Hd;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1072539
    :cond_7
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    sget-object v10, LX/6Hd;->d:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v8, v10

    double-to-int v9, v8

    .line 1072540
    sget-object v8, LX/6Hd;->d:Ljava/util/ArrayList;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1072541
    sget-object v10, LX/6Hd;->d:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_3
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1072542
    iget-boolean v0, p0, LX/6Hd;->j:Z

    if-eqz v0, :cond_0

    .line 1072543
    iput-boolean v7, p0, LX/6Hd;->j:Z

    .line 1072544
    :goto_0
    return-void

    .line 1072545
    :cond_0
    iget-object v0, p0, LX/6Hd;->i:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1072546
    iget-object v0, p0, LX/6Hd;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1072547
    iget-object v1, p0, LX/6Hd;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Paint;

    .line 1072548
    iget-object v2, p0, LX/6Hd;->f:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 1072549
    iget-object v4, p0, LX/6Hd;->g:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1072550
    if-nez v0, :cond_1

    .line 1072551
    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 1072552
    :cond_1
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1072553
    iget-object v1, p0, LX/6Hd;->i:Ljava/util/Collection;

    const-string v4, "translationX"

    new-array v5, v9, [F

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v7

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v8

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1072554
    iget-object v1, p0, LX/6Hd;->i:Ljava/util/Collection;

    const-string v4, "translationY"

    new-array v5, v9, [F

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v7

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    aput v2, v5, v8

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1072555
    :cond_2
    iget-object v0, p0, LX/6Hd;->h:Landroid/animation/AnimatorSet;

    iget-object v1, p0, LX/6Hd;->i:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1072556
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1072557
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1072558
    iput v0, p0, LX/6Hd;->a:I

    .line 1072559
    iput v1, p0, LX/6Hd;->b:I

    .line 1072560
    return-void
.end method
