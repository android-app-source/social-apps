.class public LX/623;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/621;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/621",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1041195
    const/4 v0, 0x0

    .line 1041196
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1041197
    invoke-direct {p0, v0, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1041198
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1041199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041200
    iput-object p1, p0, LX/623;->a:Ljava/lang/String;

    .line 1041201
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/623;->b:Ljava/util/List;

    .line 1041202
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 1041203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041204
    iput-object p1, p0, LX/623;->a:Ljava/lang/String;

    .line 1041205
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/623;->b:Ljava/util/List;

    .line 1041206
    iput-boolean p3, p0, LX/623;->c:Z

    .line 1041207
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1041208
    iget-object v0, p0, LX/623;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1041209
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1041210
    iget-object v0, p0, LX/623;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1041211
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1041212
    const/4 v0, 0x0

    return v0
.end method
