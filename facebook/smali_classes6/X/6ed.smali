.class public LX/6ed;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:LX/2MK;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/47d;

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:I

.field public final g:I


# direct methods
.method private constructor <init>(Landroid/net/Uri;LX/2MK;Ljava/lang/String;LX/47d;Landroid/net/Uri;II)V
    .locals 0
    .param p5    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1117990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117991
    iput-object p1, p0, LX/6ed;->a:Landroid/net/Uri;

    .line 1117992
    iput-object p2, p0, LX/6ed;->b:LX/2MK;

    .line 1117993
    iput-object p3, p0, LX/6ed;->c:Ljava/lang/String;

    .line 1117994
    iput-object p4, p0, LX/6ed;->d:LX/47d;

    .line 1117995
    iput-object p5, p0, LX/6ed;->e:Landroid/net/Uri;

    .line 1117996
    iput p6, p0, LX/6ed;->f:I

    .line 1117997
    iput p7, p0, LX/6ed;->g:I

    .line 1117998
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;
    .locals 8

    .prologue
    .line 1117999
    new-instance v0, LX/6ed;

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    iget-object v5, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    iget v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    iget v7, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-direct/range {v0 .. v7}, LX/6ed;-><init>(Landroid/net/Uri;LX/2MK;Ljava/lang/String;LX/47d;Landroid/net/Uri;II)V

    return-object v0
.end method

.method public static b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;
    .locals 1

    .prologue
    .line 1118000
    :goto_0
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_0

    .line 1118001
    iget-object p0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_0

    .line 1118002
    :cond_0
    invoke-static {p0}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1118003
    if-ne p0, p1, :cond_1

    .line 1118004
    :cond_0
    :goto_0
    return v0

    .line 1118005
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1118006
    goto :goto_0

    .line 1118007
    :cond_3
    check-cast p1, LX/6ed;

    .line 1118008
    iget-object v2, p0, LX/6ed;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/6ed;->a:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/6ed;->b:LX/2MK;

    iget-object v3, p1, LX/6ed;->b:LX/2MK;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/6ed;->c:Ljava/lang/String;

    iget-object v3, p1, LX/6ed;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/6ed;->d:LX/47d;

    iget-object v3, p1, LX/6ed;->d:LX/47d;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/6ed;->e:Landroid/net/Uri;

    iget-object v3, p1, LX/6ed;->e:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, LX/6ed;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, LX/6ed;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, LX/6ed;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, LX/6ed;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1118009
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/6ed;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/6ed;->b:LX/2MK;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/6ed;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/6ed;->d:LX/47d;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/6ed;->e:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/6ed;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, LX/6ed;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
