.class public final LX/6Vn;
.super LX/1Ra;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ra",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field public final synthetic c:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

.field public final synthetic d:Ljava/lang/Object;

.field public final synthetic e:LX/1Qx;

.field public final synthetic f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1Qx;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1104459
    iput-object p1, p0, LX/6Vn;->c:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iput-object p2, p0, LX/6Vn;->d:Ljava/lang/Object;

    iput-object p3, p0, LX/6Vn;->e:LX/1Qx;

    iput-object p4, p0, LX/6Vn;->f:Landroid/content/Context;

    invoke-direct {p0}, LX/1Ra;-><init>()V

    .line 1104460
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6Vn;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1104461
    iget-object v0, p0, LX/6Vn;->c:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v2, p0, LX/6Vn;->d:Ljava/lang/Object;

    iget-object v3, p0, LX/6Vn;->e:LX/1Qx;

    invoke-static {v0, v2, p1, v3}, LX/1RD;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/6Vn;->b:LX/0Px;

    .line 1104462
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 1104463
    iget-object v4, p0, LX/6Vn;->a:Ljava/util/List;

    invoke-virtual {v0}, LX/1RZ;->b()LX/1Cz;

    move-result-object v0

    iget-object v5, p0, LX/6Vn;->f:Landroid/content/Context;

    invoke-virtual {v0, v5}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1104464
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1104465
    :cond_0
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 1104466
    invoke-virtual {v0, p1}, LX/1Ra;->a(LX/1PW;)V

    .line 1104467
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1104468
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1104453
    check-cast p1, Landroid/view/ViewGroup;

    .line 1104454
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1104455
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    iget-object v1, p0, LX/6Vn;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, LX/1Ra;->a(Landroid/view/View;)V

    .line 1104456
    iget-object v0, p0, LX/6Vn;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1104457
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1104458
    :cond_0
    return-void
.end method

.method public final b(LX/1PW;)V
    .locals 3

    .prologue
    .line 1104443
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 1104444
    invoke-virtual {v0, p1}, LX/1Ra;->b(LX/1PW;)V

    .line 1104445
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1104446
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1104447
    check-cast p1, Landroid/view/ViewGroup;

    .line 1104448
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1104449
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1104450
    iget-object v0, p0, LX/6Vn;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    iget-object v1, p0, LX/6Vn;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, LX/1Ra;->b(Landroid/view/View;)V

    .line 1104451
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1104452
    :cond_0
    return-void
.end method
