.class public final enum LX/5ni;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5ni;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5ni;

.field public static final enum CHANGE_PRIVACY:LX/5ni;

.field public static final enum DELETE_APP_AND_POSTS:LX/5ni;

.field public static final enum DELETE_APP_ONLY:LX/5ni;


# instance fields
.field private final actionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1004558
    new-instance v0, LX/5ni;

    const-string v1, "DELETE_APP_ONLY"

    const-string v2, "delete_app_only"

    invoke-direct {v0, v1, v3, v2}, LX/5ni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ni;->DELETE_APP_ONLY:LX/5ni;

    .line 1004559
    new-instance v0, LX/5ni;

    const-string v1, "DELETE_APP_AND_POSTS"

    const-string v2, "delete_app_and_posts"

    invoke-direct {v0, v1, v4, v2}, LX/5ni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ni;->DELETE_APP_AND_POSTS:LX/5ni;

    .line 1004560
    new-instance v0, LX/5ni;

    const-string v1, "CHANGE_PRIVACY"

    const-string v2, "change_privacy"

    invoke-direct {v0, v1, v5, v2}, LX/5ni;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ni;->CHANGE_PRIVACY:LX/5ni;

    .line 1004561
    const/4 v0, 0x3

    new-array v0, v0, [LX/5ni;

    sget-object v1, LX/5ni;->DELETE_APP_ONLY:LX/5ni;

    aput-object v1, v0, v3

    sget-object v1, LX/5ni;->DELETE_APP_AND_POSTS:LX/5ni;

    aput-object v1, v0, v4

    sget-object v1, LX/5ni;->CHANGE_PRIVACY:LX/5ni;

    aput-object v1, v0, v5

    sput-object v0, LX/5ni;->$VALUES:[LX/5ni;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004565
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004566
    iput-object p3, p0, LX/5ni;->actionName:Ljava/lang/String;

    .line 1004567
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5ni;
    .locals 1

    .prologue
    .line 1004564
    const-class v0, LX/5ni;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5ni;

    return-object v0
.end method

.method public static values()[LX/5ni;
    .locals 1

    .prologue
    .line 1004563
    sget-object v0, LX/5ni;->$VALUES:[LX/5ni;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5ni;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004562
    iget-object v0, p0, LX/5ni;->actionName:Ljava/lang/String;

    return-object v0
.end method
