.class public final LX/6B3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 46

    .prologue
    .line 1061328
    const/16 v42, 0x0

    .line 1061329
    const/16 v41, 0x0

    .line 1061330
    const/16 v40, 0x0

    .line 1061331
    const/16 v39, 0x0

    .line 1061332
    const/16 v38, 0x0

    .line 1061333
    const/16 v37, 0x0

    .line 1061334
    const/16 v36, 0x0

    .line 1061335
    const/16 v35, 0x0

    .line 1061336
    const/16 v34, 0x0

    .line 1061337
    const/16 v33, 0x0

    .line 1061338
    const/16 v32, 0x0

    .line 1061339
    const/16 v31, 0x0

    .line 1061340
    const/16 v30, 0x0

    .line 1061341
    const/16 v29, 0x0

    .line 1061342
    const/16 v28, 0x0

    .line 1061343
    const/16 v27, 0x0

    .line 1061344
    const/16 v26, 0x0

    .line 1061345
    const/16 v25, 0x0

    .line 1061346
    const/16 v24, 0x0

    .line 1061347
    const/16 v23, 0x0

    .line 1061348
    const/16 v22, 0x0

    .line 1061349
    const/16 v21, 0x0

    .line 1061350
    const/16 v20, 0x0

    .line 1061351
    const/16 v19, 0x0

    .line 1061352
    const/16 v18, 0x0

    .line 1061353
    const/16 v17, 0x0

    .line 1061354
    const/16 v16, 0x0

    .line 1061355
    const/4 v15, 0x0

    .line 1061356
    const/4 v14, 0x0

    .line 1061357
    const/4 v13, 0x0

    .line 1061358
    const/4 v12, 0x0

    .line 1061359
    const/4 v11, 0x0

    .line 1061360
    const/4 v10, 0x0

    .line 1061361
    const/4 v9, 0x0

    .line 1061362
    const/4 v8, 0x0

    .line 1061363
    const/4 v7, 0x0

    .line 1061364
    const/4 v6, 0x0

    .line 1061365
    const/4 v5, 0x0

    .line 1061366
    const/4 v4, 0x0

    .line 1061367
    const/4 v3, 0x0

    .line 1061368
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1

    .line 1061369
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1061370
    const/4 v3, 0x0

    .line 1061371
    :goto_0
    return v3

    .line 1061372
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1061373
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1e

    .line 1061374
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v43

    .line 1061375
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1061376
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1

    if-eqz v43, :cond_1

    .line 1061377
    const-string v44, "__type__"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_2

    const-string v44, "__typename"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_3

    .line 1061378
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v42

    goto :goto_1

    .line 1061379
    :cond_3
    const-string v44, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_4

    .line 1061380
    const/4 v14, 0x1

    .line 1061381
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 1061382
    :cond_4
    const-string v44, "can_see_voice_switcher"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_5

    .line 1061383
    const/4 v13, 0x1

    .line 1061384
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 1061385
    :cond_5
    const-string v44, "can_viewer_comment"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_6

    .line 1061386
    const/4 v12, 0x1

    .line 1061387
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 1061388
    :cond_6
    const-string v44, "can_viewer_comment_with_photo"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_7

    .line 1061389
    const/4 v11, 0x1

    .line 1061390
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 1061391
    :cond_7
    const-string v44, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_8

    .line 1061392
    const/4 v10, 0x1

    .line 1061393
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 1061394
    :cond_8
    const-string v44, "can_viewer_comment_with_video"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_9

    .line 1061395
    const/4 v9, 0x1

    .line 1061396
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 1061397
    :cond_9
    const-string v44, "can_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_a

    .line 1061398
    const/4 v8, 0x1

    .line 1061399
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 1061400
    :cond_a
    const-string v44, "can_viewer_react"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 1061401
    const/4 v7, 0x1

    .line 1061402
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 1061403
    :cond_b
    const-string v44, "can_viewer_subscribe"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 1061404
    const/4 v6, 0x1

    .line 1061405
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 1061406
    :cond_c
    const-string v44, "comments"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_d

    .line 1061407
    invoke-static/range {p0 .. p1}, LX/6B2;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 1061408
    :cond_d
    const-string v44, "comments_mirroring_domain"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_e

    .line 1061409
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 1061410
    :cond_e
    const-string v44, "does_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_f

    .line 1061411
    const/4 v5, 0x1

    .line 1061412
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 1061413
    :cond_f
    const-string v44, "id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_10

    .line 1061414
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 1061415
    :cond_10
    const-string v44, "important_reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_11

    .line 1061416
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 1061417
    :cond_11
    const-string v44, "is_viewer_subscribed"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_12

    .line 1061418
    const/4 v4, 0x1

    .line 1061419
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 1061420
    :cond_12
    const-string v44, "legacy_api_post_id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 1061421
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto/16 :goto_1

    .line 1061422
    :cond_13
    const-string v44, "like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_14

    .line 1061423
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1061424
    :cond_14
    const-string v44, "reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_15

    .line 1061425
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1061426
    :cond_15
    const-string v44, "remixable_photo_uri"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_16

    .line 1061427
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 1061428
    :cond_16
    const-string v44, "seen_by"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 1061429
    invoke-static/range {p0 .. p1}, LX/6B4;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1061430
    :cond_17
    const-string v44, "supported_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_18

    .line 1061431
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1061432
    :cond_18
    const-string v44, "top_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 1061433
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1061434
    :cond_19
    const-string v44, "viewer_acts_as_page"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1a

    .line 1061435
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1061436
    :cond_1a
    const-string v44, "viewer_acts_as_person"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1b

    .line 1061437
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1061438
    :cond_1b
    const-string v44, "viewer_does_not_like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1c

    .line 1061439
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1061440
    :cond_1c
    const-string v44, "viewer_feedback_reaction_key"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1d

    .line 1061441
    const/4 v3, 0x1

    .line 1061442
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 1061443
    :cond_1d
    const-string v44, "viewer_likes_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_0

    .line 1061444
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1061445
    :cond_1e
    const/16 v43, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1061446
    const/16 v43, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1061447
    if-eqz v14, :cond_1f

    .line 1061448
    const/4 v14, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 1061449
    :cond_1f
    if-eqz v13, :cond_20

    .line 1061450
    const/4 v13, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1061451
    :cond_20
    if-eqz v12, :cond_21

    .line 1061452
    const/4 v12, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1061453
    :cond_21
    if-eqz v11, :cond_22

    .line 1061454
    const/4 v11, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1061455
    :cond_22
    if-eqz v10, :cond_23

    .line 1061456
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1061457
    :cond_23
    if-eqz v9, :cond_24

    .line 1061458
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1061459
    :cond_24
    if-eqz v8, :cond_25

    .line 1061460
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1061461
    :cond_25
    if-eqz v7, :cond_26

    .line 1061462
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1061463
    :cond_26
    if-eqz v6, :cond_27

    .line 1061464
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1061465
    :cond_27
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1061466
    const/16 v6, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1061467
    if-eqz v5, :cond_28

    .line 1061468
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1061469
    :cond_28
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1061470
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1061471
    if-eqz v4, :cond_29

    .line 1061472
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1061473
    :cond_29
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061474
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061475
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061476
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061477
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061478
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061479
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061480
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061481
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061482
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1061483
    if-eqz v3, :cond_2a

    .line 1061484
    const/16 v3, 0x1a

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1, v4}, LX/186;->a(III)V

    .line 1061485
    :cond_2a
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1061486
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
