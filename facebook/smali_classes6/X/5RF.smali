.class public final enum LX/5RF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5RF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5RF;

.field public static final enum CHECKIN:LX/5RF;

.field public static final enum FACECAST:LX/5RF;

.field public static final enum GIF_VIDEO:LX/5RF;

.field public static final enum MINUTIAE:LX/5RF;

.field public static final enum MULTIMEDIA:LX/5RF;

.field public static final enum MULTIPLE_PHOTOS:LX/5RF;

.field public static final enum MULTIPLE_VIDEOS:LX/5RF;

.field public static final enum NO_ATTACHMENTS:LX/5RF;

.field public static final enum SHARE_ATTACHMENT:LX/5RF;

.field public static final enum SINGLE_PHOTO:LX/5RF;

.field public static final enum SINGLE_VIDEO:LX/5RF;

.field public static final enum SLIDESHOW:LX/5RF;

.field public static final enum STICKER:LX/5RF;

.field public static final enum STORYLINE:LX/5RF;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 914773
    new-instance v0, LX/5RF;

    const-string v1, "NO_ATTACHMENTS"

    invoke-direct {v0, v1, v3}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    .line 914774
    new-instance v0, LX/5RF;

    const-string v1, "SINGLE_PHOTO"

    invoke-direct {v0, v1, v4}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    .line 914775
    new-instance v0, LX/5RF;

    const-string v1, "MULTIPLE_PHOTOS"

    invoke-direct {v0, v1, v5}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    .line 914776
    new-instance v0, LX/5RF;

    const-string v1, "SINGLE_VIDEO"

    invoke-direct {v0, v1, v6}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    .line 914777
    new-instance v0, LX/5RF;

    const-string v1, "MULTIPLE_VIDEOS"

    invoke-direct {v0, v1, v7}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    .line 914778
    new-instance v0, LX/5RF;

    const-string v1, "GIF_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->GIF_VIDEO:LX/5RF;

    .line 914779
    new-instance v0, LX/5RF;

    const-string v1, "STICKER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->STICKER:LX/5RF;

    .line 914780
    new-instance v0, LX/5RF;

    const-string v1, "SHARE_ATTACHMENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->SHARE_ATTACHMENT:LX/5RF;

    .line 914781
    new-instance v0, LX/5RF;

    const-string v1, "SLIDESHOW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->SLIDESHOW:LX/5RF;

    .line 914782
    new-instance v0, LX/5RF;

    const-string v1, "MULTIMEDIA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->MULTIMEDIA:LX/5RF;

    .line 914783
    new-instance v0, LX/5RF;

    const-string v1, "FACECAST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->FACECAST:LX/5RF;

    .line 914784
    new-instance v0, LX/5RF;

    const-string v1, "MINUTIAE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->MINUTIAE:LX/5RF;

    .line 914785
    new-instance v0, LX/5RF;

    const-string v1, "CHECKIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->CHECKIN:LX/5RF;

    .line 914786
    new-instance v0, LX/5RF;

    const-string v1, "STORYLINE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/5RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RF;->STORYLINE:LX/5RF;

    .line 914787
    const/16 v0, 0xe

    new-array v0, v0, [LX/5RF;

    sget-object v1, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    aput-object v1, v0, v3

    sget-object v1, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    aput-object v1, v0, v4

    sget-object v1, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    aput-object v1, v0, v5

    sget-object v1, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    aput-object v1, v0, v6

    sget-object v1, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5RF;->GIF_VIDEO:LX/5RF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5RF;->STICKER:LX/5RF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5RF;->SHARE_ATTACHMENT:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5RF;->SLIDESHOW:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5RF;->MULTIMEDIA:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5RF;->FACECAST:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5RF;->MINUTIAE:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5RF;->CHECKIN:LX/5RF;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/5RF;->STORYLINE:LX/5RF;

    aput-object v2, v0, v1

    sput-object v0, LX/5RF;->$VALUES:[LX/5RF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 914788
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 914789
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5RF;
    .locals 1

    .prologue
    .line 914790
    const-class v0, LX/5RF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5RF;

    return-object v0
.end method

.method public static values()[LX/5RF;
    .locals 1

    .prologue
    .line 914791
    sget-object v0, LX/5RF;->$VALUES:[LX/5RF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5RF;

    return-object v0
.end method
