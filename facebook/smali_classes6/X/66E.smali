.class public final LX/66E;
.super LX/66A;
.source ""


# instance fields
.field public final synthetic d:LX/66H;

.field private e:J


# direct methods
.method public constructor <init>(LX/66H;J)V
    .locals 4

    .prologue
    .line 1049655
    iput-object p1, p0, LX/66E;->d:LX/66H;

    invoke-direct {p0, p1}, LX/66A;-><init>(LX/66H;)V

    .line 1049656
    iput-wide p2, p0, LX/66E;->e:J

    .line 1049657
    iget-wide v0, p0, LX/66E;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1049658
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049659
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    const-wide/16 v6, 0x0

    .line 1049660
    cmp-long v2, p2, v6

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049661
    :cond_0
    iget-boolean v2, p0, LX/66A;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049662
    :cond_1
    iget-wide v2, p0, LX/66E;->e:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 1049663
    :goto_0
    return-wide v0

    .line 1049664
    :cond_2
    iget-object v2, p0, LX/66E;->d:LX/66H;

    iget-object v2, v2, LX/66H;->c:LX/671;

    iget-wide v4, p0, LX/66E;->e:J

    invoke-static {v4, v5, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    .line 1049665
    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    .line 1049666
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049667
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049668
    :cond_3
    iget-wide v0, p0, LX/66E;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/66E;->e:J

    .line 1049669
    iget-wide v0, p0, LX/66E;->e:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 1049670
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    :cond_4
    move-wide v0, v2

    .line 1049671
    goto :goto_0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 1049650
    iget-boolean v0, p0, LX/66A;->b:Z

    if-eqz v0, :cond_0

    .line 1049651
    :goto_0
    return-void

    .line 1049652
    :cond_0
    iget-wide v0, p0, LX/66E;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p0, v0, v1}, LX/65A;->a(LX/65D;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1049653
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049654
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66E;->b:Z

    goto :goto_0
.end method
