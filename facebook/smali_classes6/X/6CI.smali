.class public final enum LX/6CI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6CI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD:LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD_COMPOSER_BANNER:LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD_COMPOSER_MENU:LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD_CONTINUE_SHOPPING_BANNER:LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD_INBOX_CART_CTA:LX/6CI;

.field public static final enum MESSENGER_COMMERCE_THREAD_INBOX_CTA:LX/6CI;

.field public static final enum MESSENGER_PLATFORM_THREAD:LX/6CI;

.field public static final enum STORY_ATTACHMENT:LX/6CI;

.field public static final enum UNKNOWN:LX/6CI;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1063713
    new-instance v0, LX/6CI;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->UNKNOWN:LX/6CI;

    .line 1063714
    new-instance v0, LX/6CI;

    const-string v1, "STORY_ATTACHMENT"

    const-string v2, "story_attachment"

    invoke-direct {v0, v1, v5, v2}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->STORY_ATTACHMENT:LX/6CI;

    .line 1063715
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD"

    const-string v2, "messenger_commerce_thread"

    invoke-direct {v0, v1, v6, v2}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD:LX/6CI;

    .line 1063716
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_PLATFORM_THREAD"

    const-string v2, "messenger_platform_thread"

    invoke-direct {v0, v1, v7, v2}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_PLATFORM_THREAD:LX/6CI;

    .line 1063717
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD_INBOX_CTA"

    const-string v2, "messenger_thread_inbox_cta"

    invoke-direct {v0, v1, v8, v2}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD_INBOX_CTA:LX/6CI;

    .line 1063718
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD_INBOX_CART_CTA"

    const/4 v2, 0x5

    const-string v3, "messenger_thread_inbox_cart_cta"

    invoke-direct {v0, v1, v2, v3}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD_INBOX_CART_CTA:LX/6CI;

    .line 1063719
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD_CONTINUE_SHOPPING_BANNER"

    const/4 v2, 0x6

    const-string v3, "messenger_thread_continue_shopping_banner"

    invoke-direct {v0, v1, v2, v3}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD_CONTINUE_SHOPPING_BANNER:LX/6CI;

    .line 1063720
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD_COMPOSER_BANNER"

    const/4 v2, 0x7

    const-string v3, "messenger_thread_composer_banner"

    invoke-direct {v0, v1, v2, v3}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD_COMPOSER_BANNER:LX/6CI;

    .line 1063721
    new-instance v0, LX/6CI;

    const-string v1, "MESSENGER_COMMERCE_THREAD_COMPOSER_MENU"

    const/16 v2, 0x8

    const-string v3, "messenger_thread_composer_menu"

    invoke-direct {v0, v1, v2, v3}, LX/6CI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6CI;->MESSENGER_COMMERCE_THREAD_COMPOSER_MENU:LX/6CI;

    .line 1063722
    const/16 v0, 0x9

    new-array v0, v0, [LX/6CI;

    sget-object v1, LX/6CI;->UNKNOWN:LX/6CI;

    aput-object v1, v0, v4

    sget-object v1, LX/6CI;->STORY_ATTACHMENT:LX/6CI;

    aput-object v1, v0, v5

    sget-object v1, LX/6CI;->MESSENGER_COMMERCE_THREAD:LX/6CI;

    aput-object v1, v0, v6

    sget-object v1, LX/6CI;->MESSENGER_PLATFORM_THREAD:LX/6CI;

    aput-object v1, v0, v7

    sget-object v1, LX/6CI;->MESSENGER_COMMERCE_THREAD_INBOX_CTA:LX/6CI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6CI;->MESSENGER_COMMERCE_THREAD_INBOX_CART_CTA:LX/6CI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6CI;->MESSENGER_COMMERCE_THREAD_CONTINUE_SHOPPING_BANNER:LX/6CI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6CI;->MESSENGER_COMMERCE_THREAD_COMPOSER_BANNER:LX/6CI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6CI;->MESSENGER_COMMERCE_THREAD_COMPOSER_MENU:LX/6CI;

    aput-object v2, v0, v1

    sput-object v0, LX/6CI;->$VALUES:[LX/6CI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063723
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1063724
    iput-object p3, p0, LX/6CI;->value:Ljava/lang/String;

    .line 1063725
    return-void
.end method

.method public static fromValue(Ljava/lang/String;)LX/6CI;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1063726
    invoke-static {}, LX/6CI;->values()[LX/6CI;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1063727
    iget-object v4, v0, LX/6CI;->value:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1063728
    :goto_1
    return-object v0

    .line 1063729
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1063730
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6CI;
    .locals 1

    .prologue
    .line 1063731
    const-class v0, LX/6CI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6CI;

    return-object v0
.end method

.method public static values()[LX/6CI;
    .locals 1

    .prologue
    .line 1063732
    sget-object v0, LX/6CI;->$VALUES:[LX/6CI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6CI;

    return-object v0
.end method
