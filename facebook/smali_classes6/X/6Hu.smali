.class public LX/6Hu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6IG;

.field public b:LX/6IG;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;ZLjava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Z",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1072846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072847
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1072848
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 1072849
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-gt v4, v0, :cond_6

    move v0, v1

    .line 1072850
    :goto_0
    if-eqz v0, :cond_0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    :cond_0
    if-nez v0, :cond_7

    if-eq v3, v1, :cond_1

    const/4 v1, 0x3

    if-ne v3, v1, :cond_7

    .line 1072851
    :cond_1
    packed-switch v3, :pswitch_data_0

    .line 1072852
    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    .line 1072853
    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    .line 1072854
    :goto_1
    iput v2, p0, LX/6Hu;->c:I

    .line 1072855
    :goto_2
    if-eqz p2, :cond_2

    .line 1072856
    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    .line 1072857
    :cond_2
    sget-boolean v1, LX/6Hs;->b:Z

    move v1, v1

    .line 1072858
    if-nez v1, :cond_3

    .line 1072859
    sget-boolean v1, LX/6Hs;->c:Z

    move v1, v1

    .line 1072860
    if-eqz v1, :cond_4

    .line 1072861
    :cond_3
    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    .line 1072862
    :cond_4
    const/4 v4, 0x0

    .line 1072863
    sget-object v1, LX/6Ht;->a:[I

    iget-object v2, p0, LX/6Hu;->b:LX/6IG;

    invoke-virtual {v2}, LX/6IG;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1072864
    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    .line 1072865
    iput v4, p0, LX/6Hu;->e:I

    .line 1072866
    const v1, 0x7f030228

    iput v1, p0, LX/6Hu;->d:I

    .line 1072867
    :goto_3
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1072868
    if-eqz v1, :cond_5

    .line 1072869
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Rotation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_8

    const-string v0, " Tall"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1072870
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Layout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6Hu;->b:LX/6IG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/6Hu;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/6Hu;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1072871
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Orientation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6Hu;->a:LX/6IG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/6Hu;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1072872
    :cond_5
    return-void

    :cond_6
    move v0, v2

    .line 1072873
    goto/16 :goto_0

    .line 1072874
    :pswitch_0
    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    .line 1072875
    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    goto/16 :goto_1

    .line 1072876
    :pswitch_1
    sget-object v1, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    .line 1072877
    sget-object v1, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    goto/16 :goto_1

    .line 1072878
    :pswitch_2
    sget-object v1, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    .line 1072879
    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    goto/16 :goto_1

    .line 1072880
    :cond_7
    packed-switch v3, :pswitch_data_2

    .line 1072881
    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    .line 1072882
    :goto_5
    iget-object v1, p0, LX/6Hu;->a:LX/6IG;

    iput-object v1, p0, LX/6Hu;->b:LX/6IG;

    .line 1072883
    const/16 v1, -0x5a

    iput v1, p0, LX/6Hu;->c:I

    goto/16 :goto_2

    .line 1072884
    :pswitch_3
    sget-object v1, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    goto :goto_5

    .line 1072885
    :pswitch_4
    sget-object v1, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    goto :goto_5

    .line 1072886
    :pswitch_5
    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v1, p0, LX/6Hu;->a:LX/6IG;

    goto :goto_5

    .line 1072887
    :cond_8
    const-string v0, " Wide"

    goto/16 :goto_4

    .line 1072888
    :pswitch_6
    iput v4, p0, LX/6Hu;->e:I

    .line 1072889
    const v1, 0x7f030228

    iput v1, p0, LX/6Hu;->d:I

    goto/16 :goto_3

    .line 1072890
    :pswitch_7
    const/4 v1, 0x1

    iput v1, p0, LX/6Hu;->e:I

    .line 1072891
    const v1, 0x7f03022c

    iput v1, p0, LX/6Hu;->d:I

    goto/16 :goto_3

    .line 1072892
    :pswitch_8
    const/16 v1, 0x8

    iput v1, p0, LX/6Hu;->e:I

    .line 1072893
    const v1, 0x7f03022e

    iput v1, p0, LX/6Hu;->d:I

    goto/16 :goto_3

    .line 1072894
    :pswitch_9
    const/16 v1, 0x9

    iput v1, p0, LX/6Hu;->e:I

    .line 1072895
    const v1, 0x7f03022c

    iput v1, p0, LX/6Hu;->d:I

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
