.class public abstract LX/54F;
.super LX/54E;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/54E",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final i:J


# instance fields
.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 827789
    :try_start_0
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    const-class v1, LX/54F;

    const-string v2, "f"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, LX/54F;->i:J
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 827790
    return-void

    .line 827791
    :catch_0
    move-exception v0

    .line 827792
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 827793
    invoke-direct {p0, p1}, LX/54E;-><init>(I)V

    .line 827794
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 827795
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    sget-wide v2, LX/54F;->i:J

    invoke-virtual {v0, p0, v2, v3}, Lsun/misc/Unsafe;->getLongVolatile(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method
