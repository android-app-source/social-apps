.class public LX/6dt;
.super LX/2Rg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:LX/6cp;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1117246
    invoke-static {}, LX/6cp;->newBuilder()LX/6cn;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "threads"

    const-string v3, "_ROWID_"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "thread_key"

    const-string v2, "threads"

    const-string v3, "thread_key"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "legacy_thread_id"

    const-string v2, "threads"

    const-string v3, "legacy_thread_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "action_id"

    const-string v2, "threads"

    const-string v3, "action_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "refetch_action_id"

    const-string v2, "threads"

    const-string v3, "refetch_action_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_visible_action_id"

    const-string v2, "threads"

    const-string v3, "last_visible_action_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "sequence_id"

    const-string v2, "threads"

    const-string v3, "sequence_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "name"

    const-string v2, "threads"

    const-string v3, "name"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "senders"

    const-string v2, "threads"

    const-string v3, "senders"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "snippet"

    const-string v2, "threads"

    const-string v3, "snippet"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "snippet_sender"

    const-string v2, "threads"

    const-string v3, "snippet_sender"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_snippet"

    const-string v2, "threads"

    const-string v3, "admin_snippet"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "timestamp_ms"

    const-string v2, "threads"

    const-string v3, "timestamp_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_read_timestamp_ms"

    const-string v2, "threads"

    const-string v3, "last_read_timestamp_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "approx_total_message_count"

    const-string v2, "threads"

    const-string v3, "approx_total_message_count"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "unread_message_count"

    const-string v2, "threads"

    const-string v3, "unread_message_count"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "pic_hash"

    const-string v2, "threads"

    const-string v3, "pic_hash"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "can_reply_to"

    const-string v2, "threads"

    const-string v3, "can_reply_to"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "cannot_reply_reason"

    const-string v2, "threads"

    const-string v3, "cannot_reply_reason"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "pic"

    const-string v2, "threads"

    const-string v3, "pic"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_subscribed"

    const-string v2, "threads"

    const-string v3, "is_subscribed"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "folder"

    const-string v2, "threads"

    const-string v3, "folder"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "draft"

    const-string v2, "threads"

    const-string v3, "draft"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_fetch_time_ms"

    const-string v2, "threads"

    const-string v3, "last_fetch_time_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "missed_call_status"

    const-string v2, "threads"

    const-string v3, "missed_call_status"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "mute_until"

    const-string v2, "threads"

    const-string v3, "mute_until"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "timestamp_in_folder_ms"

    const-string v2, "folders"

    const-string v3, "timestamp_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "group_rank"

    const-string v2, "group_conversations"

    const-string v3, "rank"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "group_chat_rank"

    const-string v2, "threads"

    const-string v3, "group_chat_rank"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_fetch_action_id"

    const-string v2, "threads"

    const-string v3, "last_fetch_action_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "initial_fetch_complete"

    const-string v2, "threads"

    const-string v3, "initial_fetch_complete"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "pinned_threads_display_order"

    const-string v2, "pinned_threads"

    const-string v3, "display_order"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "me_bubble_color"

    const-string v2, "threads"

    const-string v3, "me_bubble_color"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "other_bubble_color"

    const-string v2, "threads"

    const-string v3, "other_bubble_color"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "wallpaper_color"

    const-string v2, "threads"

    const-string v3, "wallpaper_color"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "custom_like_emoji"

    const-string v2, "threads"

    const-string v3, "custom_like_emoji"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "outgoing_message_lifetime"

    const-string v2, "threads"

    const-string v3, "outgoing_message_lifetime"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "custom_nicknames"

    const-string v2, "threads"

    const-string v3, "custom_nicknames"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "invite_uri"

    const-string v2, "threads"

    const-string v3, "invite_uri"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_last_message_sponsored"

    const-string v2, "threads"

    const-string v3, "is_last_message_sponsored"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "game_data"

    const-string v2, "threads"

    const-string v3, "game_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_joinable"

    const-string v2, "threads"

    const-string v3, "is_joinable"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "requires_approval"

    const-string v2, "threads"

    const-string v3, "requires_approval"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "rtc_call_info"

    const-string v2, "threads"

    const-string v3, "rtc_call_info"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_message_commerce_message_type"

    const-string v2, "threads"

    const-string v3, "last_message_commerce_message_type"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_thread_queue_enabled"

    const-string v2, "threads"

    const-string v3, "is_thread_queue_enabled"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "group_description"

    const-string v2, "threads"

    const-string v3, "group_description"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "media_preview"

    const-string v2, "threads"

    const-string v3, "media_preview"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "booking_requests"

    const-string v2, "threads"

    const-string v3, "booking_requests"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_call_ms"

    const-string v2, "threads"

    const-string v3, "last_call_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_discoverable"

    const-string v2, "threads"

    const-string v3, "is_discoverable"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "last_sponsored_message_call_to_action"

    const-string v2, "threads"

    const-string v3, "last_sponsored_message_call_to_action"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_thread_key"

    const-string v2, "threads"

    sget-object v3, LX/6dq;->W:LX/0U1;

    .line 1117247
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117248
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_thread_read_watermark_timestamp_ms"

    const-string v2, "montage_threads"

    sget-object v3, LX/6dq;->m:LX/0U1;

    .line 1117249
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117250
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_preview_message_id"

    const-string v2, "montage_messages"

    sget-object v3, LX/6df;->a:LX/0U1;

    .line 1117251
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117252
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_preview_attachments"

    const-string v2, "montage_messages"

    sget-object v3, LX/6df;->i:LX/0U1;

    .line 1117253
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117254
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_preview_sticker_id"

    const-string v2, "montage_messages"

    sget-object v3, LX/6df;->k:LX/0U1;

    .line 1117255
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117256
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_preview_text"

    const-string v2, "montage_messages"

    sget-object v3, LX/6df;->d:LX/0U1;

    .line 1117257
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117258
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_latest_message_timestamp_ms"

    const-string v2, "montage_messages"

    sget-object v3, LX/6df;->g:LX/0U1;

    .line 1117259
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1117260
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    invoke-virtual {v0}, LX/6cn;->a()LX/6cp;

    move-result-object v0

    sput-object v0, LX/6dt;->a:LX/6cp;

    .line 1117261
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6dt;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117242
    invoke-direct {p0}, LX/2Rg;-><init>()V

    .line 1117243
    iput-object p1, p0, LX/6dt;->b:LX/0Or;

    .line 1117244
    iput-object p2, p0, LX/6dt;->c:LX/0SG;

    .line 1117245
    return-void
.end method

.method public static a(LX/0QB;)LX/6dt;
    .locals 8

    .prologue
    .line 1117213
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1117214
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1117215
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1117216
    if-nez v1, :cond_0

    .line 1117217
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1117218
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1117219
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1117220
    sget-object v1, LX/6dt;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1117221
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1117222
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1117223
    :cond_1
    if-nez v1, :cond_4

    .line 1117224
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1117225
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1117226
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1117227
    new-instance v7, LX/6dt;

    const/16 v1, 0x274b

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v7, p0, v1}, LX/6dt;-><init>(LX/0Or;LX/0SG;)V

    .line 1117228
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1117229
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1117230
    if-nez v1, :cond_2

    .line 1117231
    sget-object v0, LX/6dt;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dt;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1117232
    :goto_1
    if-eqz v0, :cond_3

    .line 1117233
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1117234
    :goto_3
    check-cast v0, LX/6dt;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1117235
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1117236
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1117237
    :catchall_1
    move-exception v0

    .line 1117238
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1117239
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1117240
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1117241
    :cond_2
    :try_start_8
    sget-object v0, LX/6dt;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dt;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1117156
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1117157
    invoke-virtual {v2, p1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    .line 1117158
    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1117159
    invoke-static {p3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1117160
    sget-object v0, LX/6dt;->a:LX/6cp;

    invoke-virtual {v0}, LX/6cp;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1117161
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1117162
    :cond_1
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 1117163
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1117164
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1117165
    const-string v0, "t._ROWID_ AS _id"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117166
    const-string v0, "threads AS t"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117167
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 1117168
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1117169
    const-string v9, "_id"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 1117170
    sget-object v9, LX/6dt;->a:LX/6cp;

    invoke-virtual {v9, v0}, LX/6cp;->a(Ljava/lang/String;)LX/6co;

    move-result-object v9

    .line 1117171
    if-nez v9, :cond_3

    .line 1117172
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1117173
    :cond_3
    const-string v0, "threads"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1117174
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v10, ", t."

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v9, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " AS "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v9, v9, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1117175
    :cond_4
    const-string v0, "folders"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1117176
    if-nez v4, :cond_f

    .line 1117177
    const-string v0, " INNER JOIN folders AS f ON t.thread_key = f.thread_key"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v5

    .line 1117178
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, ", f."

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v9, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " AS "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v9, v9, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v4, v0

    goto/16 :goto_1

    .line 1117179
    :cond_5
    const-string v0, "group_conversations"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1117180
    if-nez v3, :cond_e

    .line 1117181
    const-string v0, " INNER JOIN group_conversations AS g ON t.thread_key = g.thread_key"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v5

    .line 1117182
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, ", g."

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v9, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, " AS "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v9, v9, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v0

    goto/16 :goto_1

    .line 1117183
    :cond_6
    const-string v0, "pinned_threads"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1117184
    if-nez v2, :cond_d

    .line 1117185
    const-string v0, " INNER JOIN "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117186
    const-string v0, "pinned_threads"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117187
    const-string v0, " AS p ON t.thread_key = p.thread_key"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v5

    .line 1117188
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, ", p."

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v9, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, " AS "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v9, v9, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v0

    goto/16 :goto_1

    .line 1117189
    :cond_7
    const-string v0, "montage_messages"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "montage_threads"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1117190
    :cond_8
    if-nez v1, :cond_9

    .line 1117191
    const-string v0, " LEFT JOIN "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117192
    const-string v0, "threads"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117193
    const-string v0, " AS mt ON mt.thread_key = t."

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117194
    const-string v0, "montage_thread_key"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " AND mt.folder=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/6ek;->MONTAGE:LX/6ek;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117196
    const-string v0, " LEFT JOIN"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117197
    const-string v0, " (SELECT *, max(timestamp_ms) FROM "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117198
    const-string v0, "messages"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117199
    const-string v0, " WHERE msg_type="

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117200
    sget-object v0, LX/2uW;->REGULAR:LX/2uW;

    iget v0, v0, LX/2uW;->dbKeyValue:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1117201
    const-string v0, " AND timestamp_ms > "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117202
    iget-object v0, p0, LX/6dt;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v10, 0x5265c00

    sub-long/2addr v0, v10

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1117203
    const-string v0, " GROUP BY thread_key)"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117204
    const-string v0, " AS mm ON mm.thread_key = mt.thread_key"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v5

    .line 1117205
    :cond_9
    const-string v0, "montage_messages"

    iget-object v10, v9, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, ", mm"

    :goto_5
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117206
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v10, "."

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v9, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " AS "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v9, v9, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    move v0, v1

    move v1, v0

    .line 1117207
    goto/16 :goto_1

    .line 1117208
    :cond_b
    const-string v0, ", mt"

    goto :goto_5

    .line 1117209
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_d
    move v0, v2

    goto/16 :goto_4

    :cond_e
    move v0, v3

    goto/16 :goto_3

    :cond_f
    move v0, v4

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 1117210
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1117211
    invoke-direct {p0, p2, p3, p5}, LX/6dt;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1117212
    iget-object v1, p0, LX/6dt;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
