.class public final enum LX/6LB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6LB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6LB;

.field public static final enum PREPARED:LX/6LB;

.field public static final enum STARTED:LX/6LB;

.field public static final enum STOPPED:LX/6LB;

.field public static final enum STOP_IN_PROGRESS:LX/6LB;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1078059
    new-instance v0, LX/6LB;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v2}, LX/6LB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LB;->PREPARED:LX/6LB;

    .line 1078060
    new-instance v0, LX/6LB;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3}, LX/6LB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LB;->STARTED:LX/6LB;

    .line 1078061
    new-instance v0, LX/6LB;

    const-string v1, "STOP_IN_PROGRESS"

    invoke-direct {v0, v1, v4}, LX/6LB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LB;->STOP_IN_PROGRESS:LX/6LB;

    .line 1078062
    new-instance v0, LX/6LB;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v5}, LX/6LB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LB;->STOPPED:LX/6LB;

    .line 1078063
    const/4 v0, 0x4

    new-array v0, v0, [LX/6LB;

    sget-object v1, LX/6LB;->PREPARED:LX/6LB;

    aput-object v1, v0, v2

    sget-object v1, LX/6LB;->STARTED:LX/6LB;

    aput-object v1, v0, v3

    sget-object v1, LX/6LB;->STOP_IN_PROGRESS:LX/6LB;

    aput-object v1, v0, v4

    sget-object v1, LX/6LB;->STOPPED:LX/6LB;

    aput-object v1, v0, v5

    sput-object v0, LX/6LB;->$VALUES:[LX/6LB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1078056
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6LB;
    .locals 1

    .prologue
    .line 1078057
    const-class v0, LX/6LB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6LB;

    return-object v0
.end method

.method public static values()[LX/6LB;
    .locals 1

    .prologue
    .line 1078058
    sget-object v0, LX/6LB;->$VALUES:[LX/6LB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6LB;

    return-object v0
.end method
