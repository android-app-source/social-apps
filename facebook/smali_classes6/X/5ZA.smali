.class public final LX/5ZA;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 944221
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    const v0, 0x5a6cf533

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "ThreadQuery"

    const-string v6, "09531a80448f179f5ac63d5a319bbda7"

    const-string v7, "message_threads"

    const-string v8, "10155265581831729"

    const/4 v9, 0x0

    const-string v0, "actor_id"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 944222
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 944223
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 944224
    sparse-switch v0, :sswitch_data_0

    .line 944225
    :goto_0
    return-object p1

    .line 944226
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 944227
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 944228
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 944229
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 944230
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 944231
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 944232
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 944233
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 944234
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 944235
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 944236
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 944237
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 944238
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 944239
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 944240
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 944241
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 944242
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 944243
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 944244
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 944245
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 944246
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 944247
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 944248
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 944249
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 944250
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_8
        -0x7226305f -> :sswitch_17
        -0x5f54881d -> :sswitch_3
        -0x5bcbf522 -> :sswitch_14
        -0x56855c4a -> :sswitch_12
        -0x4c47f2a9 -> :sswitch_11
        -0x469c40fd -> :sswitch_13
        -0x4450092f -> :sswitch_d
        -0x39e54905 -> :sswitch_18
        -0x26451294 -> :sswitch_1
        -0x1b236af7 -> :sswitch_16
        -0x179abbec -> :sswitch_15
        -0xf820fe3 -> :sswitch_7
        -0x786d0bb -> :sswitch_b
        -0x3224078 -> :sswitch_c
        -0x132889c -> :sswitch_10
        -0x8d30fe -> :sswitch_a
        0x8da57ae -> :sswitch_4
        0xe0e2e5a -> :sswitch_6
        0x19ec4b2a -> :sswitch_0
        0x2f1911b0 -> :sswitch_e
        0x3349e8c0 -> :sswitch_f
        0x5af48aaa -> :sswitch_2
        0x5ba7488b -> :sswitch_9
        0x69308369 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 944216
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 944217
    :goto_1
    return v0

    .line 944218
    :pswitch_1
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "21"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 944219
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944220
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x63f
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 944215
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueries$ThreadQueryString$1;

    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueries$ThreadQueryString$1;-><init>(LX/5ZA;Ljava/lang/Class;)V

    return-object v0
.end method
