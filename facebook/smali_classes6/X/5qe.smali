.class public LX/5qe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5qd;


# instance fields
.field private volatile a:I

.field private b:Landroid/view/ViewParent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1009505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009506
    const/4 v0, -0x1

    iput v0, p0, LX/5qe;->a:I

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1009501
    iget-object v0, p0, LX/5qe;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    .line 1009502
    iget-object v0, p0, LX/5qe;->b:Landroid/view/ViewParent;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1009503
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qe;->b:Landroid/view/ViewParent;

    .line 1009504
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1009498
    const/4 v0, -0x1

    iput v0, p0, LX/5qe;->a:I

    .line 1009499
    invoke-direct {p0}, LX/5qe;->b()V

    .line 1009500
    return-void
.end method

.method public final a(ILandroid/view/ViewParent;)V
    .locals 1
    .param p2    # Landroid/view/ViewParent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1009486
    iput p1, p0, LX/5qe;->a:I

    .line 1009487
    invoke-direct {p0}, LX/5qe;->b()V

    .line 1009488
    if-eqz p2, :cond_0

    .line 1009489
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1009490
    iput-object p2, p0, LX/5qe;->b:Landroid/view/ViewParent;

    .line 1009491
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1009492
    iget v2, p0, LX/5qe;->a:I

    .line 1009493
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v0, :cond_1

    .line 1009494
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 1009495
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 1009496
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1009497
    goto :goto_0
.end method
