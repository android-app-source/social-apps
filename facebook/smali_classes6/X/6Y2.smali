.class public LX/6Y2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/11H;

.field public final b:LX/2ae;

.field public final c:LX/0iA;


# direct methods
.method public constructor <init>(LX/11H;LX/2ae;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1109090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109091
    iput-object p1, p0, LX/6Y2;->a:LX/11H;

    .line 1109092
    iput-object p2, p0, LX/6Y2;->b:LX/2ae;

    .line 1109093
    iput-object p3, p0, LX/6Y2;->c:LX/0iA;

    .line 1109094
    return-void
.end method

.method public static a(LX/0QB;)LX/6Y2;
    .locals 6

    .prologue
    .line 1109095
    const-class v1, LX/6Y2;

    monitor-enter v1

    .line 1109096
    :try_start_0
    sget-object v0, LX/6Y2;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1109097
    sput-object v2, LX/6Y2;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1109098
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109099
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1109100
    new-instance p0, LX/6Y2;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    invoke-static {v0}, LX/2ae;->b(LX/0QB;)LX/2ae;

    move-result-object v4

    check-cast v4, LX/2ae;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-direct {p0, v3, v4, v5}, LX/6Y2;-><init>(LX/11H;LX/2ae;LX/0iA;)V

    .line 1109101
    move-object v0, p0

    .line 1109102
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1109103
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6Y2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1109104
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1109105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1109106
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1109107
    const-string v1, "interstitials_fetch_and_update"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1109108
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1109109
    const-string v1, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    .line 1109110
    iget-object v1, p0, LX/6Y2;->a:LX/11H;

    iget-object v2, p0, LX/6Y2;->b:LX/2ae;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1109111
    iget-object v2, p0, LX/6Y2;->c:LX/0iA;

    .line 1109112
    iget-object p0, v0, Lcom/facebook/interstitial/api/FetchInterstitialsParams;->a:LX/0Px;

    move-object v0, p0

    .line 1109113
    invoke-virtual {v2, v0, v1}, LX/0iA;->a(Ljava/util/List;Ljava/util/List;)V

    .line 1109114
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1109115
    move-object v0, v0

    .line 1109116
    return-object v0

    .line 1109117
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
