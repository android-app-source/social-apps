.class public LX/6Os;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/6PW;


# direct methods
.method public constructor <init>(LX/6PW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085641
    iput-object p1, p0, LX/6Os;->a:LX/6PW;

    .line 1085642
    return-void
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3

    .prologue
    .line 1085643
    const v0, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot toggle page like on a non-page_like action link."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1085644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1085645
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 1085646
    :goto_1
    invoke-static {v0}, LX/6XA;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/6XA;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/6XA;->a(Z)LX/6XA;

    move-result-object v1

    .line 1085647
    iget-object v2, v1, LX/6XA;->a:Lcom/facebook/graphql/model/GraphQLPage;

    move-object v1, v2

    .line 1085648
    move-object v0, v1

    .line 1085649
    invoke-static {p0}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v1

    .line 1085650
    iput-object v0, v1, LX/4Ys;->aI:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085651
    invoke-virtual {v1}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0

    .line 1085652
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1085653
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/6Os;
    .locals 2

    .prologue
    .line 1085654
    new-instance v1, LX/6Os;

    .line 1085655
    new-instance v0, LX/6PW;

    invoke-direct {v0}, LX/6PW;-><init>()V

    .line 1085656
    move-object v0, v0

    .line 1085657
    move-object v0, v0

    .line 1085658
    check-cast v0, LX/6PW;

    invoke-direct {v1, v0}, LX/6Os;-><init>(LX/6PW;)V

    .line 1085659
    return-object v1
.end method
