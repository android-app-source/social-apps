.class public LX/5gy;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;",
        "Lcom/facebook/graphql/model/GraphQLAlbum;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:LX/0wC;


# instance fields
.field private final c:LX/0rq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 977284
    sget-object v0, LX/0wC;->NUMBER_3:LX/0wC;

    sput-object v0, LX/5gy;->b:LX/0wC;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/0rq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 977285
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 977286
    iput-object p2, p0, LX/5gy;->c:LX/0rq;

    .line 977287
    return-void
.end method

.method public static a(LX/0QB;)LX/5gy;
    .locals 3

    .prologue
    .line 977288
    new-instance v2, LX/5gy;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v1

    check-cast v1, LX/0rq;

    invoke-direct {v2, v0, v1}, LX/5gy;-><init>(LX/0sO;LX/0rq;)V

    .line 977289
    move-object v0, v2

    .line 977290
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 977291
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 977292
    invoke-static {v0}, LX/5gX;->a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;)Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 977293
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 977294
    check-cast p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;

    .line 977295
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 977296
    iget-object v0, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 977297
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 977298
    new-instance v0, LX/5gY;

    invoke-direct {v0}, LX/5gY;-><init>()V

    move-object v0, v0

    .line 977299
    const-string v1, "node_id"

    .line 977300
    iget-object v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 977301
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977302
    const-string v1, "before"

    .line 977303
    iget-object v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 977304
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977305
    const-string v1, "after"

    .line 977306
    iget-object v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 977307
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977308
    const-string v1, "media_type"

    iget-object v2, p0, LX/5gy;->c:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->a()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 977309
    iget v1, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->d:I

    move v1, v1

    .line 977310
    if-lez v1, :cond_0

    .line 977311
    const-string v1, "first"

    .line 977312
    iget v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->d:I

    move v2, v2

    .line 977313
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977314
    :cond_0
    iget v1, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->e:I

    move v1, v1

    .line 977315
    if-lez v1, :cond_1

    .line 977316
    const-string v1, "image_width"

    .line 977317
    iget v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->e:I

    move v2, v2

    .line 977318
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977319
    :cond_1
    iget v1, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->f:I

    move v1, v1

    .line 977320
    if-lez v1, :cond_2

    .line 977321
    const-string v1, "image_height"

    .line 977322
    iget v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->f:I

    move v2, v2

    .line 977323
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977324
    :cond_2
    iget v1, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->g:I

    move v1, v1

    .line 977325
    if-lez v1, :cond_3

    .line 977326
    const-string v1, "contributor_pic_width"

    .line 977327
    iget v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->g:I

    move v2, v2

    .line 977328
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977329
    :cond_3
    iget v1, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->h:I

    move v1, v1

    .line 977330
    if-lez v1, :cond_4

    .line 977331
    const-string v1, "contributor_pic_height"

    .line 977332
    iget v2, p1, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->h:I

    move v2, v2

    .line 977333
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 977334
    :cond_4
    const-string v1, "scale"

    sget-object v2, LX/5gy;->b:LX/0wC;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 977335
    return-object v0
.end method
