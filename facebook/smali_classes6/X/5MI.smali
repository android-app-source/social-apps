.class public LX/5MI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jp;


# static fields
.field public static a:I


# instance fields
.field private b:LX/1jk;

.field private c:Ljava/lang/String;

.field private d:I

.field private final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 905363
    const v0, 0xf4240

    sput v0, LX/5MI;->a:I

    return-void
.end method

.method public constructor <init>(LX/1jk;Ljava/lang/String;J)V
    .locals 1
    .param p1    # LX/1jk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 905357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905358
    iput-object p1, p0, LX/5MI;->b:LX/1jk;

    .line 905359
    iput-object p2, p0, LX/5MI;->c:Ljava/lang/String;

    .line 905360
    const/4 v0, 0x1

    iput v0, p0, LX/5MI;->d:I

    .line 905361
    iput-wide p3, p0, LX/5MI;->e:J

    .line 905362
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 905347
    const/4 v0, -0x1

    return v0
.end method

.method public final a()LX/1jk;
    .locals 1

    .prologue
    .line 905356
    iget-object v0, p0, LX/5MI;->b:LX/1jk;

    return-object v0
.end method

.method public final a(LX/8K8;)LX/1jr;
    .locals 2
    .param p1    # LX/8K8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 905355
    new-instance v0, LX/5MH;

    iget-object v1, p0, LX/5MI;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 905350
    iget v0, p0, LX/5MI;->d:I

    .line 905351
    iget v1, p0, LX/5MI;->d:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/5MI;->d:I

    .line 905352
    iget v1, p0, LX/5MI;->d:I

    sget v2, LX/5MI;->a:I

    if-le v1, v2, :cond_0

    .line 905353
    sget v1, LX/5MI;->a:I

    iput v1, p0, LX/5MI;->d:I

    .line 905354
    :cond_0
    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905349
    const-string v0, "ConfigError"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905348
    const-string v0, "ConfigError"

    return-object v0
.end method
