.class public final LX/6KP;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ANQ;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/6KS;


# direct methods
.method public constructor <init>(LX/6KS;LX/ANQ;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1076682
    iput-object p1, p0, LX/6KP;->c:LX/6KS;

    iput-object p2, p0, LX/6KP;->a:LX/ANQ;

    iput-object p3, p0, LX/6KP;->b:Landroid/net/Uri;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1076683
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1076684
    iget-object v0, p0, LX/6KP;->c:LX/6KS;

    iget-object v0, v0, LX/6KS;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$1;-><init>(LX/6KP;)V

    const v2, -0x4ad7c48

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076685
    :goto_0
    return-void

    .line 1076686
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1076687
    if-nez v0, :cond_1

    .line 1076688
    iget-object v0, p0, LX/6KP;->c:LX/6KS;

    iget-object v0, v0, LX/6KS;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$2;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$2;-><init>(LX/6KP;)V

    const v2, 0x77cae972

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1076689
    :cond_1
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    .line 1076690
    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, LX/6KR;

    invoke-direct {v2, v0}, LX/6KR;-><init>(LX/1FJ;)V

    invoke-static {v1, v2}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    .line 1076691
    iget-object v1, p0, LX/6KP;->c:LX/6KS;

    iget-object v1, v1, LX/6KS;->a:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$3;-><init>(LX/6KP;LX/1FJ;)V

    const v0, -0xdb7c429

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1076692
    iget-object v0, p0, LX/6KP;->c:LX/6KS;

    iget-object v0, v0, LX/6KS;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$4;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/fbspecific/FbImageLoader$1$4;-><init>(LX/6KP;)V

    const v2, 0x2b73777b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076693
    return-void
.end method
