.class public final LX/5Fc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 885838
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 885839
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885840
    :goto_0
    return v1

    .line 885841
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885842
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 885843
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 885844
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885845
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 885846
    const-string v9, "cover_photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 885847
    invoke-static {p0, p1}, LX/5FO;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 885848
    :cond_2
    const-string v9, "description"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 885849
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 885850
    :cond_3
    const-string v9, "group_mediaset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 885851
    invoke-static {p0, p1}, LX/5FQ;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 885852
    :cond_4
    const-string v9, "group_member_profiles"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 885853
    invoke-static {p0, p1}, LX/5FT;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 885854
    :cond_5
    const-string v9, "group_pinned_stories"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 885855
    invoke-static {p0, p1}, LX/5Fb;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 885856
    :cond_6
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 885857
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 885858
    :cond_7
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 885859
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 885860
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 885861
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 885862
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 885863
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 885864
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 885865
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 885866
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 885867
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 885868
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 885869
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885870
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885871
    if-eqz v0, :cond_0

    .line 885872
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885873
    invoke-static {p0, v0, p2, p3}, LX/5FO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 885874
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885875
    if-eqz v0, :cond_1

    .line 885876
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885877
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885878
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885879
    if-eqz v0, :cond_2

    .line 885880
    const-string v1, "group_mediaset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885881
    invoke-static {p0, v0, p2, p3}, LX/5FQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 885882
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885883
    if-eqz v0, :cond_3

    .line 885884
    const-string v1, "group_member_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885885
    invoke-static {p0, v0, p2, p3}, LX/5FT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 885886
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885887
    if-eqz v0, :cond_4

    .line 885888
    const-string v1, "group_pinned_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885889
    invoke-static {p0, v0, p2, p3}, LX/5Fb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 885890
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885891
    if-eqz v0, :cond_5

    .line 885892
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885893
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885894
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885895
    if-eqz v0, :cond_6

    .line 885896
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885897
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885898
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885899
    return-void
.end method
