.class public LX/6Op;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:[Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Landroid/content/ContentResolver;

.field private final e:LX/1Ml;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1085576
    const-class v0, LX/6Op;

    sput-object v0, LX/6Op;->c:Ljava/lang/Class;

    .line 1085577
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "times_contacted"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "starred"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "last_time_contacted"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "custom_ringtone"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "in_visible_group"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "send_to_voicemail"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_user_profile"

    aput-object v2, v0, v1

    sput-object v0, LX/6Op;->a:[Ljava/lang/String;

    .line 1085578
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "account_type"

    aput-object v1, v0, v3

    sput-object v0, LX/6Op;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/1Ml;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/util/IsContactAccountTypeUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/1Ml;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085628
    iput-object p1, p0, LX/6Op;->d:Landroid/content/ContentResolver;

    .line 1085629
    iput-object p2, p0, LX/6Op;->e:LX/1Ml;

    .line 1085630
    iput-object p3, p0, LX/6Op;->f:LX/0Or;

    .line 1085631
    return-void
.end method

.method private static a(Landroid/database/Cursor;)Lcom/facebook/contacts/model/PhonebookContactMetadata;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x0

    .line 1085612
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    move-object v0, v10

    .line 1085613
    :goto_0
    return-object v0

    .line 1085614
    :cond_1
    const-wide/16 v4, 0x0

    .line 1085615
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1085616
    const-string v1, "_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1085617
    const-string v1, "times_contacted"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1085618
    const-string v1, "starred"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1085619
    const-string v1, "last_time_contacted"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1085620
    const-string v1, "custom_ringtone"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1085621
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1085622
    :goto_1
    const-string v1, "in_visible_group"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1085623
    const-string v1, "send_to_voicemail"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1085624
    const-string v1, "is_user_profile"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move v9, v1

    move-object v1, v8

    move v8, v2

    move v2, v7

    move v7, v3

    move v3, v6

    move v6, v0

    .line 1085625
    :goto_2
    new-instance v0, Lcom/facebook/contacts/model/PhonebookContactMetadata;

    invoke-static {v3}, LX/6Op;->a(I)Z

    move-result v3

    invoke-static {v6}, LX/6Op;->a(I)Z

    move-result v6

    invoke-static {v7}, LX/6Op;->a(I)Z

    move-result v7

    invoke-static {v8}, LX/6Op;->a(I)Z

    move-result v8

    invoke-static {v9}, LX/6Op;->a(I)Z

    move-result v9

    invoke-direct/range {v0 .. v10}, Lcom/facebook/contacts/model/PhonebookContactMetadata;-><init>(Ljava/lang/String;IZJZZZZLjava/lang/String;)V

    goto :goto_0

    .line 1085626
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    move v9, v0

    move v8, v0

    move v7, v0

    move v6, v0

    move v3, v0

    move v2, v0

    move-object v1, v10

    goto :goto_2
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1085611
    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/6Op;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1085596
    :try_start_0
    iget-object v0, p0, LX/6Op;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/6Op;->b:[Ljava/lang/String;

    const-string v3, "contact_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1085597
    :try_start_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1085598
    :cond_0
    const/4 v0, 0x0

    .line 1085599
    :goto_0
    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1085600
    if-eqz v1, :cond_1

    .line 1085601
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1085602
    :cond_1
    return-object v0

    .line 1085603
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 1085604
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1085605
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 1085606
    :cond_3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1085607
    const-string v2, "account_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1085608
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1085609
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1085610
    :cond_4
    const-string v2, ";"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/contacts/model/PhonebookContactMetadata;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1085579
    iget-object v0, p0, LX/6Op;->e:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v6

    .line 1085580
    :cond_0
    :goto_0
    return-object v0

    .line 1085581
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/6Op;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/6Op;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1085582
    :try_start_1
    invoke-static {v2}, LX/6Op;->a(Landroid/database/Cursor;)Lcom/facebook/contacts/model/PhonebookContactMetadata;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1085583
    :try_start_2
    iget-object v0, p0, LX/6Op;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085584
    invoke-static {p0, p1}, LX/6Op;->b(LX/6Op;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1085585
    iput-object v0, v1, Lcom/facebook/contacts/model/PhonebookContactMetadata;->i:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1085586
    :cond_2
    if-eqz v2, :cond_4

    .line 1085587
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    goto :goto_0

    .line 1085588
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    .line 1085589
    :goto_1
    :try_start_3
    sget-object v2, LX/6Op;->c:Ljava/lang/Class;

    const-string v3, "Got Exception when getting metadata for contact %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1085590
    if-eqz v6, :cond_0

    .line 1085591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1085592
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_3

    .line 1085593
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1085594
    :catchall_1
    move-exception v0

    move-object v6, v2

    goto :goto_2

    .line 1085595
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    move-object v6, v2

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v6, v2

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method
