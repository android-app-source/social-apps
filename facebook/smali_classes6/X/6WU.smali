.class public LX/6WU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field private b:Landroid/text/Layout;

.field public final c:Landroid/graphics/Rect;

.field public final d:LX/1nq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1106347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106348
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    .line 1106349
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, LX/6WU;->d:LX/1nq;

    .line 1106350
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1106343
    iget-boolean v0, p0, LX/6WU;->a:Z

    if-eqz v0, :cond_0

    .line 1106344
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6WU;->a:Z

    .line 1106345
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, LX/6WU;->b:Landroid/text/Layout;

    .line 1106346
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1106333
    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-ne v0, p4, :cond_0

    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-eq v0, p3, :cond_2

    :cond_0
    move v0, v1

    .line 1106334
    :goto_0
    if-eqz v0, :cond_1

    .line 1106335
    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 1106336
    iget-object v2, p0, LX/6WU;->c:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1106337
    sub-int v2, p3, p1

    .line 1106338
    if-eq v2, v0, :cond_1

    .line 1106339
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0, v2}, LX/1nq;->a(I)LX/1nq;

    .line 1106340
    iput-boolean v1, p0, LX/6WU;->a:Z

    .line 1106341
    :cond_1
    return-void

    .line 1106342
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1106325
    invoke-direct {p0}, LX/6WU;->a()V

    .line 1106326
    iget-object v0, p0, LX/6WU;->b:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v0

    shr-int/lit8 v0, v0, 0x1

    .line 1106327
    iget-object v1, p0, LX/6WU;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 1106328
    iget-object v2, p0, LX/6WU;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v0, v2, v0

    .line 1106329
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1106330
    iget-object v2, p0, LX/6WU;->b:Landroid/text/Layout;

    invoke-virtual {v2, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1106331
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1106332
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 1106323
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, LX/6WU;->a(IIII)V

    .line 1106324
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1106351
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1106352
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6WU;->a:Z

    .line 1106353
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1106310
    iget-object v1, p0, LX/6WU;->d:LX/1nq;

    if-eqz p1, :cond_0

    sget-object v0, LX/0zo;->a:LX/0zr;

    :goto_0
    invoke-virtual {v1, v0}, LX/1nq;->a(LX/0zr;)LX/1nq;

    .line 1106311
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6WU;->a:Z

    .line 1106312
    return-void

    .line 1106313
    :cond_0
    sget-object v0, LX/0zo;->c:LX/0zr;

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1106321
    invoke-direct {p0}, LX/6WU;->a()V

    .line 1106322
    iget-object v0, p0, LX/6WU;->b:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 1106318
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->b(I)LX/1nq;

    .line 1106319
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6WU;->a:Z

    .line 1106320
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1106317
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 1106314
    iget-object v0, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->c(I)LX/1nq;

    .line 1106315
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6WU;->a:Z

    .line 1106316
    return-void
.end method
