.class public LX/6Z1;
.super Ljava/lang/ref/WeakReference;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1110806
    invoke-direct {p0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 1110807
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1110808
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/6Z1;

    if-nez v1, :cond_1

    .line 1110809
    :cond_0
    :goto_0
    return v0

    .line 1110810
    :cond_1
    check-cast p1, LX/6Z1;

    .line 1110811
    invoke-virtual {p0}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1110812
    invoke-virtual {p1}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object v2

    .line 1110813
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 1110814
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1110815
    invoke-virtual {p0}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object v0

    .line 1110816
    if-eqz v0, :cond_0

    .line 1110817
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 1110818
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
