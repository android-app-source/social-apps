.class public LX/6Lu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1078938
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->values()[Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/6Lu;->c:LX/0Rf;

    .line 1078939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/6Lu;->d:LX/0Rf;

    .line 1078940
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEIGHBOURHOOD:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/6Lu;->e:LX/0Rf;

    .line 1078941
    sget-object v0, LX/6Lu;->c:LX/0Rf;

    sget-object v1, LX/6Lu;->d:LX/0Rf;

    invoke-static {v0, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 1078942
    sput-object v0, LX/6Lu;->a:LX/0Rf;

    sget-object v1, LX/6Lu;->e:LX/0Rf;

    invoke-static {v0, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/6Lu;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1078937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
