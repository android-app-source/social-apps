.class public LX/6Zj;
.super Lcom/google/android/maps/MapView;
.source ""

# interfaces
.implements LX/31N;


# instance fields
.field private a:Lcom/google/android/maps/GeoPoint;

.field private b:LX/6aC;

.field private c:LX/6aB;

.field private d:Lcom/google/android/maps/GeoPoint;

.field private e:Z

.field private f:I

.field public g:Z

.field private h:LX/0Sh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1111428
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1111429
    iput-boolean v1, p0, LX/6Zj;->g:Z

    .line 1111430
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v1, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, LX/6Zj;->a:Lcom/google/android/maps/GeoPoint;

    .line 1111431
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v1, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, LX/6Zj;->d:Lcom/google/android/maps/GeoPoint;

    .line 1111432
    invoke-virtual {p0}, LX/6Zj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 1111433
    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/6Zj;->h:LX/0Sh;

    .line 1111434
    return-void
.end method


# virtual methods
.method public final computeScroll()V
    .locals 2

    .prologue
    .line 1111453
    invoke-super {p0}, Lcom/google/android/maps/MapView;->computeScroll()V

    .line 1111454
    invoke-virtual {p0}, LX/6Zj;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    .line 1111455
    iget-boolean v1, p0, LX/6Zj;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6Zj;->d:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/GeoPoint;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1111456
    iget-object v0, p0, LX/6Zj;->h:LX/0Sh;

    new-instance v1, Lcom/facebook/maps/FacebookMapView$2;

    invoke-direct {v1, p0}, Lcom/facebook/maps/FacebookMapView$2;-><init>(LX/6Zj;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1111457
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Zj;->e:Z

    .line 1111458
    :goto_0
    return-void

    .line 1111459
    :cond_0
    iput-object v0, p0, LX/6Zj;->d:Lcom/google/android/maps/GeoPoint;

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1111446
    invoke-super {p0, p1}, Lcom/google/android/maps/MapView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1111447
    invoke-virtual {p0}, LX/6Zj;->getZoomLevel()I

    move-result v0

    .line 1111448
    iget v1, p0, LX/6Zj;->f:I

    if-eq v0, v1, :cond_0

    .line 1111449
    iput v0, p0, LX/6Zj;->f:I

    .line 1111450
    iget-object v0, p0, LX/6Zj;->b:LX/6aC;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6Zj;->e:Z

    if-nez v0, :cond_0

    .line 1111451
    iget-object v0, p0, LX/6Zj;->h:LX/0Sh;

    new-instance v1, Lcom/facebook/maps/FacebookMapView$1;

    invoke-direct {v1, p0}, Lcom/facebook/maps/FacebookMapView$1;-><init>(LX/6Zj;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 1111452
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1111460
    invoke-super/range {p0 .. p5}, Lcom/google/android/maps/MapView;->onLayout(ZIIII)V

    .line 1111461
    iget-object v0, p0, LX/6Zj;->h:LX/0Sh;

    new-instance v1, Lcom/facebook/maps/FacebookMapView$3;

    invoke-direct {v1, p0}, Lcom/facebook/maps/FacebookMapView$3;-><init>(LX/6Zj;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1111462
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1111439
    invoke-virtual {p0}, LX/6Zj;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    .line 1111440
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 1111441
    iput-object v1, p0, LX/6Zj;->a:Lcom/google/android/maps/GeoPoint;

    move-object v0, p0

    .line 1111442
    :goto_0
    const/4 v1, 0x0

    move v3, v1

    move-object v1, v0

    move v0, v3

    :goto_1
    iput-boolean v0, v1, LX/6Zj;->e:Z

    .line 1111443
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/maps/MapView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1111444
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1111445
    iget-object v2, p0, LX/6Zj;->a:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2, v1}, Lcom/google/android/maps/GeoPoint;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move-object v1, p0

    goto :goto_1

    :cond_2
    move-object v0, p0

    goto :goto_0
.end method

.method public setOnMapReadyListener(LX/6aB;)V
    .locals 0

    .prologue
    .line 1111437
    iput-object p1, p0, LX/6Zj;->c:LX/6aB;

    .line 1111438
    return-void
.end method

.method public setOnMovementListener(LX/6aC;)V
    .locals 0

    .prologue
    .line 1111435
    iput-object p1, p0, LX/6Zj;->b:LX/6aC;

    .line 1111436
    return-void
.end method
