.class public LX/55H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/feed/DeleteStoryMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/55H;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 829119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829120
    return-void
.end method

.method public static a(LX/0QB;)LX/55H;
    .locals 3

    .prologue
    .line 829121
    sget-object v0, LX/55H;->a:LX/55H;

    if-nez v0, :cond_1

    .line 829122
    const-class v1, LX/55H;

    monitor-enter v1

    .line 829123
    :try_start_0
    sget-object v0, LX/55H;->a:LX/55H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 829124
    if-eqz v2, :cond_0

    .line 829125
    :try_start_1
    new-instance v0, LX/55H;

    invoke-direct {v0}, LX/55H;-><init>()V

    .line 829126
    move-object v0, v0

    .line 829127
    sput-object v0, LX/55H;->a:LX/55H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 829128
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 829129
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 829130
    :cond_1
    sget-object v0, LX/55H;->a:LX/55H;

    return-object v0

    .line 829131
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 829132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 829133
    check-cast p1, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    .line 829134
    iget-object v0, p1, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    sget-object v1, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    if-eq v0, v1, :cond_0

    .line 829135
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to get API request when we should not be deleting from server"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 829136
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 829137
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829138
    new-instance v0, LX/14N;

    const-string v1, "graphStoryDelete"

    const-string v2, "DELETE"

    iget-object v3, p1, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 829139
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 829140
    const/4 v0, 0x0

    return-object v0
.end method
