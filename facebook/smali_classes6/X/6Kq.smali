.class public final LX/6Kq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/6Kr;


# direct methods
.method public constructor <init>(LX/6Kr;)V
    .locals 0

    .prologue
    .line 1077507
    iput-object p1, p0, LX/6Kq;->a:LX/6Kr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 10

    .prologue
    .line 1077508
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-boolean v0, v0, LX/6Kr;->f:Z

    if-eqz v0, :cond_1

    .line 1077509
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v1, v1, LX/6Kr;->a:LX/6Kq;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1077510
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    const/4 v1, 0x0

    .line 1077511
    iput-object v1, v0, LX/6Kr;->c:LX/6Km;

    .line 1077512
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    const/4 v1, 0x0

    .line 1077513
    iput-boolean v1, v0, LX/6Kr;->f:Z

    .line 1077514
    :cond_0
    :goto_0
    return-void

    .line 1077515
    :cond_1
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->c:LX/6Km;

    if-eqz v0, :cond_0

    .line 1077516
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-wide v0, v0, LX/6Kr;->e:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_4

    .line 1077517
    :cond_2
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1077518
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v1, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v1, v1, LX/6Kr;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v1, v1, LX/6Kr;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long v4, p1, v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    mul-long/2addr v2, v4

    .line 1077519
    iput-wide v2, v0, LX/6Kr;->e:J

    .line 1077520
    :cond_3
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->c:LX/6Km;

    invoke-virtual {v0}, LX/6Km;->a()V

    goto :goto_0

    .line 1077521
    :cond_4
    iget-object v0, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v0, v0, LX/6Kr;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/6Kq;->a:LX/6Kr;

    iget-object v1, v1, LX/6Kr;->a:LX/6Kq;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method
