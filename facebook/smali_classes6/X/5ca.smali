.class public final LX/5ca;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 963162
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 963163
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 963164
    :goto_0
    return v1

    .line 963165
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 963166
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 963167
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 963168
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 963169
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 963170
    const-string v7, "passenger"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 963171
    invoke-static {p0, p1}, LX/5cZ;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 963172
    :cond_2
    const-string v7, "product_items"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 963173
    invoke-static {p0, p1}, LX/5cb;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 963174
    :cond_3
    const-string v7, "seat"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 963175
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 963176
    :cond_4
    const-string v7, "seat_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 963177
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 963178
    :cond_5
    const-string v7, "travel_class"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 963179
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 963180
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 963181
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 963182
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 963183
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 963184
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 963185
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 963186
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 963187
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 963188
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963189
    if-eqz v0, :cond_0

    .line 963190
    const-string v1, "passenger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963191
    invoke-static {p0, v0, p2}, LX/5cZ;->a(LX/15i;ILX/0nX;)V

    .line 963192
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 963193
    if-eqz v0, :cond_1

    .line 963194
    const-string v1, "product_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963195
    invoke-static {p0, v0, p2, p3}, LX/5cb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 963196
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963197
    if-eqz v0, :cond_2

    .line 963198
    const-string v1, "seat"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963199
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963200
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963201
    if-eqz v0, :cond_3

    .line 963202
    const-string v1, "seat_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963203
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963204
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963205
    if-eqz v0, :cond_4

    .line 963206
    const-string v1, "travel_class"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963207
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963208
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 963209
    return-void
.end method
