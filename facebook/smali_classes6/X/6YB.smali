.class public final enum LX/6YB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6YB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6YB;

.field public static final enum BUY_LATER:LX/6YB;

.field public static final enum CARRIER_ONLY:LX/6YB;

.field public static final enum DATA:LX/6YB;

.field public static final enum DATA_MB:LX/6YB;

.field public static final enum DATA_TIME:LX/6YB;

.field public static final enum DATA_UNLIMITED:LX/6YB;

.field public static final enum LOAN:LX/6YB;

.field public static final enum SMS:LX/6YB;

.field public static final enum UNKNOWN:LX/6YB;

.field public static final enum URL_PROMO:LX/6YB;

.field public static final enum VOICE:LX/6YB;


# instance fields
.field private final mType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1109307
    new-instance v0, LX/6YB;

    const-string v1, "DATA"

    const-string v2, "data"

    invoke-direct {v0, v1, v4, v2}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->DATA:LX/6YB;

    .line 1109308
    new-instance v0, LX/6YB;

    const-string v1, "DATA_MB"

    const-string v2, "data_mb"

    invoke-direct {v0, v1, v5, v2}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->DATA_MB:LX/6YB;

    .line 1109309
    new-instance v0, LX/6YB;

    const-string v1, "DATA_TIME"

    const-string v2, "data_time"

    invoke-direct {v0, v1, v6, v2}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->DATA_TIME:LX/6YB;

    .line 1109310
    new-instance v0, LX/6YB;

    const-string v1, "DATA_UNLIMITED"

    const-string v2, "data_unlimited"

    invoke-direct {v0, v1, v7, v2}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->DATA_UNLIMITED:LX/6YB;

    .line 1109311
    new-instance v0, LX/6YB;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v8, v2}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->UNKNOWN:LX/6YB;

    .line 1109312
    new-instance v0, LX/6YB;

    const-string v1, "SMS"

    const/4 v2, 0x5

    const-string v3, "sms"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->SMS:LX/6YB;

    .line 1109313
    new-instance v0, LX/6YB;

    const-string v1, "VOICE"

    const/4 v2, 0x6

    const-string v3, "voice"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->VOICE:LX/6YB;

    .line 1109314
    new-instance v0, LX/6YB;

    const-string v1, "CARRIER_ONLY"

    const/4 v2, 0x7

    const-string v3, "carrier_only"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->CARRIER_ONLY:LX/6YB;

    .line 1109315
    new-instance v0, LX/6YB;

    const-string v1, "LOAN"

    const/16 v2, 0x8

    const-string v3, "loan"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->LOAN:LX/6YB;

    .line 1109316
    new-instance v0, LX/6YB;

    const-string v1, "URL_PROMO"

    const/16 v2, 0x9

    const-string v3, "url_promo"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->URL_PROMO:LX/6YB;

    .line 1109317
    new-instance v0, LX/6YB;

    const-string v1, "BUY_LATER"

    const/16 v2, 0xa

    const-string v3, "buy_later"

    invoke-direct {v0, v1, v2, v3}, LX/6YB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6YB;->BUY_LATER:LX/6YB;

    .line 1109318
    const/16 v0, 0xb

    new-array v0, v0, [LX/6YB;

    sget-object v1, LX/6YB;->DATA:LX/6YB;

    aput-object v1, v0, v4

    sget-object v1, LX/6YB;->DATA_MB:LX/6YB;

    aput-object v1, v0, v5

    sget-object v1, LX/6YB;->DATA_TIME:LX/6YB;

    aput-object v1, v0, v6

    sget-object v1, LX/6YB;->DATA_UNLIMITED:LX/6YB;

    aput-object v1, v0, v7

    sget-object v1, LX/6YB;->UNKNOWN:LX/6YB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6YB;->SMS:LX/6YB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6YB;->VOICE:LX/6YB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6YB;->CARRIER_ONLY:LX/6YB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6YB;->LOAN:LX/6YB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6YB;->URL_PROMO:LX/6YB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6YB;->BUY_LATER:LX/6YB;

    aput-object v2, v0, v1

    sput-object v0, LX/6YB;->$VALUES:[LX/6YB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1109319
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1109320
    iput-object p3, p0, LX/6YB;->mType:Ljava/lang/String;

    .line 1109321
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/6YB;
    .locals 5

    .prologue
    .line 1109322
    if-eqz p0, :cond_1

    .line 1109323
    invoke-static {}, LX/6YB;->values()[LX/6YB;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1109324
    iget-object v4, v0, LX/6YB;->mType:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1109325
    :goto_1
    return-object v0

    .line 1109326
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1109327
    :cond_1
    sget-object v0, LX/6YB;->UNKNOWN:LX/6YB;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6YB;
    .locals 1

    .prologue
    .line 1109328
    const-class v0, LX/6YB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6YB;

    return-object v0
.end method

.method public static values()[LX/6YB;
    .locals 1

    .prologue
    .line 1109329
    sget-object v0, LX/6YB;->$VALUES:[LX/6YB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6YB;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109330
    iget-object v0, p0, LX/6YB;->mType:Ljava/lang/String;

    return-object v0
.end method
