.class public final LX/69C;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/Bitmap;

.field private static final m:LX/68r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68r",
            "<",
            "LX/69C;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:LX/68r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:LX/68r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static p:Landroid/graphics/BitmapFactory$Options;

.field private static q:Z


# instance fields
.field public b:I

.field public c:I

.field public d:J

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public final i:[LX/69C;

.field public j:LX/69C;

.field public k:LX/69C;

.field public volatile l:I

.field public r:Landroid/graphics/Bitmap;

.field private s:Landroid/graphics/BitmapFactory$Options;

.field public final t:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1057928
    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, LX/69C;->a:Landroid/graphics/Bitmap;

    .line 1057929
    new-instance v1, LX/68r;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/68r;-><init>(I)V

    sput-object v1, LX/69C;->m:LX/68r;

    .line 1057930
    new-instance v1, LX/68r;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, LX/68r;-><init>(I)V

    sput-object v1, LX/69C;->n:LX/68r;

    .line 1057931
    new-instance v1, LX/68r;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, LX/68r;-><init>(I)V

    sput-object v1, LX/69C;->o:LX/68r;

    .line 1057932
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    :goto_0
    sput-boolean v0, LX/69C;->q:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(II)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1057903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1057904
    iput v2, p0, LX/69C;->b:I

    .line 1057905
    iput v2, p0, LX/69C;->c:I

    .line 1057906
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/69C;->d:J

    .line 1057907
    iput v2, p0, LX/69C;->e:I

    .line 1057908
    iput v2, p0, LX/69C;->f:I

    .line 1057909
    iput v2, p0, LX/69C;->g:I

    .line 1057910
    iput v3, p0, LX/69C;->h:I

    .line 1057911
    const/4 v0, 0x4

    new-array v0, v0, [LX/69C;

    iput-object v0, p0, LX/69C;->i:[LX/69C;

    .line 1057912
    iput-object v5, p0, LX/69C;->j:LX/69C;

    .line 1057913
    iput-object v5, p0, LX/69C;->k:LX/69C;

    .line 1057914
    iput v3, p0, LX/69C;->l:I

    .line 1057915
    new-instance v0, Lcom/facebook/android/maps/model/Tile$1;

    invoke-direct {v0, p0}, Lcom/facebook/android/maps/model/Tile$1;-><init>(LX/69C;)V

    iput-object v0, p0, LX/69C;->t:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1057916
    iput p1, p0, LX/69C;->c:I

    .line 1057917
    iput p2, p0, LX/69C;->b:I

    .line 1057918
    sget-boolean v0, LX/69C;->q:Z

    if-nez v0, :cond_1

    .line 1057919
    sget-object v0, LX/69C;->p:Landroid/graphics/BitmapFactory$Options;

    if-nez v0, :cond_0

    .line 1057920
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1057921
    sput-object v0, LX/69C;->p:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1057922
    :cond_0
    sget-object v0, LX/69C;->p:Landroid/graphics/BitmapFactory$Options;

    iput-object v0, p0, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    .line 1057923
    :goto_0
    return-void

    .line 1057924
    :cond_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    .line 1057925
    iget-object v0, p0, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1057926
    iget-object v0, p0, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1057927
    iget-object v0, p0, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    goto :goto_0
.end method

.method public static a()LX/69C;
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1057902
    invoke-static {v0, v0}, LX/69C;->a(II)LX/69C;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)LX/69C;
    .locals 1

    .prologue
    .line 1057896
    sget-object v0, LX/69C;->m:LX/68r;

    invoke-virtual {v0}, LX/68r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69C;

    .line 1057897
    if-nez v0, :cond_0

    .line 1057898
    new-instance v0, LX/69C;

    invoke-direct {v0, p0, p1}, LX/69C;-><init>(II)V

    .line 1057899
    :goto_0
    return-object v0

    .line 1057900
    :cond_0
    iput p0, v0, LX/69C;->c:I

    .line 1057901
    iput p1, v0, LX/69C;->b:I

    goto :goto_0
.end method

.method public static a([BI)LX/69C;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1057847
    invoke-static {}, LX/69C;->a()LX/69C;

    move-result-object v2

    .line 1057848
    sget-boolean v0, LX/69C;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 1057849
    iget-object v3, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    sget-object v0, LX/69C;->n:LX/68r;

    invoke-virtual {v0}, LX/68r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 1057850
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v3, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p0, v0, p1, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v2, LX/69C;->r:Landroid/graphics/Bitmap;

    .line 1057851
    sget-boolean v0, LX/69C;->q:Z

    if-eqz v0, :cond_1

    .line 1057852
    iget-object v0, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x0

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1057853
    :cond_1
    :goto_0
    iget-object v0, v2, LX/69C;->r:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 1057854
    invoke-virtual {v2}, LX/69C;->c()V

    move-object v0, v1

    .line 1057855
    :goto_1
    return-object v0

    .line 1057856
    :catch_0
    move-exception v0

    .line 1057857
    sget-object v3, LX/31U;->l:LX/31U;

    invoke-virtual {v3, v0}, LX/31U;->a(Ljava/lang/Throwable;)V

    .line 1057858
    sput-boolean v4, LX/69C;->q:Z

    .line 1057859
    iget-object v0, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1057860
    iget-object v0, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 1057861
    sget-object v0, LX/69C;->n:LX/68r;

    invoke-virtual {v0}, LX/68r;->d()V

    .line 1057862
    iget-object v0, v2, LX/69C;->s:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p0, v4, p1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v2, LX/69C;->r:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 1057863
    :cond_2
    iget-object v0, v2, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, v2, LX/69C;->c:I

    .line 1057864
    iget-object v0, v2, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, v2, LX/69C;->b:I

    move-object v0, v2

    .line 1057865
    goto :goto_1
.end method

.method public static e(LX/69C;)V
    .locals 1

    .prologue
    .line 1057892
    const/4 v0, 0x0

    iput v0, p0, LX/69C;->l:I

    .line 1057893
    iget-object v0, p0, LX/69C;->k:LX/69C;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/69C;->j:LX/69C;

    if-nez v0, :cond_0

    .line 1057894
    invoke-virtual {p0}, LX/69C;->c()V

    .line 1057895
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1057933
    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    sget-object v1, LX/69C;->a:Landroid/graphics/Bitmap;

    if-ne v0, v1, :cond_1

    .line 1057934
    :cond_0
    iput-object v2, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    .line 1057935
    :goto_0
    return-void

    .line 1057936
    :cond_1
    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v0, v1, :cond_2

    .line 1057937
    sget-object v0, LX/69C;->o:LX/68r;

    iget-object v1, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1057938
    :goto_1
    iput-object v2, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 1057939
    :cond_2
    sget-boolean v0, LX/69C;->q:Z

    if-eqz v0, :cond_3

    .line 1057940
    sget-object v0, LX/69C;->n:LX/68r;

    iget-object v1, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LX/68r;->a(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1057941
    :cond_3
    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1
.end method


# virtual methods
.method public final a(III)LX/69C;
    .locals 0

    .prologue
    .line 1057888
    iput p1, p0, LX/69C;->f:I

    .line 1057889
    iput p2, p0, LX/69C;->g:I

    .line 1057890
    iput p3, p0, LX/69C;->e:I

    .line 1057891
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1057884
    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    if-eq v0, p1, :cond_0

    .line 1057885
    invoke-direct {p0}, LX/69C;->f()V

    .line 1057886
    :cond_0
    iput-object p1, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    .line 1057887
    return-void
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1057883
    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1057867
    iput v3, p0, LX/69C;->c:I

    .line 1057868
    iput v3, p0, LX/69C;->b:I

    move v0, v1

    .line 1057869
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 1057870
    iget-object v2, p0, LX/69C;->i:[LX/69C;

    aput-object v4, v2, v0

    .line 1057871
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1057872
    :cond_0
    invoke-direct {p0}, LX/69C;->f()V

    .line 1057873
    iput v1, p0, LX/69C;->l:I

    .line 1057874
    iput-object v4, p0, LX/69C;->k:LX/69C;

    .line 1057875
    iput-object v4, p0, LX/69C;->j:LX/69C;

    .line 1057876
    iput v3, p0, LX/69C;->f:I

    .line 1057877
    iput v3, p0, LX/69C;->g:I

    .line 1057878
    iput v3, p0, LX/69C;->e:I

    .line 1057879
    iput v1, p0, LX/69C;->h:I

    .line 1057880
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/69C;->d:J

    .line 1057881
    sget-object v0, LX/69C;->m:LX/68r;

    invoke-virtual {v0, p0}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1057882
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1057866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " {x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/69C;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/69C;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zoom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/69C;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/69C;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/69C;->r:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const-string v0, "x"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "o"

    goto :goto_0
.end method
