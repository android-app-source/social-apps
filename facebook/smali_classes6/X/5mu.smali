.class public final enum LX/5mu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5mu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5mu;

.field public static final enum PROFILE_PHOTO_ALBUM:LX/5mu;


# instance fields
.field private final mAlbumName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1002914
    new-instance v0, LX/5mu;

    const-string v1, "PROFILE_PHOTO_ALBUM"

    const-string v2, "PROFILE_PHOTO"

    invoke-direct {v0, v1, v3, v2}, LX/5mu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5mu;->PROFILE_PHOTO_ALBUM:LX/5mu;

    .line 1002915
    const/4 v0, 0x1

    new-array v0, v0, [LX/5mu;

    sget-object v1, LX/5mu;->PROFILE_PHOTO_ALBUM:LX/5mu;

    aput-object v1, v0, v3

    sput-object v0, LX/5mu;->$VALUES:[LX/5mu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1002911
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1002912
    iput-object p3, p0, LX/5mu;->mAlbumName:Ljava/lang/String;

    .line 1002913
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5mu;
    .locals 1

    .prologue
    .line 1002910
    const-class v0, LX/5mu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5mu;

    return-object v0
.end method

.method public static values()[LX/5mu;
    .locals 1

    .prologue
    .line 1002909
    sget-object v0, LX/5mu;->$VALUES:[LX/5mu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5mu;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1002908
    iget-object v0, p0, LX/5mu;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method
