.class public final enum LX/6dz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6dz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6dz;

.field public static final enum DELETE:LX/6dz;

.field public static final enum NORMAL:LX/6dz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1117307
    new-instance v0, LX/6dz;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/6dz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6dz;->NORMAL:LX/6dz;

    .line 1117308
    new-instance v0, LX/6dz;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, LX/6dz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6dz;->DELETE:LX/6dz;

    .line 1117309
    const/4 v0, 0x2

    new-array v0, v0, [LX/6dz;

    sget-object v1, LX/6dz;->NORMAL:LX/6dz;

    aput-object v1, v0, v2

    sget-object v1, LX/6dz;->DELETE:LX/6dz;

    aput-object v1, v0, v3

    sput-object v0, LX/6dz;->$VALUES:[LX/6dz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1117310
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6dz;
    .locals 1

    .prologue
    .line 1117311
    const-class v0, LX/6dz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6dz;

    return-object v0
.end method

.method public static values()[LX/6dz;
    .locals 1

    .prologue
    .line 1117312
    sget-object v0, LX/6dz;->$VALUES:[LX/6dz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6dz;

    return-object v0
.end method
