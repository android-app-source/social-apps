.class public LX/5QQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 913194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913195
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/5QQ;->a:Landroid/os/Bundle;

    .line 913196
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/5QQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/5QQ;"
        }
    .end annotation

    .prologue
    .line 913197
    if-eqz p1, :cond_0

    .line 913198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 913199
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 913200
    iget-object v1, p0, LX/5QQ;->a:Landroid/os/Bundle;

    const-string v2, "group_feed_hoisted_story_ids"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 913201
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5QQ;
    .locals 2

    .prologue
    .line 913202
    iget-object v0, p0, LX/5QQ;->a:Landroid/os/Bundle;

    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 913203
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5QQ;
    .locals 2

    .prologue
    .line 913204
    iget-object v0, p0, LX/5QQ;->a:Landroid/os/Bundle;

    const-string v1, "group_feed_title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 913205
    return-object p0
.end method
