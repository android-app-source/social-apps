.class public final LX/6e0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/dialog/MenuDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/dialog/MenuDialogFragment;)V
    .locals 0

    .prologue
    .line 1117344
    iput-object p1, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1117345
    iget-object v0, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->n:LX/6e1;

    if-eqz v0, :cond_0

    .line 1117346
    iget-object v0, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117347
    iget-object v1, v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1117348
    iget-object v0, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    iget-object v2, v0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->n:LX/6e1;

    iget-object v0, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117349
    iget-object p1, v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    move-object v0, p1

    .line 1117350
    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/dialog/MenuDialogItem;

    invoke-interface {v2, v0, v1}, LX/6e1;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1117351
    iget-object v0, p0, LX/6e0;->a:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1117352
    :cond_0
    return-void
.end method
