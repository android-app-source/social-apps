.class public final LX/5tO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1015838
    const/4 v13, 0x0

    .line 1015839
    const/4 v12, 0x0

    .line 1015840
    const/4 v11, 0x0

    .line 1015841
    const/4 v10, 0x0

    .line 1015842
    const/4 v9, 0x0

    .line 1015843
    const/4 v8, 0x0

    .line 1015844
    const/4 v7, 0x0

    .line 1015845
    const/4 v6, 0x0

    .line 1015846
    const/4 v5, 0x0

    .line 1015847
    const/4 v4, 0x0

    .line 1015848
    const/4 v3, 0x0

    .line 1015849
    const/4 v2, 0x0

    .line 1015850
    const/4 v1, 0x0

    .line 1015851
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1015852
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1015853
    const/4 v1, 0x0

    .line 1015854
    :goto_0
    return v1

    .line 1015855
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1015856
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_c

    .line 1015857
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1015858
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1015859
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1015860
    const-string v15, "address"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1015861
    invoke-static/range {p0 .. p1}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1015862
    :cond_2
    const-string v15, "can_viewer_like"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1015863
    const/4 v2, 0x1

    .line 1015864
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1015865
    :cond_3
    const-string v15, "contextItemRows"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1015866
    invoke-static/range {p0 .. p1}, LX/5O0;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1015867
    :cond_4
    const-string v15, "cover_photo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1015868
    invoke-static/range {p0 .. p1}, LX/5tN;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1015869
    :cond_5
    const-string v15, "does_viewer_like"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1015870
    const/4 v1, 0x1

    .line 1015871
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1015872
    :cond_6
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1015873
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1015874
    :cond_7
    const-string v15, "location"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1015875
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1015876
    :cond_8
    const-string v15, "menu_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1015877
    invoke-static/range {p0 .. p1}, LX/5tP;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1015878
    :cond_9
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1015879
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1015880
    :cond_a
    const-string v15, "overall_star_rating"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1015881
    invoke-static/range {p0 .. p1}, LX/5tQ;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1015882
    :cond_b
    const-string v15, "subscribe_status"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1015883
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1015884
    :cond_c
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1015885
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1015886
    if-eqz v2, :cond_d

    .line 1015887
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1015888
    :cond_d
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1015889
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1015890
    if-eqz v1, :cond_e

    .line 1015891
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->a(IZ)V

    .line 1015892
    :cond_e
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1015893
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1015894
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1015895
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1015896
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1015897
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1015898
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 1015899
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1015900
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015901
    if-eqz v0, :cond_0

    .line 1015902
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015903
    invoke-static {p0, v0, p2}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1015904
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1015905
    if-eqz v0, :cond_1

    .line 1015906
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015907
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1015908
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015909
    if-eqz v0, :cond_2

    .line 1015910
    const-string v1, "contextItemRows"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015911
    invoke-static {p0, v0, p2, p3}, LX/5O0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1015912
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015913
    if-eqz v0, :cond_3

    .line 1015914
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015915
    invoke-static {p0, v0, p2, p3}, LX/5tN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1015916
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1015917
    if-eqz v0, :cond_4

    .line 1015918
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015919
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1015920
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1015921
    if-eqz v0, :cond_5

    .line 1015922
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015923
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1015924
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015925
    if-eqz v0, :cond_6

    .line 1015926
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015927
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1015928
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015929
    if-eqz v0, :cond_7

    .line 1015930
    const-string v1, "menu_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015931
    invoke-static {p0, v0, p2}, LX/5tP;->a(LX/15i;ILX/0nX;)V

    .line 1015932
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1015933
    if-eqz v0, :cond_8

    .line 1015934
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015935
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1015936
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1015937
    if-eqz v0, :cond_9

    .line 1015938
    const-string v1, "overall_star_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015939
    invoke-static {p0, v0, p2}, LX/5tQ;->a(LX/15i;ILX/0nX;)V

    .line 1015940
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1015941
    if-eqz v0, :cond_a

    .line 1015942
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015943
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1015944
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1015945
    return-void
.end method
