.class public abstract LX/6UZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1102386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1102387
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, LX/6UZ;->a:Landroid/database/DataSetObservable;

    return-void
.end method


# virtual methods
.method public abstract b(I)Ljava/lang/CharSequence;
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1102388
    iget-object v0, p0, LX/6UZ;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 1102389
    return-void
.end method

.method public final b(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1102390
    iget-object v0, p0, LX/6UZ;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 1102391
    return-void
.end method
