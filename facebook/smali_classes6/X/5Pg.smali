.class public LX/5Pg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/nio/FloatBuffer;

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:I

.field public final f:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;III)V
    .locals 2

    .prologue
    .line 911021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 911022
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/64O;->a(Z)V

    .line 911023
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    .line 911024
    div-int/lit8 v0, p2, 0x4

    iput v0, p0, LX/5Pg;->f:I

    .line 911025
    iput p3, p0, LX/5Pg;->e:I

    .line 911026
    iput p4, p0, LX/5Pg;->b:I

    .line 911027
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LX/5Pg;->e:I

    div-int/2addr v0, v1

    iput v0, p0, LX/5Pg;->c:I

    .line 911028
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5Pg;->d:Z

    .line 911029
    return-void
.end method

.method public constructor <init>([FI)V
    .locals 1

    .prologue
    .line 911030
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/5Pg;-><init>([FIZ)V

    .line 911031
    return-void
.end method

.method private constructor <init>([FIZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 911032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 911033
    if-lez p2, :cond_0

    const/4 v0, 0x4

    if-gt p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/64O;->a(Z)V

    .line 911034
    array-length v0, p1

    rem-int/2addr v0, p2

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/64O;->a(Z)V

    .line 911035
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    iput-object v0, p0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    .line 911036
    iput v2, p0, LX/5Pg;->f:I

    .line 911037
    iput v2, p0, LX/5Pg;->e:I

    .line 911038
    iput p2, p0, LX/5Pg;->b:I

    .line 911039
    array-length v0, p1

    iget v1, p0, LX/5Pg;->b:I

    div-int/2addr v0, v1

    iput v0, p0, LX/5Pg;->c:I

    .line 911040
    iput-boolean p3, p0, LX/5Pg;->d:Z

    .line 911041
    return-void

    :cond_0
    move v0, v2

    .line 911042
    goto :goto_0

    :cond_1
    move v1, v2

    .line 911043
    goto :goto_1
.end method
