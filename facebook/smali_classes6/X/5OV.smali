.class public final enum LX/5OV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5OV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5OV;

.field public static final enum ALPHA:LX/5OV;

.field public static final enum NONE:LX/5OV;

.field public static final enum SLIDE_DOWN:LX/5OV;

.field public static final enum SLIDE_LEFT:LX/5OV;

.field public static final enum SLIDE_RIGHT:LX/5OV;

.field public static final enum SLIDE_UP:LX/5OV;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 908894
    new-instance v0, LX/5OV;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->NONE:LX/5OV;

    .line 908895
    new-instance v0, LX/5OV;

    const-string v1, "ALPHA"

    invoke-direct {v0, v1, v4}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->ALPHA:LX/5OV;

    .line 908896
    new-instance v0, LX/5OV;

    const-string v1, "SLIDE_LEFT"

    invoke-direct {v0, v1, v5}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->SLIDE_LEFT:LX/5OV;

    .line 908897
    new-instance v0, LX/5OV;

    const-string v1, "SLIDE_RIGHT"

    invoke-direct {v0, v1, v6}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->SLIDE_RIGHT:LX/5OV;

    .line 908898
    new-instance v0, LX/5OV;

    const-string v1, "SLIDE_UP"

    invoke-direct {v0, v1, v7}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->SLIDE_UP:LX/5OV;

    .line 908899
    new-instance v0, LX/5OV;

    const-string v1, "SLIDE_DOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5OV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OV;->SLIDE_DOWN:LX/5OV;

    .line 908900
    const/4 v0, 0x6

    new-array v0, v0, [LX/5OV;

    sget-object v1, LX/5OV;->NONE:LX/5OV;

    aput-object v1, v0, v3

    sget-object v1, LX/5OV;->ALPHA:LX/5OV;

    aput-object v1, v0, v4

    sget-object v1, LX/5OV;->SLIDE_LEFT:LX/5OV;

    aput-object v1, v0, v5

    sget-object v1, LX/5OV;->SLIDE_RIGHT:LX/5OV;

    aput-object v1, v0, v6

    sget-object v1, LX/5OV;->SLIDE_UP:LX/5OV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5OV;->SLIDE_DOWN:LX/5OV;

    aput-object v2, v0, v1

    sput-object v0, LX/5OV;->$VALUES:[LX/5OV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 908901
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5OV;
    .locals 1

    .prologue
    .line 908902
    const-class v0, LX/5OV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5OV;

    return-object v0
.end method

.method public static values()[LX/5OV;
    .locals 1

    .prologue
    .line 908903
    sget-object v0, LX/5OV;->$VALUES:[LX/5OV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5OV;

    return-object v0
.end method
