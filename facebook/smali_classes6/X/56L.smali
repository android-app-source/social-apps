.class public final LX/56L;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 837614
    const/16 v25, 0x0

    .line 837615
    const/16 v24, 0x0

    .line 837616
    const/16 v23, 0x0

    .line 837617
    const/16 v22, 0x0

    .line 837618
    const/16 v19, 0x0

    .line 837619
    const-wide/16 v20, 0x0

    .line 837620
    const/16 v18, 0x0

    .line 837621
    const/16 v17, 0x0

    .line 837622
    const/16 v16, 0x0

    .line 837623
    const/4 v13, 0x0

    .line 837624
    const-wide/16 v14, 0x0

    .line 837625
    const/4 v12, 0x0

    .line 837626
    const/4 v11, 0x0

    .line 837627
    const/4 v10, 0x0

    .line 837628
    const/4 v9, 0x0

    .line 837629
    const/4 v8, 0x0

    .line 837630
    const/4 v7, 0x0

    .line 837631
    const/4 v6, 0x0

    .line 837632
    const/4 v5, 0x0

    .line 837633
    const/4 v4, 0x0

    .line 837634
    const/4 v3, 0x0

    .line 837635
    const/4 v2, 0x0

    .line 837636
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_18

    .line 837637
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 837638
    const/4 v2, 0x0

    .line 837639
    :goto_0
    return v2

    .line 837640
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_11

    .line 837641
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 837642
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 837643
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 837644
    const-string v27, "album_type"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 837645
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 837646
    :cond_1
    const-string v27, "allow_contributors"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 837647
    const/4 v2, 0x1

    .line 837648
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v25, v10

    move v10, v2

    goto :goto_1

    .line 837649
    :cond_2
    const-string v27, "can_edit_caption"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 837650
    const/4 v2, 0x1

    .line 837651
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v24, v9

    move v9, v2

    goto :goto_1

    .line 837652
    :cond_3
    const-string v27, "can_upload"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 837653
    const/4 v2, 0x1

    .line 837654
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v23, v7

    move v7, v2

    goto :goto_1

    .line 837655
    :cond_4
    const-string v27, "can_viewer_delete"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 837656
    const/4 v2, 0x1

    .line 837657
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v22, v6

    move v6, v2

    goto/16 :goto_1

    .line 837658
    :cond_5
    const-string v27, "created_time"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 837659
    const/4 v2, 0x1

    .line 837660
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 837661
    :cond_6
    const-string v27, "explicit_place"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 837662
    invoke-static/range {p0 .. p1}, LX/63w;->a$redex0(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 837663
    :cond_7
    const-string v27, "id"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 837664
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 837665
    :cond_8
    const-string v27, "media_owner_object"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 837666
    invoke-static/range {p0 .. p1}, LX/56F;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 837667
    :cond_9
    const-string v27, "message"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 837668
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 837669
    :cond_a
    const-string v27, "modified_time"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 837670
    const/4 v2, 0x1

    .line 837671
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 837672
    :cond_b
    const-string v27, "owner"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 837673
    invoke-static/range {p0 .. p1}, LX/56G;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 837674
    :cond_c
    const-string v27, "photo_items"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 837675
    invoke-static/range {p0 .. p1}, LX/56H;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 837676
    :cond_d
    const-string v27, "privacy_scope"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 837677
    invoke-static/range {p0 .. p1}, LX/56K;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 837678
    :cond_e
    const-string v27, "title"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 837679
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 837680
    :cond_f
    const-string v27, "url"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 837681
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 837682
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 837683
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 837684
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 837685
    if-eqz v10, :cond_12

    .line 837686
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 837687
    :cond_12
    if-eqz v9, :cond_13

    .line 837688
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 837689
    :cond_13
    if-eqz v7, :cond_14

    .line 837690
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 837691
    :cond_14
    if-eqz v6, :cond_15

    .line 837692
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 837693
    :cond_15
    if-eqz v3, :cond_16

    .line 837694
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 837695
    :cond_16
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 837696
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 837697
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 837698
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 837699
    if-eqz v8, :cond_17

    .line 837700
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 837701
    :cond_17
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 837702
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 837703
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 837704
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 837705
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 837706
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_18
    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v29, v9

    move v9, v6

    move v6, v4

    move/from16 v30, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v31, v14

    move/from16 v14, v30

    move v15, v12

    move/from16 v12, v29

    move/from16 v33, v17

    move-wide/from16 v16, v31

    move/from16 v34, v18

    move/from16 v18, v13

    move v13, v10

    move v10, v7

    move v7, v5

    move-wide/from16 v4, v20

    move/from16 v20, v33

    move/from16 v21, v34

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 837707
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 837708
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 837709
    if-eqz v0, :cond_0

    .line 837710
    const-string v0, "album_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837711
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837712
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 837713
    if-eqz v0, :cond_1

    .line 837714
    const-string v1, "allow_contributors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837715
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 837716
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 837717
    if-eqz v0, :cond_2

    .line 837718
    const-string v1, "can_edit_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837719
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 837720
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 837721
    if-eqz v0, :cond_3

    .line 837722
    const-string v1, "can_upload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837723
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 837724
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 837725
    if-eqz v0, :cond_4

    .line 837726
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837727
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 837728
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 837729
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 837730
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837731
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 837732
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837733
    if-eqz v0, :cond_6

    .line 837734
    const-string v1, "explicit_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837735
    invoke-static {p0, v0, p2}, LX/63w;->a(LX/15i;ILX/0nX;)V

    .line 837736
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837737
    if-eqz v0, :cond_7

    .line 837738
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837739
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837740
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837741
    if-eqz v0, :cond_8

    .line 837742
    const-string v1, "media_owner_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837743
    invoke-static {p0, v0, p2}, LX/56F;->a(LX/15i;ILX/0nX;)V

    .line 837744
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837745
    if-eqz v0, :cond_9

    .line 837746
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837747
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 837748
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 837749
    cmp-long v2, v0, v4

    if-eqz v2, :cond_a

    .line 837750
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837751
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 837752
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837753
    if-eqz v0, :cond_b

    .line 837754
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837755
    invoke-static {p0, v0, p2}, LX/56G;->a(LX/15i;ILX/0nX;)V

    .line 837756
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837757
    if-eqz v0, :cond_c

    .line 837758
    const-string v1, "photo_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837759
    invoke-static {p0, v0, p2}, LX/56H;->a(LX/15i;ILX/0nX;)V

    .line 837760
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837761
    if-eqz v0, :cond_d

    .line 837762
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837763
    invoke-static {p0, v0, p2, p3}, LX/56K;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 837764
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837765
    if-eqz v0, :cond_e

    .line 837766
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837767
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 837768
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837769
    if-eqz v0, :cond_f

    .line 837770
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837771
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837772
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 837773
    return-void
.end method
