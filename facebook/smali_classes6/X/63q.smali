.class public final LX/63q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/63p;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/63p;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1043651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043652
    iput-object p1, p0, LX/63q;->a:LX/0QB;

    .line 1043653
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1043702
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/63q;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1043655
    packed-switch p2, :pswitch_data_0

    .line 1043656
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1043657
    :pswitch_0
    new-instance v1, LX/G6y;

    invoke-static {p1}, LX/11w;->a(LX/0QB;)LX/11w;

    move-result-object v0

    check-cast v0, LX/11w;

    const/16 p0, 0x14d1

    invoke-static {p1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/G6y;-><init>(LX/11w;LX/0Or;)V

    .line 1043658
    move-object v0, v1

    .line 1043659
    :goto_0
    return-object v0

    .line 1043660
    :pswitch_1
    new-instance v0, LX/G6z;

    invoke-direct {v0}, LX/G6z;-><init>()V

    .line 1043661
    move-object v0, v0

    .line 1043662
    move-object v0, v0

    .line 1043663
    goto :goto_0

    .line 1043664
    :pswitch_2
    new-instance v0, LX/G70;

    invoke-direct {v0}, LX/G70;-><init>()V

    .line 1043665
    move-object v0, v0

    .line 1043666
    move-object v0, v0

    .line 1043667
    goto :goto_0

    .line 1043668
    :pswitch_3
    new-instance p0, LX/G71;

    invoke-static {p1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-static {p1}, LX/1hs;->a(LX/0QB;)LX/1hs;

    move-result-object v1

    check-cast v1, LX/1hs;

    invoke-direct {p0, v0, v1}, LX/G71;-><init>(LX/01T;LX/1hs;)V

    .line 1043669
    move-object v0, p0

    .line 1043670
    goto :goto_0

    .line 1043671
    :pswitch_4
    new-instance p0, LX/G72;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {p0, v0, v1}, LX/G72;-><init>(Landroid/content/Context;LX/03V;)V

    .line 1043672
    move-object v0, p0

    .line 1043673
    goto :goto_0

    .line 1043674
    :pswitch_5
    new-instance v0, LX/G73;

    invoke-direct {v0}, LX/G73;-><init>()V

    .line 1043675
    move-object v0, v0

    .line 1043676
    move-object v0, v0

    .line 1043677
    goto :goto_0

    .line 1043678
    :pswitch_6
    new-instance v0, LX/G74;

    invoke-direct {v0}, LX/G74;-><init>()V

    .line 1043679
    move-object v0, v0

    .line 1043680
    move-object v0, v0

    .line 1043681
    goto :goto_0

    .line 1043682
    :pswitch_7
    new-instance v0, LX/G75;

    invoke-direct {v0}, LX/G75;-><init>()V

    .line 1043683
    move-object v0, v0

    .line 1043684
    move-object v0, v0

    .line 1043685
    goto :goto_0

    .line 1043686
    :pswitch_8
    new-instance v0, LX/G76;

    invoke-direct {v0}, LX/G76;-><init>()V

    .line 1043687
    move-object v0, v0

    .line 1043688
    move-object v0, v0

    .line 1043689
    goto :goto_0

    .line 1043690
    :pswitch_9
    new-instance v0, LX/G77;

    invoke-direct {v0}, LX/G77;-><init>()V

    .line 1043691
    move-object v0, v0

    .line 1043692
    move-object v0, v0

    .line 1043693
    goto :goto_0

    .line 1043694
    :pswitch_a
    new-instance v0, LX/G78;

    invoke-direct {v0}, LX/G78;-><init>()V

    .line 1043695
    move-object v0, v0

    .line 1043696
    move-object v0, v0

    .line 1043697
    goto :goto_0

    .line 1043698
    :pswitch_b
    new-instance v0, LX/G79;

    invoke-direct {v0}, LX/G79;-><init>()V

    .line 1043699
    move-object v0, v0

    .line 1043700
    move-object v0, v0

    .line 1043701
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1043654
    const/16 v0, 0xc

    return v0
.end method
