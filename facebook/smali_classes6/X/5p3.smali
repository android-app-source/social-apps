.class public final LX/5p3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/5p5;

.field public b:Ljava/lang/reflect/Method;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5p5;Ljava/lang/reflect/Method;)V
    .locals 2

    .prologue
    .line 1007748
    iput-object p1, p0, LX/5p3;->a:LX/5p5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007749
    iput-object p2, p0, LX/5p3;->b:Ljava/lang/reflect/Method;

    .line 1007750
    iget-object v0, p0, LX/5p3;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1007751
    invoke-direct {p0, p2}, LX/5p3;->a(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5p3;->c:Ljava/lang/String;

    .line 1007752
    return-void
.end method

.method private a(Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1007753
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    .line 1007754
    new-instance v4, Ljava/lang/StringBuilder;

    array-length v0, v3

    add-int/lit8 v0, v0, 0x2

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1007755
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/5p5;->d(Ljava/lang/Class;)C

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1007756
    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1007757
    :goto_0
    array-length v2, v3

    if-ge v0, v2, :cond_3

    .line 1007758
    aget-object v5, v3, v0

    .line 1007759
    const-class v2, Lcom/facebook/react/bridge/ExecutorToken;

    if-ne v5, v2, :cond_0

    .line 1007760
    iget-object v2, p0, LX/5p3;->a:LX/5p5;

    invoke-virtual {v2}, LX/5p5;->bP_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1007761
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5p3;->a:LX/5p5;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t support web workers, but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5p3;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " takes an ExecutorToken."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007762
    :cond_0
    const-class v2, LX/5pW;

    if-ne v5, v2, :cond_1

    .line 1007763
    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v6, "Promise must be used as last parameter only"

    invoke-static {v2, v6}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1007764
    :cond_1
    invoke-static {v5}, LX/5p5;->c(Ljava/lang/Class;)C

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1007765
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1007766
    goto :goto_1

    .line 1007767
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
