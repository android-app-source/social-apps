.class public LX/5ok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# instance fields
.field public a:I

.field private final b:LX/0ag;

.field private final c:LX/0ej;

.field private final d:LX/26w;

.field private final e:I


# direct methods
.method public constructor <init>(LX/0ag;LX/0ej;LX/26w;I)V
    .locals 1

    .prologue
    .line 1007517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007518
    const/4 v0, -0x1

    iput v0, p0, LX/5ok;->a:I

    .line 1007519
    iput-object p1, p0, LX/5ok;->b:LX/0ag;

    .line 1007520
    iput-object p2, p0, LX/5ok;->c:LX/0ej;

    .line 1007521
    iput-object p3, p0, LX/5ok;->d:LX/26w;

    .line 1007522
    iput p4, p0, LX/5ok;->e:I

    .line 1007523
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1007500
    iget-object v0, p0, LX/5ok;->b:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/5ok;->a:I

    .line 1007501
    iget v0, p0, LX/5ok;->e:I

    iget v1, p0, LX/5ok;->a:I

    if-ne v0, v1, :cond_0

    .line 1007502
    add-int/lit8 v0, p2, 0x0

    .line 1007503
    add-int/lit8 v1, p2, 0x1

    .line 1007504
    add-int/lit8 v2, p2, 0x2

    .line 1007505
    add-int/lit8 v3, p2, 0x3

    .line 1007506
    iget-object v4, p0, LX/5ok;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-virtual {v4, v5, v0, v6}, LX/26w;->a(LX/0oc;IZ)V

    .line 1007507
    iget-object v0, p0, LX/5ok;->d:LX/26w;

    sget-object v4, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-virtual {v0, v4, v1, v6}, LX/26w;->a(LX/0oc;IZ)V

    .line 1007508
    iget-object v0, p0, LX/5ok;->d:LX/26w;

    invoke-virtual {v0, v2}, LX/26w;->a(I)V

    .line 1007509
    iget-object v0, p0, LX/5ok;->d:LX/26w;

    invoke-virtual {v0, v3}, LX/26w;->a(I)V

    .line 1007510
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 6

    .prologue
    .line 1007511
    iget-object v3, p0, LX/5ok;->c:LX/0ej;

    iget-object v4, p0, LX/5ok;->d:LX/26w;

    sget-object v5, LX/0oc;->ASSIGNED:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007512
    iget v0, p0, LX/5ok;->e:I

    iget v1, p0, LX/5ok;->a:I

    if-eq v0, v1, :cond_1

    .line 1007513
    iget-object v3, p0, LX/5ok;->c:LX/0ej;

    iget-object v4, p0, LX/5ok;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007514
    :cond_0
    :goto_0
    return-void

    .line 1007515
    :cond_1
    if-nez p4, :cond_0

    .line 1007516
    iget-object v0, p0, LX/5ok;->d:LX/26w;

    invoke-virtual {v0, p2}, LX/26w;->a(I)V

    goto :goto_0
.end method
