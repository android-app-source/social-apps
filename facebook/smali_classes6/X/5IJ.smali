.class public final LX/5IJ;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 894454
    const-class v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    const v0, -0x4bb1a863

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "DeletePlaceRecommendationFromCommentMutation"

    const-string v6, "a95be2ef060391aee9dfcb12a1d3ceda"

    const-string v7, "comment_place_recommendation_delete"

    const-string v8, "0"

    const-string v9, "10155256824121729"

    const/4 v10, 0x0

    .line 894455
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 894456
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 894457
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 894458
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 894459
    sparse-switch v0, :sswitch_data_0

    .line 894460
    :goto_0
    return-object p1

    .line 894461
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 894462
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_1
        0x5fb57ca -> :sswitch_0
    .end sparse-switch
.end method
