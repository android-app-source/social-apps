.class public LX/5qk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/view/View$AccessibilityDelegate;

.field private static final b:Landroid/view/View$AccessibilityDelegate;

.field private static final c:Landroid/view/View$AccessibilityDelegate;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1009534
    new-instance v0, LX/5qh;

    invoke-direct {v0}, LX/5qh;-><init>()V

    sput-object v0, LX/5qk;->a:Landroid/view/View$AccessibilityDelegate;

    .line 1009535
    new-instance v0, LX/5qi;

    invoke-direct {v0}, LX/5qi;-><init>()V

    sput-object v0, LX/5qk;->b:Landroid/view/View$AccessibilityDelegate;

    .line 1009536
    new-instance v0, LX/5qj;

    invoke-direct {v0}, LX/5qj;-><init>()V

    sput-object v0, LX/5qk;->c:Landroid/view/View$AccessibilityDelegate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1009537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1009538
    if-nez p1, :cond_0

    .line 1009539
    invoke-virtual {p0, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 1009540
    :goto_0
    return-void

    .line 1009541
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1009542
    invoke-virtual {p0, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 1009543
    :sswitch_0
    const-string v1, "button"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "radiobutton_checked"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "radiobutton_unchecked"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    .line 1009544
    :pswitch_0
    sget-object v0, LX/5qk;->a:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 1009545
    :pswitch_1
    sget-object v0, LX/5qk;->b:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 1009546
    :pswitch_2
    sget-object v0, LX/5qk;->c:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x521dd8ce -> :sswitch_0
        -0x4eb523e4 -> :sswitch_2
        -0x2a90b3ab -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
