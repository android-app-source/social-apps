.class public final LX/5Jn;
.super LX/0gG;
.source ""

# interfaces
.implements LX/3mh;


# instance fields
.field private final a:LX/5Jq;

.field private final b:Landroid/content/Context;

.field private final c:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "Lcom/facebook/components/ComponentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5Jq;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 897725
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 897726
    new-instance v0, LX/0Zj;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    iput-object v0, p0, LX/5Jn;->c:LX/0Zj;

    .line 897727
    iput-object p1, p0, LX/5Jn;->a:LX/5Jq;

    .line 897728
    iput-object p2, p0, LX/5Jn;->b:Landroid/content/Context;

    .line 897729
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 897690
    const/4 v0, 0x0

    move-object v0, v0

    .line 897691
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 897719
    check-cast p1, Lcom/facebook/components/ComponentView;

    .line 897720
    iget-object v0, p1, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    move-object v0, v0

    .line 897721
    iget-object v1, p0, LX/5Jn;->a:LX/5Jq;

    invoke-virtual {v1, v0}, LX/3mY;->a(LX/1dV;)I

    move-result v0

    .line 897722
    if-gez v0, :cond_0

    .line 897723
    const/4 v0, -0x2

    .line 897724
    :cond_0
    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 897708
    iget-object v0, p0, LX/5Jn;->c:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 897709
    if-nez v0, :cond_0

    .line 897710
    new-instance v0, Lcom/facebook/components/ComponentView;

    iget-object v1, p0, LX/5Jn;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 897711
    :cond_0
    iget-object v1, p0, LX/5Jn;->a:LX/5Jq;

    invoke-virtual {v1, p2}, LX/3mY;->d(I)LX/1dV;

    move-result-object v1

    .line 897712
    if-nez v1, :cond_1

    iget-object v2, p0, LX/5Jn;->a:LX/5Jq;

    iget-object v2, v2, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/5Jn;->a:LX/5Jq;

    .line 897713
    iget v3, v2, LX/5Jq;->a:I

    move v2, v3

    .line 897714
    iget-object v3, p0, LX/5Jn;->a:LX/5Jq;

    iget-object v3, v3, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 897715
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null component while initializing a new page."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 897716
    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897717
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 897718
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 897703
    move-object v0, p3

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 897704
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897705
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 897706
    iget-object v1, p0, LX/5Jn;->c:LX/0Zj;

    invoke-virtual {v1, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 897707
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 897702
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(II)V
    .locals 0

    .prologue
    .line 897730
    invoke-virtual {p0}, LX/5Jn;->bG_()V

    .line 897731
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 897701
    iget-object v0, p0, LX/5Jn;->a:LX/5Jq;

    invoke-virtual {v0}, LX/3mY;->e()I

    move-result v0

    return v0
.end method

.method public final bG_()V
    .locals 0

    .prologue
    .line 897699
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 897700
    return-void
.end method

.method public final c_(I)V
    .locals 0

    .prologue
    .line 897697
    invoke-virtual {p0}, LX/5Jn;->bG_()V

    .line 897698
    return-void
.end method

.method public final d(I)F
    .locals 1

    .prologue
    .line 897696
    iget-object v0, p0, LX/5Jn;->a:LX/5Jq;

    iget v0, v0, LX/5Jq;->d:F

    return v0
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 897694
    invoke-virtual {p0}, LX/5Jn;->bG_()V

    .line 897695
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 897692
    invoke-virtual {p0}, LX/5Jn;->bG_()V

    .line 897693
    return-void
.end method
