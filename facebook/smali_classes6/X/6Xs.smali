.class public final enum LX/6Xs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Xs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Xs;

.field public static final enum ACTION:LX/6Xs;

.field public static final enum DISMISSAL:LX/6Xs;

.field public static final enum IMPRESSION:LX/6Xs;

.field public static final enum RESET_VIEW_STATE:LX/6Xs;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1108937
    new-instance v0, LX/6Xs;

    const-string v1, "IMPRESSION"

    invoke-direct {v0, v1, v2}, LX/6Xs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xs;->IMPRESSION:LX/6Xs;

    .line 1108938
    new-instance v0, LX/6Xs;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v3}, LX/6Xs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xs;->ACTION:LX/6Xs;

    .line 1108939
    new-instance v0, LX/6Xs;

    const-string v1, "DISMISSAL"

    invoke-direct {v0, v1, v4}, LX/6Xs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xs;->DISMISSAL:LX/6Xs;

    .line 1108940
    new-instance v0, LX/6Xs;

    const-string v1, "RESET_VIEW_STATE"

    invoke-direct {v0, v1, v5}, LX/6Xs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xs;->RESET_VIEW_STATE:LX/6Xs;

    .line 1108941
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Xs;

    sget-object v1, LX/6Xs;->IMPRESSION:LX/6Xs;

    aput-object v1, v0, v2

    sget-object v1, LX/6Xs;->ACTION:LX/6Xs;

    aput-object v1, v0, v3

    sget-object v1, LX/6Xs;->DISMISSAL:LX/6Xs;

    aput-object v1, v0, v4

    sget-object v1, LX/6Xs;->RESET_VIEW_STATE:LX/6Xs;

    aput-object v1, v0, v5

    sput-object v0, LX/6Xs;->$VALUES:[LX/6Xs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1108936
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Xs;
    .locals 1

    .prologue
    .line 1108934
    const-class v0, LX/6Xs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Xs;

    return-object v0
.end method

.method public static values()[LX/6Xs;
    .locals 1

    .prologue
    .line 1108935
    sget-object v0, LX/6Xs;->$VALUES:[LX/6Xs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Xs;

    return-object v0
.end method
