.class public final LX/6CC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/6CD;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/6CI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:D


# direct methods
.method public constructor <init>(LX/6CD;)V
    .locals 0

    .prologue
    .line 1063590
    iput-object p1, p0, LX/6CC;->a:LX/6CD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063591
    return-void
.end method

.method private static b(LX/6CC;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1063586
    iget-object v0, p0, LX/6CC;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1063587
    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1063588
    invoke-static {v0}, LX/1H1;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1063589
    :cond_0
    return-object v0
.end method

.method private static c(LX/6CC;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1063576
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1063577
    const-string v1, "JS_BRIDGE_SESSION_ID"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063578
    const-string v1, "JS_BRIDGE_EXTENSION_TYPE"

    sget-object v2, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    iget-object v2, v2, LX/6Cx;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063579
    const-string v1, "JS_BRIDGE_PAGE_ID"

    iget-object v2, p0, LX/6CC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063580
    const-string v1, "JS_BRIDGE_LOG_SOURCE"

    iget-object v2, p0, LX/6CC;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063581
    const-string v1, "JS_BRIDGE_TRIGGERING_SURFACE"

    iget-object v2, p0, LX/6CC;->k:LX/6CI;

    iget-object v2, v2, LX/6CI;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063582
    iget-object v1, p0, LX/6CC;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1063583
    const-string v1, "JS_BRIDGE_AD_ID"

    iget-object v2, p0, LX/6CC;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063584
    const-string v1, "JS_BRIDGE_WHITELISTED_DOMAINS"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/6CC;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1063585
    :cond_0
    return-object v0
.end method

.method private static d(LX/6CC;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 1063529
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1063530
    const-string v1, "JS_BRIDGE_PAGE_ID"

    iget-object v2, p0, LX/6CC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063531
    const-string v1, "JS_BRIDGE_PAGE_NAME"

    iget-object v2, p0, LX/6CC;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063532
    const-string v1, "JS_BRIDGE_PAGE_POLICY_URL"

    iget-object v2, p0, LX/6CC;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063533
    const-string v1, "JS_BRIDGE_AD_ID"

    iget-object v2, p0, LX/6CC;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063534
    const-string v1, "JS_BRIDGE_ASID"

    iget-object v2, p0, LX/6CC;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063535
    const-string v1, "JS_BRIDGE_PSID"

    iget-object v2, p0, LX/6CC;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063536
    const-string v1, "JS_BRIDGE_WHITELISTED_DOMAINS"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/6CC;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1063537
    const-string v1, "JS_BRIDGE_EXTENSION_TYPE"

    sget-object v2, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    iget-object v2, v2, LX/6Cx;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063538
    const-string v1, "JS_BRIDGE_LOG_SOURCE"

    iget-object v2, p0, LX/6CC;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063539
    iget-object v1, p0, LX/6CC;->k:LX/6CI;

    if-eqz v1, :cond_0

    .line 1063540
    const-string v1, "JS_BRIDGE_TRIGGERING_SURFACE"

    iget-object v2, p0, LX/6CC;->k:LX/6CI;

    iget-object v2, v2, LX/6CI;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063541
    :cond_0
    iget-wide v2, p0, LX/6CC;->l:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_1

    .line 1063542
    const-string v1, "JS_BRIDGE_BROWSER_DISPLAY_HEIGHT_RATIO"

    iget-wide v2, p0, LX/6CC;->l:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1063543
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1063554
    iget-object v0, p0, LX/6CC;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063555
    new-instance v0, LX/0DW;

    invoke-direct {v0}, LX/0DW;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, LX/0DW;->a(Ljava/util/Locale;)LX/0DW;

    move-result-object v4

    .line 1063556
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bg;

    invoke-virtual {v0, v4}, LX/1Bg;->a(LX/0DW;)V

    .line 1063557
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bg;

    iget-object v1, p0, LX/6CC;->a:LX/6CD;

    iget-object v1, v1, LX/6CD;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/01T;

    iget-object v3, p0, LX/6CC;->a:LX/6CD;

    iget-object v3, v3, LX/6CD;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0WV;

    invoke-static {v1, v3}, LX/1Bg;->a(LX/01T;LX/0WV;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/FB_MEXT_IAB"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0DW;->e(Ljava/lang/String;)LX/0DW;

    .line 1063558
    iget-wide v0, p0, LX/6CC;->l:D

    const-wide/16 v6, 0x0

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_0

    .line 1063559
    iget-wide v0, p0, LX/6CC;->l:D

    invoke-virtual {v4, v0, v1}, LX/0DW;->a(D)LX/0DW;

    .line 1063560
    :cond_0
    invoke-virtual {p0}, LX/6CC;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1063561
    new-instance v0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    invoke-direct {v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;-><init>()V

    .line 1063562
    invoke-static {p0}, LX/6CC;->d(LX/6CC;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Landroid/os/Bundle;)V

    .line 1063563
    invoke-virtual {v4, v0}, LX/0DW;->a(Landroid/os/Parcelable;)LX/0DW;

    .line 1063564
    invoke-static {p0}, LX/6CC;->c(LX/6CC;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    .line 1063565
    :cond_1
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x11c

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1063566
    const v0, 0x7f0208cd

    sget-object v1, LX/6Eo;->MESSNEGER_FAVORITE:LX/6Eo;

    invoke-virtual {v1}, LX/6Eo;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1063567
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1063568
    const-string v6, "KEY_ICON_RES"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1063569
    const-string v6, "action"

    invoke-virtual {v3, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063570
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "BrowserLiteIntent.EXTRA_SUB_ACTION_BUTTON"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1063571
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0}, LX/6CC;->b(LX/6CC;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v11

    .line 1063572
    invoke-virtual {v4}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1063573
    const-string v0, "iab_click_source"

    const-string v1, "browser_extensions"

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1063574
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nC;

    iget-object v1, p0, LX/6CC;->i:Ljava/lang/String;

    const-string v3, "browser_extensions"

    const-string v4, "tracking_codes"

    invoke-virtual {v11, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move v6, v5

    move-object v7, v2

    move-object v8, v2

    move v9, v5

    move-object v10, v2

    invoke-virtual/range {v0 .. v10}, LX/2nC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 1063575
    return-object v11
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1063544
    invoke-static {p0}, LX/6CC;->b(LX/6CC;)Landroid/net/Uri;

    move-result-object v3

    .line 1063545
    if-eqz v3, :cond_0

    iget-object v0, p0, LX/6CC;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 1063546
    :goto_0
    return v0

    .line 1063547
    :cond_1
    iget-object v0, p0, LX/6CC;->k:LX/6CI;

    sget-object v1, LX/6CI;->MESSENGER_PLATFORM_THREAD:LX/6CI;

    if-ne v0, v1, :cond_2

    .line 1063548
    const/16 v0, 0x1aa

    move v1, v0

    .line 1063549
    :goto_1
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1063550
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CH;

    const-string v1, "MessengerExtensionIntent"

    const-string v3, "Could not load extension for click surface %s"

    iget-object v4, p0, LX/6CC;->k:LX/6CI;

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/6CC;->b:Ljava/lang/String;

    iget-object v5, p0, LX/6CC;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, v4, v5}, LX/6CH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1063551
    goto :goto_0

    .line 1063552
    :cond_2
    const/16 v0, 0x11d

    move v1, v0

    goto :goto_1

    .line 1063553
    :cond_3
    iget-object v0, p0, LX/6CC;->a:LX/6CD;

    iget-object v0, v0, LX/6CD;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6D1;

    invoke-static {p0}, LX/6CC;->c(LX/6CC;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/6CC;->h:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, LX/6D1;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    goto :goto_0
.end method
