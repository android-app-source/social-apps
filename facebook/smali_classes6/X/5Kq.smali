.class public final LX/5Kq;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Kq;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Ko;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Kr;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899170
    const/4 v0, 0x0

    sput-object v0, LX/5Kq;->a:LX/5Kq;

    .line 899171
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Kq;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 899172
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 899173
    new-instance v0, LX/5Kr;

    invoke-direct {v0}, LX/5Kr;-><init>()V

    iput-object v0, p0, LX/5Kq;->c:LX/5Kr;

    .line 899174
    return-void
.end method

.method public static declared-synchronized q()LX/5Kq;
    .locals 2

    .prologue
    .line 899175
    const-class v1, LX/5Kq;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Kq;->a:LX/5Kq;

    if-nez v0, :cond_0

    .line 899176
    new-instance v0, LX/5Kq;

    invoke-direct {v0}, LX/5Kq;-><init>()V

    sput-object v0, LX/5Kq;->a:LX/5Kq;

    .line 899177
    :cond_0
    sget-object v0, LX/5Kq;->a:LX/5Kq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 899178
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 899179
    check-cast p2, LX/5Kp;

    .line 899180
    iget-object v0, p2, LX/5Kp;->a:Lcom/facebook/java2js/JSValue;

    iget-object v1, p2, LX/5Kp;->b:LX/5KI;

    .line 899181
    const-string v2, "component"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v2

    .line 899182
    const/4 v3, 0x0

    .line 899183
    new-instance v4, LX/5Jw;

    invoke-direct {v4}, LX/5Jw;-><init>()V

    .line 899184
    sget-object v5, LX/5Jx;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Jv;

    .line 899185
    if-nez v5, :cond_0

    .line 899186
    new-instance v5, LX/5Jv;

    invoke-direct {v5}, LX/5Jv;-><init>()V

    .line 899187
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/5Jv;->a$redex0(LX/5Jv;LX/1De;IILX/5Jw;)V

    .line 899188
    move-object v4, v5

    .line 899189
    move-object v3, v4

    .line 899190
    move-object v3, v3

    .line 899191
    const-string v4, "ratio"

    invoke-virtual {v0, v4}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-double v4, v4

    .line 899192
    iget-object v6, v3, LX/5Jv;->a:LX/5Jw;

    iput-wide v4, v6, LX/5Jw;->a:D

    .line 899193
    iget-object v6, v3, LX/5Jv;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 899194
    move-object v3, v3

    .line 899195
    iget-object v4, v3, LX/5Jv;->a:LX/5Jw;

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v5

    iput-object v5, v4, LX/5Jw;->b:LX/1X1;

    .line 899196
    iget-object v4, v3, LX/5Jv;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 899197
    move-object v2, v3

    .line 899198
    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 899199
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 899200
    invoke-static {}, LX/1dS;->b()V

    .line 899201
    const/4 v0, 0x0

    return-object v0
.end method
