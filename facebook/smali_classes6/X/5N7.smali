.class public LX/5N7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/Bitmap;

.field public b:Landroid/graphics/Bitmap;

.field public c:Landroid/graphics/Bitmap;

.field public d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field public final f:Landroid/content/Context;

.field public final g:LX/0yc;

.field public final h:LX/1pl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0yc;LX/1pl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 905885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905886
    iput-object p1, p0, LX/5N7;->f:Landroid/content/Context;

    .line 905887
    iput-object p2, p0, LX/5N7;->g:LX/0yc;

    .line 905888
    iput-object p3, p0, LX/5N7;->h:LX/1pl;

    .line 905889
    return-void
.end method

.method public static a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 905890
    iget-object v0, p0, LX/5N7;->f:Landroid/content/Context;

    const/high16 v1, 0x43960000    # 300.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 905891
    invoke-virtual {p1, v0, v0}, Landroid/view/View;->measure(II)V

    .line 905892
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 905893
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 905894
    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 905895
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 905896
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 905897
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 905898
    return-object v0
.end method

.method public static b(LX/5N7;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 905899
    iget-object v0, p0, LX/5N7;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 905900
    iget-object v0, p0, LX/5N7;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 905901
    const v1, 0x7f03041a

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 905902
    const v1, 0x7f0d0c98

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 905903
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 905904
    const v1, 0x7f0d0c99

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 905905
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 905906
    const v1, 0x7f0d0c97

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 905907
    iget-object v2, p0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 905908
    invoke-virtual {v1, v2, v2, v2, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setPadding(IIII)V

    .line 905909
    iget-object v2, p0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 905910
    iget-object v2, p0, LX/5N7;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 905911
    iget-object v2, p0, LX/5N7;->f:Landroid/content/Context;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 905912
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 905913
    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 905914
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 905915
    invoke-static {p0, v0}, LX/5N7;->a(LX/5N7;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LX/5N7;->e:Landroid/graphics/Bitmap;

    .line 905916
    :cond_0
    iget-object v0, p0, LX/5N7;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method
