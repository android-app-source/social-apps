.class public final LX/52y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 826543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826544
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 826545
    instance-of v0, p0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 826546
    check-cast p0, Ljava/lang/RuntimeException;

    throw p0

    .line 826547
    :cond_0
    instance-of v0, p0, Ljava/lang/Error;

    if-eqz v0, :cond_1

    .line 826548
    check-cast p0, Ljava/lang/Error;

    throw p0

    .line 826549
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 826550
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 826551
    const/4 v0, 0x0

    .line 826552
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 826553
    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0x19

    if-lt v0, v3, :cond_0

    .line 826554
    :goto_1
    return-void

    .line 826555
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    .line 826556
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 826557
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    .line 826558
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 826559
    :catch_0
    goto :goto_1
.end method

.method public static b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 826560
    instance-of v0, p0, LX/531;

    if-eqz v0, :cond_0

    .line 826561
    check-cast p0, LX/531;

    throw p0

    .line 826562
    :cond_0
    instance-of v0, p0, LX/530;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 826563
    check-cast v0, LX/530;

    invoke-virtual {v0}, LX/530;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 826564
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    .line 826565
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 826566
    :cond_1
    check-cast p0, LX/530;

    throw p0

    .line 826567
    :cond_2
    instance-of v0, p0, Ljava/lang/StackOverflowError;

    if-eqz v0, :cond_3

    .line 826568
    check-cast p0, Ljava/lang/StackOverflowError;

    throw p0

    .line 826569
    :cond_3
    instance-of v0, p0, Ljava/lang/VirtualMachineError;

    if-eqz v0, :cond_4

    .line 826570
    check-cast p0, Ljava/lang/VirtualMachineError;

    throw p0

    .line 826571
    :cond_4
    instance-of v0, p0, Ljava/lang/ThreadDeath;

    if-eqz v0, :cond_5

    .line 826572
    check-cast p0, Ljava/lang/ThreadDeath;

    throw p0

    .line 826573
    :cond_5
    instance-of v0, p0, Ljava/lang/LinkageError;

    if-eqz v0, :cond_6

    .line 826574
    check-cast p0, Ljava/lang/LinkageError;

    throw p0

    .line 826575
    :cond_6
    return-void
.end method

.method public static c(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 3

    .prologue
    .line 826576
    const/4 v0, 0x0

    .line 826577
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 826578
    add-int/lit8 v1, v0, 0x1

    const/16 v2, 0x19

    if-lt v0, v2, :cond_1

    .line 826579
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Stack too deep to get final cause"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 826580
    :cond_0
    return-object p0

    .line 826581
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    move v0, v1

    goto :goto_0
.end method
