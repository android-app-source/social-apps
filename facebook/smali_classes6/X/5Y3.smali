.class public final LX/5Y3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 940814
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 940815
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 940816
    :goto_0
    return v1

    .line 940817
    :cond_0
    const-string v9, "can_donate"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 940818
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    .line 940819
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 940820
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 940821
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 940822
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 940823
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 940824
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 940825
    :cond_3
    const-string v9, "fundraiser_detailed_progress_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 940826
    invoke-static {p0, p1}, LX/5Y1;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 940827
    :cond_4
    const-string v9, "fundraiser_for_charity_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 940828
    invoke-static {p0, p1}, LX/5Y2;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 940829
    :cond_5
    const-string v9, "mobile_donate_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 940830
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 940831
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 940832
    :cond_7
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 940833
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 940834
    if-eqz v0, :cond_8

    .line 940835
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 940836
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 940837
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 940838
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 940839
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 940840
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 940841
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 940842
    if-eqz v0, :cond_0

    .line 940843
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 940844
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 940845
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 940846
    if-eqz v0, :cond_1

    .line 940847
    const-string v1, "can_donate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 940848
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 940849
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 940850
    if-eqz v0, :cond_2

    .line 940851
    const-string v1, "fundraiser_detailed_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 940852
    invoke-static {p0, v0, p2}, LX/5Y1;->a(LX/15i;ILX/0nX;)V

    .line 940853
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 940854
    if-eqz v0, :cond_3

    .line 940855
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 940856
    invoke-static {p0, v0, p2}, LX/5Y2;->a(LX/15i;ILX/0nX;)V

    .line 940857
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 940858
    if-eqz v0, :cond_4

    .line 940859
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 940860
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 940861
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 940862
    return-void
.end method
