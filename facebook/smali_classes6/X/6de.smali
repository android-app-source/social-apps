.class public final LX/6de;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0su;

.field private static final c:LX/0sv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1116264
    sget-object v0, LX/6dd;->a:LX/0U1;

    sget-object v1, LX/6dd;->b:LX/0U1;

    sget-object v2, LX/6dd;->c:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/6de;->a:LX/0Px;

    .line 1116265
    new-instance v0, LX/0su;

    sget-object v1, LX/6dd;->a:LX/0U1;

    sget-object v2, LX/6dd;->b:LX/0U1;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/6de;->b:LX/0su;

    .line 1116266
    new-instance v0, LX/2Qn;

    sget-object v1, LX/6dd;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "messages"

    sget-object v3, LX/6df;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    const-string v4, "ON DELETE CASCADE"

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    sput-object v0, LX/6de;->c:LX/0sv;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1116267
    const-string v0, "message_reactions"

    sget-object v1, LX/6de;->a:LX/0Px;

    sget-object v2, LX/6de;->b:LX/0su;

    sget-object v3, LX/6de;->c:LX/0sv;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 1116268
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1116269
    return-void
.end method
