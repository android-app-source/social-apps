.class public LX/62T;
.super LX/3wu;
.source ""

# interfaces
.implements LX/1OS;


# instance fields
.field private t:LX/1P4;

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Integer;

.field private w:Ljava/lang/Integer;

.field private x:Ljava/lang/Integer;

.field private y:Ljava/lang/Integer;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1041651
    invoke-direct {p0, p1, p2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 1041652
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/62T;->z:Z

    .line 1041653
    return-void
.end method

.method private K()LX/1P4;
    .locals 1

    .prologue
    .line 1041654
    iget-object v0, p0, LX/62T;->t:LX/1P4;

    if-nez v0, :cond_0

    .line 1041655
    new-instance v0, LX/1P4;

    invoke-direct {v0, p0}, LX/1P4;-><init>(LX/1P1;)V

    iput-object v0, p0, LX/62T;->t:LX/1P4;

    .line 1041656
    :cond_0
    iget-object v0, p0, LX/62T;->t:LX/1P4;

    return-object v0
.end method


# virtual methods
.method public final I()I
    .locals 1

    .prologue
    .line 1041648
    iget-boolean v0, p0, LX/62T;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/62T;->u:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1041649
    :cond_0
    invoke-direct {p0}, LX/62T;->K()LX/1P4;

    move-result-object v0

    invoke-virtual {v0}, LX/1P4;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/62T;->u:Ljava/lang/Integer;

    .line 1041650
    :cond_1
    iget-object v0, p0, LX/62T;->u:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final J()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1041642
    iput-object v0, p0, LX/62T;->y:Ljava/lang/Integer;

    .line 1041643
    iput-object v0, p0, LX/62T;->x:Ljava/lang/Integer;

    .line 1041644
    iput-object v0, p0, LX/62T;->w:Ljava/lang/Integer;

    .line 1041645
    iput-object v0, p0, LX/62T;->v:Ljava/lang/Integer;

    .line 1041646
    iput-object v0, p0, LX/62T;->u:Ljava/lang/Integer;

    .line 1041647
    return-void
.end method

.method public final b(ILX/1Od;LX/1Ok;)I
    .locals 4

    .prologue
    .line 1041657
    :try_start_0
    invoke-super {p0, p1, p2, p3}, LX/3wu;->b(ILX/1Od;LX/1Ok;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 1041658
    :catch_0
    move-exception v0

    .line 1041659
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adapter count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Scroll amount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1041639
    invoke-super {p0, p1}, LX/3wu;->b(I)V

    .line 1041640
    invoke-direct {p0}, LX/62T;->K()LX/1P4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1P4;->a(I)V

    .line 1041641
    return-void
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1041636
    iget-boolean v0, p0, LX/62T;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/62T;->v:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1041637
    :cond_0
    invoke-super {p0}, LX/3wu;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/62T;->v:Ljava/lang/Integer;

    .line 1041638
    :cond_1
    iget-object v0, p0, LX/62T;->v:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 1041633
    iget-boolean v0, p0, LX/62T;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/62T;->w:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1041634
    :cond_0
    invoke-super {p0}, LX/3wu;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/62T;->w:Ljava/lang/Integer;

    .line 1041635
    :cond_1
    iget-object v0, p0, LX/62T;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1041627
    iget-boolean v0, p0, LX/62T;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/62T;->x:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1041628
    :cond_0
    invoke-super {p0}, LX/3wu;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/62T;->x:Ljava/lang/Integer;

    .line 1041629
    :cond_1
    iget-object v0, p0, LX/62T;->x:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 1041630
    iget-boolean v0, p0, LX/62T;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/62T;->y:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1041631
    :cond_0
    invoke-super {p0}, LX/3wu;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/62T;->y:Ljava/lang/Integer;

    .line 1041632
    :cond_1
    iget-object v0, p0, LX/62T;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
