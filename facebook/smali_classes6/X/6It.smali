.class public final LX/6It;
.super Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
.source ""


# instance fields
.field public final synthetic a:LX/6Iu;

.field public final b:LX/6Ik;


# direct methods
.method public constructor <init>(LX/6Iu;LX/6Ik;)V
    .locals 0

    .prologue
    .line 1074678
    iput-object p1, p0, LX/6It;->a:LX/6Iu;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$StateCallback;-><init>()V

    .line 1074679
    iput-object p2, p0, LX/6It;->b:LX/6Ik;

    .line 1074680
    return-void
.end method


# virtual methods
.method public final onConfigureFailed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2

    .prologue
    .line 1074681
    iget-object v0, p0, LX/6It;->a:LX/6Iu;

    const/16 v1, 0x8

    .line 1074682
    invoke-static {v0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074683
    iget-object v0, p0, LX/6It;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$PreviewSessionStateCallback$2;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$PreviewSessionStateCallback$2;-><init>(LX/6It;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1074684
    return-void
.end method

.method public final onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 3

    .prologue
    .line 1074685
    iget-object v0, p0, LX/6It;->a:LX/6Iu;

    .line 1074686
    iput-object p1, v0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    .line 1074687
    :try_start_0
    iget-object v0, p0, LX/6It;->a:LX/6Iu;

    iget-object v1, p0, LX/6It;->b:LX/6Ik;

    invoke-static {v0, v1}, LX/6Iu;->e(LX/6Iu;LX/6Ik;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074688
    :goto_0
    return-void

    .line 1074689
    :catch_0
    move-exception v0

    .line 1074690
    iget-object v1, p0, LX/6It;->a:LX/6Iu;

    const/16 v2, 0x8

    .line 1074691
    invoke-static {v1, v2}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074692
    iget-object v1, p0, LX/6It;->a:LX/6Iu;

    iget-object v1, v1, LX/6Iu;->e:LX/6Iv;

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$PreviewSessionStateCallback$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$PreviewSessionStateCallback$1;-><init>(LX/6It;Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
