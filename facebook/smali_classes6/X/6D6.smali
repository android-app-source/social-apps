.class public final enum LX/6D6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6D6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6D6;

.field public static final enum EMAIL:LX/6D6;

.field public static final enum LOCATION:LX/6D6;

.field public static final enum PUBLIC_PROFILE:LX/6D6;

.field public static final enum USER_MOBILE_PHONE:LX/6D6;


# instance fields
.field private final text:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1064617
    new-instance v0, LX/6D6;

    const-string v1, "LOCATION"

    const-string v2, "location"

    invoke-direct {v0, v1, v3, v2}, LX/6D6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6D6;->LOCATION:LX/6D6;

    .line 1064618
    new-instance v0, LX/6D6;

    const-string v1, "EMAIL"

    const-string v2, "email"

    invoke-direct {v0, v1, v4, v2}, LX/6D6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6D6;->EMAIL:LX/6D6;

    .line 1064619
    new-instance v0, LX/6D6;

    const-string v1, "PUBLIC_PROFILE"

    const-string v2, "public_profile"

    invoke-direct {v0, v1, v5, v2}, LX/6D6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    .line 1064620
    new-instance v0, LX/6D6;

    const-string v1, "USER_MOBILE_PHONE"

    const-string v2, "user_mobile_phone"

    invoke-direct {v0, v1, v6, v2}, LX/6D6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6D6;->USER_MOBILE_PHONE:LX/6D6;

    .line 1064621
    const/4 v0, 0x4

    new-array v0, v0, [LX/6D6;

    sget-object v1, LX/6D6;->LOCATION:LX/6D6;

    aput-object v1, v0, v3

    sget-object v1, LX/6D6;->EMAIL:LX/6D6;

    aput-object v1, v0, v4

    sget-object v1, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    aput-object v1, v0, v5

    sget-object v1, LX/6D6;->USER_MOBILE_PHONE:LX/6D6;

    aput-object v1, v0, v6

    sput-object v0, LX/6D6;->$VALUES:[LX/6D6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064613
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1064614
    iput-object p3, p0, LX/6D6;->text:Ljava/lang/String;

    .line 1064615
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6D6;
    .locals 1

    .prologue
    .line 1064608
    const-class v0, LX/6D6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6D6;

    return-object v0
.end method

.method public static values()[LX/6D6;
    .locals 1

    .prologue
    .line 1064616
    sget-object v0, LX/6D6;->$VALUES:[LX/6D6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6D6;

    return-object v0
.end method


# virtual methods
.method public final loginPermissionDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1064609
    sget-object v0, LX/6D5;->a:[I

    invoke-virtual {p0}, LX/6D6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1064610
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid permission. Please map this permission to a string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064611
    :pswitch_0
    const v0, 0x7f081d5e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1064612
    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f081d5f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final permissionDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1064601
    sget-object v0, LX/6D5;->a:[I

    invoke-virtual {p0}, LX/6D6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1064602
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid permission. Please map this permission to a string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064603
    :pswitch_0
    const v0, 0x7f081d64

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1064604
    :goto_0
    return-object v0

    .line 1064605
    :pswitch_1
    const v0, 0x7f081d65

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1064606
    :pswitch_2
    const v0, 0x7f081d66

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1064607
    :pswitch_3
    const v0, 0x7f081d67

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1064600
    iget-object v0, p0, LX/6D6;->text:Ljava/lang/String;

    return-object v0
.end method
