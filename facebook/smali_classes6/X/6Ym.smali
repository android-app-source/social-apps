.class public final LX/6Ym;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/6Yn;


# direct methods
.method public constructor <init>(LX/6Yn;)V
    .locals 0

    .prologue
    .line 1110508
    iput-object p1, p0, LX/6Ym;->a:LX/6Yn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6c008067

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 1110509
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1110510
    iget-boolean v0, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    move v0, v0

    .line 1110511
    if-eqz v0, :cond_1

    .line 1110512
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Yn;->c:LX/4g9;

    sget-object v1, LX/4g6;->i:LX/4g6;

    invoke-virtual {v0, v1}, LX/4g9;->a(LX/4g5;)V

    .line 1110513
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-boolean v0, v0, LX/6Yn;->h:Z

    if-eqz v0, :cond_0

    .line 1110514
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Yn;->g:LX/6YV;

    iget-object v1, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v1, v1, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110515
    iget-object v3, v1, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v1, v3

    .line 1110516
    invoke-interface {v0, v1}, LX/6YV;->a(LX/0yY;)V

    .line 1110517
    :cond_0
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->j()V

    .line 1110518
    const v0, -0x9bf3064

    invoke-static {v2, v2, v0, v10}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1110519
    :goto_0
    return-void

    .line 1110520
    :cond_1
    iget-object v0, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1110521
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1110522
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Yn;->c:LX/4g9;

    sget-object v1, LX/4g6;->m:LX/4g6;

    invoke-virtual {v0, v1}, LX/4g9;->a(LX/4g5;)V

    .line 1110523
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    .line 1110524
    iget-object v3, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->n:Ljava/lang/String;

    move-object v3, v3

    .line 1110525
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 1110526
    const v0, 0x291910b

    invoke-static {v0, v10}, LX/02F;->a(II)V

    goto :goto_0

    .line 1110527
    :cond_2
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 1110528
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v11, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    new-instance v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110529
    iget-object v1, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1110530
    const v2, 0x7f080e08

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 1110531
    iget-object v4, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1110532
    iget-object v5, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1110533
    iget-object v6, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1110534
    const v9, 0x7f080e09

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1110535
    iget-object v9, v8, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->o:Ljava/lang/String;

    move-object v8, v9

    .line 1110536
    sget-object v9, LX/6Y4;->INTERSTITIAL:LX/6Y4;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V

    invoke-virtual {v11, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1110537
    iget-object v0, p0, LX/6Ym;->a:LX/6Yn;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->BUY_CONFIRM:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    .line 1110538
    const v0, -0x10c1a54d

    invoke-static {v0, v10}, LX/02F;->a(II)V

    goto :goto_0
.end method
