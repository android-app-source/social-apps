.class public final LX/66O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/65W;

.field public final c:LX/66G;

.field public final d:LX/65S;

.field private final e:I

.field public final f:LX/650;

.field private g:I


# direct methods
.method public constructor <init>(Ljava/util/List;LX/65W;LX/66G;LX/65S;ILX/650;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;",
            "LX/65W;",
            "LX/66G;",
            "Lokhttp3/Connection;",
            "I",
            "LX/650;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1050067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050068
    iput-object p1, p0, LX/66O;->a:Ljava/util/List;

    .line 1050069
    iput-object p4, p0, LX/66O;->d:LX/65S;

    .line 1050070
    iput-object p2, p0, LX/66O;->b:LX/65W;

    .line 1050071
    iput-object p3, p0, LX/66O;->c:LX/66G;

    .line 1050072
    iput p5, p0, LX/66O;->e:I

    .line 1050073
    iput-object p6, p0, LX/66O;->f:LX/650;

    .line 1050074
    return-void
.end method


# virtual methods
.method public final a(LX/650;)LX/655;
    .locals 3

    .prologue
    .line 1050075
    iget-object v0, p0, LX/66O;->b:LX/65W;

    iget-object v1, p0, LX/66O;->c:LX/66G;

    iget-object v2, p0, LX/66O;->d:LX/65S;

    invoke-virtual {p0, p1, v0, v1, v2}, LX/66O;->a(LX/650;LX/65W;LX/66G;LX/65S;)LX/655;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/650;LX/65W;LX/66G;LX/65S;)LX/655;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1050076
    iget v0, p0, LX/66O;->e:I

    iget-object v1, p0, LX/66O;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1050077
    :cond_0
    iget v0, p0, LX/66O;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/66O;->g:I

    .line 1050078
    iget-object v0, p0, LX/66O;->c:LX/66G;

    if-eqz v0, :cond_1

    .line 1050079
    iget-object v0, p1, LX/650;->a:LX/64q;

    move-object v0, v0

    .line 1050080
    iget-object v1, v0, LX/64q;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1050081
    iget-object v2, p0, LX/66O;->d:LX/65S;

    .line 1050082
    iget-object v3, v2, LX/65S;->k:LX/657;

    move-object v2, v3

    .line 1050083
    iget-object v3, v2, LX/657;->a:LX/64R;

    move-object v2, v3

    .line 1050084
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1050085
    iget-object v3, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1050086
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1050087
    iget v1, v0, LX/64q;->f:I

    move v1, v1

    .line 1050088
    iget-object v2, p0, LX/66O;->d:LX/65S;

    .line 1050089
    iget-object v3, v2, LX/65S;->k:LX/657;

    move-object v2, v3

    .line 1050090
    iget-object v3, v2, LX/657;->a:LX/64R;

    move-object v2, v3

    .line 1050091
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1050092
    iget v3, v2, LX/64q;->f:I

    move v2, v3

    .line 1050093
    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1050094
    if-nez v0, :cond_1

    .line 1050095
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "network interceptor "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66O;->a:Ljava/util/List;

    iget v3, p0, LX/66O;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must retain the same host and port"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050096
    :cond_1
    iget-object v0, p0, LX/66O;->c:LX/66G;

    if-eqz v0, :cond_2

    iget v0, p0, LX/66O;->g:I

    if-le v0, v7, :cond_2

    .line 1050097
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "network interceptor "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66O;->a:Ljava/util/List;

    iget v3, p0, LX/66O;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must call proceed() exactly once"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050098
    :cond_2
    new-instance v0, LX/66O;

    iget-object v1, p0, LX/66O;->a:Ljava/util/List;

    iget v2, p0, LX/66O;->e:I

    add-int/lit8 v5, v2, 0x1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LX/66O;-><init>(Ljava/util/List;LX/65W;LX/66G;LX/65S;ILX/650;)V

    .line 1050099
    iget-object v1, p0, LX/66O;->a:Ljava/util/List;

    iget v2, p0, LX/66O;->e:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/64r;

    .line 1050100
    invoke-interface {v1, v0}, LX/64r;->a(LX/66O;)LX/655;

    move-result-object v2

    .line 1050101
    if-eqz p3, :cond_3

    iget v3, p0, LX/66O;->e:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, LX/66O;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget v0, v0, LX/66O;->g:I

    if-eq v0, v7, :cond_3

    .line 1050102
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "network interceptor "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must call proceed() exactly once"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050103
    :cond_3
    if-nez v2, :cond_4

    .line 1050104
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "interceptor "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050105
    :cond_4
    return-object v2

    :cond_5
    const/4 v1, 0x0

    .line 1050106
    goto/16 :goto_0
.end method
