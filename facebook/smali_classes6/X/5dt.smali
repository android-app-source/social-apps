.class public LX/5dt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/5ds;",
            "LX/5dg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966316
    const/4 v0, 0x0

    sput-object v0, LX/5dt;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/03V;LX/0lC;Ljava/lang/String;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0lC;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966295
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966296
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 966297
    :goto_0
    return-object v0

    .line 966298
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 966299
    const/4 v0, 0x0

    .line 966300
    :try_start_0
    invoke-virtual {p1, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 966301
    :goto_1
    if-eqz v0, :cond_3

    .line 966302
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    .line 966303
    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 966304
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 966305
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/5ds;->fromRawValue(Ljava/lang/String;)LX/5ds;

    move-result-object v1

    .line 966306
    sget-object v4, LX/5dt;->a:LX/0P1;

    if-nez v4, :cond_2

    .line 966307
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    sget-object p0, LX/5ds;->IGNORE_FOR_WEBHOOK:LX/5ds;

    sget-object p1, Lcom/facebook/messaging/model/messagemetadata/IgnoreForWebhookPlatformMetadata;->CREATOR:LX/5dg;

    invoke-virtual {v4, p0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    sget-object p0, LX/5ds;->QUICK_REPLIES:LX/5ds;

    sget-object p1, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->CREATOR:LX/5dg;

    invoke-virtual {v4, p0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    sget-object p0, LX/5ds;->AGENT_SUGGESTIONS:LX/5ds;

    sget-object p1, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->CREATOR:LX/5dg;

    invoke-virtual {v4, p0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    sget-object p0, LX/5ds;->BROADCAST_UNIT_ID:LX/5ds;

    sget-object p1, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->CREATOR:LX/5dg;

    invoke-virtual {v4, p0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    sput-object v4, LX/5dt;->a:LX/0P1;

    .line 966308
    :cond_2
    sget-object v4, LX/5dt;->a:LX/0P1;

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5dg;

    move-object v4, v4

    .line 966309
    if-eqz v4, :cond_1

    .line 966310
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-interface {v4, v0}, LX/5dg;->a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    move-result-object v0

    .line 966311
    invoke-virtual {v2, v1, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 966312
    :catch_0
    move-exception v1

    .line 966313
    const-string v3, "PlatformMetadata"

    const-string v4, "Exception thrown when deserializing platform metadata"

    invoke-virtual {p0, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 966314
    :cond_3
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0P1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966254
    sget-object v0, LX/5ds;->AGENT_SUGGESTIONS:LX/5ds;

    invoke-static {v0, p0}, LX/5dt;->a(LX/5ds;LX/0P1;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    .line 966255
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 966256
    :cond_0
    const/4 v0, 0x0

    .line 966257
    :goto_0
    return-object v0

    .line 966258
    :cond_1
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 966259
    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 966260
    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 966261
    :goto_1
    move-object v1, v0

    .line 966262
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 966263
    invoke-virtual {p0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5ds;

    .line 966264
    sget-object v4, LX/5ds;->AGENT_SUGGESTIONS:LX/5ds;

    if-ne v0, v4, :cond_2

    .line 966265
    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 966266
    :cond_2
    invoke-virtual {p0, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 966267
    :cond_3
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 966268
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 966269
    iget-object v4, v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    .line 966270
    if-eqz p3, :cond_5

    const/4 v7, 0x0

    .line 966271
    iget-object v6, v1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    if-nez v6, :cond_7

    move-object v6, v7

    .line 966272
    :goto_4
    move-object v6, v6

    .line 966273
    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 966274
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 966275
    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 966276
    :cond_5
    invoke-static {v1, p1, p2}, LX/5df;->a(Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    move-result-object v1

    .line 966277
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 966278
    :cond_6
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;-><init>(LX/0Px;)V

    goto :goto_1

    .line 966279
    :cond_7
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    iget-object v0, v1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    invoke-direct {v6, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 966280
    const-string v0, "suggestion_id"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 966281
    :catch_0
    move-object v6, v7

    goto :goto_4
.end method

.method public static varargs a([Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ")",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966290
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 966291
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 966292
    invoke-virtual {v3}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;->a()LX/5ds;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 966293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 966294
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0P1;Z)LX/0m9;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;Z)",
            "LX/0m9;"
        }
    .end annotation

    .prologue
    .line 966286
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 966287
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 966288
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5ds;

    iget-object v1, v1, LX/5ds;->value:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;->c()LX/0lF;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v1, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;->b()LX/0lF;

    move-result-object v0

    goto :goto_1

    .line 966289
    :cond_1
    return-object v2
.end method

.method public static a(LX/5ds;LX/0P1;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">(",
            "LX/5ds;",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966283
    invoke-virtual {p1, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966284
    invoke-virtual {p1, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    .line 966285
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0P1;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966282
    new-instance v0, Landroid/util/Pair;

    const-string v1, "platform_xmd"

    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/5dt;->a(LX/0P1;Z)LX/0m9;

    move-result-object v2

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
