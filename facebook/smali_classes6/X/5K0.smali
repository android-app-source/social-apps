.class public final LX/5K0;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5K1;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dQ;

.field public b:LX/1PH;

.field public c:LX/5Je;

.field public d:LX/1Of;

.field public e:LX/5K7;

.field public f:LX/3x6;

.field public g:LX/1OX;

.field public h:Z

.field public i:LX/1Of;

.field public j:Z

.field public k:Z

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 898031
    invoke-static {}, LX/5K1;->q()LX/5K1;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 898032
    sget-object v0, LX/5K9;->a:LX/1Of;

    iput-object v0, p0, LX/5K0;->d:LX/1Of;

    .line 898033
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5K0;->j:Z

    .line 898034
    const/4 v0, 0x0

    iput v0, p0, LX/5K0;->l:I

    .line 898035
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897987
    const-string v0, "Recycler"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/5K1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 898027
    check-cast p1, LX/5K0;

    .line 898028
    iget-object v0, p1, LX/5K0;->b:LX/1PH;

    iput-object v0, p0, LX/5K0;->b:LX/1PH;

    .line 898029
    iget-object v0, p1, LX/5K0;->i:LX/1Of;

    iput-object v0, p0, LX/5K0;->i:LX/1Of;

    .line 898030
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897993
    if-ne p0, p1, :cond_1

    .line 897994
    :cond_0
    :goto_0
    return v0

    .line 897995
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 897996
    goto :goto_0

    .line 897997
    :cond_3
    check-cast p1, LX/5K0;

    .line 897998
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 897999
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 898000
    if-eq v2, v3, :cond_0

    .line 898001
    iget-object v2, p0, LX/5K0;->a:LX/1dQ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5K0;->a:LX/1dQ;

    iget-object v3, p1, LX/5K0;->a:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 898002
    goto :goto_0

    .line 898003
    :cond_5
    iget-object v2, p1, LX/5K0;->a:LX/1dQ;

    if-nez v2, :cond_4

    .line 898004
    :cond_6
    iget-object v2, p0, LX/5K0;->c:LX/5Je;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/5K0;->c:LX/5Je;

    iget-object v3, p1, LX/5K0;->c:LX/5Je;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 898005
    goto :goto_0

    .line 898006
    :cond_8
    iget-object v2, p1, LX/5K0;->c:LX/5Je;

    if-nez v2, :cond_7

    .line 898007
    :cond_9
    iget-object v2, p0, LX/5K0;->d:LX/1Of;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/5K0;->d:LX/1Of;

    iget-object v3, p1, LX/5K0;->d:LX/1Of;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 898008
    goto :goto_0

    .line 898009
    :cond_b
    iget-object v2, p1, LX/5K0;->d:LX/1Of;

    if-nez v2, :cond_a

    .line 898010
    :cond_c
    iget-object v2, p0, LX/5K0;->e:LX/5K7;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/5K0;->e:LX/5K7;

    iget-object v3, p1, LX/5K0;->e:LX/5K7;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 898011
    goto :goto_0

    .line 898012
    :cond_e
    iget-object v2, p1, LX/5K0;->e:LX/5K7;

    if-nez v2, :cond_d

    .line 898013
    :cond_f
    iget-object v2, p0, LX/5K0;->f:LX/3x6;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/5K0;->f:LX/3x6;

    iget-object v3, p1, LX/5K0;->f:LX/3x6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 898014
    goto :goto_0

    .line 898015
    :cond_11
    iget-object v2, p1, LX/5K0;->f:LX/3x6;

    if-nez v2, :cond_10

    .line 898016
    :cond_12
    iget-object v2, p0, LX/5K0;->g:LX/1OX;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/5K0;->g:LX/1OX;

    iget-object v3, p1, LX/5K0;->g:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 898017
    goto/16 :goto_0

    .line 898018
    :cond_14
    iget-object v2, p1, LX/5K0;->g:LX/1OX;

    if-nez v2, :cond_13

    .line 898019
    :cond_15
    iget-boolean v2, p0, LX/5K0;->h:Z

    iget-boolean v3, p1, LX/5K0;->h:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 898020
    goto/16 :goto_0

    .line 898021
    :cond_16
    iget-boolean v2, p0, LX/5K0;->j:Z

    iget-boolean v3, p1, LX/5K0;->j:Z

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 898022
    goto/16 :goto_0

    .line 898023
    :cond_17
    iget-boolean v2, p0, LX/5K0;->k:Z

    iget-boolean v3, p1, LX/5K0;->k:Z

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 898024
    goto/16 :goto_0

    .line 898025
    :cond_18
    iget v2, p0, LX/5K0;->l:I

    iget v3, p1, LX/5K0;->l:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 898026
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 897988
    const/4 v1, 0x0

    .line 897989
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/5K0;

    .line 897990
    iput-object v1, v0, LX/5K0;->b:LX/1PH;

    .line 897991
    iput-object v1, v0, LX/5K0;->i:LX/1Of;

    .line 897992
    return-object v0
.end method
