.class public LX/5pl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pk;


# instance fields
.field private final a:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

.field private final b:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

.field private final c:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;


# direct methods
.method private constructor <init>(Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;)V
    .locals 0

    .prologue
    .line 1008483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008484
    iput-object p1, p0, LX/5pl;->a:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    .line 1008485
    iput-object p2, p0, LX/5pl;->b:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    .line 1008486
    iput-object p3, p0, LX/5pl;->c:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    .line 1008487
    return-void
.end method

.method public static a(LX/5pn;LX/5pj;)LX/5pl;
    .locals 4

    .prologue
    .line 1008491
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 1008492
    sget-object v0, LX/5pi;->a:LX/5pi;

    move-object v0, v0

    .line 1008493
    invoke-static {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a(LX/5pi;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    move-result-object v3

    .line 1008494
    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008495
    iget-object v0, p0, LX/5pn;->b:LX/5pi;

    move-object v0, v0

    .line 1008496
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    .line 1008497
    if-nez v0, :cond_1

    .line 1008498
    iget-object v0, p0, LX/5pn;->b:LX/5pi;

    move-object v0, v0

    .line 1008499
    invoke-static {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a(LX/5pi;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    move-result-object v0

    move-object v1, v0

    .line 1008500
    :goto_0
    iget-object v0, p0, LX/5pn;->a:LX/5pi;

    move-object v0, v0

    .line 1008501
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    .line 1008502
    if-nez v0, :cond_0

    .line 1008503
    iget-object v0, p0, LX/5pn;->a:LX/5pi;

    move-object v0, v0

    .line 1008504
    invoke-static {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a(LX/5pi;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    move-result-object v0

    .line 1008505
    :cond_0
    new-instance v2, LX/5pl;

    invoke-direct {v2, v3, v0, v1}, LX/5pl;-><init>(Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;)V

    return-object v2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .locals 1

    .prologue
    .line 1008490
    iget-object v0, p0, LX/5pl;->a:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    return-object v0
.end method

.method public final b()Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .locals 1

    .prologue
    .line 1008489
    iget-object v0, p0, LX/5pl;->b:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    return-object v0
.end method

.method public final c()Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .locals 1

    .prologue
    .line 1008488
    iget-object v0, p0, LX/5pl;->c:Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    return-object v0
.end method
