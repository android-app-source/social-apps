.class public final enum LX/6IE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6IE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6IE;

.field public static final enum HIGH:LX/6IE;

.field public static final enum LOW:LX/6IE;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1073527
    new-instance v0, LX/6IE;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, LX/6IE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IE;->HIGH:LX/6IE;

    .line 1073528
    new-instance v0, LX/6IE;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, LX/6IE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IE;->LOW:LX/6IE;

    .line 1073529
    const/4 v0, 0x2

    new-array v0, v0, [LX/6IE;

    sget-object v1, LX/6IE;->HIGH:LX/6IE;

    aput-object v1, v0, v2

    sget-object v1, LX/6IE;->LOW:LX/6IE;

    aput-object v1, v0, v3

    sput-object v0, LX/6IE;->$VALUES:[LX/6IE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1073530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6IE;
    .locals 1

    .prologue
    .line 1073531
    const-class v0, LX/6IE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6IE;

    return-object v0
.end method

.method public static values()[LX/6IE;
    .locals 1

    .prologue
    .line 1073532
    sget-object v0, LX/6IE;->$VALUES:[LX/6IE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6IE;

    return-object v0
.end method
