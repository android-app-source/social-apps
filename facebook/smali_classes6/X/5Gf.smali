.class public final LX/5Gf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 49

    .prologue
    .line 888631
    const/16 v41, 0x0

    .line 888632
    const/16 v40, 0x0

    .line 888633
    const/16 v39, 0x0

    .line 888634
    const/16 v38, 0x0

    .line 888635
    const/16 v35, 0x0

    .line 888636
    const-wide/16 v36, 0x0

    .line 888637
    const/16 v34, 0x0

    .line 888638
    const/16 v33, 0x0

    .line 888639
    const/16 v32, 0x0

    .line 888640
    const/16 v31, 0x0

    .line 888641
    const/16 v30, 0x0

    .line 888642
    const/16 v29, 0x0

    .line 888643
    const/16 v28, 0x0

    .line 888644
    const/16 v27, 0x0

    .line 888645
    const/16 v26, 0x0

    .line 888646
    const/16 v25, 0x0

    .line 888647
    const/16 v24, 0x0

    .line 888648
    const/16 v23, 0x0

    .line 888649
    const/16 v22, 0x0

    .line 888650
    const/16 v21, 0x0

    .line 888651
    const/16 v20, 0x0

    .line 888652
    const/16 v19, 0x0

    .line 888653
    const/16 v18, 0x0

    .line 888654
    const/16 v17, 0x0

    .line 888655
    const/16 v16, 0x0

    .line 888656
    const/4 v15, 0x0

    .line 888657
    const/4 v14, 0x0

    .line 888658
    const/4 v13, 0x0

    .line 888659
    const/4 v12, 0x0

    .line 888660
    const/4 v11, 0x0

    .line 888661
    const/4 v10, 0x0

    .line 888662
    const/4 v9, 0x0

    .line 888663
    const/4 v8, 0x0

    .line 888664
    const/4 v7, 0x0

    .line 888665
    const/4 v6, 0x0

    .line 888666
    const/4 v5, 0x0

    .line 888667
    const/4 v4, 0x0

    .line 888668
    const/4 v3, 0x0

    .line 888669
    const/4 v2, 0x0

    .line 888670
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_2a

    .line 888671
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 888672
    const/4 v2, 0x0

    .line 888673
    :goto_0
    return v2

    .line 888674
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v43, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v43

    if-eq v2, v0, :cond_1d

    .line 888675
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 888676
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 888677
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 888678
    const-string v43, "__type__"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-nez v43, :cond_1

    const-string v43, "__typename"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_2

    .line 888679
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v42, v2

    goto :goto_1

    .line 888680
    :cond_2
    const-string v43, "atom_size"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_3

    .line 888681
    const/4 v2, 0x1

    .line 888682
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v41, v7

    move v7, v2

    goto :goto_1

    .line 888683
    :cond_3
    const-string v43, "bitrate"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_4

    .line 888684
    const/4 v2, 0x1

    .line 888685
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v40, v6

    move v6, v2

    goto :goto_1

    .line 888686
    :cond_4
    const-string v43, "broadcast_status"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_5

    .line 888687
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v39, v2

    goto :goto_1

    .line 888688
    :cond_5
    const-string v43, "captions_url"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_6

    .line 888689
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 888690
    :cond_6
    const-string v43, "created_time"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_7

    .line 888691
    const/4 v2, 0x1

    .line 888692
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 888693
    :cond_7
    const-string v43, "focus"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_8

    .line 888694
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 888695
    :cond_8
    const-string v43, "hdAtomSize"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_9

    .line 888696
    const/4 v2, 0x1

    .line 888697
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    move/from16 v36, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 888698
    :cond_9
    const-string v43, "hdBitrate"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_a

    .line 888699
    const/4 v2, 0x1

    .line 888700
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    move/from16 v35, v15

    move v15, v2

    goto/16 :goto_1

    .line 888701
    :cond_a
    const-string v43, "height"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_b

    .line 888702
    const/4 v2, 0x1

    .line 888703
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v34, v14

    move v14, v2

    goto/16 :goto_1

    .line 888704
    :cond_b
    const-string v43, "id"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_c

    .line 888705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 888706
    :cond_c
    const-string v43, "image"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_d

    .line 888707
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 888708
    :cond_d
    const-string v43, "imageHigh"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_e

    .line 888709
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 888710
    :cond_e
    const-string v43, "imageLargeAspect"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_f

    .line 888711
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 888712
    :cond_f
    const-string v43, "imageLow"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_10

    .line 888713
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 888714
    :cond_10
    const-string v43, "imageMedium"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_11

    .line 888715
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 888716
    :cond_11
    const-string v43, "image_blurred"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_12

    .line 888717
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 888718
    :cond_12
    const-string v43, "is_age_restricted"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_13

    .line 888719
    const/4 v2, 0x1

    .line 888720
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v26, v13

    move v13, v2

    goto/16 :goto_1

    .line 888721
    :cond_13
    const-string v43, "is_disturbing"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_14

    .line 888722
    const/4 v2, 0x1

    .line 888723
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v25, v12

    move v12, v2

    goto/16 :goto_1

    .line 888724
    :cond_14
    const-string v43, "is_live_streaming"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_15

    .line 888725
    const/4 v2, 0x1

    .line 888726
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v24, v11

    move v11, v2

    goto/16 :goto_1

    .line 888727
    :cond_15
    const-string v43, "is_playable"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_16

    .line 888728
    const/4 v2, 0x1

    .line 888729
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v23, v10

    move v10, v2

    goto/16 :goto_1

    .line 888730
    :cond_16
    const-string v43, "playableUrlHdString"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_17

    .line 888731
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 888732
    :cond_17
    const-string v43, "playable_duration_in_ms"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_18

    .line 888733
    const/4 v2, 0x1

    .line 888734
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v21, v9

    move v9, v2

    goto/16 :goto_1

    .line 888735
    :cond_18
    const-string v43, "playable_url"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_19

    .line 888736
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 888737
    :cond_19
    const-string v43, "playlist"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1a

    .line 888738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 888739
    :cond_1a
    const-string v43, "preferredPlayableUrlString"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_1b

    .line 888740
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 888741
    :cond_1b
    const-string v43, "width"

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 888742
    const/4 v2, 0x1

    .line 888743
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v17, v8

    move v8, v2

    goto/16 :goto_1

    .line 888744
    :cond_1c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 888745
    :cond_1d
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 888746
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888747
    if-eqz v7, :cond_1e

    .line 888748
    const/4 v2, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 888749
    :cond_1e
    if-eqz v6, :cond_1f

    .line 888750
    const/4 v2, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 888751
    :cond_1f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888752
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888753
    if-eqz v3, :cond_20

    .line 888754
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888755
    :cond_20
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888756
    if-eqz v16, :cond_21

    .line 888757
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 888758
    :cond_21
    if-eqz v15, :cond_22

    .line 888759
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 888760
    :cond_22
    if-eqz v14, :cond_23

    .line 888761
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 888762
    :cond_23
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888763
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888764
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888765
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888766
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888767
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888768
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888769
    if-eqz v13, :cond_24

    .line 888770
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888771
    :cond_24
    if-eqz v12, :cond_25

    .line 888772
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888773
    :cond_25
    if-eqz v11, :cond_26

    .line 888774
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888775
    :cond_26
    if-eqz v10, :cond_27

    .line 888776
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888777
    :cond_27
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888778
    if-eqz v9, :cond_28

    .line 888779
    const/16 v2, 0x16

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 888780
    :cond_28
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888781
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888782
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888783
    if-eqz v8, :cond_29

    .line 888784
    const/16 v2, 0x1a

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 888785
    :cond_29
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2a
    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v35

    move/from16 v35, v32

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v23, v20

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v8

    move v8, v2

    move/from16 v45, v16

    move/from16 v16, v10

    move v10, v4

    move/from16 v46, v18

    move/from16 v18, v15

    move v15, v9

    move v9, v3

    move v3, v11

    move v11, v5

    move-wide/from16 v4, v36

    move/from16 v36, v33

    move/from16 v37, v34

    move/from16 v33, v30

    move/from16 v34, v31

    move/from16 v31, v28

    move/from16 v30, v27

    move/from16 v27, v24

    move/from16 v28, v25

    move/from16 v25, v22

    move/from16 v24, v21

    move/from16 v22, v19

    move/from16 v21, v46

    move/from16 v19, v45

    move/from16 v47, v7

    move v7, v13

    move/from16 v13, v47

    move/from16 v48, v12

    move v12, v6

    move/from16 v6, v48

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x0

    .line 888786
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888787
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 888788
    if-eqz v0, :cond_0

    .line 888789
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888790
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 888791
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888792
    if-eqz v0, :cond_1

    .line 888793
    const-string v1, "atom_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888794
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888795
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888796
    if-eqz v0, :cond_2

    .line 888797
    const-string v1, "bitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888798
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888799
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 888800
    if-eqz v0, :cond_3

    .line 888801
    const-string v0, "broadcast_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888802
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888803
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888804
    if-eqz v0, :cond_4

    .line 888805
    const-string v1, "captions_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888806
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888807
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888808
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 888809
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888810
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888811
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888812
    if-eqz v0, :cond_6

    .line 888813
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888814
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 888815
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888816
    if-eqz v0, :cond_7

    .line 888817
    const-string v1, "hdAtomSize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888818
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888819
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888820
    if-eqz v0, :cond_8

    .line 888821
    const-string v1, "hdBitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888822
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888823
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888824
    if-eqz v0, :cond_9

    .line 888825
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888826
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888827
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888828
    if-eqz v0, :cond_a

    .line 888829
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888830
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888831
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888832
    if-eqz v0, :cond_b

    .line 888833
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888834
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888835
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888836
    if-eqz v0, :cond_c

    .line 888837
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888838
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888839
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888840
    if-eqz v0, :cond_d

    .line 888841
    const-string v1, "imageLargeAspect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888842
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888843
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888844
    if-eqz v0, :cond_e

    .line 888845
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888846
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888847
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888848
    if-eqz v0, :cond_f

    .line 888849
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888850
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888851
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888852
    if-eqz v0, :cond_10

    .line 888853
    const-string v1, "image_blurred"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888854
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 888855
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888856
    if-eqz v0, :cond_11

    .line 888857
    const-string v1, "is_age_restricted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888858
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888859
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888860
    if-eqz v0, :cond_12

    .line 888861
    const-string v1, "is_disturbing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888862
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888863
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888864
    if-eqz v0, :cond_13

    .line 888865
    const-string v1, "is_live_streaming"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888866
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888867
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888868
    if-eqz v0, :cond_14

    .line 888869
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888870
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888871
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888872
    if-eqz v0, :cond_15

    .line 888873
    const-string v1, "playableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888874
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888875
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888876
    if-eqz v0, :cond_16

    .line 888877
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888878
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888879
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888880
    if-eqz v0, :cond_17

    .line 888881
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888882
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888883
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888884
    if-eqz v0, :cond_18

    .line 888885
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888886
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888887
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888888
    if-eqz v0, :cond_19

    .line 888889
    const-string v1, "preferredPlayableUrlString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888890
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888891
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 888892
    if-eqz v0, :cond_1a

    .line 888893
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888894
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 888895
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888896
    return-void
.end method
