.class public LX/6Ef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CA;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/location/FbLocationOperationParams;

.field private static volatile f:LX/6Ef;


# instance fields
.field public b:Z

.field public c:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1066412
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x493e0

    .line 1066413
    iput-wide v2, v0, LX/1S7;->b:J

    .line 1066414
    move-object v0, v0

    .line 1066415
    const/high16 v1, 0x43480000    # 200.0f

    .line 1066416
    iput v1, v0, LX/1S7;->c:F

    .line 1066417
    move-object v0, v0

    .line 1066418
    const-wide/16 v2, 0x1388

    .line 1066419
    iput-wide v2, v0, LX/1S7;->d:J

    .line 1066420
    move-object v0, v0

    .line 1066421
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, LX/6Ef;->a:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066410
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ef;->e:J

    .line 1066411
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ef;
    .locals 5

    .prologue
    .line 1066394
    sget-object v0, LX/6Ef;->f:LX/6Ef;

    if-nez v0, :cond_1

    .line 1066395
    const-class v1, LX/6Ef;

    monitor-enter v1

    .line 1066396
    :try_start_0
    sget-object v0, LX/6Ef;->f:LX/6Ef;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1066397
    if-eqz v2, :cond_0

    .line 1066398
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1066399
    new-instance p0, LX/6Ef;

    invoke-direct {p0}, LX/6Ef;-><init>()V

    .line 1066400
    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    .line 1066401
    iput-object v3, p0, LX/6Ef;->c:LX/0W3;

    iput-object v4, p0, LX/6Ef;->d:LX/0SG;

    .line 1066402
    move-object v0, p0

    .line 1066403
    sput-object v0, LX/6Ef;->f:LX/6Ef;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066404
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1066405
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1066406
    :cond_1
    sget-object v0, LX/6Ef;->f:LX/6Ef;

    return-object v0

    .line 1066407
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1066408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 1066390
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066391
    iget-object v0, p0, LX/6Ef;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/6Ef;->e:J

    .line 1066392
    :goto_0
    return-void

    .line 1066393
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ef;->e:J

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1066422
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ef;->e:J

    .line 1066423
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Ef;->b:Z

    .line 1066424
    return-void
.end method

.method public final a()Z
    .locals 6

    .prologue
    .line 1066388
    iget-object v0, p0, LX/6Ef;->c:LX/0W3;

    sget-wide v2, LX/0X5;->fu:J

    invoke-interface {v0, v2, v3}, LX/0W4;->g(J)D

    move-result-wide v0

    double-to-long v0, v0

    .line 1066389
    iget-wide v2, p0, LX/6Ef;->e:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    add-long/2addr v0, v2

    iget-object v2, p0, LX/6Ef;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 1066387
    invoke-static {p1}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1066386
    return-void
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1066383
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Ef;->e:J

    .line 1066384
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Ef;->b:Z

    .line 1066385
    return-void
.end method
