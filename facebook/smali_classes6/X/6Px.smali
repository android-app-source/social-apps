.class public LX/6Px;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/0yc;

.field private final b:LX/0dO;

.field private final c:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0yc;LX/0dO;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yc;",
            "LX/0dO;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086565
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1086566
    iput-object p1, p0, LX/6Px;->a:LX/0yc;

    .line 1086567
    iput-object p2, p0, LX/6Px;->b:LX/0dO;

    .line 1086568
    iput-object p3, p0, LX/6Px;->c:LX/0Or;

    .line 1086569
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1086570
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086571
    iget-object v0, p0, LX/6Px;->a:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Px;->b:LX/0dO;

    invoke-virtual {v0}, LX/0dO;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Px;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    if-ne v0, v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
