.class public final LX/58G;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 846462
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 846463
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 846464
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 846465
    const/4 v2, 0x0

    .line 846466
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 846467
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846468
    :goto_1
    move v1, v2

    .line 846469
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 846470
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 846471
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846472
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 846473
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 846474
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 846475
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 846476
    const-string v4, "concise_text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 846477
    const/4 v3, 0x0

    .line 846478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 846479
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846480
    :goto_3
    move v1, v3

    .line 846481
    goto :goto_2

    .line 846482
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 846483
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 846484
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    .line 846485
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 846486
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 846487
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 846488
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 846489
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 846490
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 846491
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_4

    .line 846492
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 846493
    invoke-virtual {p1, v3, v1}, LX/186;->b(II)V

    .line 846494
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 846495
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 846496
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 846497
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 846498
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 846499
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 846500
    if-eqz v2, :cond_1

    .line 846501
    const-string v3, "concise_text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846502
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 846503
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 846504
    if-eqz v3, :cond_0

    .line 846505
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846506
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 846507
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 846508
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 846509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 846510
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 846511
    return-void
.end method
