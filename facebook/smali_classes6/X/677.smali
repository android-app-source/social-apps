.class public final LX/677;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field private final a:LX/671;

.field private final b:Ljava/util/zip/Inflater;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(LX/65D;Ljava/util/zip/Inflater;)V
    .locals 1

    .prologue
    .line 1052256
    invoke-static {p1}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/677;-><init>(LX/671;Ljava/util/zip/Inflater;)V

    .line 1052257
    return-void
.end method

.method public constructor <init>(LX/671;Ljava/util/zip/Inflater;)V
    .locals 2

    .prologue
    .line 1052258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052259
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052260
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "inflater == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052261
    :cond_1
    iput-object p1, p0, LX/677;->a:LX/671;

    .line 1052262
    iput-object p2, p0, LX/677;->b:Ljava/util/zip/Inflater;

    .line 1052263
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 1052264
    iget v0, p0, LX/677;->c:I

    if-nez v0, :cond_0

    .line 1052265
    :goto_0
    return-void

    .line 1052266
    :cond_0
    iget v0, p0, LX/677;->c:I

    iget-object v1, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->getRemaining()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1052267
    iget v1, p0, LX/677;->c:I

    sub-int/2addr v1, v0

    iput v1, p0, LX/677;->c:I

    .line 1052268
    iget-object v1, p0, LX/677;->a:LX/671;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, LX/671;->f(J)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 1052269
    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052270
    :cond_0
    iget-boolean v2, p0, LX/677;->d:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052271
    :cond_1
    cmp-long v2, p2, v0

    if-nez v2, :cond_2

    .line 1052272
    :goto_0
    return-wide v0

    .line 1052273
    :cond_2
    invoke-virtual {p0}, LX/677;->b()Z

    move-result v0

    .line 1052274
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p1, v1}, LX/672;->e(I)LX/67F;

    move-result-object v1

    .line 1052275
    iget-object v2, p0, LX/677;->b:Ljava/util/zip/Inflater;

    iget-object v3, v1, LX/67F;->a:[B

    iget v4, v1, LX/67F;->c:I

    iget v5, v1, LX/67F;->c:I

    rsub-int v5, v5, 0x2000

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v2

    .line 1052276
    if-lez v2, :cond_3

    .line 1052277
    iget v0, v1, LX/67F;->c:I

    add-int/2addr v0, v2

    iput v0, v1, LX/67F;->c:I

    .line 1052278
    iget-wide v0, p1, LX/672;->b:J

    int-to-long v4, v2

    add-long/2addr v0, v4

    iput-wide v0, p1, LX/672;->b:J

    .line 1052279
    int-to-long v0, v2

    goto :goto_0

    .line 1052280
    :cond_3
    iget-object v2, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->finished()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1052281
    :cond_4
    invoke-direct {p0}, LX/677;->c()V

    .line 1052282
    iget v0, v1, LX/67F;->b:I

    iget v2, v1, LX/67F;->c:I

    if-ne v0, v2, :cond_5

    .line 1052283
    invoke-virtual {v1}, LX/67F;->a()LX/67F;

    move-result-object v0

    iput-object v0, p1, LX/672;->a:LX/67F;

    .line 1052284
    invoke-static {v1}, LX/67G;->a(LX/67F;)V

    .line 1052285
    :cond_5
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1052286
    :cond_6
    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/EOFException;

    const-string v1, "source exhausted prematurely"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052287
    :catch_0
    move-exception v0

    .line 1052288
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052289
    iget-object v0, p0, LX/677;->a:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1052290
    iget-object v1, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1052291
    :goto_0
    return v0

    .line 1052292
    :cond_0
    invoke-direct {p0}, LX/677;->c()V

    .line 1052293
    iget-object v1, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->getRemaining()I

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052294
    :cond_1
    iget-object v1, p0, LX/677;->a:LX/671;

    invoke-interface {v1}, LX/671;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 1052295
    :cond_2
    iget-object v1, p0, LX/677;->a:LX/671;

    invoke-interface {v1}, LX/671;->c()LX/672;

    move-result-object v1

    iget-object v1, v1, LX/672;->a:LX/67F;

    .line 1052296
    iget v2, v1, LX/67F;->c:I

    iget v3, v1, LX/67F;->b:I

    sub-int/2addr v2, v3

    iput v2, p0, LX/677;->c:I

    .line 1052297
    iget-object v2, p0, LX/677;->b:Ljava/util/zip/Inflater;

    iget-object v3, v1, LX/67F;->a:[B

    iget v1, v1, LX/67F;->b:I

    iget v4, p0, LX/677;->c:I

    invoke-virtual {v2, v3, v1, v4}, Ljava/util/zip/Inflater;->setInput([BII)V

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052298
    iget-boolean v0, p0, LX/677;->d:Z

    if-eqz v0, :cond_0

    .line 1052299
    :goto_0
    return-void

    .line 1052300
    :cond_0
    iget-object v0, p0, LX/677;->b:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    .line 1052301
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/677;->d:Z

    .line 1052302
    iget-object v0, p0, LX/677;->a:LX/671;

    invoke-interface {v0}, LX/65D;->close()V

    goto :goto_0
.end method
