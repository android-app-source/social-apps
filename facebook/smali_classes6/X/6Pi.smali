.class public LX/6Pi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPage;",
        "LX/4XZ;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1086317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086318
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pi;->a:Ljava/lang/String;

    .line 1086319
    iput-boolean p2, p0, LX/6Pi;->b:Z

    .line 1086320
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086321
    iget-object v0, p0, LX/6Pi;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086322
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPage;

    check-cast p2, LX/4XZ;

    .line 1086323
    iget-object v0, p0, LX/6Pi;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086324
    :goto_0
    return-void

    .line 1086325
    :cond_0
    iget-boolean v0, p0, LX/6Pi;->b:Z

    .line 1086326
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "does_viewer_like"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086327
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086328
    const-class v0, Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086329
    const-string v0, "TogglePageLikeMutatingVisitor"

    return-object v0
.end method
