.class public LX/64H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/12Q;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private a:Landroid/app/Notification;

.field private b:Ljava/lang/String;

.field private c:Landroid/app/NotificationManager;

.field private d:I

.field private e:Z

.field private f:Landroid/content/Context;

.field private g:I

.field private h:Landroid/graphics/Bitmap;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044414
    const v0, 0x2921131

    iput v0, p0, LX/64H;->d:I

    .line 1044415
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/64H;->e:Z

    .line 1044416
    iput-object p1, p0, LX/64H;->c:Landroid/app/NotificationManager;

    .line 1044417
    iput-object p2, p0, LX/64H;->f:Landroid/content/Context;

    .line 1044418
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1044419
    iget-boolean v0, p0, LX/64H;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/64H;->a:Landroid/app/Notification;

    if-eqz v0, :cond_0

    .line 1044420
    iget-object v0, p0, LX/64H;->c:Landroid/app/NotificationManager;

    const-string v1, "ZeroIndicatorBase"

    iget v2, p0, LX/64H;->d:I

    iget-object v3, p0, LX/64H;->a:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1044421
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/64H;->e:Z

    .line 1044422
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1044423
    iget-boolean v0, p0, LX/64H;->e:Z

    if-eqz v0, :cond_0

    .line 1044424
    iget-object v0, p0, LX/64H;->c:Landroid/app/NotificationManager;

    const-string v1, "ZeroIndicatorBase"

    iget v2, p0, LX/64H;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1044425
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/64H;->e:Z

    .line 1044426
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1044427
    iget-boolean v0, p0, LX/64H;->e:Z

    return v0
.end method

.method public final setIndicatorData(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1044428
    if-nez p1, :cond_1

    .line 1044429
    invoke-virtual {p0}, LX/64H;->b()V

    .line 1044430
    iput-object v3, p0, LX/64H;->a:Landroid/app/Notification;

    .line 1044431
    :cond_0
    :goto_0
    return-void

    .line 1044432
    :cond_1
    iget-object v0, p0, LX/64H;->a:Landroid/app/Notification;

    if-eqz v0, :cond_2

    .line 1044433
    iget-object v0, p0, LX/64H;->c:Landroid/app/NotificationManager;

    const-string v1, "ZeroIndicatorBase"

    iget v2, p0, LX/64H;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1044434
    iput-object v3, p0, LX/64H;->a:Landroid/app/Notification;

    .line 1044435
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64H;->b:Ljava/lang/String;

    .line 1044436
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64H;->i:Ljava/lang/String;

    .line 1044437
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/64H;->f:Landroid/content/Context;

    iget-object v2, p0, LX/64H;->j:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1044438
    const-string v1, "zero_action_url"

    iget-object v2, p0, LX/64H;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1044439
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, LX/64H;->f:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/64H;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    iget v2, p0, LX/64H;->g:I

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 1044440
    iget-object v2, p0, LX/64H;->j:Ljava/lang/Class;

    if-eqz v2, :cond_3

    .line 1044441
    iget-object v2, p0, LX/64H;->f:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1044442
    :cond_3
    iget-object v0, p0, LX/64H;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 1044443
    iget-object v0, p0, LX/64H;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 1044444
    :cond_4
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, LX/64H;->a:Landroid/app/Notification;

    .line 1044445
    iget-boolean v0, p0, LX/64H;->e:Z

    if-eqz v0, :cond_0

    .line 1044446
    iget-object v0, p0, LX/64H;->c:Landroid/app/NotificationManager;

    const-string v1, "ZeroIndicatorBase"

    iget v2, p0, LX/64H;->d:I

    iget-object v3, p0, LX/64H;->a:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public final setListener(LX/11x;)V
    .locals 0

    .prologue
    .line 1044447
    return-void
.end method
