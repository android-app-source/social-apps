.class public LX/68i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/69C;

.field public b:LX/69C;

.field public c:[LX/69C;

.field public d:Landroid/graphics/Paint;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field private final i:Landroid/graphics/Rect;

.field private final j:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1056107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1056108
    iput-object v0, p0, LX/68i;->a:LX/69C;

    .line 1056109
    iput-object v0, p0, LX/68i;->b:LX/69C;

    .line 1056110
    const/4 v0, 0x4

    new-array v0, v0, [LX/69C;

    iput-object v0, p0, LX/68i;->c:[LX/69C;

    .line 1056111
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68i;->d:Landroid/graphics/Paint;

    .line 1056112
    const/4 v0, 0x0

    iput v0, p0, LX/68i;->h:I

    .line 1056113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/68i;->i:Landroid/graphics/Rect;

    .line 1056114
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/68i;->j:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;FF)V
    .locals 18

    .prologue
    .line 1056115
    move-object/from16 v0, p0

    iget-object v4, v0, LX/68i;->a:LX/69C;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/68i;->a:LX/69C;

    invoke-virtual {v4}, LX/69C;->b()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1056116
    :goto_0
    sget-object v5, LX/69C;->a:Landroid/graphics/Bitmap;

    if-ne v4, v5, :cond_2

    .line 1056117
    :cond_0
    :goto_1
    return-void

    .line 1056118
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 1056119
    :cond_2
    const/16 v5, 0xff

    .line 1056120
    if-eqz v4, :cond_6

    const/4 v6, 0x1

    move v8, v6

    .line 1056121
    :goto_2
    if-eqz v8, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68i;->a:LX/69C;

    iget-wide v6, v6, LX/69C;->d:J

    const-wide/16 v10, -0x1

    cmp-long v6, v6, v10

    if-eqz v6, :cond_3

    .line 1056122
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v5, v0, LX/68i;->a:LX/69C;

    iget-wide v10, v5, LX/69C;->d:J

    sub-long/2addr v6, v10

    long-to-int v5, v6

    .line 1056123
    const/16 v6, 0xff

    if-lt v5, v6, :cond_3

    .line 1056124
    const/16 v5, 0xff

    .line 1056125
    move-object/from16 v0, p0

    iget-object v6, v0, LX/68i;->a:LX/69C;

    const-wide/16 v10, -0x1

    iput-wide v10, v6, LX/69C;->d:J

    :cond_3
    move v7, v5

    .line 1056126
    const/16 v5, 0xff

    if-ne v7, v5, :cond_4

    if-nez v8, :cond_c

    .line 1056127
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, LX/68i;->d:Landroid/graphics/Paint;

    const/16 v6, 0xff

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056128
    const/4 v5, 0x0

    .line 1056129
    move-object/from16 v0, p0

    iget-object v6, v0, LX/68i;->c:[LX/69C;

    if-eqz v6, :cond_7

    .line 1056130
    const/4 v6, 0x0

    :goto_3
    const/4 v9, 0x4

    if-ge v6, v9, :cond_7

    .line 1056131
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->c:[LX/69C;

    aget-object v9, v9, v6

    if-eqz v9, :cond_5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->c:[LX/69C;

    aget-object v9, v9, v6

    iget v9, v9, LX/69C;->e:I

    move-object/from16 v0, p0

    iget v10, v0, LX/68i;->g:I

    add-int/lit8 v10, v10, 0x1

    if-ne v9, v10, :cond_5

    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->c:[LX/69C;

    aget-object v9, v9, v6

    invoke-virtual {v9}, LX/69C;->b()Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 1056132
    add-int/lit8 v5, v5, 0x1

    .line 1056133
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1056134
    :cond_6
    const/4 v6, 0x0

    move v8, v6

    goto :goto_2

    .line 1056135
    :cond_7
    move-object/from16 v0, p0

    iget-object v6, v0, LX/68i;->b:LX/69C;

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68i;->b:LX/69C;

    invoke-virtual {v6}, LX/69C;->b()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1056136
    :goto_4
    const/4 v9, 0x4

    if-eq v5, v9, :cond_8

    if-eqz v6, :cond_8

    sget-object v9, LX/69C;->a:Landroid/graphics/Bitmap;

    if-eq v6, v9, :cond_8

    .line 1056137
    move-object/from16 v0, p0

    iget v9, v0, LX/68i;->g:I

    move-object/from16 v0, p0

    iget-object v10, v0, LX/68i;->b:LX/69C;

    iget v10, v10, LX/69C;->e:I

    sub-int/2addr v9, v10

    .line 1056138
    const/4 v10, 0x1

    shl-int/2addr v10, v9

    .line 1056139
    move-object/from16 v0, p0

    iget-object v11, v0, LX/68i;->b:LX/69C;

    iget v11, v11, LX/69C;->b:I

    shr-int v9, v11, v9

    .line 1056140
    move-object/from16 v0, p0

    iget v11, v0, LX/68i;->e:I

    add-int/lit8 v12, v10, -0x1

    and-int/2addr v11, v12

    mul-int/2addr v11, v9

    .line 1056141
    move-object/from16 v0, p0

    iget v12, v0, LX/68i;->f:I

    add-int/lit8 v10, v10, -0x1

    and-int/2addr v10, v12

    mul-int/2addr v10, v9

    .line 1056142
    move-object/from16 v0, p0

    iget-object v12, v0, LX/68i;->i:Landroid/graphics/Rect;

    add-int v13, v11, v9

    add-int/2addr v9, v10

    invoke-virtual {v12, v11, v10, v13, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 1056143
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->j:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/68i;->b:LX/69C;

    iget v10, v10, LX/69C;->c:I

    int-to-float v10, v10

    add-float v10, v10, p2

    move-object/from16 v0, p0

    iget-object v11, v0, LX/68i;->b:LX/69C;

    iget v11, v11, LX/69C;->b:I

    int-to-float v11, v11

    add-float v11, v11, p3

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v9, v0, v1, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1056144
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->i:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/68i;->j:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/68i;->d:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1056145
    :cond_8
    if-lez v5, :cond_c

    .line 1056146
    const/4 v5, 0x0

    move v6, v5

    :goto_5
    const/4 v5, 0x2

    if-ge v6, v5, :cond_c

    .line 1056147
    const/4 v5, 0x0

    :goto_6
    const/4 v9, 0x2

    if-ge v5, v9, :cond_b

    .line 1056148
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->c:[LX/69C;

    shl-int/lit8 v10, v6, 0x1

    add-int/2addr v10, v5

    aget-object v9, v9, v10

    .line 1056149
    if-eqz v9, :cond_9

    iget v10, v9, LX/69C;->e:I

    move-object/from16 v0, p0

    iget v11, v0, LX/68i;->g:I

    add-int/lit8 v11, v11, 0x1

    if-ne v10, v11, :cond_9

    invoke-virtual {v9}, LX/69C;->b()Landroid/graphics/Bitmap;

    move-result-object v10

    if-eqz v10, :cond_9

    sget-object v11, LX/69C;->a:Landroid/graphics/Bitmap;

    if-eq v10, v11, :cond_9

    .line 1056150
    iget v11, v9, LX/69C;->b:I

    shr-int/lit8 v11, v11, 0x1

    .line 1056151
    mul-int v12, v11, v6

    int-to-float v12, v12

    add-float v12, v12, p2

    .line 1056152
    mul-int v13, v11, v5

    int-to-float v13, v13

    add-float v13, v13, p3

    .line 1056153
    move-object/from16 v0, p0

    iget-object v14, v0, LX/68i;->i:Landroid/graphics/Rect;

    const/4 v15, 0x0

    const/16 v16, 0x0

    iget v0, v9, LX/69C;->c:I

    move/from16 v17, v0

    iget v9, v9, LX/69C;->b:I

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v15, v0, v1, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 1056154
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->j:Landroid/graphics/RectF;

    int-to-float v14, v11

    add-float/2addr v14, v12

    int-to-float v11, v11

    add-float/2addr v11, v13

    invoke-virtual {v9, v12, v13, v14, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1056155
    move-object/from16 v0, p0

    iget-object v9, v0, LX/68i;->i:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/68i;->j:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/68i;->d:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1056156
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1056157
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 1056158
    :cond_b
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_5

    .line 1056159
    :cond_c
    if-eqz v8, :cond_0

    .line 1056160
    move-object/from16 v0, p0

    iget-object v5, v0, LX/68i;->d:Landroid/graphics/Paint;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056161
    move-object/from16 v0, p0

    iget-object v5, v0, LX/68i;->d:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v4, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1056162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{tile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/68i;->a:LX/69C;

    if-nez v0, :cond_0

    const-string v0, "{x}"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mParentTile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/68i;->b:LX/69C;

    if-nez v0, :cond_1

    const-string v0, "{x}"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/68i;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/68i;->a:LX/69C;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/68i;->b:LX/69C;

    goto :goto_1
.end method
