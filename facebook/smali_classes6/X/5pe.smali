.class public LX/5pe;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1008336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 1008337
    const-class v1, LX/5pe;

    monitor-enter v1

    .line 1008338
    :try_start_0
    sget-object v0, LX/5pe;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1008339
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/5pe;->a:Landroid/os/Handler;

    .line 1008340
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008341
    sget-object v0, LX/5pe;->a:Landroid/os/Handler;

    const v1, 0x6f2b2cc0

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1008342
    return-void

    .line 1008343
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 1008344
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 1008345
    invoke-static {}, LX/5pe;->a()Z

    move-result v0

    const-string v1, "Expected to run on UI thread!"

    invoke-static {v0, v1}, LX/5pd;->a(ZLjava/lang/String;)V

    .line 1008346
    return-void
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 1008347
    invoke-static {}, LX/5pe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expected not to run on UI thread!"

    invoke-static {v0, v1}, LX/5pd;->a(ZLjava/lang/String;)V

    .line 1008348
    return-void

    .line 1008349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
