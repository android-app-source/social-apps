.class public final LX/5BA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 46

    .prologue
    .line 861775
    const/16 v42, 0x0

    .line 861776
    const/16 v41, 0x0

    .line 861777
    const/16 v40, 0x0

    .line 861778
    const/16 v39, 0x0

    .line 861779
    const/16 v38, 0x0

    .line 861780
    const/16 v37, 0x0

    .line 861781
    const/16 v36, 0x0

    .line 861782
    const/16 v35, 0x0

    .line 861783
    const/16 v34, 0x0

    .line 861784
    const/16 v33, 0x0

    .line 861785
    const/16 v32, 0x0

    .line 861786
    const/16 v31, 0x0

    .line 861787
    const/16 v30, 0x0

    .line 861788
    const/16 v29, 0x0

    .line 861789
    const/16 v28, 0x0

    .line 861790
    const/16 v27, 0x0

    .line 861791
    const/16 v26, 0x0

    .line 861792
    const/16 v25, 0x0

    .line 861793
    const/16 v24, 0x0

    .line 861794
    const/16 v23, 0x0

    .line 861795
    const/16 v22, 0x0

    .line 861796
    const/16 v21, 0x0

    .line 861797
    const/16 v20, 0x0

    .line 861798
    const/16 v19, 0x0

    .line 861799
    const/16 v18, 0x0

    .line 861800
    const/16 v17, 0x0

    .line 861801
    const/16 v16, 0x0

    .line 861802
    const/4 v15, 0x0

    .line 861803
    const/4 v14, 0x0

    .line 861804
    const/4 v13, 0x0

    .line 861805
    const/4 v12, 0x0

    .line 861806
    const/4 v11, 0x0

    .line 861807
    const/4 v10, 0x0

    .line 861808
    const/4 v9, 0x0

    .line 861809
    const/4 v8, 0x0

    .line 861810
    const/4 v7, 0x0

    .line 861811
    const/4 v6, 0x0

    .line 861812
    const/4 v5, 0x0

    .line 861813
    const/4 v4, 0x0

    .line 861814
    const/4 v3, 0x0

    .line 861815
    const/4 v2, 0x0

    .line 861816
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1

    .line 861817
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861818
    const/4 v2, 0x0

    .line 861819
    :goto_0
    return v2

    .line 861820
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861821
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1e

    .line 861822
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v43

    .line 861823
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 861824
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1

    if-eqz v43, :cond_1

    .line 861825
    const-string v44, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2

    .line 861826
    const/4 v13, 0x1

    .line 861827
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 861828
    :cond_2
    const-string v44, "can_see_voice_switcher"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_3

    .line 861829
    const/4 v12, 0x1

    .line 861830
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 861831
    :cond_3
    const-string v44, "can_viewer_comment"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_4

    .line 861832
    const/4 v11, 0x1

    .line 861833
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 861834
    :cond_4
    const-string v44, "can_viewer_comment_with_photo"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_5

    .line 861835
    const/4 v10, 0x1

    .line 861836
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 861837
    :cond_5
    const-string v44, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_6

    .line 861838
    const/4 v9, 0x1

    .line 861839
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 861840
    :cond_6
    const-string v44, "can_viewer_comment_with_video"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_7

    .line 861841
    const/4 v8, 0x1

    .line 861842
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 861843
    :cond_7
    const-string v44, "can_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_8

    .line 861844
    const/4 v7, 0x1

    .line 861845
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 861846
    :cond_8
    const-string v44, "can_viewer_react"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_9

    .line 861847
    const/4 v6, 0x1

    .line 861848
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 861849
    :cond_9
    const-string v44, "can_viewer_subscribe"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_a

    .line 861850
    const/4 v5, 0x1

    .line 861851
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 861852
    :cond_a
    const-string v44, "comments_mirroring_domain"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 861853
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 861854
    :cond_b
    const-string v44, "default_comment_ordering"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 861855
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 861856
    :cond_c
    const-string v44, "does_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_d

    .line 861857
    const/4 v4, 0x1

    .line 861858
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 861859
    :cond_d
    const-string v44, "id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_e

    .line 861860
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 861861
    :cond_e
    const-string v44, "important_reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_f

    .line 861862
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 861863
    :cond_f
    const-string v44, "is_viewer_subscribed"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_10

    .line 861864
    const/4 v3, 0x1

    .line 861865
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 861866
    :cond_10
    const-string v44, "legacy_api_post_id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_11

    .line 861867
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 861868
    :cond_11
    const-string v44, "like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_12

    .line 861869
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 861870
    :cond_12
    const-string v44, "likers"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 861871
    invoke-static/range {p0 .. p1}, LX/5B6;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 861872
    :cond_13
    const-string v44, "reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_14

    .line 861873
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 861874
    :cond_14
    const-string v44, "remixable_photo_uri"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_15

    .line 861875
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 861876
    :cond_15
    const-string v44, "seen_by"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_16

    .line 861877
    invoke-static/range {p0 .. p1}, LX/5B7;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 861878
    :cond_16
    const-string v44, "supported_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 861879
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 861880
    :cond_17
    const-string v44, "top_level_comments"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_18

    .line 861881
    invoke-static/range {p0 .. p1}, LX/59L;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 861882
    :cond_18
    const-string v44, "top_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 861883
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 861884
    :cond_19
    const-string v44, "viewer_acts_as_page"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1a

    .line 861885
    invoke-static/range {p0 .. p1}, LX/5B8;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 861886
    :cond_1a
    const-string v44, "viewer_acts_as_person"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1b

    .line 861887
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 861888
    :cond_1b
    const-string v44, "viewer_does_not_like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1c

    .line 861889
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 861890
    :cond_1c
    const-string v44, "viewer_feedback_reaction_key"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1d

    .line 861891
    const/4 v2, 0x1

    .line 861892
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 861893
    :cond_1d
    const-string v44, "viewer_likes_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_0

    .line 861894
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 861895
    :cond_1e
    const/16 v43, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 861896
    if-eqz v13, :cond_1f

    .line 861897
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 861898
    :cond_1f
    if-eqz v12, :cond_20

    .line 861899
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 861900
    :cond_20
    if-eqz v11, :cond_21

    .line 861901
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 861902
    :cond_21
    if-eqz v10, :cond_22

    .line 861903
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 861904
    :cond_22
    if-eqz v9, :cond_23

    .line 861905
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 861906
    :cond_23
    if-eqz v8, :cond_24

    .line 861907
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 861908
    :cond_24
    if-eqz v7, :cond_25

    .line 861909
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 861910
    :cond_25
    if-eqz v6, :cond_26

    .line 861911
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 861912
    :cond_26
    if-eqz v5, :cond_27

    .line 861913
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 861914
    :cond_27
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 861915
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 861916
    if-eqz v4, :cond_28

    .line 861917
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 861918
    :cond_28
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 861919
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 861920
    if-eqz v3, :cond_29

    .line 861921
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 861922
    :cond_29
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861923
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861924
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861925
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861926
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861927
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861928
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861929
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861930
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861931
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861932
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861933
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 861934
    if-eqz v2, :cond_2a

    .line 861935
    const/16 v2, 0x1b

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 861936
    :cond_2a
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 861937
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
