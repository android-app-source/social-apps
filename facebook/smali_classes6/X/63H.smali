.class public LX/63H;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private final a:Landroid/text/TextPaint;

.field private final b:Landroid/graphics/Rect;

.field private final c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/text/TextPaint;I)V
    .locals 1

    .prologue
    .line 1043064
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 1043065
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/63H;->b:Landroid/graphics/Rect;

    .line 1043066
    iput-object p1, p0, LX/63H;->a:Landroid/text/TextPaint;

    .line 1043067
    iput p2, p0, LX/63H;->c:I

    .line 1043068
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 7

    .prologue
    .line 1043069
    iget-object v0, p0, LX/63H;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v0

    iput v0, p0, LX/63H;->d:I

    .line 1043070
    iget-object v0, p0, LX/63H;->a:Landroid/text/TextPaint;

    iget v1, p0, LX/63H;->c:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1043071
    int-to-float v5, p7

    iget-object v6, p0, LX/63H;->a:Landroid/text/TextPaint;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 1043072
    iget-object v0, p0, LX/63H;->a:Landroid/text/TextPaint;

    iget v1, p0, LX/63H;->d:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1043073
    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    .line 1043074
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/63H;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1043075
    iget-object v0, p0, LX/63H;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method
