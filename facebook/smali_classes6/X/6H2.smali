.class public LX/6H2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6Fa;

.field private final b:LX/0TD;

.field public final c:LX/2E5;


# direct methods
.method public constructor <init>(LX/6Fa;LX/0TD;LX/2E5;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1071224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071225
    iput-object p1, p0, LX/6H2;->a:LX/6Fa;

    .line 1071226
    iput-object p2, p0, LX/6H2;->b:LX/0TD;

    .line 1071227
    iput-object p3, p0, LX/6H2;->c:LX/2E5;

    .line 1071228
    return-void
.end method

.method public static b(LX/0QB;)LX/6H2;
    .locals 4

    .prologue
    .line 1071229
    new-instance v3, LX/6H2;

    invoke-static {p0}, LX/6Fa;->a(LX/0QB;)LX/6Fa;

    move-result-object v0

    check-cast v0, LX/6Fa;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, LX/2E5;->a(LX/0QB;)LX/2E5;

    move-result-object v2

    check-cast v2, LX/2E5;

    invoke-direct {v3, v0, v1, v2}, LX/6H2;-><init>(LX/6Fa;LX/0TD;LX/2E5;)V

    .line 1071230
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1071231
    iget-object v0, p0, LX/6H2;->b:LX/0TD;

    new-instance v1, Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;

    invoke-direct {v1, p0}, Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;-><init>(LX/6H2;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
