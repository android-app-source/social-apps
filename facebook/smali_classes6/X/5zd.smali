.class public LX/5zd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/5zd;


# instance fields
.field public final a:D

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final c:LX/0Zb;

.field private final d:LX/0V6;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "LX/5za;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/5zb;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0V6;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1035319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035320
    new-instance v0, LX/442;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, LX/442;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    .line 1035321
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/5zd;->f:Ljava/util/LinkedList;

    .line 1035322
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5zd;->g:Z

    .line 1035323
    iput-object p1, p0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1035324
    iput-object p2, p0, LX/5zd;->c:LX/0Zb;

    .line 1035325
    iput-object p3, p0, LX/5zd;->d:LX/0V6;

    .line 1035326
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    iput-wide v0, p0, LX/5zd;->a:D

    .line 1035327
    return-void

    .line 1035328
    :cond_0
    const-wide v0, 0x3fb999999999999aL    # 0.1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/5zd;
    .locals 7

    .prologue
    .line 1035329
    sget-object v0, LX/5zd;->h:LX/5zd;

    if-nez v0, :cond_1

    .line 1035330
    const-class v1, LX/5zd;

    monitor-enter v1

    .line 1035331
    :try_start_0
    sget-object v0, LX/5zd;->h:LX/5zd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1035332
    if-eqz v2, :cond_0

    .line 1035333
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1035334
    new-instance p0, LX/5zd;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v5

    check-cast v5, LX/0V6;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v6

    check-cast v6, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {p0, v3, v4, v5, v6}, LX/5zd;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0V6;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 1035335
    move-object v0, p0

    .line 1035336
    sput-object v0, LX/5zd;->h:LX/5zd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1035337
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1035338
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1035339
    :cond_1
    sget-object v0, LX/5zd;->h:LX/5zd;

    return-object v0

    .line 1035340
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1035341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized c(LX/5zd;)V
    .locals 4

    .prologue
    .line 1035342
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, LX/5zd;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/5zd;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1035343
    iget-object v0, p0, LX/5zd;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5zb;

    .line 1035344
    iget-object v1, p0, LX/5zd;->c:LX/0Zb;

    const-string v2, "wasteful_image_load"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1035345
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1035346
    const-string v2, "render_width"

    iget v3, v0, LX/5zb;->a:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1035347
    const-string v2, "render_height"

    iget v3, v0, LX/5zb;->b:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1035348
    const-string v2, "img_width"

    iget v3, v0, LX/5zb;->c:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1035349
    const-string v2, "img_height"

    iget v3, v0, LX/5zb;->d:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1035350
    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1035351
    :cond_0
    goto :goto_0

    .line 1035352
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1035353
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static d(LX/5zd;)Z
    .locals 1

    .prologue
    .line 1035354
    iget-object v0, p0, LX/5zd;->d:LX/0V6;

    invoke-virtual {v0}, LX/0V6;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 1035355
    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5za;

    .line 1035356
    if-nez v0, :cond_0

    .line 1035357
    const/4 v0, 0x0

    .line 1035358
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1035359
    iget-object p0, v0, LX/5za;->f:LX/4h4;

    .line 1035360
    iget-object v0, p0, LX/4h4;->b:Ljava/lang/String;

    move-object p0, v0

    .line 1035361
    move-object v0, p0

    .line 1035362
    invoke-interface {v1, p2, p3, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1035363
    invoke-static {p0}, LX/5zd;->d(LX/5zd;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1035364
    :goto_0
    return-object v0

    .line 1035365
    :cond_1
    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5za;

    .line 1035366
    iget-object v2, v0, LX/5za;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 1035367
    const/4 v3, 0x1

    .line 1035368
    const-string v4, "UrlImagePipelineExperiment"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1035369
    :cond_2
    :goto_1
    move v3, v3

    .line 1035370
    if-nez v3, :cond_3

    move-object v0, v1

    .line 1035371
    goto :goto_0

    .line 1035372
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1035373
    iget-object v3, v0, LX/5za;->f:LX/4h4;

    invoke-virtual {v3, p2, p3}, LX/4h4;->a(ILjava/lang/String;)LX/0Yj;

    move-result-object v3

    .line 1035374
    iget-object v4, v3, LX/0Yj;->l:Ljava/util/Map;

    move-object v4, v4

    .line 1035375
    const-string v5, "uri_key"

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1035376
    move-object v1, v3

    .line 1035377
    iget-object v0, v1, LX/0Yj;->l:Ljava/util/Map;

    move-object v0, v0

    .line 1035378
    const-string v3, "UrlImageUrlBeingFetched"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1035379
    invoke-virtual {v1, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 1035380
    iget-object v0, p0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v3, LX/0Yj;

    invoke-direct {v3, v1}, LX/0Yj;-><init>(LX/0Yj;)V

    invoke-interface {v0, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 1035381
    iget-object v0, p0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->e(LX/0Yj;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1035382
    const-string v0, "UrlImagePrefetch"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1035383
    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5za;

    .line 1035384
    iput-object v1, v0, LX/5za;->d:LX/0Yj;

    .line 1035385
    :cond_4
    :goto_2
    move-object v0, v1

    .line 1035386
    goto :goto_0

    .line 1035387
    :cond_5
    const-string v0, "UrlImageBindModelToRender"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1035388
    iget-object v0, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5za;

    .line 1035389
    iput-object v1, v0, LX/5za;->e:LX/0Yj;

    .line 1035390
    goto :goto_2

    .line 1035391
    :cond_6
    const-string v4, "UrlImageBindModelToRender"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "UrlImagePrefetch"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1035392
    :cond_7
    invoke-static {p0, v2, p2, p3}, LX/5zd;->d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1035393
    :cond_8
    const v3, 0x530001

    const-string v4, "UrlImageBindModelToRender"

    invoke-static {p0, v2, v3, v4}, LX/5zd;->d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const v3, 0x530003

    const-string v4, "UrlImagePrefetch"

    invoke-static {p0, v2, v3, v4}, LX/5zd;->d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1035394
    :cond_9
    const/4 v3, 0x1

    .line 1035395
    :goto_3
    move v3, v3

    .line 1035396
    goto/16 :goto_1

    :cond_a
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final a(LX/0Yj;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Yj;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1035397
    invoke-static {p0}, LX/5zd;->d(LX/5zd;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1035398
    :cond_0
    :goto_0
    return-void

    .line 1035399
    :cond_1
    iget-object v0, p1, LX/0Yj;->l:Ljava/util/Map;

    move-object v0, v0

    .line 1035400
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1035401
    invoke-virtual {p1, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 1035402
    iget-object v0, p0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1035403
    if-eqz p1, :cond_0

    .line 1035404
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/5zd;->g:Z

    .line 1035405
    invoke-static {p0}, LX/5zd;->c(LX/5zd;)V

    .line 1035406
    :goto_0
    return-void

    .line 1035407
    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, LX/5zd;->g:Z

    .line 1035408
    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1035409
    if-nez p1, :cond_1

    .line 1035410
    :cond_0
    :goto_0
    return-object v0

    .line 1035411
    :cond_1
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1035412
    invoke-static {p0}, LX/5zd;->d(LX/5zd;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1035413
    :cond_2
    invoke-static {p0, p1, p2, p3}, LX/5zd;->d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1035414
    invoke-virtual {p0, p1, p2, p3}, LX/5zd;->a(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;

    move-result-object v0

    goto :goto_0

    .line 1035415
    :cond_3
    new-instance v3, LX/5za;

    invoke-direct {v3, p0, p1, v1}, LX/5za;-><init>(LX/5zd;Landroid/net/Uri;Ljava/util/List;)V

    .line 1035416
    iget-object v2, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1035417
    iget-object v2, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1035418
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1035419
    iget-object v5, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1035420
    iget-object v5, p0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
