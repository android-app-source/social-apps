.class public LX/55b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/feed/SubmitSurveyResponseParams;",
        "Lcom/facebook/api/feed/SubmitSurveyResponseResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 829485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829486
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 829487
    check-cast p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;

    .line 829488
    iget-object v0, p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;->c:Ljava/lang/String;

    .line 829489
    new-instance v2, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/162;-><init>(LX/0mC;)V

    .line 829490
    invoke-virtual {v2, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 829491
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 829492
    invoke-virtual {v3, v0, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 829493
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 829494
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 829495
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "answers"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829496
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "response"

    iget-object v2, p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829497
    new-instance v0, LX/14N;

    const-string v1, "postResponse"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/facebook/api/feed/SubmitSurveyResponseParams;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/responses"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 829498
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 829499
    const-string v1, "response"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 829500
    const-string v2, "state"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v2, "next_page"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 829501
    new-instance v2, Lcom/facebook/api/feed/SubmitSurveyResponseResult;

    invoke-direct {v2, v1, v0}, Lcom/facebook/api/feed/SubmitSurveyResponseResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method
