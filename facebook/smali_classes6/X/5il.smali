.class public final LX/5il;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 984731
    const-wide/16 v8, 0x0

    .line 984732
    const/4 v6, 0x0

    .line 984733
    const/4 v5, 0x0

    .line 984734
    const/4 v4, 0x0

    .line 984735
    const-wide/16 v2, 0x0

    .line 984736
    const/4 v1, 0x0

    .line 984737
    const/4 v0, 0x0

    .line 984738
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_a

    .line 984739
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 984740
    const/4 v0, 0x0

    .line 984741
    :goto_0
    return v0

    .line 984742
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_7

    .line 984743
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 984744
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 984745
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 984746
    const-string v4, "end_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 984747
    const/4 v0, 0x1

    .line 984748
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 984749
    :cond_1
    const-string v4, "frames"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 984750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 984751
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 984752
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 984753
    invoke-static {p0, p1}, LX/5j6;->a(LX/15w;LX/186;)I

    move-result v4

    .line 984754
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 984755
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 984756
    move v11, v0

    goto :goto_1

    .line 984757
    :cond_3
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 984758
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 984759
    :cond_4
    const-string v4, "name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 984760
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 984761
    :cond_5
    const-string v4, "start_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 984762
    const/4 v0, 0x1

    .line 984763
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto/16 :goto_1

    .line 984764
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 984765
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 984766
    if-eqz v1, :cond_8

    .line 984767
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 984768
    :cond_8
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 984769
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 984770
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 984771
    if-eqz v6, :cond_9

    .line 984772
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 984773
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v7, v4

    move v10, v5

    move v11, v6

    move v6, v0

    move-wide v12, v2

    move-wide v2, v8

    move-wide v8, v12

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 984774
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 984775
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 984776
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 984777
    const-string v2, "end_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984778
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 984779
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 984780
    if-eqz v0, :cond_2

    .line 984781
    const-string v1, "frames"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984782
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 984783
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 984784
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5j6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 984785
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 984786
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 984787
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 984788
    if-eqz v0, :cond_3

    .line 984789
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984790
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 984791
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 984792
    if-eqz v0, :cond_4

    .line 984793
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984794
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 984795
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 984796
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 984797
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984798
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 984799
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 984800
    return-void
.end method
