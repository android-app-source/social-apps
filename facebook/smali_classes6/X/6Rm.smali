.class public final LX/6Rm;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1090217
    const-class v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;

    const v0, -0x48339902

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "LiveReactionsAddStreamingReactionMutation"

    const-string v6, "292f7fc736febdc4e8520eed16869fce"

    const-string v7, "feedback_add_streaming_reaction"

    const-string v8, "0"

    const-string v9, "10155069967361729"

    const/4 v10, 0x0

    .line 1090218
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1090219
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1090220
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1090221
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1090222
    sparse-switch v0, :sswitch_data_0

    .line 1090223
    :goto_0
    return-object p1

    .line 1090224
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1090225
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1090226
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x191facd1 -> :sswitch_2
        0x5684d8c -> :sswitch_1
        0x5fb57ca -> :sswitch_0
    .end sparse-switch
.end method
