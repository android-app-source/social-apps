.class public LX/5PW;
.super LX/5PV;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# direct methods
.method public constructor <init>(LX/5PM;Landroid/view/Surface;)V
    .locals 4

    .prologue
    .line 910821
    invoke-direct {p0, p1}, LX/5PV;-><init>(LX/5PM;)V

    .line 910822
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    const/4 p1, 0x0

    .line 910823
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/16 v2, 0x3038

    aput v2, v1, p1

    .line 910824
    iget-object v2, v0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    iget-object v3, v0, LX/5PM;->c:Landroid/opengl/EGLConfig;

    invoke-static {v2, v3, p2, v1, p1}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v1

    .line 910825
    const-string v2, "eglCreateWindowSurface"

    invoke-static {v2}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910826
    invoke-static {v1}, LX/64O;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910827
    move-object v0, v1

    .line 910828
    iput-object v0, p0, LX/5PW;->a:Landroid/opengl/EGLSurface;

    .line 910829
    return-void
.end method

.method public constructor <init>(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 910830
    new-instance v0, LX/5PM;

    invoke-direct {v0}, LX/5PM;-><init>()V

    invoke-virtual {v0}, LX/5PM;->e()LX/5PM;

    move-result-object v0

    invoke-direct {p0, v0}, LX/5PV;-><init>(LX/5PM;)V

    .line 910831
    iget-object v0, p0, LX/5PV;->b:LX/5PM;

    invoke-virtual {v0, p1}, LX/5PM;->a(Landroid/graphics/SurfaceTexture;)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/5PW;->a:Landroid/opengl/EGLSurface;

    .line 910832
    return-void
.end method
