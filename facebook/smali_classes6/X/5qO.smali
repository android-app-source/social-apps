.class public LX/5qO;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "JSCSamplingProfiler"
.end annotation


# static fields
.field private static final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/5qO;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Z

.field private c:I

.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1009236
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/5qO;->f:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1009221
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1009222
    iput-object v0, p0, LX/5qO;->a:Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    .line 1009223
    iput-boolean v1, p0, LX/5qO;->b:Z

    .line 1009224
    iput v1, p0, LX/5qO;->c:I

    .line 1009225
    iput-object v0, p0, LX/5qO;->d:Ljava/lang/String;

    .line 1009226
    iput-object v0, p0, LX/5qO;->e:Ljava/lang/String;

    .line 1009227
    return-void
.end method

.method private static declared-synchronized a(LX/5qO;)V
    .locals 3

    .prologue
    .line 1009231
    const-class v1, LX/5qO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5qO;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009232
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "a JSCSamplingProfiler registered more than once"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009233
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1009234
    :cond_0
    :try_start_1
    sget-object v0, LX/5qO;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009235
    monitor-exit v1

    return-void
.end method

.method private static declared-synchronized b(LX/5qO;)V
    .locals 2

    .prologue
    .line 1009228
    const-class v1, LX/5qO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5qO;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009229
    monitor-exit v1

    return-void

    .line 1009230
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 1009237
    invoke-super {p0}, LX/5pb;->d()V

    .line 1009238
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1009239
    const-class v1, Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    iput-object v0, p0, LX/5qO;->a:Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    .line 1009240
    invoke-static {p0}, LX/5qO;->a(LX/5qO;)V

    .line 1009241
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1009210
    invoke-super {p0}, LX/5pb;->f()V

    .line 1009211
    invoke-static {p0}, LX/5qO;->b(LX/5qO;)V

    .line 1009212
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qO;->a:Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    .line 1009213
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009209
    const-string v0, "JSCSamplingProfiler"

    return-object v0
.end method

.method public declared-synchronized operationComplete(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1009214
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/5qO;->c:I

    if-ne p1, v0, :cond_0

    .line 1009215
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5qO;->b:Z

    .line 1009216
    iput-object p2, p0, LX/5qO;->e:Ljava/lang/String;

    .line 1009217
    iput-object p3, p0, LX/5qO;->d:Ljava/lang/String;

    .line 1009218
    const v0, -0x1f9b4eaa

    invoke-static {p0, v0}, LX/02L;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 1009219
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Completed operation is not in progress."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
