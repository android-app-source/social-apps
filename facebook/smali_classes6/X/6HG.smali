.class public LX/6HG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:J

.field public f:Z

.field public g:J

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1071532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071533
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6HG;->f:Z

    .line 1071534
    iput-object p1, p0, LX/6HG;->h:Ljava/lang/String;

    .line 1071535
    invoke-virtual {p0}, LX/6HG;->a()V

    .line 1071536
    return-void
.end method

.method public static m()J
    .locals 2

    .prologue
    .line 1071537
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1071538
    iput v0, p0, LX/6HG;->a:I

    .line 1071539
    iput v0, p0, LX/6HG;->b:I

    .line 1071540
    iput v0, p0, LX/6HG;->c:I

    .line 1071541
    iput v0, p0, LX/6HG;->d:I

    .line 1071542
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6HG;->e:J

    .line 1071543
    invoke-static {}, LX/6HG;->m()J

    move-result-wide v0

    iput-wide v0, p0, LX/6HG;->g:J

    .line 1071544
    return-void
.end method

.method public final l()F
    .locals 6

    .prologue
    .line 1071545
    iget-wide v0, p0, LX/6HG;->e:J

    .line 1071546
    iget-boolean v2, p0, LX/6HG;->f:Z

    if-eqz v2, :cond_0

    .line 1071547
    invoke-static {}, LX/6HG;->m()J

    move-result-wide v2

    iget-wide v4, p0, LX/6HG;->g:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 1071548
    :cond_0
    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method
