.class public final LX/6JL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1075423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075424
    return-void
.end method

.method private static a(LX/6JR;LX/6JR;)I
    .locals 6

    .prologue
    .line 1075425
    iget v0, p0, LX/6JR;->a:I

    int-to-long v0, v0

    iget v2, p0, LX/6JR;->b:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p1, LX/6JR;->a:I

    int-to-long v2, v2

    iget v4, p1, LX/6JR;->b:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/List;II)LX/6JR;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;II)",
            "LX/6JR;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1075426
    const/4 v6, 0x0

    .line 1075427
    const/4 v3, 0x0

    .line 1075428
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 1075429
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 1075430
    int-to-double v4, v10

    int-to-double v8, v11

    div-double v12, v4, v8

    .line 1075431
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1075432
    const/4 v2, 0x0

    move v7, v2

    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v7, v2, :cond_5

    .line 1075433
    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6JR;

    .line 1075434
    iget v8, v2, LX/6JR;->a:I

    iget v9, v2, LX/6JR;->b:I

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 1075435
    iget v8, v2, LX/6JR;->a:I

    iget v9, v2, LX/6JR;->b:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1075436
    int-to-double v8, v14

    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v8, v8, v16

    .line 1075437
    sub-double v8, v12, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    .line 1075438
    sub-double v16, v8, v4

    .line 1075439
    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v18, v16, v18

    if-gtz v18, :cond_3

    .line 1075440
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v16, v16, v18

    if-lez v16, :cond_0

    .line 1075441
    const/4 v6, 0x0

    .line 1075442
    const/4 v3, 0x0

    move-wide v4, v8

    .line 1075443
    :cond_0
    if-le v14, v10, :cond_4

    if-le v15, v11, :cond_4

    .line 1075444
    if-nez v3, :cond_2

    .line 1075445
    const/4 v3, 0x1

    :cond_1
    :goto_1
    move-wide/from16 v20, v4

    move v4, v3

    move-object v5, v2

    move-wide/from16 v2, v20

    .line 1075446
    :goto_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v5

    move/from16 v20, v4

    move-wide v4, v2

    move/from16 v3, v20

    goto :goto_0

    .line 1075447
    :cond_2
    invoke-static {v2, v6}, LX/6JL;->a(LX/6JR;LX/6JR;)I

    move-result v8

    if-ltz v8, :cond_1

    :cond_3
    move-wide/from16 v20, v4

    move v4, v3

    move-object v5, v6

    move-wide/from16 v2, v20

    goto :goto_2

    .line 1075448
    :cond_4
    if-nez v3, :cond_3

    if-eqz v6, :cond_1

    invoke-static {v2, v6}, LX/6JL;->a(LX/6JR;LX/6JR;)I

    move-result v8

    if-lez v8, :cond_3

    goto :goto_1

    .line 1075449
    :cond_5
    return-object v6
.end method
