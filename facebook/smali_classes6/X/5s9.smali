.class public LX/5s9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pQ;


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/5r0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/Object;

.field public final c:Ljava/lang/Object;

.field public final d:LX/5pY;

.field public final e:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5r0;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5sA;",
            ">;"
        }
    .end annotation
.end field

.field public j:[LX/5r0;

.field public k:I

.field public volatile l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/5s8;

.field private n:S

.field public volatile o:Z

.field public volatile p:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1012543
    new-instance v0, LX/5s7;

    invoke-direct {v0}, LX/5s7;-><init>()V

    sput-object v0, LX/5s9;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1012551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012552
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/5s9;->b:Ljava/lang/Object;

    .line 1012553
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/5s9;->c:Ljava/lang/Object;

    .line 1012554
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, LX/5s9;->e:Landroid/util/LongSparseArray;

    .line 1012555
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/5s9;->f:Ljava/util/Map;

    .line 1012556
    new-instance v0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;-><init>(LX/5s9;)V

    iput-object v0, p0, LX/5s9;->g:Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;

    .line 1012557
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5s9;->h:Ljava/util/ArrayList;

    .line 1012558
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5s9;->i:Ljava/util/ArrayList;

    .line 1012559
    const/16 v0, 0x10

    new-array v0, v0, [LX/5r0;

    iput-object v0, p0, LX/5s9;->j:[LX/5r0;

    .line 1012560
    iput v1, p0, LX/5s9;->k:I

    .line 1012561
    iput-short v1, p0, LX/5s9;->n:S

    .line 1012562
    iput-boolean v1, p0, LX/5s9;->o:Z

    .line 1012563
    iput v1, p0, LX/5s9;->p:I

    .line 1012564
    iput-object p1, p0, LX/5s9;->d:LX/5pY;

    .line 1012565
    iget-object v0, p0, LX/5s9;->d:LX/5pY;

    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 1012566
    new-instance v0, LX/5s8;

    invoke-direct {v0, p0}, LX/5s8;-><init>(LX/5s9;)V

    iput-object v0, p0, LX/5s9;->m:LX/5s8;

    .line 1012567
    return-void
.end method

.method private a(ILjava/lang/String;S)J
    .locals 3

    .prologue
    .line 1012545
    iget-object v0, p0, LX/5s9;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    .line 1012546
    if-eqz v0, :cond_0

    .line 1012547
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    .line 1012548
    :goto_0
    invoke-static {p1, v0, p3}, LX/5s9;->a(ISS)J

    move-result-wide v0

    return-wide v0

    .line 1012549
    :cond_0
    iget-short v0, p0, LX/5s9;->n:S

    add-int/lit8 v1, v0, 0x1

    int-to-short v1, v1

    iput-short v1, p0, LX/5s9;->n:S

    .line 1012550
    iget-object v1, p0, LX/5s9;->f:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(ISS)J
    .locals 8

    .prologue
    const-wide/32 v6, 0xffff

    .line 1012544
    int-to-long v0, p0

    int-to-long v2, p1

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p2

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private b(LX/5r0;)V
    .locals 3

    .prologue
    .line 1012539
    iget v0, p0, LX/5s9;->k:I

    iget-object v1, p0, LX/5s9;->j:[LX/5r0;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 1012540
    iget-object v0, p0, LX/5s9;->j:[LX/5r0;

    iget-object v1, p0, LX/5s9;->j:[LX/5r0;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5r0;

    iput-object v0, p0, LX/5s9;->j:[LX/5r0;

    .line 1012541
    :cond_0
    iget-object v0, p0, LX/5s9;->j:[LX/5r0;

    iget v1, p0, LX/5s9;->k:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/5s9;->k:I

    aput-object p1, v0, v1

    .line 1012542
    return-void
.end method

.method public static f(LX/5s9;)V
    .locals 1

    .prologue
    .line 1012536
    invoke-static {}, LX/5pe;->b()V

    .line 1012537
    iget-object v0, p0, LX/5s9;->m:LX/5s8;

    invoke-virtual {v0}, LX/5s8;->a()V

    .line 1012538
    return-void
.end method

.method public static g(LX/5s9;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1012568
    iget-object v6, p0, LX/5s9;->b:Ljava/lang/Object;

    monitor-enter v6

    .line 1012569
    :try_start_0
    iget-object v7, p0, LX/5s9;->c:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1012570
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    :try_start_1
    iget-object v0, p0, LX/5s9;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    .line 1012571
    iget-object v0, p0, LX/5s9;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r0;

    .line 1012572
    invoke-virtual {v0}, LX/5r0;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1012573
    invoke-direct {p0, v0}, LX/5s9;->b(LX/5r0;)V

    .line 1012574
    :cond_0
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1012575
    :cond_1
    iget v1, v0, LX/5r0;->c:I

    move v1, v1

    .line 1012576
    invoke-virtual {v0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/5r0;->f()S

    move-result v4

    invoke-direct {p0, v1, v3, v4}, LX/5s9;->a(ILjava/lang/String;S)J

    move-result-wide v8

    .line 1012577
    iget-object v1, p0, LX/5s9;->e:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1012578
    if-nez v1, :cond_3

    .line 1012579
    iget-object v1, p0, LX/5s9;->e:Landroid/util/LongSparseArray;

    iget v3, p0, LX/5s9;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v8, v9, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v1, v0

    move-object v0, v2

    .line 1012580
    :goto_2
    if-eqz v1, :cond_2

    .line 1012581
    invoke-direct {p0, v1}, LX/5s9;->b(LX/5r0;)V

    .line 1012582
    :cond_2
    if-eqz v0, :cond_0

    .line 1012583
    invoke-virtual {v0}, LX/5r0;->i()V

    goto :goto_1

    .line 1012584
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 1012585
    :catchall_1
    move-exception v0

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1012586
    :cond_3
    :try_start_3
    iget-object v3, p0, LX/5s9;->j:[LX/5r0;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-object v3, v3, v4

    .line 1012587
    invoke-virtual {v0, v3}, LX/5r0;->a(LX/5r0;)LX/5r0;

    move-result-object v4

    .line 1012588
    if-eq v4, v3, :cond_4

    .line 1012589
    iget-object v0, p0, LX/5s9;->e:Landroid/util/LongSparseArray;

    iget v10, p0, LX/5s9;->k:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v8, v9, v10}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1012590
    iget-object v0, p0, LX/5s9;->j:[LX/5r0;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v8, 0x0

    aput-object v8, v0, v1

    move-object v0, v3

    move-object v1, v4

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 1012591
    goto :goto_2

    .line 1012592
    :cond_5
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1012593
    :try_start_4
    iget-object v0, p0, LX/5s9;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1012594
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public static synthetic h(LX/5s9;)I
    .locals 2

    .prologue
    .line 1012535
    iget v0, p0, LX/5s9;->p:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/5s9;->p:I

    return v0
.end method

.method public static h(LX/5s9;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1012532
    iget-object v0, p0, LX/5s9;->j:[LX/5r0;

    iget v1, p0, LX/5s9;->k:I

    const/4 v2, 0x0

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 1012533
    iput v3, p0, LX/5s9;->k:I

    .line 1012534
    return-void
.end method


# virtual methods
.method public final a(LX/5r0;)V
    .locals 5

    .prologue
    .line 1012518
    iget-boolean v0, p1, LX/5r0;->b:Z

    move v0, v0

    .line 1012519
    const-string v1, "Dispatched event hasn\'t been initialized"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1012520
    iget-object v0, p0, LX/5s9;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sA;

    .line 1012521
    invoke-interface {v0, p1}, LX/5sA;->a(LX/5r0;)V

    goto :goto_0

    .line 1012522
    :cond_0
    iget-object v1, p0, LX/5s9;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1012523
    :try_start_0
    iget-object v0, p0, LX/5s9;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012524
    const-wide/16 v2, 0x2000

    invoke-virtual {p1}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v0

    .line 1012525
    iget v4, p1, LX/5r0;->e:I

    move v4, v4

    .line 1012526
    invoke-static {v2, v3, v0, v4}, LX/018;->d(JLjava/lang/String;I)V

    .line 1012527
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1012528
    iget-object v0, p0, LX/5s9;->l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    if-eqz v0, :cond_1

    .line 1012529
    iget-object v0, p0, LX/5s9;->m:LX/5s8;

    invoke-virtual {v0}, LX/5s8;->c()V

    .line 1012530
    :cond_1
    return-void

    .line 1012531
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/5sA;)V
    .locals 1

    .prologue
    .line 1012507
    iget-object v0, p0, LX/5s9;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012508
    return-void
.end method

.method public final bM_()V
    .locals 2

    .prologue
    .line 1012513
    invoke-static {}, LX/5pe;->b()V

    .line 1012514
    iget-object v0, p0, LX/5s9;->l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    if-nez v0, :cond_0

    .line 1012515
    iget-object v0, p0, LX/5s9;->d:LX/5pY;

    const-class v1, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    iput-object v0, p0, LX/5s9;->l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    .line 1012516
    :cond_0
    iget-object v0, p0, LX/5s9;->m:LX/5s8;

    invoke-virtual {v0}, LX/5s8;->b()V

    .line 1012517
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 1012511
    invoke-static {p0}, LX/5s9;->f(LX/5s9;)V

    .line 1012512
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 1012509
    invoke-static {p0}, LX/5s9;->f(LX/5s9;)V

    .line 1012510
    return-void
.end method
