.class public final LX/5Q8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 912319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 912320
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 912321
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 912322
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 912323
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 912324
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 912325
    :goto_1
    move v1, v2

    .line 912326
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 912327
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 912328
    :cond_1
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 912329
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v4

    move v4, v3

    .line 912330
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 912331
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 912332
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 912333
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 912334
    const-string v9, "inline_style"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 912335
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_2

    .line 912336
    :cond_3
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 912337
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_2

    .line 912338
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 912339
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 912340
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 912341
    if-eqz v4, :cond_6

    .line 912342
    invoke-virtual {p1, v3, v6, v2}, LX/186;->a(III)V

    .line 912343
    :cond_6
    if-eqz v1, :cond_7

    .line 912344
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 912345
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 912346
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 912347
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 912348
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 912349
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 912350
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 912351
    if-eqz v2, :cond_0

    .line 912352
    const-string v2, "inline_style"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912353
    invoke-virtual {p0, v1, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912354
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 912355
    if-eqz v2, :cond_1

    .line 912356
    const-string v3, "length"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912357
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 912358
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 912359
    if-eqz v2, :cond_2

    .line 912360
    const-string v3, "offset"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912361
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 912362
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 912363
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 912364
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 912365
    return-void
.end method
