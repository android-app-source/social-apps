.class public final LX/6Ni;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1083914
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method private static a(LX/0eX;)I
    .locals 1

    .prologue
    .line 1083915
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 1083916
    return v0
.end method

.method public static a(LX/0eX;IIIIIIZIJIIIZZB)I
    .locals 3

    .prologue
    .line 1083917
    const/16 v1, 0xf

    invoke-virtual {p0, v1}, LX/0eX;->b(I)V

    .line 1083918
    invoke-static {p0, p9, p10}, LX/6Ni;->a(LX/0eX;J)V

    .line 1083919
    move/from16 v0, p13

    invoke-static {p0, v0}, LX/6Ni;->k(LX/0eX;I)V

    .line 1083920
    invoke-static {p0, p12}, LX/6Ni;->j(LX/0eX;I)V

    .line 1083921
    invoke-static {p0, p11}, LX/6Ni;->i(LX/0eX;I)V

    .line 1083922
    invoke-static {p0, p8}, LX/6Ni;->h(LX/0eX;I)V

    .line 1083923
    invoke-static {p0, p6}, LX/6Ni;->g(LX/0eX;I)V

    .line 1083924
    invoke-static {p0, p5}, LX/6Ni;->f(LX/0eX;I)V

    .line 1083925
    invoke-static {p0, p4}, LX/6Ni;->e(LX/0eX;I)V

    .line 1083926
    invoke-static {p0, p3}, LX/6Ni;->d(LX/0eX;I)V

    .line 1083927
    invoke-static {p0, p2}, LX/6Ni;->c(LX/0eX;I)V

    .line 1083928
    invoke-static {p0, p1}, LX/6Ni;->b(LX/0eX;I)V

    .line 1083929
    move/from16 v0, p16

    invoke-static {p0, v0}, LX/6Ni;->a(LX/0eX;B)V

    .line 1083930
    move/from16 v0, p15

    invoke-static {p0, v0}, LX/6Ni;->c(LX/0eX;Z)V

    .line 1083931
    move/from16 v0, p14

    invoke-static {p0, v0}, LX/6Ni;->b(LX/0eX;Z)V

    .line 1083932
    invoke-static {p0, p7}, LX/6Ni;->a(LX/0eX;Z)V

    .line 1083933
    invoke-static {p0}, LX/6Ni;->a(LX/0eX;)I

    move-result v1

    return v1
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/6Ni;
    .locals 3

    .prologue
    .line 1083934
    new-instance v0, LX/6Ni;

    invoke-direct {v0}, LX/6Ni;-><init>()V

    .line 1083935
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    .line 1083936
    iput v1, v0, LX/6Ni;->a:I

    iput-object p0, v0, LX/6Ni;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1083937
    move-object v0, v1

    .line 1083938
    return-object v0
.end method

.method private static a(LX/0eX;B)V
    .locals 2

    .prologue
    .line 1083939
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IBI)V

    return-void
.end method

.method public static a(LX/0eX;I)V
    .locals 0

    .prologue
    .line 1083940
    invoke-virtual {p0, p1}, LX/0eX;->c(I)V

    return-void
.end method

.method private static a(LX/0eX;J)V
    .locals 7

    .prologue
    .line 1083942
    const/16 v1, 0x8

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, LX/0eX;->a(IJJ)V

    return-void
.end method

.method private static a(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083941
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static b(LX/0eX;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1083946
    invoke-virtual {p0, v0, p1, v0}, LX/0eX;->c(III)V

    return-void
.end method

.method private static b(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083945
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static c(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083947
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static c(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083944
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static d(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083943
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static e(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083912
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static f(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083913
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static g(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083911
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static h(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083910
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static i(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083909
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static j(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083908
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static k(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083907
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method


# virtual methods
.method public final a(LX/6Ng;)LX/6Ng;
    .locals 2

    .prologue
    .line 1083904
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083905
    iput v0, p1, LX/6Ng;->a:I

    iput-object v1, p1, LX/6Ng;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 1083906
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/6Nm;)LX/6Nm;
    .locals 2

    .prologue
    .line 1083903
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, LX/6Nm;->a(ILjava/nio/ByteBuffer;)LX/6Nm;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/6Nq;)LX/6Nq;
    .locals 2

    .prologue
    .line 1083902
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, LX/6Nq;->a(ILjava/nio/ByteBuffer;)LX/6Nq;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/6Nm;)LX/6Nm;
    .locals 2

    .prologue
    .line 1083901
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, LX/6Nm;->a(ILjava/nio/ByteBuffer;)LX/6Nm;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/6Nq;)LX/6Nq;
    .locals 2

    .prologue
    .line 1083900
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, LX/6Nq;->a(ILjava/nio/ByteBuffer;)LX/6Nq;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/6Nq;)LX/6Nq;
    .locals 2

    .prologue
    .line 1083899
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, LX/6Nq;->a(ILjava/nio/ByteBuffer;)LX/6Nq;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()J
    .locals 3

    .prologue
    .line 1083898
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1083897
    const/16 v1, 0x1c

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final n()B
    .locals 3

    .prologue
    .line 1083896
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
