.class public final LX/62J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/listview/SplitHideableListView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/listview/SplitHideableListView;)V
    .locals 0

    .prologue
    .line 1041367
    iput-object p1, p0, LX/62J;->a:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1041373
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1041374
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 1041368
    iget-object v0, p0, LX/62J;->a:Lcom/facebook/widget/listview/SplitHideableListView;

    const/4 v1, 0x1

    .line 1041369
    iput-boolean v1, v0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041370
    check-cast p1, LX/2qr;

    .line 1041371
    iget-object v0, p1, LX/2qr;->b:Landroid/view/View;

    new-instance v1, Lcom/facebook/widget/listview/SplitHideableListView$5$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/widget/listview/SplitHideableListView$5$1;-><init>(LX/62J;LX/2qr;)V

    invoke-virtual {p1}, LX/2qr;->getDuration()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1041372
    return-void
.end method
