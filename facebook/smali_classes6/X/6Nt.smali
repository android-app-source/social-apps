.class public LX/6Nt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/contacts/server/AddContactParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3fx;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3fx;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fx;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084019
    iput-object p1, p0, LX/6Nt;->a:LX/3fx;

    .line 1084020
    iput-object p2, p0, LX/6Nt;->b:LX/0Or;

    .line 1084021
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1084009
    check-cast p1, Lcom/facebook/contacts/server/AddContactParams;

    .line 1084010
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1084011
    iget-object v0, p1, Lcom/facebook/contacts/server/AddContactParams;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1084012
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "profile_id"

    iget-object v2, p1, Lcom/facebook/contacts/server/AddContactParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084013
    :cond_0
    :goto_0
    new-instance v0, LX/14N;

    const-string v1, "addUserToRolodexMethod"

    const-string v2, "POST"

    const-string v3, "me/contacts"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1084014
    :cond_1
    iget-object v0, p1, Lcom/facebook/contacts/server/AddContactParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1084015
    iget-object v0, p0, LX/6Nt;->a:LX/3fx;

    iget-object v1, p1, Lcom/facebook/contacts/server/AddContactParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3fx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1084016
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "phone"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084017
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "country_code"

    iget-object v0, p0, LX/6Nt;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1084006
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1084007
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1084008
    return-object v0
.end method
