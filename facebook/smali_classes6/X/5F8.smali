.class public final LX/5F8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 884667
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 884668
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884669
    :goto_0
    return v1

    .line 884670
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884671
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 884672
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 884673
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884674
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 884675
    const-string v9, "all_donations_summary_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 884676
    invoke-static {p0, p1}, LX/4ar;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 884677
    :cond_2
    const-string v9, "charity_interface"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 884678
    invoke-static {p0, p1}, LX/5F4;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 884679
    :cond_3
    const-string v9, "detailed_amount_raised_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 884680
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 884681
    :cond_4
    const-string v9, "donors_social_context_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 884682
    invoke-static {p0, p1}, LX/5F5;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 884683
    :cond_5
    const-string v9, "friend_donors"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 884684
    invoke-static {p0, p1}, LX/5F7;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 884685
    :cond_6
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 884686
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 884687
    :cond_7
    const-string v9, "mobile_donate_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 884688
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 884689
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 884690
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 884691
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 884692
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 884693
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 884694
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 884695
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 884696
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 884697
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 884698
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884699
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884700
    if-eqz v0, :cond_0

    .line 884701
    const-string v1, "all_donations_summary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884702
    invoke-static {p0, v0, p2, p3}, LX/4ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884703
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884704
    if-eqz v0, :cond_1

    .line 884705
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884706
    invoke-static {p0, v0, p2, p3}, LX/5F4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884707
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884708
    if-eqz v0, :cond_2

    .line 884709
    const-string v1, "detailed_amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884710
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884711
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884712
    if-eqz v0, :cond_3

    .line 884713
    const-string v1, "donors_social_context_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884714
    invoke-static {p0, v0, p2}, LX/5F5;->a(LX/15i;ILX/0nX;)V

    .line 884715
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884716
    if-eqz v0, :cond_4

    .line 884717
    const-string v1, "friend_donors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884718
    invoke-static {p0, v0, p2, p3}, LX/5F7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884719
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884720
    if-eqz v0, :cond_5

    .line 884721
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884722
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884723
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884724
    if-eqz v0, :cond_6

    .line 884725
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884726
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884727
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884728
    return-void
.end method
