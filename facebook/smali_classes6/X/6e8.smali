.class public LX/6e8;
.super LX/1qS;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6e8;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/6eB;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117522
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "messaging_emoji_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 1117523
    return-void
.end method

.method public static a(LX/0QB;)LX/6e8;
    .locals 7

    .prologue
    .line 1117524
    sget-object v0, LX/6e8;->a:LX/6e8;

    if-nez v0, :cond_1

    .line 1117525
    const-class v1, LX/6e8;

    monitor-enter v1

    .line 1117526
    :try_start_0
    sget-object v0, LX/6e8;->a:LX/6e8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1117527
    if-eqz v2, :cond_0

    .line 1117528
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1117529
    new-instance p0, LX/6e8;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    .line 1117530
    new-instance v6, LX/6eB;

    invoke-direct {v6}, LX/6eB;-><init>()V

    .line 1117531
    move-object v6, v6

    .line 1117532
    move-object v6, v6

    .line 1117533
    check-cast v6, LX/6eB;

    invoke-direct {p0, v3, v4, v5, v6}, LX/6e8;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/6eB;)V

    .line 1117534
    move-object v0, p0

    .line 1117535
    sput-object v0, LX/6e8;->a:LX/6e8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1117536
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1117537
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1117538
    :cond_1
    sget-object v0, LX/6e8;->a:LX/6e8;

    return-object v0

    .line 1117539
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1117540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 1117541
    invoke-virtual {p0}, LX/0Tr;->f()V

    .line 1117542
    return-void
.end method
