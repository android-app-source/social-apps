.class public final LX/68t;
.super Landroid/os/Handler;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1056810
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1056811
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1056812
    :cond_0
    :goto_0
    return-void

    .line 1056813
    :pswitch_0
    sget-object v0, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    sget-object v0, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    :cond_1
    move v1, v3

    .line 1056814
    :goto_1
    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1056815
    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_3

    .line 1056816
    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1056817
    iget-wide v6, v0, LX/68u;->v:J

    cmp-long v6, v6, v10

    if-nez v6, :cond_2

    .line 1056818
    invoke-static {v0}, LX/68u;->s(LX/68u;)V

    .line 1056819
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 1056820
    :cond_2
    sget-object v6, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1056821
    :cond_3
    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    :pswitch_1
    move v1, v2

    .line 1056822
    :cond_4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    .line 1056823
    sget-object v0, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_6

    .line 1056824
    sget-object v0, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1056825
    invoke-static {v0, v6, v7}, LX/68u;->c(LX/68u;J)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1056826
    sget-object v8, LX/68u;->g:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056827
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    .line 1056828
    :cond_6
    sget-object v0, LX/68u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1056829
    if-lez v0, :cond_8

    .line 1056830
    sget-object v0, LX/68u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_5
    if-ge v4, v5, :cond_7

    .line 1056831
    sget-object v0, LX/68u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1056832
    invoke-static {v0}, LX/68u;->s(LX/68u;)V

    .line 1056833
    iput-boolean v2, v0, LX/68u;->r:Z

    .line 1056834
    sget-object v8, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1056835
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    .line 1056836
    :cond_7
    sget-object v0, LX/68u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056837
    :cond_8
    sget-object v0, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v4, v3

    move v5, v0

    .line 1056838
    :goto_6
    if-ge v4, v5, :cond_b

    .line 1056839
    sget-object v0, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1056840
    invoke-static {v0, v6, v7}, LX/68u;->d(LX/68u;J)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1056841
    sget-object v8, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056842
    :cond_9
    sget-object v8, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v8, v5, :cond_a

    .line 1056843
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    .line 1056844
    :cond_a
    add-int/lit8 v5, v5, -0x1

    .line 1056845
    sget-object v8, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1056846
    :cond_b
    sget-object v0, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 1056847
    sget-object v0, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_7
    if-ge v3, v4, :cond_c

    .line 1056848
    sget-object v0, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    invoke-static {v0}, LX/68u;->r(LX/68u;)V

    .line 1056849
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1056850
    :cond_c
    sget-object v0, LX/68u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056851
    :cond_d
    if-eqz v1, :cond_0

    sget-object v0, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1056852
    :cond_e
    sget-wide v0, LX/68u;->w:J

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v6

    sub-long/2addr v0, v4

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, LX/68t;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_f
    move v1, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
