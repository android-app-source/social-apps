.class public final LX/6ZL;
.super LX/6ZK;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/android/gms/location/LocationRequest;

.field public final synthetic b:Landroid/app/PendingIntent;

.field public final synthetic c:LX/2Fw;


# direct methods
.method public constructor <init>(LX/2Fw;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1111072
    iput-object p1, p0, LX/6ZL;->c:LX/2Fw;

    iput-object p2, p0, LX/6ZL;->a:Lcom/google/android/gms/location/LocationRequest;

    iput-object p3, p0, LX/6ZL;->b:Landroid/app/PendingIntent;

    invoke-direct {p0}, LX/6ZK;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1111073
    iget-object v0, p0, LX/6ZL;->c:LX/2Fw;

    invoke-static {v0}, LX/2Fw;->a$redex0(LX/2Fw;)V

    .line 1111074
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1111075
    iget-object v0, p0, LX/6ZL;->c:LX/2Fw;

    .line 1111076
    iget-object v1, p0, LX/6ZK;->a:LX/2wX;

    move-object v1, v1

    .line 1111077
    iget-object v2, p0, LX/6ZL;->a:Lcom/google/android/gms/location/LocationRequest;

    iget-object v3, p0, LX/6ZL;->b:Landroid/app/PendingIntent;

    invoke-static {v0, v1, v2, v3, p0}, LX/2Fw;->a$redex0(LX/2Fw;LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;LX/6ZK;)V

    .line 1111078
    iget-object v0, p0, LX/6ZL;->c:LX/2Fw;

    .line 1111079
    iget-object v1, p0, LX/6ZK;->a:LX/2wX;

    move-object v1, v1

    .line 1111080
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111081
    :try_start_0
    invoke-virtual {v1}, LX/2wX;->g()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1111082
    :goto_0
    return-void

    .line 1111083
    :catch_0
    move-exception v2

    .line 1111084
    invoke-static {v2}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 1111085
    iget-object v3, v0, LX/2Fw;->c:LX/03V;

    const-string p1, "fb_location_continuous_listener_google_play"

    const-string p0, "Google exception on disconnect"

    invoke-virtual {v3, p1, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    .prologue
    .line 1111086
    iget-object v0, p0, LX/6ZL;->c:LX/2Fw;

    invoke-static {v0, p1}, LX/2Fw;->a$redex0(LX/2Fw;Lcom/google/android/gms/common/ConnectionResult;)V

    .line 1111087
    return-void
.end method
