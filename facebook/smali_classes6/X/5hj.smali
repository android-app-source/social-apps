.class public final LX/5hj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 979804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 979783
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 979784
    iget-object v1, p0, LX/5hj;->d:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 979785
    iget-object v3, p0, LX/5hj;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 979786
    iget-object v5, p0, LX/5hj;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 979787
    iget-object v6, p0, LX/5hj;->h:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 979788
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 979789
    iget-boolean v7, p0, LX/5hj;->a:Z

    invoke-virtual {v0, v9, v7}, LX/186;->a(IZ)V

    .line 979790
    iget-boolean v7, p0, LX/5hj;->b:Z

    invoke-virtual {v0, v4, v7}, LX/186;->a(IZ)V

    .line 979791
    const/4 v7, 0x2

    iget-boolean v8, p0, LX/5hj;->c:Z

    invoke-virtual {v0, v7, v8}, LX/186;->a(IZ)V

    .line 979792
    const/4 v7, 0x3

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 979793
    const/4 v1, 0x4

    iget-boolean v7, p0, LX/5hj;->e:Z

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 979794
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 979795
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 979796
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 979797
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 979798
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 979799
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 979800
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 979801
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 979802
    new-instance v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;-><init>(LX/15i;)V

    .line 979803
    return-object v1
.end method
