.class public LX/6Pk;
.super LX/6PU;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(LX/0Ot;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086346
    invoke-direct {p0, p2}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086347
    iput-object p1, p0, LX/6Pk;->a:LX/0Ot;

    .line 1086348
    iput-object p3, p0, LX/6Pk;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1086349
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 2

    .prologue
    .line 1086342
    iget-object v0, p0, LX/6Pk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    iget-object v1, p0, LX/6Pk;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1086343
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1086344
    check-cast v0, LX/16f;

    invoke-virtual {p2, v0}, LX/40T;->a(LX/16f;)V

    .line 1086345
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086341
    const-string v0, "UpdateEditedStoryMutatingVisitor"

    return-object v0
.end method
