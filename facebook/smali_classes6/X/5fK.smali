.class public final LX/5fK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:LX/5fS;

.field public final synthetic d:LX/5fQ;


# direct methods
.method public constructor <init>(LX/5fQ;IILX/5fS;)V
    .locals 0

    .prologue
    .line 971186
    iput-object p1, p0, LX/5fK;->d:LX/5fQ;

    iput p2, p0, LX/5fK;->a:I

    iput p3, p0, LX/5fK;->b:I

    iput-object p4, p0, LX/5fK;->c:LX/5fS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 5

    .prologue
    .line 971187
    iget-object v0, p0, LX/5fK;->d:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->r:LX/5fY;

    if-eqz v0, :cond_0

    .line 971188
    iget-object v0, p0, LX/5fK;->d:LX/5fQ;

    iget-object v1, v0, LX/5fQ;->r:LX/5fY;

    if-eqz p1, :cond_1

    sget-object v0, LX/5fd;->SUCCESS:LX/5fd;

    :goto_0
    new-instance v2, Landroid/graphics/Point;

    iget v3, p0, LX/5fK;->a:I

    iget v4, p0, LX/5fK;->b:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v1, v0, v2}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    .line 971189
    :cond_0
    iget-object v0, p0, LX/5fK;->d:LX/5fQ;

    new-instance v1, Lcom/facebook/optic/CameraDevice$9$1;

    invoke-direct {v1, p0}, Lcom/facebook/optic/CameraDevice$9$1;-><init>(LX/5fK;)V

    .line 971190
    iput-object v1, v0, LX/5fQ;->w:Ljava/lang/Runnable;

    .line 971191
    iget-object v0, p0, LX/5fK;->d:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->w:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    .line 971192
    sget-object v1, LX/5fo;->e:Landroid/os/Handler;

    const v4, -0x6f28fce3

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 971193
    return-void

    .line 971194
    :cond_1
    sget-object v0, LX/5fd;->FAILED:LX/5fd;

    goto :goto_0
.end method
