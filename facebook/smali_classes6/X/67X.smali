.class public LX/67X;
.super LX/0kI;
.source ""


# instance fields
.field private final a:LX/67T;

.field private b:LX/3u3;


# direct methods
.method public constructor <init>(LX/67T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1053071
    invoke-direct {p0}, LX/0kI;-><init>()V

    .line 1053072
    iput-object p1, p0, LX/67X;->a:LX/67T;

    .line 1053073
    return-void
.end method

.method public static b(LX/0QB;)LX/67X;
    .locals 3

    .prologue
    .line 1053066
    new-instance v1, LX/67X;

    .line 1053067
    new-instance v2, LX/67T;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v2, v0}, LX/67T;-><init>(LX/0Uh;)V

    .line 1053068
    move-object v0, v2

    .line 1053069
    check-cast v0, LX/67T;

    invoke-direct {v1, v0}, LX/67X;-><init>(LX/67T;)V

    .line 1053070
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053065
    iget-object v0, p0, LX/67X;->a:LX/67T;

    invoke-virtual {v0, p1, p2, p3}, LX/0T0;->a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;LX/0jn;ILandroid/view/MenuItem;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/0jn;",
            "I",
            "Landroid/view/MenuItem;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053046
    invoke-interface {p2, p3, p4}, LX/0jn;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1053047
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1053048
    :goto_0
    return-object v0

    .line 1053049
    :cond_0
    invoke-virtual {p0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 1053050
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/3u1;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 1053051
    invoke-static {p1}, LX/3pR;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1053052
    if-eqz v0, :cond_3

    .line 1053053
    invoke-static {p1, v0}, LX/3pR;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1053054
    invoke-static {p1}, LX/3qU;->a(Landroid/content/Context;)LX/3qU;

    move-result-object v0

    .line 1053055
    invoke-virtual {v0, p1}, LX/3qU;->a(Landroid/app/Activity;)LX/3qU;

    .line 1053056
    invoke-virtual {v0}, LX/3qU;->a()V

    .line 1053057
    :try_start_0
    invoke-static {p1}, LX/3p6;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1053058
    :goto_1
    const/4 v0, 0x1

    .line 1053059
    :goto_2
    move v0, v0

    .line 1053060
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 1053061
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 1053062
    :catch_0
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 1053063
    :cond_2
    invoke-static {p1, v0}, LX/3pR;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_1

    .line 1053064
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/app/Activity;LX/0jn;)V
    .locals 3

    .prologue
    .line 1053039
    new-instance v0, LX/67W;

    invoke-direct {v0, p0}, LX/67W;-><init>(LX/67X;)V

    .line 1053040
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 1053041
    new-instance v1, LX/3uA;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, LX/3uA;-><init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V

    .line 1053042
    :goto_0
    move-object v0, v1

    .line 1053043
    iput-object v0, p0, LX/67X;->b:LX/3u3;

    .line 1053044
    iget-object v0, p0, LX/67X;->a:LX/67T;

    invoke-virtual {v0, p1, p2}, LX/0kI;->a(Landroid/app/Activity;LX/0jn;)V

    .line 1053045
    return-void

    :cond_0
    new-instance v1, LX/3u8;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, LX/3u8;-><init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1053036
    instance-of v0, p1, Landroid/preference/PreferenceActivity;

    if-eqz v0, :cond_0

    .line 1053037
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p2}, LX/3u3;->a(Landroid/os/Bundle;)V

    .line 1053038
    :cond_0
    return-void
.end method

.method public final a(LX/0jn;)Z
    .locals 1

    .prologue
    .line 1053033
    invoke-interface {p1}, LX/0jn;->a()V

    .line 1053034
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->d()V

    .line 1053035
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1053074
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p1}, LX/3u3;->a(Landroid/view/View;)V

    .line 1053075
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1053031
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p1, p2}, LX/3u3;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1053032
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053030
    iget-object v0, p0, LX/67X;->a:LX/67T;

    invoke-virtual {v0, p1, p2, p3}, LX/0T0;->b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1053027
    instance-of v0, p1, Landroid/preference/PreferenceActivity;

    if-nez v0, :cond_0

    .line 1053028
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p2}, LX/3u3;->a(Landroid/os/Bundle;)V

    .line 1053029
    :cond_0
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 1053025
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p1}, LX/3u3;->a(I)V

    .line 1053026
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1053023
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0, p1, p2}, LX/3u3;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1053024
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 1053021
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->b()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/3u1;
    .locals 1

    .prologue
    .line 1053022
    iget-object v0, p0, LX/67X;->b:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->a()LX/3u1;

    move-result-object v0

    return-object v0
.end method
