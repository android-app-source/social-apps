.class public final LX/5ed;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:I

.field public Q:I

.field public R:D

.field public S:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:I

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:I

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Z

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:D

.field public ab:D

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:I

.field public af:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Z

.field public ai:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:I

.field public b:I

.field public c:J

.field public d:I

.field public e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:D

.field public o:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:I

.field public s:I

.field public t:I

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 968831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;
    .locals 30

    .prologue
    .line 968832
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 968833
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5ed;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 968834
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 968835
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->i:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 968836
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->k:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 968837
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 968838
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->o:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 968839
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->u:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 968840
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 968841
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 968842
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 968843
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 968844
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 968845
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->D:LX/0Px;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v19

    .line 968846
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->S:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 968847
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->U:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 968848
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->W:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 968849
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->X:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 968850
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->Y:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 968851
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->ac:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 968852
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->ad:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 968853
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->af:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 968854
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->ag:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 968855
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5ed;->ai:LX/0Px;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v29

    .line 968856
    const/16 v4, 0x3e

    invoke-virtual {v2, v4}, LX/186;->c(I)V

    .line 968857
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, LX/186;->b(II)V

    .line 968858
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->b:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968859
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->c:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 968860
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->d:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968861
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 968862
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968863
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->g:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968864
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->h:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968865
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 968866
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->j:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 968867
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 968868
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->l:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968869
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 968870
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->n:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 968871
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 968872
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->p:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968873
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968874
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->r:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968875
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->s:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968876
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->t:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968877
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 968878
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 968879
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 968880
    const/16 v3, 0x17

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968881
    const/16 v3, 0x18

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968882
    const/16 v3, 0x19

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968883
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->A:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968884
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->B:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968885
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->C:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968886
    const/16 v3, 0x1d

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968887
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->E:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968888
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->F:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968889
    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->G:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968890
    const/16 v3, 0x21

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->H:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968891
    const/16 v3, 0x22

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->I:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968892
    const/16 v3, 0x23

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->J:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968893
    const/16 v3, 0x24

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->K:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968894
    const/16 v3, 0x25

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->L:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968895
    const/16 v3, 0x26

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->M:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968896
    const/16 v3, 0x27

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->N:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968897
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->O:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968898
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->P:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968899
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->Q:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968900
    const/16 v3, 0x2b

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->R:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 968901
    const/16 v3, 0x2c

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968902
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->T:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968903
    const/16 v3, 0x2e

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968904
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->V:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968905
    const/16 v3, 0x30

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968906
    const/16 v3, 0x31

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968907
    const/16 v3, 0x32

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968908
    const/16 v3, 0x33

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->Z:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968909
    const/16 v3, 0x34

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->aa:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 968910
    const/16 v3, 0x35

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5ed;->ab:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 968911
    const/16 v3, 0x36

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968912
    const/16 v3, 0x37

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968913
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->ae:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968914
    const/16 v3, 0x39

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968915
    const/16 v3, 0x3a

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968916
    const/16 v3, 0x3b

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5ed;->ah:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 968917
    const/16 v3, 0x3c

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 968918
    const/16 v3, 0x3d

    move-object/from16 v0, p0

    iget v4, v0, LX/5ed;->aj:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 968919
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 968920
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 968921
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 968922
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 968923
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 968924
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;-><init>(LX/15i;)V

    .line 968925
    return-object v3
.end method
