.class public LX/6Iv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Landroid/os/Handler;

.field public d:Landroid/os/Handler;

.field private e:Landroid/os/HandlerThread;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075119
    const-class v0, LX/6Iv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Iv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1075120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075121
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/6Iv;->c:Landroid/os/Handler;

    .line 1075122
    iput-object p1, p0, LX/6Iv;->b:Ljava/lang/String;

    .line 1075123
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1075124
    iget-object v0, p0, LX/6Iv;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1075125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Background thread already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075126
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, LX/6Iv;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    .line 1075127
    iget-object v0, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1075128
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/6Iv;->d:Landroid/os/Handler;

    .line 1075129
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1075130
    iget-object v0, p0, LX/6Iv;->c:Landroid/os/Handler;

    const v1, -0x22417a18

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1075131
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1075132
    iget-object v0, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1075133
    :try_start_0
    iget-object v0, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075134
    iput-object v1, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    .line 1075135
    iput-object v1, p0, LX/6Iv;->d:Landroid/os/Handler;

    .line 1075136
    :goto_0
    return-void

    .line 1075137
    :catch_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1075138
    iput-object v1, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    .line 1075139
    iput-object v1, p0, LX/6Iv;->d:Landroid/os/Handler;

    goto :goto_0

    .line 1075140
    :catchall_0
    move-exception v0

    iput-object v1, p0, LX/6Iv;->e:Landroid/os/HandlerThread;

    .line 1075141
    iput-object v1, p0, LX/6Iv;->d:Landroid/os/Handler;

    throw v0
.end method

.method public final c()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 1075142
    iget-object v0, p0, LX/6Iv;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1075143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Background thread was not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075144
    :cond_0
    iget-object v0, p0, LX/6Iv;->d:Landroid/os/Handler;

    return-object v0
.end method
