.class public LX/54c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vR;


# instance fields
.field private final a:LX/0vR;

.field private final b:LX/52r;

.field private final c:J


# direct methods
.method public constructor <init>(LX/0vR;LX/52r;J)V
    .locals 1

    .prologue
    .line 828164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828165
    iput-object p1, p0, LX/54c;->a:LX/0vR;

    .line 828166
    iput-object p2, p0, LX/54c;->b:LX/52r;

    .line 828167
    iput-wide p3, p0, LX/54c;->c:J

    .line 828168
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 828169
    iget-object v0, p0, LX/54c;->b:LX/52r;

    invoke-virtual {v0}, LX/52r;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 828170
    :cond_0
    :goto_0
    return-void

    .line 828171
    :cond_1
    iget-wide v0, p0, LX/54c;->c:J

    invoke-static {}, LX/52r;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 828172
    iget-wide v0, p0, LX/54c;->c:J

    invoke-static {}, LX/52r;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 828173
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 828174
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828175
    :cond_2
    iget-object v0, p0, LX/54c;->b:LX/52r;

    invoke-virtual {v0}, LX/52r;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 828176
    iget-object v0, p0, LX/54c;->a:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V

    goto :goto_0

    .line 828177
    :catch_0
    move-exception v0

    .line 828178
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 828179
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
