.class public final LX/5YH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 941197
    const-wide/16 v12, 0x0

    .line 941198
    const/4 v10, 0x0

    .line 941199
    const/4 v9, 0x0

    .line 941200
    const/4 v8, 0x0

    .line 941201
    const/4 v5, 0x0

    .line 941202
    const-wide/16 v6, 0x0

    .line 941203
    const/4 v4, 0x0

    .line 941204
    const/4 v3, 0x0

    .line 941205
    const/4 v2, 0x0

    .line 941206
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v14, :cond_b

    .line 941207
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 941208
    const/4 v2, 0x0

    .line 941209
    :goto_0
    return v2

    .line 941210
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_7

    .line 941211
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 941212
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 941213
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 941214
    const-string v6, "end_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 941215
    const/4 v2, 0x1

    .line 941216
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 941217
    :cond_1
    const-string v6, "event_coordinates"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 941218
    invoke-static/range {p0 .. p1}, LX/5YF;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 941219
    :cond_2
    const-string v6, "event_place"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 941220
    invoke-static/range {p0 .. p1}, LX/5YG;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 941221
    :cond_3
    const-string v6, "event_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 941222
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 941223
    :cond_4
    const-string v6, "is_all_day"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 941224
    const/4 v2, 0x1

    .line 941225
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v12, v6

    goto :goto_1

    .line 941226
    :cond_5
    const-string v6, "start_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 941227
    const/4 v2, 0x1

    .line 941228
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto :goto_1

    .line 941229
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 941230
    :cond_7
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 941231
    if-eqz v3, :cond_8

    .line 941232
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 941233
    :cond_8
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 941234
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 941235
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 941236
    if-eqz v9, :cond_9

    .line 941237
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 941238
    :cond_9
    if-eqz v8, :cond_a

    .line 941239
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 941240
    :cond_a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v14, v9

    move v15, v10

    move-wide v10, v6

    move v9, v3

    move v3, v4

    move/from16 v16, v8

    move v8, v2

    move/from16 v17, v5

    move-wide v4, v12

    move/from16 v13, v16

    move/from16 v12, v17

    goto/16 :goto_1
.end method
