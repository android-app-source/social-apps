.class public LX/6a9;
.super Landroid/widget/ListView;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public a:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Intent;

.field public c:Landroid/os/Handler;

.field public d:Landroid/os/Handler;

.field public e:Z

.field public f:LX/6a8;

.field private g:Z

.field public final h:Landroid/content/Context;

.field public i:Ljava/lang/Double;

.field public j:Ljava/lang/Double;

.field public final k:Ljava/lang/Double;

.field public final l:Ljava/lang/Double;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private o:LX/6Zg;

.field private p:LX/6Zh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1112147
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 1112148
    const-class v0, LX/6a9;

    invoke-static {v0, p0}, LX/6a9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1112149
    iput-object p1, p0, LX/6a9;->h:Landroid/content/Context;

    .line 1112150
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/6a9;->k:Ljava/lang/Double;

    .line 1112151
    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/6a9;->l:Ljava/lang/Double;

    .line 1112152
    iput-object p7, p0, LX/6a9;->m:Ljava/lang/String;

    .line 1112153
    iput-object p8, p0, LX/6a9;->n:Ljava/lang/String;

    .line 1112154
    const/4 p3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1112155
    iput-object p2, p0, LX/6a9;->b:Landroid/content/Intent;

    .line 1112156
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p4

    invoke-direct {v0, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/6a9;->c:Landroid/os/Handler;

    .line 1112157
    new-instance v0, Landroid/os/HandlerThread;

    const-string p4, "background"

    invoke-direct {v0, p4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1112158
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1112159
    new-instance p4, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p4, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p4, p0, LX/6a9;->d:Landroid/os/Handler;

    .line 1112160
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1112161
    new-array p4, v1, [I

    const p5, 0x10100a7

    aput p5, p4, v2

    new-instance p5, Landroid/graphics/drawable/ColorDrawable;

    const/high16 p6, 0xc000000

    invoke-direct {p5, p6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p4, p5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1112162
    invoke-virtual {p0, v0}, LX/6a9;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1112163
    invoke-virtual {p0, v1}, LX/6a9;->setDrawSelectorOnTop(Z)V

    .line 1112164
    invoke-virtual {p0, v2}, LX/6a9;->setDividerHeight(I)V

    .line 1112165
    invoke-virtual {p0, p0}, LX/6a9;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1112166
    invoke-virtual {p0, v2}, LX/6a9;->setCacheColorHint(I)V

    .line 1112167
    invoke-virtual {p0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1112168
    float-to-int p4, v0

    mul-int/lit8 p4, p4, 0x10

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v2, p4, v2, v0}, LX/6a9;->setPadding(IIII)V

    .line 1112169
    new-instance p5, Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object p4, p0, LX/6a9;->b:Landroid/content/Intent;

    const/high16 p6, 0x10000

    invoke-virtual {v0, p4, p6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-direct {p5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1112170
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p6

    move p4, v2

    :goto_0
    if-ge p4, p6, :cond_3

    .line 1112171
    invoke-interface {p5, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 1112172
    const-string p7, "com.here.app.maps"

    iget-object p8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object p8, p8, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p7, p8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p7

    if-eqz p7, :cond_0

    .line 1112173
    invoke-interface {p5, p4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-object p4, v0

    .line 1112174
    :goto_1
    if-eqz p4, :cond_1

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/6a9;->e:Z

    .line 1112175
    new-instance v1, Lcom/facebook/maps/HereMapsUpsellView$1;

    invoke-direct {v1, p0}, Lcom/facebook/maps/HereMapsUpsellView$1;-><init>(LX/6a9;)V

    .line 1112176
    if-eqz p4, :cond_2

    iget-object v0, p4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    :goto_3
    iput-object v0, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1112177
    invoke-interface {p5, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1112178
    new-instance v0, LX/6a8;

    invoke-virtual {p0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p5}, LX/6a8;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, LX/6a9;->f:LX/6a8;

    .line 1112179
    iget-object v0, p0, LX/6a9;->f:LX/6a8;

    invoke-virtual {p0, v0}, LX/6a9;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1112180
    return-void

    .line 1112181
    :cond_0
    add-int/lit8 v0, p4, 0x1

    move p4, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1112182
    goto :goto_2

    :cond_2
    move-object v0, p3

    .line 1112183
    goto :goto_3

    :cond_3
    move-object p4, p3

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/6a9;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object p0

    check-cast p0, LX/0W9;

    iput-object p0, p1, LX/6a9;->a:LX/0W9;

    return-void
.end method

.method public static a$redex0(LX/6a9;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;II)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 1112086
    if-lez p5, :cond_0

    if-gtz p6, :cond_3

    .line 1112087
    :cond_0
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v0, :cond_1

    .line 1112088
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    const-string v1, "Width or height too small"

    invoke-virtual {v0, v1}, LX/6Zh;->a(Ljava/lang/String;)V

    .line 1112089
    :cond_1
    const/4 v0, 0x0

    .line 1112090
    :cond_2
    :goto_0
    return-object v0

    .line 1112091
    :cond_3
    if-eqz p3, :cond_4

    if-nez p4, :cond_6

    .line 1112092
    :cond_4
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v0, :cond_5

    .line 1112093
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    const-string v1, "No dest location"

    invoke-virtual {v0, v1}, LX/6Zh;->a(Ljava/lang/String;)V

    .line 1112094
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 1112095
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1112096
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 1112097
    sget-object v0, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v3, v0}, Ljava/text/NumberFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 1112098
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Ljava/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 1112099
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 1112100
    invoke-virtual {p0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1112101
    const/16 v1, 0x48

    if-gt v0, v1, :cond_8

    const/16 v0, 0x48

    .line 1112102
    :goto_1
    invoke-virtual {p0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40900000    # 4.5f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1112103
    if-eqz p1, :cond_b

    if-eqz p2, :cond_b

    .line 1112104
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "https://image.maps.api.here.com/mia/1.6/routing?app_id=caQ6Zhx3wcs0WZsbDu6A&app_code=quSjSoc_a3XZp8p_icIMmQ&t=0&ppi="

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "&w="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "&h="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "&lw="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lc=FF1652B4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&waypoint0="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3, p2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&waypoint1="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3, p3}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3, p4}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1112105
    const/4 v1, 0x0

    .line 1112106
    const/4 v6, 0x0

    .line 1112107
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1112108
    :try_start_1
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v1

    .line 1112109
    if-eqz v1, :cond_a

    .line 1112110
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1112111
    :goto_2
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 1112112
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Route"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v0, v2, v4, v5}, LX/6Zh;->a(Ljava/lang/String;J)V

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    .line 1112113
    :cond_8
    const/16 v1, 0xfa

    if-gt v0, v1, :cond_9

    const/16 v0, 0xfa

    goto/16 :goto_1

    :cond_9
    const/16 v0, 0x140

    goto/16 :goto_1

    .line 1112114
    :cond_a
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 1112115
    :goto_3
    iget-object v2, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v2, :cond_b

    if-eqz v1, :cond_b

    .line 1112116
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Route"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    invoke-virtual {v1, v2, v6, v7}, LX/6Zh;->a(Ljava/lang/String;J)V

    .line 1112117
    :cond_b
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://image.maps.api.here.com/mia/1.6/mapview?app_id=caQ6Zhx3wcs0WZsbDu6A&app_code=quSjSoc_a3XZp8p_icIMmQ&t=0&ppi="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&w="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&h="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&z=17"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&poix0="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, p3}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, p4}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";white;white;25;."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1112118
    const/4 v0, 0x0

    .line 1112119
    const/4 v2, 0x0

    .line 1112120
    :try_start_4
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 1112121
    :try_start_5
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v0

    .line 1112122
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 1112123
    :goto_5
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v1, :cond_2

    .line 1112124
    if-eqz v0, :cond_d

    .line 1112125
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Place"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v1, v2, v4, v5}, LX/6Zh;->a(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 1112126
    :catch_0
    :goto_6
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 1112127
    :goto_7
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v1, :cond_b

    if-eqz v6, :cond_b

    .line 1112128
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Route"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    invoke-virtual {v1, v2, v6, v7}, LX/6Zh;->a(Ljava/lang/String;J)V

    goto/16 :goto_4

    .line 1112129
    :catchall_0
    move-exception v0

    .line 1112130
    :goto_8
    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 1112131
    :goto_9
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v1, :cond_c

    if-eqz v6, :cond_c

    .line 1112132
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Route"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v1, v2, v4, v5}, LX/6Zh;->a(Ljava/lang/String;J)V

    :cond_c
    throw v0

    .line 1112133
    :cond_d
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Loading error"

    invoke-virtual {v1, v2}, LX/6Zh;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1112134
    :catch_1
    :goto_a
    :try_start_9
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 1112135
    :goto_b
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v0, :cond_e

    .line 1112136
    if-eqz v2, :cond_f

    .line 1112137
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    const-string v1, "Place"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/6Zh;->a(Ljava/lang/String;J)V

    .line 1112138
    :cond_e
    :goto_c
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, LX/6a9;->p:LX/6Zh;

    const-string v1, "Loading error"

    invoke-virtual {v0, v1}, LX/6Zh;->a(Ljava/lang/String;)V

    goto :goto_c

    .line 1112139
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 1112140
    :goto_d
    :try_start_a
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    .line 1112141
    :goto_e
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    if-eqz v1, :cond_10

    .line 1112142
    if-eqz v2, :cond_11

    .line 1112143
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Place"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v1, v2, v4, v5}, LX/6Zh;->a(Ljava/lang/String;J)V

    .line 1112144
    :cond_10
    :goto_f
    throw v0

    :cond_11
    iget-object v1, p0, LX/6a9;->p:LX/6Zh;

    const-string v2, "Loading error"

    invoke-virtual {v1, v2}, LX/6Zh;->a(Ljava/lang/String;)V

    goto :goto_f

    :catch_2
    goto/16 :goto_2

    :catch_3
    goto/16 :goto_3

    :catch_4
    goto :goto_7

    :catch_5
    goto :goto_9

    :catch_6
    goto/16 :goto_5

    :catch_7
    goto :goto_b

    :catch_8
    goto :goto_e

    .line 1112145
    :catchall_2
    move-exception v0

    goto :goto_d

    :catch_9
    move-object v0, v1

    goto :goto_a

    .line 1112146
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_8

    :catch_a
    move-object v1, v2

    goto/16 :goto_6
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1112081
    invoke-super {p0, p1}, Landroid/widget/ListView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1112082
    iget-boolean v0, p0, LX/6a9;->g:Z

    if-nez v0, :cond_0

    .line 1112083
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6a9;->g:Z

    .line 1112084
    iget-object v0, p0, LX/6a9;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/maps/HereMapsUpsellView$2;

    invoke-direct {v1, p0}, Lcom/facebook/maps/HereMapsUpsellView$2;-><init>(LX/6a9;)V

    const v2, -0x73138f73

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1112085
    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.content.Context.startActivity"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1112061
    iget-object v0, p0, LX/6a9;->f:LX/6a8;

    invoke-virtual {v0, p3}, LX/6a8;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 1112062
    if-nez p3, :cond_3

    iget-boolean v1, p0, LX/6a9;->e:Z

    if-nez v1, :cond_3

    .line 1112063
    const-string v0, "http://share.here.com/r/mylocation/e-"

    .line 1112064
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1112065
    const-string v3, "name"

    iget-object v1, p0, LX/6a9;->m:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, ""

    :goto_0
    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1112066
    const-string v1, "latitude"

    iget-object v3, p0, LX/6a9;->k:Ljava/lang/Double;

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1112067
    const-string v1, "longitude"

    iget-object v3, p0, LX/6a9;->l:Ljava/lang/Double;

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1112068
    iget-object v1, p0, LX/6a9;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1112069
    const-string v1, "address"

    iget-object v3, p0, LX/6a9;->n:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1112070
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1112071
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?ref=fb_android&fb_locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/6a9;->a:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1112072
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1112073
    iget-object v1, p0, LX/6a9;->o:LX/6Zg;

    if-eqz v1, :cond_1

    .line 1112074
    iget-object v1, p0, LX/6a9;->o:LX/6Zg;

    const-string v2, "HERE Web"

    invoke-virtual {v1, v2}, LX/6Zg;->a(Ljava/lang/String;)V

    .line 1112075
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/6a9;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1112076
    return-void

    .line 1112077
    :cond_2
    :try_start_1
    iget-object v1, p0, LX/6a9;->m:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1112078
    :cond_3
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/6a9;->b:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1112079
    iget-object v2, p0, LX/6a9;->o:LX/6Zg;

    if-eqz v2, :cond_4

    .line 1112080
    iget-object v2, p0, LX/6a9;->o:LX/6Zg;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "App: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/6Zg;->a(Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    goto :goto_2

    :catch_0
    goto :goto_1
.end method

.method public setOnIntentClickListener(LX/6Zg;)V
    .locals 0

    .prologue
    .line 1112059
    iput-object p1, p0, LX/6a9;->o:LX/6Zg;

    .line 1112060
    return-void
.end method

.method public setOnMapImageDownloadListener(LX/6Zh;)V
    .locals 0

    .prologue
    .line 1112057
    iput-object p1, p0, LX/6a9;->p:LX/6Zh;

    .line 1112058
    return-void
.end method
