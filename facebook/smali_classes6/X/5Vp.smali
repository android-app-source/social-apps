.class public interface abstract LX/5Vp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Vo;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Forwarder;
    processor = "com.facebook.dracula.transformer.Transformer"
    to = "ThreadInfo$"
.end annotation


# virtual methods
.method public abstract A()Z
.end method

.method public abstract B()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()Z
.end method

.method public abstract N()Z
.end method

.method public abstract O()Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract P()I
.end method

.method public abstract T()I
.end method

.method public abstract U()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract W()Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Y()Z
.end method

.method public abstract Z()I
.end method

.method public abstract aa()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ak()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract al()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ar()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract as()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract at()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()Z
.end method

.method public abstract q()Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()I
.end method

.method public abstract y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract z()D
.end method
