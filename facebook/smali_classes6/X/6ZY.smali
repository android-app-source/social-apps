.class public final enum LX/6ZY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ZY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ZY;

.field public static final enum DIALOG_CANCEL:LX/6ZY;

.field public static final enum DIALOG_NOT_NEEDED:LX/6ZY;

.field public static final enum DIALOG_NOT_POSSIBLE:LX/6ZY;

.field public static final enum DIALOG_SUCCESS:LX/6ZY;

.field public static final enum UNKNOWN_FAILURE:LX/6ZY;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1111228
    new-instance v0, LX/6ZY;

    const-string v1, "DIALOG_SUCCESS"

    invoke-direct {v0, v1, v2}, LX/6ZY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZY;->DIALOG_SUCCESS:LX/6ZY;

    .line 1111229
    new-instance v0, LX/6ZY;

    const-string v1, "DIALOG_CANCEL"

    invoke-direct {v0, v1, v3}, LX/6ZY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZY;->DIALOG_CANCEL:LX/6ZY;

    .line 1111230
    new-instance v0, LX/6ZY;

    const-string v1, "DIALOG_NOT_NEEDED"

    invoke-direct {v0, v1, v4}, LX/6ZY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZY;->DIALOG_NOT_NEEDED:LX/6ZY;

    .line 1111231
    new-instance v0, LX/6ZY;

    const-string v1, "DIALOG_NOT_POSSIBLE"

    invoke-direct {v0, v1, v5}, LX/6ZY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZY;->DIALOG_NOT_POSSIBLE:LX/6ZY;

    .line 1111232
    new-instance v0, LX/6ZY;

    const-string v1, "UNKNOWN_FAILURE"

    invoke-direct {v0, v1, v6}, LX/6ZY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZY;->UNKNOWN_FAILURE:LX/6ZY;

    .line 1111233
    const/4 v0, 0x5

    new-array v0, v0, [LX/6ZY;

    sget-object v1, LX/6ZY;->DIALOG_SUCCESS:LX/6ZY;

    aput-object v1, v0, v2

    sget-object v1, LX/6ZY;->DIALOG_CANCEL:LX/6ZY;

    aput-object v1, v0, v3

    sget-object v1, LX/6ZY;->DIALOG_NOT_NEEDED:LX/6ZY;

    aput-object v1, v0, v4

    sget-object v1, LX/6ZY;->DIALOG_NOT_POSSIBLE:LX/6ZY;

    aput-object v1, v0, v5

    sget-object v1, LX/6ZY;->UNKNOWN_FAILURE:LX/6ZY;

    aput-object v1, v0, v6

    sput-object v0, LX/6ZY;->$VALUES:[LX/6ZY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1111234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ZY;
    .locals 1

    .prologue
    .line 1111235
    const-class v0, LX/6ZY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ZY;

    return-object v0
.end method

.method public static values()[LX/6ZY;
    .locals 1

    .prologue
    .line 1111236
    sget-object v0, LX/6ZY;->$VALUES:[LX/6ZY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ZY;

    return-object v0
.end method
