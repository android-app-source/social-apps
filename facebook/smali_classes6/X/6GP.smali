.class public LX/6GP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/text/Collator;

.field private final b:LX/03R;


# direct methods
.method public constructor <init>(Ljava/text/Collator;LX/03R;)V
    .locals 0
    .param p2    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070447
    iput-object p1, p0, LX/6GP;->a:Ljava/text/Collator;

    .line 1070448
    iput-object p2, p0, LX/6GP;->b:LX/03R;

    .line 1070449
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1070450
    check-cast p1, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    check-cast p2, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070451
    iget-object v0, p0, LX/6GP;->a:Ljava/text/Collator;

    iget-object v1, p0, LX/6GP;->b:LX/03R;

    invoke-virtual {p1, v1}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(LX/03R;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/6GP;->b:LX/03R;

    invoke-virtual {p2, v2}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(LX/03R;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
