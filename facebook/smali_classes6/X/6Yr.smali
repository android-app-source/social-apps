.class public LX/6Yr;
.super LX/6Ya;
.source ""


# instance fields
.field private final c:LX/4g9;


# direct methods
.method public constructor <init>(LX/4g9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110669
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110670
    iput-object p1, p0, LX/6Yr;->c:LX/4g9;

    .line 1110671
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1110672
    iget-object v0, p0, LX/6Yr;->c:LX/4g9;

    sget-object v1, LX/4g6;->a:LX/4g6;

    invoke-virtual {v0, v1}, LX/4g9;->a(LX/4g5;)V

    .line 1110673
    invoke-virtual {p0}, LX/6Ya;->f()LX/6Y5;

    move-result-object v0

    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110674
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1110675
    iput-object v1, v0, LX/6Y5;->c:Ljava/lang/String;

    .line 1110676
    move-object v0, v0

    .line 1110677
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v2, 0x7f080e0b

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v1

    .line 1110678
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110679
    iget-object v2, v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v2, v2

    .line 1110680
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v3, 0x7f080e0c

    invoke-virtual {v0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1110681
    sget-object v3, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0yY;->DIALTONE_PHOTO_CAPPING:LX/0yY;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0yY;->DIALTONE_FEED_CAPPING:LX/0yY;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    if-ne v2, v3, :cond_1

    .line 1110682
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v2, 0x7f080e11

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1110683
    :cond_1
    invoke-virtual {p0}, LX/6Ya;->d()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    .line 1110684
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110685
    invoke-virtual {v0, v1}, LX/6YP;->a(LX/6Y5;)V

    .line 1110686
    return-object v0
.end method
