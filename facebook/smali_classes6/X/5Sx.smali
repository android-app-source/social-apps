.class public LX/5Sx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 919550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919551
    return-void
.end method

.method public static a(LX/5UU;)LX/5TX;
    .locals 3
    .param p0    # LX/5UU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 919420
    if-nez p0, :cond_0

    .line 919421
    const/4 v0, 0x0

    .line 919422
    :goto_0
    return-object v0

    .line 919423
    :cond_0
    new-instance v0, LX/5TX;

    invoke-direct {v0}, LX/5TX;-><init>()V

    .line 919424
    invoke-interface {p0}, LX/5UU;->c()Ljava/lang/String;

    move-result-object v1

    .line 919425
    iput-object v1, v0, LX/5TX;->b:Ljava/lang/String;

    .line 919426
    invoke-interface {p0}, LX/5UU;->cP_()Ljava/lang/String;

    move-result-object v1

    .line 919427
    iput-object v1, v0, LX/5TX;->c:Ljava/lang/String;

    .line 919428
    invoke-interface {p0}, LX/5UU;->cG_()Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    move-result-object v1

    .line 919429
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_RECEIPT:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_1

    .line 919430
    sget-object v2, LX/5TJ;->RECEIPT:LX/5TJ;

    .line 919431
    :goto_1
    move-object v1, v2

    .line 919432
    iput-object v1, v0, LX/5TX;->a:LX/5TJ;

    .line 919433
    invoke-interface {p0}, LX/5UU;->q()Ljava/lang/String;

    move-result-object v1

    .line 919434
    iput-object v1, v0, LX/5TX;->e:Ljava/lang/String;

    .line 919435
    invoke-interface {p0}, LX/5UU;->n()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v1

    invoke-static {v1}, LX/5Sx;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v1

    .line 919436
    iput-object v1, v0, LX/5TX;->f:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 919437
    goto :goto_0

    .line 919438
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_2

    .line 919439
    sget-object v2, LX/5TJ;->CANCELLATION:LX/5TJ;

    goto :goto_1

    .line 919440
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_3

    .line 919441
    sget-object v2, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    goto :goto_1

    .line 919442
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_4

    .line 919443
    sget-object v2, LX/5TJ;->SHIPMENT:LX/5TJ;

    goto :goto_1

    .line 919444
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_ETA:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_5

    .line 919445
    sget-object v2, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    goto :goto_1

    .line 919446
    :cond_5
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_TRACKING_EVENT_ETA:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_6

    .line 919447
    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    goto :goto_1

    .line 919448
    :cond_6
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_7

    .line 919449
    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    goto :goto_1

    .line 919450
    :cond_7
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_8

    .line 919451
    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    goto :goto_1

    .line 919452
    :cond_8
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_9

    .line 919453
    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    goto :goto_1

    .line 919454
    :cond_9
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_a

    .line 919455
    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    goto :goto_1

    .line 919456
    :cond_a
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_NOW_IN_STOCK:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_b

    .line 919457
    sget-object v2, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    goto :goto_1

    .line 919458
    :cond_b
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->FB_RETAIL_AGENT_ITEM_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    if-ne v1, v2, :cond_c

    .line 919459
    sget-object v2, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    goto :goto_1

    .line 919460
    :cond_c
    sget-object v2, LX/5TJ;->UNKNOWN:LX/5TJ;

    goto :goto_1
.end method

.method public static a(LX/5Uc;)Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .locals 2
    .param p0    # LX/5Uc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 919540
    if-nez p0, :cond_0

    .line 919541
    const/4 v0, 0x0

    .line 919542
    :goto_0
    return-object v0

    .line 919543
    :cond_0
    new-instance v0, LX/5Sq;

    invoke-direct {v0}, LX/5Sq;-><init>()V

    invoke-interface {p0}, LX/5Uc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5Sq;->a(Ljava/lang/String;)LX/5Sq;

    move-result-object v0

    invoke-interface {p0}, LX/5Uc;->c()I

    move-result v1

    .line 919544
    iput v1, v0, LX/5Sq;->b:I

    .line 919545
    move-object v0, v0

    .line 919546
    invoke-interface {p0}, LX/5Uc;->a()I

    move-result v1

    .line 919547
    iput v1, v0, LX/5Sq;->c:I

    .line 919548
    move-object v0, v0

    .line 919549
    invoke-virtual {v0}, LX/5Sq;->d()Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .locals 5
    .param p0    # Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 919515
    if-nez p0, :cond_0

    .line 919516
    const/4 v0, 0x0

    .line 919517
    :goto_0
    return-object v0

    .line 919518
    :cond_0
    new-instance v1, LX/5TQ;

    invoke-direct {v1}, LX/5TQ;-><init>()V

    .line 919519
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 919520
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 919521
    iput-object v0, v1, LX/5TQ;->a:Ljava/lang/String;

    .line 919522
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 919523
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cB_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 919524
    iput-object v0, v1, LX/5TQ;->b:Ljava/lang/String;

    .line 919525
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 919526
    iput-object v0, v1, LX/5TQ;->c:Ljava/lang/String;

    .line 919527
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->cA_()Ljava/lang/String;

    move-result-object v0

    .line 919528
    iput-object v0, v1, LX/5TQ;->d:Ljava/lang/String;

    .line 919529
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 919530
    iput-object v0, v1, LX/5TQ;->f:Ljava/lang/String;

    .line 919531
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 919532
    iput-object v0, v1, LX/5TQ;->e:Ljava/lang/String;

    .line 919533
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 919534
    iput-object v0, v1, LX/5TQ;->g:Ljava/lang/String;

    .line 919535
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->c()D

    move-result-wide v2

    .line 919536
    iput-wide v2, v1, LX/5TQ;->h:D

    .line 919537
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;->d()D

    move-result-wide v2

    .line 919538
    iput-wide v2, v1, LX/5TQ;->i:D

    .line 919539
    invoke-virtual {v1}, LX/5TQ;->j()Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/5Sz;)Lcom/facebook/messaging/business/commerce/model/retail/Shipment;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 919461
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 919462
    new-instance v3, LX/5TV;

    invoke-direct {v3}, LX/5TV;-><init>()V

    .line 919463
    invoke-interface {p0}, LX/5Sz;->c()Ljava/lang/String;

    move-result-object v0

    .line 919464
    iput-object v0, v3, LX/5TV;->a:Ljava/lang/String;

    .line 919465
    invoke-interface {p0}, LX/5Sz;->C()Ljava/lang/String;

    move-result-object v0

    .line 919466
    iput-object v0, v3, LX/5TV;->b:Ljava/lang/String;

    .line 919467
    invoke-interface {p0}, LX/5Sz;->I()Ljava/lang/String;

    move-result-object v0

    .line 919468
    iput-object v0, v3, LX/5TV;->c:Ljava/lang/String;

    .line 919469
    invoke-interface {p0}, LX/5Sz;->D()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    move-result-object v0

    .line 919470
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 919471
    new-instance v2, LX/5TT;

    invoke-direct {v2}, LX/5TT;-><init>()V

    .line 919472
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 919473
    iput-object v4, v2, LX/5TT;->a:Ljava/lang/String;

    .line 919474
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;->c()LX/5Uc;

    move-result-object v4

    invoke-static {v4}, LX/5Sx;->a(LX/5Uc;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v4

    .line 919475
    iput-object v4, v2, LX/5TT;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 919476
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;->b()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel$LegalTermsOfServiceTextModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 919477
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;->b()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel$LegalTermsOfServiceTextModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel$LegalTermsOfServiceTextModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 919478
    iput-object v4, v2, LX/5TT;->d:Ljava/lang/String;

    .line 919479
    :cond_0
    invoke-virtual {v2}, LX/5TT;->e()Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    move-result-object v2

    move-object v0, v2

    .line 919480
    iput-object v0, v3, LX/5TV;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    .line 919481
    invoke-interface {p0}, LX/5Sz;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/5TV;->d(Ljava/lang/String;)LX/5TV;

    .line 919482
    invoke-interface {p0}, LX/5Sz;->G()Ljava/lang/String;

    move-result-object v0

    .line 919483
    iput-object v0, v3, LX/5TV;->g:Ljava/lang/String;

    .line 919484
    invoke-interface {p0}, LX/5Sz;->z()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v0

    invoke-static {v0}, LX/5Sx;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    .line 919485
    iput-object v0, v3, LX/5TV;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 919486
    invoke-interface {p0}, LX/5Sz;->y()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v0

    invoke-static {v0}, LX/5Sx;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v0

    .line 919487
    iput-object v0, v3, LX/5TV;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 919488
    invoke-interface {p0}, LX/5Sz;->B()Ljava/lang/String;

    move-result-object v0

    .line 919489
    iput-object v0, v3, LX/5TV;->k:Ljava/lang/String;

    .line 919490
    invoke-interface {p0}, LX/5Sz;->A()Ljava/lang/String;

    move-result-object v0

    .line 919491
    iput-object v0, v3, LX/5TV;->m:Ljava/lang/String;

    .line 919492
    invoke-interface {p0}, LX/5Sz;->F()Ljava/lang/String;

    move-result-object v0

    .line 919493
    iput-object v0, v3, LX/5TV;->n:Ljava/lang/String;

    .line 919494
    invoke-interface {p0}, LX/5Sz;->D()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 919495
    invoke-interface {p0}, LX/5Sz;->D()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;->c()LX/5Uc;

    move-result-object v0

    invoke-static {v0}, LX/5Sx;->a(LX/5Uc;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v0

    .line 919496
    iput-object v0, v3, LX/5TV;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 919497
    :cond_1
    invoke-interface {p0}, LX/5Sz;->E()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 919498
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 919499
    invoke-interface {p0}, LX/5Sz;->E()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, LX/5Sz;->E()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 919500
    invoke-interface {p0}, LX/5Sz;->E()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    .line 919501
    :goto_0
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5UY;

    .line 919502
    invoke-static {v0}, LX/5Sa;->a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 919503
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 919504
    :cond_2
    iput-object v4, v3, LX/5TV;->p:Ljava/util/List;

    .line 919505
    :cond_3
    invoke-interface {p0}, LX/5Sz;->H()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p0}, LX/5Sz;->H()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 919506
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 919507
    invoke-interface {p0}, LX/5Sz;->H()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 919508
    :goto_1
    if-ge v1, v5, :cond_5

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5UU;

    .line 919509
    invoke-static {v0}, LX/5Sx;->a(LX/5UU;)LX/5TX;

    move-result-object v0

    .line 919510
    if-eqz v0, :cond_4

    .line 919511
    invoke-virtual {v0}, LX/5TX;->h()Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 919512
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 919513
    :cond_5
    iput-object v2, v3, LX/5TV;->q:Ljava/util/List;

    .line 919514
    :cond_6
    invoke-virtual {v3}, LX/5TV;->r()Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-result-object v0

    return-object v0
.end method
