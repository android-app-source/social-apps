.class public final LX/59H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 851475
    const/16 v26, 0x0

    .line 851476
    const/16 v25, 0x0

    .line 851477
    const/16 v24, 0x0

    .line 851478
    const/16 v23, 0x0

    .line 851479
    const/16 v22, 0x0

    .line 851480
    const/16 v21, 0x0

    .line 851481
    const/16 v20, 0x0

    .line 851482
    const/16 v17, 0x0

    .line 851483
    const-wide/16 v18, 0x0

    .line 851484
    const/16 v16, 0x0

    .line 851485
    const/4 v15, 0x0

    .line 851486
    const/4 v14, 0x0

    .line 851487
    const/4 v13, 0x0

    .line 851488
    const/4 v12, 0x0

    .line 851489
    const/4 v11, 0x0

    .line 851490
    const/4 v10, 0x0

    .line 851491
    const/4 v9, 0x0

    .line 851492
    const/4 v8, 0x0

    .line 851493
    const/4 v7, 0x0

    .line 851494
    const/4 v6, 0x0

    .line 851495
    const/4 v5, 0x0

    .line 851496
    const/4 v4, 0x0

    .line 851497
    const/4 v3, 0x0

    .line 851498
    const/4 v2, 0x0

    .line 851499
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1a

    .line 851500
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 851501
    const/4 v2, 0x0

    .line 851502
    :goto_0
    return v2

    .line 851503
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    if-eq v2, v0, :cond_11

    .line 851504
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 851505
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 851506
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 851507
    const-string v28, "author"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 851508
    invoke-static/range {p0 .. p1}, LX/59B;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 851509
    :cond_1
    const-string v28, "body"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 851510
    invoke-static/range {p0 .. p1}, LX/59F;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 851511
    :cond_2
    const-string v28, "body_markdown_html"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 851512
    invoke-static/range {p0 .. p1}, LX/59C;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 851513
    :cond_3
    const-string v28, "can_edit_constituent_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 851514
    const/4 v2, 0x1

    .line 851515
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v24, v12

    move v12, v2

    goto :goto_1

    .line 851516
    :cond_4
    const-string v28, "can_viewer_delete"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 851517
    const/4 v2, 0x1

    .line 851518
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v23, v11

    move v11, v2

    goto :goto_1

    .line 851519
    :cond_5
    const-string v28, "can_viewer_edit"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 851520
    const/4 v2, 0x1

    .line 851521
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto/16 :goto_1

    .line 851522
    :cond_6
    const-string v28, "can_viewer_share"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 851523
    const/4 v2, 0x1

    .line 851524
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v21, v6

    move v6, v2

    goto/16 :goto_1

    .line 851525
    :cond_7
    const-string v28, "constituent_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 851526
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 851527
    :cond_8
    const-string v28, "created_time"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 851528
    const/4 v2, 0x1

    .line 851529
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 851530
    :cond_9
    const-string v28, "edit_history"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 851531
    invoke-static/range {p0 .. p1}, LX/59G;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 851532
    :cond_a
    const-string v28, "id"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 851533
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 851534
    :cond_b
    const-string v28, "is_featured"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 851535
    const/4 v2, 0x1

    .line 851536
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v17, v10

    move v10, v2

    goto/16 :goto_1

    .line 851537
    :cond_c
    const-string v28, "is_marked_as_spam"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 851538
    const/4 v2, 0x1

    .line 851539
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v16, v9

    move v9, v2

    goto/16 :goto_1

    .line 851540
    :cond_d
    const-string v28, "is_pinned"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 851541
    const/4 v2, 0x1

    .line 851542
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v15, v8

    move v8, v2

    goto/16 :goto_1

    .line 851543
    :cond_e
    const-string v28, "spam_display_mode"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 851544
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 851545
    :cond_f
    const-string v28, "translatability_for_viewer"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 851546
    invoke-static/range {p0 .. p1}, LX/59K;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 851547
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 851548
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 851549
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851550
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851551
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851552
    if-eqz v12, :cond_12

    .line 851553
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851554
    :cond_12
    if-eqz v11, :cond_13

    .line 851555
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851556
    :cond_13
    if-eqz v7, :cond_14

    .line 851557
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851558
    :cond_14
    if-eqz v6, :cond_15

    .line 851559
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851560
    :cond_15
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851561
    if-eqz v3, :cond_16

    .line 851562
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 851563
    :cond_16
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851564
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851565
    if-eqz v10, :cond_17

    .line 851566
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851567
    :cond_17
    if-eqz v9, :cond_18

    .line 851568
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851569
    :cond_18
    if-eqz v8, :cond_19

    .line 851570
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 851571
    :cond_19
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 851572
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 851573
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1a
    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v11

    move v11, v8

    move v8, v2

    move/from16 v30, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v31, v18

    move/from16 v19, v30

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v31

    goto/16 :goto_1
.end method
