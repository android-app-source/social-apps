.class public final LX/5j6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 985628
    const/16 v18, 0x0

    .line 985629
    const/16 v17, 0x0

    .line 985630
    const/16 v16, 0x0

    .line 985631
    const-wide/16 v14, 0x0

    .line 985632
    const/4 v13, 0x0

    .line 985633
    const/4 v12, 0x0

    .line 985634
    const/4 v11, 0x0

    .line 985635
    const/4 v10, 0x0

    .line 985636
    const/4 v9, 0x0

    .line 985637
    const/4 v8, 0x0

    .line 985638
    const-wide/16 v6, 0x0

    .line 985639
    const/4 v5, 0x0

    .line 985640
    const/4 v4, 0x0

    .line 985641
    const/4 v3, 0x0

    .line 985642
    const/4 v2, 0x0

    .line 985643
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 985644
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 985645
    const/4 v2, 0x0

    .line 985646
    :goto_0
    return v2

    .line 985647
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 985648
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 985649
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 985650
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 985651
    const-string v6, "attribution_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 985652
    invoke-static/range {p0 .. p1}, LX/5im;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 985653
    :cond_1
    const-string v6, "attribution_thumbnail"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 985654
    invoke-static/range {p0 .. p1}, LX/5in;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 985655
    :cond_2
    const-string v6, "creative_filter"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 985656
    invoke-static/range {p0 .. p1}, LX/5io;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 985657
    :cond_3
    const-string v6, "end_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 985658
    const/4 v2, 0x1

    .line 985659
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 985660
    :cond_4
    const-string v6, "frame_image_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 985661
    invoke-static/range {p0 .. p1}, LX/5iv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 985662
    :cond_5
    const-string v6, "frame_text_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 985663
    invoke-static/range {p0 .. p1}, LX/5j4;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 985664
    :cond_6
    const-string v6, "has_location_constraints"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 985665
    const/4 v2, 0x1

    .line 985666
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v16, v6

    goto/16 :goto_1

    .line 985667
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 985668
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 985669
    :cond_8
    const-string v6, "instructions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 985670
    invoke-static/range {p0 .. p1}, LX/5j5;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 985671
    :cond_9
    const-string v6, "is_logging_disabled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 985672
    const/4 v2, 0x1

    .line 985673
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v11, v6

    goto/16 :goto_1

    .line 985674
    :cond_a
    const-string v6, "start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 985675
    const/4 v2, 0x1

    .line 985676
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto/16 :goto_1

    .line 985677
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 985678
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 985679
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 985680
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 985681
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 985682
    if-eqz v3, :cond_d

    .line 985683
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 985684
    :cond_d
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 985685
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 985686
    if-eqz v10, :cond_e

    .line 985687
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 985688
    :cond_e
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 985689
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 985690
    if-eqz v9, :cond_f

    .line 985691
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 985692
    :cond_f
    if-eqz v8, :cond_10

    .line 985693
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 985694
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v19, v16

    move/from16 v20, v17

    move/from16 v21, v18

    move/from16 v16, v11

    move/from16 v17, v12

    move/from16 v18, v13

    move-wide v12, v6

    move v11, v8

    move v8, v2

    move/from16 v22, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v23, v14

    move v15, v10

    move/from16 v14, v22

    move v10, v4

    move-wide/from16 v4, v23

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 985695
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985696
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985697
    if-eqz v0, :cond_0

    .line 985698
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985699
    invoke-static {p0, v0, p2}, LX/5im;->a(LX/15i;ILX/0nX;)V

    .line 985700
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985701
    if-eqz v0, :cond_1

    .line 985702
    const-string v1, "attribution_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985703
    invoke-static {p0, v0, p2}, LX/5in;->a(LX/15i;ILX/0nX;)V

    .line 985704
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985705
    if-eqz v0, :cond_2

    .line 985706
    const-string v1, "creative_filter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985707
    invoke-static {p0, v0, p2}, LX/5io;->a(LX/15i;ILX/0nX;)V

    .line 985708
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 985709
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 985710
    const-string v2, "end_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985711
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 985712
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985713
    if-eqz v0, :cond_4

    .line 985714
    const-string v1, "frame_image_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985715
    invoke-static {p0, v0, p2, p3}, LX/5iv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 985716
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985717
    if-eqz v0, :cond_5

    .line 985718
    const-string v1, "frame_text_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985719
    invoke-static {p0, v0, p2, p3}, LX/5j4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 985720
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 985721
    if-eqz v0, :cond_6

    .line 985722
    const-string v1, "has_location_constraints"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985723
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 985724
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 985725
    if-eqz v0, :cond_7

    .line 985726
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985727
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985728
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985729
    if-eqz v0, :cond_8

    .line 985730
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985731
    invoke-static {p0, v0, p2}, LX/5j5;->a(LX/15i;ILX/0nX;)V

    .line 985732
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 985733
    if-eqz v0, :cond_9

    .line 985734
    const-string v1, "is_logging_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985735
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 985736
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 985737
    cmp-long v2, v0, v4

    if-eqz v2, :cond_a

    .line 985738
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985739
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 985740
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985741
    return-void
.end method
