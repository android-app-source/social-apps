.class public LX/64I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/12Q;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yN;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yi;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1044458
    const-class v0, LX/64I;

    sput-object v0, LX/64I;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/12Q;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/12Q;",
            "LX/0Ot",
            "<",
            "LX/0yN;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yi;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044449
    iput-object p1, p0, LX/64I;->b:LX/12Q;

    .line 1044450
    iput-object p2, p0, LX/64I;->c:LX/0Ot;

    .line 1044451
    iput-object p3, p0, LX/64I;->d:LX/0Ot;

    .line 1044452
    iput-object p4, p0, LX/64I;->e:LX/0Ot;

    .line 1044453
    iput-object p5, p0, LX/64I;->f:LX/0Or;

    .line 1044454
    iput-object p6, p0, LX/64I;->g:LX/0Or;

    .line 1044455
    iput-object p7, p0, LX/64I;->h:LX/0Ot;

    .line 1044456
    iput-object p8, p0, LX/64I;->i:LX/0Ot;

    .line 1044457
    return-void
.end method
