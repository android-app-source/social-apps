.class public final enum LX/5dX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5dX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5dX;

.field public static final enum QUICKCAM:LX/5dX;

.field public static final enum VIDEO_ATTACHMENT:LX/5dX;

.field public static final enum VIDEO_MAIL:LX/5dX;

.field public static final enum VIDEO_STICKER:LX/5dX;


# instance fields
.field public final apiStringValue:Ljava/lang/String;

.field public final intValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 965771
    new-instance v0, LX/5dX;

    const-string v1, "VIDEO_ATTACHMENT"

    const-string v2, "FILE_ATTACHMENT"

    invoke-direct {v0, v1, v6, v4, v2}, LX/5dX;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    .line 965772
    new-instance v0, LX/5dX;

    const-string v1, "QUICKCAM"

    const-string v2, "RECORDED_VIDEO"

    invoke-direct {v0, v1, v4, v5, v2}, LX/5dX;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dX;->QUICKCAM:LX/5dX;

    .line 965773
    new-instance v0, LX/5dX;

    const-string v1, "VIDEO_STICKER"

    const-string v2, "RECORDED_STICKER"

    invoke-direct {v0, v1, v5, v8, v2}, LX/5dX;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dX;->VIDEO_STICKER:LX/5dX;

    .line 965774
    new-instance v0, LX/5dX;

    const-string v1, "VIDEO_MAIL"

    const/4 v2, 0x5

    const-string v3, "VIDEO_MAIL"

    invoke-direct {v0, v1, v7, v2, v3}, LX/5dX;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dX;->VIDEO_MAIL:LX/5dX;

    .line 965775
    new-array v0, v8, [LX/5dX;

    sget-object v1, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    aput-object v1, v0, v6

    sget-object v1, LX/5dX;->QUICKCAM:LX/5dX;

    aput-object v1, v0, v4

    sget-object v1, LX/5dX;->VIDEO_STICKER:LX/5dX;

    aput-object v1, v0, v5

    sget-object v1, LX/5dX;->VIDEO_MAIL:LX/5dX;

    aput-object v1, v0, v7

    sput-object v0, LX/5dX;->$VALUES:[LX/5dX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 965776
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 965777
    iput p3, p0, LX/5dX;->intValue:I

    .line 965778
    iput-object p4, p0, LX/5dX;->apiStringValue:Ljava/lang/String;

    .line 965779
    return-void
.end method

.method public static fromIntVal(I)LX/5dX;
    .locals 1

    .prologue
    .line 965780
    sget-object v0, LX/5dX;->QUICKCAM:LX/5dX;

    iget v0, v0, LX/5dX;->intValue:I

    if-ne p0, v0, :cond_0

    .line 965781
    sget-object v0, LX/5dX;->QUICKCAM:LX/5dX;

    .line 965782
    :goto_0
    return-object v0

    .line 965783
    :cond_0
    sget-object v0, LX/5dX;->VIDEO_STICKER:LX/5dX;

    iget v0, v0, LX/5dX;->intValue:I

    if-ne p0, v0, :cond_1

    .line 965784
    sget-object v0, LX/5dX;->VIDEO_STICKER:LX/5dX;

    goto :goto_0

    .line 965785
    :cond_1
    sget-object v0, LX/5dX;->VIDEO_MAIL:LX/5dX;

    iget v0, v0, LX/5dX;->intValue:I

    if-ne p0, v0, :cond_2

    .line 965786
    sget-object v0, LX/5dX;->VIDEO_MAIL:LX/5dX;

    goto :goto_0

    .line 965787
    :cond_2
    sget-object v0, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5dX;
    .locals 1

    .prologue
    .line 965788
    const-class v0, LX/5dX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5dX;

    return-object v0
.end method

.method public static values()[LX/5dX;
    .locals 1

    .prologue
    .line 965789
    sget-object v0, LX/5dX;->$VALUES:[LX/5dX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5dX;

    return-object v0
.end method
