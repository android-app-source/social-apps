.class public final LX/5QL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 913006
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 913007
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913008
    :goto_0
    return v1

    .line 913009
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913010
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 913011
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 913012
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913013
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 913014
    const-string v5, "greeting_card_template"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 913015
    invoke-static {p0, p1}, LX/5QJ;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 913016
    :cond_2
    const-string v5, "slides"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 913017
    const/4 v4, 0x0

    .line 913018
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_a

    .line 913019
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913020
    :goto_2
    move v2, v4

    .line 913021
    goto :goto_1

    .line 913022
    :cond_3
    const-string v5, "theme"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 913023
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 913024
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 913025
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 913026
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 913027
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 913028
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 913029
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913030
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 913031
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 913032
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913033
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 913034
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 913035
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 913036
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_8

    .line 913037
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_8

    .line 913038
    const/4 v6, 0x0

    .line 913039
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_11

    .line 913040
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913041
    :goto_5
    move v5, v6

    .line 913042
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 913043
    :cond_8
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 913044
    goto :goto_3

    .line 913045
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 913046
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 913047
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    move v2, v4

    goto :goto_3

    .line 913048
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913049
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_10

    .line 913050
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 913051
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913052
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_c

    if-eqz v10, :cond_c

    .line 913053
    const-string v11, "message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 913054
    const/4 v10, 0x0

    .line 913055
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v11, :cond_15

    .line 913056
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913057
    :goto_7
    move v9, v10

    .line 913058
    goto :goto_6

    .line 913059
    :cond_d
    const-string v11, "photos"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 913060
    const/4 v10, 0x0

    .line 913061
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v11, :cond_1a

    .line 913062
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913063
    :goto_8
    move v8, v10

    .line 913064
    goto :goto_6

    .line 913065
    :cond_e
    const-string v11, "slide_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 913066
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_6

    .line 913067
    :cond_f
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 913068
    const/4 v10, 0x0

    .line 913069
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v11, :cond_1e

    .line 913070
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913071
    :goto_9
    move v5, v10

    .line 913072
    goto :goto_6

    .line 913073
    :cond_10
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 913074
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 913075
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 913076
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 913077
    const/4 v6, 0x3

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 913078
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_11
    move v5, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto/16 :goto_6

    .line 913079
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913080
    :cond_13
    :goto_a
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_14

    .line 913081
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 913082
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_13

    if-eqz v11, :cond_13

    .line 913084
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 913085
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_a

    .line 913086
    :cond_14
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 913087
    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 913088
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_7

    :cond_15
    move v9, v10

    goto :goto_a

    .line 913089
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913090
    :cond_17
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_19

    .line 913091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 913092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_17

    if-eqz v11, :cond_17

    .line 913094
    const-string v12, "nodes"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 913095
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 913096
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_18

    .line 913097
    :goto_c
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_18

    .line 913098
    invoke-static {p0, p1}, LX/5QK;->b(LX/15w;LX/186;)I

    move-result v11

    .line 913099
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 913100
    :cond_18
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 913101
    goto :goto_b

    .line 913102
    :cond_19
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 913103
    invoke-virtual {p1, v10, v8}, LX/186;->b(II)V

    .line 913104
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_8

    :cond_1a
    move v8, v10

    goto :goto_b

    .line 913105
    :cond_1b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 913106
    :cond_1c
    :goto_d
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1d

    .line 913107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 913108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 913109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1c

    if-eqz v11, :cond_1c

    .line 913110
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1b

    .line 913111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_d

    .line 913112
    :cond_1d
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 913113
    invoke-virtual {p1, v10, v5}, LX/186;->b(II)V

    .line 913114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_9

    :cond_1e
    move v5, v10

    goto :goto_d
.end method
