.class public final LX/5dk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5di",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;
    .locals 5

    .prologue
    .line 966040
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;

    const-string v1, "confidence"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->f(LX/0lF;)F

    move-result v1

    const-string v2, "method"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "provider"

    invoke-virtual {p1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "to_location"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;-><init>(FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966039
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966038
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;

    return-object v0
.end method
