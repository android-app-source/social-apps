.class public LX/6WG;
.super Lcom/facebook/resources/ui/FbCheckBox;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1105041
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;)V

    .line 1105042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1105043
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105044
    return-void
.end method


# virtual methods
.method public setAllCaps(Z)V
    .locals 2

    .prologue
    .line 1105045
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105046
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setAllCaps"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105047
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setAllCaps(Z)V

    .line 1105048
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105049
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105050
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackground"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105051
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1105052
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 1105053
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105054
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105055
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackgroundColor(I)V

    .line 1105056
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105057
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105058
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundDrawable"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105059
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1105060
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2

    .prologue
    .line 1105061
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105062
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundResource"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105063
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackgroundResource(I)V

    .line 1105064
    return-void
.end method

.method public setBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 1105065
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105066
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundTintList"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105067
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 1105068
    return-void
.end method

.method public setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 1105069
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105070
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundTintMode"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105071
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 1105072
    return-void
.end method

.method public setButtonDrawable(I)V
    .locals 2

    .prologue
    .line 1105073
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105074
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setButtonDrawable"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105075
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setButtonDrawable(I)V

    .line 1105076
    return-void
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105081
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105082
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setButtonDrawable"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105083
    :cond_0
    return-void
.end method

.method public setCompoundDrawablePadding(I)V
    .locals 2

    .prologue
    .line 1105077
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105078
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablePadding"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105079
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablePadding(I)V

    .line 1105080
    return-void
.end method

.method public setCompoundDrawableTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 1105116
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105117
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawableTintList"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105118
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawableTintList(Landroid/content/res/ColorStateList;)V

    .line 1105119
    return-void
.end method

.method public setCompoundDrawableTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 1105112
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105113
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawableTintMode"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105114
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawableTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 1105115
    return-void
.end method

.method public final setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105108
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawables"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105110
    :cond_0
    invoke-super {p0, p1, p2, p3, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1105111
    return-void
.end method

.method public final setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105104
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105105
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesRelative"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105106
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1105107
    return-void
.end method

.method public final setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V
    .locals 2

    .prologue
    .line 1105100
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesRelativeWithIntrinsicBounds"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105102
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 1105103
    return-void
.end method

.method public final setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105096
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105097
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesRelativeWithIntrinsicBounds"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105098
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1105099
    return-void
.end method

.method public final setCompoundDrawablesWithIntrinsicBounds(IIII)V
    .locals 2

    .prologue
    .line 1105092
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105093
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesWithIntrinsicBounds"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105094
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1105095
    return-void
.end method

.method public final setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1105088
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105089
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesWithIntrinsicBounds"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105090
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1105091
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 2

    .prologue
    .line 1105084
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105085
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCursorVisible"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105086
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setCursorVisible(Z)V

    .line 1105087
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 2

    .prologue
    .line 1105033
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105034
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setEllipsize"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105035
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1105036
    return-void
.end method

.method public setEms(I)V
    .locals 2

    .prologue
    .line 1105037
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105038
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setEms"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105039
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setEms(I)V

    .line 1105040
    return-void
.end method

.method public setGravity(I)V
    .locals 2

    .prologue
    .line 1104984
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104985
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setGravity"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104986
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setGravity(I)V

    .line 1104987
    return-void
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 1104948
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104949
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104950
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setHeight(I)V

    .line 1104951
    return-void
.end method

.method public setHighlightColor(I)V
    .locals 2

    .prologue
    .line 1104952
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104953
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setHighlightColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104954
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setHighlightColor(I)V

    .line 1104955
    return-void
.end method

.method public setIncludeFontPadding(Z)V
    .locals 2

    .prologue
    .line 1104956
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104957
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setIncludeFontPadding"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104958
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setIncludeFontPadding(Z)V

    .line 1104959
    return-void
.end method

.method public setLines(I)V
    .locals 2

    .prologue
    .line 1104960
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104961
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104962
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setLines(I)V

    .line 1104963
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 2

    .prologue
    .line 1104964
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104965
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104966
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMaxHeight(I)V

    .line 1104967
    return-void
.end method

.method public setMaxLines(I)V
    .locals 2

    .prologue
    .line 1104968
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104969
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104970
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMaxLines(I)V

    .line 1104971
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 2

    .prologue
    .line 1104972
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104973
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104974
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMaxWidth(I)V

    .line 1104975
    return-void
.end method

.method public setMinHeight(I)V
    .locals 2

    .prologue
    .line 1104976
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104977
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104978
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMinHeight(I)V

    .line 1104979
    return-void
.end method

.method public setMinLines(I)V
    .locals 2

    .prologue
    .line 1104980
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104981
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104982
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMinLines(I)V

    .line 1104983
    return-void
.end method

.method public setMinWidth(I)V
    .locals 2

    .prologue
    .line 1104988
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104989
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104990
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setMinWidth(I)V

    .line 1104991
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 1

    .prologue
    .line 1104992
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-nez v0, :cond_0

    .line 1104993
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbCheckBox;->setPadding(IIII)V

    .line 1104994
    :cond_0
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 1104946
    const-string v0, ""

    invoke-super {p0, v0, p2}, Lcom/facebook/resources/ui/FbCheckBox;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1104947
    return-void
.end method

.method public setTextAppearance(I)V
    .locals 2

    .prologue
    .line 1104995
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1104996
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextAppearance"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104997
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTextAppearance(I)V

    .line 1104998
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 1104999
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105000
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextAppearance"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105001
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckBox;->setTextAppearance(Landroid/content/Context;I)V

    .line 1105002
    return-void
.end method

.method public setTextColor(I)V
    .locals 2

    .prologue
    .line 1105003
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105004
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105005
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTextColor(I)V

    .line 1105006
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 1105007
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105008
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105009
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1105010
    return-void
.end method

.method public setTextScaleX(F)V
    .locals 2

    .prologue
    .line 1105011
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105012
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextScaleX"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105013
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTextScaleX(F)V

    .line 1105014
    return-void
.end method

.method public setTextSize(F)V
    .locals 2

    .prologue
    .line 1105015
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105016
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextSize"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105017
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTextSize(F)V

    .line 1105018
    return-void
.end method

.method public final setTextSize(IF)V
    .locals 2

    .prologue
    .line 1105019
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105020
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextSize"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105021
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckBox;->setTextSize(IF)V

    .line 1105022
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1105023
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-nez v0, :cond_0

    .line 1105024
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1105025
    :cond_0
    return-void
.end method

.method public final setTypeface(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 1105026
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-nez v0, :cond_0

    .line 1105027
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckBox;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1105028
    :cond_0
    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1105029
    iget-boolean v0, p0, LX/6WG;->a:Z

    if-eqz v0, :cond_0

    .line 1105030
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1105031
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setWidth(I)V

    .line 1105032
    return-void
.end method
