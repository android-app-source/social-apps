.class public LX/6DU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:LX/6Cz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/6D0;

.field public final e:LX/1Bf;

.field private final f:LX/6DM;

.field private final g:LX/0Uh;

.field private final h:Ljava/util/concurrent/Executor;

.field public final i:LX/03V;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065176
    const-class v0, LX/6DU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6DU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Bf;LX/6DM;LX/0Uh;Ljava/util/concurrent/Executor;LX/6D0;LX/03V;LX/0Or;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1Bf;",
            "LX/6DM;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/util/concurrent/Executor;",
            "LX/6D0;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1065177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065178
    iput-object p1, p0, LX/6DU;->b:Landroid/content/Context;

    .line 1065179
    iput-object p2, p0, LX/6DU;->e:LX/1Bf;

    .line 1065180
    iput-object p3, p0, LX/6DU;->f:LX/6DM;

    .line 1065181
    iput-object p4, p0, LX/6DU;->g:LX/0Uh;

    .line 1065182
    iput-object p5, p0, LX/6DU;->h:Ljava/util/concurrent/Executor;

    .line 1065183
    iput-object p7, p0, LX/6DU;->i:LX/03V;

    .line 1065184
    iput-object p6, p0, LX/6DU;->d:LX/6D0;

    .line 1065185
    iput-object p8, p0, LX/6DU;->j:LX/0Or;

    .line 1065186
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065187
    const-string v0, "requestAutoFill"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 8

    .prologue
    .line 1065188
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

    .line 1065189
    iget-object v0, p0, LX/6DU;->g:LX/0Uh;

    const/16 v1, 0x5c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1065190
    if-nez v0, :cond_1

    .line 1065191
    :cond_0
    :goto_0
    return-void

    .line 1065192
    :cond_1
    const/4 v2, 0x0

    .line 1065193
    const-string v0, "autofillFields"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1065194
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1065195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 1065196
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 1065197
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1065198
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1065199
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065200
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1065201
    :catch_0
    const-string v0, "requestAutoFill"

    const-string v1, "Exception deserializing call params!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1065202
    const/4 v0, 0x0

    :cond_3
    move-object v0, v0

    .line 1065203
    const-string v1, "selectedAutoCompleteTag"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1065204
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1065205
    iget-object v2, p0, LX/6DU;->d:LX/6D0;

    .line 1065206
    iget-object v3, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v3, v3

    .line 1065207
    invoke-virtual {v2, v3}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v2

    iput-object v2, p0, LX/6DU;->c:LX/6Cz;

    .line 1065208
    iget-object v2, p0, LX/6DU;->c:LX/6Cz;

    const-string v3, "browser_extensions_autofill_requested"

    invoke-virtual {v2, v0, p1, v3}, LX/6Cz;->a(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065209
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1065210
    iget-object v3, p0, LX/6DU;->f:LX/6DM;

    .line 1065211
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1065212
    new-instance v5, LX/6DL;

    invoke-direct {v5, v3, v1, v4}, LX/6DL;-><init>(LX/6DM;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {v3, v5}, LX/6DM;->a(LX/6DM;LX/6DJ;)V

    .line 1065213
    move-object v4, v4

    .line 1065214
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1065215
    invoke-static {v1}, Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1065216
    iget-object v6, v3, LX/6DM;->c:LX/6DO;

    invoke-virtual {v6}, LX/6DO;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065217
    :goto_2
    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v5, v5

    .line 1065218
    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    const/4 v5, 0x1

    aput-object v4, v6, v5

    invoke-static {v6}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/6DI;

    invoke-direct {v5, v3, v2}, LX/6DI;-><init>(LX/6DM;Ljava/util/Set;)V

    iget-object v6, v3, LX/6DM;->b:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 1065219
    new-instance v2, LX/6DT;

    invoke-direct {v2, p0, v0, p1}, LX/6DT;-><init>(LX/6DU;Ljava/util/ArrayList;Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;)V

    iget-object v0, p0, LX/6DU;->h:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 1065220
    :cond_4
    invoke-static {v1}, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1065221
    iget-object v6, v3, LX/6DM;->c:LX/6DO;

    invoke-virtual {v6}, LX/6DO;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1065222
    :cond_5
    invoke-static {v1}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1065223
    iget-object v6, v3, LX/6DM;->c:LX/6DO;

    invoke-virtual {v6}, LX/6DO;->c()Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1065224
    :cond_6
    iget-object v6, v3, LX/6DM;->c:LX/6DO;

    invoke-virtual {v6, v1}, LX/6DO;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method
