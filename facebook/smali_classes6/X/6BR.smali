.class public LX/6BR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/ufiservices/common/ToggleLikeParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1062310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062311
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1062312
    check-cast p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    .line 1062313
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1062314
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_3

    .line 1062315
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1062316
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v0, v1

    .line 1062317
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-lez v1, :cond_0

    .line 1062318
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tracking"

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062319
    :cond_0
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1062320
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1062321
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1062322
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "nectar_module"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062323
    :cond_1
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1062324
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1062325
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1062326
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "feedback_source"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062327
    :cond_2
    const-string v2, "native_newsfeed"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1062328
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Likes from news feed must include tracking codes"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1062329
    :cond_3
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1062330
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fan_origin"

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062331
    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1062332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/likes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1062333
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    if-eqz v0, :cond_6

    const-string v2, "POST"

    .line 1062334
    :goto_1
    new-instance v0, LX/14N;

    const-string v1, "graphObjectLike"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1062335
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 1062336
    :cond_6
    const-string v2, "DELETE"

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1062337
    check-cast p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    .line 1062338
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1062339
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
