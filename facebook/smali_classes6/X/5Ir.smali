.class public final LX/5Ir;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 895936
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 895937
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 895938
    :goto_0
    return v1

    .line 895939
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 895940
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 895941
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 895942
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 895943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 895944
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 895945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 895946
    :cond_2
    const-string v6, "place_recommendation_page"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 895947
    invoke-static {p0, p1}, LX/5GD;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 895948
    :cond_3
    const-string v6, "recommended_by_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 895949
    invoke-static {p0, p1}, LX/4ar;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 895950
    :cond_4
    const-string v6, "recommending_comments"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 895951
    invoke-static {p0, p1}, LX/5Iq;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 895952
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 895953
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 895954
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 895955
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 895956
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 895957
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 895917
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 895918
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 895919
    if-eqz v0, :cond_0

    .line 895920
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895921
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 895922
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895923
    if-eqz v0, :cond_1

    .line 895924
    const-string v1, "place_recommendation_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895925
    invoke-static {p0, v0, p2, p3}, LX/5GD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 895926
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895927
    if-eqz v0, :cond_2

    .line 895928
    const-string v1, "recommended_by_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895929
    invoke-static {p0, v0, p2, p3}, LX/4ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895930
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895931
    if-eqz v0, :cond_3

    .line 895932
    const-string v1, "recommending_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895933
    invoke-static {p0, v0, p2, p3}, LX/5Iq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895934
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 895935
    return-void
.end method
