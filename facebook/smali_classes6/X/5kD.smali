.class public interface abstract LX/5kD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5kA;
.implements LX/1U7;
.implements LX/5kB;
.implements LX/5kC;


# virtual methods
.method public abstract A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()J
.end method

.method public abstract C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()Z
.end method

.method public abstract E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()D
.end method

.method public abstract I()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()Z
.end method

.method public abstract K()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract L()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract M()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract N()I
.end method

.method public abstract O()I
.end method

.method public abstract P()I
.end method

.method public abstract Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract R()Z
.end method

.method public abstract S()Z
.end method

.method public abstract T()Z
.end method

.method public abstract U()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract W()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract X()LX/175;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Y()D
.end method

.method public abstract Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aa()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ab()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$Photo360EncodingFields$PhotoEncodings;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ac()I
.end method

.method public abstract ad()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract af()LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ag()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ah()Z
.end method

.method public abstract ai()D
.end method

.method public abstract ai_()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aj()D
.end method

.method public abstract aj_()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ak()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract al()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract am()I
.end method

.method public abstract an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()LX/1f8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Z
.end method

.method public abstract p()Z
.end method

.method public abstract q()Z
.end method

.method public abstract r()Z
.end method

.method public abstract s()Z
.end method

.method public abstract t()Z
.end method

.method public abstract u()Z
.end method

.method public abstract v()Z
.end method

.method public abstract w()Z
.end method

.method public abstract x()Z
.end method

.method public abstract y()Z
.end method

.method public abstract z()Z
.end method
