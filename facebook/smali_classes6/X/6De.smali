.class public LX/6De;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Uh;

.field public final e:LX/6DM;

.field public final f:LX/6DS;

.field public final g:LX/03V;

.field public final h:Ljava/util/concurrent/Executor;

.field public final i:LX/1Bf;

.field private final j:LX/6D0;

.field public final k:LX/0W3;

.field public l:LX/6Cz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065385
    const-class v0, LX/6De;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6De;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/6DM;LX/6DS;LX/03V;Ljava/util/concurrent/Executor;LX/1Bf;LX/6D0;LX/0W3;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1065386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065387
    iput-object p1, p0, LX/6De;->b:Landroid/content/Context;

    .line 1065388
    iput-object p2, p0, LX/6De;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1065389
    iput-object p3, p0, LX/6De;->d:LX/0Uh;

    .line 1065390
    iput-object p4, p0, LX/6De;->e:LX/6DM;

    .line 1065391
    iput-object p5, p0, LX/6De;->f:LX/6DS;

    .line 1065392
    iput-object p6, p0, LX/6De;->g:LX/03V;

    .line 1065393
    iput-object p7, p0, LX/6De;->h:Ljava/util/concurrent/Executor;

    .line 1065394
    iput-object p8, p0, LX/6De;->i:LX/1Bf;

    .line 1065395
    iput-object p9, p0, LX/6De;->j:LX/6D0;

    .line 1065396
    iput-object p10, p0, LX/6De;->k:LX/0W3;

    .line 1065397
    return-void
.end method

.method public static synthetic a(LX/6De;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1065398
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1065399
    const-string p0, "EXTRA_AUTO_FILL_JS_BRIDGE"

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1065400
    const-string p0, "EXTRA_AUTOFILL_SAVE_DATA"

    invoke-virtual {v0, p0, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1065401
    move-object v0, v0

    .line 1065402
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065403
    const-string v0, "saveAutofillData"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 6

    .prologue
    .line 1065404
    check-cast p1, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    .line 1065405
    iget-object v0, p0, LX/6De;->d:LX/0Uh;

    const/16 v1, 0x511

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1065406
    :goto_0
    return-void

    .line 1065407
    :cond_0
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1065408
    iget-object v1, p0, LX/6De;->j:LX/6D0;

    invoke-virtual {v1, v0}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v1

    iput-object v1, p0, LX/6De;->l:LX/6Cz;

    .line 1065409
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;->h()Ljava/util/HashMap;

    move-result-object v1

    .line 1065410
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1065411
    iget-object v1, p0, LX/6De;->l:LX/6Cz;

    const-string v3, "browser_extensions_save_autofill_requested"

    invoke-virtual {v1, v2, p1, v3}, LX/6Cz;->a(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065412
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1065413
    iget-object v3, p0, LX/6De;->e:LX/6DM;

    .line 1065414
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1065415
    new-instance v5, LX/6DK;

    invoke-direct {v5, v3, v4}, LX/6DK;-><init>(LX/6DM;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {v3, v5}, LX/6DM;->a(LX/6DM;LX/6DJ;)V

    .line 1065416
    move-object v3, v4

    .line 1065417
    new-instance v4, LX/6Dd;

    invoke-direct {v4, p0, v1, p1}, LX/6Dd;-><init>(LX/6De;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;)V

    iget-object v5, p0, LX/6De;->h:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1065418
    move-object v1, v1

    .line 1065419
    new-instance v3, LX/6Dc;

    invoke-direct {v3, p0, v2, p1, v0}, LX/6Dc;-><init>(LX/6De;Ljava/util/ArrayList;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;Landroid/os/Bundle;)V

    iget-object v0, p0, LX/6De;->h:Ljava/util/concurrent/Executor;

    invoke-static {v1, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
