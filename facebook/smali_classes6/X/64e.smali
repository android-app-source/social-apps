.class public final LX/64e;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/64e;

.field public static final b:LX/64e;

.field public static final c:LX/64e;

.field private static final d:[LX/64b;


# instance fields
.field public final e:Z

.field public final f:Z

.field public final g:[Ljava/lang/String;

.field public final h:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1045137
    const/16 v0, 0xd

    new-array v0, v0, [LX/64b;

    sget-object v1, LX/64b;->aK:LX/64b;

    aput-object v1, v0, v4

    sget-object v1, LX/64b;->aO:LX/64b;

    aput-object v1, v0, v3

    sget-object v1, LX/64b;->W:LX/64b;

    aput-object v1, v0, v5

    sget-object v1, LX/64b;->am:LX/64b;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, LX/64b;->al:LX/64b;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/64b;->av:LX/64b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/64b;->aw:LX/64b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/64b;->F:LX/64b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/64b;->J:LX/64b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/64b;->U:LX/64b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/64b;->D:LX/64b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/64b;->H:LX/64b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/64b;->h:LX/64b;

    aput-object v2, v0, v1

    sput-object v0, LX/64e;->d:[LX/64b;

    .line 1045138
    new-instance v0, LX/64d;

    invoke-direct {v0, v3}, LX/64d;-><init>(Z)V

    sget-object v1, LX/64e;->d:[LX/64b;

    .line 1045139
    invoke-virtual {v0, v1}, LX/64d;->a([LX/64b;)LX/64d;

    move-result-object v0

    new-array v1, v6, [LX/658;

    sget-object v2, LX/658;->TLS_1_2:LX/658;

    aput-object v2, v1, v4

    sget-object v2, LX/658;->TLS_1_1:LX/658;

    aput-object v2, v1, v3

    sget-object v2, LX/658;->TLS_1_0:LX/658;

    aput-object v2, v1, v5

    .line 1045140
    invoke-virtual {v0, v1}, LX/64d;->a([LX/658;)LX/64d;

    move-result-object v0

    .line 1045141
    invoke-virtual {v0, v3}, LX/64d;->a(Z)LX/64d;

    move-result-object v0

    .line 1045142
    invoke-virtual {v0}, LX/64d;->a()LX/64e;

    move-result-object v0

    sput-object v0, LX/64e;->a:LX/64e;

    .line 1045143
    new-instance v0, LX/64d;

    sget-object v1, LX/64e;->a:LX/64e;

    invoke-direct {v0, v1}, LX/64d;-><init>(LX/64e;)V

    new-array v1, v3, [LX/658;

    sget-object v2, LX/658;->TLS_1_0:LX/658;

    aput-object v2, v1, v4

    .line 1045144
    invoke-virtual {v0, v1}, LX/64d;->a([LX/658;)LX/64d;

    move-result-object v0

    .line 1045145
    invoke-virtual {v0, v3}, LX/64d;->a(Z)LX/64d;

    move-result-object v0

    .line 1045146
    invoke-virtual {v0}, LX/64d;->a()LX/64e;

    move-result-object v0

    sput-object v0, LX/64e;->b:LX/64e;

    .line 1045147
    new-instance v0, LX/64d;

    invoke-direct {v0, v4}, LX/64d;-><init>(Z)V

    invoke-virtual {v0}, LX/64d;->a()LX/64e;

    move-result-object v0

    sput-object v0, LX/64e;->c:LX/64e;

    return-void
.end method

.method public constructor <init>(LX/64d;)V
    .locals 1

    .prologue
    .line 1045131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045132
    iget-boolean v0, p1, LX/64d;->a:Z

    iput-boolean v0, p0, LX/64e;->e:Z

    .line 1045133
    iget-object v0, p1, LX/64d;->b:[Ljava/lang/String;

    iput-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    .line 1045134
    iget-object v0, p1, LX/64d;->c:[Ljava/lang/String;

    iput-object v0, p0, LX/64e;->h:[Ljava/lang/String;

    .line 1045135
    iget-boolean v0, p1, LX/64d;->d:Z

    iput-boolean v0, p0, LX/64e;->f:Z

    .line 1045136
    return-void
.end method

.method private static a([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1045060
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    array-length v1, p0

    if-eqz v1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 1045061
    :cond_0
    :goto_0
    return v0

    .line 1045062
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    .line 1045063
    invoke-static {p1, v3}, LX/65A;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 1045064
    const/4 v0, 0x1

    goto :goto_0

    .line 1045065
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static b(LX/64e;Ljavax/net/ssl/SSLSocket;Z)LX/64e;
    .locals 4

    .prologue
    .line 1045114
    iget-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    if-eqz v0, :cond_1

    const-class v0, Ljava/lang/String;

    iget-object v1, p0, LX/64e;->g:[Ljava/lang/String;

    .line 1045115
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/65A;->a(Ljava/lang/Class;[Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v1, v0

    .line 1045116
    :goto_0
    iget-object v0, p0, LX/64e;->h:[Ljava/lang/String;

    if-eqz v0, :cond_2

    const-class v0, Ljava/lang/String;

    iget-object v2, p0, LX/64e;->h:[Ljava/lang/String;

    .line 1045117
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/65A;->a(Ljava/lang/Class;[Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1045118
    :goto_1
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "TLS_FALLBACK_SCSV"

    invoke-static {v2, v3}, LX/65A;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1045119
    const-string v2, "TLS_FALLBACK_SCSV"

    const/4 p2, 0x0

    .line 1045120
    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    .line 1045121
    array-length p1, v1

    invoke-static {v1, p2, v3, p2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1045122
    array-length p1, v3

    add-int/lit8 p1, p1, -0x1

    aput-object v2, v3, p1

    .line 1045123
    move-object v1, v3

    .line 1045124
    :cond_0
    new-instance v2, LX/64d;

    invoke-direct {v2, p0}, LX/64d;-><init>(LX/64e;)V

    .line 1045125
    invoke-virtual {v2, v1}, LX/64d;->a([Ljava/lang/String;)LX/64d;

    move-result-object v1

    .line 1045126
    invoke-virtual {v1, v0}, LX/64d;->b([Ljava/lang/String;)LX/64d;

    move-result-object v0

    .line 1045127
    invoke-virtual {v0}, LX/64d;->a()LX/64e;

    move-result-object v0

    .line 1045128
    return-object v0

    .line 1045129
    :cond_1
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1045130
    :cond_2
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljavax/net/ssl/SSLSocket;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1045107
    iget-boolean v1, p0, LX/64e;->e:Z

    if-nez v1, :cond_1

    .line 1045108
    :cond_0
    :goto_0
    return v0

    .line 1045109
    :cond_1
    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    .line 1045110
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/64e;->a([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1045111
    :cond_2
    iget-object v1, p0, LX/64e;->g:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/64e;->g:[Ljava/lang/String;

    .line 1045112
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/64e;->a([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1045113
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1045097
    instance-of v2, p1, LX/64e;

    if-nez v2, :cond_1

    .line 1045098
    :cond_0
    :goto_0
    return v0

    .line 1045099
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    .line 1045100
    :cond_2
    check-cast p1, LX/64e;

    .line 1045101
    iget-boolean v2, p0, LX/64e;->e:Z

    iget-boolean v3, p1, LX/64e;->e:Z

    if-ne v2, v3, :cond_0

    .line 1045102
    iget-boolean v2, p0, LX/64e;->e:Z

    if-eqz v2, :cond_3

    .line 1045103
    iget-object v2, p0, LX/64e;->g:[Ljava/lang/String;

    iget-object v3, p1, LX/64e;->g:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1045104
    iget-object v2, p0, LX/64e;->h:[Ljava/lang/String;

    iget-object v3, p1, LX/64e;->h:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1045105
    iget-boolean v2, p0, LX/64e;->f:Z

    iget-boolean v3, p1, LX/64e;->f:Z

    if-ne v2, v3, :cond_0

    :cond_3
    move v0, v1

    .line 1045106
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1045090
    const/16 v0, 0x11

    .line 1045091
    iget-boolean v1, p0, LX/64e;->e:Z

    if-eqz v1, :cond_0

    .line 1045092
    iget-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1045093
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045094
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, LX/64e;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1045095
    :cond_0
    return v0

    .line 1045096
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1045066
    iget-boolean v0, p0, LX/64e;->e:Z

    if-nez v0, :cond_0

    .line 1045067
    const-string v0, "ConnectionSpec()"

    .line 1045068
    :goto_0
    return-object v0

    .line 1045069
    :cond_0
    iget-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1045070
    iget-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1045071
    :goto_1
    move-object v0, v0

    .line 1045072
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1045073
    :goto_2
    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1045074
    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    if-nez v1, :cond_5

    const/4 v1, 0x0

    .line 1045075
    :goto_3
    move-object v1, v1

    .line 1045076
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1045077
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConnectionSpec(cipherSuites="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", tlsVersions="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", supportsTlsExtensions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/64e;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1045078
    :cond_1
    const-string v0, "[all enabled]"

    goto :goto_2

    .line 1045079
    :cond_2
    const-string v1, "[all enabled]"

    goto :goto_4

    .line 1045080
    :cond_3
    iget-object v0, p0, LX/64e;->g:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [LX/64b;

    .line 1045081
    const/4 v0, 0x0

    :goto_5
    iget-object v2, p0, LX/64e;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1045082
    iget-object v2, p0, LX/64e;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1045083
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1045084
    :cond_4
    invoke-static {v1}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 1045085
    :cond_5
    iget-object v1, p0, LX/64e;->h:[Ljava/lang/String;

    array-length v1, v1

    new-array v2, v1, [LX/658;

    .line 1045086
    const/4 v1, 0x0

    :goto_6
    iget-object v3, p0, LX/64e;->h:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 1045087
    iget-object v3, p0, LX/64e;->h:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, LX/658;->forJavaName(Ljava/lang/String;)LX/658;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1045088
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1045089
    :cond_6
    invoke-static {v2}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_3
.end method
