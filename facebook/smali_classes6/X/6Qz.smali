.class public final enum LX/6Qz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Qz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Qz;

.field public static final enum FAILED:LX/6Qz;

.field public static final enum LOADED:LX/6Qz;

.field public static final enum LOADING:LX/6Qz;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1088361
    new-instance v0, LX/6Qz;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/6Qz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Qz;->LOADING:LX/6Qz;

    .line 1088362
    new-instance v0, LX/6Qz;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v3}, LX/6Qz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Qz;->LOADED:LX/6Qz;

    .line 1088363
    new-instance v0, LX/6Qz;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/6Qz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Qz;->FAILED:LX/6Qz;

    .line 1088364
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Qz;

    sget-object v1, LX/6Qz;->LOADING:LX/6Qz;

    aput-object v1, v0, v2

    sget-object v1, LX/6Qz;->LOADED:LX/6Qz;

    aput-object v1, v0, v3

    sget-object v1, LX/6Qz;->FAILED:LX/6Qz;

    aput-object v1, v0, v4

    sput-object v0, LX/6Qz;->$VALUES:[LX/6Qz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1088365
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Qz;
    .locals 1

    .prologue
    .line 1088366
    const-class v0, LX/6Qz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Qz;

    return-object v0
.end method

.method public static values()[LX/6Qz;
    .locals 1

    .prologue
    .line 1088367
    sget-object v0, LX/6Qz;->$VALUES:[LX/6Qz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Qz;

    return-object v0
.end method
