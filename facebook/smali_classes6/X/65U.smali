.class public final LX/65U;
.super Ljava/lang/RuntimeException;
.source ""


# static fields
.field public static final a:Ljava/lang/reflect/Method;


# instance fields
.field public lastException:Ljava/io/IOException;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1047379
    :try_start_0
    const-class v0, Ljava/lang/Throwable;

    const-string v1, "addSuppressed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/Throwable;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1047380
    :goto_0
    sput-object v0, LX/65U;->a:Ljava/lang/reflect/Method;

    .line 1047381
    return-void

    .line 1047382
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/IOException;)V
    .locals 0

    .prologue
    .line 1047383
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 1047384
    iput-object p1, p0, LX/65U;->lastException:Ljava/io/IOException;

    .line 1047385
    return-void
.end method
