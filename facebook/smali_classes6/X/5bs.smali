.class public final LX/5bs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 960144
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 960145
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 960146
    :goto_0
    return v1

    .line 960147
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 960148
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 960149
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 960150
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 960151
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 960152
    const-string v4, "parts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 960153
    invoke-static {p0, p1}, LX/5br;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 960154
    :cond_2
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 960155
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 960156
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 960157
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 960158
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 960159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 960160
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960161
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960162
    if-eqz v0, :cond_0

    .line 960163
    const-string v1, "parts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960164
    invoke-static {p0, v0, p2, p3}, LX/5br;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960165
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960166
    if-eqz v0, :cond_1

    .line 960167
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960168
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960169
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960170
    return-void
.end method
