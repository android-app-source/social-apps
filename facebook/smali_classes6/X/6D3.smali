.class public LX/6D3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0EY;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6CR;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;

.field public final d:LX/6D1;

.field public final e:LX/6D0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1064587
    const/4 v0, 0x0

    sput-object v0, LX/6D3;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;LX/03V;LX/6D1;LX/6D0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6CR;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/6D1;",
            "LX/6D0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064589
    iput-object p1, p0, LX/6D3;->b:Ljava/util/Set;

    .line 1064590
    iput-object p2, p0, LX/6D3;->c:LX/03V;

    .line 1064591
    iput-object p3, p0, LX/6D3;->d:LX/6D1;

    .line 1064592
    iput-object p4, p0, LX/6D3;->e:LX/6D0;

    .line 1064593
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/6Cx;
    .locals 1

    .prologue
    .line 1064594
    if-nez p0, :cond_0

    .line 1064595
    sget-object v0, LX/6Cx;->NONE:LX/6Cx;

    .line 1064596
    :goto_0
    return-object v0

    .line 1064597
    :cond_0
    const-string v0, "JS_BRIDGE_EXTENSION_TYPE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1064598
    invoke-static {v0}, LX/6Cx;->fromRawValue(Ljava/lang/String;)LX/6Cx;

    move-result-object v0

    goto :goto_0
.end method
