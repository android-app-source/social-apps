.class public LX/6EW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6EB;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6EW;


# instance fields
.field public a:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066226
    return-void
.end method

.method public static a(LX/0QB;)LX/6EW;
    .locals 3

    .prologue
    .line 1066227
    sget-object v0, LX/6EW;->b:LX/6EW;

    if-nez v0, :cond_1

    .line 1066228
    const-class v1, LX/6EW;

    monitor-enter v1

    .line 1066229
    :try_start_0
    sget-object v0, LX/6EW;->b:LX/6EW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1066230
    if-eqz v2, :cond_0

    .line 1066231
    :try_start_1
    new-instance v0, LX/6EW;

    invoke-direct {v0}, LX/6EW;-><init>()V

    .line 1066232
    move-object v0, v0

    .line 1066233
    sput-object v0, LX/6EW;->b:LX/6EW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066234
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1066235
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1066236
    :cond_1
    sget-object v0, LX/6EW;->b:LX/6EW;

    return-object v0

    .line 1066237
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1066238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Cy;)V
    .locals 2

    .prologue
    .line 1066239
    iget-object v0, p0, LX/6EW;->a:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    invoke-virtual {p1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1066240
    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 1066241
    iget-object v0, p0, LX/6EW;->a:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    iget-object v1, p0, LX/6EW;->a:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v1

    .line 1066242
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1066243
    const-string v3, "callbackID"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066244
    const-string v3, "result"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066245
    move-object v1, v2

    .line 1066246
    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1066247
    return-void
.end method
