.class public final LX/6Fv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/bugreporter/BugReport;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6FY;

.field public final synthetic b:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;LX/6FY;)V
    .locals 0

    .prologue
    .line 1069466
    iput-object p1, p0, LX/6Fv;->b:LX/6G2;

    iput-object p2, p0, LX/6Fv;->a:LX/6FY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1069467
    instance-of p0, p1, Ljava/util/concurrent/CancellationException;

    if-nez p0, :cond_0

    .line 1069468
    const-string p0, "BugReporter.onBugReportFailure"

    const-string v0, "Failed to create bug report"

    invoke-static {p0, v0, p1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069469
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1069470
    check-cast p1, Lcom/facebook/bugreporter/BugReport;

    .line 1069471
    iget-object v0, p0, LX/6Fv;->b:LX/6G2;

    iget-object v0, v0, LX/6G2;->e:LX/6G0;

    .line 1069472
    iget-object v1, p0, LX/6Fv;->a:LX/6FY;

    iget-object v1, v1, LX/6FY;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1069473
    new-instance v1, LX/6G1;

    iget-object v0, p0, LX/6Fv;->b:LX/6G2;

    iget-object v2, v0, LX/6G2;->e:LX/6G0;

    iget-object v0, p0, LX/6Fv;->a:LX/6FY;

    iget-object v0, v0, LX/6FY;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, LX/6G1;-><init>(LX/6G0;J)V

    move-object v0, v1

    .line 1069474
    :cond_0
    iget-object v1, p0, LX/6Fv;->b:LX/6G2;

    iget-object v2, p0, LX/6Fv;->a:LX/6FY;

    iget-object v2, v2, LX/6FY;->a:Landroid/content/Context;

    .line 1069475
    invoke-static {v2, p1, v0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(Landroid/content/Context;Lcom/facebook/bugreporter/BugReport;LX/6G0;)Landroid/content/Intent;

    move-result-object v3

    .line 1069476
    const-string v4, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-static {p1}, LX/6Fi;->a(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1069477
    instance-of v4, v2, Landroid/app/Activity;

    if-eqz v4, :cond_1

    .line 1069478
    check-cast v2, Landroid/app/Activity;

    .line 1069479
    iget-object v4, v1, LX/6G2;->i:Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x4693

    invoke-interface {v4, v3, v5, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1069480
    :goto_0
    return-void

    .line 1069481
    :cond_1
    iget-object v4, v1, LX/6G2;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
