.class public final LX/5SJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public o:Z

.field public p:I

.field public q:I

.field public r:Z

.field public s:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917670
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/5SJ;->e:J

    .line 917671
    const v0, 0x7f081893

    iput v0, p0, LX/5SJ;->f:I

    .line 917672
    const v0, 0x7f0813d1    # 1.808779E38f

    iput v0, p0, LX/5SJ;->g:I

    .line 917673
    const v0, 0x7f0813d4

    iput v0, p0, LX/5SJ;->h:I

    .line 917674
    const-string v0, "timeline"

    iput-object v0, p0, LX/5SJ;->j:Ljava/lang/String;

    .line 917675
    iput-boolean v2, p0, LX/5SJ;->o:Z

    .line 917676
    iput v2, p0, LX/5SJ;->q:I

    .line 917677
    iput-boolean v2, p0, LX/5SJ;->r:Z

    .line 917678
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5SJ;->s:Z

    .line 917679
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 917680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917681
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/5SJ;->e:J

    .line 917682
    const v0, 0x7f081893

    iput v0, p0, LX/5SJ;->f:I

    .line 917683
    const v0, 0x7f0813d1    # 1.808779E38f

    iput v0, p0, LX/5SJ;->g:I

    .line 917684
    const v0, 0x7f0813d4

    iput v0, p0, LX/5SJ;->h:I

    .line 917685
    const-string v0, "timeline"

    iput-object v0, p0, LX/5SJ;->j:Ljava/lang/String;

    .line 917686
    iput-boolean v2, p0, LX/5SJ;->o:Z

    .line 917687
    iput v2, p0, LX/5SJ;->q:I

    .line 917688
    iput-boolean v2, p0, LX/5SJ;->r:Z

    .line 917689
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5SJ;->s:Z

    .line 917690
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 917691
    iput-object v0, p0, LX/5SJ;->a:Landroid/net/Uri;

    .line 917692
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->b:Ljava/lang/String;

    move-object v0, v0

    .line 917693
    iput-object v0, p0, LX/5SJ;->b:Ljava/lang/String;

    .line 917694
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    move-object v0, v0

    .line 917695
    iput-object v0, p0, LX/5SJ;->c:Ljava/lang/String;

    .line 917696
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->d:Ljava/lang/String;

    move-object v0, v0

    .line 917697
    iput-object v0, p0, LX/5SJ;->d:Ljava/lang/String;

    .line 917698
    iget-wide v3, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->e:J

    move-wide v0, v3

    .line 917699
    iput-wide v0, p0, LX/5SJ;->e:J

    .line 917700
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->f:I

    move v0, v0

    .line 917701
    iput v0, p0, LX/5SJ;->f:I

    .line 917702
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->g:I

    move v0, v0

    .line 917703
    iput v0, p0, LX/5SJ;->g:I

    .line 917704
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->h:I

    move v0, v0

    .line 917705
    iput v0, p0, LX/5SJ;->h:I

    .line 917706
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    move v0, v0

    .line 917707
    iput v0, p0, LX/5SJ;->i:I

    .line 917708
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->j:Ljava/lang/String;

    move-object v0, v0

    .line 917709
    iput-object v0, p0, LX/5SJ;->j:Ljava/lang/String;

    .line 917710
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-object v0, v0

    .line 917711
    iput-object v0, p0, LX/5SJ;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 917712
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    move-object v0, v0

    .line 917713
    iput-object v0, p0, LX/5SJ;->l:LX/5QV;

    .line 917714
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 917715
    iput-object v0, p0, LX/5SJ;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 917716
    iget-object v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v0

    .line 917717
    iput-object v0, p0, LX/5SJ;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917718
    iget-boolean v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->o:Z

    move v0, v0

    .line 917719
    iput-boolean v0, p0, LX/5SJ;->o:Z

    .line 917720
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    move v0, v0

    .line 917721
    iput v0, p0, LX/5SJ;->p:I

    .line 917722
    iget v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->q:I

    move v0, v0

    .line 917723
    iput v0, p0, LX/5SJ;->q:I

    .line 917724
    iget-boolean v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->r:Z

    move v0, v0

    .line 917725
    iput-boolean v0, p0, LX/5SJ;->r:Z

    .line 917726
    iget-boolean v0, p1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->s:Z

    move v0, v0

    .line 917727
    iput-boolean v0, p0, LX/5SJ;->s:Z

    .line 917728
    return-void
.end method


# virtual methods
.method public final a(J)LX/5SJ;
    .locals 1

    .prologue
    .line 917729
    iput-wide p1, p0, LX/5SJ;->e:J

    .line 917730
    return-object p0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;
    .locals 0

    .prologue
    .line 917731
    iput-object p1, p0, LX/5SJ;->a:Landroid/net/Uri;

    .line 917732
    iput-object p2, p0, LX/5SJ;->b:Ljava/lang/String;

    .line 917733
    return-object p0
.end method

.method public final b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 917734
    iget-object v0, p0, LX/5SJ;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917735
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5SJ;->d:Ljava/lang/String;

    .line 917736
    :cond_0
    iget-object v0, p0, LX/5SJ;->l:LX/5QV;

    invoke-static {v0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v2

    .line 917737
    iget-object v0, p0, LX/5SJ;->l:LX/5QV;

    if-eqz v0, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    const-string v3, "If a selected image overlay is passed in, it must have a URI"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 917738
    iget-object v0, p0, LX/5SJ;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    if-eqz v0, :cond_2

    .line 917739
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 917740
    iget-object v0, p0, LX/5SJ;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iget-object v2, p0, LX/5SJ;->l:LX/5QV;

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 917741
    invoke-static {v0}, LX/5Qm;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 917742
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;->b()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel$ProfilePictureOverlaysModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel$ProfilePictureOverlaysModel;->a()LX/0Px;

    move-result-object v5

    .line 917743
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5QV;

    .line 917744
    invoke-interface {v1}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 917745
    const/4 v1, 0x1

    .line 917746
    :goto_2
    move v0, v1

    .line 917747
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 917748
    :cond_2
    new-instance v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;-><init>(LX/5SJ;)V

    return-object v0

    :cond_3
    move v0, v1

    .line 917749
    goto :goto_0

    .line 917750
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_5
    move v1, v3

    .line 917751
    goto :goto_2
.end method
