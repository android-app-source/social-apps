.class public final LX/6EE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/6EF;


# direct methods
.method public constructor <init>(LX/6EF;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1065946
    iput-object p1, p0, LX/6EE;->c:LX/6EF;

    iput-object p2, p0, LX/6EE;->a:Ljava/lang/String;

    iput-object p3, p0, LX/6EE;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1065947
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1065948
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;

    .line 1065949
    iget-object v1, v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1065950
    iget-object v2, p0, LX/6EE;->c:LX/6EF;

    iget-object v2, v2, LX/6EF;->c:LX/6EV;

    iget-object v3, p0, LX/6EE;->a:Ljava/lang/String;

    .line 1065951
    iget-object p1, v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->b:Ljava/lang/String;

    move-object v0, p1

    .line 1065952
    iget-object v4, v2, LX/6EV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    .line 1065953
    sget-object v4, LX/6D7;->c:LX/0Tn;

    invoke-virtual {v4, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    invoke-interface {p1, v4, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1065954
    sget-object v4, LX/6D7;->d:LX/0Tn;

    invoke-virtual {v4, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    invoke-interface {p1, v4, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1065955
    invoke-interface {p1}, LX/0hN;->commit()V

    .line 1065956
    iget-object v0, p0, LX/6EE;->c:LX/6EF;

    iget-object v0, v0, LX/6EF;->c:LX/6EV;

    iget-object v2, p0, LX/6EE;->b:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/6EV;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
