.class public LX/6cU;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6cU;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114388
    const-string v0, "time_skew"

    const/4 v1, 0x1

    new-instance v2, LX/6cT;

    invoke-direct {v2}, LX/6cT;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1114389
    return-void
.end method

.method public static a(LX/0QB;)LX/6cU;
    .locals 3

    .prologue
    .line 1114390
    sget-object v0, LX/6cU;->a:LX/6cU;

    if-nez v0, :cond_1

    .line 1114391
    const-class v1, LX/6cU;

    monitor-enter v1

    .line 1114392
    :try_start_0
    sget-object v0, LX/6cU;->a:LX/6cU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1114393
    if-eqz v2, :cond_0

    .line 1114394
    :try_start_1
    new-instance v0, LX/6cU;

    invoke-direct {v0}, LX/6cU;-><init>()V

    .line 1114395
    move-object v0, v0

    .line 1114396
    sput-object v0, LX/6cU;->a:LX/6cU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114397
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1114398
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1114399
    :cond_1
    sget-object v0, LX/6cU;->a:LX/6cU;

    return-object v0

    .line 1114400
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1114401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
