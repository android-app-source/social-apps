.class public final LX/6EH;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1065984
    iput-object p1, p0, LX/6EH;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iput-object p2, p0, LX/6EH;->a:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1065985
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6EH;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1065986
    iget-object v1, p0, LX/6EH;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v1, v1, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->o:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6EH;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1065987
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1065988
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1065989
    iget-object v0, p0, LX/6EH;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a066f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1065990
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1065991
    return-void
.end method
