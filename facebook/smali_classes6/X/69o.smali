.class public final LX/69o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/69o;


# instance fields
.field public final a:LX/69l;

.field private final b:LX/29L;

.field public final c:LX/0oy;

.field public final d:LX/0Ym;

.field public final e:LX/0pn;

.field public final f:LX/1fN;


# direct methods
.method public constructor <init>(LX/69l;LX/29L;LX/0oy;LX/0Ym;LX/0pn;LX/1fN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058294
    iput-object p1, p0, LX/69o;->a:LX/69l;

    .line 1058295
    iput-object p2, p0, LX/69o;->b:LX/29L;

    .line 1058296
    iput-object p3, p0, LX/69o;->c:LX/0oy;

    .line 1058297
    iput-object p4, p0, LX/69o;->d:LX/0Ym;

    .line 1058298
    iput-object p5, p0, LX/69o;->e:LX/0pn;

    .line 1058299
    iput-object p6, p0, LX/69o;->f:LX/1fN;

    .line 1058300
    return-void
.end method

.method public static a(LX/0QB;)LX/69o;
    .locals 10

    .prologue
    .line 1058301
    sget-object v0, LX/69o;->g:LX/69o;

    if-nez v0, :cond_1

    .line 1058302
    const-class v1, LX/69o;

    monitor-enter v1

    .line 1058303
    :try_start_0
    sget-object v0, LX/69o;->g:LX/69o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1058304
    if-eqz v2, :cond_0

    .line 1058305
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1058306
    new-instance v3, LX/69o;

    invoke-static {v0}, LX/69l;->a(LX/0QB;)LX/69l;

    move-result-object v4

    check-cast v4, LX/69l;

    invoke-static {v0}, LX/29L;->a(LX/0QB;)LX/29L;

    move-result-object v5

    check-cast v5, LX/29L;

    invoke-static {v0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v6

    check-cast v6, LX/0oy;

    invoke-static {v0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v7

    check-cast v7, LX/0Ym;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v8

    check-cast v8, LX/0pn;

    invoke-static {v0}, LX/1fN;->a(LX/0QB;)LX/1fN;

    move-result-object v9

    check-cast v9, LX/1fN;

    invoke-direct/range {v3 .. v9}, LX/69o;-><init>(LX/69l;LX/29L;LX/0oy;LX/0Ym;LX/0pn;LX/1fN;)V

    .line 1058307
    move-object v0, v3

    .line 1058308
    sput-object v0, LX/69o;->g:LX/69o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058309
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1058310
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1058311
    :cond_1
    sget-object v0, LX/69o;->g:LX/69o;

    return-object v0

    .line 1058312
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1058313
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 15

    .prologue
    .line 1058314
    iget-object v0, p0, LX/69o;->b:LX/29L;

    .line 1058315
    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, LX/69o;->c:LX/0oy;

    invoke-virtual {v3}, LX/0oy;->i()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 1058316
    iget-object v4, p0, LX/69o;->d:LX/0Ym;

    invoke-virtual {v4}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    .line 1058317
    iget-object v5, p0, LX/69o;->e:LX/0pn;

    invoke-virtual {v5, v4, v2, v3}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v5

    .line 1058318
    iget-object v2, p0, LX/69o;->a:LX/69l;

    .line 1058319
    iget-object v11, v2, LX/69l;->b:LX/0W3;

    sget-wide v13, LX/0X5;->dD:J

    invoke-interface {v11, v13, v14}, LX/0W4;->c(J)J

    move-result-wide v11

    move-wide v6, v11

    .line 1058320
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1058321
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v9, :cond_1

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1058322
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 1058323
    instance-of v10, v3, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v10, :cond_0

    .line 1058324
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1058325
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 1058326
    if-eqz v3, :cond_0

    .line 1058327
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    .line 1058328
    invoke-interface {v8, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1058329
    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v2, v6

    if-gez v2, :cond_1

    .line 1058330
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 1058331
    :cond_1
    move-object v1, v8

    .line 1058332
    invoke-virtual {v0, v1}, LX/29L;->a(Ljava/util/Map;)V

    .line 1058333
    return-void
.end method
