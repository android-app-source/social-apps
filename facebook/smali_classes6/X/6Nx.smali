.class public LX/6Nx;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchContactsParams;",
        "Lcom/facebook/contacts/server/FetchContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fb;

.field private final d:LX/3fc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084093
    const-class v0, LX/6Nx;

    sput-object v0, LX/6Nx;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fb;LX/3fc;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084094
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 1084095
    iput-object p1, p0, LX/6Nx;->c:LX/3fb;

    .line 1084096
    iput-object p2, p0, LX/6Nx;->d:LX/3fc;

    .line 1084097
    return-void
.end method

.method public static a(LX/0QB;)LX/6Nx;
    .locals 4

    .prologue
    .line 1084098
    new-instance v3, LX/6Nx;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v0

    check-cast v0, LX/3fb;

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v1

    check-cast v1, LX/3fc;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v2

    check-cast v2, LX/0sO;

    invoke-direct {v3, v0, v1, v2}, LX/6Nx;-><init>(LX/3fb;LX/3fc;LX/0sO;)V

    .line 1084099
    move-object v0, v3

    .line 1084100
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1084101
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1084102
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v0

    .line 1084103
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 1084104
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 1084105
    :try_start_0
    iget-object v4, p0, LX/6Nx;->c:LX/3fb;

    invoke-virtual {v4, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1084106
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1084107
    :catch_0
    move-exception v0

    .line 1084108
    sget-object v1, LX/6Nx;->b:Ljava/lang/Class;

    const-string v2, "Couldn\'t deserialize contact. ID = %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v1, v0, v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1084109
    throw v0

    .line 1084110
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1084111
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1084112
    new-instance v1, Lcom/facebook/contacts/server/FetchContactsResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1084113
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 1084114
    check-cast p1, Lcom/facebook/contacts/server/FetchContactsParams;

    .line 1084115
    invoke-static {}, LX/3gr;->c()LX/6MB;

    move-result-object v0

    .line 1084116
    iget-object v1, p0, LX/6Nx;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->a(LX/0gW;)V

    .line 1084117
    iget-object v1, p0, LX/6Nx;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->c(LX/0gW;)V

    .line 1084118
    const-string v1, "contact_ids"

    .line 1084119
    iget-object v2, p1, Lcom/facebook/contacts/server/FetchContactsParams;->a:LX/0Rf;

    move-object v2, v2

    .line 1084120
    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1084121
    return-object v0
.end method
