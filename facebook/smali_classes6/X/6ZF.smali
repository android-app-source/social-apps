.class public LX/6ZF;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field public final type:LX/6ZE;


# direct methods
.method public constructor <init>(LX/6ZE;)V
    .locals 1

    .prologue
    .line 1111038
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/6ZF;-><init>(LX/6ZE;Ljava/lang/Throwable;)V

    .line 1111039
    return-void
.end method

.method public constructor <init>(LX/6ZE;Ljava/lang/Throwable;)V
    .locals 2
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111035
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Location error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1111036
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ZE;

    iput-object v0, p0, LX/6ZF;->type:LX/6ZE;

    .line 1111037
    return-void
.end method
