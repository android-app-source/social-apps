.class public final LX/6Gd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/bugreporter/debug/BugReportUploadStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Ge;


# direct methods
.method public constructor <init>(LX/6Ge;)V
    .locals 0

    .prologue
    .line 1070748
    iput-object p1, p0, LX/6Gd;->a:LX/6Ge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1070749
    check-cast p1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    check-cast p2, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    .line 1070750
    iget-wide v0, p1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    iget-wide v2, p2, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    iget-wide v2, p2, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
