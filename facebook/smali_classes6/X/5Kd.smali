.class public LX/5Kd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Kb;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/componentscript/components/CSFBNetworkImageSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 898865
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Kd;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/componentscript/components/CSFBNetworkImageSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 898836
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 898837
    iput-object p1, p0, LX/5Kd;->b:LX/0Ot;

    .line 898838
    return-void
.end method

.method public static a(LX/0QB;)LX/5Kd;
    .locals 4

    .prologue
    .line 898854
    const-class v1, LX/5Kd;

    monitor-enter v1

    .line 898855
    :try_start_0
    sget-object v0, LX/5Kd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 898856
    sput-object v2, LX/5Kd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 898857
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 898858
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 898859
    new-instance v3, LX/5Kd;

    const/16 p0, 0x194d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/5Kd;-><init>(LX/0Ot;)V

    .line 898860
    move-object v0, v3

    .line 898861
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 898862
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/5Kd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 898863
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 898864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 898841
    check-cast p2, LX/5Kc;

    .line 898842
    iget-object v0, p0, LX/5Kd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/components/CSFBNetworkImageSpec;

    iget-object v1, p2, LX/5Kc;->a:Lcom/facebook/java2js/JSValue;

    .line 898843
    const-string v2, "url"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 898844
    const-string v3, "focus"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    .line 898845
    new-instance v4, Landroid/graphics/PointF;

    const-string v5, "x"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v5, v6

    const-string v6, "y"

    invoke-virtual {v3, v6}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 898846
    const-string v3, "size"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    .line 898847
    const-string v5, "aspectRatio"

    invoke-virtual {v1, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    .line 898848
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/componentscript/components/CSFBNetworkImageSpec;->a:LX/1Ad;

    invoke-virtual {v5, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v5, Lcom/facebook/componentscript/components/CSFBNetworkImageSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    double-to-float v2, v6

    :goto_0
    invoke-virtual {v4, v2}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v4, -0x777778

    invoke-interface {v2, v4}, LX/1Di;->y(I)LX/1Di;

    move-result-object v2

    .line 898849
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/java2js/JSValue;->isNull()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v4

    if-nez v4, :cond_0

    .line 898850
    const-string v4, "width"

    invoke-virtual {v3, v4}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-interface {v2, v4}, LX/1Di;->j(I)LX/1Di;

    move-result-object v2

    const-string v4, "height"

    invoke-virtual {v3, v4}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-interface {v2, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    .line 898851
    :cond_0
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 898852
    return-object v0

    .line 898853
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898839
    invoke-static {}, LX/1dS;->b()V

    .line 898840
    const/4 v0, 0x0

    return-object v0
.end method
