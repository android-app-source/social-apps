.class public final LX/54H;
.super LX/54G;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/54G",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 827798
    invoke-direct {p0, p1}, LX/54G;-><init>(I)V

    .line 827799
    return-void
.end method


# virtual methods
.method public final offer(Ljava/lang/Object;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 827800
    if-nez p1, :cond_0

    .line 827801
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null is not a valid element"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827802
    :cond_0
    iget-object v0, p0, LX/53v;->d:[Ljava/lang/Object;

    .line 827803
    iget-wide v2, p0, LX/54D;->g:J

    iget-wide v4, p0, LX/54D;->h:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 827804
    iget-wide v2, p0, LX/54D;->g:J

    iget v1, p0, LX/54B;->e:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/53v;->b([Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 827805
    const/4 v0, 0x0

    .line 827806
    :goto_0
    return v0

    .line 827807
    :cond_1
    iget-wide v2, p0, LX/54D;->g:J

    iget v1, p0, LX/54B;->e:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/54H;->h:J

    .line 827808
    :cond_2
    iget-wide v2, p0, LX/54D;->g:J

    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v2

    .line 827809
    iget-wide v4, p0, LX/54D;->g:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/54H;->g:J

    .line 827810
    invoke-static {v0, v2, v3, p1}, LX/53v;->b([Ljava/lang/Object;JLjava/lang/Object;)V

    .line 827811
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final peek()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 827812
    iget-wide v0, p0, LX/54F;->f:J

    invoke-virtual {p0, v0, v1}, LX/53v;->a(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/53v;->c(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 827813
    iget-wide v2, p0, LX/54F;->f:J

    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v2

    .line 827814
    iget-object v4, p0, LX/53v;->d:[Ljava/lang/Object;

    .line 827815
    invoke-static {v4, v2, v3}, LX/53v;->b([Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 827816
    if-nez v1, :cond_0

    .line 827817
    :goto_0
    return-object v0

    .line 827818
    :cond_0
    iget-wide v6, p0, LX/54F;->f:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, p0, LX/54H;->f:J

    .line 827819
    invoke-static {v4, v2, v3, v0}, LX/53v;->b([Ljava/lang/Object;JLjava/lang/Object;)V

    move-object v0, v1

    .line 827820
    goto :goto_0
.end method

.method public final size()I
    .locals 6

    .prologue
    .line 827821
    invoke-virtual {p0}, LX/54F;->a()J

    move-result-wide v0

    .line 827822
    :goto_0
    invoke-virtual {p0}, LX/54D;->b()J

    move-result-wide v4

    .line 827823
    invoke-virtual {p0}, LX/54F;->a()J

    move-result-wide v2

    .line 827824
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 827825
    sub-long v0, v4, v2

    long-to-int v0, v0

    return v0

    :cond_0
    move-wide v0, v2

    .line 827826
    goto :goto_0
.end method
