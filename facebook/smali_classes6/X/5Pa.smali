.class public final LX/5Pa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/5Pb;


# direct methods
.method public constructor <init>(LX/5Pb;)V
    .locals 0

    .prologue
    .line 910924
    iput-object p1, p0, LX/5Pa;->a:LX/5Pb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 910916
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 910917
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 910918
    :goto_0
    return v0

    .line 910919
    :cond_0
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget v0, v0, LX/5Pb;->a:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v3

    .line 910920
    const/4 v0, -0x1

    if-eq v3, v0, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "Uniform location not found: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v4, v1}, LX/64O;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 910921
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v3

    .line 910922
    goto :goto_0

    :cond_1
    move v0, v2

    .line 910923
    goto :goto_1
.end method

.method public static c(LX/5Pa;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 910911
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 910912
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 910913
    :goto_0
    return v0

    .line 910914
    :cond_0
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v0, v0, LX/5Pb;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    .line 910915
    iget-object v1, p0, LX/5Pa;->a:LX/5Pb;

    iget-object v1, v1, LX/5Pb;->d:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5PR;)LX/5Pa;
    .locals 12

    .prologue
    .line 910872
    iget-object v0, p0, LX/5Pa;->a:LX/5Pb;

    .line 910873
    iget-object v8, p1, LX/5PR;->a:Ljava/util/Map;

    .line 910874
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    .line 910875
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 910876
    iget-object v1, v0, LX/5Pb;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 910877
    iget-object v1, v0, LX/5Pb;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 910878
    :goto_1
    move v1, v1

    .line 910879
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 910880
    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, LX/5Pg;

    .line 910881
    iget-object v2, v7, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->position()I

    move-result v10

    .line 910882
    iget v2, v7, LX/5Pg;->b:I

    const/16 v3, 0x1406

    iget-boolean v4, v7, LX/5Pg;->d:Z

    iget v5, v7, LX/5Pg;->e:I

    iget-object v6, v7, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    iget v11, v7, LX/5Pg;->f:I

    add-int/2addr v11, v10

    invoke-virtual {v6, v11}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 910883
    invoke-static {v1}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 910884
    iget-object v1, v7, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v10}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0

    .line 910885
    :cond_1
    iget-object v1, p1, LX/5PR;->b:LX/5PY;

    if-eqz v1, :cond_3

    .line 910886
    iget-object v1, p1, LX/5PR;->b:LX/5PY;

    iget v1, v1, LX/5PY;->b:I

    .line 910887
    iget-object v2, p1, LX/5PR;->b:LX/5PY;

    instance-of v2, v2, LX/5PZ;

    if-eqz v2, :cond_2

    .line 910888
    iget-object v1, p1, LX/5PR;->b:LX/5PY;

    check-cast v1, LX/5PZ;

    iget v1, v1, LX/5PZ;->d:I

    .line 910889
    :cond_2
    iget v2, p1, LX/5PR;->c:I

    iget-object v3, p1, LX/5PR;->b:LX/5PY;

    iget v3, v3, LX/5PY;->c:I

    iget-object v4, p1, LX/5PR;->b:LX/5PY;

    iget-object v4, v4, LX/5PY;->a:Ljava/nio/Buffer;

    invoke-static {v2, v1, v3, v4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 910890
    :goto_2
    return-object p0

    .line 910891
    :cond_3
    iget v1, p1, LX/5PR;->c:I

    const/4 v2, 0x0

    iget v3, p1, LX/5PR;->d:I

    invoke-static {v1, v2, v3}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_2

    .line 910892
    :cond_4
    iget v1, v0, LX/5Pb;->a:I

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v5

    .line 910893
    const/4 v1, -0x1

    if-eq v5, v1, :cond_5

    move v1, v3

    :goto_3
    const-string v6, "Vertex attribute location not found: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v1, v6, v3}, LX/64O;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 910894
    iget-object v1, v0, LX/5Pb;->b:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v5

    .line 910895
    goto :goto_1

    :cond_5
    move v1, v4

    .line 910896
    goto :goto_3
.end method

.method public final a(Ljava/lang/String;F)LX/5Pa;
    .locals 1

    .prologue
    .line 910908
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910909
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 910910
    return-object p0
.end method

.method public final a(Ljava/lang/String;FF)LX/5Pa;
    .locals 1

    .prologue
    .line 910905
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910906
    invoke-static {v0, p2, p3}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 910907
    return-object p0
.end method

.method public final a(Ljava/lang/String;FFFF)LX/5Pa;
    .locals 1

    .prologue
    .line 910925
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910926
    invoke-static {v0, p2, p3, p4, p5}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 910927
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)LX/5Pa;
    .locals 1

    .prologue
    .line 910902
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910903
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 910904
    return-object p0
.end method

.method public final a(Ljava/lang/String;III)LX/5Pa;
    .locals 2

    .prologue
    .line 910897
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910898
    const v1, 0x84c0

    add-int/2addr v1, p2

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 910899
    invoke-static {p3, p4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 910900
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 910901
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/5Pf;)LX/5Pa;
    .locals 3

    .prologue
    .line 910870
    invoke-static {p0, p1}, LX/5Pa;->c(LX/5Pa;Ljava/lang/String;)I

    move-result v0

    .line 910871
    iget v1, p2, LX/5Pf;->a:I

    iget v2, p2, LX/5Pf;->b:I

    invoke-virtual {p0, p1, v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;III)LX/5Pa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)LX/5Pa;
    .locals 1

    .prologue
    .line 910869
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/5Pa;->a(Ljava/lang/String;I)LX/5Pa;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[F)LX/5Pa;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 910866
    invoke-direct {p0, p1}, LX/5Pa;->b(Ljava/lang/String;)I

    move-result v0

    .line 910867
    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p2, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 910868
    return-object p0
.end method
