.class public LX/6VP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1104006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104007
    iput-object p1, p0, LX/6VP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1104008
    iput-object p2, p0, LX/6VP;->b:LX/0Or;

    .line 1104009
    return-void
.end method

.method public static a(LX/0QB;)LX/6VP;
    .locals 1

    .prologue
    .line 1104010
    invoke-static {p0}, LX/6VP;->b(LX/0QB;)LX/6VP;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6VP;
    .locals 3

    .prologue
    .line 1104011
    new-instance v1, LX/6VP;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x122d

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/6VP;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 1104012
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1104013
    iget-object v0, p0, LX/6VP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0pP;->B:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1104014
    return-void
.end method
