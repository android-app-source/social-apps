.class public LX/5ui;
.super Lcom/facebook/drawee/view/DraweeView;
.source ""

# interfaces
.implements LX/1OA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/drawee/view/DraweeView",
        "<",
        "LX/1af;",
        ">;",
        "LX/1OA;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Z

.field private final c:Landroid/graphics/RectF;

.field private final d:Landroid/graphics/RectF;

.field private e:LX/1aZ;

.field private f:LX/5ua;

.field private g:Landroid/view/GestureDetector;

.field private h:Z

.field private final i:LX/1Ai;

.field private final j:LX/5uh;

.field private final k:LX/5uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1019935
    const-class v0, LX/5ui;

    sput-object v0, LX/5ui;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1019936
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;)V

    .line 1019937
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5ui;->b:Z

    .line 1019938
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->c:Landroid/graphics/RectF;

    .line 1019939
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->d:Landroid/graphics/RectF;

    .line 1019940
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5ui;->h:Z

    .line 1019941
    new-instance v0, LX/5ug;

    invoke-direct {v0, p0}, LX/5ug;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->i:LX/1Ai;

    .line 1019942
    new-instance v0, LX/5uh;

    invoke-direct {v0, p0}, LX/5uh;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->j:LX/5uh;

    .line 1019943
    new-instance v0, LX/5uf;

    invoke-direct {v0}, LX/5uf;-><init>()V

    iput-object v0, p0, LX/5ui;->k:LX/5uf;

    .line 1019944
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5ui;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1019945
    invoke-direct {p0}, LX/5ui;->c()V

    .line 1019946
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 1

    .prologue
    .line 1019947
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;)V

    .line 1019948
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5ui;->b:Z

    .line 1019949
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->c:Landroid/graphics/RectF;

    .line 1019950
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->d:Landroid/graphics/RectF;

    .line 1019951
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5ui;->h:Z

    .line 1019952
    new-instance v0, LX/5ug;

    invoke-direct {v0, p0}, LX/5ug;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->i:LX/1Ai;

    .line 1019953
    new-instance v0, LX/5uh;

    invoke-direct {v0, p0}, LX/5uh;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->j:LX/5uh;

    .line 1019954
    new-instance v0, LX/5uf;

    invoke-direct {v0}, LX/5uf;-><init>()V

    iput-object v0, p0, LX/5ui;->k:LX/5uf;

    .line 1019955
    invoke-virtual {p0, p2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1019956
    invoke-direct {p0}, LX/5ui;->c()V

    .line 1019957
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1019958
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1019959
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5ui;->b:Z

    .line 1019960
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->c:Landroid/graphics/RectF;

    .line 1019961
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->d:Landroid/graphics/RectF;

    .line 1019962
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5ui;->h:Z

    .line 1019963
    new-instance v0, LX/5ug;

    invoke-direct {v0, p0}, LX/5ug;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->i:LX/1Ai;

    .line 1019964
    new-instance v0, LX/5uh;

    invoke-direct {v0, p0}, LX/5uh;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->j:LX/5uh;

    .line 1019965
    new-instance v0, LX/5uf;

    invoke-direct {v0}, LX/5uf;-><init>()V

    iput-object v0, p0, LX/5ui;->k:LX/5uf;

    .line 1019966
    invoke-direct {p0, p1, p2}, LX/5ui;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1019967
    invoke-direct {p0}, LX/5ui;->c()V

    .line 1019968
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1019969
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1019970
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5ui;->b:Z

    .line 1019971
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->c:Landroid/graphics/RectF;

    .line 1019972
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ui;->d:Landroid/graphics/RectF;

    .line 1019973
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5ui;->h:Z

    .line 1019974
    new-instance v0, LX/5ug;

    invoke-direct {v0, p0}, LX/5ug;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->i:LX/1Ai;

    .line 1019975
    new-instance v0, LX/5uh;

    invoke-direct {v0, p0}, LX/5uh;-><init>(LX/5ui;)V

    iput-object v0, p0, LX/5ui;->j:LX/5uh;

    .line 1019976
    new-instance v0, LX/5uf;

    invoke-direct {v0}, LX/5uf;-><init>()V

    iput-object v0, p0, LX/5ui;->k:LX/5uf;

    .line 1019977
    invoke-direct {p0, p1, p2}, LX/5ui;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1019978
    invoke-direct {p0}, LX/5ui;->c()V

    .line 1019979
    return-void
.end method

.method private a(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1019980
    instance-of v0, p1, LX/1bp;

    if-eqz v0, :cond_0

    .line 1019981
    check-cast p1, LX/1bp;

    iget-object v0, p0, LX/5ui;->i:LX/1Ai;

    .line 1019982
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019983
    iget-object p0, p1, LX/1bp;->g:LX/1Ai;

    instance-of p0, p0, LX/1fe;

    if-eqz p0, :cond_1

    .line 1019984
    iget-object p0, p1, LX/1bp;->g:LX/1Ai;

    check-cast p0, LX/1fe;

    invoke-virtual {p0, v0}, LX/1ff;->b(LX/1Ai;)V

    .line 1019985
    :cond_0
    :goto_0
    return-void

    .line 1019986
    :cond_1
    iget-object p0, p1, LX/1bp;->g:LX/1Ai;

    if-ne p0, v0, :cond_0

    .line 1019987
    const/4 p0, 0x0

    iput-object p0, p1, LX/1bp;->g:LX/1Ai;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1019988
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1019989
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v0, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v0}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    .line 1019990
    invoke-static {v0, p1, p2}, LX/1ad;->a(LX/1Uo;Landroid/content/Context;Landroid/util/AttributeSet;)LX/1Uo;

    .line 1019991
    iget v1, v0, LX/1Uo;->e:F

    move v1, v1

    .line 1019992
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1019993
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1019994
    return-void
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1019887
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1019888
    return-void
.end method

.method private b(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1019995
    instance-of v0, p1, LX/1bp;

    if-eqz v0, :cond_0

    .line 1019996
    check-cast p1, LX/1bp;

    iget-object v0, p0, LX/5ui;->i:LX/1Ai;

    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    .line 1019997
    :cond_0
    return-void
.end method

.method private b(LX/1aZ;LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1019998
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    invoke-direct {p0, v0}, LX/5ui;->a(LX/1aZ;)V

    .line 1019999
    invoke-direct {p0, p1}, LX/5ui;->b(LX/1aZ;)V

    .line 1020000
    iput-object p2, p0, LX/5ui;->e:LX/1aZ;

    .line 1020001
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1020002
    return-void
.end method

.method private b(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1020003
    invoke-virtual {p0}, LX/5ui;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LX/5ui;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1020004
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1020005
    invoke-static {}, LX/5ue;->i()LX/5ue;

    move-result-object v0

    iput-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1020006
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    iget-object v1, p0, LX/5ui;->j:LX/5uh;

    .line 1020007
    iput-object v1, v0, LX/5ua;->d:LX/5uh;

    .line 1020008
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/5ui;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/5ui;->k:LX/5uf;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/5ui;->g:Landroid/view/GestureDetector;

    .line 1020009
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1020010
    iget-object v0, p0, LX/5ui;->e:LX/1aZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    invoke-virtual {v0}, LX/5ua;->m()F

    move-result v0

    const v1, 0x3f8ccccd    # 1.1f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1020011
    iget-object v0, p0, LX/5ui;->e:LX/1aZ;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/5ui;->b(LX/1aZ;LX/1aZ;)V

    .line 1020012
    :cond_0
    return-void
.end method

.method public static e(LX/5ui;)V
    .locals 2

    .prologue
    .line 1020013
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1020014
    iget-boolean v1, v0, LX/5ua;->e:Z

    move v0, v1

    .line 1020015
    if-nez v0, :cond_0

    .line 1020016
    invoke-direct {p0}, LX/5ui;->g()V

    .line 1020017
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/5ua;->b(Z)V

    .line 1020018
    :cond_0
    return-void
.end method

.method public static f(LX/5ui;)V
    .locals 2

    .prologue
    .line 1020019
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5ua;->b(Z)V

    .line 1020020
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1020021
    iget-object v0, p0, LX/5ui;->c:Landroid/graphics/RectF;

    invoke-direct {p0, v0}, LX/5ui;->a(Landroid/graphics/RectF;)V

    .line 1020022
    iget-object v0, p0, LX/5ui;->d:Landroid/graphics/RectF;

    invoke-direct {p0, v0}, LX/5ui;->b(Landroid/graphics/RectF;)V

    .line 1020023
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    iget-object v1, p0, LX/5ui;->c:Landroid/graphics/RectF;

    .line 1020024
    iget-object v2, v0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1020025
    iget-object v2, v0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1020026
    invoke-static {v0}, LX/5ua;->c(LX/5ua;)V

    .line 1020027
    :cond_0
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    iget-object v1, p0, LX/5ui;->d:Landroid/graphics/RectF;

    .line 1020028
    iget-object v2, v0, LX/5ua;->k:Landroid/graphics/RectF;

    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1020029
    return-void
.end method


# virtual methods
.method public final a(LX/1aZ;LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1019928
    invoke-direct {p0, v0, v0}, LX/5ui;->b(LX/1aZ;LX/1aZ;)V

    .line 1019929
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5ua;->b(Z)V

    .line 1019930
    invoke-direct {p0, p1, p2}, LX/5ui;->b(LX/1aZ;LX/1aZ;)V

    .line 1019931
    return-void
.end method

.method public a(Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 1019932
    invoke-direct {p0}, LX/5ui;->d()V

    .line 1019933
    invoke-virtual {p0}, LX/5ui;->invalidate()V

    .line 1019934
    return-void
.end method

.method public final computeHorizontalScrollExtent()I
    .locals 1

    .prologue
    .line 1019857
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019858
    iget-object p0, v0, LX/5ua;->k:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result p0

    float-to-int p0, p0

    move v0, p0

    .line 1019859
    return v0
.end method

.method public final computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    .line 1019860
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019861
    iget-object v1, v0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object p0, v0, LX/5ua;->m:Landroid/graphics/RectF;

    iget p0, p0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, p0

    float-to-int v1, v1

    move v0, v1

    .line 1019862
    return v0
.end method

.method public final computeHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 1019863
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019864
    iget-object p0, v0, LX/5ua;->m:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result p0

    float-to-int p0, p0

    move v0, p0

    .line 1019865
    return v0
.end method

.method public final computeVerticalScrollExtent()I
    .locals 1

    .prologue
    .line 1019866
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019867
    iget-object p0, v0, LX/5ua;->k:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result p0

    float-to-int p0, p0

    move v0, p0

    .line 1019868
    return v0
.end method

.method public final computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 1019869
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019870
    iget-object v1, v0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object p0, v0, LX/5ua;->m:Landroid/graphics/RectF;

    iget p0, p0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, p0

    float-to-int v1, v1

    move v0, v1

    .line 1019871
    return v0
.end method

.method public final computeVerticalScrollRange()I
    .locals 1

    .prologue
    .line 1019872
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    .line 1019873
    iget-object p0, v0, LX/5ua;->m:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result p0

    float-to-int p0, p0

    move v0, p0

    .line 1019874
    return v0
.end method

.method public getLogTag()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1019875
    sget-object v0, LX/5ui;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public getZoomableController()LX/5ua;
    .locals 1

    .prologue
    .line 1019876
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1019877
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 1019878
    iget-object v1, p0, LX/5ui;->f:LX/5ua;

    .line 1019879
    iget-object v2, v1, LX/5ua;->o:Landroid/graphics/Matrix;

    move-object v1, v2

    .line 1019880
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1019881
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1019882
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1019883
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1019884
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/view/DraweeView;->onLayout(ZIIII)V

    .line 1019885
    invoke-direct {p0}, LX/5ui;->g()V

    .line 1019886
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4e8df705

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1019889
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 1019890
    iget-object v2, p0, LX/5ui;->g:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1019891
    const v2, -0x1e4b0d94

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1019892
    :goto_0
    return v0

    .line 1019893
    :cond_0
    iget-boolean v2, p0, LX/5ui;->b:Z

    if-eqz v2, :cond_1

    .line 1019894
    iget-object v2, p0, LX/5ui;->f:LX/5ua;

    invoke-virtual {v2, p1}, LX/5ua;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1019895
    const v2, 0x720d4216

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1019896
    :cond_1
    iget-object v2, p0, LX/5ui;->f:LX/5ua;

    invoke-virtual {v2, p1}, LX/5ua;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1019897
    iget-boolean v2, p0, LX/5ui;->h:Z

    if-nez v2, :cond_2

    iget-object v2, p0, LX/5ui;->f:LX/5ua;

    invoke-virtual {v2}, LX/5ua;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-boolean v2, p0, LX/5ui;->h:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/5ui;->f:LX/5ua;

    .line 1019898
    iget-boolean v3, v2, LX/5ua;->s:Z

    move v2, v3

    .line 1019899
    if-nez v2, :cond_4

    .line 1019900
    :cond_3
    invoke-virtual {p0}, LX/5ui;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1019901
    :cond_4
    const v2, 0x15163eac

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1019902
    :cond_5
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1019903
    const v2, 0x4153541e

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1019904
    :cond_6
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1019905
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1019906
    iget-object v2, p0, LX/5ui;->g:Landroid/view/GestureDetector;

    invoke-virtual {v2, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1019907
    iget-object v2, p0, LX/5ui;->f:LX/5ua;

    invoke-virtual {v2, v0}, LX/5ua;->a(Landroid/view/MotionEvent;)Z

    .line 1019908
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1019909
    const/4 v0, 0x0

    const v2, -0x4777643f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAllowTouchInterceptionWhileZoomed(Z)V
    .locals 0

    .prologue
    .line 1019910
    iput-boolean p1, p0, LX/5ui;->h:Z

    .line 1019911
    return-void
.end method

.method public setController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1019912
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/5ui;->a(LX/1aZ;LX/1aZ;)V

    .line 1019913
    return-void
.end method

.method public setExperimentalSimpleTouchHandlingEnabled(Z)V
    .locals 0

    .prologue
    .line 1019914
    iput-boolean p1, p0, LX/5ui;->b:Z

    .line 1019915
    return-void
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 1

    .prologue
    .line 1019916
    iget-object v0, p0, LX/5ui;->g:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1019917
    return-void
.end method

.method public setTapListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V
    .locals 1

    .prologue
    .line 1019918
    iget-object v0, p0, LX/5ui;->k:LX/5uf;

    .line 1019919
    iput-object p1, v0, LX/5uf;->a:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 1019920
    return-void
.end method

.method public setZoomableController(LX/5ua;)V
    .locals 2

    .prologue
    .line 1019921
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019922
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    const/4 v1, 0x0

    .line 1019923
    iput-object v1, v0, LX/5ua;->d:LX/5uh;

    .line 1019924
    iput-object p1, p0, LX/5ui;->f:LX/5ua;

    .line 1019925
    iget-object v0, p0, LX/5ui;->f:LX/5ua;

    iget-object v1, p0, LX/5ui;->j:LX/5uh;

    .line 1019926
    iput-object v1, v0, LX/5ua;->d:LX/5uh;

    .line 1019927
    return-void
.end method
