.class public final LX/5Av;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 44

    .prologue
    .line 860151
    const/16 v40, 0x0

    .line 860152
    const/16 v39, 0x0

    .line 860153
    const/16 v38, 0x0

    .line 860154
    const/16 v37, 0x0

    .line 860155
    const/16 v36, 0x0

    .line 860156
    const/16 v35, 0x0

    .line 860157
    const/16 v34, 0x0

    .line 860158
    const/16 v33, 0x0

    .line 860159
    const/16 v32, 0x0

    .line 860160
    const/16 v31, 0x0

    .line 860161
    const/16 v30, 0x0

    .line 860162
    const/16 v29, 0x0

    .line 860163
    const/16 v28, 0x0

    .line 860164
    const/16 v27, 0x0

    .line 860165
    const/16 v26, 0x0

    .line 860166
    const/16 v25, 0x0

    .line 860167
    const/16 v24, 0x0

    .line 860168
    const/16 v23, 0x0

    .line 860169
    const/16 v22, 0x0

    .line 860170
    const/16 v21, 0x0

    .line 860171
    const/16 v20, 0x0

    .line 860172
    const/16 v19, 0x0

    .line 860173
    const/16 v18, 0x0

    .line 860174
    const/16 v17, 0x0

    .line 860175
    const/16 v16, 0x0

    .line 860176
    const/4 v15, 0x0

    .line 860177
    const/4 v14, 0x0

    .line 860178
    const/4 v13, 0x0

    .line 860179
    const/4 v12, 0x0

    .line 860180
    const/4 v11, 0x0

    .line 860181
    const/4 v10, 0x0

    .line 860182
    const/4 v9, 0x0

    .line 860183
    const/4 v8, 0x0

    .line 860184
    const/4 v7, 0x0

    .line 860185
    const/4 v6, 0x0

    .line 860186
    const/4 v5, 0x0

    .line 860187
    const/4 v4, 0x0

    .line 860188
    const/4 v3, 0x0

    .line 860189
    const/4 v2, 0x0

    .line 860190
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    .line 860191
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 860192
    const/4 v2, 0x0

    .line 860193
    :goto_0
    return v2

    .line 860194
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 860195
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1c

    .line 860196
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v41

    .line 860197
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 860198
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    if-eqz v41, :cond_1

    .line 860199
    const-string v42, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_2

    .line 860200
    const/4 v13, 0x1

    .line 860201
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 860202
    :cond_2
    const-string v42, "can_see_voice_switcher"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_3

    .line 860203
    const/4 v12, 0x1

    .line 860204
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 860205
    :cond_3
    const-string v42, "can_viewer_comment"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 860206
    const/4 v11, 0x1

    .line 860207
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 860208
    :cond_4
    const-string v42, "can_viewer_comment_with_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 860209
    const/4 v10, 0x1

    .line 860210
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 860211
    :cond_5
    const-string v42, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_6

    .line 860212
    const/4 v9, 0x1

    .line 860213
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 860214
    :cond_6
    const-string v42, "can_viewer_comment_with_video"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 860215
    const/4 v8, 0x1

    .line 860216
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 860217
    :cond_7
    const-string v42, "can_viewer_like"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_8

    .line 860218
    const/4 v7, 0x1

    .line 860219
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 860220
    :cond_8
    const-string v42, "can_viewer_react"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_9

    .line 860221
    const/4 v6, 0x1

    .line 860222
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 860223
    :cond_9
    const-string v42, "can_viewer_subscribe"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 860224
    const/4 v5, 0x1

    .line 860225
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 860226
    :cond_a
    const-string v42, "comments_mirroring_domain"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_b

    .line 860227
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 860228
    :cond_b
    const-string v42, "default_comment_ordering"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_c

    .line 860229
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 860230
    :cond_c
    const-string v42, "does_viewer_like"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_d

    .line 860231
    const/4 v4, 0x1

    .line 860232
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 860233
    :cond_d
    const-string v42, "id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 860234
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 860235
    :cond_e
    const-string v42, "important_reactors"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_f

    .line 860236
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 860237
    :cond_f
    const-string v42, "is_viewer_subscribed"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_10

    .line 860238
    const/4 v3, 0x1

    .line 860239
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 860240
    :cond_10
    const-string v42, "legacy_api_post_id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 860241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 860242
    :cond_11
    const-string v42, "likers"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 860243
    invoke-static/range {p0 .. p1}, LX/5Ac;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 860244
    :cond_12
    const-string v42, "reactors"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_13

    .line 860245
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 860246
    :cond_13
    const-string v42, "real_time_activity_info"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_14

    .line 860247
    invoke-static/range {p0 .. p1}, LX/5Ab;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 860248
    :cond_14
    const-string v42, "remixable_photo_uri"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_15

    .line 860249
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 860250
    :cond_15
    const-string v42, "reshares"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_16

    .line 860251
    invoke-static/range {p0 .. p1}, LX/5Ad;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 860252
    :cond_16
    const-string v42, "supported_reactions"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 860253
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 860254
    :cond_17
    const-string v42, "top_level_comments"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 860255
    invoke-static/range {p0 .. p1}, LX/5Ae;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 860256
    :cond_18
    const-string v42, "top_reactions"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_19

    .line 860257
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 860258
    :cond_19
    const-string v42, "viewer_acts_as_page"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 860259
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 860260
    :cond_1a
    const-string v42, "viewer_acts_as_person"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1b

    .line 860261
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 860262
    :cond_1b
    const-string v42, "viewer_feedback_reaction_key"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_0

    .line 860263
    const/4 v2, 0x1

    .line 860264
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 860265
    :cond_1c
    const/16 v41, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 860266
    if-eqz v13, :cond_1d

    .line 860267
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 860268
    :cond_1d
    if-eqz v12, :cond_1e

    .line 860269
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 860270
    :cond_1e
    if-eqz v11, :cond_1f

    .line 860271
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 860272
    :cond_1f
    if-eqz v10, :cond_20

    .line 860273
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 860274
    :cond_20
    if-eqz v9, :cond_21

    .line 860275
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 860276
    :cond_21
    if-eqz v8, :cond_22

    .line 860277
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 860278
    :cond_22
    if-eqz v7, :cond_23

    .line 860279
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 860280
    :cond_23
    if-eqz v6, :cond_24

    .line 860281
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 860282
    :cond_24
    if-eqz v5, :cond_25

    .line 860283
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 860284
    :cond_25
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 860285
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 860286
    if-eqz v4, :cond_26

    .line 860287
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 860288
    :cond_26
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 860289
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 860290
    if-eqz v3, :cond_27

    .line 860291
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 860292
    :cond_27
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860293
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860294
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860295
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860296
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860297
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860298
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860299
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860300
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860301
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 860302
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 860303
    if-eqz v2, :cond_28

    .line 860304
    const/16 v2, 0x1a

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 860305
    :cond_28
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 860306
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 860307
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 860308
    if-eqz v0, :cond_0

    .line 860309
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860310
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860311
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860312
    if-eqz v0, :cond_1

    .line 860313
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860314
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860315
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860316
    if-eqz v0, :cond_2

    .line 860317
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860318
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860319
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860320
    if-eqz v0, :cond_3

    .line 860321
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860322
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860323
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860324
    if-eqz v0, :cond_4

    .line 860325
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860326
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860327
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860328
    if-eqz v0, :cond_5

    .line 860329
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860330
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860331
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860332
    if-eqz v0, :cond_6

    .line 860333
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860334
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860335
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860336
    if-eqz v0, :cond_7

    .line 860337
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860338
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860339
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860340
    if-eqz v0, :cond_8

    .line 860341
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860342
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860343
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 860344
    if-eqz v0, :cond_9

    .line 860345
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860346
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860347
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 860348
    if-eqz v0, :cond_a

    .line 860349
    const-string v1, "default_comment_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860350
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860351
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860352
    if-eqz v0, :cond_b

    .line 860353
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860354
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860355
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 860356
    if-eqz v0, :cond_c

    .line 860357
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860358
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860359
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860360
    if-eqz v0, :cond_d

    .line 860361
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860362
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860363
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 860364
    if-eqz v0, :cond_e

    .line 860365
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860366
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 860367
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 860368
    if-eqz v0, :cond_f

    .line 860369
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860370
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860371
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860372
    if-eqz v0, :cond_10

    .line 860373
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860374
    invoke-static {p0, v0, p2}, LX/5Ac;->a(LX/15i;ILX/0nX;)V

    .line 860375
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860376
    if-eqz v0, :cond_11

    .line 860377
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860378
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 860379
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860380
    if-eqz v0, :cond_12

    .line 860381
    const-string v1, "real_time_activity_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860382
    invoke-static {p0, v0, p2, p3}, LX/5Ab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860383
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 860384
    if-eqz v0, :cond_13

    .line 860385
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860386
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860387
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860388
    if-eqz v0, :cond_14

    .line 860389
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860390
    invoke-static {p0, v0, p2}, LX/5Ad;->a(LX/15i;ILX/0nX;)V

    .line 860391
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860392
    if-eqz v0, :cond_15

    .line 860393
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860394
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860395
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860396
    if-eqz v0, :cond_16

    .line 860397
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860398
    invoke-static {p0, v0, p2}, LX/5Ae;->a(LX/15i;ILX/0nX;)V

    .line 860399
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860400
    if-eqz v0, :cond_17

    .line 860401
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860402
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860403
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860404
    if-eqz v0, :cond_18

    .line 860405
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860406
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860407
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 860408
    if-eqz v0, :cond_19

    .line 860409
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860410
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 860411
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 860412
    if-eqz v0, :cond_1a

    .line 860413
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860414
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 860415
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 860416
    return-void
.end method
