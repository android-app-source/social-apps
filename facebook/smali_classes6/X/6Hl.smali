.class public LX/6Hl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field public b:Landroid/app/Activity;

.field public c:J

.field private d:Ljava/lang/String;

.field public e:Landroid/content/Intent;

.field private f:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/camera/ipc/CameraActivityAction;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1072813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072814
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/6Hl;->c:J

    .line 1072815
    const/4 v0, 0x0

    iput v0, p0, LX/6Hl;->f:I

    .line 1072816
    iput-object p1, p0, LX/6Hl;->a:Ljava/lang/String;

    .line 1072817
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, LX/6Hl;->e:Landroid/content/Intent;

    .line 1072818
    return-void
.end method

.method public static b(LX/0QB;)LX/6Hl;
    .locals 2

    .prologue
    .line 1072809
    new-instance v1, LX/6Hl;

    .line 1072810
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/6Hm;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1072811
    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, LX/6Hl;-><init>(Ljava/lang/String;)V

    .line 1072812
    return-object v1
.end method


# virtual methods
.method public final a(I)LX/6Hl;
    .locals 2

    .prologue
    .line 1072794
    iget-object v0, p0, LX/6Hl;->e:Landroid/content/Intent;

    const-string v1, "desired_initial_facing"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1072795
    return-object p0
.end method

.method public final a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1072802
    iget-object v0, p0, LX/6Hl;->b:Landroid/app/Activity;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v0

    check-cast v0, LX/0aU;

    iget-object v1, p0, LX/6Hl;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1072803
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "extra_target_id"

    iget-wide v2, p0, LX/6Hl;->c:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source_activity"

    iget-object v2, p0, LX/6Hl;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "publisher_type"

    iget-object v2, p0, LX/6Hl;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1072804
    iget-object v1, p0, LX/6Hl;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1072805
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "From "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6Hl;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/6Hl;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n-> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1072806
    iget v1, p0, LX/6Hl;->f:I

    if-eqz v1, :cond_0

    .line 1072807
    iget v1, p0, LX/6Hl;->f:I

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1072808
    :cond_0
    return-object v0
.end method

.method public final b(Z)LX/6Hl;
    .locals 2

    .prologue
    .line 1072800
    iget-object v0, p0, LX/6Hl;->e:Landroid/content/Intent;

    const-string v1, "extra_disable_video"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1072801
    return-object p0
.end method

.method public final c(Z)LX/6Hl;
    .locals 2

    .prologue
    .line 1072798
    iget-object v0, p0, LX/6Hl;->e:Landroid/content/Intent;

    const-string v1, "return_after_snap"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1072799
    return-object p0
.end method

.method public final d(Z)LX/6Hl;
    .locals 2

    .prologue
    .line 1072796
    iget-object v0, p0, LX/6Hl;->e:Landroid/content/Intent;

    const-string v1, "show_profile_crop_overlay"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1072797
    return-object p0
.end method
