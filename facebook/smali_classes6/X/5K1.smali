.class public final LX/5K1;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5K1;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Jz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5K9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 898100
    const/4 v0, 0x0

    sput-object v0, LX/5K1;->a:LX/5K1;

    .line 898101
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5K1;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 898102
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 898103
    new-instance v0, LX/5K9;

    invoke-direct {v0}, LX/5K9;-><init>()V

    iput-object v0, p0, LX/5K1;->c:LX/5K9;

    .line 898104
    return-void
.end method

.method public static c(LX/1De;)LX/5Jz;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 898105
    new-instance v1, LX/5K0;

    invoke-direct {v1}, LX/5K0;-><init>()V

    .line 898106
    sget-object v2, LX/5K1;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Jz;

    .line 898107
    if-nez v2, :cond_0

    .line 898108
    new-instance v2, LX/5Jz;

    invoke-direct {v2}, LX/5Jz;-><init>()V

    .line 898109
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/5Jz;->a$redex0(LX/5Jz;LX/1De;IILX/5K0;)V

    .line 898110
    move-object v1, v2

    .line 898111
    move-object v0, v1

    .line 898112
    return-object v0
.end method

.method public static declared-synchronized q()LX/5K1;
    .locals 2

    .prologue
    .line 898113
    const-class v1, LX/5K1;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5K1;->a:LX/5K1;

    if-nez v0, :cond_0

    .line 898114
    new-instance v0, LX/5K1;

    invoke-direct {v0}, LX/5K1;-><init>()V

    sput-object v0, LX/5K1;->a:LX/5K1;

    .line 898115
    :cond_0
    sget-object v0, LX/5K1;->a:LX/5K1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 898116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898125
    invoke-static {}, LX/1dS;->b()V

    .line 898126
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7e2eb3d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 898117
    invoke-static {}, LX/5K9;->a()V

    .line 898118
    const/16 v1, 0x1f

    const v2, 0x666a3d79

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1

    .prologue
    .line 898119
    check-cast p3, LX/5K0;

    .line 898120
    iget-object v0, p3, LX/5K0;->c:LX/5Je;

    .line 898121
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result p0

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result p1

    invoke-virtual {v0, p0, p1}, LX/3mY;->b(II)V

    .line 898122
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898123
    new-instance v0, LX/5KB;

    invoke-direct {v0, p1}, LX/5KB;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 898124
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 898089
    check-cast p2, LX/5K0;

    .line 898090
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 898091
    iget-object v0, p2, LX/5K0;->a:LX/1dQ;

    .line 898092
    if-eqz v0, :cond_0

    .line 898093
    new-instance p0, LX/5K8;

    invoke-direct {p0, v0}, LX/5K8;-><init>(LX/1dQ;)V

    .line 898094
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 898095
    :cond_0
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 898096
    check-cast v0, LX/1PH;

    iput-object v0, p2, LX/5K0;->b:LX/1PH;

    .line 898097
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 898098
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 898099
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 5

    .prologue
    .line 898036
    check-cast p1, LX/5K0;

    .line 898037
    check-cast p2, LX/5K0;

    .line 898038
    iget-object v0, p1, LX/5K0;->c:LX/5Je;

    iget-object v1, p2, LX/5K0;->c:LX/5Je;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 898039
    iget-boolean v1, p1, LX/5K0;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p2, LX/5K0;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 898040
    iget-boolean v2, p1, LX/5K0;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p2, LX/5K0;->k:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v2

    .line 898041
    iget v3, p1, LX/5K0;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p2, LX/5K0;->l:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v3

    .line 898042
    const/4 p0, 0x1

    .line 898043
    iget-object v4, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898044
    iget-object p1, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898045
    if-eq v4, p1, :cond_0

    move v4, p0

    .line 898046
    :goto_0
    move v4, v4

    .line 898047
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 898048
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 898049
    invoke-static {v2}, LX/1cy;->a(LX/3lz;)V

    .line 898050
    invoke-static {v3}, LX/1cy;->a(LX/3lz;)V

    .line 898051
    return v4

    .line 898052
    :cond_0
    iget-object v4, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898053
    check-cast v4, Ljava/lang/Boolean;

    .line 898054
    iget-object p1, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898055
    invoke-virtual {v4, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, p0

    .line 898056
    goto :goto_0

    .line 898057
    :cond_1
    iget-object v4, v2, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898058
    check-cast v4, Ljava/lang/Boolean;

    .line 898059
    iget-object p1, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898060
    invoke-virtual {v4, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, p0

    .line 898061
    goto :goto_0

    .line 898062
    :cond_2
    iget-object v4, v3, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898063
    check-cast v4, Ljava/lang/Integer;

    .line 898064
    iget-object p1, v3, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898065
    invoke-virtual {v4, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    move v4, p0

    .line 898066
    goto :goto_0

    .line 898067
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 898068
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 898069
    check-cast p3, LX/5K0;

    .line 898070
    check-cast p2, LX/5KB;

    iget-object v0, p3, LX/5K0;->c:LX/5Je;

    iget-boolean v1, p3, LX/5K0;->j:Z

    iget-boolean v2, p3, LX/5K0;->k:Z

    iget v3, p3, LX/5K0;->l:I

    invoke-static {p2, v0, v1, v2, v3}, LX/5K9;->a(LX/5KB;LX/5Je;ZZI)V

    .line 898071
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 898072
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 898073
    check-cast p3, LX/5K0;

    .line 898074
    check-cast p2, LX/5KB;

    iget-object v0, p3, LX/5K0;->c:LX/5Je;

    invoke-static {p2, v0}, LX/5K9;->a(LX/5KB;LX/5Je;)V

    .line 898075
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 9

    .prologue
    .line 898076
    check-cast p3, LX/5K0;

    .line 898077
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    move-object v0, p2

    .line 898078
    check-cast v0, LX/5KB;

    iget-object v1, p3, LX/5K0;->d:LX/1Of;

    iget-object v2, p3, LX/5K0;->c:LX/5Je;

    iget-object v3, p3, LX/5K0;->e:LX/5K7;

    iget-object v4, p3, LX/5K0;->f:LX/3x6;

    iget-object v5, p3, LX/5K0;->g:LX/1OX;

    iget-boolean v6, p3, LX/5K0;->h:Z

    iget-object v7, p3, LX/5K0;->b:LX/1PH;

    invoke-static/range {v0 .. v8}, LX/5K9;->a(LX/5KB;LX/1Of;LX/5Je;LX/5K7;LX/3x6;LX/1OX;ZLX/1PH;LX/1np;)V

    .line 898079
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 898080
    check-cast v0, LX/1Of;

    iput-object v0, p3, LX/5K0;->i:LX/1Of;

    .line 898081
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 898082
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 898083
    check-cast p3, LX/5K0;

    move-object v0, p2

    .line 898084
    check-cast v0, LX/5KB;

    iget-object v1, p3, LX/5K0;->c:LX/5Je;

    iget-object v2, p3, LX/5K0;->e:LX/5K7;

    iget-object v3, p3, LX/5K0;->f:LX/3x6;

    iget-object v4, p3, LX/5K0;->g:LX/1OX;

    iget-object v5, p3, LX/5K0;->i:LX/1Of;

    invoke-static/range {v0 .. v5}, LX/5K9;->a(LX/5KB;LX/5Je;LX/5K7;LX/3x6;LX/1OX;LX/1Of;)V

    .line 898085
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 898086
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 898088
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 898087
    const/16 v0, 0xf

    return v0
.end method
