.class public final enum LX/5QD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5QD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5QD;

.field public static final enum LOCAL:LX/5QD;

.field public static final enum LOCAL_UPLOADED:LX/5QD;

.field public static final enum REMOTE:LX/5QD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 912389
    new-instance v0, LX/5QD;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2}, LX/5QD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QD;->LOCAL:LX/5QD;

    .line 912390
    new-instance v0, LX/5QD;

    const-string v1, "LOCAL_UPLOADED"

    invoke-direct {v0, v1, v3}, LX/5QD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QD;->LOCAL_UPLOADED:LX/5QD;

    .line 912391
    new-instance v0, LX/5QD;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v4}, LX/5QD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QD;->REMOTE:LX/5QD;

    .line 912392
    const/4 v0, 0x3

    new-array v0, v0, [LX/5QD;

    sget-object v1, LX/5QD;->LOCAL:LX/5QD;

    aput-object v1, v0, v2

    sget-object v1, LX/5QD;->LOCAL_UPLOADED:LX/5QD;

    aput-object v1, v0, v3

    sget-object v1, LX/5QD;->REMOTE:LX/5QD;

    aput-object v1, v0, v4

    sput-object v0, LX/5QD;->$VALUES:[LX/5QD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 912393
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5QD;
    .locals 1

    .prologue
    .line 912394
    const-class v0, LX/5QD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5QD;

    return-object v0
.end method

.method public static values()[LX/5QD;
    .locals 1

    .prologue
    .line 912395
    sget-object v0, LX/5QD;->$VALUES:[LX/5QD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5QD;

    return-object v0
.end method
