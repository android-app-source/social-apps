.class public LX/6Q2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47S;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6Qw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6Qw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086705
    iput-object p1, p0, LX/6Q2;->a:LX/0Or;

    .line 1086706
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1086707
    if-eqz p4, :cond_0

    .line 1086708
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1086709
    const-string v1, "app_details_dialog"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p4, v0

    .line 1086710
    :cond_0
    invoke-static {p3}, LX/6Qv;->a(Landroid/content/Intent;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v0

    .line 1086711
    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1086712
    if-eqz v0, :cond_2

    .line 1086713
    iget-object v0, p0, LX/6Q2;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qw;

    const/4 p3, 0x1

    .line 1086714
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1086715
    if-eqz p4, :cond_1

    .line 1086716
    invoke-virtual {v1, p4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1086717
    :cond_1
    const-string v2, "did_direct_install"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086718
    iget-object v2, v0, LX/6Qw;->e:LX/17d;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v2, p1, p0}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 1086719
    if-eqz v2, :cond_4

    .line 1086720
    iget-object p0, v0, LX/6Qw;->d:LX/0Ot;

    invoke-static {p1, v2, p0, v1}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;Ljava/util/Map;)V

    .line 1086721
    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1086722
    :cond_4
    const-string v1, "Could not log open_application for direct install ad  with url %s"

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1086723
    sget-object v2, LX/6Qw;->a:Ljava/lang/String;

    invoke-static {v2, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1086724
    iput p3, v1, LX/0VK;->e:I

    .line 1086725
    move-object v1, v1

    .line 1086726
    iput-boolean p3, v1, LX/0VK;->d:Z

    .line 1086727
    move-object v1, v1

    .line 1086728
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 1086729
    iget-object v2, v0, LX/6Qw;->c:LX/03V;

    invoke-virtual {v2, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_1
.end method
