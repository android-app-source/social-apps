.class public LX/6VX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/6VX;


# instance fields
.field private final b:LX/0SG;

.field public final c:LX/0Uh;

.field public final d:LX/0lB;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/platformads/TrackedPackage;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1104246
    const-class v0, LX/6VX;

    sput-object v0, LX/6VX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Uh;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1104238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104239
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6VX;->g:Z

    .line 1104240
    iput-object p1, p0, LX/6VX;->b:LX/0SG;

    .line 1104241
    iput-object p2, p0, LX/6VX;->c:LX/0Uh;

    .line 1104242
    iput-object p4, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1104243
    iput-object p3, p0, LX/6VX;->d:LX/0lB;

    .line 1104244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/6VX;->f:Ljava/util/Map;

    .line 1104245
    return-void
.end method

.method public static a(LX/0QB;)LX/6VX;
    .locals 7

    .prologue
    .line 1104225
    sget-object v0, LX/6VX;->h:LX/6VX;

    if-nez v0, :cond_1

    .line 1104226
    const-class v1, LX/6VX;

    monitor-enter v1

    .line 1104227
    :try_start_0
    sget-object v0, LX/6VX;->h:LX/6VX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1104228
    if-eqz v2, :cond_0

    .line 1104229
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1104230
    new-instance p0, LX/6VX;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, LX/6VX;-><init>(LX/0SG;LX/0Uh;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1104231
    move-object v0, p0

    .line 1104232
    sput-object v0, LX/6VX;->h:LX/6VX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104233
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1104234
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1104235
    :cond_1
    sget-object v0, LX/6VX;->h:LX/6VX;

    return-object v0

    .line 1104236
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1104237
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6VX;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/platformads/TrackedPackage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1104217
    invoke-static {p0}, LX/6VX;->b(LX/6VX;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104218
    :goto_0
    return-void

    .line 1104219
    :cond_0
    iget-object v0, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1104220
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/platformads/TrackedPackage;

    .line 1104221
    sget-object v3, LX/6VY;->b:LX/0Tn;

    iget-object v0, v0, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_1

    .line 1104222
    :cond_1
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;JLX/162;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1104247
    new-instance v0, Lcom/facebook/feed/platformads/TrackedPackage;

    invoke-direct {v0}, Lcom/facebook/feed/platformads/TrackedPackage;-><init>()V

    .line 1104248
    iput-object p5, v0, Lcom/facebook/feed/platformads/TrackedPackage;->appLaunchUrl:Ljava/lang/String;

    .line 1104249
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    .line 1104250
    iput-object p1, v0, Lcom/facebook/feed/platformads/TrackedPackage;->packageName:Ljava/lang/String;

    .line 1104251
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, LX/6VX;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v2, p6

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v0, Lcom/facebook/feed/platformads/TrackedPackage;->trackUntil:Ljava/util/Date;

    .line 1104252
    iput-object p4, v0, Lcom/facebook/feed/platformads/TrackedPackage;->trackingCodes:LX/162;

    .line 1104253
    invoke-static {p0}, LX/6VX;->c(LX/6VX;)V

    .line 1104254
    iget-object v1, p0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1104255
    iget-object v1, p0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104256
    invoke-static {p0, v0}, LX/6VX;->b(LX/6VX;Lcom/facebook/feed/platformads/TrackedPackage;)V

    .line 1104257
    :cond_0
    iget-object v1, p0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104258
    invoke-static {p0}, LX/6VX;->b(LX/6VX;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1104259
    :goto_0
    return-void

    .line 1104260
    :cond_1
    iget-object v0, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 1104261
    iget-object v0, p0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/platformads/TrackedPackage;

    .line 1104262
    :try_start_0
    sget-object v1, LX/6VY;->b:LX/0Tn;

    iget-object p1, v0, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    invoke-virtual {v1, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    iget-object p1, p0, LX/6VX;->d:LX/0lB;

    invoke-virtual {p1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1104263
    :catch_0
    goto :goto_1

    .line 1104264
    :cond_2
    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static b(LX/6VX;Lcom/facebook/feed/platformads/TrackedPackage;)V
    .locals 2

    .prologue
    .line 1104223
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/platformads/TrackedPackage;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/6VX;->a(LX/6VX;Ljava/util/List;)V

    .line 1104224
    return-void
.end method

.method public static b(LX/6VX;)Z
    .locals 1

    .prologue
    .line 1104214
    :try_start_0
    iget-object v0, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1104215
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1104216
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized c(LX/6VX;)V
    .locals 7

    .prologue
    .line 1104192
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/6VX;->g:Z

    if-nez v0, :cond_0

    .line 1104193
    invoke-static {p0}, LX/6VX;->b(LX/6VX;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1104194
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6VX;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104195
    :cond_0
    monitor-exit p0

    return-void

    .line 1104196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1104197
    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1104198
    iget-object v0, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6VY;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 1104199
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 1104200
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1104201
    iget-object v1, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v5, 0x0

    invoke-interface {v1, v0, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1104202
    if-eqz v1, :cond_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104203
    :try_start_2
    iget-object v5, p0, LX/6VX;->d:LX/0lB;

    const-class v6, Lcom/facebook/feed/platformads/TrackedPackage;

    invoke-virtual {v5, v1, v6}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/platformads/TrackedPackage;

    .line 1104204
    if-eqz v1, :cond_2

    .line 1104205
    iget-object v5, v1, Lcom/facebook/feed/platformads/TrackedPackage;->trackUntil:Ljava/util/Date;

    invoke-virtual {v5, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1104206
    iget-object v5, p0, LX/6VX;->f:Ljava/util/Map;

    iget-object v6, v1, Lcom/facebook/feed/platformads/TrackedPackage;->packageName:Ljava/lang/String;

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    goto :goto_1

    .line 1104207
    :catch_0
    invoke-static {p0}, LX/6VX;->b(LX/6VX;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1104208
    :goto_2
    goto :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1104209
    :cond_3
    :try_start_4
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1104210
    :cond_4
    invoke-static {p0, v2}, LX/6VX;->a(LX/6VX;Ljava/util/List;)V

    goto :goto_0

    .line 1104211
    :cond_5
    iget-object v1, p0, LX/6VX;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1104212
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1104213
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/162;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1104190
    const-wide v6, 0x90321000L

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, LX/6VX;->a(Ljava/lang/String;JLX/162;Ljava/lang/String;J)V

    .line 1104191
    return-void
.end method
