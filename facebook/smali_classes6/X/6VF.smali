.class public final enum LX/6VF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VF;

.field public static final enum LARGE:LX/6VF;

.field public static final enum MEDIUM:LX/6VF;

.field public static final enum SMALL:LX/6VF;

.field public static final enum XLARGE:LX/6VF;

.field public static final enum XSMALL:LX/6VF;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103774
    new-instance v0, LX/6VF;

    const-string v1, "XSMALL"

    invoke-direct {v0, v1, v2}, LX/6VF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VF;->XSMALL:LX/6VF;

    .line 1103775
    new-instance v0, LX/6VF;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, LX/6VF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VF;->SMALL:LX/6VF;

    .line 1103776
    new-instance v0, LX/6VF;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v4}, LX/6VF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VF;->MEDIUM:LX/6VF;

    .line 1103777
    new-instance v0, LX/6VF;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v5}, LX/6VF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VF;->LARGE:LX/6VF;

    .line 1103778
    new-instance v0, LX/6VF;

    const-string v1, "XLARGE"

    invoke-direct {v0, v1, v6}, LX/6VF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VF;->XLARGE:LX/6VF;

    .line 1103779
    const/4 v0, 0x5

    new-array v0, v0, [LX/6VF;

    sget-object v1, LX/6VF;->XSMALL:LX/6VF;

    aput-object v1, v0, v2

    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    aput-object v1, v0, v3

    sget-object v1, LX/6VF;->MEDIUM:LX/6VF;

    aput-object v1, v0, v4

    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    aput-object v1, v0, v5

    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    aput-object v1, v0, v6

    sput-object v0, LX/6VF;->$VALUES:[LX/6VF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103780
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VF;
    .locals 1

    .prologue
    .line 1103781
    const-class v0, LX/6VF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VF;

    return-object v0
.end method

.method public static values()[LX/6VF;
    .locals 1

    .prologue
    .line 1103782
    sget-object v0, LX/6VF;->$VALUES:[LX/6VF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VF;

    return-object v0
.end method
