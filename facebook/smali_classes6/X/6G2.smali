.class public LX/6G2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final n:Ljava/lang/String;

.field private static volatile o:LX/6G2;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0TD;

.field private final c:Ljava/util/concurrent/Executor;

.field public final d:LX/03V;

.field public final e:LX/6G0;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Zd;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0kL;

.field private final h:LX/6V0;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/6GZ;

.field private final k:LX/03R;

.field public final l:LX/6Ft;

.field public m:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1069745
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/6G2;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6G2;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0TD;Ljava/util/concurrent/Executor;LX/03V;LX/6G0;Ljava/util/Set;LX/0kL;LX/6V0;Lcom/facebook/content/SecureContextHelper;LX/6GZ;LX/03R;LX/6Ft;LX/0Uh;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/6G0;",
            "Ljava/util/Set",
            "<",
            "LX/0Zd;",
            ">;",
            "LX/0kL;",
            "LX/6V0;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6GZ;",
            "LX/03R;",
            "LX/6Ft;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1069730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069731
    iput-object p1, p0, LX/6G2;->a:Landroid/content/res/Resources;

    .line 1069732
    iput-object p2, p0, LX/6G2;->b:LX/0TD;

    .line 1069733
    iput-object p3, p0, LX/6G2;->c:Ljava/util/concurrent/Executor;

    .line 1069734
    iput-object p4, p0, LX/6G2;->d:LX/03V;

    .line 1069735
    iput-object p5, p0, LX/6G2;->e:LX/6G0;

    .line 1069736
    iput-object p6, p0, LX/6G2;->f:Ljava/util/Set;

    .line 1069737
    iput-object p7, p0, LX/6G2;->g:LX/0kL;

    .line 1069738
    iput-object p8, p0, LX/6G2;->h:LX/6V0;

    .line 1069739
    iput-object p9, p0, LX/6G2;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1069740
    iput-object p10, p0, LX/6G2;->j:LX/6GZ;

    .line 1069741
    iput-object p11, p0, LX/6G2;->k:LX/03R;

    .line 1069742
    iput-object p12, p0, LX/6G2;->l:LX/6Ft;

    .line 1069743
    iput-object p13, p0, LX/6G2;->m:LX/0Uh;

    .line 1069744
    return-void
.end method

.method public static a(LX/0QB;)LX/6G2;
    .locals 3

    .prologue
    .line 1069720
    sget-object v0, LX/6G2;->o:LX/6G2;

    if-nez v0, :cond_1

    .line 1069721
    const-class v1, LX/6G2;

    monitor-enter v1

    .line 1069722
    :try_start_0
    sget-object v0, LX/6G2;->o:LX/6G2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1069723
    if-eqz v2, :cond_0

    .line 1069724
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/6G2;->b(LX/0QB;)LX/6G2;

    move-result-object v0

    sput-object v0, LX/6G2;->o:LX/6G2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1069725
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1069726
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1069727
    :cond_1
    sget-object v0, LX/6G2;->o:LX/6G2;

    return-object v0

    .line 1069728
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1069729
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/6G2;Landroid/content/Context;ILjava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1069685
    const/4 v0, 0x0

    .line 1069686
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1069687
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 1069688
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1069689
    if-eqz v1, :cond_0

    .line 1069690
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1069691
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1069692
    const v0, 0x7f0818d9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1069693
    :goto_1
    iget-object v0, p0, LX/6G2;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1069694
    iget-object v2, p0, LX/6G2;->b:LX/0TD;

    invoke-interface {v2, p3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1069695
    invoke-static {v0, v4, v5}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 1069696
    iget-object v3, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069697
    if-eqz v3, :cond_4

    .line 1069698
    iget-object v3, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069699
    invoke-virtual {v3, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1069700
    :goto_2
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1069701
    new-instance v1, LX/6Fy;

    invoke-direct {v1, p0, v2}, LX/6Fy;-><init>(LX/6G2;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1069702
    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 1069703
    const/16 v1, 0x7d2

    .line 1069704
    iget-object v3, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069705
    if-eqz v3, :cond_5

    .line 1069706
    iget-object v3, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069707
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Window;->setType(I)V

    .line 1069708
    :cond_1
    :goto_3
    instance-of v1, p1, LX/0ew;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 1069709
    check-cast v1, LX/0ew;

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v3, "bug_report_in_progress"

    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1069710
    :cond_2
    new-instance v1, LX/6Fz;

    invoke-direct {v1, p0, p1, v0}, LX/6Fz;-><init>(LX/6G2;Landroid/content/Context;Lcom/facebook/ui/dialogs/ProgressDialogFragment;)V

    .line 1069711
    iget-object v3, p0, LX/6G2;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1069712
    invoke-virtual {v0, v4}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1069713
    return-object v2

    .line 1069714
    :catch_0
    move-exception v1

    .line 1069715
    const-string v2, "BugReporter"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 1069716
    :cond_4
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1069717
    const-string v5, "title"

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1069718
    :cond_5
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1069719
    const-string v5, "window_type"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3
.end method

.method public static a(LX/0gc;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1069676
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mAdded"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1069677
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1069678
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1069679
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069680
    if-eqz v0, :cond_0

    .line 1069681
    :goto_0
    return-object v0

    .line 1069682
    :catch_0
    move-exception v0

    .line 1069683
    const-string v1, "BugReporter"

    const-string v2, "Could not access fragment list for screenshot."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069684
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0f6;Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0f6;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1069670
    :try_start_0
    invoke-interface {p1}, LX/0f6;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    .line 1069671
    if-eqz v0, :cond_0

    .line 1069672
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069673
    :cond_0
    :goto_0
    return-void

    .line 1069674
    :catch_0
    move-exception v0

    .line 1069675
    iget-object v1, p0, LX/6G2;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/6G2;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "addComponentDebugInfo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(LX/0gc;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1069661
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1069662
    :cond_0
    return-void

    .line 1069663
    :cond_1
    sget-object v3, LX/0jj;->c:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 1069664
    invoke-virtual {p1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 1069665
    instance-of v0, v1, LX/0f6;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1069666
    check-cast v0, LX/0f6;

    invoke-direct {p0, v0, p2}, LX/6G2;->a(LX/0f6;Ljava/util/HashMap;)V

    .line 1069667
    :cond_2
    if-eqz v1, :cond_3

    .line 1069668
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/6G2;->a(LX/0gc;Ljava/util/HashMap;)V

    .line 1069669
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public static a(LX/6G2;Ljava/util/List;Landroid/graphics/Canvas;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/graphics/Canvas;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1069643
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1069644
    instance-of v1, v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1069645
    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 1069646
    const/4 p1, 0x1

    const/4 v7, 0x0

    .line 1069647
    iget-boolean v3, v1, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v3, v3

    .line 1069648
    if-nez v3, :cond_0

    .line 1069649
    iget-object v3, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069650
    if-eqz v3, :cond_0

    .line 1069651
    iget-object v3, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v3, v3

    .line 1069652
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 1069653
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 1069654
    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1069655
    aget v5, v4, v7

    int-to-float v5, v5

    aget v6, v4, p1

    int-to-float v6, v6

    invoke-virtual {p2, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1069656
    invoke-virtual {v3, p2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1069657
    aget v3, v4, v7

    neg-int v3, v3

    int-to-float v3, v3

    aget v4, v4, p1

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1069658
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1069659
    invoke-static {v0}, LX/6G2;->a(LX/0gc;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/6G2;->a(LX/6G2;Ljava/util/List;Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1069660
    :cond_1
    return-void
.end method

.method private static b(LX/0QB;)LX/6G2;
    .locals 14

    .prologue
    .line 1069538
    new-instance v0, LX/6G2;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/FB1;->a(LX/0QB;)LX/FB1;

    move-result-object v5

    check-cast v5, LX/6G0;

    invoke-static {p0}, LX/43T;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {p0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v8

    check-cast v8, LX/6V0;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/6GZ;->b(LX/0QB;)LX/6GZ;

    move-result-object v10

    check-cast v10, LX/6GZ;

    invoke-static {p0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v11

    check-cast v11, LX/03R;

    invoke-static {p0}, LX/6Ft;->b(LX/0QB;)LX/6Ft;

    move-result-object v12

    check-cast v12, LX/6Ft;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v0 .. v13}, LX/6G2;-><init>(Landroid/content/res/Resources;LX/0TD;Ljava/util/concurrent/Executor;LX/03V;LX/6G0;Ljava/util/Set;LX/0kL;LX/6V0;Lcom/facebook/content/SecureContextHelper;LX/6GZ;LX/03R;LX/6Ft;LX/0Uh;)V

    .line 1069539
    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1069636
    const-class v0, LX/18x;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18x;

    .line 1069637
    if-eqz v0, :cond_0

    .line 1069638
    invoke-interface {v0}, LX/18x;->a()Landroid/view/View;

    move-result-object v0

    .line 1069639
    :goto_0
    return-object v0

    .line 1069640
    :cond_0
    const-class v0, Landroid/app/Activity;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1069641
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069642
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6FY;)V
    .locals 10

    .prologue
    .line 1069564
    iget-object v0, p0, LX/6G2;->j:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_BEGIN_FLOW:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1069565
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1069566
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    instance-of v0, v0, LX/0f6;

    if-eqz v0, :cond_0

    .line 1069567
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    check-cast v0, LX/0f6;

    invoke-direct {p0, v0, v1}, LX/6G2;->a(LX/0f6;Ljava/util/HashMap;)V

    .line 1069568
    :cond_0
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    instance-of v0, v0, LX/0ew;

    if-eqz v0, :cond_1

    .line 1069569
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    check-cast v0, LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 1069570
    invoke-direct {p0, v0, v1}, LX/6G2;->a(LX/0gc;Ljava/util/HashMap;)V

    .line 1069571
    :cond_1
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 1069572
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1069573
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1069574
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1069575
    const-string v3, "intent_extras"

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069576
    :cond_2
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    instance-of v0, v0, LX/0f2;

    if-eqz v0, :cond_3

    .line 1069577
    iget-object v0, p1, LX/6FY;->a:Landroid/content/Context;

    check-cast v0, LX/0f2;

    .line 1069578
    :try_start_0
    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v2

    .line 1069579
    if-eqz v2, :cond_3

    .line 1069580
    const-string v3, "activity_analytics_tag"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1069581
    :cond_3
    :goto_0
    iget-object v0, p0, LX/6G2;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zd;

    .line 1069582
    :try_start_1
    invoke-interface {v0}, LX/0Zd;->c()Ljava/util/Map;

    move-result-object v0

    .line 1069583
    if-eqz v0, :cond_4

    .line 1069584
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1069585
    :catch_0
    move-exception v0

    .line 1069586
    iget-object v3, p0, LX/6G2;->d:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/6G2;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "addExtraDataFromUI"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1069587
    :cond_5
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    .line 1069588
    const/4 v0, 0x0

    .line 1069589
    iget-object v3, p1, LX/6FY;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v3, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_6

    iget-object v3, p1, LX/6FY;->a:Landroid/content/Context;

    const-class v4, LX/18x;

    invoke-static {v3, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 1069590
    :cond_6
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1069591
    iget-object v3, p0, LX/6G2;->m:LX/0Uh;

    const/16 v6, 0x37b

    invoke-virtual {v3, v6}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    .line 1069592
    sget-object v6, LX/03R;->YES:LX/03R;

    invoke-virtual {v6, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1069593
    :cond_7
    :goto_2
    move v1, v4

    .line 1069594
    if-eqz v1, :cond_a

    .line 1069595
    iget-object v1, p1, LX/6FY;->a:Landroid/content/Context;

    const/4 v5, 0x0

    .line 1069596
    const-class v3, Landroid/app/Activity;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 1069597
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1069598
    invoke-virtual {v3}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v3

    move-object v1, v3

    .line 1069599
    :cond_8
    invoke-static {v1}, LX/6G2;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v6

    .line 1069600
    :try_start_2
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1069601
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1069602
    invoke-virtual {v6, v7}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1069603
    instance-of v8, v3, LX/0ew;

    if-eqz v8, :cond_9

    .line 1069604
    check-cast v3, LX/0ew;

    invoke-interface {v3}, LX/0ew;->iC_()LX/0gc;

    move-result-object v3

    .line 1069605
    invoke-static {v3}, LX/6G2;->a(LX/0gc;)Ljava/util/List;

    move-result-object v3

    .line 1069606
    const/4 v8, 0x2

    new-array v8, v8, [I

    .line 1069607
    invoke-virtual {v6, v8}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1069608
    const/4 v6, 0x0

    aget v6, v8, v6

    neg-int v6, v6

    int-to-float v6, v6

    const/4 v9, 0x1

    aget v9, v8, v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v7, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1069609
    invoke-static {p0, v3, v7}, LX/6G2;->a(LX/6G2;Ljava/util/List;Landroid/graphics/Canvas;)V

    .line 1069610
    const/4 v3, 0x0

    aget v3, v8, v3

    int-to-float v3, v3

    const/4 v6, 0x1

    aget v6, v8, v6

    int-to-float v6, v6

    invoke-virtual {v7, v3, v6}, Landroid/graphics/Canvas;->translate(FF)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_9
    move-object v3, v4

    .line 1069611
    :goto_3
    move-object v1, v3

    .line 1069612
    if-eqz v1, :cond_a

    .line 1069613
    iget-object v3, p1, LX/6FY;->e:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1069614
    :cond_a
    sget-object v1, LX/03R;->YES:LX/03R;

    iget-object v3, p0, LX/6G2;->k:LX/03R;

    invoke-virtual {v1, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1069615
    iget-object v0, p0, LX/6G2;->h:LX/6V0;

    iget-object v1, p1, LX/6FY;->a:Landroid/content/Context;

    invoke-static {v1}, LX/6G2;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    sget-object v3, LX/6Uz;->ALL:LX/6Uz;

    invoke-virtual {v0, v1, v3}, LX/6V0;->a(Landroid/view/View;LX/6Uz;)Landroid/os/Bundle;

    move-result-object v0

    .line 1069616
    :cond_b
    iget-object v1, p1, LX/6FY;->a:Landroid/content/Context;

    const v3, 0x7f0818f8

    new-instance v4, LX/6Fu;

    invoke-direct {v4, p0, p1, v0, v2}, LX/6Fu;-><init>(LX/6G2;LX/6FY;Landroid/os/Bundle;LX/0P1;)V

    invoke-static {p0, v1, v3, v4}, LX/6G2;->a(LX/6G2;Landroid/content/Context;ILjava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1069617
    new-instance v1, LX/6Fv;

    invoke-direct {v1, p0, p1}, LX/6Fv;-><init>(LX/6G2;LX/6FY;)V

    .line 1069618
    iget-object v2, p0, LX/6G2;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1069619
    return-void

    .line 1069620
    :catch_1
    move-exception v2

    .line 1069621
    iget-object v3, p0, LX/6G2;->d:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/6G2;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "addActivityAnalyticsInfo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1069622
    :cond_c
    const-string v3, "activity_analytics_tag"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1069623
    if-eqz v3, :cond_d

    const-string v6, "bookmarks"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    move v3, v4

    .line 1069624
    :goto_4
    iget-object v6, p1, LX/6FY;->b:LX/6Fb;

    sget-object v7, LX/6Fb;->SETTINGS_REPORT_PROBLEM:LX/6Fb;

    if-ne v6, v7, :cond_e

    move v6, v4

    .line 1069625
    :goto_5
    if-eqz v6, :cond_7

    if-eqz v3, :cond_7

    move v4, v5

    goto/16 :goto_2

    :cond_d
    move v3, v5

    .line 1069626
    goto :goto_4

    :cond_e
    move v6, v5

    .line 1069627
    goto :goto_5

    .line 1069628
    :catch_2
    move-exception v3

    .line 1069629
    iget-object v4, p0, LX/6G2;->g:LX/0kL;

    new-instance v6, LX/27k;

    const-string v7, "Insufficient memory to capture a screenshot. Sorry!"

    invoke-direct {v6, v7}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v6}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1069630
    iget-object v4, p0, LX/6G2;->d:LX/03V;

    const-string v6, "BugReporter"

    const-string v7, "Out of memory while creating screenshot"

    invoke-virtual {v4, v6, v7, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v5

    .line 1069631
    goto/16 :goto_3

    .line 1069632
    :catch_3
    move-exception v3

    .line 1069633
    iget-object v4, p0, LX/6G2;->g:LX/0kL;

    new-instance v6, LX/27k;

    const-string v7, "Failed to capture a screenshot. Sorry!"

    invoke-direct {v6, v7}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v6}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1069634
    iget-object v4, p0, LX/6G2;->d:LX/03V;

    const-string v6, "BugReporter"

    const-string v7, "Exception while creating screenshot"

    invoke-virtual {v4, v6, v7, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v5

    .line 1069635
    goto/16 :goto_3
.end method

.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1069544
    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v0

    invoke-virtual {v0}, LX/6FX;->a()LX/6FY;

    move-result-object v0

    .line 1069545
    iget-object v1, v0, LX/6FY;->a:Landroid/content/Context;

    .line 1069546
    iget-object v2, p0, LX/6G2;->j:LX/6GZ;

    sget-object v3, LX/6GY;->RAP_BEGIN_FLOW:LX/6GY;

    invoke-virtual {v2, v3}, LX/6GZ;->a(LX/6GY;)V

    .line 1069547
    iget-object v2, p0, LX/6G2;->e:LX/6G0;

    invoke-interface {v2}, LX/6G0;->b()LX/0Px;

    move-result-object v2

    .line 1069548
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1069549
    :cond_0
    invoke-virtual {p0, v0}, LX/6G2;->a(LX/6FY;)V

    .line 1069550
    :cond_1
    :goto_0
    return-void

    .line 1069551
    :cond_2
    instance-of v3, v1, LX/0ew;

    if-eqz v3, :cond_3

    .line 1069552
    check-cast v1, LX/0ew;

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 1069553
    const-string v3, "bug_reporter_chooser"

    invoke-virtual {v1, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 1069554
    if-nez v3, :cond_1

    .line 1069555
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1069556
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069557
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1069558
    const-string p1, "CHOOSER_OPTIONS"

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {v3, p1, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1069559
    new-instance p1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-direct {p1}, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;-><init>()V

    .line 1069560
    invoke-virtual {p1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1069561
    move-object v2, p1

    .line 1069562
    const-string v3, "bug_reporter_chooser"

    const/4 p1, 0x1

    invoke-virtual {v2, v1, v3, p1}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    goto :goto_0

    .line 1069563
    :cond_3
    invoke-virtual {p0, v0}, LX/6G2;->a(LX/6FY;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/6Fb;LX/0Rf;LX/0am;Ljava/util/List;Landroid/net/Uri;LX/0P1;)V
    .locals 9
    .param p6    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6Fb;",
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/net/Uri;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1069540
    const v8, 0x7f0818f8

    new-instance v0, LX/6Fw;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    move-object v4, p1

    move-object/from16 v5, p7

    move-object v6, p3

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/6Fw;-><init>(LX/6G2;Ljava/util/List;Landroid/net/Uri;Landroid/content/Context;LX/0P1;LX/0Rf;LX/6Fb;)V

    invoke-static {p0, p1, v8, v0}, LX/6G2;->a(LX/6G2;Landroid/content/Context;ILjava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1069541
    new-instance v1, LX/6Fx;

    invoke-direct {v1, p0, p1, p4}, LX/6Fx;-><init>(LX/6G2;Landroid/content/Context;LX/0am;)V

    .line 1069542
    iget-object v2, p0, LX/6G2;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1069543
    return-void
.end method
