.class public LX/6QI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6QI;


# instance fields
.field private a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1087245
    iput-object p1, p0, LX/6QI;->a:LX/0Zb;

    .line 1087246
    return-void
.end method

.method public static a(LX/0QB;)LX/6QI;
    .locals 4

    .prologue
    .line 1087247
    sget-object v0, LX/6QI;->b:LX/6QI;

    if-nez v0, :cond_1

    .line 1087248
    const-class v1, LX/6QI;

    monitor-enter v1

    .line 1087249
    :try_start_0
    sget-object v0, LX/6QI;->b:LX/6QI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1087250
    if-eqz v2, :cond_0

    .line 1087251
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1087252
    new-instance p0, LX/6QI;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/6QI;-><init>(LX/0Zb;)V

    .line 1087253
    move-object v0, p0

    .line 1087254
    sput-object v0, LX/6QI;->b:LX/6QI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1087255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1087256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1087257
    :cond_1
    sget-object v0, LX/6QI;->b:LX/6QI;

    return-object v0

    .line 1087258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1087259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6QI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1087260
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1087261
    if-eqz p5, :cond_0

    .line 1087262
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1087263
    :cond_0
    const-string v1, "interaction_type"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087264
    const-string v1, "interaction_position"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087265
    const-string v1, "neko_di_app_details_screenshot_interaction"

    invoke-virtual {p0, v1, p2, p3, v0}, LX/6QI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1087266
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1087267
    iget-object v0, p0, LX/6QI;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "app_details"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1087268
    iput-object p3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1087269
    move-object v1, v1

    .line 1087270
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1087271
    return-void
.end method
