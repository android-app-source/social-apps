.class public final LX/64V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1044634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044635
    iput v0, p0, LX/64V;->c:I

    .line 1044636
    iput v0, p0, LX/64V;->d:I

    .line 1044637
    iput v0, p0, LX/64V;->e:I

    return-void
.end method


# virtual methods
.method public final a(ILjava/util/concurrent/TimeUnit;)LX/64V;
    .locals 4

    .prologue
    .line 1044628
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxStale < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044629
    :cond_0
    int-to-long v0, p1

    invoke-virtual {p2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    .line 1044630
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const v0, 0x7fffffff

    :goto_0
    iput v0, p0, LX/64V;->d:I

    .line 1044631
    return-object p0

    .line 1044632
    :cond_1
    long-to-int v0, v0

    goto :goto_0
.end method

.method public final d()LX/64W;
    .locals 2

    .prologue
    .line 1044633
    new-instance v0, LX/64W;

    invoke-direct {v0, p0}, LX/64W;-><init>(LX/64V;)V

    return-object v0
.end method
