.class public final LX/5fH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5fM;

.field public final synthetic b:Landroid/graphics/SurfaceTexture;

.field public final synthetic c:I

.field public final synthetic d:LX/5fO;

.field public final synthetic e:LX/5fO;

.field public final synthetic f:I

.field public final synthetic g:I

.field public final synthetic h:LX/5fi;

.field public final synthetic i:LX/5fQ;


# direct methods
.method public constructor <init>(LX/5fQ;LX/5fM;Landroid/graphics/SurfaceTexture;ILX/5fO;LX/5fO;IILX/5fi;)V
    .locals 0

    .prologue
    .line 971155
    iput-object p1, p0, LX/5fH;->i:LX/5fQ;

    iput-object p2, p0, LX/5fH;->a:LX/5fM;

    iput-object p3, p0, LX/5fH;->b:Landroid/graphics/SurfaceTexture;

    iput p4, p0, LX/5fH;->c:I

    iput-object p5, p0, LX/5fH;->d:LX/5fO;

    iput-object p6, p0, LX/5fH;->e:LX/5fO;

    iput p7, p0, LX/5fH;->f:I

    iput p8, p0, LX/5fH;->g:I

    iput-object p9, p0, LX/5fH;->h:LX/5fi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 971112
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 971113
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 971114
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 971115
    iget-object v0, p0, LX/5fH;->a:LX/5fM;

    invoke-static {v0}, LX/5fQ;->b(LX/5fM;)I

    move-result v0

    .line 971116
    iget-object v1, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v2, p0, LX/5fH;->b:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 971117
    iget-object v1, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->d:Landroid/hardware/Camera;

    iget v2, p0, LX/5fH;->c:I

    invoke-static {v2, v0}, LX/5fQ;->d(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 971118
    iget-object v1, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-static {v1, v0}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v8

    .line 971119
    invoke-virtual {v8, v9}, LX/5fS;->c(Z)V

    .line 971120
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, p0, LX/5fH;->d:LX/5fO;

    iget-object v2, p0, LX/5fH;->e:LX/5fO;

    iget v3, p0, LX/5fH;->f:I

    iget v4, p0, LX/5fH;->g:I

    iget-object v5, p0, LX/5fH;->h:LX/5fi;

    .line 971121
    invoke-static/range {v0 .. v5}, LX/5fQ;->a$redex0(LX/5fQ;LX/5fO;LX/5fO;IILX/5fi;)V

    .line 971122
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, p0, LX/5fH;->b:Landroid/graphics/SurfaceTexture;

    .line 971123
    iput-object v1, v0, LX/5fQ;->e:Landroid/graphics/SurfaceTexture;

    .line 971124
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget v1, p0, LX/5fH;->c:I

    .line 971125
    iput v1, v0, LX/5fQ;->f:I

    .line 971126
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget v1, p0, LX/5fH;->f:I

    .line 971127
    iput v1, v0, LX/5fQ;->g:I

    .line 971128
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget v1, p0, LX/5fH;->g:I

    .line 971129
    iput v1, v0, LX/5fQ;->h:I

    .line 971130
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, p0, LX/5fH;->d:LX/5fO;

    .line 971131
    iput-object v1, v0, LX/5fQ;->k:LX/5fO;

    .line 971132
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, p0, LX/5fH;->e:LX/5fO;

    .line 971133
    iput-object v1, v0, LX/5fQ;->j:LX/5fO;

    .line 971134
    invoke-virtual {v8}, LX/5fS;->u()V

    .line 971135
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    invoke-virtual {v8}, LX/5fS;->f()Z

    move-result v1

    .line 971136
    iput-boolean v1, v0, LX/5fQ;->n:Z

    .line 971137
    invoke-virtual {v8}, LX/5fS;->w()V

    .line 971138
    invoke-virtual {v8}, LX/5fS;->x()V

    .line 971139
    invoke-virtual {v8}, LX/5fS;->A()V

    .line 971140
    invoke-virtual {v8}, LX/5fS;->y()V

    .line 971141
    invoke-virtual {v8}, LX/5fS;->z()V

    .line 971142
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    .line 971143
    iput-boolean v9, v0, LX/5fQ;->l:Z

    .line 971144
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    .line 971145
    iput-boolean v10, v0, LX/5fQ;->m:Z

    .line 971146
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    invoke-static {v0, v9}, LX/5fQ;->c(LX/5fQ;Z)V

    .line 971147
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    new-instance v1, LX/5fP;

    iget-object v2, p0, LX/5fH;->i:LX/5fQ;

    invoke-direct {v1, v2}, LX/5fP;-><init>(LX/5fQ;)V

    .line 971148
    iput-object v1, v0, LX/5fQ;->s:LX/5fP;

    .line 971149
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fH;->i:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->s:LX/5fP;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    .line 971150
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    const v1, 0x3115e336

    invoke-static {v0, v1}, LX/0J2;->b(Landroid/hardware/Camera;I)V

    .line 971151
    iget-object v0, p0, LX/5fH;->i:LX/5fQ;

    invoke-static {v0}, LX/5fQ;->y(LX/5fQ;)V

    .line 971152
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "time to setPreviewSurfaceTexture:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 971153
    invoke-virtual {v8}, LX/5fS;->r()Landroid/hardware/Camera$Size;

    move-result-object v0

    return-object v0

    .line 971154
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t connect to the camera."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
