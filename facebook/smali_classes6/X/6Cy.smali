.class public final enum LX/6Cy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Cy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Cy;

.field public static final enum BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION:LX/6Cy;

.field public static final enum BROWSER_EXTENSIONS_PAYMENT_UNAUTHORIZED_PAYMENT:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_FAILED_TO_GET_USERID:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_FAILED_TO_LAUNCH_SHARE_FLOW:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_INVALID_PARAM:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_INVALID_WEBVIEW_SHARE_DATA:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_MISSING_PAYMENT_METHOD:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_MISSING_PAYMENT_PRIVACY_URL:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_PAYMENT_INVALID_CALLBACK:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_RESET_CART_FAILED:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_UNSUPPORTED_CALL:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_UPDATE_CART_FAILED:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_USER_CANCELED_PAYMENT_FLOW:LX/6Cy;

.field public static final enum BROWSER_EXTENSION_USER_DENIED_PERMISSION:LX/6Cy;

.field public static final enum SECURE:LX/6Cy;

.field public static final enum SUCCESS:LX/6Cy;

.field public static final enum USER_TOO_MANY_CALLS:LX/6Cy;


# instance fields
.field private final resultCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0x11

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1064467
    new-instance v0, LX/6Cy;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->SUCCESS:LX/6Cy;

    .line 1064468
    new-instance v0, LX/6Cy;

    const-string v1, "SECURE"

    invoke-direct {v0, v1, v7, v5}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->SECURE:LX/6Cy;

    .line 1064469
    new-instance v0, LX/6Cy;

    const-string v1, "USER_TOO_MANY_CALLS"

    invoke-direct {v0, v1, v8, v6}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->USER_TOO_MANY_CALLS:LX/6Cy;

    .line 1064470
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_USER_DENIED_PERMISSION"

    const/4 v2, 0x3

    const/16 v3, 0x5dc0

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_USER_DENIED_PERMISSION:LX/6Cy;

    .line 1064471
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_USER_CANCELED_PAYMENT_FLOW"

    const/4 v2, 0x4

    const/16 v3, 0x5dc1

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_USER_CANCELED_PAYMENT_FLOW:LX/6Cy;

    .line 1064472
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_MISSING_PAYMENT_PRIVACY_URL"

    const/4 v2, 0x5

    const/16 v3, 0x5dc2

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_PRIVACY_URL:LX/6Cy;

    .line 1064473
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_RESET_CART_FAILED"

    const/4 v2, 0x6

    const/16 v3, 0x5dc3

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_RESET_CART_FAILED:LX/6Cy;

    .line 1064474
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_UPDATE_CART_FAILED"

    const/4 v2, 0x7

    const/16 v3, 0x5dc4

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_UPDATE_CART_FAILED:LX/6Cy;

    .line 1064475
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_FAILED_TO_GET_USERID"

    const/16 v2, 0x5dc5

    invoke-direct {v0, v1, v5, v2}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_FAILED_TO_GET_USERID:LX/6Cy;

    .line 1064476
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_MISSING_PAYMENT_METHOD"

    const/16 v2, 0x9

    const/16 v3, 0x5dc6

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_METHOD:LX/6Cy;

    .line 1064477
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED"

    const/16 v2, 0xa

    const/16 v3, 0x5dc7

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED:LX/6Cy;

    .line 1064478
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_INVALID_PARAM"

    const/16 v2, 0xb

    const/16 v3, 0x5dc8

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_INVALID_PARAM:LX/6Cy;

    .line 1064479
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG"

    const/16 v2, 0xc

    const/16 v3, 0x5dd2

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG:LX/6Cy;

    .line 1064480
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_UNSUPPORTED_CALL"

    const/16 v2, 0xd

    const/16 v3, 0x5dd3

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_UNSUPPORTED_CALL:LX/6Cy;

    .line 1064481
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_INVALID_WEBVIEW_SHARE_DATA"

    const/16 v2, 0xe

    const/16 v3, 0x5dd4

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_INVALID_WEBVIEW_SHARE_DATA:LX/6Cy;

    .line 1064482
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_FAILED_TO_LAUNCH_SHARE_FLOW"

    const/16 v2, 0xf

    const/16 v3, 0x5dd5

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_FAILED_TO_LAUNCH_SHARE_FLOW:LX/6Cy;

    .line 1064483
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSION_PAYMENT_INVALID_CALLBACK"

    const/16 v2, 0x10

    const/16 v3, 0x5dd6

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CALLBACK:LX/6Cy;

    .line 1064484
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION"

    const/16 v2, 0x5dd7

    invoke-direct {v0, v1, v6, v2}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION:LX/6Cy;

    .line 1064485
    new-instance v0, LX/6Cy;

    const-string v1, "BROWSER_EXTENSIONS_PAYMENT_UNAUTHORIZED_PAYMENT"

    const/16 v2, 0x12

    const/16 v3, 0x5dd8

    invoke-direct {v0, v1, v2, v3}, LX/6Cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_UNAUTHORIZED_PAYMENT:LX/6Cy;

    .line 1064486
    const/16 v0, 0x13

    new-array v0, v0, [LX/6Cy;

    sget-object v1, LX/6Cy;->SUCCESS:LX/6Cy;

    aput-object v1, v0, v4

    sget-object v1, LX/6Cy;->SECURE:LX/6Cy;

    aput-object v1, v0, v7

    sget-object v1, LX/6Cy;->USER_TOO_MANY_CALLS:LX/6Cy;

    aput-object v1, v0, v8

    const/4 v1, 0x3

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_USER_DENIED_PERMISSION:LX/6Cy;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_USER_CANCELED_PAYMENT_FLOW:LX/6Cy;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_PRIVACY_URL:LX/6Cy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_RESET_CART_FAILED:LX/6Cy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_UPDATE_CART_FAILED:LX/6Cy;

    aput-object v2, v0, v1

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_FAILED_TO_GET_USERID:LX/6Cy;

    aput-object v1, v0, v5

    const/16 v1, 0x9

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_METHOD:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_INVALID_PARAM:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CHECKOUT_CONFIG:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_UNSUPPORTED_CALL:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_INVALID_WEBVIEW_SHARE_DATA:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_FAILED_TO_LAUNCH_SHARE_FLOW:LX/6Cy;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6Cy;->BROWSER_EXTENSION_PAYMENT_INVALID_CALLBACK:LX/6Cy;

    aput-object v2, v0, v1

    sget-object v1, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_INVALID_OPERATION:LX/6Cy;

    aput-object v1, v0, v6

    const/16 v1, 0x12

    sget-object v2, LX/6Cy;->BROWSER_EXTENSIONS_PAYMENT_UNAUTHORIZED_PAYMENT:LX/6Cy;

    aput-object v2, v0, v1

    sput-object v0, LX/6Cy;->$VALUES:[LX/6Cy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1064487
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1064488
    iput p3, p0, LX/6Cy;->resultCode:I

    .line 1064489
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Cy;
    .locals 1

    .prologue
    .line 1064490
    const-class v0, LX/6Cy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Cy;

    return-object v0
.end method

.method public static values()[LX/6Cy;
    .locals 1

    .prologue
    .line 1064491
    sget-object v0, LX/6Cy;->$VALUES:[LX/6Cy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Cy;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1064492
    iget v0, p0, LX/6Cy;->resultCode:I

    return v0
.end method
