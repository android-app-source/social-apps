.class public final LX/5rd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/5rl;

.field private final b:I

.field private final c:Lcom/facebook/react/bridge/Callback;


# direct methods
.method public constructor <init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 1011939
    iput-object p1, p0, LX/5rd;->a:LX/5rl;

    .line 1011940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011941
    iput p2, p0, LX/5rd;->b:I

    .line 1011942
    iput-object p3, p0, LX/5rd;->c:Lcom/facebook/react/bridge/Callback;

    .line 1011943
    return-void
.end method

.method public synthetic constructor <init>(LX/5rl;ILcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 1011944
    invoke-direct {p0, p1, p2, p3}, LX/5rd;-><init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1011945
    :try_start_0
    iget-object v0, p0, LX/5rd;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->b:LX/5qw;

    iget v1, p0, LX/5rd;->b:I

    iget-object v2, p0, LX/5rd;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->a(I[I)V
    :try_end_0
    .catch LX/5qz; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011946
    iget-object v0, p0, LX/5rd;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->a:[I

    aget v0, v0, v7

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 1011947
    iget-object v1, p0, LX/5rd;->a:LX/5rl;

    iget-object v1, v1, LX/5rl;->a:[I

    aget v1, v1, v8

    int-to-float v1, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 1011948
    iget-object v2, p0, LX/5rd;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->a:[I

    aget v2, v2, v9

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 1011949
    iget-object v3, p0, LX/5rd;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->a:[I

    aget v3, v3, v10

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 1011950
    iget-object v4, p0, LX/5rd;->c:Lcom/facebook/react/bridge/Callback;

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v10

    const/4 v2, 0x4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v0, 0x5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-interface {v4, v5}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1011951
    :goto_0
    return-void

    .line 1011952
    :catch_0
    iget-object v0, p0, LX/5rd;->c:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
