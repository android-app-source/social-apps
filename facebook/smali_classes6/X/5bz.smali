.class public final LX/5bz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 58

    .prologue
    .line 960353
    const/16 v47, 0x0

    .line 960354
    const/16 v46, 0x0

    .line 960355
    const/16 v45, 0x0

    .line 960356
    const/16 v44, 0x0

    .line 960357
    const/16 v43, 0x0

    .line 960358
    const/16 v42, 0x0

    .line 960359
    const-wide/16 v40, 0x0

    .line 960360
    const/16 v39, 0x0

    .line 960361
    const/16 v38, 0x0

    .line 960362
    const/16 v37, 0x0

    .line 960363
    const/16 v36, 0x0

    .line 960364
    const/16 v35, 0x0

    .line 960365
    const/16 v34, 0x0

    .line 960366
    const/16 v33, 0x0

    .line 960367
    const/16 v32, 0x0

    .line 960368
    const/16 v31, 0x0

    .line 960369
    const/16 v30, 0x0

    .line 960370
    const/16 v29, 0x0

    .line 960371
    const/16 v28, 0x0

    .line 960372
    const/16 v27, 0x0

    .line 960373
    const/16 v26, 0x0

    .line 960374
    const/16 v25, 0x0

    .line 960375
    const/16 v24, 0x0

    .line 960376
    const/16 v23, 0x0

    .line 960377
    const/16 v22, 0x0

    .line 960378
    const/16 v21, 0x0

    .line 960379
    const/16 v20, 0x0

    .line 960380
    const/16 v19, 0x0

    .line 960381
    const/16 v18, 0x0

    .line 960382
    const/16 v17, 0x0

    .line 960383
    const/16 v16, 0x0

    .line 960384
    const/4 v15, 0x0

    .line 960385
    const/4 v14, 0x0

    .line 960386
    const/4 v13, 0x0

    .line 960387
    const/4 v12, 0x0

    .line 960388
    const/4 v11, 0x0

    .line 960389
    const/4 v10, 0x0

    .line 960390
    const/4 v9, 0x0

    .line 960391
    const/4 v8, 0x0

    .line 960392
    const/4 v7, 0x0

    .line 960393
    const/4 v6, 0x0

    .line 960394
    const/4 v5, 0x0

    .line 960395
    const/4 v4, 0x0

    .line 960396
    const/4 v3, 0x0

    .line 960397
    const/4 v2, 0x0

    .line 960398
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_30

    .line 960399
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 960400
    const/4 v2, 0x0

    .line 960401
    :goto_0
    return v2

    .line 960402
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v49, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v49

    if-eq v2, v0, :cond_22

    .line 960403
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 960404
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 960405
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v49

    sget-object v50, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 960406
    const-string v49, "__type__"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_1

    const-string v49, "__typename"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_2

    .line 960407
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v48, v2

    goto :goto_1

    .line 960408
    :cond_2
    const-string v49, "accepts_messenger_user_feedback"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_3

    .line 960409
    const/4 v2, 0x1

    .line 960410
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    move/from16 v47, v17

    move/from16 v17, v2

    goto :goto_1

    .line 960411
    :cond_3
    const-string v49, "can_see_viewer_montage_thread"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_4

    .line 960412
    const/4 v2, 0x1

    .line 960413
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v46, v7

    move v7, v2

    goto :goto_1

    .line 960414
    :cond_4
    const-string v49, "can_viewer_message"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5

    .line 960415
    const/4 v2, 0x1

    .line 960416
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v45, v6

    move v6, v2

    goto :goto_1

    .line 960417
    :cond_5
    const-string v49, "commerce_page_settings"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_6

    .line 960418
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto/16 :goto_1

    .line 960419
    :cond_6
    const-string v49, "commerce_page_type"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_7

    .line 960420
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 960421
    :cond_7
    const-string v49, "communicationRank"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_8

    .line 960422
    const/4 v2, 0x1

    .line 960423
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 960424
    :cond_8
    const-string v49, "customer_data"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_9

    .line 960425
    invoke-static/range {p0 .. p1}, LX/5bl;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 960426
    :cond_9
    const-string v49, "email_addresses"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_a

    .line 960427
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 960428
    :cond_a
    const-string v49, "id"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_b

    .line 960429
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 960430
    :cond_b
    const-string v49, "is_blocked_by_viewer"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_c

    .line 960431
    const/4 v2, 0x1

    .line 960432
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v39, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 960433
    :cond_c
    const-string v49, "is_commerce"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_d

    .line 960434
    const/4 v2, 0x1

    .line 960435
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v38, v15

    move v15, v2

    goto/16 :goto_1

    .line 960436
    :cond_d
    const-string v49, "is_message_blocked_by_viewer"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_e

    .line 960437
    const/4 v2, 0x1

    .line 960438
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v37, v14

    move v14, v2

    goto/16 :goto_1

    .line 960439
    :cond_e
    const-string v49, "is_messenger_platform_bot"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_f

    .line 960440
    const/4 v2, 0x1

    .line 960441
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v36, v13

    move v13, v2

    goto/16 :goto_1

    .line 960442
    :cond_f
    const-string v49, "is_messenger_promotion_blocked_by_viewer"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_10

    .line 960443
    const/4 v2, 0x1

    .line 960444
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v35, v12

    move v12, v2

    goto/16 :goto_1

    .line 960445
    :cond_10
    const-string v49, "is_messenger_user"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_11

    .line 960446
    const/4 v2, 0x1

    .line 960447
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v34, v11

    move v11, v2

    goto/16 :goto_1

    .line 960448
    :cond_11
    const-string v49, "is_partial"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_12

    .line 960449
    const/4 v2, 0x1

    .line 960450
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v33, v10

    move v10, v2

    goto/16 :goto_1

    .line 960451
    :cond_12
    const-string v49, "is_vc_endpoint"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_13

    .line 960452
    const/4 v2, 0x1

    .line 960453
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v32, v9

    move v9, v2

    goto/16 :goto_1

    .line 960454
    :cond_13
    const-string v49, "is_viewer_friend"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_14

    .line 960455
    const/4 v2, 0x1

    .line 960456
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v31, v8

    move v8, v2

    goto/16 :goto_1

    .line 960457
    :cond_14
    const-string v49, "messenger_contact"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_15

    .line 960458
    invoke-static/range {p0 .. p1}, LX/5bx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 960459
    :cond_15
    const-string v49, "messenger_ctas"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_16

    .line 960460
    invoke-static/range {p0 .. p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 960461
    :cond_16
    const-string v49, "messenger_extension"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_17

    .line 960462
    invoke-static/range {p0 .. p1}, LX/5bq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 960463
    :cond_17
    const-string v49, "messenger_structured_menu_ctas"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_18

    .line 960464
    invoke-static/range {p0 .. p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 960465
    :cond_18
    const-string v49, "montage_thread_fbid"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_19

    .line 960466
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 960467
    :cond_19
    const-string v49, "name"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1a

    .line 960468
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 960469
    :cond_1a
    const-string v49, "page_messenger_bot"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1b

    .line 960470
    invoke-static/range {p0 .. p1}, LX/5U5;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 960471
    :cond_1b
    const-string v49, "phone_number"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1c

    .line 960472
    invoke-static/range {p0 .. p1}, LX/5bw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 960473
    :cond_1c
    const-string v49, "profile_pic_large"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1d

    .line 960474
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 960475
    :cond_1d
    const-string v49, "profile_pic_medium"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1e

    .line 960476
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 960477
    :cond_1e
    const-string v49, "profile_pic_small"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1f

    .line 960478
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 960479
    :cond_1f
    const-string v49, "structured_name"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_20

    .line 960480
    invoke-static/range {p0 .. p1}, LX/5by;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 960481
    :cond_20
    const-string v49, "username"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 960482
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 960483
    :cond_21
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 960484
    :cond_22
    const/16 v2, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 960485
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960486
    if-eqz v17, :cond_23

    .line 960487
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960488
    :cond_23
    if-eqz v7, :cond_24

    .line 960489
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960490
    :cond_24
    if-eqz v6, :cond_25

    .line 960491
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960492
    :cond_25
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960493
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960494
    if-eqz v3, :cond_26

    .line 960495
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 960496
    :cond_26
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960497
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960498
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960499
    if-eqz v16, :cond_27

    .line 960500
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960501
    :cond_27
    if-eqz v15, :cond_28

    .line 960502
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960503
    :cond_28
    if-eqz v14, :cond_29

    .line 960504
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960505
    :cond_29
    if-eqz v13, :cond_2a

    .line 960506
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960507
    :cond_2a
    if-eqz v12, :cond_2b

    .line 960508
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960509
    :cond_2b
    if-eqz v11, :cond_2c

    .line 960510
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960511
    :cond_2c
    if-eqz v10, :cond_2d

    .line 960512
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960513
    :cond_2d
    if-eqz v9, :cond_2e

    .line 960514
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960515
    :cond_2e
    if-eqz v8, :cond_2f

    .line 960516
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 960517
    :cond_2f
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960518
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960519
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960520
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960521
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960522
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960523
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960524
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960525
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960526
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960527
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960528
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960529
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 960530
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_30
    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v39

    move/from16 v39, v36

    move/from16 v36, v33

    move/from16 v33, v30

    move/from16 v30, v27

    move/from16 v27, v24

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v15

    move v15, v9

    move v9, v3

    move v3, v11

    move v11, v5

    move/from16 v51, v14

    move v14, v8

    move v8, v2

    move/from16 v52, v23

    move/from16 v23, v20

    move/from16 v20, v17

    move/from16 v17, v51

    move/from16 v53, v26

    move/from16 v26, v52

    move/from16 v54, v12

    move v12, v6

    move/from16 v6, v54

    move/from16 v55, v29

    move/from16 v29, v53

    move/from16 v56, v19

    move/from16 v19, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v4, v40

    move/from16 v40, v37

    move/from16 v41, v38

    move/from16 v37, v34

    move/from16 v38, v35

    move/from16 v34, v31

    move/from16 v35, v32

    move/from16 v31, v28

    move/from16 v32, v55

    move/from16 v28, v25

    move/from16 v25, v22

    move/from16 v22, v56

    move/from16 v57, v13

    move v13, v7

    move/from16 v7, v57

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 960531
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960532
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 960533
    if-eqz v0, :cond_0

    .line 960534
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960535
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 960536
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960537
    if-eqz v0, :cond_1

    .line 960538
    const-string v1, "accepts_messenger_user_feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960539
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960540
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960541
    if-eqz v0, :cond_2

    .line 960542
    const-string v1, "can_see_viewer_montage_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960543
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960544
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960545
    if-eqz v0, :cond_3

    .line 960546
    const-string v1, "can_viewer_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960547
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960548
    :cond_3
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 960549
    if-eqz v0, :cond_4

    .line 960550
    const-string v0, "commerce_page_settings"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960551
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 960552
    :cond_4
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 960553
    if-eqz v0, :cond_5

    .line 960554
    const-string v0, "commerce_page_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960555
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960556
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 960557
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_6

    .line 960558
    const-string v2, "communicationRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960559
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 960560
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960561
    if-eqz v0, :cond_7

    .line 960562
    const-string v1, "customer_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960563
    invoke-static {p0, v0, p2, p3}, LX/5bl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960564
    :cond_7
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 960565
    if-eqz v0, :cond_8

    .line 960566
    const-string v0, "email_addresses"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960567
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 960568
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960569
    if-eqz v0, :cond_9

    .line 960570
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960572
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960573
    if-eqz v0, :cond_a

    .line 960574
    const-string v1, "is_blocked_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960575
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960576
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960577
    if-eqz v0, :cond_b

    .line 960578
    const-string v1, "is_commerce"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960579
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960580
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960581
    if-eqz v0, :cond_c

    .line 960582
    const-string v1, "is_message_blocked_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960583
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960584
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960585
    if-eqz v0, :cond_d

    .line 960586
    const-string v1, "is_messenger_platform_bot"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960587
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960588
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960589
    if-eqz v0, :cond_e

    .line 960590
    const-string v1, "is_messenger_promotion_blocked_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960591
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960592
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960593
    if-eqz v0, :cond_f

    .line 960594
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960595
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960596
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960597
    if-eqz v0, :cond_10

    .line 960598
    const-string v1, "is_partial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960599
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960600
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960601
    if-eqz v0, :cond_11

    .line 960602
    const-string v1, "is_vc_endpoint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960603
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960604
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960605
    if-eqz v0, :cond_12

    .line 960606
    const-string v1, "is_viewer_friend"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960607
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960608
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960609
    if-eqz v0, :cond_13

    .line 960610
    const-string v1, "messenger_contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960611
    invoke-static {p0, v0, p2}, LX/5bx;->a(LX/15i;ILX/0nX;)V

    .line 960612
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960613
    if-eqz v0, :cond_14

    .line 960614
    const-string v1, "messenger_ctas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960615
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960616
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960617
    if-eqz v0, :cond_15

    .line 960618
    const-string v1, "messenger_extension"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960619
    invoke-static {p0, v0, p2, p3}, LX/5bq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960620
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960621
    if-eqz v0, :cond_16

    .line 960622
    const-string v1, "messenger_structured_menu_ctas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960623
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960624
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960625
    if-eqz v0, :cond_17

    .line 960626
    const-string v1, "montage_thread_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960627
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960628
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960629
    if-eqz v0, :cond_18

    .line 960630
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960631
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960632
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960633
    if-eqz v0, :cond_19

    .line 960634
    const-string v1, "page_messenger_bot"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960635
    invoke-static {p0, v0, p2}, LX/5U5;->a(LX/15i;ILX/0nX;)V

    .line 960636
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960637
    if-eqz v0, :cond_1a

    .line 960638
    const-string v1, "phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960639
    invoke-static {p0, v0, p2}, LX/5bw;->a(LX/15i;ILX/0nX;)V

    .line 960640
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960641
    if-eqz v0, :cond_1b

    .line 960642
    const-string v1, "profile_pic_large"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960643
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 960644
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960645
    if-eqz v0, :cond_1c

    .line 960646
    const-string v1, "profile_pic_medium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960647
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 960648
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960649
    if-eqz v0, :cond_1d

    .line 960650
    const-string v1, "profile_pic_small"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960651
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 960652
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960653
    if-eqz v0, :cond_1e

    .line 960654
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960655
    invoke-static {p0, v0, p2, p3}, LX/5by;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960656
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960657
    if-eqz v0, :cond_1f

    .line 960658
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960659
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960660
    :cond_1f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960661
    return-void
.end method
