.class public LX/6cZ;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1114513
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1114514
    return-void
.end method

.method public static a(LX/0Uh;LX/0ad;)LX/6cb;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1114515
    const/16 v0, 0x16a

    invoke-virtual {p0, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1114516
    const/4 v0, 0x0

    .line 1114517
    :cond_0
    :goto_0
    return-object v0

    .line 1114518
    :cond_1
    new-instance v0, LX/6cb;

    invoke-direct {v0}, LX/6cb;-><init>()V

    .line 1114519
    sget-short v1, LX/6ca;->c:S

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1114520
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, LX/6cb;->a(Ljava/lang/String;)V

    .line 1114521
    :cond_2
    sget-short v1, LX/6ca;->b:S

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1114522
    const-string v1, "image/gif"

    invoke-virtual {v0, v1}, LX/6cb;->a(Ljava/lang/String;)V

    .line 1114523
    :cond_3
    sget-short v1, LX/6ca;->d:S

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1114524
    const-string v1, "image/png"

    invoke-virtual {v0, v1}, LX/6cb;->a(Ljava/lang/String;)V

    .line 1114525
    :cond_4
    sget-short v1, LX/6ca;->e:S

    invoke-interface {p1, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1114526
    const-string v1, "image/webp"

    invoke-virtual {v0, v1}, LX/6cb;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1114527
    return-void
.end method
