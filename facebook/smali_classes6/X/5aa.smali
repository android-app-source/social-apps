.class public final LX/5aa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 955211
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 955212
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955213
    :goto_0
    return v1

    .line 955214
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955215
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 955216
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 955217
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 955218
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 955219
    const-string v4, "instant_game_id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 955220
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 955221
    :cond_2
    const-string v4, "list_item"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 955222
    invoke-static {p0, p1}, LX/5aZ;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 955223
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 955224
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 955225
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 955226
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 955227
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955228
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955229
    if-eqz v0, :cond_0

    .line 955230
    const-string v1, "instant_game_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955231
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955232
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955233
    if-eqz v0, :cond_1

    .line 955234
    const-string v1, "list_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955235
    invoke-static {p0, v0, p2, p3}, LX/5aZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955236
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955237
    return-void
.end method
