.class public LX/6bv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1114011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILcom/facebook/messaging/attachments/ImageAttachmentUris;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1114012
    if-nez p1, :cond_0

    .line 1114013
    const/4 v0, 0x0

    .line 1114014
    :goto_0
    return-object v0

    .line 1114015
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 1114016
    iget-object v0, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    goto :goto_0

    .line 1114017
    :cond_1
    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-ne p0, v0, :cond_3

    .line 1114018
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    goto :goto_0

    .line 1114019
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    goto :goto_0
.end method
