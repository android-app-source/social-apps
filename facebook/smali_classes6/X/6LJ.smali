.class public LX/6LJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1078292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078293
    iput-object p1, p0, LX/6LJ;->a:Landroid/content/Context;

    .line 1078294
    iput-object p2, p0, LX/6LJ;->b:LX/0Zb;

    .line 1078295
    return-void
.end method

.method public static b(LX/0QB;)LX/6LJ;
    .locals 3

    .prologue
    .line 1078311
    new-instance v2, LX/6LJ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {v2, v0, v1}, LX/6LJ;-><init>(Landroid/content/Context;LX/0Zb;)V

    .line 1078312
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1078309
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/6LJ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1078310
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1078296
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "MessengerBannerNotifications"

    .line 1078297
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1078298
    move-object v0, v0

    .line 1078299
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1078300
    move-object v0, v0

    .line 1078301
    iput-object p3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1078302
    move-object v1, v0

    .line 1078303
    iget-object v0, p0, LX/6LJ;->a:Landroid/content/Context;

    instance-of v0, v0, LX/0f2;

    if-eqz v0, :cond_0

    .line 1078304
    const-string v2, "NotificationLocationActivity"

    iget-object v0, p0, LX/6LJ;->a:Landroid/content/Context;

    check-cast v0, LX/0f2;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1078305
    :cond_0
    if-eqz p4, :cond_1

    .line 1078306
    invoke-virtual {v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1078307
    :cond_1
    iget-object v0, p0, LX/6LJ;->b:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1078308
    return-void
.end method
