.class public interface abstract LX/5Vu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Vq;
.implements LX/5Vr;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "UserInfo"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract V()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageMessengerBot"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ag()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerExtension"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
