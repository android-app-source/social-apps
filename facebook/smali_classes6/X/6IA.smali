.class public LX/6IA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6HO;
.implements LX/6I9;


# static fields
.field private static final e:Landroid/graphics/Point;


# instance fields
.field private A:Z

.field private B:F

.field public C:Landroid/animation/ObjectAnimator;

.field public D:LX/6I8;

.field public E:Z

.field private F:Z

.field private G:Z

.field private H:J

.field private I:Ljava/lang/String;

.field private J:I

.field private K:Z

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field public X:I

.field public Y:LX/1ql;

.field public Z:LX/6I6;

.field private final a:LX/0Zr;

.field private final aa:LX/1r1;

.field public ab:LX/6HF;

.field public ac:LX/6HG;

.field public final ad:LX/6Hr;

.field private final ae:LX/6Hj;

.field public final af:LX/03V;

.field public final ag:Lcom/facebook/content/SecureContextHelper;

.field private final ah:LX/0Sh;

.field public final ai:LX/2Ib;

.field public final aj:LX/0kL;

.field private final ak:Z

.field private final al:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final am:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public an:LX/6IG;

.field public ao:LX/6IG;

.field public ap:I

.field public aq:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public ar:LX/6HU;

.field public as:Z

.field public at:Z

.field private au:Z

.field public av:Z

.field public aw:Landroid/net/Uri;

.field private final ax:Landroid/view/View$OnTouchListener;

.field private final ay:Landroid/view/View$OnClickListener;

.field private final az:Landroid/view/View$OnClickListener;

.field public b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Intent;

.field public d:LX/6HE;

.field private f:Landroid/view/View;

.field private g:LX/6HV;

.field private h:Landroid/widget/RelativeLayout;

.field public i:Landroid/widget/RelativeLayout;

.field public j:Landroid/widget/ImageView;

.field private k:Lcom/facebook/camera/views/RotateLayout;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:LX/6IL;

.field public o:LX/6IL;

.field public p:Landroid/graphics/Rect;

.field private q:Landroid/widget/RelativeLayout;

.field private r:Landroid/widget/RelativeLayout;

.field private s:Landroid/widget/RelativeLayout;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field public v:Lcom/facebook/camera/views/RotateLayout;

.field private w:Landroid/widget/ImageView;

.field private x:LX/6Hd;

.field private y:Lcom/facebook/camera/views/ShutterView;

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1073385
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x4

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, LX/6IA;->e:Landroid/graphics/Point;

    return-void
.end method

.method public constructor <init>(LX/6Hj;LX/6Hr;LX/2Ib;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zr;LX/0kL;Ljava/lang/Boolean;LX/0Or;LX/0Or;LX/1r1;)V
    .locals 2
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/camera/gating/UseCustomVideoRecorder;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/DisableCameraPreviewBleedGk;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/ForceCameraInLandscape;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Hj;",
            "Lcom/facebook/camera/support/CameraSupport;",
            "LX/2Ib;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Zr;",
            "LX/0kL;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1r1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1073324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073325
    iput-boolean v1, p0, LX/6IA;->z:Z

    .line 1073326
    iput-boolean v1, p0, LX/6IA;->A:Z

    .line 1073327
    const/4 v0, 0x0

    iput-object v0, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    .line 1073328
    iput-boolean v1, p0, LX/6IA;->K:Z

    .line 1073329
    const/4 v0, 0x1

    iput v0, p0, LX/6IA;->X:I

    .line 1073330
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v0, p0, LX/6IA;->an:LX/6IG;

    .line 1073331
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v0, p0, LX/6IA;->ao:LX/6IG;

    .line 1073332
    iput v1, p0, LX/6IA;->ap:I

    .line 1073333
    iput-boolean v1, p0, LX/6IA;->as:Z

    .line 1073334
    iput-boolean v1, p0, LX/6IA;->at:Z

    .line 1073335
    iput-boolean v1, p0, LX/6IA;->au:Z

    .line 1073336
    iput-boolean v1, p0, LX/6IA;->av:Z

    .line 1073337
    new-instance v0, LX/6Hx;

    invoke-direct {v0, p0}, LX/6Hx;-><init>(LX/6IA;)V

    iput-object v0, p0, LX/6IA;->ax:Landroid/view/View$OnTouchListener;

    .line 1073338
    new-instance v0, LX/6Hy;

    invoke-direct {v0, p0}, LX/6Hy;-><init>(LX/6IA;)V

    iput-object v0, p0, LX/6IA;->ay:Landroid/view/View$OnClickListener;

    .line 1073339
    new-instance v0, LX/6Hz;

    invoke-direct {v0, p0}, LX/6Hz;-><init>(LX/6IA;)V

    iput-object v0, p0, LX/6IA;->az:Landroid/view/View$OnClickListener;

    .line 1073340
    iput-object p1, p0, LX/6IA;->ae:LX/6Hj;

    .line 1073341
    iput-object p2, p0, LX/6IA;->ad:LX/6Hr;

    .line 1073342
    iput-object p3, p0, LX/6IA;->ai:LX/2Ib;

    .line 1073343
    iput-object p4, p0, LX/6IA;->af:LX/03V;

    .line 1073344
    iput-object p5, p0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    .line 1073345
    iput-object p6, p0, LX/6IA;->ah:LX/0Sh;

    .line 1073346
    iput-object p7, p0, LX/6IA;->aq:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1073347
    iput-object p8, p0, LX/6IA;->a:LX/0Zr;

    .line 1073348
    iput-object p9, p0, LX/6IA;->aj:LX/0kL;

    .line 1073349
    invoke-virtual {p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/6IA;->ak:Z

    .line 1073350
    iput-object p11, p0, LX/6IA;->al:LX/0Or;

    .line 1073351
    iput-object p12, p0, LX/6IA;->am:LX/0Or;

    .line 1073352
    iput-object p13, p0, LX/6IA;->aa:LX/1r1;

    .line 1073353
    return-void
.end method

.method public static a(LX/6IA;LX/6IG;)F
    .locals 2

    .prologue
    .line 1073354
    iget v0, p1, LX/6IG;->mRotation:I

    iget-object v1, p0, LX/6IA;->ao:LX/6IG;

    iget v1, v1, LX/6IG;->mReverseRotation:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x5a

    int-to-float v0, v0

    return v0
.end method

.method public static a(LX/6IA;FI)V
    .locals 5

    .prologue
    .line 1073355
    iget v0, p0, LX/6IA;->B:F

    invoke-static {v0, p1}, LX/6IF;->a(FF)F

    move-result v0

    .line 1073356
    iget-object v1, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_0

    .line 1073357
    iget-object v1, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1073358
    :cond_0
    iget-object v1, p0, LX/6IA;->l:Landroid/view/View;

    iget-object v2, p0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotX(F)V

    .line 1073359
    iget-object v1, p0, LX/6IA;->l:Landroid/view/View;

    iget-object v2, p0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotY(F)V

    .line 1073360
    iget-object v1, p0, LX/6IA;->j:Landroid/widget/ImageView;

    iget-object v2, p0, LX/6IA;->j:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1073361
    iget-object v1, p0, LX/6IA;->j:Landroid/widget/ImageView;

    iget-object v2, p0, LX/6IA;->j:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1073362
    iget-object v1, p0, LX/6IA;->w:Landroid/widget/ImageView;

    iget-object v2, p0, LX/6IA;->w:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1073363
    iget-object v1, p0, LX/6IA;->w:Landroid/widget/ImageView;

    iget-object v2, p0, LX/6IA;->w:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1073364
    iget v1, p0, LX/6IA;->B:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_1

    .line 1073365
    const-string v1, "IconsRotationAngle"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v4, p0, LX/6IA;->B:F

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v0, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    .line 1073366
    iget-object v0, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1073367
    iget-object v0, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/6I0;

    invoke-direct {v1, p0}, LX/6I0;-><init>(LX/6IA;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1073368
    iget-object v0, p0, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1073369
    :goto_0
    return-void

    .line 1073370
    :cond_1
    invoke-virtual {p0, v0}, LX/6IA;->setIconsRotationAngle(F)V

    goto :goto_0
.end method

.method public static a(LX/6IA;Landroid/net/Uri;I)V
    .locals 4

    .prologue
    .line 1073371
    goto :goto_1

    .line 1073372
    :goto_0
    return-void

    .line 1073373
    :goto_1
    iget-object v0, p0, LX/6IA;->b:Ljava/lang/Class;

    const-string v1, "no ReviewActivity intent could be created"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6IA;
    .locals 14

    .prologue
    .line 1073374
    new-instance v0, LX/6IA;

    invoke-static {p0}, LX/6Hj;->b(LX/0QB;)LX/6Hj;

    move-result-object v1

    check-cast v1, LX/6Hj;

    .line 1073375
    new-instance v2, LX/6Hr;

    invoke-direct {v2}, LX/6Hr;-><init>()V

    .line 1073376
    move-object v2, v2

    .line 1073377
    move-object v2, v2

    .line 1073378
    check-cast v2, LX/6Hr;

    invoke-static {p0}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v3

    check-cast v3, LX/2Ib;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v8

    check-cast v8, LX/0Zr;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    .line 1073379
    invoke-static {}, LX/6HA;->a()Ljava/lang/Boolean;

    move-result-object v10

    move-object v10, v10

    .line 1073380
    check-cast v10, Ljava/lang/Boolean;

    const/16 v11, 0x145b

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x145d

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {p0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v13

    check-cast v13, LX/1r1;

    invoke-direct/range {v0 .. v13}, LX/6IA;-><init>(LX/6Hj;LX/6Hr;LX/2Ib;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zr;LX/0kL;Ljava/lang/Boolean;LX/0Or;LX/0Or;LX/1r1;)V

    .line 1073381
    return-object v0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 1073382
    invoke-direct {p0}, LX/6IA;->s()F

    move-result v0

    .line 1073383
    iget-object v1, p0, LX/6IA;->n:LX/6IL;

    invoke-virtual {v1, v0, p1}, LX/6IL;->a(FZ)V

    .line 1073384
    return-void
.end method

.method private static f(LX/6IA;Z)V
    .locals 2

    .prologue
    .line 1073386
    invoke-direct {p0}, LX/6IA;->s()F

    move-result v0

    .line 1073387
    iget-object v1, p0, LX/6IA;->o:LX/6IL;

    invoke-virtual {v1, v0, p1}, LX/6IL;->a(FZ)V

    .line 1073388
    return-void
.end method

.method public static g(LX/6IA;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1073389
    if-eqz p1, :cond_0

    .line 1073390
    iget-object v0, p0, LX/6IA;->m:Landroid/view/View;

    const v3, 0x7f0201dd

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1073391
    :goto_0
    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {p0, v0}, LX/6IA;->f(LX/6IA;Z)V

    .line 1073392
    iget-object v3, p0, LX/6IA;->o:LX/6IL;

    if-nez p1, :cond_2

    move v0, v1

    .line 1073393
    :goto_2
    iget-object v4, v3, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1073394
    iget-object v3, p0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1073395
    iget-object v0, p0, LX/6IA;->r:Landroid/widget/RelativeLayout;

    if-nez p1, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1073396
    return-void

    .line 1073397
    :cond_0
    iget-object v0, p0, LX/6IA;->m:Landroid/view/View;

    const v3, 0x7f0201de

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1073398
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1073399
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1073400
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1073401
    goto :goto_4
.end method

.method public static p(LX/6IA;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1073514
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    invoke-interface {v0}, LX/6HE;->m()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static q(LX/6IA;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1073402
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    invoke-interface {v0}, LX/6HE;->n()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static r(LX/6IA;)V
    .locals 11

    .prologue
    const/16 v6, 0x14

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 1073403
    iget-boolean v9, p0, LX/6IA;->au:Z

    .line 1073404
    iget-boolean v0, p0, LX/6IA;->as:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/6IA;->at:Z

    if-eqz v0, :cond_2

    .line 1073405
    iget-boolean v0, p0, LX/6IA;->au:Z

    if-nez v0, :cond_0

    .line 1073406
    iget-boolean v0, p0, LX/6IA;->av:Z

    if-nez v0, :cond_0

    .line 1073407
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->j()V

    .line 1073408
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-static {p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v1

    .line 1073409
    iput-object v1, v0, LX/6HU;->c:Landroid/app/Activity;

    .line 1073410
    invoke-static {v0}, LX/6HU;->x(LX/6HU;)V

    .line 1073411
    iput-boolean v10, p0, LX/6IA;->au:Z

    .line 1073412
    iget-object v0, p0, LX/6IA;->Y:LX/1ql;

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v2, v3}, LX/1ql;->a(J)V

    .line 1073413
    iget-boolean v0, p0, LX/6IA;->A:Z

    if-nez v0, :cond_0

    .line 1073414
    iget-object v0, p0, LX/6IA;->al:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 1073415
    invoke-static {p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, LX/6IA;->f:Landroid/view/View;

    iget-object v2, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    iget-object v3, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    sget-object v4, LX/6IA;->e:Landroid/graphics/Point;

    iget-object v5, p0, LX/6IA;->p:Landroid/graphics/Rect;

    if-eqz v8, :cond_1

    :goto_0
    move v8, v6

    invoke-static/range {v0 .. v8}, LX/6IF;->a(Landroid/app/Activity;Landroid/view/View;Landroid/widget/RelativeLayout;Landroid/widget/RelativeLayout;Landroid/graphics/Point;Landroid/graphics/Rect;III)Z

    move-result v0

    iput-boolean v0, p0, LX/6IA;->A:Z

    .line 1073416
    iget-object v0, p0, LX/6IA;->b:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Preview resized "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, LX/6IA;->A:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1073417
    :cond_0
    iget-object v0, p0, LX/6IA;->Z:LX/6I6;

    invoke-virtual {v0, v10}, LX/6I6;->a(Z)V

    .line 1073418
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updatePreviewState "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/6IA;->au:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1073419
    return-void

    .line 1073420
    :cond_1
    const/16 v7, 0xa

    goto :goto_0

    .line 1073421
    :cond_2
    iget-boolean v0, p0, LX/6IA;->au:Z

    if-eqz v0, :cond_4

    .line 1073422
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    .line 1073423
    iget-boolean v1, v0, LX/6HU;->k:Z

    move v0, v1

    .line 1073424
    if-eqz v0, :cond_3

    .line 1073425
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->h()V

    .line 1073426
    invoke-static {p0, v7}, LX/6IA;->g(LX/6IA;Z)V

    .line 1073427
    :cond_3
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->l()V

    .line 1073428
    iget-object v0, p0, LX/6IA;->Y:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 1073429
    iput-boolean v7, p0, LX/6IA;->au:Z

    .line 1073430
    :cond_4
    iget-object v0, p0, LX/6IA;->Z:LX/6I6;

    invoke-virtual {v0, v7}, LX/6I6;->a(Z)V

    goto :goto_1
.end method

.method private s()F
    .locals 1

    .prologue
    .line 1073431
    iget-object v0, p0, LX/6IA;->an:LX/6IG;

    invoke-static {p0, v0}, LX/6IA;->a(LX/6IA;LX/6IG;)F

    move-result v0

    return v0
.end method

.method public static t(LX/6IA;)V
    .locals 3

    .prologue
    .line 1073432
    invoke-static {p0}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08119c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1073433
    return-void
.end method

.method private static w(LX/6IA;)V
    .locals 3

    .prologue
    .line 1073434
    iget-object v0, p0, LX/6IA;->f:Landroid/view/View;

    const v1, 0x7f0d0681

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    .line 1073435
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d086c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1073436
    new-instance v1, LX/6IL;

    iget-object v2, p0, LX/6IA;->p:Landroid/graphics/Rect;

    invoke-direct {v1, v0, v2}, LX/6IL;-><init>(Landroid/view/View;Landroid/graphics/Rect;)V

    iput-object v1, p0, LX/6IA;->n:LX/6IL;

    .line 1073437
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d086d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1073438
    new-instance v1, LX/6IL;

    iget-object v2, p0, LX/6IA;->p:Landroid/graphics/Rect;

    invoke-direct {v1, v0, v2}, LX/6IL;-><init>(Landroid/view/View;Landroid/graphics/Rect;)V

    iput-object v1, p0, LX/6IA;->o:LX/6IL;

    .line 1073439
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d086e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/camera/views/ShutterView;

    iput-object v0, p0, LX/6IA;->y:Lcom/facebook/camera/views/ShutterView;

    .line 1073440
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d11d9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/camera/views/RotateLayout;

    iput-object v0, p0, LX/6IA;->k:Lcom/facebook/camera/views/RotateLayout;

    .line 1073441
    iget-object v0, p0, LX/6IA;->f:Landroid/view/View;

    const v1, 0x7f0d086b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    .line 1073442
    iget-object v0, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0872

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/6IA;->l:Landroid/view/View;

    .line 1073443
    iget-object v0, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0873

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/6IA;->m:Landroid/view/View;

    .line 1073444
    iget-object v0, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d086f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/6IA;->r:Landroid/widget/RelativeLayout;

    .line 1073445
    iget-object v0, p0, LX/6IA;->r:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0870

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/6IA;->w:Landroid/widget/ImageView;

    .line 1073446
    iget-object v0, p0, LX/6IA;->q:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0874

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    .line 1073447
    iget-object v0, p0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0875

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/6IA;->j:Landroid/widget/ImageView;

    .line 1073448
    iget-object v0, p0, LX/6IA;->f:Landroid/view/View;

    const v1, 0x7f0d0876

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    .line 1073449
    iget-object v0, p0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0877

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/camera/views/RotateLayout;

    iput-object v0, p0, LX/6IA;->v:Lcom/facebook/camera/views/RotateLayout;

    .line 1073450
    iget-object v0, p0, LX/6IA;->v:Lcom/facebook/camera/views/RotateLayout;

    const v1, 0x7f0d0879

    invoke-virtual {v0, v1}, Lcom/facebook/camera/views/RotateLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6IA;->t:Landroid/widget/TextView;

    .line 1073451
    iget-object v0, p0, LX/6IA;->v:Lcom/facebook/camera/views/RotateLayout;

    const v1, 0x7f0d087a

    invoke-virtual {v0, v1}, Lcom/facebook/camera/views/RotateLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6IA;->u:Landroid/widget/TextView;

    .line 1073452
    const/4 v0, 0x0

    iput-object v0, p0, LX/6IA;->x:LX/6Hd;

    .line 1073453
    return-void
.end method

.method public static x(LX/6IA;)V
    .locals 5

    .prologue
    .line 1073454
    iget v0, p0, LX/6IA;->X:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1073455
    :cond_0
    :goto_0
    return-void

    .line 1073456
    :cond_1
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    invoke-virtual {v0}, LX/6I7;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1073457
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    .line 1073458
    iget-boolean v1, v0, LX/6HU;->s:Z

    move v0, v1

    .line 1073459
    if-nez v0, :cond_2

    .line 1073460
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    .line 1073461
    sget-object v1, LX/6IH;->a:[I

    iget-object v2, v0, LX/6I7;->c:LX/6II;

    invoke-virtual {v2}, LX/6II;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1073462
    iget-object v1, v0, LX/6I7;->a:LX/03V;

    sget-object v2, LX/6I7;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "queueCapture while "

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v0, LX/6I7;->c:LX/6II;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073463
    :goto_1
    goto :goto_0

    .line 1073464
    :cond_2
    invoke-static {}, LX/2Ib;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1073465
    invoke-static {p0}, LX/6IA;->t(LX/6IA;)V

    goto :goto_0

    .line 1073466
    :cond_3
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    .line 1073467
    iget-object v1, v0, LX/6HU;->n:LX/6Ha;

    if-eqz v1, :cond_7

    .line 1073468
    iget-object v1, v0, LX/6HU;->n:LX/6Ha;

    const/4 v2, 0x1

    .line 1073469
    iget-boolean v3, v1, LX/6Ha;->d:Z

    if-nez v3, :cond_8

    .line 1073470
    sget-object v2, LX/6Ha;->b:Ljava/lang/Class;

    const-string v3, "FocusManager.doSnap not initialized"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1073471
    const/4 v2, 0x0

    .line 1073472
    :cond_4
    :goto_2
    move v1, v2

    .line 1073473
    :goto_3
    move v0, v1

    .line 1073474
    if-eqz v0, :cond_6

    .line 1073475
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    .line 1073476
    invoke-virtual {v0}, LX/6I7;->c()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1073477
    iget-object v1, v0, LX/6I7;->a:LX/03V;

    sget-object v2, LX/6I7;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "captureStarted while "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1073478
    iget-object v4, v0, LX/6I7;->c:LX/6II;

    move-object v4, v4

    .line 1073479
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073480
    :cond_5
    sget-object v1, LX/6II;->CAPTURE_PENDING:LX/6II;

    invoke-static {v0, v1}, LX/6I7;->b(LX/6I7;LX/6II;)V

    .line 1073481
    iget-object v0, p0, LX/6IA;->Z:LX/6I6;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6I6;->a(Z)V

    goto/16 :goto_0

    .line 1073482
    :cond_6
    iget-object v0, p0, LX/6IA;->b:Ljava/lang/Class;

    const-string v1, "takeOrQueuePictureTaking denied by CameraHolder"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1073483
    :pswitch_0
    sget-object v1, LX/6II;->QUEUED:LX/6II;

    invoke-static {v0, v1}, LX/6I7;->b(LX/6I7;LX/6II;)V

    goto :goto_1

    .line 1073484
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queueCapture while "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, LX/6I7;->c:LX/6II;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    .line 1073485
    :cond_8
    invoke-static {v1}, LX/6Ha;->n(LX/6Ha;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget v3, v1, LX/6Ha;->c:I

    const/4 v0, 0x3

    if-eq v3, v0, :cond_9

    iget v3, v1, LX/6Ha;->c:I

    const/4 v0, 0x4

    if-ne v3, v0, :cond_a

    .line 1073486
    :cond_9
    invoke-static {v1}, LX/6Ha;->j(LX/6Ha;)V

    goto :goto_2

    .line 1073487
    :cond_a
    iget v3, v1, LX/6Ha;->c:I

    if-ne v3, v2, :cond_b

    .line 1073488
    const/4 v3, 0x2

    iput v3, v1, LX/6Ha;->c:I

    goto :goto_2

    .line 1073489
    :cond_b
    iget v3, v1, LX/6Ha;->c:I

    if-nez v3, :cond_4

    .line 1073490
    invoke-static {v1}, LX/6Ha;->j(LX/6Ha;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static y(LX/6IA;)Z
    .locals 2

    .prologue
    .line 1073491
    iget-object v0, p0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1073492
    invoke-static {p0}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040043

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1073493
    iget-object v1, p0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1073494
    iget-object v0, p0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1073495
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    invoke-virtual {v0}, LX/6I7;->b()V

    .line 1073496
    const/4 v0, 0x1

    .line 1073497
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static z(LX/6IA;)F
    .locals 3

    .prologue
    .line 1073498
    invoke-static {p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    .line 1073499
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 1073500
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1073501
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 1073502
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 1073503
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1073504
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1073505
    int-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1073506
    invoke-direct {p0, v2}, LX/6IA;->e(Z)V

    .line 1073507
    invoke-static {p0, v2}, LX/6IA;->f(LX/6IA;Z)V

    .line 1073508
    iget-object v0, p0, LX/6IA;->k:Lcom/facebook/camera/views/RotateLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/camera/views/RotateLayout;->setVisibility(I)V

    .line 1073509
    iget-object v0, p0, LX/6IA;->g:LX/6HV;

    if-eqz v0, :cond_0

    .line 1073510
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LX/6IA;->g:LX/6HV;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1073511
    const/4 v0, 0x0

    iput-object v0, p0, LX/6IA;->g:LX/6HV;

    .line 1073512
    :cond_0
    iget-object v0, p0, LX/6IA;->Z:LX/6I6;

    invoke-virtual {v0, v2}, LX/6I6;->a(Z)V

    .line 1073513
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1073318
    iget-object v0, p0, LX/6IA;->n:LX/6IL;

    .line 1073319
    iget-object p0, v0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1073320
    return-void
.end method

.method public final a(LX/6HV;)V
    .locals 2

    .prologue
    .line 1073321
    iget-object v0, p0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1073322
    iput-object p1, p0, LX/6IA;->g:LX/6HV;

    .line 1073323
    return-void
.end method

.method public final a(LX/6HX;)V
    .locals 2

    .prologue
    .line 1073155
    sget-object v0, LX/6Hw;->b:[I

    invoke-virtual {p1}, LX/6HX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1073156
    :goto_0
    return-void

    .line 1073157
    :pswitch_0
    iget-object v0, p0, LX/6IA;->ac:LX/6HG;

    .line 1073158
    iget v1, v0, LX/6HG;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/6HG;->b:I

    .line 1073159
    goto :goto_0

    .line 1073160
    :pswitch_1
    iget-object v0, p0, LX/6IA;->ac:LX/6HG;

    .line 1073161
    iget v1, v0, LX/6HG;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/6HG;->c:I

    .line 1073162
    goto :goto_0

    .line 1073163
    :pswitch_2
    iget-object v0, p0, LX/6IA;->ac:LX/6HG;

    .line 1073164
    iget v1, v0, LX/6HG;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/6HG;->d:I

    .line 1073165
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1073166
    if-nez p1, :cond_0

    .line 1073167
    invoke-static {p0}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08119e

    const/4 p1, 0x1

    invoke-static {v0, v1, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1073168
    :goto_0
    return-void

    .line 1073169
    :cond_0
    iget-boolean v0, p0, LX/6IA;->K:Z

    if-eqz v0, :cond_1

    .line 1073170
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, LX/6IA;->a(LX/6IA;Landroid/net/Uri;I)V

    goto :goto_0

    .line 1073171
    :cond_1
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/6HE;->d(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 24

    .prologue
    .line 1073172
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/6IA;->f:Landroid/view/View;

    .line 1073173
    invoke-static/range {p0 .. p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v4

    .line 1073174
    invoke-static/range {p0 .. p0}, LX/6IA;->w(LX/6IA;)V

    .line 1073175
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->ao:LX/6IG;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6IA;->an:LX/6IG;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/6IA;->n:LX/6IL;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/6IA;->o:LX/6IL;

    invoke-static {v2, v3, v5, v6}, LX/6IF;->a(LX/6IG;LX/6IG;LX/6IL;LX/6IL;)V

    .line 1073176
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "desired_initial_facing"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->J:I

    .line 1073177
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_profile"

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->L:I

    .line 1073178
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_max_duration"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->M:I

    .line 1073179
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_format"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->N:I

    .line 1073180
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_codec"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->O:I

    .line 1073181
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_width"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->P:I

    .line 1073182
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_height"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->Q:I

    .line 1073183
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_frame"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->R:I

    .line 1073184
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "video_bit_rate"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->S:I

    .line 1073185
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "audio_codec"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->T:I

    .line 1073186
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "audio_sample_rate"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->U:I

    .line 1073187
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "audio_bit_rate"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->V:I

    .line 1073188
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "audio_channels"

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/6IA;->W:I

    .line 1073189
    new-instance v2, LX/6HU;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/6IA;->aq:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/6IA;->ab:LX/6HF;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/6IA;->ai:LX/2Ib;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/6IA;->ae:LX/6Hj;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/6IA;->ah:LX/0Sh;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/6IA;->a:LX/0Zr;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/6IA;->af:LX/03V;

    move-object/from16 v0, p0

    iget v12, v0, LX/6IA;->L:I

    move-object/from16 v0, p0

    iget v13, v0, LX/6IA;->M:I

    move-object/from16 v0, p0

    iget v14, v0, LX/6IA;->N:I

    move-object/from16 v0, p0

    iget v15, v0, LX/6IA;->O:I

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->P:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->Q:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->R:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->S:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->T:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->U:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->V:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/6IA;->W:I

    move/from16 v23, v0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v23}, LX/6HU;-><init>(LX/6HO;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/2Ib;LX/6Hj;LX/0Sh;LX/0Zr;LX/03V;IIIIIIIIIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/6IA;->ar:LX/6HU;

    .line 1073190
    new-instance v2, LX/6I6;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v3}, LX/6I6;-><init>(LX/6IA;Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/6IA;->Z:LX/6I6;

    .line 1073191
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v2}, LX/6HU;->e()V

    .line 1073192
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "extra_no_composer"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/6IA;->E:Z

    .line 1073193
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "show_profile_crop_overlay"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/6IA;->F:Z

    .line 1073194
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "extra_target_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/6IA;->H:J

    .line 1073195
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "publisher_type"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/6IA;->I:Ljava/lang/String;

    .line 1073196
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "extra_disable_video"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/6IA;->G:Z

    .line 1073197
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v3, "fire_review_after_snap"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/6IA;->K:Z

    .line 1073198
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->n:LX/6IL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v3}, LX/6HU;->m()Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6IL;->a(Landroid/view/View$OnTouchListener;)V

    .line 1073199
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->r:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1073200
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/6IA;->F:Z

    if-eqz v2, :cond_0

    invoke-static/range {p0 .. p0}, LX/6IA;->z(LX/6IA;)F

    move-result v2

    const v3, 0x3f19999a    # 0.6f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 1073201
    invoke-static/range {p0 .. p0}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 1073202
    const v3, 0x7f03022d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1073203
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1073204
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6IA;->h:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1073205
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v2}, LX/6HU;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1073206
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6IA;->f(LX/6IA;Z)V

    .line 1073207
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->s:Landroid/widget/RelativeLayout;

    new-instance v3, LX/6I2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6I2;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1073208
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->l:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6IA;->ax:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1073209
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->l:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6IA;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1073210
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->m:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6IA;->az:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1073211
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1073212
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->m:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1073213
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->l:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1073214
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/6IA;->G:Z

    if-eqz v2, :cond_2

    .line 1073215
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1073216
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->t:Landroid/widget/TextView;

    new-instance v3, LX/6I5;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6I5;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1073217
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->u:Landroid/widget/TextView;

    new-instance v3, LX/6Hv;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6Hv;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1073218
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->aa:LX/1r1;

    const/16 v3, 0xa

    const-string v4, "Camera"

    invoke-virtual {v2, v3, v4}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/6IA;->Y:LX/1ql;

    .line 1073219
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->Y:LX/1ql;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ql;->a(Z)V

    .line 1073220
    return-void

    .line 1073221
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->o:LX/6IL;

    new-instance v3, LX/6I1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6I1;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, LX/6IL;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1073222
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/6IA;->ak:Z

    if-eqz v2, :cond_3

    .line 1073223
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    new-instance v3, LX/6I3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6I3;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1073224
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    new-instance v3, LX/6I4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/6I4;-><init>(LX/6IA;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/6HR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "LX/6HR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1073225
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    sget-object v1, LX/6IA;->e:Landroid/graphics/Point;

    invoke-interface {v0, p1, p2, p3, v1}, LX/6HE;->a(Ljava/util/List;Ljava/util/List;LX/6HR;Landroid/graphics/Point;)V

    .line 1073226
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1073227
    if-nez p1, :cond_1

    .line 1073228
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    invoke-interface {v0}, LX/6HE;->o()V

    .line 1073229
    :cond_0
    :goto_0
    return-void

    .line 1073230
    :cond_1
    iget-object v0, p0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v0}, LX/6HF;->b()V

    .line 1073231
    iget-boolean v0, p0, LX/6IA;->z:Z

    if-nez v0, :cond_2

    .line 1073232
    iget-object v0, p0, LX/6IA;->n:LX/6IL;

    invoke-virtual {v0}, LX/6IL;->a()V

    .line 1073233
    iget-object v0, p0, LX/6IA;->o:LX/6IL;

    invoke-virtual {v0}, LX/6IL;->a()V

    .line 1073234
    invoke-direct {p0}, LX/6IA;->s()F

    move-result v0

    invoke-static {p0, v0, v2}, LX/6IA;->a(LX/6IA;FI)V

    .line 1073235
    iput-boolean v1, p0, LX/6IA;->z:Z

    .line 1073236
    :cond_2
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    .line 1073237
    iget-boolean p1, v0, LX/6HU;->k:Z

    move v0, p1

    .line 1073238
    if-nez v0, :cond_3

    .line 1073239
    invoke-static {p0, v1}, LX/6IA;->f(LX/6IA;Z)V

    .line 1073240
    :cond_3
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1073241
    invoke-direct {p0, v1}, LX/6IA;->e(Z)V

    .line 1073242
    :cond_4
    iget-object v0, p0, LX/6IA;->k:Lcom/facebook/camera/views/RotateLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/camera/views/RotateLayout;->setVisibility(I)V

    .line 1073243
    iget-object v0, p0, LX/6IA;->Z:LX/6I6;

    invoke-virtual {v0, v1}, LX/6I6;->a(Z)V

    .line 1073244
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    .line 1073245
    iget-object v1, v0, LX/6I7;->c:LX/6II;

    move-object v0, v1

    .line 1073246
    sget-object v1, LX/6II;->QUEUED:LX/6II;

    if-ne v0, v1, :cond_0

    .line 1073247
    invoke-static {p0}, LX/6IA;->x(LX/6IA;)V

    goto :goto_0
.end method

.method public final a([BI)V
    .locals 13

    .prologue
    const/4 v3, 0x1

    .line 1073248
    iget-object v0, p0, LX/6IA;->ac:LX/6HG;

    .line 1073249
    iget v1, v0, LX/6HG;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/6HG;->a:I

    .line 1073250
    iget-object v0, p0, LX/6IA;->ab:LX/6HF;

    iget-object v1, p0, LX/6IA;->ac:LX/6HG;

    array-length v2, p1

    invoke-interface {v0, v1, v2}, LX/6HF;->a(LX/6HG;I)V

    .line 1073251
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    invoke-interface {v0, p1, p2}, LX/6HE;->a([BI)V

    .line 1073252
    iget-boolean v0, p0, LX/6IA;->K:Z

    if-eqz v0, :cond_1

    .line 1073253
    const/4 v0, 0x0

    invoke-static {p0, v0, v3}, LX/6IA;->a(LX/6IA;Landroid/net/Uri;I)V

    .line 1073254
    :cond_0
    :goto_0
    return-void

    .line 1073255
    :cond_1
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/6HE;->d(I)V

    .line 1073256
    iget-object v0, p0, LX/6IA;->ar:LX/6HU;

    .line 1073257
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/6HU;->s:Z

    .line 1073258
    iget-object v4, v0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v4, :cond_2

    .line 1073259
    :try_start_0
    iget-object v4, v0, LX/6HU;->d:Landroid/hardware/Camera;

    const v5, -0x4e60bb31

    invoke-static {v4, v5}, LX/0J2;->b(Landroid/hardware/Camera;I)V

    .line 1073260
    iget-object v4, v0, LX/6HU;->n:LX/6Ha;

    const-wide/16 v11, 0x7d0

    const/4 v9, 0x0

    .line 1073261
    iget v7, v4, LX/6Ha;->c:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_4

    .line 1073262
    iput v9, v4, LX/6Ha;->c:I

    .line 1073263
    const-wide/16 v7, 0x0

    iput-wide v7, v4, LX/6Ha;->z:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1073264
    :cond_2
    :goto_1
    iget-boolean v4, v0, LX/6HU;->s:Z

    if-nez v4, :cond_3

    .line 1073265
    invoke-static {v0}, LX/6HU;->K(LX/6HU;)V

    .line 1073266
    invoke-static {v0}, LX/6HU;->G(LX/6HU;)V

    .line 1073267
    :cond_3
    iget-boolean v4, v0, LX/6HU;->s:Z

    move v0, v4

    .line 1073268
    if-eqz v0, :cond_0

    .line 1073269
    invoke-virtual {p0, v3}, LX/6IA;->a(Z)V

    goto :goto_0

    .line 1073270
    :catch_0
    move-exception v4

    .line 1073271
    iget-object v5, v0, LX/6HU;->B:LX/6HF;

    const-string v6, "onResumePreview/startPreview failed"

    invoke-interface {v5, v6, v4}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1073272
    const/4 v4, 0x0

    iput-boolean v4, v0, LX/6HU;->s:Z

    goto :goto_1

    .line 1073273
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    add-long/2addr v7, v11

    iput-wide v7, v4, LX/6Ha;->z:J

    .line 1073274
    iget-object v7, v4, LX/6Ha;->q:Landroid/os/Handler;

    invoke-virtual {v7, v9, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1073275
    iget-object v0, p0, LX/6IA;->y:Lcom/facebook/camera/views/ShutterView;

    invoke-virtual {v0, p0}, Lcom/facebook/camera/views/ShutterView;->a(LX/6I9;)V

    .line 1073276
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 1073277
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1073278
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 1073279
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1073280
    :goto_0
    iget-object v1, p0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x537

    invoke-static {p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1073281
    :cond_0
    return-void

    .line 1073282
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 1073283
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 1073284
    invoke-static {p0}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1073285
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1073286
    invoke-direct {p0, p1}, LX/6IA;->e(Z)V

    .line 1073287
    return-void
.end method

.method public final c()Lcom/facebook/camera/views/RotateLayout;
    .locals 1

    .prologue
    .line 1073288
    iget-object v0, p0, LX/6IA;->k:Lcom/facebook/camera/views/RotateLayout;

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1073289
    iget-object v0, p0, LX/6IA;->d:LX/6HE;

    invoke-interface {v0, p1}, LX/6HE;->a(Landroid/net/Uri;)V

    .line 1073290
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 1073291
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1073292
    iget v0, p0, LX/6IA;->J:I

    return v0
.end method

.method public final e()LX/6IG;
    .locals 1

    .prologue
    .line 1073317
    iget-object v0, p0, LX/6IA;->an:LX/6IG;

    return-object v0
.end method

.method public final f()LX/6IG;
    .locals 1

    .prologue
    .line 1073293
    iget-object v0, p0, LX/6IA;->ao:LX/6IG;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1073294
    iget v0, p0, LX/6IA;->ap:I

    return v0
.end method

.method public final h()LX/6Hd;
    .locals 1

    .prologue
    .line 1073295
    iget-object v0, p0, LX/6IA;->x:LX/6Hd;

    return-object v0
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 1073296
    iget-object v0, p0, LX/6IA;->D:LX/6I8;

    .line 1073297
    iget-object v1, v0, LX/6I7;->c:LX/6II;

    move-object v1, v1

    .line 1073298
    sget-object v2, LX/6II;->CAPTURE_PENDING:LX/6II;

    if-eq v1, v2, :cond_0

    .line 1073299
    iget-object v1, v0, LX/6I7;->a:LX/03V;

    sget-object v2, LX/6I7;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "shutterAnimationComplete while "

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1073300
    iget-object p0, v0, LX/6I7;->c:LX/6II;

    move-object p0, p0

    .line 1073301
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073302
    :cond_0
    sget-object v1, LX/6II;->READY:LX/6II;

    invoke-static {v0, v1}, LX/6I7;->b(LX/6I7;LX/6II;)V

    .line 1073303
    return-void
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 1073304
    :try_start_0
    iget-object v0, p0, LX/6IA;->ab:LX/6HF;

    const-string v1, "launching_video_recorder"

    invoke-interface {v0, v1}, LX/6HF;->a(Ljava/lang/String;)V

    .line 1073305
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1073306
    iget-object v1, p0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x538

    invoke-static {p0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1073307
    :goto_0
    return-void

    .line 1073308
    :catch_0
    iget-object v0, p0, LX/6IA;->aj:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0811a0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    const/16 v2, 0x11

    .line 1073309
    iput v2, v1, LX/27k;->b:I

    .line 1073310
    move-object v1, v1

    .line 1073311
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public setIconsRotationAngle(F)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1073312
    iput p1, p0, LX/6IA;->B:F

    .line 1073313
    iget-object v0, p0, LX/6IA;->l:Landroid/view/View;

    iget v1, p0, LX/6IA;->B:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 1073314
    iget-object v0, p0, LX/6IA;->j:Landroid/widget/ImageView;

    iget v1, p0, LX/6IA;->B:F

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1073315
    iget-object v0, p0, LX/6IA;->w:Landroid/widget/ImageView;

    iget v1, p0, LX/6IA;->B:F

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1073316
    return-void
.end method
