.class public final LX/6BQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1062309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1061997
    if-nez p0, :cond_1

    .line 1061998
    :cond_0
    :goto_0
    return-object v0

    .line 1061999
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->s()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1062000
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x78fb05b

    if-ne v1, v2, :cond_0

    .line 1062001
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 1062002
    invoke-interface {p0}, LX/220;->b()Z

    move-result v0

    .line 1062003
    iput-boolean v0, v2, LX/3dM;->d:Z

    .line 1062004
    invoke-interface {p0}, LX/220;->c()Z

    move-result v0

    .line 1062005
    iput-boolean v0, v2, LX/3dM;->e:Z

    .line 1062006
    invoke-interface {p0}, LX/220;->d()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->c(Z)LX/3dM;

    .line 1062007
    invoke-interface {p0}, LX/220;->e()Z

    move-result v0

    .line 1062008
    iput-boolean v0, v2, LX/3dM;->g:Z

    .line 1062009
    invoke-interface {p0}, LX/220;->ac_()Z

    move-result v0

    .line 1062010
    iput-boolean v0, v2, LX/3dM;->h:Z

    .line 1062011
    invoke-interface {p0}, LX/220;->ad_()Z

    move-result v0

    .line 1062012
    iput-boolean v0, v2, LX/3dM;->i:Z

    .line 1062013
    invoke-interface {p0}, LX/220;->j()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->g(Z)LX/3dM;

    .line 1062014
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->t()Z

    move-result v0

    .line 1062015
    iput-boolean v0, v2, LX/3dM;->k:Z

    .line 1062016
    invoke-interface {p0}, LX/220;->k()Z

    move-result v0

    .line 1062017
    iput-boolean v0, v2, LX/3dM;->l:Z

    .line 1062018
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->u()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;

    move-result-object v0

    .line 1062019
    if-nez v0, :cond_4

    .line 1062020
    const/4 v1, 0x0

    .line 1062021
    :goto_1
    move-object v0, v1

    .line 1062022
    iput-object v0, v2, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 1062023
    invoke-interface {p0}, LX/220;->l()Ljava/lang/String;

    move-result-object v0

    .line 1062024
    iput-object v0, v2, LX/3dM;->p:Ljava/lang/String;

    .line 1062025
    invoke-interface {p0}, LX/220;->m()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->j(Z)LX/3dM;

    .line 1062026
    invoke-interface {p0}, LX/220;->n()Ljava/lang/String;

    move-result-object v0

    .line 1062027
    iput-object v0, v2, LX/3dM;->y:Ljava/lang/String;

    .line 1062028
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    .line 1062029
    if-nez v0, :cond_5

    .line 1062030
    const/4 v1, 0x0

    .line 1062031
    :goto_2
    move-object v0, v1

    .line 1062032
    iput-object v0, v2, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1062033
    invoke-interface {p0}, LX/220;->o()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->l(Z)LX/3dM;

    .line 1062034
    invoke-interface {p0}, LX/220;->p()Ljava/lang/String;

    move-result-object v0

    .line 1062035
    iput-object v0, v2, LX/3dM;->D:Ljava/lang/String;

    .line 1062036
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062037
    iput-object v0, v2, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062038
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->E()Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;

    move-result-object v0

    .line 1062039
    if-nez v0, :cond_9

    .line 1062040
    const/4 v1, 0x0

    .line 1062041
    :goto_3
    move-object v0, v1

    .line 1062042
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1062043
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1062044
    if-nez v0, :cond_f

    .line 1062045
    const/4 v1, 0x0

    .line 1062046
    :goto_4
    move-object v0, v1

    .line 1062047
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1062048
    invoke-interface {p0}, LX/220;->q()Ljava/lang/String;

    move-result-object v0

    .line 1062049
    iput-object v0, v2, LX/3dM;->J:Ljava/lang/String;

    .line 1062050
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->F()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1062051
    if-nez v0, :cond_10

    .line 1062052
    const/4 v3, 0x0

    .line 1062053
    :goto_5
    move-object v0, v3

    .line 1062054
    iput-object v0, v2, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 1062055
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1062056
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1062057
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1062058
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 1062059
    if-nez v0, :cond_11

    .line 1062060
    const/4 v4, 0x0

    .line 1062061
    :goto_7
    move-object v0, v4

    .line 1062062
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062063
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1062064
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062065
    iput-object v0, v2, LX/3dM;->N:LX/0Px;

    .line 1062066
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    .line 1062067
    if-nez v0, :cond_12

    .line 1062068
    const/4 v1, 0x0

    .line 1062069
    :goto_8
    move-object v0, v1

    .line 1062070
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1062071
    invoke-interface {p0}, LX/220;->r()LX/59N;

    move-result-object v0

    .line 1062072
    if-nez v0, :cond_17

    .line 1062073
    const/4 v1, 0x0

    .line 1062074
    :goto_9
    move-object v0, v1

    .line 1062075
    iput-object v0, v2, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1062076
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    .line 1062077
    if-nez v0, :cond_18

    .line 1062078
    const/4 v1, 0x0

    .line 1062079
    :goto_a
    move-object v0, v1

    .line 1062080
    iput-object v0, v2, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1062081
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->B()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062082
    iput-object v0, v2, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062083
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->C()I

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->b(I)LX/3dM;

    .line 1062084
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;->D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062085
    iput-object v0, v2, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062086
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0

    .line 1062087
    :cond_4
    new-instance v1, LX/4Vv;

    invoke-direct {v1}, LX/4Vv;-><init>()V

    .line 1062088
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;->a()I

    move-result v3

    .line 1062089
    iput v3, v1, LX/4Vv;->b:I

    .line 1062090
    invoke-virtual {v1}, LX/4Vv;->a()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v1

    goto/16 :goto_1

    .line 1062091
    :cond_5
    new-instance v4, LX/4Wx;

    invoke-direct {v4}, LX/4Wx;-><init>()V

    .line 1062092
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1062093
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1062094
    const/4 v1, 0x0

    move v3, v1

    :goto_b
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_6

    .line 1062095
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 1062096
    if-nez v1, :cond_8

    .line 1062097
    const/4 v6, 0x0

    .line 1062098
    :goto_c
    move-object v1, v6

    .line 1062099
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062100
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_b

    .line 1062101
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1062102
    iput-object v1, v4, LX/4Wx;->b:LX/0Px;

    .line 1062103
    :cond_7
    invoke-virtual {v4}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v1

    goto/16 :goto_2

    .line 1062104
    :cond_8
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 1062105
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1062106
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062107
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 1062108
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 1062109
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto :goto_c

    .line 1062110
    :cond_9
    new-instance v4, LX/3dN;

    invoke-direct {v4}, LX/3dN;-><init>()V

    .line 1062111
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->b()I

    move-result v1

    .line 1062112
    iput v1, v4, LX/3dN;->b:I

    .line 1062113
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1062114
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1062115
    const/4 v1, 0x0

    move v3, v1

    :goto_d
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_a

    .line 1062116
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;

    .line 1062117
    if-nez v1, :cond_c

    .line 1062118
    const/4 v6, 0x0

    .line 1062119
    :goto_e
    move-object v1, v6

    .line 1062120
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062121
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_d

    .line 1062122
    :cond_a
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1062123
    iput-object v1, v4, LX/3dN;->c:LX/0Px;

    .line 1062124
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$LikersOfFeedbackFieldModel$LikersModel;->c()LX/0us;

    move-result-object v1

    .line 1062125
    if-nez v1, :cond_e

    .line 1062126
    const/4 v3, 0x0

    .line 1062127
    :goto_f
    move-object v1, v3

    .line 1062128
    iput-object v1, v4, LX/3dN;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1062129
    invoke-virtual {v4}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    goto/16 :goto_3

    .line 1062130
    :cond_c
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 1062131
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1062132
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062133
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 1062134
    iput-object v7, v6, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1062135
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 1062136
    iput-object v7, v6, LX/3dL;->E:Ljava/lang/String;

    .line 1062137
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->bt_()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 1062138
    if-nez v7, :cond_d

    .line 1062139
    const/4 v9, 0x0

    .line 1062140
    :goto_10
    move-object v7, v9

    .line 1062141
    iput-object v7, v6, LX/3dL;->af:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 1062142
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->e()Ljava/lang/String;

    move-result-object v7

    .line 1062143
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 1062144
    invoke-virtual {v1}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$DefaultLikersActorModel;->bu_()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/6BQ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1062145
    iput-object v7, v6, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1062146
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto :goto_e

    .line 1062147
    :cond_d
    new-instance v9, LX/4XM;

    invoke-direct {v9}, LX/4XM;-><init>()V

    .line 1062148
    const/4 v10, 0x0

    invoke-virtual {v8, v7, v10}, LX/15i;->j(II)I

    move-result v10

    .line 1062149
    iput v10, v9, LX/4XM;->b:I

    .line 1062150
    invoke-virtual {v9}, LX/4XM;->a()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v9

    goto :goto_10

    .line 1062151
    :cond_e
    new-instance v3, LX/17L;

    invoke-direct {v3}, LX/17L;-><init>()V

    .line 1062152
    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v5

    .line 1062153
    iput-object v5, v3, LX/17L;->c:Ljava/lang/String;

    .line 1062154
    invoke-interface {v1}, LX/0us;->b()Z

    move-result v5

    .line 1062155
    iput-boolean v5, v3, LX/17L;->d:Z

    .line 1062156
    invoke-interface {v1}, LX/0us;->c()Z

    move-result v5

    .line 1062157
    iput-boolean v5, v3, LX/17L;->e:Z

    .line 1062158
    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v5

    .line 1062159
    iput-object v5, v3, LX/17L;->f:Ljava/lang/String;

    .line 1062160
    invoke-virtual {v3}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    goto :goto_f

    .line 1062161
    :cond_f
    new-instance v1, LX/3dO;

    invoke-direct {v1}, LX/3dO;-><init>()V

    .line 1062162
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v3

    .line 1062163
    iput v3, v1, LX/3dO;->b:I

    .line 1062164
    invoke-virtual {v1}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    goto/16 :goto_4

    .line 1062165
    :cond_10
    new-instance v3, LX/4Yj;

    invoke-direct {v3}, LX/4Yj;-><init>()V

    .line 1062166
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, LX/15i;->j(II)I

    move-result v4

    .line 1062167
    iput v4, v3, LX/4Yj;->b:I

    .line 1062168
    invoke-virtual {v3}, LX/4Yj;->a()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v3

    goto/16 :goto_5

    .line 1062169
    :cond_11
    new-instance v4, LX/4WL;

    invoke-direct {v4}, LX/4WL;-><init>()V

    .line 1062170
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v5

    .line 1062171
    iput v5, v4, LX/4WL;->b:I

    .line 1062172
    invoke-virtual {v4}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v4

    goto/16 :goto_7

    .line 1062173
    :cond_12
    new-instance v4, LX/3dQ;

    invoke-direct {v4}, LX/3dQ;-><init>()V

    .line 1062174
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1062175
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1062176
    const/4 v1, 0x0

    move v3, v1

    :goto_11
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_13

    .line 1062177
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 1062178
    if-nez v1, :cond_15

    .line 1062179
    const/4 v6, 0x0

    .line 1062180
    :goto_12
    move-object v1, v6

    .line 1062181
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062182
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_11

    .line 1062183
    :cond_13
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1062184
    iput-object v1, v4, LX/3dQ;->b:LX/0Px;

    .line 1062185
    :cond_14
    invoke-virtual {v4}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    goto/16 :goto_8

    .line 1062186
    :cond_15
    new-instance v6, LX/4ZJ;

    invoke-direct {v6}, LX/4ZJ;-><init>()V

    .line 1062187
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v7

    .line 1062188
    if-nez v7, :cond_16

    .line 1062189
    const/4 v8, 0x0

    .line 1062190
    :goto_13
    move-object v7, v8

    .line 1062191
    iput-object v7, v6, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1062192
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v7

    .line 1062193
    iput v7, v6, LX/4ZJ;->c:I

    .line 1062194
    invoke-virtual {v6}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v6

    goto :goto_12

    .line 1062195
    :cond_16
    new-instance v8, LX/4WM;

    invoke-direct {v8}, LX/4WM;-><init>()V

    .line 1062196
    invoke-virtual {v7}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v9

    .line 1062197
    iput v9, v8, LX/4WM;->f:I

    .line 1062198
    invoke-virtual {v8}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v8

    goto :goto_13

    .line 1062199
    :cond_17
    new-instance v1, LX/4XY;

    invoke-direct {v1}, LX/4XY;-><init>()V

    .line 1062200
    invoke-interface {v0}, LX/59N;->b()Ljava/lang/String;

    move-result-object v3

    .line 1062201
    iput-object v3, v1, LX/4XY;->ag:Ljava/lang/String;

    .line 1062202
    invoke-interface {v0}, LX/59N;->c()Ljava/lang/String;

    move-result-object v3

    .line 1062203
    iput-object v3, v1, LX/4XY;->aT:Ljava/lang/String;

    .line 1062204
    invoke-interface {v0}, LX/59N;->d()LX/1Fb;

    move-result-object v3

    invoke-static {v3}, LX/6BQ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1062205
    iput-object v3, v1, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1062206
    invoke-virtual {v1}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    goto/16 :goto_9

    .line 1062207
    :cond_18
    new-instance v1, LX/33O;

    invoke-direct {v1}, LX/33O;-><init>()V

    .line 1062208
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1062209
    iput-object v3, v1, LX/33O;->aI:Ljava/lang/String;

    .line 1062210
    invoke-virtual {v1}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    goto/16 :goto_a
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1062211
    if-nez p0, :cond_0

    .line 1062212
    const/4 v0, 0x0

    .line 1062213
    :goto_0
    return-object v0

    .line 1062214
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1062215
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1062216
    iput v1, v0, LX/2dc;->c:I

    .line 1062217
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1062218
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1062219
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1062220
    iput v1, v0, LX/2dc;->i:I

    .line 1062221
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1062222
    if-nez p0, :cond_0

    .line 1062223
    const/4 v0, 0x0

    .line 1062224
    :goto_0
    return-object v0

    .line 1062225
    :cond_0
    new-instance v3, LX/173;

    invoke-direct {v3}, LX/173;-><init>()V

    .line 1062226
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062227
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1062228
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062229
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;

    .line 1062230
    if-nez v0, :cond_5

    .line 1062231
    const/4 v5, 0x0

    .line 1062232
    :goto_2
    move-object v0, v5

    .line 1062233
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062235
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062236
    iput-object v0, v3, LX/173;->b:LX/0Px;

    .line 1062237
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1062238
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1062239
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1062240
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1062241
    if-nez v0, :cond_9

    .line 1062242
    const/4 v4, 0x0

    .line 1062243
    :goto_4
    move-object v0, v4

    .line 1062244
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062245
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1062246
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062247
    iput-object v0, v3, LX/173;->e:LX/0Px;

    .line 1062248
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1062249
    iput-object v0, v3, LX/173;->f:Ljava/lang/String;

    .line 1062250
    invoke-virtual {v3}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1062251
    :cond_5
    new-instance v7, LX/4Vo;

    invoke-direct {v7}, LX/4Vo;-><init>()V

    .line 1062252
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->a()I

    move-result v5

    .line 1062253
    iput v5, v7, LX/4Vo;->b:I

    .line 1062254
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->b()I

    move-result v5

    .line 1062255
    iput v5, v7, LX/4Vo;->c:I

    .line 1062256
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->c()I

    move-result v5

    .line 1062257
    iput v5, v7, LX/4Vo;->d:I

    .line 1062258
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 1062259
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1062260
    const/4 v5, 0x0

    move v6, v5

    :goto_5
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_6

    .line 1062261
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;

    .line 1062262
    if-nez v5, :cond_8

    .line 1062263
    const/4 v9, 0x0

    .line 1062264
    :goto_6
    move-object v5, v9

    .line 1062265
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062266
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_5

    .line 1062267
    :cond_6
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1062268
    iput-object v5, v7, LX/4Vo;->e:LX/0Px;

    .line 1062269
    :cond_7
    invoke-virtual {v7}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v5

    goto/16 :goto_2

    .line 1062270
    :cond_8
    new-instance v9, LX/170;

    invoke-direct {v9}, LX/170;-><init>()V

    .line 1062271
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    .line 1062272
    iput-object v10, v9, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062273
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 1062274
    iput-object v10, v9, LX/170;->o:Ljava/lang/String;

    .line 1062275
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->d()Ljava/lang/String;

    move-result-object v10

    .line 1062276
    iput-object v10, v9, LX/170;->A:Ljava/lang/String;

    .line 1062277
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->e()LX/1Fb;

    move-result-object v10

    invoke-static {v10}, LX/6BQ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    .line 1062278
    iput-object v10, v9, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1062279
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->B_()Ljava/lang/String;

    move-result-object v10

    .line 1062280
    iput-object v10, v9, LX/170;->Y:Ljava/lang/String;

    .line 1062281
    invoke-virtual {v9}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    goto :goto_6

    .line 1062282
    :cond_9
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 1062283
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v5

    .line 1062284
    if-nez v5, :cond_a

    .line 1062285
    const/4 v6, 0x0

    .line 1062286
    :goto_7
    move-object v5, v6

    .line 1062287
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1062288
    invoke-interface {v0}, LX/1W5;->b()I

    move-result v5

    .line 1062289
    iput v5, v4, LX/4W6;->c:I

    .line 1062290
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    .line 1062291
    iput v5, v4, LX/4W6;->d:I

    .line 1062292
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto/16 :goto_4

    .line 1062293
    :cond_a
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 1062294
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1062295
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062296
    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v7

    .line 1062297
    iput-object v7, v6, LX/170;->b:LX/0Px;

    .line 1062298
    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v7

    .line 1062299
    iput-object v7, v6, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1062300
    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v7

    .line 1062301
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 1062302
    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v7

    .line 1062303
    iput-object v7, v6, LX/170;->A:Ljava/lang/String;

    .line 1062304
    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v7

    .line 1062305
    iput-object v7, v6, LX/170;->X:Ljava/lang/String;

    .line 1062306
    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    .line 1062307
    iput-object v7, v6, LX/170;->Y:Ljava/lang/String;

    .line 1062308
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_7
.end method
