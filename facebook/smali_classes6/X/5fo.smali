.class public LX/5fo;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/util/concurrent/ThreadPoolExecutor;

.field private static final b:Ljava/util/concurrent/ThreadPoolExecutor;

.field public static final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/util/concurrent/FutureTask;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 972195
    invoke-static {}, LX/5fo;->d()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    sput-object v0, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 972196
    invoke-static {}, LX/5fo;->d()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    sput-object v0, LX/5fo;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 972197
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/5fo;->c:Ljava/util/HashSet;

    .line 972198
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/5fo;->d:Ljava/util/HashSet;

    .line 972199
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/5fo;->e:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 972164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972165
    return-void
.end method

.method public static declared-synchronized a()V
    .locals 5

    .prologue
    .line 972177
    const-class v1, LX/5fo;

    monitor-enter v1

    .line 972178
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 972179
    if-nez v0, :cond_0

    .line 972180
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Background tasks may only be terminated on the UI thread"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 972181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 972182
    :cond_0
    :try_start_1
    sget-object v0, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 972183
    sget-object v3, LX/5fo;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 972184
    :cond_1
    sget-object v0, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 972185
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 972186
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 972187
    sget-object v3, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 972188
    :cond_2
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 972189
    sget-object v0, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 972190
    :try_start_2
    sget-object v0, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 972191
    :goto_3
    :try_start_3
    invoke-static {}, LX/5fo;->d()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    sput-object v0, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 972192
    monitor-exit v1

    return-void

    :catch_0
    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 972193
    sget-object v0, LX/5fo;->e:Landroid/os/Handler;

    const v1, -0x7e2c3da2

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 972194
    return-void
.end method

.method public static declared-synchronized a(Ljava/util/concurrent/FutureTask;LX/5f5;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/FutureTask",
            "<TT;>;",
            "LX/5f5",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 972172
    const-class v1, LX/5fo;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1}, LX/5fo;->b(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    .line 972173
    sget-object v0, LX/5fo;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 972174
    sget-object v0, LX/5fo;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    const v2, 0x4722dc89

    invoke-static {v0, p0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 972175
    monitor-exit v1

    return-void

    .line 972176
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Ljava/util/concurrent/FutureTask;LX/5f5;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/FutureTask",
            "<TT;>;",
            "LX/5f5",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 972167
    const-class v1, LX/5fo;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/facebook/optic/util/ThreadUtil$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/optic/util/ThreadUtil$1;-><init>(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    .line 972168
    sget-object v2, LX/5fo;->d:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 972169
    sget-object v2, LX/5fo;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, -0x59bac7e4

    invoke-static {v2, v0, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 972170
    monitor-exit v1

    return-void

    .line 972171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized d()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .prologue
    .line 972166
    const-class v8, LX/5fo;

    monitor-enter v8

    :try_start_0
    new-instance v1, LX/5fn;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, LX/5fn;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method
