.class public LX/6BW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/6BW;


# instance fields
.field public final a:LX/2IT;

.field private final b:LX/2Ia;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sh;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6Bb;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(LX/2IT;LX/2Ia;LX/0Or;LX/0Sh;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/assetdownload/IsInAssetDownloadMainGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2IT;",
            "LX/2Ia;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/6Bb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1062793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062794
    const/4 v0, 0x0

    iput-object v0, p0, LX/6BW;->f:Ljava/util/regex/Pattern;

    .line 1062795
    iput-object p1, p0, LX/6BW;->a:LX/2IT;

    .line 1062796
    iput-object p2, p0, LX/6BW;->b:LX/2Ia;

    .line 1062797
    iput-object p3, p0, LX/6BW;->c:LX/0Or;

    .line 1062798
    iput-object p4, p0, LX/6BW;->d:LX/0Sh;

    .line 1062799
    iput-object p5, p0, LX/6BW;->e:LX/0Or;

    .line 1062800
    return-void
.end method

.method public static a(LX/0QB;)LX/6BW;
    .locals 9

    .prologue
    .line 1062801
    sget-object v0, LX/6BW;->g:LX/6BW;

    if-nez v0, :cond_1

    .line 1062802
    const-class v1, LX/6BW;

    monitor-enter v1

    .line 1062803
    :try_start_0
    sget-object v0, LX/6BW;->g:LX/6BW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1062804
    if-eqz v2, :cond_0

    .line 1062805
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1062806
    new-instance v3, LX/6BW;

    invoke-static {v0}, LX/2IT;->b(LX/0QB;)LX/2IT;

    move-result-object v4

    check-cast v4, LX/2IT;

    invoke-static {v0}, LX/2Ia;->b(LX/0QB;)LX/2Ia;

    move-result-object v5

    check-cast v5, LX/2Ia;

    const/16 v6, 0x144d

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    const/16 v8, 0x1787

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/6BW;-><init>(LX/2IT;LX/2Ia;LX/0Or;LX/0Sh;LX/0Or;)V

    .line 1062807
    move-object v0, v3

    .line 1062808
    sput-object v0, LX/6BW;->g:LX/6BW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1062809
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1062810
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1062811
    :cond_1
    sget-object v0, LX/6BW;->g:LX/6BW;

    return-object v0

    .line 1062812
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1062813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/6BW;)Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 1062814
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6BW;->f:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 1062815
    const-string v0, "[a-zA-Z0-9_]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/6BW;->f:Ljava/util/regex/Pattern;

    .line 1062816
    :cond_0
    iget-object v0, p0, LX/6BW;->f:Ljava/util/regex/Pattern;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1062817
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
