.class public abstract LX/5ub;
.super LX/5ua;
.source ""


# instance fields
.field public a:Z

.field public final b:[F

.field public final c:[F

.field public final d:[F

.field private final e:Landroid/graphics/Matrix;

.field public final f:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(LX/5uZ;)V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 1019763
    invoke-direct {p0, p1}, LX/5ua;-><init>(LX/5uZ;)V

    .line 1019764
    new-array v0, v1, [F

    iput-object v0, p0, LX/5ub;->b:[F

    .line 1019765
    new-array v0, v1, [F

    iput-object v0, p0, LX/5ub;->c:[F

    .line 1019766
    new-array v0, v1, [F

    iput-object v0, p0, LX/5ub;->d:[F

    .line 1019767
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/5ub;->e:Landroid/graphics/Matrix;

    .line 1019768
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/5ub;->f:Landroid/graphics/Matrix;

    .line 1019769
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1019758
    invoke-virtual {p0}, LX/5ub;->g()V

    .line 1019759
    iget-object v0, p0, LX/5ub;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1019760
    iget-object v0, p0, LX/5ub;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1019761
    invoke-super {p0}, LX/5ua;->a()V

    .line 1019762
    return-void
.end method

.method public final a(FLandroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 9

    .prologue
    .line 1019756
    const/4 v5, 0x7

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v1 .. v8}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    .line 1019757
    return-void
.end method

.method public final a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V
    .locals 7
    .param p7    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1019753
    iget-object v1, p0, LX/5ub;->e:Landroid/graphics/Matrix;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/5ua;->a(Landroid/graphics/Matrix;FLandroid/graphics/PointF;Landroid/graphics/PointF;I)Z

    .line 1019754
    iget-object v0, p0, LX/5ub;->e:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, p5, p6, p7}, LX/5ub;->a(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V

    .line 1019755
    return-void
.end method

.method public final a(LX/5uZ;)V
    .locals 0

    .prologue
    .line 1019770
    invoke-virtual {p0}, LX/5ub;->g()V

    .line 1019771
    invoke-super {p0, p1}, LX/5ua;->a(LX/5uZ;)V

    .line 1019772
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V
    .locals 2
    .param p4    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1019745
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 1019746
    invoke-virtual {p0}, LX/5ub;->g()V

    .line 1019747
    iget-object v0, p0, LX/5ub;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1019748
    invoke-super {p0, p1}, LX/5ua;->b(Landroid/graphics/Matrix;)V

    .line 1019749
    iget-object v0, p0, LX/5ua;->c:LX/5uZ;

    move-object v0, v0

    .line 1019750
    invoke-virtual {v0}, LX/5uZ;->e()V

    .line 1019751
    :goto_0
    return-void

    .line 1019752
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/5ub;->b(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(LX/5uZ;)V
    .locals 1

    .prologue
    .line 1019741
    iget-boolean v0, p0, LX/5ub;->a:Z

    move v0, v0

    .line 1019742
    if-eqz v0, :cond_0

    .line 1019743
    :goto_0
    return-void

    .line 1019744
    :cond_0
    invoke-super {p0, p1}, LX/5ua;->b(LX/5uZ;)V

    goto :goto_0
.end method

.method public abstract b(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V
    .param p4    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1019739
    iget-boolean v0, p0, LX/5ub;->a:Z

    move v0, v0

    .line 1019740
    if-nez v0, :cond_0

    invoke-super {p0}, LX/5ua;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract g()V
.end method
