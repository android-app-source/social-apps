.class public final LX/6X4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1107219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107220
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    .line 1107221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    .line 1107222
    iput-object v1, v0, LX/3dL;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 1107223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v1

    .line 1107224
    iput-object v1, v0, LX/3dL;->c:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 1107225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->n()Z

    move-result v1

    .line 1107226
    iput-boolean v1, v0, LX/3dL;->k:Z

    .line 1107227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->p()Z

    move-result v1

    .line 1107228
    iput-boolean v1, v0, LX/3dL;->n:Z

    .line 1107229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->q()Z

    move-result v1

    .line 1107230
    iput-boolean v1, v0, LX/3dL;->p:Z

    .line 1107231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v1

    .line 1107232
    iput-object v1, v0, LX/3dL;->r:LX/0Px;

    .line 1107233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    .line 1107234
    iput-object v1, v0, LX/3dL;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 1107235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->x()LX/0Px;

    move-result-object v1

    .line 1107236
    iput-object v1, v0, LX/3dL;->w:LX/0Px;

    .line 1107237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    .line 1107238
    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 1107239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->D()Z

    move-result v1

    .line 1107240
    iput-boolean v1, v0, LX/3dL;->H:Z

    .line 1107241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->E()Z

    move-result v1

    .line 1107242
    iput-boolean v1, v0, LX/3dL;->L:Z

    .line 1107243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->F()Z

    move-result v1

    .line 1107244
    iput-boolean v1, v0, LX/3dL;->N:Z

    .line 1107245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v1

    .line 1107246
    iput-boolean v1, v0, LX/3dL;->R:Z

    .line 1107247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->J()Z

    move-result v1

    .line 1107248
    iput-boolean v1, v0, LX/3dL;->S:Z

    .line 1107249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->L()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v1

    .line 1107250
    iput-object v1, v0, LX/3dL;->W:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 1107251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->M()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v1

    .line 1107252
    iput-object v1, v0, LX/3dL;->X:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 1107253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 1107254
    iput-object v1, v0, LX/3dL;->Y:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1107255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->P()Ljava/lang/String;

    move-result-object v1

    .line 1107256
    iput-object v1, v0, LX/3dL;->ae:Ljava/lang/String;

    .line 1107257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v1

    .line 1107258
    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 1107259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->R()LX/0Px;

    move-result-object v1

    .line 1107260
    iput-object v1, v0, LX/3dL;->ah:LX/0Px;

    .line 1107261
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ba()Z

    move-result v1

    .line 1107262
    iput-boolean v1, v0, LX/3dL;->aj:Z

    .line 1107263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    .line 1107264
    iput-object v1, v0, LX/3dL;->ak:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 1107265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107266
    iput-object v1, v0, LX/3dL;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107267
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107268
    iput-object v1, v0, LX/3dL;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Z()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    .line 1107270
    iput-object v1, v0, LX/3dL;->an:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1107271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aa()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1107272
    iput-object v1, v0, LX/3dL;->ao:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1107273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107274
    iput-object v1, v0, LX/3dL;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107275
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ah()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    .line 1107276
    iput-object v1, v0, LX/3dL;->ar:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1107277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107278
    iput-object v1, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aj()Z

    move-result v1

    .line 1107280
    iput-boolean v1, v0, LX/3dL;->au:Z

    .line 1107281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ak()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v1

    .line 1107282
    iput-object v1, v0, LX/3dL;->av:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 1107283
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->am()Ljava/lang/String;

    move-result-object v1

    .line 1107284
    iput-object v1, v0, LX/3dL;->aw:Ljava/lang/String;

    .line 1107285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->an()Ljava/lang/String;

    move-result-object v1

    .line 1107286
    iput-object v1, v0, LX/3dL;->ax:Ljava/lang/String;

    .line 1107287
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    .line 1107288
    iput-object v1, v0, LX/3dL;->ay:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1107289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v1

    .line 1107290
    iput-object v1, v0, LX/3dL;->aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 1107291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107292
    iput-object v1, v0, LX/3dL;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107293
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107294
    iput-object v1, v0, LX/3dL;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107296
    iput-object v1, v0, LX/3dL;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107297
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 1107298
    iput-object v1, v0, LX/3dL;->aG:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1107299
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aD()I

    move-result v1

    .line 1107300
    iput v1, v0, LX/3dL;->aI:I

    .line 1107301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v1

    .line 1107302
    iput-object v1, v0, LX/3dL;->aK:Ljava/lang/String;

    .line 1107303
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aF()Ljava/lang/String;

    move-result-object v1

    .line 1107304
    iput-object v1, v0, LX/3dL;->aL:Ljava/lang/String;

    .line 1107305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aG()Z

    move-result v1

    .line 1107306
    iput-boolean v1, v0, LX/3dL;->aM:Z

    .line 1107307
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aH()Z

    move-result v1

    .line 1107308
    iput-boolean v1, v0, LX/3dL;->aN:Z

    .line 1107309
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aI()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1107310
    iput-object v1, v0, LX/3dL;->aO:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1107311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aJ()Z

    move-result v1

    .line 1107312
    iput-boolean v1, v0, LX/3dL;->aP:Z

    .line 1107313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aK()Z

    move-result v1

    .line 1107314
    iput-boolean v1, v0, LX/3dL;->aQ:Z

    .line 1107315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107316
    iput-object v1, v0, LX/3dL;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107317
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107318
    iput-object v1, v0, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107319
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aN()Z

    move-result v1

    .line 1107320
    iput-boolean v1, v0, LX/3dL;->aT:Z

    .line 1107321
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x25d6af

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1107322
    iput-object v1, v0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1107323
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107324
    new-instance v0, LX/25F;

    invoke-direct {v0}, LX/25F;-><init>()V

    .line 1107325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    .line 1107326
    iput-object v1, v0, LX/25F;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 1107327
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->l()LX/0Px;

    move-result-object v1

    .line 1107328
    iput-object v1, v0, LX/25F;->c:LX/0Px;

    .line 1107329
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107330
    iput-object v1, v0, LX/25F;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->o()Z

    move-result v1

    .line 1107332
    iput-boolean v1, v0, LX/25F;->g:Z

    .line 1107333
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->p()Z

    move-result v1

    .line 1107334
    iput-boolean v1, v0, LX/25F;->h:Z

    .line 1107335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->q()Z

    move-result v1

    .line 1107336
    iput-boolean v1, v0, LX/25F;->i:Z

    .line 1107337
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v1

    .line 1107338
    iput-object v1, v0, LX/25F;->k:LX/0Px;

    .line 1107339
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    .line 1107340
    iput-object v1, v0, LX/25F;->o:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 1107341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v1

    .line 1107342
    iput-boolean v1, v0, LX/25F;->p:Z

    .line 1107343
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->x()LX/0Px;

    move-result-object v1

    .line 1107344
    iput-object v1, v0, LX/25F;->q:LX/0Px;

    .line 1107345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->y()I

    move-result v1

    .line 1107346
    iput v1, v0, LX/25F;->s:I

    .line 1107347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->z()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v1

    .line 1107348
    iput-object v1, v0, LX/25F;->t:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 1107349
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107350
    iput-object v1, v0, LX/25F;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107351
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    .line 1107352
    iput-object v1, v0, LX/25F;->C:Ljava/lang/String;

    .line 1107353
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aV()Z

    move-result v1

    .line 1107354
    iput-boolean v1, v0, LX/25F;->F:Z

    .line 1107355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->F()Z

    move-result v1

    .line 1107356
    iput-boolean v1, v0, LX/25F;->I:Z

    .line 1107357
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->H()Z

    move-result v1

    .line 1107358
    iput-boolean v1, v0, LX/25F;->L:Z

    .line 1107359
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v1

    .line 1107360
    iput-boolean v1, v0, LX/25F;->N:Z

    .line 1107361
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->J()Z

    move-result v1

    .line 1107362
    iput-boolean v1, v0, LX/25F;->O:Z

    .line 1107363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v1

    .line 1107364
    iput-object v1, v0, LX/25F;->T:Ljava/lang/String;

    .line 1107365
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->R()LX/0Px;

    move-result-object v1

    .line 1107366
    iput-object v1, v0, LX/25F;->U:LX/0Px;

    .line 1107367
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->T()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1107368
    iput-object v1, v0, LX/25F;->V:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1107369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->V()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    .line 1107370
    iput-object v1, v0, LX/25F;->X:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 1107371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aa()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1107372
    iput-object v1, v0, LX/25F;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1107373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107374
    iput-object v1, v0, LX/25F;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107376
    iput-object v1, v0, LX/25F;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107378
    iput-object v1, v0, LX/25F;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107380
    iput-object v1, v0, LX/25F;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107382
    iput-object v1, v0, LX/25F;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107384
    iput-object v1, v0, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aj()Z

    move-result v1

    .line 1107386
    iput-boolean v1, v0, LX/25F;->ah:Z

    .line 1107387
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107388
    iput-object v1, v0, LX/25F;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107389
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->al()Ljava/lang/String;

    move-result-object v1

    .line 1107390
    iput-object v1, v0, LX/25F;->aj:Ljava/lang/String;

    .line 1107391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    .line 1107392
    iput-object v1, v0, LX/25F;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1107393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->av()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 1107394
    iput-object v1, v0, LX/25F;->an:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 1107395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 1107396
    iput-object v1, v0, LX/25F;->ap:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1107397
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107398
    iput-object v1, v0, LX/25F;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aA()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v1

    .line 1107400
    iput-object v1, v0, LX/25F;->as:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 1107401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aB()Ljava/lang/String;

    move-result-object v1

    .line 1107402
    iput-object v1, v0, LX/25F;->at:Ljava/lang/String;

    .line 1107403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aC()Ljava/lang/String;

    move-result-object v1

    .line 1107404
    iput-object v1, v0, LX/25F;->au:Ljava/lang/String;

    .line 1107405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aD()I

    move-result v1

    .line 1107406
    iput v1, v0, LX/25F;->av:I

    .line 1107407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v1

    .line 1107408
    iput-object v1, v0, LX/25F;->ax:Ljava/lang/String;

    .line 1107409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aF()Ljava/lang/String;

    move-result-object v1

    .line 1107410
    iput-object v1, v0, LX/25F;->ay:Ljava/lang/String;

    .line 1107411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aP()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 1107412
    iput-object v1, v0, LX/25F;->aD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1107413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aR()LX/0Px;

    move-result-object v1

    .line 1107414
    iput-object v1, v0, LX/25F;->aG:LX/0Px;

    .line 1107415
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x25d6af

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1107416
    iput-object v1, v0, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1107417
    invoke-virtual {v0}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    return-object v0
.end method
