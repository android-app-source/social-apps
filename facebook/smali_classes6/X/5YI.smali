.class public final LX/5YI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 941241
    const-wide/16 v22, 0x0

    .line 941242
    const-wide/16 v20, 0x0

    .line 941243
    const-wide/16 v18, 0x0

    .line 941244
    const-wide/16 v16, 0x0

    .line 941245
    const-wide/16 v14, 0x0

    .line 941246
    const-wide/16 v12, 0x0

    .line 941247
    const-wide/16 v10, 0x0

    .line 941248
    const/4 v9, 0x0

    .line 941249
    const/4 v8, 0x0

    .line 941250
    const/4 v7, 0x0

    .line 941251
    const/4 v6, 0x0

    .line 941252
    const/4 v5, 0x0

    .line 941253
    const/4 v4, 0x0

    .line 941254
    const/4 v3, 0x0

    .line 941255
    const/4 v2, 0x0

    .line 941256
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_11

    .line 941257
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 941258
    const/4 v2, 0x0

    .line 941259
    :goto_0
    return v2

    .line 941260
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_9

    .line 941261
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 941262
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 941263
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 941264
    const-string v6, "accuracy_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 941265
    const/4 v2, 0x1

    .line 941266
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 941267
    :cond_1
    const-string v6, "altitude_accuracy_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 941268
    const/4 v2, 0x1

    .line 941269
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v13, v2

    move-wide/from16 v26, v6

    goto :goto_1

    .line 941270
    :cond_2
    const-string v6, "altitude_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 941271
    const/4 v2, 0x1

    .line 941272
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v24, v6

    goto :goto_1

    .line 941273
    :cond_3
    const-string v6, "bearing_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 941274
    const/4 v2, 0x1

    .line 941275
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v22, v6

    goto :goto_1

    .line 941276
    :cond_4
    const-string v6, "latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 941277
    const/4 v2, 0x1

    .line 941278
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 941279
    :cond_5
    const-string v6, "longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 941280
    const/4 v2, 0x1

    .line 941281
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v18, v6

    goto :goto_1

    .line 941282
    :cond_6
    const-string v6, "speed_meters_per_second"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 941283
    const/4 v2, 0x1

    .line 941284
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 941285
    :cond_7
    const-string v6, "timestamp_milliseconds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 941286
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 941287
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 941288
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 941289
    if-eqz v3, :cond_a

    .line 941290
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941291
    :cond_a
    if-eqz v13, :cond_b

    .line 941292
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941293
    :cond_b
    if-eqz v12, :cond_c

    .line 941294
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941295
    :cond_c
    if-eqz v11, :cond_d

    .line 941296
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941297
    :cond_d
    if-eqz v10, :cond_e

    .line 941298
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941299
    :cond_e
    if-eqz v9, :cond_f

    .line 941300
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941301
    :cond_f
    if-eqz v8, :cond_10

    .line 941302
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 941303
    :cond_10
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 941304
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move-wide/from16 v24, v18

    move-wide/from16 v26, v20

    move-wide/from16 v18, v12

    move-wide/from16 v20, v14

    move v12, v6

    move v13, v7

    move v14, v9

    move v9, v3

    move v3, v8

    move v8, v2

    move/from16 v28, v5

    move-wide/from16 v29, v16

    move-wide/from16 v16, v10

    move/from16 v11, v28

    move v10, v4

    move-wide/from16 v4, v22

    move-wide/from16 v22, v29

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 941305
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 941306
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941307
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 941308
    const-string v2, "accuracy_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941309
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941310
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941311
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 941312
    const-string v2, "altitude_accuracy_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941313
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941314
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941315
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 941316
    const-string v2, "altitude_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941317
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941318
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941319
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 941320
    const-string v2, "bearing_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941321
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941322
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941323
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 941324
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941325
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941326
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941327
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 941328
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941329
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941330
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 941331
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 941332
    const-string v2, "speed_meters_per_second"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941333
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 941334
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 941335
    if-eqz v0, :cond_7

    .line 941336
    const-string v1, "timestamp_milliseconds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 941337
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 941338
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 941339
    return-void
.end method
