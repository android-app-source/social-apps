.class public LX/617;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/611;

.field public final b:Landroid/media/MediaCodec;

.field public final c:Landroid/view/Surface;

.field private final d:Z

.field public e:Landroid/media/MediaFormat;

.field private f:Z

.field private g:[Ljava/nio/ByteBuffer;

.field private h:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(LX/611;Landroid/media/MediaCodec;Landroid/view/Surface;Z)V
    .locals 1

    .prologue
    .line 1039672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039673
    if-eqz p3, :cond_0

    sget-object v0, LX/611;->ENCODER:LX/611;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1039674
    iput-object p1, p0, LX/617;->a:LX/611;

    .line 1039675
    iput-object p2, p0, LX/617;->b:Landroid/media/MediaCodec;

    .line 1039676
    iput-object p3, p0, LX/617;->c:Landroid/view/Surface;

    .line 1039677
    iput-boolean p4, p0, LX/617;->d:Z

    .line 1039678
    return-void

    .line 1039679
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)LX/614;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1039680
    iget-object v0, p0, LX/617;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1039681
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v2

    .line 1039682
    if-ltz v2, :cond_1

    .line 1039683
    new-instance v0, LX/614;

    iget-object v3, p0, LX/617;->g:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v2

    invoke-direct {v0, v3, v2, v1}, LX/614;-><init>(Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;)V

    .line 1039684
    :goto_1
    return-object v0

    .line 1039685
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1039686
    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1039687
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 1039688
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/617;->f:Z

    .line 1039689
    iget-object v0, p0, LX/617;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    .line 1039690
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/617;->g:[Ljava/nio/ByteBuffer;

    .line 1039691
    :cond_0
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/617;->h:[Ljava/nio/ByteBuffer;

    .line 1039692
    return-void
.end method

.method public final a(LX/614;)V
    .locals 7

    .prologue
    .line 1039693
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    .line 1039694
    iget v1, p1, LX/614;->b:I

    move v1, v1

    .line 1039695
    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v2

    iget v2, v2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    iget v3, v3, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v4

    iget-wide v4, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v6

    iget v6, v6, Landroid/media/MediaCodec$BufferInfo;->flags:I

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1039696
    return-void
.end method

.method public final b(J)LX/614;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1039697
    new-instance v1, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v1}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1039698
    iget-object v2, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v2, v1, p1, p2}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    .line 1039699
    if-ltz v2, :cond_0

    .line 1039700
    new-instance v0, LX/614;

    iget-object v3, p0, LX/617;->h:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v2

    invoke-direct {v0, v3, v2, v1}, LX/614;-><init>(Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;)V

    .line 1039701
    :goto_0
    :pswitch_0
    return-object v0

    .line 1039702
    :cond_0
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1039703
    :pswitch_1
    iget-object v1, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, LX/617;->h:[Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1039704
    :pswitch_2
    iget-object v1, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v1

    iput-object v1, p0, LX/617;->e:Landroid/media/MediaFormat;

    .line 1039705
    new-instance v1, LX/614;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2, v0}, LX/614;-><init>(Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;)V

    .line 1039706
    const/4 v0, 0x1

    .line 1039707
    iput-boolean v0, v1, LX/614;->d:Z

    .line 1039708
    move-object v0, v1

    .line 1039709
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1039710
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    .line 1039711
    iget-boolean v0, p0, LX/617;->f:Z

    if-eqz v0, :cond_0

    .line 1039712
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1039713
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/617;->f:Z

    .line 1039714
    :cond_0
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 1039715
    iput-object v1, p0, LX/617;->g:[Ljava/nio/ByteBuffer;

    .line 1039716
    iput-object v1, p0, LX/617;->h:[Ljava/nio/ByteBuffer;

    .line 1039717
    iput-object v1, p0, LX/617;->e:Landroid/media/MediaFormat;

    .line 1039718
    :cond_1
    iget-object v0, p0, LX/617;->c:Landroid/view/Surface;

    if-eqz v0, :cond_2

    .line 1039719
    iget-object v0, p0, LX/617;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1039720
    :cond_2
    return-void
.end method

.method public final b(LX/614;)V
    .locals 3

    .prologue
    .line 1039721
    invoke-virtual {p1}, LX/614;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039722
    iget-object v0, p0, LX/617;->b:Landroid/media/MediaCodec;

    .line 1039723
    iget v1, p1, LX/614;->b:I

    move v1, v1

    .line 1039724
    iget-boolean v2, p0, LX/617;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1039725
    :cond_0
    return-void
.end method
