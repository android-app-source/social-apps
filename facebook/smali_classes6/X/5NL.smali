.class public LX/5NL;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public a:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 906232
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 906233
    const-class v0, LX/5NL;

    invoke-static {v0, p0}, LX/5NL;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 906234
    const v0, 0x7f08064e

    invoke-virtual {p0, v0}, LX/5NL;->setTitle(I)V

    .line 906235
    iget-object v0, p0, LX/5NL;->a:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Enabled"

    :goto_0
    invoke-virtual {p0, v0}, LX/5NL;->setSummary(Ljava/lang/CharSequence;)V

    .line 906236
    return-void

    .line 906237
    :cond_0
    const-string v0, "Disabled"

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/5NL;

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object p0

    check-cast p0, LX/0yc;

    iput-object p0, p1, LX/5NL;->a:LX/0yc;

    return-void
.end method
