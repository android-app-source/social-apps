.class public LX/6FS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/6FT;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1068028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1067989
    check-cast p1, LX/6FT;

    .line 1067990
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    .line 1067991
    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v8

    .line 1067992
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0nA;->a(LX/0nD;)V

    .line 1067993
    const-string v0, "config_id"

    const-string v1, "624618737631578"

    .line 1067994
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1067995
    const-string v0, "id"

    iget-object v1, p1, LX/6FT;->a:Ljava/lang/String;

    .line 1067996
    invoke-static {v8, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1067997
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1067998
    iget-object v1, p1, LX/6FT;->c:Ljava/io/File;

    .line 1067999
    iget-object v3, p1, LX/6FT;->b:Ljava/lang/String;

    .line 1068000
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068001
    const/4 v0, 0x0

    .line 1068002
    :goto_0
    return-object v0

    .line 1068003
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1068004
    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".jpeg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1068005
    :cond_1
    const-string v2, "image/jpeg"

    .line 1068006
    :goto_1
    new-instance v0, LX/4cu;

    const-wide/16 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 1068007
    new-instance v1, LX/4cQ;

    invoke-direct {v1, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1068008
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "bugReportAttachmentUpload"

    .line 1068009
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1068010
    move-object v0, v0

    .line 1068011
    const-string v1, "POST"

    .line 1068012
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1068013
    move-object v0, v0

    .line 1068014
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/6FT;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/attachments"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1068015
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1068016
    move-object v0, v0

    .line 1068017
    iput-object v9, v0, LX/14O;->l:Ljava/util/List;

    .line 1068018
    move-object v0, v0

    .line 1068019
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1068020
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1068021
    move-object v0, v0

    .line 1068022
    iput-object v8, v0, LX/14O;->h:LX/0n9;

    .line 1068023
    move-object v0, v0

    .line 1068024
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    goto :goto_0

    .line 1068025
    :cond_2
    const-string v2, ".png"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1068026
    const-string v2, "image/png"

    goto :goto_1

    .line 1068027
    :cond_3
    const-string v2, "text/plain"

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1068029
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1068030
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
