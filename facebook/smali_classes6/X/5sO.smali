.class public LX/5sO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/5sH;

.field private final b:LX/5sH;

.field private final c:LX/5sH;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1012826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012827
    new-instance v0, LX/5sQ;

    invoke-direct {v0}, LX/5sQ;-><init>()V

    iput-object v0, p0, LX/5sO;->a:LX/5sH;

    .line 1012828
    new-instance v0, LX/5sS;

    invoke-direct {v0}, LX/5sS;-><init>()V

    iput-object v0, p0, LX/5sO;->b:LX/5sH;

    .line 1012829
    new-instance v0, LX/5sR;

    invoke-direct {v0}, LX/5sR;-><init>()V

    iput-object v0, p0, LX/5sO;->c:LX/5sH;

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1012819
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1012820
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 1012821
    check-cast p1, Landroid/view/ViewGroup;

    .line 1012822
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1012823
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, LX/5sO;->b(Landroid/view/View;)V

    .line 1012824
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012825
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1012830
    iget-object v0, p0, LX/5sO;->a:LX/5sH;

    invoke-virtual {v0}, LX/5sH;->b()V

    .line 1012831
    iget-object v0, p0, LX/5sO;->b:LX/5sH;

    invoke-virtual {v0}, LX/5sH;->b()V

    .line 1012832
    iget-object v0, p0, LX/5sO;->c:LX/5sH;

    invoke-virtual {v0}, LX/5sH;->b()V

    .line 1012833
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5sO;->d:Z

    .line 1012834
    return-void
.end method

.method public final a(LX/5pG;)V
    .locals 4
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 1012805
    if-nez p1, :cond_1

    .line 1012806
    invoke-virtual {p0}, LX/5sO;->a()V

    .line 1012807
    :cond_0
    :goto_0
    return-void

    .line 1012808
    :cond_1
    iput-boolean v0, p0, LX/5sO;->d:Z

    .line 1012809
    const-string v1, "duration"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "duration"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1012810
    :cond_2
    sget-object v1, LX/5sP;->CREATE:LX/5sP;

    invoke-virtual {v1}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1012811
    iget-object v1, p0, LX/5sO;->a:LX/5sH;

    sget-object v2, LX/5sP;->CREATE:LX/5sP;

    invoke-virtual {v2}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/5sH;->a(LX/5pG;I)V

    .line 1012812
    iput-boolean v3, p0, LX/5sO;->d:Z

    .line 1012813
    :cond_3
    sget-object v1, LX/5sP;->UPDATE:LX/5sP;

    invoke-virtual {v1}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1012814
    iget-object v1, p0, LX/5sO;->b:LX/5sH;

    sget-object v2, LX/5sP;->UPDATE:LX/5sP;

    invoke-virtual {v2}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/5sH;->a(LX/5pG;I)V

    .line 1012815
    iput-boolean v3, p0, LX/5sO;->d:Z

    .line 1012816
    :cond_4
    sget-object v1, LX/5sP;->DELETE:LX/5sP;

    invoke-virtual {v1}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012817
    iget-object v1, p0, LX/5sO;->c:LX/5sH;

    sget-object v2, LX/5sP;->DELETE:LX/5sP;

    invoke-virtual {v2}, LX/5sP;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/5sH;->a(LX/5pG;I)V

    .line 1012818
    iput-boolean v3, p0, LX/5sO;->d:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 1012796
    invoke-static {}, LX/5pe;->b()V

    .line 1012797
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    iget-object v0, p0, LX/5sO;->a:LX/5sH;

    :goto_0
    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 1012798
    invoke-virtual/range {v0 .. v5}, LX/5sH;->b(Landroid/view/View;IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1012799
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/5sL;

    if-nez v1, :cond_2

    .line 1012800
    :cond_1
    add-int v1, p2, p4

    add-int v2, p3, p5

    invoke-virtual {p1, p2, p3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 1012801
    :cond_2
    if-eqz v0, :cond_3

    .line 1012802
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1012803
    :cond_3
    return-void

    .line 1012804
    :cond_4
    iget-object v0, p0, LX/5sO;->b:LX/5sH;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/5qt;)V
    .locals 6

    .prologue
    .line 1012787
    invoke-static {}, LX/5pe;->b()V

    .line 1012788
    iget-object v0, p0, LX/5sO;->c:LX/5sH;

    .line 1012789
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/5sH;->b(Landroid/view/View;IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1012790
    if-eqz v0, :cond_0

    .line 1012791
    invoke-direct {p0, p1}, LX/5sO;->b(Landroid/view/View;)V

    .line 1012792
    new-instance v1, LX/5sN;

    invoke-direct {v1, p0, p2}, LX/5sN;-><init>(LX/5sO;LX/5qt;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1012793
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1012794
    :goto_0
    return-void

    .line 1012795
    :cond_0
    invoke-interface {p2}, LX/5qt;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1012786
    iget-boolean v0, p0, LX/5sO;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
