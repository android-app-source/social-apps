.class public final LX/5Km;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Km;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Kk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Kn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899109
    const/4 v0, 0x0

    sput-object v0, LX/5Km;->a:LX/5Km;

    .line 899110
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Km;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 899111
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 899112
    new-instance v0, LX/5Kn;

    invoke-direct {v0}, LX/5Kn;-><init>()V

    iput-object v0, p0, LX/5Km;->c:LX/5Kn;

    .line 899113
    return-void
.end method

.method public static declared-synchronized q()LX/5Km;
    .locals 2

    .prologue
    .line 899114
    const-class v1, LX/5Km;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Km;->a:LX/5Km;

    if-nez v0, :cond_0

    .line 899115
    new-instance v0, LX/5Km;

    invoke-direct {v0}, LX/5Km;-><init>()V

    sput-object v0, LX/5Km;->a:LX/5Km;

    .line 899116
    :cond_0
    sget-object v0, LX/5Km;->a:LX/5Km;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 899117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 899118
    check-cast p2, LX/5Kl;

    .line 899119
    iget-object v0, p2, LX/5Kl;->a:Lcom/facebook/java2js/JSValue;

    iget-object v1, p2, LX/5Kl;->b:LX/5KI;

    .line 899120
    const-string v2, "component"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v2

    .line 899121
    if-nez v2, :cond_0

    .line 899122
    const/4 v2, 0x0

    .line 899123
    :goto_0
    move-object v0, v2

    .line 899124
    return-object v0

    .line 899125
    :cond_0
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 899126
    const-string v3, "insets"

    invoke-virtual {v0, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    .line 899127
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x1

    const-string v5, "top"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-int v5, v6

    invoke-interface {v2, v4, v5}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x0

    const-string v5, "left"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-int v5, v6

    invoke-interface {v2, v4, v5}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x3

    const-string v5, "bottom"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-int v5, v6

    invoke-interface {v2, v4, v5}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x2

    const-string v5, "right"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-int v3, v6

    invoke-interface {v2, v4, v3}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 899128
    invoke-static {}, LX/1dS;->b()V

    .line 899129
    const/4 v0, 0x0

    return-object v0
.end method
