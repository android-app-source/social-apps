.class public final enum LX/5fO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5fO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5fO;

.field public static final enum DEACTIVATED:LX/5fO;

.field public static final enum HIGH:LX/5fO;

.field public static final enum LOW:LX/5fO;

.field public static final enum MEDIUM:LX/5fO;


# instance fields
.field public mId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 971225
    new-instance v0, LX/5fO;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2, v2}, LX/5fO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fO;->HIGH:LX/5fO;

    .line 971226
    new-instance v0, LX/5fO;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3, v3}, LX/5fO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fO;->MEDIUM:LX/5fO;

    .line 971227
    new-instance v0, LX/5fO;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4, v4}, LX/5fO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fO;->LOW:LX/5fO;

    .line 971228
    new-instance v0, LX/5fO;

    const-string v1, "DEACTIVATED"

    invoke-direct {v0, v1, v5, v5}, LX/5fO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fO;->DEACTIVATED:LX/5fO;

    .line 971229
    const/4 v0, 0x4

    new-array v0, v0, [LX/5fO;

    sget-object v1, LX/5fO;->HIGH:LX/5fO;

    aput-object v1, v0, v2

    sget-object v1, LX/5fO;->MEDIUM:LX/5fO;

    aput-object v1, v0, v3

    sget-object v1, LX/5fO;->LOW:LX/5fO;

    aput-object v1, v0, v4

    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    aput-object v1, v0, v5

    sput-object v0, LX/5fO;->$VALUES:[LX/5fO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 971222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 971223
    iput p3, p0, LX/5fO;->mId:I

    .line 971224
    return-void
.end method

.method public static fromId(I)LX/5fO;
    .locals 5

    .prologue
    .line 971217
    invoke-static {}, LX/5fO;->values()[LX/5fO;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 971218
    iget v4, v3, LX/5fO;->mId:I

    if-ne v4, p0, :cond_0

    .line 971219
    return-object v3

    .line 971220
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 971221
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5fO;
    .locals 1

    .prologue
    .line 971215
    const-class v0, LX/5fO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5fO;

    return-object v0
.end method

.method public static values()[LX/5fO;
    .locals 1

    .prologue
    .line 971216
    sget-object v0, LX/5fO;->$VALUES:[LX/5fO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5fO;

    return-object v0
.end method
