.class public final LX/6On;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Oo;


# direct methods
.method public constructor <init>(LX/6Oo;)V
    .locals 0

    .prologue
    .line 1085551
    iput-object p1, p0, LX/6On;->a:LX/6Oo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1085552
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1085553
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchContactsResult;

    .line 1085554
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v1, v1

    .line 1085555
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1085556
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v0, v1

    .line 1085557
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 1085558
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
