.class public final LX/5HA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lcom/facebook/graphql/model/GraphQLActor;

.field public d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public e:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 889671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889672
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5HA;
    .locals 0

    .prologue
    .line 889669
    iput-object p1, p0, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889670
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLActor;)LX/5HA;
    .locals 0

    .prologue
    .line 889667
    iput-object p1, p0, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889668
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/5HA;
    .locals 0

    .prologue
    .line 889662
    iput-object p1, p0, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 889663
    return-object p0
.end method

.method public final a(Z)LX/5HA;
    .locals 0

    .prologue
    .line 889665
    iput-boolean p1, p0, LX/5HA;->b:Z

    .line 889666
    return-object p0
.end method

.method public final a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;
    .locals 7

    .prologue
    .line 889664
    new-instance v0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    iget-object v1, p0, LX/5HA;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/5HA;->b:Z

    iget-object v3, p0, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v5, p0, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v6, p0, LX/5HA;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;-><init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    return-object v0
.end method
