.class public final LX/6UH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1101218
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1101219
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1101220
    :goto_0
    return v1

    .line 1101221
    :cond_0
    const-string v11, "tip_amount"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1101222
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    .line 1101223
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 1101224
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1101225
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1101226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1101227
    const-string v11, "amount_received"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1101228
    invoke-static {p0, p1}, LX/6UF;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1101229
    :cond_2
    const-string v11, "comment"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1101230
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1101231
    :cond_3
    const-string v11, "tip_giver"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1101232
    invoke-static {p0, p1}, LX/6UG;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1101233
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1101234
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1101235
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1101236
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 1101237
    if-eqz v0, :cond_6

    .line 1101238
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1101239
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1101240
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    goto :goto_1
.end method
