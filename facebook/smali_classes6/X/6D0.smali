.class public LX/6D0;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/6Cz;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064549
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1064550
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)LX/6Cz;
    .locals 12

    .prologue
    .line 1064533
    new-instance v0, LX/6Cz;

    .line 1064534
    new-instance v4, LX/6D3;

    .line 1064535
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/6DB;

    invoke-direct {v3, p0}, LX/6DB;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v1

    .line 1064536
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/6D1;->b(LX/0QB;)LX/6D1;

    move-result-object v2

    check-cast v2, LX/6D1;

    const-class v3, LX/6D0;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/6D0;

    invoke-direct {v4, v5, v1, v2, v3}, LX/6D3;-><init>(Ljava/util/Set;LX/03V;LX/6D1;LX/6D0;)V

    .line 1064537
    move-object v1, v4

    .line 1064538
    check-cast v1, LX/6D3;

    .line 1064539
    new-instance v2, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/6DE;

    invoke-direct {v4, p0}, LX/6DE;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v2

    .line 1064540
    new-instance v3, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v4

    new-instance v5, LX/6D8;

    invoke-direct {v5, p0}, LX/6D8;-><init>(LX/0QB;)V

    invoke-direct {v3, v4, v5}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v3, v3

    .line 1064541
    new-instance v4, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/6DF;

    invoke-direct {v6, p0}, LX/6DF;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1064542
    new-instance v5, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance v7, LX/6DC;

    invoke-direct {v7, p0}, LX/6DC;-><init>(LX/0QB;)V

    invoke-direct {v5, v6, v7}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v5

    .line 1064543
    new-instance v6, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/6Eq;

    invoke-direct {v8, p0}, LX/6Eq;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 1064544
    new-instance v7, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance v9, LX/6DA;

    invoke-direct {v9, p0}, LX/6DA;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, v9}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    .line 1064545
    new-instance v8, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v9

    new-instance v10, LX/6D9;

    invoke-direct {v10, p0}, LX/6D9;-><init>(LX/0QB;)V

    invoke-direct {v8, v9, v10}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v8, v8

    .line 1064546
    new-instance v9, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    new-instance v11, LX/6DD;

    invoke-direct {v11, p0}, LX/6DD;-><init>(LX/0QB;)V

    invoke-direct {v9, v10, v11}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v9, v9

    .line 1064547
    move-object v10, p1

    invoke-direct/range {v0 .. v10}, LX/6Cz;-><init>(LX/6D3;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Landroid/os/Bundle;)V

    .line 1064548
    return-object v0
.end method
