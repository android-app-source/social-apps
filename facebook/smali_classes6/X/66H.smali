.class public final LX/66H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/66G;


# instance fields
.field public final a:LX/64w;

.field public final b:LX/65W;

.field public final c:LX/671;

.field public final d:LX/670;

.field public e:I


# direct methods
.method public constructor <init>(LX/64w;LX/65W;LX/671;LX/670;)V
    .locals 1

    .prologue
    .line 1049785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049786
    const/4 v0, 0x0

    iput v0, p0, LX/66H;->e:I

    .line 1049787
    iput-object p1, p0, LX/66H;->a:LX/64w;

    .line 1049788
    iput-object p2, p0, LX/66H;->b:LX/65W;

    .line 1049789
    iput-object p3, p0, LX/66H;->c:LX/671;

    .line 1049790
    iput-object p4, p0, LX/66H;->d:LX/670;

    .line 1049791
    return-void
.end method

.method public static a(LX/66H;LX/64q;)LX/65D;
    .locals 3

    .prologue
    .line 1049782
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049783
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, LX/66H;->e:I

    .line 1049784
    new-instance v0, LX/66C;

    invoke-direct {v0, p0, p1}, LX/66C;-><init>(LX/66H;LX/64q;)V

    return-object v0
.end method

.method private b(J)LX/65J;
    .locals 3

    .prologue
    .line 1049779
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049780
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, LX/66H;->e:I

    .line 1049781
    new-instance v0, LX/66D;

    invoke-direct {v0, p0, p1, p2}, LX/66D;-><init>(LX/66H;J)V

    return-object v0
.end method

.method private f()LX/65J;
    .locals 3

    .prologue
    .line 1049776
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049777
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, LX/66H;->e:I

    .line 1049778
    new-instance v0, LX/66B;

    invoke-direct {v0, p0}, LX/66B;-><init>(LX/66H;)V

    return-object v0
.end method

.method public static g(LX/66H;)LX/65D;
    .locals 3

    .prologue
    .line 1049771
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049772
    :cond_0
    iget-object v0, p0, LX/66H;->b:LX/65W;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "streamAllocation == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049773
    :cond_1
    const/4 v0, 0x5

    iput v0, p0, LX/66H;->e:I

    .line 1049774
    iget-object v0, p0, LX/66H;->b:LX/65W;

    invoke-virtual {v0}, LX/65W;->d()V

    .line 1049775
    new-instance v0, LX/66F;

    invoke-direct {v0, p0}, LX/66F;-><init>(LX/66H;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/655;)LX/656;
    .locals 7

    .prologue
    .line 1049687
    invoke-static {p1}, LX/66M;->b(LX/655;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1049688
    const-wide/16 v3, 0x0

    invoke-virtual {p0, v3, v4}, LX/66H;->a(J)LX/65D;

    move-result-object v3

    .line 1049689
    :goto_0
    move-object v0, v3

    .line 1049690
    new-instance v1, LX/66P;

    .line 1049691
    iget-object v2, p1, LX/655;->f:LX/64n;

    move-object v2, v2

    .line 1049692
    invoke-static {v0}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/66P;-><init>(LX/64n;LX/671;)V

    return-object v1

    .line 1049693
    :cond_0
    const-string v3, "chunked"

    const-string v4, "Transfer-Encoding"

    invoke-virtual {p1, v4}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1049694
    iget-object v3, p1, LX/655;->a:LX/650;

    move-object v3, v3

    .line 1049695
    iget-object v4, v3, LX/650;->a:LX/64q;

    move-object v3, v4

    .line 1049696
    invoke-static {p0, v3}, LX/66H;->a(LX/66H;LX/64q;)LX/65D;

    move-result-object v3

    goto :goto_0

    .line 1049697
    :cond_1
    invoke-static {p1}, LX/66M;->a(LX/655;)J

    move-result-wide v3

    .line 1049698
    const-wide/16 v5, -0x1

    cmp-long v5, v3, v5

    if-eqz v5, :cond_2

    .line 1049699
    invoke-virtual {p0, v3, v4}, LX/66H;->a(J)LX/65D;

    move-result-object v3

    goto :goto_0

    .line 1049700
    :cond_2
    invoke-static {p0}, LX/66H;->g(LX/66H;)LX/65D;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(J)LX/65D;
    .locals 3

    .prologue
    .line 1049768
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049769
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, LX/66H;->e:I

    .line 1049770
    new-instance v0, LX/66E;

    invoke-direct {v0, p0, p1, p2}, LX/66E;-><init>(LX/66H;J)V

    return-object v0
.end method

.method public final a(LX/650;J)LX/65J;
    .locals 2

    .prologue
    .line 1049762
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    invoke-virtual {p1, v1}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1049763
    invoke-direct {p0}, LX/66H;->f()LX/65J;

    move-result-object v0

    .line 1049764
    :goto_0
    return-object v0

    .line 1049765
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    .line 1049766
    invoke-direct {p0, p2, p3}, LX/66H;->b(J)LX/65J;

    move-result-object v0

    goto :goto_0

    .line 1049767
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1049759
    iget-object v0, p0, LX/66H;->b:LX/65W;

    invoke-virtual {v0}, LX/65W;->b()LX/65S;

    move-result-object v0

    .line 1049760
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/65S;->b()V

    .line 1049761
    :cond_0
    return-void
.end method

.method public final a(LX/64n;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1049748
    iget v0, p0, LX/66H;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049749
    :cond_0
    iget-object v0, p0, LX/66H;->d:LX/670;

    invoke-interface {v0, p2}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1049750
    const/4 v0, 0x0

    invoke-virtual {p1}, LX/64n;->a()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1049751
    iget-object v2, p0, LX/66H;->d:LX/670;

    invoke-virtual {p1, v0}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v2

    const-string v3, ": "

    .line 1049752
    invoke-interface {v2, v3}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v2

    .line 1049753
    invoke-virtual {p1, v0}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v2

    const-string v3, "\r\n"

    .line 1049754
    invoke-interface {v2, v3}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1049755
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1049756
    :cond_1
    iget-object v0, p0, LX/66H;->d:LX/670;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1049757
    const/4 v0, 0x1

    iput v0, p0, LX/66H;->e:I

    .line 1049758
    return-void
.end method

.method public final a(LX/650;)V
    .locals 3

    .prologue
    .line 1049728
    iget-object v0, p0, LX/66H;->b:LX/65W;

    .line 1049729
    invoke-virtual {v0}, LX/65W;->b()LX/65S;

    move-result-object v0

    .line 1049730
    iget-object v1, v0, LX/65S;->k:LX/657;

    move-object v0, v1

    .line 1049731
    iget-object v1, v0, LX/657;->b:Ljava/net/Proxy;

    move-object v0, v1

    .line 1049732
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    .line 1049733
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1049734
    iget-object v2, p1, LX/650;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1049735
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049736
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1049737
    invoke-virtual {p1}, LX/650;->f()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1049738
    if-eqz v2, :cond_0

    .line 1049739
    iget-object v2, p1, LX/650;->a:LX/64q;

    move-object v2, v2

    .line 1049740
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1049741
    :goto_1
    const-string v2, " HTTP/1.1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049742
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1049743
    iget-object v1, p1, LX/650;->c:LX/64n;

    move-object v1, v1

    .line 1049744
    invoke-virtual {p0, v1, v0}, LX/66H;->a(LX/64n;Ljava/lang/String;)V

    .line 1049745
    return-void

    .line 1049746
    :cond_0
    iget-object v2, p1, LX/650;->a:LX/64q;

    move-object v2, v2

    .line 1049747
    invoke-static {v2}, LX/66Q;->a(LX/64q;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()LX/654;
    .locals 1

    .prologue
    .line 1049727
    invoke-virtual {p0}, LX/66H;->d()LX/654;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1049725
    iget-object v0, p0, LX/66H;->d:LX/670;

    invoke-interface {v0}, LX/670;->flush()V

    .line 1049726
    return-void
.end method

.method public final d()LX/654;
    .locals 4

    .prologue
    .line 1049705
    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/66H;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1049706
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66H;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049707
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/66H;->c:LX/671;

    invoke-interface {v0}, LX/671;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/66S;->a(Ljava/lang/String;)LX/66S;

    move-result-object v0

    .line 1049708
    new-instance v1, LX/654;

    invoke-direct {v1}, LX/654;-><init>()V

    iget-object v2, v0, LX/66S;->a:LX/64x;

    .line 1049709
    iput-object v2, v1, LX/654;->b:LX/64x;

    .line 1049710
    move-object v1, v1

    .line 1049711
    iget v2, v0, LX/66S;->b:I

    .line 1049712
    iput v2, v1, LX/654;->c:I

    .line 1049713
    move-object v1, v1

    .line 1049714
    iget-object v2, v0, LX/66S;->c:Ljava/lang/String;

    .line 1049715
    iput-object v2, v1, LX/654;->d:Ljava/lang/String;

    .line 1049716
    move-object v1, v1

    .line 1049717
    invoke-virtual {p0}, LX/66H;->e()LX/64n;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/654;->a(LX/64n;)LX/654;

    move-result-object v1

    .line 1049718
    iget v0, v0, LX/66S;->b:I

    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    .line 1049719
    const/4 v0, 0x4

    iput v0, p0, LX/66H;->e:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1049720
    return-object v1

    .line 1049721
    :catch_0
    move-exception v0

    .line 1049722
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected end of stream on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/66H;->b:LX/65W;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1049723
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1049724
    throw v1
.end method

.method public final e()LX/64n;
    .locals 3

    .prologue
    .line 1049701
    new-instance v0, LX/64m;

    invoke-direct {v0}, LX/64m;-><init>()V

    .line 1049702
    :goto_0
    iget-object v1, p0, LX/66H;->c:LX/671;

    invoke-interface {v1}, LX/671;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1049703
    sget-object v2, LX/64t;->a:LX/64t;

    invoke-virtual {v2, v0, v1}, LX/64t;->a(LX/64m;Ljava/lang/String;)V

    goto :goto_0

    .line 1049704
    :cond_0
    invoke-virtual {v0}, LX/64m;->a()LX/64n;

    move-result-object v0

    return-object v0
.end method
