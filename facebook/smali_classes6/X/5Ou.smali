.class public LX/5Ou;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0tH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 909602
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/5Ou;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 909598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909599
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 909600
    iput-object v0, p0, LX/5Ou;->a:LX/0Ot;

    .line 909601
    return-void
.end method

.method public static a(LX/0QB;)LX/5Ou;
    .locals 8

    .prologue
    .line 909567
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 909568
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 909569
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 909570
    if-nez v1, :cond_0

    .line 909571
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 909572
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 909573
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 909574
    sget-object v1, LX/5Ou;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 909575
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 909576
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 909577
    :cond_1
    if-nez v1, :cond_4

    .line 909578
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 909579
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 909580
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 909581
    new-instance v7, LX/5Ou;

    invoke-direct {v7}, LX/5Ou;-><init>()V

    .line 909582
    const/16 v1, 0x1032

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v1

    check-cast v1, LX/0tH;

    .line 909583
    iput-object p0, v7, LX/5Ou;->a:LX/0Ot;

    iput-object v1, v7, LX/5Ou;->b:LX/0tH;

    .line 909584
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 909585
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 909586
    if-nez v1, :cond_2

    .line 909587
    sget-object v0, LX/5Ou;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ou;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 909588
    :goto_1
    if-eqz v0, :cond_3

    .line 909589
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 909590
    :goto_3
    check-cast v0, LX/5Ou;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 909591
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 909592
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 909593
    :catchall_1
    move-exception v0

    .line 909594
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 909595
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 909596
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 909597
    :cond_2
    :try_start_8
    sget-object v0, LX/5Ou;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ou;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 909603
    iget-object v0, p0, LX/5Ou;->b:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 909604
    iget-object v0, p0, LX/5Ou;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 909605
    iget-object v0, p0, LX/5Ou;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1sg;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/5Ou;->c:Ljava/lang/Boolean;

    .line 909606
    :cond_0
    iget-object v0, p0, LX/5Ou;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 909607
    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/5Ou;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 909564
    iget-object v0, p0, LX/5Ou;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 909565
    iget-object v0, p0, LX/5Ou;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1sg;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/5Ou;->d:Ljava/lang/Boolean;

    .line 909566
    :cond_0
    iget-object v0, p0, LX/5Ou;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 909561
    iget-object v0, p0, LX/5Ou;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 909562
    iget-object v0, p0, LX/5Ou;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1sg;->i:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/5Ou;->e:Ljava/lang/Boolean;

    .line 909563
    :cond_0
    iget-object v0, p0, LX/5Ou;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 909558
    iget-object v0, p0, LX/5Ou;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 909559
    iget-object v0, p0, LX/5Ou;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1sg;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/5Ou;->f:Ljava/lang/Boolean;

    .line 909560
    :cond_0
    iget-object v0, p0, LX/5Ou;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
