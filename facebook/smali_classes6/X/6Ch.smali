.class public final LX/6Ch;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Cl;

.field public final synthetic b:LX/6Ck;


# direct methods
.method public constructor <init>(LX/6Ck;LX/6Cl;)V
    .locals 0

    .prologue
    .line 1064268
    iput-object p1, p0, LX/6Ch;->b:LX/6Ck;

    iput-object p2, p0, LX/6Ch;->a:LX/6Cl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1064269
    iget-object v0, p0, LX/6Ch;->b:LX/6Ck;

    iget-object v0, v0, LX/6Ck;->a:LX/03V;

    const-string v1, "MessengerExtensionFavoriteManager"

    const-string v2, "Extension favorite query failed."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064270
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1064271
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1064272
    if-eqz p1, :cond_0

    .line 1064273
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1064274
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1064275
    iget-object v0, p0, LX/6Ch;->b:LX/6Ck;

    iget-object v0, v0, LX/6Ck;->a:LX/03V;

    const-string v1, "MessengerExtensionFavoriteManager"

    const-string v2, "Extension favorite query return wrong data."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064276
    :goto_1
    return-void

    .line 1064277
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1064278
    check-cast v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1064279
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1064280
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1064281
    check-cast v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x249d2f5f

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    iget-object v1, p0, LX/6Ch;->a:LX/6Cl;

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1064282
    :goto_2
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1064283
    iget-object p0, v1, LX/6Cl;->a:LX/6Cp;

    iget-object p0, p0, LX/6Cp;->f:Ljava/util/Set;

    const/4 p1, 0x4

    invoke-virtual {v4, v3, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1064284
    :cond_4
    goto :goto_1

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2
.end method
