.class public final LX/5Uq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 926854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel;
    .locals 17

    .prologue
    .line 926855
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 926856
    move-object/from16 v0, p0

    iget-object v2, v0, LX/5Uq;->a:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    invoke-virtual {v1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 926857
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5Uq;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 926858
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5Uq;->c:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 926859
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5Uq;->d:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 926860
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5Uq;->e:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 926861
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5Uq;->f:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 926862
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5Uq;->g:Ljava/lang/String;

    invoke-virtual {v1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 926863
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5Uq;->h:Ljava/lang/String;

    invoke-virtual {v1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 926864
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5Uq;->i:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 926865
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5Uq;->j:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 926866
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5Uq;->k:Ljava/lang/String;

    invoke-virtual {v1, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 926867
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5Uq;->l:Ljava/lang/String;

    invoke-virtual {v1, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 926868
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5Uq;->m:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 926869
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5Uq;->n:Ljava/lang/String;

    invoke-virtual {v1, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 926870
    const/16 v16, 0xe

    move/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 926871
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 926872
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 926873
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 926874
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 926875
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 926876
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 926877
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 926878
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 926879
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 926880
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 926881
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 926882
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 926883
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 926884
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 926885
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 926886
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 926887
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 926888
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 926889
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 926890
    new-instance v2, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel;

    invoke-direct {v2, v1}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel;-><init>(LX/15i;)V

    .line 926891
    return-object v2
.end method
