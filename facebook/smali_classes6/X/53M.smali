.class public final LX/53M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vR;


# instance fields
.field public final synthetic a:LX/53N;


# direct methods
.method public constructor <init>(LX/53N;)V
    .locals 0

    .prologue
    .line 827082
    iput-object p1, p0, LX/53M;->a:LX/53N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 827083
    iget-object v0, p0, LX/53M;->a:LX/53N;

    const-wide/16 v5, 0x0

    .line 827084
    const/4 v1, 0x0

    .line 827085
    :cond_0
    sget-object v2, LX/53N;->e:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    const-wide/16 v3, 0x1

    invoke-virtual {v2, v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->set(Ljava/lang/Object;J)V

    .line 827086
    :cond_1
    :goto_0
    iget-object v2, v0, LX/53N;->g:LX/53P;

    invoke-virtual {v2}, LX/53P;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 827087
    iget-boolean v2, v0, LX/53N;->j:Z

    if-eqz v2, :cond_3

    .line 827088
    iget-object v2, v0, LX/53N;->h:LX/53n;

    invoke-virtual {v2}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v2

    .line 827089
    instance-of v3, v2, LX/1pS;

    move v3, v3

    .line 827090
    if-eqz v3, :cond_1

    .line 827091
    iget-object v1, v0, LX/53N;->a:LX/0zZ;

    invoke-static {v1, v2}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    .line 827092
    :cond_2
    :goto_1
    return-void

    .line 827093
    :cond_3
    sget-object v2, LX/53N;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndDecrement(Ljava/lang/Object;)J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_6

    .line 827094
    iget-object v2, v0, LX/53N;->h:LX/53n;

    invoke-virtual {v2}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v2

    .line 827095
    if-nez v2, :cond_5

    .line 827096
    sget-object v2, LX/53N;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->incrementAndGet(Ljava/lang/Object;)J

    .line 827097
    :cond_4
    :goto_2
    sget-object v2, LX/53N;->e:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->decrementAndGet(Ljava/lang/Object;)J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-gtz v2, :cond_0

    .line 827098
    if-lez v1, :cond_2

    .line 827099
    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, LX/0zZ;->a(J)V

    goto :goto_1

    .line 827100
    :cond_5
    iget-object v3, v0, LX/53N;->a:LX/0zZ;

    invoke-static {v3, v2}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 827101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 827102
    :cond_6
    sget-object v2, LX/53N;->c:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->incrementAndGet(Ljava/lang/Object;)J

    goto :goto_2
.end method
