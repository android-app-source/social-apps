.class public final LX/5Id;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 895553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 895554
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 895555
    :goto_0
    return v1

    .line 895556
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 895557
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 895558
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 895559
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 895560
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 895561
    const-string v3, "legacy_api_post_id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 895562
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 895563
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 895564
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 895565
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 895546
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 895547
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 895548
    if-eqz v0, :cond_0

    .line 895549
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895550
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 895551
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 895552
    return-void
.end method
