.class public final LX/5hS;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 978872
    const-class v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;

    const v0, -0x12bb2eb3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "VideosUploadedByUserSimpleQuery"

    const-string v6, "0245417f9a67a73928b852eb8cf5d2fa"

    const-string v7, "node"

    const-string v8, "10155069969846729"

    const-string v9, "10155259090581729"

    .line 978873
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 978874
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 978875
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 978876
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 978877
    sparse-switch v0, :sswitch_data_0

    .line 978878
    :goto_0
    return-object p1

    .line 978879
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 978880
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 978881
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 978882
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 978883
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_2
        0x5a7510f -> :sswitch_0
        0x1918b88b -> :sswitch_1
        0x73a026b5 -> :sswitch_3
        0x7e07ec78 -> :sswitch_4
    .end sparse-switch
.end method
