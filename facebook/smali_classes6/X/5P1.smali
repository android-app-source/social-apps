.class public final enum LX/5P1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5P1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5P1;

.field public static final enum KEEP_FEMALE:LX/5P1;

.field public static final enum KEEP_HAS_MUTUAL_FRIEND:LX/5P1;

.field public static final enum NONE:LX/5P1;


# instance fields
.field private final filterName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 909840
    new-instance v0, LX/5P1;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/5P1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P1;->NONE:LX/5P1;

    .line 909841
    new-instance v0, LX/5P1;

    const-string v1, "KEEP_HAS_MUTUAL_FRIEND"

    const-string v2, "keep_has_mutual_friend"

    invoke-direct {v0, v1, v4, v2}, LX/5P1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P1;->KEEP_HAS_MUTUAL_FRIEND:LX/5P1;

    .line 909842
    new-instance v0, LX/5P1;

    const-string v1, "KEEP_FEMALE"

    const-string v2, "keep_female"

    invoke-direct {v0, v1, v5, v2}, LX/5P1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P1;->KEEP_FEMALE:LX/5P1;

    .line 909843
    const/4 v0, 0x3

    new-array v0, v0, [LX/5P1;

    sget-object v1, LX/5P1;->NONE:LX/5P1;

    aput-object v1, v0, v3

    sget-object v1, LX/5P1;->KEEP_HAS_MUTUAL_FRIEND:LX/5P1;

    aput-object v1, v0, v4

    sget-object v1, LX/5P1;->KEEP_FEMALE:LX/5P1;

    aput-object v1, v0, v5

    sput-object v0, LX/5P1;->$VALUES:[LX/5P1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 909834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 909835
    iput-object p3, p0, LX/5P1;->filterName:Ljava/lang/String;

    .line 909836
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5P1;
    .locals 1

    .prologue
    .line 909837
    const-class v0, LX/5P1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5P1;

    return-object v0
.end method

.method public static values()[LX/5P1;
    .locals 1

    .prologue
    .line 909838
    sget-object v0, LX/5P1;->$VALUES:[LX/5P1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5P1;

    return-object v0
.end method


# virtual methods
.method public final getFilterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909839
    iget-object v0, p0, LX/5P1;->filterName:Ljava/lang/String;

    return-object v0
.end method
