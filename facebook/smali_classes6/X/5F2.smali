.class public final LX/5F2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    .line 884498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884499
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 884500
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 884501
    const/4 v2, 0x0

    .line 884502
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 884503
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884504
    :goto_1
    move v1, v2

    .line 884505
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 884506
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 884507
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884508
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 884509
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 884510
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884511
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 884512
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 884513
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 884514
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 884515
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 884516
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 884517
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 884518
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 884519
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 884520
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884521
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 884522
    if-eqz v2, :cond_0

    .line 884523
    const-string v3, "image"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884524
    invoke-static {p0, v2, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 884525
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884527
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 884528
    return-void
.end method
