.class public final LX/571;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 839576
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 839577
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 839578
    :goto_0
    return v1

    .line 839579
    :cond_0
    const-string v11, "does_viewer_like"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 839580
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    .line 839581
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 839582
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 839583
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 839584
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 839585
    const-string v11, "category_names"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 839586
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 839587
    :cond_2
    const-string v11, "cover_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 839588
    invoke-static {p0, p1}, LX/56b;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 839589
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 839590
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 839591
    :cond_4
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 839592
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 839593
    :cond_5
    const-string v11, "page_likers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 839594
    invoke-static {p0, p1}, LX/570;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 839595
    :cond_6
    const-string v11, "profile_picture"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 839596
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 839597
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 839598
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 839599
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 839600
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 839601
    if-eqz v0, :cond_9

    .line 839602
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 839603
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 839604
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 839605
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 839606
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 839607
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 839608
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 839609
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 839610
    if-eqz v0, :cond_0

    .line 839611
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839612
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 839613
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839614
    if-eqz v0, :cond_1

    .line 839615
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839616
    invoke-static {p0, v0, p2, p3}, LX/56b;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 839617
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 839618
    if-eqz v0, :cond_2

    .line 839619
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839620
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 839621
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839622
    if-eqz v0, :cond_3

    .line 839623
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839624
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839625
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839626
    if-eqz v0, :cond_4

    .line 839627
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839628
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839629
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839630
    if-eqz v0, :cond_5

    .line 839631
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839632
    invoke-static {p0, v0, p2}, LX/570;->a(LX/15i;ILX/0nX;)V

    .line 839633
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839634
    if-eqz v0, :cond_6

    .line 839635
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839636
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 839637
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 839638
    return-void
.end method
