.class public final LX/5Tv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 923266
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 923267
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923268
    :goto_0
    return v1

    .line 923269
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 923270
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 923271
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 923272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 923273
    const-string v9, "hide_attribution"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 923274
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_1

    .line 923275
    :cond_1
    const-string v9, "hide_install_button"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 923276
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 923277
    :cond_2
    const-string v9, "hide_reply_button"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 923278
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 923279
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 923280
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 923281
    if-eqz v4, :cond_5

    .line 923282
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 923283
    :cond_5
    if-eqz v3, :cond_6

    .line 923284
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 923285
    :cond_6
    if-eqz v0, :cond_7

    .line 923286
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 923287
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method
