.class public LX/6K6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Jx;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/os/Handler;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/6Jt;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/6KK;

.field public e:Landroid/os/Handler;

.field public f:Landroid/os/Handler;

.field public g:Landroid/os/HandlerThread;

.field public h:Landroid/os/HandlerThread;

.field public i:LX/6L3;

.field public j:Z

.field public k:Z

.field public l:Landroid/view/Surface;

.field public m:LX/6Jc;

.field public n:J

.field public o:LX/6JR;

.field public p:LX/6JG;

.field public q:LX/6K9;

.field private r:Z

.field private s:Ljava/lang/Runnable;

.field public final t:LX/6K0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1076369
    const-class v0, LX/6K6;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6K6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Jt;Landroid/os/Handler;LX/6KK;)V
    .locals 1

    .prologue
    .line 1076360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076361
    new-instance v0, LX/6K0;

    invoke-direct {v0, p0}, LX/6K0;-><init>(LX/6K6;)V

    iput-object v0, p0, LX/6K6;->t:LX/6K0;

    .line 1076362
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    .line 1076363
    iput-object p2, p0, LX/6K6;->b:Landroid/os/Handler;

    .line 1076364
    sget-object v0, LX/6K9;->STOPPED:LX/6K9;

    iput-object v0, p0, LX/6K6;->q:LX/6K9;

    .line 1076365
    iput-object p3, p0, LX/6K6;->d:LX/6KK;

    .line 1076366
    const/4 v0, 0x0

    iput-object v0, p0, LX/6K6;->s:Ljava/lang/Runnable;

    .line 1076367
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6K6;->r:Z

    .line 1076368
    return-void
.end method

.method public static a(LX/6K6;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1076349
    invoke-static {}, LX/6K6;->h()V

    .line 1076350
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6K6;->r:Z

    .line 1076351
    if-eqz p1, :cond_1

    .line 1076352
    iget-object v0, p0, LX/6K6;->s:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1076353
    iget-object v0, p0, LX/6K6;->s:Ljava/lang/Runnable;

    .line 1076354
    iput-object v1, p0, LX/6K6;->s:Ljava/lang/Runnable;

    .line 1076355
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/6K6;->r:Z

    .line 1076356
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1076357
    :cond_0
    :goto_0
    return-void

    .line 1076358
    :cond_1
    iput-object v1, p0, LX/6K6;->s:Ljava/lang/Runnable;

    .line 1076359
    invoke-static {p0}, LX/6K6;->c(LX/6K6;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1076343
    invoke-static {}, LX/6K6;->h()V

    .line 1076344
    iget-boolean v0, p0, LX/6K6;->r:Z

    if-eqz v0, :cond_0

    .line 1076345
    iput-object p1, p0, LX/6K6;->s:Ljava/lang/Runnable;

    .line 1076346
    :goto_0
    return-void

    .line 1076347
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6K6;->r:Z

    .line 1076348
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public static a$redex0(LX/6K6;LX/6JU;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1076339
    sget-object v0, LX/6K9;->PREPARED:LX/6K9;

    iput-object v0, p0, LX/6K6;->q:LX/6K9;

    .line 1076340
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1076341
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076342
    return-void
.end method

.method public static a$redex0(LX/6K6;LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1076335
    sget-object v0, LX/6K9;->STOPPED:LX/6K9;

    iput-object v0, p0, LX/6K6;->q:LX/6K9;

    .line 1076336
    invoke-static {p1, p2, p3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1076337
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076338
    return-void
.end method

.method public static b(LX/6K6;Ljava/io/File;LX/6JG;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1076311
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076312
    iget-object v0, p0, LX/6K6;->q:LX/6K9;

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    if-ne v0, v1, :cond_0

    .line 1076313
    invoke-static {p0, v2}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076314
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recording video has already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076315
    :cond_0
    iget-object v0, p0, LX/6K6;->q:LX/6K9;

    sget-object v1, LX/6K9;->PREPARED:LX/6K9;

    if-eq v0, v1, :cond_1

    .line 1076316
    invoke-static {p0, v2}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076317
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "prepare() must be called before start"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076318
    :cond_1
    sget-object v0, LX/6K9;->RECORDING_STARTED:LX/6K9;

    iput-object v0, p0, LX/6K6;->q:LX/6K9;

    .line 1076319
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->b(I)V

    .line 1076320
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    invoke-virtual {v0}, LX/6Jt;->f()V

    .line 1076321
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6K6;->n:J

    .line 1076322
    iput-object p2, p0, LX/6K6;->p:LX/6JG;

    .line 1076323
    iget-object v0, p0, LX/6K6;->m:LX/6Jc;

    new-instance v1, LX/6K4;

    invoke-direct {v1, p0}, LX/6K4;-><init>(LX/6K6;)V

    new-instance v2, LX/6K5;

    invoke-direct {v2, p0}, LX/6K5;-><init>(LX/6K6;)V

    iget-object v3, p0, LX/6K6;->b:Landroid/os/Handler;

    .line 1076324
    iget-object v4, v0, LX/6Jc;->g:LX/6L1;

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/6Jc;->h:LX/6LC;

    if-nez v4, :cond_3

    .line 1076325
    :cond_2
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string p0, "Cannot call start() before prepare"

    invoke-direct {v4, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1076326
    :goto_0
    return-void

    .line 1076327
    :cond_3
    iget-boolean v4, v0, LX/6Jc;->l:Z

    if-eqz v4, :cond_4

    .line 1076328
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string p0, "Cannot call start() again after encoding has started"

    invoke-direct {v4, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1076329
    :cond_4
    iput-object p1, v0, LX/6Jc;->i:Ljava/io/File;

    .line 1076330
    iput-object v2, v0, LX/6Jc;->j:LX/6JU;

    .line 1076331
    iput-object v3, v0, LX/6Jc;->k:Landroid/os/Handler;

    .line 1076332
    iget-object v4, v0, LX/6Jc;->g:LX/6L1;

    new-instance p0, LX/6JX;

    invoke-direct {p0, v0, v1, v3}, LX/6JX;-><init>(LX/6Jc;LX/6JU;Landroid/os/Handler;)V

    iget-object p2, v0, LX/6Jc;->e:Landroid/os/Handler;

    .line 1076333
    iget-object v0, v4, LX/6L1;->b:Landroid/os/Handler;

    new-instance p1, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;

    invoke-direct {p1, v4, p0, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;-><init>(LX/6L1;LX/6JU;Landroid/os/Handler;)V

    const v1, 0x63a336a1

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076334
    goto :goto_0
.end method

.method public static c(LX/6K6;)V
    .locals 3

    .prologue
    .line 1076255
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076256
    iget-object v0, p0, LX/6K6;->q:LX/6K9;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    if-ne v0, v1, :cond_0

    .line 1076257
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076258
    :goto_0
    return-void

    .line 1076259
    :cond_0
    sget-object v0, LX/6K9;->STOP_STARTED:LX/6K9;

    iput-object v0, p0, LX/6K6;->q:LX/6K9;

    .line 1076260
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->b(I)V

    .line 1076261
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    iget-object v1, p0, LX/6K6;->l:Landroid/view/Surface;

    .line 1076262
    iget-object v2, v0, LX/6Jt;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Ku;

    .line 1076263
    if-eqz v2, :cond_3

    .line 1076264
    iget-object v1, v2, LX/6Ku;->c:Landroid/view/Surface;

    if-eqz v1, :cond_2

    .line 1076265
    iget-object v1, v2, LX/6Ku;->d:LX/6Kl;

    if-eqz v1, :cond_1

    .line 1076266
    iget-object v1, v2, LX/6Ku;->d:LX/6Kl;

    invoke-virtual {v1, v2}, LX/6Kl;->b(LX/6Kd;)V

    .line 1076267
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v2, LX/6Ku;->c:Landroid/view/Surface;

    .line 1076268
    :cond_2
    invoke-virtual {v0, v2}, LX/6Jt;->b(LX/6Kd;)V

    .line 1076269
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/6K6;->l:Landroid/view/Surface;

    .line 1076270
    iget-object v0, p0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, LX/6Jt;->b(I)V

    .line 1076271
    iget-object v0, p0, LX/6K6;->i:LX/6L3;

    new-instance v1, LX/6Jz;

    invoke-direct {v1, p0}, LX/6Jz;-><init>(LX/6K6;)V

    iget-object v2, p0, LX/6K6;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, LX/6L3;->c(LX/6JU;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public static d(LX/6K6;)Z
    .locals 1

    .prologue
    .line 1076310
    iget-boolean v0, p0, LX/6K6;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6K6;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/6K6;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1076289
    iget-object v0, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 1076290
    iget-object v0, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1076291
    :try_start_0
    iget-object v0, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076292
    iput-object v1, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    .line 1076293
    iput-object v1, p0, LX/6K6;->e:Landroid/os/Handler;

    .line 1076294
    :cond_0
    :goto_0
    iget-object v0, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 1076295
    iget-object v0, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1076296
    :try_start_1
    iget-object v0, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1076297
    iput-object v1, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    .line 1076298
    iput-object v1, p0, LX/6K6;->f:Landroid/os/Handler;

    .line 1076299
    :cond_1
    :goto_1
    return-void

    .line 1076300
    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1076301
    iput-object v1, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    .line 1076302
    iput-object v1, p0, LX/6K6;->e:Landroid/os/Handler;

    goto :goto_0

    .line 1076303
    :catchall_0
    move-exception v0

    iput-object v1, p0, LX/6K6;->g:Landroid/os/HandlerThread;

    .line 1076304
    iput-object v1, p0, LX/6K6;->e:Landroid/os/Handler;

    throw v0

    .line 1076305
    :catch_1
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1076306
    iput-object v1, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    .line 1076307
    iput-object v1, p0, LX/6K6;->f:Landroid/os/Handler;

    goto :goto_1

    .line 1076308
    :catchall_1
    move-exception v0

    iput-object v1, p0, LX/6K6;->h:Landroid/os/HandlerThread;

    .line 1076309
    iput-object v1, p0, LX/6K6;->f:Landroid/os/Handler;

    throw v0
.end method

.method private static h()V
    .locals 2

    .prologue
    .line 1076286
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1076287
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecorderCoordinatorImpl methods must be called on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076288
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/6K9;
    .locals 1

    .prologue
    .line 1076285
    iget-object v0, p0, LX/6K6;->q:LX/6K9;

    return-object v0
.end method

.method public final a(LX/6Ia;)V
    .locals 0

    .prologue
    .line 1076284
    return-void
.end method

.method public final a(LX/6JR;LX/6JU;I)V
    .locals 2

    .prologue
    .line 1076279
    iget-object v0, p0, LX/6K6;->d:LX/6KK;

    .line 1076280
    iget-boolean v1, v0, LX/6KK;->b:Z

    move v0, v1

    .line 1076281
    if-nez v0, :cond_0

    .line 1076282
    :goto_0
    return-void

    .line 1076283
    :cond_0
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;-><init>(LX/6K6;LX/6JR;LX/6JU;I)V

    invoke-direct {p0, v0}, LX/6K6;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 2

    .prologue
    .line 1076274
    iget-object v0, p0, LX/6K6;->d:LX/6KK;

    .line 1076275
    iget-boolean v1, v0, LX/6KK;->b:Z

    move v0, v1

    .line 1076276
    if-nez v0, :cond_0

    .line 1076277
    :goto_0
    return-void

    .line 1076278
    :cond_0
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$4;-><init>(LX/6K6;Ljava/io/File;LX/6JG;)V

    invoke-direct {p0, v0}, LX/6K6;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1076272
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$5;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$5;-><init>(LX/6K6;)V

    invoke-direct {p0, v0}, LX/6K6;->a(Ljava/lang/Runnable;)V

    .line 1076273
    return-void
.end method
