.class public final LX/67P;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/67N;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/67N;)V
    .locals 1

    .prologue
    .line 1052912
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1052913
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/67P;->a:Ljava/lang/ref/WeakReference;

    .line 1052914
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 1052915
    iget-object v0, p0, LX/67P;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67N;

    .line 1052916
    if-eqz v0, :cond_1

    .line 1052917
    iget v1, p1, Landroid/os/Message;->what:I

    .line 1052918
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 1052919
    iget v3, p1, Landroid/os/Message;->arg2:I

    .line 1052920
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1052921
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v5

    .line 1052922
    const/4 p0, 0x1

    .line 1052923
    packed-switch v1, :pswitch_data_0

    .line 1052924
    :cond_0
    const/4 p0, 0x0

    :goto_0
    :pswitch_0
    move v0, p0

    .line 1052925
    if-nez v0, :cond_1

    .line 1052926
    sget-boolean v0, LX/3Fm;->a:Z

    if-eqz v0, :cond_1

    .line 1052927
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unhandled message from server: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1052928
    :cond_1
    return-void

    .line 1052929
    :pswitch_1
    iget v1, v0, LX/67N;->h:I

    if-ne v2, v1, :cond_3

    .line 1052930
    const/4 v1, 0x0

    iput v1, v0, LX/67N;->h:I

    .line 1052931
    iget-object v1, v0, LX/67N;->a:LX/3Fm;

    const-string v3, "Registation failed"

    .line 1052932
    iget-object v4, v1, LX/3Fm;->g:LX/67N;

    if-ne v4, v0, :cond_3

    .line 1052933
    sget-boolean v4, LX/3Fm;->a:Z

    if-eqz v4, :cond_2

    .line 1052934
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Service connection error - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1052935
    :cond_2
    invoke-static {v1}, LX/3Fm;->m(LX/3Fm;)V

    .line 1052936
    :cond_3
    iget-object v1, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/67L;

    .line 1052937
    if-eqz v1, :cond_4

    .line 1052938
    iget-object v1, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 1052939
    :cond_4
    goto :goto_0

    .line 1052940
    :pswitch_2
    if-eqz v4, :cond_5

    instance-of p0, v4, Landroid/os/Bundle;

    if-eqz p0, :cond_0

    .line 1052941
    :cond_5
    check-cast v4, Landroid/os/Bundle;

    const/4 p0, 0x0

    .line 1052942
    iget v1, v0, LX/67N;->g:I

    if-nez v1, :cond_8

    iget v1, v0, LX/67N;->h:I

    if-ne v2, v1, :cond_8

    if-lez v3, :cond_8

    .line 1052943
    iput p0, v0, LX/67N;->h:I

    .line 1052944
    iput v3, v0, LX/67N;->g:I

    .line 1052945
    iget-object p0, v0, LX/67N;->a:LX/3Fm;

    invoke-static {v4}, LX/382;->a(Landroid/os/Bundle;)LX/382;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/3Fm;->a$redex0(LX/3Fm;LX/67N;LX/382;)V

    .line 1052946
    iget-object p0, v0, LX/67N;->a:LX/3Fm;

    .line 1052947
    iget-object v1, p0, LX/3Fm;->g:LX/67N;

    if-ne v1, v0, :cond_7

    .line 1052948
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3Fm;->h:Z

    .line 1052949
    iget-object v1, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1052950
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_6

    .line 1052951
    iget-object v1, p0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/67O;

    iget-object v0, p0, LX/3Fm;->g:LX/67N;

    invoke-virtual {v1, v0}, LX/67O;->a(LX/67N;)V

    .line 1052952
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1052953
    :cond_6
    iget-object v1, p0, LX/37v;->e:LX/38A;

    move-object v1, v1

    .line 1052954
    if-eqz v1, :cond_7

    .line 1052955
    iget-object v2, p0, LX/3Fm;->g:LX/67N;

    invoke-virtual {v2, v1}, LX/67N;->a(LX/38A;)V

    .line 1052956
    :cond_7
    const/4 p0, 0x1

    .line 1052957
    :cond_8
    move p0, p0

    .line 1052958
    goto/16 :goto_0

    .line 1052959
    :pswitch_3
    if-eqz v4, :cond_9

    instance-of p0, v4, Landroid/os/Bundle;

    if-eqz p0, :cond_0

    .line 1052960
    :cond_9
    check-cast v4, Landroid/os/Bundle;

    .line 1052961
    iget p0, v0, LX/67N;->g:I

    if-eqz p0, :cond_d

    .line 1052962
    iget-object p0, v0, LX/67N;->a:LX/3Fm;

    invoke-static {v4}, LX/382;->a(Landroid/os/Bundle;)LX/382;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/3Fm;->a$redex0(LX/3Fm;LX/67N;LX/382;)V

    .line 1052963
    const/4 p0, 0x1

    .line 1052964
    :goto_2
    move p0, p0

    .line 1052965
    goto/16 :goto_0

    .line 1052966
    :pswitch_4
    if-eqz v4, :cond_a

    instance-of p0, v4, Landroid/os/Bundle;

    if-eqz p0, :cond_0

    .line 1052967
    :cond_a
    check-cast v4, Landroid/os/Bundle;

    .line 1052968
    iget-object p0, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/67L;

    .line 1052969
    if-eqz p0, :cond_e

    .line 1052970
    iget-object p0, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 1052971
    const/4 p0, 0x1

    .line 1052972
    :goto_3
    move p0, p0

    .line 1052973
    goto/16 :goto_0

    .line 1052974
    :pswitch_5
    if-eqz v4, :cond_b

    instance-of p0, v4, Landroid/os/Bundle;

    if-eqz p0, :cond_0

    .line 1052975
    :cond_b
    if-nez v5, :cond_c

    .line 1052976
    :goto_4
    check-cast v4, Landroid/os/Bundle;

    .line 1052977
    iget-object v1, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/67L;

    .line 1052978
    if-eqz v1, :cond_f

    .line 1052979
    iget-object v1, v0, LX/67N;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 1052980
    const/4 v1, 0x1

    .line 1052981
    :goto_5
    move p0, v1

    .line 1052982
    goto/16 :goto_0

    .line 1052983
    :cond_c
    const-string p0, "error"

    invoke-virtual {v5, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_4

    :cond_d
    const/4 p0, 0x0

    goto :goto_2

    :cond_e
    const/4 p0, 0x0

    goto :goto_3

    :cond_f
    const/4 v1, 0x0

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method
