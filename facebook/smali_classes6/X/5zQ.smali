.class public final LX/5zQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/ui/common/ProfilePrivacyView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1034530
    iput-object p1, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iput-object p2, p0, LX/5zQ;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x38235968

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1034531
    iget-object v0, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget-object v0, v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    if-nez v0, :cond_0

    .line 1034532
    iget-object v0, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    const v2, 0x7f0d276b

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1034533
    if-eqz v0, :cond_1

    .line 1034534
    :goto_0
    iget-object v2, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    new-instance v3, LX/0hs;

    iget-object v4, p0, LX/5zQ;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 1034535
    iput-object v3, v2, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    .line 1034536
    iget-object v2, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget-object v2, v2, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    const/4 v3, -0x1

    .line 1034537
    iput v3, v2, LX/0hs;->t:I

    .line 1034538
    iget-object v2, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget-object v2, v2, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    invoke-virtual {v2, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 1034539
    :cond_0
    iget-object v0, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget-object v0, v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    iget-object v2, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget v2, v2, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->c:I

    invoke-virtual {v0, v2}, LX/0hs;->b(I)V

    .line 1034540
    iget-object v0, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    iget-object v0, v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->b:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1034541
    const v0, -0x4fb9f71a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1034542
    :cond_1
    iget-object v0, p0, LX/5zQ;->b:Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    goto :goto_0
.end method
