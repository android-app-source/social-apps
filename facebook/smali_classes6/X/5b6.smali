.class public final LX/5b6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 956795
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 956796
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 956797
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 956798
    invoke-static {p0, p1}, LX/5b6;->b(LX/15w;LX/186;)I

    move-result v1

    .line 956799
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 956800
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 37

    .prologue
    .line 956908
    const/16 v33, 0x0

    .line 956909
    const/16 v32, 0x0

    .line 956910
    const/16 v31, 0x0

    .line 956911
    const/16 v30, 0x0

    .line 956912
    const/16 v29, 0x0

    .line 956913
    const/16 v28, 0x0

    .line 956914
    const/16 v27, 0x0

    .line 956915
    const/16 v26, 0x0

    .line 956916
    const/16 v25, 0x0

    .line 956917
    const/16 v24, 0x0

    .line 956918
    const/16 v23, 0x0

    .line 956919
    const/16 v22, 0x0

    .line 956920
    const/16 v21, 0x0

    .line 956921
    const/16 v20, 0x0

    .line 956922
    const/16 v19, 0x0

    .line 956923
    const/16 v18, 0x0

    .line 956924
    const/16 v17, 0x0

    .line 956925
    const/16 v16, 0x0

    .line 956926
    const/4 v15, 0x0

    .line 956927
    const/4 v14, 0x0

    .line 956928
    const/4 v13, 0x0

    .line 956929
    const/4 v12, 0x0

    .line 956930
    const/4 v11, 0x0

    .line 956931
    const/4 v10, 0x0

    .line 956932
    const/4 v9, 0x0

    .line 956933
    const/4 v8, 0x0

    .line 956934
    const/4 v7, 0x0

    .line 956935
    const/4 v6, 0x0

    .line 956936
    const/4 v5, 0x0

    .line 956937
    const/4 v4, 0x0

    .line 956938
    const/4 v3, 0x0

    .line 956939
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1

    .line 956940
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 956941
    const/4 v3, 0x0

    .line 956942
    :goto_0
    return v3

    .line 956943
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 956944
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1c

    .line 956945
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v34

    .line 956946
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 956947
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    if-eqz v34, :cond_1

    .line 956948
    const-string v35, "__type__"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_2

    const-string v35, "__typename"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3

    .line 956949
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v33

    goto :goto_1

    .line 956950
    :cond_3
    const-string v35, "animated_image_full_screen"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 956951
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v32

    goto :goto_1

    .line 956952
    :cond_4
    const-string v35, "animated_image_large_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 956953
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v31

    goto :goto_1

    .line 956954
    :cond_5
    const-string v35, "animated_image_medium_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_6

    .line 956955
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 956956
    :cond_6
    const-string v35, "animated_image_original_dimensions"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 956957
    invoke-static/range {p0 .. p1}, LX/5ac;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 956958
    :cond_7
    const-string v35, "animated_image_render_as_sticker"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 956959
    const/4 v7, 0x1

    .line 956960
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 956961
    :cond_8
    const-string v35, "animated_image_small_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_9

    .line 956962
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 956963
    :cond_9
    const-string v35, "animated_static_image_full_screen"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 956964
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 956965
    :cond_a
    const-string v35, "animated_static_image_large_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 956966
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 956967
    :cond_b
    const-string v35, "animated_static_image_medium_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 956968
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 956969
    :cond_c
    const-string v35, "animated_static_image_small_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_d

    .line 956970
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 956971
    :cond_d
    const-string v35, "attachment_video_url"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 956972
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 956973
    :cond_e
    const-string v35, "filename"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_f

    .line 956974
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 956975
    :cond_f
    const-string v35, "image_full_screen"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_10

    .line 956976
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 956977
    :cond_10
    const-string v35, "image_large_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_11

    .line 956978
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 956979
    :cond_11
    const-string v35, "image_medium_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_12

    .line 956980
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 956981
    :cond_12
    const-string v35, "image_small_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_13

    .line 956982
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 956983
    :cond_13
    const-string v35, "image_type"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_14

    .line 956984
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLMessageImageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageImageType;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto/16 :goto_1

    .line 956985
    :cond_14
    const-string v35, "mini_preview"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_15

    .line 956986
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 956987
    :cond_15
    const-string v35, "original_dimensions"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_16

    .line 956988
    invoke-static/range {p0 .. p1}, LX/5b5;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 956989
    :cond_16
    const-string v35, "playable_duration_in_ms"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_17

    .line 956990
    const/4 v6, 0x1

    .line 956991
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 956992
    :cond_17
    const-string v35, "render_as_sticker"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_18

    .line 956993
    const/4 v5, 0x1

    .line 956994
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 956995
    :cond_18
    const-string v35, "rotation"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_19

    .line 956996
    const/4 v4, 0x1

    .line 956997
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 956998
    :cond_19
    const-string v35, "streamingImageThumbnail"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_1a

    .line 956999
    invoke-static/range {p0 .. p1}, LX/5ao;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 957000
    :cond_1a
    const-string v35, "video_filesize"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_1b

    .line 957001
    const/4 v3, 0x1

    .line 957002
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 957003
    :cond_1b
    const-string v35, "video_type"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_0

    .line 957004
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto/16 :goto_1

    .line 957005
    :cond_1c
    const/16 v34, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 957006
    const/16 v34, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957007
    const/16 v33, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957008
    const/16 v32, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957009
    const/16 v31, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957010
    const/16 v30, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957011
    if-eqz v7, :cond_1d

    .line 957012
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 957013
    :cond_1d
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957014
    const/4 v7, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957015
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957016
    const/16 v7, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957017
    const/16 v7, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957018
    const/16 v7, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957019
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957020
    const/16 v7, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957021
    const/16 v7, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957022
    const/16 v7, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957023
    const/16 v7, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957024
    const/16 v7, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 957025
    const/16 v7, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->b(II)V

    .line 957026
    const/16 v7, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->b(II)V

    .line 957027
    if-eqz v6, :cond_1e

    .line 957028
    const/16 v6, 0x14

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 957029
    :cond_1e
    if-eqz v5, :cond_1f

    .line 957030
    const/16 v5, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 957031
    :cond_1f
    if-eqz v4, :cond_20

    .line 957032
    const/16 v4, 0x16

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v5}, LX/186;->a(III)V

    .line 957033
    :cond_20
    const/16 v4, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 957034
    if-eqz v3, :cond_21

    .line 957035
    const/16 v3, 0x18

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 957036
    :cond_21
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 957037
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x19

    const/16 v3, 0x11

    const/4 v2, 0x0

    .line 956801
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956802
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 956803
    if-eqz v0, :cond_0

    .line 956804
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956805
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 956806
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956807
    if-eqz v0, :cond_1

    .line 956808
    const-string v1, "animated_image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956809
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956810
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956811
    if-eqz v0, :cond_2

    .line 956812
    const-string v1, "animated_image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956813
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956814
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956815
    if-eqz v0, :cond_3

    .line 956816
    const-string v1, "animated_image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956817
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956818
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956819
    if-eqz v0, :cond_4

    .line 956820
    const-string v1, "animated_image_original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956821
    invoke-static {p0, v0, p2}, LX/5ac;->a(LX/15i;ILX/0nX;)V

    .line 956822
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 956823
    if-eqz v0, :cond_5

    .line 956824
    const-string v1, "animated_image_render_as_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956825
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 956826
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956827
    if-eqz v0, :cond_6

    .line 956828
    const-string v1, "animated_image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956829
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956830
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956831
    if-eqz v0, :cond_7

    .line 956832
    const-string v1, "animated_static_image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956833
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956834
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956835
    if-eqz v0, :cond_8

    .line 956836
    const-string v1, "animated_static_image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956837
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956838
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956839
    if-eqz v0, :cond_9

    .line 956840
    const-string v1, "animated_static_image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956841
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956842
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956843
    if-eqz v0, :cond_a

    .line 956844
    const-string v1, "animated_static_image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956845
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956846
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956847
    if-eqz v0, :cond_b

    .line 956848
    const-string v1, "attachment_video_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956849
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956850
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956851
    if-eqz v0, :cond_c

    .line 956852
    const-string v1, "filename"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956853
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956854
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956855
    if-eqz v0, :cond_d

    .line 956856
    const-string v1, "image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956857
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956858
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956859
    if-eqz v0, :cond_e

    .line 956860
    const-string v1, "image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956861
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956862
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956863
    if-eqz v0, :cond_f

    .line 956864
    const-string v1, "image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956865
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956866
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956867
    if-eqz v0, :cond_10

    .line 956868
    const-string v1, "image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956869
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 956870
    :cond_10
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 956871
    if-eqz v0, :cond_11

    .line 956872
    const-string v0, "image_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956873
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956874
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 956875
    if-eqz v0, :cond_12

    .line 956876
    const-string v1, "mini_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956877
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956878
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956879
    if-eqz v0, :cond_13

    .line 956880
    const-string v1, "original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956881
    invoke-static {p0, v0, p2}, LX/5b5;->a(LX/15i;ILX/0nX;)V

    .line 956882
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 956883
    if-eqz v0, :cond_14

    .line 956884
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956885
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956886
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 956887
    if-eqz v0, :cond_15

    .line 956888
    const-string v1, "render_as_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956889
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 956890
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 956891
    if-eqz v0, :cond_16

    .line 956892
    const-string v1, "rotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956893
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956894
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956895
    if-eqz v0, :cond_17

    .line 956896
    const-string v1, "streamingImageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956897
    invoke-static {p0, v0, p2}, LX/5ao;->a(LX/15i;ILX/0nX;)V

    .line 956898
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 956899
    if-eqz v0, :cond_18

    .line 956900
    const-string v1, "video_filesize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956901
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956902
    :cond_18
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 956903
    if-eqz v0, :cond_19

    .line 956904
    const-string v0, "video_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956905
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 956906
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956907
    return-void
.end method
