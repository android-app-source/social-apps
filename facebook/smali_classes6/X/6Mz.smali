.class public abstract LX/6Mz;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Lcom/facebook/contacts/model/PhonebookContact;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# instance fields
.field public c:Landroid/database/Cursor;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1082233
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "data15"

    aput-object v2, v0, v1

    sput-object v0, LX/6Mz;->a:[Ljava/lang/String;

    .line 1082234
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "account_type"

    aput-object v1, v0, v3

    sput-object v0, LX/6Mz;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2J0;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 1082230
    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 1082231
    invoke-static {p2}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;

    move-result-object v0

    iput-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    .line 1082232
    return-void
.end method

.method public static h(LX/6Mz;)I
    .locals 4

    .prologue
    .line 1082219
    const/4 v0, 0x0

    .line 1082220
    :goto_0
    const/4 v1, 0x0

    .line 1082221
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1082222
    :cond_0
    :goto_1
    move v1, v1

    .line 1082223
    if-eqz v1, :cond_1

    .line 1082224
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1082225
    :cond_1
    return v0

    .line 1082226
    :cond_2
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "deleted"

    invoke-static {v2, v3}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 1082227
    if-eqz v2, :cond_3

    .line 1082228
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 1082229
    :cond_3
    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1082175
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082176
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 1082177
    :cond_0
    invoke-static {p0}, LX/6Mz;->h(LX/6Mz;)I

    .line 1082178
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082179
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContact;

    .line 1082180
    :goto_0
    return-object v0

    .line 1082181
    :cond_1
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "contact_id"

    invoke-static {v1, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1082182
    new-instance v2, LX/6NG;

    invoke-direct {v2, v1}, LX/6NG;-><init>(Ljava/lang/String;)V

    .line 1082183
    invoke-virtual {p0, v2}, LX/6Mz;->m(LX/6NG;)V

    .line 1082184
    :cond_2
    invoke-static {p0}, LX/6Mz;->h(LX/6Mz;)I

    .line 1082185
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1082186
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "contact_id"

    invoke-static {v3, v4}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1082187
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1082188
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "mimetype"

    invoke-static {v3, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082189
    const-string v4, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1082190
    invoke-virtual {p0, v2}, LX/6Mz;->a(LX/6NG;)V

    .line 1082191
    :cond_3
    :goto_1
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1082192
    :cond_4
    invoke-virtual {v2}, LX/6NG;->c()Lcom/facebook/contacts/model/PhonebookContact;

    move-result-object v1

    :goto_2
    move-object v0, v1

    .line 1082193
    goto :goto_0

    .line 1082194
    :cond_5
    invoke-virtual {v2}, LX/6NG;->c()Lcom/facebook/contacts/model/PhonebookContact;

    move-result-object v1

    goto :goto_2

    .line 1082195
    :cond_6
    const-string v4, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1082196
    invoke-virtual {p0, v2}, LX/6Mz;->b(LX/6NG;)V

    goto :goto_1

    .line 1082197
    :cond_7
    const-string v4, "vnd.android.cursor.item/name"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1082198
    invoke-virtual {p0, v2}, LX/6Mz;->c(LX/6NG;)V

    goto :goto_1

    .line 1082199
    :cond_8
    const-string v4, "vnd.android.cursor.item/photo"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1082200
    invoke-virtual {p0, v2}, LX/6Mz;->d(LX/6NG;)V

    goto :goto_1

    .line 1082201
    :cond_9
    const-string v4, "vnd.android.cursor.item/note"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1082202
    invoke-virtual {p0, v2}, LX/6Mz;->e(LX/6NG;)V

    goto :goto_1

    .line 1082203
    :cond_a
    const-string v4, "vnd.android.cursor.item/im"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1082204
    invoke-virtual {p0, v2}, LX/6Mz;->f(LX/6NG;)V

    goto :goto_1

    .line 1082205
    :cond_b
    const-string v4, "vnd.android.cursor.item/nickname"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1082206
    invoke-virtual {p0, v2}, LX/6Mz;->g(LX/6NG;)V

    goto :goto_1

    .line 1082207
    :cond_c
    const-string v4, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1082208
    invoke-virtual {p0, v2}, LX/6Mz;->h(LX/6NG;)V

    goto :goto_1

    .line 1082209
    :cond_d
    const-string v4, "vnd.android.cursor.item/website"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1082210
    invoke-virtual {p0, v2}, LX/6Mz;->i(LX/6NG;)V

    goto :goto_1

    .line 1082211
    :cond_e
    const-string v4, "vnd.android.cursor.item/relation"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1082212
    invoke-virtual {p0, v2}, LX/6Mz;->j(LX/6NG;)V

    goto :goto_1

    .line 1082213
    :cond_f
    const-string v4, "vnd.android.cursor.item/organization"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1082214
    invoke-virtual {p0, v2}, LX/6Mz;->k(LX/6NG;)V

    goto/16 :goto_1

    .line 1082215
    :cond_10
    const-string v4, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1082216
    invoke-virtual {p0, v2}, LX/6Mz;->l(LX/6NG;)V

    goto/16 :goto_1

    .line 1082217
    :cond_11
    const-string v4, "vnd.android.cursor.item/vnd.com.whatsapp.profile"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1082218
    invoke-virtual {p0, v2}, LX/6Mz;->n(LX/6NG;)V

    goto/16 :goto_1
.end method

.method public abstract a(LX/6NG;)V
.end method

.method public abstract b(LX/6NG;)V
.end method

.method public final c()I
    .locals 7

    .prologue
    .line 1082162
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 1082163
    const-wide/16 v2, -0x1

    .line 1082164
    const/4 v0, 0x0

    .line 1082165
    iget-object v4, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const/4 v5, -0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1082166
    :cond_0
    :goto_0
    iget-object v4, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1082167
    iget-object v4, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "deleted"

    invoke-static {v4, v5}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 1082168
    if-nez v4, :cond_0

    .line 1082169
    iget-object v4, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "contact_id"

    invoke-static {v4, v5}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 1082170
    cmp-long v6, v4, v2

    if-eqz v6, :cond_0

    .line 1082171
    add-int/lit8 v0, v0, 0x1

    move-wide v2, v4

    .line 1082172
    goto :goto_0

    .line 1082173
    :cond_1
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1082174
    return v0
.end method

.method public abstract c(LX/6NG;)V
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1082160
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1082161
    return-void
.end method

.method public d(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082150
    return-void
.end method

.method public e(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082159
    return-void
.end method

.method public f(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082149
    return-void
.end method

.method public g(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082151
    return-void
.end method

.method public h(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082152
    return-void
.end method

.method public i(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082153
    return-void
.end method

.method public j(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082154
    return-void
.end method

.method public k(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082155
    return-void
.end method

.method public l(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082156
    return-void
.end method

.method public m(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082157
    return-void
.end method

.method public n(LX/6NG;)V
    .locals 0

    .prologue
    .line 1082158
    return-void
.end method
