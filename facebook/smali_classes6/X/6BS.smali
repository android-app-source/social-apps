.class public final LX/6BS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1062679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 4

    .prologue
    .line 1062620
    if-nez p0, :cond_0

    .line 1062621
    const/4 v0, 0x0

    .line 1062622
    :goto_0
    return-object v0

    .line 1062623
    :cond_0
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 1062624
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->b()Z

    move-result v0

    .line 1062625
    iput-boolean v0, v2, LX/3dM;->d:Z

    .line 1062626
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->c()Z

    move-result v0

    .line 1062627
    iput-boolean v0, v2, LX/3dM;->e:Z

    .line 1062628
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->d()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->c(Z)LX/3dM;

    .line 1062629
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->e()Z

    move-result v0

    .line 1062630
    iput-boolean v0, v2, LX/3dM;->g:Z

    .line 1062631
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->ac_()Z

    move-result v0

    .line 1062632
    iput-boolean v0, v2, LX/3dM;->h:Z

    .line 1062633
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->ad_()Z

    move-result v0

    .line 1062634
    iput-boolean v0, v2, LX/3dM;->i:Z

    .line 1062635
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->j()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->g(Z)LX/3dM;

    .line 1062636
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->s()Z

    move-result v0

    .line 1062637
    iput-boolean v0, v2, LX/3dM;->k:Z

    .line 1062638
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->k()Z

    move-result v0

    .line 1062639
    iput-boolean v0, v2, LX/3dM;->l:Z

    .line 1062640
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1062641
    iput-object v0, v2, LX/3dM;->p:Ljava/lang/String;

    .line 1062642
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->t()Ljava/lang/String;

    move-result-object v0

    .line 1062643
    iput-object v0, v2, LX/3dM;->r:Ljava/lang/String;

    .line 1062644
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->m()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->j(Z)LX/3dM;

    .line 1062645
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->n()Ljava/lang/String;

    move-result-object v0

    .line 1062646
    iput-object v0, v2, LX/3dM;->y:Ljava/lang/String;

    .line 1062647
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;)Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    .line 1062648
    iput-object v0, v2, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1062649
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->o()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->l(Z)LX/3dM;

    .line 1062650
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v0

    .line 1062651
    iput-object v0, v2, LX/3dM;->D:Ljava/lang/String;

    .line 1062652
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062653
    iput-object v0, v2, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062654
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1062655
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1062656
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->q()Ljava/lang/String;

    move-result-object v0

    .line 1062657
    iput-object v0, v2, LX/3dM;->J:Ljava/lang/String;

    .line 1062658
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    .line 1062659
    iput-object v0, v2, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 1062660
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062661
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1062662
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062663
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062664
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062665
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062666
    iput-object v0, v2, LX/3dM;->N:LX/0Px;

    .line 1062667
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1062668
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1062669
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->C()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1062670
    iput-object v0, v2, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1062671
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;)Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 1062672
    iput-object v0, v2, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1062673
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->E()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062674
    iput-object v0, v2, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062675
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F()I

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->b(I)LX/3dM;

    .line 1062676
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->G()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062677
    iput-object v0, v2, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062678
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 4

    .prologue
    .line 1062561
    if-nez p0, :cond_0

    .line 1062562
    const/4 v0, 0x0

    .line 1062563
    :goto_0
    return-object v0

    .line 1062564
    :cond_0
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 1062565
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->b()Z

    move-result v0

    .line 1062566
    iput-boolean v0, v2, LX/3dM;->d:Z

    .line 1062567
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->c()Z

    move-result v0

    .line 1062568
    iput-boolean v0, v2, LX/3dM;->e:Z

    .line 1062569
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->d()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->c(Z)LX/3dM;

    .line 1062570
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->e()Z

    move-result v0

    .line 1062571
    iput-boolean v0, v2, LX/3dM;->g:Z

    .line 1062572
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->ac_()Z

    move-result v0

    .line 1062573
    iput-boolean v0, v2, LX/3dM;->h:Z

    .line 1062574
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->ad_()Z

    move-result v0

    .line 1062575
    iput-boolean v0, v2, LX/3dM;->i:Z

    .line 1062576
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->j()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->g(Z)LX/3dM;

    .line 1062577
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->s()Z

    move-result v0

    .line 1062578
    iput-boolean v0, v2, LX/3dM;->k:Z

    .line 1062579
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->k()Z

    move-result v0

    .line 1062580
    iput-boolean v0, v2, LX/3dM;->l:Z

    .line 1062581
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1062582
    iput-object v0, v2, LX/3dM;->p:Ljava/lang/String;

    .line 1062583
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->t()Ljava/lang/String;

    move-result-object v0

    .line 1062584
    iput-object v0, v2, LX/3dM;->r:Ljava/lang/String;

    .line 1062585
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->m()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->j(Z)LX/3dM;

    .line 1062586
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->n()Ljava/lang/String;

    move-result-object v0

    .line 1062587
    iput-object v0, v2, LX/3dM;->y:Ljava/lang/String;

    .line 1062588
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;)Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    .line 1062589
    iput-object v0, v2, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1062590
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->o()Z

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->l(Z)LX/3dM;

    .line 1062591
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v0

    .line 1062592
    iput-object v0, v2, LX/3dM;->D:Ljava/lang/String;

    .line 1062593
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062594
    iput-object v0, v2, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062595
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1062596
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1062597
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->q()Ljava/lang/String;

    move-result-object v0

    .line 1062598
    iput-object v0, v2, LX/3dM;->J:Ljava/lang/String;

    .line 1062599
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    .line 1062600
    iput-object v0, v2, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 1062601
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062602
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1062603
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062604
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062605
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062606
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062607
    iput-object v0, v2, LX/3dM;->N:LX/0Px;

    .line 1062608
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1062609
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1062610
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->C()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1062611
    iput-object v0, v2, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1062612
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;)Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 1062613
    iput-object v0, v2, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1062614
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->E()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062615
    iput-object v0, v2, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062616
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F()I

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->b(I)LX/3dM;

    .line 1062617
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->G()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1062618
    iput-object v0, v2, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1062619
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .locals 2

    .prologue
    .line 1062554
    if-nez p0, :cond_0

    .line 1062555
    const/4 v0, 0x0

    .line 1062556
    :goto_0
    return-object v0

    .line 1062557
    :cond_0
    new-instance v0, LX/4WL;

    invoke-direct {v0}, LX/4WL;-><init>()V

    .line 1062558
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v1

    .line 1062559
    iput v1, v0, LX/4WL;->b:I

    .line 1062560
    invoke-virtual {v0}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1062543
    if-nez p0, :cond_0

    .line 1062544
    const/4 v0, 0x0

    .line 1062545
    :goto_0
    return-object v0

    .line 1062546
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1062547
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1062548
    iput v1, v0, LX/2dc;->c:I

    .line 1062549
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1062550
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1062551
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1062552
    iput v1, v0, LX/2dc;->i:I

    .line 1062553
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;)Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;
    .locals 6

    .prologue
    .line 1062521
    if-nez p0, :cond_0

    .line 1062522
    const/4 v0, 0x0

    .line 1062523
    :goto_0
    return-object v0

    .line 1062524
    :cond_0
    new-instance v2, LX/4Wx;

    invoke-direct {v2}, LX/4Wx;-><init>()V

    .line 1062525
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062526
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1062527
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062528
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 1062529
    if-nez v0, :cond_3

    .line 1062530
    const/4 v4, 0x0

    .line 1062531
    :goto_2
    move-object v0, v4

    .line 1062532
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062534
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062535
    iput-object v0, v2, LX/4Wx;->b:LX/0Px;

    .line 1062536
    :cond_2
    invoke-virtual {v2}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    goto :goto_0

    .line 1062537
    :cond_3
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    .line 1062538
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 1062539
    iput-object v5, v4, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062540
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1062541
    iput-object v5, v4, LX/3dL;->ag:Ljava/lang/String;

    .line 1062542
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    goto :goto_2
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 2

    .prologue
    .line 1062514
    if-nez p0, :cond_0

    .line 1062515
    const/4 v0, 0x0

    .line 1062516
    :goto_0
    return-object v0

    .line 1062517
    :cond_0
    new-instance v0, LX/3dN;

    invoke-direct {v0}, LX/3dN;-><init>()V

    .line 1062518
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a()I

    move-result v1

    .line 1062519
    iput v1, v0, LX/3dN;->b:I

    .line 1062520
    invoke-virtual {v0}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 2

    .prologue
    .line 1062340
    if-nez p0, :cond_0

    .line 1062341
    const/4 v0, 0x0

    .line 1062342
    :goto_0
    return-object v0

    .line 1062343
    :cond_0
    new-instance v0, LX/4XY;

    invoke-direct {v0}, LX/4XY;-><init>()V

    .line 1062344
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1062345
    iput-object v1, v0, LX/4XY;->ag:Ljava/lang/String;

    .line 1062346
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1062347
    iput-object v1, v0, LX/4XY;->aT:Ljava/lang/String;

    .line 1062348
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;->d()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/6BS;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1062349
    iput-object v1, v0, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1062350
    invoke-virtual {v0}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .locals 2

    .prologue
    .line 1062507
    if-nez p0, :cond_0

    .line 1062508
    const/4 v0, 0x0

    .line 1062509
    :goto_0
    return-object v0

    .line 1062510
    :cond_0
    new-instance v0, LX/3dO;

    invoke-direct {v0}, LX/3dO;-><init>()V

    .line 1062511
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    .line 1062512
    iput v1, v0, LX/3dO;->b:I

    .line 1062513
    invoke-virtual {v0}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .locals 2

    .prologue
    .line 1062500
    if-nez p0, :cond_0

    .line 1062501
    const/4 v0, 0x0

    .line 1062502
    :goto_0
    return-object v0

    .line 1062503
    :cond_0
    new-instance v0, LX/4Yj;

    invoke-direct {v0}, LX/4Yj;-><init>()V

    .line 1062504
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a()I

    move-result v1

    .line 1062505
    iput v1, v0, LX/4Yj;->b:I

    .line 1062506
    invoke-virtual {v0}, LX/4Yj;->a()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1062413
    if-nez p0, :cond_0

    .line 1062414
    const/4 v0, 0x0

    .line 1062415
    :goto_0
    return-object v0

    .line 1062416
    :cond_0
    new-instance v3, LX/173;

    invoke-direct {v3}, LX/173;-><init>()V

    .line 1062417
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062418
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1062419
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062420
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;

    .line 1062421
    if-nez v0, :cond_5

    .line 1062422
    const/4 v5, 0x0

    .line 1062423
    :goto_2
    move-object v0, v5

    .line 1062424
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062425
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062426
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062427
    iput-object v0, v3, LX/173;->b:LX/0Px;

    .line 1062428
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1062429
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1062430
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1062431
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1062432
    if-nez v0, :cond_9

    .line 1062433
    const/4 v4, 0x0

    .line 1062434
    :goto_4
    move-object v0, v4

    .line 1062435
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062436
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1062437
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062438
    iput-object v0, v3, LX/173;->e:LX/0Px;

    .line 1062439
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1062440
    iput-object v0, v3, LX/173;->f:Ljava/lang/String;

    .line 1062441
    invoke-virtual {v3}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1062442
    :cond_5
    new-instance v7, LX/4Vo;

    invoke-direct {v7}, LX/4Vo;-><init>()V

    .line 1062443
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->a()I

    move-result v5

    .line 1062444
    iput v5, v7, LX/4Vo;->b:I

    .line 1062445
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->b()I

    move-result v5

    .line 1062446
    iput v5, v7, LX/4Vo;->c:I

    .line 1062447
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->c()I

    move-result v5

    .line 1062448
    iput v5, v7, LX/4Vo;->d:I

    .line 1062449
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 1062450
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1062451
    const/4 v5, 0x0

    move v6, v5

    :goto_5
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_6

    .line 1062452
    invoke-virtual {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;

    .line 1062453
    if-nez v5, :cond_8

    .line 1062454
    const/4 v9, 0x0

    .line 1062455
    :goto_6
    move-object v5, v9

    .line 1062456
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062457
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_5

    .line 1062458
    :cond_6
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1062459
    iput-object v5, v7, LX/4Vo;->e:LX/0Px;

    .line 1062460
    :cond_7
    invoke-virtual {v7}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v5

    goto/16 :goto_2

    .line 1062461
    :cond_8
    new-instance v9, LX/170;

    invoke-direct {v9}, LX/170;-><init>()V

    .line 1062462
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    .line 1062463
    iput-object v10, v9, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062464
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 1062465
    iput-object v10, v9, LX/170;->o:Ljava/lang/String;

    .line 1062466
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->d()Ljava/lang/String;

    move-result-object v10

    .line 1062467
    iput-object v10, v9, LX/170;->A:Ljava/lang/String;

    .line 1062468
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->e()LX/1Fb;

    move-result-object v10

    invoke-static {v10}, LX/6BS;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    .line 1062469
    iput-object v10, v9, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1062470
    invoke-virtual {v5}, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$TextWithEntitiesAggregatedRangeFieldsModel$SampleEntitiesModel;->B_()Ljava/lang/String;

    move-result-object v10

    .line 1062471
    iput-object v10, v9, LX/170;->Y:Ljava/lang/String;

    .line 1062472
    invoke-virtual {v9}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v9

    goto :goto_6

    .line 1062473
    :cond_9
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 1062474
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v5

    .line 1062475
    if-nez v5, :cond_a

    .line 1062476
    const/4 v6, 0x0

    .line 1062477
    :goto_7
    move-object v5, v6

    .line 1062478
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1062479
    invoke-interface {v0}, LX/1W5;->b()I

    move-result v5

    .line 1062480
    iput v5, v4, LX/4W6;->c:I

    .line 1062481
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    .line 1062482
    iput v5, v4, LX/4W6;->d:I

    .line 1062483
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto/16 :goto_4

    .line 1062484
    :cond_a
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 1062485
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1062486
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1062487
    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v7

    .line 1062488
    iput-object v7, v6, LX/170;->b:LX/0Px;

    .line 1062489
    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v7

    .line 1062490
    iput-object v7, v6, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1062491
    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v7

    .line 1062492
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 1062493
    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v7

    .line 1062494
    iput-object v7, v6, LX/170;->A:Ljava/lang/String;

    .line 1062495
    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v7

    .line 1062496
    iput-object v7, v6, LX/170;->X:Ljava/lang/String;

    .line 1062497
    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    .line 1062498
    iput-object v7, v6, LX/170;->Y:Ljava/lang/String;

    .line 1062499
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_7
.end method

.method private static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 4

    .prologue
    .line 1062387
    if-nez p0, :cond_0

    .line 1062388
    const/4 v0, 0x0

    .line 1062389
    :goto_0
    return-object v0

    .line 1062390
    :cond_0
    new-instance v0, LX/4ZH;

    invoke-direct {v0}, LX/4ZH;-><init>()V

    .line 1062391
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a()I

    move-result v1

    .line 1062392
    iput v1, v0, LX/4ZH;->b:I

    .line 1062393
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v1

    .line 1062394
    iput-object v1, v0, LX/4ZH;->c:LX/0Px;

    .line 1062395
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->c()LX/0us;

    move-result-object v1

    .line 1062396
    if-nez v1, :cond_1

    .line 1062397
    const/4 v2, 0x0

    .line 1062398
    :goto_1
    move-object v1, v2

    .line 1062399
    iput-object v1, v0, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1062400
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->d()I

    move-result v1

    .line 1062401
    iput v1, v0, LX/4ZH;->e:I

    .line 1062402
    invoke-virtual {v0}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    goto :goto_0

    .line 1062403
    :cond_1
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 1062404
    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 1062405
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 1062406
    invoke-interface {v1}, LX/0us;->b()Z

    move-result v3

    .line 1062407
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 1062408
    invoke-interface {v1}, LX/0us;->c()Z

    move-result v3

    .line 1062409
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 1062410
    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v3

    .line 1062411
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 1062412
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto :goto_1
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .locals 8

    .prologue
    .line 1062358
    if-nez p0, :cond_0

    .line 1062359
    const/4 v0, 0x0

    .line 1062360
    :goto_0
    return-object v0

    .line 1062361
    :cond_0
    new-instance v2, LX/3dQ;

    invoke-direct {v2}, LX/3dQ;-><init>()V

    .line 1062362
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062363
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1062364
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1062365
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 1062366
    if-nez v0, :cond_3

    .line 1062367
    const/4 v4, 0x0

    .line 1062368
    :goto_2
    move-object v0, v4

    .line 1062369
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1062370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1062371
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1062372
    iput-object v0, v2, LX/3dQ;->b:LX/0Px;

    .line 1062373
    :cond_2
    invoke-virtual {v2}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    goto :goto_0

    .line 1062374
    :cond_3
    new-instance v4, LX/4ZJ;

    invoke-direct {v4}, LX/4ZJ;-><init>()V

    .line 1062375
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 1062376
    if-nez v5, :cond_4

    .line 1062377
    const/4 v6, 0x0

    .line 1062378
    :goto_3
    move-object v5, v6

    .line 1062379
    iput-object v5, v4, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1062380
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v5

    .line 1062381
    iput v5, v4, LX/4ZJ;->c:I

    .line 1062382
    invoke-virtual {v4}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v4

    goto :goto_2

    .line 1062383
    :cond_4
    new-instance v6, LX/4WM;

    invoke-direct {v6}, LX/4WM;-><init>()V

    .line 1062384
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v7

    .line 1062385
    iput v7, v6, LX/4WM;->f:I

    .line 1062386
    invoke-virtual {v6}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v6

    goto :goto_3
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;)Lcom/facebook/graphql/model/GraphQLUser;
    .locals 2

    .prologue
    .line 1062351
    if-nez p0, :cond_0

    .line 1062352
    const/4 v0, 0x0

    .line 1062353
    :goto_0
    return-object v0

    .line 1062354
    :cond_0
    new-instance v0, LX/33O;

    invoke-direct {v0}, LX/33O;-><init>()V

    .line 1062355
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1062356
    iput-object v1, v0, LX/33O;->aI:Ljava/lang/String;

    .line 1062357
    invoke-virtual {v0}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    goto :goto_0
.end method
