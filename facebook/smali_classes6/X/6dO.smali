.class public final LX/6dO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/AutoCloseable;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field public final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private final Q:I

.field private final R:I

.field private final S:I

.field private final T:I

.field private final U:I

.field private final V:I

.field private final W:I

.field private final X:I

.field private final Y:I

.field public final Z:I

.field public final synthetic a:LX/2N5;

.field private final aa:I

.field private final ab:I

.field private final ac:I

.field private final ad:I

.field private final ae:I

.field private af:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field private ag:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field private ah:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field private ai:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;"
        }
    .end annotation
.end field

.field private aj:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadJoinRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/database/Cursor;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(LX/2N5;Landroid/database/Cursor;Z)V
    .locals 1

    .prologue
    .line 1116031
    iput-object p1, p0, LX/6dO;->a:LX/2N5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1116032
    iput-object p2, p0, LX/6dO;->b:Landroid/database/Cursor;

    .line 1116033
    const-string v0, "thread_key"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->c:I

    .line 1116034
    const-string v0, "legacy_thread_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->d:I

    .line 1116035
    const-string v0, "action_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->e:I

    .line 1116036
    const-string v0, "refetch_action_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->f:I

    .line 1116037
    const-string v0, "last_visible_action_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->g:I

    .line 1116038
    const-string v0, "sequence_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->h:I

    .line 1116039
    const-string v0, "name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->i:I

    .line 1116040
    const-string v0, "senders"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->j:I

    .line 1116041
    const-string v0, "snippet"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->k:I

    .line 1116042
    const-string v0, "snippet_sender"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->l:I

    .line 1116043
    const-string v0, "admin_snippet"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->m:I

    .line 1116044
    if-eqz p3, :cond_0

    .line 1116045
    const-string v0, "timestamp_in_folder_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->n:I

    .line 1116046
    :goto_0
    const-string v0, "last_read_timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->o:I

    .line 1116047
    const-string v0, "approx_total_message_count"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->p:I

    .line 1116048
    const-string v0, "unread_message_count"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->q:I

    .line 1116049
    const-string v0, "pic_hash"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->r:I

    .line 1116050
    const-string v0, "can_reply_to"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->s:I

    .line 1116051
    const-string v0, "cannot_reply_reason"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->t:I

    .line 1116052
    const-string v0, "pic"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->u:I

    .line 1116053
    const-string v0, "media_preview"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->v:I

    .line 1116054
    const-string v0, "last_fetch_time_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->w:I

    .line 1116055
    const-string v0, "is_subscribed"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->x:I

    .line 1116056
    const-string v0, "folder"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->y:I

    .line 1116057
    const-string v0, "draft"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->z:I

    .line 1116058
    const-string v0, "missed_call_status"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->A:I

    .line 1116059
    const-string v0, "mute_until"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->B:I

    .line 1116060
    const-string v0, "me_bubble_color"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->C:I

    .line 1116061
    const-string v0, "other_bubble_color"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->D:I

    .line 1116062
    const-string v0, "wallpaper_color"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->E:I

    .line 1116063
    const-string v0, "last_fetch_action_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->F:I

    .line 1116064
    const-string v0, "initial_fetch_complete"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->G:I

    .line 1116065
    const-string v0, "custom_like_emoji"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->H:I

    .line 1116066
    const-string v0, "outgoing_message_lifetime"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->I:I

    .line 1116067
    const-string v0, "custom_nicknames"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->J:I

    .line 1116068
    const-string v0, "invite_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->K:I

    .line 1116069
    const-string v0, "is_last_message_sponsored"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->L:I

    .line 1116070
    const-string v0, "group_chat_rank"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->M:I

    .line 1116071
    const-string v0, "game_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->N:I

    .line 1116072
    const-string v0, "is_joinable"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->O:I

    .line 1116073
    const-string v0, "requires_approval"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->P:I

    .line 1116074
    const-string v0, "rtc_call_info"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->Q:I

    .line 1116075
    const-string v0, "last_message_commerce_message_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->R:I

    .line 1116076
    const-string v0, "is_thread_queue_enabled"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->S:I

    .line 1116077
    const-string v0, "group_description"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->T:I

    .line 1116078
    const-string v0, "booking_requests"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->U:I

    .line 1116079
    const-string v0, "last_call_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->V:I

    .line 1116080
    const-string v0, "is_discoverable"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->W:I

    .line 1116081
    const-string v0, "last_sponsored_message_call_to_action"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->X:I

    .line 1116082
    const-string v0, "montage_thread_key"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->Y:I

    .line 1116083
    const-string v0, "montage_preview_message_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->Z:I

    .line 1116084
    const-string v0, "montage_preview_text"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->aa:I

    .line 1116085
    const-string v0, "montage_preview_sticker_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->ab:I

    .line 1116086
    const-string v0, "montage_preview_attachments"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->ac:I

    .line 1116087
    const-string v0, "montage_thread_read_watermark_timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->ad:I

    .line 1116088
    const-string v0, "montage_latest_message_timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->ae:I

    .line 1116089
    return-void

    .line 1116090
    :cond_0
    const-string v0, "timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6dO;->n:I

    goto/16 :goto_0
.end method

.method private static a(LX/0Xu;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Xu",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "TT;>;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1116091
    invoke-interface {p0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 1116092
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1116093
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1116094
    goto :goto_0
.end method

.method private a(LX/6g6;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1116095
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->Y:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1116096
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->Y:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 1116097
    :goto_0
    iput-object v0, p1, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1116098
    if-eqz v0, :cond_0

    .line 1116099
    iget v0, p0, LX/6dO;->Z:I

    if-ltz v0, :cond_3

    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->Z:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1116100
    if-eqz v0, :cond_0

    .line 1116101
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->Z:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1116102
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2NI;

    iget-object v3, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6dO;->ac:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, LX/2NI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1116103
    invoke-static {v2}, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a(Ljava/lang/String;)LX/6fg;

    move-result-object v2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1116104
    :goto_2
    iput-object v1, v2, LX/6fg;->d:Lcom/facebook/messaging/model/attachment/Attachment;

    .line 1116105
    move-object v0, v2

    .line 1116106
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->ab:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1116107
    iput-object v1, v0, LX/6fg;->e:Ljava/lang/String;

    .line 1116108
    move-object v0, v0

    .line 1116109
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->aa:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1116110
    iput-object v1, v0, LX/6fg;->f:Ljava/lang/String;

    .line 1116111
    move-object v0, v0

    .line 1116112
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->ad:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1116113
    iput-wide v2, v0, LX/6fg;->b:J

    .line 1116114
    move-object v0, v0

    .line 1116115
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6dO;->ae:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1116116
    iput-wide v2, v0, LX/6fg;->c:J

    .line 1116117
    move-object v0, v0

    .line 1116118
    invoke-virtual {v0}, LX/6fg;->a()Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    move-result-object v0

    .line 1116119
    iput-object v0, p1, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 1116120
    :cond_0
    return-void

    .line 1116121
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    move-object v1, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private f()V
    .locals 13

    .prologue
    .line 1115988
    iget-object v0, p0, LX/6dO;->ai:LX/0Xu;

    if-eqz v0, :cond_1

    .line 1115989
    :cond_0
    :goto_0
    return-void

    .line 1115990
    :cond_1
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/6dO;->ai:LX/0Xu;

    .line 1115991
    invoke-static {p0}, LX/6dO;->h(LX/6dO;)V

    .line 1115992
    iget-object v0, p0, LX/6dO;->af:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1115993
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1115994
    iget-object v1, p0, LX/6dO;->af:LX/0Px;

    iget-object v2, p0, LX/6dO;->a:LX/2N5;

    iget-object v2, v2, LX/2N5;->b:LX/2N6;

    invoke-virtual {v2}, LX/2N6;->a()J

    move-result-wide v2

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1115995
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    invoke-direct {v9, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1115996
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v6, v8

    :goto_1
    if-ge v6, v10, :cond_2

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1115997
    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1115998
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1115999
    :cond_2
    sget-object v5, LX/6dV;->b:LX/0U1;

    .line 1116000
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1116001
    invoke-static {v5, v9}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v5

    .line 1116002
    sget-object v6, LX/6dV;->d:LX/0U1;

    .line 1116003
    iget-object v9, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v9

    .line 1116004
    const-wide/32 v9, 0xa4cb80

    sub-long v9, v2, v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    .line 1116005
    const/4 v9, 0x2

    new-array v9, v9, [LX/0ux;

    aput-object v5, v9, v8

    const/4 v5, 0x1

    aput-object v6, v9, v5

    invoke-static {v9}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v5

    .line 1116006
    const-string v6, "event_reminders"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v9

    const-string v12, "event_reminder_timestamp ASC"

    move-object v5, v0

    move-object v10, v7

    move-object v11, v7

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v0, v5

    .line 1116007
    new-instance v1, LX/6dJ;

    invoke-direct {v1, v0}, LX/6dJ;-><init>(Landroid/database/Cursor;)V

    .line 1116008
    :try_start_0
    invoke-virtual {v1}, LX/6dJ;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dH;

    .line 1116009
    iget-object v3, p0, LX/6dO;->ai:LX/0Xu;

    iget-object v4, v0, LX/6dH;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, LX/6dH;->b:Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    invoke-interface {v3, v4, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1116010
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/6dJ;->a()V

    throw v0

    :cond_3
    invoke-virtual {v1}, LX/6dJ;->a()V

    goto/16 :goto_0
.end method

.method private g()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1116011
    iget-object v0, p0, LX/6dO;->ag:LX/0Xu;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6dO;->ah:LX/0Xu;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6dO;->aj:LX/0Xu;

    if-eqz v0, :cond_1

    .line 1116012
    :cond_0
    :goto_0
    return-void

    .line 1116013
    :cond_1
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/6dO;->ag:LX/0Xu;

    .line 1116014
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/6dO;->ah:LX/0Xu;

    .line 1116015
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/6dO;->aj:LX/0Xu;

    .line 1116016
    invoke-static {p0}, LX/6dO;->h(LX/6dO;)V

    .line 1116017
    iget-object v0, p0, LX/6dO;->af:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1116018
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1116019
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1116020
    iget-object v3, p0, LX/6dO;->af:LX/0Px;

    invoke-static {v3}, LX/6cw;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1116021
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1116022
    new-instance v1, LX/6dM;

    invoke-direct {v1, v0}, LX/6dM;-><init>(Landroid/database/Cursor;)V

    .line 1116023
    :try_start_0
    invoke-virtual {v1}, LX/6dM;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dL;

    .line 1116024
    sget-object v3, LX/6dN;->a:[I

    iget-object v4, v0, LX/6dL;->b:LX/6cv;

    invoke-virtual {v4}, LX/6cv;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1116025
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Improper Thread Participant Type"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1116026
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/6dM;->a()V

    throw v0

    .line 1116027
    :pswitch_0
    :try_start_1
    iget-object v3, p0, LX/6dO;->ag:LX/0Xu;

    iget-object v4, v0, LX/6dL;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, LX/6dL;->c:Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-interface {v3, v4, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 1116028
    :pswitch_1
    iget-object v3, p0, LX/6dO;->ah:LX/0Xu;

    iget-object v4, v0, LX/6dL;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, LX/6dL;->c:Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-interface {v3, v4, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 1116029
    :pswitch_2
    iget-object v3, p0, LX/6dO;->aj:LX/0Xu;

    iget-object v4, v0, LX/6dL;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v5, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;

    iget-object v6, v0, LX/6dL;->d:Lcom/facebook/user/model/UserKey;

    iget-wide v8, v0, LX/6dL;->e:J

    invoke-direct {v5, v6, v8, v9}, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;-><init>(Lcom/facebook/user/model/UserKey;J)V

    invoke-interface {v3, v4, v5}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1116030
    :cond_2
    invoke-virtual {v1}, LX/6dM;->a()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static h(LX/6dO;)V
    .locals 7

    .prologue
    .line 1115974
    iget-object v0, p0, LX/6dO;->af:LX/0Px;

    if-eqz v0, :cond_0

    .line 1115975
    :goto_0
    return-void

    .line 1115976
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1115977
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 1115978
    :try_start_0
    iget-object v2, p0, LX/6dO;->b:Landroid/database/Cursor;

    const/4 v3, -0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1115979
    :goto_1
    iget-object v2, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1115980
    iget-object v2, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->c:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1115981
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    .line 1115982
    if-nez v3, :cond_1

    .line 1115983
    const-string v3, "ThreadSummaryCursorUtil"

    const-string v4, "Error parsing thread key from db cursor: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1115984
    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v0

    .line 1115985
    :cond_1
    :try_start_1
    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1115986
    :cond_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/6dO;->af:LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1115987
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/6g6;
    .locals 1

    .prologue
    .line 1115972
    invoke-virtual {p0}, LX/6dO;->c()LX/6dP;

    move-result-object v0

    .line 1115973
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/6dP;->a:LX/6g6;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 1115970
    invoke-virtual {p0}, LX/6dO;->c()LX/6dP;

    move-result-object v0

    .line 1115971
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/6dP;->a:LX/6g6;

    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/6dP;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1115804
    invoke-direct {p0}, LX/6dO;->g()V

    .line 1115805
    invoke-direct {p0}, LX/6dO;->f()V

    .line 1115806
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1115807
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v4

    .line 1115808
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6dO;->c:I

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    .line 1115809
    iput-object v5, v4, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1115810
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->d:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115811
    iput-object v0, v4, LX/6g6;->b:Ljava/lang/String;

    .line 1115812
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->e:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115813
    iput-wide v6, v4, LX/6g6;->c:J

    .line 1115814
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->f:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115815
    iput-wide v6, v4, LX/6g6;->e:J

    .line 1115816
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->g:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115817
    iput-wide v6, v4, LX/6g6;->f:J

    .line 1115818
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->h:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115819
    iput-wide v6, v4, LX/6g6;->d:J

    .line 1115820
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->i:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1115821
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->i:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115822
    iput-object v0, v4, LX/6g6;->g:Ljava/lang/String;

    .line 1115823
    :cond_0
    iget-object v0, p0, LX/6dO;->ag:LX/0Xu;

    invoke-static {v0, v5}, LX/6dO;->a(LX/0Xu;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v0

    .line 1115824
    iput-object v0, v4, LX/6g6;->h:Ljava/util/List;

    .line 1115825
    iget-object v0, p0, LX/6dO;->ah:LX/0Xu;

    invoke-static {v0, v5}, LX/6dO;->a(LX/0Xu;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v0

    .line 1115826
    iput-object v0, v4, LX/6g6;->i:Ljava/util/List;

    .line 1115827
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->c:LX/2N7;

    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->j:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/2N7;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1115828
    iput-object v0, v4, LX/6g6;->n:Ljava/util/List;

    .line 1115829
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->c:LX/2N7;

    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->l:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/2N7;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 1115830
    iput-object v0, v4, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1115831
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->k:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115832
    iput-object v0, v4, LX/6g6;->o:Ljava/lang/String;

    .line 1115833
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->m:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115834
    iput-object v0, v4, LX/6g6;->q:Ljava/lang/String;

    .line 1115835
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->n:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115836
    iput-wide v6, v4, LX/6g6;->j:J

    .line 1115837
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->o:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115838
    iput-wide v6, v4, LX/6g6;->k:J

    .line 1115839
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->p:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115840
    iput-wide v6, v4, LX/6g6;->l:J

    .line 1115841
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->q:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115842
    iput-wide v6, v4, LX/6g6;->m:J

    .line 1115843
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->r:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1115844
    iput-object v0, v4, LX/6g6;->r:Ljava/lang/String;

    .line 1115845
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->s:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 1115846
    :goto_0
    iput-boolean v0, v4, LX/6g6;->u:Z

    .line 1115847
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->t:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-result-object v0

    .line 1115848
    iput-object v0, v4, LX/6g6;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1115849
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->u:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1115850
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->u:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1115851
    iput-object v0, v4, LX/6g6;->s:Landroid/net/Uri;

    .line 1115852
    :cond_1
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->v:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1115853
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->v:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115854
    iget-object v6, p0, LX/6dO;->a:LX/2N5;

    iget-object v6, v6, LX/2N5;->g:LX/2NF;

    invoke-virtual {v6, v0}, LX/2NF;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    .line 1115855
    iput-object v0, v4, LX/6g6;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 1115856
    :cond_2
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->x:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 1115857
    :goto_1
    iput-boolean v0, v4, LX/6g6;->w:Z

    .line 1115858
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->y:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    .line 1115859
    iput-object v0, v4, LX/6g6;->A:LX/6ek;

    .line 1115860
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->d:LX/2NA;

    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->z:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/2NA;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessageDraft;

    move-result-object v0

    .line 1115861
    iput-object v0, v4, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 1115862
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->A:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, LX/6g5;->fromValue(I)LX/6g5;

    move-result-object v0

    .line 1115863
    iput-object v0, v4, LX/6g6;->y:LX/6g5;

    .line 1115864
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->B:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    .line 1115865
    iput-object v0, v4, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1115866
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v0

    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->E:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1115867
    iput v6, v0, LX/6fr;->a:I

    .line 1115868
    move-object v0, v0

    .line 1115869
    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->C:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1115870
    iput v6, v0, LX/6fr;->b:I

    .line 1115871
    move-object v0, v0

    .line 1115872
    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->D:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1115873
    iput v6, v0, LX/6fr;->c:I

    .line 1115874
    move-object v0, v0

    .line 1115875
    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->H:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1115876
    iput-object v6, v0, LX/6fr;->e:Ljava/lang/String;

    .line 1115877
    move-object v0, v0

    .line 1115878
    new-instance v6, Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1115879
    iget-object v7, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6dO;->J:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 1115880
    const/4 v7, 0x0

    .line 1115881
    :goto_2
    move-object v7, v7

    .line 1115882
    invoke-direct {v6, v7}, Lcom/facebook/messaging/model/threads/NicknamesMap;-><init>(Ljava/lang/String;)V

    .line 1115883
    iput-object v6, v0, LX/6fr;->f:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1115884
    move-object v0, v0

    .line 1115885
    invoke-virtual {v0}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v0

    .line 1115886
    iput-object v0, v4, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1115887
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->F:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115888
    iput-wide v6, v4, LX/6g6;->G:J

    .line 1115889
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->G:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 1115890
    :goto_3
    iput-boolean v0, v4, LX/6g6;->H:Z

    .line 1115891
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->I:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1115892
    iput v0, v4, LX/6g6;->I:I

    .line 1115893
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->L:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    .line 1115894
    :goto_4
    iput-boolean v0, v4, LX/6g6;->K:Z

    .line 1115895
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->M:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 1115896
    iput v0, v4, LX/6g6;->N:F

    .line 1115897
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->e:LX/2ND;

    iget-object v6, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6dO;->N:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1115898
    if-eqz v6, :cond_3

    const-string v7, "{}"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 1115899
    :cond_3
    sget-object v7, LX/0Rg;->a:LX/0Rg;

    move-object v7, v7

    .line 1115900
    :goto_5
    move-object v0, v7

    .line 1115901
    iput-object v0, v4, LX/6g6;->O:Ljava/util/Map;

    .line 1115902
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6dO;->w:I

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115903
    iget-object v0, p0, LX/6dO;->ai:LX/0Xu;

    invoke-static {v0, v5}, LX/6dO;->a(LX/0Xu;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v0

    .line 1115904
    iput-object v0, v4, LX/6g6;->J:Ljava/util/List;

    .line 1115905
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->f:LX/2NE;

    iget-object v8, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v9, p0, LX/6dO;->Q:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    .line 1115906
    if-nez v8, :cond_16

    .line 1115907
    :goto_6
    move-object v0, v10

    .line 1115908
    iput-object v0, v4, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1115909
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6dO;->R:I

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_e

    move-object v0, v3

    .line 1115910
    :goto_7
    iput-object v0, v4, LX/6g6;->Q:Ljava/lang/String;

    .line 1115911
    invoke-static {}, Lcom/facebook/messaging/model/threads/RoomThreadData;->newBuilder()LX/6fm;

    move-result-object v3

    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6dO;->O:I

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    .line 1115912
    :goto_8
    iput-boolean v0, v3, LX/6fm;->b:Z

    .line 1115913
    move-object v3, v3

    .line 1115914
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6dO;->P:I

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    .line 1115915
    :goto_9
    iput-boolean v0, v3, LX/6fm;->d:Z

    .line 1115916
    move-object v0, v3

    .line 1115917
    iget-object v3, p0, LX/6dO;->aj:LX/0Xu;

    invoke-static {v3, v5}, LX/6dO;->a(LX/0Xu;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/6fm;->a(LX/0Px;)LX/6fm;

    move-result-object v0

    iget-object v3, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6dO;->W:I

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_11

    .line 1115918
    :goto_a
    iput-boolean v1, v0, LX/6fm;->c:Z

    .line 1115919
    move-object v0, v0

    .line 1115920
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->K:I

    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1115921
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->K:I

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1115922
    iput-object v1, v0, LX/6fm;->a:Landroid/net/Uri;

    .line 1115923
    :cond_4
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->T:I

    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1115924
    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->T:I

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115925
    iput-object v1, v0, LX/6fm;->f:Ljava/lang/String;

    .line 1115926
    :cond_5
    invoke-virtual {v0}, LX/6fm;->a()Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)LX/6g6;

    .line 1115927
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->S:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1115928
    if-nez v0, :cond_12

    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 1115929
    :goto_b
    iput-object v0, v4, LX/6g6;->S:LX/03R;

    .line 1115930
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->U:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1115931
    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    iget-object v0, v0, LX/2N5;->h:LX/2NG;

    iget-object v1, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->U:I

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2NG;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    move-result-object v0

    .line 1115932
    iput-object v0, v4, LX/6g6;->U:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 1115933
    :cond_6
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->V:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1115934
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->V:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1115935
    iput-wide v0, v4, LX/6g6;->V:J

    .line 1115936
    :cond_7
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->X:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1115937
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6dO;->X:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4gg;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1115938
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1115939
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1115940
    iput-object v0, v4, LX/6g6;->L:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1115941
    :cond_8
    invoke-direct {p0, v4}, LX/6dO;->a(LX/6g6;)V

    .line 1115942
    new-instance v3, LX/6dP;

    iget-object v0, p0, LX/6dO;->a:LX/2N5;

    invoke-direct {v3, v0, v4, v6, v7}, LX/6dP;-><init>(LX/2N5;LX/6g6;J)V

    .line 1115943
    :cond_9
    return-object v3

    :cond_a
    move v0, v2

    .line 1115944
    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 1115945
    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 1115946
    goto/16 :goto_3

    :cond_d
    move v0, v2

    .line 1115947
    goto/16 :goto_4

    .line 1115948
    :cond_e
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v3, p0, LX/6dO;->R:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_f
    move v0, v2

    .line 1115949
    goto/16 :goto_8

    :cond_10
    move v0, v2

    goto/16 :goto_9

    :cond_11
    move v1, v2

    goto/16 :goto_a

    .line 1115950
    :cond_12
    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    goto/16 :goto_b

    :cond_13
    iget-object v7, p0, LX/6dO;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6dO;->J:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    .line 1115951
    :cond_14
    iget-object v7, v0, LX/2ND;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2N8;

    invoke-virtual {v7, v6}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 1115952
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v8

    .line 1115953
    invoke-virtual {v7}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v9

    .line 1115954
    :goto_c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_15

    .line 1115955
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 1115956
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0lF;

    .line 1115957
    const-string v11, "high_score_user"

    invoke-virtual {v7, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    invoke-virtual {v11}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v11

    .line 1115958
    const-string v0, "high_score"

    invoke-virtual {v7, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    .line 1115959
    new-instance v6, Lcom/facebook/messaging/model/threads/ThreadGameData;

    invoke-direct {v6, v11, v0}, Lcom/facebook/messaging/model/threads/ThreadGameData;-><init>(Ljava/lang/String;I)V

    move-object v7, v6

    .line 1115960
    invoke-virtual {v8, v10, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_c

    .line 1115961
    :cond_15
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    goto/16 :goto_5

    .line 1115962
    :cond_16
    iget-object v9, v0, LX/2NE;->a:LX/2N8;

    invoke-virtual {v9, v8}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    .line 1115963
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->newBuilder()LX/6g1;

    move-result-object v12

    const-string v9, "call_state"

    invoke-virtual {v11, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    if-eqz v9, :cond_18

    const-string v9, "call_state"

    invoke-virtual {v11, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-virtual {v9}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v9

    :goto_d
    invoke-virtual {v12, v9}, LX/6g1;->a(Ljava/lang/String;)LX/6g1;

    move-result-object v12

    const-string v9, "server_info"

    invoke-virtual {v11, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    if-eqz v9, :cond_19

    const-string v9, "server_info"

    invoke-virtual {v11, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-virtual {v9}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v9

    .line 1115964
    :goto_e
    iput-object v9, v12, LX/6g1;->b:Ljava/lang/String;

    .line 1115965
    move-object v9, v12

    .line 1115966
    const-string v12, "initiator_id"

    invoke-virtual {v11, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v12

    if-eqz v12, :cond_17

    const-string v10, "initiator_id"

    invoke-virtual {v11, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-virtual {v10}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v10

    .line 1115967
    :cond_17
    iput-object v10, v9, LX/6g1;->c:Ljava/lang/String;

    .line 1115968
    move-object v9, v9

    .line 1115969
    invoke-virtual {v9}, LX/6g1;->d()Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    move-result-object v10

    goto/16 :goto_6

    :cond_18
    move-object v9, v10

    goto :goto_d

    :cond_19
    move-object v9, v10

    goto :goto_e
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1115802
    iget-object v0, p0, LX/6dO;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1115803
    return-void
.end method
