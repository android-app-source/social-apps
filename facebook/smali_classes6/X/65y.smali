.class public final LX/65y;
.super Ljava/util/zip/Inflater;
.source ""


# instance fields
.field public final synthetic a:LX/65z;


# direct methods
.method public constructor <init>(LX/65z;)V
    .locals 0

    .prologue
    .line 1049116
    iput-object p1, p0, LX/65y;->a:LX/65z;

    invoke-direct {p0}, Ljava/util/zip/Inflater;-><init>()V

    return-void
.end method


# virtual methods
.method public final inflate([BII)I
    .locals 2

    .prologue
    .line 1049117
    invoke-super {p0, p1, p2, p3}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v0

    .line 1049118
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/65y;->needsDictionary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1049119
    sget-object v0, LX/666;->a:[B

    invoke-virtual {p0, v0}, LX/65y;->setDictionary([B)V

    .line 1049120
    invoke-super {p0, p1, p2, p3}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v0

    .line 1049121
    :cond_0
    return v0
.end method
