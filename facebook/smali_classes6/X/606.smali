.class public LX/606;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/text/NumberFormat;


# instance fields
.field public b:Lcom/facebook/uicontrib/datepicker/Period;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/facebook/uicontrib/datepicker/Period;)V
    .locals 1

    .prologue
    .line 1036472
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1036473
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 1036474
    sput-object v0, LX/606;->a:Ljava/text/NumberFormat;

    const/4 p2, 0x0

    invoke-virtual {v0, p2}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 1036475
    iput-object p3, p0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036476
    invoke-static {p0}, LX/606;->b(LX/606;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/606;->a(LX/606;Ljava/util/List;)V

    .line 1036477
    return-void
.end method

.method public static a(LX/606;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036479
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1036480
    invoke-virtual {p0, v0}, LX/606;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 1036481
    :cond_0
    return-void
.end method

.method public static b(LX/606;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1036482
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1036483
    iget-object v0, p0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v0

    :goto_0
    iget-object v2, p0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1036484
    sget-object v2, LX/606;->a:Ljava/text/NumberFormat;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036485
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1036486
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(I)I
    .locals 4

    .prologue
    .line 1036478
    sget-object v0, LX/606;->a:Ljava/text/NumberFormat;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/606;->getPosition(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
