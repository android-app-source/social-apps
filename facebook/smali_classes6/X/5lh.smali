.class public final LX/5lh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 999200
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 999201
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 999202
    :goto_0
    return v1

    .line 999203
    :cond_0
    const-string v9, "is_service_item"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 999204
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    .line 999205
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 999206
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 999207
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 999208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 999209
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 999210
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 999211
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 999212
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 999213
    :cond_4
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 999214
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 999215
    :cond_5
    const-string v9, "page"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 999216
    invoke-static {p0, p1}, LX/5lg;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 999217
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 999218
    :cond_7
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 999219
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 999220
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 999221
    if-eqz v0, :cond_8

    .line 999222
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 999223
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 999224
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 999225
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 999226
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 999227
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 999228
    if-eqz v0, :cond_0

    .line 999229
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999230
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 999231
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999232
    if-eqz v0, :cond_1

    .line 999233
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999234
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999235
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999236
    if-eqz v0, :cond_2

    .line 999237
    const-string v1, "is_service_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999238
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999239
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999240
    if-eqz v0, :cond_3

    .line 999241
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999242
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999243
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999244
    if-eqz v0, :cond_4

    .line 999245
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999246
    invoke-static {p0, v0, p2}, LX/5lg;->a(LX/15i;ILX/0nX;)V

    .line 999247
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 999248
    return-void
.end method
