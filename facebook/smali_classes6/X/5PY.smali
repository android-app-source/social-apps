.class public LX/5PY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/nio/Buffer;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Ljava/nio/Buffer;I)V
    .locals 1

    .prologue
    .line 910845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910846
    iput-object p1, p0, LX/5PY;->a:Ljava/nio/Buffer;

    .line 910847
    iput p2, p0, LX/5PY;->b:I

    .line 910848
    const/16 v0, 0x1405

    iput v0, p0, LX/5PY;->c:I

    .line 910849
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .prologue
    .line 910850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910851
    array-length v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    iput-object v0, p0, LX/5PY;->a:Ljava/nio/Buffer;

    .line 910852
    array-length v0, p1

    iput v0, p0, LX/5PY;->b:I

    .line 910853
    const/16 v0, 0x1401

    iput v0, p0, LX/5PY;->c:I

    .line 910854
    return-void
.end method

.method public constructor <init>([S)V
    .locals 2

    .prologue
    .line 910855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910856
    array-length v0, p1

    invoke-static {v0}, Ljava/nio/ShortBuffer;->allocate(I)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    iput-object v0, p0, LX/5PY;->a:Ljava/nio/Buffer;

    .line 910857
    array-length v0, p1

    iput v0, p0, LX/5PY;->b:I

    .line 910858
    const/16 v0, 0x1403

    iput v0, p0, LX/5PY;->c:I

    .line 910859
    return-void
.end method
