.class public final enum LX/65X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/65X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/65X;

.field public static final enum CANCEL:LX/65X;

.field public static final enum COMPRESSION_ERROR:LX/65X;

.field public static final enum CONNECT_ERROR:LX/65X;

.field public static final enum ENHANCE_YOUR_CALM:LX/65X;

.field public static final enum FLOW_CONTROL_ERROR:LX/65X;

.field public static final enum FRAME_TOO_LARGE:LX/65X;

.field public static final enum HTTP_1_1_REQUIRED:LX/65X;

.field public static final enum INADEQUATE_SECURITY:LX/65X;

.field public static final enum INTERNAL_ERROR:LX/65X;

.field public static final enum INVALID_CREDENTIALS:LX/65X;

.field public static final enum INVALID_STREAM:LX/65X;

.field public static final enum NO_ERROR:LX/65X;

.field public static final enum PROTOCOL_ERROR:LX/65X;

.field public static final enum REFUSED_STREAM:LX/65X;

.field public static final enum STREAM_ALREADY_CLOSED:LX/65X;

.field public static final enum STREAM_CLOSED:LX/65X;

.field public static final enum STREAM_IN_USE:LX/65X;

.field public static final enum UNSUPPORTED_VERSION:LX/65X;


# instance fields
.field public final httpCode:I

.field public final spdyGoAwayCode:I

.field public final spdyRstCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v2, 0x0

    const/4 v14, 0x2

    const/4 v7, 0x1

    const/4 v4, -0x1

    .line 1047626
    new-instance v0, LX/65X;

    const-string v1, "NO_ERROR"

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/65X;->NO_ERROR:LX/65X;

    .line 1047627
    new-instance v5, LX/65X;

    const-string v6, "PROTOCOL_ERROR"

    move v8, v7

    move v9, v7

    move v10, v7

    invoke-direct/range {v5 .. v10}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/65X;->PROTOCOL_ERROR:LX/65X;

    .line 1047628
    new-instance v8, LX/65X;

    const-string v9, "INVALID_STREAM"

    move v10, v14

    move v11, v7

    move v12, v14

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->INVALID_STREAM:LX/65X;

    .line 1047629
    new-instance v8, LX/65X;

    const-string v9, "UNSUPPORTED_VERSION"

    const/4 v12, 0x4

    move v10, v15

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->UNSUPPORTED_VERSION:LX/65X;

    .line 1047630
    new-instance v8, LX/65X;

    const-string v9, "STREAM_IN_USE"

    const/4 v10, 0x4

    const/16 v12, 0x8

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->STREAM_IN_USE:LX/65X;

    .line 1047631
    new-instance v8, LX/65X;

    const-string v9, "STREAM_ALREADY_CLOSED"

    const/4 v10, 0x5

    const/16 v12, 0x9

    move v11, v7

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->STREAM_ALREADY_CLOSED:LX/65X;

    .line 1047632
    new-instance v8, LX/65X;

    const-string v9, "INTERNAL_ERROR"

    const/4 v10, 0x6

    const/4 v12, 0x6

    move v11, v14

    move v13, v14

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->INTERNAL_ERROR:LX/65X;

    .line 1047633
    new-instance v8, LX/65X;

    const-string v9, "FLOW_CONTROL_ERROR"

    const/4 v10, 0x7

    const/4 v12, 0x7

    move v11, v15

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->FLOW_CONTROL_ERROR:LX/65X;

    .line 1047634
    new-instance v8, LX/65X;

    const-string v9, "STREAM_CLOSED"

    const/16 v10, 0x8

    const/4 v11, 0x5

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->STREAM_CLOSED:LX/65X;

    .line 1047635
    new-instance v8, LX/65X;

    const-string v9, "FRAME_TOO_LARGE"

    const/16 v10, 0x9

    const/4 v11, 0x6

    const/16 v12, 0xb

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->FRAME_TOO_LARGE:LX/65X;

    .line 1047636
    new-instance v8, LX/65X;

    const-string v9, "REFUSED_STREAM"

    const/16 v10, 0xa

    const/4 v11, 0x7

    move v12, v15

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->REFUSED_STREAM:LX/65X;

    .line 1047637
    new-instance v8, LX/65X;

    const-string v9, "CANCEL"

    const/16 v10, 0xb

    const/16 v11, 0x8

    const/4 v12, 0x5

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->CANCEL:LX/65X;

    .line 1047638
    new-instance v8, LX/65X;

    const-string v9, "COMPRESSION_ERROR"

    const/16 v10, 0xc

    const/16 v11, 0x9

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->COMPRESSION_ERROR:LX/65X;

    .line 1047639
    new-instance v8, LX/65X;

    const-string v9, "CONNECT_ERROR"

    const/16 v10, 0xd

    const/16 v11, 0xa

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->CONNECT_ERROR:LX/65X;

    .line 1047640
    new-instance v8, LX/65X;

    const-string v9, "ENHANCE_YOUR_CALM"

    const/16 v10, 0xe

    const/16 v11, 0xb

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->ENHANCE_YOUR_CALM:LX/65X;

    .line 1047641
    new-instance v8, LX/65X;

    const-string v9, "INADEQUATE_SECURITY"

    const/16 v10, 0xf

    const/16 v11, 0xc

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->INADEQUATE_SECURITY:LX/65X;

    .line 1047642
    new-instance v8, LX/65X;

    const-string v9, "HTTP_1_1_REQUIRED"

    const/16 v10, 0x10

    const/16 v11, 0xd

    move v12, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->HTTP_1_1_REQUIRED:LX/65X;

    .line 1047643
    new-instance v8, LX/65X;

    const-string v9, "INVALID_CREDENTIALS"

    const/16 v10, 0x11

    const/16 v12, 0xa

    move v11, v4

    move v13, v4

    invoke-direct/range {v8 .. v13}, LX/65X;-><init>(Ljava/lang/String;IIII)V

    sput-object v8, LX/65X;->INVALID_CREDENTIALS:LX/65X;

    .line 1047644
    const/16 v0, 0x12

    new-array v0, v0, [LX/65X;

    sget-object v1, LX/65X;->NO_ERROR:LX/65X;

    aput-object v1, v0, v2

    sget-object v1, LX/65X;->PROTOCOL_ERROR:LX/65X;

    aput-object v1, v0, v7

    sget-object v1, LX/65X;->INVALID_STREAM:LX/65X;

    aput-object v1, v0, v14

    sget-object v1, LX/65X;->UNSUPPORTED_VERSION:LX/65X;

    aput-object v1, v0, v15

    const/4 v1, 0x4

    sget-object v2, LX/65X;->STREAM_IN_USE:LX/65X;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/65X;->STREAM_ALREADY_CLOSED:LX/65X;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/65X;->INTERNAL_ERROR:LX/65X;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/65X;->FLOW_CONTROL_ERROR:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/65X;->STREAM_CLOSED:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/65X;->FRAME_TOO_LARGE:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/65X;->REFUSED_STREAM:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/65X;->CANCEL:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/65X;->COMPRESSION_ERROR:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/65X;->CONNECT_ERROR:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/65X;->ENHANCE_YOUR_CALM:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/65X;->INADEQUATE_SECURITY:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/65X;->HTTP_1_1_REQUIRED:LX/65X;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/65X;->INVALID_CREDENTIALS:LX/65X;

    aput-object v2, v0, v1

    sput-object v0, LX/65X;->$VALUES:[LX/65X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 1047645
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1047646
    iput p3, p0, LX/65X;->httpCode:I

    .line 1047647
    iput p4, p0, LX/65X;->spdyRstCode:I

    .line 1047648
    iput p5, p0, LX/65X;->spdyGoAwayCode:I

    .line 1047649
    return-void
.end method

.method public static fromHttp2(I)LX/65X;
    .locals 5

    .prologue
    .line 1047650
    invoke-static {}, LX/65X;->values()[LX/65X;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1047651
    iget v4, v0, LX/65X;->httpCode:I

    if-ne v4, p0, :cond_0

    .line 1047652
    :goto_1
    return-object v0

    .line 1047653
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1047654
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static fromSpdy3Rst(I)LX/65X;
    .locals 5

    .prologue
    .line 1047655
    invoke-static {}, LX/65X;->values()[LX/65X;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1047656
    iget v4, v0, LX/65X;->spdyRstCode:I

    if-ne v4, p0, :cond_0

    .line 1047657
    :goto_1
    return-object v0

    .line 1047658
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1047659
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static fromSpdyGoAway(I)LX/65X;
    .locals 5

    .prologue
    .line 1047660
    invoke-static {}, LX/65X;->values()[LX/65X;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1047661
    iget v4, v0, LX/65X;->spdyGoAwayCode:I

    if-ne v4, p0, :cond_0

    .line 1047662
    :goto_1
    return-object v0

    .line 1047663
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1047664
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/65X;
    .locals 1

    .prologue
    .line 1047665
    const-class v0, LX/65X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/65X;

    return-object v0
.end method

.method public static values()[LX/65X;
    .locals 1

    .prologue
    .line 1047666
    sget-object v0, LX/65X;->$VALUES:[LX/65X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65X;

    return-object v0
.end method
