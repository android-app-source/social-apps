.class public final LX/5kN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:I

.field public L:I

.field public M:I

.field public N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:D

.field public W:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:I

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Z

.field public af:D

.field public ag:D

.field public ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:I

.field public ak:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:J

.field public t:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 992934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)LX/5kN;
    .locals 4

    .prologue
    .line 992935
    new-instance v0, LX/5kN;

    invoke-direct {v0}, LX/5kN;-><init>()V

    .line 992936
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 992937
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->b:Ljava/lang/String;

    .line 992938
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ap()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->c:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    .line 992939
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aq()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->d:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 992940
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->e:Ljava/lang/String;

    .line 992941
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->f:Z

    .line 992942
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->g:Z

    .line 992943
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->h:Z

    .line 992944
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->i:Z

    .line 992945
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->j:Z

    .line 992946
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->k:Z

    .line 992947
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->l:Z

    .line 992948
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->v()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->m:Z

    .line 992949
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->n:Z

    .line 992950
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->x()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->o:Z

    .line 992951
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->p:Z

    .line 992952
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->z()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->q:Z

    .line 992953
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ar()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    .line 992954
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->B()J

    move-result-wide v2

    iput-wide v2, v0, LX/5kN;->s:J

    .line 992955
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->as()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->t:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    .line 992956
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->D()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->u:Z

    .line 992957
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->at()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 992958
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->au()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    .line 992959
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->av()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 992960
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aw()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 992961
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->H()D

    move-result-wide v2

    iput-wide v2, v0, LX/5kN;->z:D

    .line 992962
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ax()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->A:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    .line 992963
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->J()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->B:Z

    .line 992964
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->C:Ljava/lang/String;

    .line 992965
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->D:Ljava/lang/String;

    .line 992966
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ay()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992967
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->az()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992968
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aA()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992969
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992970
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aC()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992971
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992972
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->N()I

    move-result v1

    iput v1, v0, LX/5kN;->K:I

    .line 992973
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->O()I

    move-result v1

    iput v1, v0, LX/5kN;->L:I

    .line 992974
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->P()I

    move-result v1

    iput v1, v0, LX/5kN;->M:I

    .line 992975
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aE()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    .line 992976
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->R()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->O:Z

    .line 992977
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->S()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->P:Z

    .line 992978
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->T()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->Q:Z

    .line 992979
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aF()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 992980
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aG()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->S:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    .line 992981
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aH()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    .line 992982
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->U:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 992983
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->Y()D

    move-result-wide v2

    iput-wide v2, v0, LX/5kN;->V:D

    .line 992984
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aJ()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->W:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    .line 992985
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aK()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->X:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 992986
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ab()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->Y:LX/0Px;

    .line 992987
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ac()I

    move-result v1

    iput v1, v0, LX/5kN;->Z:I

    .line 992988
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ad()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->aa:Ljava/lang/String;

    .line 992989
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aL()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ab:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    .line 992990
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aM()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ac:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 992991
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ad:Ljava/lang/String;

    .line 992992
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ah()Z

    move-result v1

    iput-boolean v1, v0, LX/5kN;->ae:Z

    .line 992993
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ai()D

    move-result-wide v2

    iput-wide v2, v0, LX/5kN;->af:D

    .line 992994
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aj()D

    move-result-wide v2

    iput-wide v2, v0, LX/5kN;->ag:D

    .line 992995
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->ak()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ah:Ljava/lang/String;

    .line 992996
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->al()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ai:Ljava/lang/String;

    .line 992997
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->am()I

    move-result v1

    iput v1, v0, LX/5kN;->aj:I

    .line 992998
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->ak:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 992999
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aO()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v1

    iput-object v1, v0, LX/5kN;->al:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    .line 993000
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 40

    .prologue
    .line 993001
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 993002
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5kN;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 993003
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5kN;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 993004
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5kN;->c:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 993005
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5kN;->d:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 993006
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5kN;->e:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 993007
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5kN;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 993008
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5kN;->t:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 993009
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5kN;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 993010
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5kN;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 993011
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5kN;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 993012
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5kN;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 993013
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5kN;->A:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 993014
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5kN;->C:Ljava/lang/String;

    invoke-virtual {v2, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 993015
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->D:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 993016
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 993017
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 993018
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 993019
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 993020
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 993021
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 993022
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 993023
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 993024
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->S:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 993025
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 993026
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->U:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 993027
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->W:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 993028
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->X:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 993029
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->Y:LX/0Px;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v30

    .line 993030
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->aa:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 993031
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ab:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 993032
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ac:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 993033
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ad:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 993034
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ah:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 993035
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ai:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 993036
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->ak:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 993037
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5kN;->al:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 993038
    const/16 v39, 0x40

    move/from16 v0, v39

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 993039
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 993040
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 993041
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 993042
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 993043
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 993044
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993045
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->g:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993046
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->h:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993047
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->i:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993048
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->j:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993049
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->k:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993050
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->l:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993051
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->m:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993052
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->n:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993053
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993054
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->p:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993055
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993056
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 993057
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5kN;->s:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 993058
    const/16 v3, 0x13

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 993059
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->u:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993060
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 993061
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 993062
    const/16 v3, 0x17

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 993063
    const/16 v3, 0x18

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 993064
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5kN;->z:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 993065
    const/16 v3, 0x1a

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 993066
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->B:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993067
    const/16 v3, 0x1c

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 993068
    const/16 v3, 0x1d

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993069
    const/16 v3, 0x1e

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993070
    const/16 v3, 0x1f

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993071
    const/16 v3, 0x20

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993072
    const/16 v3, 0x21

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993073
    const/16 v3, 0x22

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993074
    const/16 v3, 0x23

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993075
    const/16 v3, 0x24

    move-object/from16 v0, p0

    iget v4, v0, LX/5kN;->K:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 993076
    const/16 v3, 0x25

    move-object/from16 v0, p0

    iget v4, v0, LX/5kN;->L:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 993077
    const/16 v3, 0x26

    move-object/from16 v0, p0

    iget v4, v0, LX/5kN;->M:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 993078
    const/16 v3, 0x27

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993079
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->O:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993080
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->P:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993081
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->Q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993082
    const/16 v3, 0x2b

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993083
    const/16 v3, 0x2c

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993084
    const/16 v3, 0x2d

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993085
    const/16 v3, 0x2e

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993086
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5kN;->V:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 993087
    const/16 v3, 0x30

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993088
    const/16 v3, 0x31

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993089
    const/16 v3, 0x32

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993090
    const/16 v3, 0x33

    move-object/from16 v0, p0

    iget v4, v0, LX/5kN;->Z:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 993091
    const/16 v3, 0x34

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993092
    const/16 v3, 0x35

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993093
    const/16 v3, 0x36

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993094
    const/16 v3, 0x37

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993095
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5kN;->ae:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 993096
    const/16 v3, 0x39

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5kN;->af:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 993097
    const/16 v3, 0x3a

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5kN;->ag:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 993098
    const/16 v3, 0x3b

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993099
    const/16 v3, 0x3c

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993100
    const/16 v3, 0x3d

    move-object/from16 v0, p0

    iget v4, v0, LX/5kN;->aj:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 993101
    const/16 v3, 0x3e

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993102
    const/16 v3, 0x3f

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 993103
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 993104
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 993105
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 993106
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 993107
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 993108
    new-instance v3, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;-><init>(LX/15i;)V

    .line 993109
    return-object v3
.end method
