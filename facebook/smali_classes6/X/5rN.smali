.class public LX/5rN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5s9;

.field private final b:LX/5rF;

.field private final c:LX/5rq;

.field private final d:LX/5rl;

.field private final e:LX/5qy;

.field private final f:[I

.field private final g:LX/5pY;

.field private h:D

.field private i:D


# direct methods
.method public constructor <init>(LX/5pY;LX/5rq;LX/5rl;LX/5s9;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1011438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011439
    new-instance v0, LX/5rF;

    invoke-direct {v0}, LX/5rF;-><init>()V

    iput-object v0, p0, LX/5rN;->b:LX/5rF;

    .line 1011440
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, LX/5rN;->f:[I

    .line 1011441
    iput-wide v2, p0, LX/5rN;->h:D

    .line 1011442
    iput-wide v2, p0, LX/5rN;->i:D

    .line 1011443
    iput-object p1, p0, LX/5rN;->g:LX/5pY;

    .line 1011444
    iput-object p2, p0, LX/5rN;->c:LX/5rq;

    .line 1011445
    iput-object p3, p0, LX/5rN;->d:LX/5rl;

    .line 1011446
    new-instance v0, LX/5qy;

    iget-object v1, p0, LX/5rN;->d:LX/5rl;

    iget-object v2, p0, LX/5rN;->b:LX/5rF;

    invoke-direct {v0, v1, v2}, LX/5qy;-><init>(LX/5rl;LX/5rF;)V

    iput-object v0, p0, LX/5rN;->e:LX/5qy;

    .line 1011447
    iput-object p4, p0, LX/5rN;->a:LX/5s9;

    .line 1011448
    return-void
.end method

.method private constructor <init>(LX/5pY;LX/5rq;LX/5s9;)V
    .locals 2

    .prologue
    .line 1011434
    new-instance v0, LX/5rl;

    new-instance v1, LX/5qw;

    invoke-direct {v1, p2}, LX/5qw;-><init>(LX/5rq;)V

    invoke-direct {v0, p1, v1}, LX/5rl;-><init>(LX/5pY;LX/5qw;)V

    invoke-direct {p0, p1, p2, v0, p3}, LX/5rN;-><init>(LX/5pY;LX/5rq;LX/5rl;LX/5s9;)V

    .line 1011435
    return-void
.end method

.method public constructor <init>(LX/5pY;Ljava/util/List;LX/5s9;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;",
            "LX/5s9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1011436
    new-instance v0, LX/5rq;

    invoke-direct {v0, p2}, LX/5rq;-><init>(Ljava/util/List;)V

    invoke-direct {p0, p1, v0, p3}, LX/5rN;-><init>(LX/5pY;LX/5rq;LX/5s9;)V

    .line 1011437
    return-void
.end method

.method private a(II[I)V
    .locals 4

    .prologue
    .line 1011449
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011450
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p2}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    .line 1011451
    if-eqz v1, :cond_0

    if-nez v2, :cond_2

    .line 1011452
    :cond_0
    new-instance v0, LX/5qo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tag "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_1

    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move p1, p2

    goto :goto_0

    .line 1011453
    :cond_2
    if-eq v1, v2, :cond_4

    .line 1011454
    iget-object v0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1011455
    :goto_1
    if-eq v0, v2, :cond_4

    .line 1011456
    if-nez v0, :cond_3

    .line 1011457
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not an ancestor of tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011458
    :cond_3
    iget-object v3, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v3

    .line 1011459
    goto :goto_1

    .line 1011460
    :cond_4
    invoke-direct {p0, v1, v2, p3}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;[I)V

    .line 1011461
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1011462
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1011463
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to execute operation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on view with tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", since the view does not exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011464
    :cond_0
    return-void
.end method

.method private a(I[I)V
    .locals 3

    .prologue
    .line 1011465
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011466
    if-nez v0, :cond_0

    .line 1011467
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No native view for tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exists!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011468
    :cond_0
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v1, v1

    .line 1011469
    if-nez v1, :cond_1

    .line 1011470
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t have a parent!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011471
    :cond_1
    invoke-direct {p0, v0, v1, p2}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;[I)V

    .line 1011472
    return-void
.end method

.method private a(Lcom/facebook/react/uimanager/ReactShadowNode;Lcom/facebook/react/uimanager/ReactShadowNode;[I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1011473
    if-eq p1, p2, :cond_1

    .line 1011474
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1011475
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1011476
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1011477
    move-object v5, v0

    move v0, v2

    move v2, v3

    move-object v3, v5

    .line 1011478
    :goto_0
    if-eq v3, p2, :cond_0

    .line 1011479
    invoke-static {v3}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011480
    invoke-direct {p0, v3}, LX/5rN;->c(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011481
    invoke-virtual {v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/2addr v4, v2

    .line 1011482
    invoke-virtual {v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v2, v0

    .line 1011483
    iget-object v0, v3, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1011484
    move-object v3, v0

    move v0, v2

    move v2, v4

    goto :goto_0

    .line 1011485
    :cond_0
    invoke-direct {p0, p2}, LX/5rN;->c(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011486
    :goto_1
    aput v2, p3, v1

    .line 1011487
    const/4 v1, 0x1

    aput v0, p3, v1

    .line 1011488
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->D()I

    move-result v1

    aput v1, p3, v0

    .line 1011489
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->E()I

    move-result v1

    aput v1, p3, v0

    .line 1011490
    return-void

    :cond_1
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method private b(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 2

    .prologue
    .line 1011491
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->t()V

    .line 1011492
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    .line 1011493
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 1011494
    invoke-virtual {v0, v1}, LX/5rF;->b(I)V

    .line 1011495
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 1011496
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    invoke-direct {p0, v1}, LX/5rN;->b(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011497
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1011498
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->j()V

    .line 1011499
    return-void
.end method

.method private c(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 3

    .prologue
    .line 1011500
    iget-object v0, p0, LX/5rN;->c:LX/5rq;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5rq;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ViewManager;

    .line 1011501
    instance-of v1, v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    if-eqz v1, :cond_0

    .line 1011502
    check-cast v0, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 1011503
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1011504
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to measure a view using measureLayout/measureLayoutRelativeToParent relative to an ancestor that requires custom layout for it\'s children ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Use measure instead."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011505
    :cond_0
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to use view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as a parent, but its Manager doesn\'t extends ViewGroupManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011506
    :cond_1
    return-void
.end method

.method private d(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 2

    .prologue
    .line 1011507
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011508
    :goto_0
    return-void

    .line 1011509
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1011510
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    invoke-direct {p0, v1}, LX/5rN;->d(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011511
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1011512
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->k()V

    goto :goto_0
.end method

.method private e(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 12

    .prologue
    const-wide v10, 0x41cdcd6500000000L    # 1.0E9

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v4, 0x2000

    .line 1011513
    const-string v0, "cssRoot.calculateLayout"

    invoke-static {v4, v5, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v1, "rootTag"

    .line 1011514
    iget v2, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v2

    .line 1011515
    invoke-virtual {v0, v1, v2}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1011516
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-double v0, v0

    .line 1011517
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011518
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1011519
    iget-wide v2, p0, LX/5rN;->i:D

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    long-to-double v4, v4

    sub-double v0, v4, v0

    div-double/2addr v0, v10

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/5rN;->i:D

    .line 1011520
    iget-wide v0, p0, LX/5rN;->h:D

    add-double/2addr v0, v8

    iput-wide v0, p0, LX/5rN;->h:D

    .line 1011521
    return-void

    .line 1011522
    :catchall_0
    move-exception v2

    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1011523
    iget-wide v4, p0, LX/5rN;->i:D

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    long-to-double v6, v6

    sub-double v0, v6, v0

    div-double/2addr v0, v10

    add-double/2addr v0, v4

    iput-wide v0, p0, LX/5rN;->i:D

    .line 1011524
    iget-wide v0, p0, LX/5rN;->h:D

    add-double/2addr v0, v8

    iput-wide v0, p0, LX/5rN;->h:D

    throw v2
.end method


# virtual methods
.method public a()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 1011425
    new-instance v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;-><init>()V

    .line 1011426
    invoke-static {}, LX/5qa;->a()LX/5qa;

    move-result-object v1

    .line 1011427
    iget-object v2, p0, LX/5rN;->g:LX/5pY;

    invoke-virtual {v1, v2}, LX/5qa;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1011428
    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaDirection;)V

    .line 1011429
    :cond_0
    const-string v1, "Root"

    .line 1011430
    iput-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->b:Ljava/lang/String;

    .line 1011431
    return-object v0
.end method

.method public final a(I)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 1011525
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 1011526
    iget-object v0, p0, LX/5rN;->c:LX/5rq;

    invoke-virtual {v0, p1}, LX/5rq;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    .line 1011527
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->s()Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public a(IFFLcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1011528
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/5rl;->a(IFFLcom/facebook/react/bridge/Callback;)V

    .line 1011529
    return-void
.end method

.method public final a(II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1011530
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p2}, LX/5rF;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1011531
    :cond_0
    new-instance v0, LX/5qo;

    const-string v1, "Trying to add or replace a root tag!"

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011532
    :cond_1
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011533
    if-nez v0, :cond_2

    .line 1011534
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to replace unknown view tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011535
    :cond_2
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v1, v1

    .line 1011536
    if-nez v1, :cond_3

    .line 1011537
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Node is not attached to a parent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011538
    :cond_3
    invoke-virtual {v1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;)I

    move-result v0

    .line 1011539
    if-gez v0, :cond_4

    .line 1011540
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Didn\'t find child tag in parent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011541
    :cond_4
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v4

    .line 1011542
    invoke-interface {v4, p2}, LX/5pD;->pushInt(I)V

    .line 1011543
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v5

    .line 1011544
    invoke-interface {v5, v0}, LX/5pD;->pushInt(I)V

    .line 1011545
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v6

    .line 1011546
    invoke-interface {v6, v0}, LX/5pD;->pushInt(I)V

    .line 1011547
    iget v0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v0

    .line 1011548
    move-object v0, p0

    move-object v3, v2

    invoke-virtual/range {v0 .. v6}, LX/5rN;->a(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V

    .line 1011549
    return-void
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 1011550
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011551
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(F)V

    .line 1011552
    int-to-float v1, p3

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(F)V

    .line 1011553
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0}, LX/5rl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011554
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/5rN;->d(I)V

    .line 1011555
    :cond_0
    return-void
.end method

.method public a(IILX/5pC;)V
    .locals 1

    .prologue
    .line 1011556
    const-string v0, "dispatchViewManagerCommand"

    invoke-direct {p0, p1, v0}, LX/5rN;->a(ILjava/lang/String;)V

    .line 1011557
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2, p3}, LX/5rl;->a(IILX/5pC;)V

    .line 1011558
    return-void
.end method

.method public final a(IILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1011559
    :try_start_0
    iget-object v0, p0, LX/5rN;->f:[I

    invoke-direct {p0, p1, p2, v0}, LX/5rN;->a(II[I)V

    .line 1011560
    iget-object v0, p0, LX/5rN;->f:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 1011561
    iget-object v1, p0, LX/5rN;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 1011562
    iget-object v2, p0, LX/5rN;->f:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 1011563
    iget-object v3, p0, LX/5rN;->f:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 1011564
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {p4, v4}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch LX/5qo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011565
    :goto_0
    return-void

    .line 1011566
    :catch_0
    move-exception v0

    .line 1011567
    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/5qo;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-interface {p3, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(ILX/5pC;)V
    .locals 4

    .prologue
    .line 1011568
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011569
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1011570
    iget-object v2, p0, LX/5rN;->b:LX/5rF;

    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    .line 1011571
    if-nez v2, :cond_0

    .line 1011572
    new-instance v1, LX/5qo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to add unknown view tag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1011573
    :cond_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1011574
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1011575
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1011576
    iget-object v0, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {v0, v1, p2}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5pC;)V

    .line 1011577
    :cond_2
    return-void
.end method

.method public a(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V
    .locals 14
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1011578
    iget-object v1, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v1, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v5

    .line 1011579
    if-nez p2, :cond_1

    const/4 v1, 0x0

    move v4, v1

    .line 1011580
    :goto_0
    if-nez p4, :cond_2

    const/4 v1, 0x0

    move v3, v1

    .line 1011581
    :goto_1
    if-nez p6, :cond_3

    const/4 v1, 0x0

    move v2, v1

    .line 1011582
    :goto_2
    if-eqz v4, :cond_4

    if-eqz p3, :cond_0

    invoke-interface/range {p3 .. p3}, LX/5pC;->size()I

    move-result v1

    if-eq v4, v1, :cond_4

    .line 1011583
    :cond_0
    new-instance v1, LX/5qo;

    const-string v2, "Size of moveFrom != size of moveTo!"

    invoke-direct {v1, v2}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1011584
    :cond_1
    invoke-interface/range {p2 .. p2}, LX/5pC;->size()I

    move-result v1

    move v4, v1

    goto :goto_0

    .line 1011585
    :cond_2
    invoke-interface/range {p4 .. p4}, LX/5pC;->size()I

    move-result v1

    move v3, v1

    goto :goto_1

    .line 1011586
    :cond_3
    invoke-interface/range {p6 .. p6}, LX/5pC;->size()I

    move-result v1

    move v2, v1

    goto :goto_2

    .line 1011587
    :cond_4
    if-eqz v3, :cond_6

    if-eqz p5, :cond_5

    invoke-interface/range {p5 .. p5}, LX/5pC;->size()I

    move-result v1

    if-eq v3, v1, :cond_6

    .line 1011588
    :cond_5
    new-instance v1, LX/5qo;

    const-string v2, "Size of addChildTags != size of addAtIndices!"

    invoke-direct {v1, v2}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1011589
    :cond_6
    add-int v1, v4, v3

    new-array v6, v1, [LX/5rn;

    .line 1011590
    add-int v1, v4, v2

    new-array v7, v1, [I

    .line 1011591
    array-length v1, v7

    new-array v8, v1, [I

    .line 1011592
    new-array v9, v2, [I

    .line 1011593
    if-lez v4, :cond_7

    .line 1011594
    invoke-static/range {p2 .. p2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011595
    invoke-static/range {p3 .. p3}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011596
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v4, :cond_7

    .line 1011597
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, LX/5pC;->getInt(I)I

    move-result v10

    .line 1011598
    invoke-virtual {v5, v10}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v11

    .line 1011599
    new-instance v12, LX/5rn;

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, LX/5pC;->getInt(I)I

    move-result v13

    invoke-direct {v12, v11, v13}, LX/5rn;-><init>(II)V

    aput-object v12, v6, v1

    .line 1011600
    aput v10, v7, v1

    .line 1011601
    aput v11, v8, v1

    .line 1011602
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1011603
    :cond_7
    if-lez v3, :cond_8

    .line 1011604
    invoke-static/range {p4 .. p4}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011605
    invoke-static/range {p5 .. p5}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011606
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v3, :cond_8

    .line 1011607
    move-object/from16 v0, p4

    invoke-interface {v0, v1}, LX/5pC;->getInt(I)I

    move-result v10

    .line 1011608
    move-object/from16 v0, p5

    invoke-interface {v0, v1}, LX/5pC;->getInt(I)I

    move-result v11

    .line 1011609
    add-int v12, v4, v1

    new-instance v13, LX/5rn;

    invoke-direct {v13, v10, v11}, LX/5rn;-><init>(II)V

    aput-object v13, v6, v12

    .line 1011610
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1011611
    :cond_8
    if-lez v2, :cond_9

    .line 1011612
    invoke-static/range {p6 .. p6}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011613
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_9

    .line 1011614
    move-object/from16 v0, p6

    invoke-interface {v0, v1}, LX/5pC;->getInt(I)I

    move-result v3

    .line 1011615
    invoke-virtual {v5, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v10

    .line 1011616
    add-int v11, v4, v1

    aput v3, v7, v11

    .line 1011617
    add-int v3, v4, v1

    aput v10, v8, v3

    .line 1011618
    aput v10, v9, v1

    .line 1011619
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1011620
    :cond_9
    sget-object v1, LX/5rn;->a:Ljava/util/Comparator;

    invoke-static {v6, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1011621
    invoke-static {v7}, Ljava/util/Arrays;->sort([I)V

    .line 1011622
    const/4 v2, -0x1

    .line 1011623
    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    :goto_6
    if-ltz v1, :cond_b

    .line 1011624
    aget v3, v7, v1

    .line 1011625
    if-ne v3, v2, :cond_a

    .line 1011626
    new-instance v1, LX/5qo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Repeated indices in Removal list for view tag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1011627
    :cond_a
    aget v2, v7, v1

    invoke-virtual {v5, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1011628
    aget v2, v7, v1

    .line 1011629
    add-int/lit8 v1, v1, -0x1

    goto :goto_6

    .line 1011630
    :cond_b
    const/4 v1, 0x0

    :goto_7
    array-length v2, v6

    if-ge v1, v2, :cond_d

    .line 1011631
    aget-object v2, v6, v1

    .line 1011632
    iget-object v3, p0, LX/5rN;->b:LX/5rF;

    iget v4, v2, LX/5rn;->b:I

    invoke-virtual {v3, v4}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v3

    .line 1011633
    if-nez v3, :cond_c

    .line 1011634
    new-instance v1, LX/5qo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to add unknown view tag: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, LX/5rn;->b:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1011635
    :cond_c
    iget v2, v2, LX/5rn;->c:I

    invoke-virtual {v5, v3, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 1011636
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1011637
    :cond_d
    invoke-virtual {v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->b()Z

    move-result v1

    if-nez v1, :cond_e

    .line 1011638
    iget-object v1, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {v1, v5, v8, v6, v9}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;[I[LX/5rn;[I)V

    .line 1011639
    :cond_e
    const/4 v1, 0x0

    :goto_8
    array-length v2, v9

    if-ge v1, v2, :cond_f

    .line 1011640
    iget-object v2, p0, LX/5rN;->b:LX/5rF;

    aget v3, v9, v1

    invoke-virtual {v2, v3}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011641
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1011642
    :cond_f
    return-void
.end method

.method public a(ILX/5pC;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1011643
    const-string v0, "showPopupMenu"

    invoke-direct {p0, p1, v0}, LX/5rN;->a(ILjava/lang/String;)V

    .line 1011644
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2, p4}, LX/5rl;->a(ILX/5pC;Lcom/facebook/react/bridge/Callback;)V

    .line 1011645
    return-void
.end method

.method public final a(ILX/5rC;)V
    .locals 1

    .prologue
    .line 1011646
    invoke-static {}, LX/5pe;->b()V

    .line 1011647
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    .line 1011648
    iget-object p0, v0, LX/5rl;->b:LX/5qw;

    move-object v0, p0

    .line 1011649
    invoke-virtual {v0, p1, p2}, LX/5qw;->a(ILX/5rC;)V

    .line 1011650
    return-void
.end method

.method public a(ILcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1011432
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2}, LX/5rl;->a(ILcom/facebook/react/bridge/Callback;)V

    .line 1011433
    return-void
.end method

.method public final a(ILcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1011379
    :try_start_0
    iget-object v0, p0, LX/5rN;->f:[I

    invoke-direct {p0, p1, v0}, LX/5rN;->a(I[I)V

    .line 1011380
    iget-object v0, p0, LX/5rN;->f:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 1011381
    iget-object v1, p0, LX/5rN;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 1011382
    iget-object v2, p0, LX/5rN;->f:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 1011383
    iget-object v3, p0, LX/5rN;->f:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 1011384
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-interface {p3, v4}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch LX/5qo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011385
    :goto_0
    return-void

    .line 1011386
    :catch_0
    move-exception v0

    .line 1011387
    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/5qo;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;ILX/5pG;)V
    .locals 2

    .prologue
    .line 1011366
    invoke-virtual {p0, p2}, LX/5rN;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011367
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p3}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011368
    invoke-virtual {v1, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(I)V

    .line 1011369
    iput-object p2, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->b:Ljava/lang/String;

    .line 1011370
    iput-object v0, v1, Lcom/facebook/react/uimanager/ReactShadowNode;->c:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1011371
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rJ;)V

    .line 1011372
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, v1}, LX/5rF;->b(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011373
    const/4 v0, 0x0

    .line 1011374
    if-eqz p4, :cond_0

    .line 1011375
    new-instance v0, LX/5rC;

    invoke-direct {v0, p4}, LX/5rC;-><init>(LX/5pG;)V

    .line 1011376
    invoke-static {v1, v0}, LX/5rp;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V

    .line 1011377
    :cond_0
    invoke-virtual {p0, v1, p3, v0}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;ILX/5rC;)V

    .line 1011378
    return-void
.end method

.method public final a(ILjava/lang/String;LX/5pG;)V
    .locals 3

    .prologue
    .line 1011355
    iget-object v0, p0, LX/5rN;->c:LX/5rq;

    invoke-virtual {v0, p2}, LX/5rq;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    .line 1011356
    if-nez v0, :cond_0

    .line 1011357
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011358
    :cond_0
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011359
    if-nez v0, :cond_1

    .line 1011360
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to update non-existent view with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011361
    :cond_1
    if-eqz p3, :cond_2

    .line 1011362
    new-instance v1, LX/5rC;

    invoke-direct {v1, p3}, LX/5rC;-><init>(LX/5pG;)V

    .line 1011363
    invoke-static {v0, v1}, LX/5rp;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V

    .line 1011364
    invoke-virtual {p0, v0, p2, v1}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V

    .line 1011365
    :cond_2
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 1011344
    const-string v0, "setJSResponder"

    invoke-direct {p0, p1, v0}, LX/5rN;->a(ILjava/lang/String;)V

    .line 1011345
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011346
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1011347
    iget-boolean v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    move v1, v1

    .line 1011348
    if-eqz v1, :cond_1

    .line 1011349
    :cond_0
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v1

    .line 1011350
    goto :goto_0

    .line 1011351
    :cond_1
    iget-object v1, p0, LX/5rN;->d:LX/5rl;

    .line 1011352
    iget p0, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, p0

    .line 1011353
    invoke-virtual {v1, v0, p1, p2}, LX/5rl;->a(IIZ)V

    .line 1011354
    return-void
.end method

.method public final a(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1011342
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1}, LX/5rl;->a(LX/5pG;)V

    .line 1011343
    return-void
.end method

.method public final a(LX/5qU;)V
    .locals 1
    .param p1    # LX/5qU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1011339
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    .line 1011340
    iput-object p1, v0, LX/5rl;->k:LX/5qU;

    .line 1011341
    return-void
.end method

.method public final a(LX/5rH;IIILX/5rJ;)V
    .locals 2

    .prologue
    .line 1011298
    invoke-virtual {p0}, LX/5rN;->a()Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011299
    invoke-virtual {v0, p2}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(I)V

    .line 1011300
    invoke-virtual {v0, p5}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rJ;)V

    .line 1011301
    int-to-float v1, p3

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(F)V

    .line 1011302
    int-to-float v1, p4

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(F)V

    .line 1011303
    iget-object v1, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v1, v0}, LX/5rF;->a(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011304
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p2, p1, p5}, LX/5rl;->a(ILX/5rH;LX/5rJ;)V

    .line 1011305
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 0

    .prologue
    .line 1011306
    invoke-direct {p0, p1}, LX/5rN;->b(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011307
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->G()V

    .line 1011308
    return-void
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;FF)V
    .locals 6

    .prologue
    .line 1011312
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011313
    :goto_0
    return-void

    .line 1011314
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1011315
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1011316
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v2

    add-float/2addr v2, p2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v3

    add-float/2addr v3, p3

    invoke-virtual {p0, v1, v2, v3}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;FF)V

    .line 1011317
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1011318
    :cond_1
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 1011319
    iget-object v1, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v1, v0}, LX/5rF;->d(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1011320
    iget-object v1, p0, LX/5rN;->d:LX/5rl;

    iget-object v2, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(FFLX/5rl;LX/5qy;)Z

    move-result v1

    .line 1011321
    if-eqz v1, :cond_2

    .line 1011322
    iget-boolean v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->e:Z

    move v1, v1

    .line 1011323
    if-eqz v1, :cond_2

    .line 1011324
    iget-object v1, p0, LX/5rN;->a:LX/5s9;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->B()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->C()I

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->D()I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->E()I

    move-result v5

    invoke-static {v0, v2, v3, v4, v5}, LX/5r1;->a(IIIII)LX/5r1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/5s9;->a(LX/5r0;)V

    .line 1011325
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    goto :goto_0
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;ILX/5rC;)V
    .locals 2
    .param p3    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1011326
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011327
    iget-object v0, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p3}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rJ;LX/5rC;)V

    .line 1011328
    :cond_0
    return-void
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 1

    .prologue
    .line 1011309
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011310
    iget-object v0, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {v0, p1, p2, p3}, LX/5qy;->a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V

    .line 1011311
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1011388
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1}, LX/5rl;->a(Z)V

    .line 1011389
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;
    .locals 1

    .prologue
    .line 1011390
    iget-object v0, p0, LX/5rN;->c:LX/5rq;

    invoke-virtual {v0, p1}, LX/5rq;->a(Ljava/lang/String;)Lcom/facebook/react/uimanager/ViewManager;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1011391
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v1}, LX/5rF;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1011392
    iget-object v1, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v1, v0}, LX/5rF;->e(I)I

    move-result v1

    .line 1011393
    iget-object v2, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v2, v1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011394
    invoke-direct {p0, v1}, LX/5rN;->d(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011395
    invoke-direct {p0, v1}, LX/5rN;->e(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1011396
    invoke-virtual {p0, v1, v3, v3}, LX/5rN;->a(Lcom/facebook/react/uimanager/ReactShadowNode;FF)V

    .line 1011397
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1011398
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1011399
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->a(I)V

    .line 1011400
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1}, LX/5rl;->a(I)V

    .line 1011401
    return-void
.end method

.method public b(II)V
    .locals 1

    .prologue
    .line 1011402
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2}, LX/5rl;->a(II)V

    .line 1011403
    return-void
.end method

.method public b(ILcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1011404
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1, p2}, LX/5rl;->b(ILcom/facebook/react/bridge/Callback;)V

    .line 1011405
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1011406
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0}, LX/5rl;->c()V

    .line 1011407
    return-void
.end method

.method public final c(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1011408
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->c(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011409
    if-nez v1, :cond_0

    .line 1011410
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to remove subviews of an unknown view tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011411
    :cond_0
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v6

    .line 1011412
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1011413
    invoke-interface {v6, v0}, LX/5pD;->pushInt(I)V

    .line 1011414
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, p0

    move v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 1011415
    invoke-virtual/range {v0 .. v6}, LX/5rN;->a(ILX/5pC;LX/5pC;LX/5pC;LX/5pC;LX/5pC;)V

    .line 1011416
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1011417
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0}, LX/5rl;->d()V

    .line 1011418
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 1011421
    invoke-virtual {p0}, LX/5rN;->b()V

    .line 1011422
    iget-object v0, p0, LX/5rN;->e:LX/5qy;

    invoke-virtual {v0}, LX/5qy;->a()V

    .line 1011423
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0, p1}, LX/5rl;->b(I)V

    .line 1011424
    return-void
.end method

.method public final e(I)I
    .locals 4

    .prologue
    .line 1011329
    iget-object v0, p0, LX/5rN;->b:LX/5rF;

    invoke-virtual {v0, p1}, LX/5rF;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011330
    :goto_0
    return p1

    .line 1011331
    :cond_0
    invoke-virtual {p0, p1}, LX/5rN;->a(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 1011332
    const/4 v0, 0x0

    .line 1011333
    if-eqz v1, :cond_1

    .line 1011334
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->m()Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1011335
    iget v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v1

    .line 1011336
    :goto_1
    move p1, v0

    .line 1011337
    goto :goto_0

    .line 1011338
    :cond_1
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Warning : attempted to resolve a non-existent react shadow node. reactTag="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1011419
    iget-object v0, p0, LX/5rN;->d:LX/5rl;

    invoke-virtual {v0}, LX/5rl;->e()V

    .line 1011420
    return-void
.end method
