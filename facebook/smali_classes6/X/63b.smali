.class public LX/63b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/23P;

.field private final b:Ljava/lang/String;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILX/23P;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1043415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043416
    iput-object p2, p0, LX/63b;->a:LX/23P;

    .line 1043417
    invoke-virtual {p3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/63b;->b:Ljava/lang/String;

    .line 1043418
    return-void
.end method

.method private a(Z)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1043405
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1043406
    iput-boolean p1, v0, LX/108;->d:Z

    .line 1043407
    move-object v0, v0

    .line 1043408
    const/4 v1, -0x2

    .line 1043409
    iput v1, v0, LX/108;->h:I

    .line 1043410
    move-object v0, v0

    .line 1043411
    iget-object v1, p0, LX/63b;->a:LX/23P;

    iget-object v2, p0, LX/63b;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1043412
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1043413
    move-object v0, v0

    .line 1043414
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1043402
    iget-object v0, p0, LX/63b;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 1043403
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/63b;->a(Z)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/63b;->c:LX/0Px;

    .line 1043404
    :cond_0
    iget-object v0, p0, LX/63b;->c:LX/0Px;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1043399
    iget-object v0, p0, LX/63b;->d:LX/0Px;

    if-nez v0, :cond_0

    .line 1043400
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/63b;->a(Z)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/63b;->d:LX/0Px;

    .line 1043401
    :cond_0
    iget-object v0, p0, LX/63b;->d:LX/0Px;

    return-object v0
.end method
