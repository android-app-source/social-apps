.class public final enum LX/6eR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6eR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6eR;

.field public static final enum CORRUPTED:LX/6eR;

.field public static final enum INACCESSIBLE:LX/6eR;

.field public static final enum NONEXISTENT:LX/6eR;

.field public static final enum VALID:LX/6eR;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1117907
    new-instance v0, LX/6eR;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v2}, LX/6eR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eR;->VALID:LX/6eR;

    .line 1117908
    new-instance v0, LX/6eR;

    const-string v1, "NONEXISTENT"

    invoke-direct {v0, v1, v3}, LX/6eR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eR;->NONEXISTENT:LX/6eR;

    .line 1117909
    new-instance v0, LX/6eR;

    const-string v1, "INACCESSIBLE"

    invoke-direct {v0, v1, v4}, LX/6eR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eR;->INACCESSIBLE:LX/6eR;

    .line 1117910
    new-instance v0, LX/6eR;

    const-string v1, "CORRUPTED"

    invoke-direct {v0, v1, v5}, LX/6eR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eR;->CORRUPTED:LX/6eR;

    .line 1117911
    const/4 v0, 0x4

    new-array v0, v0, [LX/6eR;

    sget-object v1, LX/6eR;->VALID:LX/6eR;

    aput-object v1, v0, v2

    sget-object v1, LX/6eR;->NONEXISTENT:LX/6eR;

    aput-object v1, v0, v3

    sget-object v1, LX/6eR;->INACCESSIBLE:LX/6eR;

    aput-object v1, v0, v4

    sget-object v1, LX/6eR;->CORRUPTED:LX/6eR;

    aput-object v1, v0, v5

    sput-object v0, LX/6eR;->$VALUES:[LX/6eR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1117912
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6eR;
    .locals 1

    .prologue
    .line 1117913
    const-class v0, LX/6eR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6eR;

    return-object v0
.end method

.method public static values()[LX/6eR;
    .locals 1

    .prologue
    .line 1117914
    sget-object v0, LX/6eR;->$VALUES:[LX/6eR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6eR;

    return-object v0
.end method
