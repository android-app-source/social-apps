.class public LX/6Cp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Co;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6CH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064359
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1064360
    iput-object v0, p0, LX/6Cp;->a:LX/0Ot;

    .line 1064361
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1064362
    iput-object v0, p0, LX/6Cp;->b:LX/0Ot;

    .line 1064363
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1064364
    iput-object v0, p0, LX/6Cp;->c:LX/0Ot;

    .line 1064365
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1064366
    iput-object v0, p0, LX/6Cp;->d:LX/0Ot;

    .line 1064367
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/6Cp;->f:Ljava/util/Set;

    .line 1064368
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1064342
    iget-object v0, p0, LX/6Cp;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1064343
    iget-object v0, p0, LX/6Cp;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ck;

    iget-object v1, p0, LX/6Cp;->e:Ljava/lang/String;

    .line 1064344
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1064345
    new-instance v3, LX/6Cm;

    invoke-direct {v3, p0, p1}, LX/6Cm;-><init>(LX/6Cp;Ljava/lang/String;)V

    .line 1064346
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064347
    new-instance v4, LX/4HI;

    invoke-direct {v4}, LX/4HI;-><init>()V

    invoke-virtual {v4, v1}, LX/4HI;->b(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/4HI;->c(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    const-string p0, "ADD_FAVORITE"

    invoke-virtual {v4, p0}, LX/4HI;->a(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    .line 1064348
    invoke-static {v0, v4, v2, v3}, LX/6Ck;->a(LX/6Ck;LX/4HI;Ljava/util/concurrent/Executor;LX/6Cj;)V

    .line 1064349
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1064350
    iget-object v0, p0, LX/6Cp;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1064351
    iget-object v0, p0, LX/6Cp;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ck;

    iget-object v1, p0, LX/6Cp;->e:Ljava/lang/String;

    .line 1064352
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1064353
    new-instance v3, LX/6Cn;

    invoke-direct {v3, p0, p1}, LX/6Cn;-><init>(LX/6Cp;Ljava/lang/String;)V

    .line 1064354
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064355
    new-instance v4, LX/4HI;

    invoke-direct {v4}, LX/4HI;-><init>()V

    invoke-virtual {v4, v1}, LX/4HI;->b(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/4HI;->c(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    const-string p0, "REMOVE_FAVORITE"

    invoke-virtual {v4, p0}, LX/4HI;->a(Ljava/lang/String;)LX/4HI;

    move-result-object v4

    .line 1064356
    invoke-static {v0, v4, v2, v3}, LX/6Ck;->a(LX/6Ck;LX/4HI;Ljava/util/concurrent/Executor;LX/6Cj;)V

    .line 1064357
    return-void
.end method


# virtual methods
.method public final a()LX/6Eo;
    .locals 1

    .prologue
    .line 1064341
    sget-object v0, LX/6Eo;->MESSNEGER_FAVORITE:LX/6Eo;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1064330
    const-string v0, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6Cp;->e:Ljava/lang/String;

    .line 1064331
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/6Cp;->f:Ljava/util/Set;

    .line 1064332
    iget-object v0, p0, LX/6Cp;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ck;

    iget-object v1, p0, LX/6Cp;->e:Ljava/lang/String;

    new-instance v2, LX/6Cl;

    invoke-direct {v2, p0}, LX/6Cl;-><init>(LX/6Cp;)V

    .line 1064333
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064334
    iget-object v3, v0, LX/6Ck;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/6Ck;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v3}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1064335
    :cond_0
    :goto_0
    new-instance v3, LX/6Cb;

    invoke-direct {v3}, LX/6Cb;-><init>()V

    move-object v3, v3

    .line 1064336
    const-string p0, "page_id"

    invoke-virtual {v3, p0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1064337
    iget-object p0, v0, LX/6Ck;->c:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    iput-object v3, v0, LX/6Ck;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1064338
    iget-object v3, v0, LX/6Ck;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p0, LX/6Ch;

    invoke-direct {p0, v0, v2}, LX/6Ch;-><init>(LX/6Ck;LX/6Cl;)V

    iget-object p1, v0, LX/6Ck;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1064339
    return-void

    .line 1064340
    :cond_1
    iget-object v3, v0, LX/6Ck;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 p0, 0x1

    invoke-interface {v3, p0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1064316
    iget-object v0, p0, LX/6Cp;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1064317
    :goto_0
    return-void

    .line 1064318
    :cond_0
    iget-object v0, p0, LX/6Cp;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 1064319
    :goto_1
    if-eqz v1, :cond_2

    .line 1064320
    invoke-direct {p0, p1}, LX/6Cp;->a(Ljava/lang/String;)V

    .line 1064321
    :goto_2
    iget-object v0, p0, LX/6Cp;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CH;

    iget-object v2, p0, LX/6Cp;->e:Ljava/lang/String;

    .line 1064322
    if-eqz v1, :cond_3

    const-string p0, "browser_extension_favorite_clicked"

    .line 1064323
    :goto_3
    const/4 p2, 0x0

    invoke-static {v0, p0, p2}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object p0

    .line 1064324
    if-nez p0, :cond_4

    .line 1064325
    :goto_4
    goto :goto_0

    .line 1064326
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 1064327
    :cond_2
    invoke-direct {p0, p1}, LX/6Cp;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 1064328
    :cond_3
    const-string p0, "browser_extension_favorite_unclicked"

    goto :goto_3

    .line 1064329
    :cond_4
    const-string p2, "page_id"

    invoke-virtual {p0, p2, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p2, "website_url"

    invoke-virtual {p0, p2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0}, LX/0oG;->d()V

    goto :goto_4
.end method
