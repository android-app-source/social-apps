.class public LX/6Kr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Kf;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/6Kq;

.field public final b:Landroid/view/Choreographer;

.field public c:LX/6Km;

.field public d:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public volatile f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1077536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077537
    iput-boolean v1, p0, LX/6Kr;->f:Z

    .line 1077538
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1077539
    if-nez v0, :cond_0

    .line 1077540
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Vsync rendering is not supported on this version of android"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077541
    :cond_0
    new-instance v0, LX/6Kq;

    invoke-direct {v0, p0}, LX/6Kq;-><init>(LX/6Kr;)V

    iput-object v0, p0, LX/6Kr;->a:LX/6Kq;

    .line 1077542
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, LX/6Kr;->b:Landroid/view/Choreographer;

    .line 1077543
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1077544
    return-void
.end method

.method public final a(LX/6Km;)V
    .locals 2

    .prologue
    .line 1077526
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Kr;->f:Z

    .line 1077527
    iput-object p1, p0, LX/6Kr;->c:LX/6Km;

    .line 1077528
    iget-object v0, p0, LX/6Kr;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/6Kr;->a:LX/6Kq;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1077529
    return-void
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 4
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1077530
    if-nez p1, :cond_0

    .line 1077531
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Kr;->d:Ljava/lang/Long;

    .line 1077532
    :goto_0
    return-void

    .line 1077533
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1077534
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target FPS must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077535
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/6Kr;->d:Ljava/lang/Long;

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1077522
    iget-object v0, p0, LX/6Kr;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/6Kr;->a:LX/6Kq;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1077523
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1077524
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Kr;->f:Z

    .line 1077525
    return-void
.end method
