.class public LX/5JH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:Ljava/util/concurrent/Semaphore;


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/01D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:[LX/5JE;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 896912
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, LX/5JH;->e:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method public constructor <init>(LX/5JD;)V
    .locals 2

    .prologue
    .line 896904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 896905
    iget-object v0, p1, LX/5JD;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/5JH;->b:Landroid/content/Context;

    .line 896906
    iget-object v0, p1, LX/5JD;->b:Ljava/io/File;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/5JH;->a:Ljava/io/File;

    .line 896907
    iget-object v0, p1, LX/5JD;->c:Ljava/util/ArrayList;

    iget-object v1, p1, LX/5JD;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LX/5JE;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5JE;

    iput-object v0, p0, LX/5JH;->g:[LX/5JE;

    .line 896908
    iget-boolean v0, p1, LX/5JD;->d:Z

    iput-boolean v0, p0, LX/5JH;->c:Z

    .line 896909
    iget-object v0, p1, LX/5JD;->e:Ljava/lang/Runnable;

    iput-object v0, p0, LX/5JH;->d:Ljava/lang/Runnable;

    .line 896910
    const/4 v0, 0x0

    iput-object v0, p0, LX/5JH;->f:LX/01D;

    .line 896911
    return-void
.end method

.method public static a(Ljava/io/OutputStream;Ljava/io/InputStream;[BI)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 896896
    move v0, v1

    .line 896897
    :goto_0
    if-ge v0, p3, :cond_0

    .line 896898
    sub-int v2, p3, v0

    array-length v3, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p1, p2, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 896899
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 896900
    invoke-virtual {p0, p2, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 896901
    add-int/2addr v0, v2

    .line 896902
    goto :goto_0

    .line 896903
    :cond_0
    return v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 7

    .prologue
    const-wide v4, 0x80000000L

    .line 896886
    const-string v0, "AppUnpacker.fsync"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 896887
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v2, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 896888
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 896889
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 896890
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 896891
    return-void

    .line 896892
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 896893
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 896894
    :catchall_1
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0

    .line 896895
    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;[BI)[B
    .locals 1

    .prologue
    .line 896913
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 896914
    invoke-static {v0, p0, p1, p2}, LX/5JH;->a(Ljava/io/OutputStream;Ljava/io/InputStream;[BI)I

    .line 896915
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/5JH;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 896864
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/5JH;->a:Ljava/io/File;

    const-string v4, ".unpacked"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 896865
    iget-object v3, p0, LX/5JH;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 896866
    :goto_0
    const/16 v3, 0x4000

    new-array v4, v3, [B

    move v3, v0

    move v0, v1

    .line 896867
    :goto_1
    iget-object v5, p0, LX/5JH;->g:[LX/5JE;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    if-nez v3, :cond_2

    .line 896868
    iget-object v3, p0, LX/5JH;->g:[LX/5JE;

    aget-object v3, v3, v0

    iget-object v5, p0, LX/5JH;->b:Landroid/content/Context;

    invoke-virtual {v3, v5, v4}, LX/5JE;->a(Landroid/content/Context;[B)Z

    move-result v3

    .line 896869
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 896870
    goto :goto_0

    .line 896871
    :cond_2
    if-nez v3, :cond_3

    .line 896872
    :goto_2
    return v1

    .line 896873
    :cond_3
    :try_start_0
    iget-object v0, p0, LX/5JH;->a:Ljava/io/File;

    invoke-static {v0}, LX/01z;->a(Ljava/io/File;)V

    .line 896874
    iget-object v0, p0, LX/5JH;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_4

    .line 896875
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Coult not create the destination directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 896876
    :catchall_0
    move-exception v0

    .line 896877
    iget-object v1, p0, LX/5JH;->a:Ljava/io/File;

    invoke-static {v1}, LX/01z;->a(Ljava/io/File;)V

    throw v0

    .line 896878
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/5JH;->g:[LX/5JE;

    array-length v3, v0

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v5, v0, v1

    .line 896879
    iget-object v6, p0, LX/5JH;->b:Landroid/content/Context;

    invoke-virtual {v5, v6, v4}, LX/5JE;->b(Landroid/content/Context;[B)V

    .line 896880
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 896881
    :cond_5
    iget-boolean v0, p0, LX/5JH;->c:Z

    if-eqz v0, :cond_6

    .line 896882
    new-instance v0, Lcom/facebook/common/appunpacker/AppUnpacker$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/appunpacker/AppUnpacker$1;-><init>(LX/5JH;)V

    const v1, -0x3932ccdc

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 896883
    :goto_4
    move v1, v2

    .line 896884
    goto :goto_2

    .line 896885
    :cond_6
    invoke-static {p0}, LX/5JH;->c(LX/5JH;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method

.method public static c(LX/5JH;)V
    .locals 4

    .prologue
    .line 896857
    iget-object v1, p0, LX/5JH;->g:[LX/5JE;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 896858
    invoke-virtual {v3}, LX/5JE;->a()V

    .line 896859
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 896860
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/5JH;->a:Ljava/io/File;

    const-string v2, ".unpacked"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 896861
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-nez v0, :cond_1

    .line 896862
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not create .unpacked file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 896863
    :cond_1
    return-void
.end method

.method private static e(LX/5JH;)V
    .locals 6

    .prologue
    const-wide v4, 0x80000000L

    .line 896847
    const-string v0, "AppUnpacker.lock"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 896848
    :try_start_0
    sget-object v0, LX/5JH;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 896849
    :try_start_1
    iget-object v0, p0, LX/5JH;->f:LX/01D;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 896850
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/5JH;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "appunpacker.lock"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, LX/01D;->a(Ljava/io/File;)LX/01D;

    move-result-object v0

    iput-object v0, p0, LX/5JH;->f:LX/01D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 896851
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 896852
    return-void

    .line 896853
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 896854
    :catchall_0
    move-exception v0

    .line 896855
    :try_start_2
    sget-object v1, LX/5JH;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 896856
    :catchall_1
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method

.method public static f(LX/5JH;)V
    .locals 1

    .prologue
    .line 896843
    iget-object v0, p0, LX/5JH;->f:LX/01D;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/01D;

    invoke-virtual {v0}, LX/01D;->close()V

    .line 896844
    const/4 v0, 0x0

    iput-object v0, p0, LX/5JH;->f:LX/01D;

    .line 896845
    sget-object v0, LX/5JH;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 896846
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 896829
    :try_start_0
    invoke-static {p0}, LX/5JH;->e(LX/5JH;)V

    .line 896830
    const-wide v0, 0x80000000L

    const-string v2, "AppUnpacker.unpack()"

    invoke-static {v0, v1, v2}, LX/018;->a(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 896831
    :try_start_1
    invoke-static {p0}, LX/5JH;->b(LX/5JH;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 896832
    const-wide v2, 0x80000000L

    :try_start_2
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 896833
    iget-boolean v1, p0, LX/5JH;->c:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 896834
    :cond_0
    invoke-static {p0}, LX/5JH;->f(LX/5JH;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 896835
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/5JH;->d:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 896836
    iget-object v1, p0, LX/5JH;->d:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 896837
    :cond_2
    return v0

    .line 896838
    :catchall_0
    move-exception v0

    const-wide v2, 0x80000000L

    :try_start_3
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 896839
    invoke-static {p0}, LX/5JH;->f(LX/5JH;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 896840
    :catch_0
    move-exception v0

    .line 896841
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 896842
    :catch_1
    move-exception v0

    goto :goto_0
.end method
