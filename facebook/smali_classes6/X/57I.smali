.class public final LX/57I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 841188
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 841189
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841190
    :goto_0
    return v1

    .line 841191
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841192
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 841193
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 841194
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 841195
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 841196
    const-string v6, "button_icon_image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 841197
    invoke-static {p0, p1}, LX/57H;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 841198
    :cond_2
    const-string v6, "button_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 841199
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 841200
    :cond_3
    const-string v6, "button_uri"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 841201
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 841202
    :cond_4
    const-string v6, "render_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 841203
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 841204
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 841205
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 841206
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 841207
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 841208
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 841209
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 841169
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 841170
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841171
    if-eqz v0, :cond_0

    .line 841172
    const-string v1, "button_icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841173
    invoke-static {p0, v0, p2, p3}, LX/57H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 841174
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 841175
    if-eqz v0, :cond_1

    .line 841176
    const-string v0, "button_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841177
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841178
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841179
    if-eqz v0, :cond_2

    .line 841180
    const-string v1, "button_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841181
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841182
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 841183
    if-eqz v0, :cond_3

    .line 841184
    const-string v0, "render_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841185
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841186
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 841187
    return-void
.end method
