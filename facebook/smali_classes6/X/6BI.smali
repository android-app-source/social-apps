.class public LX/6BI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/ufiservices/common/EditCommentParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1061817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061818
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1061819
    check-cast p1, Lcom/facebook/api/ufiservices/common/EditCommentParams;

    .line 1061820
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1061821
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "message"

    .line 1061822
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/EditCommentParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1061823
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061824
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/EditCommentParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1061825
    if-eqz v0, :cond_0

    .line 1061826
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "attachment_id"

    .line 1061827
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/EditCommentParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1061828
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061829
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "editComment"

    const-string v2, "POST"

    .line 1061830
    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1061831
    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1061832
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1061833
    const/4 v0, 0x0

    return-object v0
.end method
