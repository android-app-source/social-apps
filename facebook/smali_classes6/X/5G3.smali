.class public final LX/5G3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 886877
    const/4 v9, 0x0

    .line 886878
    const/4 v8, 0x0

    .line 886879
    const/4 v7, 0x0

    .line 886880
    const/4 v6, 0x0

    .line 886881
    const/4 v5, 0x0

    .line 886882
    const/4 v4, 0x0

    .line 886883
    const/4 v3, 0x0

    .line 886884
    const/4 v2, 0x0

    .line 886885
    const/4 v1, 0x0

    .line 886886
    const/4 v0, 0x0

    .line 886887
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 886888
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886889
    const/4 v0, 0x0

    .line 886890
    :goto_0
    return v0

    .line 886891
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886892
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 886893
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 886894
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 886895
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 886896
    const-string v11, "can_viewer_edit_items"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 886897
    const/4 v1, 0x1

    .line 886898
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 886899
    :cond_2
    const-string v11, "confirmed_location"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 886900
    invoke-static {p0, p1}, LX/5GF;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 886901
    :cond_3
    const-string v11, "distinct_recommenders_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 886902
    const/4 v0, 0x1

    .line 886903
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 886904
    :cond_4
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 886905
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 886906
    :cond_5
    const-string v11, "invited_friends_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 886907
    invoke-static {p0, p1}, LX/5Fs;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 886908
    :cond_6
    const-string v11, "list_items"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 886909
    invoke-static {p0, p1}, LX/5G1;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 886910
    :cond_7
    const-string v11, "pending_location"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 886911
    invoke-static {p0, p1}, LX/5GF;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 886912
    :cond_8
    const-string v11, "world_view_bounding_box"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 886913
    invoke-static {p0, p1}, LX/5G2;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 886914
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 886915
    if-eqz v1, :cond_a

    .line 886916
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 886917
    :cond_a
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 886918
    if-eqz v0, :cond_b

    .line 886919
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 886920
    :cond_b
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 886921
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 886922
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 886923
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 886924
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 886925
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 886926
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 886927
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 886928
    if-eqz v0, :cond_0

    .line 886929
    const-string v1, "can_viewer_edit_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886930
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 886931
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886932
    if-eqz v0, :cond_1

    .line 886933
    const-string v1, "confirmed_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886934
    invoke-static {p0, v0, p2, p3}, LX/5GF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886935
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 886936
    if-eqz v0, :cond_2

    .line 886937
    const-string v1, "distinct_recommenders_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886938
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 886939
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 886940
    if-eqz v0, :cond_3

    .line 886941
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886942
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 886943
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886944
    if-eqz v0, :cond_4

    .line 886945
    const-string v1, "invited_friends_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886946
    invoke-static {p0, v0, p2}, LX/5Fs;->a(LX/15i;ILX/0nX;)V

    .line 886947
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886948
    if-eqz v0, :cond_5

    .line 886949
    const-string v1, "list_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886950
    invoke-static {p0, v0, p2, p3}, LX/5G1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886951
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886952
    if-eqz v0, :cond_6

    .line 886953
    const-string v1, "pending_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886954
    invoke-static {p0, v0, p2, p3}, LX/5GF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886955
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886956
    if-eqz v0, :cond_7

    .line 886957
    const-string v1, "world_view_bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886958
    invoke-static {p0, v0, p2}, LX/5G2;->a(LX/15i;ILX/0nX;)V

    .line 886959
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 886960
    return-void
.end method
