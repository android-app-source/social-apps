.class public LX/6Dl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dk;


# instance fields
.field private final a:LX/6FE;

.field private final b:LX/6rC;


# direct methods
.method public constructor <init>(LX/6FE;LX/6rC;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1065626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065627
    iput-object p1, p0, LX/6Dl;->a:LX/6FE;

    .line 1065628
    iput-object p2, p0, LX/6Dl;->b:LX/6rC;

    .line 1065629
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1065630
    iget-object v0, p0, LX/6Dl;->a:LX/6FE;

    .line 1065631
    iget-object v1, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065632
    iget-object v1, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    sget-object p0, LX/6Cy;->BROWSER_EXTENSION_USER_CANCELED_PAYMENT_FLOW:LX/6Cy;

    invoke-virtual {p0}, LX/6Cy;->getValue()I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1065633
    return-void
.end method

.method public final a(LX/6qn;)V
    .locals 1

    .prologue
    .line 1065634
    iget-object v0, p0, LX/6Dl;->b:LX/6rC;

    invoke-virtual {v0, p1}, LX/6rC;->a(LX/6qn;)V

    .line 1065635
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1065636
    iget-object v0, p0, LX/6Dl;->b:LX/6rC;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/6rC;->a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V

    .line 1065637
    return-void
.end method
