.class public final LX/6Pg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6P1;


# direct methods
.method public constructor <init>(LX/6P1;)V
    .locals 0

    .prologue
    .line 1086292
    iput-object p1, p0, LX/6Pg;->a:LX/6P1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1086293
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1086294
    if-nez p1, :cond_1

    .line 1086295
    const/4 p1, 0x0

    .line 1086296
    :cond_0
    :goto_0
    return-object p1

    .line 1086297
    :cond_1
    iget-object v0, p0, LX/6Pg;->a:LX/6P1;

    iget-object v0, v0, LX/6P1;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/6Pg;->a:LX/6P1;

    iget-object v0, v0, LX/6P1;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/6Pg;->a:LX/6P1;

    iget-object v0, v0, LX/6P1;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086298
    :cond_2
    iget-object v0, p0, LX/6Pg;->a:LX/6P1;

    invoke-virtual {v0, p1}, LX/6P1;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0
.end method
