.class public final LX/58m;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 848991
    const-class v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    const v0, -0x1a92eb1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "CommentCreateMutation"

    const-string v6, "7ddc875d0a76371324f755989338f474"

    const-string v7, "comment_create"

    const-string v8, "46"

    const-string v9, "10155258465316729"

    const/4 v10, 0x0

    .line 848992
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 848993
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 848994
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 848995
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 848996
    sparse-switch v0, :sswitch_data_0

    .line 848997
    :goto_0
    return-object p1

    .line 848998
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 848999
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 849000
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 849001
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 849002
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 849003
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 849004
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 849005
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 849006
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 849007
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 849008
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 849009
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 849010
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 849011
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 849012
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 849013
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 849014
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 849015
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 849016
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 849017
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 849018
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 849019
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 849020
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 849021
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 849022
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 849023
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 849024
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 849025
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 849026
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 849027
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 849028
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 849029
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 849030
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 849031
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    .line 849032
    :sswitch_22
    const-string p1, "34"

    goto :goto_0

    .line 849033
    :sswitch_23
    const-string p1, "35"

    goto :goto_0

    .line 849034
    :sswitch_24
    const-string p1, "36"

    goto :goto_0

    .line 849035
    :sswitch_25
    const-string p1, "37"

    goto :goto_0

    .line 849036
    :sswitch_26
    const-string p1, "38"

    goto :goto_0

    .line 849037
    :sswitch_27
    const-string p1, "39"

    goto :goto_0

    .line 849038
    :sswitch_28
    const-string p1, "40"

    goto :goto_0

    .line 849039
    :sswitch_29
    const-string p1, "41"

    goto :goto_0

    .line 849040
    :sswitch_2a
    const-string p1, "42"

    goto/16 :goto_0

    .line 849041
    :sswitch_2b
    const-string p1, "43"

    goto/16 :goto_0

    .line 849042
    :sswitch_2c
    const-string p1, "44"

    goto/16 :goto_0

    .line 849043
    :sswitch_2d
    const-string p1, "45"

    goto/16 :goto_0

    .line 849044
    :sswitch_2e
    const-string p1, "46"

    goto/16 :goto_0

    .line 849045
    :sswitch_2f
    const-string p1, "47"

    goto/16 :goto_0

    .line 849046
    :sswitch_30
    const-string p1, "48"

    goto/16 :goto_0

    .line 849047
    :sswitch_31
    const-string p1, "49"

    goto/16 :goto_0

    .line 849048
    :sswitch_32
    const-string p1, "50"

    goto/16 :goto_0

    .line 849049
    :sswitch_33
    const-string p1, "51"

    goto/16 :goto_0

    .line 849050
    :sswitch_34
    const-string p1, "52"

    goto/16 :goto_0

    .line 849051
    :sswitch_35
    const-string p1, "53"

    goto/16 :goto_0

    .line 849052
    :sswitch_36
    const-string p1, "54"

    goto/16 :goto_0

    .line 849053
    :sswitch_37
    const-string p1, "55"

    goto/16 :goto_0

    .line 849054
    :sswitch_38
    const-string p1, "56"

    goto/16 :goto_0

    .line 849055
    :sswitch_39
    const-string p1, "57"

    goto/16 :goto_0

    .line 849056
    :sswitch_3a
    const-string p1, "58"

    goto/16 :goto_0

    .line 849057
    :sswitch_3b
    const-string p1, "59"

    goto/16 :goto_0

    .line 849058
    :sswitch_3c
    const-string p1, "60"

    goto/16 :goto_0

    .line 849059
    :sswitch_3d
    const-string p1, "61"

    goto/16 :goto_0

    .line 849060
    :sswitch_3e
    const-string p1, "62"

    goto/16 :goto_0

    .line 849061
    :sswitch_3f
    const-string p1, "63"

    goto/16 :goto_0

    .line 849062
    :sswitch_40
    const-string p1, "64"

    goto/16 :goto_0

    .line 849063
    :sswitch_41
    const-string p1, "65"

    goto/16 :goto_0

    .line 849064
    :sswitch_42
    const-string p1, "66"

    goto/16 :goto_0

    .line 849065
    :sswitch_43
    const-string p1, "67"

    goto/16 :goto_0

    .line 849066
    :sswitch_44
    const-string p1, "68"

    goto/16 :goto_0

    .line 849067
    :sswitch_45
    const-string p1, "69"

    goto/16 :goto_0

    .line 849068
    :sswitch_46
    const-string p1, "70"

    goto/16 :goto_0

    .line 849069
    :sswitch_47
    const-string p1, "71"

    goto/16 :goto_0

    .line 849070
    :sswitch_48
    const-string p1, "72"

    goto/16 :goto_0

    .line 849071
    :sswitch_49
    const-string p1, "73"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f566515 -> :sswitch_2f
        -0x7b752021 -> :sswitch_3
        -0x7531a756 -> :sswitch_23
        -0x6e3ba572 -> :sswitch_11
        -0x6a24640d -> :sswitch_40
        -0x6a02a4f4 -> :sswitch_32
        -0x69f19a9a -> :sswitch_1
        -0x680de62a -> :sswitch_29
        -0x66730232 -> :sswitch_18
        -0x6326fdb3 -> :sswitch_26
        -0x5e743804 -> :sswitch_c
        -0x5c3c5330 -> :sswitch_20
        -0x5709d77d -> :sswitch_44
        -0x55ff6f9b -> :sswitch_2
        -0x5349037c -> :sswitch_30
        -0x51484e72 -> :sswitch_e
        -0x513764de -> :sswitch_41
        -0x50cab1c8 -> :sswitch_8
        -0x4eea3afb -> :sswitch_b
        -0x4c89ce34 -> :sswitch_49
        -0x4ae70342 -> :sswitch_9
        -0x4496acc9 -> :sswitch_2a
        -0x41a91745 -> :sswitch_38
        -0x3c54de38 -> :sswitch_2d
        -0x3b85b241 -> :sswitch_43
        -0x39e54905 -> :sswitch_37
        -0x39c63c8e -> :sswitch_1f
        -0x3862b5b3 -> :sswitch_31
        -0x30b65c8f -> :sswitch_21
        -0x2f1c601a -> :sswitch_24
        -0x2893147a -> :sswitch_1e
        -0x25a646c8 -> :sswitch_1d
        -0x24e1906f -> :sswitch_4
        -0x2177e47b -> :sswitch_22
        -0x201d08e7 -> :sswitch_35
        -0x1b87b280 -> :sswitch_25
        -0x17e48248 -> :sswitch_5
        -0x14283bca -> :sswitch_34
        -0x12efdeb3 -> :sswitch_2b
        -0x8ca6426 -> :sswitch_7
        -0x6c84690 -> :sswitch_48
        -0x587d3fa -> :sswitch_27
        -0xf798bc -> :sswitch_1a
        0x180aba4 -> :sswitch_15
        0x271a169 -> :sswitch_19
        0x5fb57ca -> :sswitch_2e
        0xa1fa812 -> :sswitch_10
        0xc168ff8 -> :sswitch_a
        0xf83d490 -> :sswitch_1b
        0x11850e88 -> :sswitch_3b
        0x18ce3dbb -> :sswitch_f
        0x214100e0 -> :sswitch_2c
        0x2292beef -> :sswitch_3f
        0x244e76e6 -> :sswitch_3e
        0x26d0c0ff -> :sswitch_3a
        0x27208b4a -> :sswitch_3c
        0x291d8de0 -> :sswitch_39
        0x2e315071 -> :sswitch_47
        0x2f8b060e -> :sswitch_46
        0x3052e0ff -> :sswitch_13
        0x34e16755 -> :sswitch_0
        0x410878b1 -> :sswitch_36
        0x420eb51c -> :sswitch_12
        0x43ee5105 -> :sswitch_45
        0x54ace343 -> :sswitch_3d
        0x54df6484 -> :sswitch_d
        0x5e7957c4 -> :sswitch_1c
        0x5f424068 -> :sswitch_14
        0x63c03b07 -> :sswitch_28
        0x6cf31bd4 -> :sswitch_17
        0x73a026b5 -> :sswitch_33
        0x7506f93c -> :sswitch_42
        0x7ac228c1 -> :sswitch_16
        0x7c6b80b3 -> :sswitch_6
    .end sparse-switch
.end method
