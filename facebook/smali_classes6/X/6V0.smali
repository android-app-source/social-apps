.class public LX/6V0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6V2;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1103364
    const-class v0, LX/6V0;

    sput-object v0, LX/6V0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;Ljava/util/Set;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Ljava/util/Set",
            "<",
            "LX/6V2;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103273
    iput-object p1, p0, LX/6V0;->b:LX/0lC;

    .line 1103274
    iput-object p2, p0, LX/6V0;->c:Ljava/util/Set;

    .line 1103275
    iput-object p3, p0, LX/6V0;->d:LX/03V;

    .line 1103276
    return-void
.end method

.method private a(Landroid/os/Bundle;)LX/0m9;
    .locals 10

    .prologue
    .line 1103325
    iget-object v0, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v6

    .line 1103326
    const/4 v4, 0x0

    .line 1103327
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 1103328
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1103329
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 1103330
    if-nez v1, :cond_0

    .line 1103331
    const-string v1, "null"

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 1103332
    :cond_0
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1103333
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 1103334
    :cond_1
    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 1103335
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;

    goto :goto_0

    .line 1103336
    :cond_2
    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_3

    .line 1103337
    check-cast v1, Landroid/os/Bundle;

    invoke-direct {p0, v1}, LX/6V0;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 1103338
    :cond_3
    instance-of v2, v1, Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 1103339
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    goto :goto_0

    .line 1103340
    :cond_4
    instance-of v2, v1, Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 1103341
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0m9;

    goto :goto_0

    .line 1103342
    :cond_5
    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_6

    .line 1103343
    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    goto :goto_0

    .line 1103344
    :cond_6
    instance-of v2, v1, Ljava/lang/Double;

    if-eqz v2, :cond_7

    .line 1103345
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Double;)LX/0m9;

    goto :goto_0

    .line 1103346
    :cond_7
    instance-of v2, v1, Ljava/lang/Short;

    if-eqz v2, :cond_8

    .line 1103347
    check-cast v1, Ljava/lang/Short;

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Short;)LX/0m9;

    goto :goto_0

    .line 1103348
    :cond_8
    instance-of v2, v1, [B

    if-eqz v2, :cond_9

    .line 1103349
    check-cast v1, [B

    check-cast v1, [B

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;[B)LX/0m9;

    goto :goto_0

    .line 1103350
    :cond_9
    instance-of v2, v1, Ljava/util/ArrayList;

    if-eqz v2, :cond_d

    .line 1103351
    iget-object v2, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->f()LX/162;

    move-result-object v3

    .line 1103352
    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v8, :cond_b

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 1103353
    instance-of v9, v2, Landroid/os/Bundle;

    if-eqz v9, :cond_a

    .line 1103354
    check-cast v2, Landroid/os/Bundle;

    invoke-direct {p0, v2}, LX/6V0;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/162;->a(LX/0lF;)LX/162;

    .line 1103355
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 1103356
    :cond_a
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_2

    .line 1103357
    :cond_b
    const-string v1, "children"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move-object v0, v3

    :goto_3
    move-object v4, v0

    .line 1103358
    goto/16 :goto_0

    .line 1103359
    :cond_c
    invoke-virtual {v6, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto/16 :goto_0

    .line 1103360
    :cond_d
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-object v0, v4

    goto :goto_3

    .line 1103361
    :cond_e
    if-eqz v4, :cond_f

    .line 1103362
    const-string v0, "children"

    invoke-virtual {v6, v0, v4}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103363
    :cond_f
    return-object v6
.end method

.method public static a(LX/0QB;)LX/6V0;
    .locals 1

    .prologue
    .line 1103324
    invoke-static {p0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/6V0;Landroid/view/View;LX/6Uz;I)Landroid/os/Bundle;
    .locals 10

    .prologue
    .line 1103295
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1103296
    sget-object v1, LX/6Uz;->ALL:LX/6Uz;

    if-ne p2, v1, :cond_0

    .line 1103297
    const-string v1, "depth_level"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103298
    :cond_0
    iget-object v1, p0, LX/6V0;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6V2;

    .line 1103299
    invoke-interface {v1}, LX/6V2;->a()Ljava/lang/Class;

    move-result-object v3

    .line 1103300
    invoke-virtual {v3, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1103301
    invoke-virtual {v3, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3, v0}, LX/6V2;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1103302
    :cond_2
    sget-object v1, LX/6Uz;->NONE:LX/6Uz;

    if-eq p2, v1, :cond_5

    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    .line 1103303
    check-cast p1, Landroid/view/ViewGroup;

    add-int/lit8 v1, p3, 0x1

    const/4 v4, 0x0

    .line 1103304
    sget-object v2, LX/6Ux;->a:[I

    invoke-virtual {p2}, LX/6Uz;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1103305
    const/4 v3, 0x1

    .line 1103306
    sget-object v2, LX/6Uz;->ALL:LX/6Uz;

    .line 1103307
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    .line 1103308
    invoke-static {v8}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v9

    move v7, v4

    move v6, v4

    .line 1103309
    :goto_2
    if-ge v7, v8, :cond_3

    .line 1103310
    invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1103311
    invoke-static {p0, v5, v2, v1}, LX/6V0;->a(LX/6V0;Landroid/view/View;LX/6Uz;I)Landroid/os/Bundle;

    move-result-object v5

    .line 1103312
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1103313
    if-eqz v3, :cond_7

    const-string p3, "max_child_depth"

    invoke-virtual {v5, p3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_7

    .line 1103314
    const-string p3, "max_child_depth"

    invoke-virtual {v5, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1103315
    if-le v5, v6, :cond_7

    .line 1103316
    :goto_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    goto :goto_2

    .line 1103317
    :pswitch_0
    sget-object v2, LX/6Uz;->NONE:LX/6Uz;

    move v3, v4

    .line 1103318
    goto :goto_1

    .line 1103319
    :cond_3
    if-eqz v3, :cond_4

    .line 1103320
    const-string v2, "max_child_depth"

    if-nez v8, :cond_6

    :goto_4
    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103321
    :cond_4
    const-string v2, "children"

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1103322
    :cond_5
    return-object v0

    .line 1103323
    :cond_6
    add-int/lit8 v4, v6, 0x1

    goto :goto_4

    :cond_7
    move v5, v6

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/6V0;
    .locals 5

    .prologue
    .line 1103365
    new-instance v2, LX/6V0;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    .line 1103366
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/6Uw;

    invoke-direct {v4, p0}, LX/6Uw;-><init>(LX/0QB;)V

    invoke-direct {v1, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v3, v1

    .line 1103367
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v3, v1}, LX/6V0;-><init>(LX/0lC;Ljava/util/Set;LX/03V;)V

    .line 1103368
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/6Uz;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1103294
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/6V0;->a(LX/6V0;Landroid/view/View;LX/6Uz;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;LX/6Uz;LX/6Uy;)Ljava/lang/String;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1103286
    const/4 v0, 0x0

    .line 1103287
    invoke-virtual {p0, p1, p2}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v1

    .line 1103288
    :try_start_0
    sget-object v2, LX/6Uy;->PRETTY:LX/6Uy;

    if-ne p3, v2, :cond_0

    .line 1103289
    iget-object v2, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->g()LX/4ps;

    move-result-object v2

    invoke-virtual {v2}, LX/4ps;->a()LX/4ps;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1103290
    :goto_0
    return-object v0

    .line 1103291
    :cond_0
    iget-object v2, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v2, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1103292
    :catch_0
    move-exception v1

    .line 1103293
    iget-object v2, p0, LX/6V0;->d:LX/03V;

    sget-object v3, LX/6V0;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error building view description JSON"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/File;LX/6Uy;Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    .line 1103278
    invoke-direct {p0, p3}, LX/6V0;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v0

    .line 1103279
    :try_start_0
    sget-object v1, LX/6Uy;->PRETTY:LX/6Uy;

    if-ne p2, v1, :cond_0

    .line 1103280
    iget-object v1, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v1}, LX/0lC;->g()LX/4ps;

    move-result-object v1

    invoke-virtual {v1}, LX/4ps;->a()LX/4ps;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/4ps;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 1103281
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1103282
    :cond_0
    iget-object v1, p0, LX/6V0;->b:LX/0lC;

    invoke-virtual {v1, p1, v0}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1103283
    :catch_0
    move-exception v0

    .line 1103284
    iget-object v1, p0, LX/6V0;->d:LX/03V;

    sget-object v2, LX/6V0;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error building view description JSON File"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1103285
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/view/View;LX/6Uz;)LX/0m9;
    .locals 1

    .prologue
    .line 1103277
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/6V0;->a(LX/6V0;Landroid/view/View;LX/6Uz;I)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6V0;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v0

    return-object v0
.end method
