.class public LX/5qp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private final b:[F

.field private c:Z

.field private final d:Landroid/view/ViewGroup;

.field private final e:LX/5sE;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1009674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009675
    const/4 v0, -0x1

    iput v0, p0, LX/5qp;->a:I

    .line 1009676
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/5qp;->b:[F

    .line 1009677
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5qp;->c:Z

    .line 1009678
    new-instance v0, LX/5sE;

    invoke-direct {v0}, LX/5sE;-><init>()V

    iput-object v0, p0, LX/5qp;->e:LX/5sE;

    .line 1009679
    iput-object p1, p0, LX/5qp;->d:Landroid/view/ViewGroup;

    .line 1009680
    return-void
.end method

.method private c(Landroid/view/MotionEvent;LX/5s9;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1009707
    iget v0, p0, LX/5qp;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1009708
    const-string v0, "React"

    const-string v1, "Can\'t cancel already finished gesture. Is a child View trying to start a gesture from an UP/CANCEL event?"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009709
    :goto_0
    return-void

    .line 1009710
    :cond_0
    iget-boolean v0, p0, LX/5qp;->c:Z

    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    const-string v1, "Expected to not have already sent a cancel for this gesture"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1009711
    invoke-static {p2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/5s9;

    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->CANCEL:LX/5sF;

    iget-object v4, p0, LX/5qp;->b:[F

    aget v3, v4, v3

    iget-object v4, p0, LX/5qp;->b:[F

    aget v4, v4, v2

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0

    :cond_1
    move v0, v3

    .line 1009712
    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;LX/5s9;)V
    .locals 1

    .prologue
    .line 1009713
    iget-boolean v0, p0, LX/5qp;->c:Z

    if-eqz v0, :cond_0

    .line 1009714
    :goto_0
    return-void

    .line 1009715
    :cond_0
    invoke-direct {p0, p1, p2}, LX/5qp;->c(Landroid/view/MotionEvent;LX/5s9;)V

    .line 1009716
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5qp;->c:Z

    .line 1009717
    const/4 v0, -0x1

    iput v0, p0, LX/5qp;->a:I

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;LX/5s9;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1009681
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1009682
    if-nez v0, :cond_2

    .line 1009683
    iget v0, p0, LX/5qp;->a:I

    if-eq v0, v7, :cond_0

    .line 1009684
    const-string v0, "React"

    const-string v1, "Got DOWN touch before receiving UP or CANCEL from last gesture"

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009685
    :cond_0
    iput-boolean v5, p0, LX/5qp;->c:Z

    .line 1009686
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, LX/5qp;->d:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/5qp;->b:[F

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, LX/5rK;->a(FFLandroid/view/ViewGroup;[F[I)I

    move-result v0

    iput v0, p0, LX/5qp;->a:I

    .line 1009687
    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->START:LX/5sF;

    iget-object v2, p0, LX/5qp;->b:[F

    aget v3, v2, v5

    iget-object v2, p0, LX/5qp;->b:[F

    aget v4, v2, v6

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5s9;->a(LX/5r0;)V

    .line 1009688
    :cond_1
    :goto_0
    return-void

    .line 1009689
    :cond_2
    iget-boolean v1, p0, LX/5qp;->c:Z

    if-nez v1, :cond_1

    .line 1009690
    iget v1, p0, LX/5qp;->a:I

    if-ne v1, v7, :cond_3

    .line 1009691
    const-string v0, "React"

    const-string v1, "Unexpected state: received touch event but didn\'t get starting ACTION_DOWN for this gesture before"

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1009692
    :cond_3
    if-ne v0, v6, :cond_4

    .line 1009693
    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->END:LX/5sF;

    iget-object v2, p0, LX/5qp;->b:[F

    aget v3, v2, v5

    iget-object v2, p0, LX/5qp;->b:[F

    aget v4, v2, v6

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5s9;->a(LX/5r0;)V

    .line 1009694
    iput v7, p0, LX/5qp;->a:I

    goto :goto_0

    .line 1009695
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 1009696
    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->MOVE:LX/5sF;

    iget-object v2, p0, LX/5qp;->b:[F

    aget v3, v2, v5

    iget-object v2, p0, LX/5qp;->b:[F

    aget v4, v2, v6

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0

    .line 1009697
    :cond_5
    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    .line 1009698
    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->START:LX/5sF;

    iget-object v2, p0, LX/5qp;->b:[F

    aget v3, v2, v5

    iget-object v2, p0, LX/5qp;->b:[F

    aget v4, v2, v6

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0

    .line 1009699
    :cond_6
    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 1009700
    iget v0, p0, LX/5qp;->a:I

    sget-object v1, LX/5sF;->END:LX/5sF;

    iget-object v2, p0, LX/5qp;->b:[F

    aget v3, v2, v5

    iget-object v2, p0, LX/5qp;->b:[F

    aget v4, v2, v6

    iget-object v5, p0, LX/5qp;->e:LX/5sE;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/5sD;->a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0

    .line 1009701
    :cond_7
    const/4 v1, 0x3

    if-ne v0, v1, :cond_9

    .line 1009702
    iget-object v0, p0, LX/5qp;->e:LX/5sE;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/5sE;->e(J)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1009703
    invoke-direct {p0, p1, p2}, LX/5qp;->c(Landroid/view/MotionEvent;LX/5s9;)V

    .line 1009704
    :goto_1
    iput v7, p0, LX/5qp;->a:I

    goto/16 :goto_0

    .line 1009705
    :cond_8
    const-string v0, "React"

    const-string v1, "Received an ACTION_CANCEL touch event for which we have no corresponding ACTION_DOWN"

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1009706
    :cond_9
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Warning : touch event was ignored. Action="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Target="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LX/5qp;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
