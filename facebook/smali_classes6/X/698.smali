.class public final LX/698;
.super LX/67m;
.source ""


# static fields
.field public static final o:Landroid/graphics/Matrix;

.field private static final p:Landroid/graphics/Paint;

.field private static final q:Landroid/graphics/Path;

.field private static final r:LX/31i;

.field private static s:Z

.field private static t:Landroid/os/Vibrator;


# instance fields
.field private A:LX/68w;

.field private B:Lcom/facebook/android/maps/model/LatLng;

.field private C:F

.field public D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private final F:[F

.field private final G:[F

.field public H:LX/67o;

.field private I:Landroid/view/View;

.field private J:Z

.field public K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:F

.field private P:F

.field public Q:F

.field private R:F

.field public S:F

.field private T:F

.field public U:F

.field public V:F

.field public W:F

.field public X:F

.field public Y:I

.field public Z:I

.field public final aa:[F

.field public ab:F

.field public ac:F

.field public ad:F

.field private ae:F

.field private af:F

.field private ag:F

.field public final u:F

.field public final v:F

.field private final w:I

.field private x:F

.field private y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1057629
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/698;->o:Landroid/graphics/Matrix;

    .line 1057630
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LX/698;->p:Landroid/graphics/Paint;

    .line 1057631
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, LX/698;->q:Landroid/graphics/Path;

    .line 1057632
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    sput-object v0, LX/698;->r:LX/31i;

    return-void
.end method

.method public constructor <init>(LX/680;LX/699;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 1057483
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1057484
    new-array v0, v1, [F

    iput-object v0, p0, LX/698;->F:[F

    .line 1057485
    new-array v0, v1, [F

    iput-object v0, p0, LX/698;->G:[F

    .line 1057486
    new-array v0, v1, [F

    iput-object v0, p0, LX/698;->aa:[F

    .line 1057487
    iget-object v0, p2, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v0

    .line 1057488
    iput-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    .line 1057489
    iget-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, LX/31h;->d(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->m:D

    .line 1057490
    iget-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->n:D

    .line 1057491
    iget-object v0, p2, LX/699;->c:LX/68w;

    move-object v0, v0

    .line 1057492
    iput-object v0, p0, LX/698;->A:LX/68w;

    .line 1057493
    iget-boolean v0, p2, LX/699;->e:Z

    move v0, v0

    .line 1057494
    iput-boolean v0, p0, LX/698;->y:Z

    .line 1057495
    iget-boolean v0, p2, LX/699;->f:Z

    move v0, v0

    .line 1057496
    iput-boolean v0, p0, LX/698;->z:Z

    .line 1057497
    iget v0, p2, LX/699;->g:F

    move v0, v0

    .line 1057498
    iput v0, p0, LX/698;->C:F

    .line 1057499
    iget-object v0, p2, LX/699;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1057500
    iput-object v0, p0, LX/698;->E:Ljava/lang/String;

    .line 1057501
    iget-object v0, p2, LX/699;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1057502
    iput-object v0, p0, LX/698;->D:Ljava/lang/String;

    .line 1057503
    iget v0, p2, LX/699;->d:F

    move v0, v0

    .line 1057504
    iput v0, p0, LX/698;->x:F

    .line 1057505
    iget-boolean v0, p2, LX/699;->k:Z

    move v0, v0

    .line 1057506
    iput-boolean v0, p0, LX/698;->i:Z

    .line 1057507
    iget v0, p2, LX/699;->j:F

    move v0, v0

    .line 1057508
    iput v0, p0, LX/698;->k:F

    .line 1057509
    iget-object v0, p0, LX/698;->G:[F

    invoke-virtual {p2}, LX/699;->b()F

    move-result v1

    aput v1, v0, v2

    .line 1057510
    iget-object v0, p0, LX/698;->G:[F

    invoke-virtual {p2}, LX/699;->c()F

    move-result v1

    aput v1, v0, v3

    .line 1057511
    iget-object v0, p0, LX/698;->F:[F

    invoke-virtual {p2}, LX/699;->e()F

    move-result v1

    aput v1, v0, v2

    .line 1057512
    iget-object v0, p0, LX/698;->F:[F

    invoke-virtual {p2}, LX/699;->f()F

    move-result v1

    aput v1, v0, v3

    .line 1057513
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x42400000    # 48.0f

    mul-float/2addr v0, v1

    iput v0, p0, LX/698;->u:F

    .line 1057514
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x41000000    # 8.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/698;->w:I

    .line 1057515
    iget v0, p0, LX/67m;->d:F

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    iput v0, p0, LX/698;->v:F

    .line 1057516
    invoke-direct {p0}, LX/698;->r()V

    .line 1057517
    return-void
.end method

.method private r()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1057518
    iget-object v0, p0, LX/698;->A:LX/68w;

    iget-object v0, v0, LX/68w;->a:Landroid/graphics/Bitmap;

    .line 1057519
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 1057520
    iget-object v2, p0, LX/698;->G:[F

    aget v2, v2, v3

    mul-float/2addr v2, v1

    iput v2, p0, LX/698;->Q:F

    .line 1057521
    iget v2, p0, LX/698;->Q:F

    sub-float v2, v1, v2

    iput v2, p0, LX/698;->R:F

    .line 1057522
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    .line 1057523
    iget-object v2, p0, LX/698;->G:[F

    aget v2, v2, v4

    mul-float/2addr v2, v0

    iput v2, p0, LX/698;->S:F

    .line 1057524
    iget v2, p0, LX/698;->S:F

    sub-float v2, v0, v2

    iput v2, p0, LX/698;->T:F

    .line 1057525
    iget-object v2, p0, LX/698;->F:[F

    aget v2, v2, v3

    mul-float/2addr v2, v1

    iput v2, p0, LX/698;->U:F

    .line 1057526
    iget-object v2, p0, LX/698;->F:[F

    aget v2, v2, v4

    mul-float/2addr v2, v0

    iput v2, p0, LX/698;->V:F

    .line 1057527
    iget v2, p0, LX/698;->u:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 1057528
    iget v2, p0, LX/698;->u:F

    sub-float v1, v2, v1

    div-float/2addr v1, v6

    iput v1, p0, LX/698;->W:F

    .line 1057529
    :goto_0
    iget v1, p0, LX/698;->u:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 1057530
    iget v1, p0, LX/698;->u:F

    sub-float v0, v1, v0

    div-float/2addr v0, v6

    iput v0, p0, LX/698;->X:F

    .line 1057531
    :goto_1
    sget-object v0, LX/698;->o:Landroid/graphics/Matrix;

    iget v1, p0, LX/698;->C:F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 1057532
    iget-object v0, p0, LX/67m;->c:[F

    iget v1, p0, LX/698;->Q:F

    iget v2, p0, LX/698;->U:F

    sub-float/2addr v1, v2

    aput v1, v0, v3

    .line 1057533
    iget-object v0, p0, LX/67m;->c:[F

    iget v1, p0, LX/698;->S:F

    iget v2, p0, LX/698;->V:F

    sub-float/2addr v1, v2

    aput v1, v0, v4

    .line 1057534
    sget-object v0, LX/698;->o:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/67m;->c:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1057535
    iget v0, p0, LX/698;->Y:I

    int-to-float v0, v0

    iget v1, p0, LX/698;->v:F

    add-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->c:[F

    aget v1, v1, v4

    add-float/2addr v0, v1

    iput v0, p0, LX/698;->ae:F

    .line 1057536
    iget v0, p0, LX/698;->Z:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, LX/67m;->c:[F

    aget v1, v1, v3

    add-float/2addr v0, v1

    iput v0, p0, LX/698;->af:F

    .line 1057537
    iget v0, p0, LX/698;->Z:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, LX/67m;->c:[F

    aget v1, v1, v3

    sub-float/2addr v0, v1

    iput v0, p0, LX/698;->ag:F

    .line 1057538
    return-void

    .line 1057539
    :cond_0
    iput v5, p0, LX/698;->W:F

    goto :goto_0

    .line 1057540
    :cond_1
    iput v5, p0, LX/698;->X:F

    goto :goto_1
.end method

.method public static t(LX/698;)V
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v2, -0x2

    .line 1057541
    const/4 v0, 0x0

    iput-object v0, p0, LX/698;->I:Landroid/view/View;

    .line 1057542
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1057543
    iget-object v1, v0, LX/680;->N:LX/6aU;

    move-object v0, v1

    .line 1057544
    if-eqz v0, :cond_0

    .line 1057545
    iget-object v1, v0, LX/6aU;->a:LX/IJL;

    invoke-static {p0}, LX/6ax;->a(LX/698;)LX/6ax;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/IJL;->b(LX/6ax;)Landroid/view/View;

    move-result-object v1

    move-object v1, v1

    .line 1057546
    iput-object v1, p0, LX/698;->I:Landroid/view/View;

    .line 1057547
    iget-object v1, p0, LX/698;->I:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1057548
    iput-boolean v8, p0, LX/698;->J:Z

    .line 1057549
    :cond_0
    :goto_0
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 1057550
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1057551
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1057552
    :cond_1
    :goto_1
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    iget-object v1, p0, LX/67m;->e:LX/680;

    .line 1057553
    iget-object v2, v1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v1, v2

    .line 1057554
    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v1

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, LX/67m;->e:LX/680;

    .line 1057555
    iget-object v3, v2, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v2, v3

    .line 1057556
    invoke-virtual {v2}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v2

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 1057557
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, LX/698;->Z:I

    .line 1057558
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, LX/698;->Y:I

    .line 1057559
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    iget v1, p0, LX/698;->Z:I

    iget v2, p0, LX/698;->Y:I

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 1057560
    invoke-direct {p0}, LX/698;->r()V

    .line 1057561
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057562
    return-void

    .line 1057563
    :cond_2
    iget-object v1, v0, LX/6aU;->a:LX/IJL;

    invoke-static {p0}, LX/6ax;->a(LX/698;)LX/6ax;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/IJL;->a(LX/6ax;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1057564
    iput-object v0, p0, LX/698;->I:Landroid/view/View;

    .line 1057565
    iput-boolean v7, p0, LX/698;->J:Z

    goto :goto_0

    .line 1057566
    :cond_3
    iput-boolean v7, p0, LX/698;->J:Z

    .line 1057567
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1057568
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v0, p0, LX/67m;->g:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1057569
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1057570
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1057571
    iget-object v0, p0, LX/698;->E:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/698;->D:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1057572
    iget v0, p0, LX/698;->w:I

    div-int/lit8 v0, v0, 0x3

    .line 1057573
    :goto_2
    iget-object v3, p0, LX/698;->E:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 1057574
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, LX/67m;->g:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1057575
    iget v4, p0, LX/698;->w:I

    iget v5, p0, LX/698;->w:I

    iget v6, p0, LX/698;->w:I

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1057576
    iget-object v4, p0, LX/698;->E:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1057577
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1057578
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1057579
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1057580
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1057581
    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1057582
    const/4 v4, -0x1

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1057583
    invoke-virtual {v2, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1057584
    :cond_4
    iget-object v3, p0, LX/698;->D:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 1057585
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, LX/67m;->g:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1057586
    iget v4, p0, LX/698;->w:I

    iget v5, p0, LX/698;->w:I

    iget v6, p0, LX/698;->w:I

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1057587
    iget-object v0, p0, LX/698;->D:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1057588
    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1057589
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1057590
    const v0, -0xbbbbbc

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1057591
    const/4 v0, 0x3

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1057592
    invoke-virtual {v2, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1057593
    :cond_5
    iput-object v2, p0, LX/698;->I:Landroid/view/View;

    goto/16 :goto_1

    .line 1057594
    :cond_6
    iget v0, p0, LX/698;->w:I

    goto :goto_2
.end method

.method private u()Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1057595
    iget-boolean v0, p0, LX/698;->K:Z

    move v0, v0

    .line 1057596
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/698;->E:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/698;->D:Ljava/lang/String;

    if-eqz v0, :cond_5

    :cond_0
    move v0, v8

    .line 1057597
    :goto_0
    iget v3, p0, LX/698;->C:F

    iget-boolean v1, p0, LX/698;->z:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/67m;->f:LX/31h;

    invoke-virtual {v1}, LX/31h;->b()F

    move-result v1

    :goto_1
    add-float/2addr v1, v3

    iput v1, p0, LX/698;->ad:F

    .line 1057598
    sget-object v1, LX/698;->r:LX/31i;

    iget-wide v4, p0, LX/67m;->m:D

    iget-object v3, p0, LX/67m;->f:LX/31h;

    iget v6, p0, LX/698;->Q:F

    invoke-virtual {v3, v6}, LX/31h;->a(F)D

    move-result-wide v6

    sub-double/2addr v4, v6

    iput-wide v4, v1, LX/31i;->c:D

    .line 1057599
    sget-object v1, LX/698;->r:LX/31i;

    iget-wide v4, p0, LX/67m;->m:D

    iget-object v3, p0, LX/67m;->f:LX/31h;

    iget v6, p0, LX/698;->R:F

    invoke-virtual {v3, v6}, LX/31h;->a(F)D

    move-result-wide v6

    add-double/2addr v4, v6

    iput-wide v4, v1, LX/31i;->d:D

    .line 1057600
    sget-object v1, LX/698;->r:LX/31i;

    iget-wide v4, p0, LX/67m;->n:D

    iget-object v3, p0, LX/67m;->f:LX/31h;

    iget v6, p0, LX/698;->S:F

    invoke-virtual {v3, v6}, LX/31h;->a(F)D

    move-result-wide v6

    sub-double/2addr v4, v6

    iput-wide v4, v1, LX/31i;->a:D

    .line 1057601
    sget-object v1, LX/698;->r:LX/31i;

    iget-wide v4, p0, LX/67m;->n:D

    iget-object v3, p0, LX/67m;->f:LX/31h;

    iget v6, p0, LX/698;->T:F

    invoke-virtual {v3, v6}, LX/31h;->a(F)D

    move-result-wide v6

    add-double/2addr v4, v6

    iput-wide v4, v1, LX/31i;->b:D

    .line 1057602
    iget v1, p0, LX/698;->C:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 1057603
    sget-object v1, LX/698;->r:LX/31i;

    iget v2, p0, LX/698;->C:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    iget-wide v4, p0, LX/67m;->m:D

    iget-wide v6, p0, LX/67m;->n:D

    invoke-virtual/range {v1 .. v7}, LX/31i;->a(DDD)V

    .line 1057604
    :cond_1
    if-eqz v0, :cond_4

    .line 1057605
    iget-wide v0, p0, LX/67m;->n:D

    iget-object v2, p0, LX/67m;->f:LX/31h;

    iget v3, p0, LX/698;->ae:F

    invoke-virtual {v2, v3}, LX/31h;->a(F)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 1057606
    sget-object v2, LX/698;->r:LX/31i;

    iget-wide v2, v2, LX/31i;->a:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    .line 1057607
    sget-object v2, LX/698;->r:LX/31i;

    iput-wide v0, v2, LX/31i;->a:D

    .line 1057608
    :cond_2
    iget-wide v0, p0, LX/67m;->m:D

    iget-object v2, p0, LX/67m;->f:LX/31h;

    iget v3, p0, LX/698;->af:F

    invoke-virtual {v2, v3}, LX/31h;->a(F)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 1057609
    sget-object v2, LX/698;->r:LX/31i;

    iget-wide v2, v2, LX/31i;->c:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_3

    .line 1057610
    sget-object v2, LX/698;->r:LX/31i;

    iput-wide v0, v2, LX/31i;->c:D

    .line 1057611
    :cond_3
    iget-wide v0, p0, LX/67m;->m:D

    iget-object v2, p0, LX/67m;->f:LX/31h;

    iget v3, p0, LX/698;->ag:F

    invoke-virtual {v2, v3}, LX/31h;->a(F)D

    move-result-wide v2

    add-double/2addr v0, v2

    .line 1057612
    sget-object v2, LX/698;->r:LX/31i;

    iget-wide v2, v2, LX/31i;->d:D

    cmpg-double v2, v2, v0

    if-gez v2, :cond_4

    .line 1057613
    sget-object v2, LX/698;->r:LX/31i;

    iput-wide v0, v2, LX/31i;->d:D

    .line 1057614
    :cond_4
    sget-object v0, LX/698;->r:LX/31i;

    iget-object v1, p0, LX/67m;->c:[F

    invoke-virtual {p0, v0, v1}, LX/67m;->a(LX/31i;[F)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1057615
    :goto_2
    return v9

    :cond_5
    move v0, v9

    .line 1057616
    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 1057617
    goto/16 :goto_1

    .line 1057618
    :cond_7
    iget-object v0, p0, LX/67m;->c:[F

    aget v0, v0, v9

    .line 1057619
    iget-boolean v1, p0, LX/698;->N:Z

    if-nez v1, :cond_8

    .line 1057620
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-wide v2, p0, LX/67m;->m:D

    float-to-double v4, v0

    add-double/2addr v2, v4

    iget-wide v4, p0, LX/67m;->n:D

    iget-object v6, p0, LX/67m;->c:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->b(DD[F)V

    .line 1057621
    iget-object v0, p0, LX/67m;->c:[F

    aget v0, v0, v9

    iput v0, p0, LX/698;->ab:F

    .line 1057622
    iget-object v0, p0, LX/67m;->c:[F

    aget v0, v0, v8

    iput v0, p0, LX/698;->ac:F

    :cond_8
    move v9, v8

    .line 1057623
    goto :goto_2
.end method

.method private v()V
    .locals 6

    .prologue
    .line 1057624
    iget-object v0, p0, LX/67m;->f:LX/31h;

    iget v1, p0, LX/698;->ab:F

    iget v2, p0, LX/698;->ac:F

    iget-object v3, p0, LX/67m;->c:[F

    invoke-virtual {v0, v1, v2, v3}, LX/31h;->a(FF[F)V

    .line 1057625
    iget-object v0, p0, LX/67m;->c:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->m:D

    .line 1057626
    iget-object v0, p0, LX/67m;->c:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->n:D

    .line 1057627
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, p0, LX/67m;->n:D

    invoke-static {v2, v3}, LX/31h;->a(D)D

    move-result-wide v2

    iget-wide v4, p0, LX/67m;->m:D

    invoke-static {v4, v5}, LX/31h;->c(D)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    .line 1057628
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1057643
    iget-boolean v3, p0, LX/698;->K:Z

    move v3, v3

    .line 1057644
    if-eqz v3, :cond_2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1057645
    sget-object v3, LX/698;->o:Landroid/graphics/Matrix;

    iget v4, p0, LX/698;->ad:F

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 1057646
    iget-object v3, p0, LX/698;->aa:[F

    iget v4, p0, LX/698;->U:F

    iget v7, p0, LX/698;->Q:F

    sub-float/2addr v4, v7

    aput v4, v3, v6

    .line 1057647
    iget-object v3, p0, LX/698;->aa:[F

    iget v4, p0, LX/698;->V:F

    iget v7, p0, LX/698;->S:F

    sub-float/2addr v4, v7

    aput v4, v3, v5

    .line 1057648
    sget-object v3, LX/698;->o:Landroid/graphics/Matrix;

    iget-object v4, p0, LX/698;->aa:[F

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1057649
    iget-object v3, p0, LX/698;->aa:[F

    aget v4, v3, v6

    iget v7, p0, LX/698;->ab:F

    add-float/2addr v4, v7

    aput v4, v3, v6

    .line 1057650
    iget-object v3, p0, LX/698;->aa:[F

    aget v4, v3, v5

    iget v7, p0, LX/698;->ac:F

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    add-float/2addr v4, v7

    aput v4, v3, v5

    .line 1057651
    iget v3, p0, LX/698;->Z:I

    div-int/lit8 v7, v3, 0x2

    .line 1057652
    iget-object v3, p0, LX/698;->aa:[F

    aget v3, v3, v5

    iget v4, p0, LX/698;->Y:I

    int-to-float v4, v4

    sub-float v4, v3, v4

    .line 1057653
    iget-object v3, p0, LX/698;->aa:[F

    aget v3, v3, v5

    .line 1057654
    iget-object v8, p0, LX/698;->aa:[F

    aget v8, v8, v6

    int-to-float v9, v7

    sub-float/2addr v8, v9

    .line 1057655
    iget-object v9, p0, LX/698;->aa:[F

    aget v9, v9, v6

    int-to-float v7, v7

    add-float/2addr v7, v9

    .line 1057656
    iget v9, p0, LX/698;->Y:I

    int-to-float v9, v9

    iget v10, p0, LX/698;->u:F

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_0

    .line 1057657
    iget v9, p0, LX/698;->X:F

    sub-float/2addr v4, v9

    .line 1057658
    iget v9, p0, LX/698;->X:F

    sub-float/2addr v3, v9

    .line 1057659
    :cond_0
    iget v9, p0, LX/698;->Z:I

    int-to-float v9, v9

    iget v10, p0, LX/698;->u:F

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_1

    .line 1057660
    iget v9, p0, LX/698;->W:F

    sub-float/2addr v8, v9

    .line 1057661
    iget v9, p0, LX/698;->W:F

    add-float/2addr v7, v9

    .line 1057662
    :cond_1
    cmpl-float v8, p1, v8

    if-ltz v8, :cond_6

    cmpg-float v7, p1, v7

    if-gtz v7, :cond_6

    cmpl-float v4, p2, v4

    if-ltz v4, :cond_6

    cmpg-float v3, p2, v3

    if-gtz v3, :cond_6

    move v3, v5

    .line 1057663
    :goto_0
    move v3, v3

    .line 1057664
    if-eqz v3, :cond_2

    .line 1057665
    iput-boolean v2, p0, LX/698;->M:Z

    .line 1057666
    :goto_1
    return v0

    .line 1057667
    :cond_2
    iput-boolean v1, p0, LX/698;->M:Z

    .line 1057668
    invoke-direct {p0}, LX/698;->u()Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    .line 1057669
    goto :goto_1

    .line 1057670
    :cond_3
    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    .line 1057671
    :try_start_0
    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x0

    aput p1, v3, v6

    .line 1057672
    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x1

    aput p2, v3, v6

    .line 1057673
    iget v3, p0, LX/698;->ad:F

    neg-float v3, v3

    .line 1057674
    sget-object v6, LX/698;->o:Landroid/graphics/Matrix;

    iget v7, p0, LX/698;->ab:F

    iget v8, p0, LX/698;->ac:F

    invoke-virtual {v6, v3, v7, v8}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1057675
    sget-object v3, LX/698;->o:Landroid/graphics/Matrix;

    iget-object v6, p0, LX/698;->aa:[F

    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1057676
    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x0

    aget v3, v3, v6

    iget v6, p0, LX/698;->ab:F

    iget v7, p0, LX/698;->Q:F

    sub-float/2addr v6, v7

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_4

    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x0

    aget v3, v3, v6

    iget v6, p0, LX/698;->ab:F

    iget v7, p0, LX/698;->R:F

    add-float/2addr v6, v7

    cmpg-float v3, v3, v6

    if-gtz v3, :cond_4

    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    iget v6, p0, LX/698;->ac:F

    iget v7, p0, LX/698;->S:F

    sub-float/2addr v6, v7

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_4

    iget-object v3, p0, LX/698;->aa:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    iget v6, p0, LX/698;->ac:F

    iget v7, p0, LX/698;->T:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-float/2addr v6, v7

    cmpg-float v3, v3, v6

    if-gtz v3, :cond_4

    .line 1057677
    sget-object v1, LX/31U;->h:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    goto :goto_1

    .line 1057678
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/698;->aa:[F

    const/4 v3, 0x0

    aget v0, v0, v3

    iget v3, p0, LX/698;->ab:F

    iget v6, p0, LX/698;->Q:F

    sub-float/2addr v3, v6

    iget v6, p0, LX/698;->W:F

    sub-float/2addr v3, v6

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_5

    iget-object v0, p0, LX/698;->aa:[F

    const/4 v3, 0x0

    aget v0, v0, v3

    iget v3, p0, LX/698;->ab:F

    iget v6, p0, LX/698;->R:F

    add-float/2addr v3, v6

    iget v6, p0, LX/698;->W:F

    add-float/2addr v3, v6

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_5

    iget-object v0, p0, LX/698;->aa:[F

    const/4 v3, 0x1

    aget v0, v0, v3

    iget v3, p0, LX/698;->ac:F

    iget v6, p0, LX/698;->S:F

    sub-float/2addr v3, v6

    iget v6, p0, LX/698;->X:F

    sub-float/2addr v3, v6

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_5

    iget-object v0, p0, LX/698;->aa:[F

    const/4 v3, 0x1

    aget v0, v0, v3

    iget v3, p0, LX/698;->ac:F

    iget v6, p0, LX/698;->T:F

    add-float/2addr v3, v6

    iget v6, p0, LX/698;->X:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-float/2addr v3, v6

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_5

    .line 1057679
    sget-object v0, LX/31U;->h:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v0, v4, v5}, LX/31U;->a(J)V

    move v0, v2

    goto/16 :goto_1

    :cond_5
    sget-object v0, LX/31U;->h:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    move v0, v1

    .line 1057680
    goto/16 :goto_1

    .line 1057681
    :catchall_0
    move-exception v0

    sget-object v1, LX/31U;->h:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    throw v0

    :cond_6
    move v3, v6

    goto/16 :goto_0
.end method

.method public final a()Lcom/facebook/android/maps/model/LatLng;
    .locals 1

    .prologue
    .line 1057633
    iget-boolean v0, p0, LX/698;->N:Z

    if-eqz v0, :cond_0

    .line 1057634
    invoke-direct {p0}, LX/698;->v()V

    .line 1057635
    :cond_0
    iget-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    return-object v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1057636
    iput p1, p0, LX/698;->x:F

    .line 1057637
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057638
    return-void
.end method

.method public final a(LX/68w;)V
    .locals 0

    .prologue
    .line 1057639
    iput-object p1, p0, LX/698;->A:LX/68w;

    .line 1057640
    invoke-direct {p0}, LX/698;->r()V

    .line 1057641
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057642
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1057437
    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    .line 1057438
    :try_start_0
    iget-object v4, p0, LX/698;->A:LX/68w;

    iget-object v4, v4, LX/68w;->a:Landroid/graphics/Bitmap;

    .line 1057439
    iget-boolean v5, p0, LX/698;->K:Z

    move v5, v5

    .line 1057440
    if-eqz v5, :cond_1

    iget-object v5, p0, LX/698;->E:Ljava/lang/String;

    if-nez v5, :cond_0

    iget-object v5, p0, LX/698;->D:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 1057441
    :cond_0
    :goto_0
    invoke-direct {p0}, LX/698;->u()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 1057442
    sget-object v0, LX/31U;->g:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    .line 1057443
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1057444
    goto :goto_0

    .line 1057445
    :cond_2
    :try_start_1
    sget-object v1, LX/698;->p:Landroid/graphics/Paint;

    iget v5, p0, LX/698;->x:F

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1057446
    sget-object v1, LX/698;->o:Landroid/graphics/Matrix;

    iget v5, p0, LX/698;->ab:F

    iget v6, p0, LX/698;->Q:F

    sub-float/2addr v5, v6

    iget v6, p0, LX/698;->ac:F

    iget v7, p0, LX/698;->S:F

    sub-float/2addr v6, v7

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 1057447
    sget-object v1, LX/698;->o:Landroid/graphics/Matrix;

    iget v5, p0, LX/698;->ad:F

    iget v6, p0, LX/698;->ab:F

    iget v7, p0, LX/698;->ac:F

    invoke-virtual {v1, v5, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1057448
    sget-object v1, LX/698;->o:Landroid/graphics/Matrix;

    sget-object v5, LX/698;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1057449
    sget-object v1, LX/698;->p:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1057450
    if-eqz v0, :cond_4

    .line 1057451
    iget-object v0, p0, LX/698;->aa:[F

    const/4 v1, 0x0

    iget v4, p0, LX/698;->U:F

    aput v4, v0, v1

    .line 1057452
    iget-object v0, p0, LX/698;->aa:[F

    const/4 v1, 0x1

    iget v4, p0, LX/698;->V:F

    aput v4, v0, v1

    .line 1057453
    sget-object v0, LX/698;->o:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/698;->aa:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1057454
    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    .line 1057455
    iget v0, p0, LX/698;->Z:I

    div-int/lit8 v1, v0, 0x2

    .line 1057456
    iget-boolean v0, p0, LX/698;->J:Z

    if-eqz v0, :cond_3

    .line 1057457
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1057458
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v1

    sub-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->Y:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1057459
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->Y:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057460
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057461
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    iget v7, p0, LX/698;->v:F

    add-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057462
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057463
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    iget v7, p0, LX/698;->v:F

    sub-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057464
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v7, v1

    sub-float/2addr v6, v7

    iget-object v7, p0, LX/698;->aa:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    iget v8, p0, LX/698;->v:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057465
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1057466
    sget-object v0, LX/698;->p:Landroid/graphics/Paint;

    const/high16 v6, -0x1000000

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057467
    sget-object v0, LX/698;->p:Landroid/graphics/Paint;

    const/high16 v6, 0x41400000    # 12.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, -0x1000000

    invoke-virtual {v0, v6, v7, v8, v9}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1057468
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    sget-object v6, LX/698;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1057469
    sget-object v6, LX/698;->p:Landroid/graphics/Paint;

    iget-boolean v0, p0, LX/698;->L:Z

    if-eqz v0, :cond_5

    const v0, -0x222223

    :goto_2
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057470
    sget-object v0, LX/698;->q:Landroid/graphics/Path;

    sget-object v6, LX/698;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1057471
    :cond_3
    sget-object v0, LX/698;->o:Landroid/graphics/Matrix;

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-float v1, v1

    sub-float v1, v6, v1

    iget-object v6, p0, LX/698;->aa:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    iget v7, p0, LX/698;->Y:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    iget v7, p0, LX/698;->v:F

    sub-float/2addr v6, v7

    invoke-virtual {v0, v1, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 1057472
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1057473
    iget-object v0, p0, LX/698;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v1, LX/698;->o:Landroid/graphics/Matrix;

    sget-object v6, LX/698;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1057474
    sget-object v0, LX/31U;->i:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v0, v4, v5}, LX/31U;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1057475
    :cond_4
    sget-object v0, LX/31U;->g:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    goto/16 :goto_1

    .line 1057476
    :cond_5
    const/4 v0, -0x1

    goto :goto_2

    .line 1057477
    :catchall_0
    move-exception v0

    sget-object v1, LX/31U;->g:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    throw v0
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 1057478
    iput-object p1, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    .line 1057479
    iget-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, LX/31h;->d(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->m:D

    .line 1057480
    iget-object v0, p0, LX/698;->B:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/698;->n:D

    .line 1057481
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057482
    return-void
.end method

.method public final b(FF)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1057369
    iget-boolean v0, p0, LX/698;->M:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/698;->H:LX/67o;

    invoke-interface {v0, p0}, LX/67o;->a(LX/698;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1057370
    :cond_0
    :goto_0
    return v4

    .line 1057371
    :cond_1
    iget-object v0, p0, LX/698;->H:LX/67o;

    invoke-interface {v0, p0}, LX/67o;->b(LX/698;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1057372
    invoke-virtual {p0}, LX/698;->q()V

    .line 1057373
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {p0}, LX/67m;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    invoke-static {v1}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;

    move-result-object v1

    const/16 v2, 0x1f4

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    goto :goto_0
.end method

.method public final c(FF)Z
    .locals 1

    .prologue
    .line 1057374
    iget-object v0, p0, LX/698;->H:LX/67o;

    invoke-interface {v0, p0}, LX/67o;->c(LX/698;)Z

    move-result v0

    return v0
.end method

.method public final d(FF)Z
    .locals 1

    .prologue
    .line 1057375
    iget-boolean v0, p0, LX/698;->N:Z

    if-nez v0, :cond_0

    .line 1057376
    const/4 v0, 0x0

    .line 1057377
    :goto_0
    return v0

    .line 1057378
    :cond_0
    iget v0, p0, LX/698;->O:F

    sub-float v0, p1, v0

    iput v0, p0, LX/698;->ab:F

    .line 1057379
    iget v0, p0, LX/698;->P:F

    sub-float v0, p2, v0

    iput v0, p0, LX/698;->ac:F

    .line 1057380
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057381
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1057382
    invoke-virtual {p0}, LX/698;->p()V

    .line 1057383
    return-void
.end method

.method public final e(FF)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 1057384
    iget-boolean v1, p0, LX/698;->y:Z

    if-nez v1, :cond_0

    .line 1057385
    :goto_0
    return v0

    .line 1057386
    :cond_0
    iput-boolean v7, p0, LX/698;->N:Z

    .line 1057387
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/67m;->a(I)V

    .line 1057388
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-wide v2, p0, LX/67m;->m:D

    iget-wide v4, p0, LX/67m;->n:D

    iget-object v6, p0, LX/67m;->c:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->b(DD[F)V

    .line 1057389
    iget-object v1, p0, LX/67m;->c:[F

    aget v0, v1, v0

    iput v0, p0, LX/698;->ab:F

    .line 1057390
    iget-object v0, p0, LX/67m;->c:[F

    aget v0, v0, v7

    iget v1, p0, LX/698;->u:F

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, LX/698;->ac:F

    .line 1057391
    iget v0, p0, LX/698;->ab:F

    sub-float v0, p1, v0

    iput v0, p0, LX/698;->O:F

    .line 1057392
    iget v0, p0, LX/698;->ac:F

    sub-float v0, p2, v0

    iput v0, p0, LX/698;->P:F

    .line 1057393
    sget-boolean v0, LX/698;->s:Z

    if-nez v0, :cond_1

    .line 1057394
    sput-boolean v7, LX/698;->s:Z

    .line 1057395
    iget-object v0, p0, LX/67m;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1057396
    const-string v1, "android.permission.VIBRATE"

    iget-object v2, p0, LX/67m;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1057397
    if-nez v0, :cond_1

    .line 1057398
    iget-object v0, p0, LX/67m;->g:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    sput-object v0, LX/698;->t:Landroid/os/Vibrator;

    .line 1057399
    :cond_1
    sget-object v0, LX/698;->t:Landroid/os/Vibrator;

    if-eqz v0, :cond_2

    .line 1057400
    sget-object v0, LX/698;->t:Landroid/os/Vibrator;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1057401
    :cond_2
    invoke-virtual {p0}, LX/67m;->f()V

    move v0, v7

    .line 1057402
    goto :goto_0
.end method

.method public final f(FF)V
    .locals 2

    .prologue
    .line 1057432
    iget-object v0, p0, LX/698;->G:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1057433
    iget-object v0, p0, LX/698;->G:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1057434
    invoke-direct {p0}, LX/698;->r()V

    .line 1057435
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057436
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1057403
    iget-boolean v0, p0, LX/698;->K:Z

    move v0, v0

    .line 1057404
    if-eqz v0, :cond_0

    .line 1057405
    invoke-virtual {p0}, LX/698;->p()V

    .line 1057406
    :cond_0
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0, p0}, LX/680;->b(LX/67m;)V

    .line 1057407
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1057408
    iget-boolean v0, p0, LX/698;->M:Z

    if-eqz v0, :cond_0

    .line 1057409
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/698;->L:Z

    .line 1057410
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057411
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1057412
    iget-boolean v0, p0, LX/698;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/698;->L:Z

    if-eqz v0, :cond_0

    .line 1057413
    iput-boolean v1, p0, LX/698;->L:Z

    .line 1057414
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057415
    :cond_0
    iget-boolean v0, p0, LX/698;->N:Z

    if-eqz v0, :cond_1

    .line 1057416
    invoke-direct {p0}, LX/698;->v()V

    .line 1057417
    iput-boolean v1, p0, LX/698;->N:Z

    .line 1057418
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/67m;->a(I)V

    .line 1057419
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057420
    :cond_1
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 1057421
    iget-object v0, p0, LX/698;->H:LX/67o;

    invoke-interface {v0, p0}, LX/67o;->d(LX/698;)V

    .line 1057422
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1057423
    iget-boolean v0, p0, LX/698;->K:Z

    move v0, v0

    .line 1057424
    if-eqz v0, :cond_0

    .line 1057425
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/67m;->a(I)V

    .line 1057426
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/698;->K:Z

    .line 1057427
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 1057428
    invoke-static {p0}, LX/698;->t(LX/698;)V

    .line 1057429
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/67m;->a(I)V

    .line 1057430
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/698;->K:Z

    .line 1057431
    return-void
.end method
