.class public final enum LX/6Fb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Fb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Fb;

.field public static final enum DEFAULT:LX/6Fb;

.field public static final enum FEED_STORY:LX/6Fb;

.field public static final enum MESSENGER_FAILED_TO_SEND_MESSAGE:LX/6Fb;

.field public static final enum MESSENGER_LOCATION_SHARING:LX/6Fb;

.field public static final enum MESSENGER_PLATFORM_CTA:LX/6Fb;

.field public static final enum MESSENGER_SETTINGS:LX/6Fb;

.field public static final enum MESSENGER_SYSTEM_MENU:LX/6Fb;

.field public static final enum MESSENGER_THREAD_SETTINGS:LX/6Fb;

.field public static final enum PMA_UNIFIED_INBOX:LX/6Fb;

.field public static final enum POST_FAILURE:LX/6Fb;

.field public static final enum RAGE_SHAKE:LX/6Fb;

.field public static final enum SETTINGS_REPORT_PROBLEM:LX/6Fb;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1068376
    new-instance v0, LX/6Fb;

    const-string v1, "DEFAULT"

    const-string v2, "FBBugReportSourceDefault"

    invoke-direct {v0, v1, v4, v2}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->DEFAULT:LX/6Fb;

    .line 1068377
    new-instance v0, LX/6Fb;

    const-string v1, "FEED_STORY"

    const-string v2, "FBBugReportSourceFeedStory"

    invoke-direct {v0, v1, v5, v2}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->FEED_STORY:LX/6Fb;

    .line 1068378
    new-instance v0, LX/6Fb;

    const-string v1, "POST_FAILURE"

    const-string v2, "FBBugReportSourcePostFailure"

    invoke-direct {v0, v1, v6, v2}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->POST_FAILURE:LX/6Fb;

    .line 1068379
    new-instance v0, LX/6Fb;

    const-string v1, "RAGE_SHAKE"

    const-string v2, "FBBugReportSourceRageShake"

    invoke-direct {v0, v1, v7, v2}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->RAGE_SHAKE:LX/6Fb;

    .line 1068380
    new-instance v0, LX/6Fb;

    const-string v1, "SETTINGS_REPORT_PROBLEM"

    const-string v2, "FBBugReportSourceSettingsReportProblem"

    invoke-direct {v0, v1, v8, v2}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->SETTINGS_REPORT_PROBLEM:LX/6Fb;

    .line 1068381
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_SYSTEM_MENU"

    const/4 v2, 0x5

    const-string v3, "MessengerSystemMenu"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_SYSTEM_MENU:LX/6Fb;

    .line 1068382
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_SETTINGS"

    const/4 v2, 0x6

    const-string v3, "MessengerSettings"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_SETTINGS:LX/6Fb;

    .line 1068383
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_THREAD_SETTINGS"

    const/4 v2, 0x7

    const-string v3, "MessengerThreadSettings"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_THREAD_SETTINGS:LX/6Fb;

    .line 1068384
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_FAILED_TO_SEND_MESSAGE"

    const/16 v2, 0x8

    const-string v3, "MessengerFailedToSendMessage"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_FAILED_TO_SEND_MESSAGE:LX/6Fb;

    .line 1068385
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_LOCATION_SHARING"

    const/16 v2, 0x9

    const-string v3, "MessengerLocationSharing"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_LOCATION_SHARING:LX/6Fb;

    .line 1068386
    new-instance v0, LX/6Fb;

    const-string v1, "MESSENGER_PLATFORM_CTA"

    const/16 v2, 0xa

    const-string v3, "MessengerPlatformCTA"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->MESSENGER_PLATFORM_CTA:LX/6Fb;

    .line 1068387
    new-instance v0, LX/6Fb;

    const-string v1, "PMA_UNIFIED_INBOX"

    const/16 v2, 0xb

    const-string v3, "FBBugReportInitiateSourceStringPagesCommsHub"

    invoke-direct {v0, v1, v2, v3}, LX/6Fb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Fb;->PMA_UNIFIED_INBOX:LX/6Fb;

    .line 1068388
    const/16 v0, 0xc

    new-array v0, v0, [LX/6Fb;

    sget-object v1, LX/6Fb;->DEFAULT:LX/6Fb;

    aput-object v1, v0, v4

    sget-object v1, LX/6Fb;->FEED_STORY:LX/6Fb;

    aput-object v1, v0, v5

    sget-object v1, LX/6Fb;->POST_FAILURE:LX/6Fb;

    aput-object v1, v0, v6

    sget-object v1, LX/6Fb;->RAGE_SHAKE:LX/6Fb;

    aput-object v1, v0, v7

    sget-object v1, LX/6Fb;->SETTINGS_REPORT_PROBLEM:LX/6Fb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6Fb;->MESSENGER_SYSTEM_MENU:LX/6Fb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6Fb;->MESSENGER_SETTINGS:LX/6Fb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6Fb;->MESSENGER_THREAD_SETTINGS:LX/6Fb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6Fb;->MESSENGER_FAILED_TO_SEND_MESSAGE:LX/6Fb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6Fb;->MESSENGER_LOCATION_SHARING:LX/6Fb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6Fb;->MESSENGER_PLATFORM_CTA:LX/6Fb;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6Fb;->PMA_UNIFIED_INBOX:LX/6Fb;

    aput-object v2, v0, v1

    sput-object v0, LX/6Fb;->$VALUES:[LX/6Fb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1068373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1068374
    iput-object p3, p0, LX/6Fb;->name:Ljava/lang/String;

    .line 1068375
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Fb;
    .locals 1

    .prologue
    .line 1068372
    const-class v0, LX/6Fb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Fb;

    return-object v0
.end method

.method public static values()[LX/6Fb;
    .locals 1

    .prologue
    .line 1068370
    sget-object v0, LX/6Fb;->$VALUES:[LX/6Fb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Fb;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068371
    iget-object v0, p0, LX/6Fb;->name:Ljava/lang/String;

    return-object v0
.end method
