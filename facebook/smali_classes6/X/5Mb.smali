.class public LX/5Mb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/5Mb;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+TTEdge;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 905554
    new-instance v0, LX/5Mb;

    .line 905555
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 905556
    move-object v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    sput-object v0, LX/5Mb;->a:LX/5Mb;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/15i;I)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+TTEdge;>;",
            "LX/15i;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 905545
    const/4 v0, 0x3

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {p2, p3, v0}, LX/15i;->h(II)Z

    move-result v4

    const/4 v0, 0x1

    invoke-virtual {p2, p3, v0}, LX/15i;->h(II)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 905546
    return-void
.end method

.method public constructor <init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+TTEdge;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 905547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905548
    iput-object p1, p0, LX/5Mb;->b:LX/0Px;

    .line 905549
    iput-object p2, p0, LX/5Mb;->c:Ljava/lang/String;

    .line 905550
    iput-object p3, p0, LX/5Mb;->d:Ljava/lang/String;

    .line 905551
    iput-boolean p4, p0, LX/5Mb;->e:Z

    .line 905552
    iput-boolean p5, p0, LX/5Mb;->f:Z

    .line 905553
    return-void
.end method
