.class public final LX/5br;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 960097
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 960098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 960099
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 960100
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 960101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 960102
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 960103
    :goto_1
    move v1, v2

    .line 960104
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 960105
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 960106
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 960107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 960108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 960109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 960110
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 960111
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v3

    goto :goto_2

    .line 960112
    :cond_2
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 960113
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_2

    .line 960114
    :cond_3
    const-string v9, "part"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 960115
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_2

    .line 960116
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 960117
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 960118
    if-eqz v4, :cond_6

    .line 960119
    invoke-virtual {p1, v2, v7, v2}, LX/186;->a(III)V

    .line 960120
    :cond_6
    if-eqz v1, :cond_7

    .line 960121
    invoke-virtual {p1, v3, v6, v2}, LX/186;->a(III)V

    .line 960122
    :cond_7
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 960123
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 960124
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 960125
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 960126
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x2

    const/4 v4, 0x0

    .line 960127
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960128
    invoke-virtual {p0, v1, v4, v4}, LX/15i;->a(III)I

    move-result v2

    .line 960129
    if-eqz v2, :cond_0

    .line 960130
    const-string v3, "length"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960131
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 960132
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 960133
    if-eqz v2, :cond_1

    .line 960134
    const-string v3, "offset"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960135
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 960136
    :cond_1
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 960137
    if-eqz v2, :cond_2

    .line 960138
    const-string v2, "part"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960139
    invoke-virtual {p0, v1, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960140
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 960142
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 960143
    return-void
.end method
