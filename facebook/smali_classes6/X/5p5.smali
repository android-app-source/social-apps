.class public abstract LX/5p5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5p4;


# static fields
.field public static final a:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Lcom/facebook/react/bridge/ReadableNativeArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "LX/5pG;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "Lcom/facebook/react/bridge/Callback;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LX/5or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5or",
            "<",
            "LX/5pW;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/5p1;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007842
    new-instance v0, LX/5os;

    invoke-direct {v0}, LX/5os;-><init>()V

    sput-object v0, LX/5p5;->a:LX/5or;

    .line 1007843
    new-instance v0, LX/5ot;

    invoke-direct {v0}, LX/5ot;-><init>()V

    sput-object v0, LX/5p5;->b:LX/5or;

    .line 1007844
    new-instance v0, LX/5ou;

    invoke-direct {v0}, LX/5ou;-><init>()V

    sput-object v0, LX/5p5;->c:LX/5or;

    .line 1007845
    new-instance v0, LX/5ov;

    invoke-direct {v0}, LX/5ov;-><init>()V

    sput-object v0, LX/5p5;->d:LX/5or;

    .line 1007846
    new-instance v0, LX/5ow;

    invoke-direct {v0}, LX/5ow;-><init>()V

    sput-object v0, LX/5p5;->e:LX/5or;

    .line 1007847
    new-instance v0, LX/5ox;

    invoke-direct {v0}, LX/5ox;-><init>()V

    sput-object v0, LX/5p5;->f:LX/5or;

    .line 1007848
    new-instance v0, LX/5oy;

    invoke-direct {v0}, LX/5oy;-><init>()V

    sput-object v0, LX/5p5;->g:LX/5or;

    .line 1007849
    new-instance v0, LX/5oz;

    invoke-direct {v0}, LX/5oz;-><init>()V

    sput-object v0, LX/5p5;->h:LX/5or;

    .line 1007850
    new-instance v0, LX/5p0;

    invoke-direct {v0}, LX/5p0;-><init>()V

    sput-object v0, LX/5p5;->i:LX/5or;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007841
    return-void
.end method

.method public static c(Ljava/lang/Class;)C
    .locals 3

    .prologue
    .line 1007826
    invoke-static {p0}, LX/5p5;->e(Ljava/lang/Class;)C

    move-result v0

    .line 1007827
    if-eqz v0, :cond_0

    .line 1007828
    :goto_0
    return v0

    .line 1007829
    :cond_0
    const-class v0, Lcom/facebook/react/bridge/ExecutorToken;

    if-ne p0, v0, :cond_1

    .line 1007830
    const/16 v0, 0x54

    goto :goto_0

    .line 1007831
    :cond_1
    const-class v0, Lcom/facebook/react/bridge/Callback;

    if-ne p0, v0, :cond_2

    .line 1007832
    const/16 v0, 0x58

    goto :goto_0

    .line 1007833
    :cond_2
    const-class v0, LX/5pW;

    if-ne p0, v0, :cond_3

    .line 1007834
    const/16 v0, 0x50

    goto :goto_0

    .line 1007835
    :cond_3
    const-class v0, LX/5pG;

    if-ne p0, v0, :cond_4

    .line 1007836
    const/16 v0, 0x4d

    goto :goto_0

    .line 1007837
    :cond_4
    const-class v0, LX/5pC;

    if-ne p0, v0, :cond_5

    .line 1007838
    const/16 v0, 0x41

    goto :goto_0

    .line 1007839
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown param class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d(Ljava/lang/Class;)C
    .locals 3

    .prologue
    .line 1007816
    invoke-static {p0}, LX/5p5;->e(Ljava/lang/Class;)C

    move-result v0

    .line 1007817
    if-eqz v0, :cond_0

    .line 1007818
    :goto_0
    return v0

    .line 1007819
    :cond_0
    sget-object v0, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    if-ne p0, v0, :cond_1

    .line 1007820
    const/16 v0, 0x76

    goto :goto_0

    .line 1007821
    :cond_1
    const-class v0, LX/5pH;

    if-ne p0, v0, :cond_2

    .line 1007822
    const/16 v0, 0x4d

    goto :goto_0

    .line 1007823
    :cond_2
    const-class v0, LX/5pD;

    if-ne p0, v0, :cond_3

    .line 1007824
    const/16 v0, 0x41

    goto :goto_0

    .line 1007825
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown return class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e(Ljava/lang/Class;)C
    .locals 1

    .prologue
    .line 1007796
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne p0, v0, :cond_0

    .line 1007797
    const/16 v0, 0x7a

    .line 1007798
    :goto_0
    return v0

    .line 1007799
    :cond_0
    const-class v0, Ljava/lang/Boolean;

    if-ne p0, v0, :cond_1

    .line 1007800
    const/16 v0, 0x5a

    goto :goto_0

    .line 1007801
    :cond_1
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p0, v0, :cond_2

    .line 1007802
    const/16 v0, 0x69

    goto :goto_0

    .line 1007803
    :cond_2
    const-class v0, Ljava/lang/Integer;

    if-ne p0, v0, :cond_3

    .line 1007804
    const/16 v0, 0x49

    goto :goto_0

    .line 1007805
    :cond_3
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne p0, v0, :cond_4

    .line 1007806
    const/16 v0, 0x64

    goto :goto_0

    .line 1007807
    :cond_4
    const-class v0, Ljava/lang/Double;

    if-ne p0, v0, :cond_5

    .line 1007808
    const/16 v0, 0x44

    goto :goto_0

    .line 1007809
    :cond_5
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne p0, v0, :cond_6

    .line 1007810
    const/16 v0, 0x66

    goto :goto_0

    .line 1007811
    :cond_6
    const-class v0, Ljava/lang/Float;

    if-ne p0, v0, :cond_7

    .line 1007812
    const/16 v0, 0x46

    goto :goto_0

    .line 1007813
    :cond_7
    const-class v0, Ljava/lang/String;

    if-ne p0, v0, :cond_8

    .line 1007814
    const/16 v0, 0x53

    goto :goto_0

    .line 1007815
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x2000

    .line 1007777
    iget-object v0, p0, LX/5p5;->j:Ljava/util/Map;

    if-nez v0, :cond_7

    .line 1007778
    const-string v0, "findMethods"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1007779
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5p5;->j:Ljava/util/Map;

    .line 1007780
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5p5;->k:Ljava/util/Map;

    .line 1007781
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1007782
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 1007783
    const-class v4, Lcom/facebook/react/bridge/ReactMethod;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1007784
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1007785
    iget-object v5, p0, LX/5p5;->k:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, LX/5p5;->j:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1007786
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Java Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sync method name already registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007787
    :cond_1
    iget-object v5, p0, LX/5p5;->j:Ljava/util/Map;

    new-instance v6, LX/5p2;

    invoke-direct {v6, p0, v3}, LX/5p2;-><init>(LX/5p5;Ljava/lang/reflect/Method;)V

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007788
    :cond_2
    const-class v4, Lcom/facebook/react/bridge/ReactSyncHook;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1007789
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1007790
    iget-object v5, p0, LX/5p5;->k:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, LX/5p5;->j:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1007791
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Java Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sync method name already registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007792
    :cond_4
    iget-object v5, p0, LX/5p5;->k:Ljava/util/Map;

    new-instance v6, LX/5p3;

    invoke-direct {v6, p0, v3}, LX/5p3;-><init>(LX/5p5;Ljava/lang/reflect/Method;)V

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007793
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1007794
    :cond_6
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1007795
    :cond_7
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1007768
    const/4 v0, 0x0

    return-object v0
.end method

.method public bP_()Z
    .locals 1

    .prologue
    .line 1007776
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1007774
    invoke-direct {p0}, LX/5p5;->h()V

    .line 1007775
    iget-object v0, p0, LX/5p5;->k:Ljava/util/Map;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1007773
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1007772
    const/4 v0, 0x0

    return v0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 1007771
    return-void
.end method

.method public final gh_()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/5p1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1007769
    invoke-direct {p0}, LX/5p5;->h()V

    .line 1007770
    iget-object v0, p0, LX/5p5;->j:Ljava/util/Map;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
