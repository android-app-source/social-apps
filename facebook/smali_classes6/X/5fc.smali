.class public final LX/5fc;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/optic/CameraPreviewView;

.field private b:I

.field private c:I

.field private d:F


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;)V
    .locals 0

    .prologue
    .line 971748
    iput-object p1, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/optic/CameraPreviewView;B)V
    .locals 0

    .prologue
    .line 971749
    invoke-direct {p0, p1}, LX/5fc;-><init>(Lcom/facebook/optic/CameraPreviewView;)V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 971750
    iget-object v1, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v1, v1, Lcom/facebook/optic/CameraPreviewView;->f:Z

    if-eqz v1, :cond_0

    .line 971751
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 971752
    invoke-virtual {v1}, LX/5fQ;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 971753
    :cond_0
    :goto_0
    return v0

    .line 971754
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    .line 971755
    iget v2, p0, LX/5fc;->d:F

    sub-float/2addr v1, v2

    iget-object v2, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v2}, Lcom/facebook/optic/CameraPreviewView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, LX/5fc;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, LX/5fc;->b:I

    add-int/2addr v1, v2

    .line 971756
    iget v2, p0, LX/5fc;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 971757
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 971758
    invoke-virtual {v1, v0}, LX/5fQ;->c(I)V

    .line 971759
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 971760
    sget-object v2, LX/5fQ;->b:LX/5fQ;

    move-object v2, v2

    .line 971761
    iget-boolean v3, v2, LX/5fQ;->m:Z

    move v2, v3

    .line 971762
    if-eqz v2, :cond_1

    .line 971763
    :cond_0
    :goto_0
    return v0

    .line 971764
    :cond_1
    iget-object v2, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v2, v2, Lcom/facebook/optic/CameraPreviewView;->f:Z

    if-eqz v2, :cond_0

    .line 971765
    sget-object v2, LX/5fQ;->b:LX/5fQ;

    move-object v2, v2

    .line 971766
    invoke-virtual {v2}, LX/5fQ;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 971767
    iget-object v0, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 971768
    if-eqz v0, :cond_2

    .line 971769
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 971770
    :cond_2
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971771
    invoke-virtual {v0}, LX/5fQ;->u()I

    move-result v0

    iput v0, p0, LX/5fc;->b:I

    .line 971772
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 971773
    invoke-virtual {v0}, LX/5fQ;->v()I

    move-result v0

    iput v0, p0, LX/5fc;->c:I

    .line 971774
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, LX/5fc;->d:F

    .line 971775
    iget-object v0, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    if-eqz v0, :cond_3

    .line 971776
    iget-object v0, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    invoke-interface {v0}, LX/5ff;->a()V

    :cond_3
    move v0, v1

    .line 971777
    goto :goto_0
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 1

    .prologue
    .line 971778
    iget-object v0, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    if-eqz v0, :cond_0

    .line 971779
    iget-object v0, p0, LX/5fc;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    invoke-interface {v0}, LX/5ff;->b()V

    .line 971780
    :cond_0
    return-void
.end method
