.class public LX/63E;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private final a:F

.field private final b:F

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/RectF;

.field private final f:Landroid/graphics/Paint$FontMetrics;

.field private g:F

.field private h:Ljava/lang/Integer;


# direct methods
.method private constructor <init>(FFI)V
    .locals 1

    .prologue
    .line 1042979
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 1042980
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/63E;->d:Landroid/graphics/Rect;

    .line 1042981
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/63E;->e:Landroid/graphics/RectF;

    .line 1042982
    new-instance v0, Landroid/graphics/Paint$FontMetrics;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    iput-object v0, p0, LX/63E;->f:Landroid/graphics/Paint$FontMetrics;

    .line 1042983
    iput p1, p0, LX/63E;->a:F

    .line 1042984
    iput p2, p0, LX/63E;->b:F

    .line 1042985
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/63E;->c:Landroid/graphics/Paint;

    .line 1042986
    iget-object v0, p0, LX/63E;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1042987
    return-void
.end method

.method public constructor <init>(FFILjava/lang/Integer;)V
    .locals 0
    .param p4    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1042988
    invoke-direct {p0, p1, p2, p3}, LX/63E;-><init>(FFI)V

    .line 1042989
    iput-object p4, p0, LX/63E;->h:Ljava/lang/Integer;

    .line 1042990
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 9

    .prologue
    .line 1042991
    iget-object v1, p0, LX/63E;->f:Landroid/graphics/Paint$FontMetrics;

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    .line 1042992
    iget-object v1, p0, LX/63E;->f:Landroid/graphics/Paint$FontMetrics;

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget-object v2, p0, LX/63E;->f:Landroid/graphics/Paint$FontMetrics;

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, LX/63E;->a:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 1042993
    move/from16 v0, p7

    int-to-float v2, v0

    iget-object v3, p0, LX/63E;->f:Landroid/graphics/Paint$FontMetrics;

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    add-float/2addr v2, v3

    iget v3, p0, LX/63E;->a:F

    sub-float/2addr v2, v3

    .line 1042994
    iget-object v3, p0, LX/63E;->e:Landroid/graphics/RectF;

    iget v4, p0, LX/63E;->g:F

    add-float/2addr v4, p5

    add-float v5, v2, v1

    invoke-virtual {v3, p5, v2, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1042995
    iget-object v2, p0, LX/63E;->e:Landroid/graphics/RectF;

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v1, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    iget-object v4, p0, LX/63E;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1042996
    iget-object v1, p0, LX/63E;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1042997
    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getColor()I

    move-result v8

    .line 1042998
    iget-object v1, p0, LX/63E;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1042999
    iget v1, p0, LX/63E;->b:F

    add-float v5, p5, v1

    move/from16 v0, p7

    int-to-float v6, v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 1043000
    move-object/from16 v0, p9

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 1043001
    :goto_0
    return-void

    .line 1043002
    :cond_0
    iget v1, p0, LX/63E;->b:F

    add-float v5, p5, v1

    move/from16 v0, p7

    int-to-float v6, v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 3

    .prologue
    .line 1043003
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/63E;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1043004
    iget-object v0, p0, LX/63E;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, LX/63E;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/63E;->g:F

    .line 1043005
    iget v0, p0, LX/63E;->g:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
