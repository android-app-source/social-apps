.class public final LX/60r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039342
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)V
    .locals 1

    .prologue
    .line 1039343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039344
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    iput v0, p0, LX/60r;->a:I

    .line 1039345
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    iput v0, p0, LX/60r;->b:I

    .line 1039346
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    iput v0, p0, LX/60r;->c:I

    .line 1039347
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    iput v0, p0, LX/60r;->d:I

    .line 1039348
    iget-boolean v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->allowBFrames:Z

    iput-boolean v0, p0, LX/60r;->e:Z

    .line 1039349
    iget-object v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    iput-object v0, p0, LX/60r;->f:Ljava/lang/String;

    .line 1039350
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    iput v0, p0, LX/60r;->g:I

    .line 1039351
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .locals 2

    .prologue
    .line 1039352
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-direct {v0, p0}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;-><init>(LX/60r;)V

    return-object v0
.end method
