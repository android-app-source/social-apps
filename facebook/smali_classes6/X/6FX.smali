.class public final LX/6FX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/6Fb;

.field public c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1068137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068138
    sget-object v0, LX/6Fb;->DEFAULT:LX/6Fb;

    iput-object v0, p0, LX/6FX;->b:LX/6Fb;

    .line 1068139
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1068140
    iput-object v0, p0, LX/6FX;->c:LX/0Rf;

    .line 1068141
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/6FX;->d:LX/0am;

    .line 1068142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6FX;->e:Ljava/util/List;

    .line 1068143
    return-void
.end method


# virtual methods
.method public final a(LX/0Rf;)LX/6FX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;)",
            "LX/6FX;"
        }
    .end annotation

    .prologue
    .line 1068144
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, LX/6FX;->c:LX/0Rf;

    .line 1068145
    return-object p0
.end method

.method public final a(LX/6Fb;)LX/6FX;
    .locals 1

    .prologue
    .line 1068146
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Fb;

    iput-object v0, p0, LX/6FX;->b:LX/6Fb;

    .line 1068147
    return-object p0
.end method

.method public final a(Landroid/content/Context;)LX/6FX;
    .locals 1

    .prologue
    .line 1068148
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/6FX;->a:Landroid/content/Context;

    .line 1068149
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)LX/6FX;
    .locals 1

    .prologue
    .line 1068150
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/6FX;->d:LX/0am;

    .line 1068151
    return-object p0
.end method

.method public final a()LX/6FY;
    .locals 2

    .prologue
    .line 1068152
    new-instance v0, LX/6FY;

    invoke-direct {v0, p0}, LX/6FY;-><init>(LX/6FX;)V

    return-object v0
.end method
