.class public abstract LX/67m;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/67m;",
            ">;"
        }
    .end annotation
.end field

.field private static o:I


# instance fields
.field public final b:I

.field public final c:[F

.field public final d:F

.field public final e:LX/680;

.field public final f:LX/31h;

.field public final g:Landroid/content/Context;

.field public final h:I

.field public i:Z

.field public j:I

.field public k:F

.field public l:Z

.field public m:D

.field public n:D

.field private final p:LX/31i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1053504
    new-instance v0, LX/683;

    invoke-direct {v0}, LX/683;-><init>()V

    sput-object v0, LX/67m;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/680;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1053487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1053488
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/67m;->c:[F

    .line 1053489
    iput-boolean v1, p0, LX/67m;->i:Z

    .line 1053490
    iput v1, p0, LX/67m;->j:I

    .line 1053491
    iput-boolean v1, p0, LX/67m;->l:Z

    .line 1053492
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/67m;->p:LX/31i;

    .line 1053493
    sget v0, LX/67m;->o:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LX/67m;->o:I

    iput v0, p0, LX/67m;->b:I

    .line 1053494
    iput-object p1, p0, LX/67m;->e:LX/680;

    .line 1053495
    iget-object v0, p1, LX/680;->k:LX/31h;

    move-object v0, v0

    .line 1053496
    iput-object v0, p0, LX/67m;->f:LX/31h;

    .line 1053497
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1053498
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1053499
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/67m;->g:Landroid/content/Context;

    .line 1053500
    iget-object v0, p0, LX/67m;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, LX/67m;->d:F

    .line 1053501
    iget v0, p1, LX/680;->B:I

    move v0, v0

    .line 1053502
    iput v0, p0, LX/67m;->h:I

    .line 1053503
    return-void
.end method


# virtual methods
.method public a(FF)I
    .locals 1

    .prologue
    .line 1053486
    const/4 v0, 0x0

    return v0
.end method

.method public a()Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 1053485
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, p0, LX/67m;->n:D

    invoke-static {v2, v3}, LX/31h;->a(D)D

    move-result-wide v2

    iget-wide v4, p0, LX/67m;->m:D

    invoke-static {v4, v5}, LX/31h;->c(D)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1053481
    iget v0, p0, LX/67m;->j:I

    if-eq v0, p1, :cond_0

    .line 1053482
    iput p1, p0, LX/67m;->j:I

    .line 1053483
    invoke-virtual {p0}, LX/67m;->o()V

    .line 1053484
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/graphics/Canvas;)V
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1053478
    iput-boolean p1, p0, LX/67m;->i:Z

    .line 1053479
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1053480
    return-void
.end method

.method public final a(LX/31i;[F)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1053472
    iget-object v2, p0, LX/67m;->f:LX/31h;

    iget-object v3, p0, LX/67m;->p:LX/31i;

    invoke-virtual {v2, v3}, LX/31h;->a(LX/31i;)V

    .line 1053473
    iget-wide v2, p1, LX/31i;->b:D

    iget-object v4, p0, LX/67m;->p:LX/31i;

    iget-wide v4, v4, LX/31i;->a:D

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    iget-wide v2, p1, LX/31i;->a:D

    iget-object v4, p0, LX/67m;->p:LX/31i;

    iget-wide v4, v4, LX/31i;->b:D

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1053474
    :cond_1
    :goto_0
    return v0

    .line 1053475
    :cond_2
    iget-object v2, p0, LX/67m;->p:LX/31i;

    iget-wide v2, v2, LX/31i;->c:D

    iget-wide v4, p1, LX/31i;->d:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    aput v2, p2, v1

    .line 1053476
    iget-object v2, p0, LX/67m;->p:LX/31i;

    iget-wide v2, v2, LX/31i;->d:D

    iget-wide v4, p1, LX/31i;->c:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    aput v2, p2, v0

    .line 1053477
    aget v2, p2, v1

    aget v3, p2, v0

    cmpg-float v2, v2, v3

    if-lez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 1053471
    return-void
.end method

.method public b(FF)Z
    .locals 1

    .prologue
    .line 1053470
    const/4 v0, 0x0

    return v0
.end method

.method public c(FF)Z
    .locals 1

    .prologue
    .line 1053454
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1053469
    return-void
.end method

.method public d(FF)Z
    .locals 1

    .prologue
    .line 1053468
    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1053467
    return-void
.end method

.method public e(FF)Z
    .locals 1

    .prologue
    .line 1053466
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1053462
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1053463
    iget-object p0, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, p0

    .line 1053464
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1053465
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 1053460
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0, p0}, LX/680;->b(LX/67m;)V

    .line 1053461
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 1053459
    return-void
.end method

.method public n()V
    .locals 0

    .prologue
    .line 1053458
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 1053455
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0, p0}, LX/680;->b(LX/67m;)V

    .line 1053456
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0, p0}, LX/680;->a(LX/67m;)LX/67m;

    .line 1053457
    return-void
.end method
