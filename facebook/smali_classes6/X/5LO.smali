.class public final LX/5LO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 901030
    const/16 v26, 0x0

    .line 901031
    const/16 v25, 0x0

    .line 901032
    const/16 v24, 0x0

    .line 901033
    const/16 v23, 0x0

    .line 901034
    const/16 v22, 0x0

    .line 901035
    const/16 v21, 0x0

    .line 901036
    const/16 v20, 0x0

    .line 901037
    const/16 v19, 0x0

    .line 901038
    const/16 v18, 0x0

    .line 901039
    const/16 v17, 0x0

    .line 901040
    const/16 v16, 0x0

    .line 901041
    const/4 v15, 0x0

    .line 901042
    const/4 v14, 0x0

    .line 901043
    const/4 v13, 0x0

    .line 901044
    const/4 v12, 0x0

    .line 901045
    const/4 v11, 0x0

    .line 901046
    const/4 v10, 0x0

    .line 901047
    const/4 v9, 0x0

    .line 901048
    const/4 v8, 0x0

    .line 901049
    const/4 v7, 0x0

    .line 901050
    const/4 v6, 0x0

    .line 901051
    const/4 v5, 0x0

    .line 901052
    const/4 v4, 0x0

    .line 901053
    const/4 v3, 0x0

    .line 901054
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 901055
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 901056
    const/4 v3, 0x0

    .line 901057
    :goto_0
    return v3

    .line 901058
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 901059
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_14

    .line 901060
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 901061
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 901062
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 901063
    const-string v28, "all_icons"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 901064
    invoke-static/range {p0 .. p1}, LX/5Lm;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 901065
    :cond_2
    const-string v28, "glyph"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 901066
    invoke-static/range {p0 .. p1}, LX/5Lj;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 901067
    :cond_3
    const-string v28, "iconImageLarge"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 901068
    invoke-static/range {p0 .. p1}, LX/5Lk;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 901069
    :cond_4
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 901070
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 901071
    :cond_5
    const-string v28, "is_linking_verb"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 901072
    const/4 v7, 0x1

    .line 901073
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 901074
    :cond_6
    const-string v28, "legacy_api_id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 901075
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 901076
    :cond_7
    const-string v28, "prefetch_priority"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 901077
    const/4 v6, 0x1

    .line 901078
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto/16 :goto_1

    .line 901079
    :cond_8
    const-string v28, "present_participle"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 901080
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 901081
    :cond_9
    const-string v28, "previewTemplateAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 901082
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 901083
    :cond_a
    const-string v28, "previewTemplateNoTags"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 901084
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 901085
    :cond_b
    const-string v28, "previewTemplateWithPeople"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 901086
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 901087
    :cond_c
    const-string v28, "previewTemplateWithPeopleAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 901088
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 901089
    :cond_d
    const-string v28, "previewTemplateWithPerson"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 901090
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 901091
    :cond_e
    const-string v28, "previewTemplateWithPersonAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 901092
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 901093
    :cond_f
    const-string v28, "prompt"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 901094
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 901095
    :cond_10
    const-string v28, "supports_audio_suggestions"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 901096
    const/4 v5, 0x1

    .line 901097
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 901098
    :cond_11
    const-string v28, "supports_freeform"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 901099
    const/4 v4, 0x1

    .line 901100
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 901101
    :cond_12
    const-string v28, "supports_offline_posting"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_13

    .line 901102
    const/4 v3, 0x1

    .line 901103
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 901104
    :cond_13
    const-string v28, "taggable_activity_suggestions"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 901105
    invoke-static/range {p0 .. p1}, LX/5LP;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 901106
    :cond_14
    const/16 v27, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 901107
    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901108
    const/16 v26, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901109
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901110
    const/16 v24, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901111
    if-eqz v7, :cond_15

    .line 901112
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 901113
    :cond_15
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 901114
    if-eqz v6, :cond_16

    .line 901115
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 901116
    :cond_16
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901117
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901118
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901119
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901120
    const/16 v6, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 901121
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 901122
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 901123
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 901124
    if-eqz v5, :cond_17

    .line 901125
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 901126
    :cond_17
    if-eqz v4, :cond_18

    .line 901127
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 901128
    :cond_18
    if-eqz v3, :cond_19

    .line 901129
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 901130
    :cond_19
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 901131
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
