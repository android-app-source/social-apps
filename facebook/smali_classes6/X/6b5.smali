.class public final LX/6b5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/textinput/FigEditText;

.field public final synthetic b:LX/3Ag;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/3BS;


# direct methods
.method public constructor <init>(LX/3BS;Lcom/facebook/fig/textinput/FigEditText;LX/3Ag;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1112892
    iput-object p1, p0, LX/6b5;->d:LX/3BS;

    iput-object p2, p0, LX/6b5;->a:Lcom/facebook/fig/textinput/FigEditText;

    iput-object p3, p0, LX/6b5;->b:LX/3Ag;

    iput-object p4, p0, LX/6b5;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2d7e2f25

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1112893
    iget-object v1, p0, LX/6b5;->d:LX/3BS;

    iget-object v2, p0, LX/6b5;->a:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1112894
    iput-object v2, v1, LX/3BS;->m:Ljava/lang/String;

    .line 1112895
    iget-object v1, p0, LX/6b5;->d:LX/3BS;

    const-string v2, "submit_comment"

    .line 1112896
    iput-object v2, v1, LX/3BS;->k:Ljava/lang/String;

    .line 1112897
    iget-object v1, p0, LX/6b5;->b:LX/3Ag;

    invoke-virtual {v1}, LX/3Ag;->dismiss()V

    .line 1112898
    iget-object v1, p0, LX/6b5;->d:LX/3BS;

    iget-object v1, v1, LX/3BT;->a:LX/31X;

    iget-object v2, p0, LX/6b5;->d:LX/3BS;

    iget-object v2, v2, LX/3BS;->i:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/31X;->a(Ljava/lang/CharSequence;)LX/31X;

    move-result-object v1

    iget-object v2, p0, LX/6b5;->d:LX/3BS;

    iget-object v2, v2, LX/3BS;->j:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/31X;->b(Ljava/lang/CharSequence;)LX/31X;

    move-result-object v1

    iget-object v2, p0, LX/6b5;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/6b4;

    invoke-direct {v3, p0}, LX/6b4;-><init>(LX/6b5;)V

    invoke-interface {v1, v2, v3}, LX/31X;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;

    move-result-object v1

    invoke-interface {v1}, LX/31X;->a()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 1112899
    const v1, -0xa0dbce6

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
