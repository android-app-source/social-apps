.class public LX/6Kc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static e:LX/6Kc;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5PO;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field public c:I

.field public d:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1076889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076890
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    .line 1076891
    return-void
.end method

.method public static a()LX/6Kc;
    .locals 1

    .prologue
    .line 1076892
    sget-object v0, LX/6Kc;->e:LX/6Kc;

    if-nez v0, :cond_0

    .line 1076893
    new-instance v0, LX/6Kc;

    invoke-direct {v0}, LX/6Kc;-><init>()V

    sput-object v0, LX/6Kc;->e:LX/6Kc;

    .line 1076894
    :cond_0
    sget-object v0, LX/6Kc;->e:LX/6Kc;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1076895
    monitor-enter p0

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    :try_start_0
    const-string v1, "Non zero width and height required"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1076896
    iput p1, p0, LX/6Kc;->c:I

    .line 1076897
    iput p2, p0, LX/6Kc;->d:I

    .line 1076898
    const/4 v0, 0x0

    iput v0, p0, LX/6Kc;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076899
    monitor-exit p0

    return-void

    .line 1076900
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/5PO;)V
    .locals 1

    .prologue
    .line 1076901
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1076902
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076903
    iget v0, p0, LX/6Kc;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/6Kc;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076904
    :cond_0
    monitor-exit p0

    return-void

    .line 1076905
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1076906
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/6Kc;->b:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "There are allocated frame buffers unaccounted for, we\'re leaking!"

    invoke-static {v0, v1}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 1076907
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5PO;

    .line 1076908
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1076909
    iget-object v2, v0, LX/5PO;->d:LX/5Pf;

    if-eqz v2, :cond_2

    .line 1076910
    new-array v2, v5, [I

    .line 1076911
    iget v3, v0, LX/5PO;->c:I

    aput v3, v2, v4

    .line 1076912
    invoke-static {v5, v2, v4}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    .line 1076913
    iget-object v2, v0, LX/5PO;->e:[I

    if-eqz v2, :cond_1

    .line 1076914
    iget-object v2, v0, LX/5PO;->e:[I

    invoke-static {v5, v2, v4}, Landroid/opengl/GLES20;->glDeleteRenderbuffers(I[II)V

    .line 1076915
    iput-object v6, v0, LX/5PO;->e:[I

    .line 1076916
    :cond_1
    iget-object v2, v0, LX/5PO;->d:LX/5Pf;

    invoke-virtual {v2}, LX/5Pf;->a()V

    .line 1076917
    iput-object v6, v0, LX/5PO;->d:LX/5Pf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076918
    :cond_2
    goto :goto_0

    .line 1076919
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1076920
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1076921
    const/4 v0, 0x0

    iput v0, p0, LX/6Kc;->d:I

    iput v0, p0, LX/6Kc;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1076922
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized e()LX/5PO;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1076923
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/6Kc;->f()Z

    move-result v1

    const-string v2, "Frame buffer provider not initialized"

    invoke-static {v1, v2}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 1076924
    iget v1, p0, LX/6Kc;->b:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Using more than the expected # of framebuffers"

    invoke-static {v0, v1}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 1076925
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1076926
    iget-object v0, p0, LX/6Kc;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5PO;

    .line 1076927
    :goto_0
    iget v1, p0, LX/6Kc;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/6Kc;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076928
    monitor-exit p0

    return-object v0

    .line 1076929
    :cond_1
    :try_start_1
    new-instance v0, LX/5PO;

    iget v1, p0, LX/6Kc;->c:I

    iget v2, p0, LX/6Kc;->d:I

    invoke-direct {v0, v1, v2}, LX/5PO;-><init>(II)V

    .line 1076930
    invoke-virtual {v0}, LX/5PO;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1076931
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 1076932
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/6Kc;->c:I

    if-lez v0, :cond_0

    iget v0, p0, LX/6Kc;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
