.class public final LX/57e;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 841875
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 841876
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841877
    :goto_0
    return v1

    .line 841878
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 841879
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 841880
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 841881
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 841882
    const-string v10, "height"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 841883
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 841884
    :cond_1
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 841885
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 841886
    :cond_2
    const-string v10, "playable_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 841887
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 841888
    :cond_3
    const-string v10, "video_preview_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 841889
    invoke-static {p0, p1}, LX/57d;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 841890
    :cond_4
    const-string v10, "width"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 841891
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 841892
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 841893
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 841894
    if-eqz v3, :cond_7

    .line 841895
    invoke-virtual {p1, v1, v8, v1}, LX/186;->a(III)V

    .line 841896
    :cond_7
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 841897
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 841898
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 841899
    if-eqz v0, :cond_8

    .line 841900
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 841901
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 841902
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 841903
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 841904
    if-eqz v0, :cond_0

    .line 841905
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841906
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 841907
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841908
    if-eqz v0, :cond_1

    .line 841909
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841910
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841911
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841912
    if-eqz v0, :cond_2

    .line 841913
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841914
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841915
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841916
    if-eqz v0, :cond_3

    .line 841917
    const-string v1, "video_preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841918
    invoke-static {p0, v0, p2}, LX/57d;->a(LX/15i;ILX/0nX;)V

    .line 841919
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 841920
    if-eqz v0, :cond_4

    .line 841921
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841922
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 841923
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 841924
    return-void
.end method
