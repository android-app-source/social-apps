.class public final enum LX/5nc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nc;

.field public static final enum CHOSE_FIRST_OPTION:LX/5nc;

.field public static final enum CHOSE_OTHER_OPTION:LX/5nc;

.field public static final enum CHOSE_SECOND_OPTION:LX/5nc;

.field public static final enum DISMISSED:LX/5nc;

.field public static final enum EXPOSED:LX/5nc;

.field public static final enum MORE_OPTIONS:LX/5nc;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004437
    new-instance v0, LX/5nc;

    const-string v1, "EXPOSED"

    const-string v2, "exposed"

    invoke-direct {v0, v1, v4, v2}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->EXPOSED:LX/5nc;

    .line 1004438
    new-instance v0, LX/5nc;

    const-string v1, "CHOSE_FIRST_OPTION"

    const-string v2, "chose_first_option"

    invoke-direct {v0, v1, v5, v2}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->CHOSE_FIRST_OPTION:LX/5nc;

    .line 1004439
    new-instance v0, LX/5nc;

    const-string v1, "CHOSE_SECOND_OPTION"

    const-string v2, "chose_second_option"

    invoke-direct {v0, v1, v6, v2}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->CHOSE_SECOND_OPTION:LX/5nc;

    .line 1004440
    new-instance v0, LX/5nc;

    const-string v1, "CHOSE_OTHER_OPTION"

    const-string v2, "chose_other_option"

    invoke-direct {v0, v1, v7, v2}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->CHOSE_OTHER_OPTION:LX/5nc;

    .line 1004441
    new-instance v0, LX/5nc;

    const-string v1, "MORE_OPTIONS"

    const-string v2, "more_options"

    invoke-direct {v0, v1, v8, v2}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->MORE_OPTIONS:LX/5nc;

    .line 1004442
    new-instance v0, LX/5nc;

    const-string v1, "DISMISSED"

    const/4 v2, 0x5

    const-string v3, "dismissed"

    invoke-direct {v0, v1, v2, v3}, LX/5nc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nc;->DISMISSED:LX/5nc;

    .line 1004443
    const/4 v0, 0x6

    new-array v0, v0, [LX/5nc;

    sget-object v1, LX/5nc;->EXPOSED:LX/5nc;

    aput-object v1, v0, v4

    sget-object v1, LX/5nc;->CHOSE_FIRST_OPTION:LX/5nc;

    aput-object v1, v0, v5

    sget-object v1, LX/5nc;->CHOSE_SECOND_OPTION:LX/5nc;

    aput-object v1, v0, v6

    sget-object v1, LX/5nc;->CHOSE_OTHER_OPTION:LX/5nc;

    aput-object v1, v0, v7

    sget-object v1, LX/5nc;->MORE_OPTIONS:LX/5nc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nc;->DISMISSED:LX/5nc;

    aput-object v2, v0, v1

    sput-object v0, LX/5nc;->$VALUES:[LX/5nc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004434
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004435
    iput-object p3, p0, LX/5nc;->eventName:Ljava/lang/String;

    .line 1004436
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nc;
    .locals 1

    .prologue
    .line 1004433
    const-class v0, LX/5nc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nc;

    return-object v0
.end method

.method public static values()[LX/5nc;
    .locals 1

    .prologue
    .line 1004432
    sget-object v0, LX/5nc;->$VALUES:[LX/5nc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nc;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004431
    iget-object v0, p0, LX/5nc;->eventName:Ljava/lang/String;

    return-object v0
.end method
