.class public LX/6Bu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/6Bt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1063232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063233
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/6Bu;->a:Ljava/util/Set;

    .line 1063234
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Bt;

    .line 1063235
    invoke-interface {v0, p0}, LX/6Bt;->a(LX/6Bu;)V

    goto :goto_0

    .line 1063236
    :cond_0
    iget-object v0, p0, LX/6Bu;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "AttachmentStyleUtil has an empty supported style list."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1063237
    return-void

    .line 1063238
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/6Bu;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1063239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/6Bu;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)LX/6Bu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;)",
            "LX/6Bu;"
        }
    .end annotation

    .prologue
    .line 1063240
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1063241
    iget-object v2, p0, LX/6Bu;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1063242
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;"
        }
    .end annotation

    .prologue
    .line 1063243
    const/4 v0, 0x0

    .line 1063244
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1063245
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1063246
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v2, :cond_0

    .line 1063247
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1063248
    :goto_0
    move-object v0, v1

    .line 1063249
    return-object v0

    .line 1063250
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1063251
    add-int/lit8 v2, v2, 0x1

    move v3, v2

    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 1063252
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1063253
    if-eqz v2, :cond_3

    invoke-static {p0, v2}, LX/6Bu;->a(LX/6Bu;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1063254
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1063255
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1063256
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1063257
    :cond_2
    move-object v1, v2

    .line 1063258
    goto :goto_0

    .line 1063259
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1063260
    :cond_4
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0
.end method
