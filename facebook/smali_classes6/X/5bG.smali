.class public final LX/5bG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 53

    .prologue
    .line 957456
    const/16 v47, 0x0

    .line 957457
    const/16 v46, 0x0

    .line 957458
    const/16 v45, 0x0

    .line 957459
    const/16 v44, 0x0

    .line 957460
    const/16 v43, 0x0

    .line 957461
    const/16 v42, 0x0

    .line 957462
    const/16 v41, 0x0

    .line 957463
    const/16 v40, 0x0

    .line 957464
    const/16 v39, 0x0

    .line 957465
    const/16 v38, 0x0

    .line 957466
    const/16 v37, 0x0

    .line 957467
    const/16 v36, 0x0

    .line 957468
    const-wide/16 v34, 0x0

    .line 957469
    const/16 v33, 0x0

    .line 957470
    const/16 v32, 0x0

    .line 957471
    const/16 v31, 0x0

    .line 957472
    const/16 v30, 0x0

    .line 957473
    const/16 v29, 0x0

    .line 957474
    const/16 v28, 0x0

    .line 957475
    const/16 v27, 0x0

    .line 957476
    const/16 v26, 0x0

    .line 957477
    const/16 v25, 0x0

    .line 957478
    const/16 v24, 0x0

    .line 957479
    const/16 v23, 0x0

    .line 957480
    const/16 v22, 0x0

    .line 957481
    const/16 v21, 0x0

    .line 957482
    const/16 v20, 0x0

    .line 957483
    const/16 v19, 0x0

    .line 957484
    const/16 v18, 0x0

    .line 957485
    const/16 v17, 0x0

    .line 957486
    const/16 v16, 0x0

    .line 957487
    const/4 v15, 0x0

    .line 957488
    const/4 v14, 0x0

    .line 957489
    const/4 v13, 0x0

    .line 957490
    const/4 v12, 0x0

    .line 957491
    const/4 v11, 0x0

    .line 957492
    const/4 v10, 0x0

    .line 957493
    const/4 v9, 0x0

    .line 957494
    const/4 v8, 0x0

    .line 957495
    const/4 v7, 0x0

    .line 957496
    const/4 v6, 0x0

    .line 957497
    const/4 v5, 0x0

    .line 957498
    const/4 v4, 0x0

    .line 957499
    const/4 v3, 0x0

    .line 957500
    const/4 v2, 0x0

    .line 957501
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_2f

    .line 957502
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 957503
    const/4 v2, 0x0

    .line 957504
    :goto_0
    return v2

    .line 957505
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v49, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v49

    if-eq v2, v0, :cond_24

    .line 957506
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 957507
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 957508
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v49

    sget-object v50, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 957509
    const-string v49, "active_bots"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1

    .line 957510
    invoke-static/range {p0 .. p1}, LX/5az;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v48, v2

    goto :goto_1

    .line 957511
    :cond_1
    const-string v49, "all_participant_ids"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_2

    .line 957512
    invoke-static/range {p0 .. p1}, LX/5b0;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 957513
    :cond_2
    const-string v49, "all_participants"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_3

    .line 957514
    invoke-static/range {p0 .. p1}, LX/5b1;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v46, v2

    goto :goto_1

    .line 957515
    :cond_3
    const-string v49, "can_viewer_reply"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_4

    .line 957516
    const/4 v2, 0x1

    .line 957517
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v45, v7

    move v7, v2

    goto :goto_1

    .line 957518
    :cond_4
    const-string v49, "cannot_reply_reason"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_5

    .line 957519
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v44, v2

    goto :goto_1

    .line 957520
    :cond_5
    const-string v49, "confirmedBookingRequest"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_6

    .line 957521
    invoke-static/range {p0 .. p1}, LX/5Zs;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 957522
    :cond_6
    const-string v49, "customization_info"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_7

    .line 957523
    invoke-static/range {p0 .. p1}, LX/5b2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 957524
    :cond_7
    const-string v49, "delivery_receipts"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_8

    .line 957525
    invoke-static/range {p0 .. p1}, LX/5b3;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 957526
    :cond_8
    const-string v49, "description"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_9

    .line 957527
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 957528
    :cond_9
    const-string v49, "ephemeral_ttl_mode"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_a

    .line 957529
    const/4 v2, 0x1

    .line 957530
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v39, v6

    move v6, v2

    goto/16 :goto_1

    .line 957531
    :cond_a
    const-string v49, "event_reminders"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_b

    .line 957532
    invoke-static/range {p0 .. p1}, LX/5aD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 957533
    :cond_b
    const-string v49, "folder"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_c

    .line 957534
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 957535
    :cond_c
    const-string v49, "group_rank_coefficient"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_d

    .line 957536
    const/4 v2, 0x1

    .line 957537
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 957538
    :cond_d
    const-string v49, "has_viewer_archived"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_e

    .line 957539
    const/4 v2, 0x1

    .line 957540
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v36, v14

    move v14, v2

    goto/16 :goto_1

    .line 957541
    :cond_e
    const-string v49, "id"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_f

    .line 957542
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 957543
    :cond_f
    const-string v49, "image"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_10

    .line 957544
    invoke-static/range {p0 .. p1}, LX/5b4;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 957545
    :cond_10
    const-string v49, "is_group_thread"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_11

    .line 957546
    const/4 v2, 0x1

    .line 957547
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v33, v13

    move v13, v2

    goto/16 :goto_1

    .line 957548
    :cond_11
    const-string v49, "is_viewer_subscribed"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_12

    .line 957549
    const/4 v2, 0x1

    .line 957550
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v32, v12

    move v12, v2

    goto/16 :goto_1

    .line 957551
    :cond_12
    const-string v49, "last_message"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_13

    .line 957552
    invoke-static/range {p0 .. p1}, LX/5bB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 957553
    :cond_13
    const-string v49, "mentions_mute_mode"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_14

    .line 957554
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 957555
    :cond_14
    const-string v49, "messages"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_15

    .line 957556
    invoke-static/range {p0 .. p1}, LX/5ap;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 957557
    :cond_15
    const-string v49, "messages_count"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_16

    .line 957558
    const/4 v2, 0x1

    .line 957559
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v28, v11

    move v11, v2

    goto/16 :goto_1

    .line 957560
    :cond_16
    const-string v49, "mute_until"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_17

    .line 957561
    const/4 v2, 0x1

    .line 957562
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v27, v10

    move v10, v2

    goto/16 :goto_1

    .line 957563
    :cond_17
    const-string v49, "name"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_18

    .line 957564
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 957565
    :cond_18
    const-string v49, "pendingBookingRequest"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_19

    .line 957566
    invoke-static/range {p0 .. p1}, LX/5Zu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 957567
    :cond_19
    const-string v49, "reactions_mute_mode"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1a

    .line 957568
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 957569
    :cond_1a
    const-string v49, "read_receipts"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1b

    .line 957570
    invoke-static/range {p0 .. p1}, LX/5bD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 957571
    :cond_1b
    const-string v49, "requestedBookingRequest"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1c

    .line 957572
    invoke-static/range {p0 .. p1}, LX/5Zw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 957573
    :cond_1c
    const-string v49, "rtc_call_data"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1d

    .line 957574
    invoke-static/range {p0 .. p1}, LX/5WW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 957575
    :cond_1d
    const-string v49, "thread_games"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1e

    .line 957576
    invoke-static/range {p0 .. p1}, LX/5bE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 957577
    :cond_1e
    const-string v49, "thread_key"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_1f

    .line 957578
    invoke-static/range {p0 .. p1}, LX/5bF;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 957579
    :cond_1f
    const-string v49, "thread_queue_enabled"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_20

    .line 957580
    const/4 v2, 0x1

    .line 957581
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v18, v9

    move v9, v2

    goto/16 :goto_1

    .line 957582
    :cond_20
    const-string v49, "thread_queue_metadata"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_21

    .line 957583
    invoke-static/range {p0 .. p1}, LX/5bN;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 957584
    :cond_21
    const-string v49, "unread_count"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_22

    .line 957585
    const/4 v2, 0x1

    .line 957586
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v16, v8

    move v8, v2

    goto/16 :goto_1

    .line 957587
    :cond_22
    const-string v49, "updated_time_precise"

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 957588
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 957589
    :cond_23
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 957590
    :cond_24
    const/16 v2, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 957591
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957592
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957593
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957594
    if-eqz v7, :cond_25

    .line 957595
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 957596
    :cond_25
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957597
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957598
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957599
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957600
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957601
    if-eqz v6, :cond_26

    .line 957602
    const/16 v2, 0x9

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 957603
    :cond_26
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957604
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957605
    if-eqz v3, :cond_27

    .line 957606
    const/16 v3, 0xc

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 957607
    :cond_27
    if-eqz v14, :cond_28

    .line 957608
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 957609
    :cond_28
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957610
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957611
    if-eqz v13, :cond_29

    .line 957612
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 957613
    :cond_29
    if-eqz v12, :cond_2a

    .line 957614
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 957615
    :cond_2a
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957616
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957617
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957618
    if-eqz v11, :cond_2b

    .line 957619
    const/16 v2, 0x15

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 957620
    :cond_2b
    if-eqz v10, :cond_2c

    .line 957621
    const/16 v2, 0x16

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 957622
    :cond_2c
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957623
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957624
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957625
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957626
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957627
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957628
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957629
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957630
    if-eqz v9, :cond_2d

    .line 957631
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 957632
    :cond_2d
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 957633
    if-eqz v8, :cond_2e

    .line 957634
    const/16 v2, 0x21

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 957635
    :cond_2e
    const/16 v2, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 957636
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2f
    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v37

    move/from16 v37, v36

    move/from16 v36, v33

    move/from16 v33, v30

    move/from16 v30, v27

    move/from16 v27, v24

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v15

    move v15, v12

    move v12, v6

    move v6, v10

    move v10, v4

    move/from16 v51, v25

    move/from16 v25, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v7

    move v7, v11

    move v11, v5

    move-wide/from16 v4, v34

    move/from16 v35, v32

    move/from16 v34, v31

    move/from16 v31, v28

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v28, v51

    move/from16 v26, v23

    move/from16 v23, v20

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v8

    move v8, v2

    move/from16 v52, v3

    move v3, v9

    move/from16 v9, v52

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 957637
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 957638
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 957639
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 957640
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 957641
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 957642
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 957643
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 957644
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 957645
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 957646
    invoke-static {p0, p1}, LX/5bG;->a(LX/15w;LX/186;)I

    move-result v1

    .line 957647
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 957648
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x13

    const/16 v6, 0xb

    const/4 v2, 0x4

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 957649
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957650
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 957651
    if-eqz v0, :cond_0

    .line 957652
    const-string v1, "active_bots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957653
    invoke-static {p0, v0, p2, p3}, LX/5az;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957654
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957655
    if-eqz v0, :cond_1

    .line 957656
    const-string v1, "all_participant_ids"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957657
    invoke-static {p0, v0, p2, p3}, LX/5b0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957658
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957659
    if-eqz v0, :cond_2

    .line 957660
    const-string v1, "all_participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957661
    invoke-static {p0, v0, p2, p3}, LX/5b1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957662
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957663
    if-eqz v0, :cond_3

    .line 957664
    const-string v1, "can_viewer_reply"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957665
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957666
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 957667
    if-eqz v0, :cond_4

    .line 957668
    const-string v0, "cannot_reply_reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957669
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957670
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957671
    if-eqz v0, :cond_5

    .line 957672
    const-string v1, "confirmedBookingRequest"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957673
    invoke-static {p0, v0, p2, p3}, LX/5Zs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957674
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957675
    if-eqz v0, :cond_6

    .line 957676
    const-string v1, "customization_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957677
    invoke-static {p0, v0, p2, p3}, LX/5b2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957678
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957679
    if-eqz v0, :cond_7

    .line 957680
    const-string v1, "delivery_receipts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957681
    invoke-static {p0, v0, p2, p3}, LX/5b3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957682
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957683
    if-eqz v0, :cond_8

    .line 957684
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957685
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957686
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 957687
    if-eqz v0, :cond_9

    .line 957688
    const-string v1, "ephemeral_ttl_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957689
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957690
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957691
    if-eqz v0, :cond_a

    .line 957692
    const-string v1, "event_reminders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957693
    invoke-static {p0, v0, p2, p3}, LX/5aD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957694
    :cond_a
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 957695
    if-eqz v0, :cond_b

    .line 957696
    const-string v0, "folder"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957697
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957698
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 957699
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_c

    .line 957700
    const-string v2, "group_rank_coefficient"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957701
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 957702
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957703
    if-eqz v0, :cond_d

    .line 957704
    const-string v1, "has_viewer_archived"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957705
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957706
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957707
    if-eqz v0, :cond_e

    .line 957708
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957709
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957710
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957711
    if-eqz v0, :cond_f

    .line 957712
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957713
    invoke-static {p0, v0, p2}, LX/5b4;->a(LX/15i;ILX/0nX;)V

    .line 957714
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957715
    if-eqz v0, :cond_10

    .line 957716
    const-string v1, "is_group_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957717
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957718
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957719
    if-eqz v0, :cond_11

    .line 957720
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957721
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957722
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957723
    if-eqz v0, :cond_12

    .line 957724
    const-string v1, "last_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957725
    invoke-static {p0, v0, p2, p3}, LX/5bB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957726
    :cond_12
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 957727
    if-eqz v0, :cond_13

    .line 957728
    const-string v0, "mentions_mute_mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957729
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957730
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957731
    if-eqz v0, :cond_14

    .line 957732
    const-string v1, "messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957733
    invoke-static {p0, v0, p2, p3}, LX/5ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957734
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 957735
    if-eqz v0, :cond_15

    .line 957736
    const-string v1, "messages_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957737
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957738
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 957739
    if-eqz v0, :cond_16

    .line 957740
    const-string v1, "mute_until"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957741
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957742
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957743
    if-eqz v0, :cond_17

    .line 957744
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957745
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957746
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957747
    if-eqz v0, :cond_18

    .line 957748
    const-string v1, "pendingBookingRequest"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957749
    invoke-static {p0, v0, p2, p3}, LX/5Zu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957750
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957751
    if-eqz v0, :cond_19

    .line 957752
    const-string v0, "reactions_mute_mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957753
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957754
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957755
    if-eqz v0, :cond_1a

    .line 957756
    const-string v1, "read_receipts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957757
    invoke-static {p0, v0, p2, p3}, LX/5bD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957758
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957759
    if-eqz v0, :cond_1b

    .line 957760
    const-string v1, "requestedBookingRequest"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957761
    invoke-static {p0, v0, p2, p3}, LX/5Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957762
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957763
    if-eqz v0, :cond_1c

    .line 957764
    const-string v1, "rtc_call_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957765
    invoke-static {p0, v0, p2, p3}, LX/5WW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957766
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957767
    if-eqz v0, :cond_1d

    .line 957768
    const-string v1, "thread_games"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957769
    invoke-static {p0, v0, p2, p3}, LX/5bE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957770
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957771
    if-eqz v0, :cond_1e

    .line 957772
    const-string v1, "thread_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957773
    invoke-static {p0, v0, p2}, LX/5bF;->a(LX/15i;ILX/0nX;)V

    .line 957774
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957775
    if-eqz v0, :cond_1f

    .line 957776
    const-string v1, "thread_queue_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957777
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957778
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957779
    if-eqz v0, :cond_20

    .line 957780
    const-string v1, "thread_queue_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957781
    invoke-static {p0, v0, p2, p3}, LX/5bN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957782
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 957783
    if-eqz v0, :cond_21

    .line 957784
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957785
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957786
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957787
    if-eqz v0, :cond_22

    .line 957788
    const-string v1, "updated_time_precise"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957789
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957790
    :cond_22
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957791
    return-void
.end method
