.class public LX/6DO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field private static volatile i:LX/6DO;


# instance fields
.field private final f:Z

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1064895
    const-class v0, LX/6DO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6DO;->a:Ljava/lang/String;

    .line 1064896
    sget-object v0, LX/6D7;->e:LX/0Tn;

    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6DO;->b:LX/0Tn;

    .line 1064897
    sget-object v0, LX/6D7;->e:LX/0Tn;

    const-string v1, "telephone"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6DO;->c:LX/0Tn;

    .line 1064898
    sget-object v0, LX/6D7;->e:LX/0Tn;

    const-string v1, "address"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6DO;->d:LX/0Tn;

    .line 1064899
    sget-object v0, LX/6D7;->e:LX/0Tn;

    const-string v1, "string/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6DO;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064901
    const/16 v0, 0x514

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/6DO;->f:Z

    .line 1064902
    iput-object p1, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1064903
    iput-object p3, p0, LX/6DO;->h:LX/03V;

    .line 1064904
    return-void
.end method

.method public static a(LX/0QB;)LX/6DO;
    .locals 6

    .prologue
    .line 1064905
    sget-object v0, LX/6DO;->i:LX/6DO;

    if-nez v0, :cond_1

    .line 1064906
    const-class v1, LX/6DO;

    monitor-enter v1

    .line 1064907
    :try_start_0
    sget-object v0, LX/6DO;->i:LX/6DO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1064908
    if-eqz v2, :cond_0

    .line 1064909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1064910
    new-instance p0, LX/6DO;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/6DO;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/03V;)V

    .line 1064911
    move-object v0, p0

    .line 1064912
    sput-object v0, LX/6DO;->i:LX/6DO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1064913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1064914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1064915
    :cond_1
    sget-object v0, LX/6DO;->i:LX/6DO;

    return-object v0

    .line 1064916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1064917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064918
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1064919
    iget-object v1, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6DO;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1064920
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1064921
    :cond_0
    :goto_0
    return-object v0

    .line 1064922
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1064923
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1064924
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1064925
    new-instance v4, Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;

    invoke-direct {v4, v3}, Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064926
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1064927
    :catch_0
    move-exception v1

    .line 1064928
    iget-object v2, p0, LX/6DO;->h:LX/03V;

    sget-object v3, LX/6DO;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064929
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1064930
    iget-object v2, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/6DO;->e:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1064931
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1064932
    :goto_0
    return-object v0

    .line 1064933
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1064934
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1064935
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1064936
    new-instance v4, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;

    invoke-direct {v4, v3}, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064937
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1064938
    :catch_0
    move-exception v0

    .line 1064939
    iget-object v2, p0, LX/6DO;->h:LX/03V;

    sget-object v3, LX/6DO;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v1

    .line 1064940
    goto :goto_0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 1064941
    instance-of v0, p1, Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;

    if-eqz v0, :cond_3

    .line 1064942
    invoke-virtual {p0}, LX/6DO;->a()Ljava/util/List;

    move-result-object v1

    .line 1064943
    sget-object v0, LX/6DO;->b:LX/0Tn;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 1064944
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1064945
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1064946
    invoke-virtual {v3, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1064947
    :goto_1
    move-object v3, v3

    .line 1064948
    if-eqz v3, :cond_1

    .line 1064949
    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1064950
    :cond_1
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1064951
    iget-boolean v3, p0, LX/6DO;->f:Z

    if-eqz v3, :cond_2

    .line 1064952
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064953
    :cond_2
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 1064954
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1064955
    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->d()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1064956
    :catch_0
    move-exception v0

    .line 1064957
    iget-object v4, p0, LX/6DO;->h:LX/03V;

    sget-object v5, LX/6DO;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1064958
    :cond_3
    instance-of v0, p1, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;

    if-eqz v0, :cond_4

    .line 1064959
    invoke-virtual {p0}, LX/6DO;->b()Ljava/util/List;

    move-result-object v1

    .line 1064960
    sget-object v0, LX/6DO;->c:LX/0Tn;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_0

    .line 1064961
    :cond_4
    instance-of v0, p1, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;

    if-eqz v0, :cond_5

    .line 1064962
    invoke-virtual {p0}, LX/6DO;->c()Ljava/util/List;

    move-result-object v1

    .line 1064963
    sget-object v0, LX/6DO;->d:LX/0Tn;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_0

    .line 1064964
    :cond_5
    instance-of v0, p1, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1064965
    check-cast v0, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;->f()Ljava/lang/String;

    move-result-object v0

    .line 1064966
    invoke-virtual {p0, v0}, LX/6DO;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1064967
    sget-object v2, LX/6DO;->e:LX/0Tn;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 1064968
    goto/16 :goto_0

    .line 1064969
    :cond_6
    iget-object v0, p0, LX/6DO;->h:LX/03V;

    sget-object v1, LX/6DO;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064970
    :goto_3
    return-void

    .line 1064971
    :cond_7
    iget-object v0, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064972
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1064973
    iget-object v1, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6DO;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1064974
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1064975
    :cond_0
    :goto_0
    return-object v0

    .line 1064976
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1064977
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1064978
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1064979
    new-instance v4, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;

    invoke-direct {v4, v3}, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064980
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1064981
    :catch_0
    move-exception v1

    .line 1064982
    iget-object v2, p0, LX/6DO;->h:LX/03V;

    sget-object v3, LX/6DO;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064983
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1064984
    iget-object v1, p0, LX/6DO;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6DO;->d:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1064985
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1064986
    :cond_0
    :goto_0
    return-object v0

    .line 1064987
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1064988
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1064989
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1064990
    new-instance v4, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;

    invoke-direct {v4, v3}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064991
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1064992
    :catch_0
    move-exception v1

    .line 1064993
    iget-object v2, p0, LX/6DO;->h:LX/03V;

    sget-object v3, LX/6DO;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
