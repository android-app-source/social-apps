.class public final LX/5LN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 900928
    const/16 v26, 0x0

    .line 900929
    const/16 v25, 0x0

    .line 900930
    const/16 v24, 0x0

    .line 900931
    const/16 v23, 0x0

    .line 900932
    const/16 v22, 0x0

    .line 900933
    const/16 v21, 0x0

    .line 900934
    const/16 v20, 0x0

    .line 900935
    const/16 v19, 0x0

    .line 900936
    const/16 v18, 0x0

    .line 900937
    const/16 v17, 0x0

    .line 900938
    const/16 v16, 0x0

    .line 900939
    const/4 v15, 0x0

    .line 900940
    const/4 v14, 0x0

    .line 900941
    const/4 v13, 0x0

    .line 900942
    const/4 v12, 0x0

    .line 900943
    const/4 v11, 0x0

    .line 900944
    const/4 v10, 0x0

    .line 900945
    const/4 v9, 0x0

    .line 900946
    const/4 v8, 0x0

    .line 900947
    const/4 v7, 0x0

    .line 900948
    const/4 v6, 0x0

    .line 900949
    const/4 v5, 0x0

    .line 900950
    const/4 v4, 0x0

    .line 900951
    const/4 v3, 0x0

    .line 900952
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 900953
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 900954
    const/4 v3, 0x0

    .line 900955
    :goto_0
    return v3

    .line 900956
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 900957
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_14

    .line 900958
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 900959
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 900960
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 900961
    const-string v28, "all_icons"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 900962
    invoke-static/range {p0 .. p1}, LX/5Lm;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 900963
    :cond_2
    const-string v28, "glyph"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 900964
    invoke-static/range {p0 .. p1}, LX/5Lj;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 900965
    :cond_3
    const-string v28, "iconImageLarge"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 900966
    invoke-static/range {p0 .. p1}, LX/5Lk;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 900967
    :cond_4
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 900968
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 900969
    :cond_5
    const-string v28, "is_linking_verb"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 900970
    const/4 v7, 0x1

    .line 900971
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 900972
    :cond_6
    const-string v28, "legacy_api_id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 900973
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 900974
    :cond_7
    const-string v28, "prefetch_priority"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 900975
    const/4 v6, 0x1

    .line 900976
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto/16 :goto_1

    .line 900977
    :cond_8
    const-string v28, "present_participle"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 900978
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 900979
    :cond_9
    const-string v28, "previewTemplateAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 900980
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 900981
    :cond_a
    const-string v28, "previewTemplateNoTags"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 900982
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 900983
    :cond_b
    const-string v28, "previewTemplateWithPeople"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 900984
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 900985
    :cond_c
    const-string v28, "previewTemplateWithPeopleAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 900986
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 900987
    :cond_d
    const-string v28, "previewTemplateWithPerson"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 900988
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 900989
    :cond_e
    const-string v28, "previewTemplateWithPersonAtPlace"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 900990
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 900991
    :cond_f
    const-string v28, "prompt"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 900992
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 900993
    :cond_10
    const-string v28, "supports_audio_suggestions"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 900994
    const/4 v5, 0x1

    .line 900995
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 900996
    :cond_11
    const-string v28, "supports_freeform"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 900997
    const/4 v4, 0x1

    .line 900998
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 900999
    :cond_12
    const-string v28, "supports_offline_posting"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_13

    .line 901000
    const/4 v3, 0x1

    .line 901001
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 901002
    :cond_13
    const-string v28, "taggable_activity_suggestions"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 901003
    invoke-static/range {p0 .. p1}, LX/5LP;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 901004
    :cond_14
    const/16 v27, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 901005
    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901006
    const/16 v26, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901007
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901008
    const/16 v24, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 901009
    if-eqz v7, :cond_15

    .line 901010
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 901011
    :cond_15
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 901012
    if-eqz v6, :cond_16

    .line 901013
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 901014
    :cond_16
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901015
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901016
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901017
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 901018
    const/16 v6, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 901019
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 901020
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 901021
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 901022
    if-eqz v5, :cond_17

    .line 901023
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 901024
    :cond_17
    if-eqz v4, :cond_18

    .line 901025
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 901026
    :cond_18
    if-eqz v3, :cond_19

    .line 901027
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 901028
    :cond_19
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 901029
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
