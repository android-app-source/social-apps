.class public LX/64F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/33e;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044377
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044378
    new-instance v0, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    const-string v1, "ZeroIndicatorData"

    const-string v2, "Zero Rating"

    const-string v3, "You are browsing for free courtesy of [carrier]"

    const-string v4, "Learn more"

    const-string v5, "http://www.facebook.com"

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1044379
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044380
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044381
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044382
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044383
    new-instance v1, Lcom/facebook/zero/sdk/token/ZeroToken;

    const-string v2, "TestCampaign"

    const-string v3, "registered"

    const-string v4, "Jack\'s Test Carrier"

    const-string v5, "1234"

    const-string v6, "noLogoForNow"

    const/16 v7, 0xe10

    sget-object v8, LX/0yY;->LEAVING_APP_INTERSTITIAL:LX/0yY;

    sget-object v9, LX/0yY;->ZERO_INDICATOR:LX/0yY;

    sget-object v10, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    invoke-static {v8, v9, v10}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    new-instance v9, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v10, "^(https?)://(www|web|z-m-www)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|/.*$)"

    const-string v11, "$1://web.$3facebook.com$4$5"

    invoke-direct {v9, v10, v11}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    const-string v10, "unavailable"

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v11

    const-string v12, "TestHash"

    const/16 v13, 0x3e8

    const-string v14, "FastTokenHash"

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v15

    invoke-direct/range {v1 .. v15}, Lcom/facebook/zero/sdk/token/ZeroToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Rf;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;ILjava/lang/String;LX/0P1;)V

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1044384
    const/4 v1, 0x0

    return-object v1
.end method

.method public final a(Lcom/facebook/zero/sdk/request/ZeroOptinParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptinParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044385
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/ZeroOptoutParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044386
    const/4 v0, 0x0

    return-object v0
.end method
