.class public final LX/5jG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5iE;


# instance fields
.field public final a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:Z

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 986103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986104
    iput v0, p0, LX/5jG;->d:F

    .line 986105
    iput v0, p0, LX/5jG;->e:F

    .line 986106
    iput v0, p0, LX/5jG;->f:F

    .line 986107
    iput v0, p0, LX/5jG;->g:F

    .line 986108
    iput v0, p0, LX/5jG;->h:F

    .line 986109
    iput-boolean v2, p0, LX/5jG;->k:Z

    .line 986110
    iput-object v1, p0, LX/5jG;->a:Landroid/net/Uri;

    .line 986111
    iput-object v1, p0, LX/5jG;->b:Ljava/lang/String;

    .line 986112
    iput-object v1, p0, LX/5jG;->c:Ljava/lang/String;

    .line 986113
    iput-boolean v2, p0, LX/5jG;->i:Z

    .line 986114
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5jG;->j:Z

    .line 986115
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 986053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986054
    iput v0, p0, LX/5jG;->d:F

    .line 986055
    iput v0, p0, LX/5jG;->e:F

    .line 986056
    iput v0, p0, LX/5jG;->f:F

    .line 986057
    iput v0, p0, LX/5jG;->g:F

    .line 986058
    iput v0, p0, LX/5jG;->h:F

    .line 986059
    iput-boolean v1, p0, LX/5jG;->k:Z

    .line 986060
    iput-object p1, p0, LX/5jG;->a:Landroid/net/Uri;

    .line 986061
    iput-object p2, p0, LX/5jG;->b:Ljava/lang/String;

    .line 986062
    const/4 v0, 0x0

    iput-object v0, p0, LX/5jG;->c:Ljava/lang/String;

    .line 986063
    iput-boolean v1, p0, LX/5jG;->i:Z

    .line 986064
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5jG;->j:Z

    .line 986065
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/StickerParams;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 986084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986085
    iput v0, p0, LX/5jG;->d:F

    .line 986086
    iput v0, p0, LX/5jG;->e:F

    .line 986087
    iput v0, p0, LX/5jG;->f:F

    .line 986088
    iput v0, p0, LX/5jG;->g:F

    .line 986089
    iput v0, p0, LX/5jG;->h:F

    .line 986090
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5jG;->k:Z

    .line 986091
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/5jG;->a:Landroid/net/Uri;

    .line 986092
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5jG;->b:Ljava/lang/String;

    .line 986093
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    iput-object v0, p0, LX/5jG;->c:Ljava/lang/String;

    .line 986094
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->c()F

    move-result v0

    iput v0, p0, LX/5jG;->d:F

    .line 986095
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v0

    iput v0, p0, LX/5jG;->e:F

    .line 986096
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v0

    iput v0, p0, LX/5jG;->f:F

    .line 986097
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v0

    iput v0, p0, LX/5jG;->g:F

    .line 986098
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v0

    iput v0, p0, LX/5jG;->h:F

    .line 986099
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->h()Z

    move-result v0

    iput-boolean v0, p0, LX/5jG;->i:Z

    .line 986100
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->k()Z

    move-result v0

    iput-boolean v0, p0, LX/5jG;->j:Z

    .line 986101
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/5jG;->k:Z

    .line 986102
    return-void
.end method


# virtual methods
.method public final synthetic a(Z)LX/5iE;
    .locals 1

    .prologue
    .line 986081
    iput-boolean p1, p0, LX/5jG;->i:Z

    .line 986082
    move-object v0, p0

    .line 986083
    return-object v0
.end method

.method public final a()Lcom/facebook/photos/creativeediting/model/StickerParams;
    .locals 2

    .prologue
    .line 986078
    iget-object v0, p0, LX/5jG;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 986079
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5jG;->c:Ljava/lang/String;

    .line 986080
    :cond_0
    new-instance v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;-><init>(LX/5jG;)V

    return-object v0
.end method

.method public final synthetic b()LX/5i8;
    .locals 1

    .prologue
    .line 986116
    invoke-virtual {p0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f(F)LX/5iE;
    .locals 1

    .prologue
    .line 986075
    iput p1, p0, LX/5jG;->h:F

    .line 986076
    move-object v0, p0

    .line 986077
    return-object v0
.end method

.method public final synthetic g(F)LX/5iE;
    .locals 1

    .prologue
    .line 986072
    iput p1, p0, LX/5jG;->g:F

    .line 986073
    move-object v0, p0

    .line 986074
    return-object v0
.end method

.method public final synthetic h(F)LX/5iE;
    .locals 1

    .prologue
    .line 986069
    iput p1, p0, LX/5jG;->f:F

    .line 986070
    move-object v0, p0

    .line 986071
    return-object v0
.end method

.method public final synthetic i(F)LX/5iE;
    .locals 1

    .prologue
    .line 986066
    iput p1, p0, LX/5jG;->e:F

    .line 986067
    move-object v0, p0

    .line 986068
    return-object v0
.end method

.method public final synthetic j(F)LX/5iE;
    .locals 1

    .prologue
    .line 986050
    iput p1, p0, LX/5jG;->d:F

    .line 986051
    move-object v0, p0

    .line 986052
    return-object v0
.end method
