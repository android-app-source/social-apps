.class public final LX/5CC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 866995
    const/16 v27, 0x0

    .line 866996
    const-wide/16 v28, 0x0

    .line 866997
    const/16 v26, 0x0

    .line 866998
    const/16 v25, 0x0

    .line 866999
    const/16 v24, 0x0

    .line 867000
    const/16 v23, 0x0

    .line 867001
    const/16 v22, 0x0

    .line 867002
    const-wide/16 v20, 0x0

    .line 867003
    const/4 v15, 0x0

    .line 867004
    const-wide/16 v18, 0x0

    .line 867005
    const-wide/16 v16, 0x0

    .line 867006
    const/4 v14, 0x0

    .line 867007
    const/4 v13, 0x0

    .line 867008
    const/4 v12, 0x0

    .line 867009
    const/4 v11, 0x0

    .line 867010
    const/4 v10, 0x0

    .line 867011
    const/4 v9, 0x0

    .line 867012
    const/4 v8, 0x0

    .line 867013
    const/4 v7, 0x0

    .line 867014
    const/4 v6, 0x0

    .line 867015
    const/4 v5, 0x0

    .line 867016
    const/4 v4, 0x0

    .line 867017
    const/4 v3, 0x0

    .line 867018
    const/4 v2, 0x0

    .line 867019
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1a

    .line 867020
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 867021
    const/4 v2, 0x0

    .line 867022
    :goto_0
    return v2

    .line 867023
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_f

    .line 867024
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 867025
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 867026
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 867027
    const-string v7, "enable_focus"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 867028
    const/4 v2, 0x1

    .line 867029
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v31, v6

    move v6, v2

    goto :goto_1

    .line 867030
    :cond_1
    const-string v7, "focus_width_degrees"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 867031
    const/4 v2, 0x1

    .line 867032
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 867033
    :cond_2
    const-string v7, "guided_tour"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 867034
    invoke-static/range {p0 .. p1}, LX/5CB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 867035
    :cond_3
    const-string v7, "initial_view_heading_degrees"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 867036
    const/4 v2, 0x1

    .line 867037
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v15, v2

    move/from16 v29, v7

    goto :goto_1

    .line 867038
    :cond_4
    const-string v7, "initial_view_pitch_degrees"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 867039
    const/4 v2, 0x1

    .line 867040
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v14, v2

    move/from16 v28, v7

    goto :goto_1

    .line 867041
    :cond_5
    const-string v7, "initial_view_roll_degrees"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 867042
    const/4 v2, 0x1

    .line 867043
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v13, v2

    move/from16 v27, v7

    goto :goto_1

    .line 867044
    :cond_6
    const-string v7, "is_spherical"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 867045
    const/4 v2, 0x1

    .line 867046
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v12, v2

    move/from16 v26, v7

    goto/16 :goto_1

    .line 867047
    :cond_7
    const-string v7, "off_focus_level"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 867048
    const/4 v2, 0x1

    .line 867049
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v24

    move v11, v2

    goto/16 :goto_1

    .line 867050
    :cond_8
    const-string v7, "projection_type"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 867051
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 867052
    :cond_9
    const-string v7, "sphericalFullscreenAspectRatio"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 867053
    const/4 v2, 0x1

    .line 867054
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v22

    move v10, v2

    goto/16 :goto_1

    .line 867055
    :cond_a
    const-string v7, "sphericalInlineAspectRatio"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 867056
    const/4 v2, 0x1

    .line 867057
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v20

    move v9, v2

    goto/16 :goto_1

    .line 867058
    :cond_b
    const-string v7, "sphericalPlayableUrlHdString"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 867059
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 867060
    :cond_c
    const-string v7, "sphericalPlayableUrlSdString"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 867061
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 867062
    :cond_d
    const-string v7, "sphericalPreferredFov"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 867063
    const/4 v2, 0x1

    .line 867064
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v8, v2

    move/from16 v16, v7

    goto/16 :goto_1

    .line 867065
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 867066
    :cond_f
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 867067
    if-eqz v6, :cond_10

    .line 867068
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 867069
    :cond_10
    if-eqz v3, :cond_11

    .line 867070
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 867071
    :cond_11
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 867072
    if-eqz v15, :cond_12

    .line 867073
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 867074
    :cond_12
    if-eqz v14, :cond_13

    .line 867075
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 867076
    :cond_13
    if-eqz v13, :cond_14

    .line 867077
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 867078
    :cond_14
    if-eqz v12, :cond_15

    .line 867079
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 867080
    :cond_15
    if-eqz v11, :cond_16

    .line 867081
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 867082
    :cond_16
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 867083
    if-eqz v10, :cond_17

    .line 867084
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 867085
    :cond_17
    if-eqz v9, :cond_18

    .line 867086
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 867087
    :cond_18
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 867088
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 867089
    if-eqz v8, :cond_19

    .line 867090
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 867091
    :cond_19
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1a
    move/from16 v30, v26

    move/from16 v31, v27

    move/from16 v26, v22

    move/from16 v27, v23

    move-wide/from16 v22, v18

    move/from16 v18, v14

    move/from16 v19, v15

    move v15, v9

    move v14, v8

    move v8, v2

    move v9, v3

    move v3, v10

    move v10, v4

    move-wide/from16 v33, v16

    move/from16 v17, v13

    move/from16 v16, v12

    move v13, v7

    move v12, v6

    move v6, v11

    move v11, v5

    move-wide/from16 v4, v28

    move/from16 v29, v25

    move/from16 v28, v24

    move-wide/from16 v24, v20

    move-wide/from16 v20, v33

    goto/16 :goto_1
.end method
