.class public LX/6b9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/6b9;


# instance fields
.field public a:Lcom/facebook/compactdisk/StoreManagerFactory;

.field public b:Lcom/facebook/compactdisk/PersistentKeyValueStore;

.field public c:LX/0ad;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1113071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113072
    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113074
    iput-object p1, p0, LX/6b9;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 1113075
    iput-object p2, p0, LX/6b9;->c:LX/0ad;

    .line 1113076
    return-void
.end method

.method public static a(LX/0QB;)LX/6b9;
    .locals 5

    .prologue
    .line 1113077
    sget-object v0, LX/6b9;->d:LX/6b9;

    if-nez v0, :cond_1

    .line 1113078
    const-class v1, LX/6b9;

    monitor-enter v1

    .line 1113079
    :try_start_0
    sget-object v0, LX/6b9;->d:LX/6b9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1113080
    if-eqz v2, :cond_0

    .line 1113081
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1113082
    new-instance p0, LX/6b9;

    invoke-static {v0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v3

    check-cast v3, Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/6b9;-><init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/0ad;)V

    .line 1113083
    move-object v0, p0

    .line 1113084
    sput-object v0, LX/6b9;->d:LX/6b9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1113085
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1113086
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1113087
    :cond_1
    sget-object v0, LX/6b9;->d:LX/6b9;

    return-object v0

    .line 1113088
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1113089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()Lcom/facebook/compactdisk/PersistentKeyValueStore;
    .locals 3

    .prologue
    .line 1113090
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6b9;->b:Lcom/facebook/compactdisk/PersistentKeyValueStore;

    if-nez v0, :cond_0

    .line 1113091
    iget-object v0, p0, LX/6b9;->c:LX/0ad;

    const v1, -0x7702

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1113092
    if-eqz v0, :cond_0

    .line 1113093
    iget-object v0, p0, LX/6b9;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    const-string v1, "diskstoremanager_fb4a"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v0

    .line 1113094
    const-string v1, "media_library_pkvs"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManager;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    iput-object v0, p0, LX/6b9;->b:Lcom/facebook/compactdisk/PersistentKeyValueStore;

    .line 1113095
    :cond_0
    iget-object v0, p0, LX/6b9;->b:Lcom/facebook/compactdisk/PersistentKeyValueStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1113096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1113097
    invoke-direct {p0}, LX/6b9;->a()Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    .line 1113098
    if-eqz v0, :cond_0

    .line 1113099
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetch(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, -0x20f90244

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1113100
    if-eqz v0, :cond_0

    .line 1113101
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1113102
    new-instance v0, Ljava/io/ObjectInputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1113103
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 1113104
    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1113105
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1113106
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/Serializable;)V
    .locals 3

    .prologue
    .line 1113107
    invoke-direct {p0}, LX/6b9;->a()Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    .line 1113108
    if-nez v0, :cond_0

    .line 1113109
    :goto_0
    return-void

    .line 1113110
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1113111
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1113112
    invoke-virtual {v2, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1113113
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->flush()V

    .line 1113114
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
