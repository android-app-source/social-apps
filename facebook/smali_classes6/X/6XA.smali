.class public LX/6XA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLPage;


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLPage;)V
    .locals 0

    .prologue
    .line 1107871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1107872
    iput-object p1, p0, LX/6XA;->a:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1107873
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;)LX/6XA;
    .locals 2

    .prologue
    .line 1107874
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107875
    new-instance v1, LX/6XA;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-direct {v1, v0}, LX/6XA;-><init>(Lcom/facebook/graphql/model/GraphQLPage;)V

    return-object v1
.end method


# virtual methods
.method public final a(Z)LX/6XA;
    .locals 1

    .prologue
    .line 1107876
    iget-object v0, p0, LX/6XA;->a:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1107877
    invoke-virtual {v0, p1}, Lcom/facebook/graphql/model/GraphQLPage;->a(Z)V

    .line 1107878
    return-object p0
.end method
