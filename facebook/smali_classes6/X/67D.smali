.class public final LX/67D;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final synthetic a:LX/67E;


# direct methods
.method public constructor <init>(LX/67E;)V
    .locals 0

    .prologue
    .line 1052469
    iput-object p1, p0, LX/67D;->a:LX/67E;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public final available()I
    .locals 4

    .prologue
    .line 1052465
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-boolean v0, v0, LX/67E;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052466
    :cond_0
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052467
    iget-object v0, p0, LX/67D;->a:LX/67E;

    invoke-virtual {v0}, LX/67E;->close()V

    .line 1052468
    return-void
.end method

.method public final read()I
    .locals 4

    .prologue
    .line 1052460
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-boolean v0, v0, LX/67E;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052461
    :cond_0
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1052462
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67D;->a:LX/67E;

    iget-object v1, v1, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    .line 1052463
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 1052464
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    .line 1052454
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-boolean v0, v0, LX/67E;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052455
    :cond_0
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052456
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1052457
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67D;->a:LX/67E;

    iget-object v1, v1, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    .line 1052458
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 1052459
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/67D;->a:LX/67E;

    iget-object v0, v0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->a([BII)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052453
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/67D;->a:LX/67E;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".inputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
