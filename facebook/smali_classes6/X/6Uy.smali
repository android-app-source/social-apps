.class public final enum LX/6Uy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Uy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Uy;

.field public static final enum PRETTY:LX/6Uy;

.field public static final enum SINGLE_LINE:LX/6Uy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103259
    new-instance v0, LX/6Uy;

    const-string v1, "SINGLE_LINE"

    invoke-direct {v0, v1, v2}, LX/6Uy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Uy;->SINGLE_LINE:LX/6Uy;

    .line 1103260
    new-instance v0, LX/6Uy;

    const-string v1, "PRETTY"

    invoke-direct {v0, v1, v3}, LX/6Uy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Uy;->PRETTY:LX/6Uy;

    .line 1103261
    const/4 v0, 0x2

    new-array v0, v0, [LX/6Uy;

    sget-object v1, LX/6Uy;->SINGLE_LINE:LX/6Uy;

    aput-object v1, v0, v2

    sget-object v1, LX/6Uy;->PRETTY:LX/6Uy;

    aput-object v1, v0, v3

    sput-object v0, LX/6Uy;->$VALUES:[LX/6Uy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103262
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Uy;
    .locals 1

    .prologue
    .line 1103263
    const-class v0, LX/6Uy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Uy;

    return-object v0
.end method

.method public static values()[LX/6Uy;
    .locals 1

    .prologue
    .line 1103264
    sget-object v0, LX/6Uy;->$VALUES:[LX/6Uy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Uy;

    return-object v0
.end method
