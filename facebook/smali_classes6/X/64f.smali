.class public final LX/64f;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field private final g:J

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1045291
    const-string v0, "(\\d{2,4})[^\\d]*"

    .line 1045292
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/64f;->a:Ljava/util/regex/Pattern;

    .line 1045293
    const-string v0, "(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*"

    .line 1045294
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/64f;->b:Ljava/util/regex/Pattern;

    .line 1045295
    const-string v0, "(\\d{1,2})[^\\d]*"

    .line 1045296
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/64f;->c:Ljava/util/regex/Pattern;

    .line 1045297
    const-string v0, "(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*"

    .line 1045298
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/64f;->d:Ljava/util/regex/Pattern;

    .line 1045299
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 1

    .prologue
    .line 1045280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045281
    iput-object p1, p0, LX/64f;->e:Ljava/lang/String;

    .line 1045282
    iput-object p2, p0, LX/64f;->f:Ljava/lang/String;

    .line 1045283
    iput-wide p3, p0, LX/64f;->g:J

    .line 1045284
    iput-object p5, p0, LX/64f;->h:Ljava/lang/String;

    .line 1045285
    iput-object p6, p0, LX/64f;->i:Ljava/lang/String;

    .line 1045286
    iput-boolean p7, p0, LX/64f;->j:Z

    .line 1045287
    iput-boolean p8, p0, LX/64f;->k:Z

    .line 1045288
    iput-boolean p9, p0, LX/64f;->m:Z

    .line 1045289
    iput-boolean p10, p0, LX/64f;->l:Z

    .line 1045290
    return-void
.end method

.method private static a(Ljava/lang/String;IIZ)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1045272
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_5

    .line 1045273
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1045274
    const/16 v4, 0x20

    if-ge v1, v4, :cond_0

    const/16 v4, 0x9

    if-ne v1, v4, :cond_4

    :cond_0
    const/16 v4, 0x7f

    if-ge v1, v4, :cond_4

    const/16 v4, 0x30

    if-lt v1, v4, :cond_1

    const/16 v4, 0x39

    if-le v1, v4, :cond_4

    :cond_1
    const/16 v4, 0x61

    if-lt v1, v4, :cond_2

    const/16 v4, 0x7a

    if-le v1, v4, :cond_4

    :cond_2
    const/16 v4, 0x41

    if-lt v1, v4, :cond_3

    const/16 v4, 0x5a

    if-le v1, v4, :cond_4

    :cond_3
    const/16 v4, 0x3a

    if-ne v1, v4, :cond_6

    :cond_4
    move v4, v2

    .line 1045275
    :goto_1
    if-nez p3, :cond_7

    move v1, v2

    :goto_2
    if-ne v4, v1, :cond_8

    move p2, v0

    .line 1045276
    :cond_5
    return p2

    :cond_6
    move v4, v3

    .line 1045277
    goto :goto_1

    :cond_7
    move v1, v3

    .line 1045278
    goto :goto_2

    .line 1045279
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 1045264
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1045265
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1

    .line 1045266
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    move-wide v0, v2

    .line 1045267
    goto :goto_0

    .line 1045268
    :catch_0
    move-exception v2

    .line 1045269
    const-string v3, "-?\\d+"

    invoke-virtual {p0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1045270
    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 1045271
    :cond_2
    throw v2
.end method

.method private static a(Ljava/lang/String;II)J
    .locals 10

    .prologue
    .line 1045300
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/64f;->a(Ljava/lang/String;IIZ)I

    move-result v6

    .line 1045301
    const/4 v5, -0x1

    .line 1045302
    const/4 v4, -0x1

    .line 1045303
    const/4 v3, -0x1

    .line 1045304
    const/4 v2, -0x1

    .line 1045305
    const/4 v1, -0x1

    .line 1045306
    const/4 v0, -0x1

    .line 1045307
    sget-object v7, LX/64f;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v7, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 1045308
    :goto_0
    if-ge v6, p2, :cond_4

    .line 1045309
    add-int/lit8 v8, v6, 0x1

    const/4 v9, 0x1

    invoke-static {p0, v8, p2, v9}, LX/64f;->a(Ljava/lang/String;IIZ)I

    move-result v8

    .line 1045310
    invoke-virtual {v7, v6, v8}, Ljava/util/regex/Matcher;->region(II)Ljava/util/regex/Matcher;

    .line 1045311
    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    sget-object v6, LX/64f;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v6}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1045312
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1045313
    const/4 v3, 0x2

    invoke-virtual {v7, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1045314
    const/4 v3, 0x3

    invoke-virtual {v7, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1045315
    :cond_0
    :goto_1
    add-int/lit8 v6, v8, 0x1

    const/4 v8, 0x0

    invoke-static {p0, v6, p2, v8}, LX/64f;->a(Ljava/lang/String;IIZ)I

    move-result v6

    goto :goto_0

    .line 1045316
    :cond_1
    const/4 v6, -0x1

    if-ne v2, v6, :cond_2

    sget-object v6, LX/64f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v6}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1045317
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1045318
    :cond_2
    const/4 v6, -0x1

    if-ne v1, v6, :cond_3

    sget-object v6, LX/64f;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v6}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1045319
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 1045320
    sget-object v6, LX/64f;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v6}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    goto :goto_1

    .line 1045321
    :cond_3
    const/4 v6, -0x1

    if-ne v0, v6, :cond_0

    sget-object v6, LX/64f;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v6}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1045322
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1045323
    :cond_4
    const/16 v6, 0x46

    if-lt v0, v6, :cond_5

    const/16 v6, 0x63

    if-gt v0, v6, :cond_5

    add-int/lit16 v0, v0, 0x76c

    .line 1045324
    :cond_5
    if-ltz v0, :cond_6

    const/16 v6, 0x45

    if-gt v0, v6, :cond_6

    add-int/lit16 v0, v0, 0x7d0

    .line 1045325
    :cond_6
    const/16 v6, 0x641

    if-ge v0, v6, :cond_7

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045326
    :cond_7
    const/4 v6, -0x1

    if-ne v1, v6, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045327
    :cond_8
    if-lez v2, :cond_9

    const/16 v6, 0x1f

    if-le v2, v6, :cond_a

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045328
    :cond_a
    if-ltz v5, :cond_b

    const/16 v6, 0x17

    if-le v5, v6, :cond_c

    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045329
    :cond_c
    if-ltz v4, :cond_d

    const/16 v6, 0x3b

    if-le v4, v6, :cond_e

    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045330
    :cond_e
    if-ltz v3, :cond_f

    const/16 v6, 0x3b

    if-le v3, v6, :cond_10

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045331
    :cond_10
    new-instance v6, Ljava/util/GregorianCalendar;

    sget-object v7, LX/65A;->d:Ljava/util/TimeZone;

    invoke-direct {v6, v7}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1045332
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->setLenient(Z)V

    .line 1045333
    const/4 v7, 0x1

    invoke-virtual {v6, v7, v0}, Ljava/util/Calendar;->set(II)V

    .line 1045334
    const/4 v0, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1045335
    const/4 v0, 0x5

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 1045336
    const/16 v0, 0xb

    invoke-virtual {v6, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 1045337
    const/16 v0, 0xc

    invoke-virtual {v6, v0, v4}, Ljava/util/Calendar;->set(II)V

    .line 1045338
    const/16 v0, 0xd

    invoke-virtual {v6, v0, v3}, Ljava/util/Calendar;->set(II)V

    .line 1045339
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 1045340
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(JLX/64q;Ljava/lang/String;)LX/64f;
    .locals 20

    .prologue
    .line 1045202
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v16

    .line 1045203
    const/4 v2, 0x0

    const/16 v3, 0x3b

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-static {v0, v2, v1, v3}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v3

    .line 1045204
    const/4 v2, 0x0

    const/16 v4, 0x3d

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3, v4}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v2

    .line 1045205
    if-ne v2, v3, :cond_0

    const/4 v3, 0x0

    .line 1045206
    :goto_0
    return-object v3

    .line 1045207
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v4, v2}, LX/65A;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 1045208
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    .line 1045209
    :cond_1
    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3}, LX/65A;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    .line 1045210
    const-wide v14, 0xe677d21fdbffL

    .line 1045211
    const-wide/16 v6, -0x1

    .line 1045212
    const/4 v8, 0x0

    .line 1045213
    const/4 v2, 0x0

    .line 1045214
    const/4 v10, 0x0

    .line 1045215
    const/4 v11, 0x0

    .line 1045216
    const/4 v12, 0x1

    .line 1045217
    const/4 v13, 0x0

    .line 1045218
    add-int/lit8 v3, v3, 0x1

    .line 1045219
    :goto_1
    move/from16 v0, v16

    if-ge v3, v0, :cond_8

    .line 1045220
    const/16 v9, 0x3b

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-static {v0, v3, v1, v9}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v17

    .line 1045221
    const/16 v9, 0x3d

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-static {v0, v3, v1, v9}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v9

    .line 1045222
    move-object/from16 v0, p3

    invoke-static {v0, v3, v9}, LX/65A;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v18

    .line 1045223
    move/from16 v0, v17

    if-ge v9, v0, :cond_2

    add-int/lit8 v3, v9, 0x1

    .line 1045224
    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-static {v0, v3, v1}, LX/65A;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    .line 1045225
    :goto_2
    const-string v9, "expires"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1045226
    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    invoke-static {v3, v9, v0}, LX/64f;->a(Ljava/lang/String;II)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    .line 1045227
    const/4 v13, 0x1

    move-object v3, v8

    move-wide v8, v14

    .line 1045228
    :goto_3
    add-int/lit8 v14, v17, 0x1

    move-object/from16 v19, v3

    move v3, v14

    move-wide v14, v8

    move-object/from16 v8, v19

    .line 1045229
    goto :goto_1

    .line 1045230
    :cond_2
    const-string v3, ""

    goto :goto_2

    .line 1045231
    :catch_0
    move-object v3, v8

    move-wide v8, v14

    goto :goto_3

    .line 1045232
    :cond_3
    const-string v9, "max-age"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1045233
    :try_start_1
    invoke-static {v3}, LX/64f;->a(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v6

    .line 1045234
    const/4 v13, 0x1

    move-object v3, v8

    move-wide v8, v14

    .line 1045235
    goto :goto_3

    :catch_1
    move-object v3, v8

    move-wide v8, v14

    goto :goto_3

    .line 1045236
    :cond_4
    const-string v9, "domain"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1045237
    :try_start_2
    invoke-static {v3}, LX/64f;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    .line 1045238
    const/4 v12, 0x0

    move-wide v8, v14

    .line 1045239
    goto :goto_3

    :catch_2
    move-object v3, v8

    move-wide v8, v14

    goto :goto_3

    .line 1045240
    :cond_5
    const-string v9, "path"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    move-object v2, v3

    move-object v3, v8

    move-wide v8, v14

    .line 1045241
    goto :goto_3

    .line 1045242
    :cond_6
    const-string v3, "secure"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1045243
    const/4 v10, 0x1

    move-object v3, v8

    move-wide v8, v14

    goto :goto_3

    .line 1045244
    :cond_7
    const-string v3, "httponly"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1045245
    const/4 v11, 0x1

    move-object v3, v8

    move-wide v8, v14

    goto :goto_3

    .line 1045246
    :cond_8
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v3, v6, v16

    if-nez v3, :cond_c

    .line 1045247
    const-wide/high16 v6, -0x8000000000000000L

    .line 1045248
    :cond_9
    :goto_4
    if-nez v8, :cond_f

    .line 1045249
    invoke-virtual/range {p2 .. p2}, LX/64q;->f()Ljava/lang/String;

    move-result-object v8

    .line 1045250
    :cond_a
    if-eqz v2, :cond_b

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 1045251
    :cond_b
    invoke-virtual/range {p2 .. p2}, LX/64q;->h()Ljava/lang/String;

    move-result-object v2

    .line 1045252
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1045253
    if-eqz v3, :cond_10

    const/4 v9, 0x0

    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_5
    move-object v9, v2

    .line 1045254
    :goto_6
    new-instance v3, LX/64f;

    invoke-direct/range {v3 .. v13}, LX/64f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V

    goto/16 :goto_0

    .line 1045255
    :cond_c
    const-wide/16 v16, -0x1

    cmp-long v3, v6, v16

    if-eqz v3, :cond_12

    .line 1045256
    const-wide v14, 0x20c49ba5e353f7L

    cmp-long v3, v6, v14

    if-gtz v3, :cond_e

    const-wide/16 v14, 0x3e8

    mul-long/2addr v6, v14

    .line 1045257
    :goto_7
    add-long v6, v6, p0

    .line 1045258
    cmp-long v3, v6, p0

    if-ltz v3, :cond_d

    const-wide v14, 0xe677d21fdbffL

    cmp-long v3, v6, v14

    if-lez v3, :cond_9

    .line 1045259
    :cond_d
    const-wide v6, 0xe677d21fdbffL

    goto :goto_4

    .line 1045260
    :cond_e
    const-wide v6, 0x7fffffffffffffffL

    goto :goto_7

    .line 1045261
    :cond_f
    move-object/from16 v0, p2

    invoke-static {v0, v8}, LX/64f;->a(LX/64q;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1045262
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1045263
    :cond_10
    const-string v2, "/"

    goto :goto_5

    :cond_11
    move-object v9, v2

    goto :goto_6

    :cond_12
    move-wide v6, v14

    goto :goto_4

    :cond_13
    move-object v3, v8

    move-wide v8, v14

    goto/16 :goto_3
.end method

.method private static a(LX/64q;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1045195
    iget-object v1, p0, LX/64q;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1045196
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1045197
    :cond_0
    :goto_0
    return v0

    .line 1045198
    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1045199
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_2

    .line 1045200
    invoke-static {v1}, LX/65A;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1045201
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1045187
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045189
    :cond_0
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1045190
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 1045191
    :cond_1
    invoke-static {p0}, LX/65A;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1045192
    if-nez v0, :cond_2

    .line 1045193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1045194
    :cond_2
    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1045180
    instance-of v1, p1, LX/64f;

    if-nez v1, :cond_1

    .line 1045181
    :cond_0
    :goto_0
    return v0

    .line 1045182
    :cond_1
    check-cast p1, LX/64f;

    .line 1045183
    iget-object v1, p1, LX/64f;->e:Ljava/lang/String;

    iget-object v2, p0, LX/64f;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/64f;->f:Ljava/lang/String;

    iget-object v2, p0, LX/64f;->f:Ljava/lang/String;

    .line 1045184
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/64f;->h:Ljava/lang/String;

    iget-object v2, p0, LX/64f;->h:Ljava/lang/String;

    .line 1045185
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/64f;->i:Ljava/lang/String;

    iget-object v2, p0, LX/64f;->i:Ljava/lang/String;

    .line 1045186
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p1, LX/64f;->g:J

    iget-wide v4, p0, LX/64f;->g:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p1, LX/64f;->j:Z

    iget-boolean v2, p0, LX/64f;->j:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p1, LX/64f;->k:Z

    iget-boolean v2, p0, LX/64f;->k:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p1, LX/64f;->l:Z

    iget-boolean v2, p0, LX/64f;->l:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p1, LX/64f;->m:Z

    iget-boolean v2, p0, LX/64f;->m:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1045166
    iget-object v0, p0, LX/64f;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1045167
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, LX/64f;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 1045168
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, LX/64f;->h:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 1045169
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, LX/64f;->i:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 1045170
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, LX/64f;->g:J

    iget-wide v6, p0, LX/64f;->g:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 1045171
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/64f;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 1045172
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/64f;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 1045173
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/64f;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 1045174
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/64f;->m:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1045175
    return v0

    :cond_0
    move v0, v2

    .line 1045176
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1045177
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1045178
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1045179
    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1045148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1045149
    iget-object v1, p0, LX/64f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045150
    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045151
    iget-object v1, p0, LX/64f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045152
    iget-boolean v1, p0, LX/64f;->l:Z

    if-eqz v1, :cond_0

    .line 1045153
    iget-wide v2, p0, LX/64f;->g:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 1045154
    const-string v1, "; max-age=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045155
    :cond_0
    :goto_0
    iget-boolean v1, p0, LX/64f;->m:Z

    if-nez v1, :cond_1

    .line 1045156
    const-string v1, "; domain="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/64f;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045157
    :cond_1
    const-string v1, "; path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/64f;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045158
    iget-boolean v1, p0, LX/64f;->j:Z

    if-eqz v1, :cond_2

    .line 1045159
    const-string v1, "; secure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045160
    :cond_2
    iget-boolean v1, p0, LX/64f;->k:Z

    if-eqz v1, :cond_3

    .line 1045161
    const-string v1, "; httponly"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045162
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1045163
    :cond_4
    const-string v1, "; expires="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, LX/64f;->g:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 1045164
    sget-object v3, LX/66L;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1045165
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
