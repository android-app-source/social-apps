.class public final LX/5Vm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 930088
    const/16 v18, 0x0

    .line 930089
    const/16 v17, 0x0

    .line 930090
    const/16 v16, 0x0

    .line 930091
    const/4 v15, 0x0

    .line 930092
    const/4 v14, 0x0

    .line 930093
    const/4 v13, 0x0

    .line 930094
    const/4 v12, 0x0

    .line 930095
    const/4 v11, 0x0

    .line 930096
    const/4 v10, 0x0

    .line 930097
    const/4 v9, 0x0

    .line 930098
    const/4 v8, 0x0

    .line 930099
    const/4 v7, 0x0

    .line 930100
    const/4 v6, 0x0

    .line 930101
    const/4 v5, 0x0

    .line 930102
    const/4 v4, 0x0

    .line 930103
    const/4 v3, 0x0

    .line 930104
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 930105
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 930106
    const/4 v3, 0x0

    .line 930107
    :goto_0
    return v3

    .line 930108
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 930109
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_e

    .line 930110
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 930111
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 930112
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 930113
    const-string v20, "currency"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 930114
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto :goto_1

    .line 930115
    :cond_2
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 930116
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 930117
    :cond_3
    const-string v20, "invoice_notes"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 930118
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 930119
    :cond_4
    const-string v20, "page"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 930120
    invoke-static/range {p0 .. p1}, LX/5Vc;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 930121
    :cond_5
    const-string v20, "platform_context"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 930122
    invoke-static/range {p0 .. p1}, LX/5Vd;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 930123
    :cond_6
    const-string v20, "selected_transaction_payment_option"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 930124
    invoke-static/range {p0 .. p1}, LX/5Ve;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 930125
    :cond_7
    const-string v20, "transaction_discount"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 930126
    const/4 v5, 0x1

    .line 930127
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 930128
    :cond_8
    const-string v20, "transaction_payment"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 930129
    invoke-static/range {p0 .. p1}, LX/5Vg;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 930130
    :cond_9
    const-string v20, "transaction_products"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 930131
    invoke-static/range {p0 .. p1}, LX/5Vl;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 930132
    :cond_a
    const-string v20, "transaction_status"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 930133
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 930134
    :cond_b
    const-string v20, "transaction_status_display"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 930135
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 930136
    :cond_c
    const-string v20, "transaction_subtotal_cost"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 930137
    const/4 v4, 0x1

    .line 930138
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 930139
    :cond_d
    const-string v20, "transaction_total_cost"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 930140
    const/4 v3, 0x1

    .line 930141
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 930142
    :cond_e
    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 930143
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 930144
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 930145
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 930146
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 930147
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 930148
    const/4 v14, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 930149
    if-eqz v5, :cond_f

    .line 930150
    const/4 v5, 0x6

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v13}, LX/186;->a(III)V

    .line 930151
    :cond_f
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 930152
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 930153
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 930154
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 930155
    if-eqz v4, :cond_10

    .line 930156
    const/16 v4, 0xb

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v5}, LX/186;->a(III)V

    .line 930157
    :cond_10
    if-eqz v3, :cond_11

    .line 930158
    const/16 v3, 0xc

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v4}, LX/186;->a(III)V

    .line 930159
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
