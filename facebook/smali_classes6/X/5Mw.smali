.class public LX/5Mw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# instance fields
.field private final a:LX/0yc;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0yc;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 905766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905767
    iput-object p1, p0, LX/5Mw;->a:LX/0yc;

    .line 905768
    iput-object p2, p0, LX/5Mw;->b:Lcom/facebook/content/SecureContextHelper;

    .line 905769
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 905770
    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905771
    iget-object v1, p0, LX/5Mw;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p3, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 905772
    const/4 v0, 0x1

    .line 905773
    :cond_0
    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 905774
    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905775
    iget-object v1, p0, LX/5Mw;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 905776
    const/4 v0, 0x1

    .line 905777
    :cond_0
    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 905778
    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/5Mw;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905779
    iget-object v1, p0, LX/5Mw;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p2, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 905780
    const/4 v0, 0x1

    .line 905781
    :cond_0
    return v0
.end method
