.class public final LX/5zX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1035129
    const/16 v17, 0x0

    .line 1035130
    const/16 v16, 0x0

    .line 1035131
    const/4 v15, 0x0

    .line 1035132
    const/4 v14, 0x0

    .line 1035133
    const/4 v13, 0x0

    .line 1035134
    const/4 v12, 0x0

    .line 1035135
    const/4 v11, 0x0

    .line 1035136
    const/4 v10, 0x0

    .line 1035137
    const/4 v9, 0x0

    .line 1035138
    const/4 v8, 0x0

    .line 1035139
    const/4 v7, 0x0

    .line 1035140
    const/4 v6, 0x0

    .line 1035141
    const/4 v5, 0x0

    .line 1035142
    const/4 v4, 0x0

    .line 1035143
    const/4 v3, 0x0

    .line 1035144
    const/4 v2, 0x0

    .line 1035145
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1035146
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1035147
    const/4 v2, 0x0

    .line 1035148
    :goto_0
    return v2

    .line 1035149
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1035150
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_b

    .line 1035151
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1035152
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1035153
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1035154
    const-string v19, "can_viewer_act_as_memorial_contact"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1035155
    const/4 v7, 0x1

    .line 1035156
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto :goto_1

    .line 1035157
    :cond_2
    const-string v19, "can_viewer_block"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1035158
    const/4 v6, 0x1

    .line 1035159
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 1035160
    :cond_3
    const-string v19, "can_viewer_message"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1035161
    const/4 v5, 0x1

    .line 1035162
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 1035163
    :cond_4
    const-string v19, "can_viewer_poke"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1035164
    const/4 v4, 0x1

    .line 1035165
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 1035166
    :cond_5
    const-string v19, "can_viewer_post"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1035167
    const/4 v3, 0x1

    .line 1035168
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 1035169
    :cond_6
    const-string v19, "can_viewer_report"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1035170
    const/4 v2, 0x1

    .line 1035171
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1035172
    :cond_7
    const-string v19, "friends"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1035173
    invoke-static/range {p0 .. p1}, LX/5zW;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1035174
    :cond_8
    const-string v19, "friendship_status"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1035175
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1035176
    :cond_9
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1035177
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1035178
    :cond_a
    const-string v19, "name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1035179
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1035180
    :cond_b
    const/16 v18, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1035181
    if-eqz v7, :cond_c

    .line 1035182
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1035183
    :cond_c
    if-eqz v6, :cond_d

    .line 1035184
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1035185
    :cond_d
    if-eqz v5, :cond_e

    .line 1035186
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 1035187
    :cond_e
    if-eqz v4, :cond_f

    .line 1035188
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->a(IZ)V

    .line 1035189
    :cond_f
    if-eqz v3, :cond_10

    .line 1035190
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->a(IZ)V

    .line 1035191
    :cond_10
    if-eqz v2, :cond_11

    .line 1035192
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1035193
    :cond_11
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1035194
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1035195
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1035196
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1035197
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
