.class public final LX/5DM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 871506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 871507
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 871508
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 871509
    invoke-static {p0, p1}, LX/5DM;->b(LX/15w;LX/186;)I

    move-result v1

    .line 871510
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 871511
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 871512
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 871513
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 871514
    if-eqz v0, :cond_0

    .line 871515
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871516
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 871517
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 871518
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 871519
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 871520
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 871521
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;)V

    .line 871522
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871523
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 871524
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 871525
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_4

    .line 871526
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 871527
    :goto_0
    return v1

    .line 871528
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 871529
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 871530
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 871531
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_0

    if-eqz v4, :cond_0

    .line 871532
    const-string v5, "key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 871533
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 871534
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 871535
    :cond_2
    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 871536
    if-eqz v0, :cond_3

    .line 871537
    invoke-virtual {p1, v1, v3, v1}, LX/186;->a(III)V

    .line 871538
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v3, v1

    goto :goto_1
.end method
