.class public LX/5PO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:LX/5Pf;

.field public e:[I


# direct methods
.method public constructor <init>(II)V
    .locals 10

    .prologue
    const/16 v2, 0x1908

    const/4 v4, 0x1

    const v9, 0x8d40

    const/16 v0, 0xde1

    const/4 v1, 0x0

    .line 910643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910644
    iput p1, p0, LX/5PO;->a:I

    .line 910645
    iput p2, p0, LX/5PO;->b:I

    .line 910646
    new-array v3, v4, [I

    .line 910647
    invoke-static {v4, v3, v1}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    .line 910648
    aget v3, v3, v1

    iput v3, p0, LX/5PO;->c:I

    .line 910649
    iget v3, p0, LX/5PO;->c:I

    invoke-static {v9, v3}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 910650
    new-instance v3, LX/5Pe;

    invoke-direct {v3}, LX/5Pe;-><init>()V

    .line 910651
    iput v0, v3, LX/5Pe;->a:I

    .line 910652
    move-object v3, v3

    .line 910653
    const/16 v4, 0x2801

    const/16 v5, 0x2601

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2800

    const/16 v5, 0x2601

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2802

    const v5, 0x812f

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2803

    const v5, 0x812f

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    invoke-virtual {v3}, LX/5Pe;->a()LX/5Pf;

    move-result-object v3

    iput-object v3, p0, LX/5PO;->d:LX/5Pf;

    .line 910654
    iget-object v3, p0, LX/5PO;->d:LX/5Pf;

    iget v3, v3, LX/5Pf;->b:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 910655
    const/16 v7, 0x1401

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 910656
    const v2, 0x8ce0

    iget-object v3, p0, LX/5PO;->d:LX/5Pf;

    iget v3, v3, LX/5Pf;->b:I

    invoke-static {v9, v2, v0, v3, v1}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 910657
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 910658
    invoke-static {v9, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 910659
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const v5, 0x8d41

    const v4, 0x8d40

    const/4 v3, 0x0

    .line 910660
    iget v0, p0, LX/5PO;->c:I

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 910661
    new-array v0, v1, [I

    iput-object v0, p0, LX/5PO;->e:[I

    .line 910662
    iget-object v0, p0, LX/5PO;->e:[I

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glGenRenderbuffers(I[II)V

    .line 910663
    iget-object v0, p0, LX/5PO;->e:[I

    aget v0, v0, v3

    invoke-static {v5, v0}, Landroid/opengl/GLES20;->glBindRenderbuffer(II)V

    .line 910664
    const v0, 0x81a5

    iget v1, p0, LX/5PO;->a:I

    iget v2, p0, LX/5PO;->b:I

    invoke-static {v5, v0, v1, v2}, Landroid/opengl/GLES20;->glRenderbufferStorage(IIII)V

    .line 910665
    const v0, 0x8d00

    iget-object v1, p0, LX/5PO;->e:[I

    aget v1, v1, v3

    invoke-static {v4, v0, v5, v1}, Landroid/opengl/GLES20;->glFramebufferRenderbuffer(IIII)V

    .line 910666
    invoke-static {v4}, Landroid/opengl/GLES20;->glCheckFramebufferStatus(I)I

    move-result v0

    const v1, 0x8cd5

    if-eq v0, v1, :cond_0

    .line 910667
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Depth buffer attachment to FrameBufferTexture failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910668
    :cond_0
    invoke-static {v5, v3}, Landroid/opengl/GLES20;->glBindRenderbuffer(II)V

    .line 910669
    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 910670
    return-void
.end method
