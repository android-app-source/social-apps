.class public final LX/6DX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;)V
    .locals 0

    .prologue
    .line 1065266
    iput-object p1, p0, LX/6DX;->a:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x7cedb5ea

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1065267
    iget-object v1, p0, LX/6DX;->a:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    .line 1065268
    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->p:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1065269
    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, p0, :cond_0

    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1065270
    iget-object p1, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->s:LX/6DO;

    invoke-virtual {p1, v3}, LX/6DO;->a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)V

    .line 1065271
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1065272
    :cond_0
    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->r:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    if-eqz v3, :cond_1

    .line 1065273
    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->t:LX/6D0;

    iget-object v4, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->r:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 1065274
    iget-object p0, v4, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v4, p0

    .line 1065275
    invoke-virtual {v3, v4}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->u:LX/6Cz;

    .line 1065276
    iget-object v3, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->u:LX/6Cz;

    iget-object v4, v1, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->r:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    const-string p0, "browser_extensions_save_autofill_dialog_accepted"

    invoke-virtual {v3, v4, p0}, LX/6Cz;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065277
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1065278
    const v1, 0x84eaa35

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
