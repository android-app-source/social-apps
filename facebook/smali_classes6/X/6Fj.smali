.class public final LX/6Fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Ljava/util/Map;

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:LX/6Ft;


# direct methods
.method public constructor <init>(LX/6Ft;Landroid/os/Bundle;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1068902
    iput-object p1, p0, LX/6Fj;->d:LX/6Ft;

    iput-object p2, p0, LX/6Fj;->a:Landroid/os/Bundle;

    iput-object p3, p0, LX/6Fj;->b:Ljava/util/Map;

    iput-object p4, p0, LX/6Fj;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1068903
    iget-object v0, p0, LX/6Fj;->d:LX/6Ft;

    iget-object v0, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aX:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 1068904
    iget-object v1, p0, LX/6Fj;->d:LX/6Ft;

    iget-object v2, p0, LX/6Fj;->a:Landroid/os/Bundle;

    .line 1068905
    iget-object v3, v1, LX/6Ft;->l:LX/1Er;

    const-string v4, "view_hierarchy"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v3

    .line 1068906
    iget-object v4, v1, LX/6Ft;->m:LX/6V0;

    sget-object v5, LX/6Uy;->PRETTY:LX/6Uy;

    invoke-virtual {v4, v3, v5, v2}, LX/6V0;->a(Ljava/io/File;LX/6Uy;Landroid/os/Bundle;)Z

    .line 1068907
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    move-object v1, v3

    .line 1068908
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Fj;->b:Ljava/util/Map;

    :goto_0
    const-string v2, "view_hierarchy.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1068909
    const/4 v0, 0x0

    return-object v0

    .line 1068910
    :cond_0
    iget-object v0, p0, LX/6Fj;->c:Ljava/util/Map;

    goto :goto_0
.end method
