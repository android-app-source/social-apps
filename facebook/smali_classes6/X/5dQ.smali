.class public final enum LX/5dQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5dQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5dQ;

.field public static final enum FULL_SCREEN:LX/5dQ;

.field public static final enum LARGE_PREVIEW:LX/5dQ;

.field public static final enum MEDIUM_PREVIEW:LX/5dQ;

.field public static final enum SMALL_PREVIEW:LX/5dQ;


# instance fields
.field public final persistentIndex:Ljava/lang/Integer;

.field public final serializedName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 965631
    new-instance v0, LX/5dQ;

    const-string v1, "FULL_SCREEN"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FULL_SCREEN"

    invoke-direct {v0, v1, v4, v2, v3}, LX/5dQ;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;)V

    sput-object v0, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    .line 965632
    new-instance v0, LX/5dQ;

    const-string v1, "SMALL_PREVIEW"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "SMALL_PREVIEW"

    invoke-direct {v0, v1, v5, v2, v3}, LX/5dQ;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;)V

    sput-object v0, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    .line 965633
    new-instance v0, LX/5dQ;

    const-string v1, "MEDIUM_PREVIEW"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "MEDIUM_PREVIEW"

    invoke-direct {v0, v1, v6, v2, v3}, LX/5dQ;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;)V

    sput-object v0, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    .line 965634
    new-instance v0, LX/5dQ;

    const-string v1, "LARGE_PREVIEW"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "LARGE_PREVIEW"

    invoke-direct {v0, v1, v7, v2, v3}, LX/5dQ;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;)V

    sput-object v0, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    .line 965635
    const/4 v0, 0x4

    new-array v0, v0, [LX/5dQ;

    sget-object v1, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    aput-object v1, v0, v4

    sget-object v1, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    aput-object v1, v0, v5

    sget-object v1, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    aput-object v1, v0, v6

    sget-object v1, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    aput-object v1, v0, v7

    sput-object v0, LX/5dQ;->$VALUES:[LX/5dQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 965636
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 965637
    iput-object p3, p0, LX/5dQ;->persistentIndex:Ljava/lang/Integer;

    .line 965638
    iput-object p4, p0, LX/5dQ;->serializedName:Ljava/lang/String;

    .line 965639
    return-void
.end method

.method public static fromPersistentIndex(I)LX/5dQ;
    .locals 1

    .prologue
    .line 965640
    if-ltz p0, :cond_0

    const/4 v0, 0x3

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 965641
    packed-switch p0, :pswitch_data_0

    .line 965642
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 965643
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 965644
    :pswitch_0
    sget-object v0, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    .line 965645
    :goto_1
    return-object v0

    .line 965646
    :pswitch_1
    sget-object v0, LX/5dQ;->LARGE_PREVIEW:LX/5dQ;

    goto :goto_1

    .line 965647
    :pswitch_2
    sget-object v0, LX/5dQ;->MEDIUM_PREVIEW:LX/5dQ;

    goto :goto_1

    .line 965648
    :pswitch_3
    sget-object v0, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static fromSerializedName(Ljava/lang/String;)LX/5dQ;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965649
    invoke-static {}, LX/5dQ;->values()[LX/5dQ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 965650
    iget-object v4, v0, LX/5dQ;->serializedName:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 965651
    :goto_1
    return-object v0

    .line 965652
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 965653
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5dQ;
    .locals 1

    .prologue
    .line 965654
    const-class v0, LX/5dQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5dQ;

    return-object v0
.end method

.method public static values()[LX/5dQ;
    .locals 1

    .prologue
    .line 965655
    sget-object v0, LX/5dQ;->$VALUES:[LX/5dQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5dQ;

    return-object v0
.end method
