.class public LX/5OI;
.super Landroid/view/MenuInflater;
.source ""


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 908308
    invoke-direct {p0, p1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 908309
    iput-object p1, p0, LX/5OI;->a:Landroid/content/Context;

    .line 908310
    return-void
.end method

.method private static a(LX/5OH;Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 908370
    iget-boolean v0, p0, LX/5OH;->i:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, LX/5OH;->j:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget v1, p0, LX/5OH;->f:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, LX/5OH;->g:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, LX/5OH;->h:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 908371
    instance-of v0, p1, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 908372
    check-cast v0, LX/3Ai;

    iget-object v1, p0, LX/5OH;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 908373
    check-cast p1, LX/3Ai;

    iget-object v0, p0, LX/5OH;->e:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, LX/3Ai;->b(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 908374
    :cond_0
    return-void
.end method

.method private a(Landroid/content/res/XmlResourceParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 908324
    const/4 v1, 0x0

    .line 908325
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v0

    move v6, v0

    move-object v0, v1

    move v1, v6

    .line 908326
    :cond_0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 908327
    packed-switch v1, :pswitch_data_0

    .line 908328
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 908329
    if-ne v1, v5, :cond_0

    .line 908330
    :goto_1
    return-void

    .line 908331
    :pswitch_0
    const-string v1, "item"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 908332
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 908333
    new-instance v0, LX/5OH;

    invoke-direct {v0, p0}, LX/5OH;-><init>(LX/5OI;)V

    .line 908334
    iget-object v1, p0, LX/5OI;->a:Landroid/content/Context;

    sget-object v2, LX/03r;->MenuItemImpl:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 908335
    const/16 v2, 0x3

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, v0, LX/5OH;->a:I

    .line 908336
    const/16 v2, 0x6

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, v0, LX/5OH;->b:I

    .line 908337
    const/16 v2, 0x7

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 908338
    if-lez v2, :cond_4

    .line 908339
    iget-object v3, p0, LX/5OI;->a:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->c:Ljava/lang/CharSequence;

    .line 908340
    :goto_2
    const/16 v2, 0x2

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 908341
    if-lez v2, :cond_5

    .line 908342
    iget-object v3, p0, LX/5OI;->a:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->d:Ljava/lang/CharSequence;

    .line 908343
    :goto_3
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 908344
    if-lez v2, :cond_6

    .line 908345
    iget-object v3, p0, LX/5OI;->a:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->e:Ljava/lang/CharSequence;

    .line 908346
    :goto_4
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, v0, LX/5OH;->f:I

    .line 908347
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v0, LX/5OH;->g:Z

    .line 908348
    const/16 v2, 0x4

    invoke-virtual {v1, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v0, LX/5OH;->h:Z

    .line 908349
    const/16 v2, 0x5

    invoke-virtual {v1, v2, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v0, LX/5OH;->i:Z

    .line 908350
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v0, LX/5OH;->j:Z

    .line 908351
    iput-boolean v6, v0, LX/5OH;->k:Z

    .line 908352
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 908353
    move-object v0, v0

    .line 908354
    goto/16 :goto_0

    .line 908355
    :cond_2
    const-string v1, "menu"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 908356
    if-eqz v0, :cond_1

    .line 908357
    iget v1, v0, LX/5OH;->a:I

    iget v2, v0, LX/5OH;->b:I

    iget-object v3, v0, LX/5OH;->c:Ljava/lang/CharSequence;

    invoke-interface {p3, v4, v1, v2, v3}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    .line 908358
    iput-boolean v5, v0, LX/5OH;->k:Z

    .line 908359
    invoke-interface {v1}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v2

    .line 908360
    invoke-static {v0, v2}, LX/5OI;->a(LX/5OH;Landroid/view/MenuItem;)V

    .line 908361
    invoke-direct {p0, p1, p2, v1}, LX/5OI;->a(Landroid/content/res/XmlResourceParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V

    goto/16 :goto_0

    .line 908362
    :pswitch_1
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "item"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 908363
    iget-boolean v1, v0, LX/5OH;->k:Z

    if-nez v1, :cond_1

    .line 908364
    iget v1, v0, LX/5OH;->a:I

    iget v2, v0, LX/5OH;->b:I

    iget-object v3, v0, LX/5OH;->c:Ljava/lang/CharSequence;

    invoke-interface {p3, v4, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 908365
    invoke-static {v0, v1}, LX/5OI;->a(LX/5OH;Landroid/view/MenuItem;)V

    goto/16 :goto_0

    .line 908366
    :cond_3
    const-string v1, "menu"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_1

    .line 908367
    :cond_4
    const/16 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->c:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 908368
    :cond_5
    const/16 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->d:Ljava/lang/CharSequence;

    goto/16 :goto_3

    .line 908369
    :cond_6
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, LX/5OH;->e:Ljava/lang/CharSequence;

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final inflate(ILandroid/view/Menu;)V
    .locals 4

    .prologue
    .line 908311
    const/4 v1, 0x0

    .line 908312
    :try_start_0
    iget-object v0, p0, LX/5OI;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 908313
    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 908314
    invoke-direct {p0, v1, v0, p2}, LX/5OI;->a(Landroid/content/res/XmlResourceParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 908315
    if-eqz v1, :cond_0

    .line 908316
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 908317
    :cond_0
    return-void

    .line 908318
    :catch_0
    move-exception v0

    .line 908319
    :try_start_1
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 908320
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 908321
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    throw v0

    .line 908322
    :catch_1
    move-exception v0

    .line 908323
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
