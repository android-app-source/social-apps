.class public final LX/6Bz;
.super LX/6By;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/blescan/BleScanBroadcastReceiver;


# direct methods
.method public constructor <init>(Lcom/facebook/blescan/BleScanBroadcastReceiver;)V
    .locals 0

    .prologue
    .line 1063329
    iput-object p1, p0, LX/6Bz;->a:Lcom/facebook/blescan/BleScanBroadcastReceiver;

    invoke-direct {p0}, LX/6By;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/nearby/messages/Message;)V
    .locals 7

    .prologue
    .line 1063320
    iget-object v0, p1, Lcom/google/android/gms/nearby/messages/Message;->c:[B

    move-object v0, v0

    .line 1063321
    invoke-static {v0}, LX/6C1;->a([B)Ljava/lang/String;

    .line 1063322
    const-string v0, "__i_beacon_id"

    iget-object v1, p1, Lcom/google/android/gms/nearby/messages/Message;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1063323
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063324
    invoke-static {p1}, LX/7dy;->a(Lcom/google/android/gms/nearby/messages/Message;)LX/7dy;

    move-result-object v0

    .line 1063325
    invoke-virtual {v0}, LX/7dy;->a()Ljava/util/UUID;

    invoke-virtual {v0}, LX/7dy;->b()S

    invoke-virtual {v0}, LX/7dy;->c()S

    .line 1063326
    iget-object v0, p0, LX/6Bz;->a:Lcom/facebook/blescan/BleScanBroadcastReceiver;

    iget-object v0, v0, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a:LX/3C7;

    new-instance v1, Lcom/facebook/blescan/BleScanResult;

    iget-object v2, p0, LX/6Bz;->a:Lcom/facebook/blescan/BleScanBroadcastReceiver;

    iget-object v2, v2, Lcom/facebook/blescan/BleScanBroadcastReceiver;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-string v4, ""

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/gms/nearby/messages/Message;->c:[B

    move-object v6, v6

    .line 1063327
    invoke-static {v6}, LX/6C1;->a([B)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/facebook/blescan/BleScanResult;-><init>(JLjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/3C7;->a(Lcom/facebook/blescan/BleScanResult;)V

    .line 1063328
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/nearby/messages/Message;)V
    .locals 2

    .prologue
    .line 1063311
    iget-object v0, p1, Lcom/google/android/gms/nearby/messages/Message;->c:[B

    move-object v0, v0

    .line 1063312
    invoke-static {v0}, LX/6C1;->a([B)Ljava/lang/String;

    .line 1063313
    const-string v0, "__i_beacon_id"

    iget-object v1, p1, Lcom/google/android/gms/nearby/messages/Message;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1063314
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063315
    invoke-static {p1}, LX/7dy;->a(Lcom/google/android/gms/nearby/messages/Message;)LX/7dy;

    move-result-object v0

    .line 1063316
    invoke-virtual {v0}, LX/7dy;->a()Ljava/util/UUID;

    invoke-virtual {v0}, LX/7dy;->b()S

    invoke-virtual {v0}, LX/7dy;->c()S

    .line 1063317
    iget-object v0, p0, LX/6Bz;->a:Lcom/facebook/blescan/BleScanBroadcastReceiver;

    iget-object v0, v0, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a:LX/3C7;

    iget-object v1, p1, Lcom/google/android/gms/nearby/messages/Message;->c:[B

    move-object v1, v1

    .line 1063318
    invoke-static {v1}, LX/6C1;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3C7;->a(Ljava/lang/String;)V

    .line 1063319
    :cond_0
    return-void
.end method
