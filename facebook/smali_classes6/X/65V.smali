.class public final LX/65V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64R;

.field public final b:LX/65T;

.field private c:Ljava/net/Proxy;

.field private d:Ljava/net/InetSocketAddress;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/net/Proxy;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/net/InetSocketAddress;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/657;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/64R;LX/65T;)V
    .locals 2

    .prologue
    .line 1047447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1047448
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/65V;->e:Ljava/util/List;

    .line 1047449
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/65V;->g:Ljava/util/List;

    .line 1047450
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/65V;->i:Ljava/util/List;

    .line 1047451
    iput-object p1, p0, LX/65V;->a:LX/64R;

    .line 1047452
    iput-object p2, p0, LX/65V;->b:LX/65T;

    .line 1047453
    iget-object v0, p1, LX/64R;->a:LX/64q;

    move-object v0, v0

    .line 1047454
    iget-object v1, p1, LX/64R;->h:Ljava/net/Proxy;

    move-object v1, v1

    .line 1047455
    if-eqz v1, :cond_0

    .line 1047456
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, LX/65V;->e:Ljava/util/List;

    .line 1047457
    :goto_0
    const/4 p1, 0x0

    iput p1, p0, LX/65V;->f:I

    .line 1047458
    return-void

    .line 1047459
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, LX/65V;->e:Ljava/util/List;

    .line 1047460
    iget-object p1, p0, LX/65V;->a:LX/64R;

    .line 1047461
    iget-object p2, p1, LX/64R;->g:Ljava/net/ProxySelector;

    move-object p1, p2

    .line 1047462
    invoke-virtual {v0}, LX/64q;->a()Ljava/net/URI;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object p1

    .line 1047463
    if-eqz p1, :cond_1

    iget-object p2, p0, LX/65V;->e:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1047464
    :cond_1
    iget-object p1, p0, LX/65V;->e:Ljava/util/List;

    sget-object p2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-static {p2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1047465
    iget-object p1, p0, LX/65V;->e:Ljava/util/List;

    sget-object p2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/net/Proxy;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1047415
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/65V;->g:Ljava/util/List;

    .line 1047416
    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->SOCKS:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_2

    .line 1047417
    :cond_0
    iget-object v0, p0, LX/65V;->a:LX/64R;

    .line 1047418
    iget-object v1, v0, LX/64R;->a:LX/64q;

    move-object v0, v1

    .line 1047419
    iget-object v1, v0, LX/64q;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1047420
    iget-object v0, p0, LX/65V;->a:LX/64R;

    .line 1047421
    iget-object v3, v0, LX/64R;->a:LX/64q;

    move-object v0, v3

    .line 1047422
    iget v3, v0, LX/64q;->f:I

    move v0, v3

    .line 1047423
    move v3, v0

    move-object v0, v1

    .line 1047424
    :goto_0
    if-lez v3, :cond_1

    const v1, 0xffff

    if-le v3, v1, :cond_4

    .line 1047425
    :cond_1
    new-instance v1, Ljava/net/SocketException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No route to "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; port is out of range"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1047426
    :cond_2
    invoke-virtual {p1}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    .line 1047427
    instance-of v1, v0, Ljava/net/InetSocketAddress;

    if-nez v1, :cond_3

    .line 1047428
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Proxy.address() is not an InetSocketAddress: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1047429
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1047430
    :cond_3
    check-cast v0, Ljava/net/InetSocketAddress;

    .line 1047431
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 1047432
    if-nez v1, :cond_7

    .line 1047433
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 1047434
    :goto_1
    move-object v1, v1

    .line 1047435
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    move v3, v0

    move-object v0, v1

    goto :goto_0

    .line 1047436
    :cond_4
    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v4, Ljava/net/Proxy$Type;->SOCKS:Ljava/net/Proxy$Type;

    if-ne v1, v4, :cond_6

    .line 1047437
    iget-object v1, p0, LX/65V;->g:Ljava/util/List;

    invoke-static {v0, v3}, Ljava/net/InetSocketAddress;->createUnresolved(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047438
    :cond_5
    iput v2, p0, LX/65V;->h:I

    .line 1047439
    return-void

    .line 1047440
    :cond_6
    iget-object v1, p0, LX/65V;->a:LX/64R;

    .line 1047441
    iget-object v4, v1, LX/64R;->b:LX/64j;

    move-object v1, v4

    .line 1047442
    invoke-interface {v1, v0}, LX/64j;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 1047443
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_5

    .line 1047444
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 1047445
    iget-object v6, p0, LX/65V;->g:Ljava/util/List;

    new-instance v7, Ljava/net/InetSocketAddress;

    invoke-direct {v7, v0, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047446
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static c(LX/65V;)Z
    .locals 2

    .prologue
    .line 1047414
    iget v0, p0, LX/65V;->f:I

    iget-object v1, p0, LX/65V;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/net/Proxy;
    .locals 4

    .prologue
    .line 1047406
    invoke-static {p0}, LX/65V;->c(LX/65V;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1047407
    new-instance v0, Ljava/net/SocketException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No route to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/65V;->a:LX/64R;

    .line 1047408
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1047409
    iget-object v3, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1047410
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; exhausted proxy configurations: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/65V;->e:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047411
    :cond_0
    iget-object v0, p0, LX/65V;->e:Ljava/util/List;

    iget v1, p0, LX/65V;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/65V;->f:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Proxy;

    .line 1047412
    invoke-direct {p0, v0}, LX/65V;->a(Ljava/net/Proxy;)V

    .line 1047413
    return-object v0
.end method

.method public static e(LX/65V;)Z
    .locals 2

    .prologue
    .line 1047405
    iget v0, p0, LX/65V;->h:I

    iget-object v1, p0, LX/65V;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Ljava/net/InetSocketAddress;
    .locals 4

    .prologue
    .line 1047399
    invoke-static {p0}, LX/65V;->e(LX/65V;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1047400
    new-instance v0, Ljava/net/SocketException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No route to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/65V;->a:LX/64R;

    .line 1047401
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1047402
    iget-object v3, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1047403
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; exhausted inet socket addresses: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/65V;->g:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047404
    :cond_0
    iget-object v0, p0, LX/65V;->g:Ljava/util/List;

    iget v1, p0, LX/65V;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/65V;->h:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method public static g(LX/65V;)Z
    .locals 1

    .prologue
    .line 1047386
    iget-object v0, p0, LX/65V;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/657;
    .locals 4

    .prologue
    .line 1047387
    invoke-static {p0}, LX/65V;->e(LX/65V;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1047388
    invoke-static {p0}, LX/65V;->c(LX/65V;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1047389
    invoke-static {p0}, LX/65V;->g(LX/65V;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1047390
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1047391
    :cond_0
    iget-object v0, p0, LX/65V;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/657;

    move-object v0, v0

    .line 1047392
    :cond_1
    :goto_0
    return-object v0

    .line 1047393
    :cond_2
    invoke-direct {p0}, LX/65V;->d()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, LX/65V;->c:Ljava/net/Proxy;

    .line 1047394
    :cond_3
    invoke-direct {p0}, LX/65V;->f()Ljava/net/InetSocketAddress;

    move-result-object v0

    iput-object v0, p0, LX/65V;->d:Ljava/net/InetSocketAddress;

    .line 1047395
    new-instance v0, LX/657;

    iget-object v1, p0, LX/65V;->a:LX/64R;

    iget-object v2, p0, LX/65V;->c:Ljava/net/Proxy;

    iget-object v3, p0, LX/65V;->d:Ljava/net/InetSocketAddress;

    invoke-direct {v0, v1, v2, v3}, LX/657;-><init>(LX/64R;Ljava/net/Proxy;Ljava/net/InetSocketAddress;)V

    .line 1047396
    iget-object v1, p0, LX/65V;->b:LX/65T;

    invoke-virtual {v1, v0}, LX/65T;->c(LX/657;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1047397
    iget-object v1, p0, LX/65V;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047398
    invoke-virtual {p0}, LX/65V;->b()LX/657;

    move-result-object v0

    goto :goto_0
.end method
