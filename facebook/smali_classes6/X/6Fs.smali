.class public final LX/6Fs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:LX/0P1;

.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Ljava/util/Map;

.field public final synthetic f:LX/6Ft;


# direct methods
.method public constructor <init>(LX/6Ft;Ljava/io/File;Ljava/io/File;LX/0P1;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1069004
    iput-object p1, p0, LX/6Fs;->f:LX/6Ft;

    iput-object p2, p0, LX/6Fs;->a:Ljava/io/File;

    iput-object p3, p0, LX/6Fs;->b:Ljava/io/File;

    iput-object p4, p0, LX/6Fs;->c:LX/0P1;

    iput-object p5, p0, LX/6Fs;->d:Ljava/util/Map;

    iput-object p6, p0, LX/6Fs;->e:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1069005
    iget-object v0, p0, LX/6Fs;->f:LX/6Ft;

    iget-object v0, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aP:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 1069006
    iget-object v2, p0, LX/6Fs;->f:LX/6Ft;

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/6Fs;->a:Ljava/io/File;

    :goto_0
    iget-object v3, p0, LX/6Fs;->c:LX/0P1;

    invoke-static {v2, v0, v3}, LX/6Ft;->a$redex0(LX/6Ft;Ljava/io/File;LX/0P1;)Landroid/net/Uri;

    move-result-object v2

    .line 1069007
    if-eqz v2, :cond_0

    .line 1069008
    if-eqz v1, :cond_2

    iget-object v0, p0, LX/6Fs;->d:Ljava/util/Map;

    :goto_1
    const-string v1, "extra_data.txt"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069009
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 1069010
    :cond_1
    iget-object v0, p0, LX/6Fs;->b:Ljava/io/File;

    goto :goto_0

    .line 1069011
    :cond_2
    iget-object v0, p0, LX/6Fs;->e:Ljava/util/Map;

    goto :goto_1
.end method
