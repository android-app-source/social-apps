.class public abstract LX/5Jl;
.super LX/5Je;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5Je",
        "<",
        "LX/1P1;",
        "LX/5K5;",
        ">;",
        "Lcom/facebook/components/widget/HasStickyHeader;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1P1;)V
    .locals 1

    .prologue
    .line 897684
    new-instance v0, LX/5K5;

    invoke-direct {v0}, LX/5K5;-><init>()V

    invoke-direct {p0, p1, p2, v0}, LX/5Jl;-><init>(Landroid/content/Context;LX/1P1;LX/5K5;)V

    .line 897685
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1P1;LX/5K5;)V
    .locals 0

    .prologue
    .line 897686
    invoke-direct {p0, p1, p2, p3}, LX/5Je;-><init>(Landroid/content/Context;LX/1OR;LX/5K5;)V

    .line 897687
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 897688
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 897675
    invoke-super {p0, p1, p2, p3}, LX/5Je;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 897676
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 897677
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    .line 897678
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v2

    .line 897679
    if-eq v1, v3, :cond_0

    if-ne v2, v3, :cond_1

    .line 897680
    :cond_0
    :goto_0
    return-void

    .line 897681
    :cond_1
    goto :goto_1

    .line 897682
    :goto_1
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897683
    check-cast v0, LX/5K5;

    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/5K5;->a(II)V

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 897674
    const/4 v0, 0x0

    return v0
.end method

.method public final b(III)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 897654
    invoke-virtual {p0}, LX/3mY;->bF_()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 897655
    iput v2, p0, LX/5Jl;->a:I

    .line 897656
    :cond_0
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 897657
    iget v3, v0, LX/1P1;->j:I

    move v0, v3

    .line 897658
    if-ne v0, v1, :cond_1

    .line 897659
    invoke-virtual {p0}, LX/3mY;->c()I

    move-result v0

    .line 897660
    :goto_0
    iget v3, p0, LX/5Jl;->a:I

    add-int/2addr v3, p3

    iput v3, p0, LX/5Jl;->a:I

    .line 897661
    iget v3, p0, LX/5Jl;->a:I

    mul-int/lit8 v0, v0, 0x2

    if-ge v3, v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    .line 897662
    :cond_1
    invoke-virtual {p0}, LX/3mY;->d()I

    move-result v0

    move p3, p2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 897663
    goto :goto_1
.end method

.method public final e(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 897669
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 897670
    iget v1, v0, LX/1P1;->j:I

    move v0, v1

    .line 897671
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 897672
    invoke-super {p0, p1}, LX/5Je;->e(I)I

    move-result v0

    .line 897673
    :goto_0
    return v0

    :cond_0
    invoke-static {v2, v2}, LX/1mh;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final f(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 897664
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 897665
    iget v1, v0, LX/1P1;->j:I

    move v0, v1

    .line 897666
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 897667
    invoke-static {v2, v2}, LX/1mh;->a(II)I

    move-result v0

    .line 897668
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/5Je;->f(I)I

    move-result v0

    goto :goto_0
.end method
