.class public final LX/5er;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 970027
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 970028
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 970029
    :goto_0
    return v1

    .line 970030
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 970031
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 970032
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 970033
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 970034
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 970035
    const-string v4, "current_value"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 970036
    invoke-static {p0, p1}, LX/5eq;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 970037
    :cond_2
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 970038
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 970039
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 970040
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 970041
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 970042
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 970043
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 970044
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 970045
    if-eqz v0, :cond_0

    .line 970046
    const-string v1, "current_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 970047
    invoke-static {p0, v0, p2}, LX/5eq;->a(LX/15i;ILX/0nX;)V

    .line 970048
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 970049
    if-eqz v0, :cond_1

    .line 970050
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 970051
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 970052
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 970053
    return-void
.end method
