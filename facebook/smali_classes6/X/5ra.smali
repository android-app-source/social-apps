.class public final LX/5ra;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/5rl;

.field private final b:I

.field private final c:F

.field private final d:F

.field private final e:Lcom/facebook/react/bridge/Callback;


# direct methods
.method private constructor <init>(LX/5rl;IFFLcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 1011896
    iput-object p1, p0, LX/5ra;->a:LX/5rl;

    .line 1011897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011898
    iput p2, p0, LX/5ra;->b:I

    .line 1011899
    iput p3, p0, LX/5ra;->c:F

    .line 1011900
    iput p4, p0, LX/5ra;->d:F

    .line 1011901
    iput-object p5, p0, LX/5ra;->e:Lcom/facebook/react/bridge/Callback;

    .line 1011902
    return-void
.end method

.method public synthetic constructor <init>(LX/5rl;IFFLcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 1011903
    invoke-direct/range {p0 .. p5}, LX/5ra;-><init>(LX/5rl;IFFLcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1011904
    :try_start_0
    iget-object v0, p0, LX/5ra;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->b:LX/5qw;

    iget v1, p0, LX/5ra;->b:I

    iget-object v2, p0, LX/5ra;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->a(I[I)V
    :try_end_0
    .catch LX/5qo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011905
    iget-object v0, p0, LX/5ra;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->a:[I

    aget v0, v0, v7

    int-to-float v0, v0

    .line 1011906
    iget-object v1, p0, LX/5ra;->a:LX/5rl;

    iget-object v1, v1, LX/5rl;->a:[I

    aget v1, v1, v8

    int-to-float v1, v1

    .line 1011907
    iget-object v2, p0, LX/5ra;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->b:LX/5qw;

    iget v3, p0, LX/5ra;->b:I

    iget v4, p0, LX/5ra;->c:F

    iget v5, p0, LX/5ra;->d:F

    invoke-virtual {v2, v3, v4, v5}, LX/5qw;->a(IFF)I

    move-result v2

    .line 1011908
    :try_start_1
    iget-object v3, p0, LX/5ra;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->b:LX/5qw;

    iget-object v4, p0, LX/5ra;->a:LX/5rl;

    iget-object v4, v4, LX/5rl;->a:[I

    invoke-virtual {v3, v2, v4}, LX/5qw;->a(I[I)V
    :try_end_1
    .catch LX/5qo; {:try_start_1 .. :try_end_1} :catch_1

    .line 1011909
    iget-object v3, p0, LX/5ra;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->a:[I

    aget v3, v3, v7

    int-to-float v3, v3

    sub-float v0, v3, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 1011910
    iget-object v3, p0, LX/5ra;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->a:[I

    aget v3, v3, v8

    int-to-float v3, v3

    sub-float v1, v3, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 1011911
    iget-object v3, p0, LX/5ra;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->a:[I

    aget v3, v3, v9

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 1011912
    iget-object v4, p0, LX/5ra;->a:LX/5rl;

    iget-object v4, v4, LX/5rl;->a:[I

    aget v4, v4, v10

    int-to-float v4, v4

    invoke-static {v4}, LX/5r2;->c(F)F

    move-result v4

    .line 1011913
    iget-object v5, p0, LX/5ra;->e:Lcom/facebook/react/bridge/Callback;

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v6, v10

    const/4 v0, 0x4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-interface {v5, v6}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1011914
    :goto_0
    return-void

    .line 1011915
    :catch_0
    iget-object v0, p0, LX/5ra;->e:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 1011916
    :catch_1
    iget-object v0, p0, LX/5ra;->e:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
