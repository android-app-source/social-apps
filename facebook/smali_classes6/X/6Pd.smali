.class public LX/6Pd;
.super LX/6PU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLPrivacyScope;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V
    .locals 0

    .prologue
    .line 1086272
    invoke-direct {p0, p1}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086273
    iput-object p2, p0, LX/6Pd;->a:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1086274
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 1

    .prologue
    .line 1086275
    iget-object v0, p0, LX/6Pd;->a:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1086276
    iget-object p0, p2, LX/40T;->a:LX/4VK;

    const-string p1, "privacy_scope"

    invoke-virtual {p0, p1, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086277
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086278
    const-string v0, "SetStoryPrivacyScopeMutatingVisitor"

    return-object v0
.end method
