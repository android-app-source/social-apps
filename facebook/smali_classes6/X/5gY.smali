.class public final LX/5gY;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 975136
    const-class v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    const v0, 0x2e3820bb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "AlbumDetailQuery"

    const-string v6, "9689e649af5efab9d8de81f681b75d90"

    const-string v7, "album"

    const-string v8, "10155156451481729"

    const/4 v9, 0x0

    .line 975137
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 975138
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 975139
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 975140
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 975141
    sparse-switch v0, :sswitch_data_0

    .line 975142
    :goto_0
    return-object p1

    .line 975143
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 975144
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 975145
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 975146
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 975147
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 975148
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 975149
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 975150
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 975151
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 975152
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 975153
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 975154
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 975155
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 975156
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 975157
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 975158
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 975159
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 975160
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_11
        -0x69b6761e -> :sswitch_d
        -0x680de62a -> :sswitch_9
        -0x6326fdb3 -> :sswitch_8
        -0x5305c081 -> :sswitch_2
        -0x4496acc9 -> :sswitch_a
        -0x39da4793 -> :sswitch_4
        -0x1b87b280 -> :sswitch_7
        -0x1b3da4a0 -> :sswitch_3
        -0x12efdeb3 -> :sswitch_b
        0x58705dc -> :sswitch_1
        0x5ced2b0 -> :sswitch_5
        0x683094a -> :sswitch_10
        0xa1fa812 -> :sswitch_0
        0x1918b88b -> :sswitch_6
        0x214100e0 -> :sswitch_c
        0x73a026b5 -> :sswitch_e
        0x7e07ec78 -> :sswitch_f
    .end sparse-switch
.end method
