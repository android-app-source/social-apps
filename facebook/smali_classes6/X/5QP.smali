.class public LX/5QP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/5QP;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 913173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913174
    iput-object p1, p0, LX/5QP;->a:LX/0Or;

    .line 913175
    return-void
.end method

.method public static a(LX/0QB;)LX/5QP;
    .locals 4

    .prologue
    .line 913181
    sget-object v0, LX/5QP;->b:LX/5QP;

    if-nez v0, :cond_1

    .line 913182
    const-class v1, LX/5QP;

    monitor-enter v1

    .line 913183
    :try_start_0
    sget-object v0, LX/5QP;->b:LX/5QP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 913184
    if-eqz v2, :cond_0

    .line 913185
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 913186
    new-instance v3, LX/5QP;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/5QP;-><init>(LX/0Or;)V

    .line 913187
    move-object v0, v3

    .line 913188
    sput-object v0, LX/5QP;->b:LX/5QP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 913189
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 913190
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 913191
    :cond_1
    sget-object v0, LX/5QP;->b:LX/5QP;

    return-object v0

    .line 913192
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 913193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 913176
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/5QP;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 913177
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_MEMBER_BIO_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 913178
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 913179
    const-string v1, "group_member_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 913180
    return-object v0
.end method
