.class public abstract LX/544;
.super LX/543;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/543",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final f:J


# instance fields
.field public volatile e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 827714
    :try_start_0
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    const-class v1, LX/544;

    const-string v2, "e"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, LX/544;->f:J
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 827715
    return-void

    .line 827716
    :catch_0
    move-exception v0

    .line 827717
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 827712
    invoke-direct {p0, p1}, LX/543;-><init>(I)V

    .line 827713
    return-void
.end method


# virtual methods
.method public final d(J)V
    .locals 7

    .prologue
    .line 827710
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    sget-wide v2, LX/544;->f:J

    move-object v1, p0

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->putOrderedLong(Ljava/lang/Object;JJ)V

    .line 827711
    return-void
.end method
