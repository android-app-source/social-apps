.class public LX/6DR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/6DR;


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/1Ck;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/6DS;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0Ot;LX/6DS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/6DS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065024
    iput-object p1, p0, LX/6DR;->a:LX/0tX;

    .line 1065025
    iput-object p2, p0, LX/6DR;->b:LX/1Ck;

    .line 1065026
    iput-object p3, p0, LX/6DR;->c:LX/0Ot;

    .line 1065027
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/6DR;->d:Ljava/util/HashSet;

    .line 1065028
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/6DR;->e:Ljava/util/HashSet;

    .line 1065029
    iput-object p4, p0, LX/6DR;->f:LX/6DS;

    .line 1065030
    return-void
.end method

.method public static a(LX/0QB;)LX/6DR;
    .locals 7

    .prologue
    .line 1065031
    sget-object v0, LX/6DR;->g:LX/6DR;

    if-nez v0, :cond_1

    .line 1065032
    const-class v1, LX/6DR;

    monitor-enter v1

    .line 1065033
    :try_start_0
    sget-object v0, LX/6DR;->g:LX/6DR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1065034
    if-eqz v2, :cond_0

    .line 1065035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1065036
    new-instance v6, LX/6DR;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/6DS;->b(LX/0QB;)LX/6DS;

    move-result-object v5

    check-cast v5, LX/6DS;

    invoke-direct {v6, v3, v4, p0, v5}, LX/6DR;-><init>(LX/0tX;LX/1Ck;LX/0Ot;LX/6DS;)V

    .line 1065037
    move-object v0, v6

    .line 1065038
    sput-object v0, LX/6DR;->g:LX/6DR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1065039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1065040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1065041
    :cond_1
    sget-object v0, LX/6DR;->g:LX/6DR;

    return-object v0

    .line 1065042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1065043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/6DR;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1065044
    iget-object v0, p0, LX/6DR;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, LX/6DR;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1065045
    return-void
.end method

.method public static b(LX/6DR;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1065046
    iget-object v0, p0, LX/6DR;->a:LX/0tX;

    .line 1065047
    new-instance v1, LX/6Df;

    invoke-direct {v1}, LX/6Df;-><init>()V

    move-object v1, v1

    .line 1065048
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/32 v2, 0x2a300

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1065049
    iget-object v0, p0, LX/6DR;->b:LX/1Ck;

    const-string v1, "fetch_auto_fill_data"

    invoke-static {p0}, LX/6DR;->b(LX/6DR;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/6DP;

    invoke-direct {v3, p0}, LX/6DP;-><init>(LX/6DR;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1065050
    return-void
.end method
