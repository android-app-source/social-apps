.class public final enum LX/6bf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6bf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6bf;

.field public static final enum LOCAL:LX/6bf;

.field public static final enum SERVER:LX/6bf;

.field public static final enum THREAD_CACHE:LX/6bf;

.field public static final enum THREAD_DB:LX/6bf;

.field public static final enum UNKNOWN:LX/6bf;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1113772
    new-instance v0, LX/6bf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/6bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bf;->UNKNOWN:LX/6bf;

    .line 1113773
    new-instance v0, LX/6bf;

    const-string v1, "THREAD_CACHE"

    invoke-direct {v0, v1, v3}, LX/6bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bf;->THREAD_CACHE:LX/6bf;

    .line 1113774
    new-instance v0, LX/6bf;

    const-string v1, "THREAD_DB"

    invoke-direct {v0, v1, v4}, LX/6bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bf;->THREAD_DB:LX/6bf;

    .line 1113775
    new-instance v0, LX/6bf;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v5}, LX/6bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bf;->LOCAL:LX/6bf;

    .line 1113776
    new-instance v0, LX/6bf;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v6}, LX/6bf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bf;->SERVER:LX/6bf;

    .line 1113777
    const/4 v0, 0x5

    new-array v0, v0, [LX/6bf;

    sget-object v1, LX/6bf;->UNKNOWN:LX/6bf;

    aput-object v1, v0, v2

    sget-object v1, LX/6bf;->THREAD_CACHE:LX/6bf;

    aput-object v1, v0, v3

    sget-object v1, LX/6bf;->THREAD_DB:LX/6bf;

    aput-object v1, v0, v4

    sget-object v1, LX/6bf;->LOCAL:LX/6bf;

    aput-object v1, v0, v5

    sget-object v1, LX/6bf;->SERVER:LX/6bf;

    aput-object v1, v0, v6

    sput-object v0, LX/6bf;->$VALUES:[LX/6bf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1113771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6bf;
    .locals 1

    .prologue
    .line 1113778
    const-class v0, LX/6bf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6bf;

    return-object v0
.end method

.method public static values()[LX/6bf;
    .locals 1

    .prologue
    .line 1113770
    sget-object v0, LX/6bf;->$VALUES:[LX/6bf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6bf;

    return-object v0
.end method
