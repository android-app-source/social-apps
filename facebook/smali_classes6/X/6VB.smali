.class public final enum LX/6VB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VB;

.field public static final enum META:LX/6VB;

.field public static final enum NONE:LX/6VB;

.field public static final enum SUBTITLE:LX/6VB;

.field public static final enum TITLE:LX/6VB;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103634
    new-instance v0, LX/6VB;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2}, LX/6VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VB;->TITLE:LX/6VB;

    .line 1103635
    new-instance v0, LX/6VB;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v3}, LX/6VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VB;->SUBTITLE:LX/6VB;

    .line 1103636
    new-instance v0, LX/6VB;

    const-string v1, "META"

    invoke-direct {v0, v1, v4}, LX/6VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VB;->META:LX/6VB;

    .line 1103637
    new-instance v0, LX/6VB;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/6VB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VB;->NONE:LX/6VB;

    .line 1103638
    const/4 v0, 0x4

    new-array v0, v0, [LX/6VB;

    sget-object v1, LX/6VB;->TITLE:LX/6VB;

    aput-object v1, v0, v2

    sget-object v1, LX/6VB;->SUBTITLE:LX/6VB;

    aput-object v1, v0, v3

    sget-object v1, LX/6VB;->META:LX/6VB;

    aput-object v1, v0, v4

    sget-object v1, LX/6VB;->NONE:LX/6VB;

    aput-object v1, v0, v5

    sput-object v0, LX/6VB;->$VALUES:[LX/6VB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103631
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VB;
    .locals 1

    .prologue
    .line 1103633
    const-class v0, LX/6VB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VB;

    return-object v0
.end method

.method public static values()[LX/6VB;
    .locals 1

    .prologue
    .line 1103632
    sget-object v0, LX/6VB;->$VALUES:[LX/6VB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VB;

    return-object v0
.end method
