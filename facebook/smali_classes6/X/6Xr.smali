.class public final enum LX/6Xr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Xr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Xr;

.field public static final enum PRIMARY:LX/6Xr;

.field public static final enum SECONDARY:LX/6Xr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1108928
    new-instance v0, LX/6Xr;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v2}, LX/6Xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xr;->PRIMARY:LX/6Xr;

    .line 1108929
    new-instance v0, LX/6Xr;

    const-string v1, "SECONDARY"

    invoke-direct {v0, v1, v3}, LX/6Xr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Xr;->SECONDARY:LX/6Xr;

    .line 1108930
    const/4 v0, 0x2

    new-array v0, v0, [LX/6Xr;

    sget-object v1, LX/6Xr;->PRIMARY:LX/6Xr;

    aput-object v1, v0, v2

    sget-object v1, LX/6Xr;->SECONDARY:LX/6Xr;

    aput-object v1, v0, v3

    sput-object v0, LX/6Xr;->$VALUES:[LX/6Xr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1108931
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Xr;
    .locals 1

    .prologue
    .line 1108932
    const-class v0, LX/6Xr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Xr;

    return-object v0
.end method

.method public static values()[LX/6Xr;
    .locals 1

    .prologue
    .line 1108933
    sget-object v0, LX/6Xr;->$VALUES:[LX/6Xr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Xr;

    return-object v0
.end method
