.class public LX/6C8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6C7;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063493
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1063494
    iput-object v0, p0, LX/6C8;->b:LX/0Ot;

    .line 1063495
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1063496
    iput-object v0, p0, LX/6C8;->c:LX/0Ot;

    .line 1063497
    new-instance v0, LX/6C7;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/6C7;-><init>(LX/6C8;Landroid/os/Looper;)V

    iput-object v0, p0, LX/6C8;->d:LX/6C7;

    .line 1063498
    return-void
.end method

.method public static b(LX/0QB;)LX/6C8;
    .locals 4

    .prologue
    .line 1063488
    new-instance v1, LX/6C8;

    invoke-direct {v1}, LX/6C8;-><init>()V

    .line 1063489
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v2, 0x12c4

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x329d

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1063490
    iput-object v0, v1, LX/6C8;->a:Landroid/content/Context;

    iput-object v2, v1, LX/6C8;->b:LX/0Ot;

    iput-object v3, v1, LX/6C8;->c:LX/0Ot;

    .line 1063491
    return-object v1
.end method
