.class public final LX/5GF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 887435
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 887436
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887437
    :goto_0
    return v1

    .line 887438
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 887439
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 887440
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 887441
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 887442
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 887443
    const-string v7, "contextual_name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 887444
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 887445
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 887446
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 887447
    :cond_3
    const-string v7, "location"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 887448
    invoke-static {p0, p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 887449
    :cond_4
    const-string v7, "map_bounding_box"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 887450
    invoke-static {p0, p1}, LX/5GE;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 887451
    :cond_5
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 887452
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 887453
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 887454
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 887455
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 887456
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 887457
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 887458
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 887459
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 887460
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887461
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887462
    if-eqz v0, :cond_0

    .line 887463
    const-string v1, "contextual_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887464
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887465
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887466
    if-eqz v0, :cond_1

    .line 887467
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887468
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887469
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887470
    if-eqz v0, :cond_2

    .line 887471
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887472
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 887473
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887474
    if-eqz v0, :cond_7

    .line 887475
    const-string v1, "map_bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887476
    const-wide/16 v6, 0x0

    .line 887477
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887478
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 887479
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 887480
    const-string v4, "east"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887481
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 887482
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 887483
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_4

    .line 887484
    const-string v4, "north"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887485
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 887486
    :cond_4
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 887487
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_5

    .line 887488
    const-string v4, "south"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887489
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 887490
    :cond_5
    const/4 v2, 0x3

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 887491
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_6

    .line 887492
    const-string v4, "west"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887493
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 887494
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887495
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887496
    if-eqz v0, :cond_8

    .line 887497
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887498
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887499
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887500
    return-void
.end method
