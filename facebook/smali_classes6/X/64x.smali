.class public final enum LX/64x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/64x;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/64x;

.field public static final enum HTTP_1_0:LX/64x;

.field public static final enum HTTP_1_1:LX/64x;

.field public static final enum HTTP_2:LX/64x;

.field public static final enum SPDY_3:LX/64x;


# instance fields
.field private final protocol:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1046178
    new-instance v0, LX/64x;

    const-string v1, "HTTP_1_0"

    const-string v2, "http/1.0"

    invoke-direct {v0, v1, v3, v2}, LX/64x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/64x;->HTTP_1_0:LX/64x;

    .line 1046179
    new-instance v0, LX/64x;

    const-string v1, "HTTP_1_1"

    const-string v2, "http/1.1"

    invoke-direct {v0, v1, v4, v2}, LX/64x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/64x;->HTTP_1_1:LX/64x;

    .line 1046180
    new-instance v0, LX/64x;

    const-string v1, "SPDY_3"

    const-string v2, "spdy/3.1"

    invoke-direct {v0, v1, v5, v2}, LX/64x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/64x;->SPDY_3:LX/64x;

    .line 1046181
    new-instance v0, LX/64x;

    const-string v1, "HTTP_2"

    const-string v2, "h2"

    invoke-direct {v0, v1, v6, v2}, LX/64x;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/64x;->HTTP_2:LX/64x;

    .line 1046182
    const/4 v0, 0x4

    new-array v0, v0, [LX/64x;

    sget-object v1, LX/64x;->HTTP_1_0:LX/64x;

    aput-object v1, v0, v3

    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;

    aput-object v1, v0, v4

    sget-object v1, LX/64x;->SPDY_3:LX/64x;

    aput-object v1, v0, v5

    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    aput-object v1, v0, v6

    sput-object v0, LX/64x;->$VALUES:[LX/64x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1046183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1046184
    iput-object p3, p0, LX/64x;->protocol:Ljava/lang/String;

    .line 1046185
    return-void
.end method

.method public static get(Ljava/lang/String;)LX/64x;
    .locals 3

    .prologue
    .line 1046186
    sget-object v0, LX/64x;->HTTP_1_0:LX/64x;

    iget-object v0, v0, LX/64x;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/64x;->HTTP_1_0:LX/64x;

    .line 1046187
    :goto_0
    return-object v0

    .line 1046188
    :cond_0
    sget-object v0, LX/64x;->HTTP_1_1:LX/64x;

    iget-object v0, v0, LX/64x;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/64x;->HTTP_1_1:LX/64x;

    goto :goto_0

    .line 1046189
    :cond_1
    sget-object v0, LX/64x;->HTTP_2:LX/64x;

    iget-object v0, v0, LX/64x;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/64x;->HTTP_2:LX/64x;

    goto :goto_0

    .line 1046190
    :cond_2
    sget-object v0, LX/64x;->SPDY_3:LX/64x;

    iget-object v0, v0, LX/64x;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/64x;->SPDY_3:LX/64x;

    goto :goto_0

    .line 1046191
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected protocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/64x;
    .locals 1

    .prologue
    .line 1046192
    const-class v0, LX/64x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/64x;

    return-object v0
.end method

.method public static values()[LX/64x;
    .locals 1

    .prologue
    .line 1046193
    sget-object v0, LX/64x;->$VALUES:[LX/64x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/64x;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1046194
    iget-object v0, p0, LX/64x;->protocol:Ljava/lang/String;

    return-object v0
.end method
