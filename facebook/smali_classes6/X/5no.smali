.class public final enum LX/5no;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5no;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5no;

.field public static final enum ACCEPTED:LX/5no;

.field public static final enum CLOSED:LX/5no;

.field public static final enum DECLINED:LX/5no;

.field public static final enum DISMISSED:LX/5no;

.field public static final enum EXPOSED:LX/5no;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1004690
    new-instance v0, LX/5no;

    const-string v1, "EXPOSED"

    const-string v2, "client_seen"

    invoke-direct {v0, v1, v3, v2}, LX/5no;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5no;->EXPOSED:LX/5no;

    .line 1004691
    new-instance v0, LX/5no;

    const-string v1, "ACCEPTED"

    const-string v2, "converted"

    invoke-direct {v0, v1, v4, v2}, LX/5no;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5no;->ACCEPTED:LX/5no;

    .line 1004692
    new-instance v0, LX/5no;

    const-string v1, "DECLINED"

    const-string v2, "declined"

    invoke-direct {v0, v1, v5, v2}, LX/5no;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5no;->DECLINED:LX/5no;

    .line 1004693
    new-instance v0, LX/5no;

    const-string v1, "CLOSED"

    const-string v2, "closed"

    invoke-direct {v0, v1, v6, v2}, LX/5no;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5no;->CLOSED:LX/5no;

    .line 1004694
    new-instance v0, LX/5no;

    const-string v1, "DISMISSED"

    const-string v2, "dismissed"

    invoke-direct {v0, v1, v7, v2}, LX/5no;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5no;->DISMISSED:LX/5no;

    .line 1004695
    const/4 v0, 0x5

    new-array v0, v0, [LX/5no;

    sget-object v1, LX/5no;->EXPOSED:LX/5no;

    aput-object v1, v0, v3

    sget-object v1, LX/5no;->ACCEPTED:LX/5no;

    aput-object v1, v0, v4

    sget-object v1, LX/5no;->DECLINED:LX/5no;

    aput-object v1, v0, v5

    sget-object v1, LX/5no;->CLOSED:LX/5no;

    aput-object v1, v0, v6

    sget-object v1, LX/5no;->DISMISSED:LX/5no;

    aput-object v1, v0, v7

    sput-object v0, LX/5no;->$VALUES:[LX/5no;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004696
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004697
    iput-object p3, p0, LX/5no;->eventName:Ljava/lang/String;

    .line 1004698
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5no;
    .locals 1

    .prologue
    .line 1004699
    const-class v0, LX/5no;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5no;

    return-object v0
.end method

.method public static values()[LX/5no;
    .locals 1

    .prologue
    .line 1004700
    sget-object v0, LX/5no;->$VALUES:[LX/5no;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5no;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004701
    iget-object v0, p0, LX/5no;->eventName:Ljava/lang/String;

    return-object v0
.end method
