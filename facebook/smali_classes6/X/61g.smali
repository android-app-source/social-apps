.class public LX/61g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/61g;


# instance fields
.field private final c:LX/616;

.field private final d:LX/03V;

.field private final e:LX/61l;

.field private final f:LX/5Ov;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1040455
    const-class v0, LX/61g;

    sput-object v0, LX/61g;->a:Ljava/lang/Class;

    .line 1040456
    const-string v0, "audio/3gpp"

    const-string v1, "audio/amr-wb"

    const-string v2, "audio/mp4a-latm"

    const-string v3, "audio/vorbis"

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/61g;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/616;LX/03V;LX/61l;LX/5Ov;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040458
    iput-object p1, p0, LX/61g;->c:LX/616;

    .line 1040459
    iput-object p2, p0, LX/61g;->d:LX/03V;

    .line 1040460
    iput-object p3, p0, LX/61g;->e:LX/61l;

    .line 1040461
    iput-object p4, p0, LX/61g;->f:LX/5Ov;

    .line 1040462
    return-void
.end method

.method public static a(LX/0QB;)LX/61g;
    .locals 7

    .prologue
    .line 1040463
    sget-object v0, LX/61g;->g:LX/61g;

    if-nez v0, :cond_1

    .line 1040464
    const-class v1, LX/61g;

    monitor-enter v1

    .line 1040465
    :try_start_0
    sget-object v0, LX/61g;->g:LX/61g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1040466
    if-eqz v2, :cond_0

    .line 1040467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1040468
    new-instance p0, LX/61g;

    invoke-static {v0}, LX/616;->a(LX/0QB;)LX/616;

    move-result-object v3

    check-cast v3, LX/616;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/61l;->b(LX/0QB;)LX/61l;

    move-result-object v5

    check-cast v5, LX/61l;

    invoke-static {v0}, LX/5Ov;->b(LX/0QB;)LX/5Ov;

    move-result-object v6

    check-cast v6, LX/5Ov;

    invoke-direct {p0, v3, v4, v5, v6}, LX/61g;-><init>(LX/616;LX/03V;LX/61l;LX/5Ov;)V

    .line 1040469
    move-object v0, p0

    .line 1040470
    sput-object v0, LX/61g;->g:LX/61g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1040471
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1040472
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1040473
    :cond_1
    sget-object v0, LX/61g;->g:LX/61g;

    return-object v0

    .line 1040474
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1040475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/61f;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1040476
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1040477
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61f;

    .line 1040478
    iget-object v0, v0, LX/61f;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1040479
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " tracks: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-static {v2}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/media/MediaExtractor;)LX/61f;
    .locals 6

    .prologue
    .line 1040480
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1040481
    invoke-virtual {p1}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v2

    .line 1040482
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1040483
    invoke-virtual {p1, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v3

    .line 1040484
    const-string v4, "mime"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1040485
    const-string v5, "video/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1040486
    new-instance v5, LX/61f;

    invoke-direct {v5, v4, v3, v0}, LX/61f;-><init>(Ljava/lang/String;Landroid/media/MediaFormat;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040487
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1040488
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1040489
    new-instance v0, LX/61d;

    invoke-direct {v0}, LX/61d;-><init>()V

    throw v0

    .line 1040490
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61f;

    .line 1040491
    iget-object v3, v0, LX/61f;->a:Ljava/lang/String;

    invoke-static {v3}, LX/616;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1040492
    :goto_1
    move-object v0, v0

    .line 1040493
    if-nez v0, :cond_4

    .line 1040494
    new-instance v0, LX/60t;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported video codec. Contained "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/61g;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/60t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040495
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 1040496
    iget-object v2, p0, LX/61g;->d:LX/03V;

    const-string v3, "VideoTrackExtractor_multiple_video_tracks"

    invoke-static {v1}, LX/61g;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040497
    :cond_5
    return-object v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/media/MediaExtractor;Landroid/net/Uri;)LX/61f;
    .locals 6

    .prologue
    .line 1040498
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1040499
    invoke-virtual {p1}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v3

    .line 1040500
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 1040501
    invoke-virtual {p1, v1}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v4

    .line 1040502
    const-string v0, "mime"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1040503
    const-string v5, "audio/unknown"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1040504
    :try_start_0
    iget-object v0, p0, LX/61g;->f:LX/5Ov;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/5Ov;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a()Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;

    move-result-object v0

    .line 1040505
    iget-object v5, p0, LX/61g;->e:LX/61l;

    invoke-virtual {v5, v0}, LX/61l;->b(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;

    move-result-object v0

    .line 1040506
    iget-object v0, v0, LX/61k;->a:Ljava/lang/String;

    .line 1040507
    const-string v5, "mime"

    invoke-virtual {v4, v5, v0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch LX/61j; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040508
    :cond_0
    const-string v5, "audio/"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1040509
    new-instance v5, LX/61f;

    invoke-direct {v5, v0, v4, v1}, LX/61f;-><init>(Ljava/lang/String;Landroid/media/MediaFormat;I)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040510
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1040511
    :catch_0
    move-exception v0

    .line 1040512
    new-instance v1, LX/60t;

    invoke-virtual {v0}, LX/61j;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/60t;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1040513
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1040514
    const/4 v0, 0x0

    .line 1040515
    :cond_3
    :goto_1
    return-object v0

    .line 1040516
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61f;

    .line 1040517
    sget-object v3, LX/61g;->b:LX/0Rf;

    iget-object v4, v0, LX/61f;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1040518
    :goto_2
    move-object v0, v0

    .line 1040519
    if-nez v0, :cond_6

    .line 1040520
    new-instance v0, LX/60t;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported audio codec. Contained "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, LX/61g;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/60t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040521
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_3

    .line 1040522
    iget-object v1, p0, LX/61g;->d:LX/03V;

    const-string v3, "VideoTrackExtractor_multiple_audio_tracks"

    invoke-static {v2}, LX/61g;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method
