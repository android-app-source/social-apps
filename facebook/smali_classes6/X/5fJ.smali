.class public final LX/5fJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field public final synthetic a:LX/6IY;

.field public final synthetic b:LX/5fQ;


# direct methods
.method public constructor <init>(LX/5fQ;LX/6IY;)V
    .locals 0

    .prologue
    .line 971167
    iput-object p1, p0, LX/5fJ;->b:LX/5fQ;

    iput-object p2, p0, LX/5fJ;->a:LX/6IY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 2

    .prologue
    .line 971168
    iget-object v0, p0, LX/5fJ;->a:LX/6IY;

    .line 971169
    iget-object v1, v0, LX/6IY;->a:LX/6Ib;

    iget-object v1, v1, LX/6Ib;->l:LX/6Jj;

    if-eqz v1, :cond_0

    .line 971170
    iget-object v1, v0, LX/6IY;->b:LX/6JQ;

    .line 971171
    iput-object p1, v1, LX/6JQ;->a:[B

    .line 971172
    iget-object v1, v0, LX/6IY;->a:LX/6Ib;

    iget-object v1, v1, LX/6Ib;->l:LX/6Jj;

    iget-object p0, v0, LX/6IY;->b:LX/6JQ;

    invoke-virtual {v1, p0}, LX/6Jj;->a(LX/6JQ;)V

    .line 971173
    :cond_0
    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 971174
    return-void
.end method
