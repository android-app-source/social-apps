.class public final LX/6HQ;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public a:[B

.field public final synthetic b:LX/6HU;


# direct methods
.method public constructor <init>(LX/6HU;[B)V
    .locals 0

    .prologue
    .line 1071820
    iput-object p1, p0, LX/6HQ;->b:LX/6HU;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1071821
    iput-object p2, p0, LX/6HQ;->a:[B

    .line 1071822
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1071823
    iget-object v0, p0, LX/6HQ;->b:LX/6HU;

    iget-object v0, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1071824
    iget-object v0, p0, LX/6HQ;->b:LX/6HU;

    iget-object v0, v0, LX/6HU;->j:LX/2Ib;

    iget-object v1, p0, LX/6HQ;->a:[B

    .line 1071825
    invoke-virtual {v0}, LX/2Ib;->c()Landroid/net/Uri;

    move-result-object v2

    .line 1071826
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1071827
    :try_start_0
    invoke-static {v1, v3}, LX/1t3;->a([BLjava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071828
    :goto_0
    move-object v0, v2

    .line 1071829
    iget-object v1, p0, LX/6HQ;->b:LX/6HU;

    iget-object v1, v1, LX/6HU;->a:LX/6HO;

    invoke-interface {v1, v0}, LX/6HO;->b(Landroid/net/Uri;)V

    .line 1071830
    return-object v0

    .line 1071831
    :catch_0
    move-exception v3

    .line 1071832
    sget-object v4, LX/2Ib;->a:Ljava/lang/Class;

    const-string p1, "Unable to write to file "

    invoke-static {v4, p1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1071833
    check-cast p1, Landroid/net/Uri;

    .line 1071834
    iget-object v0, p0, LX/6HQ;->b:LX/6HU;

    iget-object v0, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1071835
    iget-object v0, p0, LX/6HQ;->b:LX/6HU;

    iget-object v0, v0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0, p1}, LX/6HO;->c(Landroid/net/Uri;)V

    .line 1071836
    return-void
.end method
