.class public final enum LX/6OQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6OQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6OQ;

.field public static final enum ADD:LX/6OQ;

.field public static final enum MODIFY:LX/6OQ;

.field public static final enum NONE:LX/6OQ;

.field public static final enum REMOVE:LX/6OQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085124
    new-instance v0, LX/6OQ;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, LX/6OQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OQ;->ADD:LX/6OQ;

    .line 1085125
    new-instance v0, LX/6OQ;

    const-string v1, "MODIFY"

    invoke-direct {v0, v1, v3}, LX/6OQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OQ;->MODIFY:LX/6OQ;

    .line 1085126
    new-instance v0, LX/6OQ;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v4}, LX/6OQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OQ;->REMOVE:LX/6OQ;

    .line 1085127
    new-instance v0, LX/6OQ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/6OQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OQ;->NONE:LX/6OQ;

    .line 1085128
    const/4 v0, 0x4

    new-array v0, v0, [LX/6OQ;

    sget-object v1, LX/6OQ;->ADD:LX/6OQ;

    aput-object v1, v0, v2

    sget-object v1, LX/6OQ;->MODIFY:LX/6OQ;

    aput-object v1, v0, v3

    sget-object v1, LX/6OQ;->REMOVE:LX/6OQ;

    aput-object v1, v0, v4

    sget-object v1, LX/6OQ;->NONE:LX/6OQ;

    aput-object v1, v0, v5

    sput-object v0, LX/6OQ;->$VALUES:[LX/6OQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1085129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6OQ;
    .locals 1

    .prologue
    .line 1085130
    const-class v0, LX/6OQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OQ;

    return-object v0
.end method

.method public static values()[LX/6OQ;
    .locals 1

    .prologue
    .line 1085131
    sget-object v0, LX/6OQ;->$VALUES:[LX/6OQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6OQ;

    return-object v0
.end method
