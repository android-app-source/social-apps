.class public final LX/6I6;
.super Landroid/view/OrientationEventListener;
.source ""


# instance fields
.field public final synthetic a:LX/6IA;

.field private b:LX/6IJ;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(LX/6IA;Landroid/content/Context;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1073060
    iput-object p1, p0, LX/6I6;->a:LX/6IA;

    .line 1073061
    invoke-direct {p0, p2, p3}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 1073062
    iput-boolean v0, p0, LX/6I6;->c:Z

    .line 1073063
    iput-boolean v0, p0, LX/6I6;->d:Z

    .line 1073064
    new-instance v0, LX/6IJ;

    invoke-direct {v0, p2}, LX/6IJ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6I6;->b:LX/6IJ;

    .line 1073065
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1073066
    iget-boolean v0, p0, LX/6I6;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6I6;->a:LX/6IA;

    iget-object v0, v0, LX/6IA;->D:LX/6I8;

    .line 1073067
    iget-object v1, v0, LX/6I7;->c:LX/6II;

    move-object v0, v1

    .line 1073068
    sget-object v1, LX/6II;->READY:LX/6II;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1073069
    :goto_0
    if-eqz v0, :cond_1

    .line 1073070
    invoke-virtual {p0}, LX/6I6;->enable()V

    .line 1073071
    :goto_1
    return-void

    .line 1073072
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1073073
    :cond_1
    invoke-virtual {p0}, LX/6I6;->disable()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1073074
    iget-boolean v0, p0, LX/6I6;->d:Z

    if-eq v0, p1, :cond_0

    .line 1073075
    iput-boolean p1, p0, LX/6I6;->d:Z

    .line 1073076
    invoke-virtual {p0}, LX/6I6;->a()V

    .line 1073077
    :cond_0
    return-void
.end method

.method public final disable()V
    .locals 2

    .prologue
    .line 1073078
    iget-boolean v0, p0, LX/6I6;->c:Z

    if-eqz v0, :cond_1

    .line 1073079
    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1073080
    iget-object v0, p0, LX/6I6;->b:LX/6IJ;

    .line 1073081
    iget-boolean v1, v0, LX/6IJ;->d:Z

    if-eqz v1, :cond_0

    .line 1073082
    iget-object v1, v0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1073083
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6I6;->c:Z

    .line 1073084
    :cond_1
    return-void
.end method

.method public final enable()V
    .locals 4

    .prologue
    .line 1073085
    iget-boolean v0, p0, LX/6I6;->c:Z

    if-nez v0, :cond_1

    .line 1073086
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    .line 1073087
    iget-object v0, p0, LX/6I6;->b:LX/6IJ;

    const/4 v3, 0x2

    .line 1073088
    iget-boolean v1, v0, LX/6IJ;->d:Z

    if-eqz v1, :cond_0

    .line 1073089
    iget-object v1, v0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    iget-object v2, v0, LX/6IJ;->b:Landroid/hardware/Sensor;

    invoke-virtual {v1, v0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1073090
    iget-object v1, v0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    iget-object v2, v0, LX/6IJ;->c:Landroid/hardware/Sensor;

    invoke-virtual {v1, v0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1073091
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6I6;->c:Z

    .line 1073092
    :cond_1
    return-void
.end method

.method public final onOrientationChanged(I)V
    .locals 9

    .prologue
    .line 1073093
    iget-object v0, p0, LX/6I6;->a:LX/6IA;

    iget v0, v0, LX/6IA;->ap:I

    add-int/2addr v0, p1

    add-int/lit16 v0, v0, 0x168

    add-int/lit8 v0, v0, 0x2d

    rem-int/lit16 v0, v0, 0x168

    add-int/lit8 v1, v0, -0x2d

    .line 1073094
    const/16 v0, 0x2d

    if-gt v1, v0, :cond_1

    .line 1073095
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    .line 1073096
    :goto_0
    iget-object v2, p0, LX/6I6;->a:LX/6IA;

    iget-object v2, v2, LX/6IA;->an:LX/6IG;

    if-eq v0, v2, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0x28

    if-gt v1, v2, :cond_0

    iget-object v1, p0, LX/6I6;->b:LX/6IJ;

    const/high16 v2, 0x42340000    # 45.0f

    .line 1073097
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, v1, LX/6IJ;->l:J

    const-wide/16 v7, 0x1f4

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_4

    iget v3, v1, LX/6IJ;->j:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v2

    if-gez v3, :cond_4

    iget v3, v1, LX/6IJ;->k:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v2

    if-gez v3, :cond_4

    .line 1073098
    const/4 v3, 0x1

    .line 1073099
    :goto_1
    move v1, v3

    .line 1073100
    if-nez v1, :cond_0

    .line 1073101
    iget-object v1, p0, LX/6I6;->a:LX/6IA;

    .line 1073102
    iput-object v0, v1, LX/6IA;->an:LX/6IG;

    .line 1073103
    iget-object v0, p0, LX/6I6;->a:LX/6IA;

    iget-object v1, p0, LX/6I6;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->an:LX/6IG;

    .line 1073104
    invoke-static {v0, v1}, LX/6IA;->a(LX/6IA;LX/6IG;)F

    move-result v2

    .line 1073105
    const/16 v3, 0x190

    invoke-static {v0, v2, v3}, LX/6IA;->a(LX/6IA;FI)V

    .line 1073106
    iget-object v3, v0, LX/6IA;->ao:LX/6IG;

    iget-object v4, v0, LX/6IA;->n:LX/6IL;

    iget-object v5, v0, LX/6IA;->o:LX/6IL;

    invoke-static {v3, v1, v4, v5}, LX/6IF;->a(LX/6IG;LX/6IG;LX/6IL;LX/6IL;)V

    .line 1073107
    iget-object v3, v0, LX/6IA;->n:LX/6IL;

    iget-object v4, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v4}, LX/6HU;->n()Z

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/6IL;->a(FZ)V

    .line 1073108
    iget-object v3, v0, LX/6IA;->o:LX/6IL;

    iget-object v4, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v4}, LX/6HU;->b()Z

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/6IL;->a(FZ)V

    .line 1073109
    iget-object v3, v0, LX/6IA;->v:Lcom/facebook/camera/views/RotateLayout;

    float-to-int v2, v2

    invoke-virtual {v3, v2}, Lcom/facebook/camera/views/RotateLayout;->setOrientation(I)V

    .line 1073110
    iget-object v0, p0, LX/6I6;->a:LX/6IA;

    iget-object v0, v0, LX/6IA;->ab:LX/6HF;

    iget-object v1, p0, LX/6I6;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->an:LX/6IG;

    invoke-interface {v0, v1}, LX/6HF;->b(LX/6IG;)V

    .line 1073111
    :cond_0
    return-void

    .line 1073112
    :cond_1
    const/16 v0, 0x87

    if-gt v1, v0, :cond_2

    .line 1073113
    sget-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    .line 1073114
    add-int/lit8 v1, v1, -0x5a

    goto/16 :goto_0

    .line 1073115
    :cond_2
    const/16 v0, 0xe1

    if-gt v1, v0, :cond_3

    .line 1073116
    sget-object v0, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    .line 1073117
    add-int/lit16 v1, v1, -0xb4

    goto/16 :goto_0

    .line 1073118
    :cond_3
    sget-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    .line 1073119
    add-int/lit16 v1, v1, -0x10e

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method
