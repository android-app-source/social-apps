.class public LX/6ba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6bX;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6bX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6bX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1113721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113722
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/6ba;->a:LX/0Px;

    .line 1113723
    return-void
.end method

.method public varargs constructor <init>([LX/6bX;)V
    .locals 1

    .prologue
    .line 1113724
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6ba;-><init>(LX/0Px;)V

    .line 1113725
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1113726
    iget-object v0, p0, LX/6ba;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/6ba;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bX;

    .line 1113727
    invoke-interface {v0, p1}, LX/6bX;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1113728
    const/4 v0, 0x1

    .line 1113729
    :goto_1
    return v0

    .line 1113730
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1113731
    goto :goto_1
.end method
