.class public LX/68X;
.super LX/67m;
.source ""


# instance fields
.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private final w:F

.field private x:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 12

    .prologue
    const/high16 v11, -0x3d4c0000    # -90.0f

    const/4 v10, 0x3

    const/4 v9, 0x1

    const v6, 0x3f8a3d71    # 1.08f

    const/high16 v4, 0x40000000    # 2.0f

    .line 1055515
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1055516
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->o:F

    .line 1055517
    const v0, 0x3ecccccd    # 0.4f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->p:F

    .line 1055518
    const/high16 v0, 0x41800000    # 16.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->q:F

    .line 1055519
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->r:F

    .line 1055520
    const v0, 0x4099999a    # 4.8f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->s:F

    .line 1055521
    const v0, 0x3fcccccd    # 1.6f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->t:F

    .line 1055522
    const/high16 v0, 0x42300000    # 44.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68X;->w:F

    .line 1055523
    iput v10, p0, LX/68X;->j:I

    .line 1055524
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/68X;->k:F

    .line 1055525
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68X;->l:Z

    .line 1055526
    iget v0, p0, LX/68X;->q:F

    mul-float/2addr v0, v6

    mul-float/2addr v0, v4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1055527
    iget v1, p0, LX/68X;->q:F

    mul-float/2addr v1, v6

    mul-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 1055528
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, LX/68X;->x:Landroid/graphics/Bitmap;

    .line 1055529
    new-instance v7, Landroid/graphics/Canvas;

    iget-object v1, p0, LX/68X;->x:Landroid/graphics/Bitmap;

    invoke-direct {v7, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1055530
    int-to-float v0, v0

    div-float v1, v0, v4

    .line 1055531
    int-to-float v0, v2

    div-float v2, v0, v4

    .line 1055532
    new-instance v8, Landroid/graphics/RectF;

    iget v0, p0, LX/68X;->t:F

    sub-float v0, v1, v0

    iget v3, p0, LX/68X;->t:F

    sub-float v3, v2, v3

    iget v4, p0, LX/68X;->t:F

    add-float/2addr v4, v1

    iget v5, p0, LX/68X;->t:F

    add-float/2addr v5, v2

    invoke-direct {v8, v0, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1055533
    iget v0, p0, LX/68X;->q:F

    mul-float v3, v6, v0

    .line 1055534
    new-array v4, v10, [I

    fill-array-data v4, :array_0

    .line 1055535
    new-array v5, v10, [F

    fill-array-data v5, :array_1

    .line 1055536
    new-instance v0, Landroid/graphics/RadialGradient;

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1055537
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 1055538
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5, v9}, Landroid/graphics/Paint;-><init>(I)V

    .line 1055539
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055540
    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1055541
    invoke-virtual {v7, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1055542
    invoke-virtual {v5}, Landroid/graphics/Paint;->reset()V

    .line 1055543
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setFlags(I)V

    .line 1055544
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055545
    const v0, -0x7a000001

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055546
    iget v0, p0, LX/68X;->q:F

    invoke-virtual {v7, v1, v2, v0, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1055547
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055548
    const v0, -0x5d5d5e

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055549
    iget v0, p0, LX/68X;->p:F

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1055550
    iget v0, p0, LX/68X;->q:F

    invoke-virtual {v7, v1, v2, v0, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1055551
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setFlags(I)V

    .line 1055552
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055553
    const v0, -0x14d6dc

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055554
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 1055555
    iget v0, p0, LX/68X;->s:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1055556
    iget v0, p0, LX/68X;->t:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055557
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v4, v8, v0, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1055558
    iget v0, p0, LX/68X;->r:F

    sub-float v0, v2, v0

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055559
    iget v0, p0, LX/68X;->s:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055560
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1055561
    invoke-virtual {v7, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1055562
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055563
    const v0, -0x2ae4ea

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055564
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 1055565
    iget v0, p0, LX/68X;->s:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1055566
    iget v0, p0, LX/68X;->t:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055567
    const/4 v0, 0x0

    invoke-virtual {v4, v8, v0, v11}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1055568
    iget v0, p0, LX/68X;->r:F

    sub-float v0, v2, v0

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055569
    iget v0, p0, LX/68X;->s:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055570
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1055571
    invoke-virtual {v7, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1055572
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055573
    const v0, -0x3d3d3e

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055574
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 1055575
    iget v0, p0, LX/68X;->s:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1055576
    iget v0, p0, LX/68X;->t:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055577
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v4, v8, v0, v11}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1055578
    iget v0, p0, LX/68X;->r:F

    add-float/2addr v0, v2

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055579
    iget v0, p0, LX/68X;->s:F

    sub-float v0, v1, v0

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055580
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1055581
    invoke-virtual {v7, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1055582
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1055583
    const v0, -0x252526

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055584
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 1055585
    iget v0, p0, LX/68X;->s:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1055586
    iget v0, p0, LX/68X;->t:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055587
    const/4 v0, 0x0

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v4, v8, v0, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1055588
    iget v0, p0, LX/68X;->r:F

    add-float/2addr v0, v2

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055589
    iget v0, p0, LX/68X;->s:F

    add-float/2addr v0, v1

    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1055590
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1055591
    invoke-virtual {v7, v4, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1055592
    return-void

    .line 1055593
    :array_0
    .array-data 4
        0x22000000
        0x22000000
        0x0
    .end array-data

    .line 1055594
    :array_1
    .array-data 4
        0x3f6d097b
        0x3f6d097b
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(FF)I
    .locals 2

    .prologue
    .line 1055595
    iget v0, p0, LX/68X;->u:F

    iget v1, p0, LX/68X;->q:F

    sub-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, LX/68X;->u:F

    iget v1, p0, LX/68X;->q:F

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    iget v0, p0, LX/68X;->v:F

    iget v1, p0, LX/68X;->q:F

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, LX/68X;->v:F

    iget v1, p0, LX/68X;->q:F

    add-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    .line 1055596
    const/4 v0, 0x2

    .line 1055597
    :goto_0
    return v0

    .line 1055598
    :cond_0
    iget v0, p0, LX/68X;->u:F

    iget v1, p0, LX/68X;->w:F

    sub-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    iget v0, p0, LX/68X;->u:F

    iget v1, p0, LX/68X;->w:F

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    iget v0, p0, LX/68X;->v:F

    iget v1, p0, LX/68X;->w:F

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_1

    iget v0, p0, LX/68X;->v:F

    iget v1, p0, LX/68X;->w:F

    add-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_1

    .line 1055599
    const/4 v0, 0x1

    goto :goto_0

    .line 1055600
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1055601
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 1055602
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1055603
    iget-object v1, v0, LX/680;->k:LX/31h;

    move-object v0, v1

    .line 1055604
    invoke-virtual {v0}, LX/31h;->b()F

    move-result v0

    iget v1, p0, LX/68X;->u:F

    iget v2, p0, LX/68X;->v:F

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1055605
    iget-object v0, p0, LX/68X;->x:Landroid/graphics/Bitmap;

    iget v1, p0, LX/68X;->u:F

    iget v2, p0, LX/68X;->q:F

    sub-float/2addr v1, v2

    iget v2, p0, LX/68X;->v:F

    iget v3, p0, LX/68X;->q:F

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1055606
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1055607
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1055608
    iget v0, p0, LX/68X;->o:F

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->c:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 1055609
    iget v1, p0, LX/68X;->o:F

    iget-object v2, p0, LX/67m;->e:LX/680;

    iget v2, v2, LX/680;->d:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 1055610
    iget v2, p0, LX/68X;->q:F

    add-float/2addr v0, v2

    iput v0, p0, LX/68X;->u:F

    .line 1055611
    iget v0, p0, LX/68X;->q:F

    add-float/2addr v0, v1

    iput v0, p0, LX/68X;->v:F

    .line 1055612
    return-void
.end method

.method public final b(FF)Z
    .locals 2

    .prologue
    .line 1055613
    iget-object v0, p0, LX/67m;->e:LX/680;

    const/4 v1, 0x0

    .line 1055614
    new-instance p0, LX/67d;

    invoke-direct {p0}, LX/67d;-><init>()V

    .line 1055615
    iput v1, p0, LX/67d;->h:F

    .line 1055616
    move-object v1, p0

    .line 1055617
    invoke-virtual {v0, v1}, LX/680;->a(LX/67d;)V

    .line 1055618
    const/4 v0, 0x1

    return v0
.end method
