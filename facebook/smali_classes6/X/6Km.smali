.class public LX/6Km;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:I

.field public B:LX/6Ki;

.field public final b:LX/5PK;

.field public final c:LX/6KZ;

.field public final d:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "LX/6Kd;",
            "LX/5PS;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "LX/6Ke;",
            "LX/5PS;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/6Jh;

.field public final g:LX/6Jq;

.field private h:LX/6Kc;

.field public final i:LX/6Kl;

.field public final j:LX/6Kk;

.field private final k:Landroid/os/HandlerThread;

.field public final l:LX/6Kj;

.field public final m:LX/6Kp;

.field public final n:LX/6KN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:LX/7Se;

.field public q:Landroid/graphics/SurfaceTexture;

.field public r:LX/6Kf;

.field public s:LX/5Pf;

.field public final t:[F

.field private final u:[F

.field private final v:[F

.field public final w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/6Kd;",
            "[F>;"
        }
    .end annotation
.end field

.field public final x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/6Kd;",
            "[F>;"
        }
    .end annotation
.end field

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1077137
    const-class v0, LX/6Km;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Km;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Jq;Landroid/os/HandlerThread;LX/6KZ;LX/5PK;LX/6Kc;LX/7Se;LX/6KN;ILjava/lang/String;)V
    .locals 3
    .param p7    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x0

    .line 1077138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077139
    new-array v0, v1, [F

    iput-object v0, p0, LX/6Km;->t:[F

    .line 1077140
    new-array v0, v1, [F

    iput-object v0, p0, LX/6Km;->u:[F

    .line 1077141
    new-array v0, v1, [F

    iput-object v0, p0, LX/6Km;->v:[F

    .line 1077142
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/6Km;->d:LX/026;

    .line 1077143
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/6Km;->e:LX/026;

    .line 1077144
    iput-object p3, p0, LX/6Km;->c:LX/6KZ;

    .line 1077145
    new-instance v0, LX/6Kl;

    invoke-direct {v0, p0}, LX/6Kl;-><init>(LX/6Km;)V

    iput-object v0, p0, LX/6Km;->i:LX/6Kl;

    .line 1077146
    iput-object p1, p0, LX/6Km;->g:LX/6Jq;

    .line 1077147
    new-instance v0, LX/6Kk;

    invoke-direct {v0, p0}, LX/6Kk;-><init>(LX/6Km;)V

    iput-object v0, p0, LX/6Km;->j:LX/6Kk;

    .line 1077148
    iput-object p2, p0, LX/6Km;->k:Landroid/os/HandlerThread;

    .line 1077149
    iget-object v0, p0, LX/6Km;->k:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1077150
    new-instance v0, LX/6Kj;

    iget-object v1, p0, LX/6Km;->k:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/6Kj;-><init>(LX/6Km;Landroid/os/Looper;)V

    iput-object v0, p0, LX/6Km;->l:LX/6Kj;

    .line 1077151
    iput-object p4, p0, LX/6Km;->b:LX/5PK;

    .line 1077152
    iput-object p5, p0, LX/6Km;->h:LX/6Kc;

    .line 1077153
    iput-object p6, p0, LX/6Km;->p:LX/7Se;

    .line 1077154
    iget-object v0, p0, LX/6Km;->p:LX/7Se;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/7Se;->a(ZLX/5Pc;)V

    .line 1077155
    new-instance v0, LX/6Kp;

    invoke-direct {v0}, LX/6Kp;-><init>()V

    iput-object v0, p0, LX/6Km;->m:LX/6Kp;

    .line 1077156
    sget-object v0, LX/6Ki;->RESUMING:LX/6Ki;

    iput-object v0, p0, LX/6Km;->B:LX/6Ki;

    .line 1077157
    iput p8, p0, LX/6Km;->y:I

    .line 1077158
    iget-object v0, p0, LX/6Km;->t:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077159
    iget-object v0, p0, LX/6Km;->u:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077160
    iget-object v0, p0, LX/6Km;->v:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077161
    iput-object p7, p0, LX/6Km;->n:LX/6KN;

    .line 1077162
    iput-object p9, p0, LX/6Km;->o:Ljava/lang/String;

    .line 1077163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6Km;->w:Ljava/util/Map;

    .line 1077164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6Km;->x:Ljava/util/Map;

    .line 1077165
    iput v2, p0, LX/6Km;->z:I

    .line 1077166
    iput v2, p0, LX/6Km;->A:I

    .line 1077167
    new-instance v0, LX/6Kg;

    invoke-direct {v0}, LX/6Kg;-><init>()V

    iput-object v0, p0, LX/6Km;->r:LX/6Kf;

    .line 1077168
    return-void
.end method

.method public static a(LX/6Km;LX/6Ke;)V
    .locals 2

    .prologue
    .line 1077169
    invoke-interface {p1}, LX/6Kd;->dE_()V

    .line 1077170
    iget-object v0, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5PS;

    .line 1077171
    if-eqz v0, :cond_0

    .line 1077172
    invoke-interface {v0}, LX/5PS;->c()V

    .line 1077173
    iget-object v0, p0, LX/6Km;->e:LX/026;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077174
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/6Km;ILjava/lang/Object;)V
    .locals 2
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1077175
    if-nez p2, :cond_0

    .line 1077176
    iget-object v0, p0, LX/6Km;->l:LX/6Kj;

    iget-object v1, p0, LX/6Km;->l:LX/6Kj;

    invoke-virtual {v1, p1}, LX/6Kj;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Kj;->sendMessage(Landroid/os/Message;)Z

    .line 1077177
    :goto_0
    return-void

    .line 1077178
    :cond_0
    iget-object v0, p0, LX/6Km;->l:LX/6Kj;

    iget-object v1, p0, LX/6Km;->l:LX/6Kj;

    invoke-virtual {v1, p1, p2}, LX/6Kj;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Kj;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public static a$redex0(LX/6Km;Landroid/os/Message;)V
    .locals 10

    .prologue
    .line 1077179
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1077180
    packed-switch v0, :pswitch_data_0

    .line 1077181
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown msg "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1077182
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/5Pf;

    .line 1077183
    iget-object v1, p0, LX/6Km;->s:LX/5Pf;

    if-eq v0, v1, :cond_0

    .line 1077184
    invoke-static {p0, v0}, LX/6Km;->c(LX/6Km;LX/5Pf;)V

    .line 1077185
    :cond_0
    iget-object v1, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v1}, LX/6Kf;->a()V

    .line 1077186
    :goto_0
    return-void

    .line 1077187
    :pswitch_2
    invoke-direct {p0}, LX/6Km;->i()V

    goto :goto_0

    .line 1077188
    :pswitch_3
    const/4 v7, 0x0

    .line 1077189
    iget v4, p0, LX/6Km;->z:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/6Km;->z:I

    .line 1077190
    iget-object v4, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-nez v4, :cond_15

    .line 1077191
    :cond_1
    goto :goto_0

    .line 1077192
    :pswitch_4
    const/4 v2, 0x0

    .line 1077193
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v0

    .line 1077194
    sget-object v1, LX/6Ki;->PAUSED:LX/6Ki;

    iput-object v1, p0, LX/6Km;->B:LX/6Ki;

    .line 1077195
    iget-object v1, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v1}, LX/6Kf;->c()V

    .line 1077196
    if-nez v0, :cond_19

    .line 1077197
    :goto_1
    goto :goto_0

    .line 1077198
    :pswitch_5
    iget-object v0, p0, LX/6Km;->B:LX/6Ki;

    sget-object v1, LX/6Ki;->PAUSED:LX/6Ki;

    if-eq v0, v1, :cond_1f

    .line 1077199
    :cond_2
    goto :goto_0

    .line 1077200
    :pswitch_6
    const/16 v2, 0xd

    .line 1077201
    invoke-static {p0, v2}, LX/6Km;->c(LX/6Km;I)V

    .line 1077202
    invoke-static {p0}, LX/6Km;->s(LX/6Km;)V

    .line 1077203
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1077204
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1077205
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    invoke-static {p0, v0}, LX/6Km;->f(LX/6Km;LX/6Kd;)V

    .line 1077206
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 1077207
    :cond_3
    iget-object v0, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1077208
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1077209
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ke;

    .line 1077210
    invoke-static {p0, v0}, LX/6Km;->f(LX/6Km;LX/6Kd;)V

    .line 1077211
    invoke-static {p0, v0}, LX/6Km;->a(LX/6Km;LX/6Ke;)V

    .line 1077212
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 1077213
    :cond_4
    iget-object v0, p0, LX/6Km;->b:LX/5PK;

    invoke-interface {v0}, LX/5PK;->a()V

    .line 1077214
    iget-object v0, p0, LX/6Km;->c:LX/6KZ;

    .line 1077215
    iget-object v1, v0, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6KY;

    .line 1077216
    const/4 v0, 0x0

    iput-boolean v0, v1, LX/6KY;->e:Z

    .line 1077217
    goto :goto_4

    .line 1077218
    :cond_5
    invoke-static {p0}, LX/6Km;->w(LX/6Km;)V

    .line 1077219
    invoke-static {p0, v2}, LX/6Km;->d(LX/6Km;I)V

    .line 1077220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/6Km;->a(Ljava/util/Map;)V

    .line 1077221
    goto/16 :goto_0

    .line 1077222
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/6Jh;

    .line 1077223
    invoke-static {p0}, LX/6Km;->s(LX/6Km;)V

    .line 1077224
    iput-object v0, p0, LX/6Km;->f:LX/6Jh;

    .line 1077225
    invoke-static {p0}, LX/6Km;->t(LX/6Km;)V

    .line 1077226
    goto/16 :goto_0

    .line 1077227
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1077228
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Kd;

    .line 1077229
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1077230
    if-eqz v1, :cond_21

    move v3, v4

    :goto_6
    const-string v6, "videoOutput cannot be null."

    invoke-static {v3, v6}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1077231
    iget-object v3, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v3, v1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    move v3, v4

    :goto_7
    const-string v6, "This videoOutput already exists. Nothing was added. (%s)"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v3, v6, v4}, LX/03g;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1077232
    iget-object v3, p0, LX/6Km;->i:LX/6Kl;

    invoke-interface {v1, v3}, LX/6Kd;->a(LX/6Kl;)V

    .line 1077233
    instance-of v3, v1, LX/6Ke;

    if-eqz v3, :cond_23

    .line 1077234
    check-cast v1, LX/6Ke;

    .line 1077235
    iget-object v3, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v3, v1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077236
    :goto_8
    goto :goto_5

    .line 1077237
    :cond_6
    goto/16 :goto_0

    .line 1077238
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1077239
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Kd;

    .line 1077240
    if-eqz v1, :cond_7

    iget-object v3, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v3, v1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1077241
    invoke-static {p0, v1}, LX/6Km;->f(LX/6Km;LX/6Kd;)V

    .line 1077242
    iget-object v3, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v3, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 1077243
    :cond_8
    goto/16 :goto_0

    .line 1077244
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 1077245
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/6Kd;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/Surface;

    const/16 v5, 0xc

    .line 1077246
    iget-object v2, p0, LX/6Km;->B:LX/6Ki;

    sget-object v3, LX/6Ki;->PAUSED:LX/6Ki;

    if-eq v2, v3, :cond_9

    invoke-interface {v1}, LX/6Kd;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_24

    .line 1077247
    :cond_9
    :goto_a
    goto/16 :goto_0

    .line 1077248
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/6Kd;

    .line 1077249
    iget-object v1, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v1, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 1077250
    iget-object v1, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v1, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5PS;

    invoke-interface {v1}, LX/5PS;->c()V

    .line 1077251
    iget-object v1, p0, LX/6Km;->d:LX/026;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077252
    :cond_a
    goto/16 :goto_0

    .line 1077253
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/6Kd;

    .line 1077254
    iget-object v1, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077255
    iget-object v1, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077256
    invoke-static {p0}, LX/6Km;->v(LX/6Km;)V

    .line 1077257
    goto/16 :goto_0

    .line 1077258
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/6Ko;

    .line 1077259
    iget-object v1, v0, LX/6Ko;->a:LX/7Sb;

    invoke-interface {v1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->isSystemEvent()Z

    move-result v1

    if-nez v1, :cond_b

    .line 1077260
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/6Km;->a(Ljava/util/Map;)V

    .line 1077261
    :cond_b
    iget-object v1, v0, LX/6Ko;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Jv;

    .line 1077262
    iget-object v3, v0, LX/6Ko;->a:LX/7Sb;

    invoke-interface {v1, v3}, LX/6Jv;->a(LX/7Sb;)V

    goto :goto_b

    .line 1077263
    :cond_c
    iget-object v1, p0, LX/6Km;->m:LX/6Kp;

    invoke-virtual {v1, v0}, LX/6Kp;->a(LX/6Ko;)V

    .line 1077264
    goto/16 :goto_0

    .line 1077265
    :pswitch_e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1077266
    iget-object v1, p0, LX/6Km;->c:LX/6KZ;

    .line 1077267
    if-eqz v0, :cond_f

    .line 1077268
    :goto_c
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1077269
    iget-object v2, v1, LX/6KZ;->d:LX/6KY;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077270
    :cond_d
    iget-boolean v2, v1, LX/6KZ;->e:Z

    if-eqz v2, :cond_12

    .line 1077271
    iget-object v2, v1, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_e
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6KY;

    .line 1077272
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 1077273
    invoke-virtual {v2}, LX/6KY;->a()V

    goto :goto_d

    .line 1077274
    :cond_f
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_c

    .line 1077275
    :cond_10
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_11
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6KY;

    .line 1077276
    iget-object v4, v1, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 1077277
    iget-object v4, v1, LX/6KZ;->c:LX/5Pc;

    invoke-virtual {v2, v4}, LX/6KY;->a(LX/5Pc;)V

    .line 1077278
    iget v4, v1, LX/6KZ;->g:I

    if-lez v4, :cond_11

    iget v4, v1, LX/6KZ;->f:I

    if-lez v4, :cond_11

    .line 1077279
    iget v4, v1, LX/6KZ;->f:I

    iget p0, v1, LX/6KZ;->g:I

    invoke-virtual {v2, v4, p0}, LX/6KY;->a(II)V

    goto :goto_e

    .line 1077280
    :cond_12
    iput-object v0, v1, LX/6KZ;->b:Ljava/util/List;

    .line 1077281
    goto/16 :goto_0

    .line 1077282
    :pswitch_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1077283
    iget v1, p0, LX/6Km;->y:I

    if-eq v1, v0, :cond_13

    .line 1077284
    iput v0, p0, LX/6Km;->y:I

    .line 1077285
    invoke-static {p0}, LX/6Km;->m(LX/6Km;)V

    .line 1077286
    :cond_13
    goto/16 :goto_0

    .line 1077287
    :pswitch_10
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/6Kf;

    .line 1077288
    iget-object v1, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v1}, LX/6Kf;->c()V

    .line 1077289
    iput-object v0, p0, LX/6Km;->r:LX/6Kf;

    .line 1077290
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1077291
    iget-object v1, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v1, p0}, LX/6Kf;->a(LX/6Km;)V

    .line 1077292
    :cond_14
    goto/16 :goto_0

    .line 1077293
    :cond_15
    iget-object v4, p0, LX/6Km;->n:LX/6KN;

    if-eqz v4, :cond_16

    .line 1077294
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 1077295
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1077296
    invoke-static {v8, v9}, LX/6KM;->a(J)V

    .line 1077297
    :cond_16
    move v6, v7

    .line 1077298
    :goto_f
    iget-object v4, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v4}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v6, v4, :cond_1

    .line 1077299
    iget-object v4, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v4, v6}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6Kd;

    .line 1077300
    invoke-interface {v4}, LX/6Kd;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1077301
    iget-object v5, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v5, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5PS;

    .line 1077302
    if-eqz v4, :cond_18

    .line 1077303
    invoke-interface {v4}, LX/5PS;->a()V

    .line 1077304
    invoke-static {p0}, LX/6Km;->o(LX/6Km;)V

    .line 1077305
    iget-object v4, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    iget-object v5, p0, LX/6Km;->t:[F

    invoke-virtual {v4, v5}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1077306
    iget-object v4, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v4}, LX/6Kf;->a()V

    move v8, v7

    .line 1077307
    :goto_10
    iget-object v4, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v4}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v8, v4, :cond_1

    .line 1077308
    iget-object v4, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v4, v6}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6Ke;

    .line 1077309
    iget-object v5, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v5, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5PS;

    .line 1077310
    if-eqz v5, :cond_17

    invoke-interface {v4}, LX/6Kd;->isEnabled()Z

    move-result v9

    if-eqz v9, :cond_17

    .line 1077311
    invoke-interface {v5}, LX/5PS;->a()V

    .line 1077312
    invoke-static {v7}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1077313
    invoke-interface {v4}, LX/6Kd;->f()V

    .line 1077314
    invoke-interface {v5}, LX/5PS;->b()V

    .line 1077315
    const-string v4, "RenderManager::handleFrameAvailable - frameprocessors"

    invoke-static {v4}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1077316
    :cond_17
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_10

    .line 1077317
    :cond_18
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_f

    .line 1077318
    :cond_19
    invoke-virtual {p0, v2}, LX/6Km;->a(Ljava/util/Map;)V

    .line 1077319
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-eqz v0, :cond_1a

    .line 1077320
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->dE_()V

    .line 1077321
    :cond_1a
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1b

    .line 1077322
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1077323
    iput-object v2, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    .line 1077324
    :cond_1b
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    if-eqz v0, :cond_1c

    .line 1077325
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1077326
    iput-object v2, p0, LX/6Km;->s:LX/5Pf;

    .line 1077327
    :cond_1c
    invoke-static {p0}, LX/6Km;->w(LX/6Km;)V

    .line 1077328
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    .line 1077329
    invoke-interface {v0}, LX/6Kd;->dE_()V

    .line 1077330
    invoke-static {p0, v0}, LX/6Km;->g(LX/6Km;LX/6Kd;)V

    .line 1077331
    goto :goto_11

    .line 1077332
    :cond_1d
    iget-object v0, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ke;

    .line 1077333
    invoke-static {p0, v0}, LX/6Km;->a(LX/6Km;LX/6Ke;)V

    goto :goto_12

    .line 1077334
    :cond_1e
    iget-object v0, p0, LX/6Km;->b:LX/5PK;

    invoke-interface {v0}, LX/5PK;->a()V

    goto/16 :goto_1

    .line 1077335
    :cond_1f
    sget-object v0, LX/6Ki;->RESUMING:LX/6Ki;

    iput-object v0, p0, LX/6Km;->B:LX/6Ki;

    .line 1077336
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    .line 1077337
    iget-object v2, p0, LX/6Km;->i:LX/6Kl;

    invoke-interface {v0, v2}, LX/6Kd;->a(LX/6Kl;)V

    goto :goto_13

    .line 1077338
    :cond_20
    iget-object v0, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ke;

    .line 1077339
    iget-object v2, p0, LX/6Km;->i:LX/6Kl;

    invoke-interface {v0, v2}, LX/6Kd;->a(LX/6Kl;)V

    goto :goto_14

    :cond_21
    move v3, v5

    .line 1077340
    goto/16 :goto_6

    :cond_22
    move v3, v5

    .line 1077341
    goto/16 :goto_7

    .line 1077342
    :cond_23
    iget-object v3, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v3, v1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 1077343
    :cond_24
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v2

    .line 1077344
    if-nez v2, :cond_25

    .line 1077345
    invoke-static {p0, v5}, LX/6Km;->c(LX/6Km;I)V

    .line 1077346
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 1077347
    :cond_25
    :goto_15
    iget-object v3, p0, LX/6Km;->b:LX/5PK;

    invoke-interface {v3, v0}, LX/5PK;->a(Landroid/view/Surface;)LX/5PS;

    move-result-object v3

    .line 1077348
    invoke-interface {v3}, LX/5PS;->a()V

    .line 1077349
    instance-of v4, v1, LX/6Ke;

    if-eqz v4, :cond_27

    .line 1077350
    check-cast v1, LX/6Ke;

    .line 1077351
    iget-object v4, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v4, v1, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077352
    :goto_16
    invoke-static {p0}, LX/6Km;->o(LX/6Km;)V

    .line 1077353
    if-nez v2, :cond_26

    .line 1077354
    iget-object v2, p0, LX/6Km;->p:LX/7Se;

    iget-object v3, p0, LX/6Km;->c:LX/6KZ;

    .line 1077355
    iget-object v4, v3, LX/6KZ;->c:LX/5Pc;

    move-object v3, v4

    .line 1077356
    invoke-virtual {v2, v3}, LX/7Se;->a(LX/5Pc;)V

    .line 1077357
    invoke-static {}, LX/6Km;->x()LX/5Pf;

    move-result-object v2

    invoke-static {p0, v2}, LX/6Km;->c(LX/6Km;LX/5Pf;)V

    .line 1077358
    sget-object v2, LX/6Ki;->INITIALIZED:LX/6Ki;

    iput-object v2, p0, LX/6Km;->B:LX/6Ki;

    .line 1077359
    invoke-static {p0}, LX/6Km;->t(LX/6Km;)V

    .line 1077360
    iget-object v2, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v2, p0}, LX/6Kf;->a(LX/6Km;)V

    .line 1077361
    const-string v2, "RenderManager::completeInitialization"

    invoke-static {v2}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1077362
    invoke-static {p0, v5}, LX/6Km;->d(LX/6Km;I)V

    .line 1077363
    :cond_26
    const-string v2, "RenderManager::handleOutputSurfaceCreated"

    invoke-static {v2}, LX/5PP;->a(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1077364
    :cond_27
    iget-object v4, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v4, v1, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_16

    .line 1077365
    :cond_28
    iget-object v3, p0, LX/6Km;->b:LX/5PK;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/5PK;->a(I)LX/5PK;

    goto :goto_15

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_0
        :pswitch_6
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_c
        :pswitch_10
    .end packed-switch
.end method

.method private b(Ljava/util/Map;)V
    .locals 13
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1077366
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    if-eqz v0, :cond_3

    .line 1077367
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    iget-object v1, p0, LX/6Km;->c:LX/6KZ;

    .line 1077368
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1077369
    iget-object v2, v1, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6KY;

    .line 1077370
    iget-object v5, v2, LX/6KY;->b:LX/61G;

    if-eqz v5, :cond_0

    .line 1077371
    iget-object v5, v2, LX/6KY;->c:LX/6KI;

    iget-object v1, v2, LX/6KY;->b:LX/61G;

    invoke-interface {v1}, LX/61G;->d()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v5, LX/6KI;->d:Ljava/util/Map;

    .line 1077372
    :cond_0
    iget-object v5, v2, LX/6KY;->c:LX/6KI;

    iget-object v1, v2, LX/6KY;->a:LX/61B;

    invoke-interface {v1}, LX/61B;->c()Z

    move-result v1

    iput-boolean v1, v5, LX/6KI;->a:Z

    .line 1077373
    iget-object v5, v2, LX/6KY;->c:LX/6KI;

    move-object v2, v5

    .line 1077374
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1077375
    :cond_1
    move-object v1, v3

    .line 1077376
    iget-object v2, p0, LX/6Km;->o:Ljava/lang/String;

    .line 1077377
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6KI;

    .line 1077378
    iget-wide v5, v3, LX/6KI;->c:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    iget-boolean v5, v3, LX/6KI;->a:Z

    if-eqz v5, :cond_2

    .line 1077379
    iget-wide v5, v3, LX/6KI;->c:J

    long-to-float v5, v5

    iget-wide v7, v3, LX/6KI;->b:J

    long-to-float v6, v7

    const v7, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v6, v7

    div-float/2addr v5, v6

    float-to-int v5, v5

    .line 1077380
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "filter_performance_report"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1077381
    const-string v7, "product_name"

    iget-object v8, v0, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077382
    const-string v7, "frame_count"

    iget-wide v9, v3, LX/6KI;->c:J

    invoke-virtual {v6, v7, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077383
    const-string v7, "fps"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077384
    const-string v5, "media_pipeline_session_id"

    invoke-virtual {v6, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077385
    const-string v5, "product_session_id"

    iget-object v7, v0, LX/6KN;->f:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077386
    iget-object v5, v3, LX/6KI;->d:Ljava/util/Map;

    invoke-virtual {v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077387
    invoke-virtual {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077388
    const-wide/16 v11, 0x0

    .line 1077389
    iput-wide v11, v3, LX/6KI;->b:J

    .line 1077390
    iput-wide v11, v3, LX/6KI;->c:J

    .line 1077391
    const/4 v11, 0x0

    iput-object v11, v3, LX/6KI;->d:Ljava/util/Map;

    .line 1077392
    const/4 v11, 0x0

    iput-boolean v11, v3, LX/6KI;->a:Z

    .line 1077393
    iget-object v3, v0, LX/6KN;->b:LX/0Zb;

    invoke-interface {v3, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    .line 1077394
    :cond_3
    return-void
.end method

.method public static c(LX/6Km;I)V
    .locals 2

    .prologue
    .line 1077395
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    if-eqz v0, :cond_0

    .line 1077396
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    iget-object v1, p0, LX/6Km;->o:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/6KN;->a(ILjava/lang/String;)V

    .line 1077397
    :cond_0
    return-void
.end method

.method public static c(LX/6Km;LX/5Pf;)V
    .locals 2

    .prologue
    .line 1077398
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    if-ne v0, p1, :cond_0

    .line 1077399
    :goto_0
    return-void

    .line 1077400
    :cond_0
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    if-eqz v0, :cond_1

    .line 1077401
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1077402
    :cond_1
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_2

    .line 1077403
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1077404
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    .line 1077405
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, LX/6Km;->z:I

    .line 1077406
    iput-object p1, p0, LX/6Km;->s:LX/5Pf;

    .line 1077407
    iget-object v0, p0, LX/6Km;->s:LX/5Pf;

    iget v0, v0, LX/5Pf;->a:I

    const v1, 0x8d65

    if-ne v0, v1, :cond_4

    .line 1077408
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_3

    .line 1077409
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1077410
    :cond_3
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/6Km;->s:LX/5Pf;

    iget v1, v1, LX/5Pf;->b:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    .line 1077411
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/6Km;->j:LX/6Kk;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1077412
    const-string v0, "RenderManager::initSurfaceTexture"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1077413
    :cond_4
    invoke-direct {p0}, LX/6Km;->u()V

    goto :goto_0
.end method

.method private static c(LX/6Km;LX/6Kd;)[F
    .locals 12

    .prologue
    .line 1077414
    iget-object v0, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077415
    iget-object v0, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 1077416
    :goto_0
    return-object v0

    .line 1077417
    :cond_0
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 1077418
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077419
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1077420
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->getInputWidth()I

    move-result v1

    iget-object v2, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v2}, LX/6Jh;->getInputHeight()I

    move-result v2

    invoke-interface {p1}, LX/6Kd;->getWidth()I

    move-result v3

    invoke-interface {p1}, LX/6Kd;->getHeight()I

    move-result v4

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 1077421
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    array-length v5, v0

    const/16 v7, 0x10

    if-ne v5, v7, :cond_3

    const/4 v5, 0x1

    :goto_1
    const-string v7, "Invalid input parameters to calculate crop matrix"

    invoke-static {v5, v7}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1077422
    int-to-float v5, v3

    int-to-float v7, v4

    div-float/2addr v5, v7

    .line 1077423
    cmpl-float v7, v5, v9

    if-lez v7, :cond_5

    .line 1077424
    div-float v5, v9, v5

    move v7, v5

    .line 1077425
    :goto_2
    int-to-float v5, v1

    int-to-float v8, v2

    div-float/2addr v5, v8

    .line 1077426
    cmpl-float v8, v5, v9

    if-lez v8, :cond_1

    .line 1077427
    div-float v5, v9, v5

    .line 1077428
    :cond_1
    cmpg-float v8, v7, v5

    if-gez v8, :cond_4

    .line 1077429
    mul-float/2addr v5, v9

    .line 1077430
    mul-float/2addr v7, v9

    .line 1077431
    sub-float v7, v5, v7

    div-float/2addr v7, v10

    .line 1077432
    div-float v5, v7, v5

    move v11, v6

    move v6, v5

    move v5, v11

    .line 1077433
    :goto_3
    new-instance v7, Landroid/graphics/RectF;

    sub-float v8, v9, v6

    sub-float/2addr v9, v5

    invoke-direct {v7, v6, v5, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1077434
    invoke-static {v0, v7}, LX/61I;->a([FLandroid/graphics/RectF;)V

    .line 1077435
    :cond_2
    iget-object v1, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1077436
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 1077437
    :cond_4
    div-float v5, v9, v5

    .line 1077438
    div-float v7, v9, v7

    .line 1077439
    sub-float v7, v5, v7

    div-float/2addr v7, v10

    .line 1077440
    div-float v5, v7, v5

    goto :goto_3

    :cond_5
    move v7, v5

    goto :goto_2
.end method

.method public static d(LX/6Km;I)V
    .locals 1

    .prologue
    .line 1077441
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    if-eqz v0, :cond_0

    .line 1077442
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->a(I)V

    .line 1077443
    :cond_0
    return-void
.end method

.method private static d(LX/6Km;LX/6Kd;)[F
    .locals 11

    .prologue
    .line 1077444
    iget-object v0, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077445
    iget-object v0, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 1077446
    :goto_0
    return-object v0

    .line 1077447
    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    .line 1077448
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1077449
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->getInputWidth()I

    move-result v1

    iget-object v2, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v2}, LX/6Jh;->getInputHeight()I

    move-result v2

    invoke-interface {p1}, LX/6Kd;->getWidth()I

    move-result v3

    invoke-interface {p1}, LX/6Kd;->getHeight()I

    move-result v4

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    .line 1077450
    int-to-float v5, v1

    int-to-float v7, v2

    div-float/2addr v5, v7

    .line 1077451
    int-to-float v7, v3

    int-to-float v9, v4

    div-float/2addr v7, v9

    .line 1077452
    cmpl-float v9, v5, v7

    if-nez v9, :cond_2

    .line 1077453
    :cond_1
    :goto_1
    iget-object v1, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1077454
    :cond_2
    cmpl-float v9, v5, v7

    if-lez v9, :cond_3

    .line 1077455
    int-to-float v9, v4

    mul-float/2addr v7, v9

    div-float v5, v7, v5

    .line 1077456
    int-to-float v7, v4

    sub-float v5, v7, v5

    .line 1077457
    int-to-float v7, v4

    div-float/2addr v5, v7

    .line 1077458
    add-float v7, v8, v5

    .line 1077459
    sub-float v5, v6, v5

    move v10, v7

    move v7, v6

    move v6, v10

    .line 1077460
    :goto_2
    const/4 v9, 0x0

    aput v8, v0, v9

    .line 1077461
    const/4 v9, 0x1

    aput v6, v0, v9

    .line 1077462
    const/4 v9, 0x2

    aput v7, v0, v9

    .line 1077463
    const/4 v9, 0x3

    aput v6, v0, v9

    .line 1077464
    const/4 v6, 0x4

    aput v8, v0, v6

    .line 1077465
    const/4 v6, 0x5

    aput v5, v0, v6

    .line 1077466
    const/4 v6, 0x6

    aput v7, v0, v6

    .line 1077467
    const/4 v6, 0x7

    aput v5, v0, v6

    goto :goto_1

    .line 1077468
    :cond_3
    int-to-float v9, v3

    mul-float/2addr v5, v9

    div-float/2addr v5, v7

    .line 1077469
    int-to-float v7, v3

    sub-float v5, v7, v5

    .line 1077470
    int-to-float v7, v3

    div-float/2addr v5, v7

    .line 1077471
    add-float v7, v8, v5

    .line 1077472
    sub-float v5, v6, v5

    move v10, v6

    move v6, v8

    move v8, v7

    move v7, v5

    move v5, v10

    goto :goto_2

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static f(LX/6Km;LX/6Kd;)V
    .locals 1

    .prologue
    .line 1077128
    iget-object v0, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077129
    iget-object v0, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077130
    invoke-interface {p1}, LX/6Kd;->c()V

    .line 1077131
    invoke-static {p0, p1}, LX/6Km;->g(LX/6Km;LX/6Kd;)V

    .line 1077132
    return-void
.end method

.method public static g(LX/6Km;LX/6Kd;)V
    .locals 2

    .prologue
    .line 1077473
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5PS;

    .line 1077474
    if-eqz v0, :cond_0

    .line 1077475
    invoke-interface {v0}, LX/5PS;->c()V

    .line 1077476
    iget-object v0, p0, LX/6Km;->d:LX/026;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077477
    :cond_0
    return-void
.end method

.method public static g(LX/6Km;)Z
    .locals 2

    .prologue
    .line 1077478
    iget-object v0, p0, LX/6Km;->B:LX/6Ki;

    sget-object v1, LX/6Ki;->INITIALIZED:LX/6Ki;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1077133
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077134
    invoke-direct {p0}, LX/6Km;->n()V

    .line 1077135
    :cond_0
    iget-object v0, p0, LX/6Km;->r:LX/6Kf;

    invoke-interface {v0}, LX/6Kf;->b()V

    .line 1077136
    return-void
.end method

.method public static m(LX/6Km;)V
    .locals 3

    .prologue
    .line 1077479
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-nez v0, :cond_0

    .line 1077480
    :goto_0
    return-void

    .line 1077481
    :cond_0
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getRotationDegrees$134621()I

    move-result v0

    .line 1077482
    iget-object v1, p0, LX/6Km;->u:[F

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077483
    iget-object v1, p0, LX/6Km;->u:[F

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    invoke-static {v1, v0}, LX/61I;->a([FI)V

    goto :goto_0
.end method

.method private n()V
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 1076972
    iget-object v3, p0, LX/6Km;->t:[F

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v3}, LX/01J;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/6Km;->f:LX/6Jh;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v3}, LX/6Kc;->f()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1076973
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v12, v2

    .line 1076974
    :goto_1
    :try_start_0
    iget-object v2, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v2}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-ge v12, v2, :cond_5

    .line 1076975
    iget-object v2, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v2, v12}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/6Kd;

    move-object v9, v0

    .line 1076976
    iget-object v2, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v2, v9}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/5PS;

    move-object v10, v0

    .line 1076977
    if-eqz v10, :cond_4

    invoke-interface {v9}, LX/6Kd;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1076978
    invoke-interface {v10}, LX/5PS;->a()V

    .line 1076979
    iget-object v2, p0, LX/6Km;->c:LX/6KZ;

    iget-object v3, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v3}, LX/6Jh;->getClock()LX/6Jg;

    move-result-object v3

    iget-object v4, p0, LX/6Km;->s:LX/5Pf;

    iget-object v5, p0, LX/6Km;->h:LX/6Kc;

    iget-object v6, p0, LX/6Km;->t:[F

    iget-object v7, p0, LX/6Km;->u:[F

    iget-object v8, p0, LX/6Km;->v:[F

    invoke-virtual/range {v2 .. v8}, LX/6KZ;->a(LX/6Jg;LX/5Pf;LX/6Kc;[F[F[F)LX/5PO;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v8

    .line 1076980
    if-nez v8, :cond_2

    .line 1076981
    if-eqz v8, :cond_0

    .line 1076982
    iget-object v2, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v2, v8}, LX/6Kc;->a(LX/5PO;)V

    goto :goto_0

    .line 1076983
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-interface {v9}, LX/6Kd;->getWidth()I

    move-result v4

    invoke-interface {v9}, LX/6Kd;->getHeight()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1076984
    iget-object v2, p0, LX/6Km;->p:LX/7Se;

    iget-object v3, v8, LX/5PO;->d:LX/5Pf;

    .line 1076985
    iput-object v3, v2, LX/7Se;->e:LX/5Pf;

    .line 1076986
    iget-object v2, p0, LX/6Km;->p:LX/7Se;

    invoke-static {p0, v9}, LX/6Km;->d(LX/6Km;LX/6Kd;)[F

    move-result-object v3

    const/4 v1, 0x0

    .line 1076987
    if-eqz v3, :cond_9

    array-length v0, v3

    const/16 v4, 0x8

    if-ne v0, v4, :cond_9

    const/4 v0, 0x1

    :goto_2
    const-string v4, "Positional data must contain 8 elements"

    invoke-static {v0, v4}, LX/64O;->a(ZLjava/lang/Object;)V

    .line 1076988
    iget-object v0, v2, LX/7Se;->i:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 1076989
    iget-object v0, v2, LX/7Se;->i:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 1076990
    iget-object v3, p0, LX/6Km;->v:[F

    .line 1076991
    sget-object v2, LX/61D;->DEFAULT:LX/61D;

    .line 1076992
    instance-of v4, v9, LX/6Kh;

    if-eqz v4, :cond_3

    .line 1076993
    move-object v0, v9

    check-cast v0, LX/6Kh;

    move-object v2, v0

    .line 1076994
    invoke-interface {v2}, LX/6Kh;->a()[F

    move-result-object v3

    .line 1076995
    invoke-interface {v2}, LX/6Kh;->b()LX/61D;

    move-result-object v2

    .line 1076996
    :cond_3
    iget-object v4, p0, LX/6Km;->p:LX/7Se;

    .line 1076997
    iput-object v2, v4, LX/7Se;->g:LX/61D;

    .line 1076998
    iget-object v2, p0, LX/6Km;->p:LX/7Se;

    invoke-static {p0, v9}, LX/6Km;->c(LX/6Km;LX/6Kd;)[F

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/7Se;->a([F[F[FJ)V

    .line 1076999
    invoke-interface {v9}, LX/6Kd;->f()V

    .line 1077000
    invoke-interface {v10}, LX/5PS;->b()V

    .line 1077001
    iget-object v2, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v2, v8}, LX/6Kc;->a(LX/5PO;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1077002
    :try_start_2
    iget-object v2, p0, LX/6Km;->p:LX/7Se;

    sget-object v3, LX/61D;->DEFAULT:LX/61D;

    .line 1077003
    iput-object v3, v2, LX/7Se;->g:LX/61D;

    .line 1077004
    const/4 v2, 0x0

    iput v2, p0, LX/6Km;->A:I

    .line 1077005
    const-string v2, "RenderManager::renderTextureToOutputs"

    invoke-static {v2}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1077006
    :cond_4
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto/16 :goto_1

    .line 1077007
    :cond_5
    iget-object v2, p0, LX/6Km;->n:LX/6KN;

    if-eqz v2, :cond_0

    .line 1077008
    iget-object v2, p0, LX/6Km;->n:LX/6KN;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 1077009
    iget-object v0, v2, LX/6KN;->d:LX/6Kb;

    invoke-virtual {v0, v4, v5}, LX/6Kb;->a(J)V

    .line 1077010
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1077011
    invoke-static {v4, v5}, LX/6KM;->b(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1077012
    :cond_6
    goto/16 :goto_0

    .line 1077013
    :catch_0
    move-exception v2

    move-object v3, v11

    .line 1077014
    :goto_3
    :try_start_3
    sget-object v4, LX/6Km;->a:Ljava/lang/String;

    const-string v5, "Failed to render to outputs"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1077015
    iget v4, p0, LX/6Km;->A:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/6Km;->A:I

    const/16 v5, 0xa

    if-lt v4, v5, :cond_8

    .line 1077016
    invoke-static {v2}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1077017
    :catchall_0
    move-exception v2

    move-object v11, v3

    :goto_4
    if-eqz v11, :cond_7

    .line 1077018
    iget-object v3, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v3, v11}, LX/6Kc;->a(LX/5PO;)V

    :cond_7
    throw v2

    .line 1077019
    :cond_8
    if-eqz v3, :cond_0

    .line 1077020
    iget-object v2, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v2, v3}, LX/6Kc;->a(LX/5PO;)V

    goto/16 :goto_0

    .line 1077021
    :catchall_1
    move-exception v2

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v11, v8

    goto :goto_4

    .line 1077022
    :catch_1
    move-exception v2

    move-object v3, v8

    goto :goto_3

    :cond_9
    move v0, v1

    .line 1077023
    goto/16 :goto_2
.end method

.method public static o(LX/6Km;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1077024
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_0

    .line 1077025
    :goto_0
    return-void

    :cond_0
    move v0, v1

    .line 1077026
    :goto_1
    iget v2, p0, LX/6Km;->z:I

    if-ge v0, v2, :cond_1

    .line 1077027
    iget-object v2, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1077028
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1077029
    :cond_1
    iput v1, p0, LX/6Km;->z:I

    .line 1077030
    const-string v0, "renderManager::updateTextImage"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static s(LX/6Km;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1077031
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-eqz v0, :cond_0

    .line 1077032
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->c()V

    .line 1077033
    iput-object v1, p0, LX/6Km;->f:LX/6Jh;

    .line 1077034
    :cond_0
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    .line 1077035
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1077036
    iput-object v1, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    .line 1077037
    :cond_1
    return-void
.end method

.method public static t(LX/6Km;)V
    .locals 2

    .prologue
    .line 1077038
    invoke-static {p0}, LX/6Km;->g(LX/6Km;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-nez v0, :cond_1

    .line 1077039
    :cond_0
    :goto_0
    return-void

    .line 1077040
    :cond_1
    iget-object v0, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_2

    .line 1077041
    invoke-static {}, LX/6Km;->x()LX/5Pf;

    move-result-object v0

    invoke-static {p0, v0}, LX/6Km;->c(LX/6Km;LX/5Pf;)V

    .line 1077042
    :cond_2
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    iget-object v1, p0, LX/6Km;->q:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1, p0}, LX/6Jh;->a(Landroid/graphics/SurfaceTexture;LX/6Km;)V

    .line 1077043
    invoke-direct {p0}, LX/6Km;->u()V

    .line 1077044
    iget-object v0, p0, LX/6Km;->t:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1077045
    iget-object v0, p0, LX/6Km;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1077046
    iget-object v0, p0, LX/6Km;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1077047
    invoke-static {p0}, LX/6Km;->m(LX/6Km;)V

    .line 1077048
    const-string v0, "RenderManager::initInput"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 1077049
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputHeight()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputWidth()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/01J;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6Km;->e:LX/026;

    invoke-virtual {v0}, LX/01J;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v0}, LX/6Kc;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/6Km;->h:LX/6Kc;

    .line 1077050
    iget v1, v0, LX/6Kc;->c:I

    move v0, v1

    .line 1077051
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->getInputWidth()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, LX/6Km;->h:LX/6Kc;

    .line 1077052
    iget v1, v0, LX/6Kc;->d:I

    move v0, v1

    .line 1077053
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->getInputHeight()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1077054
    :cond_2
    :goto_0
    return-void

    .line 1077055
    :cond_3
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 1077056
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputWidth()I

    move-result v0

    .line 1077057
    iget-object v1, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v1}, LX/6Jh;->getInputHeight()I

    move-result v1

    .line 1077058
    iget-object v2, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v2}, LX/6Kc;->f()Z

    move-result v2

    .line 1077059
    iget-object v3, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v3, v0, v1}, LX/6Kc;->a(II)V

    .line 1077060
    if-nez v2, :cond_5

    .line 1077061
    iget-object v0, p0, LX/6Km;->c:LX/6KZ;

    .line 1077062
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/6KZ;->e:Z

    .line 1077063
    iget-object v1, v0, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6KY;

    .line 1077064
    iget-object v3, v0, LX/6KZ;->c:LX/5Pc;

    invoke-virtual {v1, v3}, LX/6KY;->a(LX/5Pc;)V

    goto :goto_1

    .line 1077065
    :cond_4
    iget-object v1, v0, LX/6KZ;->b:Ljava/util/List;

    iget-object v2, v0, LX/6KZ;->d:LX/6KY;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1077066
    iget-object v1, v0, LX/6KZ;->d:LX/6KY;

    iget-object v2, v0, LX/6KZ;->c:LX/5Pc;

    invoke-virtual {v1, v2}, LX/6KY;->a(LX/5Pc;)V

    .line 1077067
    :cond_5
    invoke-static {p0}, LX/6Km;->v(LX/6Km;)V

    goto :goto_0
.end method

.method public static v(LX/6Km;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1077068
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputHeight()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputWidth()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/01J;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1077069
    :cond_1
    :goto_0
    return-void

    .line 1077070
    :cond_2
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputWidth()I

    move-result v1

    .line 1077071
    iget-object v0, p0, LX/6Km;->f:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->getInputHeight()I

    move-result v2

    .line 1077072
    iget-object v0, p0, LX/6Km;->d:LX/026;

    invoke-virtual {v0}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    .line 1077073
    invoke-interface {v0}, LX/6Kd;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-interface {v0}, LX/6Kd;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v3, v0

    .line 1077074
    int-to-float v3, v1

    int-to-float v4, v2

    div-float/2addr v3, v4

    .line 1077075
    cmpl-float v4, v3, v0

    if-eqz v4, :cond_7

    cmpl-float v4, v0, v5

    if-ltz v4, :cond_3

    cmpg-float v4, v3, v5

    if-lez v4, :cond_4

    :cond_3
    cmpg-float v0, v0, v5

    if-gtz v0, :cond_7

    cmpl-float v0, v3, v5

    if-ltz v0, :cond_7

    :cond_4
    move v0, v1

    move v1, v2

    .line 1077076
    :goto_1
    iget-object v2, p0, LX/6Km;->c:LX/6KZ;

    .line 1077077
    iput v1, v2, LX/6KZ;->f:I

    .line 1077078
    iput v0, v2, LX/6KZ;->g:I

    .line 1077079
    iget-object v3, v2, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6KY;

    .line 1077080
    invoke-virtual {v3, v1, v0}, LX/6KY;->a(II)V

    goto :goto_2

    .line 1077081
    :cond_5
    iget-object v3, v2, LX/6KZ;->b:Ljava/util/List;

    iget-object v4, v2, LX/6KZ;->d:LX/6KY;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1077082
    iget-object v3, v2, LX/6KZ;->d:LX/6KY;

    invoke-virtual {v3, v1, v0}, LX/6KY;->a(II)V

    .line 1077083
    :cond_6
    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public static w(LX/6Km;)V
    .locals 2

    .prologue
    .line 1077084
    iget-object v0, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v0}, LX/6Kc;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1077085
    :goto_0
    return-void

    .line 1077086
    :cond_0
    iget-object v0, p0, LX/6Km;->h:LX/6Kc;

    invoke-virtual {v0}, LX/6Kc;->b()V

    .line 1077087
    iget-object v0, p0, LX/6Km;->c:LX/6KZ;

    const/4 v1, 0x0

    .line 1077088
    iput-boolean v1, v0, LX/6KZ;->e:Z

    .line 1077089
    iput v1, v0, LX/6KZ;->f:I

    iput v1, v0, LX/6KZ;->g:I

    .line 1077090
    iget-object v1, v0, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6KY;

    .line 1077091
    invoke-virtual {v1}, LX/6KY;->a()V

    goto :goto_1

    .line 1077092
    :cond_1
    iget-object v1, v0, LX/6KZ;->b:Ljava/util/List;

    iget-object p0, v0, LX/6KZ;->d:LX/6KY;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1077093
    iget-object v1, v0, LX/6KZ;->d:LX/6KY;

    invoke-virtual {v1}, LX/6KY;->a()V

    .line 1077094
    :cond_2
    goto :goto_0
.end method

.method public static x()LX/5Pf;
    .locals 4

    .prologue
    const v3, 0x812f

    const/16 v2, 0x2601

    .line 1076968
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    const v1, 0x8d65

    .line 1076969
    iput v1, v0, LX/5Pe;->a:I

    .line 1076970
    move-object v0, v0

    .line 1076971
    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2802

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2803

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1077095
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LX/6Km;->k:Landroid/os/HandlerThread;

    if-ne v0, v1, :cond_0

    .line 1077096
    invoke-direct {p0}, LX/6Km;->i()V

    .line 1077097
    :goto_0
    return-void

    .line 1077098
    :cond_0
    iget-object v0, p0, LX/6Km;->l:LX/6Kj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/6Kj;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(LX/6Jh;)V
    .locals 1

    .prologue
    .line 1077099
    const/4 v0, 0x7

    invoke-static {p0, v0, p1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1077100
    return-void
.end method

.method public final a(LX/6Ko;)V
    .locals 3

    .prologue
    .line 1077101
    iget-object v0, p1, LX/6Ko;->a:LX/7Sb;

    invoke-interface {v0}, LX/7Sb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077102
    const/16 v0, 0xc

    invoke-static {p0, v0, p1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1077103
    :goto_0
    return-void

    .line 1077104
    :cond_0
    iget-object v0, p1, LX/6Ko;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jv;

    .line 1077105
    iget-object v2, p1, LX/6Ko;->a:LX/7Sb;

    invoke-interface {v0, v2}, LX/6Jv;->a(LX/7Sb;)V

    goto :goto_1

    .line 1077106
    :cond_1
    iget-object v0, p0, LX/6Km;->m:LX/6Kp;

    invoke-virtual {v0, p1}, LX/6Kp;->a(LX/6Ko;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1077107
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    if-eqz v0, :cond_0

    .line 1077108
    iget-object v0, p0, LX/6Km;->n:LX/6KN;

    iget-object v1, p0, LX/6Km;->o:Ljava/lang/String;

    .line 1077109
    iget-object v2, v0, LX/6KN;->d:LX/6Kb;

    invoke-virtual {v2}, LX/6Kb;->a()LX/6Ka;

    move-result-object v2

    .line 1077110
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "camera_frame_peformance_report"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1077111
    const-string v4, "duration_in_second"

    iget v5, v2, LX/6Ka;->a:F

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077112
    const-string v4, "frame_count"

    iget-wide v6, v2, LX/6Ka;->b:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077113
    const-string v4, "large_drop_event"

    iget-wide v6, v2, LX/6Ka;->d:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077114
    const-string v4, "big_drop_event"

    iget-wide v6, v2, LX/6Ka;->c:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077115
    const-string v4, "small_drop_event"

    iget-wide v6, v2, LX/6Ka;->e:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077116
    const-string v2, "camera_type"

    iget-object v4, v0, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077117
    const-string v2, "media_pipeline_session_id"

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077118
    const-string v2, "product_session_id"

    iget-object v4, v0, LX/6KN;->f:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1077119
    iget-object v2, v0, LX/6KN;->b:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1077120
    iget-object v2, v0, LX/6KN;->d:LX/6Kb;

    invoke-virtual {v2}, LX/6Kb;->b()V

    .line 1077121
    invoke-direct {p0, p1}, LX/6Km;->b(Ljava/util/Map;)V

    .line 1077122
    :cond_0
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6Kd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1077123
    const/16 v0, 0x9

    invoke-static {p0, v0, p1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1077124
    return-void
.end method

.method public final c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6Kd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1077125
    const/16 v0, 0x8

    invoke-static {p0, v0, p1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1077126
    return-void
.end method

.method public final e()LX/6Ko;
    .locals 1

    .prologue
    .line 1077127
    iget-object v0, p0, LX/6Km;->m:LX/6Kp;

    invoke-virtual {v0}, LX/6Kp;->a()LX/6Ko;

    move-result-object v0

    return-object v0
.end method
