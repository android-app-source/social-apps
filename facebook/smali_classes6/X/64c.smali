.class public final LX/64c;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic c:Z

.field private static final d:Ljava/util/concurrent/Executor;


# instance fields
.field public final a:LX/65T;

.field public b:Z

.field private final e:I

.field private final f:J

.field private final g:Ljava/lang/Runnable;

.field private final h:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "LX/65S;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1044947
    const-class v0, LX/64c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, LX/64c;->c:Z

    .line 1044948
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp ConnectionPool"

    .line 1044949
    invoke-static {v0, v8}, LX/65A;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, LX/64c;->d:Ljava/util/concurrent/Executor;

    .line 1044950
    return-void

    :cond_0
    move v0, v2

    .line 1044951
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1045014
    const/4 v0, 0x5

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, v0, v2, v3, v1}, LX/64c;-><init>(IJLjava/util/concurrent/TimeUnit;)V

    .line 1045015
    return-void
.end method

.method private constructor <init>(IJLjava/util/concurrent/TimeUnit;)V
    .locals 4

    .prologue
    .line 1045016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045017
    new-instance v0, Lokhttp3/ConnectionPool$1;

    invoke-direct {v0, p0}, Lokhttp3/ConnectionPool$1;-><init>(LX/64c;)V

    iput-object v0, p0, LX/64c;->g:Ljava/lang/Runnable;

    .line 1045018
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/64c;->h:Ljava/util/Deque;

    .line 1045019
    new-instance v0, LX/65T;

    invoke-direct {v0}, LX/65T;-><init>()V

    iput-object v0, p0, LX/64c;->a:LX/65T;

    .line 1045020
    iput p1, p0, LX/64c;->e:I

    .line 1045021
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/64c;->f:J

    .line 1045022
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 1045023
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "keepAliveDuration <= 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045024
    :cond_0
    return-void
.end method

.method private static a(LX/64c;LX/65S;J)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1044972
    iget-object v3, p1, LX/65S;->g:Ljava/util/List;

    move v1, v2

    .line 1044973
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1044974
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/Reference;

    .line 1044975
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1044976
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1044977
    goto :goto_0

    .line 1044978
    :cond_1
    sget-object v0, LX/66Y;->a:LX/66Y;

    move-object v0, v0

    .line 1044979
    const/4 v4, 0x5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "A connection to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1044980
    iget-object v6, p1, LX/65S;->k:LX/657;

    move-object v6, v6

    .line 1044981
    iget-object v7, v6, LX/657;->a:LX/64R;

    move-object v6, v7

    .line 1044982
    iget-object v7, v6, LX/64R;->a:LX/64q;

    move-object v6, v7

    .line 1044983
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was leaked. Did you forget to close a response body?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v5, v6}, LX/66Y;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1044984
    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1044985
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/65S;->h:Z

    .line 1044986
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1044987
    iget-wide v0, p0, LX/64c;->f:J

    sub-long v0, p2, v0

    iput-wide v0, p1, LX/65S;->i:J

    .line 1044988
    :goto_1
    return v2

    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public final a(J)J
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1044989
    const/4 v1, 0x0

    .line 1044990
    const-wide/high16 v4, -0x8000000000000000L

    .line 1044991
    monitor-enter p0

    .line 1044992
    :try_start_0
    iget-object v2, p0, LX/64c;->h:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    move v7, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1044993
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65S;

    .line 1044994
    invoke-static {p0, v0, p1, p2}, LX/64c;->a(LX/64c;LX/65S;J)I

    move-result v3

    if-lez v3, :cond_0

    .line 1044995
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    .line 1044996
    goto :goto_0

    .line 1044997
    :cond_0
    add-int/lit8 v6, v2, 0x1

    .line 1044998
    iget-wide v2, v0, LX/65S;->i:J

    sub-long v2, p1, v2

    .line 1044999
    cmp-long v9, v2, v4

    if-lez v9, :cond_6

    move-wide v10, v2

    move-object v2, v0

    move-wide v0, v10

    :goto_1
    move-wide v4, v0

    move-object v1, v2

    move v2, v6

    .line 1045000
    goto :goto_0

    .line 1045001
    :cond_1
    iget-wide v8, p0, LX/64c;->f:J

    cmp-long v0, v4, v8

    if-gez v0, :cond_2

    iget v0, p0, LX/64c;->e:I

    if-le v2, v0, :cond_3

    .line 1045002
    :cond_2
    iget-object v0, p0, LX/64c;->h:Ljava/util/Deque;

    invoke-interface {v0, v1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 1045003
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1045004
    iget-object v0, v1, LX/65S;->a:Ljava/net/Socket;

    move-object v0, v0

    .line 1045005
    invoke-static {v0}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1045006
    const-wide/16 v0, 0x0

    :goto_2
    return-wide v0

    .line 1045007
    :cond_3
    if-lez v2, :cond_4

    .line 1045008
    :try_start_1
    iget-wide v0, p0, LX/64c;->f:J

    sub-long/2addr v0, v4

    monitor-exit p0

    goto :goto_2

    .line 1045009
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1045010
    :cond_4
    if-lez v7, :cond_5

    .line 1045011
    :try_start_2
    iget-wide v0, p0, LX/64c;->f:J

    monitor-exit p0

    goto :goto_2

    .line 1045012
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/64c;->b:Z

    .line 1045013
    const-wide/16 v0, -0x1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_6
    move-object v2, v1

    move-wide v0, v4

    goto :goto_1
.end method

.method public final a(LX/64R;LX/65W;)LX/65S;
    .locals 4

    .prologue
    .line 1044965
    sget-boolean v0, LX/64c;->c:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1044966
    :cond_0
    iget-object v0, p0, LX/64c;->h:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65S;

    .line 1044967
    iget-object v2, v0, LX/65S;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, v0, LX/65S;->f:I

    if-ge v2, v3, :cond_1

    .line 1044968
    iget-object v2, v0, LX/65S;->k:LX/657;

    move-object v2, v2

    .line 1044969
    iget-object v2, v2, LX/657;->a:LX/64R;

    invoke-virtual {p1, v2}, LX/64R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, LX/65S;->h:Z

    if-nez v2, :cond_1

    .line 1044970
    invoke-virtual {p2, v0}, LX/65W;->a(LX/65S;)V

    .line 1044971
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/65S;)V
    .locals 3

    .prologue
    .line 1044959
    sget-boolean v0, LX/64c;->c:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1044960
    :cond_0
    iget-boolean v0, p0, LX/64c;->b:Z

    if-nez v0, :cond_1

    .line 1044961
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/64c;->b:Z

    .line 1044962
    sget-object v0, LX/64c;->d:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/64c;->g:Ljava/lang/Runnable;

    const v2, -0x2877aa07

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1044963
    :cond_1
    iget-object v0, p0, LX/64c;->h:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 1044964
    return-void
.end method

.method public final b(LX/65S;)Z
    .locals 1

    .prologue
    .line 1044952
    sget-boolean v0, LX/64c;->c:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1044953
    :cond_0
    iget-boolean v0, p1, LX/65S;->h:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/64c;->e:I

    if-nez v0, :cond_2

    .line 1044954
    :cond_1
    iget-object v0, p0, LX/64c;->h:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 1044955
    const/4 v0, 0x1

    .line 1044956
    :goto_0
    return v0

    .line 1044957
    :cond_2
    const v0, -0x71d80a96

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1044958
    const/4 v0, 0x0

    goto :goto_0
.end method
