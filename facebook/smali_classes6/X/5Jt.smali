.class public final LX/5Jt;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Jt;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Jr;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Ju;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 897873
    const/4 v0, 0x0

    sput-object v0, LX/5Jt;->a:LX/5Jt;

    .line 897874
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Jt;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 897870
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 897871
    new-instance v0, LX/5Ju;

    invoke-direct {v0}, LX/5Ju;-><init>()V

    iput-object v0, p0, LX/5Jt;->c:LX/5Ju;

    .line 897872
    return-void
.end method

.method public static a(LX/1De;II)LX/5Jr;
    .locals 2

    .prologue
    .line 897834
    new-instance v0, LX/5Js;

    invoke-direct {v0}, LX/5Js;-><init>()V

    .line 897835
    sget-object v1, LX/5Jt;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5Jr;

    .line 897836
    if-nez v1, :cond_0

    .line 897837
    new-instance v1, LX/5Jr;

    invoke-direct {v1}, LX/5Jr;-><init>()V

    .line 897838
    :cond_0
    invoke-static {v1, p0, p1, p2, v0}, LX/5Jr;->a$redex0(LX/5Jr;LX/1De;IILX/5Js;)V

    .line 897839
    move-object v0, v1

    .line 897840
    return-object v0
.end method

.method public static c(LX/1De;)LX/5Jr;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 897869
    invoke-static {p0, v0, v0}, LX/5Jt;->a(LX/1De;II)LX/5Jr;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/5Jt;
    .locals 2

    .prologue
    .line 897865
    const-class v1, LX/5Jt;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Jt;->a:LX/5Jt;

    if-nez v0, :cond_0

    .line 897866
    new-instance v0, LX/5Jt;

    invoke-direct {v0}, LX/5Jt;-><init>()V

    sput-object v0, LX/5Jt;->a:LX/5Jt;

    .line 897867
    :cond_0
    sget-object v0, LX/5Jt;->a:LX/5Jt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 897868
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897863
    invoke-static {}, LX/1dS;->b()V

    .line 897864
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1b847166

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 897857
    const/16 v2, 0x32

    .line 897858
    invoke-static {p3}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p4}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 897859
    iput v2, p5, LX/1no;->a:I

    .line 897860
    iput v2, p5, LX/1no;->b:I

    .line 897861
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x6596fd4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 897862
    :cond_0
    invoke-static {p3, p4, p5}, LX/1oC;->a(IILX/1no;)V

    goto :goto_0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897855
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 897856
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 897854
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 897849
    check-cast p3, LX/5Js;

    .line 897850
    check-cast p2, Landroid/widget/ProgressBar;

    iget v0, p3, LX/5Js;->a:I

    .line 897851
    if-eqz v0, :cond_0

    .line 897852
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 897853
    :cond_0
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 897848
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 897843
    check-cast p3, LX/5Js;

    .line 897844
    check-cast p2, Landroid/widget/ProgressBar;

    iget v0, p3, LX/5Js;->a:I

    .line 897845
    if-eqz v0, :cond_0

    .line 897846
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 897847
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 897842
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 897841
    const/16 v0, 0xf

    return v0
.end method
