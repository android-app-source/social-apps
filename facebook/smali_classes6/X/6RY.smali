.class public final enum LX/6RY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6RY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6RY;

.field public static final enum FUTURE:LX/6RY;

.field public static final enum NEXT_WEEK:LX/6RY;

.field public static final enum PAST:LX/6RY;

.field public static final enum THIS_WEEK:LX/6RY;

.field public static final enum TODAY:LX/6RY;

.field public static final enum TOMORROW:LX/6RY;

.field public static final enum YESTERDAY:LX/6RY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1089849
    new-instance v0, LX/6RY;

    const-string v1, "PAST"

    invoke-direct {v0, v1, v3}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->PAST:LX/6RY;

    .line 1089850
    new-instance v0, LX/6RY;

    const-string v1, "YESTERDAY"

    invoke-direct {v0, v1, v4}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->YESTERDAY:LX/6RY;

    .line 1089851
    new-instance v0, LX/6RY;

    const-string v1, "TODAY"

    invoke-direct {v0, v1, v5}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->TODAY:LX/6RY;

    .line 1089852
    new-instance v0, LX/6RY;

    const-string v1, "TOMORROW"

    invoke-direct {v0, v1, v6}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->TOMORROW:LX/6RY;

    .line 1089853
    new-instance v0, LX/6RY;

    const-string v1, "THIS_WEEK"

    invoke-direct {v0, v1, v7}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->THIS_WEEK:LX/6RY;

    .line 1089854
    new-instance v0, LX/6RY;

    const-string v1, "NEXT_WEEK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->NEXT_WEEK:LX/6RY;

    .line 1089855
    new-instance v0, LX/6RY;

    const-string v1, "FUTURE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6RY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6RY;->FUTURE:LX/6RY;

    .line 1089856
    const/4 v0, 0x7

    new-array v0, v0, [LX/6RY;

    sget-object v1, LX/6RY;->PAST:LX/6RY;

    aput-object v1, v0, v3

    sget-object v1, LX/6RY;->YESTERDAY:LX/6RY;

    aput-object v1, v0, v4

    sget-object v1, LX/6RY;->TODAY:LX/6RY;

    aput-object v1, v0, v5

    sget-object v1, LX/6RY;->TOMORROW:LX/6RY;

    aput-object v1, v0, v6

    sget-object v1, LX/6RY;->THIS_WEEK:LX/6RY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6RY;->NEXT_WEEK:LX/6RY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6RY;->FUTURE:LX/6RY;

    aput-object v2, v0, v1

    sput-object v0, LX/6RY;->$VALUES:[LX/6RY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1089848
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6RY;
    .locals 1

    .prologue
    .line 1089846
    const-class v0, LX/6RY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6RY;

    return-object v0
.end method

.method public static values()[LX/6RY;
    .locals 1

    .prologue
    .line 1089847
    sget-object v0, LX/6RY;->$VALUES:[LX/6RY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6RY;

    return-object v0
.end method
