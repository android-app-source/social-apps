.class public LX/54s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/54r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 828621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private h(LX/54q;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 828597
    invoke-interface {p1}, LX/54q;->getUseCompatPadding()Z

    move-result v0

    if-nez v0, :cond_0

    .line 828598
    invoke-interface {p1, v1, v1, v1, v1}, LX/54q;->a(IIII)V

    .line 828599
    :goto_0
    return-void

    .line 828600
    :cond_0
    invoke-virtual {p0, p1}, LX/54s;->a(LX/54q;)F

    move-result v0

    .line 828601
    invoke-virtual {p0, p1}, LX/54s;->d(LX/54q;)F

    move-result v1

    .line 828602
    invoke-interface {p1}, LX/54q;->getPreventCornerOverlap()Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/54z;->b(FFZ)F

    move-result v2

    float-to-double v2, v2

    .line 828603
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 828604
    invoke-interface {p1}, LX/54q;->getPreventCornerOverlap()Z

    move-result v3

    invoke-static {v0, v1, v3}, LX/54z;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    .line 828605
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 828606
    invoke-interface {p1, v2, v0, v2, v0}, LX/54q;->a(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/54q;)F
    .locals 1

    .prologue
    .line 828607
    invoke-interface {p1}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54y;

    check-cast v0, LX/54y;

    .line 828608
    iget p0, v0, LX/54y;->e:F

    move v0, p0

    .line 828609
    return v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 828610
    return-void
.end method

.method public final a(LX/54q;F)V
    .locals 1

    .prologue
    .line 828611
    invoke-interface {p1}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54y;

    check-cast v0, LX/54y;

    .line 828612
    iget p0, v0, LX/54y;->a:F

    cmpl-float p0, p2, p0

    if-nez p0, :cond_0

    .line 828613
    :goto_0
    return-void

    .line 828614
    :cond_0
    iput p2, v0, LX/54y;->a:F

    .line 828615
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/54y;->a(LX/54y;Landroid/graphics/Rect;)V

    .line 828616
    invoke-virtual {v0}, LX/54y;->invalidateSelf()V

    goto :goto_0
.end method

.method public final a(LX/54q;I)V
    .locals 1

    .prologue
    .line 828617
    invoke-interface {p1}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54y;

    check-cast v0, LX/54y;

    .line 828618
    iget-object p0, v0, LX/54y;->b:Landroid/graphics/Paint;

    invoke-virtual {p0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 828619
    invoke-virtual {v0}, LX/54y;->invalidateSelf()V

    .line 828620
    return-void
.end method

.method public final a(LX/54q;Landroid/content/Context;IFFF)V
    .locals 2

    .prologue
    .line 828589
    new-instance v0, LX/54y;

    invoke-direct {v0, p3, p4}, LX/54y;-><init>(IF)V

    .line 828590
    invoke-interface {p1, v0}, LX/54q;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, p1

    .line 828591
    check-cast v0, Landroid/view/View;

    .line 828592
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 828593
    invoke-virtual {v0, p5}, Landroid/view/View;->setElevation(F)V

    .line 828594
    invoke-virtual {p0, p1, p6}, LX/54s;->b(LX/54q;F)V

    .line 828595
    return-void
.end method

.method public final b(LX/54q;)F
    .locals 2

    .prologue
    .line 828596
    invoke-virtual {p0, p1}, LX/54s;->d(LX/54q;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final b(LX/54q;F)V
    .locals 4

    .prologue
    .line 828579
    invoke-interface {p1}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54y;

    check-cast v0, LX/54y;

    .line 828580
    invoke-interface {p1}, LX/54q;->getUseCompatPadding()Z

    move-result v1

    invoke-interface {p1}, LX/54q;->getPreventCornerOverlap()Z

    move-result v2

    .line 828581
    iget v3, v0, LX/54y;->e:F

    cmpl-float v3, p2, v3

    if-nez v3, :cond_0

    iget-boolean v3, v0, LX/54y;->f:Z

    if-ne v3, v1, :cond_0

    iget-boolean v3, v0, LX/54y;->g:Z

    if-ne v3, v2, :cond_0

    .line 828582
    :goto_0
    invoke-direct {p0, p1}, LX/54s;->h(LX/54q;)V

    .line 828583
    return-void

    .line 828584
    :cond_0
    iput p2, v0, LX/54y;->e:F

    .line 828585
    iput-boolean v1, v0, LX/54y;->f:Z

    .line 828586
    iput-boolean v2, v0, LX/54y;->g:Z

    .line 828587
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/54y;->a(LX/54y;Landroid/graphics/Rect;)V

    .line 828588
    invoke-virtual {v0}, LX/54y;->invalidateSelf()V

    goto :goto_0
.end method

.method public final c(LX/54q;)F
    .locals 2

    .prologue
    .line 828578
    invoke-virtual {p0, p1}, LX/54s;->d(LX/54q;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final c(LX/54q;F)V
    .locals 0

    .prologue
    .line 828576
    check-cast p1, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    .line 828577
    return-void
.end method

.method public final d(LX/54q;)F
    .locals 1

    .prologue
    .line 828568
    invoke-interface {p1}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54y;

    check-cast v0, LX/54y;

    .line 828569
    iget p0, v0, LX/54y;->a:F

    move v0, p0

    .line 828570
    return v0
.end method

.method public final e(LX/54q;)F
    .locals 1

    .prologue
    .line 828575
    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getElevation()F

    move-result v0

    return v0
.end method

.method public final f(LX/54q;)V
    .locals 1

    .prologue
    .line 828573
    invoke-virtual {p0, p1}, LX/54s;->a(LX/54q;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/54s;->b(LX/54q;F)V

    .line 828574
    return-void
.end method

.method public final g(LX/54q;)V
    .locals 1

    .prologue
    .line 828571
    invoke-virtual {p0, p1}, LX/54s;->a(LX/54q;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/54s;->b(LX/54q;F)V

    .line 828572
    return-void
.end method
