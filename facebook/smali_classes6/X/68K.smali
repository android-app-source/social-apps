.class public final LX/68K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/67g;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/31i;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:I

.field public d:Z

.field public e:LX/68K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68K",
            "<TT;>;"
        }
    .end annotation
.end field

.field public f:LX/68K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68K",
            "<TT;>;"
        }
    .end annotation
.end field

.field public g:LX/68K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68K",
            "<TT;>;"
        }
    .end annotation
.end field

.field public h:LX/68K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68K",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/31i;I)V
    .locals 1

    .prologue
    .line 1054768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054769
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/68K;->b:Ljava/util/ArrayList;

    .line 1054770
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/68K;->d:Z

    .line 1054771
    iput-object p1, p0, LX/68K;->a:LX/31i;

    .line 1054772
    iput p2, p0, LX/68K;->c:I

    .line 1054773
    return-void
.end method

.method public static synthetic a(LX/68K;)LX/31i;
    .locals 1

    .prologue
    .line 1054767
    iget-object v0, p0, LX/68K;->a:LX/31i;

    return-object v0
.end method

.method public static synthetic a(LX/68K;LX/68K;)LX/68K;
    .locals 0

    .prologue
    .line 1054766
    iput-object p1, p0, LX/68K;->f:LX/68K;

    return-object p1
.end method

.method public static synthetic a(LX/68K;Z)Z
    .locals 0

    .prologue
    .line 1054765
    iput-boolean p1, p0, LX/68K;->d:Z

    return p1
.end method

.method public static synthetic b(LX/68K;LX/68K;)LX/68K;
    .locals 0

    .prologue
    .line 1054774
    iput-object p1, p0, LX/68K;->g:LX/68K;

    return-object p1
.end method

.method public static synthetic b(LX/68K;)Z
    .locals 1

    .prologue
    .line 1054764
    iget-boolean v0, p0, LX/68K;->d:Z

    return v0
.end method

.method public static synthetic c(LX/68K;LX/68K;)LX/68K;
    .locals 0

    .prologue
    .line 1054763
    iput-object p1, p0, LX/68K;->e:LX/68K;

    return-object p1
.end method

.method public static synthetic c(LX/68K;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1054762
    iget-object v0, p0, LX/68K;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic d(LX/68K;)I
    .locals 1

    .prologue
    .line 1054761
    iget v0, p0, LX/68K;->c:I

    return v0
.end method

.method public static synthetic d(LX/68K;LX/68K;)LX/68K;
    .locals 0

    .prologue
    .line 1054760
    iput-object p1, p0, LX/68K;->h:LX/68K;

    return-object p1
.end method
