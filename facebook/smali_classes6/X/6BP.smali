.class public LX/6BP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile b:LX/6BP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1061984
    const-class v0, LX/6BP;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6BP;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1061983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/6BP;
    .locals 3

    .prologue
    .line 1061985
    sget-object v0, LX/6BP;->b:LX/6BP;

    if-nez v0, :cond_1

    .line 1061986
    const-class v1, LX/6BP;

    monitor-enter v1

    .line 1061987
    :try_start_0
    sget-object v0, LX/6BP;->b:LX/6BP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1061988
    if-eqz v2, :cond_0

    .line 1061989
    :try_start_1
    new-instance v0, LX/6BP;

    invoke-direct {v0}, LX/6BP;-><init>()V

    .line 1061990
    move-object v0, v0

    .line 1061991
    sput-object v0, LX/6BP;->b:LX/6BP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1061992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1061993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1061994
    :cond_1
    sget-object v0, LX/6BP;->b:LX/6BP;

    return-object v0

    .line 1061995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1061996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1061951
    check-cast p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    .line 1061952
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    move v0, v0

    .line 1061953
    if-eqz v0, :cond_0

    const-string v0, "POST"

    .line 1061954
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1061955
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "m_source"

    .line 1061956
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1061957
    if-eqz v1, :cond_1

    .line 1061958
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1061959
    :goto_1
    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061960
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1061961
    iget-object v4, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1061962
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/subscribed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1061963
    iput-object v3, v1, LX/14O;->d:Ljava/lang/String;

    .line 1061964
    move-object v1, v1

    .line 1061965
    iput-object v0, v1, LX/14O;->c:Ljava/lang/String;

    .line 1061966
    move-object v0, v1

    .line 1061967
    sget-object v1, LX/6BP;->a:Ljava/lang/String;

    .line 1061968
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1061969
    move-object v0, v0

    .line 1061970
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 1061971
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1061972
    move-object v0, v0

    .line 1061973
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 1061974
    move-object v0, v0

    .line 1061975
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    .line 1061976
    return-object v0

    .line 1061977
    :cond_0
    const-string v0, "DELETE"

    goto :goto_0

    .line 1061978
    :cond_1
    const-string v1, "SOURCE_NOT_FOUND"

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1061979
    check-cast p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    .line 1061980
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1061981
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    move v0, v0

    .line 1061982
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
