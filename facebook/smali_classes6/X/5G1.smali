.class public final LX/5G1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 886789
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 886790
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886791
    :goto_0
    return v1

    .line 886792
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886793
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 886794
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 886795
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 886796
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 886797
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 886798
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 886799
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 886800
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 886801
    invoke-static {p0, p1}, LX/5G0;->b(LX/15w;LX/186;)I

    move-result v2

    .line 886802
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 886803
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 886804
    goto :goto_1

    .line 886805
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 886806
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 886807
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 886808
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 886809
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886810
    if-eqz v0, :cond_1

    .line 886811
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886812
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 886813
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 886814
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/5G0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 886815
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 886816
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 886817
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 886818
    return-void
.end method
