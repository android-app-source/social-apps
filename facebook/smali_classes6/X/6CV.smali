.class public final LX/6CV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CO;


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

.field public final synthetic b:LX/6CW;


# direct methods
.method public constructor <init>(LX/6CW;Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1063875
    iput-object p1, p0, LX/6CV;->b:LX/6CW;

    iput-object p2, p0, LX/6CV;->a:Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1063868
    iget-object v0, p0, LX/6CV;->a:Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    iget-object v1, p0, LX/6CV;->a:Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 1063869
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1063870
    const-string p0, "callbackID"

    invoke-virtual {v3, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063871
    const-string p0, "callback_result"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1063872
    move-object v1, v3

    .line 1063873
    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1063874
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1063866
    iget-object v0, p0, LX/6CV;->a:Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_UPDATE_CART_FAILED:LX/6Cy;

    invoke-virtual {v1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1063867
    return-void
.end method
