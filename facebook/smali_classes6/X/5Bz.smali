.class public final LX/5Bz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 866086
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 866087
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 866088
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 866089
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 866090
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 866091
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 866092
    :goto_1
    move v1, v2

    .line 866093
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 866094
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 866095
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 866096
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 866097
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 866098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 866099
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 866100
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v3

    goto :goto_2

    .line 866101
    :cond_2
    const-string v11, "length"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 866102
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v3

    goto :goto_2

    .line 866103
    :cond_3
    const-string v11, "offset"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 866104
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_2

    .line 866105
    :cond_4
    const-string v11, "sample_entities"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 866106
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 866107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_5

    .line 866108
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_5

    .line 866109
    invoke-static {p0, p1}, LX/5By;->b(LX/15w;LX/186;)I

    move-result v10

    .line 866110
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 866111
    :cond_5
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 866112
    goto :goto_2

    .line 866113
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 866114
    :cond_7
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 866115
    if-eqz v5, :cond_8

    .line 866116
    invoke-virtual {p1, v2, v9, v2}, LX/186;->a(III)V

    .line 866117
    :cond_8
    if-eqz v4, :cond_9

    .line 866118
    invoke-virtual {p1, v3, v8, v2}, LX/186;->a(III)V

    .line 866119
    :cond_9
    if-eqz v1, :cond_a

    .line 866120
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7, v2}, LX/186;->a(III)V

    .line 866121
    :cond_a
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 866122
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_2
.end method
