.class public final LX/54S;
.super LX/52r;
.source ""


# instance fields
.field private final a:LX/54i;

.field private final b:LX/54U;


# direct methods
.method public constructor <init>(LX/54U;)V
    .locals 1

    .prologue
    .line 828016
    invoke-direct {p0}, LX/52r;-><init>()V

    .line 828017
    new-instance v0, LX/54i;

    invoke-direct {v0}, LX/54i;-><init>()V

    iput-object v0, p0, LX/54S;->a:LX/54i;

    .line 828018
    iput-object p1, p0, LX/54S;->b:LX/54U;

    .line 828019
    return-void
.end method


# virtual methods
.method public final a(LX/0vR;)LX/0za;
    .locals 3

    .prologue
    .line 828020
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, LX/54S;->a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 2

    .prologue
    .line 828021
    iget-object v0, p0, LX/54S;->a:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828022
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 828023
    :goto_0
    return-object v0

    .line 828024
    :cond_0
    iget-object v0, p0, LX/54S;->b:LX/54U;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/53a;->b(LX/0vR;JLjava/util/concurrent/TimeUnit;)Lrx/internal/schedulers/ScheduledAction;

    move-result-object v0

    .line 828025
    iget-object v1, p0, LX/54S;->a:LX/54i;

    invoke-virtual {v1, v0}, LX/54i;->a(LX/0za;)V

    .line 828026
    iget-object v1, p0, LX/54S;->a:LX/54i;

    invoke-virtual {v0, v1}, Lrx/internal/schedulers/ScheduledAction;->a(LX/54i;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 828027
    iget-object v0, p0, LX/54S;->a:LX/54i;

    invoke-virtual {v0}, LX/54i;->b()V

    .line 828028
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828029
    iget-object v0, p0, LX/54S;->a:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    return v0
.end method
