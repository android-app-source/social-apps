.class public final LX/5JZ;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5JZ;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5JX;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Jd;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 897165
    const/4 v0, 0x0

    sput-object v0, LX/5JZ;->a:LX/5JZ;

    .line 897166
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5JZ;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 897272
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 897273
    new-instance v0, LX/5Jd;

    invoke-direct {v0}, LX/5Jd;-><init>()V

    iput-object v0, p0, LX/5JZ;->c:LX/5Jd;

    .line 897274
    return-void
.end method

.method public static c(LX/1De;)LX/1dQ;
    .locals 1

    .prologue
    .line 897266
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 897267
    if-nez v0, :cond_0

    .line 897268
    const/4 v0, 0x0

    .line 897269
    :goto_0
    return-object v0

    .line 897270
    :cond_0
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 897271
    check-cast v0, LX/5JY;

    iget-object v0, v0, LX/5JY;->z:LX/1dQ;

    goto :goto_0
.end method

.method public static declared-synchronized q()LX/5JZ;
    .locals 2

    .prologue
    .line 897262
    const-class v1, LX/5JZ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5JZ;->a:LX/5JZ;

    if-nez v0, :cond_0

    .line 897263
    new-instance v0, LX/5JZ;

    invoke-direct {v0}, LX/5JZ;-><init>()V

    sput-object v0, LX/5JZ;->a:LX/5JZ;

    .line 897264
    :cond_0
    sget-object v0, LX/5JZ;->a:LX/5JZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 897265
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897260
    invoke-static {}, LX/1dS;->b()V

    .line 897261
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 31

    .prologue
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const v3, -0x311450ff

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v30

    .line 897257
    check-cast p6, LX/5JY;

    .line 897258
    move-object/from16 v0, p6

    iget-object v5, v0, LX/5JY;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iget-object v6, v0, LX/5JY;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iget-object v7, v0, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p6

    iget v8, v0, LX/5JY;->d:I

    move-object/from16 v0, p6

    iget v9, v0, LX/5JY;->e:I

    move-object/from16 v0, p6

    iget v10, v0, LX/5JY;->f:I

    move-object/from16 v0, p6

    iget v11, v0, LX/5JY;->g:F

    move-object/from16 v0, p6

    iget v12, v0, LX/5JY;->h:F

    move-object/from16 v0, p6

    iget v13, v0, LX/5JY;->i:F

    move-object/from16 v0, p6

    iget v14, v0, LX/5JY;->j:I

    move-object/from16 v0, p6

    iget-boolean v15, v0, LX/5JY;->k:Z

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->l:I

    move/from16 v16, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/5JY;->m:Landroid/content/res/ColorStateList;

    move-object/from16 v17, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->n:I

    move/from16 v18, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/5JY;->o:Landroid/content/res/ColorStateList;

    move-object/from16 v19, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->p:I

    move/from16 v20, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->q:I

    move/from16 v21, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->r:I

    move/from16 v22, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->s:F

    move/from16 v23, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->t:F

    move/from16 v24, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->u:I

    move/from16 v25, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/5JY;->v:Landroid/graphics/Typeface;

    move-object/from16 v26, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    move-object/from16 v27, v0

    move-object/from16 v0, p6

    iget v0, v0, LX/5JY;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p6

    iget-boolean v0, v0, LX/5JY;->y:Z

    move/from16 v29, v0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, p4

    move-object/from16 v4, p5

    invoke-static/range {v1 .. v29}, LX/5Jd;->a(LX/1De;IILX/1no;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V

    .line 897259
    const/16 v1, 0x8

    const/16 v2, 0x1f

    const v3, -0x271ea2fd

    move/from16 v0, v30

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897255
    new-instance v0, LX/5Jc;

    invoke-direct {v0, p1}, LX/5Jc;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 897256
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 897184
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 897185
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 897186
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 897187
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 897188
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 897189
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 897190
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 897191
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 897192
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 897193
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v11

    .line 897194
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v12

    .line 897195
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v13

    .line 897196
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v14

    .line 897197
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v15

    .line 897198
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v16

    .line 897199
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v17

    .line 897200
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v18

    move-object/from16 v1, p1

    .line 897201
    invoke-static/range {v1 .. v18}, LX/5Jd;->a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 897202
    check-cast p2, LX/5JY;

    .line 897203
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 897204
    invoke-virtual {v2}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    .line 897205
    :cond_0
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 897206
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 897207
    invoke-virtual {v3}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->t:F

    .line 897208
    :cond_1
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 897209
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 897210
    invoke-virtual {v4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->d:I

    .line 897211
    :cond_2
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 897212
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 897213
    invoke-virtual {v5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->e:I

    .line 897214
    :cond_3
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 897215
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 897216
    invoke-virtual {v6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move-object/from16 v0, p2

    iput-boolean v1, v0, LX/5JY;->k:Z

    .line 897217
    :cond_4
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 897218
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 897219
    invoke-virtual {v7}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/5JY;->a:Ljava/lang/CharSequence;

    .line 897220
    :cond_5
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 897221
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 897222
    invoke-virtual {v8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/ColorStateList;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/5JY;->m:Landroid/content/res/ColorStateList;

    .line 897223
    :cond_6
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 897224
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 897225
    invoke-virtual {v9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->p:I

    .line 897226
    :cond_7
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 897227
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 897228
    invoke-virtual {v10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->q:I

    .line 897229
    :cond_8
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 897230
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 897231
    invoke-virtual {v11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->r:I

    .line 897232
    :cond_9
    invoke-static {v11}, LX/1cy;->a(LX/1np;)V

    .line 897233
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 897234
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout$Alignment;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    .line 897235
    :cond_a
    invoke-static {v12}, LX/1cy;->a(LX/1np;)V

    .line 897236
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 897237
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->u:I

    .line 897238
    :cond_b
    invoke-static {v13}, LX/1cy;->a(LX/1np;)V

    .line 897239
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 897240
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->g:F

    .line 897241
    :cond_c
    invoke-static {v14}, LX/1cy;->a(LX/1np;)V

    .line 897242
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 897243
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->h:F

    .line 897244
    :cond_d
    invoke-static {v15}, LX/1cy;->a(LX/1np;)V

    .line 897245
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 897246
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->i:F

    .line 897247
    :cond_e
    invoke-static/range {v16 .. v16}, LX/1S3;->a(LX/1np;)V

    .line 897248
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 897249
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->j:I

    .line 897250
    :cond_f
    invoke-static/range {v17 .. v17}, LX/1S3;->a(LX/1np;)V

    .line 897251
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 897252
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p2

    iput v1, v0, LX/5JY;->x:I

    .line 897253
    :cond_10
    invoke-static/range {v18 .. v18}, LX/1S3;->a(LX/1np;)V

    .line 897254
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 897183
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 28

    .prologue
    .line 897180
    check-cast p3, LX/5JY;

    move-object/from16 v2, p2

    .line 897181
    check-cast v2, LX/5Jc;

    move-object/from16 v0, p3

    iget-object v3, v0, LX/5JY;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/5JY;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v5, v0, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p3

    iget v6, v0, LX/5JY;->d:I

    move-object/from16 v0, p3

    iget v7, v0, LX/5JY;->e:I

    move-object/from16 v0, p3

    iget v8, v0, LX/5JY;->f:I

    move-object/from16 v0, p3

    iget v9, v0, LX/5JY;->g:F

    move-object/from16 v0, p3

    iget v10, v0, LX/5JY;->h:F

    move-object/from16 v0, p3

    iget v11, v0, LX/5JY;->i:F

    move-object/from16 v0, p3

    iget v12, v0, LX/5JY;->j:I

    move-object/from16 v0, p3

    iget-boolean v13, v0, LX/5JY;->k:Z

    move-object/from16 v0, p3

    iget v14, v0, LX/5JY;->l:I

    move-object/from16 v0, p3

    iget-object v15, v0, LX/5JY;->m:Landroid/content/res/ColorStateList;

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->n:I

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/5JY;->o:Landroid/content/res/ColorStateList;

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->p:I

    move/from16 v18, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->q:I

    move/from16 v19, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->r:I

    move/from16 v20, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->s:F

    move/from16 v21, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->t:F

    move/from16 v22, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->u:I

    move/from16 v23, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/5JY;->v:Landroid/graphics/Typeface;

    move-object/from16 v24, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    move-object/from16 v25, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/5JY;->x:I

    move/from16 v26, v0

    move-object/from16 v0, p3

    iget-boolean v0, v0, LX/5JY;->y:Z

    move/from16 v27, v0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v27}, LX/5Jd;->a(LX/1De;LX/5Jc;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V

    .line 897182
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 897179
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 897175
    check-cast p2, LX/5Jc;

    .line 897176
    const/4 p0, 0x0

    .line 897177
    iput-object p0, p2, LX/5Jc;->b:LX/1dQ;

    .line 897178
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 897172
    check-cast p2, LX/5Jc;

    .line 897173
    iget-object p0, p2, LX/5Jc;->a:Landroid/text/TextWatcher;

    invoke-virtual {p2, p0}, LX/5Jc;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 897174
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 897169
    check-cast p2, LX/5Jc;

    .line 897170
    iget-object p0, p2, LX/5Jc;->a:Landroid/text/TextWatcher;

    invoke-virtual {p2, p0}, LX/5Jc;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 897171
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 897168
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 897167
    const/16 v0, 0xf

    return v0
.end method
