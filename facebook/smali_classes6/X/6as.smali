.class public LX/6as;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1112723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/util/AttributeSet;)I
    .locals 2

    .prologue
    .line 1112698
    const-string v0, "http://schemas.android.com/apk/facebook"

    const-string v1, "map_source"

    invoke-interface {p0, v0, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1112699
    const-string v1, "facebook"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112700
    const/4 v0, 0x0

    .line 1112701
    :goto_0
    return v0

    .line 1112702
    :cond_0
    const-string v1, "google"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1112703
    const/4 v0, 0x1

    goto :goto_0

    .line 1112704
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/692;
    .locals 5

    .prologue
    .line 1112705
    if-nez p0, :cond_0

    .line 1112706
    const/4 v0, 0x0

    .line 1112707
    :goto_0
    return-object v0

    .line 1112708
    :cond_0
    new-instance v0, LX/691;

    invoke-direct {v0}, LX/691;-><init>()V

    move-object v0, v0

    .line 1112709
    iget v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    .line 1112710
    iput v1, v0, LX/691;->d:F

    .line 1112711
    move-object v0, v0

    .line 1112712
    iget-object v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    .line 1112713
    iput-object v1, v0, LX/691;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1112714
    move-object v0, v0

    .line 1112715
    iget v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    .line 1112716
    iput v1, v0, LX/691;->c:F

    .line 1112717
    move-object v0, v0

    .line 1112718
    iget v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    .line 1112719
    iput v1, v0, LX/691;->b:F

    .line 1112720
    move-object v0, v0

    .line 1112721
    new-instance v1, LX/692;

    iget-object v2, v0, LX/691;->a:Lcom/facebook/android/maps/model/LatLng;

    iget v3, v0, LX/691;->b:F

    iget v4, v0, LX/691;->c:F

    iget p0, v0, LX/691;->d:F

    invoke-direct {v1, v2, v3, v4, p0}, LX/692;-><init>(Lcom/facebook/android/maps/model/LatLng;FFF)V

    move-object v0, v1

    .line 1112722
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/maps/model/VisibleRegion;)LX/69F;
    .locals 8

    .prologue
    .line 1112724
    if-nez p0, :cond_0

    .line 1112725
    const/4 v0, 0x0

    .line 1112726
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/69F;

    iget-object v1, p0, Lcom/google/android/gms/maps/model/VisibleRegion;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/model/VisibleRegion;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v2}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/maps/model/VisibleRegion;->c:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/maps/model/VisibleRegion;->d:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v4}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/maps/model/VisibleRegion;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 1112727
    if-eqz v5, :cond_1

    iget-object v6, v5, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v6, :cond_1

    iget-object v6, v5, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    if-nez v6, :cond_2

    .line 1112728
    :cond_1
    const/4 v6, 0x0

    .line 1112729
    :goto_1
    move-object v5, v6

    .line 1112730
    invoke-direct/range {v0 .. v5}, LX/69F;-><init>(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;LX/697;)V

    goto :goto_0

    :cond_2
    new-instance v6, LX/697;

    iget-object v7, v5, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v7}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v7

    iget-object p0, v5, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {p0}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object p0

    invoke-direct {v6, v7, p0}, LX/697;-><init>(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_1
.end method

.method public static a(LX/68w;)LX/7c6;
    .locals 1

    .prologue
    .line 1112690
    if-nez p0, :cond_0

    .line 1112691
    const/4 v0, 0x0

    .line 1112692
    :goto_0
    return-object v0

    .line 1112693
    :cond_0
    iget-object v0, p0, LX/68w;->a:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1112694
    invoke-static {v0}, LX/7c7;->a(Landroid/graphics/Bitmap;)LX/7c6;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 1112695
    if-nez p0, :cond_0

    .line 1112696
    const/4 v0, 0x0

    .line 1112697
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public static a(LX/681;)Lcom/google/android/gms/maps/GoogleMapOptions;
    .locals 3

    .prologue
    .line 1112645
    if-nez p0, :cond_0

    .line 1112646
    const/4 v0, 0x0

    .line 1112647
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    .line 1112648
    iget-object v1, p0, LX/681;->a:LX/692;

    move-object v1, v1

    .line 1112649
    invoke-static {v1}, LX/6as;->a(LX/692;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    move-object v0, v0

    .line 1112650
    iget-boolean v1, p0, LX/681;->b:Z

    move v1, v1

    .line 1112651
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->g:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112652
    iget v1, p0, LX/681;->c:I

    move v1, v1

    .line 1112653
    iput v1, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->d:I

    move-object v0, v0

    .line 1112654
    iget-boolean v1, p0, LX/681;->e:Z

    move v1, v1

    .line 1112655
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->h:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112656
    iget-boolean v1, p0, LX/681;->d:Z

    move v1, v1

    .line 1112657
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->k:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112658
    iget-boolean v1, p0, LX/681;->f:Z

    move v1, v1

    .line 1112659
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->j:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112660
    iget-boolean v1, p0, LX/681;->g:Z

    move v1, v1

    .line 1112661
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->c:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112662
    iget-boolean v1, p0, LX/681;->h:Z

    move v1, v1

    .line 1112663
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->b:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112664
    iget-boolean v1, p0, LX/681;->i:Z

    move v1, v1

    .line 1112665
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->f:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112666
    iget-boolean v1, p0, LX/681;->j:Z

    move v1, v1

    .line 1112667
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/maps/GoogleMapOptions;->i:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1112668
    goto :goto_0
.end method

.method public static a(LX/692;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 3

    .prologue
    .line 1112686
    if-nez p0, :cond_0

    .line 1112687
    const/4 v0, 0x0

    .line 1112688
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget v1, p0, LX/692;->b:F

    const/4 p0, 0x0

    new-instance v2, Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-direct {v2, v0, v1, p0, p0}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    move-object v0, v2

    .line 1112689
    goto :goto_0
.end method

.method public static a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 6

    .prologue
    .line 1112669
    if-nez p0, :cond_0

    .line 1112670
    const/4 v0, 0x0

    .line 1112671
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p0, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v4, p0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public static a(LX/697;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 3

    .prologue
    .line 1112672
    if-nez p0, :cond_0

    .line 1112673
    const/4 v0, 0x0

    .line 1112674
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v1, p0, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v1}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-object v2, p0, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v2}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public static b(Landroid/util/AttributeSet;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1112675
    const-string v0, "http://schemas.android.com/apk/facebook"

    const-string v1, "report_button_position"

    invoke-interface {p0, v0, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1112676
    const-string v1, "bottom_left"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112677
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1112678
    :goto_0
    return-object v0

    .line 1112679
    :cond_0
    const-string v1, "top_left"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1112680
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1112681
    :cond_1
    const-string v1, "top_right"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1112682
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1112683
    :cond_2
    const-string v1, "bottom_right"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1112684
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1112685
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
