.class public final LX/62I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/listview/SplitHideableListView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/listview/SplitHideableListView;)V
    .locals 0

    .prologue
    .line 1041355
    iput-object p1, p0, LX/62I;->a:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1041356
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1041357
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 1041358
    iget-object v0, p0, LX/62I;->a:Lcom/facebook/widget/listview/SplitHideableListView;

    const/4 v1, 0x0

    .line 1041359
    iput-boolean v1, v0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041360
    check-cast p1, LX/2qr;

    .line 1041361
    iget-object v0, p1, LX/2qr;->b:Landroid/view/View;

    new-instance v1, Lcom/facebook/widget/listview/SplitHideableListView$4$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/widget/listview/SplitHideableListView$4$1;-><init>(LX/62I;LX/2qr;)V

    invoke-virtual {p1}, LX/2qr;->getDuration()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1041362
    return-void
.end method
