.class public LX/5uZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5uY;

.field public b:LX/5ua;


# direct methods
.method public constructor <init>(LX/5uY;)V
    .locals 1

    .prologue
    .line 1019472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1019473
    const/4 v0, 0x0

    iput-object v0, p0, LX/5uZ;->b:LX/5ua;

    .line 1019474
    iput-object p1, p0, LX/5uZ;->a:LX/5uY;

    .line 1019475
    iget-object v0, p0, LX/5uZ;->a:LX/5uY;

    .line 1019476
    iput-object p0, v0, LX/5uY;->i:LX/5uZ;

    .line 1019477
    return-void
.end method

.method public static a([FI)F
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1019468
    const/4 v1, 0x0

    move v2, v0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 1019469
    aget v3, p0, v1

    add-float/2addr v2, v3

    .line 1019470
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1019471
    :cond_0
    if-lez p1, :cond_1

    int-to-float v0, p1

    div-float v0, v2, v0

    :cond_1
    return v0
.end method


# virtual methods
.method public final e()V
    .locals 3

    .prologue
    .line 1019459
    iget-object v0, p0, LX/5uZ;->a:LX/5uY;

    .line 1019460
    iget-boolean v1, v0, LX/5uY;->a:Z

    if-nez v1, :cond_0

    .line 1019461
    :goto_0
    return-void

    .line 1019462
    :cond_0
    invoke-static {v0}, LX/5uY;->k(LX/5uY;)V

    .line 1019463
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 1019464
    iget-object v2, v0, LX/5uY;->e:[F

    iget-object p0, v0, LX/5uY;->g:[F

    aget p0, p0, v1

    aput p0, v2, v1

    .line 1019465
    iget-object v2, v0, LX/5uY;->f:[F

    iget-object p0, v0, LX/5uY;->h:[F

    aget p0, p0, v1

    aput p0, v2, v1

    .line 1019466
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1019467
    :cond_1
    invoke-static {v0}, LX/5uY;->j(LX/5uY;)V

    goto :goto_0
.end method

.method public final f()F
    .locals 2

    .prologue
    .line 1019454
    iget-object v0, p0, LX/5uZ;->a:LX/5uY;

    .line 1019455
    iget-object v1, v0, LX/5uY;->e:[F

    move-object v0, v1

    .line 1019456
    iget-object v1, p0, LX/5uZ;->a:LX/5uY;

    .line 1019457
    iget p0, v1, LX/5uY;->b:I

    move v1, p0

    .line 1019458
    invoke-static {v0, v1}, LX/5uZ;->a([FI)F

    move-result v0

    return v0
.end method

.method public final g()F
    .locals 2

    .prologue
    .line 1019440
    iget-object v0, p0, LX/5uZ;->a:LX/5uY;

    .line 1019441
    iget-object v1, v0, LX/5uY;->f:[F

    move-object v0, v1

    .line 1019442
    iget-object v1, p0, LX/5uZ;->a:LX/5uY;

    .line 1019443
    iget p0, v1, LX/5uY;->b:I

    move v1, p0

    .line 1019444
    invoke-static {v0, v1}, LX/5uZ;->a([FI)F

    move-result v0

    return v0
.end method

.method public final h()F
    .locals 3

    .prologue
    .line 1019445
    iget-object v0, p0, LX/5uZ;->a:LX/5uY;

    .line 1019446
    iget-object v1, v0, LX/5uY;->g:[F

    move-object v0, v1

    .line 1019447
    iget-object v1, p0, LX/5uZ;->a:LX/5uY;

    .line 1019448
    iget v2, v1, LX/5uY;->b:I

    move v1, v2

    .line 1019449
    invoke-static {v0, v1}, LX/5uZ;->a([FI)F

    move-result v0

    iget-object v1, p0, LX/5uZ;->a:LX/5uY;

    .line 1019450
    iget-object v2, v1, LX/5uY;->e:[F

    move-object v1, v2

    .line 1019451
    iget-object v2, p0, LX/5uZ;->a:LX/5uY;

    .line 1019452
    iget p0, v2, LX/5uY;->b:I

    move v2, p0

    .line 1019453
    invoke-static {v1, v2}, LX/5uZ;->a([FI)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method
