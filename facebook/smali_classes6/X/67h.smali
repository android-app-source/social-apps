.class public LX/67h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67g;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/android/maps/ClusterItem;",
        ">",
        "Ljava/lang/Object;",
        "LX/67g;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:[LX/682;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/682",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:I

.field private c:Lcom/facebook/android/maps/model/LatLng;

.field private d:Z

.field private final e:[D

.field private final f:[D

.field private g:I

.field private h:Z

.field private i:Z

.field private j:D

.field private k:D

.field private l:D

.field private m:D

.field private n:LX/697;

.field private o:Z

.field public p:LX/67h;

.field public q:LX/67m;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1053395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1053396
    iput-boolean v1, p0, LX/67h;->d:Z

    .line 1053397
    new-array v0, v2, [D

    iput-object v0, p0, LX/67h;->e:[D

    .line 1053398
    new-array v0, v2, [D

    iput-object v0, p0, LX/67h;->f:[D

    .line 1053399
    iput-boolean v1, p0, LX/67h;->h:Z

    .line 1053400
    iput-boolean v1, p0, LX/67h;->i:Z

    .line 1053401
    iput-boolean v1, p0, LX/67h;->o:Z

    .line 1053402
    new-array v0, v2, [LX/682;

    check-cast v0, [LX/682;

    iput-object v0, p0, LX/67h;->a:[LX/682;

    .line 1053403
    iput v1, p0, LX/67h;->b:I

    return-void
.end method

.method public static a(D)D
    .locals 2

    .prologue
    .line 1053394
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-double v0, v0

    add-double/2addr v0, p0

    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(D)D
    .locals 2

    .prologue
    .line 1053393
    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/16 v0, 0x168

    :goto_0
    int-to-double v0, v0

    add-double/2addr v0, p0

    return-wide v0

    :cond_0
    const-wide v0, 0x4066800000000000L    # 180.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    const/16 v0, -0x168

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 12

    .prologue
    .line 1053364
    iget-boolean v0, p0, LX/67h;->d:Z

    if-eqz v0, :cond_0

    .line 1053365
    :goto_0
    return-void

    .line 1053366
    :cond_0
    iget v0, p0, LX/67h;->b:I

    move v8, v0

    .line 1053367
    if-nez v8, :cond_1

    .line 1053368
    sget-object v0, LX/31U;->s:LX/31U;

    const-string v1, "Cannot compute centroid of an empty cluster"

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1053369
    :cond_1
    invoke-direct {p0}, LX/67h;->f()V

    .line 1053370
    const/4 v0, 0x1

    if-ne v8, v0, :cond_2

    .line 1053371
    iget-object v0, p0, LX/67h;->e:[D

    const/4 v1, 0x0

    iget-wide v2, p0, LX/67h;->l:D

    aput-wide v2, v0, v1

    .line 1053372
    iget-object v0, p0, LX/67h;->e:[D

    const/4 v1, 0x1

    iget-wide v2, p0, LX/67h;->j:D

    aput-wide v2, v0, v1

    .line 1053373
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->d:Z

    .line 1053374
    const/4 v0, 0x0

    iput-object v0, p0, LX/67h;->c:Lcom/facebook/android/maps/model/LatLng;

    goto :goto_0

    .line 1053375
    :cond_2
    iget-wide v0, p0, LX/67h;->l:D

    iget-wide v2, p0, LX/67h;->m:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_4

    const/4 v0, 0x1

    .line 1053376
    :goto_1
    const-wide/16 v4, 0x0

    .line 1053377
    const-wide/16 v2, 0x0

    .line 1053378
    const/4 v1, 0x0

    :goto_2
    iget v6, p0, LX/67h;->b:I

    if-ge v1, v6, :cond_5

    .line 1053379
    iget-object v6, p0, LX/67h;->a:[LX/682;

    aget-object v6, v6, v1

    iget-object v7, p0, LX/67h;->f:[D

    invoke-virtual {v6, v7}, LX/682;->a([D)V

    .line 1053380
    iget-object v6, p0, LX/67h;->f:[D

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    .line 1053381
    iget-object v9, p0, LX/67h;->f:[D

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    .line 1053382
    add-double/2addr v2, v10

    .line 1053383
    invoke-static {v6, v7}, LX/67h;->a(D)D

    move-result-wide v6

    .line 1053384
    if-eqz v0, :cond_3

    const-wide/16 v10, 0x0

    cmpg-double v9, v10, v6

    if-gtz v9, :cond_3

    iget-wide v10, p0, LX/67h;->m:D

    cmpg-double v9, v6, v10

    if-gtz v9, :cond_3

    .line 1053385
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v6, v10

    .line 1053386
    :cond_3
    add-double/2addr v4, v6

    .line 1053387
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1053388
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1053389
    :cond_5
    iget-object v0, p0, LX/67h;->e:[D

    const/4 v1, 0x0

    int-to-double v6, v8

    div-double/2addr v4, v6

    invoke-static {v4, v5}, LX/67h;->a(D)D

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 1053390
    iget-object v0, p0, LX/67h;->e:[D

    const/4 v1, 0x1

    int-to-double v4, v8

    div-double/2addr v2, v4

    aput-wide v2, v0, v1

    .line 1053391
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->d:Z

    .line 1053392
    const/4 v0, 0x0

    iput-object v0, p0, LX/67h;->c:Lcom/facebook/android/maps/model/LatLng;

    goto/16 :goto_0
.end method

.method private f()V
    .locals 15

    .prologue
    .line 1053293
    iget-boolean v0, p0, LX/67h;->o:Z

    if-eqz v0, :cond_0

    .line 1053294
    :goto_0
    return-void

    .line 1053295
    :cond_0
    iget v0, p0, LX/67h;->b:I

    if-nez v0, :cond_1

    .line 1053296
    sget-object v0, LX/31U;->s:LX/31U;

    const-string v1, "Cannot compute bounds of an empty cluster"

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1053297
    :cond_1
    iget v0, p0, LX/67h;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1053298
    iget-object v0, p0, LX/67h;->a:[LX/682;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, LX/67h;->f:[D

    invoke-virtual {v0, v1}, LX/682;->a([D)V

    .line 1053299
    iget-object v0, p0, LX/67h;->f:[D

    const/4 v1, 0x1

    aget-wide v0, v0, v1

    iput-wide v0, p0, LX/67h;->j:D

    .line 1053300
    iget-object v0, p0, LX/67h;->f:[D

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, LX/67h;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, LX/67h;->l:D

    .line 1053301
    iget-wide v0, p0, LX/67h;->j:D

    iput-wide v0, p0, LX/67h;->k:D

    .line 1053302
    iget-wide v0, p0, LX/67h;->l:D

    iput-wide v0, p0, LX/67h;->m:D

    .line 1053303
    const/4 v0, 0x0

    iput-object v0, p0, LX/67h;->n:LX/697;

    .line 1053304
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->o:Z

    goto :goto_0

    .line 1053305
    :cond_2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/67h;->j:D

    .line 1053306
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/67h;->k:D

    .line 1053307
    iget v0, p0, LX/67h;->b:I

    new-array v1, v0, [D

    .line 1053308
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, LX/67h;->b:I

    if-ge v0, v2, :cond_5

    .line 1053309
    iget-object v2, p0, LX/67h;->a:[LX/682;

    aget-object v2, v2, v0

    iget-object v3, p0, LX/67h;->f:[D

    invoke-virtual {v2, v3}, LX/682;->a([D)V

    .line 1053310
    iget-object v2, p0, LX/67h;->f:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    .line 1053311
    iget-object v4, p0, LX/67h;->f:[D

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    .line 1053312
    iget-wide v6, p0, LX/67h;->j:D

    cmpg-double v6, v4, v6

    if-gez v6, :cond_3

    .line 1053313
    iput-wide v4, p0, LX/67h;->j:D

    .line 1053314
    :cond_3
    iget-wide v6, p0, LX/67h;->k:D

    cmpl-double v6, v4, v6

    if-lez v6, :cond_4

    .line 1053315
    iput-wide v4, p0, LX/67h;->k:D

    .line 1053316
    :cond_4
    invoke-static {v2, v3}, LX/67h;->a(D)D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 1053317
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1053318
    :cond_5
    invoke-static {v1}, Ljava/util/Arrays;->sort([D)V

    .line 1053319
    iget v0, p0, LX/67h;->b:I

    add-int/lit8 v0, v0, -0x1

    aget-wide v6, v1, v0

    .line 1053320
    const/4 v0, 0x0

    aget-wide v4, v1, v0

    .line 1053321
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v8, v4, v6

    add-double/2addr v2, v8

    .line 1053322
    const/4 v0, 0x1

    :goto_2
    iget v8, p0, LX/67h;->b:I

    if-ge v0, v8, :cond_7

    .line 1053323
    add-int/lit8 v8, v0, -0x1

    aget-wide v12, v1, v8

    .line 1053324
    aget-wide v10, v1, v0

    .line 1053325
    sub-double v8, v10, v12

    .line 1053326
    cmpl-double v14, v8, v2

    if-lez v14, :cond_6

    move-wide v2, v8

    move-wide v4, v10

    move-wide v6, v12

    .line 1053327
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1053328
    :cond_7
    iput-wide v4, p0, LX/67h;->l:D

    .line 1053329
    iput-wide v6, p0, LX/67h;->m:D

    .line 1053330
    const/4 v0, 0x0

    iput-object v0, p0, LX/67h;->n:LX/697;

    .line 1053331
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->o:Z

    goto/16 :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1053360
    iget-boolean v0, p0, LX/67h;->i:Z

    if-eqz v0, :cond_0

    .line 1053361
    :goto_0
    return-void

    .line 1053362
    :cond_0
    iget-object v0, p0, LX/67h;->a:[LX/682;

    const/4 v1, 0x0

    iget v2, p0, LX/67h;->b:I

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;II)V

    .line 1053363
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->i:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 1053356
    invoke-direct {p0}, LX/67h;->e()V

    .line 1053357
    iget-object v0, p0, LX/67h;->c:Lcom/facebook/android/maps/model/LatLng;

    if-nez v0, :cond_0

    .line 1053358
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-object v1, p0, LX/67h;->e:[D

    const/4 v2, 0x1

    aget-wide v2, v1, v2

    invoke-static {v2, v3}, LX/31h;->a(D)D

    move-result-wide v2

    iget-object v1, p0, LX/67h;->e:[D

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-static {v4, v5}, LX/31h;->c(D)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, LX/67h;->c:Lcom/facebook/android/maps/model/LatLng;

    .line 1053359
    :cond_0
    iget-object v0, p0, LX/67h;->c:Lcom/facebook/android/maps/model/LatLng;

    return-object v0
.end method

.method public final a([D)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1053352
    invoke-direct {p0}, LX/67h;->e()V

    .line 1053353
    iget-object v0, p0, LX/67h;->e:[D

    aget-wide v0, v0, v2

    aput-wide v0, p1, v2

    .line 1053354
    iget-object v0, p0, LX/67h;->e:[D

    aget-wide v0, v0, v3

    aput-wide v0, p1, v3

    .line 1053355
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1053341
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 1053342
    :cond_0
    :goto_0
    return v1

    .line 1053343
    :cond_1
    instance-of v0, p1, LX/67h;

    if-eqz v0, :cond_0

    .line 1053344
    check-cast p1, LX/67h;

    .line 1053345
    iget v0, p0, LX/67h;->b:I

    iget v3, p1, LX/67h;->b:I

    if-ne v0, v3, :cond_0

    .line 1053346
    invoke-direct {p0}, LX/67h;->g()V

    .line 1053347
    invoke-direct {p1}, LX/67h;->g()V

    move v0, v1

    .line 1053348
    :goto_1
    iget v3, p0, LX/67h;->b:I

    if-ge v0, v3, :cond_2

    .line 1053349
    iget-object v3, p0, LX/67h;->a:[LX/682;

    aget-object v3, v3, v0

    iget-object v4, p1, LX/67h;->a:[LX/682;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, LX/682;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1053350
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1053351
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1053334
    iget-boolean v1, p0, LX/67h;->h:Z

    if-nez v1, :cond_1

    .line 1053335
    iput v0, p0, LX/67h;->g:I

    .line 1053336
    :goto_0
    iget v1, p0, LX/67h;->b:I

    if-ge v0, v1, :cond_0

    .line 1053337
    iget v1, p0, LX/67h;->g:I

    iget-object v2, p0, LX/67h;->a:[LX/682;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LX/682;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, LX/67h;->g:I

    .line 1053338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1053339
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67h;->h:Z

    .line 1053340
    :cond_1
    iget v0, p0, LX/67h;->g:I

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1053332
    invoke-direct {p0}, LX/67h;->g()V

    .line 1053333
    new-instance v0, LX/67f;

    invoke-direct {v0, p0}, LX/67f;-><init>(LX/67h;)V

    return-object v0
.end method
