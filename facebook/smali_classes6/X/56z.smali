.class public final LX/56z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 36

    .prologue
    .line 839363
    const/16 v29, 0x0

    .line 839364
    const/16 v28, 0x0

    .line 839365
    const-wide/16 v26, 0x0

    .line 839366
    const/16 v25, 0x0

    .line 839367
    const/16 v24, 0x0

    .line 839368
    const/16 v23, 0x0

    .line 839369
    const/16 v22, 0x0

    .line 839370
    const/16 v21, 0x0

    .line 839371
    const/16 v20, 0x0

    .line 839372
    const/16 v19, 0x0

    .line 839373
    const/16 v18, 0x0

    .line 839374
    const/16 v17, 0x0

    .line 839375
    const/16 v16, 0x0

    .line 839376
    const/4 v13, 0x0

    .line 839377
    const-wide/16 v14, 0x0

    .line 839378
    const/4 v12, 0x0

    .line 839379
    const/4 v11, 0x0

    .line 839380
    const/4 v10, 0x0

    .line 839381
    const/4 v9, 0x0

    .line 839382
    const/4 v8, 0x0

    .line 839383
    const/4 v7, 0x0

    .line 839384
    const/4 v6, 0x0

    .line 839385
    const/4 v5, 0x0

    .line 839386
    const/4 v4, 0x0

    .line 839387
    const/4 v3, 0x0

    .line 839388
    const/4 v2, 0x0

    .line 839389
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1c

    .line 839390
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 839391
    const/4 v2, 0x0

    .line 839392
    :goto_0
    return v2

    .line 839393
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_15

    .line 839394
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 839395
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 839396
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 839397
    const-string v7, "can_viewer_change_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 839398
    const/4 v2, 0x1

    .line 839399
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v31, v6

    move v6, v2

    goto :goto_1

    .line 839400
    :cond_1
    const-string v7, "connection_style"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 839401
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 839402
    :cond_2
    const-string v7, "end_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 839403
    const/4 v2, 0x1

    .line 839404
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 839405
    :cond_3
    const-string v7, "event_cover_photo"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 839406
    invoke-static/range {p0 .. p1}, LX/5En;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 839407
    :cond_4
    const-string v7, "event_members"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 839408
    invoke-static/range {p0 .. p1}, LX/5Eo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 839409
    :cond_5
    const-string v7, "event_place"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 839410
    invoke-static/range {p0 .. p1}, LX/5Fk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 839411
    :cond_6
    const-string v7, "event_watchers"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 839412
    invoke-static/range {p0 .. p1}, LX/5Ep;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 839413
    :cond_7
    const-string v7, "friendEventMembersFirst3"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 839414
    invoke-static/range {p0 .. p1}, LX/5Eu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 839415
    :cond_8
    const-string v7, "friendEventWatchersFirst3"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 839416
    invoke-static/range {p0 .. p1}, LX/5Ex;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 839417
    :cond_9
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 839418
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 839419
    :cond_a
    const-string v7, "is_all_day"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 839420
    const/4 v2, 0x1

    .line 839421
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v11, v2

    move/from16 v22, v7

    goto/16 :goto_1

    .line 839422
    :cond_b
    const-string v7, "is_canceled"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 839423
    const/4 v2, 0x1

    .line 839424
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v10, v2

    move/from16 v21, v7

    goto/16 :goto_1

    .line 839425
    :cond_c
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 839426
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 839427
    :cond_d
    const-string v7, "social_context"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 839428
    invoke-static/range {p0 .. p1}, LX/5Eq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 839429
    :cond_e
    const-string v7, "start_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 839430
    const/4 v2, 0x1

    .line 839431
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 839432
    :cond_f
    const-string v7, "timezone"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 839433
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 839434
    :cond_10
    const-string v7, "url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 839435
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 839436
    :cond_11
    const-string v7, "viewer_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 839437
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 839438
    :cond_12
    const-string v7, "viewer_has_pending_invite"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 839439
    const/4 v2, 0x1

    .line 839440
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v13, v7

    goto/16 :goto_1

    .line 839441
    :cond_13
    const-string v7, "viewer_watch_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 839442
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 839443
    :cond_14
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 839444
    :cond_15
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 839445
    if-eqz v6, :cond_16

    .line 839446
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 839447
    :cond_16
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839448
    if-eqz v3, :cond_17

    .line 839449
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 839450
    :cond_17
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839451
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839452
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839453
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839454
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839455
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839456
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839457
    if-eqz v11, :cond_18

    .line 839458
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 839459
    :cond_18
    if-eqz v10, :cond_19

    .line 839460
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 839461
    :cond_19
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839462
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839463
    if-eqz v9, :cond_1a

    .line 839464
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 839465
    :cond_1a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 839466
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 839467
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 839468
    if-eqz v8, :cond_1b

    .line 839469
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 839470
    :cond_1b
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 839471
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1c
    move/from16 v30, v28

    move/from16 v31, v29

    move/from16 v28, v24

    move/from16 v29, v25

    move/from16 v24, v20

    move/from16 v25, v21

    move/from16 v20, v16

    move/from16 v21, v17

    move/from16 v16, v12

    move/from16 v17, v13

    move v12, v8

    move v13, v9

    move v8, v2

    move v9, v3

    move v3, v6

    move v6, v7

    move/from16 v33, v19

    move-wide/from16 v34, v14

    move v15, v11

    move v14, v10

    move v10, v4

    move v11, v5

    move-wide/from16 v4, v26

    move/from16 v26, v22

    move/from16 v27, v23

    move/from16 v22, v18

    move/from16 v23, v33

    move-wide/from16 v18, v34

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x13

    const/16 v3, 0x11

    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 839472
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 839473
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 839474
    if-eqz v0, :cond_0

    .line 839475
    const-string v1, "can_viewer_change_guest_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839476
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 839477
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 839478
    if-eqz v0, :cond_1

    .line 839479
    const-string v0, "connection_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839480
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839481
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 839482
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 839483
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839484
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 839485
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839486
    if-eqz v0, :cond_3

    .line 839487
    const-string v1, "event_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839488
    invoke-static {p0, v0, p2, p3}, LX/5En;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 839489
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839490
    if-eqz v0, :cond_4

    .line 839491
    const-string v1, "event_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839492
    invoke-static {p0, v0, p2}, LX/5Eo;->a(LX/15i;ILX/0nX;)V

    .line 839493
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839494
    if-eqz v0, :cond_5

    .line 839495
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839496
    invoke-static {p0, v0, p2, p3}, LX/5Fk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 839497
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839498
    if-eqz v0, :cond_6

    .line 839499
    const-string v1, "event_watchers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839500
    invoke-static {p0, v0, p2}, LX/5Ep;->a(LX/15i;ILX/0nX;)V

    .line 839501
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839502
    if-eqz v0, :cond_7

    .line 839503
    const-string v1, "friendEventMembersFirst3"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839504
    invoke-static {p0, v0, p2, p3}, LX/5Eu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 839505
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839506
    if-eqz v0, :cond_8

    .line 839507
    const-string v1, "friendEventWatchersFirst3"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839508
    invoke-static {p0, v0, p2, p3}, LX/5Ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 839509
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839510
    if-eqz v0, :cond_9

    .line 839511
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839512
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839513
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 839514
    if-eqz v0, :cond_a

    .line 839515
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839516
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 839517
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 839518
    if-eqz v0, :cond_b

    .line 839519
    const-string v1, "is_canceled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839520
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 839521
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839522
    if-eqz v0, :cond_c

    .line 839523
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839524
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839525
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 839526
    if-eqz v0, :cond_d

    .line 839527
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839528
    invoke-static {p0, v0, p2}, LX/5Eq;->a(LX/15i;ILX/0nX;)V

    .line 839529
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 839530
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 839531
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839532
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 839533
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839534
    if-eqz v0, :cond_f

    .line 839535
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839536
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839537
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 839538
    if-eqz v0, :cond_10

    .line 839539
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839540
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839541
    :cond_10
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 839542
    if-eqz v0, :cond_11

    .line 839543
    const-string v0, "viewer_guest_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839544
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839545
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 839546
    if-eqz v0, :cond_12

    .line 839547
    const-string v1, "viewer_has_pending_invite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839548
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 839549
    :cond_12
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 839550
    if-eqz v0, :cond_13

    .line 839551
    const-string v0, "viewer_watch_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 839552
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 839553
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 839554
    return-void
.end method
