.class public final LX/5Z6;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 944060
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;

    const v0, -0x330af9c9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PinnedThreadsQuery"

    const-string v6, "c1de579cbe29098c224f27ad2eb2e83d"

    const-string v7, "viewer"

    const-string v8, "10155265581911729"

    const-string v9, "10155259696866729"

    const-string v0, "actor_id"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 944061
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 944062
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 944063
    sparse-switch v0, :sswitch_data_0

    .line 944064
    :goto_0
    return-object p1

    .line 944065
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 944066
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 944067
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 944068
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 944069
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 944070
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 944071
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 944072
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 944073
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 944074
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 944075
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 944076
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 944077
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 944078
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 944079
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 944080
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 944081
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 944082
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 944083
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 944084
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 944085
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 944086
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 944087
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 944088
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 944089
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 944090
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_9
        -0x7226305f -> :sswitch_18
        -0x5f54881d -> :sswitch_4
        -0x5bcbf522 -> :sswitch_15
        -0x56855c4a -> :sswitch_13
        -0x4c47f2a9 -> :sswitch_12
        -0x4450092f -> :sswitch_e
        -0x39e54905 -> :sswitch_19
        -0x26451294 -> :sswitch_2
        -0x1b236af7 -> :sswitch_17
        -0x179abbec -> :sswitch_16
        -0x10df6d66 -> :sswitch_14
        -0xf820fe3 -> :sswitch_8
        -0x786d0bb -> :sswitch_c
        -0x3224078 -> :sswitch_d
        -0x132889c -> :sswitch_11
        -0x8d30fe -> :sswitch_b
        0x8da57ae -> :sswitch_5
        0xe0e2e5a -> :sswitch_7
        0x19ec4b2a -> :sswitch_1
        0x2a4412e2 -> :sswitch_0
        0x2f1911b0 -> :sswitch_f
        0x3349e8c0 -> :sswitch_10
        0x5af48aaa -> :sswitch_3
        0x5ba7488b -> :sswitch_a
        0x69308369 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 944091
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 944092
    :goto_1
    return v0

    .line 944093
    :pswitch_1
    const-string v2, "24"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "22"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 944094
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944095
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x640
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
