.class public LX/5ON;
.super LX/5OM;
.source ""


# instance fields
.field private final l:Landroid/widget/AdapterView$OnItemClickListener;

.field public m:Landroid/widget/ListAdapter;

.field public n:I

.field public o:F

.field public p:Landroid/widget/AdapterView$OnItemClickListener;

.field private q:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 908665
    invoke-direct {p0, p1}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 908666
    new-instance v0, LX/5OL;

    invoke-direct {v0, p0}, LX/5OL;-><init>(LX/5ON;)V

    iput-object v0, p0, LX/5ON;->l:Landroid/widget/AdapterView$OnItemClickListener;

    .line 908667
    iput v2, p0, LX/5ON;->n:I

    .line 908668
    iput v1, p0, LX/5ON;->o:F

    .line 908669
    invoke-virtual {p0, v2}, LX/0ht;->c(Z)V

    .line 908670
    invoke-virtual {p0, v1}, LX/0ht;->b(F)V

    .line 908671
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 908672
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 908673
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 908674
    const v4, 0x7f01024e

    const/4 p1, 0x1

    invoke-virtual {v3, v4, v1, p1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 908675
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    .line 908676
    :goto_0
    move v1, v1

    .line 908677
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setBackgroundResource(I)V

    .line 908678
    iput-boolean v2, p0, LX/0ht;->e:Z

    .line 908679
    invoke-virtual {p0, v2}, LX/0ht;->e(Z)V

    .line 908680
    return-void

    :cond_0
    const v1, 0x7f02097d

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/5OK;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 908681
    iget-object v0, p0, LX/5ON;->m:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5ON;->m:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 908682
    :cond_0
    invoke-super {p0}, LX/5OM;->b()LX/5OK;

    move-result-object v0

    .line 908683
    :goto_0
    return-object v0

    .line 908684
    :cond_1
    new-instance v1, LX/5OK;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5OK;-><init>(Landroid/content/Context;)V

    .line 908685
    iget-object v0, p0, LX/5ON;->m:Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908686
    invoke-virtual {v1, v2}, LX/5OK;->setFocusable(Z)V

    .line 908687
    invoke-virtual {v1, v2}, LX/5OK;->setFocusableInTouchMode(Z)V

    .line 908688
    iget v0, p0, LX/5ON;->n:I

    invoke-virtual {v1, v0}, LX/5OK;->setSelection(I)V

    .line 908689
    new-instance v0, Lcom/facebook/fbui/popover/PopoverListViewWindow$2;

    invoke-direct {v0, p0, v1}, Lcom/facebook/fbui/popover/PopoverListViewWindow$2;-><init>(LX/5ON;LX/5OK;)V

    invoke-virtual {v1, v0}, LX/5OK;->post(Ljava/lang/Runnable;)Z

    .line 908690
    iget-boolean v0, p0, LX/0ht;->e:Z

    invoke-virtual {v1, v0}, LX/5OK;->setShowFullWidth(Z)V

    .line 908691
    iget v0, p0, LX/0ht;->c:I

    invoke-virtual {v1, v0}, LX/5OK;->setMaxWidth(I)V

    .line 908692
    iget-object v0, p0, LX/5ON;->l:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v0}, LX/5OK;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 908693
    iget-object v0, p0, LX/5ON;->q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v1, v0}, LX/5OK;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 908694
    iget-boolean v0, p0, LX/5OM;->a:Z

    invoke-virtual {v1, v0}, LX/5OK;->a(Z)V

    .line 908695
    iget v0, p0, LX/5ON;->o:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 908696
    iget v0, p0, LX/5ON;->o:F

    invoke-virtual {v1, v0}, LX/5OK;->setMaxRows(F)V

    .line 908697
    :cond_2
    invoke-virtual {p0}, LX/0ht;->h()Landroid/view/View;

    move-result-object v0

    .line 908698
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, LX/5OK;->setMinimumWidth(I)V

    move-object v0, v1

    .line 908699
    goto :goto_0

    .line 908700
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
