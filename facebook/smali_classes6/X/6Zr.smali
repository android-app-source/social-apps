.class public final LX/6Zr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ai;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Ai",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/maps/FbStaticMapView;


# direct methods
.method public constructor <init>(Lcom/facebook/maps/FbStaticMapView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1111744
    iput-object p1, p0, LX/6Zr;->b:Lcom/facebook/maps/FbStaticMapView;

    iput-object p2, p0, LX/6Zr;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1111739
    iget-object v0, p0, LX/6Zr;->b:Lcom/facebook/maps/FbStaticMapView;

    iget-wide v0, v0, Lcom/facebook/maps/FbStaticMapView;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1111740
    sget-object v0, LX/31U;->C:LX/31U;

    new-instance v1, LX/6Zq;

    invoke-direct {v1, p0}, LX/6Zq;-><init>(LX/6Zr;)V

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/util/Map;)V

    .line 1111741
    iget-object v0, p0, LX/6Zr;->b:Lcom/facebook/maps/FbStaticMapView;

    .line 1111742
    iput-wide v2, v0, Lcom/facebook/maps/FbStaticMapView;->o:J

    .line 1111743
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1111733
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1111738
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111737
    invoke-direct {p0}, LX/6Zr;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1111736
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111735
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1111734
    return-void
.end method
