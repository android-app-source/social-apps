.class public LX/6Uq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/6Uq;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/6Uo;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Handler;

.field private final d:Ljava/util/Random;

.field public final e:LX/03V;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/6V0;

.field private final h:LX/0l0;

.field public final i:LX/0Sh;

.field public final j:LX/0Sy;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Uo;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Vq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1102962
    const-class v0, LX/6Uq;

    sput-object v0, LX/6Uq;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Landroid/os/Handler;LX/03V;LX/6V0;LX/0Or;LX/0Sy;LX/0l0;LX/0Sh;Ljava/util/Random;LX/0Or;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p9    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/fbui/runtimelinter/IsRuntimeLinterEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/6Uo;",
            ">;>;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/6V0;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0l0;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1102963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1102964
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/6Uq;->n:Ljava/lang/ref/WeakReference;

    .line 1102965
    iput-object p1, p0, LX/6Uq;->b:LX/0Ot;

    .line 1102966
    iput-object p2, p0, LX/6Uq;->c:Landroid/os/Handler;

    .line 1102967
    iput-object p3, p0, LX/6Uq;->e:LX/03V;

    .line 1102968
    iput-object p4, p0, LX/6Uq;->g:LX/6V0;

    .line 1102969
    iput-object p5, p0, LX/6Uq;->f:LX/0Or;

    .line 1102970
    iput-object p6, p0, LX/6Uq;->j:LX/0Sy;

    .line 1102971
    iput-object p7, p0, LX/6Uq;->h:LX/0l0;

    .line 1102972
    iput-object p8, p0, LX/6Uq;->i:LX/0Sh;

    .line 1102973
    iput-object p9, p0, LX/6Uq;->d:Ljava/util/Random;

    .line 1102974
    iput-object p10, p0, LX/6Uq;->k:LX/0Or;

    .line 1102975
    return-void
.end method

.method public static a(LX/0QB;)LX/6Uq;
    .locals 14

    .prologue
    .line 1102976
    sget-object v0, LX/6Uq;->p:LX/6Uq;

    if-nez v0, :cond_1

    .line 1102977
    const-class v1, LX/6Uq;

    monitor-enter v1

    .line 1102978
    :try_start_0
    sget-object v0, LX/6Uq;->p:LX/6Uq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1102979
    if-eqz v2, :cond_0

    .line 1102980
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1102981
    new-instance v3, LX/6Uq;

    .line 1102982
    new-instance v4, LX/6Up;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-direct {v4, v5}, LX/6Up;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 1102983
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 1102984
    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v7

    check-cast v7, LX/6V0;

    const/16 v8, 0x2fd

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v9

    check-cast v9, LX/0Sy;

    invoke-static {v0}, LX/0l0;->a(LX/0QB;)LX/0l0;

    move-result-object v10

    check-cast v10, LX/0l0;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v12

    check-cast v12, Ljava/util/Random;

    const/16 v13, 0x1491

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/6Uq;-><init>(LX/0Ot;Landroid/os/Handler;LX/03V;LX/6V0;LX/0Or;LX/0Sy;LX/0l0;LX/0Sh;Ljava/util/Random;LX/0Or;)V

    .line 1102985
    move-object v0, v3

    .line 1102986
    sput-object v0, LX/6Uq;->p:LX/6Uq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1102987
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1102988
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1102989
    :cond_1
    sget-object v0, LX/6Uq;->p:LX/6Uq;

    return-object v0

    .line 1102990
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1102991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/6Uq;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6Uo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1102992
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6Uq;->l:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1102993
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/6Uq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, LX/6Uq;->l:Ljava/util/List;

    .line 1102994
    :cond_0
    iget-object v0, p0, LX/6Uq;->l:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1102995
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
