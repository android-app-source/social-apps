.class public final LX/5LQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 901169
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 901170
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 901171
    :goto_0
    return v1

    .line 901172
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 901173
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 901174
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 901175
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 901176
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 901177
    const-string v6, "place_ids"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 901178
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 901179
    :cond_2
    const-string v6, "place_topic_ids"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 901180
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 901181
    :cond_3
    const-string v6, "show_all_contextual_places"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 901182
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 901183
    :cond_4
    const-string v6, "suggestion_mechanism"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 901184
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 901185
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 901186
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 901187
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 901188
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 901189
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 901190
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 901191
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 901192
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 901193
    if-eqz v0, :cond_0

    .line 901194
    const-string v0, "place_ids"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901195
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 901196
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 901197
    if-eqz v0, :cond_1

    .line 901198
    const-string v0, "place_topic_ids"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901199
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 901200
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 901201
    if-eqz v0, :cond_2

    .line 901202
    const-string v0, "show_all_contextual_places"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901203
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901204
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 901205
    if-eqz v0, :cond_3

    .line 901206
    const-string v1, "suggestion_mechanism"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901207
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901208
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 901209
    return-void
.end method
