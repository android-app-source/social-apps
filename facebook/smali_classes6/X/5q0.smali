.class public final LX/5q0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/cxxbridge/ReactCallback;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V
    .locals 1

    .prologue
    .line 1008769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008770
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/5q0;->a:Ljava/lang/ref/WeakReference;

    .line 1008771
    return-void
.end method


# virtual methods
.method public final decrementPendingJSCalls()V
    .locals 1

    .prologue
    .line 1008765
    iget-object v0, p0, LX/5q0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;

    .line 1008766
    if-eqz v0, :cond_0

    .line 1008767
    invoke-static {v0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->h(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    .line 1008768
    :cond_0
    return-void
.end method

.method public final incrementPendingJSCalls()V
    .locals 1

    .prologue
    .line 1008753
    iget-object v0, p0, LX/5q0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;

    .line 1008754
    if-eqz v0, :cond_0

    .line 1008755
    invoke-static {v0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->g(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    .line 1008756
    :cond_0
    return-void
.end method

.method public final onBatchComplete()V
    .locals 1

    .prologue
    .line 1008761
    iget-object v0, p0, LX/5q0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;

    .line 1008762
    if-eqz v0, :cond_0

    .line 1008763
    iget-object v0, v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v0}, LX/5qE;->c()V

    .line 1008764
    :cond_0
    return-void
.end method

.method public final onNativeException(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1008757
    iget-object v0, p0, LX/5q0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;

    .line 1008758
    if-eqz v0, :cond_0

    .line 1008759
    invoke-static {v0, p1}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->a$redex0(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;Ljava/lang/Exception;)V

    .line 1008760
    :cond_0
    return-void
.end method
