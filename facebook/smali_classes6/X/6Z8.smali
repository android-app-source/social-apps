.class public LX/6Z8;
.super Lcom/facebook/location/BaseFbLocationManager;
.source ""


# instance fields
.field private final a:LX/0y3;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Landroid/location/LocationManager;

.field public d:LX/2vk;

.field public final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public f:LX/6Z7;


# direct methods
.method public constructor <init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Landroid/location/LocationManager;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V
    .locals 9
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0y3;",
            "LX/0SG;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Landroid/location/LocationManager;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Zb;",
            "LX/0y2;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110954
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lcom/facebook/location/BaseFbLocationManager;-><init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V

    .line 1110955
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/6Z8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1110956
    iput-object p1, p0, LX/6Z8;->a:LX/0y3;

    .line 1110957
    iput-object p3, p0, LX/6Z8;->b:Ljava/util/concurrent/ExecutorService;

    .line 1110958
    iput-object p5, p0, LX/6Z8;->c:Landroid/location/LocationManager;

    .line 1110959
    return-void
.end method

.method public static a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .param p0    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1110960
    if-nez p0, :cond_0

    .line 1110961
    const/4 v0, 0x0

    .line 1110962
    :goto_0
    return-object v0

    .line 1110963
    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1110964
    const v0, 0x45505000    # 3333.0f

    invoke-virtual {p0, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 1110965
    :cond_1
    invoke-static {p0}, Lcom/facebook/location/ImmutableLocation;->c(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    goto :goto_0
.end method

.method private e()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1110966
    iget-object v0, p0, LX/6Z8;->a:LX/0y3;

    iget-object v1, p0, LX/6Z8;->d:LX/2vk;

    iget-object v1, v1, LX/2vk;->a:LX/0yF;

    invoke-virtual {v0, v1}, LX/0y3;->b(LX/0yF;)LX/1rv;

    move-result-object v0

    .line 1110967
    iget-object v1, v0, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, v2, :cond_0

    .line 1110968
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    throw v0

    .line 1110969
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/6Z8;->c:Landroid/location/LocationManager;

    const-string v2, "passive"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v1

    .line 1110970
    if-nez v1, :cond_1

    .line 1110971
    iget-object v0, v0, LX/1rv;->b:LX/0Rf;

    .line 1110972
    :goto_0
    return-object v0

    .line 1110973
    :cond_1
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    iget-object v2, v0, LX/1rv;->b:LX/0Rf;

    invoke-virtual {v1, v2}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v1

    const-string v2, "passive"

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v1

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1110974
    :catch_0
    iget-object v0, v0, LX/1rv;->b:LX/0Rf;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1wZ;
    .locals 1

    .prologue
    .line 1110975
    sget-object v0, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    return-object v0
.end method

.method public final declared-synchronized a(LX/2vk;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1110976
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6Z8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "operation already running"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1110977
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vk;

    iput-object v0, p0, LX/6Z8;->d:LX/2vk;

    .line 1110978
    new-instance v0, LX/6Z7;

    invoke-direct {v0, p0}, LX/6Z7;-><init>(LX/6Z8;)V

    iput-object v0, p0, LX/6Z8;->f:LX/6Z7;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1110979
    :try_start_1
    invoke-direct {p0}, LX/6Z8;->e()LX/0Rf;
    :try_end_1
    .catch LX/2Ce; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 1110980
    :try_start_2
    iget-object v0, p0, LX/6Z8;->c:Landroid/location/LocationManager;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    .line 1110981
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1110982
    iget-object v5, p0, LX/6Z8;->c:Landroid/location/LocationManager;

    invoke-virtual {v5, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 1110983
    invoke-static {v0}, LX/6Z8;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 1110984
    if-eqz v0, :cond_3

    .line 1110985
    invoke-virtual {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 1110986
    invoke-virtual {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v6

    const-wide/32 v8, 0xdbba0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_3

    move v0, v1

    :goto_2
    move v2, v0

    .line 1110987
    goto :goto_1

    :cond_0
    move v0, v2

    .line 1110988
    goto :goto_0

    .line 1110989
    :catch_0
    move-exception v0

    .line 1110990
    invoke-virtual {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2Ce;)V

    .line 1110991
    iget-object v0, p0, LX/6Z8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1110992
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Z8;->d:LX/2vk;

    .line 1110993
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Z8;->f:LX/6Z7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1110994
    :goto_3
    monitor-exit p0

    return-void

    .line 1110995
    :cond_1
    if-nez v2, :cond_2

    .line 1110996
    :try_start_3
    invoke-virtual {p0}, Lcom/facebook/location/BaseFbLocationManager;->d()V

    .line 1110997
    :cond_2
    iget-object v0, p0, LX/6Z8;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;

    invoke-direct {v1, p0, v3}, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;-><init>(LX/6Z8;LX/0Rf;)V

    const v2, 0x51e9584a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1110998
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1110999
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6Z8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1111000
    :goto_0
    monitor-exit p0

    return-void

    .line 1111001
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6Z8;->c:Landroid/location/LocationManager;

    iget-object v1, p0, LX/6Z8;->f:LX/6Z7;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 1111002
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Z8;->f:LX/6Z7;

    .line 1111003
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Z8;->d:LX/2vk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1111004
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
