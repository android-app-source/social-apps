.class public final LX/53c;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source ""

# interfaces
.implements LX/0za;


# instance fields
.field public final parent:LX/54i;

.field public final s:LX/0za;


# direct methods
.method public constructor <init>(LX/0za;LX/54i;)V
    .locals 0

    .prologue
    .line 827266
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 827267
    iput-object p1, p0, LX/53c;->s:LX/0za;

    .line 827268
    iput-object p2, p0, LX/53c;->parent:LX/54i;

    .line 827269
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 827263
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/53c;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827264
    iget-object v0, p0, LX/53c;->parent:LX/54i;

    iget-object v1, p0, LX/53c;->s:LX/0za;

    invoke-virtual {v0, v1}, LX/54i;->b(LX/0za;)V

    .line 827265
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827262
    iget-object v0, p0, LX/53c;->s:LX/0za;

    invoke-interface {v0}, LX/0za;->c()Z

    move-result v0

    return v0
.end method
