.class public final LX/67S;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/37z;


# direct methods
.method public constructor <init>(LX/37z;)V
    .locals 0

    .prologue
    .line 1052999
    iput-object p1, p0, LX/67S;->a:LX/37z;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/16 v0, 0x26

    const v1, -0x51ef3741

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1053000
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1053001
    const-string v1, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1053002
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 1053003
    const-string v1, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1053004
    if-ltz v1, :cond_0

    iget-object v2, p0, LX/67S;->a:LX/37z;

    iget v2, v2, LX/37z;->d:I

    if-eq v1, v2, :cond_0

    .line 1053005
    iget-object v1, p0, LX/67S;->a:LX/37z;

    invoke-static {v1}, LX/37z;->f(LX/37z;)V

    .line 1053006
    :cond_0
    const/16 v1, 0x27

    const v2, -0x20a12789

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
