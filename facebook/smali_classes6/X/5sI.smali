.class public final enum LX/5sI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5sI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5sI;

.field public static final enum OPACITY:LX/5sI;

.field public static final enum SCALE_XY:LX/5sI;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1012736
    new-instance v0, LX/5sI;

    const-string v1, "OPACITY"

    const-string v2, "opacity"

    invoke-direct {v0, v1, v3, v2}, LX/5sI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sI;->OPACITY:LX/5sI;

    .line 1012737
    new-instance v0, LX/5sI;

    const-string v1, "SCALE_XY"

    const-string v2, "scaleXY"

    invoke-direct {v0, v1, v4, v2}, LX/5sI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sI;->SCALE_XY:LX/5sI;

    .line 1012738
    const/4 v0, 0x2

    new-array v0, v0, [LX/5sI;

    sget-object v1, LX/5sI;->OPACITY:LX/5sI;

    aput-object v1, v0, v3

    sget-object v1, LX/5sI;->SCALE_XY:LX/5sI;

    aput-object v1, v0, v4

    sput-object v0, LX/5sI;->$VALUES:[LX/5sI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1012739
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1012740
    iput-object p3, p0, LX/5sI;->mName:Ljava/lang/String;

    .line 1012741
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/5sI;
    .locals 5

    .prologue
    .line 1012742
    invoke-static {}, LX/5sI;->values()[LX/5sI;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1012743
    invoke-virtual {v3}, LX/5sI;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1012744
    return-object v3

    .line 1012745
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012746
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported animated property : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5sI;
    .locals 1

    .prologue
    .line 1012747
    const-class v0, LX/5sI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5sI;

    return-object v0
.end method

.method public static values()[LX/5sI;
    .locals 1

    .prologue
    .line 1012748
    sget-object v0, LX/5sI;->$VALUES:[LX/5sI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5sI;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012749
    iget-object v0, p0, LX/5sI;->mName:Ljava/lang/String;

    return-object v0
.end method
