.class public LX/5Kh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 898934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 898935
    const/4 v3, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 898936
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898937
    :sswitch_0
    const-string v4, "stretch"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "start"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v0

    goto :goto_0

    :sswitch_2
    const-string v4, "center"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x2

    goto :goto_0

    :sswitch_3
    const-string v4, "end"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto :goto_0

    .line 898938
    :pswitch_0
    const/4 v0, 0x4

    .line 898939
    :goto_1
    :pswitch_1
    return v0

    :pswitch_2
    move v0, v1

    .line 898940
    goto :goto_1

    :pswitch_3
    move v0, v2

    .line 898941
    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x702b18fb -> :sswitch_0
        -0x514d33ab -> :sswitch_2
        0x188db -> :sswitch_3
        0x68ac462 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/1De;Lcom/facebook/java2js/JSValue;LX/5KI;)LX/1Dg;
    .locals 9
    .param p1    # Lcom/facebook/java2js/JSValue;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/5KI;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 898942
    const-string v0, "CSFlexBoxComponent.onCreateLayout"

    const v1, -0x5a3bd4c4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 898943
    :try_start_0
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    .line 898944
    const-string v1, "flexDirection"

    invoke-virtual {p1, v1}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 898945
    if-eqz v1, :cond_0

    .line 898946
    invoke-static {v1}, LX/5Kh;->b(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    .line 898947
    :cond_0
    const-string v1, "alignItems"

    invoke-virtual {p1, v1}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 898948
    if-eqz v1, :cond_1

    .line 898949
    invoke-static {v1}, LX/5Kh;->a(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    .line 898950
    :cond_1
    const-string v1, "justifyContent"

    invoke-virtual {p1, v1}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 898951
    if-eqz v1, :cond_2

    .line 898952
    invoke-static {v1}, LX/5Kh;->c(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    .line 898953
    :cond_2
    const-string v1, "children"

    invoke-virtual {p1, v1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    .line 898954
    invoke-virtual {v1}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 898955
    const/4 v3, 0x0

    .line 898956
    :goto_0
    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    .line 898957
    invoke-virtual {v4}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v5

    if-nez v5, :cond_4

    .line 898958
    :cond_3
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 898959
    const v1, -0x3b0b4078

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x262c9972

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 898960
    :cond_4
    const-string v5, "child"

    invoke-virtual {v4, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-virtual {p2, v5}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v5

    .line 898961
    if-nez v5, :cond_5

    .line 898962
    const-string v5, "componentscript"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to resolve child: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/java2js/JSValue;->toJSON()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 898963
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 898964
    :cond_5
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 898965
    const-string v6, "alignSelf"

    invoke-virtual {v4, v6}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 898966
    if-eqz v6, :cond_6

    .line 898967
    invoke-static {v6}, LX/5Kh;->a(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    .line 898968
    :cond_6
    const-string v6, "flexGrow"

    invoke-virtual {v4, v6}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v7

    .line 898969
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_7

    .line 898970
    double-to-float v6, v7

    invoke-interface {v5, v6}, LX/1Di;->b(F)LX/1Di;

    .line 898971
    :cond_7
    const-string v6, "flexShrink"

    invoke-virtual {v4, v6}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v7

    .line 898972
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_8

    .line 898973
    double-to-float v6, v7

    invoke-interface {v5, v6}, LX/1Di;->c(F)LX/1Di;

    .line 898974
    :cond_8
    const-string v6, "flexBasis"

    invoke-virtual {v4, v6}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v7

    .line 898975
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-nez v4, :cond_9

    .line 898976
    double-to-int v4, v7

    invoke-interface {v5, v4}, LX/1Di;->d(I)LX/1Di;

    .line 898977
    :cond_9
    invoke-interface {v0, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 898978
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 898979
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898980
    :sswitch_0
    const-string v2, "row"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "column"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 898981
    :pswitch_0
    const/4 v0, 0x2

    .line 898982
    :pswitch_1
    return v0

    :sswitch_data_0
    .sparse-switch
        -0x50c12caa -> :sswitch_1
        0x1b9da -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 898983
    const/4 v3, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 898984
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898985
    :sswitch_0
    const-string v4, "start"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v0

    goto :goto_0

    :sswitch_1
    const-string v4, "center"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v1

    goto :goto_0

    :sswitch_2
    const-string v4, "end"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 898986
    :goto_1
    :pswitch_1
    return v0

    :pswitch_2
    move v0, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x514d33ab -> :sswitch_1
        0x188db -> :sswitch_2
        0x68ac462 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
