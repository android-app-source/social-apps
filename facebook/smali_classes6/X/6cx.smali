.class public LX/6cx;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2bA;

.field public static final b:LX/2bA;

.field public static final c:LX/2bA;

.field public static final d:LX/2bA;

.field public static final e:LX/2bA;

.field public static final f:LX/2bA;

.field public static final g:LX/2bA;

.field public static final h:LX/2bA;

.field public static final i:LX/2bA;

.field public static final j:LX/2bA;

.field public static final k:LX/2bA;

.field public static final l:LX/2bA;

.field public static final m:LX/2bA;

.field public static final n:LX/2bA;

.field public static final o:LX/2bA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1115003
    new-instance v0, LX/2bA;

    const-string v1, "/sync/"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    .line 1115004
    sput-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_get_pinned_threads_update_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->b:LX/2bA;

    .line 1115005
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_fetch_pinned_threads_client_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->c:LX/2bA;

    .line 1115006
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_fetch_pinned_thread_suggestions_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->d:LX/2bA;

    .line 1115007
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_fetch_ranked_threads_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->e:LX/2bA;

    .line 1115008
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "user_info_fetch_latest_thread_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->f:LX/2bA;

    .line 1115009
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "sync_latest_user_info_fetch_action_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->g:LX/2bA;

    .line 1115010
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "sync_latest_user_info_fetch_timestamp_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->h:LX/2bA;

    .line 1115011
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_fetch_group_threads_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->i:LX/2bA;

    .line 1115012
    sget-object v0, LX/6cx;->a:LX/2bA;

    const-string v1, "last_fetch_room_threads_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    sput-object v0, LX/6cx;->j:LX/2bA;

    .line 1115013
    new-instance v0, LX/2bA;

    const-string v1, "sync_token"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6cx;->k:LX/2bA;

    .line 1115014
    new-instance v0, LX/2bA;

    const-string v1, "last_sequence_id"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6cx;->l:LX/2bA;

    .line 1115015
    new-instance v0, LX/2bA;

    const-string v1, "last_sync_full_refresh_ms"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6cx;->m:LX/2bA;

    .line 1115016
    new-instance v0, LX/2bA;

    const-string v1, "needs_full_refresh"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6cx;->n:LX/2bA;

    .line 1115017
    new-instance v0, LX/2bA;

    const-string v1, "full_refresh_reason"

    invoke-direct {v0, v1}, LX/2bA;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6cx;->o:LX/2bA;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1115002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/6ek;)LX/2bA;
    .locals 2

    .prologue
    .line 1115001
    sget-object v0, LX/6cx;->a:LX/2bA;

    iget-object v1, p0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    const-string v1, "/last_get_threads_client_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    return-object v0
.end method

.method public static b(LX/6ek;)LX/2bA;
    .locals 2

    .prologue
    .line 1115000
    sget-object v0, LX/6cx;->a:LX/2bA;

    iget-object v1, p0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    const-string v1, "/last_get_threads_action_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    return-object v0
.end method

.method public static c(LX/6ek;)LX/2bA;
    .locals 2

    .prologue
    .line 1114999
    sget-object v0, LX/6cx;->a:LX/2bA;

    iget-object v1, p0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    const-string v1, "/threads_table_out_of_date"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/2bA;

    return-object v0
.end method
