.class public final LX/5Gg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 888897
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 888898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 888899
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 888900
    invoke-static {p0, p1}, LX/5Gg;->b(LX/15w;LX/186;)I

    move-result v1

    .line 888901
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 888902
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 888903
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 888904
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 888905
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5Gg;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 888906
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 888907
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 888908
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 888909
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 888910
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888911
    :goto_0
    return v1

    .line 888912
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888913
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_7

    .line 888914
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 888915
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 888916
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 888917
    const-string v6, "attachment_properties"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 888918
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 888919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 888920
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 888921
    const/4 v6, 0x0

    .line 888922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_f

    .line 888923
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888924
    :goto_3
    move v5, v6

    .line 888925
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 888926
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 888927
    goto :goto_1

    .line 888928
    :cond_3
    const-string v6, "media"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 888929
    invoke-static {p0, p1}, LX/5Gf;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 888930
    :cond_4
    const-string v6, "style_infos"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 888931
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 888932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_5

    .line 888933
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_5

    .line 888934
    invoke-static {p0, p1}, LX/5Ea;->b(LX/15w;LX/186;)I

    move-result v5

    .line 888935
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 888936
    :cond_5
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 888937
    goto/16 :goto_1

    .line 888938
    :cond_6
    const-string v6, "style_list"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 888939
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 888940
    :cond_7
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 888941
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 888942
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 888943
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 888944
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 888945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 888946
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888947
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_e

    .line 888948
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 888949
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 888950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_a

    if-eqz v10, :cond_a

    .line 888951
    const-string v11, "key"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 888952
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_5

    .line 888953
    :cond_b
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 888954
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_5

    .line 888955
    :cond_c
    const-string v11, "type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 888956
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 888957
    :cond_d
    const-string v11, "value"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 888958
    invoke-static {p0, p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_5

    .line 888959
    :cond_e
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 888960
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 888961
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 888962
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 888963
    const/4 v6, 0x3

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 888964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_f
    move v5, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x3

    .line 888965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888966
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888967
    if-eqz v0, :cond_5

    .line 888968
    const-string v1, "attachment_properties"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888969
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 888970
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 888971
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    .line 888972
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888973
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 888974
    if-eqz v4, :cond_0

    .line 888975
    const-string v5, "key"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888976
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888977
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 888978
    if-eqz v4, :cond_1

    .line 888979
    const-string v5, "title"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888980
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888981
    :cond_1
    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 888982
    if-eqz v4, :cond_2

    .line 888983
    const-string v5, "type"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888984
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888985
    :cond_2
    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 888986
    if-eqz v4, :cond_3

    .line 888987
    const-string v5, "value"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888988
    invoke-static {p0, v4, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 888989
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888990
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 888991
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 888992
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888993
    if-eqz v0, :cond_6

    .line 888994
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888995
    invoke-static {p0, v0, p2, p3}, LX/5Gf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 888996
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888997
    if-eqz v0, :cond_8

    .line 888998
    const-string v1, "style_infos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888999
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 889000
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 889001
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/5Ea;->a(LX/15i;ILX/0nX;)V

    .line 889002
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 889003
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 889004
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 889005
    if-eqz v0, :cond_9

    .line 889006
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 889007
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 889008
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 889009
    return-void
.end method
