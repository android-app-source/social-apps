.class public interface abstract LX/5CI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5CG;
.implements LX/5CH;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "NewsFeedExplicitPlaceFields"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract r()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOverallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
