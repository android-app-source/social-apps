.class public final enum LX/6J1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6J1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6J1;

.field public static final enum CLOSE:LX/6J1;

.field public static final enum NONE:LX/6J1;

.field public static final enum OPEN:LX/6J1;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1075199
    new-instance v0, LX/6J1;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/6J1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J1;->NONE:LX/6J1;

    .line 1075200
    new-instance v0, LX/6J1;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v3}, LX/6J1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J1;->OPEN:LX/6J1;

    .line 1075201
    new-instance v0, LX/6J1;

    const-string v1, "CLOSE"

    invoke-direct {v0, v1, v4}, LX/6J1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J1;->CLOSE:LX/6J1;

    .line 1075202
    const/4 v0, 0x3

    new-array v0, v0, [LX/6J1;

    sget-object v1, LX/6J1;->NONE:LX/6J1;

    aput-object v1, v0, v2

    sget-object v1, LX/6J1;->OPEN:LX/6J1;

    aput-object v1, v0, v3

    sget-object v1, LX/6J1;->CLOSE:LX/6J1;

    aput-object v1, v0, v4

    sput-object v0, LX/6J1;->$VALUES:[LX/6J1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1075203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6J1;
    .locals 1

    .prologue
    .line 1075204
    const-class v0, LX/6J1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6J1;

    return-object v0
.end method

.method public static values()[LX/6J1;
    .locals 1

    .prologue
    .line 1075205
    sget-object v0, LX/6J1;->$VALUES:[LX/6J1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6J1;

    return-object v0
.end method
