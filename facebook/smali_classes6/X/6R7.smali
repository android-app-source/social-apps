.class public final LX/6R7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsImageGraphQLModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsImageGraphQLModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1088558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 18

    .prologue
    .line 1088518
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1088519
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6R7;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1088520
    move-object/from16 v0, p0

    iget-object v3, v0, LX/6R7;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1088521
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6R7;->c:LX/0Px;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1088522
    move-object/from16 v0, p0

    iget-object v5, v0, LX/6R7;->d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$DescriptionModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1088523
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6R7;->e:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {v1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1088524
    move-object/from16 v0, p0

    iget-object v7, v0, LX/6R7;->f:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1088525
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6R7;->g:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1088526
    move-object/from16 v0, p0

    iget-object v9, v0, LX/6R7;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1088527
    move-object/from16 v0, p0

    iget-object v10, v0, LX/6R7;->i:LX/0Px;

    invoke-virtual {v1, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1088528
    move-object/from16 v0, p0

    iget-object v11, v0, LX/6R7;->j:LX/0Px;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 1088529
    move-object/from16 v0, p0

    iget-object v12, v0, LX/6R7;->k:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel$PlatformApplicationModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1088530
    move-object/from16 v0, p0

    iget-object v13, v0, LX/6R7;->l:Ljava/lang/String;

    invoke-virtual {v1, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1088531
    move-object/from16 v0, p0

    iget-object v14, v0, LX/6R7;->m:LX/0Px;

    invoke-virtual {v1, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 1088532
    move-object/from16 v0, p0

    iget-object v15, v0, LX/6R7;->n:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsTextWithEntitiesGraphQLModel;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1088533
    move-object/from16 v0, p0

    iget-object v0, v0, LX/6R7;->p:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1088534
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1088535
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1088536
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1088537
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1088538
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1088539
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1088540
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1088541
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1088542
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1088543
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1088544
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1088545
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1088546
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1088547
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1088548
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1088549
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, LX/6R7;->o:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 1088550
    const/16 v2, 0xf

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1088551
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1088552
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1088553
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1088554
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1088555
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1088556
    new-instance v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-direct {v2, v1}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;-><init>(LX/15i;)V

    .line 1088557
    return-object v2
.end method
