.class public final LX/660;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/CountDownLatch;

.field private b:J

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1049158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049159
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/660;->a:Ljava/util/concurrent/CountDownLatch;

    .line 1049160
    iput-wide v2, p0, LX/660;->b:J

    .line 1049161
    iput-wide v2, p0, LX/660;->c:J

    .line 1049162
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1049155
    iget-wide v0, p0, LX/660;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1049156
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/660;->b:J

    .line 1049157
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1049151
    iget-wide v0, p0, LX/660;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, LX/660;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1049152
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/660;->c:J

    .line 1049153
    iget-object v0, p0, LX/660;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1049154
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1049147
    iget-wide v0, p0, LX/660;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, LX/660;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1049148
    :cond_1
    iget-wide v0, p0, LX/660;->b:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/660;->c:J

    .line 1049149
    iget-object v0, p0, LX/660;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1049150
    return-void
.end method
