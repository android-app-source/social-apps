.class public final LX/6b2;
.super LX/3Ag;
.source ""


# instance fields
.field public final synthetic c:LX/3BS;


# direct methods
.method public constructor <init>(LX/3BS;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 1112859
    iput-object p1, p0, LX/6b2;->c:LX/3BS;

    invoke-direct {p0, p2, p3}, LX/3Ag;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public final dismiss()V
    .locals 4

    .prologue
    .line 1112860
    invoke-virtual {p0}, LX/6b2;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1112861
    invoke-virtual {p0}, LX/6b2;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1112862
    if-eqz v1, :cond_0

    .line 1112863
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1112864
    :cond_0
    iget-object v0, p0, LX/6b2;->c:LX/3BS;

    .line 1112865
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1112866
    const-string v2, "stage"

    iget-object v3, v0, LX/3BS;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112867
    const-string v2, "category"

    iget-object v3, v0, LX/3BS;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112868
    const-string v2, "comment"

    iget-object v3, v0, LX/3BS;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112869
    const-string v2, "map_uri"

    iget-object v3, v0, LX/3BS;->n:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112870
    sget-object v2, LX/31U;->x:LX/31U;

    invoke-virtual {v2, v1}, LX/31U;->a(Ljava/util/Map;)V

    .line 1112871
    invoke-super {p0}, LX/3Ag;->dismiss()V

    .line 1112872
    return-void
.end method
