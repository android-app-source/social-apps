.class public LX/5dU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 965720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 965721
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0xc8

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 965722
    const-string v0, "Message ID: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 965723
    invoke-static {}, LX/5dQ;->values()[LX/5dQ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 965724
    invoke-virtual {p1, v4}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v5

    .line 965725
    if-nez v5, :cond_1

    .line 965726
    invoke-virtual {v4}, LX/5dQ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - Not in the URL map\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 965727
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 965728
    :cond_1
    iget-object p0, v5, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v5, p0

    .line 965729
    if-nez v5, :cond_0

    .line 965730
    invoke-virtual {v4}, LX/5dQ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - SRC is null for type\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 965731
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 965732
    invoke-static {}, LX/5dQ;->values()[LX/5dQ;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 965733
    invoke-virtual {p0, v4}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v4

    .line 965734
    invoke-static {v4}, LX/5dU;->a(Lcom/facebook/messaging/model/attachment/ImageUrl;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 965735
    :goto_1
    return v0

    .line 965736
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 965737
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/model/attachment/ImageUrl;)Z
    .locals 1
    .param p0    # Lcom/facebook/messaging/model/attachment/ImageUrl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 965738
    if-eqz p0, :cond_0

    .line 965739
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v0, v0

    .line 965740
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
