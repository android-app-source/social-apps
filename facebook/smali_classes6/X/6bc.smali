.class public LX/6bc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6bX;


# instance fields
.field private final a:LX/0Xp;


# direct methods
.method public constructor <init>(LX/0Xp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113742
    iput-object p1, p0, LX/6bc;->a:LX/0Xp;

    .line 1113743
    return-void
.end method

.method public static b(LX/0QB;)LX/6bc;
    .locals 2

    .prologue
    .line 1113744
    new-instance v1, LX/6bc;

    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v0

    check-cast v0, LX/0Xp;

    invoke-direct {v1, v0}, LX/6bc;-><init>(LX/0Xp;)V

    .line 1113745
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1113746
    sget-object v1, LX/007;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1113747
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1113748
    sget-object v1, LX/007;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1113749
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1113750
    :cond_0
    :goto_0
    return v0

    .line 1113751
    :cond_1
    const-string v1, "keyboard"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1113752
    const-string v0, "m"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1113753
    if-nez v0, :cond_2

    const-string v0, ""

    .line 1113754
    :goto_1
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/0aY;->A:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1113755
    const-string v2, "keyboard_mode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1113756
    iget-object v0, p0, LX/6bc;->a:LX/0Xp;

    invoke-virtual {v0, v1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 1113757
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
