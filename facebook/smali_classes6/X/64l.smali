.class public final LX/64l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/658;

.field public final b:LX/64b;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/658;LX/64b;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/658;",
            "LX/64b;",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1045423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045424
    iput-object p1, p0, LX/64l;->a:LX/658;

    .line 1045425
    iput-object p2, p0, LX/64l;->b:LX/64b;

    .line 1045426
    iput-object p3, p0, LX/64l;->c:Ljava/util/List;

    .line 1045427
    iput-object p4, p0, LX/64l;->d:Ljava/util/List;

    .line 1045428
    return-void
.end method

.method public static a(Ljavax/net/ssl/SSLSession;)LX/64l;
    .locals 5

    .prologue
    .line 1045394
    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    .line 1045395
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cipherSuite == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045396
    :cond_0
    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v2

    .line 1045397
    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 1045398
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "tlsVersion == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045399
    :cond_1
    invoke-static {v0}, LX/658;->forJavaName(Ljava/lang/String;)LX/658;

    move-result-object v3

    .line 1045400
    :try_start_0
    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1045401
    :goto_0
    if-eqz v0, :cond_2

    .line 1045402
    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1045403
    :goto_1
    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v1

    .line 1045404
    if-eqz v1, :cond_3

    .line 1045405
    invoke-static {v1}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1045406
    :goto_2
    new-instance v4, LX/64l;

    invoke-direct {v4, v3, v2, v0, v1}, LX/64l;-><init>(LX/658;LX/64b;Ljava/util/List;Ljava/util/List;)V

    return-object v4

    .line 1045407
    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1045408
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 1045409
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1045416
    instance-of v1, p1, LX/64l;

    if-nez v1, :cond_1

    .line 1045417
    :cond_0
    :goto_0
    return v0

    .line 1045418
    :cond_1
    check-cast p1, LX/64l;

    .line 1045419
    iget-object v1, p0, LX/64l;->b:LX/64b;

    iget-object v2, p1, LX/64l;->b:LX/64b;

    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64l;->b:LX/64b;

    iget-object v2, p1, LX/64l;->b:LX/64b;

    .line 1045420
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64l;->c:Ljava/util/List;

    iget-object v2, p1, LX/64l;->c:Ljava/util/List;

    .line 1045421
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64l;->d:Ljava/util/List;

    iget-object v2, p1, LX/64l;->d:Ljava/util/List;

    .line 1045422
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1045410
    iget-object v0, p0, LX/64l;->a:LX/658;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/64l;->a:LX/658;

    invoke-virtual {v0}, LX/658;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1045411
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64l;->b:LX/64b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1045412
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64l;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1045413
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64l;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1045414
    return v0

    .line 1045415
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
