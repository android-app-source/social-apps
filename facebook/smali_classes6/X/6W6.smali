.class public final enum LX/6W6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6W6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6W6;

.field public static final enum AUTOMATIC_RETRY:LX/6W6;

.field public static final enum FAILURE:LX/6W6;

.field public static final enum MANUAL_RETRY:LX/6W6;

.field public static final enum OFFLINE:LX/6W6;

.field public static final enum REQUEST:LX/6W6;

.field public static final enum SUCCESS:LX/6W6;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1104569
    new-instance v0, LX/6W6;

    const-string v1, "REQUEST"

    invoke-direct {v0, v1, v3}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->REQUEST:LX/6W6;

    .line 1104570
    new-instance v0, LX/6W6;

    const-string v1, "MANUAL_RETRY"

    invoke-direct {v0, v1, v4}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->MANUAL_RETRY:LX/6W6;

    .line 1104571
    new-instance v0, LX/6W6;

    const-string v1, "AUTOMATIC_RETRY"

    invoke-direct {v0, v1, v5}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->AUTOMATIC_RETRY:LX/6W6;

    .line 1104572
    new-instance v0, LX/6W6;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v6}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->SUCCESS:LX/6W6;

    .line 1104573
    new-instance v0, LX/6W6;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v7}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->FAILURE:LX/6W6;

    .line 1104574
    new-instance v0, LX/6W6;

    const-string v1, "OFFLINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6W6;->OFFLINE:LX/6W6;

    .line 1104575
    const/4 v0, 0x6

    new-array v0, v0, [LX/6W6;

    sget-object v1, LX/6W6;->REQUEST:LX/6W6;

    aput-object v1, v0, v3

    sget-object v1, LX/6W6;->MANUAL_RETRY:LX/6W6;

    aput-object v1, v0, v4

    sget-object v1, LX/6W6;->AUTOMATIC_RETRY:LX/6W6;

    aput-object v1, v0, v5

    sget-object v1, LX/6W6;->SUCCESS:LX/6W6;

    aput-object v1, v0, v6

    sget-object v1, LX/6W6;->FAILURE:LX/6W6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6W6;->OFFLINE:LX/6W6;

    aput-object v2, v0, v1

    sput-object v0, LX/6W6;->$VALUES:[LX/6W6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1104576
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6W6;
    .locals 1

    .prologue
    .line 1104577
    const-class v0, LX/6W6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6W6;

    return-object v0
.end method

.method public static values()[LX/6W6;
    .locals 1

    .prologue
    .line 1104578
    sget-object v0, LX/6W6;->$VALUES:[LX/6W6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6W6;

    return-object v0
.end method
