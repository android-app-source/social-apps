.class public final LX/5LC;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 899804
    const-class v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    const v0, -0x59815247

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchTaggableSuggestionsAtPlaceQuery"

    const-string v6, "6869bacede82afb79666eb2e4cfc6017"

    const-string v7, "taggable_activity"

    const-string v8, "10155069966011729"

    const-string v9, "10155259088396729"

    .line 899805
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 899806
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 899807
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 899808
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 899809
    sparse-switch v0, :sswitch_data_0

    .line 899810
    :goto_0
    return-object p1

    .line 899811
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 899812
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 899813
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 899814
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 899815
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 899816
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 899817
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 899818
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 899819
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 899820
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 899821
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 899822
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 899823
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 899824
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 899825
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 899826
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 899827
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 899828
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 899829
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7713b599 -> :sswitch_a
        -0x6e761353 -> :sswitch_9
        -0x6b0fbb02 -> :sswitch_b
        -0x69f19a9a -> :sswitch_3
        -0x51484e72 -> :sswitch_0
        -0x50cab1c8 -> :sswitch_d
        -0x4ae70342 -> :sswitch_c
        -0x444a93e6 -> :sswitch_4
        -0x41a91745 -> :sswitch_7
        -0x2f1f8925 -> :sswitch_2
        0x23640cb -> :sswitch_8
        0x1893e7dc -> :sswitch_f
        0x215ea710 -> :sswitch_e
        0x291d8de0 -> :sswitch_12
        0x54df6484 -> :sswitch_5
        0x6173544c -> :sswitch_1
        0x6ecd2753 -> :sswitch_6
        0x7c660157 -> :sswitch_10
        0x7d405224 -> :sswitch_11
    .end sparse-switch
.end method
