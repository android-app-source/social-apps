.class public final LX/6ZW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/6ZS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Zb;


# direct methods
.method public constructor <init>(LX/6Zb;)V
    .locals 0

    .prologue
    .line 1111226
    iput-object p1, p0, LX/6ZW;->a:LX/6Zb;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1111203
    iget-object v0, p0, LX/6ZW;->a:LX/6Zb;

    sget-object v1, LX/6ZY;->UNKNOWN_FAILURE:LX/6ZY;

    invoke-static {v0, v1}, LX/6Zb;->a$redex0(LX/6Zb;LX/6ZY;)V

    .line 1111204
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1111205
    check-cast p1, LX/6ZS;

    .line 1111206
    iget-object v0, p0, LX/6ZW;->a:LX/6Zb;

    .line 1111207
    iget-boolean v1, v0, LX/6Zb;->h:Z

    if-nez v1, :cond_0

    iget-object v1, v0, LX/6Zb;->i:LX/6ZY;

    if-eqz v1, :cond_1

    .line 1111208
    :cond_0
    :goto_0
    return-void

    .line 1111209
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111210
    sget-object v1, LX/6ZX;->a:[I

    .line 1111211
    iget-object v2, p1, LX/6ZS;->b:LX/6ZT;

    move-object v2, v2

    .line 1111212
    invoke-virtual {v2}, LX/6ZT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1111213
    sget-object v1, LX/6ZY;->DIALOG_NOT_POSSIBLE:LX/6ZY;

    invoke-static {v0, v1}, LX/6Zb;->a$redex0(LX/6Zb;LX/6ZY;)V

    goto :goto_0

    .line 1111214
    :pswitch_0
    sget-object v1, LX/6ZY;->DIALOG_NOT_NEEDED:LX/6ZY;

    invoke-static {v0, v1}, LX/6Zb;->a$redex0(LX/6Zb;LX/6ZY;)V

    goto :goto_0

    .line 1111215
    :pswitch_1
    iget-object v1, v0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    const/16 v2, 0x136f

    const/4 v3, 0x0

    .line 1111216
    iget-object v4, p1, LX/6ZS;->b:LX/6ZT;

    sget-object v5, LX/6ZT;->EASY_RESOLUTION_POSSIBLE:LX/6ZT;

    if-eq v4, v5, :cond_3

    .line 1111217
    :goto_1
    move v1, v3

    .line 1111218
    if-eqz v1, :cond_2

    .line 1111219
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/6Zb;->h:Z

    goto :goto_0

    .line 1111220
    :cond_2
    sget-object v1, LX/6ZY;->UNKNOWN_FAILURE:LX/6ZY;

    invoke-static {v0, v1}, LX/6Zb;->a$redex0(LX/6Zb;LX/6ZY;)V

    goto :goto_0

    .line 1111221
    :cond_3
    sget-object v4, LX/6ZT;->EASY_RESOLUTION_ATTEMPTED:LX/6ZT;

    iput-object v4, p1, LX/6ZS;->b:LX/6ZT;

    .line 1111222
    :try_start_0
    iget-object v4, p1, LX/6ZS;->a:Lcom/google/android/gms/location/LocationSettingsResult;

    invoke-virtual {v4}, Lcom/google/android/gms/location/LocationSettingsResult;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Lcom/google/android/gms/common/api/Status;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1111223
    const/4 v3, 0x1

    goto :goto_1

    .line 1111224
    :catch_0
    move-exception v4

    .line 1111225
    sget-object v5, LX/6ZU;->a:Ljava/lang/Class;

    const-string v6, "Error starting google play services location dialog"

    new-array p0, v3, [Ljava/lang/Object;

    invoke-static {v5, v4, v6, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
