.class public LX/6Nv;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchChatContextParams;",
        "Lcom/facebook/contacts/server/FetchChatContextResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/0SG;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0y3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084024
    const-class v0, LX/6Nv;

    sput-object v0, LX/6Nv;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/0SG;LX/0Or;LX/0y3;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/protocol/annotations/IsNearbyInChatContextEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sO;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0y3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084025
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 1084026
    iput-object p2, p0, LX/6Nv;->c:LX/0SG;

    .line 1084027
    iput-object p3, p0, LX/6Nv;->d:LX/0Or;

    .line 1084028
    iput-object p4, p0, LX/6Nv;->e:LX/0y3;

    .line 1084029
    return-void
.end method

.method private d()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1084030
    iget-object v0, p0, LX/6Nv;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Nv;->e:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    .line 1084031
    sget-object v0, LX/6Lu;->a:LX/0Rf;

    .line 1084032
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6Lu;->b:LX/0Rf;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1084033
    const-class v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$FetchChatContextsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$FetchChatContextsQueryModel;

    .line 1084034
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$FetchChatContextsQueryModel;->a()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$FetchChatContextsQueryModel$FriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$FetchChatContextsQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 1084035
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1084036
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1084037
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Lx;

    .line 1084038
    invoke-interface {v0}, LX/6Lx;->c()Ljava/lang/String;

    move-result-object v3

    .line 1084039
    invoke-interface {v0}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v4

    .line 1084040
    if-eqz v4, :cond_0

    .line 1084041
    invoke-direct {p0}, LX/6Nv;->d()LX/0Rf;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1084042
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 1084043
    :cond_1
    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v4, v5, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1084044
    :cond_2
    new-instance v0, Lcom/facebook/contacts/server/FetchChatContextResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/6Nv;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/facebook/contacts/server/FetchChatContextResult;-><init>(LX/0ta;JLX/0P1;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1084045
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1084046
    check-cast p1, Lcom/facebook/contacts/server/FetchChatContextParams;

    .line 1084047
    new-instance v0, LX/6Lv;

    invoke-direct {v0}, LX/6Lv;-><init>()V

    move-object v2, v0

    .line 1084048
    const-string v0, "context_types"

    invoke-direct {p0}, LX/6Nv;->d()LX/0Rf;

    move-result-object v1

    .line 1084049
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084050
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1084051
    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 1084052
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1084053
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v1, v3

    .line 1084054
    invoke-virtual {v2, v0, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1084055
    const-string v0, "friends_count"

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1084056
    const-string v0, "order_friends_by"

    const-string v1, "featured"

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1084057
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/contacts/server/FetchChatContextParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1084058
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchChatContextParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 1084059
    const-string v1, "latitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "longitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "accuracy"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "timestamp"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1084060
    :cond_1
    return-object v2
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1084061
    check-cast p1, Lcom/facebook/contacts/server/FetchChatContextParams;

    .line 1084062
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/facebook/contacts/server/FetchChatContextParams;->b:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
