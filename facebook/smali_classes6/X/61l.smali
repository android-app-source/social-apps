.class public LX/61l;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1040558
    const-string v0, "video/avc"

    const-string v1, "video/mp4"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/61l;->a:LX/0Rf;

    .line 1040559
    const-string v0, "audio/3gpp"

    const-string v1, "audio/amr-wb"

    const-string v2, "audio/mp4a"

    const-string v3, "audio/vorbis"

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/61l;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040561
    iput-object p1, p0, LX/61l;->c:LX/03V;

    .line 1040562
    return-void
.end method

.method public static b(LX/0QB;)LX/61l;
    .locals 2

    .prologue
    .line 1040563
    new-instance v1, LX/61l;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, LX/61l;-><init>(LX/03V;)V

    .line 1040564
    return-object v1
.end method

.method private static c(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/61k;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1040565
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1040566
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61k;

    .line 1040567
    iget-object v0, v0, LX/61k;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1040568
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " tracks: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-static {v2}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;
    .locals 6

    .prologue
    .line 1040569
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1040570
    invoke-virtual {p1}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->f()I

    move-result v2

    .line 1040571
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1040572
    invoke-virtual {p1, v0}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a(I)Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    move-result-object v3

    .line 1040573
    const-string v4, "mime"

    invoke-virtual {v3, v4}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1040574
    if-eqz v4, :cond_0

    const-string v5, "video/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1040575
    new-instance v5, LX/61k;

    invoke-direct {v5, v4, v3, v0}, LX/61k;-><init>(Ljava/lang/String;Lcom/facebook/ffmpeg/FFMpegMediaFormat;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040576
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1040577
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1040578
    new-instance v0, LX/61j;

    const-string v1, "No video track"

    invoke-direct {v0, v1}, LX/61j;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040579
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61k;

    .line 1040580
    sget-object v3, LX/61l;->a:LX/0Rf;

    iget-object v4, v0, LX/61k;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1040581
    :goto_1
    move-object v0, v0

    .line 1040582
    if-nez v0, :cond_4

    .line 1040583
    new-instance v0, LX/61j;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported video format. Contained "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/61l;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/61j;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040584
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 1040585
    iget-object v2, p0, LX/61l;->c:LX/03V;

    const-string v3, "FFMpegBasedVideoTrackExtractor_multiple_video_tracks"

    invoke-static {v1}, LX/61l;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040586
    :cond_5
    return-object v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;
    .locals 6

    .prologue
    .line 1040587
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1040588
    invoke-virtual {p1}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->f()I

    move-result v2

    .line 1040589
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1040590
    invoke-virtual {p1, v0}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a(I)Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    move-result-object v3

    .line 1040591
    const-string v4, "mime"

    invoke-virtual {v3, v4}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1040592
    if-eqz v4, :cond_0

    const-string v5, "audio/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1040593
    new-instance v5, LX/61k;

    invoke-direct {v5, v4, v3, v0}, LX/61k;-><init>(Ljava/lang/String;Lcom/facebook/ffmpeg/FFMpegMediaFormat;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040594
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1040595
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1040596
    const/4 v0, 0x0

    .line 1040597
    :cond_2
    :goto_1
    return-object v0

    .line 1040598
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61k;

    .line 1040599
    sget-object v3, LX/61l;->b:LX/0Rf;

    iget-object v4, v0, LX/61k;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1040600
    :goto_2
    move-object v0, v0

    .line 1040601
    if-nez v0, :cond_5

    .line 1040602
    new-instance v0, LX/61j;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported audio codec. Contained "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/61l;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/61j;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040603
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 1040604
    iget-object v2, p0, LX/61l;->c:LX/03V;

    const-string v3, "FFMpegBasedVideoTrackExtractor_multiple_audio_tracks"

    invoke-static {v1}, LX/61l;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method
