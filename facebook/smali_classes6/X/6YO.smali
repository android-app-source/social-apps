.class public final LX/6YO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment$ScreenController;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/6Ya;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/0Ot;)V
    .locals 1
    .param p2    # LX/0Ot;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment$ScreenController;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1109928
    iput-object p1, p0, LX/6YO;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109929
    iput-object v0, p0, LX/6YO;->c:LX/6Ya;

    .line 1109930
    iput-object v0, p0, LX/6YO;->d:Landroid/view/View;

    .line 1109931
    iput-object p2, p0, LX/6YO;->b:LX/0Ot;

    .line 1109932
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    .line 1109933
    iget-object v0, p0, LX/6YO;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1109934
    iget-object v0, p0, LX/6YO;->c:LX/6Ya;

    if-nez v0, :cond_0

    .line 1109935
    iget-object v0, p0, LX/6YO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ya;

    iput-object v0, p0, LX/6YO;->c:LX/6Ya;

    .line 1109936
    iget-object v0, p0, LX/6YO;->c:LX/6Ya;

    iget-object v1, p0, LX/6YO;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    iget-object v2, p0, LX/6YO;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-static {v2}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;)Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Ya;->a(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1109937
    :cond_0
    iget-object v0, p0, LX/6YO;->c:LX/6Ya;

    move-object v0, v0

    .line 1109938
    iget-object v1, p0, LX/6YO;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1109939
    invoke-virtual {v0, v1}, LX/6Ya;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/6YO;->d:Landroid/view/View;

    .line 1109940
    :cond_1
    iget-object v0, p0, LX/6YO;->d:Landroid/view/View;

    return-object v0
.end method
