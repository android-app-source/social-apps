.class public final LX/6GU;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/chooser/ChooserOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/chooser/ChooserOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070562
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1070563
    iput-object p1, p0, LX/6GU;->a:LX/0Px;

    .line 1070564
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/bugreporter/activity/chooser/ChooserOption;
    .locals 1

    .prologue
    .line 1070565
    iget-object v0, p0, LX/6GU;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1070566
    iget-object v0, p0, LX/6GU;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1070567
    invoke-virtual {p0, p1}, LX/6GU;->a(I)Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1070568
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1070569
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1070570
    if-nez p2, :cond_0

    .line 1070571
    const v1, 0x7f030291

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1070572
    :cond_0
    invoke-virtual {p0, p1}, LX/6GU;->a(I)Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v2

    .line 1070573
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1070574
    const v0, 0x7f0d02c4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1070575
    const v1, 0x7f0d0945

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1070576
    iget v3, v2, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->b:I

    move v3, v3

    .line 1070577
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1070578
    iget v0, v2, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->c:I

    move v0, v0

    .line 1070579
    if-eqz v0, :cond_1

    .line 1070580
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1070581
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1070582
    :goto_0
    return-object p2

    .line 1070583
    :cond_1
    const-string v0, ""

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070584
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
