.class public final LX/6dq;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0U1;

.field public static final B:LX/0U1;

.field public static final C:LX/0U1;

.field public static final D:LX/0U1;

.field public static final E:LX/0U1;

.field public static final F:LX/0U1;

.field public static final G:LX/0U1;

.field public static final H:LX/0U1;

.field public static final I:LX/0U1;

.field public static final J:LX/0U1;

.field public static final K:LX/0U1;

.field public static final L:LX/0U1;

.field public static final M:LX/0U1;

.field public static final N:LX/0U1;

.field public static final O:LX/0U1;

.field public static final P:LX/0U1;

.field public static final Q:LX/0U1;

.field public static final R:LX/0U1;

.field public static final S:LX/0U1;

.field public static final T:LX/0U1;

.field public static final U:LX/0U1;

.field public static final V:LX/0U1;

.field public static final W:LX/0U1;

.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1116428
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->a:LX/0U1;

    .line 1116429
    new-instance v0, LX/0U1;

    const-string v1, "legacy_thread_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->b:LX/0U1;

    .line 1116430
    new-instance v0, LX/0U1;

    const-string v1, "action_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->c:LX/0U1;

    .line 1116431
    new-instance v0, LX/0U1;

    const-string v1, "refetch_action_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->d:LX/0U1;

    .line 1116432
    new-instance v0, LX/0U1;

    const-string v1, "last_visible_action_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->e:LX/0U1;

    .line 1116433
    new-instance v0, LX/0U1;

    const-string v1, "sequence_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->f:LX/0U1;

    .line 1116434
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->g:LX/0U1;

    .line 1116435
    new-instance v0, LX/0U1;

    const-string v1, "senders"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->h:LX/0U1;

    .line 1116436
    new-instance v0, LX/0U1;

    const-string v1, "snippet"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->i:LX/0U1;

    .line 1116437
    new-instance v0, LX/0U1;

    const-string v1, "snippet_sender"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->j:LX/0U1;

    .line 1116438
    new-instance v0, LX/0U1;

    const-string v1, "admin_snippet"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->k:LX/0U1;

    .line 1116439
    new-instance v0, LX/0U1;

    const-string v1, "timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->l:LX/0U1;

    .line 1116440
    new-instance v0, LX/0U1;

    const-string v1, "last_read_timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->m:LX/0U1;

    .line 1116441
    new-instance v0, LX/0U1;

    const-string v1, "approx_total_message_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->n:LX/0U1;

    .line 1116442
    new-instance v0, LX/0U1;

    const-string v1, "unread_message_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->o:LX/0U1;

    .line 1116443
    new-instance v0, LX/0U1;

    const-string v1, "last_fetch_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->p:LX/0U1;

    .line 1116444
    new-instance v0, LX/0U1;

    const-string v1, "pic_hash"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->q:LX/0U1;

    .line 1116445
    new-instance v0, LX/0U1;

    const-string v1, "pic"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->r:LX/0U1;

    .line 1116446
    new-instance v0, LX/0U1;

    const-string v1, "can_reply_to"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->s:LX/0U1;

    .line 1116447
    new-instance v0, LX/0U1;

    const-string v1, "cannot_reply_reason"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->t:LX/0U1;

    .line 1116448
    new-instance v0, LX/0U1;

    const-string v1, "mute_until"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->u:LX/0U1;

    .line 1116449
    new-instance v0, LX/0U1;

    const-string v1, "is_subscribed"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->v:LX/0U1;

    .line 1116450
    new-instance v0, LX/0U1;

    const-string v1, "folder"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->w:LX/0U1;

    .line 1116451
    new-instance v0, LX/0U1;

    const-string v1, "draft"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->x:LX/0U1;

    .line 1116452
    new-instance v0, LX/0U1;

    const-string v1, "missed_call_status"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->y:LX/0U1;

    .line 1116453
    new-instance v0, LX/0U1;

    const-string v1, "me_bubble_color"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->z:LX/0U1;

    .line 1116454
    new-instance v0, LX/0U1;

    const-string v1, "other_bubble_color"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->A:LX/0U1;

    .line 1116455
    new-instance v0, LX/0U1;

    const-string v1, "wallpaper_color"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->B:LX/0U1;

    .line 1116456
    new-instance v0, LX/0U1;

    const-string v1, "last_fetch_action_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->C:LX/0U1;

    .line 1116457
    new-instance v0, LX/0U1;

    const-string v1, "initial_fetch_complete"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->D:LX/0U1;

    .line 1116458
    new-instance v0, LX/0U1;

    const-string v1, "custom_like_emoji"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->E:LX/0U1;

    .line 1116459
    new-instance v0, LX/0U1;

    const-string v1, "outgoing_message_lifetime"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->F:LX/0U1;

    .line 1116460
    new-instance v0, LX/0U1;

    const-string v1, "custom_nicknames"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->G:LX/0U1;

    .line 1116461
    new-instance v0, LX/0U1;

    const-string v1, "invite_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->H:LX/0U1;

    .line 1116462
    new-instance v0, LX/0U1;

    const-string v1, "is_last_message_sponsored"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->I:LX/0U1;

    .line 1116463
    new-instance v0, LX/0U1;

    const-string v1, "group_chat_rank"

    const-string v2, "FLOAT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->J:LX/0U1;

    .line 1116464
    new-instance v0, LX/0U1;

    const-string v1, "game_data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->K:LX/0U1;

    .line 1116465
    new-instance v0, LX/0U1;

    const-string v1, "is_joinable"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->L:LX/0U1;

    .line 1116466
    new-instance v0, LX/0U1;

    const-string v1, "requires_approval"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->M:LX/0U1;

    .line 1116467
    new-instance v0, LX/0U1;

    const-string v1, "rtc_call_info"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->N:LX/0U1;

    .line 1116468
    new-instance v0, LX/0U1;

    const-string v1, "last_message_commerce_message_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->O:LX/0U1;

    .line 1116469
    new-instance v0, LX/0U1;

    const-string v1, "is_thread_queue_enabled"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->P:LX/0U1;

    .line 1116470
    new-instance v0, LX/0U1;

    const-string v1, "group_description"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->Q:LX/0U1;

    .line 1116471
    new-instance v0, LX/0U1;

    const-string v1, "media_preview"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->R:LX/0U1;

    .line 1116472
    new-instance v0, LX/0U1;

    const-string v1, "booking_requests"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->S:LX/0U1;

    .line 1116473
    new-instance v0, LX/0U1;

    const-string v1, "last_call_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->T:LX/0U1;

    .line 1116474
    new-instance v0, LX/0U1;

    const-string v1, "is_discoverable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->U:LX/0U1;

    .line 1116475
    new-instance v0, LX/0U1;

    const-string v1, "last_sponsored_message_call_to_action"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->V:LX/0U1;

    .line 1116476
    new-instance v0, LX/0U1;

    const-string v1, "montage_thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dq;->W:LX/0U1;

    return-void
.end method
