.class public final LX/5kg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 995911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 995912
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 995913
    iget-object v1, p0, LX/5kg;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 995914
    iget-object v2, p0, LX/5kg;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 995915
    iget-object v3, p0, LX/5kg;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 995916
    iget-object v4, p0, LX/5kg;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 995917
    const/4 v5, 0x6

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 995918
    iget-boolean v5, p0, LX/5kg;->a:Z

    invoke-virtual {v0, v7, v5}, LX/186;->a(IZ)V

    .line 995919
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 995920
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 995921
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 995922
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 995923
    const/4 v1, 0x5

    iget-wide v2, p0, LX/5kg;->f:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 995924
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 995925
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 995926
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 995927
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 995928
    new-instance v0, LX/15i;

    move-object v2, v6

    move-object v3, v6

    move v4, v8

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 995929
    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;-><init>(LX/15i;)V

    .line 995930
    return-object v1
.end method
