.class public LX/5TM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/RetailAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:I

.field public p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Z

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 921750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921751
    return-void
.end method


# virtual methods
.method public final e(Ljava/lang/String;)LX/5TM;
    .locals 1

    .prologue
    .line 921746
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/5TM;->e:Landroid/net/Uri;

    .line 921747
    return-object p0

    .line 921748
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Lcom/facebook/messaging/business/commerce/model/retail/Receipt;
    .locals 1

    .prologue
    .line 921749
    new-instance v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;-><init>(LX/5TM;)V

    return-object v0
.end method
