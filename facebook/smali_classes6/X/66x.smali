.class public final LX/66x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public final synthetic a:LX/65D;

.field public final synthetic b:LX/65g;


# direct methods
.method public constructor <init>(LX/65g;LX/65D;)V
    .locals 0

    .prologue
    .line 1051382
    iput-object p1, p0, LX/66x;->b:LX/65g;

    iput-object p2, p0, LX/66x;->a:LX/65D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 4

    .prologue
    .line 1051383
    const/4 v1, 0x0

    .line 1051384
    iget-object v0, p0, LX/66x;->b:LX/65g;

    invoke-virtual {v0}, LX/65g;->c()V

    .line 1051385
    :try_start_0
    iget-object v0, p0, LX/66x;->a:LX/65D;

    invoke-interface {v0, p1, p2, p3}, LX/65D;->a(LX/672;J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1051386
    iget-object v2, p0, LX/66x;->b:LX/65g;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/65g;->a(Z)V

    .line 1051387
    return-wide v0

    .line 1051388
    :catch_0
    move-exception v0

    .line 1051389
    :try_start_1
    iget-object v2, p0, LX/66x;->b:LX/65g;

    invoke-virtual {v2, v0}, LX/65g;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1051390
    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/66x;->b:LX/65g;

    invoke-virtual {v2, v1}, LX/65g;->a(Z)V

    throw v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1051391
    iget-object v0, p0, LX/66x;->b:LX/65g;

    return-object v0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 1051392
    :try_start_0
    iget-object v0, p0, LX/66x;->a:LX/65D;

    invoke-interface {v0}, LX/65D;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051393
    iget-object v0, p0, LX/66x;->b:LX/65g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/65g;->a(Z)V

    .line 1051394
    return-void

    .line 1051395
    :catch_0
    move-exception v0

    .line 1051396
    :try_start_1
    iget-object v1, p0, LX/66x;->b:LX/65g;

    invoke-virtual {v1, v0}, LX/65g;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1051397
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/66x;->b:LX/65g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/65g;->a(Z)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1051398
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AsyncTimeout.source("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/66x;->a:LX/65D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
