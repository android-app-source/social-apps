.class public LX/6X8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/FeedUnit;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 1107860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1107861
    iput-object p1, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 1107862
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;
    .locals 3

    .prologue
    .line 1107851
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107852
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1107853
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107854
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107855
    new-instance v1, LX/6XB;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, v0}, LX/6XB;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, v1

    .line 1107856
    :goto_0
    return-object v0

    .line 1107857
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;

    if-nez v0, :cond_1

    .line 1107858
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Should only use FeedUnitMutator for feedunits deriving from BaseModel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1107859
    :cond_1
    new-instance v1, LX/6X8;

    invoke-interface {p0}, LX/0jT;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {v1, v0}, LX/6X8;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static a(LX/16f;Z)V
    .locals 3

    .prologue
    .line 1107849
    const-string v0, "local_is_completed"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p0, v0, v1, v2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 1107850
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/HideableUnit;I)V
    .locals 3

    .prologue
    .line 1107847
    const-string v0, "local_story_visible_height"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p0, v0, v1, v2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 1107848
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/HideableUnit;Lcom/facebook/graphql/enums/StoryVisibility;)V
    .locals 3

    .prologue
    .line 1107844
    const-string v1, "local_story_visibility"

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    invoke-interface {p0, v1, v0, v2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 1107845
    return-void

    .line 1107846
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/6X8;
    .locals 3

    .prologue
    .line 1107840
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-nez v0, :cond_0

    .line 1107841
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Should only call setVisibleHeight on a HideableUnit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1107842
    :cond_0
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-static {v0, p1}, LX/6X8;->a(Lcom/facebook/graphql/model/HideableUnit;I)V

    .line 1107843
    return-object p0
.end method

.method public final a(JLcom/facebook/graphql/enums/StoryVisibility;I)LX/6X8;
    .locals 1

    .prologue
    .line 1107836
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0, p1, p2}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 1107837
    invoke-virtual {p0, p3}, LX/6X8;->a(Lcom/facebook/graphql/enums/StoryVisibility;)LX/6X8;

    .line 1107838
    invoke-virtual {p0, p4}, LX/6X8;->a(I)LX/6X8;

    .line 1107839
    return-object p0
.end method

.method public final a(LX/0Px;)LX/6X8;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6X8;"
        }
    .end annotation

    .prologue
    .line 1107833
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, LX/16f;

    if-eqz v0, :cond_0

    .line 1107834
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, LX/16f;

    const-string v1, "local_valid_items"

    invoke-interface {v0, v1, p1}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1107835
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)LX/6X8;
    .locals 4

    .prologue
    .line 1107827
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v0, :cond_0

    .line 1107828
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 1107829
    const-string v2, "local_last_negative_feedback_action_type"

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 1107830
    return-object p0

    .line 1107831
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setLastNegativeFeedbackActionType called on wrong feedunit type:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1107832
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/StoryVisibility;)LX/6X8;
    .locals 3

    .prologue
    .line 1107815
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-nez v0, :cond_0

    .line 1107816
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Should only call setStoryVisibility on a HideableUnit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1107817
    :cond_0
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-static {v0, p1}, LX/6X8;->a(Lcom/facebook/graphql/model/HideableUnit;Lcom/facebook/graphql/enums/StoryVisibility;)V

    .line 1107818
    return-object p0
.end method

.method public final a(Z)LX/6X8;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1107820
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    if-eqz v0, :cond_2

    .line 1107821
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    iget-object v1, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v0, v1}, LX/6X8;->a(LX/16f;Z)V

    .line 1107822
    :goto_1
    return-object p0

    :cond_1
    move v1, v2

    .line 1107823
    goto :goto_0

    .line 1107824
    :cond_2
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    if-eqz v0, :cond_5

    .line 1107825
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iget-object v1, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    move v2, v3

    :cond_4
    invoke-static {v0, v2}, LX/6X8;->a(LX/16f;Z)V

    goto :goto_1

    .line 1107826
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCompleted called on wrong type of feedunit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 1107819
    iget-object v0, p0, LX/6X8;->a:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method
