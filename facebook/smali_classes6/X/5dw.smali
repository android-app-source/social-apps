.class public final enum LX/5dw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5dw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5dw;

.field public static final enum CREATE_EVENT:LX/5dw;

.field public static final enum LOCATION:LX/5dw;

.field public static final enum M:LX/5dw;

.field public static final enum OPEN_NATIVE:LX/5dw;

.field public static final enum P2P_PAYMENT:LX/5dw;

.field public static final enum POLL:LX/5dw;

.field public static final enum RIDE_SERVICE:LX/5dw;

.field public static final enum SCHEDULE_CALL:LX/5dw;

.field public static final enum STICKER:LX/5dw;

.field public static final enum TEXT:LX/5dw;

.field public static final enum UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE:LX/5dw;


# instance fields
.field public final dbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 966362
    new-instance v0, LX/5dw;

    const-string v1, "TEXT"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v4, v2}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->TEXT:LX/5dw;

    .line 966363
    new-instance v0, LX/5dw;

    const-string v1, "OPEN_NATIVE"

    const-string v2, "OPEN_NATIVE"

    invoke-direct {v0, v1, v5, v2}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->OPEN_NATIVE:LX/5dw;

    .line 966364
    new-instance v0, LX/5dw;

    const-string v1, "M"

    const-string v2, "M"

    invoke-direct {v0, v1, v6, v2}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->M:LX/5dw;

    .line 966365
    new-instance v0, LX/5dw;

    const-string v1, "LOCATION"

    const-string v2, "LOCATION"

    invoke-direct {v0, v1, v7, v2}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->LOCATION:LX/5dw;

    .line 966366
    new-instance v0, LX/5dw;

    const-string v1, "STICKER"

    const-string v2, "STICKER"

    invoke-direct {v0, v1, v8, v2}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->STICKER:LX/5dw;

    .line 966367
    new-instance v0, LX/5dw;

    const-string v1, "P2P_PAYMENT"

    const/4 v2, 0x5

    const-string v3, "P2P_PAYMENT"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->P2P_PAYMENT:LX/5dw;

    .line 966368
    new-instance v0, LX/5dw;

    const-string v1, "CREATE_EVENT"

    const/4 v2, 0x6

    const-string v3, "CREATE_EVENT"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->CREATE_EVENT:LX/5dw;

    .line 966369
    new-instance v0, LX/5dw;

    const-string v1, "RIDE_SERVICE"

    const/4 v2, 0x7

    const-string v3, "RIDE_SERVICE"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->RIDE_SERVICE:LX/5dw;

    .line 966370
    new-instance v0, LX/5dw;

    const-string v1, "POLL"

    const/16 v2, 0x8

    const-string v3, "POLL"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->POLL:LX/5dw;

    .line 966371
    new-instance v0, LX/5dw;

    const-string v1, "SCHEDULE_CALL"

    const/16 v2, 0x9

    const-string v3, "SCHEDULE_CALL"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->SCHEDULE_CALL:LX/5dw;

    .line 966372
    new-instance v0, LX/5dw;

    const-string v1, "UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE"

    const/16 v2, 0xa

    const-string v3, "UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE"

    invoke-direct {v0, v1, v2, v3}, LX/5dw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5dw;->UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE:LX/5dw;

    .line 966373
    const/16 v0, 0xb

    new-array v0, v0, [LX/5dw;

    sget-object v1, LX/5dw;->TEXT:LX/5dw;

    aput-object v1, v0, v4

    sget-object v1, LX/5dw;->OPEN_NATIVE:LX/5dw;

    aput-object v1, v0, v5

    sget-object v1, LX/5dw;->M:LX/5dw;

    aput-object v1, v0, v6

    sget-object v1, LX/5dw;->LOCATION:LX/5dw;

    aput-object v1, v0, v7

    sget-object v1, LX/5dw;->STICKER:LX/5dw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5dw;->P2P_PAYMENT:LX/5dw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5dw;->CREATE_EVENT:LX/5dw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5dw;->RIDE_SERVICE:LX/5dw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5dw;->POLL:LX/5dw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5dw;->SCHEDULE_CALL:LX/5dw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5dw;->UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE:LX/5dw;

    aput-object v2, v0, v1

    sput-object v0, LX/5dw;->$VALUES:[LX/5dw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 966374
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 966375
    iput-object p3, p0, LX/5dw;->dbValue:Ljava/lang/String;

    .line 966376
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)LX/5dw;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966377
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966378
    sget-object v0, LX/5dw;->UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE:LX/5dw;

    .line 966379
    :cond_0
    :goto_0
    return-object v0

    .line 966380
    :cond_1
    invoke-static {}, LX/5dw;->values()[LX/5dw;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 966381
    iget-object v4, v0, LX/5dw;->dbValue:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 966382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 966383
    :cond_2
    sget-object v0, LX/5dw;->UNSET_OR_UNRECOGNIZED_QUICK_REPLY_TYPE:LX/5dw;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5dw;
    .locals 1

    .prologue
    .line 966384
    const-class v0, LX/5dw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5dw;

    return-object v0
.end method

.method public static values()[LX/5dw;
    .locals 1

    .prologue
    .line 966385
    sget-object v0, LX/5dw;->$VALUES:[LX/5dw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5dw;

    return-object v0
.end method
