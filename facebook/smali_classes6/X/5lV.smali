.class public final LX/5lV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 998486
    const/16 v25, 0x0

    .line 998487
    const/16 v24, 0x0

    .line 998488
    const/16 v23, 0x0

    .line 998489
    const/16 v22, 0x0

    .line 998490
    const/16 v21, 0x0

    .line 998491
    const/16 v20, 0x0

    .line 998492
    const-wide/16 v18, 0x0

    .line 998493
    const-wide/16 v16, 0x0

    .line 998494
    const-wide/16 v14, 0x0

    .line 998495
    const-wide/16 v12, 0x0

    .line 998496
    const/4 v11, 0x0

    .line 998497
    const/4 v10, 0x0

    .line 998498
    const/4 v9, 0x0

    .line 998499
    const/4 v8, 0x0

    .line 998500
    const/4 v7, 0x0

    .line 998501
    const/4 v6, 0x0

    .line 998502
    const/4 v5, 0x0

    .line 998503
    const/4 v4, 0x0

    .line 998504
    const/4 v3, 0x0

    .line 998505
    const/4 v2, 0x0

    .line 998506
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_16

    .line 998507
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 998508
    const/4 v2, 0x0

    .line 998509
    :goto_0
    return v2

    .line 998510
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_b

    .line 998511
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 998512
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 998513
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 998514
    const-string v27, "cropped_area_image_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 998515
    const/4 v2, 0x1

    .line 998516
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v26, v14

    move v14, v2

    goto :goto_1

    .line 998517
    :cond_1
    const-string v27, "cropped_area_image_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 998518
    const/4 v2, 0x1

    .line 998519
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v25, v13

    move v13, v2

    goto :goto_1

    .line 998520
    :cond_2
    const-string v27, "cropped_area_left_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 998521
    const/4 v2, 0x1

    .line 998522
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v24, v12

    move v12, v2

    goto :goto_1

    .line 998523
    :cond_3
    const-string v27, "cropped_area_top_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 998524
    const/4 v2, 0x1

    .line 998525
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v23, v11

    move v11, v2

    goto :goto_1

    .line 998526
    :cond_4
    const-string v27, "full_pano_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 998527
    const/4 v2, 0x1

    .line 998528
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto :goto_1

    .line 998529
    :cond_5
    const-string v27, "full_pano_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 998530
    const/4 v2, 0x1

    .line 998531
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v15, v6

    move v6, v2

    goto/16 :goto_1

    .line 998532
    :cond_6
    const-string v27, "initial_view_heading_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 998533
    const/4 v2, 0x1

    .line 998534
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 998535
    :cond_7
    const-string v27, "initial_view_pitch_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 998536
    const/4 v2, 0x1

    .line 998537
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v20

    move v10, v2

    goto/16 :goto_1

    .line 998538
    :cond_8
    const-string v27, "initial_view_roll_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 998539
    const/4 v2, 0x1

    .line 998540
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 998541
    :cond_9
    const-string v27, "initial_view_vertical_fov_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 998542
    const/4 v2, 0x1

    .line 998543
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 998544
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 998545
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 998546
    if-eqz v14, :cond_c

    .line 998547
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 998548
    :cond_c
    if-eqz v13, :cond_d

    .line 998549
    const/4 v2, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 998550
    :cond_d
    if-eqz v12, :cond_e

    .line 998551
    const/4 v2, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 998552
    :cond_e
    if-eqz v11, :cond_f

    .line 998553
    const/4 v2, 0x3

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 998554
    :cond_f
    if-eqz v7, :cond_10

    .line 998555
    const/4 v2, 0x4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 998556
    :cond_10
    if-eqz v6, :cond_11

    .line 998557
    const/4 v2, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v6}, LX/186;->a(III)V

    .line 998558
    :cond_11
    if-eqz v3, :cond_12

    .line 998559
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 998560
    :cond_12
    if-eqz v10, :cond_13

    .line 998561
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 998562
    :cond_13
    if-eqz v9, :cond_14

    .line 998563
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 998564
    :cond_14
    if-eqz v8, :cond_15

    .line 998565
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 998566
    :cond_15
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_16
    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v29, v9

    move v9, v3

    move v3, v5

    move/from16 v30, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v31, v14

    move/from16 v15, v20

    move/from16 v14, v30

    move-wide/from16 v20, v16

    move-wide/from16 v16, v12

    move v13, v10

    move/from16 v12, v29

    move v10, v4

    move-wide/from16 v4, v18

    move-wide/from16 v18, v31

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 998567
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 998568
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998569
    if-eqz v0, :cond_0

    .line 998570
    const-string v1, "cropped_area_image_height_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998571
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998572
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998573
    if-eqz v0, :cond_1

    .line 998574
    const-string v1, "cropped_area_image_width_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998575
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998576
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998577
    if-eqz v0, :cond_2

    .line 998578
    const-string v1, "cropped_area_left_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998579
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998580
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998581
    if-eqz v0, :cond_3

    .line 998582
    const-string v1, "cropped_area_top_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998583
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998584
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998585
    if-eqz v0, :cond_4

    .line 998586
    const-string v1, "full_pano_height_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998587
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998588
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 998589
    if-eqz v0, :cond_5

    .line 998590
    const-string v1, "full_pano_width_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998591
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 998592
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 998593
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 998594
    const-string v2, "initial_view_heading_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998595
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 998596
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 998597
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 998598
    const-string v2, "initial_view_pitch_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998599
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 998600
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 998601
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_8

    .line 998602
    const-string v2, "initial_view_roll_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998603
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 998604
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 998605
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_9

    .line 998606
    const-string v2, "initial_view_vertical_fov_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 998607
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 998608
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 998609
    return-void
.end method
