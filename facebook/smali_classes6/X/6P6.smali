.class public final LX/6P6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlace;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6P8;


# direct methods
.method public constructor <init>(LX/6P8;)V
    .locals 0

    .prologue
    .line 1086013
    iput-object p1, p0, LX/6P6;->a:LX/6P8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1086014
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1086015
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6P6;->a:LX/6P8;

    iget-object v0, v0, LX/6P8;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086016
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-static {p1}, LX/4Y6;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/4Y6;

    move-result-object v0

    iget-object v1, p0, LX/6P6;->a:LX/6P8;

    iget-object v1, v1, LX/6P8;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086017
    iput-object v1, v0, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086018
    move-object v0, v0

    .line 1086019
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086020
    const/4 v0, 0x0

    return v0
.end method
