.class public LX/6bK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2MV;

.field private final b:LX/7TG;

.field private final c:LX/1Er;

.field public d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7TD;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/7SH;

.field private f:Ljava/io/File;


# direct methods
.method public constructor <init>(LX/7TG;LX/1Er;LX/2MV;LX/7SH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1113340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113341
    iput-object v0, p0, LX/6bK;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1113342
    iput-object v0, p0, LX/6bK;->f:Ljava/io/File;

    .line 1113343
    iput-object p2, p0, LX/6bK;->c:LX/1Er;

    .line 1113344
    iput-object p3, p0, LX/6bK;->a:LX/2MV;

    .line 1113345
    iput-object p1, p0, LX/6bK;->b:LX/7TG;

    .line 1113346
    iput-object p4, p0, LX/6bK;->e:LX/7SH;

    .line 1113347
    return-void
.end method

.method private static a(Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)LX/2Mf;
    .locals 2

    .prologue
    .line 1113333
    iget-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->a:Z

    if-eqz v0, :cond_1

    .line 1113334
    iget v0, p0, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Specified Transcoding"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113335
    iget v0, p0, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->b:I

    mul-int/lit16 v0, v0, 0x3e8

    .line 1113336
    invoke-static {v0}, LX/2Mf;->a(I)LX/2Mf;

    move-result-object v0

    .line 1113337
    :goto_1
    return-object v0

    .line 1113338
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1113339
    :cond_1
    invoke-static {}, LX/2Mf;->b()LX/2Mf;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(LX/6bI;IIIII)LX/7T7;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1113320
    const/4 v0, -0x1

    if-eq p4, v0, :cond_5

    move v0, p4

    .line 1113321
    :goto_0
    sget-object v3, LX/6bI;->Video:LX/6bI;

    if-ne p0, v3, :cond_3

    .line 1113322
    add-int/lit8 v3, p1, -0x1

    if-ne p2, v3, :cond_1

    move v3, v2

    .line 1113323
    :goto_1
    if-nez p2, :cond_2

    move v4, v2

    .line 1113324
    :goto_2
    if-nez v3, :cond_4

    .line 1113325
    add-int/lit8 v3, p2, 0x1

    mul-int/2addr v3, p3

    add-int v5, v3, v0

    move v3, v2

    .line 1113326
    :goto_3
    if-nez v4, :cond_0

    .line 1113327
    mul-int v4, p2, p3

    add-int p4, v4, v0

    :cond_0
    move v4, p4

    move v6, v1

    move v1, v2

    move v2, v6

    .line 1113328
    :goto_4
    new-instance v0, LX/7T7;

    invoke-direct/range {v0 .. v5}, LX/7T7;-><init>(ZZZII)V

    .line 1113329
    return-object v0

    :cond_1
    move v3, v1

    .line 1113330
    goto :goto_1

    :cond_2
    move v4, v1

    .line 1113331
    goto :goto_2

    :cond_3
    move v5, p5

    move v4, p4

    move v3, v1

    .line 1113332
    goto :goto_4

    :cond_4
    move v5, p5

    move v3, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private static a(LX/6bK;Lcom/facebook/photos/base/media/VideoItem;Ljava/io/File;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/2Md;)LX/7TH;
    .locals 4

    .prologue
    .line 1113297
    invoke-static {p1}, LX/6bO;->b(Lcom/facebook/photos/base/media/VideoItem;)Ljava/io/File;

    move-result-object v0

    .line 1113298
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v1

    .line 1113299
    iput-object v0, v1, LX/7TI;->a:Ljava/io/File;

    .line 1113300
    move-object v0, v1

    .line 1113301
    iput-object p2, v0, LX/7TI;->b:Ljava/io/File;

    .line 1113302
    move-object v0, v0

    .line 1113303
    iput-object p5, v0, LX/7TI;->c:LX/2Md;

    .line 1113304
    move-object v0, v0

    .line 1113305
    iget-object v1, p4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->d:LX/60y;

    .line 1113306
    iput-object v1, v0, LX/7TI;->h:LX/60y;

    .line 1113307
    move-object v0, v0

    .line 1113308
    invoke-virtual {p1}, Lcom/facebook/photos/base/media/VideoItem;->t()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1113309
    new-instance v1, LX/7SP;

    invoke-direct {v1}, LX/7SP;-><init>()V

    iget-object v2, p0, LX/6bK;->e:LX/7SH;

    invoke-virtual {p1}, Lcom/facebook/photos/base/media/VideoItem;->t()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1113310
    iput-object v1, v0, LX/7TI;->n:LX/0Px;

    .line 1113311
    :cond_0
    iget-object v1, p4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1113312
    if-eqz v1, :cond_2

    .line 1113313
    iget v2, v1, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    if-eqz v2, :cond_1

    .line 1113314
    iget v2, v1, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    invoke-virtual {v0, v2}, LX/7TI;->c(I)LX/7TI;

    .line 1113315
    :cond_1
    iget-object v2, v1, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    if-eqz v2, :cond_2

    .line 1113316
    iget-object v1, v1, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    .line 1113317
    iput-object v1, v0, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1113318
    :cond_2
    iget-object v1, p3, LX/6bJ;->c:LX/7T7;

    invoke-virtual {v1, v0}, LX/7T7;->a(LX/7TI;)LX/7TI;

    .line 1113319
    invoke-virtual {v0}, LX/7TI;->o()LX/7TH;

    move-result-object v0

    return-object v0
.end method

.method public static a(JIZII)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIZII)",
            "Ljava/util/List",
            "<",
            "LX/6bJ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1113228
    long-to-int v3, p0

    .line 1113229
    const/4 v0, -0x1

    if-eq p4, v0, :cond_8

    .line 1113230
    if-ltz p4, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Trim start time must be >= 0"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move v4, p4

    .line 1113231
    :goto_1
    const/4 v0, -0x2

    if-eq p5, v0, :cond_7

    .line 1113232
    int-to-long v6, p5

    cmp-long v0, v6, p0

    if-gtz v0, :cond_1

    :goto_2
    const-string v0, "Trim endtime must be less than video Duration"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move v0, p5

    .line 1113233
    :goto_3
    sub-int/2addr v0, v4

    .line 1113234
    int-to-long v4, v0

    const-wide/16 v6, 0x1388

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    .line 1113235
    const/4 v0, 0x0

    .line 1113236
    :goto_4
    return-object v0

    :cond_0
    move v0, v2

    .line 1113237
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1113238
    goto :goto_2

    .line 1113239
    :cond_2
    const/4 v1, 0x2

    .line 1113240
    int-to-long v4, v0

    const-wide/16 v6, 0x2710

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 1113241
    const/4 v1, 0x4

    .line 1113242
    :cond_3
    div-int v3, v0, v1

    .line 1113243
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1113244
    if-lez p2, :cond_4

    if-nez p3, :cond_4

    .line 1113245
    add-int/lit8 v0, v1, 0x1

    move v6, v0

    .line 1113246
    :goto_5
    if-ge v2, v6, :cond_6

    .line 1113247
    if-ge v2, v1, :cond_5

    .line 1113248
    sget-object v0, LX/6bI;->Video:LX/6bI;

    .line 1113249
    :goto_6
    new-instance v8, LX/6bJ;

    invoke-direct {v8, v0, v2}, LX/6bJ;-><init>(LX/6bI;I)V

    .line 1113250
    iget-object v0, v8, LX/6bJ;->a:LX/6bI;

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, LX/6bK;->a(LX/6bI;IIIII)LX/7T7;

    move-result-object v0

    iput-object v0, v8, LX/6bJ;->c:LX/7T7;

    .line 1113251
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1113252
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_4
    move v6, v1

    .line 1113253
    goto :goto_5

    .line 1113254
    :cond_5
    sget-object v0, LX/6bI;->Audio:LX/6bI;

    goto :goto_6

    :cond_6
    move-object v0, v7

    .line 1113255
    goto :goto_4

    :cond_7
    move v0, v3

    goto :goto_3

    :cond_8
    move v4, v2

    goto :goto_1
.end method

.method private static a(LX/6bK;Lcom/facebook/photos/base/media/VideoItem;LX/2Mf;Ljava/io/File;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)V
    .locals 50
    .param p1    # Lcom/facebook/photos/base/media/VideoItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1113263
    const-string v2, "Transcoded File Path cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113264
    const/4 v14, 0x0

    .line 1113265
    if-eqz p2, :cond_1

    move-object/from16 v7, p2

    :goto_0
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    .line 1113266
    invoke-static/range {v2 .. v7}, LX/6bK;->a(LX/6bK;Lcom/facebook/photos/base/media/VideoItem;Ljava/io/File;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/2Md;)LX/7TH;

    move-result-object v15

    .line 1113267
    :try_start_0
    move-object/from16 v0, p5

    iget-object v2, v0, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1113268
    if-nez v2, :cond_0

    .line 1113269
    invoke-static {}, Lcom/facebook/media/transcode/video/VideoEditConfig;->a()Lcom/facebook/media/transcode/video/VideoEditConfig;

    move-result-object v2

    .line 1113270
    :cond_0
    invoke-virtual {v7}, LX/2Mf;->a()LX/2Mg;

    move-result-object v3

    iget v3, v3, LX/2Mg;->b:I

    iget-boolean v4, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    iget v5, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    iget v6, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    iget-boolean v7, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    iget v8, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    iget-object v9, v2, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    move-object/from16 v0, p4

    iget v10, v0, LX/6bJ;->b:I

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->a:LX/6bI;

    invoke-virtual {v2}, LX/6bI;->getValue()I

    move-result v11

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v12, v2, LX/7T7;->d:I

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v13, v2, LX/7T7;->e:I

    move-object/from16 v2, p6

    invoke-virtual/range {v2 .. v13}, LX/6b7;->a(IZIIZILandroid/graphics/RectF;IIII)V

    .line 1113271
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6bK;->b:LX/7TG;

    invoke-virtual {v2, v15}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/6bK;->d:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/7T9; {:try_start_0 .. :try_end_0} :catch_3

    .line 1113272
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6bK;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    const v3, -0x60c1a167

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7TD;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/7T9; {:try_start_1 .. :try_end_1} :catch_3

    .line 1113273
    :try_start_2
    iget-object v0, v2, LX/7TD;->m:LX/7TE;

    move-object/from16 v49, v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/7T9; {:try_start_2 .. :try_end_2} :catch_3

    .line 1113274
    :try_start_3
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 1113275
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 1113276
    new-instance v2, LX/7T9;

    const-string v3, "empty resized file"

    invoke-direct {v2, v3}, LX/7T9;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/7T9; {:try_start_3 .. :try_end_3} :catch_4

    .line 1113277
    :catch_0
    move-exception v2

    .line 1113278
    move-object/from16 v0, p4

    iget v3, v0, LX/6bJ;->b:I

    move-object/from16 v0, p4

    iget-object v4, v0, LX/6bJ;->a:LX/6bI;

    invoke-virtual {v4}, LX/6bI;->getValue()I

    move-result v4

    move-object/from16 v0, p4

    iget-object v5, v0, LX/6bJ;->c:LX/7T7;

    iget v5, v5, LX/7T7;->d:I

    move-object/from16 v0, p4

    iget-object v6, v0, LX/6bJ;->c:LX/7T7;

    iget v6, v6, LX/7T7;->e:I

    move-object/from16 v0, p6

    invoke-virtual {v0, v3, v4, v5, v6}, LX/6b7;->a(IIII)V

    .line 1113279
    throw v2

    .line 1113280
    :cond_1
    invoke-static/range {p5 .. p5}, LX/6bK;->a(Lcom/facebook/media/transcode/video/VideoTranscodeParameters;)LX/2Mf;

    move-result-object v7

    goto/16 :goto_0

    .line 1113281
    :catch_1
    move-exception v2

    .line 1113282
    :try_start_4
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 1113283
    instance-of v2, v3, LX/7T9;

    if-eqz v2, :cond_5

    .line 1113284
    move-object v0, v3

    check-cast v0, LX/7T9;

    move-object v2, v0

    .line 1113285
    invoke-virtual {v2}, LX/7T9;->b()LX/7TE;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch LX/7T9; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v2

    .line 1113286
    :goto_1
    :try_start_5
    new-instance v4, LX/7T9;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Resizing video failed. Reason: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v3}, LX/7T9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch LX/7T9; {:try_start_5 .. :try_end_5} :catch_2

    .line 1113287
    :catch_2
    move-exception v39

    .line 1113288
    :goto_2
    if-nez v2, :cond_2

    .line 1113289
    new-instance v2, LX/7TE;

    invoke-direct {v2}, LX/7TE;-><init>()V

    .line 1113290
    :cond_2
    invoke-virtual/range {v39 .. v39}, LX/7T9;->a()Z

    move-result v4

    iget-boolean v5, v2, LX/7TE;->a:Z

    iget-boolean v6, v2, LX/7TE;->b:Z

    iget-boolean v7, v2, LX/7TE;->c:Z

    iget-boolean v8, v2, LX/7TE;->d:Z

    iget-boolean v9, v2, LX/7TE;->e:Z

    iget-boolean v10, v2, LX/7TE;->f:Z

    iget-boolean v11, v2, LX/7TE;->g:Z

    iget-wide v12, v2, LX/7TE;->h:J

    iget-wide v14, v2, LX/7TE;->i:J

    iget-wide v0, v2, LX/7TE;->j:J

    move-wide/from16 v16, v0

    iget-wide v0, v2, LX/7TE;->k:J

    move-wide/from16 v18, v0

    iget-wide v0, v2, LX/7TE;->l:J

    move-wide/from16 v20, v0

    iget-wide v0, v2, LX/7TE;->m:J

    move-wide/from16 v22, v0

    iget-wide v0, v2, LX/7TE;->n:J

    move-wide/from16 v24, v0

    iget-wide v0, v2, LX/7TE;->o:J

    move-wide/from16 v26, v0

    iget-wide v0, v2, LX/7TE;->p:J

    move-wide/from16 v28, v0

    iget-wide v0, v2, LX/7TE;->q:J

    move-wide/from16 v30, v0

    iget-object v0, v2, LX/7TE;->r:Ljava/lang/String;

    move-object/from16 v32, v0

    iget-object v0, v2, LX/7TE;->s:Ljava/lang/String;

    move-object/from16 v33, v0

    iget-object v0, v2, LX/7TE;->t:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p4

    iget v0, v0, LX/6bJ;->b:I

    move/from16 v35, v0

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->a:LX/6bI;

    invoke-virtual {v2}, LX/6bI;->getValue()I

    move-result v36

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v0, v2, LX/7T7;->d:I

    move/from16 v37, v0

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v0, v2, LX/7T7;->e:I

    move/from16 v38, v0

    move-object/from16 v3, p6

    invoke-virtual/range {v3 .. v39}, LX/6b7;->a(ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/Exception;)V

    .line 1113291
    if-eqz p3, :cond_3

    .line 1113292
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 1113293
    :cond_3
    throw v39

    .line 1113294
    :cond_4
    :try_start_6
    iget v3, v2, LX/7TD;->d:I

    iget v4, v2, LX/7TD;->e:I

    iget v5, v2, LX/7TD;->f:I

    iget v6, v2, LX/7TD;->g:I

    iget v7, v2, LX/7TD;->h:I

    iget v8, v2, LX/7TD;->i:I

    iget v9, v2, LX/7TD;->j:I

    iget v10, v2, LX/7TD;->k:I

    iget-wide v11, v2, LX/7TD;->b:J

    iget-wide v13, v2, LX/7TD;->c:J

    move-object/from16 v0, v49

    iget-boolean v15, v0, LX/7TE;->a:Z

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->b:Z

    move/from16 v16, v0

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->c:Z

    move/from16 v17, v0

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->d:Z

    move/from16 v18, v0

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->e:Z

    move/from16 v19, v0

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->f:Z

    move/from16 v20, v0

    move-object/from16 v0, v49

    iget-boolean v0, v0, LX/7TE;->g:Z

    move/from16 v21, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->h:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->i:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->j:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->k:J

    move-wide/from16 v28, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->l:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->m:J

    move-wide/from16 v32, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->n:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->o:J

    move-wide/from16 v36, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->p:J

    move-wide/from16 v38, v0

    move-object/from16 v0, v49

    iget-wide v0, v0, LX/7TE;->q:J

    move-wide/from16 v40, v0

    move-object/from16 v0, v49

    iget-object v0, v0, LX/7TE;->r:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, v49

    iget-object v0, v0, LX/7TE;->s:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v49

    iget-object v0, v0, LX/7TE;->t:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, p4

    iget v0, v0, LX/6bJ;->b:I

    move/from16 v45, v0

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->a:LX/6bI;

    invoke-virtual {v2}, LX/6bI;->getValue()I

    move-result v46

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v0, v2, LX/7T7;->d:I

    move/from16 v47, v0

    move-object/from16 v0, p4

    iget-object v2, v0, LX/6bJ;->c:LX/7T7;

    iget v0, v2, LX/7T7;->e:I

    move/from16 v48, v0

    move-object/from16 v2, p6

    invoke-virtual/range {v2 .. v48}, LX/6b7;->a(IIIIIIIIJJZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch LX/7T9; {:try_start_6 .. :try_end_6} :catch_4

    .line 1113295
    return-void

    .line 1113296
    :catch_3
    move-exception v39

    move-object v2, v14

    goto/16 :goto_2

    :catch_4
    move-exception v39

    move-object/from16 v2, v49

    goto/16 :goto_2

    :cond_5
    move-object v2, v14

    goto/16 :goto_1
.end method

.method public static a(J)Z
    .locals 2

    .prologue
    .line 1113262
    const-wide/16 v0, 0x1388

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/VideoItem;LX/2Mf;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)LX/6bA;
    .locals 7
    .param p2    # LX/2Mf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1113256
    iget-object v0, p0, LX/6bK;->c:LX/1Er;

    const-string v1, "video_transcode"

    const-string v2, "mp4"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/6bK;->f:Ljava/io/File;

    .line 1113257
    :try_start_0
    iget-object v3, p0, LX/6bK;->f:Ljava/io/File;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/6bK;->a(LX/6bK;Lcom/facebook/photos/base/media/VideoItem;LX/2Mf;Ljava/io/File;LX/6bJ;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)V

    .line 1113258
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    iget-object v1, p0, LX/6bK;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/74m;->c(Ljava/lang/String;)LX/74m;

    move-result-object v0

    const-string v1, "video/mp4"

    invoke-virtual {v0, v1}, LX/74m;->d(Ljava/lang/String;)LX/74m;

    move-result-object v0

    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    .line 1113259
    new-instance v1, LX/6bA;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v0}, LX/6bA;-><init>(ZLcom/facebook/ipc/media/MediaItem;)V
    :try_end_0
    .catch LX/7T9; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1113260
    :catch_0
    move-exception v0

    .line 1113261
    throw v0
.end method
