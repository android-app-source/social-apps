.class public abstract LX/6YK;
.super Landroid/widget/LinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1109712
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1109713
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 p1, -0x2

    invoke-direct {v0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, LX/6YK;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1109714
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/6YK;->setOrientation(I)V

    .line 1109715
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/6YK;->setVisibility(I)V

    .line 1109716
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 7

    .prologue
    .line 1109703
    const/4 v0, 0x0

    .line 1109704
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, LX/6YK;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1109705
    invoke-virtual {p0}, LX/6YK;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1109706
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const v5, 0x7f0b0034

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1109707
    const v4, 0x7f0b06dd

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0b06dd

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1109708
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1109709
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x7f0a03ef

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1109710
    move-object v0, v1

    .line 1109711
    return-object v0
.end method

.method public abstract a(LX/6Y7;)V
.end method
