.class public final LX/5kl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 996130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 996131
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 996132
    iget-object v1, p0, LX/5kl;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 996133
    iget-object v3, p0, LX/5kl;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 996134
    iget-object v5, p0, LX/5kl;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 996135
    iget-object v6, p0, LX/5kl;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 996136
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 996137
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 996138
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 996139
    const/4 v1, 0x2

    iget-boolean v3, p0, LX/5kl;->c:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 996140
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 996141
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 996142
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 996143
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 996144
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 996145
    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 996146
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 996147
    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 996148
    return-object v1
.end method
