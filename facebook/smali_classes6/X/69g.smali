.class public final LX/69g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1fQ;


# direct methods
.method public constructor <init>(LX/1fQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1058109
    iput-object p1, p0, LX/69g;->b:LX/1fQ;

    iput-object p2, p0, LX/69g;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1058110
    const-string v0, "LiveVideoStatusHandler"

    const-string v1, "LiveVideoStatusUpdateSubscription query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1058111
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1058112
    check-cast p1, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;

    .line 1058113
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;->a()Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$VideoModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1058114
    :cond_0
    :goto_0
    return-void

    .line 1058115
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;->a()Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$VideoModel;

    move-result-object v0

    .line 1058116
    invoke-virtual {v0}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$VideoModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 1058117
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v2, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v2, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v2, v1, :cond_4

    .line 1058118
    :cond_2
    const/4 v2, 0x1

    .line 1058119
    :goto_1
    move v2, v2

    .line 1058120
    move v1, v2

    .line 1058121
    if-eqz v1, :cond_0

    .line 1058122
    invoke-virtual {v0}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1058123
    iget-object v1, p0, LX/69g;->b:LX/1fQ;

    iget-object v1, v1, LX/1fQ;->c:LX/1BF;

    iget-object v2, p0, LX/69g;->a:Ljava/lang/String;

    .line 1058124
    iget-object v3, v1, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1fI;

    .line 1058125
    new-instance p1, LX/69W;

    iget-object p0, v3, LX/1fI;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1fM;

    new-instance v1, LX/69d;

    invoke-direct {v1, v2, v0}, LX/69d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p1, p0, v1}, LX/69W;-><init>(LX/1fM;LX/1fd;)V

    .line 1058126
    iget-object p0, v3, LX/1fI;->b:LX/1fJ;

    invoke-virtual {p0, p1}, LX/1fJ;->a(LX/1fL;)V

    .line 1058127
    goto :goto_2

    .line 1058128
    :cond_3
    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
