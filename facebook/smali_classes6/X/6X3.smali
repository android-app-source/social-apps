.class public final LX/6X3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1106935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1106936
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4984e12

    if-eq v0, v1, :cond_1

    .line 1106937
    :cond_0
    const/4 v0, 0x0

    .line 1106938
    :goto_0
    return-object v0

    .line 1106939
    :cond_1
    new-instance v0, LX/4Xy;

    invoke-direct {v0}, LX/4Xy;-><init>()V

    .line 1106940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->l()Ljava/lang/String;

    move-result-object v1

    .line 1106941
    iput-object v1, v0, LX/4Xy;->b:Ljava/lang/String;

    .line 1106942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->B()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 1106943
    iput-object v1, v0, LX/4Xy;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1106944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v1

    .line 1106945
    iput-object v1, v0, LX/4Xy;->d:LX/0Px;

    .line 1106946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106947
    iput-object v1, v0, LX/4Xy;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106949
    iput-object v1, v0, LX/4Xy;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ac()I

    move-result v1

    .line 1106951
    iput v1, v0, LX/4Xy;->g:I

    .line 1106952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aq()I

    move-result v1

    .line 1106953
    iput v1, v0, LX/4Xy;->j:I

    .line 1106954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aR()Z

    move-result v1

    .line 1106955
    iput-boolean v1, v0, LX/4Xy;->l:Z

    .line 1106956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aS()Z

    move-result v1

    .line 1106957
    iput-boolean v1, v0, LX/4Xy;->n:Z

    .line 1106958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bd()Z

    move-result v1

    .line 1106959
    iput-boolean v1, v0, LX/4Xy;->r:Z

    .line 1106960
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->be()Z

    move-result v1

    .line 1106961
    iput-boolean v1, v0, LX/4Xy;->s:Z

    .line 1106962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v2

    .line 1106963
    iput-wide v2, v0, LX/4Xy;->y:J

    .line 1106964
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1106965
    iput-object v1, v0, LX/4Xy;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1106966
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cd()Ljava/lang/String;

    move-result-object v1

    .line 1106967
    iput-object v1, v0, LX/4Xy;->A:Ljava/lang/String;

    .line 1106968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cT()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1106969
    iput-object v1, v0, LX/4Xy;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1106970
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dc()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1106971
    iput-object v1, v0, LX/4Xy;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1106972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dO()I

    move-result v1

    .line 1106973
    iput v1, v0, LX/4Xy;->G:I

    .line 1106974
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dP()I

    move-result v1

    .line 1106975
    iput v1, v0, LX/4Xy;->H:I

    .line 1106976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1106977
    iput-object v1, v0, LX/4Xy;->I:Ljava/lang/String;

    .line 1106978
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106979
    iput-object v1, v0, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106980
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106981
    iput-object v1, v0, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106983
    iput-object v1, v0, LX/4Xy;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eh()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 1106985
    iput-object v1, v0, LX/4Xy;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 1106986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eE()Z

    move-result v1

    .line 1106987
    iput-boolean v1, v0, LX/4Xy;->ai:Z

    .line 1106988
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106989
    iput-object v1, v0, LX/4Xy;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106990
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fB()J

    move-result-wide v2

    .line 1106991
    iput-wide v2, v0, LX/4Xy;->at:J

    .line 1106992
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1106993
    iput-object v1, v0, LX/4Xy;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1106994
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lx()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1106995
    iput-object v1, v0, LX/4Xy;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1106996
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gD()Ljava/lang/String;

    move-result-object v1

    .line 1106997
    iput-object v1, v0, LX/4Xy;->aH:Ljava/lang/String;

    .line 1106998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gF()I

    move-result v1

    .line 1106999
    iput v1, v0, LX/4Xy;->aI:I

    .line 1107000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gG()Ljava/lang/String;

    move-result-object v1

    .line 1107001
    iput-object v1, v0, LX/4Xy;->aJ:Ljava/lang/String;

    .line 1107002
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gM()Ljava/lang/String;

    move-result-object v1

    .line 1107003
    iput-object v1, v0, LX/4Xy;->aL:Ljava/lang/String;

    .line 1107004
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hd()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1107005
    iput-object v1, v0, LX/4Xy;->aN:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1107006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107007
    iput-object v1, v0, LX/4Xy;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107009
    iput-object v1, v0, LX/4Xy;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    .line 1107011
    iput-object v1, v0, LX/4Xy;->aU:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 1107012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    .line 1107013
    iput-object v1, v0, LX/4Xy;->ba:Ljava/lang/String;

    .line 1107014
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ky()I

    move-result v1

    .line 1107015
    iput v1, v0, LX/4Xy;->bb:I

    .line 1107016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lg()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    .line 1107017
    iput-object v1, v0, LX/4Xy;->bd:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 1107018
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107019
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4c808d5

    if-eq v0, v1, :cond_1

    .line 1107020
    :cond_0
    const/4 v0, 0x0

    .line 1107021
    :goto_0
    return-object v0

    .line 1107022
    :cond_1
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    .line 1107023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->p()LX/0Px;

    move-result-object v1

    .line 1107024
    iput-object v1, v0, LX/23u;->b:LX/0Px;

    .line 1107025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->r()LX/0Px;

    move-result-object v1

    .line 1107026
    iput-object v1, v0, LX/23u;->c:LX/0Px;

    .line 1107027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->t()LX/0Px;

    move-result-object v1

    .line 1107028
    iput-object v1, v0, LX/23u;->d:LX/0Px;

    .line 1107029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->J()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    .line 1107030
    iput-object v1, v0, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 1107031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v1

    .line 1107032
    iput-object v1, v0, LX/23u;->f:LX/0Px;

    .line 1107033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107034
    iput-object v1, v0, LX/23u;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    .line 1107036
    iput-object v1, v0, LX/23u;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 1107037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ad()LX/0Px;

    move-result-object v1

    .line 1107038
    iput-object v1, v0, LX/23u;->i:LX/0Px;

    .line 1107039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ae()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107040
    iput-object v1, v0, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v1

    .line 1107042
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 1107043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ak()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v1

    .line 1107044
    iput-object v1, v0, LX/23u;->l:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 1107045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aC()Ljava/lang/String;

    move-result-object v1

    .line 1107046
    iput-object v1, v0, LX/23u;->m:Ljava/lang/String;

    .line 1107047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aJ()Z

    move-result v1

    .line 1107048
    iput-boolean v1, v0, LX/23u;->n:Z

    .line 1107049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aR()Z

    move-result v1

    .line 1107050
    iput-boolean v1, v0, LX/23u;->o:Z

    .line 1107051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aS()Z

    move-result v1

    .line 1107052
    iput-boolean v1, v0, LX/23u;->p:Z

    .line 1107053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aT()Z

    move-result v1

    .line 1107054
    iput-boolean v1, v0, LX/23u;->q:Z

    .line 1107055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aU()Z

    move-result v1

    .line 1107056
    iput-boolean v1, v0, LX/23u;->r:Z

    .line 1107057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aV()Z

    move-result v1

    .line 1107058
    iput-boolean v1, v0, LX/23u;->s:Z

    .line 1107059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v2

    .line 1107060
    iput-wide v2, v0, LX/23u;->v:J

    .line 1107061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107062
    iput-object v1, v0, LX/23u;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pm()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v1

    .line 1107064
    iput-object v1, v0, LX/23u;->y:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 1107065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cl()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    .line 1107066
    iput-object v1, v0, LX/23u;->z:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 1107067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cT()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107068
    iput-object v1, v0, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->da()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v1

    .line 1107070
    iput-object v1, v0, LX/23u;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 1107071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dc()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1107072
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1107073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dd()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    .line 1107074
    iput-object v1, v0, LX/23u;->E:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 1107075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->do()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v1

    .line 1107076
    iput-object v1, v0, LX/23u;->H:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 1107077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dK()Z

    move-result v1

    .line 1107078
    iput-boolean v1, v0, LX/23u;->J:Z

    .line 1107079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dS()Ljava/lang/String;

    move-result-object v1

    .line 1107080
    iput-object v1, v0, LX/23u;->K:Ljava/lang/String;

    .line 1107081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dU()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v1

    .line 1107082
    iput-object v1, v0, LX/23u;->M:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 1107083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1107084
    iput-object v1, v0, LX/23u;->N:Ljava/lang/String;

    .line 1107085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ec()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107086
    iput-object v1, v0, LX/23u;->O:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eh()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 1107088
    iput-object v1, v0, LX/23u;->P:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 1107089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ei()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v1

    .line 1107090
    iput-object v1, v0, LX/23u;->Q:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 1107091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eZ()Ljava/lang/String;

    move-result-object v1

    .line 1107092
    iput-object v1, v0, LX/23u;->T:Ljava/lang/String;

    .line 1107093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107094
    iput-object v1, v0, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107096
    iput-object v1, v0, LX/23u;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lP()LX/0Px;

    move-result-object v1

    .line 1107098
    iput-object v1, v0, LX/23u;->af:LX/0Px;

    .line 1107099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fD()LX/0Px;

    move-result-object v1

    .line 1107100
    iput-object v1, v0, LX/23u;->ag:LX/0Px;

    .line 1107101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fL()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v1

    .line 1107102
    iput-object v1, v0, LX/23u;->ah:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 1107103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107104
    iput-object v1, v0, LX/23u;->ai:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gz()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v1

    .line 1107106
    iput-object v1, v0, LX/23u;->aj:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 1107107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gL()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v1

    .line 1107108
    iput-object v1, v0, LX/23u;->ak:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 1107109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hd()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1107110
    iput-object v1, v0, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1107111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hs()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v1

    .line 1107112
    iput-object v1, v0, LX/23u;->am:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 1107113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    .line 1107114
    iput-object v1, v0, LX/23u;->ao:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 1107115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hH()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v1

    .line 1107116
    iput-object v1, v0, LX/23u;->ap:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 1107117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hS()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v1

    .line 1107118
    iput-object v1, v0, LX/23u;->aq:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 1107119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ic()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    .line 1107120
    iput-object v1, v0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1107121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->im()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 1107122
    iput-object v1, v0, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1107123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->io()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107124
    iput-object v1, v0, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iV()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    .line 1107126
    iput-object v1, v0, LX/23u;->aw:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 1107127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jf()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    .line 1107128
    iput-object v1, v0, LX/23u;->ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 1107129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jk()LX/0Px;

    move-result-object v1

    .line 1107130
    iput-object v1, v0, LX/23u;->aB:LX/0Px;

    .line 1107131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jl()I

    move-result v1

    .line 1107132
    iput v1, v0, LX/23u;->aC:I

    .line 1107133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107134
    iput-object v1, v0, LX/23u;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107136
    iput-object v1, v0, LX/23u;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jq()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107138
    iput-object v1, v0, LX/23u;->aF:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oH()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v1

    .line 1107140
    iput-object v1, v0, LX/23u;->aG:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 1107141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107142
    iput-object v1, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107144
    iput-object v1, v0, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jN()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107146
    iput-object v1, v0, LX/23u;->aJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jO()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1107148
    iput-object v1, v0, LX/23u;->aK:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1107149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jV()Ljava/lang/String;

    move-result-object v1

    .line 1107150
    iput-object v1, v0, LX/23u;->aM:Ljava/lang/String;

    .line 1107151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ke()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    .line 1107152
    iput-object v1, v0, LX/23u;->aN:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 1107153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kg()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v1

    .line 1107154
    iput-object v1, v0, LX/23u;->aO:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 1107155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    .line 1107156
    iput-object v1, v0, LX/23u;->aP:Ljava/lang/String;

    .line 1107157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kv()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1107158
    iput-object v1, v0, LX/23u;->aQ:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1107159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kI()LX/0Px;

    move-result-object v1

    .line 1107160
    iput-object v1, v0, LX/23u;->aR:LX/0Px;

    .line 1107161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kR()Z

    move-result v1

    .line 1107162
    iput-boolean v1, v0, LX/23u;->aS:Z

    .line 1107163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lg()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    .line 1107164
    iput-object v1, v0, LX/23u;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 1107165
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107166
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x642179c1

    if-eq v0, v1, :cond_1

    .line 1107167
    :cond_0
    const/4 v0, 0x0

    .line 1107168
    :goto_0
    return-object v0

    .line 1107169
    :cond_1
    new-instance v0, LX/4Vu;

    invoke-direct {v0}, LX/4Vu;-><init>()V

    .line 1107170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ae()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107171
    iput-object v1, v0, LX/4Vu;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107172
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v1

    .line 1107173
    iput-object v1, v0, LX/4Vu;->d:LX/0Px;

    .line 1107174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lG()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1107175
    iput-object v1, v0, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1107176
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107177
    iput-object v1, v0, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107179
    iput-object v1, v0, LX/4Vu;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nH()Z

    move-result v1

    .line 1107181
    iput-boolean v1, v0, LX/4Vu;->h:Z

    .line 1107182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aR()Z

    move-result v1

    .line 1107183
    iput-boolean v1, v0, LX/4Vu;->i:Z

    .line 1107184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aS()Z

    move-result v1

    .line 1107185
    iput-boolean v1, v0, LX/4Vu;->j:Z

    .line 1107186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->be()Z

    move-result v1

    .line 1107187
    iput-boolean v1, v0, LX/4Vu;->k:Z

    .line 1107188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lI()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 1107189
    iput-object v1, v0, LX/4Vu;->l:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1107190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nL()Ljava/lang/String;

    move-result-object v1

    .line 1107191
    iput-object v1, v0, LX/4Vu;->m:Ljava/lang/String;

    .line 1107192
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v2

    .line 1107193
    iput-wide v2, v0, LX/4Vu;->n:J

    .line 1107194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cl()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    .line 1107195
    iput-object v1, v0, LX/4Vu;->o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 1107196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dc()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1107197
    iput-object v1, v0, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1107198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1107199
    iput-object v1, v0, LX/4Vu;->q:Ljava/lang/String;

    .line 1107200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mT()Z

    move-result v1

    .line 1107201
    iput-boolean v1, v0, LX/4Vu;->s:Z

    .line 1107202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nS()Z

    move-result v1

    .line 1107203
    iput-boolean v1, v0, LX/4Vu;->t:Z

    .line 1107204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nU()Z

    move-result v1

    .line 1107205
    iput-boolean v1, v0, LX/4Vu;->u:Z

    .line 1107206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107207
    iput-object v1, v0, LX/4Vu;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    .line 1107209
    iput-object v1, v0, LX/4Vu;->A:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 1107210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pT()Ljava/lang/String;

    move-result-object v1

    .line 1107211
    iput-object v1, v0, LX/4Vu;->D:Ljava/lang/String;

    .line 1107212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ke()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    .line 1107213
    iput-object v1, v0, LX/4Vu;->F:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 1107214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107215
    iput-object v1, v0, LX/4Vu;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    .line 1107217
    iput-object v1, v0, LX/4Vu;->H:Ljava/lang/String;

    .line 1107218
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    goto/16 :goto_0
.end method
