.class public LX/6cu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1114810
    sget-object v0, LX/6dV;->a:LX/0U1;

    .line 1114811
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114812
    sput-object v0, LX/6cu;->a:Ljava/lang/String;

    .line 1114813
    sget-object v0, LX/6dV;->b:LX/0U1;

    .line 1114814
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114815
    sput-object v0, LX/6cu;->b:Ljava/lang/String;

    .line 1114816
    sget-object v0, LX/6dV;->c:LX/0U1;

    .line 1114817
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114818
    sput-object v0, LX/6cu;->c:Ljava/lang/String;

    .line 1114819
    sget-object v0, LX/6dV;->d:LX/0U1;

    .line 1114820
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114821
    sput-object v0, LX/6cu;->d:Ljava/lang/String;

    .line 1114822
    sget-object v0, LX/6dV;->e:LX/0U1;

    .line 1114823
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114824
    sput-object v0, LX/6cu;->e:Ljava/lang/String;

    .line 1114825
    sget-object v0, LX/6dV;->f:LX/0U1;

    .line 1114826
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114827
    sput-object v0, LX/6cu;->f:Ljava/lang/String;

    .line 1114828
    sget-object v0, LX/6dV;->g:LX/0U1;

    .line 1114829
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114830
    sput-object v0, LX/6cu;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114809
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1114771
    const v0, 0xa6c37c7

    invoke-static {p0, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114772
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 1114773
    iget-boolean v2, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->e:Z

    move v2, v2

    .line 1114774
    if-eqz v2, :cond_0

    .line 1114775
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1114776
    sget-object v2, LX/6dV;->c:LX/0U1;

    .line 1114777
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1114778
    invoke-static {v2, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 1114779
    const-string v3, "event_reminders"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, p2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114780
    goto :goto_0

    .line 1114781
    :catchall_0
    move-exception v0

    const v1, -0x25302c96

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1114782
    :cond_0
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1114783
    sget-object v4, LX/6cu;->a:Ljava/lang/String;

    .line 1114784
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v5, v5

    .line 1114785
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114786
    sget-object v4, LX/6cu;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114787
    sget-object v4, LX/6cu;->c:Ljava/lang/String;

    .line 1114788
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1114789
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114790
    sget-object v4, LX/6cu;->d:Ljava/lang/String;

    .line 1114791
    iget-wide v7, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    move-wide v5, v7

    .line 1114792
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1114793
    sget-object v4, LX/6cu;->e:Ljava/lang/String;

    .line 1114794
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1114795
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114796
    sget-object v4, LX/6cu;->f:Ljava/lang/String;

    .line 1114797
    iget-boolean v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    move v5, v5

    .line 1114798
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1114799
    sget-object v4, LX/6cu;->g:Ljava/lang/String;

    .line 1114800
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1114801
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114802
    const-string v4, "event_reminders"

    const/4 v5, 0x0

    const v6, 0x4ccabf76    # 1.06298288E8f

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {p0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, -0x44c65575

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1114803
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 1114804
    goto/16 :goto_0

    .line 1114805
    :cond_1
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114806
    const v0, 0x3ffa2586

    invoke-static {p0, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114807
    return-void
.end method
