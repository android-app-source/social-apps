.class public LX/6N2;
.super LX/2TZ;
.source ""

# interfaces
.implements LX/6N1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;",
        "LX/6N1;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/database/Cursor;

.field private final d:LX/3fh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1082248
    const-class v0, LX/6N2;

    sput-object v0, LX/6N2;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;LX/3fh;)V
    .locals 0

    .prologue
    .line 1082238
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 1082239
    iput-object p1, p0, LX/6N2;->c:Landroid/database/Cursor;

    .line 1082240
    iput-object p2, p0, LX/6N2;->d:LX/3fh;

    .line 1082241
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1082242
    const/4 v3, 0x0

    .line 1082243
    :try_start_0
    iget-object v0, p0, LX/6N2;->d:LX/3fh;

    iget-object v1, p0, LX/6N2;->c:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3fh;->a(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1082244
    :goto_0
    return-object v0

    .line 1082245
    :catch_0
    move-exception v0

    .line 1082246
    sget-object v1, LX/6N2;->b:Ljava/lang/Class;

    const-string v2, "Error deserializing contact"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1082247
    const/4 v0, 0x0

    goto :goto_0
.end method
