.class public final LX/646;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1lF;

.field public final synthetic b:Z

.field public final synthetic c:LX/0yW;


# direct methods
.method public constructor <init>(LX/0yW;LX/1lF;Z)V
    .locals 0

    .prologue
    .line 1044161
    iput-object p1, p0, LX/646;->c:LX/0yW;

    iput-object p2, p0, LX/646;->a:LX/1lF;

    iput-boolean p3, p0, LX/646;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1044162
    iget-object v0, p0, LX/646;->c:LX/0yW;

    .line 1044163
    iput-boolean v4, v0, LX/0yW;->q:Z

    .line 1044164
    iget-object v0, p0, LX/646;->c:LX/0yW;

    const-wide/16 v2, 0x0

    .line 1044165
    iput-wide v2, v0, LX/0yW;->o:J

    .line 1044166
    iget-object v0, p0, LX/646;->c:LX/0yW;

    iget-object v0, v0, LX/0yW;->v:LX/0yX;

    const-string v1, "zb_redirect_failed"

    invoke-virtual {v0, v1}, LX/0yX;->a(Ljava/lang/String;)V

    .line 1044167
    iget-boolean v0, p0, LX/646;->b:Z

    if-eqz v0, :cond_0

    .line 1044168
    iget-object v0, p0, LX/646;->c:LX/0yW;

    iget-object v1, p0, LX/646;->a:LX/1lF;

    invoke-static {v0, v4, v1}, LX/0yW;->a$redex0(LX/0yW;ZLX/1lF;)V

    .line 1044169
    :cond_0
    sget-object v0, LX/0yW;->c:Ljava/lang/Class;

    const-string v1, "Zero Balance Ping failed due to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1044170
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1044171
    check-cast p1, Lorg/apache/http/HttpResponse;

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1044172
    iget-object v0, p0, LX/646;->c:LX/0yW;

    .line 1044173
    iput-wide v6, v0, LX/0yW;->o:J

    .line 1044174
    iget-object v0, p0, LX/646;->c:LX/0yW;

    .line 1044175
    iput-wide v6, v0, LX/0yW;->i:J

    .line 1044176
    iget-object v0, p0, LX/646;->c:LX/0yW;

    .line 1044177
    iput-boolean v3, v0, LX/0yW;->q:Z

    .line 1044178
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 1044179
    const/16 v1, 0x12c

    if-lt v0, v1, :cond_0

    const/16 v1, 0x133

    if-gt v0, v1, :cond_0

    .line 1044180
    const-string v1, "Location"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    .line 1044181
    iget-object v1, p0, LX/646;->c:LX/0yW;

    iget-object v1, v1, LX/0yW;->v:LX/0yX;

    const-string v2, "zb_redirect_success"

    invoke-virtual {v1, v2}, LX/0yX;->a(Ljava/lang/String;)V

    .line 1044182
    iget-object v1, p0, LX/646;->c:LX/0yW;

    sget-object v2, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    invoke-static {v1, v2, v0, v4}, LX/0yW;->a$redex0(LX/0yW;LX/2XZ;ILjava/lang/String;)V

    .line 1044183
    iget-object v0, p0, LX/646;->c:LX/0yW;

    iget-object v1, p0, LX/646;->a:LX/1lF;

    invoke-static {v0, v3, v1}, LX/0yW;->a$redex0(LX/0yW;ZLX/1lF;)V

    .line 1044184
    :goto_0
    return-void

    .line 1044185
    :cond_0
    iget-object v1, p0, LX/646;->c:LX/0yW;

    iget-object v1, v1, LX/0yW;->v:LX/0yX;

    const-string v2, "zb_redirect_success"

    invoke-virtual {v1, v2}, LX/0yX;->a(Ljava/lang/String;)V

    .line 1044186
    iget-object v1, p0, LX/646;->c:LX/0yW;

    sget-object v2, LX/2XZ;->CONNECTED:LX/2XZ;

    invoke-static {v1, v2, v0, v4}, LX/0yW;->a$redex0(LX/0yW;LX/2XZ;ILjava/lang/String;)V

    .line 1044187
    iget-object v0, p0, LX/646;->c:LX/0yW;

    const/4 v1, 0x1

    iget-object v2, p0, LX/646;->a:LX/1lF;

    invoke-static {v0, v1, v2}, LX/0yW;->a$redex0(LX/0yW;ZLX/1lF;)V

    goto :goto_0
.end method
