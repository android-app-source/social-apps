.class public final enum LX/635;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/635;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/635;

.field public static final enum BOTTOM:LX/635;

.field public static final enum LEFT:LX/635;

.field public static final enum RIGHT:LX/635;

.field public static final enum TOP:LX/635;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1042780
    new-instance v0, LX/635;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2}, LX/635;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/635;->TOP:LX/635;

    .line 1042781
    new-instance v0, LX/635;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LX/635;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/635;->LEFT:LX/635;

    .line 1042782
    new-instance v0, LX/635;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, LX/635;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/635;->BOTTOM:LX/635;

    .line 1042783
    new-instance v0, LX/635;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v5}, LX/635;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/635;->RIGHT:LX/635;

    .line 1042784
    const/4 v0, 0x4

    new-array v0, v0, [LX/635;

    sget-object v1, LX/635;->TOP:LX/635;

    aput-object v1, v0, v2

    sget-object v1, LX/635;->LEFT:LX/635;

    aput-object v1, v0, v3

    sget-object v1, LX/635;->BOTTOM:LX/635;

    aput-object v1, v0, v4

    sget-object v1, LX/635;->RIGHT:LX/635;

    aput-object v1, v0, v5

    sput-object v0, LX/635;->$VALUES:[LX/635;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1042785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/635;
    .locals 1

    .prologue
    .line 1042779
    const-class v0, LX/635;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/635;

    return-object v0
.end method

.method public static values()[LX/635;
    .locals 1

    .prologue
    .line 1042778
    sget-object v0, LX/635;->$VALUES:[LX/635;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/635;

    return-object v0
.end method
