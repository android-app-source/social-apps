.class public LX/5N3;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/5N3;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 905844
    invoke-direct {p0}, LX/398;-><init>()V

    .line 905845
    const-string v0, "dialtone?ref={%s}&action={%s}&follow_up_intent={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ref"

    const-string v2, "action"

    const-string v3, "follow_up_intent"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 905846
    const-string v0, "dialtone://start"

    const-string v1, "ref"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/5N2;

    const-string v2, "start"

    invoke-direct {v1, v2}, LX/5N2;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 905847
    const-string v0, "dialtone://switch_to_dialtone"

    const-string v1, "ref"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/5N2;

    const-string v2, "switch_to_dialtone"

    invoke-direct {v1, v2}, LX/5N2;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 905848
    const-string v0, "dialtone://switch_to_full_fb"

    const-string v1, "ref"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/5N2;

    const-string v2, "switch_to_full_fb"

    invoke-direct {v1, v2}, LX/5N2;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 905849
    const-string v0, "dialtone://open_zb_portal"

    const-string v1, "ref"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/5N2;

    const-string v2, "open_zb_portal"

    invoke-direct {v1, v2}, LX/5N2;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 905850
    return-void
.end method

.method public static a(LX/0QB;)LX/5N3;
    .locals 3

    .prologue
    .line 905851
    sget-object v0, LX/5N3;->a:LX/5N3;

    if-nez v0, :cond_1

    .line 905852
    const-class v1, LX/5N3;

    monitor-enter v1

    .line 905853
    :try_start_0
    sget-object v0, LX/5N3;->a:LX/5N3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 905854
    if-eqz v2, :cond_0

    .line 905855
    :try_start_1
    new-instance v0, LX/5N3;

    invoke-direct {v0}, LX/5N3;-><init>()V

    .line 905856
    move-object v0, v0

    .line 905857
    sput-object v0, LX/5N3;->a:LX/5N3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 905858
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 905859
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 905860
    :cond_1
    sget-object v0, LX/5N3;->a:LX/5N3;

    return-object v0

    .line 905861
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 905862
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
