.class public LX/6a4;
.super LX/398;
.source ""

# interfaces
.implements LX/0jq;


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6a4;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111889
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1111890
    sget-object v0, LX/0ax;->gH:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->MAPS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1111891
    return-void
.end method

.method public static a(LX/0QB;)LX/6a4;
    .locals 3

    .prologue
    .line 1111892
    sget-object v0, LX/6a4;->a:LX/6a4;

    if-nez v0, :cond_1

    .line 1111893
    const-class v1, LX/6a4;

    monitor-enter v1

    .line 1111894
    :try_start_0
    sget-object v0, LX/6a4;->a:LX/6a4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1111895
    if-eqz v2, :cond_0

    .line 1111896
    :try_start_1
    new-instance v0, LX/6a4;

    invoke-direct {v0}, LX/6a4;-><init>()V

    .line 1111897
    move-object v0, v0

    .line 1111898
    sput-object v0, LX/6a4;->a:LX/6a4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1111899
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1111900
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1111901
    :cond_1
    sget-object v0, LX/6a4;->a:LX/6a4;

    return-object v0

    .line 1111902
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1111903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 1111904
    new-instance v0, Lcom/facebook/maps/GenericMapsFragment;

    invoke-direct {v0}, Lcom/facebook/maps/GenericMapsFragment;-><init>()V

    .line 1111905
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1111906
    return-object v0
.end method
