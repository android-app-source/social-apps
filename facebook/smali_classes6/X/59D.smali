.class public final LX/59D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 851289
    const/4 v10, 0x0

    .line 851290
    const/4 v9, 0x0

    .line 851291
    const/4 v8, 0x0

    .line 851292
    const/4 v7, 0x0

    .line 851293
    const/4 v6, 0x0

    .line 851294
    const/4 v5, 0x0

    .line 851295
    const/4 v4, 0x0

    .line 851296
    const/4 v3, 0x0

    .line 851297
    const/4 v2, 0x0

    .line 851298
    const/4 v1, 0x0

    .line 851299
    const/4 v0, 0x0

    .line 851300
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 851301
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 851302
    const/4 v0, 0x0

    .line 851303
    :goto_0
    return v0

    .line 851304
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 851305
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 851306
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 851307
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 851308
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 851309
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 851310
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 851311
    :cond_3
    const-string v12, "android_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 851312
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 851313
    :cond_4
    const-string v12, "formattype"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 851314
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 851315
    :cond_5
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 851316
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 851317
    :cond_6
    const-string v12, "is_viewer_coworker"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 851318
    const/4 v1, 0x1

    .line 851319
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 851320
    :cond_7
    const-string v12, "is_work_user"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 851321
    const/4 v0, 0x1

    .line 851322
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 851323
    :cond_8
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 851324
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 851325
    :cond_9
    const-string v12, "tag"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 851326
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 851327
    :cond_a
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 851328
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 851329
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 851330
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 851331
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 851332
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 851333
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 851334
    if-eqz v1, :cond_c

    .line 851335
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 851336
    :cond_c
    if-eqz v0, :cond_d

    .line 851337
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 851338
    :cond_d
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 851339
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 851340
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 851341
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 851342
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 851343
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 851344
    if-eqz v0, :cond_0

    .line 851345
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851346
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 851347
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 851348
    if-eqz v0, :cond_1

    .line 851349
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851350
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 851351
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 851352
    if-eqz v0, :cond_2

    .line 851353
    const-string v0, "formattype"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851354
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 851355
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 851356
    if-eqz v0, :cond_3

    .line 851357
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851358
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 851359
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 851360
    if-eqz v0, :cond_4

    .line 851361
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851362
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 851363
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 851364
    if-eqz v0, :cond_5

    .line 851365
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851366
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 851367
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 851368
    if-eqz v0, :cond_6

    .line 851369
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851370
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 851371
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 851372
    if-eqz v0, :cond_7

    .line 851373
    const-string v1, "tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851374
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 851375
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 851376
    if-eqz v0, :cond_8

    .line 851377
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 851378
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 851379
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 851380
    return-void
.end method
