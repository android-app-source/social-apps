.class public final LX/5as;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 956332
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 956333
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956334
    :goto_0
    return v6

    .line 956335
    :cond_0
    const-string v9, "update_time"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 956336
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    .line 956337
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 956338
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 956339
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956340
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 956341
    const-string v9, "nodes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 956342
    invoke-static {p0, p1}, LX/5bG;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 956343
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 956344
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 956345
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 956346
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 956347
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 956348
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_5
    move v0, v6

    move-wide v2, v4

    move v7, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 956321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956322
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956323
    if-eqz v0, :cond_0

    .line 956324
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956325
    invoke-static {p0, v0, p2, p3}, LX/5bG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 956326
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 956327
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 956328
    const-string v2, "update_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956329
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 956330
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956331
    return-void
.end method
