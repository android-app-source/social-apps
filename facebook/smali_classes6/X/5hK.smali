.class public final LX/5hK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 978320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 978321
    if-nez p0, :cond_0

    .line 978322
    const/4 v0, 0x0

    .line 978323
    :goto_0
    return-object v0

    .line 978324
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 978325
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 978326
    iput v1, v0, LX/2dc;->c:I

    .line 978327
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 978328
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 978329
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 978330
    iput v1, v0, LX/2dc;->i:I

    .line 978331
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 978332
    if-nez p0, :cond_0

    .line 978333
    const/4 v0, 0x0

    .line 978334
    :goto_0
    return-object v0

    .line 978335
    :cond_0
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 978336
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 978337
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 978338
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 978339
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;

    .line 978340
    if-nez v0, :cond_5

    .line 978341
    const/4 v5, 0x0

    .line 978342
    :goto_2
    move-object v0, v5

    .line 978343
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 978344
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 978345
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 978346
    iput-object v0, v3, LX/23u;->d:LX/0Px;

    .line 978347
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 978348
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 978349
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 978350
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;

    .line 978351
    if-nez v0, :cond_6

    .line 978352
    const/4 v4, 0x0

    .line 978353
    :goto_4
    move-object v0, v4

    .line 978354
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 978355
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 978356
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 978357
    iput-object v0, v3, LX/23u;->k:LX/0Px;

    .line 978358
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    .line 978359
    if-nez v0, :cond_e

    .line 978360
    const/4 v1, 0x0

    .line 978361
    :goto_5
    move-object v0, v1

    .line 978362
    iput-object v0, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 978363
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 978364
    iput-object v0, v3, LX/23u;->N:Ljava/lang/String;

    .line 978365
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->bX_()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 978366
    iput-object v0, v3, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978367
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->bW_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ShareableModel;

    move-result-object v0

    .line 978368
    if-nez v0, :cond_11

    .line 978369
    const/4 v1, 0x0

    .line 978370
    :goto_6
    move-object v0, v1

    .line 978371
    iput-object v0, v3, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 978372
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->j()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 978373
    iput-object v0, v3, LX/23u;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978374
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->k()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 978375
    iput-object v0, v3, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978376
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto/16 :goto_0

    .line 978377
    :cond_5
    new-instance v5, LX/3dL;

    invoke-direct {v5}, LX/3dL;-><init>()V

    .line 978378
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 978379
    iput-object v6, v5, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 978380
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 978381
    iput-object v6, v5, LX/3dL;->ag:Ljava/lang/String;

    .line 978382
    invoke-virtual {v5}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    goto/16 :goto_2

    .line 978383
    :cond_6
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 978384
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    move-result-object v5

    .line 978385
    if-nez v5, :cond_7

    .line 978386
    const/4 v6, 0x0

    .line 978387
    :goto_7
    move-object v5, v6

    .line 978388
    iput-object v5, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 978389
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->b()LX/174;

    move-result-object v5

    invoke-static {v5}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 978390
    iput-object v5, v4, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978391
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v5

    .line 978392
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 978393
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel;

    move-result-object v5

    .line 978394
    if-nez v5, :cond_c

    .line 978395
    const/4 v6, 0x0

    .line 978396
    :goto_8
    move-object v5, v6

    .line 978397
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 978398
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 978399
    iput-object v5, v4, LX/39x;->t:Ljava/lang/String;

    .line 978400
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel;->bY_()Ljava/lang/String;

    move-result-object v5

    .line 978401
    iput-object v5, v4, LX/39x;->w:Ljava/lang/String;

    .line 978402
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto/16 :goto_4

    .line 978403
    :cond_7
    new-instance v6, LX/4XB;

    invoke-direct {v6}, LX/4XB;-><init>()V

    .line 978404
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 978405
    iput-object v7, v6, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 978406
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->c()I

    move-result v7

    .line 978407
    iput v7, v6, LX/4XB;->j:I

    .line 978408
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v7

    .line 978409
    if-nez v7, :cond_8

    .line 978410
    const/4 v8, 0x0

    .line 978411
    :goto_9
    move-object v7, v8

    .line 978412
    iput-object v7, v6, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 978413
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->e()I

    move-result v7

    .line 978414
    iput v7, v6, LX/4XB;->O:I

    .line 978415
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->bZ_()I

    move-result v7

    .line 978416
    iput v7, v6, LX/4XB;->S:I

    .line 978417
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->ca_()Ljava/lang/String;

    move-result-object v7

    .line 978418
    iput-object v7, v6, LX/4XB;->T:Ljava/lang/String;

    .line 978419
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->j()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/5hK;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 978420
    iput-object v7, v6, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 978421
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k()Z

    move-result v7

    .line 978422
    iput-boolean v7, v6, LX/4XB;->at:Z

    .line 978423
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->l()Z

    move-result v7

    .line 978424
    iput-boolean v7, v6, LX/4XB;->ay:Z

    .line 978425
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m()Ljava/lang/String;

    move-result-object v7

    .line 978426
    iput-object v7, v6, LX/4XB;->aZ:Ljava/lang/String;

    .line 978427
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->n()I

    move-result v7

    .line 978428
    iput v7, v6, LX/4XB;->bb:I

    .line 978429
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o()Ljava/lang/String;

    move-result-object v7

    .line 978430
    iput-object v7, v6, LX/4XB;->bc:Ljava/lang/String;

    .line 978431
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p()Ljava/lang/String;

    move-result-object v7

    .line 978432
    iput-object v7, v6, LX/4XB;->bd:Ljava/lang/String;

    .line 978433
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q()Ljava/lang/String;

    move-result-object v7

    .line 978434
    iput-object v7, v6, LX/4XB;->bg:Ljava/lang/String;

    .line 978435
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->r()Z

    move-result v7

    .line 978436
    iput-boolean v7, v6, LX/4XB;->bF:Z

    .line 978437
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->s()I

    move-result v7

    .line 978438
    iput v7, v6, LX/4XB;->bO:I

    .line 978439
    invoke-virtual {v6}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    goto/16 :goto_7

    .line 978440
    :cond_8
    new-instance v10, LX/23u;

    invoke-direct {v10}, LX/23u;-><init>()V

    .line 978441
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;->b()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 978442
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 978443
    const/4 v8, 0x0

    move v9, v8

    :goto_a
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_9

    .line 978444
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$ActorsModel;

    .line 978445
    if-nez v8, :cond_b

    .line 978446
    const/4 v12, 0x0

    .line 978447
    :goto_b
    move-object v8, v12

    .line 978448
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 978449
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_a

    .line 978450
    :cond_9
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 978451
    iput-object v8, v10, LX/23u;->d:LX/0Px;

    .line 978452
    :cond_a
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 978453
    iput-object v8, v10, LX/23u;->N:Ljava/lang/String;

    .line 978454
    invoke-virtual {v10}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    goto/16 :goto_9

    .line 978455
    :cond_b
    new-instance v12, LX/3dL;

    invoke-direct {v12}, LX/3dL;-><init>()V

    .line 978456
    invoke-virtual {v8}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 978457
    iput-object v13, v12, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 978458
    invoke-virtual {v8}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v13

    .line 978459
    iput-object v13, v12, LX/3dL;->ag:Ljava/lang/String;

    .line 978460
    invoke-virtual {v12}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v12

    goto :goto_b

    .line 978461
    :cond_c
    new-instance v6, LX/4XR;

    invoke-direct {v6}, LX/4XR;-><init>()V

    .line 978462
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 978463
    iput-object v7, v6, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 978464
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel;->b()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v7

    .line 978465
    if-nez v7, :cond_d

    .line 978466
    const/4 v8, 0x0

    .line 978467
    :goto_c
    move-object v7, v8

    .line 978468
    iput-object v7, v6, LX/4XR;->Y:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 978469
    invoke-virtual {v6}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    goto/16 :goto_8

    .line 978470
    :cond_d
    new-instance v8, LX/4Vr;

    invoke-direct {v8}, LX/4Vr;-><init>()V

    .line 978471
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 978472
    iput-object v5, v8, LX/4Vr;->m:Ljava/lang/String;

    .line 978473
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 978474
    iput-object v5, v8, LX/4Vr;->r:Ljava/lang/String;

    .line 978475
    invoke-virtual {v8}, LX/4Vr;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v8

    goto :goto_c

    .line 978476
    :cond_e
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 978477
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->b()Z

    move-result v2

    .line 978478
    iput-boolean v2, v1, LX/3dM;->e:Z

    .line 978479
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->c(Z)LX/3dM;

    .line 978480
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->g(Z)LX/3dM;

    .line 978481
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->e()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;

    move-result-object v2

    .line 978482
    if-nez v2, :cond_f

    .line 978483
    const/4 v4, 0x0

    .line 978484
    :goto_d
    move-object v2, v4

    .line 978485
    iput-object v2, v1, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 978486
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->cb_()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->j(Z)LX/3dM;

    .line 978487
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->cc_()Ljava/lang/String;

    move-result-object v2

    .line 978488
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 978489
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 978490
    iput-object v2, v1, LX/3dM;->D:Ljava/lang/String;

    .line 978491
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v2

    .line 978492
    if-nez v2, :cond_10

    .line 978493
    const/4 v4, 0x0

    .line 978494
    :goto_e
    move-object v2, v4

    .line 978495
    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 978496
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_5

    .line 978497
    :cond_f
    new-instance v4, LX/4Vv;

    invoke-direct {v4}, LX/4Vv;-><init>()V

    .line 978498
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;->a()I

    move-result v5

    .line 978499
    iput v5, v4, LX/4Vv;->b:I

    .line 978500
    invoke-virtual {v4}, LX/4Vv;->a()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v4

    goto :goto_d

    .line 978501
    :cond_10
    new-instance v4, LX/3dN;

    invoke-direct {v4}, LX/3dN;-><init>()V

    .line 978502
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;->a()I

    move-result v5

    .line 978503
    iput v5, v4, LX/3dN;->b:I

    .line 978504
    invoke-virtual {v4}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    goto :goto_e

    .line 978505
    :cond_11
    new-instance v1, LX/170;

    invoke-direct {v1}, LX/170;-><init>()V

    .line 978506
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 978507
    iput-object v2, v1, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 978508
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 978509
    iput-object v2, v1, LX/170;->o:Ljava/lang/String;

    .line 978510
    invoke-virtual {v1}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    goto/16 :goto_6
.end method

.method public static a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 978511
    if-nez p0, :cond_0

    .line 978512
    const/4 v0, 0x0

    .line 978513
    :goto_0
    return-object v0

    .line 978514
    :cond_0
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 978515
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 978516
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 978517
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 13

    .prologue
    .line 978518
    if-nez p0, :cond_0

    .line 978519
    const/4 v0, 0x0

    .line 978520
    :goto_0
    return-object v0

    .line 978521
    :cond_0
    new-instance v0, LX/2oI;

    invoke-direct {v0}, LX/2oI;-><init>()V

    .line 978522
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->b()Z

    move-result v1

    .line 978523
    iput-boolean v1, v0, LX/2oI;->m:Z

    .line 978524
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->c()Z

    move-result v1

    .line 978525
    iput-boolean v1, v0, LX/2oI;->n:Z

    .line 978526
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 978527
    iput-object v1, v0, LX/2oI;->p:Ljava/lang/String;

    .line 978528
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->e()J

    move-result-wide v2

    .line 978529
    iput-wide v2, v0, LX/2oI;->t:J

    .line 978530
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bV_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v1

    invoke-static {v1}, LX/5hK;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 978531
    iput-object v1, v0, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 978532
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bU_()Z

    move-result v1

    .line 978533
    iput-boolean v1, v0, LX/2oI;->x:Z

    .line 978534
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->j()D

    move-result-wide v2

    .line 978535
    iput-wide v2, v0, LX/2oI;->B:D

    .line 978536
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->k()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    .line 978537
    if-nez v1, :cond_1

    .line 978538
    const/4 v4, 0x0

    .line 978539
    :goto_1
    move-object v1, v4

    .line 978540
    iput-object v1, v0, LX/2oI;->D:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 978541
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->l()I

    move-result v1

    .line 978542
    iput v1, v0, LX/2oI;->M:I

    .line 978543
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 978544
    iput-object v1, v0, LX/2oI;->N:Ljava/lang/String;

    .line 978545
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->n()I

    move-result v1

    .line 978546
    iput v1, v0, LX/2oI;->aa:I

    .line 978547
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->o()I

    move-result v1

    .line 978548
    iput v1, v0, LX/2oI;->ab:I

    .line 978549
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->p()I

    move-result v1

    .line 978550
    iput v1, v0, LX/2oI;->ac:I

    .line 978551
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->q()Z

    move-result v1

    .line 978552
    iput-boolean v1, v0, LX/2oI;->al:Z

    .line 978553
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->r()Z

    move-result v1

    .line 978554
    iput-boolean v1, v0, LX/2oI;->am:Z

    .line 978555
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->s()Z

    move-result v1

    .line 978556
    iput-boolean v1, v0, LX/2oI;->aq:Z

    .line 978557
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->t()Z

    move-result v1

    .line 978558
    iput-boolean v1, v0, LX/2oI;->ar:Z

    .line 978559
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->u()I

    move-result v1

    .line 978560
    iput v1, v0, LX/2oI;->az:I

    .line 978561
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->v()LX/174;

    move-result-object v1

    invoke-static {v1}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 978562
    iput-object v1, v0, LX/2oI;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978563
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->w()D

    move-result-wide v2

    .line 978564
    iput-wide v2, v0, LX/2oI;->aL:D

    .line 978565
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->x()I

    move-result v1

    .line 978566
    iput v1, v0, LX/2oI;->aP:I

    .line 978567
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->y()I

    move-result v1

    .line 978568
    iput v1, v0, LX/2oI;->aS:I

    .line 978569
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->z()Ljava/lang/String;

    move-result-object v1

    .line 978570
    iput-object v1, v0, LX/2oI;->aT:Ljava/lang/String;

    .line 978571
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->A()Ljava/lang/String;

    move-result-object v1

    .line 978572
    iput-object v1, v0, LX/2oI;->aU:Ljava/lang/String;

    .line 978573
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->B()Ljava/lang/String;

    move-result-object v1

    .line 978574
    iput-object v1, v0, LX/2oI;->aW:Ljava/lang/String;

    .line 978575
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->C()Ljava/lang/String;

    move-result-object v1

    .line 978576
    iput-object v1, v0, LX/2oI;->aZ:Ljava/lang/String;

    .line 978577
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->D()Ljava/lang/String;

    move-result-object v1

    .line 978578
    iput-object v1, v0, LX/2oI;->bl:Ljava/lang/String;

    .line 978579
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->E()D

    move-result-wide v2

    .line 978580
    iput-wide v2, v0, LX/2oI;->bx:D

    .line 978581
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->F()D

    move-result-wide v2

    .line 978582
    iput-wide v2, v0, LX/2oI;->by:D

    .line 978583
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->G()Ljava/lang/String;

    move-result-object v1

    .line 978584
    iput-object v1, v0, LX/2oI;->bz:Ljava/lang/String;

    .line 978585
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->H()Ljava/lang/String;

    move-result-object v1

    .line 978586
    iput-object v1, v0, LX/2oI;->bA:Ljava/lang/String;

    .line 978587
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->I()I

    move-result v1

    .line 978588
    iput v1, v0, LX/2oI;->bC:I

    .line 978589
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->J()Z

    move-result v1

    .line 978590
    iput-boolean v1, v0, LX/2oI;->bI:Z

    .line 978591
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->K()LX/174;

    move-result-object v1

    invoke-static {v1}, LX/5hK;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 978592
    iput-object v1, v0, LX/2oI;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 978593
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/5hK;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 978594
    iput-object v1, v0, LX/2oI;->bO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 978595
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->M()LX/0Px;

    move-result-object v1

    .line 978596
    iput-object v1, v0, LX/2oI;->bP:LX/0Px;

    .line 978597
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->N()I

    move-result v1

    .line 978598
    iput v1, v0, LX/2oI;->cd:I

    .line 978599
    invoke-virtual {v0}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    goto/16 :goto_0

    .line 978600
    :cond_1
    new-instance v6, LX/4ZS;

    invoke-direct {v6}, LX/4ZS;-><init>()V

    .line 978601
    invoke-virtual {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 978602
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 978603
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-virtual {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 978604
    invoke-virtual {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;

    .line 978605
    if-nez v4, :cond_4

    .line 978606
    const/4 v8, 0x0

    .line 978607
    :goto_3
    move-object v4, v8

    .line 978608
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 978609
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 978610
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 978611
    iput-object v4, v6, LX/4ZS;->b:LX/0Px;

    .line 978612
    :cond_3
    invoke-virtual {v6}, LX/4ZS;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v4

    goto/16 :goto_1

    .line 978613
    :cond_4
    new-instance v8, LX/4ZT;

    invoke-direct {v8}, LX/4ZT;-><init>()V

    .line 978614
    invoke-virtual {v4}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->a()I

    move-result v9

    .line 978615
    iput v9, v8, LX/4ZT;->b:I

    .line 978616
    invoke-virtual {v4}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->b()J

    move-result-wide v10

    .line 978617
    iput-wide v10, v8, LX/4ZT;->c:J

    .line 978618
    invoke-virtual {v4}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->c()I

    move-result v9

    .line 978619
    iput v9, v8, LX/4ZT;->d:I

    .line 978620
    invoke-virtual {v8}, LX/4ZT;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    move-result-object v8

    goto :goto_3
.end method
