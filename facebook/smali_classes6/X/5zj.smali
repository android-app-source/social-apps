.class public final enum LX/5zj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5zj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5zj;

.field public static final enum ATTACHED_MEDIA:LX/5zj;

.field public static final enum AUDIO:LX/5zj;

.field public static final enum CAMERA:LX/5zj;

.field public static final enum CAMERACORE_BACK:LX/5zj;

.field public static final enum CAMERACORE_FRONT:LX/5zj;

.field public static final enum COMPOSER_LONGPRESS:LX/5zj;

.field public static final enum CONTENT_SEARCH:LX/5zj;

.field public static final enum FORWARD:LX/5zj;

.field public static final enum GALLERY:LX/5zj;

.field public static final enum KEYBOARD:LX/5zj;

.field public static final enum MEDIA_PICKER:LX/5zj;

.field public static final enum MEDIA_PICKER_GALLERY:LX/5zj;

.field public static final enum MEDIA_VIEWER_EDITOR:LX/5zj;

.field public static final enum MONTAGE:LX/5zj;

.field public static final enum MONTAGE_BACK:LX/5zj;

.field public static final enum MONTAGE_CAMERA:LX/5zj;

.field public static final enum MONTAGE_CAMERA_BACK:LX/5zj;

.field public static final enum MONTAGE_CAMERA_FRONT:LX/5zj;

.field public static final enum MONTAGE_FRONT:LX/5zj;

.field public static final enum PAGE_SAVED_REPLY:LX/5zj;

.field public static final enum QUICKCAM_BACK:LX/5zj;

.field public static final enum QUICKCAM_FRONT:LX/5zj;

.field public static final enum SHARE:LX/5zj;

.field public static final enum SHARED_MEDIA:LX/5zj;

.field public static final enum TRIMMED_VIDEO:LX/5zj;

.field public static final enum UNSPECIFIED:LX/5zj;

.field private static final VALUE_MAP:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/5zj;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VIDEO:LX/5zj;

.field public static final enum VIDEO_MMS:LX/5zj;

.field public static final enum VIDEO_STICKER:LX/5zj;

.field public static final enum VOICE_CLIP:LX/5zj;


# instance fields
.field public final DBSerialValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1035466
    new-instance v1, LX/5zj;

    const-string v2, "UNSPECIFIED"

    const-string v3, "unspecified"

    invoke-direct {v1, v2, v0, v3}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->UNSPECIFIED:LX/5zj;

    .line 1035467
    new-instance v1, LX/5zj;

    const-string v2, "KEYBOARD"

    const-string v3, "keyboard"

    invoke-direct {v1, v2, v5, v3}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->KEYBOARD:LX/5zj;

    .line 1035468
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE"

    const-string v3, "montage"

    invoke-direct {v1, v2, v6, v3}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE:LX/5zj;

    .line 1035469
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE_BACK"

    const-string v3, "montageback"

    invoke-direct {v1, v2, v7, v3}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE_BACK:LX/5zj;

    .line 1035470
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE_FRONT"

    const-string v3, "montagefront"

    invoke-direct {v1, v2, v8, v3}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    .line 1035471
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE_CAMERA"

    const/4 v3, 0x5

    const-string v4, "montagecamera"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE_CAMERA:LX/5zj;

    .line 1035472
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE_CAMERA_BACK"

    const/4 v3, 0x6

    const-string v4, "montagecameraback"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE_CAMERA_BACK:LX/5zj;

    .line 1035473
    new-instance v1, LX/5zj;

    const-string v2, "MONTAGE_CAMERA_FRONT"

    const/4 v3, 0x7

    const-string v4, "montagecamerafront"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MONTAGE_CAMERA_FRONT:LX/5zj;

    .line 1035474
    new-instance v1, LX/5zj;

    const-string v2, "CONTENT_SEARCH"

    const/16 v3, 0x8

    const-string v4, "content_search"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->CONTENT_SEARCH:LX/5zj;

    .line 1035475
    new-instance v1, LX/5zj;

    const-string v2, "MEDIA_PICKER"

    const/16 v3, 0x9

    const-string v4, "mediapicker"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MEDIA_PICKER:LX/5zj;

    .line 1035476
    new-instance v1, LX/5zj;

    const-string v2, "CAMERA"

    const/16 v3, 0xa

    const-string v4, "camera"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->CAMERA:LX/5zj;

    .line 1035477
    new-instance v1, LX/5zj;

    const-string v2, "AUDIO"

    const/16 v3, 0xb

    const-string v4, "audio"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->AUDIO:LX/5zj;

    .line 1035478
    new-instance v1, LX/5zj;

    const-string v2, "VOICE_CLIP"

    const/16 v3, 0xc

    const-string v4, "voiceclip"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->VOICE_CLIP:LX/5zj;

    .line 1035479
    new-instance v1, LX/5zj;

    const-string v2, "COMPOSER_LONGPRESS"

    const/16 v3, 0xd

    const-string v4, "composer_longpress"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->COMPOSER_LONGPRESS:LX/5zj;

    .line 1035480
    new-instance v1, LX/5zj;

    const-string v2, "VIDEO_STICKER"

    const/16 v3, 0xe

    const-string v4, "videosticker"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->VIDEO_STICKER:LX/5zj;

    .line 1035481
    new-instance v1, LX/5zj;

    const-string v2, "VIDEO"

    const/16 v3, 0xf

    const-string v4, "video"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->VIDEO:LX/5zj;

    .line 1035482
    new-instance v1, LX/5zj;

    const-string v2, "TRIMMED_VIDEO"

    const/16 v3, 0x10

    const-string v4, "trimmedvideo"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->TRIMMED_VIDEO:LX/5zj;

    .line 1035483
    new-instance v1, LX/5zj;

    const-string v2, "SHARE"

    const/16 v3, 0x11

    const-string v4, "share"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->SHARE:LX/5zj;

    .line 1035484
    new-instance v1, LX/5zj;

    const-string v2, "SHARED_MEDIA"

    const/16 v3, 0x12

    const-string v4, "shared_media"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->SHARED_MEDIA:LX/5zj;

    .line 1035485
    new-instance v1, LX/5zj;

    const-string v2, "ATTACHED_MEDIA"

    const/16 v3, 0x13

    const-string v4, "attached_media"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->ATTACHED_MEDIA:LX/5zj;

    .line 1035486
    new-instance v1, LX/5zj;

    const-string v2, "FORWARD"

    const/16 v3, 0x14

    const-string v4, "forward"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->FORWARD:LX/5zj;

    .line 1035487
    new-instance v1, LX/5zj;

    const-string v2, "GALLERY"

    const/16 v3, 0x15

    const-string v4, "gallery"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->GALLERY:LX/5zj;

    .line 1035488
    new-instance v1, LX/5zj;

    const-string v2, "QUICKCAM_FRONT"

    const/16 v3, 0x16

    const-string v4, "quickcamfront"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->QUICKCAM_FRONT:LX/5zj;

    .line 1035489
    new-instance v1, LX/5zj;

    const-string v2, "QUICKCAM_BACK"

    const/16 v3, 0x17

    const-string v4, "quickcamback"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->QUICKCAM_BACK:LX/5zj;

    .line 1035490
    new-instance v1, LX/5zj;

    const-string v2, "CAMERACORE_FRONT"

    const/16 v3, 0x18

    const-string v4, "cameracorefront"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->CAMERACORE_FRONT:LX/5zj;

    .line 1035491
    new-instance v1, LX/5zj;

    const-string v2, "CAMERACORE_BACK"

    const/16 v3, 0x19

    const-string v4, "cameracoreback"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->CAMERACORE_BACK:LX/5zj;

    .line 1035492
    new-instance v1, LX/5zj;

    const-string v2, "MEDIA_PICKER_GALLERY"

    const/16 v3, 0x1a

    const-string v4, "mediapicker_gallery"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    .line 1035493
    new-instance v1, LX/5zj;

    const-string v2, "MEDIA_VIEWER_EDITOR"

    const/16 v3, 0x1b

    const-string v4, "media_viewer_editor"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->MEDIA_VIEWER_EDITOR:LX/5zj;

    .line 1035494
    new-instance v1, LX/5zj;

    const-string v2, "PAGE_SAVED_REPLY"

    const/16 v3, 0x1c

    const-string v4, "page_saved_reply"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->PAGE_SAVED_REPLY:LX/5zj;

    .line 1035495
    new-instance v1, LX/5zj;

    const-string v2, "VIDEO_MMS"

    const/16 v3, 0x1d

    const-string v4, "video_mms"

    invoke-direct {v1, v2, v3, v4}, LX/5zj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zj;->VIDEO_MMS:LX/5zj;

    .line 1035496
    const/16 v1, 0x1e

    new-array v1, v1, [LX/5zj;

    sget-object v2, LX/5zj;->UNSPECIFIED:LX/5zj;

    aput-object v2, v1, v0

    sget-object v2, LX/5zj;->KEYBOARD:LX/5zj;

    aput-object v2, v1, v5

    sget-object v2, LX/5zj;->MONTAGE:LX/5zj;

    aput-object v2, v1, v6

    sget-object v2, LX/5zj;->MONTAGE_BACK:LX/5zj;

    aput-object v2, v1, v7

    sget-object v2, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/5zj;->MONTAGE_CAMERA:LX/5zj;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/5zj;->MONTAGE_CAMERA_BACK:LX/5zj;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/5zj;->MONTAGE_CAMERA_FRONT:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, LX/5zj;->CONTENT_SEARCH:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, LX/5zj;->MEDIA_PICKER:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, LX/5zj;->CAMERA:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, LX/5zj;->AUDIO:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, LX/5zj;->VOICE_CLIP:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, LX/5zj;->COMPOSER_LONGPRESS:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, LX/5zj;->VIDEO_STICKER:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, LX/5zj;->VIDEO:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, LX/5zj;->TRIMMED_VIDEO:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    sget-object v3, LX/5zj;->SHARE:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, LX/5zj;->SHARED_MEDIA:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, LX/5zj;->ATTACHED_MEDIA:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    sget-object v3, LX/5zj;->FORWARD:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    sget-object v3, LX/5zj;->GALLERY:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, LX/5zj;->QUICKCAM_FRONT:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    sget-object v3, LX/5zj;->QUICKCAM_BACK:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    sget-object v3, LX/5zj;->CAMERACORE_FRONT:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, LX/5zj;->CAMERACORE_BACK:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    sget-object v3, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, LX/5zj;->MEDIA_VIEWER_EDITOR:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    sget-object v3, LX/5zj;->PAGE_SAVED_REPLY:LX/5zj;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    sget-object v3, LX/5zj;->VIDEO_MMS:LX/5zj;

    aput-object v3, v1, v2

    sput-object v1, LX/5zj;->$VALUES:[LX/5zj;

    .line 1035497
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1035498
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1035499
    iget-object v5, v4, LX/5zj;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1035500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1035501
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/5zj;->VALUE_MAP:LX/0P1;

    .line 1035502
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1035463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1035464
    iput-object p3, p0, LX/5zj;->DBSerialValue:Ljava/lang/String;

    .line 1035465
    return-void
.end method

.method public static fromDBSerialValue(Ljava/lang/String;)LX/5zj;
    .locals 3

    .prologue
    .line 1035460
    sget-object v0, LX/5zj;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1035461
    sget-object v0, LX/5zj;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5zj;

    return-object v0

    .line 1035462
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Source: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5zj;
    .locals 1

    .prologue
    .line 1035459
    const-class v0, LX/5zj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5zj;

    return-object v0
.end method

.method public static values()[LX/5zj;
    .locals 1

    .prologue
    .line 1035458
    sget-object v0, LX/5zj;->$VALUES:[LX/5zj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5zj;

    return-object v0
.end method


# virtual methods
.method public final isFrontFacingCamera()Z
    .locals 1

    .prologue
    .line 1035503
    sget-object v0, LX/5zj;->QUICKCAM_FRONT:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isMediaPickerSource()Z
    .locals 1

    .prologue
    .line 1035457
    sget-object v0, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MEDIA_PICKER:LX/5zj;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isMontageSource()Z
    .locals 1

    .prologue
    .line 1035456
    sget-object v0, LX/5zj;->MONTAGE:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_BACK:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_CAMERA:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_CAMERA_FRONT:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->MONTAGE_CAMERA_BACK:LX/5zj;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isQuickCamSource()Z
    .locals 1

    .prologue
    .line 1035455
    sget-object v0, LX/5zj;->QUICKCAM_BACK:LX/5zj;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zj;->QUICKCAM_FRONT:LX/5zj;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVideoMailSource()Z
    .locals 1

    .prologue
    .line 1035454
    const/4 v0, 0x0

    return v0
.end method

.method public final isVideoStickerSource()Z
    .locals 1

    .prologue
    .line 1035453
    sget-object v0, LX/5zj;->COMPOSER_LONGPRESS:LX/5zj;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
