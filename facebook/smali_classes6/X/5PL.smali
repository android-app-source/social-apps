.class public LX/5PL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5PK;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public a:Ljavax/microedition/khronos/egl/EGLDisplay;

.field public b:Ljavax/microedition/khronos/egl/EGLContext;

.field public c:Ljavax/microedition/khronos/egl/EGLConfig;

.field public d:Ljavax/microedition/khronos/egl/EGL10;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 910543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910544
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 910545
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    return-void
.end method

.method public static b(LX/5PL;I)LX/5PL;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 910546
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    .line 910547
    iget-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 910548
    const-string v0, "eglGetDisplay"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910549
    iget-object v0, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eq v0, v1, :cond_0

    move v0, v4

    :goto_0
    invoke-static {v0}, LX/64O;->b(Z)V

    .line 910550
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 910551
    iget-object v1, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 910552
    const-string v0, "eglInitialize"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910553
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL10"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v6

    .line 910554
    goto :goto_0

    .line 910555
    :cond_1
    const/16 v0, 0xd

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 910556
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_2

    .line 910557
    const/16 v0, 0xa

    sget v1, LX/5PP;->a:I

    aput v1, v2, v0

    .line 910558
    const/16 v0, 0xb

    aput v4, v2, v0

    .line 910559
    :cond_2
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 910560
    new-array v5, v4, [I

    .line 910561
    iget-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 910562
    const-string v0, "eglChooseConfig"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910563
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to find RGB888+recordable ES2 EGL config"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910564
    :cond_3
    aget-object v0, v3, v6

    iput-object v0, p0, LX/5PL;->c:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 910565
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 910566
    iget-object v1, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, LX/5PL;->c:Ljavax/microedition/khronos/egl/EGLConfig;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    .line 910567
    const-string v0, "eglCreateContext"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910568
    iget-object v0, p0, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-static {v0}, LX/64O;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910569
    return-object p0

    .line 910570
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3040
        0x4
        0x3038
        0x0
        0x3038
    .end array-data

    .line 910571
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method


# virtual methods
.method public final synthetic a(I)LX/5PK;
    .locals 1

    .prologue
    .line 910572
    invoke-static {p0, p1}, LX/5PL;->b(LX/5PL;I)LX/5PL;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/Surface;)LX/5PS;
    .locals 1

    .prologue
    .line 910573
    new-instance v0, LX/5PU;

    invoke-direct {v0, p0, p1}, LX/5PU;-><init>(LX/5PL;Landroid/view/Surface;)V

    return-object v0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 910574
    iget-object v0, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 910575
    iget-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 910576
    iget-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 910577
    iget-object v0, p0, LX/5PL;->d:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 910578
    :cond_0
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, LX/5PL;->a:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 910579
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, LX/5PL;->b:Ljavax/microedition/khronos/egl/EGLContext;

    .line 910580
    const/4 v0, 0x0

    iput-object v0, p0, LX/5PL;->c:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 910581
    return-void
.end method
