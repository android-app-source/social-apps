.class public final LX/5As;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 859681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;
    .locals 20

    .prologue
    .line 859682
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 859683
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5As;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 859684
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5As;->k:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 859685
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5As;->m:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 859686
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5As;->n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 859687
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5As;->p:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 859688
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5As;->q:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 859689
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5As;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 859690
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5As;->s:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 859691
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5As;->t:Ljava/lang/String;

    invoke-virtual {v2, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 859692
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5As;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 859693
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5As;->v:LX/0Px;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 859694
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5As;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 859695
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5As;->x:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 859696
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5As;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 859697
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5As;->z:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 859698
    const/16 v18, 0x1b

    move/from16 v0, v18

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 859699
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->a:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859700
    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->b:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859701
    const/16 v18, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->c:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859702
    const/16 v18, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->d:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859703
    const/16 v18, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->e:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859704
    const/16 v18, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->f:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859705
    const/16 v18, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->g:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859706
    const/16 v18, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->h:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859707
    const/16 v18, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5As;->i:Z

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, LX/186;->a(IZ)V

    .line 859708
    const/16 v18, 0x9

    move/from16 v0, v18

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 859709
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 859710
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5As;->l:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 859711
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 859712
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 859713
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5As;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 859714
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 859715
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 859716
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 859717
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 859718
    const/16 v3, 0x13

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 859719
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 859720
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 859721
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 859722
    const/16 v3, 0x17

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 859723
    const/16 v3, 0x18

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 859724
    const/16 v3, 0x19

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 859725
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget v4, v0, LX/5As;->A:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 859726
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 859727
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 859728
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 859729
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 859730
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 859731
    new-instance v3, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;-><init>(LX/15i;)V

    .line 859732
    return-object v3
.end method
