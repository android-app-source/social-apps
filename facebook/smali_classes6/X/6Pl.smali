.class public LX/6Pl;
.super LX/6PU;
.source ""


# instance fields
.field private final a:LX/6Ot;

.field private final b:Lcom/facebook/ipc/composer/model/ProductItemAttachment;


# direct methods
.method public constructor <init>(LX/6Ot;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086350
    invoke-direct {p0, p2}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086351
    iput-object p1, p0, LX/6Pl;->a:LX/6Ot;

    .line 1086352
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, LX/6Pl;->b:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1086353
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 2

    .prologue
    .line 1086354
    invoke-static {p1}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1086355
    if-nez v0, :cond_0

    .line 1086356
    :goto_0
    return-void

    .line 1086357
    :cond_0
    iget-object v1, p0, LX/6Pl;->b:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {v0, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1086358
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-static {v1, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4Yr;->a(LX/0Px;)V

    .line 1086359
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4Yr;->b(Ljava/lang/String;)V

    .line 1086360
    const/4 v0, 0x0

    .line 1086361
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "local_edit_pending"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086362
    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086363
    const-string v0, "UpdateStoryProductItemMutatingVisitor"

    return-object v0
.end method
