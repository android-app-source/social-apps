.class public final LX/5V9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 928115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 928116
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 928117
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 928118
    invoke-static {p0, p1}, LX/5V9;->b(LX/15w;LX/186;)I

    move-result v1

    .line 928119
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 928120
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 928121
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 928122
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 928123
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5V9;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 928124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 928125
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 928126
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 928127
    const/4 v13, 0x0

    .line 928128
    const/4 v12, 0x0

    .line 928129
    const/4 v11, 0x0

    .line 928130
    const/4 v10, 0x0

    .line 928131
    const/4 v9, 0x0

    .line 928132
    const/4 v8, 0x0

    .line 928133
    const/4 v7, 0x0

    .line 928134
    const/4 v6, 0x0

    .line 928135
    const/4 v5, 0x0

    .line 928136
    const/4 v4, 0x0

    .line 928137
    const/4 v3, 0x0

    .line 928138
    const/4 v2, 0x0

    .line 928139
    const/4 v1, 0x0

    .line 928140
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 928141
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 928142
    const/4 v1, 0x0

    .line 928143
    :goto_0
    return v1

    .line 928144
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 928145
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 928146
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 928147
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 928148
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 928149
    const-string v15, "application"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 928150
    invoke-static/range {p0 .. p1}, LX/5V8;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 928151
    :cond_2
    const-string v15, "call_to_actions"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 928152
    invoke-static/range {p0 .. p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 928153
    :cond_3
    const-string v15, "default_action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 928154
    invoke-static/range {p0 .. p1}, LX/5To;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 928155
    :cond_4
    const-string v15, "first_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 928156
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 928157
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 928158
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 928159
    :cond_6
    const-string v15, "image_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 928160
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 928161
    :cond_7
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 928162
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 928163
    :cond_8
    const-string v15, "native_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 928164
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 928165
    :cond_9
    const-string v15, "second_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 928166
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 928167
    :cond_a
    const-string v15, "source_name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 928168
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 928169
    :cond_b
    const-string v15, "status_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 928170
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 928171
    :cond_c
    const-string v15, "target_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 928172
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 928173
    :cond_d
    const-string v15, "third_metaline"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 928174
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 928175
    :cond_e
    const/16 v14, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 928176
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 928177
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 928178
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 928179
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 928180
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 928181
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 928182
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 928183
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 928184
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 928185
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 928186
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 928187
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 928188
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 928189
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 928190
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 928191
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928192
    if-eqz v0, :cond_0

    .line 928193
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928194
    invoke-static {p0, v0, p2, p3}, LX/5V8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 928195
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928196
    if-eqz v0, :cond_1

    .line 928197
    const-string v1, "call_to_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928198
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 928199
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928200
    if-eqz v0, :cond_2

    .line 928201
    const-string v1, "default_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928202
    invoke-static {p0, v0, p2, p3}, LX/5To;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 928203
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928204
    if-eqz v0, :cond_3

    .line 928205
    const-string v1, "first_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928206
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928207
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928208
    if-eqz v0, :cond_4

    .line 928209
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928210
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928211
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928212
    if-eqz v0, :cond_5

    .line 928213
    const-string v1, "image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928214
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928215
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928216
    if-eqz v0, :cond_6

    .line 928217
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928218
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928219
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928220
    if-eqz v0, :cond_7

    .line 928221
    const-string v1, "native_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928222
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928223
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928224
    if-eqz v0, :cond_8

    .line 928225
    const-string v1, "second_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928226
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928227
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928228
    if-eqz v0, :cond_9

    .line 928229
    const-string v1, "source_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928230
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928231
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 928232
    if-eqz v0, :cond_a

    .line 928233
    const-string v0, "status_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928234
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928235
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928236
    if-eqz v0, :cond_b

    .line 928237
    const-string v1, "target_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928238
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928239
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928240
    if-eqz v0, :cond_c

    .line 928241
    const-string v1, "third_metaline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928242
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928243
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 928244
    return-void
.end method
