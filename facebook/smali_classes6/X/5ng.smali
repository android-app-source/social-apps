.class public LX/5ng;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004526
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1004527
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1004528
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    .line 1004529
    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 1004530
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1004531
    check-cast p1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;

    .line 1004532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1004533
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "events"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    invoke-static {v3}, LX/5ng;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004534
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "product"

    const-string v3, "fb4a"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004535
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "session_id"

    iget-wide v4, p1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004536
    iget-object v1, p1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1004537
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "experiment_name"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004538
    :cond_0
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "me/privacy_checkup_mobile_events"

    .line 1004539
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1004540
    move-object v1, v1

    .line 1004541
    const-string v2, "POST"

    .line 1004542
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1004543
    move-object v1, v1

    .line 1004544
    const-string v2, "reportPrivacyCheckupActions"

    .line 1004545
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1004546
    move-object v1, v1

    .line 1004547
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1004548
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1004549
    move-object v1, v1

    .line 1004550
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1004551
    move-object v0, v1

    .line 1004552
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004553
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004554
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
