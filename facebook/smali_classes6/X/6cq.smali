.class public LX/6cq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:[Ljava/lang/String;

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/6cy;

.field private final d:LX/2N5;

.field public volatile e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1114631
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thread_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "initial_fetch_complete"

    aput-object v2, v0, v1

    sput-object v0, LX/6cq;->f:[Ljava/lang/String;

    .line 1114632
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6cq;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0Or;LX/6cy;LX/2N5;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;",
            "LX/6cy;",
            "LX/2N5;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114634
    iput-object p1, p0, LX/6cq;->a:Landroid/content/ContentResolver;

    .line 1114635
    iput-object p2, p0, LX/6cq;->b:LX/0Or;

    .line 1114636
    iput-object p3, p0, LX/6cq;->c:LX/6cy;

    .line 1114637
    iput-object p4, p0, LX/6cq;->d:LX/2N5;

    .line 1114638
    return-void
.end method

.method public static a(LX/0QB;)LX/6cq;
    .locals 13

    .prologue
    .line 1114639
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1114640
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1114641
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1114642
    if-nez v1, :cond_0

    .line 1114643
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1114644
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1114645
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1114646
    sget-object v1, LX/6cq;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1114647
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1114648
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1114649
    :cond_1
    if-nez v1, :cond_4

    .line 1114650
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1114651
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1114652
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1114653
    new-instance v7, LX/6cq;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v8

    check-cast v8, Landroid/content/ContentResolver;

    const/16 v9, 0x2747

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v10

    check-cast v10, LX/6cy;

    invoke-static {v0}, LX/2N5;->a(LX/0QB;)LX/2N5;

    move-result-object v11

    check-cast v11, LX/2N5;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v7 .. v12}, LX/6cq;-><init>(Landroid/content/ContentResolver;LX/0Or;LX/6cy;LX/2N5;LX/0Uh;)V

    .line 1114654
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1114655
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1114656
    if-nez v1, :cond_2

    .line 1114657
    sget-object v0, LX/6cq;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cq;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1114658
    :goto_1
    if-eqz v0, :cond_3

    .line 1114659
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1114660
    :goto_3
    check-cast v0, LX/6cq;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1114661
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1114662
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1114663
    :catchall_1
    move-exception v0

    .line 1114664
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1114665
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1114666
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1114667
    :cond_2
    :try_start_8
    sget-object v0, LX/6cq;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cq;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1114668
    iget-object v0, p0, LX/6cq;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/6cq;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dB;

    iget-object v1, v1, LX/6dB;->c:LX/6dA;

    invoke-virtual {v1}, LX/6dA;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/2N5;->a:[Ljava/lang/String;

    const-string v3, "thread_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1114669
    iget-object v1, p0, LX/6cq;->d:LX/2N5;

    invoke-virtual {v1, v0}, LX/2N5;->a(Landroid/database/Cursor;)LX/6dO;

    move-result-object v1

    .line 1114670
    :try_start_0
    invoke-virtual {v1}, LX/6dO;->b()Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1114671
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method
