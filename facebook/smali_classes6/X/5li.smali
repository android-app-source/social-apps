.class public final LX/5li;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 999249
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 999250
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 999251
    :goto_0
    return v1

    .line 999252
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 999253
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 999254
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 999255
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 999256
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 999257
    const-string v5, "cursor"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 999258
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 999259
    :cond_2
    const-string v5, "node"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 999260
    invoke-static {p0, p1}, LX/5lh;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 999261
    :cond_3
    const-string v5, "tag"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 999262
    invoke-static {p0, p1}, LX/5lf;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 999263
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 999264
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 999265
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 999266
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 999267
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 999268
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 999269
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999270
    if-eqz v0, :cond_0

    .line 999271
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999272
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999273
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999274
    if-eqz v0, :cond_1

    .line 999275
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999276
    invoke-static {p0, v0, p2, p3}, LX/5lh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 999277
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999278
    if-eqz v0, :cond_2

    .line 999279
    const-string v1, "tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999280
    invoke-static {p0, v0, p2, p3}, LX/5lf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 999281
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 999282
    return-void
.end method
