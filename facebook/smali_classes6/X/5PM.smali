.class public LX/5PM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5PK;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field public a:Landroid/opengl/EGLDisplay;

.field public b:Landroid/opengl/EGLContext;

.field public c:Landroid/opengl/EGLConfig;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 910634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910635
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    .line 910636
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    .line 910637
    return-void
.end method

.method private b(I)LX/5PM;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 910609
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    .line 910610
    const-string v0, "eglGetDisplay"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910611
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    move v0, v5

    :goto_0
    invoke-static {v0}, LX/64O;->b(Z)V

    .line 910612
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 910613
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0, v2, v0, v5}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 910614
    const-string v0, "eglInitialize"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910615
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 910616
    goto :goto_0

    .line 910617
    :cond_1
    const/16 v0, 0xd

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 910618
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_2

    .line 910619
    const/16 v0, 0xa

    sget v3, LX/5PP;->a:I

    aput v3, v1, v0

    .line 910620
    const/16 v0, 0xb

    aput v5, v1, v0

    .line 910621
    :cond_2
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 910622
    new-array v6, v5, [I

    .line 910623
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 910624
    const-string v0, "eglChooseConfig"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910625
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to find RGB888+recordable ES2 EGL config"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910626
    :cond_3
    aget-object v0, v3, v2

    iput-object v0, p0, LX/5PM;->c:Landroid/opengl/EGLConfig;

    .line 910627
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 910628
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, LX/5PM;->c:Landroid/opengl/EGLConfig;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v1, v3, v4, v0, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    .line 910629
    const-string v0, "eglCreateContext"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910630
    iget-object v0, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    invoke-static {v0}, LX/64O;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910631
    return-object p0

    .line 910632
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3040
        0x4
        0x3038
        0x0
        0x3038
    .end array-data

    .line 910633
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method


# virtual methods
.method public final synthetic a(I)LX/5PK;
    .locals 1

    .prologue
    .line 910608
    invoke-direct {p0, p1}, LX/5PM;->b(I)LX/5PM;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/Surface;)LX/5PS;
    .locals 1

    .prologue
    .line 910607
    new-instance v0, LX/5PW;

    invoke-direct {v0, p0, p1}, LX/5PW;-><init>(LX/5PM;Landroid/view/Surface;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/SurfaceTexture;)Landroid/opengl/EGLSurface;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 910602
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x3038

    aput v1, v0, v3

    .line 910603
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/5PM;->c:Landroid/opengl/EGLConfig;

    invoke-static {v1, v2, p1, v0, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    .line 910604
    const-string v1, "eglCreateWindowSurface"

    invoke-static {v1}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910605
    invoke-static {v0}, LX/64O;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910606
    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 910593
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 910594
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 910595
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 910596
    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    .line 910597
    iget-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 910598
    :cond_0
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    .line 910599
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    .line 910600
    const/4 v0, 0x0

    iput-object v0, p0, LX/5PM;->c:Landroid/opengl/EGLConfig;

    .line 910601
    return-void
.end method

.method public final b(Landroid/graphics/SurfaceTexture;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 910583
    invoke-virtual {p0, p1}, LX/5PM;->a(Landroid/graphics/SurfaceTexture;)Landroid/opengl/EGLSurface;

    move-result-object v0

    .line 910584
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/5PM;->b:Landroid/opengl/EGLContext;

    invoke-static {v1, v0, v0, v2}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 910585
    const-string v0, "eglMakeCurrent"

    invoke-static {v0}, LX/5PP;->b(Ljava/lang/String;)V

    .line 910586
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910587
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v3, v3, v3, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 910588
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 910589
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 910590
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 910591
    iget-object v1, p0, LX/5PM;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 910592
    return-void
.end method

.method public final e()LX/5PM;
    .locals 1

    .prologue
    .line 910582
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/5PM;->b(I)LX/5PM;

    move-result-object v0

    return-object v0
.end method
