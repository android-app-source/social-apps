.class public final enum LX/6VG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VG;

.field public static final enum BLUE:LX/6VG;

.field public static final enum GRAY:LX/6VG;

.field public static final enum NONE:LX/6VG;

.field public static final enum SPECIAL:LX/6VG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103786
    new-instance v0, LX/6VG;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/6VG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VG;->NONE:LX/6VG;

    .line 1103787
    new-instance v0, LX/6VG;

    const-string v1, "GRAY"

    invoke-direct {v0, v1, v3}, LX/6VG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VG;->GRAY:LX/6VG;

    .line 1103788
    new-instance v0, LX/6VG;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v4}, LX/6VG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VG;->BLUE:LX/6VG;

    .line 1103789
    new-instance v0, LX/6VG;

    const-string v1, "SPECIAL"

    invoke-direct {v0, v1, v5}, LX/6VG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VG;->SPECIAL:LX/6VG;

    .line 1103790
    const/4 v0, 0x4

    new-array v0, v0, [LX/6VG;

    sget-object v1, LX/6VG;->NONE:LX/6VG;

    aput-object v1, v0, v2

    sget-object v1, LX/6VG;->GRAY:LX/6VG;

    aput-object v1, v0, v3

    sget-object v1, LX/6VG;->BLUE:LX/6VG;

    aput-object v1, v0, v4

    sget-object v1, LX/6VG;->SPECIAL:LX/6VG;

    aput-object v1, v0, v5

    sput-object v0, LX/6VG;->$VALUES:[LX/6VG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VG;
    .locals 1

    .prologue
    .line 1103783
    const-class v0, LX/6VG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VG;

    return-object v0
.end method

.method public static values()[LX/6VG;
    .locals 1

    .prologue
    .line 1103784
    sget-object v0, LX/6VG;->$VALUES:[LX/6VG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VG;

    return-object v0
.end method
