.class public final enum LX/6Eo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Eo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Eo;

.field public static final enum ADD_TO_HOME_SCREEN:LX/6Eo;

.field public static final enum MANAGE_SETTINGS:LX/6Eo;

.field public static final enum MESSNEGER_FAVORITE:LX/6Eo;


# instance fields
.field private final text:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1066672
    new-instance v0, LX/6Eo;

    const-string v1, "MESSNEGER_FAVORITE"

    const-string v2, "messenger_favorite"

    invoke-direct {v0, v1, v3, v2}, LX/6Eo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Eo;->MESSNEGER_FAVORITE:LX/6Eo;

    .line 1066673
    new-instance v0, LX/6Eo;

    const-string v1, "MANAGE_SETTINGS"

    const-string v2, "manage_settings"

    invoke-direct {v0, v1, v4, v2}, LX/6Eo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Eo;->MANAGE_SETTINGS:LX/6Eo;

    .line 1066674
    new-instance v0, LX/6Eo;

    const-string v1, "ADD_TO_HOME_SCREEN"

    const-string v2, "add_to_home_screen"

    invoke-direct {v0, v1, v5, v2}, LX/6Eo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Eo;->ADD_TO_HOME_SCREEN:LX/6Eo;

    .line 1066675
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Eo;

    sget-object v1, LX/6Eo;->MESSNEGER_FAVORITE:LX/6Eo;

    aput-object v1, v0, v3

    sget-object v1, LX/6Eo;->MANAGE_SETTINGS:LX/6Eo;

    aput-object v1, v0, v4

    sget-object v1, LX/6Eo;->ADD_TO_HOME_SCREEN:LX/6Eo;

    aput-object v1, v0, v5

    sput-object v0, LX/6Eo;->$VALUES:[LX/6Eo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1066676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1066677
    iput-object p3, p0, LX/6Eo;->text:Ljava/lang/String;

    .line 1066678
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Eo;
    .locals 1

    .prologue
    .line 1066679
    const-class v0, LX/6Eo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Eo;

    return-object v0
.end method

.method public static values()[LX/6Eo;
    .locals 1

    .prologue
    .line 1066680
    sget-object v0, LX/6Eo;->$VALUES:[LX/6Eo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Eo;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1066681
    iget-object v0, p0, LX/6Eo;->text:Ljava/lang/String;

    return-object v0
.end method
