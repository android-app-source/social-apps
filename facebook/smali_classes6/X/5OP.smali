.class public final LX/5OP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/popover/PopoverSpinner;


# direct methods
.method public constructor <init>(Lcom/facebook/fbui/popover/PopoverSpinner;)V
    .locals 0

    .prologue
    .line 908708
    iput-object p1, p0, LX/5OP;->a:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 908701
    iget-object v0, p0, LX/5OP;->a:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 908702
    iget-object v1, p0, LX/5OP;->a:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget v1, v1, Lcom/facebook/fbui/popover/PopoverSpinner;->e:I

    if-eq v1, p3, :cond_0

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    .line 908703
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 908704
    iget-object v0, p0, LX/5OP;->a:Lcom/facebook/fbui/popover/PopoverSpinner;

    const/4 v1, 0x1

    .line 908705
    iput-boolean v1, v0, Lcom/facebook/fbui/popover/PopoverSpinner;->g:Z

    .line 908706
    :cond_0
    iget-object v0, p0, LX/5OP;->a:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/popover/PopoverSpinner;->setSelection(I)V

    .line 908707
    return-void
.end method
