.class public final LX/6S1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1091474
    const/4 v10, 0x0

    .line 1091475
    const/4 v9, 0x0

    .line 1091476
    const/4 v8, 0x0

    .line 1091477
    const/4 v7, 0x0

    .line 1091478
    const/4 v6, 0x0

    .line 1091479
    const/4 v5, 0x0

    .line 1091480
    const/4 v4, 0x0

    .line 1091481
    const/4 v3, 0x0

    .line 1091482
    const/4 v2, 0x0

    .line 1091483
    const/4 v1, 0x0

    .line 1091484
    const/4 v0, 0x0

    .line 1091485
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1091486
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1091487
    const/4 v0, 0x0

    .line 1091488
    :goto_0
    return v0

    .line 1091489
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1091490
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1091491
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1091492
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1091493
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1091494
    const-string v12, "can_viewer_react"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1091495
    const/4 v1, 0x1

    .line 1091496
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1091497
    :cond_2
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1091498
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1091499
    :cond_3
    const-string v12, "important_reactors"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1091500
    invoke-static {p0, p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1091501
    :cond_4
    const-string v12, "legacy_api_post_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1091502
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1091503
    :cond_5
    const-string v12, "reactors"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1091504
    invoke-static {p0, p1}, LX/5DH;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1091505
    :cond_6
    const-string v12, "supported_reactions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1091506
    invoke-static {p0, p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1091507
    :cond_7
    const-string v12, "top_reactions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1091508
    invoke-static {p0, p1}, LX/5DG;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1091509
    :cond_8
    const-string v12, "viewer_acts_as_person"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1091510
    invoke-static {p0, p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1091511
    :cond_9
    const-string v12, "viewer_feedback_reaction_key"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1091512
    const/4 v0, 0x1

    .line 1091513
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    goto/16 :goto_1

    .line 1091514
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1091515
    if-eqz v1, :cond_b

    .line 1091516
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 1091517
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1091518
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1091519
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1091520
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1091521
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1091522
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1091523
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1091524
    if-eqz v0, :cond_c

    .line 1091525
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1091526
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1091527
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1091528
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1091529
    if-eqz v0, :cond_0

    .line 1091530
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091531
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1091532
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1091533
    if-eqz v0, :cond_1

    .line 1091534
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091535
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1091536
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091537
    if-eqz v0, :cond_2

    .line 1091538
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091539
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091540
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1091541
    if-eqz v0, :cond_3

    .line 1091542
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091543
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1091544
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091545
    if-eqz v0, :cond_4

    .line 1091546
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091547
    invoke-static {p0, v0, p2, p3}, LX/5DH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091548
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091549
    if-eqz v0, :cond_5

    .line 1091550
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091551
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091552
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091553
    if-eqz v0, :cond_6

    .line 1091554
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091555
    invoke-static {p0, v0, p2, p3}, LX/5DG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091556
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1091557
    if-eqz v0, :cond_7

    .line 1091558
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091559
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 1091560
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1091561
    if-eqz v0, :cond_8

    .line 1091562
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1091563
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1091564
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1091565
    return-void
.end method
