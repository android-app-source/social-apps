.class public final LX/5oM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1006692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1006693
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1006694
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1006695
    invoke-static {p0, p1}, LX/5oM;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1006696
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1006697
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1006698
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1006699
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1006700
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5oM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006701
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1006702
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1006703
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1006704
    const/16 v17, 0x0

    .line 1006705
    const/16 v16, 0x0

    .line 1006706
    const/4 v15, 0x0

    .line 1006707
    const/4 v14, 0x0

    .line 1006708
    const/4 v13, 0x0

    .line 1006709
    const/4 v12, 0x0

    .line 1006710
    const/4 v11, 0x0

    .line 1006711
    const/4 v10, 0x0

    .line 1006712
    const/4 v7, 0x0

    .line 1006713
    const-wide/16 v8, 0x0

    .line 1006714
    const/4 v6, 0x0

    .line 1006715
    const/4 v5, 0x0

    .line 1006716
    const/4 v4, 0x0

    .line 1006717
    const/4 v3, 0x0

    .line 1006718
    const/4 v2, 0x0

    .line 1006719
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 1006720
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1006721
    const/4 v2, 0x0

    .line 1006722
    :goto_0
    return v2

    .line 1006723
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    if-eq v6, v0, :cond_e

    .line 1006724
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1006725
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1006726
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_0

    if-eqz v6, :cond_0

    .line 1006727
    const-string v19, "id"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1006728
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move/from16 v18, v6

    goto :goto_1

    .line 1006729
    :cond_1
    const-string v19, "inspiration_landing_position"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1006730
    const/4 v3, 0x1

    .line 1006731
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v17, v6

    goto :goto_1

    .line 1006732
    :cond_2
    const-string v19, "prompt_confidence"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1006733
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    move/from16 v16, v6

    goto :goto_1

    .line 1006734
    :cond_3
    const-string v19, "prompt_display_reason"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1006735
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v6

    move v15, v6

    goto :goto_1

    .line 1006736
    :cond_4
    const-string v19, "prompt_feed_type"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1006737
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    move v14, v6

    goto/16 :goto_1

    .line 1006738
    :cond_5
    const-string v19, "prompt_image"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1006739
    invoke-static/range {p0 .. p1}, LX/5o3;->a(LX/15w;LX/186;)I

    move-result v6

    move v13, v6

    goto/16 :goto_1

    .line 1006740
    :cond_6
    const-string v19, "prompt_survey"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1006741
    invoke-static/range {p0 .. p1}, LX/5o4;->a(LX/15w;LX/186;)I

    move-result v6

    move v12, v6

    goto/16 :goto_1

    .line 1006742
    :cond_7
    const-string v19, "prompt_title"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1006743
    invoke-static/range {p0 .. p1}, LX/5o5;->a(LX/15w;LX/186;)I

    move-result v6

    move v11, v6

    goto/16 :goto_1

    .line 1006744
    :cond_8
    const-string v19, "prompt_type"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1006745
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPromptType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    move v7, v6

    goto/16 :goto_1

    .line 1006746
    :cond_9
    const-string v19, "ranking_score"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1006747
    const/4 v2, 0x1

    .line 1006748
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 1006749
    :cond_a
    const-string v19, "suggested_composition"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1006750
    invoke-static/range {p0 .. p1}, LX/5oK;->a(LX/15w;LX/186;)I

    move-result v6

    move v10, v6

    goto/16 :goto_1

    .line 1006751
    :cond_b
    const-string v19, "time_range"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 1006752
    invoke-static/range {p0 .. p1}, LX/5oL;->a(LX/15w;LX/186;)I

    move-result v6

    move v9, v6

    goto/16 :goto_1

    .line 1006753
    :cond_c
    const-string v19, "tracking_string"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1006754
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v8, v6

    goto/16 :goto_1

    .line 1006755
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1006756
    :cond_e
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1006757
    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1006758
    if-eqz v3, :cond_f

    .line 1006759
    const/4 v3, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1, v6}, LX/186;->a(III)V

    .line 1006760
    :cond_f
    const/4 v3, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1006761
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1006762
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1006763
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1006764
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1006765
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1006766
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1006767
    if-eqz v2, :cond_10

    .line 1006768
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1006769
    :cond_10
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1006770
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1006771
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1006772
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v6

    move-wide/from16 v21, v8

    move v9, v5

    move v8, v4

    move-wide/from16 v4, v21

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1006773
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1006774
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1006775
    if-eqz v0, :cond_0

    .line 1006776
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006777
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1006778
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 1006779
    if-eqz v0, :cond_1

    .line 1006780
    const-string v1, "inspiration_landing_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006781
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1006782
    :cond_1
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1006783
    if-eqz v0, :cond_2

    .line 1006784
    const-string v0, "prompt_confidence"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006785
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1006786
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006787
    if-eqz v0, :cond_3

    .line 1006788
    const-string v1, "prompt_display_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006789
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006790
    :cond_3
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1006791
    if-eqz v0, :cond_4

    .line 1006792
    const-string v0, "prompt_feed_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006793
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1006794
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006795
    if-eqz v0, :cond_5

    .line 1006796
    const-string v1, "prompt_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006797
    invoke-static {p0, v0, p2}, LX/5o3;->a(LX/15i;ILX/0nX;)V

    .line 1006798
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006799
    if-eqz v0, :cond_6

    .line 1006800
    const-string v1, "prompt_survey"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006801
    invoke-static {p0, v0, p2}, LX/5o4;->a(LX/15i;ILX/0nX;)V

    .line 1006802
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006803
    if-eqz v0, :cond_7

    .line 1006804
    const-string v1, "prompt_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006805
    invoke-static {p0, v0, p2}, LX/5o5;->a(LX/15i;ILX/0nX;)V

    .line 1006806
    :cond_7
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 1006807
    if-eqz v0, :cond_8

    .line 1006808
    const-string v0, "prompt_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006809
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1006810
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1006811
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_9

    .line 1006812
    const-string v2, "ranking_score"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006813
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1006814
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006815
    if-eqz v0, :cond_a

    .line 1006816
    const-string v1, "suggested_composition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006817
    invoke-static {p0, v0, p2, p3}, LX/5oK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006818
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006819
    if-eqz v0, :cond_b

    .line 1006820
    const-string v1, "time_range"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006821
    invoke-static {p0, v0, p2}, LX/5oL;->a(LX/15i;ILX/0nX;)V

    .line 1006822
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1006823
    if-eqz v0, :cond_c

    .line 1006824
    const-string v1, "tracking_string"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006825
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1006826
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1006827
    return-void
.end method
