.class public final LX/5H8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lcom/facebook/graphql/model/GraphQLActor;

.field public d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 889626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889627
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5H8;
    .locals 0

    .prologue
    .line 889632
    iput-object p1, p0, LX/5H8;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889633
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLActor;)LX/5H8;
    .locals 0

    .prologue
    .line 889628
    iput-object p1, p0, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889629
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5H8;
    .locals 0

    .prologue
    .line 889630
    iput-object p1, p0, LX/5H8;->a:Ljava/lang/String;

    .line 889631
    return-object p0
.end method

.method public final a(Z)LX/5H8;
    .locals 0

    .prologue
    .line 889622
    iput-boolean p1, p0, LX/5H8;->b:Z

    .line 889623
    return-object p0
.end method

.method public final a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;
    .locals 6

    .prologue
    .line 889620
    iget-object v0, p0, LX/5H8;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 889621
    new-instance v0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    iget-object v1, p0, LX/5H8;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/5H8;->b:Z

    iget-object v3, p0, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, LX/5H8;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v5, p0, LX/5H8;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;-><init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/5H8;
    .locals 0

    .prologue
    .line 889624
    iput-object p1, p0, LX/5H8;->e:Ljava/lang/String;

    .line 889625
    return-object p0
.end method
