.class public final LX/6JS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/6Jc;

.field private b:Z


# direct methods
.method public constructor <init>(LX/6Jc;)V
    .locals 0

    .prologue
    .line 1075533
    iput-object p1, p0, LX/6JS;->a:LX/6Jc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1075507
    iget-object v0, p0, LX/6JS;->a:LX/6Jc;

    iget-object v0, v0, LX/6Jc;->j:LX/6JU;

    iget-object v1, p0, LX/6JS;->a:LX/6Jc;

    iget-object v1, v1, LX/6Jc;->k:Landroid/os/Handler;

    invoke-static {v0, v1, p1}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1075508
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 10

    .prologue
    .line 1075509
    iget-boolean v0, p0, LX/6JS;->b:Z

    if-nez v0, :cond_0

    .line 1075510
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6JS;->b:Z

    .line 1075511
    iget-object v0, p0, LX/6JS;->a:LX/6Jc;

    iget-object v1, p0, LX/6JS;->a:LX/6Jc;

    iget-object v1, v1, LX/6Jc;->j:LX/6JU;

    iget-object v2, p0, LX/6JS;->a:LX/6Jc;

    iget-object v2, v2, LX/6Jc;->k:Landroid/os/Handler;

    .line 1075512
    iget-object v3, v0, LX/6Jc;->h:LX/6LC;

    new-instance v4, LX/6Ja;

    invoke-direct {v4, v0, v1, v2}, LX/6Ja;-><init>(LX/6Jc;LX/6JU;Landroid/os/Handler;)V

    iget-object v5, v0, LX/6Jc;->e:Landroid/os/Handler;

    invoke-interface {v3, v4, v5}, LX/6LC;->b(LX/6JU;Landroid/os/Handler;)V

    .line 1075513
    :cond_0
    iget-object v0, p0, LX/6JS;->a:LX/6Jc;

    iget-boolean v0, v0, LX/6Jc;->l:Z

    if-nez v0, :cond_1

    .line 1075514
    :goto_0
    return-void

    .line 1075515
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/6JS;->a:LX/6Jc;

    iget-object v0, v0, LX/6Jc;->f:LX/6L8;

    .line 1075516
    iget-boolean v3, v0, LX/6L8;->f:Z

    if-nez v3, :cond_2

    .line 1075517
    invoke-static {v0}, LX/6L8;->b(LX/6L8;)V

    .line 1075518
    iget-boolean v3, v0, LX/6L8;->f:Z

    if-nez v3, :cond_2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075519
    :goto_1
    goto :goto_0

    .line 1075520
    :catch_0
    move-exception v0

    .line 1075521
    invoke-virtual {p0, v0}, LX/6JS;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1075522
    :cond_2
    iget-wide v3, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v5, v0, LX/6L8;->i:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_3

    .line 1075523
    sget-object v3, LX/6L8;->a:Ljava/lang/String;

    const-string v4, "Audio PTS out of order - current pts %d last pts %d "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v7, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-wide v7, v0, LX/6L8;->i:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1075524
    :cond_3
    iget-wide v3, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v3, v0, LX/6L8;->i:J

    .line 1075525
    iget-wide v3, v0, LX/6L8;->h:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 1075526
    iget-wide v3, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v3, v0, LX/6L8;->h:J

    .line 1075527
    :cond_4
    iget-wide v3, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v5, v0, LX/6L8;->h:J

    sub-long v7, v3, v5

    .line 1075528
    iget v5, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v6, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget v9, p2, Landroid/media/MediaCodec$BufferInfo;->flags:I

    move-object v4, p2

    invoke-virtual/range {v4 .. v9}, Landroid/media/MediaCodec$BufferInfo;->set(IIJI)V

    .line 1075529
    iget-object v3, v0, LX/6L8;->b:LX/6L9;

    .line 1075530
    iget-object v4, v3, LX/6L9;->a:Landroid/media/MediaMuxer;

    iget v5, v3, LX/6L9;->b:I

    invoke-virtual {v4, v5, p1, p2}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1075531
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/6L9;->d:Z

    .line 1075532
    goto :goto_1
.end method
