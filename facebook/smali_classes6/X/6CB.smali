.class public LX/6CB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CA;


# instance fields
.field private final a:LX/6Cw;

.field private final b:LX/6CH;


# direct methods
.method public constructor <init>(LX/6Cw;LX/6CH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063500
    iput-object p1, p0, LX/6CB;->a:LX/6Cw;

    .line 1063501
    iput-object p2, p0, LX/6CB;->b:LX/6CH;

    .line 1063502
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1063503
    return-void
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 1063504
    invoke-static {p1}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1063505
    return-void
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1063506
    invoke-static {p2}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    if-ne v0, v1, :cond_0

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1063507
    :cond_0
    :goto_0
    return-void

    .line 1063508
    :cond_1
    const-string v0, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1063509
    const-string v1, "JS_BRIDGE_WEB_TITLE"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1063510
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1063511
    iget-object v0, p0, LX/6CB;->b:LX/6CH;

    const-string v1, "MessengerExtensionExitHandler"

    const-string v2, "Could not save result url %s as page id does not exists!"

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3, v3}, LX/6CH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1063512
    :cond_2
    iget-object v2, p0, LX/6CB;->a:LX/6Cw;

    .line 1063513
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1063514
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, v2, LX/6Cw;->a:Landroid/content/Context;

    const p0, 0x7f081c7f

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1063515
    :goto_1
    new-instance p0, LX/4HJ;

    invoke-direct {p0}, LX/4HJ;-><init>()V

    .line 1063516
    const-string p2, "page_id"

    invoke-virtual {p0, p2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063517
    move-object p0, p0

    .line 1063518
    const-string p2, "structured_menu_resume_url"

    invoke-virtual {p0, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063519
    move-object p0, p0

    .line 1063520
    const-string p2, "resume_url_text"

    invoke-virtual {p0, p2, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063521
    move-object v4, p0

    .line 1063522
    new-instance p0, LX/6Cq;

    invoke-direct {p0}, LX/6Cq;-><init>()V

    move-object p0, p0

    .line 1063523
    const-string p2, "input"

    invoke-virtual {p0, p2, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1063524
    iget-object v4, v2, LX/6Cw;->c:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/6Cu;

    invoke-direct {p0, v2, v3, v0}, LX/6Cu;-><init>(LX/6Cw;LX/6Cv;Ljava/lang/String;)V

    .line 1063525
    sget-object p2, LX/131;->INSTANCE:LX/131;

    move-object p2, p2

    .line 1063526
    invoke-static {v4, p0, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1063527
    goto :goto_0

    .line 1063528
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    :cond_4
    move-object v4, v1

    goto :goto_1
.end method
