.class public final LX/6Li;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)V
    .locals 0

    .prologue
    .line 1078698
    iput-object p1, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalFocusChanged(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x3e9

    .line 1078699
    iget-object v0, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    invoke-static {v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1078700
    iget-object v0, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v0, v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    invoke-virtual {v0, v1}, LX/6Lj;->removeMessages(I)V

    .line 1078701
    :cond_0
    :goto_0
    return-void

    .line 1078702
    :cond_1
    iget-object v0, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v0, v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    invoke-virtual {v0, v1}, LX/6Lj;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078703
    iget-object v0, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v0, v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1078704
    iget-object v1, p0, LX/6Li;->a:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v1, v1, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, LX/6Lj;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
