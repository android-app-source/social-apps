.class public LX/5Mk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/5Mk;


# instance fields
.field private final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class;",
            "LX/5Ml;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/5Ml;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 905679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905680
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 905681
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ml;

    .line 905682
    invoke-interface {v0}, LX/5Ml;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 905683
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/5Mk;->a:LX/0P1;

    .line 905684
    return-void
.end method

.method public static a(LX/0QB;)LX/5Mk;
    .locals 6

    .prologue
    .line 905685
    sget-object v0, LX/5Mk;->b:LX/5Mk;

    if-nez v0, :cond_1

    .line 905686
    const-class v1, LX/5Mk;

    monitor-enter v1

    .line 905687
    :try_start_0
    sget-object v0, LX/5Mk;->b:LX/5Mk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 905688
    if-eqz v2, :cond_0

    .line 905689
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 905690
    new-instance v3, LX/5Mk;

    .line 905691
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/5Mm;

    invoke-direct {p0, v0}, LX/5Mm;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 905692
    invoke-direct {v3, v4}, LX/5Mk;-><init>(Ljava/util/Set;)V

    .line 905693
    move-object v0, v3

    .line 905694
    sput-object v0, LX/5Mk;->b:LX/5Mk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 905695
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 905696
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 905697
    :cond_1
    sget-object v0, LX/5Mk;->b:LX/5Mk;

    return-object v0

    .line 905698
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 905699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/5Mj;)V
    .locals 2

    .prologue
    .line 905700
    instance-of v0, p1, LX/0fn;

    if-eqz v0, :cond_1

    .line 905701
    check-cast p1, LX/0fn;

    .line 905702
    invoke-interface {p1, p2}, LX/0fn;->a(LX/5Mj;)V

    .line 905703
    :cond_0
    :goto_0
    return-void

    .line 905704
    :cond_1
    iget-object v0, p0, LX/5Mk;->a:LX/0P1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ml;

    .line 905705
    if-nez v0, :cond_0

    .line 905706
    iget-object v0, p2, LX/5Mj;->e:Ljava/io/PrintWriter;

    move-object v0, v0

    .line 905707
    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
