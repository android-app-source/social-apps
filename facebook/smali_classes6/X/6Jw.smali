.class public LX/6Jw;
.super Lcom/facebook/videocodec/effects/renderers/TextRenderer;
.source ""

# interfaces
.implements LX/6Jv;


# instance fields
.field private final b:LX/6Kb;

.field private c:J

.field private d:LX/6Js;

.field private e:Z


# direct methods
.method public constructor <init>(LX/7Sq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1076054
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/facebook/videocodec/effects/renderers/TextRenderer;-><init>(Lcom/facebook/videocodec/effects/model/TextData;LX/7Sq;)V

    .line 1076055
    new-instance v0, LX/6Kb;

    invoke-direct {v0}, LX/6Kb;-><init>()V

    iput-object v0, p0, LX/6Jw;->b:LX/6Kb;

    .line 1076056
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1076057
    new-instance v1, Lcom/facebook/videocodec/effects/model/TextData$Text;

    const-string v2, "Fps: "

    invoke-direct {v1, v2, v3, v3}, Lcom/facebook/videocodec/effects/model/TextData$Text;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1076058
    new-instance v1, Lcom/facebook/videocodec/effects/model/TextData;

    const-string v2, ""

    const/16 v3, 0x32

    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/videocodec/effects/model/TextData;-><init>(Ljava/lang/String;ILjava/util/List;)V

    iput-object v1, p0, LX/6Jw;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076059
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/6Jw;->c:J

    .line 1076060
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Jw;->e:Z

    .line 1076061
    return-void
.end method


# virtual methods
.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1076062
    iget-object v0, p0, LX/6Jw;->d:LX/6Js;

    if-eqz v0, :cond_0

    .line 1076063
    iget-object v0, p0, LX/6Jw;->d:LX/6Js;

    sget-object v1, LX/7Sc;->FPS_TOGGLE_EVENT:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1076064
    :cond_0
    iput-object p1, p0, LX/6Jw;->d:LX/6Js;

    .line 1076065
    iget-object v0, p0, LX/6Jw;->d:LX/6Js;

    if-eqz v0, :cond_1

    .line 1076066
    iget-object v0, p0, LX/6Jw;->d:LX/6Js;

    sget-object v1, LX/7Sc;->FPS_TOGGLE_EVENT:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1076067
    :cond_1
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 2

    .prologue
    .line 1076038
    sget-object v0, LX/6Ju;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1076039
    const-string v0, "FpsRenderer"

    const-string v1, "Received an event we did not register for"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076040
    :goto_0
    return-void

    .line 1076041
    :pswitch_0
    check-cast p1, LX/7Sh;

    .line 1076042
    iget-boolean v0, p1, LX/7Sh;->a:Z

    move v0, v0

    .line 1076043
    iput-boolean v0, p0, LX/6Jw;->e:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1076045
    iget-boolean v0, p0, LX/6Jw;->e:Z

    if-nez v0, :cond_1

    .line 1076046
    :cond_0
    :goto_0
    return-void

    .line 1076047
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a([F[F[FJ)V

    .line 1076048
    iget-object v0, p0, LX/6Jw;->b:LX/6Kb;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/6Kb;->a(J)V

    .line 1076049
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, LX/6Jw;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1dcd6500

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1076050
    iget-object v0, p0, LX/6Jw;->b:LX/6Kb;

    invoke-virtual {v0}, LX/6Kb;->a()LX/6Ka;

    move-result-object v1

    .line 1076051
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/TextData;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/TextData$Text;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fps: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v1, LX/6Ka;->b:J

    long-to-float v3, v4

    iget v1, v1, LX/6Ka;->a:F

    div-float v1, v3, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/videocodec/effects/model/TextData$Text;->a:Ljava/lang/String;

    .line 1076052
    iget-object v0, p0, LX/6Jw;->b:LX/6Kb;

    invoke-virtual {v0}, LX/6Kb;->b()V

    .line 1076053
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/6Jw;->c:J

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1076044
    iget-boolean v0, p0, LX/6Jw;->e:Z

    return v0
.end method
