.class public final LX/5gn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 976849
    const/16 v28, 0x0

    .line 976850
    const/16 v27, 0x0

    .line 976851
    const/16 v26, 0x0

    .line 976852
    const/16 v25, 0x0

    .line 976853
    const/16 v24, 0x0

    .line 976854
    const/16 v23, 0x0

    .line 976855
    const/16 v22, 0x0

    .line 976856
    const-wide/16 v20, 0x0

    .line 976857
    const/16 v19, 0x0

    .line 976858
    const/16 v18, 0x0

    .line 976859
    const/16 v17, 0x0

    .line 976860
    const/16 v16, 0x0

    .line 976861
    const/4 v13, 0x0

    .line 976862
    const-wide/16 v14, 0x0

    .line 976863
    const/4 v12, 0x0

    .line 976864
    const/4 v11, 0x0

    .line 976865
    const/4 v10, 0x0

    .line 976866
    const/4 v9, 0x0

    .line 976867
    const/4 v8, 0x0

    .line 976868
    const/4 v7, 0x0

    .line 976869
    const/4 v6, 0x0

    .line 976870
    const/4 v5, 0x0

    .line 976871
    const/4 v4, 0x0

    .line 976872
    const/4 v3, 0x0

    .line 976873
    const/4 v2, 0x0

    .line 976874
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1b

    .line 976875
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 976876
    const/4 v2, 0x0

    .line 976877
    :goto_0
    return v2

    .line 976878
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v30, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v30

    if-eq v2, v0, :cond_14

    .line 976879
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 976880
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 976881
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 976882
    const-string v30, "album_cover_photo"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 976883
    invoke-static/range {p0 .. p1}, LX/5go;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 976884
    :cond_1
    const-string v30, "album_type"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    .line 976885
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 976886
    :cond_2
    const-string v30, "allow_contributors"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 976887
    const/4 v2, 0x1

    .line 976888
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v27, v10

    move v10, v2

    goto :goto_1

    .line 976889
    :cond_3
    const-string v30, "can_edit_caption"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 976890
    const/4 v2, 0x1

    .line 976891
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v26, v9

    move v9, v2

    goto :goto_1

    .line 976892
    :cond_4
    const-string v30, "can_upload"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 976893
    const/4 v2, 0x1

    .line 976894
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v25, v7

    move v7, v2

    goto/16 :goto_1

    .line 976895
    :cond_5
    const-string v30, "can_viewer_delete"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 976896
    const/4 v2, 0x1

    .line 976897
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v24, v6

    move v6, v2

    goto/16 :goto_1

    .line 976898
    :cond_6
    const-string v30, "contributors"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 976899
    invoke-static/range {p0 .. p1}, LX/5gk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 976900
    :cond_7
    const-string v30, "created_time"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 976901
    const/4 v2, 0x1

    .line 976902
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 976903
    :cond_8
    const-string v30, "explicit_place"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 976904
    invoke-static/range {p0 .. p1}, LX/5gp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 976905
    :cond_9
    const-string v30, "id"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 976906
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 976907
    :cond_a
    const-string v30, "media"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 976908
    invoke-static/range {p0 .. p1}, LX/5gm;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 976909
    :cond_b
    const-string v30, "media_owner_object"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 976910
    invoke-static/range {p0 .. p1}, LX/5gq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 976911
    :cond_c
    const-string v30, "message"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 976912
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 976913
    :cond_d
    const-string v30, "modified_time"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 976914
    const/4 v2, 0x1

    .line 976915
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 976916
    :cond_e
    const-string v30, "owner"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 976917
    invoke-static/range {p0 .. p1}, LX/5gr;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 976918
    :cond_f
    const-string v30, "photo_items"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    .line 976919
    invoke-static/range {p0 .. p1}, LX/5gs;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 976920
    :cond_10
    const-string v30, "privacy_scope"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    .line 976921
    invoke-static/range {p0 .. p1}, LX/5gv;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 976922
    :cond_11
    const-string v30, "title"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_12

    .line 976923
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 976924
    :cond_12
    const-string v30, "url"

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 976925
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 976926
    :cond_13
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 976927
    :cond_14
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 976928
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976929
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976930
    if-eqz v10, :cond_15

    .line 976931
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 976932
    :cond_15
    if-eqz v9, :cond_16

    .line 976933
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 976934
    :cond_16
    if-eqz v7, :cond_17

    .line 976935
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 976936
    :cond_17
    if-eqz v6, :cond_18

    .line 976937
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 976938
    :cond_18
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976939
    if-eqz v3, :cond_19

    .line 976940
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 976941
    :cond_19
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976942
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976943
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976944
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976945
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 976946
    if-eqz v8, :cond_1a

    .line 976947
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 976948
    :cond_1a
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 976949
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 976950
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 976951
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 976952
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 976953
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1b
    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v32, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move/from16 v33, v11

    move v11, v8

    move v8, v2

    move/from16 v34, v18

    move/from16 v18, v13

    move v13, v10

    move v10, v7

    move v7, v5

    move-wide/from16 v4, v20

    move/from16 v21, v34

    move/from16 v20, v17

    move-wide/from16 v16, v14

    move/from16 v14, v33

    move/from16 v15, v32

    goto/16 :goto_1
.end method
