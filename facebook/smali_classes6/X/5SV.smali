.class public final enum LX/5SV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5SV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5SV;

.field public static final enum METHOD:LX/5SV;

.field public static final enum READONLY_PROPERTY_GETTER:LX/5SV;

.field public static final enum UNCACHED_PROPERTY_GETTER:LX/5SV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 918342
    new-instance v0, LX/5SV;

    const-string v1, "READONLY_PROPERTY_GETTER"

    invoke-direct {v0, v1, v2}, LX/5SV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;

    .line 918343
    new-instance v0, LX/5SV;

    const-string v1, "UNCACHED_PROPERTY_GETTER"

    invoke-direct {v0, v1, v3}, LX/5SV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SV;->UNCACHED_PROPERTY_GETTER:LX/5SV;

    .line 918344
    new-instance v0, LX/5SV;

    const-string v1, "METHOD"

    invoke-direct {v0, v1, v4}, LX/5SV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SV;->METHOD:LX/5SV;

    .line 918345
    const/4 v0, 0x3

    new-array v0, v0, [LX/5SV;

    sget-object v1, LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;

    aput-object v1, v0, v2

    sget-object v1, LX/5SV;->UNCACHED_PROPERTY_GETTER:LX/5SV;

    aput-object v1, v0, v3

    sget-object v1, LX/5SV;->METHOD:LX/5SV;

    aput-object v1, v0, v4

    sput-object v0, LX/5SV;->$VALUES:[LX/5SV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 918347
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5SV;
    .locals 1

    .prologue
    .line 918348
    const-class v0, LX/5SV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SV;

    return-object v0
.end method

.method public static values()[LX/5SV;
    .locals 1

    .prologue
    .line 918346
    sget-object v0, LX/5SV;->$VALUES:[LX/5SV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5SV;

    return-object v0
.end method
