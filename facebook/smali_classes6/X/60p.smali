.class public final LX/60p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:J

.field public o:I

.field public p:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1039195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039196
    const-string v0, ""

    iput-object v0, p0, LX/60p;->b:Ljava/lang/String;

    .line 1039197
    const-string v0, ""

    iput-object v0, p0, LX/60p;->c:Ljava/lang/String;

    .line 1039198
    const-string v0, ""

    iput-object v0, p0, LX/60p;->d:Ljava/lang/String;

    .line 1039199
    iput-wide v2, p0, LX/60p;->e:J

    .line 1039200
    iput-wide v2, p0, LX/60p;->f:J

    .line 1039201
    iput-wide v2, p0, LX/60p;->g:J

    .line 1039202
    iput-wide v2, p0, LX/60p;->h:J

    .line 1039203
    iput-wide v2, p0, LX/60p;->n:J

    .line 1039204
    iput v1, p0, LX/60p;->o:I

    .line 1039205
    iput v1, p0, LX/60p;->a:I

    .line 1039206
    return-void
.end method


# virtual methods
.method public final a(I)LX/60p;
    .locals 0

    .prologue
    .line 1039193
    iput p1, p0, LX/60p;->o:I

    .line 1039194
    return-object p0
.end method

.method public final a(J)LX/60p;
    .locals 1

    .prologue
    .line 1039191
    iput-wide p1, p0, LX/60p;->e:J

    .line 1039192
    return-object p0
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)LX/60p;
    .locals 0

    .prologue
    .line 1039189
    iput-object p1, p0, LX/60p;->p:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1039190
    return-object p0
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)LX/60p;
    .locals 0
    .param p1    # Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1039187
    iput-object p1, p0, LX/60p;->k:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1039188
    return-object p0
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)LX/60p;
    .locals 0
    .param p1    # Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1039185
    iput-object p1, p0, LX/60p;->i:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039186
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/60p;
    .locals 0

    .prologue
    .line 1039183
    iput-object p1, p0, LX/60p;->b:Ljava/lang/String;

    .line 1039184
    return-object p0
.end method

.method public final a(Z)LX/60p;
    .locals 0

    .prologue
    .line 1039181
    iput-boolean p1, p0, LX/60p;->m:Z

    .line 1039182
    return-object p0
.end method

.method public final a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .locals 2

    .prologue
    .line 1039162
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-direct {v0, p0}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;-><init>(LX/60p;)V

    return-object v0
.end method

.method public final b(I)LX/60p;
    .locals 0

    .prologue
    .line 1039179
    iput p1, p0, LX/60p;->a:I

    .line 1039180
    return-object p0
.end method

.method public final b(J)LX/60p;
    .locals 1

    .prologue
    .line 1039177
    iput-wide p1, p0, LX/60p;->f:J

    .line 1039178
    return-object p0
.end method

.method public final b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)LX/60p;
    .locals 0
    .param p1    # Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1039175
    iput-object p1, p0, LX/60p;->j:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039176
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/60p;
    .locals 0

    .prologue
    .line 1039173
    iput-object p1, p0, LX/60p;->c:Ljava/lang/String;

    .line 1039174
    return-object p0
.end method

.method public final c(J)LX/60p;
    .locals 1

    .prologue
    .line 1039171
    iput-wide p1, p0, LX/60p;->g:J

    .line 1039172
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/60p;
    .locals 0

    .prologue
    .line 1039169
    iput-object p1, p0, LX/60p;->d:Ljava/lang/String;

    .line 1039170
    return-object p0
.end method

.method public final d(J)LX/60p;
    .locals 1

    .prologue
    .line 1039167
    iput-wide p1, p0, LX/60p;->h:J

    .line 1039168
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/60p;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1039165
    iput-object p1, p0, LX/60p;->l:Ljava/lang/String;

    .line 1039166
    return-object p0
.end method

.method public final e(J)LX/60p;
    .locals 1

    .prologue
    .line 1039163
    iput-wide p1, p0, LX/60p;->n:J

    .line 1039164
    return-object p0
.end method
