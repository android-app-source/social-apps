.class public final LX/65H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:LX/650;

.field public final c:LX/655;

.field private d:Ljava/util/Date;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/Date;

.field private g:Ljava/lang/String;

.field public h:Ljava/util/Date;

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:I


# direct methods
.method public constructor <init>(JLX/650;LX/655;)V
    .locals 9

    .prologue
    const/4 v6, -0x1

    .line 1046791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046792
    iput v6, p0, LX/65H;->l:I

    .line 1046793
    iput-wide p1, p0, LX/65H;->a:J

    .line 1046794
    iput-object p3, p0, LX/65H;->b:LX/650;

    .line 1046795
    iput-object p4, p0, LX/65H;->c:LX/655;

    .line 1046796
    if-eqz p4, :cond_5

    .line 1046797
    iget-wide v7, p4, LX/655;->k:J

    move-wide v0, v7

    .line 1046798
    iput-wide v0, p0, LX/65H;->i:J

    .line 1046799
    iget-wide v7, p4, LX/655;->l:J

    move-wide v0, v7

    .line 1046800
    iput-wide v0, p0, LX/65H;->j:J

    .line 1046801
    iget-object v0, p4, LX/655;->f:LX/64n;

    move-object v1, v0

    .line 1046802
    const/4 v0, 0x0

    invoke-virtual {v1}, LX/64n;->a()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_5

    .line 1046803
    invoke-virtual {v1, v0}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 1046804
    invoke-virtual {v1, v0}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 1046805
    const-string v5, "Date"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1046806
    invoke-static {v4}, LX/66L;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    iput-object v3, p0, LX/65H;->d:Ljava/util/Date;

    .line 1046807
    iput-object v4, p0, LX/65H;->e:Ljava/lang/String;

    .line 1046808
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1046809
    :cond_1
    const-string v5, "Expires"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1046810
    invoke-static {v4}, LX/66L;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    iput-object v3, p0, LX/65H;->h:Ljava/util/Date;

    goto :goto_1

    .line 1046811
    :cond_2
    const-string v5, "Last-Modified"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1046812
    invoke-static {v4}, LX/66L;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    iput-object v3, p0, LX/65H;->f:Ljava/util/Date;

    .line 1046813
    iput-object v4, p0, LX/65H;->g:Ljava/lang/String;

    goto :goto_1

    .line 1046814
    :cond_3
    const-string v5, "ETag"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1046815
    iput-object v4, p0, LX/65H;->k:Ljava/lang/String;

    goto :goto_1

    .line 1046816
    :cond_4
    const-string v5, "Age"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1046817
    invoke-static {v4, v6}, LX/66M;->b(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, LX/65H;->l:I

    goto :goto_1

    .line 1046818
    :cond_5
    return-void
.end method

.method public static b(LX/65H;)LX/65I;
    .locals 15

    .prologue
    const-wide/16 v4, 0x0

    const/4 v14, -0x1

    const/4 v13, 0x0

    .line 1046819
    iget-object v0, p0, LX/65H;->c:LX/655;

    if-nez v0, :cond_0

    .line 1046820
    new-instance v0, LX/65I;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-direct {v0, v1, v13}, LX/65I;-><init>(LX/650;LX/655;)V

    .line 1046821
    :goto_0
    return-object v0

    .line 1046822
    :cond_0
    iget-object v0, p0, LX/65H;->b:LX/650;

    invoke-virtual {v0}, LX/650;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/65H;->c:LX/655;

    .line 1046823
    iget-object v1, v0, LX/655;->e:LX/64l;

    move-object v0, v1

    .line 1046824
    if-nez v0, :cond_1

    .line 1046825
    new-instance v0, LX/65I;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-direct {v0, v1, v13}, LX/65I;-><init>(LX/650;LX/655;)V

    goto :goto_0

    .line 1046826
    :cond_1
    iget-object v0, p0, LX/65H;->c:LX/655;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-static {v0, v1}, LX/65I;->a(LX/655;LX/650;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1046827
    new-instance v0, LX/65I;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-direct {v0, v1, v13}, LX/65I;-><init>(LX/650;LX/655;)V

    goto :goto_0

    .line 1046828
    :cond_2
    iget-object v0, p0, LX/65H;->b:LX/650;

    invoke-virtual {v0}, LX/650;->e()LX/64W;

    move-result-object v6

    .line 1046829
    iget-boolean v0, v6, LX/64W;->d:Z

    move v0, v0

    .line 1046830
    if-nez v0, :cond_4

    iget-object v0, p0, LX/65H;->b:LX/650;

    .line 1046831
    const-string v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v1, "If-None-Match"

    invoke-virtual {v0, v1}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    :cond_3
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1046832
    if-eqz v0, :cond_5

    .line 1046833
    :cond_4
    new-instance v0, LX/65I;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-direct {v0, v1, v13}, LX/65I;-><init>(LX/650;LX/655;)V

    goto :goto_0

    .line 1046834
    :cond_5
    invoke-static {p0}, LX/65H;->d(LX/65H;)J

    move-result-wide v8

    .line 1046835
    invoke-static {p0}, LX/65H;->c(LX/65H;)J

    move-result-wide v0

    .line 1046836
    iget v2, v6, LX/64W;->f:I

    move v2, v2

    .line 1046837
    if-eq v2, v14, :cond_6

    .line 1046838
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1046839
    iget v3, v6, LX/64W;->f:I

    move v3, v3

    .line 1046840
    int-to-long v10, v3

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1046841
    :cond_6
    iget v2, v6, LX/64W;->l:I

    move v2, v2

    .line 1046842
    if-eq v2, v14, :cond_e

    .line 1046843
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1046844
    iget v3, v6, LX/64W;->l:I

    move v3, v3

    .line 1046845
    int-to-long v10, v3

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 1046846
    :goto_2
    iget-object v7, p0, LX/65H;->c:LX/655;

    invoke-virtual {v7}, LX/655;->h()LX/64W;

    move-result-object v7

    .line 1046847
    iget-boolean v10, v7, LX/64W;->j:Z

    move v10, v10

    .line 1046848
    if-nez v10, :cond_7

    .line 1046849
    iget v10, v6, LX/64W;->k:I

    move v10, v10

    .line 1046850
    if-eq v10, v14, :cond_7

    .line 1046851
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1046852
    iget v5, v6, LX/64W;->k:I

    move v5, v5

    .line 1046853
    int-to-long v10, v5

    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 1046854
    :cond_7
    iget-boolean v6, v7, LX/64W;->d:Z

    move v6, v6

    .line 1046855
    if-nez v6, :cond_a

    add-long v6, v8, v2

    add-long/2addr v4, v0

    cmp-long v4, v6, v4

    if-gez v4, :cond_a

    .line 1046856
    iget-object v4, p0, LX/65H;->c:LX/655;

    invoke-virtual {v4}, LX/655;->newBuilder()LX/654;

    move-result-object v4

    .line 1046857
    add-long/2addr v2, v8

    cmp-long v0, v2, v0

    if-ltz v0, :cond_8

    .line 1046858
    const-string v0, "Warning"

    const-string v1, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v4, v0, v1}, LX/654;->a(Ljava/lang/String;Ljava/lang/String;)LX/654;

    .line 1046859
    :cond_8
    const-wide/32 v0, 0x5265c00

    cmp-long v0, v8, v0

    if-lez v0, :cond_9

    .line 1046860
    iget-object v0, p0, LX/65H;->c:LX/655;

    invoke-virtual {v0}, LX/655;->h()LX/64W;

    move-result-object v0

    .line 1046861
    iget v1, v0, LX/64W;->f:I

    move v0, v1

    .line 1046862
    const/4 v1, -0x1

    if-ne v0, v1, :cond_10

    iget-object v0, p0, LX/65H;->h:Ljava/util/Date;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1046863
    if-eqz v0, :cond_9

    .line 1046864
    const-string v0, "Warning"

    const-string v1, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v4, v0, v1}, LX/654;->a(Ljava/lang/String;Ljava/lang/String;)LX/654;

    .line 1046865
    :cond_9
    new-instance v0, LX/65I;

    invoke-virtual {v4}, LX/654;->a()LX/655;

    move-result-object v1

    invoke-direct {v0, v13, v1}, LX/65I;-><init>(LX/650;LX/655;)V

    goto/16 :goto_0

    .line 1046866
    :cond_a
    iget-object v0, p0, LX/65H;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1046867
    const-string v1, "If-None-Match"

    .line 1046868
    iget-object v0, p0, LX/65H;->k:Ljava/lang/String;

    .line 1046869
    :goto_4
    iget-object v2, p0, LX/65H;->b:LX/650;

    .line 1046870
    iget-object v3, v2, LX/650;->c:LX/64n;

    move-object v2, v3

    .line 1046871
    invoke-virtual {v2}, LX/64n;->newBuilder()LX/64m;

    move-result-object v2

    .line 1046872
    sget-object v3, LX/64t;->a:LX/64t;

    invoke-virtual {v3, v2, v1, v0}, LX/64t;->a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046873
    iget-object v0, p0, LX/65H;->b:LX/650;

    invoke-virtual {v0}, LX/650;->newBuilder()LX/64z;

    move-result-object v0

    .line 1046874
    invoke-virtual {v2}, LX/64m;->a()LX/64n;

    move-result-object v1

    .line 1046875
    invoke-virtual {v1}, LX/64n;->newBuilder()LX/64m;

    move-result-object v2

    iput-object v2, v0, LX/64z;->c:LX/64m;

    .line 1046876
    move-object v0, v0

    .line 1046877
    invoke-virtual {v0}, LX/64z;->b()LX/650;

    move-result-object v1

    .line 1046878
    new-instance v0, LX/65I;

    iget-object v2, p0, LX/65H;->c:LX/655;

    invoke-direct {v0, v1, v2}, LX/65I;-><init>(LX/650;LX/655;)V

    goto/16 :goto_0

    .line 1046879
    :cond_b
    iget-object v0, p0, LX/65H;->f:Ljava/util/Date;

    if-eqz v0, :cond_c

    .line 1046880
    const-string v1, "If-Modified-Since"

    .line 1046881
    iget-object v0, p0, LX/65H;->g:Ljava/lang/String;

    goto :goto_4

    .line 1046882
    :cond_c
    iget-object v0, p0, LX/65H;->d:Ljava/util/Date;

    if-eqz v0, :cond_d

    .line 1046883
    const-string v1, "If-Modified-Since"

    .line 1046884
    iget-object v0, p0, LX/65H;->e:Ljava/lang/String;

    goto :goto_4

    .line 1046885
    :cond_d
    new-instance v0, LX/65I;

    iget-object v1, p0, LX/65H;->b:LX/650;

    invoke-direct {v0, v1, v13}, LX/65I;-><init>(LX/650;LX/655;)V

    goto/16 :goto_0

    :cond_e
    move-wide v2, v4

    goto/16 :goto_2

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static c(LX/65H;)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 1046886
    iget-object v0, p0, LX/65H;->c:LX/655;

    invoke-virtual {v0}, LX/655;->h()LX/64W;

    move-result-object v0

    .line 1046887
    iget v1, v0, LX/64W;->f:I

    move v1, v1

    .line 1046888
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    .line 1046889
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1046890
    iget v2, v0, LX/64W;->f:I

    move v0, v2

    .line 1046891
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1046892
    :cond_0
    :goto_0
    return-wide v0

    .line 1046893
    :cond_1
    iget-object v0, p0, LX/65H;->h:Ljava/util/Date;

    if-eqz v0, :cond_3

    .line 1046894
    iget-object v0, p0, LX/65H;->d:Ljava/util/Date;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/65H;->d:Ljava/util/Date;

    .line 1046895
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1046896
    :goto_1
    iget-object v4, p0, LX/65H;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 1046897
    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    move-wide v0, v2

    goto :goto_0

    .line 1046898
    :cond_2
    iget-wide v0, p0, LX/65H;->j:J

    goto :goto_1

    .line 1046899
    :cond_3
    iget-object v0, p0, LX/65H;->f:Ljava/util/Date;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/65H;->c:LX/655;

    .line 1046900
    iget-object v1, v0, LX/655;->a:LX/650;

    move-object v0, v1

    .line 1046901
    iget-object v1, v0, LX/650;->a:LX/64q;

    move-object v0, v1

    .line 1046902
    iget-object v1, v0, LX/64q;->h:Ljava/util/List;

    if-nez v1, :cond_7

    const/4 v1, 0x0

    .line 1046903
    :goto_2
    move-object v0, v1

    .line 1046904
    if-nez v0, :cond_6

    .line 1046905
    iget-object v0, p0, LX/65H;->d:Ljava/util/Date;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/65H;->d:Ljava/util/Date;

    .line 1046906
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1046907
    :goto_3
    iget-object v4, p0, LX/65H;->f:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 1046908
    cmp-long v4, v0, v2

    if-lez v4, :cond_5

    const-wide/16 v2, 0xa

    div-long/2addr v0, v2

    goto :goto_0

    .line 1046909
    :cond_4
    iget-wide v0, p0, LX/65H;->i:J

    goto :goto_3

    :cond_5
    move-wide v0, v2

    .line 1046910
    goto :goto_0

    :cond_6
    move-wide v0, v2

    .line 1046911
    goto :goto_0

    .line 1046912
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1046913
    iget-object v4, v0, LX/64q;->h:Ljava/util/List;

    invoke-static {v1, v4}, LX/64q;->b(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1046914
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private static d(LX/65H;)J
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 1046915
    iget-object v2, p0, LX/65H;->d:Ljava/util/Date;

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/65H;->j:J

    iget-object v4, p0, LX/65H;->d:Ljava/util/Date;

    .line 1046916
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1046917
    :cond_0
    iget v2, p0, LX/65H;->l:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, p0, LX/65H;->l:I

    int-to-long v4, v3

    .line 1046918
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1046919
    :cond_1
    iget-wide v2, p0, LX/65H;->j:J

    iget-wide v4, p0, LX/65H;->i:J

    sub-long/2addr v2, v4

    .line 1046920
    iget-wide v4, p0, LX/65H;->a:J

    iget-wide v6, p0, LX/65H;->j:J

    sub-long/2addr v4, v6

    .line 1046921
    add-long/2addr v0, v2

    add-long/2addr v0, v4

    return-wide v0
.end method
