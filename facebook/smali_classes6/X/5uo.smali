.class public final enum LX/5uo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5uo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5uo;

.field public static final enum ARCHIVE:LX/5uo;

.field public static final enum REMOVE_FROM_ARCHIVE:LX/5uo;

.field public static final enum SAVE:LX/5uo;

.field public static final enum UNARCHIVE:LX/5uo;

.field public static final enum UNSAVE:LX/5uo;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1020154
    new-instance v0, LX/5uo;

    const-string v1, "SAVE"

    const-string v2, "SAVE"

    invoke-direct {v0, v1, v3, v2}, LX/5uo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5uo;->SAVE:LX/5uo;

    .line 1020155
    new-instance v0, LX/5uo;

    const-string v1, "UNSAVE"

    const-string v2, "UNSAVE"

    invoke-direct {v0, v1, v4, v2}, LX/5uo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5uo;->UNSAVE:LX/5uo;

    .line 1020156
    new-instance v0, LX/5uo;

    const-string v1, "ARCHIVE"

    const-string v2, "ARCHIVE"

    invoke-direct {v0, v1, v5, v2}, LX/5uo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5uo;->ARCHIVE:LX/5uo;

    .line 1020157
    new-instance v0, LX/5uo;

    const-string v1, "UNARCHIVE"

    const-string v2, "UNARCHIVE"

    invoke-direct {v0, v1, v6, v2}, LX/5uo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5uo;->UNARCHIVE:LX/5uo;

    .line 1020158
    new-instance v0, LX/5uo;

    const-string v1, "REMOVE_FROM_ARCHIVE"

    const-string v2, "REMOVE_FROM_ARCHIVE"

    invoke-direct {v0, v1, v7, v2}, LX/5uo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5uo;->REMOVE_FROM_ARCHIVE:LX/5uo;

    .line 1020159
    const/4 v0, 0x5

    new-array v0, v0, [LX/5uo;

    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    aput-object v1, v0, v3

    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    aput-object v1, v0, v4

    sget-object v1, LX/5uo;->ARCHIVE:LX/5uo;

    aput-object v1, v0, v5

    sget-object v1, LX/5uo;->UNARCHIVE:LX/5uo;

    aput-object v1, v0, v6

    sget-object v1, LX/5uo;->REMOVE_FROM_ARCHIVE:LX/5uo;

    aput-object v1, v0, v7

    sput-object v0, LX/5uo;->$VALUES:[LX/5uo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1020160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1020161
    iput-object p3, p0, LX/5uo;->value:Ljava/lang/String;

    .line 1020162
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5uo;
    .locals 1

    .prologue
    .line 1020163
    const-class v0, LX/5uo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5uo;

    return-object v0
.end method

.method public static values()[LX/5uo;
    .locals 1

    .prologue
    .line 1020164
    sget-object v0, LX/5uo;->$VALUES:[LX/5uo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5uo;

    return-object v0
.end method
