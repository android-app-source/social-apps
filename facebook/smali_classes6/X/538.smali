.class public final LX/538;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/52p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/52p;"
    }
.end annotation


# instance fields
.field public final a:LX/53A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53A",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/53A;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53A",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826667
    iput-object p1, p0, LX/538;->a:LX/53A;

    .line 826668
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 6

    .prologue
    .line 826669
    iget-object v0, p0, LX/538;->a:LX/53A;

    .line 826670
    sget-object v1, LX/53A;->i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v1, v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 826671
    iget-object v1, v0, LX/53A;->c:LX/537;

    if-nez v1, :cond_1

    iget v1, v0, LX/53A;->d:I

    if-lez v1, :cond_1

    .line 826672
    invoke-static {v0}, LX/53A;->g(LX/53A;)V

    .line 826673
    :cond_0
    :goto_0
    return-void

    .line 826674
    :cond_1
    iget-object v1, v0, LX/53A;->c:LX/537;

    if-eqz v1, :cond_0

    .line 826675
    iget-object v1, v0, LX/53A;->c:LX/537;

    .line 826676
    invoke-virtual {v1, p1, p2}, LX/0zZ;->a(J)V

    .line 826677
    goto :goto_0
.end method
