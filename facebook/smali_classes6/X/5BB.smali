.class public final LX/5BB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 46

    .prologue
    .line 861938
    const/16 v42, 0x0

    .line 861939
    const/16 v41, 0x0

    .line 861940
    const/16 v40, 0x0

    .line 861941
    const/16 v39, 0x0

    .line 861942
    const/16 v38, 0x0

    .line 861943
    const/16 v37, 0x0

    .line 861944
    const/16 v36, 0x0

    .line 861945
    const/16 v35, 0x0

    .line 861946
    const/16 v34, 0x0

    .line 861947
    const/16 v33, 0x0

    .line 861948
    const/16 v32, 0x0

    .line 861949
    const/16 v31, 0x0

    .line 861950
    const/16 v30, 0x0

    .line 861951
    const/16 v29, 0x0

    .line 861952
    const/16 v28, 0x0

    .line 861953
    const/16 v27, 0x0

    .line 861954
    const/16 v26, 0x0

    .line 861955
    const/16 v25, 0x0

    .line 861956
    const/16 v24, 0x0

    .line 861957
    const/16 v23, 0x0

    .line 861958
    const/16 v22, 0x0

    .line 861959
    const/16 v21, 0x0

    .line 861960
    const/16 v20, 0x0

    .line 861961
    const/16 v19, 0x0

    .line 861962
    const/16 v18, 0x0

    .line 861963
    const/16 v17, 0x0

    .line 861964
    const/16 v16, 0x0

    .line 861965
    const/4 v15, 0x0

    .line 861966
    const/4 v14, 0x0

    .line 861967
    const/4 v13, 0x0

    .line 861968
    const/4 v12, 0x0

    .line 861969
    const/4 v11, 0x0

    .line 861970
    const/4 v10, 0x0

    .line 861971
    const/4 v9, 0x0

    .line 861972
    const/4 v8, 0x0

    .line 861973
    const/4 v7, 0x0

    .line 861974
    const/4 v6, 0x0

    .line 861975
    const/4 v5, 0x0

    .line 861976
    const/4 v4, 0x0

    .line 861977
    const/4 v3, 0x0

    .line 861978
    const/4 v2, 0x0

    .line 861979
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1

    .line 861980
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861981
    const/4 v2, 0x0

    .line 861982
    :goto_0
    return v2

    .line 861983
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 861984
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_1e

    .line 861985
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v43

    .line 861986
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 861987
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1

    if-eqz v43, :cond_1

    .line 861988
    const-string v44, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2

    .line 861989
    const/4 v13, 0x1

    .line 861990
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 861991
    :cond_2
    const-string v44, "can_see_voice_switcher"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_3

    .line 861992
    const/4 v12, 0x1

    .line 861993
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 861994
    :cond_3
    const-string v44, "can_viewer_comment"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_4

    .line 861995
    const/4 v11, 0x1

    .line 861996
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 861997
    :cond_4
    const-string v44, "can_viewer_comment_with_photo"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_5

    .line 861998
    const/4 v10, 0x1

    .line 861999
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 862000
    :cond_5
    const-string v44, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_6

    .line 862001
    const/4 v9, 0x1

    .line 862002
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 862003
    :cond_6
    const-string v44, "can_viewer_comment_with_video"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_7

    .line 862004
    const/4 v8, 0x1

    .line 862005
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 862006
    :cond_7
    const-string v44, "can_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_8

    .line 862007
    const/4 v7, 0x1

    .line 862008
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 862009
    :cond_8
    const-string v44, "can_viewer_react"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_9

    .line 862010
    const/4 v6, 0x1

    .line 862011
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 862012
    :cond_9
    const-string v44, "can_viewer_subscribe"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_a

    .line 862013
    const/4 v5, 0x1

    .line 862014
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 862015
    :cond_a
    const-string v44, "comments_mirroring_domain"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 862016
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 862017
    :cond_b
    const-string v44, "default_comment_ordering"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 862018
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 862019
    :cond_c
    const-string v44, "does_viewer_like"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_d

    .line 862020
    const/4 v4, 0x1

    .line 862021
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 862022
    :cond_d
    const-string v44, "id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_e

    .line 862023
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 862024
    :cond_e
    const-string v44, "important_reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_f

    .line 862025
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 862026
    :cond_f
    const-string v44, "is_viewer_subscribed"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_10

    .line 862027
    const/4 v3, 0x1

    .line 862028
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 862029
    :cond_10
    const-string v44, "legacy_api_post_id"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_11

    .line 862030
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 862031
    :cond_11
    const-string v44, "like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_12

    .line 862032
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 862033
    :cond_12
    const-string v44, "likers"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 862034
    invoke-static/range {p0 .. p1}, LX/5B6;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 862035
    :cond_13
    const-string v44, "reactors"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_14

    .line 862036
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 862037
    :cond_14
    const-string v44, "remixable_photo_uri"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_15

    .line 862038
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 862039
    :cond_15
    const-string v44, "seen_by"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_16

    .line 862040
    invoke-static/range {p0 .. p1}, LX/5B7;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 862041
    :cond_16
    const-string v44, "supported_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 862042
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 862043
    :cond_17
    const-string v44, "top_level_comments"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_18

    .line 862044
    invoke-static/range {p0 .. p1}, LX/59L;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 862045
    :cond_18
    const-string v44, "top_reactions"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 862046
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 862047
    :cond_19
    const-string v44, "viewer_acts_as_page"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1a

    .line 862048
    invoke-static/range {p0 .. p1}, LX/5B8;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 862049
    :cond_1a
    const-string v44, "viewer_acts_as_person"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1b

    .line 862050
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 862051
    :cond_1b
    const-string v44, "viewer_does_not_like_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1c

    .line 862052
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 862053
    :cond_1c
    const-string v44, "viewer_feedback_reaction_key"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1d

    .line 862054
    const/4 v2, 0x1

    .line 862055
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 862056
    :cond_1d
    const-string v44, "viewer_likes_sentence"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_0

    .line 862057
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 862058
    :cond_1e
    const/16 v43, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 862059
    if-eqz v13, :cond_1f

    .line 862060
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 862061
    :cond_1f
    if-eqz v12, :cond_20

    .line 862062
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 862063
    :cond_20
    if-eqz v11, :cond_21

    .line 862064
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 862065
    :cond_21
    if-eqz v10, :cond_22

    .line 862066
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 862067
    :cond_22
    if-eqz v9, :cond_23

    .line 862068
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 862069
    :cond_23
    if-eqz v8, :cond_24

    .line 862070
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 862071
    :cond_24
    if-eqz v7, :cond_25

    .line 862072
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 862073
    :cond_25
    if-eqz v6, :cond_26

    .line 862074
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 862075
    :cond_26
    if-eqz v5, :cond_27

    .line 862076
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 862077
    :cond_27
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 862078
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 862079
    if-eqz v4, :cond_28

    .line 862080
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 862081
    :cond_28
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 862082
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 862083
    if-eqz v3, :cond_29

    .line 862084
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 862085
    :cond_29
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862086
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862087
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862088
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862089
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862090
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862091
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862092
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862093
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862094
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862095
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862096
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 862097
    if-eqz v2, :cond_2a

    .line 862098
    const/16 v2, 0x1b

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 862099
    :cond_2a
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 862100
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
