.class public final LX/5lC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 997060
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 997061
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 997062
    if-eqz v0, :cond_0

    .line 997063
    const-string v1, "pitch"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 997064
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 997065
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 997066
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 997067
    const-string v2, "timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 997068
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 997069
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 997070
    if-eqz v0, :cond_2

    .line 997071
    const-string v1, "yaw"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 997072
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 997073
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 997074
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 997075
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 997076
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 997077
    :goto_0
    return v7

    .line 997078
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 997079
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 997080
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 997081
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 997082
    const-string v12, "pitch"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 997083
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v1

    goto :goto_1

    .line 997084
    :cond_1
    const-string v12, "timestamp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 997085
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 997086
    :cond_2
    const-string v12, "yaw"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 997087
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v1

    goto :goto_1

    .line 997088
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 997089
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 997090
    if-eqz v8, :cond_5

    .line 997091
    invoke-virtual {p1, v7, v10, v7}, LX/186;->a(III)V

    .line 997092
    :cond_5
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 997093
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 997094
    :cond_6
    if-eqz v6, :cond_7

    .line 997095
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9, v7}, LX/186;->a(III)V

    .line 997096
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_0

    :cond_8
    move v6, v7

    move v0, v7

    move v8, v7

    move v9, v7

    move-wide v2, v4

    move v10, v7

    goto :goto_1
.end method
