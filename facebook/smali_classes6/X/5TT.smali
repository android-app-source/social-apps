.class public LX/5TT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 921872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921873
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)LX/5TT;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 921874
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/5TT;->c:Landroid/net/Uri;

    .line 921875
    return-object p0

    .line 921876
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;
    .locals 1

    .prologue
    .line 921877
    new-instance v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;-><init>(LX/5TT;)V

    return-object v0
.end method
