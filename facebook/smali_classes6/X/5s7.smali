.class public final LX/5s7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/5r0;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1012431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/5r0;LX/5r0;)I
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 1012432
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1012433
    :cond_0
    :goto_0
    return v0

    .line 1012434
    :cond_1
    if-nez p0, :cond_2

    move v0, v1

    .line 1012435
    goto :goto_0

    .line 1012436
    :cond_2
    if-nez p1, :cond_3

    move v0, v2

    .line 1012437
    goto :goto_0

    .line 1012438
    :cond_3
    iget-wide v10, p0, LX/5r0;->d:J

    move-wide v4, v10

    .line 1012439
    iget-wide v10, p1, LX/5r0;->d:J

    move-wide v6, v10

    .line 1012440
    sub-long/2addr v4, v6

    .line 1012441
    cmp-long v3, v4, v8

    if-eqz v3, :cond_0

    .line 1012442
    cmp-long v0, v4, v8

    if-gez v0, :cond_4

    move v0, v1

    .line 1012443
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1012444
    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1012445
    check-cast p1, LX/5r0;

    check-cast p2, LX/5r0;

    invoke-static {p1, p2}, LX/5s7;->a(LX/5r0;LX/5r0;)I

    move-result v0

    return v0
.end method
