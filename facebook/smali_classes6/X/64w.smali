.class public LX/64w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/64i;

.field public final b:Ljava/net/Proxy;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64r;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/net/ProxySelector;

.field public final h:LX/64g;

.field public final i:LX/64U;

.field public final j:LX/65O;

.field public final k:Ljavax/net/SocketFactory;

.field public final l:Ljavax/net/ssl/SSLSocketFactory;

.field public final m:LX/66W;

.field public final n:Ljavax/net/ssl/HostnameVerifier;

.field public final o:LX/64a;

.field public final p:LX/64S;

.field public final q:LX/64S;

.field public final r:LX/64c;

.field public final s:LX/64j;

.field public final t:Z

.field public final u:Z

.field public final v:Z

.field public final w:I

.field public final x:I

.field public final y:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1046119
    new-array v0, v5, [LX/64x;

    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    aput-object v1, v0, v2

    sget-object v1, LX/64x;->SPDY_3:LX/64x;

    aput-object v1, v0, v3

    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;

    aput-object v1, v0, v4

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/64w;->z:Ljava/util/List;

    .line 1046120
    new-array v0, v5, [LX/64e;

    sget-object v1, LX/64e;->a:LX/64e;

    aput-object v1, v0, v2

    sget-object v1, LX/64e;->b:LX/64e;

    aput-object v1, v0, v3

    sget-object v1, LX/64e;->c:LX/64e;

    aput-object v1, v0, v4

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/64w;->A:Ljava/util/List;

    .line 1046121
    new-instance v0, LX/64u;

    invoke-direct {v0}, LX/64u;-><init>()V

    sput-object v0, LX/64t;->a:LX/64t;

    .line 1046122
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1046176
    new-instance v0, LX/64v;

    invoke-direct {v0}, LX/64v;-><init>()V

    invoke-direct {p0, v0}, LX/64w;-><init>(LX/64v;)V

    .line 1046177
    return-void
.end method

.method public constructor <init>(LX/64v;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1046137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046138
    iget-object v0, p1, LX/64v;->a:LX/64i;

    iput-object v0, p0, LX/64w;->a:LX/64i;

    .line 1046139
    iget-object v0, p1, LX/64v;->b:Ljava/net/Proxy;

    iput-object v0, p0, LX/64w;->b:Ljava/net/Proxy;

    .line 1046140
    iget-object v0, p1, LX/64v;->c:Ljava/util/List;

    iput-object v0, p0, LX/64w;->c:Ljava/util/List;

    .line 1046141
    iget-object v0, p1, LX/64v;->d:Ljava/util/List;

    iput-object v0, p0, LX/64w;->d:Ljava/util/List;

    .line 1046142
    iget-object v0, p1, LX/64v;->e:Ljava/util/List;

    invoke-static {v0}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64w;->e:Ljava/util/List;

    .line 1046143
    iget-object v0, p1, LX/64v;->f:Ljava/util/List;

    invoke-static {v0}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64w;->f:Ljava/util/List;

    .line 1046144
    iget-object v0, p1, LX/64v;->g:Ljava/net/ProxySelector;

    iput-object v0, p0, LX/64w;->g:Ljava/net/ProxySelector;

    .line 1046145
    iget-object v0, p1, LX/64v;->h:LX/64g;

    iput-object v0, p0, LX/64w;->h:LX/64g;

    .line 1046146
    iget-object v0, p1, LX/64v;->i:LX/64U;

    iput-object v0, p0, LX/64w;->i:LX/64U;

    .line 1046147
    iget-object v0, p1, LX/64v;->j:LX/65O;

    iput-object v0, p0, LX/64w;->j:LX/65O;

    .line 1046148
    iget-object v0, p1, LX/64v;->k:Ljavax/net/SocketFactory;

    iput-object v0, p0, LX/64w;->k:Ljavax/net/SocketFactory;

    .line 1046149
    iget-object v0, p0, LX/64w;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/64e;

    .line 1046150
    if-nez v1, :cond_0

    .line 1046151
    iget-boolean v1, v0, LX/64e;->e:Z

    move v0, v1

    .line 1046152
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 1046153
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1046154
    goto :goto_1

    .line 1046155
    :cond_2
    iget-object v0, p1, LX/64v;->l:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_3

    if-nez v1, :cond_5

    .line 1046156
    :cond_3
    iget-object v0, p1, LX/64v;->l:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, LX/64w;->l:Ljavax/net/ssl/SSLSocketFactory;

    .line 1046157
    iget-object v0, p1, LX/64v;->m:LX/66W;

    iput-object v0, p0, LX/64w;->m:LX/66W;

    .line 1046158
    :goto_2
    iget-object v0, p1, LX/64v;->n:Ljavax/net/ssl/HostnameVerifier;

    iput-object v0, p0, LX/64w;->n:Ljavax/net/ssl/HostnameVerifier;

    .line 1046159
    iget-object v0, p1, LX/64v;->o:LX/64a;

    iget-object v1, p0, LX/64w;->m:LX/66W;

    .line 1046160
    iget-object v2, v0, LX/64a;->c:LX/66W;

    if-eq v2, v1, :cond_4

    new-instance v2, LX/64a;

    iget-object v3, v0, LX/64a;->b:Ljava/util/List;

    invoke-direct {v2, v3, v1}, LX/64a;-><init>(Ljava/util/List;LX/66W;)V

    move-object v0, v2

    :cond_4
    move-object v0, v0

    .line 1046161
    iput-object v0, p0, LX/64w;->o:LX/64a;

    .line 1046162
    iget-object v0, p1, LX/64v;->p:LX/64S;

    iput-object v0, p0, LX/64w;->p:LX/64S;

    .line 1046163
    iget-object v0, p1, LX/64v;->q:LX/64S;

    iput-object v0, p0, LX/64w;->q:LX/64S;

    .line 1046164
    iget-object v0, p1, LX/64v;->r:LX/64c;

    iput-object v0, p0, LX/64w;->r:LX/64c;

    .line 1046165
    iget-object v0, p1, LX/64v;->s:LX/64j;

    iput-object v0, p0, LX/64w;->s:LX/64j;

    .line 1046166
    iget-boolean v0, p1, LX/64v;->t:Z

    iput-boolean v0, p0, LX/64w;->t:Z

    .line 1046167
    iget-boolean v0, p1, LX/64v;->u:Z

    iput-boolean v0, p0, LX/64w;->u:Z

    .line 1046168
    iget-boolean v0, p1, LX/64v;->v:Z

    iput-boolean v0, p0, LX/64w;->v:Z

    .line 1046169
    iget v0, p1, LX/64v;->w:I

    iput v0, p0, LX/64w;->w:I

    .line 1046170
    iget v0, p1, LX/64v;->x:I

    iput v0, p0, LX/64w;->x:I

    .line 1046171
    iget v0, p1, LX/64v;->y:I

    iput v0, p0, LX/64w;->y:I

    .line 1046172
    return-void

    .line 1046173
    :cond_5
    invoke-static {}, LX/64w;->z()Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    .line 1046174
    invoke-static {v0}, LX/64w;->a(Ljavax/net/ssl/X509TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    iput-object v1, p0, LX/64w;->l:Ljavax/net/ssl/SSLSocketFactory;

    .line 1046175
    invoke-static {v0}, LX/66W;->a(Ljavax/net/ssl/X509TrustManager;)LX/66W;

    move-result-object v0

    iput-object v0, p0, LX/64w;->m:LX/66W;

    goto :goto_2
.end method

.method private static a(Ljavax/net/ssl/X509TrustManager;)Ljavax/net/ssl/SSLSocketFactory;
    .locals 4

    .prologue
    .line 1046133
    :try_start_0
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 1046134
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljavax/net/ssl/TrustManager;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 1046135
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1046136
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static z()Ljavax/net/ssl/X509TrustManager;
    .locals 4

    .prologue
    .line 1046124
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 1046125
    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 1046126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 1046127
    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    .line 1046128
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    instance-of v1, v1, Ljavax/net/ssl/X509TrustManager;

    if-nez v1, :cond_1

    .line 1046129
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected default trust managers:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1046130
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1046131
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1046132
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    aget-object v0, v0, v1

    check-cast v0, Ljavax/net/ssl/X509TrustManager;
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0
.end method


# virtual methods
.method public final a(LX/650;)LX/64y;
    .locals 1

    .prologue
    .line 1046123
    new-instance v0, LX/64y;

    invoke-direct {v0, p0, p1}, LX/64y;-><init>(LX/64w;LX/650;)V

    return-object v0
.end method
