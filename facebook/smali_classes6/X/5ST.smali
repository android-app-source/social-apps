.class public final LX/5ST;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/java2js/Invokable;


# instance fields
.field private final mJSContext:Lcom/facebook/java2js/JSContext;

.field private final mObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 918039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 918040
    iput-object p1, p0, LX/5ST;->mJSContext:Lcom/facebook/java2js/JSContext;

    .line 918041
    iput-object p2, p0, LX/5ST;->mObject:Ljava/lang/Object;

    .line 918042
    return-void
.end method


# virtual methods
.method public invoke([Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
    .locals 2

    .prologue
    .line 918043
    iget-object v0, p0, LX/5ST;->mJSContext:Lcom/facebook/java2js/JSContext;

    iget-object v1, p0, LX/5ST;->mObject:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method
