.class public final LX/5bV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 958436
    const/4 v15, 0x0

    .line 958437
    const/4 v14, 0x0

    .line 958438
    const/4 v13, 0x0

    .line 958439
    const/4 v12, 0x0

    .line 958440
    const/4 v11, 0x0

    .line 958441
    const/4 v10, 0x0

    .line 958442
    const/4 v9, 0x0

    .line 958443
    const/4 v8, 0x0

    .line 958444
    const/4 v7, 0x0

    .line 958445
    const/4 v6, 0x0

    .line 958446
    const/4 v5, 0x0

    .line 958447
    const/4 v4, 0x0

    .line 958448
    const/4 v3, 0x0

    .line 958449
    const/4 v2, 0x0

    .line 958450
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 958451
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 958452
    const/4 v2, 0x0

    .line 958453
    :goto_0
    return v2

    .line 958454
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 958455
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 958456
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 958457
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 958458
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 958459
    const-string v17, "action_links"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 958460
    invoke-static/range {p0 .. p1}, LX/5bP;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 958461
    :cond_2
    const-string v17, "attachment_properties"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 958462
    invoke-static/range {p0 .. p1}, LX/5bR;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 958463
    :cond_3
    const-string v17, "deduplication_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 958464
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 958465
    :cond_4
    const-string v17, "description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 958466
    invoke-static/range {p0 .. p1}, LX/5a7;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 958467
    :cond_5
    const-string v17, "media"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 958468
    invoke-static/range {p0 .. p1}, LX/5bO;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 958469
    :cond_6
    const-string v17, "snippet"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 958470
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 958471
    :cond_7
    const-string v17, "source"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 958472
    invoke-static/range {p0 .. p1}, LX/5a8;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 958473
    :cond_8
    const-string v17, "style_list"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 958474
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 958475
    :cond_9
    const-string v17, "subattachments"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 958476
    invoke-static/range {p0 .. p1}, LX/5bU;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 958477
    :cond_a
    const-string v17, "subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 958478
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 958479
    :cond_b
    const-string v17, "target"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 958480
    invoke-static/range {p0 .. p1}, LX/5Yy;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 958481
    :cond_c
    const-string v17, "title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 958482
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 958483
    :cond_d
    const-string v17, "tracking"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 958484
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 958485
    :cond_e
    const-string v17, "url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 958486
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 958487
    :cond_f
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 958488
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 958489
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 958490
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 958491
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 958492
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 958493
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 958494
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 958495
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 958496
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 958497
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 958498
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 958499
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 958500
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 958501
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 958502
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x7

    .line 958503
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 958504
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958505
    if-eqz v0, :cond_1

    .line 958506
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958507
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 958508
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 958509
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/5bP;->a(LX/15i;ILX/0nX;)V

    .line 958510
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 958511
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 958512
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958513
    if-eqz v0, :cond_3

    .line 958514
    const-string v1, "attachment_properties"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958515
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 958516
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 958517
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/5bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 958518
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 958519
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 958520
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958521
    if-eqz v0, :cond_4

    .line 958522
    const-string v1, "deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958523
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958524
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958525
    if-eqz v0, :cond_5

    .line 958526
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958527
    invoke-static {p0, v0, p2}, LX/5a7;->a(LX/15i;ILX/0nX;)V

    .line 958528
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958529
    if-eqz v0, :cond_6

    .line 958530
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958531
    invoke-static {p0, v0, p2, p3}, LX/5bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 958532
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958533
    if-eqz v0, :cond_7

    .line 958534
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958535
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958536
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958537
    if-eqz v0, :cond_8

    .line 958538
    const-string v1, "source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958539
    invoke-static {p0, v0, p2}, LX/5a8;->a(LX/15i;ILX/0nX;)V

    .line 958540
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 958541
    if-eqz v0, :cond_9

    .line 958542
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958543
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 958544
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958545
    if-eqz v0, :cond_b

    .line 958546
    const-string v1, "subattachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958547
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 958548
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_a

    .line 958549
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5bU;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 958550
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 958551
    :cond_a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 958552
    :cond_b
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958553
    if-eqz v0, :cond_c

    .line 958554
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958555
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958556
    :cond_c
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958557
    if-eqz v0, :cond_d

    .line 958558
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958559
    invoke-static {p0, v0, p2, p3}, LX/5Yy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 958560
    :cond_d
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958561
    if-eqz v0, :cond_e

    .line 958562
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958563
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958564
    :cond_e
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958565
    if-eqz v0, :cond_f

    .line 958566
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958568
    :cond_f
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958569
    if-eqz v0, :cond_10

    .line 958570
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958572
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 958573
    return-void
.end method
