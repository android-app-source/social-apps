.class public final LX/6Hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/6IA;


# direct methods
.method public constructor <init>(LX/6IA;)V
    .locals 0

    .prologue
    .line 1072902
    iput-object p1, p0, LX/6Hx;->a:LX/6IA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1072903
    iget-object v0, p0, LX/6Hx;->a:LX/6IA;

    iget v0, v0, LX/6IA;->X:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1072904
    :cond_0
    :goto_0
    return v2

    .line 1072905
    :cond_1
    iget-object v0, p0, LX/6Hx;->a:LX/6IA;

    iget-object v0, v0, LX/6IA;->ar:LX/6HU;

    .line 1072906
    iget-boolean v1, v0, LX/6HU;->s:Z

    move v0, v1

    .line 1072907
    if-eqz v0, :cond_0

    .line 1072908
    invoke-static {}, LX/2Ib;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1072909
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1072910
    iget-object v0, p0, LX/6Hx;->a:LX/6IA;

    iget-object v0, v0, LX/6IA;->ar:LX/6HU;

    .line 1072911
    iget-object v3, v0, LX/6HU;->n:LX/6Ha;

    if-eqz v3, :cond_2

    .line 1072912
    iget-object v3, v0, LX/6HU;->n:LX/6Ha;

    .line 1072913
    iget-boolean v4, v3, LX/6Ha;->d:Z

    if-nez v4, :cond_3

    .line 1072914
    :cond_2
    :goto_1
    goto :goto_0

    .line 1072915
    :cond_3
    iget-boolean v4, v3, LX/6Ha;->g:Z

    if-nez v4, :cond_4

    .line 1072916
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/6Ha;->g:Z

    .line 1072917
    :cond_4
    invoke-static {v3}, LX/6Ha;->n(LX/6Ha;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1072918
    iget v4, v3, LX/6Ha;->c:I

    if-nez v4, :cond_2

    iget-wide v4, v3, LX/6Ha;->z:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 1072919
    sget-object v4, LX/6HX;->LAST_SECOND_AUTOFOCUS:LX/6HX;

    const/16 v5, 0x7d0

    invoke-static {v3, v4, v5}, LX/6Ha;->a(LX/6Ha;LX/6HX;I)V

    goto :goto_1
.end method
