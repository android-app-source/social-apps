.class public final LX/679;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public final synthetic a:LX/65f;

.field public final synthetic b:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(LX/65f;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 1052323
    iput-object p1, p0, LX/679;->a:LX/65f;

    iput-object p2, p0, LX/679;->b:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 1052324
    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052325
    :cond_0
    cmp-long v2, p2, v0

    if-nez v2, :cond_1

    .line 1052326
    :goto_0
    return-wide v0

    .line 1052327
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/679;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->g()V

    .line 1052328
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/672;->e(I)LX/67F;

    move-result-object v0

    .line 1052329
    iget v1, v0, LX/67F;->c:I

    rsub-int v1, v1, 0x2000

    int-to-long v2, v1

    invoke-static {p2, p3, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 1052330
    iget-object v2, p0, LX/679;->b:Ljava/io/InputStream;

    iget-object v3, v0, LX/67F;->a:[B

    iget v4, v0, LX/67F;->c:I

    invoke-virtual {v2, v3, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 1052331
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1052332
    :cond_2
    iget v2, v0, LX/67F;->c:I

    add-int/2addr v2, v1

    iput v2, v0, LX/67F;->c:I

    .line 1052333
    iget-wide v2, p1, LX/672;->b:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p1, LX/672;->b:J
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052334
    int-to-long v0, v1

    goto :goto_0

    .line 1052335
    :catch_0
    move-exception v0

    .line 1052336
    invoke-static {v0}, LX/67B;->a(Ljava/lang/AssertionError;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1052337
    :cond_3
    throw v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052338
    iget-object v0, p0, LX/679;->a:LX/65f;

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052339
    iget-object v0, p0, LX/679;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1052340
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052341
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "source("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/679;->b:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
