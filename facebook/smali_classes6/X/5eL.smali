.class public final LX/5eL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 967645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 967596
    if-nez p1, :cond_0

    .line 967597
    :goto_0
    return v0

    .line 967598
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    move-result-object v1

    const/4 v2, 0x0

    .line 967599
    if-nez v1, :cond_1

    .line 967600
    :goto_1
    move v1, v2

    .line 967601
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 967602
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 967603
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 967604
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 967605
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v3

    const/4 v4, 0x0

    .line 967606
    if-nez v3, :cond_3

    .line 967607
    :goto_2
    move v3, v4

    .line 967608
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;

    move-result-object v4

    const/4 v5, 0x0

    .line 967609
    if-nez v4, :cond_4

    .line 967610
    :cond_2
    :goto_3
    move v4, v5

    .line 967611
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 967612
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v6

    invoke-static {p0, v6}, LX/5eL;->a(LX/186;Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;)I

    move-result v6

    .line 967613
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 967614
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 967615
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->p()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 967616
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 967617
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;->r()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 967618
    const/16 p1, 0x9

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 967619
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 967620
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 967621
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 967622
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 967623
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 967624
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 967625
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 967626
    const/4 v2, 0x7

    invoke-virtual {p0, v2, v10}, LX/186;->b(II)V

    .line 967627
    const/16 v2, 0x8

    invoke-virtual {p0, v2, v11}, LX/186;->b(II)V

    .line 967628
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 967629
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 967630
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 967631
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 967632
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 967633
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 967634
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 967635
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 967636
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, -0x414badb9

    if-ne v6, v7, :cond_2

    .line 967637
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 967638
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 967639
    const/4 v8, 0x3

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 967640
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 967641
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 967642
    const/4 v5, 0x2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterCustomValue;->k()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 967643
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 967644
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;)I
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 967565
    if-nez p1, :cond_0

    .line 967566
    :goto_0
    return v2

    .line 967567
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;->a()LX/0Px;

    move-result-object v3

    .line 967568
    if-eqz v3, :cond_2

    .line 967569
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 967570
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 967571
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;

    const/4 v6, 0x0

    .line 967572
    if-nez v0, :cond_3

    .line 967573
    :goto_2
    move v0, v6

    .line 967574
    aput v0, v4, v1

    .line 967575
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 967576
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 967577
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 967578
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 967579
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 967580
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 967581
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesEdge;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    move-result-object v7

    const/4 v8, 0x0

    .line 967582
    if-nez v7, :cond_4

    .line 967583
    :goto_4
    move v7, v8

    .line 967584
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 967585
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 967586
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 967587
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 967588
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 967589
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 967590
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 967591
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;->a()Z

    move-result v0

    invoke-virtual {p0, v8, v0}, LX/186;->a(IZ)V

    .line 967592
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 967593
    const/4 v8, 0x2

    invoke-virtual {p0, v8, p1}, LX/186;->b(II)V

    .line 967594
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 967595
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 967554
    if-nez p0, :cond_0

    .line 967555
    const/4 v0, 0x0

    .line 967556
    :goto_0
    return-object v0

    .line 967557
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 967558
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 967559
    iput v1, v0, LX/2dc;->c:I

    .line 967560
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 967561
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 967562
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 967563
    iput v1, v0, LX/2dc;->i:I

    .line 967564
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;
    .locals 15

    .prologue
    .line 967180
    if-nez p0, :cond_0

    .line 967181
    const/4 v0, 0x0

    .line 967182
    :goto_0
    return-object v0

    .line 967183
    :cond_0
    new-instance v0, LX/4XN;

    invoke-direct {v0}, LX/4XN;-><init>()V

    .line 967184
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 967185
    iput-object v1, v0, LX/4XN;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967186
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 967187
    iput-object v1, v0, LX/4XN;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967188
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->c()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;

    move-result-object v1

    .line 967189
    if-nez v1, :cond_1

    .line 967190
    const/4 v2, 0x0

    .line 967191
    :goto_1
    move-object v1, v2

    .line 967192
    iput-object v1, v0, LX/4XN;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 967193
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 967194
    iput-object v1, v0, LX/4XN;->f:Ljava/lang/String;

    .line 967195
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->e()LX/3Ab;

    move-result-object v1

    .line 967196
    if-nez v1, :cond_2

    .line 967197
    const/4 v2, 0x0

    .line 967198
    :goto_2
    move-object v1, v2

    .line 967199
    iput-object v1, v0, LX/4XN;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 967200
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->bq_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;

    move-result-object v1

    .line 967201
    if-nez v1, :cond_b

    .line 967202
    const/4 v2, 0x0

    .line 967203
    :goto_3
    move-object v1, v2

    .line 967204
    iput-object v1, v0, LX/4XN;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 967205
    invoke-virtual {v0}, LX/4XN;->a()Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v0

    goto :goto_0

    .line 967206
    :cond_1
    new-instance v2, LX/4XR;

    invoke-direct {v2}, LX/4XR;-><init>()V

    .line 967207
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 967208
    iput-object v3, v2, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967209
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 967210
    iput-object v3, v2, LX/4XR;->fO:Ljava/lang/String;

    .line 967211
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    goto :goto_1

    .line 967212
    :cond_2
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 967213
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 967214
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 967215
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 967216
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Ph;

    .line 967217
    if-nez v2, :cond_5

    .line 967218
    const/4 v6, 0x0

    .line 967219
    :goto_5
    move-object v2, v6

    .line 967220
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967221
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 967222
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 967223
    iput-object v2, v4, LX/173;->e:LX/0Px;

    .line 967224
    :cond_4
    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    .line 967225
    iput-object v2, v4, LX/173;->f:Ljava/lang/String;

    .line 967226
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_2

    .line 967227
    :cond_5
    new-instance v6, LX/4W6;

    invoke-direct {v6}, LX/4W6;-><init>()V

    .line 967228
    invoke-interface {v2}, LX/5Ph;->a()LX/1yA;

    move-result-object v7

    .line 967229
    if-nez v7, :cond_6

    .line 967230
    const/4 v8, 0x0

    .line 967231
    :goto_6
    move-object v7, v8

    .line 967232
    iput-object v7, v6, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 967233
    invoke-interface {v2}, LX/5Ph;->b()I

    move-result v7

    .line 967234
    iput v7, v6, LX/4W6;->c:I

    .line 967235
    invoke-interface {v2}, LX/5Ph;->c()I

    move-result v7

    .line 967236
    iput v7, v6, LX/4W6;->d:I

    .line 967237
    invoke-virtual {v6}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v6

    goto :goto_5

    .line 967238
    :cond_6
    new-instance v10, LX/170;

    invoke-direct {v10}, LX/170;-><init>()V

    .line 967239
    invoke-interface {v7}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 967240
    iput-object v8, v10, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967241
    invoke-interface {v7}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v8

    .line 967242
    iput-object v8, v10, LX/170;->o:Ljava/lang/String;

    .line 967243
    invoke-interface {v7}, LX/1yA;->v_()Ljava/lang/String;

    move-result-object v8

    .line 967244
    iput-object v8, v10, LX/170;->A:Ljava/lang/String;

    .line 967245
    invoke-interface {v7}, LX/1yA;->n()LX/1yP;

    move-result-object v8

    .line 967246
    if-nez v8, :cond_9

    .line 967247
    const/4 v9, 0x0

    .line 967248
    :goto_7
    move-object v8, v9

    .line 967249
    iput-object v8, v10, LX/170;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 967250
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 967251
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 967252
    const/4 v8, 0x0

    move v9, v8

    :goto_8
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_7

    .line 967253
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    .line 967254
    if-nez v8, :cond_a

    .line 967255
    const/4 v12, 0x0

    .line 967256
    :goto_9
    move-object v8, v12

    .line 967257
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967258
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 967259
    :cond_7
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 967260
    iput-object v8, v10, LX/170;->N:LX/0Px;

    .line 967261
    :cond_8
    invoke-interface {v7}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v8

    .line 967262
    iput-object v8, v10, LX/170;->X:Ljava/lang/String;

    .line 967263
    invoke-interface {v7}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v8

    .line 967264
    iput-object v8, v10, LX/170;->Y:Ljava/lang/String;

    .line 967265
    invoke-virtual {v10}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    goto :goto_6

    .line 967266
    :cond_9
    new-instance v9, LX/4XY;

    invoke-direct {v9}, LX/4XY;-><init>()V

    .line 967267
    invoke-interface {v8}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v11

    .line 967268
    iput-object v11, v9, LX/4XY;->ag:Ljava/lang/String;

    .line 967269
    invoke-virtual {v9}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    goto :goto_7

    .line 967270
    :cond_a
    new-instance v12, LX/4YY;

    invoke-direct {v12}, LX/4YY;-><init>()V

    .line 967271
    invoke-virtual {v8}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v13

    .line 967272
    iput-object v13, v12, LX/4YY;->d:Ljava/lang/String;

    .line 967273
    invoke-virtual {v12}, LX/4YY;->a()Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    move-result-object v12

    goto :goto_9

    .line 967274
    :cond_b
    new-instance v4, LX/2oI;

    invoke-direct {v4}, LX/2oI;-><init>()V

    .line 967275
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->k()I

    move-result v2

    .line 967276
    iput v2, v4, LX/2oI;->e:I

    .line 967277
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->l()J

    move-result-wide v2

    .line 967278
    iput-wide v2, v4, LX/2oI;->i:J

    .line 967279
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->m()I

    move-result v2

    .line 967280
    iput v2, v4, LX/2oI;->j:I

    .line 967281
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->n()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 967282
    iput-object v2, v4, LX/2oI;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 967283
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->o()Z

    move-result v2

    .line 967284
    iput-boolean v2, v4, LX/2oI;->m:Z

    .line 967285
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->p()Z

    move-result v2

    .line 967286
    iput-boolean v2, v4, LX/2oI;->n:Z

    .line 967287
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->q()Z

    move-result v2

    .line 967288
    iput-boolean v2, v4, LX/2oI;->o:Z

    .line 967289
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->r()Ljava/lang/String;

    move-result-object v2

    .line 967290
    iput-object v2, v4, LX/2oI;->p:Ljava/lang/String;

    .line 967291
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->s()J

    move-result-wide v2

    .line 967292
    iput-wide v2, v4, LX/2oI;->t:J

    .line 967293
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->t()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    move-result-object v2

    const/4 v8, 0x0

    .line 967294
    if-nez v2, :cond_e

    .line 967295
    const/4 v6, 0x0

    .line 967296
    :goto_a
    move-object v2, v6

    .line 967297
    iput-object v2, v4, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 967298
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->u()Z

    move-result v2

    .line 967299
    iput-boolean v2, v4, LX/2oI;->x:Z

    .line 967300
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->v()D

    move-result-wide v2

    .line 967301
    iput-wide v2, v4, LX/2oI;->B:D

    .line 967302
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->w()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    .line 967303
    if-nez v2, :cond_17

    .line 967304
    const/4 v6, 0x0

    .line 967305
    :goto_b
    move-object v2, v6

    .line 967306
    iput-object v2, v4, LX/2oI;->D:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 967307
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->x()Z

    move-result v2

    .line 967308
    iput-boolean v2, v4, LX/2oI;->E:Z

    .line 967309
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->y()Z

    move-result v2

    .line 967310
    iput-boolean v2, v4, LX/2oI;->F:Z

    .line 967311
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->z()I

    move-result v2

    .line 967312
    iput v2, v4, LX/2oI;->G:I

    .line 967313
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->A()I

    move-result v2

    .line 967314
    iput v2, v4, LX/2oI;->H:I

    .line 967315
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->B()I

    move-result v2

    .line 967316
    iput v2, v4, LX/2oI;->M:I

    .line 967317
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 967318
    iput-object v2, v4, LX/2oI;->N:Ljava/lang/String;

    .line 967319
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->e()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 967320
    iput-object v2, v4, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967321
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aj_()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 967322
    iput-object v2, v4, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967323
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ai_()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 967324
    iput-object v2, v4, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967325
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->j()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 967326
    iput-object v2, v4, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967327
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->C()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 967328
    iput-object v2, v4, LX/2oI;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 967329
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->D()I

    move-result v2

    .line 967330
    iput v2, v4, LX/2oI;->aa:I

    .line 967331
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->E()I

    move-result v2

    .line 967332
    iput v2, v4, LX/2oI;->ab:I

    .line 967333
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->F()I

    move-result v2

    .line 967334
    iput v2, v4, LX/2oI;->ac:I

    .line 967335
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 967336
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 967337
    const/4 v2, 0x0

    move v3, v2

    :goto_c
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    .line 967338
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    .line 967339
    if-nez v2, :cond_1b

    .line 967340
    const/4 v6, 0x0

    .line 967341
    :goto_d
    move-object v2, v6

    .line 967342
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967343
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_c

    .line 967344
    :cond_c
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 967345
    iput-object v2, v4, LX/2oI;->ae:LX/0Px;

    .line 967346
    :cond_d
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->H()Z

    move-result v2

    .line 967347
    iput-boolean v2, v4, LX/2oI;->af:Z

    .line 967348
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->I()Z

    move-result v2

    .line 967349
    iput-boolean v2, v4, LX/2oI;->ag:Z

    .line 967350
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->J()Z

    move-result v2

    .line 967351
    iput-boolean v2, v4, LX/2oI;->ah:Z

    .line 967352
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->K()Z

    move-result v2

    .line 967353
    iput-boolean v2, v4, LX/2oI;->aj:Z

    .line 967354
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->L()Z

    move-result v2

    .line 967355
    iput-boolean v2, v4, LX/2oI;->ak:Z

    .line 967356
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->M()Z

    move-result v2

    .line 967357
    iput-boolean v2, v4, LX/2oI;->al:Z

    .line 967358
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->N()Z

    move-result v2

    .line 967359
    iput-boolean v2, v4, LX/2oI;->am:Z

    .line 967360
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->O()Z

    move-result v2

    .line 967361
    iput-boolean v2, v4, LX/2oI;->an:Z

    .line 967362
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->P()Z

    move-result v2

    .line 967363
    iput-boolean v2, v4, LX/2oI;->ap:Z

    .line 967364
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Q()Z

    move-result v2

    .line 967365
    iput-boolean v2, v4, LX/2oI;->aq:Z

    .line 967366
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->R()Z

    move-result v2

    .line 967367
    iput-boolean v2, v4, LX/2oI;->ar:Z

    .line 967368
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->S()I

    move-result v2

    .line 967369
    iput v2, v4, LX/2oI;->ay:I

    .line 967370
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->T()I

    move-result v2

    .line 967371
    iput v2, v4, LX/2oI;->az:I

    .line 967372
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->U()D

    move-result-wide v2

    .line 967373
    iput-wide v2, v4, LX/2oI;->aL:D

    .line 967374
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->V()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;

    move-result-object v2

    .line 967375
    if-nez v2, :cond_1c

    .line 967376
    const/4 v3, 0x0

    .line 967377
    :goto_e
    move-object v2, v3

    .line 967378
    iput-object v2, v4, LX/2oI;->aN:Lcom/facebook/graphql/model/GraphQLActor;

    .line 967379
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->W()I

    move-result v2

    .line 967380
    iput v2, v4, LX/2oI;->aP:I

    .line 967381
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->X()Ljava/lang/String;

    move-result-object v2

    .line 967382
    iput-object v2, v4, LX/2oI;->aQ:Ljava/lang/String;

    .line 967383
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Y()I

    move-result v2

    .line 967384
    iput v2, v4, LX/2oI;->aS:I

    .line 967385
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Z()Ljava/lang/String;

    move-result-object v2

    .line 967386
    iput-object v2, v4, LX/2oI;->aT:Ljava/lang/String;

    .line 967387
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aa()Ljava/lang/String;

    move-result-object v2

    .line 967388
    iput-object v2, v4, LX/2oI;->bl:Ljava/lang/String;

    .line 967389
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ab()LX/174;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 967390
    iput-object v2, v4, LX/2oI;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 967391
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ac()Z

    move-result v2

    .line 967392
    iput-boolean v2, v4, LX/2oI;->bv:Z

    .line 967393
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ad()D

    move-result-wide v2

    .line 967394
    iput-wide v2, v4, LX/2oI;->bx:D

    .line 967395
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ae()D

    move-result-wide v2

    .line 967396
    iput-wide v2, v4, LX/2oI;->by:D

    .line 967397
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->af()Ljava/lang/String;

    move-result-object v2

    .line 967398
    iput-object v2, v4, LX/2oI;->bz:Ljava/lang/String;

    .line 967399
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ag()Ljava/lang/String;

    move-result-object v2

    .line 967400
    iput-object v2, v4, LX/2oI;->bA:Ljava/lang/String;

    .line 967401
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ah()I

    move-result v2

    .line 967402
    iput v2, v4, LX/2oI;->bC:I

    .line 967403
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ai()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    .line 967404
    iput-object v2, v4, LX/2oI;->bG:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 967405
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aj()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/5eL;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    .line 967406
    iput-object v2, v4, LX/2oI;->bH:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 967407
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ak()Z

    move-result v2

    .line 967408
    iput-boolean v2, v4, LX/2oI;->bI:Z

    .line 967409
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->al()LX/0Px;

    move-result-object v2

    .line 967410
    iput-object v2, v4, LX/2oI;->bP:LX/0Px;

    .line 967411
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->am()I

    move-result v2

    .line 967412
    iput v2, v4, LX/2oI;->cd:I

    .line 967413
    invoke-virtual {v4}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    goto/16 :goto_3

    .line 967414
    :cond_e
    new-instance v9, LX/23u;

    invoke-direct {v9}, LX/23u;-><init>()V

    .line 967415
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 967416
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    move v7, v8

    .line 967417
    :goto_f
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_f

    .line 967418
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;

    .line 967419
    if-nez v6, :cond_13

    .line 967420
    const/4 v11, 0x0

    .line 967421
    :goto_10
    move-object v6, v11

    .line 967422
    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967423
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_f

    .line 967424
    :cond_f
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 967425
    iput-object v6, v9, LX/23u;->d:LX/0Px;

    .line 967426
    :cond_10
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_12

    .line 967427
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 967428
    :goto_11
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_11

    .line 967429
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;

    .line 967430
    if-nez v6, :cond_14

    .line 967431
    const/4 v10, 0x0

    .line 967432
    :goto_12
    move-object v6, v10

    .line 967433
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967434
    add-int/lit8 v8, v8, 0x1

    goto :goto_11

    .line 967435
    :cond_11
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 967436
    iput-object v6, v9, LX/23u;->k:LX/0Px;

    .line 967437
    :cond_12
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->d()Ljava/lang/String;

    move-result-object v6

    .line 967438
    iput-object v6, v9, LX/23u;->m:Ljava/lang/String;

    .line 967439
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e()J

    move-result-wide v6

    .line 967440
    iput-wide v6, v9, LX/23u;->v:J

    .line 967441
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bm_()Ljava/lang/String;

    move-result-object v6

    .line 967442
    iput-object v6, v9, LX/23u;->N:Ljava/lang/String;

    .line 967443
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bn_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v6

    .line 967444
    if-nez v6, :cond_16

    .line 967445
    const/4 v7, 0x0

    .line 967446
    :goto_13
    move-object v6, v7

    .line 967447
    iput-object v6, v9, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 967448
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j()Ljava/lang/String;

    move-result-object v6

    .line 967449
    iput-object v6, v9, LX/23u;->aM:Ljava/lang/String;

    .line 967450
    invoke-virtual {v9}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    goto/16 :goto_a

    .line 967451
    :cond_13
    new-instance v11, LX/3dL;

    invoke-direct {v11}, LX/3dL;-><init>()V

    .line 967452
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    .line 967453
    iput-object v12, v11, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967454
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->b()Z

    move-result v12

    .line 967455
    iput-boolean v12, v11, LX/3dL;->R:Z

    .line 967456
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->c()Ljava/lang/String;

    move-result-object v12

    .line 967457
    iput-object v12, v11, LX/3dL;->ag:Ljava/lang/String;

    .line 967458
    invoke-virtual {v11}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v11

    goto :goto_10

    .line 967459
    :cond_14
    new-instance v10, LX/39x;

    invoke-direct {v10}, LX/39x;-><init>()V

    .line 967460
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;->a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;

    move-result-object v11

    .line 967461
    if-nez v11, :cond_15

    .line 967462
    const/4 v12, 0x0

    .line 967463
    :goto_14
    move-object v11, v12

    .line 967464
    iput-object v11, v10, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 967465
    invoke-virtual {v10}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v10

    goto :goto_12

    .line 967466
    :cond_15
    new-instance v12, LX/4XB;

    invoke-direct {v12}, LX/4XB;-><init>()V

    .line 967467
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 967468
    iput-object v6, v12, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967469
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 967470
    iput-object v6, v12, LX/4XB;->T:Ljava/lang/String;

    .line 967471
    invoke-virtual {v12}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v12

    goto :goto_14

    .line 967472
    :cond_16
    new-instance v7, LX/170;

    invoke-direct {v7}, LX/170;-><init>()V

    .line 967473
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 967474
    iput-object v8, v7, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967475
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 967476
    iput-object v8, v7, LX/170;->o:Ljava/lang/String;

    .line 967477
    invoke-virtual {v7}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    goto :goto_13

    .line 967478
    :cond_17
    new-instance v8, LX/4ZS;

    invoke-direct {v8}, LX/4ZS;-><init>()V

    .line 967479
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_19

    .line 967480
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 967481
    const/4 v6, 0x0

    move v7, v6

    :goto_15
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_18

    .line 967482
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;

    .line 967483
    if-nez v6, :cond_1a

    .line 967484
    const/4 v10, 0x0

    .line 967485
    :goto_16
    move-object v6, v10

    .line 967486
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967487
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_15

    .line 967488
    :cond_18
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 967489
    iput-object v6, v8, LX/4ZS;->b:LX/0Px;

    .line 967490
    :cond_19
    invoke-virtual {v8}, LX/4ZS;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v6

    goto/16 :goto_b

    .line 967491
    :cond_1a
    new-instance v10, LX/4ZT;

    invoke-direct {v10}, LX/4ZT;-><init>()V

    .line 967492
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->a()I

    move-result v11

    .line 967493
    iput v11, v10, LX/4ZT;->b:I

    .line 967494
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->b()J

    move-result-wide v12

    .line 967495
    iput-wide v12, v10, LX/4ZT;->c:J

    .line 967496
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->c()I

    move-result v11

    .line 967497
    iput v11, v10, LX/4ZT;->d:I

    .line 967498
    invoke-virtual {v10}, LX/4ZT;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    move-result-object v10

    goto :goto_16

    .line 967499
    :cond_1b
    new-instance v6, LX/4X1;

    invoke-direct {v6}, LX/4X1;-><init>()V

    .line 967500
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->a()I

    move-result v7

    .line 967501
    iput v7, v6, LX/4X1;->b:I

    .line 967502
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->b()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v7

    .line 967503
    iput-object v7, v6, LX/4X1;->c:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 967504
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->c()I

    move-result v7

    .line 967505
    iput v7, v6, LX/4X1;->d:I

    .line 967506
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->d()I

    move-result v7

    .line 967507
    iput v7, v6, LX/4X1;->e:I

    .line 967508
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e()I

    move-result v7

    .line 967509
    iput v7, v6, LX/4X1;->f:I

    .line 967510
    invoke-virtual {v6}, LX/4X1;->a()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    move-result-object v6

    goto/16 :goto_d

    .line 967511
    :cond_1c
    new-instance v3, LX/3dL;

    invoke-direct {v3}, LX/3dL;-><init>()V

    .line 967512
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 967513
    iput-object v5, v3, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967514
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 967515
    iput-object v5, v3, LX/3dL;->E:Ljava/lang/String;

    .line 967516
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->d()Z

    move-result v5

    .line 967517
    iput-boolean v5, v3, LX/3dL;->R:Z

    .line 967518
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 967519
    iput-object v5, v3, LX/3dL;->ag:Ljava/lang/String;

    .line 967520
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->bp_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;

    move-result-object v5

    .line 967521
    if-nez v5, :cond_1d

    .line 967522
    const/4 v6, 0x0

    .line 967523
    :goto_17
    move-object v5, v6

    .line 967524
    iput-object v5, v3, LX/3dL;->aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 967525
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->bo_()LX/174;

    move-result-object v5

    invoke-static {v5}, LX/5eL;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 967526
    iput-object v5, v3, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 967527
    invoke-virtual {v3}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    goto/16 :goto_e

    .line 967528
    :cond_1d
    new-instance v8, LX/4Yk;

    invoke-direct {v8}, LX/4Yk;-><init>()V

    .line 967529
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1f

    .line 967530
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 967531
    const/4 v6, 0x0

    move v7, v6

    :goto_18
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_1e

    .line 967532
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel;

    .line 967533
    if-nez v6, :cond_20

    .line 967534
    const/4 v10, 0x0

    .line 967535
    :goto_19
    move-object v6, v10

    .line 967536
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967537
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_18

    .line 967538
    :cond_1e
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 967539
    iput-object v6, v8, LX/4Yk;->b:LX/0Px;

    .line 967540
    :cond_1f
    invoke-virtual {v8}, LX/4Yk;->a()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v6

    goto :goto_17

    .line 967541
    :cond_20
    new-instance v10, LX/4Yl;

    invoke-direct {v10}, LX/4Yl;-><init>()V

    .line 967542
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel;->a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;

    move-result-object v11

    .line 967543
    if-nez v11, :cond_21

    .line 967544
    const/4 v12, 0x0

    .line 967545
    :goto_1a
    move-object v11, v12

    .line 967546
    iput-object v11, v10, LX/4Yl;->b:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 967547
    invoke-virtual {v10}, LX/4Yl;->a()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsEdge;

    move-result-object v10

    goto :goto_19

    .line 967548
    :cond_21
    new-instance v12, LX/4ZQ;

    invoke-direct {v12}, LX/4ZQ;-><init>()V

    .line 967549
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 967550
    iput-object v6, v12, LX/4ZQ;->t:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967551
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 967552
    iput-object v6, v12, LX/4ZQ;->b:Ljava/lang/String;

    .line 967553
    invoke-virtual {v12}, LX/4ZQ;->a()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v12

    goto :goto_1a
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;
    .locals 13

    .prologue
    .line 966927
    if-nez p0, :cond_0

    .line 966928
    const/4 v0, 0x0

    .line 966929
    :goto_0
    return-object v0

    .line 966930
    :cond_0
    new-instance v2, LX/4XN;

    invoke-direct {v2}, LX/4XN;-><init>()V

    .line 966931
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 966932
    iput-object v0, v2, LX/4XN;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 966933
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->b()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;

    move-result-object v0

    .line 966934
    if-nez v0, :cond_3

    .line 966935
    const/4 v1, 0x0

    .line 966936
    :goto_1
    move-object v0, v1

    .line 966937
    iput-object v0, v2, LX/4XN;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 966938
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 966939
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 966940
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 966941
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel;

    .line 966942
    if-nez v0, :cond_14

    .line 966943
    const/4 v4, 0x0

    .line 966944
    :goto_3
    move-object v0, v4

    .line 966945
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 966946
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 966947
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 966948
    iput-object v0, v2, LX/4XN;->c:LX/0Px;

    .line 966949
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->d()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/5eL;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 966950
    iput-object v0, v2, LX/4XN;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 966951
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 966952
    iput-object v0, v2, LX/4XN;->f:Ljava/lang/String;

    .line 966953
    invoke-virtual {v2}, LX/4XN;->a()Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v0

    goto :goto_0

    .line 966954
    :cond_3
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    .line 966955
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->a()Z

    move-result v1

    .line 966956
    iput-boolean v1, v4, LX/3dM;->k:Z

    .line 966957
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->b()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v1

    .line 966958
    if-nez v1, :cond_6

    .line 966959
    const/4 v3, 0x0

    .line 966960
    :goto_4
    move-object v1, v3

    .line 966961
    iput-object v1, v4, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 966962
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->c()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;

    move-result-object v1

    .line 966963
    if-nez v1, :cond_a

    .line 966964
    const/4 v3, 0x0

    .line 966965
    :goto_5
    move-object v1, v3

    .line 966966
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 966967
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->d()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v1

    .line 966968
    if-nez v1, :cond_b

    .line 966969
    const/4 v3, 0x0

    .line 966970
    :goto_6
    move-object v1, v3

    .line 966971
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 966972
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->e()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 966973
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 966974
    const/4 v1, 0x0

    move v3, v1

    :goto_7
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 966975
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 966976
    if-nez v1, :cond_c

    .line 966977
    const/4 v6, 0x0

    .line 966978
    :goto_8
    move-object v1, v6

    .line 966979
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 966980
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_7

    .line 966981
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 966982
    iput-object v1, v4, LX/3dM;->N:LX/0Px;

    .line 966983
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->bs_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;

    move-result-object v1

    .line 966984
    if-nez v1, :cond_d

    .line 966985
    const/4 v3, 0x0

    .line 966986
    :goto_9
    move-object v1, v3

    .line 966987
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 966988
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->br_()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v1

    .line 966989
    if-nez v1, :cond_e

    .line 966990
    const/4 v3, 0x0

    .line 966991
    :goto_a
    move-object v1, v3

    .line 966992
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 966993
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->j()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v1

    .line 966994
    if-nez v1, :cond_13

    .line 966995
    const/4 v3, 0x0

    .line 966996
    :goto_b
    move-object v1, v3

    .line 966997
    iput-object v1, v4, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 966998
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel;->k()I

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->b(I)LX/3dM;

    .line 966999
    invoke-virtual {v4}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_1

    .line 967000
    :cond_6
    new-instance v6, LX/4Wx;

    invoke-direct {v6}, LX/4Wx;-><init>()V

    .line 967001
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 967002
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 967003
    const/4 v3, 0x0

    move v5, v3

    :goto_c
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_7

    .line 967004
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 967005
    if-nez v3, :cond_9

    .line 967006
    const/4 v8, 0x0

    .line 967007
    :goto_d
    move-object v3, v8

    .line 967008
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967009
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_c

    .line 967010
    :cond_7
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 967011
    iput-object v3, v6, LX/4Wx;->b:LX/0Px;

    .line 967012
    :cond_8
    invoke-virtual {v6}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v3

    goto/16 :goto_4

    .line 967013
    :cond_9
    new-instance v8, LX/3dL;

    invoke-direct {v8}, LX/3dL;-><init>()V

    .line 967014
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 967015
    iput-object v9, v8, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 967016
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 967017
    iput-object v9, v8, LX/3dL;->ag:Ljava/lang/String;

    .line 967018
    invoke-virtual {v8}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    goto :goto_d

    .line 967019
    :cond_a
    new-instance v3, LX/3dN;

    invoke-direct {v3}, LX/3dN;-><init>()V

    .line 967020
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->a()I

    move-result v5

    .line 967021
    iput v5, v3, LX/3dN;->b:I

    .line 967022
    invoke-virtual {v3}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    goto/16 :goto_5

    .line 967023
    :cond_b
    new-instance v3, LX/3dO;

    invoke-direct {v3}, LX/3dO;-><init>()V

    .line 967024
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v5

    .line 967025
    iput v5, v3, LX/3dO;->b:I

    .line 967026
    invoke-virtual {v3}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    goto/16 :goto_6

    .line 967027
    :cond_c
    new-instance v6, LX/4WL;

    invoke-direct {v6}, LX/4WL;-><init>()V

    .line 967028
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v7

    .line 967029
    iput v7, v6, LX/4WL;->b:I

    .line 967030
    invoke-virtual {v6}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v6

    goto/16 :goto_8

    .line 967031
    :cond_d
    new-instance v3, LX/4ZH;

    invoke-direct {v3}, LX/4ZH;-><init>()V

    .line 967032
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->a()I

    move-result v5

    .line 967033
    iput v5, v3, LX/4ZH;->b:I

    .line 967034
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->b()I

    move-result v5

    .line 967035
    iput v5, v3, LX/4ZH;->e:I

    .line 967036
    invoke-virtual {v3}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v3

    goto/16 :goto_9

    .line 967037
    :cond_e
    new-instance v6, LX/3dQ;

    invoke-direct {v6}, LX/3dQ;-><init>()V

    .line 967038
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 967039
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 967040
    const/4 v3, 0x0

    move v5, v3

    :goto_e
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_f

    .line 967041
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 967042
    if-nez v3, :cond_11

    .line 967043
    const/4 v8, 0x0

    .line 967044
    :goto_f
    move-object v3, v8

    .line 967045
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967046
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_e

    .line 967047
    :cond_f
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 967048
    iput-object v3, v6, LX/3dQ;->b:LX/0Px;

    .line 967049
    :cond_10
    invoke-virtual {v6}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v3

    goto/16 :goto_a

    .line 967050
    :cond_11
    new-instance v8, LX/4ZJ;

    invoke-direct {v8}, LX/4ZJ;-><init>()V

    .line 967051
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v9

    .line 967052
    if-nez v9, :cond_12

    .line 967053
    const/4 v10, 0x0

    .line 967054
    :goto_10
    move-object v9, v10

    .line 967055
    iput-object v9, v8, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 967056
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v9

    .line 967057
    iput v9, v8, LX/4ZJ;->c:I

    .line 967058
    invoke-virtual {v8}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v8

    goto :goto_f

    .line 967059
    :cond_12
    new-instance v10, LX/4WM;

    invoke-direct {v10}, LX/4WM;-><init>()V

    .line 967060
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v11

    .line 967061
    iput v11, v10, LX/4WM;->f:I

    .line 967062
    invoke-virtual {v10}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v10

    goto :goto_10

    .line 967063
    :cond_13
    new-instance v3, LX/33O;

    invoke-direct {v3}, LX/33O;-><init>()V

    .line 967064
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 967065
    iput-object v5, v3, LX/33O;->aI:Ljava/lang/String;

    .line 967066
    invoke-virtual {v3}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    goto/16 :goto_b

    .line 967067
    :cond_14
    new-instance v6, LX/4Wf;

    invoke-direct {v6}, LX/4Wf;-><init>()V

    .line 967068
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_16

    .line 967069
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 967070
    const/4 v4, 0x0

    move v5, v4

    :goto_11
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_15

    .line 967071
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    .line 967072
    if-nez v4, :cond_17

    .line 967073
    const/4 v8, 0x0

    .line 967074
    :goto_12
    move-object v4, v8

    .line 967075
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967076
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_11

    .line 967077
    :cond_15
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 967078
    iput-object v4, v6, LX/4Wf;->b:LX/0Px;

    .line 967079
    :cond_16
    new-instance v4, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;

    invoke-direct {v4, v6}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;-><init>(LX/4Wf;)V

    .line 967080
    move-object v4, v4

    .line 967081
    goto/16 :goto_3

    .line 967082
    :cond_17
    new-instance v8, LX/4We;

    invoke-direct {v8}, LX/4We;-><init>()V

    .line 967083
    invoke-virtual {v4}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v9

    .line 967084
    if-nez v9, :cond_18

    .line 967085
    const/4 v10, 0x0

    .line 967086
    :goto_13
    move-object v9, v10

    .line 967087
    iput-object v9, v8, LX/4We;->b:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    .line 967088
    new-instance v9, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;

    invoke-direct {v9, v8}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;-><init>(LX/4We;)V

    .line 967089
    move-object v8, v9

    .line 967090
    goto :goto_12

    .line 967091
    :cond_18
    new-instance v10, LX/4Wd;

    invoke-direct {v10}, LX/4Wd;-><init>()V

    .line 967092
    invoke-virtual {v9}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v11

    .line 967093
    if-nez v11, :cond_19

    .line 967094
    const/4 v12, 0x0

    .line 967095
    :goto_14
    move-object v11, v12

    .line 967096
    iput-object v11, v10, LX/4Wd;->b:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    .line 967097
    invoke-virtual {v9}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->b()Ljava/lang/String;

    move-result-object v11

    .line 967098
    iput-object v11, v10, LX/4Wd;->h:Ljava/lang/String;

    .line 967099
    new-instance v11, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;

    invoke-direct {v11, v10}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilter;-><init>(LX/4Wd;)V

    .line 967100
    move-object v10, v11

    .line 967101
    goto :goto_13

    .line 967102
    :cond_19
    new-instance v12, LX/4Wg;

    invoke-direct {v12}, LX/4Wg;-><init>()V

    .line 967103
    invoke-virtual {v11}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 967104
    iput-object v4, v12, LX/4Wg;->d:Ljava/lang/String;

    .line 967105
    new-instance v4, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;

    invoke-direct {v4, v12}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValue;-><init>(LX/4Wg;)V

    .line 967106
    move-object v12, v4

    .line 967107
    goto :goto_14
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;)Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .locals 8

    .prologue
    .line 967151
    if-nez p0, :cond_0

    .line 967152
    const/4 v0, 0x0

    .line 967153
    :goto_0
    return-object v0

    .line 967154
    :cond_0
    new-instance v2, LX/4XP;

    invoke-direct {v2}, LX/4XP;-><init>()V

    .line 967155
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 967156
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 967157
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 967158
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;

    .line 967159
    if-nez v0, :cond_3

    .line 967160
    const/4 v4, 0x0

    .line 967161
    :goto_2
    move-object v0, v4

    .line 967162
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967163
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 967164
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 967165
    iput-object v0, v2, LX/4XP;->b:LX/0Px;

    .line 967166
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 967167
    iput-object v0, v2, LX/4XP;->c:Ljava/lang/String;

    .line 967168
    invoke-virtual {v2}, LX/4XP;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    goto :goto_0

    .line 967169
    :cond_3
    new-instance v6, LX/4XO;

    invoke-direct {v6}, LX/4XO;-><init>()V

    .line 967170
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 967171
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 967172
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_4

    .line 967173
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;

    invoke-static {v4}, LX/5eL;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 967174
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 967175
    :cond_4
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 967176
    iput-object v4, v6, LX/4XO;->b:LX/0Px;

    .line 967177
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 967178
    iput-object v4, v6, LX/4XO;->c:Ljava/lang/String;

    .line 967179
    invoke-virtual {v6}, LX/4XO;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 2

    .prologue
    .line 967140
    if-nez p0, :cond_0

    .line 967141
    const/4 v0, 0x0

    .line 967142
    :goto_0
    return-object v0

    .line 967143
    :cond_0
    new-instance v0, LX/4Yz;

    invoke-direct {v0}, LX/4Yz;-><init>()V

    .line 967144
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->a()I

    move-result v1

    .line 967145
    iput v1, v0, LX/4Yz;->b:I

    .line 967146
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 967147
    iput-object v1, v0, LX/4Yz;->c:Ljava/lang/String;

    .line 967148
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->c()I

    move-result v1

    .line 967149
    iput v1, v0, LX/4Yz;->d:I

    .line 967150
    invoke-virtual {v0}, LX/4Yz;->a()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 967133
    if-nez p0, :cond_0

    .line 967134
    const/4 v0, 0x0

    .line 967135
    :goto_0
    return-object v0

    .line 967136
    :cond_0
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 967137
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 967138
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 967139
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 967108
    if-nez p0, :cond_1

    .line 967109
    :cond_0
    :goto_0
    return-object v2

    .line 967110
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 967111
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 967112
    if-nez p0, :cond_3

    .line 967113
    :goto_1
    move v1, v4

    .line 967114
    if-eqz v1, :cond_0

    .line 967115
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 967116
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 967117
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 967118
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 967119
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 967120
    const-string v1, "NativeTemplateConversionHelper.getSearchResultPageFiltersFragment"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 967121
    :cond_2
    new-instance v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-direct {v2, v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 967122
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;->a()LX/0Px;

    move-result-object v5

    .line 967123
    if-eqz v5, :cond_5

    .line 967124
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 967125
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 967126
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;

    invoke-static {v0, v1}, LX/5eL;->a(LX/186;Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterGroup;)I

    move-result v1

    aput v1, v6, v3

    .line 967127
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 967128
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 967129
    :goto_3
    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 967130
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 967131
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 967132
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_3
.end method
