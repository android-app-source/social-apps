.class public LX/5rH;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/5rG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1011106
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1011107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1011104
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1011105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1011096
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1011097
    return-void
.end method


# virtual methods
.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x51f6a437

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1011100
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 1011101
    iget-object v1, p0, LX/5rH;->a:LX/5rG;

    if-eqz v1, :cond_0

    .line 1011102
    iget-object v1, p0, LX/5rH;->a:LX/5rG;

    invoke-interface {v1, p1, p2}, LX/5rG;->a(II)V

    .line 1011103
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x1ac294ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnSizeChangedListener(LX/5rG;)V
    .locals 0

    .prologue
    .line 1011098
    iput-object p1, p0, LX/5rH;->a:LX/5rG;

    .line 1011099
    return-void
.end method
