.class public final LX/54e;
.super LX/52r;
.source ""

# interfaces
.implements LX/0za;


# instance fields
.field public final a:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/54f;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/54g;

.field private final c:LX/54h;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(LX/54g;)V
    .locals 1

    .prologue
    .line 828204
    iput-object p1, p0, LX/54e;->b:LX/54g;

    invoke-direct {p0}, LX/52r;-><init>()V

    .line 828205
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/54e;->a:Ljava/util/PriorityQueue;

    .line 828206
    new-instance v0, LX/54h;

    invoke-direct {v0}, LX/54h;-><init>()V

    iput-object v0, p0, LX/54e;->c:LX/54h;

    .line 828207
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/54e;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private a(LX/0vR;J)LX/0za;
    .locals 4

    .prologue
    .line 828191
    iget-object v0, p0, LX/54e;->c:LX/54h;

    invoke-virtual {v0}, LX/54h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828192
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 828193
    :goto_0
    return-object v0

    .line 828194
    :cond_0
    new-instance v0, LX/54f;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, LX/54g;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    iget-object v3, p0, LX/54e;->b:LX/54g;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->incrementAndGet(Ljava/lang/Object;)I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, LX/54f;-><init>(LX/0vR;Ljava/lang/Long;I)V

    .line 828195
    iget-object v1, p0, LX/54e;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 828196
    iget-object v1, p0, LX/54e;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    if-nez v1, :cond_3

    .line 828197
    :cond_1
    iget-object v0, p0, LX/54e;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/54f;

    .line 828198
    if-eqz v0, :cond_2

    .line 828199
    iget-object v0, v0, LX/54f;->a:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V

    .line 828200
    :cond_2
    iget-object v0, p0, LX/54e;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-gtz v0, :cond_1

    .line 828201
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 828202
    goto :goto_0

    .line 828203
    :cond_3
    new-instance v1, LX/54d;

    invoke-direct {v1, p0, v0}, LX/54d;-><init>(LX/54e;LX/54f;)V

    invoke-static {v1}, LX/0ze;->a(LX/0vR;)LX/0za;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0vR;)LX/0za;
    .locals 2

    .prologue
    .line 828190
    invoke-static {}, LX/52r;->a()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, LX/54e;->a(LX/0vR;J)LX/0za;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 4

    .prologue
    .line 828185
    invoke-static {}, LX/52r;->a()J

    move-result-wide v0

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 828186
    new-instance v2, LX/54c;

    invoke-direct {v2, p1, p0, v0, v1}, LX/54c;-><init>(LX/0vR;LX/52r;J)V

    invoke-direct {p0, v2, v0, v1}, LX/54e;->a(LX/0vR;J)LX/0za;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 828188
    iget-object v0, p0, LX/54e;->c:LX/54h;

    invoke-virtual {v0}, LX/54h;->b()V

    .line 828189
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828187
    iget-object v0, p0, LX/54e;->c:LX/54h;

    invoke-virtual {v0}, LX/54h;->c()Z

    move-result v0

    return v0
.end method
