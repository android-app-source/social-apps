.class public final LX/6Jy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JU;


# instance fields
.field public final synthetic a:LX/6Jz;


# direct methods
.method public constructor <init>(LX/6Jz;)V
    .locals 0

    .prologue
    .line 1076068
    iput-object p1, p0, LX/6Jy;->a:LX/6Jz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1076069
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    .line 1076070
    iput-object v2, v0, LX/6K6;->i:LX/6L3;

    .line 1076071
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    .line 1076072
    iput-object v2, v0, LX/6K6;->m:LX/6Jc;

    .line 1076073
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    .line 1076074
    iput-object v1, v0, LX/6K6;->q:LX/6K9;

    .line 1076075
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    const/4 v1, 0x0

    .line 1076076
    iput-boolean v1, v0, LX/6K6;->k:Z

    .line 1076077
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    invoke-static {v0}, LX/6K6;->g(LX/6K6;)V

    .line 1076078
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    if-eqz v0, :cond_0

    .line 1076079
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    invoke-interface {v0}, LX/6JG;->b()V

    .line 1076080
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    .line 1076081
    iput-object v2, v0, LX/6K6;->p:LX/6JG;

    .line 1076082
    :cond_0
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076083
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076084
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076085
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1076086
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    .line 1076087
    iput-object v1, v0, LX/6K6;->q:LX/6K9;

    .line 1076088
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    .line 1076089
    iput-boolean v3, v0, LX/6K6;->k:Z

    .line 1076090
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    if-eqz v0, :cond_0

    .line 1076091
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to stop av recorder"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6JG;->a(LX/6JJ;)V

    .line 1076092
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    const/4 v1, 0x0

    .line 1076093
    iput-object v1, v0, LX/6K6;->p:LX/6JG;

    .line 1076094
    :cond_0
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076095
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, LX/6Jt;->d(I)V

    .line 1076096
    iget-object v0, p0, LX/6Jy;->a:LX/6Jz;

    iget-object v0, v0, LX/6Jz;->a:LX/6K6;

    invoke-static {v0, v3}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076097
    return-void
.end method
