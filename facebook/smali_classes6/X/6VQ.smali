.class public LX/6VQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/InstantArticleMediaFetchListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/InstantArticleMediaFetchListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1104015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104016
    iput-object p1, p0, LX/6VQ;->a:Ljava/util/Set;

    .line 1104017
    iput-object p2, p0, LX/6VQ;->b:Ljava/util/Set;

    .line 1104018
    return-void
.end method

.method public static b(LX/6VQ;LX/3Bl;)V
    .locals 2

    .prologue
    .line 1104035
    invoke-virtual {p1}, LX/3Bl;->b()V

    .line 1104036
    invoke-virtual {p1}, LX/3Bl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104037
    iget-object v0, p1, LX/3Bl;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1104038
    iget-object v1, p1, LX/3Bl;->d:Ljava/lang/Integer;

    move-object v1, v1

    .line 1104039
    invoke-virtual {p0, v0, v1}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1104040
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/3Bl;)V
    .locals 2

    .prologue
    .line 1104029
    if-nez p1, :cond_1

    .line 1104030
    :cond_0
    return-void

    .line 1104031
    :cond_1
    invoke-static {p0, p2}, LX/6VQ;->b(LX/6VQ;LX/3Bl;)V

    .line 1104032
    iget-object v0, p0, LX/6VQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 1104033
    const-string p0, "ATTACHMENT_PHOTO"

    const/4 p2, 0x1

    invoke-static {v0, p1, p0, p2}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1104034
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "dispatchAttachmentAllMediaFetchEvent"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 1104025
    iget-object v0, p0, LX/6VQ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 1104026
    invoke-static {v0, p2, p1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 1104027
    goto :goto_0

    .line 1104028
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/3Bl;)V
    .locals 2

    .prologue
    .line 1104019
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1104020
    :cond_0
    return-void

    .line 1104021
    :cond_1
    invoke-virtual {p3}, LX/3Bl;->a()V

    .line 1104022
    iget-object v0, p0, LX/6VQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 1104023
    const-string p0, "ATTACHMENT_PHOTO"

    invoke-static {v0, p1, p2, p0}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1104024
    goto :goto_0
.end method
