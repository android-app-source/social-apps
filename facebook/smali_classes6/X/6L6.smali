.class public final LX/6L6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1077971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077972
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1077973
    const/4 v0, 0x0

    move v3, v0

    move-object v0, v2

    :goto_0
    const/4 v1, 0x3

    if-ge v3, v1, :cond_2

    .line 1077974
    :try_start_0
    invoke-static {p0}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    .line 1077975
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v1, p1, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077976
    :goto_1
    if-nez v1, :cond_1

    .line 1077977
    if-nez v0, :cond_0

    .line 1077978
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to create media codec encode"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1077979
    :cond_0
    throw v0

    .line 1077980
    :catch_0
    move-exception v0

    .line 1077981
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1077982
    :cond_1
    return-object v1

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method
