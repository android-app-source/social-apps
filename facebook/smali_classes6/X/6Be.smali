.class public final LX/6Be;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6BZ;


# instance fields
.field private final a:LX/0SG;

.field private final b:J

.field private c:J


# direct methods
.method public constructor <init>(LX/0SG;J)V
    .locals 0

    .prologue
    .line 1062891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062892
    iput-object p1, p0, LX/6Be;->a:LX/0SG;

    .line 1062893
    iput-wide p2, p0, LX/6Be;->b:J

    .line 1062894
    invoke-virtual {p0}, LX/6Be;->b()V

    .line 1062895
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1062890
    iget-object v0, p0, LX/6Be;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/6Be;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1062887
    iget-object v0, p0, LX/6Be;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/6Be;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/6Be;->c:J

    .line 1062888
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1062889
    const-string v0, "TimeLimit"

    return-object v0
.end method
