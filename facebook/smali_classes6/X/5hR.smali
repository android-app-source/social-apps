.class public final LX/5hR;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 978854
    const-class v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    const v0, 0x68beb383

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "VideosUploadedByUserDetailQuery"

    const-string v6, "5aebe6daeb12ea1dd62d7a17a84806c4"

    const-string v7, "node"

    const-string v8, "10155138722121729"

    const-string v9, "10155259090606729"

    .line 978855
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 978856
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 978857
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 978858
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 978859
    sparse-switch v0, :sswitch_data_0

    .line 978860
    :goto_0
    return-object p1

    .line 978861
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 978862
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 978863
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 978864
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 978865
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 978866
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 978867
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 978868
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 978869
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 978870
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 978871
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_6
        -0x5305c081 -> :sswitch_1
        -0x25a646c8 -> :sswitch_3
        -0x2177e47b -> :sswitch_4
        0x58705dc -> :sswitch_0
        0x5a7510f -> :sswitch_2
        0x1918b88b -> :sswitch_5
        0x2292beef -> :sswitch_a
        0x26d0c0ff -> :sswitch_9
        0x73a026b5 -> :sswitch_7
        0x7e07ec78 -> :sswitch_8
    .end sparse-switch
.end method
