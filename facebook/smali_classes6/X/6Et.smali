.class public LX/6Et;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066733
    const-class v0, LX/6Et;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Et;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0lB;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066735
    iput-object p1, p0, LX/6Et;->b:LX/0lB;

    .line 1066736
    return-void
.end method

.method public static a(LX/0lF;)Z
    .locals 2

    .prologue
    .line 1066737
    const-string v0, "fb_signed_token"

    const-string v1, "type"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6Et;
    .locals 2

    .prologue
    .line 1066738
    new-instance v1, LX/6Et;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-direct {v1, v0}, LX/6Et;-><init>(LX/0lB;)V

    .line 1066739
    return-object v1
.end method

.method public static b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1066740
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v0

    .line 1066741
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1066742
    :cond_0
    const/4 v0, 0x0

    .line 1066743
    :goto_0
    return-object v0

    .line 1066744
    :cond_1
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1066745
    invoke-static {}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->newBuilder()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setName(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setStreet1(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setStreet2(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setCity(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setRegion(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setPostalCode(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->f()Lcom/facebook/common/locale/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/locale/Country;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->setCountry(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    move-result-object v0

    goto :goto_0
.end method
