.class public final LX/5gV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 974739
    const/16 v22, 0x0

    .line 974740
    const/16 v21, 0x0

    .line 974741
    const/16 v20, 0x0

    .line 974742
    const/16 v19, 0x0

    .line 974743
    const/16 v18, 0x0

    .line 974744
    const-wide/16 v16, 0x0

    .line 974745
    const/4 v15, 0x0

    .line 974746
    const/4 v14, 0x0

    .line 974747
    const/4 v13, 0x0

    .line 974748
    const/4 v12, 0x0

    .line 974749
    const-wide/16 v10, 0x0

    .line 974750
    const/4 v9, 0x0

    .line 974751
    const/4 v8, 0x0

    .line 974752
    const/4 v7, 0x0

    .line 974753
    const/4 v6, 0x0

    .line 974754
    const/4 v5, 0x0

    .line 974755
    const/4 v4, 0x0

    .line 974756
    const/4 v3, 0x0

    .line 974757
    const/4 v2, 0x0

    .line 974758
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_15

    .line 974759
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 974760
    const/4 v2, 0x0

    .line 974761
    :goto_0
    return v2

    .line 974762
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    if-eq v2, v0, :cond_f

    .line 974763
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 974764
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 974765
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 974766
    const-string v24, "album_cover_photo"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 974767
    invoke-static/range {p0 .. p1}, LX/5gR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 974768
    :cond_1
    const-string v24, "album_type"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 974769
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 974770
    :cond_2
    const-string v24, "can_edit_caption"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 974771
    const/4 v2, 0x1

    .line 974772
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v21, v9

    move v9, v2

    goto :goto_1

    .line 974773
    :cond_3
    const-string v24, "can_upload"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 974774
    const/4 v2, 0x1

    .line 974775
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v20, v7

    move v7, v2

    goto :goto_1

    .line 974776
    :cond_4
    const-string v24, "can_viewer_delete"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 974777
    const/4 v2, 0x1

    .line 974778
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v19, v6

    move v6, v2

    goto/16 :goto_1

    .line 974779
    :cond_5
    const-string v24, "created_time"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 974780
    const/4 v2, 0x1

    .line 974781
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 974782
    :cond_6
    const-string v24, "feedback"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 974783
    invoke-static/range {p0 .. p1}, LX/59W;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 974784
    :cond_7
    const-string v24, "id"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 974785
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 974786
    :cond_8
    const-string v24, "media_owner_object"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 974787
    invoke-static/range {p0 .. p1}, LX/5gS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 974788
    :cond_9
    const-string v24, "message"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 974789
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 974790
    :cond_a
    const-string v24, "modified_time"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 974791
    const/4 v2, 0x1

    .line 974792
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v14

    move v8, v2

    goto/16 :goto_1

    .line 974793
    :cond_b
    const-string v24, "owner"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 974794
    invoke-static/range {p0 .. p1}, LX/5gT;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 974795
    :cond_c
    const-string v24, "privacy_scope"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 974796
    invoke-static/range {p0 .. p1}, LX/5gU;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 974797
    :cond_d
    const-string v24, "title"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 974798
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 974799
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 974800
    :cond_f
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 974801
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 974802
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 974803
    if-eqz v9, :cond_10

    .line 974804
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 974805
    :cond_10
    if-eqz v7, :cond_11

    .line 974806
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 974807
    :cond_11
    if-eqz v6, :cond_12

    .line 974808
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 974809
    :cond_12
    if-eqz v3, :cond_13

    .line 974810
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 974811
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 974812
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 974813
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 974814
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 974815
    if-eqz v8, :cond_14

    .line 974816
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 974817
    :cond_14
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 974818
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 974819
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 974820
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_15
    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v15

    move/from16 v26, v9

    move v9, v6

    move v6, v4

    move/from16 v27, v12

    move/from16 v12, v26

    move/from16 v28, v13

    move/from16 v13, v27

    move/from16 v29, v14

    move-wide v14, v10

    move v11, v8

    move v10, v7

    move v7, v5

    move v8, v2

    move-wide/from16 v4, v16

    move/from16 v17, v29

    move/from16 v16, v28

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 974821
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974822
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974823
    if-eqz v0, :cond_1

    .line 974824
    const-string v1, "album_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974825
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974826
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 974827
    if-eqz v1, :cond_0

    .line 974828
    const-string v3, "imageThumbnail"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974829
    invoke-static {p0, v1, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 974830
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974831
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 974832
    if-eqz v0, :cond_2

    .line 974833
    const-string v0, "album_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974834
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974835
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 974836
    if-eqz v0, :cond_3

    .line 974837
    const-string v1, "can_edit_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974838
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 974839
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 974840
    if-eqz v0, :cond_4

    .line 974841
    const-string v1, "can_upload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974842
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 974843
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 974844
    if-eqz v0, :cond_5

    .line 974845
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974846
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 974847
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 974848
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 974849
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974850
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 974851
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974852
    if-eqz v0, :cond_7

    .line 974853
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974854
    invoke-static {p0, v0, p2, p3}, LX/59W;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 974855
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 974856
    if-eqz v0, :cond_8

    .line 974857
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974859
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974860
    if-eqz v0, :cond_9

    .line 974861
    const-string v1, "media_owner_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974862
    invoke-static {p0, v0, p2}, LX/5gS;->a(LX/15i;ILX/0nX;)V

    .line 974863
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974864
    if-eqz v0, :cond_a

    .line 974865
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974866
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 974867
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 974868
    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    .line 974869
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974870
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 974871
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974872
    if-eqz v0, :cond_c

    .line 974873
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974874
    invoke-static {p0, v0, p2}, LX/5gT;->a(LX/15i;ILX/0nX;)V

    .line 974875
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974876
    if-eqz v0, :cond_12

    .line 974877
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974878
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974879
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 974880
    if-eqz v1, :cond_f

    .line 974881
    const-string v2, "icon_image"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974882
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974883
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 974884
    if-eqz v2, :cond_d

    .line 974885
    const-string p3, "name"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974886
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974887
    :cond_d
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 974888
    if-eqz v2, :cond_e

    .line 974889
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974890
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974891
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974892
    :cond_f
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 974893
    if-eqz v1, :cond_10

    .line 974894
    const-string v2, "label"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974895
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974896
    :cond_10
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 974897
    if-eqz v1, :cond_11

    .line 974898
    const-string v2, "legacy_graph_api_privacy_json"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974899
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974900
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974901
    :cond_12
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974902
    if-eqz v0, :cond_13

    .line 974903
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974904
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 974905
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974906
    return-void
.end method
