.class public LX/5ul;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/saved/server/UpdateSavedStateParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1020096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020097
    iput-object p1, p0, LX/5ul;->a:Ljava/lang/String;

    .line 1020098
    return-void
.end method

.method public static b(LX/0QB;)LX/5ul;
    .locals 2

    .prologue
    .line 1020099
    new-instance v1, LX/5ul;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, LX/5ul;-><init>(Ljava/lang/String;)V

    .line 1020100
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1020101
    check-cast p1, Lcom/facebook/saved/server/UpdateSavedStateParams;

    .line 1020102
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1020103
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    move-object v0, v0

    .line 1020104
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1020105
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source_story_id"

    .line 1020106
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    move-object v0, v0

    .line 1020107
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020108
    :cond_0
    :goto_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action"

    .line 1020109
    iget-object v2, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    move-object v2, v2

    .line 1020110
    invoke-virtual {v2}, LX/5uo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020111
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "surface"

    .line 1020112
    iget-object v2, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1020113
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020114
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "mechanism"

    .line 1020115
    iget-object v2, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1020116
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020117
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tracking"

    .line 1020118
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->e:LX/0am;

    move-object v0, v0

    .line 1020119
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020120
    new-instance v0, LX/14N;

    const-string v1, "updateSavedState"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/5ul;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/saved"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1020121
    :cond_1
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    move-object v0, v0

    .line 1020122
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1020123
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "object_id"

    .line 1020124
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    move-object v0, v0

    .line 1020125
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1020126
    :cond_2
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->d:LX/0am;

    move-object v0, v0

    .line 1020127
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1020128
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "url"

    .line 1020129
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->d:LX/0am;

    move-object v0, v0

    .line 1020130
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1020131
    :cond_3
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->a:LX/0am;

    move-object v0, v0

    .line 1020132
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020133
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message_id"

    .line 1020134
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->a:LX/0am;

    move-object v0, v0

    .line 1020135
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1020136
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1020137
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1020138
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
