.class public LX/5Ta;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 922040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .locals 5

    .prologue
    .line 922041
    new-instance v1, LX/4gf;

    invoke-direct {v1}, LX/4gf;-><init>()V

    .line 922042
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->cs_()Ljava/lang/String;

    move-result-object v0

    .line 922043
    iput-object v0, v1, LX/4gf;->a:Ljava/lang/String;

    .line 922044
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 922045
    iput-object v0, v1, LX/4gf;->b:Ljava/lang/String;

    .line 922046
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4gf;->c(Ljava/lang/String;)LX/4gf;

    .line 922047
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4gf;->d(Ljava/lang/String;)LX/4gf;

    .line 922048
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->j()Z

    move-result v0

    .line 922049
    iput-boolean v0, v1, LX/4gf;->g:Z

    .line 922050
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->ct_()Z

    move-result v0

    .line 922051
    iput-boolean v0, v1, LX/4gf;->h:Z

    .line 922052
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->b()Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a(Ljava/lang/String;)LX/4ge;

    move-result-object v0

    .line 922053
    iput-object v0, v1, LX/4gf;->e:LX/4ge;

    .line 922054
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 922055
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->c()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$ActionTargetsModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$ActionTargetsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 922056
    iput-object v0, v1, LX/4gf;->f:Ljava/lang/String;

    .line 922057
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->m()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;

    move-result-object v0

    .line 922058
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 922059
    if-nez v2, :cond_3

    .line 922060
    const/4 v2, 0x0

    .line 922061
    :goto_1
    move-object v0, v2

    .line 922062
    iput-object v0, v1, LX/4gf;->i:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 922063
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->l()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$PaymentMetadataModel;

    move-result-object v0

    .line 922064
    if-nez v0, :cond_5

    .line 922065
    const/4 v2, 0x0

    .line 922066
    :goto_2
    move-object v0, v2

    .line 922067
    iput-object v0, v1, LX/4gf;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 922068
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->n()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$WebviewMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 922069
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->n()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$WebviewMetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$WebviewMetadataModel;->a()D

    move-result-wide v2

    .line 922070
    iput-wide v2, v1, LX/4gf;->j:D

    .line 922071
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;->n()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$WebviewMetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$WebviewMetadataModel;->b()Z

    move-result v0

    .line 922072
    iput-boolean v0, v1, LX/4gf;->k:Z

    .line 922073
    :cond_2
    invoke-virtual {v1}, LX/4gf;->n()Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    return-object v0

    .line 922074
    :cond_3
    new-instance v2, LX/4gc;

    invoke-direct {v2}, LX/4gc;-><init>()V

    .line 922075
    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 922076
    iput-object v3, v2, LX/4gc;->a:Ljava/lang/String;

    .line 922077
    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 922078
    iput-object v3, v2, LX/4gc;->b:Ljava/lang/String;

    .line 922079
    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 922080
    iput-object v3, v2, LX/4gc;->c:Ljava/lang/String;

    .line 922081
    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCTAUserConfirmationModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 922082
    iput-object v3, v2, LX/4gc;->d:Ljava/lang/String;

    .line 922083
    invoke-virtual {v2}, LX/4gc;->e()Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    move-result-object v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    new-instance v2, LX/4ga;

    invoke-direct {v2}, LX/4ga;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$PaymentMetadataModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 922084
    iput-object v3, v2, LX/4ga;->a:Ljava/lang/String;

    .line 922085
    move-object v2, v2

    .line 922086
    invoke-virtual {v0}, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel$PaymentMetadataModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 922087
    iput-object v3, v2, LX/4ga;->b:Ljava/lang/String;

    .line 922088
    move-object v2, v2

    .line 922089
    invoke-virtual {v2}, LX/4ga;->a()Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    move-result-object v2

    goto :goto_2
.end method
