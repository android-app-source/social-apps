.class public final LX/5cS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 962893
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 962894
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 962895
    :goto_0
    return v1

    .line 962896
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 962897
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 962898
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 962899
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 962900
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 962901
    const-string v5, "arrival_airport"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 962902
    invoke-static {p0, p1}, LX/5cc;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 962903
    :cond_2
    const-string v5, "departure_airport"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 962904
    invoke-static {p0, p1}, LX/5cc;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 962905
    :cond_3
    const-string v5, "departure_time_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 962906
    invoke-static {p0, p1}, LX/5cY;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 962907
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 962908
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 962909
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 962910
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 962911
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 962912
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 962913
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962914
    if-eqz v0, :cond_0

    .line 962915
    const-string v1, "arrival_airport"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962916
    invoke-static {p0, v0, p2}, LX/5cc;->a(LX/15i;ILX/0nX;)V

    .line 962917
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962918
    if-eqz v0, :cond_1

    .line 962919
    const-string v1, "departure_airport"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962920
    invoke-static {p0, v0, p2}, LX/5cc;->a(LX/15i;ILX/0nX;)V

    .line 962921
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962922
    if-eqz v0, :cond_2

    .line 962923
    const-string v1, "departure_time_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962924
    invoke-static {p0, v0, p2}, LX/5cY;->a(LX/15i;ILX/0nX;)V

    .line 962925
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 962926
    return-void
.end method
