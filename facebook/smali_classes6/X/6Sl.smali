.class public final LX/6Sl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1096286
    const/4 v10, 0x0

    .line 1096287
    const/4 v9, 0x0

    .line 1096288
    const/4 v8, 0x0

    .line 1096289
    const/4 v7, 0x0

    .line 1096290
    const/4 v6, 0x0

    .line 1096291
    const/4 v5, 0x0

    .line 1096292
    const/4 v4, 0x0

    .line 1096293
    const/4 v3, 0x0

    .line 1096294
    const/4 v2, 0x0

    .line 1096295
    const/4 v1, 0x0

    .line 1096296
    const/4 v0, 0x0

    .line 1096297
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1096298
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096299
    const/4 v0, 0x0

    .line 1096300
    :goto_0
    return v0

    .line 1096301
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096302
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1096303
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1096304
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096305
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1096306
    const-string v12, "author"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1096307
    invoke-static {p0, p1}, LX/6T0;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1096308
    :cond_2
    const-string v12, "body"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1096309
    invoke-static {p0, p1}, LX/4ar;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1096310
    :cond_3
    const-string v12, "feedback"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1096311
    invoke-static {p0, p1}, LX/6Sg;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1096312
    :cond_4
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1096313
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1096314
    :cond_5
    const-string v12, "is_featured"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1096315
    const/4 v1, 0x1

    .line 1096316
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1096317
    :cond_6
    const-string v12, "live_streaming_comment_priority"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1096318
    const/4 v0, 0x1

    .line 1096319
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 1096320
    :cond_7
    const-string v12, "notable_likers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1096321
    invoke-static {p0, p1}, LX/6Si;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1096322
    :cond_8
    const-string v12, "translatability_for_viewer"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1096323
    invoke-static {p0, p1}, LX/6Sj;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1096324
    :cond_9
    const-string v12, "translated_body_for_viewer"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1096325
    invoke-static {p0, p1}, LX/6Sk;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1096326
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1096327
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1096328
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1096329
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1096330
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1096331
    if-eqz v1, :cond_b

    .line 1096332
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1096333
    :cond_b
    if-eqz v0, :cond_c

    .line 1096334
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 1096335
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1096336
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1096337
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1096338
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1096339
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096340
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1096341
    if-eqz v0, :cond_0

    .line 1096342
    const-string v1, "author"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096343
    invoke-static {p0, v0, p2, p3}, LX/6T0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096344
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096345
    if-eqz v0, :cond_1

    .line 1096346
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096347
    invoke-static {p0, v0, p2, p3}, LX/4ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096348
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096349
    if-eqz v0, :cond_2

    .line 1096350
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096351
    invoke-static {p0, v0, p2}, LX/6Sg;->a(LX/15i;ILX/0nX;)V

    .line 1096352
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096353
    if-eqz v0, :cond_3

    .line 1096354
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096355
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096356
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096357
    if-eqz v0, :cond_4

    .line 1096358
    const-string v1, "is_featured"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096359
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096360
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1096361
    if-eqz v0, :cond_5

    .line 1096362
    const-string v1, "live_streaming_comment_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096363
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1096364
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096365
    if-eqz v0, :cond_6

    .line 1096366
    const-string v1, "notable_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096367
    invoke-static {p0, v0, p2, p3}, LX/6Si;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096368
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096369
    if-eqz v0, :cond_7

    .line 1096370
    const-string v1, "translatability_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096371
    invoke-static {p0, v0, p2, p3}, LX/6Sj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096372
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096373
    if-eqz v0, :cond_8

    .line 1096374
    const-string v1, "translated_body_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096375
    invoke-static {p0, v0, p2}, LX/6Sk;->a(LX/15i;ILX/0nX;)V

    .line 1096376
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096377
    return-void
.end method
