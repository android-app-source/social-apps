.class public final LX/6CN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6CO;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6CP;


# direct methods
.method public constructor <init>(LX/6CP;LX/6CO;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1063811
    iput-object p1, p0, LX/6CN;->c:LX/6CP;

    iput-object p2, p0, LX/6CN;->a:LX/6CO;

    iput-object p3, p0, LX/6CN;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1063806
    iget-object v0, p0, LX/6CN;->a:LX/6CO;

    invoke-interface {v0}, LX/6CO;->b()V

    .line 1063807
    iget-object v0, p0, LX/6CN;->c:LX/6CP;

    iget-object v0, v0, LX/6CP;->a:LX/6CH;

    const-string v1, "CommerceCartMutator"

    const-string v2, "Browser commerce cart mutation fails."

    iget-object v3, p0, LX/6CN;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/6CH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1063808
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1063809
    iget-object v0, p0, LX/6CN;->a:LX/6CO;

    invoke-interface {v0}, LX/6CO;->a()V

    .line 1063810
    return-void
.end method
