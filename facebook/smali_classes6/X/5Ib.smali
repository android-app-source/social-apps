.class public final LX/5Ib;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 895426
    const/4 v12, 0x0

    .line 895427
    const/4 v11, 0x0

    .line 895428
    const/4 v10, 0x0

    .line 895429
    const/4 v9, 0x0

    .line 895430
    const/4 v8, 0x0

    .line 895431
    const/4 v7, 0x0

    .line 895432
    const/4 v6, 0x0

    .line 895433
    const/4 v5, 0x0

    .line 895434
    const/4 v4, 0x0

    .line 895435
    const/4 v3, 0x0

    .line 895436
    const/4 v2, 0x0

    .line 895437
    const/4 v1, 0x0

    .line 895438
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 895439
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 895440
    const/4 v1, 0x0

    .line 895441
    :goto_0
    return v1

    .line 895442
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 895443
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 895444
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 895445
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 895446
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 895447
    const-string v14, "__type__"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "__typename"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 895448
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    goto :goto_1

    .line 895449
    :cond_3
    const-string v14, "can_viewer_add_to_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 895450
    const/4 v2, 0x1

    .line 895451
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 895452
    :cond_4
    const-string v14, "can_viewer_remove_from_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 895453
    const/4 v1, 0x1

    .line 895454
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 895455
    :cond_5
    const-string v14, "confirmed_places_for_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 895456
    invoke-static/range {p0 .. p1}, LX/5Ec;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 895457
    :cond_6
    const-string v14, "confirmed_profiles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 895458
    invoke-static/range {p0 .. p1}, LX/5G9;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 895459
    :cond_7
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 895460
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 895461
    :cond_8
    const-string v14, "lightweight_recs"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 895462
    invoke-static/range {p0 .. p1}, LX/5G6;->b(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 895463
    :cond_9
    const-string v14, "pending_place_slots"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 895464
    invoke-static/range {p0 .. p1}, LX/5G4;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 895465
    :cond_a
    const-string v14, "pending_places_for_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 895466
    invoke-static/range {p0 .. p1}, LX/5Ec;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 895467
    :cond_b
    const-string v14, "pending_profiles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 895468
    invoke-static/range {p0 .. p1}, LX/5G9;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 895469
    :cond_c
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 895470
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 895471
    if-eqz v2, :cond_d

    .line 895472
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 895473
    :cond_d
    if-eqz v1, :cond_e

    .line 895474
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->a(IZ)V

    .line 895475
    :cond_e
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 895476
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 895477
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 895478
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 895479
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 895480
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 895481
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 895482
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 895483
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 895484
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 895485
    if-eqz v0, :cond_0

    .line 895486
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895487
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 895488
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 895489
    if-eqz v0, :cond_1

    .line 895490
    const-string v1, "can_viewer_add_to_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895491
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 895492
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 895493
    if-eqz v0, :cond_2

    .line 895494
    const-string v1, "can_viewer_remove_from_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895495
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 895496
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895497
    if-eqz v0, :cond_3

    .line 895498
    const-string v1, "confirmed_places_for_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895499
    invoke-static {p0, v0, p2, p3}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895500
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895501
    if-eqz v0, :cond_4

    .line 895502
    const-string v1, "confirmed_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895503
    invoke-static {p0, v0, p2, p3}, LX/5G9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895504
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 895505
    if-eqz v0, :cond_5

    .line 895506
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895507
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 895508
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895509
    if-eqz v0, :cond_6

    .line 895510
    const-string v1, "lightweight_recs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895511
    invoke-static {p0, v0, p2, p3}, LX/5G6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895512
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895513
    if-eqz v0, :cond_7

    .line 895514
    const-string v1, "pending_place_slots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895515
    invoke-static {p0, v0, p2, p3}, LX/5G4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895516
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895517
    if-eqz v0, :cond_8

    .line 895518
    const-string v1, "pending_places_for_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895519
    invoke-static {p0, v0, p2, p3}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895520
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 895521
    if-eqz v0, :cond_9

    .line 895522
    const-string v1, "pending_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895523
    invoke-static {p0, v0, p2, p3}, LX/5G9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895524
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 895525
    return-void
.end method
