.class public LX/54v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/54r;


# instance fields
.field public final a:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828705
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/54v;->a:Landroid/graphics/RectF;

    return-void
.end method

.method private static a(Landroid/content/Context;IFFF)LX/54z;
    .locals 6

    .prologue
    .line 828703
    new-instance v0, LX/54z;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/54z;-><init>(Landroid/content/res/Resources;IFFF)V

    return-object v0
.end method

.method private h(LX/54q;)V
    .locals 4

    .prologue
    .line 828695
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 828696
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828697
    invoke-virtual {v0, v1}, LX/54z;->getPadding(Landroid/graphics/Rect;)Z

    .line 828698
    move-object v0, p1

    .line 828699
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, p1}, LX/54v;->c(LX/54q;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    move-object v0, p1

    .line 828700
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, p1}, LX/54v;->b(LX/54q;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumWidth(I)V

    .line 828701
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-interface {p1, v0, v2, v3, v1}, LX/54q;->a(IIII)V

    .line 828702
    return-void
.end method

.method private static i(LX/54q;)LX/54z;
    .locals 1

    .prologue
    .line 828694
    invoke-interface {p0}, LX/54q;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, LX/54z;

    return-object v0
.end method


# virtual methods
.method public final a(LX/54q;)F
    .locals 1

    .prologue
    .line 828691
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828692
    iget p0, v0, LX/54z;->k:F

    move v0, p0

    .line 828693
    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 828689
    new-instance v0, LX/54u;

    invoke-direct {v0, p0}, LX/54u;-><init>(LX/54v;)V

    sput-object v0, LX/54z;->c:LX/54t;

    .line 828690
    return-void
.end method

.method public final a(LX/54q;F)V
    .locals 3

    .prologue
    .line 828646
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828647
    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p2

    float-to-int v1, v1

    int-to-float v1, v1

    .line 828648
    iget v2, v0, LX/54z;->h:F

    cmpl-float v2, v2, v1

    if-nez v2, :cond_0

    .line 828649
    :goto_0
    invoke-direct {p0, p1}, LX/54v;->h(LX/54q;)V

    .line 828650
    return-void

    .line 828651
    :cond_0
    iput v1, v0, LX/54z;->h:F

    .line 828652
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/54z;->n:Z

    .line 828653
    invoke-virtual {v0}, LX/54z;->invalidateSelf()V

    goto :goto_0
.end method

.method public final a(LX/54q;I)V
    .locals 1

    .prologue
    .line 828685
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828686
    iget-object p0, v0, LX/54z;->d:Landroid/graphics/Paint;

    invoke-virtual {p0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 828687
    invoke-virtual {v0}, LX/54z;->invalidateSelf()V

    .line 828688
    return-void
.end method

.method public final a(LX/54q;Landroid/content/Context;IFFF)V
    .locals 2

    .prologue
    .line 828680
    invoke-static {p2, p3, p4, p5, p6}, LX/54v;->a(Landroid/content/Context;IFFF)LX/54z;

    move-result-object v0

    .line 828681
    invoke-interface {p1}, LX/54q;->getPreventCornerOverlap()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/54z;->a(Z)V

    .line 828682
    invoke-interface {p1, v0}, LX/54q;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 828683
    invoke-direct {p0, p1}, LX/54v;->h(LX/54q;)V

    .line 828684
    return-void
.end method

.method public final b(LX/54q;)F
    .locals 3

    .prologue
    .line 828675
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    const/high16 p1, 0x40000000    # 2.0f

    .line 828676
    iget v1, v0, LX/54z;->k:F

    iget v2, v0, LX/54z;->h:F

    iget p0, v0, LX/54z;->b:I

    int-to-float p0, p0

    add-float/2addr v2, p0

    iget p0, v0, LX/54z;->k:F

    div-float/2addr p0, p1

    add-float/2addr v2, p0

    .line 828677
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, p1

    .line 828678
    iget v2, v0, LX/54z;->k:F

    iget p0, v0, LX/54z;->b:I

    int-to-float p0, p0

    add-float/2addr v2, p0

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    move v0, v1

    .line 828679
    return v0
.end method

.method public final b(LX/54q;F)V
    .locals 2

    .prologue
    .line 828671
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828672
    iget v1, v0, LX/54z;->m:F

    invoke-static {v0, v1, p2}, LX/54z;->a(LX/54z;FF)V

    .line 828673
    invoke-direct {p0, p1}, LX/54v;->h(LX/54q;)V

    .line 828674
    return-void
.end method

.method public final c(LX/54q;)F
    .locals 4

    .prologue
    .line 828667
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    const/high16 p1, 0x3fc00000    # 1.5f

    const/high16 p0, 0x40000000    # 2.0f

    .line 828668
    iget v1, v0, LX/54z;->k:F

    iget v2, v0, LX/54z;->h:F

    iget v3, v0, LX/54z;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v0, LX/54z;->k:F

    mul-float/2addr v3, p1

    div-float/2addr v3, p0

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, p0

    .line 828669
    iget v2, v0, LX/54z;->k:F

    mul-float/2addr v2, p1

    iget v3, v0, LX/54z;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    mul-float/2addr v2, p0

    add-float/2addr v1, v2

    move v0, v1

    .line 828670
    return v0
.end method

.method public final c(LX/54q;F)V
    .locals 1

    .prologue
    .line 828664
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828665
    iget p0, v0, LX/54z;->k:F

    invoke-static {v0, p2, p0}, LX/54z;->a(LX/54z;FF)V

    .line 828666
    return-void
.end method

.method public final d(LX/54q;)F
    .locals 1

    .prologue
    .line 828661
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828662
    iget p0, v0, LX/54z;->h:F

    move v0, p0

    .line 828663
    return v0
.end method

.method public final e(LX/54q;)F
    .locals 1

    .prologue
    .line 828658
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    .line 828659
    iget p0, v0, LX/54z;->m:F

    move v0, p0

    .line 828660
    return v0
.end method

.method public final f(LX/54q;)V
    .locals 0

    .prologue
    .line 828657
    return-void
.end method

.method public final g(LX/54q;)V
    .locals 2

    .prologue
    .line 828654
    invoke-static {p1}, LX/54v;->i(LX/54q;)LX/54z;

    move-result-object v0

    invoke-interface {p1}, LX/54q;->getPreventCornerOverlap()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/54z;->a(Z)V

    .line 828655
    invoke-direct {p0, p1}, LX/54v;->h(LX/54q;)V

    .line 828656
    return-void
.end method
