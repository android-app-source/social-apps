.class public final LX/5Ys;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 942479
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 942480
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 942481
    :goto_0
    return v1

    .line 942482
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 942483
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 942484
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 942485
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 942486
    const-string v7, "amount_in_hundredths"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 942487
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 942488
    :cond_1
    const-string v7, "currency"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 942489
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 942490
    :cond_2
    const-string v7, "formatted_amount"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 942491
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 942492
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 942493
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 942494
    if-eqz v0, :cond_5

    .line 942495
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 942496
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 942497
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 942498
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 942499
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 942500
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 942501
    if-eqz v0, :cond_0

    .line 942502
    const-string v1, "amount_in_hundredths"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942503
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 942504
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942505
    if-eqz v0, :cond_1

    .line 942506
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942507
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942508
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942509
    if-eqz v0, :cond_2

    .line 942510
    const-string v1, "formatted_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942511
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942512
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 942513
    return-void
.end method
