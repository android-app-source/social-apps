.class public LX/6aE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Landroid/os/Handler;

.field public final c:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/os/Handler;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1112209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112210
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, LX/6aE;->c:Landroid/view/animation/Interpolator;

    .line 1112211
    iput-object p1, p0, LX/6aE;->a:LX/0SG;

    .line 1112212
    iput-object p2, p0, LX/6aE;->b:Landroid/os/Handler;

    .line 1112213
    return-void
.end method

.method public static a(LX/IJO;)V
    .locals 6
    .param p0    # LX/IJO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1112214
    if-eqz p0, :cond_0

    .line 1112215
    iget-object v0, p0, LX/IJO;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    iget-object v1, p0, LX/IJO;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1112216
    iget-object v1, p0, LX/IJO;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v2, p0, LX/IJO;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-object v0, p0, LX/IJO;->c:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    invoke-static {v1, v2, v4, v5}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;D)V

    .line 1112217
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/6aE;
    .locals 3

    .prologue
    .line 1112218
    new-instance v2, LX/6aE;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-direct {v2, v0, v1}, LX/6aE;-><init>(LX/0SG;Landroid/os/Handler;)V

    .line 1112219
    return-object v2
.end method


# virtual methods
.method public final a(LX/6ax;Lcom/facebook/android/maps/model/LatLng;ILX/6al;LX/IJO;)V
    .locals 10
    .param p5    # LX/IJO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1112220
    iget-object v0, p0, LX/6aE;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1112221
    invoke-virtual {p1}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v8

    .line 1112222
    invoke-virtual {p4}, LX/6al;->b()LX/6ay;

    move-result-object v0

    invoke-virtual {v0}, LX/6ay;->a()LX/69F;

    move-result-object v0

    iget-object v0, v0, LX/69F;->e:LX/697;

    .line 1112223
    invoke-virtual {v0, v8}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1112224
    invoke-virtual {p1, p2}, LX/6ax;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1112225
    invoke-static {p5}, LX/6aE;->a(LX/IJO;)V

    .line 1112226
    :goto_0
    return-void

    .line 1112227
    :cond_0
    iget-object v9, p0, LX/6aE;->b:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/maps/MapMarkerPositionAnimator$1;

    move-object v1, p0

    move v4, p3

    move-object v5, p1

    move-object v6, p2

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/facebook/maps/MapMarkerPositionAnimator$1;-><init>(LX/6aE;JILX/6ax;Lcom/facebook/android/maps/model/LatLng;LX/IJO;Lcom/facebook/android/maps/model/LatLng;)V

    const v1, 0x64d5d3f7

    invoke-static {v9, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
