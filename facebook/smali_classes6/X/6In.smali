.class public final LX/6In;
.super Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;
.source ""


# instance fields
.field public final synthetic a:LX/6Iu;


# direct methods
.method public constructor <init>(LX/6Iu;)V
    .locals 0

    .prologue
    .line 1074565
    iput-object p1, p0, LX/6In;->a:LX/6Iu;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCaptureCompleted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 1

    .prologue
    .line 1074566
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    const/4 p2, 0x0

    .line 1074567
    iget-boolean p1, v0, LX/6Iu;->D:Z

    if-eqz p1, :cond_0

    .line 1074568
    const/4 p1, 0x4

    invoke-static {v0, p1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1074569
    iput-boolean p2, v0, LX/6Iu;->D:Z

    .line 1074570
    :cond_0
    iget-boolean p1, v0, LX/6Iu;->E:Z

    if-eqz p1, :cond_1

    .line 1074571
    const/4 p1, 0x5

    invoke-static {v0, p1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1074572
    iput-boolean p2, v0, LX/6Iu;->E:Z

    .line 1074573
    :cond_1
    iget-boolean p1, v0, LX/6Iu;->F:Z

    if-eqz p1, :cond_2

    .line 1074574
    const/4 p1, 0x6

    invoke-static {v0, p1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1074575
    iput-boolean p2, v0, LX/6Iu;->F:Z

    .line 1074576
    :cond_2
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    invoke-static {v0, p3}, LX/6Iu;->a$redex0(LX/6Iu;Landroid/hardware/camera2/CaptureResult;)V

    .line 1074577
    return-void
.end method

.method public final onCaptureFailed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureFailure;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1074578
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    const/4 p1, 0x0

    .line 1074579
    iget-boolean v1, v0, LX/6Iu;->D:Z

    if-eqz v1, :cond_0

    .line 1074580
    const/4 v1, 0x4

    invoke-static {v0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074581
    iput-boolean p1, v0, LX/6Iu;->D:Z

    .line 1074582
    :cond_0
    iget-boolean v1, v0, LX/6Iu;->E:Z

    if-eqz v1, :cond_1

    .line 1074583
    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074584
    iput-boolean p1, v0, LX/6Iu;->E:Z

    .line 1074585
    :cond_1
    iget-boolean v1, v0, LX/6Iu;->F:Z

    if-eqz v1, :cond_2

    .line 1074586
    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074587
    iput-boolean p1, v0, LX/6Iu;->F:Z

    .line 1074588
    :cond_2
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    const-string v1, "Capture request failed"

    invoke-static {v0, v1, v2}, LX/6Iu;->a$redex0(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1074589
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    invoke-static {v0, v2}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    .line 1074590
    return-void
.end method

.method public final onCaptureProgressed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 1

    .prologue
    .line 1074591
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    invoke-static {v0, p3}, LX/6Iu;->a$redex0(LX/6Iu;Landroid/hardware/camera2/CaptureResult;)V

    .line 1074592
    return-void
.end method

.method public final onCaptureStarted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;JJ)V
    .locals 3

    .prologue
    .line 1074593
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    .line 1074594
    iget-object v0, p0, LX/6In;->a:LX/6Iu;

    const/16 v1, 0x8

    .line 1074595
    invoke-static {v0, v1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1074596
    :cond_0
    return-void
.end method
