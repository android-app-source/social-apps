.class public LX/68L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/67g;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final d:LX/31i;


# instance fields
.field public final a:LX/68K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68K",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:[D

.field private final c:LX/31i;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1054775
    new-instance v1, LX/31i;

    move-wide v4, v2

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, LX/31i;-><init>(DDDD)V

    sput-object v1, LX/68L;->d:LX/31i;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1054776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054777
    const/4 v0, 0x2

    new-array v0, v0, [D

    iput-object v0, p0, LX/68L;->b:[D

    .line 1054778
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/68L;->c:LX/31i;

    .line 1054779
    new-instance v0, LX/68K;

    sget-object v1, LX/68L;->d:LX/31i;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/68K;-><init>(LX/31i;I)V

    .line 1054780
    iput-object v0, p0, LX/68L;->a:LX/68K;

    .line 1054781
    return-void
.end method

.method public static a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/68K",
            "<TT;>;",
            "LX/31i;",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1054782
    iget-wide v0, p2, LX/31i;->c:D

    iget-wide v2, p2, LX/31i;->d:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 1054783
    iget-object v0, p0, LX/68L;->c:LX/31i;

    invoke-virtual {v0, p2}, LX/31i;->a(LX/31i;)V

    .line 1054784
    iget-object v0, p0, LX/68L;->c:LX/31i;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, v0, LX/31i;->d:D

    .line 1054785
    iget-object v0, p0, LX/68L;->c:LX/31i;

    invoke-static {p0, p1, v0, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 1054786
    iget-object v0, p0, LX/68L;->c:LX/31i;

    invoke-virtual {v0, p2}, LX/31i;->a(LX/31i;)V

    .line 1054787
    iget-object v0, p0, LX/68L;->c:LX/31i;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, LX/31i;->c:D

    .line 1054788
    iget-object v0, p0, LX/68L;->c:LX/31i;

    invoke-static {p0, p1, v0, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 1054789
    :cond_0
    :goto_0
    return-void

    .line 1054790
    :cond_1
    iget-object v0, p1, LX/68K;->a:LX/31i;

    .line 1054791
    iget-wide v6, v0, LX/31i;->c:D

    iget-wide v8, p2, LX/31i;->d:D

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_5

    iget-wide v6, p2, LX/31i;->c:D

    iget-wide v8, v0, LX/31i;->d:D

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_5

    iget-wide v6, v0, LX/31i;->a:D

    iget-wide v8, p2, LX/31i;->b:D

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_5

    iget-wide v6, p2, LX/31i;->a:D

    iget-wide v8, v0, LX/31i;->b:D

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_5

    const/4 v6, 0x1

    :goto_1
    move v0, v6

    .line 1054792
    if-eqz v0, :cond_0

    .line 1054793
    iget-boolean v0, p1, LX/68K;->d:Z

    if-nez v0, :cond_2

    .line 1054794
    iget-object v0, p1, LX/68K;->e:LX/68K;

    invoke-static {p0, v0, p2, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 1054795
    iget-object v0, p1, LX/68K;->f:LX/68K;

    invoke-static {p0, v0, p2, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 1054796
    iget-object v0, p1, LX/68K;->g:LX/68K;

    invoke-static {p0, v0, p2, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    .line 1054797
    iget-object v0, p1, LX/68K;->h:LX/68K;

    invoke-static {p0, v0, p2, p3}, LX/68L;->a(LX/68L;LX/68K;LX/31i;Ljava/util/Collection;)V

    goto :goto_0

    .line 1054798
    :cond_2
    iget-object v0, p1, LX/68K;->a:LX/31i;

    invoke-virtual {p2, v0}, LX/31i;->e(LX/31i;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1054799
    iget-object v0, p1, LX/68K;->b:Ljava/util/ArrayList;

    invoke-interface {p3, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1054800
    :cond_3
    iget-object v0, p1, LX/68K;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67g;

    .line 1054801
    iget-object v2, p0, LX/68L;->b:[D

    invoke-interface {v0, v2}, LX/67g;->a([D)V

    .line 1054802
    iget-object v2, p0, LX/68L;->b:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    iget-object v4, p0, LX/68L;->b:[D

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    invoke-virtual {p2, v2, v3, v4, v5}, LX/31i;->a(DD)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1054803
    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static a(LX/68L;LX/68K;LX/67g;)Z
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/68K",
            "<TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1054804
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68L;->b:[D

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, LX/67g;->a([D)V

    .line 1054805
    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68L;->b:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68L;->b:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    invoke-virtual {v2, v4, v5, v6, v7}, LX/31i;->a(DD)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1054806
    const/4 v2, 0x0

    .line 1054807
    :goto_0
    return v2

    .line 1054808
    :cond_0
    invoke-static/range {p1 .. p1}, LX/68K;->b(LX/68K;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p1 .. p1}, LX/68K;->c(LX/68K;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x28

    if-lt v2, v3, :cond_1

    invoke-static/range {p1 .. p1}, LX/68K;->d(LX/68K;)I

    move-result v2

    const/16 v3, 0x14

    if-le v2, v3, :cond_2

    .line 1054809
    :cond_1
    invoke-static/range {p1 .. p1}, LX/68K;->c(LX/68K;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1054810
    const/4 v2, 0x1

    goto :goto_0

    .line 1054811
    :cond_2
    invoke-static/range {p1 .. p1}, LX/68K;->b(LX/68K;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1054812
    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v2

    invoke-virtual {v2}, LX/31i;->a()D

    move-result-wide v4

    .line 1054813
    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v2

    invoke-virtual {v2}, LX/31i;->b()D

    move-result-wide v10

    .line 1054814
    new-instance v2, LX/68K;

    new-instance v3, LX/31i;

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v6

    iget-wide v6, v6, LX/31i;->a:D

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v8

    iget-wide v8, v8, LX/31i;->d:D

    invoke-direct/range {v3 .. v11}, LX/31i;-><init>(DDDD)V

    invoke-static/range {p1 .. p1}, LX/68K;->d(LX/68K;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-direct {v2, v3, v6}, LX/68K;-><init>(LX/31i;I)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/68K;->a(LX/68K;LX/68K;)LX/68K;

    .line 1054815
    new-instance v2, LX/68K;

    new-instance v7, LX/31i;

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v8, v3, LX/31i;->c:D

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v14, v3, LX/31i;->b:D

    move-wide v12, v4

    invoke-direct/range {v7 .. v15}, LX/31i;-><init>(DDDD)V

    invoke-static/range {p1 .. p1}, LX/68K;->d(LX/68K;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v7, v3}, LX/68K;-><init>(LX/31i;I)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/68K;->b(LX/68K;LX/68K;)LX/68K;

    .line 1054816
    new-instance v2, LX/68K;

    new-instance v13, LX/31i;

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v14, v3, LX/31i;->c:D

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v0, v3, LX/31i;->a:D

    move-wide/from16 v16, v0

    move-wide/from16 v18, v4

    move-wide/from16 v20, v10

    invoke-direct/range {v13 .. v21}, LX/31i;-><init>(DDDD)V

    invoke-static/range {p1 .. p1}, LX/68K;->d(LX/68K;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v13, v3}, LX/68K;-><init>(LX/31i;I)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/68K;->c(LX/68K;LX/68K;)LX/68K;

    .line 1054817
    new-instance v2, LX/68K;

    new-instance v7, LX/31i;

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v12, v3, LX/31i;->d:D

    invoke-static/range {p1 .. p1}, LX/68K;->a(LX/68K;)LX/31i;

    move-result-object v3

    iget-wide v14, v3, LX/31i;->b:D

    move-wide v8, v4

    invoke-direct/range {v7 .. v15}, LX/31i;-><init>(DDDD)V

    invoke-static/range {p1 .. p1}, LX/68K;->d(LX/68K;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v7, v3}, LX/68K;-><init>(LX/31i;I)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/68K;->d(LX/68K;LX/68K;)LX/68K;

    .line 1054818
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/68K;->a(LX/68K;Z)Z

    .line 1054819
    const/4 v2, 0x0

    invoke-static/range {p1 .. p1}, LX/68K;->c(LX/68K;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 1054820
    invoke-static/range {p1 .. p1}, LX/68K;->c(LX/68K;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/67g;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, LX/68L;->b(LX/68K;LX/67g;)Z

    .line 1054821
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1054822
    :cond_3
    invoke-static/range {p1 .. p1}, LX/68K;->c(LX/68K;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1054823
    :cond_4
    invoke-direct/range {p0 .. p2}, LX/68L;->b(LX/68K;LX/67g;)Z

    move-result v2

    goto/16 :goto_0
.end method

.method private b(LX/68K;LX/67g;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/68K",
            "<TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1054824
    iget-object v0, p1, LX/68K;->e:LX/68K;

    invoke-static {p0, v0, p2}, LX/68L;->a(LX/68L;LX/68K;LX/67g;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/68K;->f:LX/68K;

    invoke-static {p0, v0, p2}, LX/68L;->a(LX/68L;LX/68K;LX/67g;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/68K;->g:LX/68K;

    invoke-static {p0, v0, p2}, LX/68L;->a(LX/68L;LX/68K;LX/67g;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/68K;->h:LX/68K;

    invoke-static {p0, v0, p2}, LX/68L;->a(LX/68L;LX/68K;LX/67g;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
