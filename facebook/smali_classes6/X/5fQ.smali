.class public LX/5fQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/5fQ;


# instance fields
.field public A:Z

.field public B:LX/5fl;

.field public C:Ljava/lang/String;

.field public D:Z

.field public c:I

.field public d:Landroid/hardware/Camera;

.field public e:Landroid/graphics/SurfaceTexture;

.field public f:I

.field public g:I

.field public h:I

.field public i:LX/5fM;

.field public j:LX/5fO;

.field public k:LX/5fO;

.field public volatile l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:LX/ALQ;

.field public q:LX/ALR;

.field public r:LX/5fY;

.field public s:LX/5fP;

.field public t:LX/ALT;

.field public u:Z

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/Runnable;

.field public final x:Ljava/lang/Object;

.field private final y:LX/5fi;

.field public z:Landroid/media/MediaRecorder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 971344
    const-class v0, LX/5fQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/5fQ;->a:Ljava/lang/String;

    .line 971345
    new-instance v0, LX/5fQ;

    invoke-direct {v0}, LX/5fQ;-><init>()V

    sput-object v0, LX/5fQ;->b:LX/5fQ;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 971346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 971347
    iput-object v1, p0, LX/5fQ;->p:LX/ALQ;

    .line 971348
    iput-object v1, p0, LX/5fQ;->q:LX/ALR;

    .line 971349
    iput-object v1, p0, LX/5fQ;->r:LX/5fY;

    .line 971350
    iput-object v1, p0, LX/5fQ;->t:LX/ALT;

    .line 971351
    iput-object v1, p0, LX/5fQ;->w:Ljava/lang/Runnable;

    .line 971352
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/5fQ;->x:Ljava/lang/Object;

    .line 971353
    new-instance v0, LX/5fj;

    invoke-direct {v0}, LX/5fj;-><init>()V

    iput-object v0, p0, LX/5fQ;->y:LX/5fi;

    .line 971354
    iput-object v1, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    .line 971355
    return-void
.end method

.method public static A(LX/5fQ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 971356
    iget-object v0, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 971357
    iget-object v0, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 971358
    iget-object v0, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 971359
    iget-object v0, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 971360
    const/4 v0, 0x0

    iput-object v0, p0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    .line 971361
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    .line 971362
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->lock()V

    .line 971363
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971364
    const-string v1, "off"

    invoke-virtual {v0, v1}, LX/5fS;->a(Ljava/lang/String;)V

    .line 971365
    invoke-static {p0, v2}, LX/5fQ;->c(LX/5fQ;Z)V

    .line 971366
    :cond_1
    iput-boolean v2, p0, LX/5fQ;->A:Z

    .line 971367
    return-void
.end method

.method public static a$redex0(LX/5fQ;LX/5fO;LX/5fO;IILX/5fi;)V
    .locals 2

    .prologue
    .line 971368
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 971369
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Set sizes failed, camera not yet initialised"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 971370
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971371
    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p2, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p1, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 971372
    invoke-interface {p5, v0, p1, p2}, LX/5fi;->a(LX/5fS;LX/5fO;LX/5fO;)V

    .line 971373
    :goto_0
    return-void

    .line 971374
    :cond_1
    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p2, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p1, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 971375
    invoke-interface {p5, v0}, LX/5fi;->a(LX/5fS;)V

    goto :goto_0

    .line 971376
    :cond_2
    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p2, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, LX/5fO;->DEACTIVATED:LX/5fO;

    invoke-virtual {p1, v1}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 971377
    invoke-interface {p5, v0}, LX/5fi;->b(LX/5fS;)V

    goto :goto_0

    .line 971378
    :cond_3
    invoke-interface {p5, v0, p3, p4}, LX/5fi;->a(LX/5fS;II)V

    goto :goto_0
.end method

.method public static b(LX/5fM;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 971332
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    .line 971333
    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    move v0, v1

    .line 971334
    :goto_0
    if-ge v0, v2, :cond_1

    .line 971335
    invoke-static {v0, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 971336
    iget v4, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    invoke-virtual {p0}, LX/5fM;->getInfoId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 971337
    :goto_1
    return v0

    .line 971338
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 971339
    goto :goto_1
.end method

.method public static c(LX/5fQ;Z)V
    .locals 3

    .prologue
    .line 971379
    iget-object v1, p0, LX/5fQ;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 971380
    :try_start_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v2, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v2}, LX/5fQ;->b(LX/5fM;)I

    move-result v2

    invoke-static {v0, v2}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971381
    invoke-virtual {v0, p1}, LX/5fS;->a(Z)V

    .line 971382
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(LX/5fQ;LX/5fM;)Z
    .locals 1

    .prologue
    .line 971383
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5fQ;->i:LX/5fM;

    if-eq v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(II)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 971384
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 971385
    invoke-static {p1, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 971386
    packed-switch p0, :pswitch_data_0

    .line 971387
    :goto_0
    :pswitch_0
    iget v2, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 971388
    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 971389
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 971390
    :goto_1
    return v0

    .line 971391
    :pswitch_1
    const/16 v0, 0x5a

    .line 971392
    goto :goto_0

    .line 971393
    :pswitch_2
    const/16 v0, 0xb4

    .line 971394
    goto :goto_0

    .line 971395
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 971396
    :cond_0
    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int v0, v1, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static e$redex0(LX/5fQ;I)I
    .locals 4

    .prologue
    .line 971397
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 971398
    const/4 v0, 0x0

    .line 971399
    :goto_0
    return v0

    .line 971400
    :cond_0
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 971401
    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 971402
    add-int/lit8 v1, p1, 0x2d

    div-int/lit8 v1, v1, 0x5a

    mul-int/lit8 v1, v1, 0x5a

    .line 971403
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 971404
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 971405
    :cond_1
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0
.end method

.method public static x(LX/5fQ;)V
    .locals 2

    .prologue
    .line 971406
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 971407
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5fQ;->l:Z

    .line 971408
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    .line 971409
    const/4 v1, 0x0

    iput-object v1, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    .line 971410
    const v1, 0x11613e94

    invoke-static {v0, v1}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 971411
    invoke-direct {p0}, LX/5fQ;->z()V

    .line 971412
    const v1, 0x2d04a1af

    invoke-static {v0, v1}, LX/0J2;->a(Landroid/hardware/Camera;I)V

    .line 971413
    :cond_0
    return-void
.end method

.method public static y(LX/5fQ;)V
    .locals 1

    .prologue
    .line 971437
    iget-object v0, p0, LX/5fQ;->p:LX/ALQ;

    if-eqz v0, :cond_0

    .line 971438
    new-instance v0, Lcom/facebook/optic/CameraDevice$5;

    invoke-direct {v0, p0}, Lcom/facebook/optic/CameraDevice$5;-><init>(LX/5fQ;)V

    invoke-static {v0}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 971439
    :cond_0
    return-void
.end method

.method private z()V
    .locals 1

    .prologue
    .line 971414
    iget-object v0, p0, LX/5fQ;->q:LX/ALR;

    if-eqz v0, :cond_0

    .line 971415
    new-instance v0, Lcom/facebook/optic/CameraDevice$6;

    invoke-direct {v0, p0}, Lcom/facebook/optic/CameraDevice$6;-><init>(LX/5fQ;)V

    invoke-static {v0}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 971416
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 971417
    iget-boolean v0, p0, LX/5fQ;->o:Z

    if-nez v0, :cond_0

    .line 971418
    iput p1, p0, LX/5fQ;->c:I

    .line 971419
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/16 v2, -0x1e

    const/16 v1, -0x3e8

    const/16 v3, 0x3e8

    .line 971420
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971421
    :goto_0
    return-void

    .line 971422
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1, p2, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 971423
    invoke-virtual {v0, v2, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 971424
    invoke-virtual {v0, v1, v1, v3, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    .line 971425
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 971426
    new-instance v2, Landroid/hardware/Camera$Area;

    invoke-direct {v2, v0, v3}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 971427
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v2, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v2}, LX/5fQ;->b(LX/5fM;)I

    move-result v2

    invoke-static {v0, v2}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971428
    invoke-virtual {v0, v1}, LX/5fS;->b(Ljava/util/List;)V

    .line 971429
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/5fQ;->c(LX/5fQ;Z)V

    goto :goto_0
.end method

.method public final a(ILX/5f5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 971430
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5f6;

    invoke-direct {v1, p0, p1}, LX/5f6;-><init>(LX/5fQ;I)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971431
    invoke-static {v0, p2}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    .line 971432
    return-void
.end method

.method public final a(LX/5f5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 971433
    invoke-static {}, LX/5fo;->a()V

    .line 971434
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5fG;

    invoke-direct {v1, p0}, LX/5fG;-><init>(LX/5fQ;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971435
    invoke-static {v0, p1}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    .line 971436
    return-void
.end method

.method public final a(LX/5f5;LX/5f5;LX/5fi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fl;",
            ">;",
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "LX/5fi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 971440
    iget-boolean v0, p0, LX/5fQ;->A:Z

    move v0, v0

    .line 971441
    if-nez v0, :cond_1

    .line 971442
    if-eqz p1, :cond_0

    .line 971443
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not recording video"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, LX/5f5;->a(Ljava/lang/Exception;)V

    .line 971444
    :cond_0
    :goto_0
    return-void

    .line 971445
    :cond_1
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5fC;

    invoke-direct {v1, p0}, LX/5fC;-><init>(LX/5fQ;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971446
    new-instance v1, LX/5fD;

    invoke-direct {v1, p0, p2}, LX/5fD;-><init>(LX/5fQ;LX/5f5;)V

    .line 971447
    new-instance v2, LX/5fE;

    invoke-direct {v2, p0, p1, v1, p3}, LX/5fE;-><init>(LX/5fQ;LX/5f5;LX/5f5;LX/5fi;)V

    invoke-static {v0, v2}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    goto :goto_0
.end method

.method public final a(LX/5f5;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "LX/5fl;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 971448
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971449
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t record video before it\'s initialised."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, LX/5f5;->a(Ljava/lang/Exception;)V

    .line 971450
    :goto_0
    return-void

    .line 971451
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5fQ;->A:Z

    .line 971452
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5fA;

    invoke-direct {v1, p0, p2}, LX/5fA;-><init>(LX/5fQ;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971453
    new-instance v1, LX/5fB;

    invoke-direct {v1, p0, p1}, LX/5fB;-><init>(LX/5fQ;LX/5f5;)V

    invoke-static {v0, v1}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    goto :goto_0
.end method

.method public final a(LX/5fM;LX/5f5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5fM;",
            "LX/5f5",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 971454
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5fF;

    invoke-direct {v1, p0, p1}, LX/5fF;-><init>(LX/5fQ;LX/5fM;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971455
    invoke-static {v0, p2}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    .line 971456
    return-void
.end method

.method public final a(LX/6IY;)V
    .locals 2

    .prologue
    .line 971340
    if-nez p1, :cond_0

    .line 971341
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 971342
    :goto_0
    return-void

    .line 971343
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    new-instance v1, LX/5fJ;

    invoke-direct {v1, p0, p1}, LX/5fJ;-><init>(LX/5fQ;LX/6IY;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/SurfaceTexture;LX/5fM;IIILX/5fO;LX/5fO;LX/5fi;LX/5f5;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/SurfaceTexture;",
            "LX/5fM;",
            "III",
            "LX/5fO;",
            "LX/5fO;",
            "LX/5fi;",
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 971457
    new-instance v11, Ljava/util/concurrent/FutureTask;

    new-instance v1, LX/5fH;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/5fH;-><init>(LX/5fQ;LX/5fM;Landroid/graphics/SurfaceTexture;ILX/5fO;LX/5fO;IILX/5fi;)V

    invoke-direct {v11, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 971458
    invoke-static {p0, p2}, LX/5fQ;->c(LX/5fQ;LX/5fM;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 971459
    new-instance v1, LX/5fI;

    move-object/from16 v0, p9

    invoke-direct {v1, p0, v11, v0}, LX/5fI;-><init>(LX/5fQ;Ljava/util/concurrent/FutureTask;LX/5f5;)V

    invoke-virtual {p0, p2, v1}, LX/5fQ;->a(LX/5fM;LX/5f5;)V

    .line 971460
    :goto_0
    return-void

    .line 971461
    :cond_0
    move-object/from16 v0, p9

    invoke-static {v11, v0}, LX/5fo;->a(Ljava/util/concurrent/FutureTask;LX/5f5;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 971250
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971251
    new-instance v0, LX/5fN;

    const-string v1, "Failed to set flash mode."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971252
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5fS;->a(Ljava/lang/String;)V

    .line 971253
    return-void
.end method

.method public final b()LX/5fS;
    .locals 2

    .prologue
    .line 971254
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 971255
    const/4 v0, 0x0

    .line 971256
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v2, -0x1e

    const/16 v1, -0x3e8

    const/16 v3, 0x3e8

    .line 971257
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971258
    :goto_0
    return-void

    .line 971259
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1, p2, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 971260
    invoke-virtual {v0, v2, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 971261
    invoke-virtual {v0, v1, v1, v3, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    .line 971262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 971263
    new-instance v2, Landroid/hardware/Camera$Area;

    invoke-direct {v2, v0, v3}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 971264
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v2, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v2}, LX/5fQ;->b(LX/5fM;)I

    move-result v2

    invoke-static {v0, v2}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971265
    invoke-virtual {v0, v1}, LX/5fS;->a(Ljava/util/List;)V

    .line 971266
    iget-boolean v1, p0, LX/5fQ;->u:Z

    if-nez v1, :cond_1

    .line 971267
    invoke-virtual {v0}, LX/5fS;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/5fQ;->v:Ljava/lang/String;

    .line 971268
    :cond_1
    const-string v1, "auto"

    invoke-virtual {v0, v1}, LX/5fS;->b(Ljava/lang/String;)V

    .line 971269
    invoke-static {p0, v4}, LX/5fQ;->c(LX/5fQ;Z)V

    .line 971270
    iget-object v1, p0, LX/5fQ;->r:LX/5fY;

    if-eqz v1, :cond_2

    .line 971271
    iget-object v1, p0, LX/5fQ;->r:LX/5fY;

    sget-object v2, LX/5fd;->CANCELLED:LX/5fd;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    .line 971272
    iget-object v1, p0, LX/5fQ;->r:LX/5fY;

    sget-object v2, LX/5fd;->FOCUSSING:LX/5fd;

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v1, v2, v3}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    .line 971273
    :cond_2
    iget-object v1, p0, LX/5fQ;->w:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    .line 971274
    iget-object v1, p0, LX/5fQ;->w:Ljava/lang/Runnable;

    .line 971275
    sget-object v2, LX/5fo;->e:Landroid/os/Handler;

    invoke-static {v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 971276
    :cond_3
    iget-boolean v1, p0, LX/5fQ;->u:Z

    if-eqz v1, :cond_4

    .line 971277
    iget-object v1, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 971278
    iput-boolean v4, p0, LX/5fQ;->u:Z

    .line 971279
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/5fQ;->u:Z

    .line 971280
    iget-object v1, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    new-instance v2, LX/5fK;

    invoke-direct {v2, p0, p1, p2, v0}, LX/5fK;-><init>(LX/5fQ;IILX/5fS;)V

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    goto :goto_0
.end method

.method public final b(LX/5f5;LX/5fi;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "LX/5fi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 971281
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5fQ;->l:Z

    .line 971282
    invoke-virtual {p0}, LX/5fQ;->j()V

    .line 971283
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 971284
    iget-object v1, p0, LX/5fQ;->e:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, LX/5fQ;->i:LX/5fM;

    iget v3, p0, LX/5fQ;->f:I

    iget v4, p0, LX/5fQ;->g:I

    iget v5, p0, LX/5fQ;->h:I

    iget-object v6, p0, LX/5fQ;->k:LX/5fO;

    iget-object v7, p0, LX/5fQ;->j:LX/5fO;

    move-object v0, p0

    move-object v8, p2

    move-object v9, p1

    invoke-virtual/range {v0 .. v9}, LX/5fQ;->a(Landroid/graphics/SurfaceTexture;LX/5fM;IIILX/5fO;LX/5fO;LX/5fi;LX/5f5;)V

    .line 971285
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 971286
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971287
    new-instance v0, LX/5fN;

    const-string v1, "Failed to toggle HDR mode."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971288
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5fS;->b(Z)V

    .line 971289
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 971290
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971291
    new-instance v0, LX/5fN;

    const-string v1, "Failed to set zoom level"

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971292
    :cond_0
    iget-object v0, p0, LX/5fQ;->s:LX/5fP;

    invoke-virtual {v0, p1}, LX/5fP;->a(I)V

    .line 971293
    return-void
.end method

.method public final clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 971294
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 971295
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 971296
    iget v0, p0, LX/5fQ;->f:I

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fQ;->d(II)I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 971297
    invoke-virtual {p0}, LX/5fQ;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 971298
    iget-boolean v0, p0, LX/5fQ;->m:Z

    move v0, v0

    .line 971299
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 971300
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/5fQ;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 971301
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 971302
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    const v1, -0xc4c1371

    invoke-static {v0, v1}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 971303
    invoke-direct {p0}, LX/5fQ;->z()V

    .line 971304
    :cond_0
    return-void
.end method

.method public final n()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 971305
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971306
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get preview rect."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971307
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->k()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final o()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 971308
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971309
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get picture rect."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971310
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->l()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 971311
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971312
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get supported flash modes."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971313
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 971314
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971315
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get flash mode."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971316
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 971317
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971318
    new-instance v0, LX/5fN;

    const-string v1, "Failed to detect auto-focus support."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971319
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->g()Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 971320
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971321
    new-instance v0, LX/5fN;

    const-string v1, "Failed to detect spot metering support."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971322
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->h()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 971323
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971324
    new-instance v0, LX/5fN;

    const-string v1, "Failed to detect zoom support."

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971325
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->m()Z

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 2

    .prologue
    .line 971326
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971327
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get current zoom level"

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971328
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->o()I

    move-result v0

    return v0
.end method

.method public final v()I
    .locals 2

    .prologue
    .line 971329
    invoke-virtual {p0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971330
    new-instance v0, LX/5fN;

    const-string v1, "Failed to get the maximum zoom level"

    invoke-direct {v0, p0, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971331
    :cond_0
    iget-object v0, p0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->p()I

    move-result v0

    return v0
.end method
