.class public final LX/5Lt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 903214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 903410
    if-nez p1, :cond_0

    .line 903411
    :goto_0
    return v0

    .line 903412
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 903413
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 903414
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 903415
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    const/4 v5, 0x0

    .line 903416
    if-nez v4, :cond_1

    .line 903417
    :goto_1
    move v4, v5

    .line 903418
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    const/4 v6, 0x0

    .line 903419
    if-nez v5, :cond_2

    .line 903420
    :goto_2
    move v5, v6

    .line 903421
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 903422
    if-nez v6, :cond_3

    .line 903423
    :goto_3
    move v6, v7

    .line 903424
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p0, v7}, LX/5Lt;->c(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v7

    .line 903425
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 903426
    const/16 v9, 0x9

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 903427
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 903428
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 903429
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->A()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 903430
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 903431
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 903432
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 903433
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 903434
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 903435
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 903436
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 903437
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 903438
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->c(Ljava/util/List;)I

    move-result v6

    .line 903439
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 903440
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 903441
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 903442
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_1

    .line 903443
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 903444
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 903445
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v8

    invoke-virtual {p0, v6, v8}, LX/186;->a(IZ)V

    .line 903446
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 903447
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 903448
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 903449
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 903450
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 903451
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 903452
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 903453
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I
    .locals 14

    .prologue
    .line 903353
    if-nez p1, :cond_0

    .line 903354
    const/4 v0, 0x0

    .line 903355
    :goto_0
    return v0

    .line 903356
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v0

    const/4 v1, 0x0

    .line 903357
    if-nez v0, :cond_1

    .line 903358
    :goto_1
    move v0, v1

    .line 903359
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    const/4 v2, 0x0

    .line 903360
    if-nez v1, :cond_2

    .line 903361
    :goto_2
    move v1, v2

    .line 903362
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 903363
    if-nez v2, :cond_3

    .line 903364
    :goto_3
    move v2, v3

    .line 903365
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 903366
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 903367
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 903368
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v6

    invoke-static {p0, v6}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v6

    .line 903369
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    invoke-static {p0, v7}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v7

    .line 903370
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v8

    invoke-static {p0, v8}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v8

    .line 903371
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v9

    invoke-static {p0, v9}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v9

    .line 903372
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v10

    invoke-static {p0, v10}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v10

    .line 903373
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v11

    invoke-static {p0, v11}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v11

    .line 903374
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 903375
    const/16 v13, 0x12

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 903376
    const/4 v13, 0x0

    invoke-virtual {p0, v13, v0}, LX/186;->b(II)V

    .line 903377
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 903378
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 903379
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 903380
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 903381
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 903382
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/186;->a(III)V

    .line 903383
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 903384
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 903385
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 903386
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 903387
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 903388
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 903389
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 903390
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v12}, LX/186;->b(II)V

    .line 903391
    const/16 v0, 0xf

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 903392
    const/16 v0, 0x10

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 903393
    const/16 v0, 0x11

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 903394
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 903395
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 903396
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 903397
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2, v1}, LX/186;->a(III)V

    .line 903398
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 903399
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 903400
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 903401
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 903402
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 903403
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 903404
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 903405
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 903406
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 903407
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 903408
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 903409
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 903329
    if-nez p1, :cond_0

    .line 903330
    :goto_0
    return v2

    .line 903331
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 903332
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v4

    .line 903333
    if-eqz v4, :cond_2

    .line 903334
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 903335
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 903336
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    const/4 v7, 0x0

    .line 903337
    if-nez v0, :cond_3

    .line 903338
    :goto_2
    move v0, v7

    .line 903339
    aput v0, v5, v1

    .line 903340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 903341
    :cond_1
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 903342
    :goto_3
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 903343
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 903344
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 903345
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 903346
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 903347
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 903348
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 903349
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->a()I

    move-result p1

    invoke-virtual {p0, v7, p1, v7}, LX/186;->a(III)V

    .line 903350
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 903351
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 903352
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;)Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 903248
    if-nez p0, :cond_1

    .line 903249
    :cond_0
    :goto_0
    return-object v2

    .line 903250
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 903251
    const/4 v13, 0x1

    const/4 v4, 0x0

    .line 903252
    if-nez p0, :cond_3

    .line 903253
    :goto_1
    move v1, v4

    .line 903254
    if-eqz v1, :cond_0

    .line 903255
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 903256
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 903257
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 903258
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 903259
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 903260
    const-string v1, "MinutiaeModelConversionHelper.getTaggableObjectEdge"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903261
    :cond_2
    new-instance v2, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-direct {v2, v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 903262
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->a()Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;

    move-result-object v1

    const/4 v3, 0x0

    .line 903263
    if-nez v1, :cond_6

    .line 903264
    :goto_2
    move v5, v3

    .line 903265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 903266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->k()LX/0Px;

    move-result-object v7

    .line 903267
    if-eqz v7, :cond_5

    .line 903268
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v1

    new-array v8, v1, [I

    move v3, v4

    .line 903269
    :goto_3
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 903270
    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    const/4 v9, 0x0

    .line 903271
    if-nez v1, :cond_7

    .line 903272
    :goto_4
    move v1, v9

    .line 903273
    aput v1, v8, v3

    .line 903274
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 903275
    :cond_4
    invoke-virtual {v0, v8, v13}, LX/186;->a([IZ)I

    move-result v1

    .line 903276
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 903277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v7

    const/4 v8, 0x0

    .line 903278
    if-nez v7, :cond_8

    .line 903279
    :goto_6
    move v7, v8

    .line 903280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {v0, v8}, LX/5Lt;->c(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 903281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    invoke-static {v0, v9}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I

    move-result v9

    .line 903282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    const/4 v11, 0x0

    .line 903283
    if-nez v10, :cond_9

    .line 903284
    :goto_7
    move v10, v11

    .line 903285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->r()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 903286
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 903287
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 903288
    invoke-virtual {v0, v13, v6}, LX/186;->b(II)V

    .line 903289
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 903290
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 903291
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 903292
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 903293
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 903294
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;->p()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 903295
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 903296
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 903297
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 903298
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_5
    move v1, v4

    goto :goto_5

    .line 903299
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 903300
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 903301
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->k()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 903302
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceFlowInfo;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 903303
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 903304
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 903305
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 903306
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 903307
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 903308
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    .line 903309
    invoke-virtual {v0, v3}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 903310
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 903311
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-static {v0, v11}, LX/5Lt;->c(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v11

    .line 903312
    const/4 v12, 0x2

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 903313
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 903314
    const/4 v9, 0x1

    invoke-virtual {v0, v9, v11}, LX/186;->b(II)V

    .line 903315
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 903316
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_4

    .line 903317
    :cond_8
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 903318
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 903319
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 903320
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 903321
    const/4 v8, 0x1

    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 903322
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 903323
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto/16 :goto_6

    .line 903324
    :cond_9
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 903325
    const/4 v14, 0x1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 903326
    invoke-virtual {v0, v11, v12}, LX/186;->b(II)V

    .line 903327
    invoke-virtual {v0}, LX/186;->d()I

    move-result v11

    .line 903328
    invoke-virtual {v0, v11}, LX/186;->d(I)V

    goto/16 :goto_7
.end method

.method public static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 6

    .prologue
    .line 903224
    if-nez p0, :cond_0

    .line 903225
    const/4 v0, 0x0

    .line 903226
    :goto_0
    return-object v0

    .line 903227
    :cond_0
    new-instance v2, LX/4Z6;

    invoke-direct {v2}, LX/4Z6;-><init>()V

    .line 903228
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 903229
    iput-object v0, v2, LX/4Z6;->b:Ljava/lang/String;

    .line 903230
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 903231
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 903232
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 903233
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 903234
    if-nez v0, :cond_3

    .line 903235
    const/4 v4, 0x0

    .line 903236
    :goto_2
    move-object v0, v4

    .line 903237
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 903238
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 903239
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 903240
    iput-object v0, v2, LX/4Z6;->c:LX/0Px;

    .line 903241
    :cond_2
    invoke-virtual {v2}, LX/4Z6;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    goto :goto_0

    .line 903242
    :cond_3
    new-instance v4, LX/4Vm;

    invoke-direct {v4}, LX/4Vm;-><init>()V

    .line 903243
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    .line 903244
    iput v5, v4, LX/4Vm;->b:I

    .line 903245
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v5

    .line 903246
    iput-object v5, v4, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 903247
    invoke-virtual {v4}, LX/4Vm;->a()Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    move-result-object v4

    goto :goto_2
.end method

.method public static c(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 903215
    if-nez p1, :cond_0

    .line 903216
    :goto_0
    return v0

    .line 903217
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 903218
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 903219
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 903220
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 903221
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 903222
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 903223
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
