.class public final enum LX/5fd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5fd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5fd;

.field public static final enum CANCELLED:LX/5fd;

.field public static final enum FAILED:LX/5fd;

.field public static final enum FOCUSSING:LX/5fd;

.field public static final enum SUCCESS:LX/5fd;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 971784
    new-instance v0, LX/5fd;

    const-string v1, "FOCUSSING"

    invoke-direct {v0, v1, v2}, LX/5fd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5fd;->FOCUSSING:LX/5fd;

    new-instance v0, LX/5fd;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v3}, LX/5fd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5fd;->CANCELLED:LX/5fd;

    new-instance v0, LX/5fd;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, LX/5fd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5fd;->SUCCESS:LX/5fd;

    new-instance v0, LX/5fd;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/5fd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5fd;->FAILED:LX/5fd;

    .line 971785
    const/4 v0, 0x4

    new-array v0, v0, [LX/5fd;

    sget-object v1, LX/5fd;->FOCUSSING:LX/5fd;

    aput-object v1, v0, v2

    sget-object v1, LX/5fd;->CANCELLED:LX/5fd;

    aput-object v1, v0, v3

    sget-object v1, LX/5fd;->SUCCESS:LX/5fd;

    aput-object v1, v0, v4

    sget-object v1, LX/5fd;->FAILED:LX/5fd;

    aput-object v1, v0, v5

    sput-object v0, LX/5fd;->$VALUES:[LX/5fd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 971783
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5fd;
    .locals 1

    .prologue
    .line 971781
    const-class v0, LX/5fd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5fd;

    return-object v0
.end method

.method public static values()[LX/5fd;
    .locals 1

    .prologue
    .line 971782
    sget-object v0, LX/5fd;->$VALUES:[LX/5fd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5fd;

    return-object v0
.end method
