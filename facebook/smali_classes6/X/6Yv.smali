.class public final LX/6Yv;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View$OnClickListener;

.field public final synthetic b:I

.field public final synthetic c:LX/6Yw;


# direct methods
.method public constructor <init>(LX/6Yw;Landroid/view/View$OnClickListener;I)V
    .locals 0

    .prologue
    .line 1110708
    iput-object p1, p0, LX/6Yv;->c:LX/6Yw;

    iput-object p2, p0, LX/6Yv;->a:Landroid/view/View$OnClickListener;

    iput p3, p0, LX/6Yv;->b:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1110706
    iget-object v0, p0, LX/6Yv;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1110707
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1110702
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1110703
    iget v0, p0, LX/6Yv;->b:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1110704
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1110705
    return-void
.end method
