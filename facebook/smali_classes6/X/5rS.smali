.class public LX/5rS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1011779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x2000

    .line 1011780
    invoke-static {}, LX/5rR;->c()Ljava/util/Map;

    move-result-object v1

    .line 1011781
    invoke-static {}, LX/5rR;->a()Ljava/util/Map;

    move-result-object v2

    .line 1011782
    invoke-static {}, LX/5rR;->b()Ljava/util/Map;

    move-result-object v3

    .line 1011783
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ViewManager;

    .line 1011784
    const-string v5, "constants for ViewManager"

    invoke-static {v8, v9, v5}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v5

    const-string v6, "ViewManager"

    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v5

    invoke-virtual {v5}, LX/0BN;->a()V

    .line 1011785
    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->w()Ljava/util/Map;

    move-result-object v5

    .line 1011786
    if-eqz v5, :cond_0

    .line 1011787
    invoke-static {v2, v5}, LX/5rS;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 1011788
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->x()Ljava/util/Map;

    move-result-object v5

    .line 1011789
    if-eqz v5, :cond_1

    .line 1011790
    invoke-static {v3, v5}, LX/5rS;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 1011791
    :cond_1
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v5

    .line 1011792
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->y()Ljava/util/Map;

    move-result-object v6

    .line 1011793
    if-eqz v6, :cond_2

    .line 1011794
    const-string v7, "Constants"

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011795
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->v()Ljava/util/Map;

    move-result-object v6

    .line 1011796
    if-eqz v6, :cond_3

    .line 1011797
    const-string v7, "Commands"

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011798
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->z()Ljava/util/Map;

    move-result-object v6

    .line 1011799
    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1011800
    const-string v7, "NativeProps"

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011801
    :cond_4
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 1011802
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ViewManager;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011803
    :cond_5
    invoke-static {v8, v9}, LX/018;->a(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    throw v0

    .line 1011804
    :cond_6
    const-string v0, "customBubblingEventTypes"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011805
    const-string v0, "customDirectEventTypes"

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011806
    return-object v1
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 5

    .prologue
    .line 1011807
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1011808
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1011809
    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1011810
    if-eqz v0, :cond_0

    instance-of v4, v1, Ljava/util/Map;

    if-eqz v4, :cond_0

    instance-of v4, v0, Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 1011811
    check-cast v0, Ljava/util/Map;

    check-cast v1, Ljava/util/Map;

    invoke-static {v0, v1}, LX/5rS;->a(Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_0

    .line 1011812
    :cond_0
    invoke-interface {p0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1011813
    :cond_1
    return-void
.end method
