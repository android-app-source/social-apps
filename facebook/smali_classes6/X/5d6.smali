.class public final LX/5d6;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupThreadSettingsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 964574
    const-class v1, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupThreadSettingsModel;

    const v0, 0x79d6d30

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "ChangeGroupThreadSettings"

    const-string v6, "b4727d1a15bda73cbf8bfe71cb322605"

    const-string v7, "messenger_group_thread_settings_change"

    const-string v8, "0"

    const-string v9, "10155069963071729"

    const/4 v10, 0x0

    .line 964575
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 964576
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 964577
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 964578
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 964579
    packed-switch v0, :pswitch_data_0

    .line 964580
    :goto_0
    return-object p1

    .line 964581
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5fb57ca
        :pswitch_0
    .end packed-switch
.end method
