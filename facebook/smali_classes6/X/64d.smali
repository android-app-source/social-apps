.class public final LX/64d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:[Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Z


# direct methods
.method public constructor <init>(LX/64e;)V
    .locals 1

    .prologue
    .line 1045025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045026
    iget-boolean v0, p1, LX/64e;->e:Z

    iput-boolean v0, p0, LX/64d;->a:Z

    .line 1045027
    iget-object v0, p1, LX/64e;->g:[Ljava/lang/String;

    iput-object v0, p0, LX/64d;->b:[Ljava/lang/String;

    .line 1045028
    iget-object v0, p1, LX/64e;->h:[Ljava/lang/String;

    iput-object v0, p0, LX/64d;->c:[Ljava/lang/String;

    .line 1045029
    iget-boolean v0, p1, LX/64e;->f:Z

    iput-boolean v0, p0, LX/64d;->d:Z

    .line 1045030
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1045031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045032
    iput-boolean p1, p0, LX/64d;->a:Z

    .line 1045033
    return-void
.end method


# virtual methods
.method public final a(Z)LX/64d;
    .locals 2

    .prologue
    .line 1045034
    iget-boolean v0, p0, LX/64d;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no TLS extensions for cleartext connections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045035
    :cond_0
    iput-boolean p1, p0, LX/64d;->d:Z

    .line 1045036
    return-object p0
.end method

.method public final varargs a([LX/64b;)LX/64d;
    .locals 3

    .prologue
    .line 1045037
    iget-boolean v0, p0, LX/64d;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no cipher suites for cleartext connections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045038
    :cond_0
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 1045039
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 1045040
    aget-object v2, p1, v0

    iget-object v2, v2, LX/64b;->aS:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 1045041
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1045042
    :cond_1
    invoke-virtual {p0, v1}, LX/64d;->a([Ljava/lang/String;)LX/64d;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([LX/658;)LX/64d;
    .locals 3

    .prologue
    .line 1045043
    iget-boolean v0, p0, LX/64d;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no TLS versions for cleartext connections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045044
    :cond_0
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 1045045
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 1045046
    aget-object v2, p1, v0

    iget-object v2, v2, LX/658;->javaName:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 1045047
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1045048
    :cond_1
    invoke-virtual {p0, v1}, LX/64d;->b([Ljava/lang/String;)LX/64d;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([Ljava/lang/String;)LX/64d;
    .locals 2

    .prologue
    .line 1045049
    iget-boolean v0, p0, LX/64d;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no cipher suites for cleartext connections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045050
    :cond_0
    array-length v0, p1

    if-nez v0, :cond_1

    .line 1045051
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one cipher suite is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045052
    :cond_1
    invoke-virtual {p1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/64d;->b:[Ljava/lang/String;

    .line 1045053
    return-object p0
.end method

.method public final a()LX/64e;
    .locals 2

    .prologue
    .line 1045054
    new-instance v0, LX/64e;

    invoke-direct {v0, p0}, LX/64e;-><init>(LX/64d;)V

    return-object v0
.end method

.method public final varargs b([Ljava/lang/String;)LX/64d;
    .locals 2

    .prologue
    .line 1045055
    iget-boolean v0, p0, LX/64d;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no TLS versions for cleartext connections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045056
    :cond_0
    array-length v0, p1

    if-nez v0, :cond_1

    .line 1045057
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one TLS version is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045058
    :cond_1
    invoke-virtual {p1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/64d;->c:[Ljava/lang/String;

    .line 1045059
    return-object p0
.end method
