.class public final LX/6aL;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:LX/1ca;

.field public final synthetic b:LX/IJY;

.field public final synthetic c:LX/6ax;

.field public final synthetic d:Lcom/facebook/maps/UrlImageMarkerController;


# direct methods
.method public constructor <init>(Lcom/facebook/maps/UrlImageMarkerController;LX/1ca;LX/IJY;LX/6ax;)V
    .locals 0

    .prologue
    .line 1112327
    iput-object p1, p0, LX/6aL;->d:Lcom/facebook/maps/UrlImageMarkerController;

    iput-object p2, p0, LX/6aL;->a:LX/1ca;

    iput-object p3, p0, LX/6aL;->b:LX/IJY;

    iput-object p4, p0, LX/6aL;->c:LX/6ax;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1112328
    iget-object v0, p0, LX/6aL;->d:Lcom/facebook/maps/UrlImageMarkerController;

    iget-object v0, v0, Lcom/facebook/maps/UrlImageMarkerController;->d:Ljava/util/List;

    iget-object v1, p0, LX/6aL;->a:LX/1ca;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1112329
    if-nez p1, :cond_0

    .line 1112330
    :goto_0
    return-void

    .line 1112331
    :cond_0
    iget-object v0, p0, LX/6aL;->b:LX/IJY;

    .line 1112332
    iget-object v1, v0, LX/IJY;->d:LX/1FZ;

    iget-object v2, v0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, v0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v2

    .line 1112333
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1112334
    new-instance v3, Landroid/graphics/Canvas;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1112335
    iget v1, v0, LX/IJY;->c:I

    int-to-float v1, v1

    iget v4, v0, LX/IJY;->c:I

    int-to-float v4, v4

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1112336
    iget-object v1, v0, LX/IJY;->b:LX/4oL;

    invoke-virtual {v1, v3, p1}, LX/4oL;->a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 1112337
    iget v1, v0, LX/IJY;->c:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v4, v0, LX/IJY;->c:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1112338
    iget-object v1, v0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1112339
    move-object v1, v2

    .line 1112340
    iget-object v2, p0, LX/6aL;->c:LX/6ax;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, LX/690;->a(Landroid/graphics/Bitmap;)LX/68w;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/6ax;->a(LX/68w;)V

    .line 1112341
    iget-object v0, p0, LX/6aL;->d:Lcom/facebook/maps/UrlImageMarkerController;

    iget-boolean v0, v0, Lcom/facebook/maps/UrlImageMarkerController;->f:Z

    if-eqz v0, :cond_1

    .line 1112342
    iget-object v0, p0, LX/6aL;->d:Lcom/facebook/maps/UrlImageMarkerController;

    iget-object v0, v0, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1112343
    :cond_1
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1112344
    return-void
.end method
