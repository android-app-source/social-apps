.class public LX/6Qb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6Qb;


# instance fields
.field private final a:LX/6Qa;

.field private final b:LX/6Qw;


# direct methods
.method public constructor <init>(LX/6Qa;LX/6Qw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1087701
    iput-object p1, p0, LX/6Qb;->a:LX/6Qa;

    .line 1087702
    iput-object p2, p0, LX/6Qb;->b:LX/6Qw;

    .line 1087703
    return-void
.end method

.method public static a(LX/0QB;)LX/6Qb;
    .locals 5

    .prologue
    .line 1087704
    sget-object v0, LX/6Qb;->c:LX/6Qb;

    if-nez v0, :cond_1

    .line 1087705
    const-class v1, LX/6Qb;

    monitor-enter v1

    .line 1087706
    :try_start_0
    sget-object v0, LX/6Qb;->c:LX/6Qb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1087707
    if-eqz v2, :cond_0

    .line 1087708
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1087709
    new-instance p0, LX/6Qb;

    invoke-static {v0}, LX/6Qa;->a(LX/0QB;)LX/6Qa;

    move-result-object v3

    check-cast v3, LX/6Qa;

    invoke-static {v0}, LX/6Qw;->a(LX/0QB;)LX/6Qw;

    move-result-object v4

    check-cast v4, LX/6Qw;

    invoke-direct {p0, v3, v4}, LX/6Qb;-><init>(LX/6Qa;LX/6Qw;)V

    .line 1087710
    move-object v0, p0

    .line 1087711
    sput-object v0, LX/6Qb;->c:LX/6Qb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1087712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1087713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1087714
    :cond_1
    sget-object v0, LX/6Qb;->c:LX/6Qb;

    return-object v0

    .line 1087715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1087716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
