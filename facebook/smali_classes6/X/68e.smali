.class public final LX/68e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final synthetic a:LX/68h;

.field private final b:Ljava/lang/String;

.field private final c:J

.field public final d:[Ljava/io/InputStream;

.field private final e:[J


# direct methods
.method private constructor <init>(LX/68h;Ljava/lang/String;J[Ljava/io/InputStream;[J)V
    .locals 1

    .prologue
    .line 1055778
    iput-object p1, p0, LX/68e;->a:LX/68h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055779
    iput-object p2, p0, LX/68e;->b:Ljava/lang/String;

    .line 1055780
    iput-wide p3, p0, LX/68e;->c:J

    .line 1055781
    iput-object p5, p0, LX/68e;->d:[Ljava/io/InputStream;

    .line 1055782
    iput-object p6, p0, LX/68e;->e:[J

    .line 1055783
    return-void
.end method

.method public synthetic constructor <init>(LX/68h;Ljava/lang/String;J[Ljava/io/InputStream;[JB)V
    .locals 1

    .prologue
    .line 1055784
    invoke-direct/range {p0 .. p6}, LX/68e;-><init>(LX/68h;Ljava/lang/String;J[Ljava/io/InputStream;[J)V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 4

    .prologue
    .line 1055785
    iget-object v1, p0, LX/68e;->d:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1055786
    invoke-static {v3}, LX/68h;->a(Ljava/io/Closeable;)V

    .line 1055787
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1055788
    :cond_0
    return-void
.end method
