.class public final LX/5ji;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 988525
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 988526
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 988527
    :goto_0
    return v1

    .line 988528
    :cond_0
    const-string v12, "has_location_constraints"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 988529
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 988530
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 988531
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 988532
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 988533
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 988534
    const-string v12, "attribution_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 988535
    invoke-static {p0, p1}, LX/5je;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 988536
    :cond_2
    const-string v12, "attribution_thumbnail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 988537
    invoke-static {p0, p1}, LX/5jf;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 988538
    :cond_3
    const-string v12, "emitters"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 988539
    invoke-static {p0, p1}, LX/5jg;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 988540
    :cond_4
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 988541
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 988542
    :cond_5
    const-string v12, "instructions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 988543
    invoke-static {p0, p1}, LX/5jh;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 988544
    :cond_6
    const-string v12, "is_logging_disabled"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 988545
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 988546
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 988547
    :cond_8
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 988548
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 988549
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 988550
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 988551
    if-eqz v3, :cond_9

    .line 988552
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 988553
    :cond_9
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 988554
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 988555
    if-eqz v0, :cond_a

    .line 988556
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 988557
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 988558
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 988559
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988560
    if-eqz v0, :cond_0

    .line 988561
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988562
    invoke-static {p0, v0, p2}, LX/5je;->a(LX/15i;ILX/0nX;)V

    .line 988563
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988564
    if-eqz v0, :cond_1

    .line 988565
    const-string v1, "attribution_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988566
    invoke-static {p0, v0, p2}, LX/5jf;->a(LX/15i;ILX/0nX;)V

    .line 988567
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988568
    if-eqz v0, :cond_2

    .line 988569
    const-string v1, "emitters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988570
    invoke-static {p0, v0, p2, p3}, LX/5jg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 988571
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 988572
    if-eqz v0, :cond_3

    .line 988573
    const-string v1, "has_location_constraints"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988574
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 988575
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988576
    if-eqz v0, :cond_4

    .line 988577
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988578
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988579
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988580
    if-eqz v0, :cond_5

    .line 988581
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988582
    invoke-static {p0, v0, p2}, LX/5jh;->a(LX/15i;ILX/0nX;)V

    .line 988583
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 988584
    if-eqz v0, :cond_6

    .line 988585
    const-string v1, "is_logging_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988586
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 988587
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 988588
    return-void
.end method
