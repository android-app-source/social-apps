.class public LX/6WN;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/1aZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Uo;

.field public final d:Landroid/graphics/Paint;

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:I

.field public g:LX/6WT;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1105690
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1105691
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/6WN;->d:Landroid/graphics/Paint;

    .line 1105692
    invoke-direct {p0}, LX/6WN;->a()V

    .line 1105693
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1105686
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105687
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/6WN;->d:Landroid/graphics/Paint;

    .line 1105688
    invoke-direct {p0}, LX/6WN;->a()V

    .line 1105689
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1105758
    iget-object v0, p0, LX/6WN;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1105759
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, LX/6WN;->a:LX/4Ac;

    .line 1105760
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, LX/6WN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/6WN;->c:LX/1Uo;

    .line 1105761
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1105755
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1105756
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    .line 1105757
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1aZ;
    .locals 1

    .prologue
    .line 1105754
    iget-object v0, p0, LX/6WN;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    return-object v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1105753
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1105750
    iget-object v0, p0, LX/6WN;->g:LX/6WT;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6WN;->g:LX/6WT;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1105751
    const/4 v0, 0x1

    .line 1105752
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getNumDraweeControllers()I
    .locals 1

    .prologue
    .line 1105749
    iget-object v0, p0, LX/6WN;->b:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6WN;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x56599a02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1105746
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1105747
    iget-object v1, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1105748
    const/16 v1, 0x2d

    const v2, -0x56eb111d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x110dfebe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1105743
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1105744
    iget-object v1, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1105745
    const/16 v1, 0x2d

    const v2, 0x5c8d7873

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 1105729
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/Canvas;)V

    .line 1105730
    iget v0, p0, LX/6WN;->f:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1105731
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v9

    .line 1105732
    iget-object v2, p0, LX/6WN;->d:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-int v10, v2

    .line 1105733
    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-ge v8, v9, :cond_0

    .line 1105734
    invoke-virtual {p0, v8}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1105735
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 1105736
    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v10

    .line 1105737
    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v10

    .line 1105738
    iget v5, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v10

    .line 1105739
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v10

    .line 1105740
    int-to-float v3, v3

    int-to-float v4, v4

    int-to-float v5, v5

    int-to-float v6, v2

    iget-object v7, p0, LX/6WN;->d:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1105741
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_0

    .line 1105742
    :cond_0
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1105726
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1105727
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1105728
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1105723
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1105724
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1105725
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x5c692d70

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1105722
    iget-object v2, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v2, p1}, LX/4Ac;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    const v2, -0x3aacc411

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAccessibilityHelper(LX/6WT;)V
    .locals 1

    .prologue
    .line 1105719
    iput-object p1, p0, LX/6WN;->g:LX/6WT;

    .line 1105720
    iget-object v0, p0, LX/6WN;->g:LX/6WT;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1105721
    return-void
.end method

.method public setCapacity(I)V
    .locals 2

    .prologue
    .line 1105705
    :goto_0
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 1105706
    iget-object v0, p0, LX/6WN;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6WN;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1105707
    :goto_1
    iget-object v1, p0, LX/6WN;->c:LX/1Uo;

    .line 1105708
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1105709
    move-object v0, v1

    .line 1105710
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1105711
    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1105712
    new-instance v1, LX/1aX;

    invoke-direct {v1, v0}, LX/1aX;-><init>(LX/1aY;)V

    .line 1105713
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->a(LX/1aX;)V

    goto :goto_0

    .line 1105714
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1105715
    :cond_1
    :goto_2
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 1105716
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(I)V

    .line 1105717
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    .line 1105718
    :cond_2
    return-void
.end method

.method public setDraweeControllers(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/1aZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1105695
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1105696
    iget-object v0, p0, LX/6WN;->b:LX/0Px;

    if-ne v0, p1, :cond_1

    .line 1105697
    :cond_0
    return-void

    .line 1105698
    :cond_1
    iput-object p1, p0, LX/6WN;->b:LX/0Px;

    .line 1105699
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->c()V

    .line 1105700
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    .line 1105701
    invoke-virtual {p0, v2}, LX/6WN;->setCapacity(I)V

    .line 1105702
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1105703
    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    invoke-virtual {v3, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1105704
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1105694
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6WN;->a:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
