.class public final LX/6DK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6DJ;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/6DM;


# direct methods
.method public constructor <init>(LX/6DM;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1064827
    iput-object p1, p0, LX/6DK;->b:LX/6DM;

    iput-object p2, p0, LX/6DK;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1064828
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1064829
    const-string v1, "name-autofill-data"

    iget-object v2, p0, LX/6DK;->b:LX/6DM;

    const-string v3, "name-autofill-data"

    invoke-static {v2, v3, p1}, LX/6DM;->a$redex0(LX/6DM;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064830
    const-string v1, "telephone-autofill-data"

    iget-object v2, p0, LX/6DK;->b:LX/6DM;

    const-string v3, "telephone-autofill-data"

    invoke-static {v2, v3, p2}, LX/6DM;->a$redex0(LX/6DM;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064831
    const-string v1, "address-autofill-data"

    iget-object v2, p0, LX/6DK;->b:LX/6DM;

    const-string v3, "address-autofill-data"

    invoke-static {v2, v3, p3}, LX/6DM;->a$redex0(LX/6DM;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064832
    const-string v1, "string-autofill-data"

    iget-object v2, p0, LX/6DK;->b:LX/6DM;

    const-string v3, "string-autofill-data"

    invoke-static {v2, v3, p4}, LX/6DM;->a$redex0(LX/6DM;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064833
    iget-object v1, p0, LX/6DK;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x385a024f

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1064834
    return-void
.end method
