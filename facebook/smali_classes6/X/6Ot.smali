.class public LX/6Ot;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2fl;

.field private final b:LX/6Ov;


# direct methods
.method public constructor <init>(LX/2fl;LX/6Ov;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085661
    iput-object p1, p0, LX/6Ot;->a:LX/2fl;

    .line 1085662
    iput-object p2, p0, LX/6Ot;->b:LX/6Ov;

    .line 1085663
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085664
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085665
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085666
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1085667
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1085668
    invoke-virtual {v0, p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, p1

    :cond_0
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1085669
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6Ot;
    .locals 3

    .prologue
    .line 1085670
    new-instance v2, LX/6Ot;

    invoke-static {p0}, LX/2fl;->a(LX/0QB;)LX/2fl;

    move-result-object v0

    check-cast v0, LX/2fl;

    invoke-static {p0}, LX/6Ov;->a(LX/0QB;)LX/6Ov;

    move-result-object v1

    check-cast v1, LX/6Ov;

    invoke-direct {v2, v0, v1}, LX/6Ot;-><init>(LX/2fl;LX/6Ov;)V

    .line 1085671
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085672
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1085673
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1085674
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    .line 1085675
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v5

    invoke-static {v5, p2}, LX/2fl;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;

    move-result-object v4

    .line 1085676
    iput-object v4, v3, LX/39x;->b:LX/0Px;

    .line 1085677
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;

    move-result-object v0

    .line 1085678
    iput-object v0, v3, LX/39x;->q:LX/0Px;

    .line 1085679
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1085680
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
