.class public final LX/67R;
.super LX/389;
.source ""


# instance fields
.field public final synthetic a:LX/37z;


# direct methods
.method public constructor <init>(LX/37z;)V
    .locals 0

    .prologue
    .line 1052988
    iput-object p1, p0, LX/67R;->a:LX/37z;

    invoke-direct {p0}, LX/389;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 1052989
    iget-object v0, p0, LX/67R;->a:LX/37z;

    iget-object v0, v0, LX/37z;->b:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1052990
    iget-object v0, p0, LX/67R;->a:LX/37z;

    invoke-static {v0}, LX/37z;->f(LX/37z;)V

    .line 1052991
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 1052992
    iget-object v0, p0, LX/67R;->a:LX/37z;

    iget-object v0, v0, LX/37z;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 1052993
    iget-object v1, p0, LX/67R;->a:LX/37z;

    iget-object v1, v1, LX/37z;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 1052994
    add-int v2, v0, p1

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1052995
    if-eq v1, v0, :cond_0

    .line 1052996
    iget-object v1, p0, LX/67R;->a:LX/37z;

    iget-object v1, v1, LX/37z;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v3, v0, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1052997
    :cond_0
    iget-object v0, p0, LX/67R;->a:LX/37z;

    invoke-static {v0}, LX/37z;->f(LX/37z;)V

    .line 1052998
    return-void
.end method
