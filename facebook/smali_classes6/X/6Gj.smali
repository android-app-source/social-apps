.class public final LX/6Gj;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/bugreporter/debug/BugReportUploadStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bugreporter/debug/BugReportUploadStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070890
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1070891
    iput-object p1, p0, LX/6Gj;->a:Landroid/content/Context;

    .line 1070892
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1070893
    if-nez p2, :cond_0

    .line 1070894
    iget-object v0, p0, LX/6Gj;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1070895
    const v1, 0x7f0303ef

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1070896
    new-instance v1, LX/6Gi;

    invoke-direct {v1}, LX/6Gi;-><init>()V

    .line 1070897
    const v0, 0x7f0d0c2a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->a:Landroid/widget/TextView;

    .line 1070898
    const v0, 0x7f0d0550

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->b:Landroid/widget/TextView;

    .line 1070899
    const v0, 0x7f0d0c2b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->c:Landroid/widget/TextView;

    .line 1070900
    const v0, 0x7f0d0c2c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->d:Landroid/widget/TextView;

    .line 1070901
    const v0, 0x7f0d0c2d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->e:Landroid/widget/TextView;

    .line 1070902
    const v0, 0x7f0d0c2e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/6Gi;->f:Landroid/widget/TextView;

    .line 1070903
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1070904
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Gi;

    .line 1070905
    invoke-virtual {p0, p1}, LX/6Gj;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    .line 1070906
    iget-object v2, v0, LX/6Gi;->a:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->creationTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070907
    iget-object v2, v0, LX/6Gi;->b:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070908
    iget-object v2, v0, LX/6Gi;->c:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->networkType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070909
    iget-object v2, v0, LX/6Gi;->d:Landroid/widget/TextView;

    iget-boolean v3, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->isSuccessfullyUploaded:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070910
    iget-object v2, v0, LX/6Gi;->e:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->failedUploadAttempts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070911
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1070912
    iget-object v1, v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->failedUploadAttempts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1070913
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1070914
    const-string v1, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1070915
    :cond_1
    iget-object v0, v0, LX/6Gi;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070916
    return-object p2
.end method
