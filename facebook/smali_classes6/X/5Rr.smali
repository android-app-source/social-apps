.class public final enum LX/5Rr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Rr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Rr;

.field public static final enum CROP:LX/5Rr;

.field public static final enum DOODLE:LX/5Rr;

.field public static final enum FILTER:LX/5Rr;

.field public static final enum STICKER:LX/5Rr;

.field public static final enum TEXT:LX/5Rr;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 916697
    new-instance v0, LX/5Rr;

    const-string v1, "CROP"

    invoke-direct {v0, v1, v2}, LX/5Rr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rr;->CROP:LX/5Rr;

    .line 916698
    new-instance v0, LX/5Rr;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v3}, LX/5Rr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rr;->STICKER:LX/5Rr;

    .line 916699
    new-instance v0, LX/5Rr;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4}, LX/5Rr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rr;->TEXT:LX/5Rr;

    .line 916700
    new-instance v0, LX/5Rr;

    const-string v1, "DOODLE"

    invoke-direct {v0, v1, v5}, LX/5Rr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rr;->DOODLE:LX/5Rr;

    .line 916701
    new-instance v0, LX/5Rr;

    const-string v1, "FILTER"

    invoke-direct {v0, v1, v6}, LX/5Rr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rr;->FILTER:LX/5Rr;

    .line 916702
    const/4 v0, 0x5

    new-array v0, v0, [LX/5Rr;

    sget-object v1, LX/5Rr;->CROP:LX/5Rr;

    aput-object v1, v0, v2

    sget-object v1, LX/5Rr;->STICKER:LX/5Rr;

    aput-object v1, v0, v3

    sget-object v1, LX/5Rr;->TEXT:LX/5Rr;

    aput-object v1, v0, v4

    sget-object v1, LX/5Rr;->DOODLE:LX/5Rr;

    aput-object v1, v0, v5

    sget-object v1, LX/5Rr;->FILTER:LX/5Rr;

    aput-object v1, v0, v6

    sput-object v0, LX/5Rr;->$VALUES:[LX/5Rr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 916703
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Rr;
    .locals 1

    .prologue
    .line 916704
    const-class v0, LX/5Rr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rr;

    return-object v0
.end method

.method public static values()[LX/5Rr;
    .locals 1

    .prologue
    .line 916705
    sget-object v0, LX/5Rr;->$VALUES:[LX/5Rr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Rr;

    return-object v0
.end method
