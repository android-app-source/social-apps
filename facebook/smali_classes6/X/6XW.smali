.class public LX/6XW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/15I;
.implements Lorg/apache/http/conn/ConnectionReleaseTrigger;


# instance fields
.field private a:LX/4iW;


# direct methods
.method public constructor <init>(LX/4iW;)V
    .locals 1

    .prologue
    .line 1108477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108478
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4iW;

    iput-object v0, p0, LX/6XW;->a:LX/4iW;

    .line 1108479
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2

    .prologue
    .line 1108480
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108481
    iget-object v0, p0, LX/6XW;->a:LX/4iW;

    .line 1108482
    invoke-virtual {p1}, Lcom/facebook/http/interfaces/RequestPriority;->getNumericValue()I

    move-result v1

    move v1, v1

    .line 1108483
    iget-object p0, v0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 1108484
    if-eqz p0, :cond_0

    .line 1108485
    invoke-virtual {p0, v1}, Lcom/facebook/proxygen/JniHandler;->changePriority(I)V

    .line 1108486
    :cond_0
    return-void
.end method

.method public final abortConnection()V
    .locals 1

    .prologue
    .line 1108487
    iget-object v0, p0, LX/6XW;->a:LX/4iW;

    invoke-virtual {v0}, LX/4iW;->cancel()V

    .line 1108488
    return-void
.end method

.method public final releaseConnection()V
    .locals 2

    .prologue
    .line 1108489
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot perform release of this connection"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
