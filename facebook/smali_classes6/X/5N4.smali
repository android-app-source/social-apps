.class public LX/5N4;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/5N4;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 905863
    invoke-direct {p0}, LX/398;-><init>()V

    .line 905864
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 905865
    const-string v1, "target_tab_name"

    const-string v2, "Feed"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 905866
    const-string v1, "feed_type_name"

    const-string v2, "top_stories"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 905867
    const-string v1, "POP_TO_ROOT"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 905868
    const-string v1, "reset_feed_view"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 905869
    sget-object v1, LX/0ax;->cL:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 905870
    return-void
.end method

.method public static a(LX/0QB;)LX/5N4;
    .locals 3

    .prologue
    .line 905871
    sget-object v0, LX/5N4;->a:LX/5N4;

    if-nez v0, :cond_1

    .line 905872
    const-class v1, LX/5N4;

    monitor-enter v1

    .line 905873
    :try_start_0
    sget-object v0, LX/5N4;->a:LX/5N4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 905874
    if-eqz v2, :cond_0

    .line 905875
    :try_start_1
    new-instance v0, LX/5N4;

    invoke-direct {v0}, LX/5N4;-><init>()V

    .line 905876
    move-object v0, v0

    .line 905877
    sput-object v0, LX/5N4;->a:LX/5N4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 905878
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 905879
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 905880
    :cond_1
    sget-object v0, LX/5N4;->a:LX/5N4;

    return-object v0

    .line 905881
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 905882
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
