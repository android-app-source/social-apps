.class public final LX/5Nw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 907545
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 907546
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 907547
    :goto_0
    return v1

    .line 907548
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 907549
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 907550
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 907551
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 907552
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 907553
    const-string v11, "fallback_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 907554
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 907555
    :cond_2
    const-string v11, "icon"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 907556
    invoke-static {p0, p1}, LX/5Nu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 907557
    :cond_3
    const-string v11, "item_links"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 907558
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 907559
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_4

    .line 907560
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_4

    .line 907561
    invoke-static {p0, p1}, LX/5Nv;->b(LX/15w;LX/186;)I

    move-result v10

    .line 907562
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 907563
    :cond_4
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 907564
    goto :goto_1

    .line 907565
    :cond_5
    const-string v11, "item_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 907566
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 907567
    :cond_6
    const-string v11, "logging_param"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 907568
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 907569
    :cond_7
    const-string v11, "reaction_surface"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 907570
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 907571
    :cond_8
    const-string v11, "subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 907572
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 907573
    :cond_9
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 907574
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 907575
    :cond_a
    const-string v11, "title_color"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 907576
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 907577
    :cond_b
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 907578
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 907579
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 907580
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 907581
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 907582
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 907583
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 907584
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 907585
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 907586
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 907587
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 907588
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 907589
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 907590
    if-eqz v0, :cond_0

    .line 907591
    const-string v1, "fallback_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907592
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 907593
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 907594
    if-eqz v0, :cond_1

    .line 907595
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907596
    invoke-static {p0, v0, p2, p3}, LX/5Nu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 907597
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 907598
    if-eqz v0, :cond_3

    .line 907599
    const-string v1, "item_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907600
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 907601
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 907602
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/5Nv;->a(LX/15i;ILX/0nX;)V

    .line 907603
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 907604
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 907605
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 907606
    if-eqz v0, :cond_4

    .line 907607
    const-string v0, "item_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907608
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 907609
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 907610
    if-eqz v0, :cond_5

    .line 907611
    const-string v1, "logging_param"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907612
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 907613
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 907614
    if-eqz v0, :cond_6

    .line 907615
    const-string v1, "reaction_surface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907616
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 907617
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 907618
    if-eqz v0, :cond_7

    .line 907619
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907620
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 907621
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 907622
    if-eqz v0, :cond_8

    .line 907623
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907624
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 907625
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 907626
    if-eqz v0, :cond_9

    .line 907627
    const-string v1, "title_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 907628
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 907629
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 907630
    return-void
.end method
