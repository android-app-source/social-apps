.class public final LX/6SJ;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveUnpinnedCommentEventCreateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1091825
    const-class v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveUnpinnedCommentEventCreateMutationModel;

    const v0, -0x4757f177

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "LiveUnpinnedCommentEventCreateMutation"

    const-string v6, "65f50aa16ad073d4a058137c8c398e09"

    const-string v7, "unpinned_comment_event_create"

    const-string v8, "0"

    const-string v9, "10155207368356729"

    const/4 v10, 0x0

    .line 1091826
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1091827
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1091828
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091829
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1091830
    sparse-switch v0, :sswitch_data_0

    .line 1091831
    :goto_0
    return-object p1

    .line 1091832
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1091833
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1091834
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x65d81c4d -> :sswitch_2
        0x5fb57ca -> :sswitch_0
        0x69d55a61 -> :sswitch_1
    .end sparse-switch
.end method
