.class public final LX/5Ky;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Ky;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Kw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Kz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899364
    const/4 v0, 0x0

    sput-object v0, LX/5Ky;->a:LX/5Ky;

    .line 899365
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Ky;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 899361
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 899362
    new-instance v0, LX/5Kz;

    invoke-direct {v0}, LX/5Kz;-><init>()V

    iput-object v0, p0, LX/5Ky;->c:LX/5Kz;

    .line 899363
    return-void
.end method

.method public static declared-synchronized q()LX/5Ky;
    .locals 2

    .prologue
    .line 899366
    const-class v1, LX/5Ky;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Ky;->a:LX/5Ky;

    if-nez v0, :cond_0

    .line 899367
    new-instance v0, LX/5Ky;

    invoke-direct {v0}, LX/5Ky;-><init>()V

    sput-object v0, LX/5Ky;->a:LX/5Ky;

    .line 899368
    :cond_0
    sget-object v0, LX/5Ky;->a:LX/5Ky;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 899369
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 899355
    check-cast p2, LX/5Kx;

    .line 899356
    iget-object v0, p2, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    iget-object v1, p2, LX/5Kx;->b:LX/5KI;

    .line 899357
    const-string p0, "component"

    invoke-virtual {v0, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 899358
    const p2, -0xaf5564c

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 899359
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 899360
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 899344
    invoke-static {}, LX/1dS;->b()V

    .line 899345
    iget v0, p1, LX/1dQ;->b:I

    .line 899346
    packed-switch v0, :pswitch_data_0

    .line 899347
    :goto_0
    return-object v2

    .line 899348
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 899349
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 899350
    check-cast v1, LX/5Kx;

    .line 899351
    iget-object p0, v1, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    .line 899352
    const-string p1, "onPress"

    invoke-virtual {p0, p1}, Lcom/facebook/java2js/JSValue;->hasProperty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 899353
    const-string p1, "onPress"

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p2, v1

    invoke-virtual {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->invokeMethod(Ljava/lang/String;[Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    .line 899354
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xaf5564c
        :pswitch_0
    .end packed-switch
.end method
