.class public final enum LX/6ZO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ZO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ZO;

.field public static final enum IMPERIAL:LX/6ZO;

.field public static final enum METRIC:LX/6ZO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1111108
    new-instance v0, LX/6ZO;

    const-string v1, "METRIC"

    invoke-direct {v0, v1, v2}, LX/6ZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZO;->METRIC:LX/6ZO;

    .line 1111109
    new-instance v0, LX/6ZO;

    const-string v1, "IMPERIAL"

    invoke-direct {v0, v1, v3}, LX/6ZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZO;->IMPERIAL:LX/6ZO;

    .line 1111110
    const/4 v0, 0x2

    new-array v0, v0, [LX/6ZO;

    sget-object v1, LX/6ZO;->METRIC:LX/6ZO;

    aput-object v1, v0, v2

    sget-object v1, LX/6ZO;->IMPERIAL:LX/6ZO;

    aput-object v1, v0, v3

    sput-object v0, LX/6ZO;->$VALUES:[LX/6ZO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1111107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ZO;
    .locals 1

    .prologue
    .line 1111111
    const-class v0, LX/6ZO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ZO;

    return-object v0
.end method

.method public static values()[LX/6ZO;
    .locals 1

    .prologue
    .line 1111106
    sget-object v0, LX/6ZO;->$VALUES:[LX/6ZO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ZO;

    return-object v0
.end method
