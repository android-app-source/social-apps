.class public final LX/5nm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004671
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1004674
    check-cast p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;

    .line 1004675
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1004676
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004677
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "client_time"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004678
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1004679
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "suggested_option_timestamp"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004680
    :cond_0
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1004681
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "from_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004682
    :cond_1
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1004683
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "to_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004684
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "product"

    const-string v2, "fb4a_composer"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004685
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004686
    new-instance v0, LX/14N;

    const-string v1, "reportStickyGuardrailAction"

    const-string v2, "POST"

    const-string v3, "me/sticky_guardrail_events"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004672
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004673
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
