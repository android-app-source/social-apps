.class public abstract LX/6F1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA_MUTATOR::",
        "LX/6uN;",
        "POST_PURCHASE_ACTION_HAND",
        "LER::Lcom/facebook/payments/confirmation/PostPurchaseActionHandler;",
        "ON_ACTIVITY_RESU",
        "LT_HANDLER::Lcom/facebook/payments/confirmation/ConfirmationOnActivityResultHandler;",
        "ROW_VIEW_HO",
        "LDER_FACTORY::Lcom/facebook/payments/confirmation/ConfirmationRowViewHolderFactory;",
        "ROWS_GENERATOR::",
        "LX/6uV;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/6uW;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TDATA_MUTATOR;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TPOST_PURCHASE_ACTION_HAND",
            "LER;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TON_ACTIVITY_RESU",
            "LT_HANDLER;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TROW_VIEW_HO",
            "LDER_FACTORY;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TROWS_GENERATOR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6uW;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6uW;",
            "LX/0Ot",
            "<TDATA_MUTATOR;>;",
            "LX/0Ot",
            "<TPOST_PURCHASE_ACTION_HAND",
            "LER;",
            ">;",
            "LX/0Ot",
            "<TON_ACTIVITY_RESU",
            "LT_HANDLER;",
            ">;",
            "LX/0Ot",
            "<TROW_VIEW_HO",
            "LDER_FACTORY;",
            ">;",
            "LX/0Ot",
            "<TROWS_GENERATOR;>;)V"
        }
    .end annotation

    .prologue
    .line 1066961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066962
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uW;

    iput-object v0, p0, LX/6F1;->a:LX/6uW;

    .line 1066963
    iput-object p2, p0, LX/6F1;->b:LX/0Ot;

    .line 1066964
    iput-object p3, p0, LX/6F1;->c:LX/0Ot;

    .line 1066965
    iput-object p4, p0, LX/6F1;->d:LX/0Ot;

    .line 1066966
    iput-object p5, p0, LX/6F1;->e:LX/0Ot;

    .line 1066967
    iput-object p6, p0, LX/6F1;->f:LX/0Ot;

    .line 1066968
    return-void
.end method
