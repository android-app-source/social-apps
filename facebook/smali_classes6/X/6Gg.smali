.class public LX/6Gg;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070878
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1070879
    iput-object p1, p0, LX/6Gg;->a:Landroid/content/Context;

    .line 1070880
    iput-object p2, p0, LX/6Gg;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1070881
    iget-object v0, p0, LX/6Gg;->a:Landroid/content/Context;

    const v1, 0x7f081936

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6Gg;->setTitle(Ljava/lang/CharSequence;)V

    .line 1070882
    return-void
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1070883
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 1070884
    new-instance v0, LX/6Gf;

    invoke-direct {v0, p0}, LX/6Gf;-><init>(LX/6Gg;)V

    invoke-virtual {p0, v0}, LX/6Gg;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1070885
    return-void
.end method
