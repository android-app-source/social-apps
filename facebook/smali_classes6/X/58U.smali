.class public final LX/58U;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    .line 847096
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 847097
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 847098
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 847099
    const/4 v2, 0x0

    .line 847100
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 847101
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 847102
    :goto_1
    move v1, v2

    .line 847103
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 847104
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 847105
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 847106
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 847107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 847108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 847109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 847110
    const-string v7, "key"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 847111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 847112
    :cond_3
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 847113
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 847114
    :cond_4
    const-string v7, "type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 847115
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 847116
    :cond_5
    const-string v7, "value"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 847117
    invoke-static {p0, p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 847118
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 847119
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 847120
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 847121
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 847122
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 847123
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 847124
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 847125
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 847126
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 847127
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 847128
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 847129
    if-eqz v2, :cond_0

    .line 847130
    const-string v3, "key"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847131
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847132
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 847133
    if-eqz v2, :cond_1

    .line 847134
    const-string v3, "title"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847135
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847136
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 847137
    if-eqz v2, :cond_2

    .line 847138
    const-string v3, "type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847139
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847140
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 847141
    if-eqz v2, :cond_3

    .line 847142
    const-string v3, "value"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847143
    invoke-static {p0, v2, p2, p3}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 847144
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 847145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 847146
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 847147
    return-void
.end method
