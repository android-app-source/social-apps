.class public LX/6FN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/6FE;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6FE;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067696
    iput-object p1, p0, LX/6FN;->a:Landroid/content/Context;

    .line 1067697
    iput-object p2, p0, LX/6FN;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1067698
    iput-object p3, p0, LX/6FN;->c:LX/6FE;

    .line 1067699
    iput-object p4, p0, LX/6FN;->d:LX/0Uh;

    .line 1067700
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067701
    const-string v0, "requestCredentials"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 10

    .prologue
    .line 1067702
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    const/4 v6, 0x0

    .line 1067703
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1067704
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_PRIVACY_URL:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067705
    :goto_0
    return-void

    .line 1067706
    :cond_0
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1067707
    if-eqz v0, :cond_1

    .line 1067708
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1067709
    const-string v1, "JS_BRIDGE_BROWSER_DISPLAY_HEIGHT_RATIO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1067710
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1067711
    const-string v1, "JS_BRIDGE_BROWSER_DISPLAY_HEIGHT_RATIO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1067712
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, LX/6FN;->d:LX/0Uh;

    const/16 v1, 0x54e

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1067713
    invoke-virtual {p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->a(Ljava/lang/String;Lcom/facebook/browserextensions/ipc/UserCredentialInfo;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1067714
    :cond_1
    iget-object v0, p0, LX/6FN;->c:LX/6FE;

    .line 1067715
    iput-object p1, v0, LX/6FE;->a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    .line 1067716
    invoke-virtual {v0}, LX/6FE;->c()V

    .line 1067717
    sget-object v0, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v0}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v0

    invoke-virtual {v0}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v0

    invoke-virtual {v0}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v3

    .line 1067718
    const-string v0, "requestedUserInfo"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    .line 1067719
    iget-object v0, p0, LX/6FN;->d:LX/0Uh;

    const/16 v1, 0x56e

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 1067720
    if-eqz v4, :cond_2

    if-nez v5, :cond_5

    .line 1067721
    :cond_2
    sget-object v0, LX/6rp;->CONTACT_NAME:LX/6rp;

    sget-object v1, LX/6rp;->CONTACT_INFO:LX/6rp;

    sget-object v2, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    sget-object v7, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-static {v0, v1, v2, v7}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 1067722
    move-object v2, v0

    .line 1067723
    :goto_1
    if-eqz v4, :cond_3

    if-nez v5, :cond_b

    .line 1067724
    :cond_3
    sget-object v0, LX/6so;->CONTACT_NAME:LX/6so;

    sget-object v1, LX/6so;->CONTACT_INFORMATION:LX/6so;

    sget-object v7, LX/6so;->MAILING_ADDRESS:LX/6so;

    sget-object v8, LX/6so;->PAYMENT_METHOD:LX/6so;

    sget-object v9, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    invoke-static {v0, v1, v7, v8, v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1067725
    move-object v1, v0

    .line 1067726
    :goto_2
    if-eqz v4, :cond_4

    if-nez v5, :cond_10

    :cond_4
    sget-object v0, LX/6vb;->EMAIL:LX/6vb;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 1067727
    :goto_3
    sget-object v4, LX/6qw;->BROWSER_EXTENSION:LX/6qw;

    sget-object v5, LX/6xg;->MOR_MESSENGER_COMMERCE:LX/6xg;

    invoke-static {v4, v5, v2, v3}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v2

    .line 1067728
    iput-object v1, v2, LX/6qZ;->y:LX/0Px;

    .line 1067729
    move-object v1, v2

    .line 1067730
    iput-object v0, v1, LX/6qZ;->t:LX/0Rf;

    .line 1067731
    move-object v0, v1

    .line 1067732
    const v1, 0x7f081d56

    .line 1067733
    iput v1, v0, LX/6qZ;->m:I

    .line 1067734
    move-object v0, v0

    .line 1067735
    const/4 v1, 0x1

    .line 1067736
    iput-boolean v1, v0, LX/6qZ;->n:Z

    .line 1067737
    move-object v0, v0

    .line 1067738
    iput-boolean v6, v0, LX/6qZ;->A:Z

    .line 1067739
    move-object v0, v0

    .line 1067740
    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    .line 1067741
    new-instance v1, LX/6Dn;

    invoke-direct {v1, v0}, LX/6Dn;-><init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    move-object v0, v1

    .line 1067742
    const-string v1, "JS_BRIDGE_PAGE_NAME"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1067743
    iput-object v1, v0, LX/6Dn;->b:Ljava/lang/String;

    .line 1067744
    move-object v0, v0

    .line 1067745
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v1

    .line 1067746
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_4
    iput-object v2, v0, LX/6Dn;->c:Landroid/net/Uri;

    .line 1067747
    move-object v0, v0

    .line 1067748
    invoke-virtual {v0}, LX/6Dn;->a()Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;

    move-result-object v0

    .line 1067749
    iget-object v1, p0, LX/6FN;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1067750
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1067751
    iget-object v1, p0, LX/6FN;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6FN;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1067752
    :cond_5
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 1067753
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1067754
    const-string v7, "CONTACT_NAME"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1067755
    sget-object v0, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 1067756
    :cond_7
    const-string v7, "CONTACT_EMAIL"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    const-string v7, "CONTACT_PHONE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1067757
    :cond_8
    sget-object v0, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 1067758
    :cond_9
    const-string v7, "SHIPPING_ADDRESS"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1067759
    sget-object v0, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 1067760
    :cond_a
    sget-object v0, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1067761
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 1067762
    move-object v2, v0

    goto/16 :goto_1

    .line 1067763
    :cond_b
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1067764
    const-string v1, "CONTACT_NAME"

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1067765
    sget-object v1, LX/6so;->CONTACT_NAME:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1067766
    :cond_c
    const-string v1, "CONTACT_EMAIL"

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "CONTACT_PHONE"

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1067767
    :cond_d
    sget-object v1, LX/6so;->CONTACT_INFORMATION:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1067768
    :cond_e
    const-string v1, "SHIPPING_ADDRESS"

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1067769
    sget-object v1, LX/6so;->MAILING_ADDRESS:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1067770
    :cond_f
    sget-object v1, LX/6so;->PAYMENT_METHOD:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1067771
    sget-object v1, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1067772
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1067773
    move-object v1, v0

    goto/16 :goto_2

    .line 1067774
    :cond_10
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 1067775
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_11
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1067776
    const-string v8, "CONTACT_EMAIL"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 1067777
    sget-object v0, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v5, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_6

    .line 1067778
    :cond_12
    const-string v8, "CONTACT_PHONE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1067779
    sget-object v0, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v5, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_6

    .line 1067780
    :cond_13
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 1067781
    goto/16 :goto_3

    .line 1067782
    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_4
.end method
