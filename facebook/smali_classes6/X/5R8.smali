.class public final LX/5R8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 914420
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 914421
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 914422
    :goto_0
    return v1

    .line 914423
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 914424
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 914425
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 914426
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 914427
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 914428
    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 914429
    const/4 v2, 0x0

    .line 914430
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 914431
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 914432
    :goto_2
    move v0, v2

    .line 914433
    goto :goto_1

    .line 914434
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 914435
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 914436
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 914437
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 914438
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 914439
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 914440
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 914441
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 914442
    const-string v4, "uri"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 914443
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 914444
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 914445
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 914446
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 914447
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 914448
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 914449
    if-eqz v0, :cond_1

    .line 914450
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914451
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 914452
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 914453
    if-eqz v1, :cond_0

    .line 914454
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914455
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914456
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 914457
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 914458
    return-void
.end method
