.class public final LX/5vu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 1023191
    const-wide/16 v12, 0x0

    .line 1023192
    const-wide/16 v10, 0x0

    .line 1023193
    const-wide/16 v8, 0x0

    .line 1023194
    const-wide/16 v6, 0x0

    .line 1023195
    const/4 v5, 0x0

    .line 1023196
    const/4 v4, 0x0

    .line 1023197
    const/4 v3, 0x0

    .line 1023198
    const/4 v2, 0x0

    .line 1023199
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 1023200
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1023201
    const/4 v2, 0x0

    .line 1023202
    :goto_0
    return v2

    .line 1023203
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 1023204
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1023205
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1023206
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1023207
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1023208
    const/4 v2, 0x1

    .line 1023209
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1023210
    :cond_1
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1023211
    const/4 v2, 0x1

    .line 1023212
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 1023213
    :cond_2
    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1023214
    const/4 v2, 0x1

    .line 1023215
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 1023216
    :cond_3
    const-string v6, "y"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1023217
    const/4 v2, 0x1

    .line 1023218
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 1023219
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1023220
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1023221
    if-eqz v3, :cond_6

    .line 1023222
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1023223
    :cond_6
    if-eqz v10, :cond_7

    .line 1023224
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1023225
    :cond_7
    if-eqz v9, :cond_8

    .line 1023226
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1023227
    :cond_8
    if-eqz v8, :cond_9

    .line 1023228
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1023229
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1023230
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1023231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1023232
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1023233
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1023234
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1023235
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1023236
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1023237
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1023238
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1023239
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1023240
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1023241
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1023242
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1023243
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1023244
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 1023245
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1023246
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1023247
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1023248
    return-void
.end method
