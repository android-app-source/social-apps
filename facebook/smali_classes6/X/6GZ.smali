.class public LX/6GZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0oz;

.field private final c:LX/6Gc;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/6Gc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070662
    iput-object p1, p0, LX/6GZ;->a:LX/0Zb;

    .line 1070663
    iput-object p2, p0, LX/6GZ;->b:LX/0oz;

    .line 1070664
    iput-object p3, p0, LX/6GZ;->c:LX/6Gc;

    .line 1070665
    return-void
.end method

.method public static a(LX/0QB;)LX/6GZ;
    .locals 1

    .prologue
    .line 1070660
    invoke-static {p0}, LX/6GZ;->b(LX/0QB;)LX/6GZ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/6GZ;LX/6GY;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6GY;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070694
    iget-object v0, p1, LX/6GY;->name:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, LX/6GZ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1070695
    const-string v0, ""

    .line 1070696
    iget-object v1, p0, LX/6GZ;->c:LX/6Gc;

    .line 1070697
    iget-object v2, v1, LX/6Gc;->a:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1070698
    if-nez v2, :cond_1

    .line 1070699
    sget-object v2, LX/6Ga;->Other:LX/6Ga;

    .line 1070700
    :goto_0
    move-object v1, v2

    .line 1070701
    sget-object v2, LX/6GX;->a:[I

    invoke-virtual {v1}, LX/6Ga;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1070702
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1070703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/6GY;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1070704
    invoke-direct {p0, v0, p2}, LX/6GZ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1070705
    return-void

    .line 1070706
    :pswitch_0
    invoke-virtual {v1}, LX/6Ga;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1070707
    :pswitch_1
    iget-object v0, p0, LX/6GZ;->c:LX/6Gc;

    .line 1070708
    iget-object v1, v0, LX/6Gc;->a:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 1070709
    if-nez v1, :cond_6

    .line 1070710
    sget-object v1, LX/6Gb;->OtherMobile:LX/6Gb;

    .line 1070711
    :goto_3
    move-object v0, v1

    .line 1070712
    invoke-virtual {v0}, LX/6Gb;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1070713
    :cond_0
    const/4 v1, 0x0

    goto :goto_2

    .line 1070714
    :cond_1
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1070715
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    move v3, v3

    .line 1070716
    if-nez v3, :cond_2

    .line 1070717
    sget-object v2, LX/6Ga;->NoConnection:LX/6Ga;

    goto :goto_0

    .line 1070718
    :cond_2
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 1070719
    sget-object v2, LX/6Ga;->Wifi:LX/6Ga;

    goto :goto_0

    .line 1070720
    :cond_3
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_4

    .line 1070721
    sget-object v2, LX/6Ga;->Mobile:LX/6Ga;

    goto :goto_0

    .line 1070722
    :cond_4
    sget-object v2, LX/6Ga;->Other:LX/6Ga;

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    .line 1070723
    :cond_6
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    .line 1070724
    packed-switch v1, :pswitch_data_1

    .line 1070725
    sget-object v1, LX/6Gb;->OtherMobile:LX/6Gb;

    goto :goto_3

    .line 1070726
    :pswitch_2
    sget-object v1, LX/6Gb;->G2:LX/6Gb;

    goto :goto_3

    .line 1070727
    :pswitch_3
    sget-object v1, LX/6Gb;->G3:LX/6Gb;

    goto :goto_3

    .line 1070728
    :pswitch_4
    sget-object v1, LX/6Gb;->G4:LX/6Gb;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070689
    iget-object v0, p0, LX/6GZ;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bugreporter"

    .line 1070690
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1070691
    move-object v1, v1

    .line 1070692
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1070693
    return-void
.end method

.method public static b(LX/0QB;)LX/6GZ;
    .locals 5

    .prologue
    .line 1070684
    new-instance v3, LX/6GZ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v1

    check-cast v1, LX/0oz;

    .line 1070685
    new-instance v4, LX/6Gc;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v4, v2}, LX/6Gc;-><init>(Landroid/content/Context;)V

    .line 1070686
    move-object v2, v4

    .line 1070687
    check-cast v2, LX/6Gc;

    invoke-direct {v3, v0, v1, v2}, LX/6GZ;-><init>(LX/0Zb;LX/0oz;LX/6Gc;)V

    .line 1070688
    return-object v3
.end method

.method public static b(LX/6GZ;LX/6GY;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6GY;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070675
    iget-object v0, p0, LX/6GZ;->b:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 1070676
    sget-object v1, LX/0p3;->EXCELLENT:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/0p3;->GOOD:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/0p3;->MODERATE:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/0p3;->POOR:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1070677
    :cond_0
    invoke-virtual {v0}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v0

    .line 1070678
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1070679
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/6GY;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1070680
    invoke-direct {p0, v0, p2}, LX/6GZ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1070681
    return-void

    .line 1070682
    :cond_1
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v0}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1070683
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/6GY;)V
    .locals 2

    .prologue
    .line 1070673
    iget-object v0, p1, LX/6GY;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/6GZ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1070674
    return-void
.end method

.method public final a(LX/6GY;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1070666
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    .line 1070667
    const-string v1, "bug_id"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070668
    const-string v1, "attachment_size"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070669
    const-string v1, "attachment_name"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070670
    move-object v0, v0

    .line 1070671
    invoke-static {p0, p1, v0}, LX/6GZ;->a(LX/6GZ;LX/6GY;Ljava/util/Map;)V

    .line 1070672
    return-void
.end method
