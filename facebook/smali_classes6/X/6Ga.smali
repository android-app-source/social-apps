.class public final enum LX/6Ga;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Ga;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Ga;

.field public static final enum Mobile:LX/6Ga;

.field public static final enum NoConnection:LX/6Ga;

.field public static final enum Other:LX/6Ga;

.field public static final enum Wifi:LX/6Ga;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1070729
    new-instance v0, LX/6Ga;

    const-string v1, "NoConnection"

    invoke-direct {v0, v1, v2}, LX/6Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ga;->NoConnection:LX/6Ga;

    .line 1070730
    new-instance v0, LX/6Ga;

    const-string v1, "Wifi"

    invoke-direct {v0, v1, v3}, LX/6Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ga;->Wifi:LX/6Ga;

    .line 1070731
    new-instance v0, LX/6Ga;

    const-string v1, "Mobile"

    invoke-direct {v0, v1, v4}, LX/6Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ga;->Mobile:LX/6Ga;

    .line 1070732
    new-instance v0, LX/6Ga;

    const-string v1, "Other"

    invoke-direct {v0, v1, v5}, LX/6Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ga;->Other:LX/6Ga;

    .line 1070733
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Ga;

    sget-object v1, LX/6Ga;->NoConnection:LX/6Ga;

    aput-object v1, v0, v2

    sget-object v1, LX/6Ga;->Wifi:LX/6Ga;

    aput-object v1, v0, v3

    sget-object v1, LX/6Ga;->Mobile:LX/6Ga;

    aput-object v1, v0, v4

    sget-object v1, LX/6Ga;->Other:LX/6Ga;

    aput-object v1, v0, v5

    sput-object v0, LX/6Ga;->$VALUES:[LX/6Ga;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1070734
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Ga;
    .locals 1

    .prologue
    .line 1070735
    const-class v0, LX/6Ga;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Ga;

    return-object v0
.end method

.method public static values()[LX/6Ga;
    .locals 1

    .prologue
    .line 1070736
    sget-object v0, LX/6Ga;->$VALUES:[LX/6Ga;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Ga;

    return-object v0
.end method
