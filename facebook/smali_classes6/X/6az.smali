.class public LX/6az;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/68Q;

.field public final b:LX/7ax;


# direct methods
.method public constructor <init>(LX/68Q;)V
    .locals 1

    .prologue
    .line 1112819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112820
    iput-object p1, p0, LX/6az;->a:LX/68Q;

    .line 1112821
    const/4 v0, 0x0

    iput-object v0, p0, LX/6az;->b:LX/7ax;

    .line 1112822
    return-void
.end method

.method public constructor <init>(LX/7ax;)V
    .locals 1

    .prologue
    .line 1112823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112824
    const/4 v0, 0x0

    iput-object v0, p0, LX/6az;->a:LX/68Q;

    .line 1112825
    iput-object p1, p0, LX/6az;->b:LX/7ax;

    .line 1112826
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 1112827
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_0

    .line 1112828
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    .line 1112829
    iput-boolean p1, v0, LX/68Q;->b:Z

    .line 1112830
    :goto_0
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_1

    .line 1112831
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    .line 1112832
    iput-boolean p1, v0, LX/68Q;->c:Z

    .line 1112833
    :goto_1
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_2

    .line 1112834
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    .line 1112835
    iput-boolean p1, v0, LX/68Q;->d:Z

    .line 1112836
    :goto_2
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_3

    .line 1112837
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    .line 1112838
    iput-boolean p1, v0, LX/68Q;->e:Z

    .line 1112839
    :goto_3
    return-void

    .line 1112840
    :cond_0
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->g(Z)V

    goto :goto_0

    .line 1112841
    :cond_1
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->d(Z)V

    goto :goto_1

    .line 1112842
    :cond_2
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->f(Z)V

    goto :goto_2

    .line 1112843
    :cond_3
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->e(Z)V

    goto :goto_3
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1112844
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_0

    .line 1112845
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    invoke-virtual {v0, p1}, LX/68Q;->a(Z)V

    .line 1112846
    :goto_0
    return-void

    .line 1112847
    :cond_0
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->b(Z)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1112848
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    if-eqz v0, :cond_0

    .line 1112849
    iget-object v0, p0, LX/6az;->a:LX/68Q;

    invoke-virtual {v0, p1}, LX/68Q;->b(Z)V

    .line 1112850
    :goto_0
    return-void

    .line 1112851
    :cond_0
    iget-object v0, p0, LX/6az;->b:LX/7ax;

    invoke-virtual {v0, p1}, LX/7ax;->c(Z)V

    goto :goto_0
.end method
