.class public final LX/537;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/537;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/53A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53A",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826645
    const-class v0, LX/537;

    const-string v1, "c"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/537;->d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/53A;LX/0zZ;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53A",
            "<TT;>;",
            "LX/0zZ",
            "<TT;>;J)V"
        }
    .end annotation

    .prologue
    .line 826646
    invoke-direct {p0}, LX/0zZ;-><init>()V

    .line 826647
    const/4 v0, 0x0

    iput v0, p0, LX/537;->c:I

    .line 826648
    iput-object p1, p0, LX/537;->b:LX/53A;

    .line 826649
    iput-object p2, p0, LX/537;->a:LX/0zZ;

    .line 826650
    invoke-virtual {p0, p3, p4}, LX/0zZ;->a(J)V

    .line 826651
    return-void
.end method


# virtual methods
.method public final R_()V
    .locals 5

    .prologue
    .line 826652
    sget-object v0, LX/537;->d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826653
    iget-object v0, p0, LX/537;->b:LX/53A;

    .line 826654
    const-wide/16 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/0zZ;->a(J)V

    .line 826655
    const/4 v3, 0x0

    iput-object v3, v0, LX/53A;->c:LX/537;

    .line 826656
    sget-object v3, LX/53A;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->decrementAndGet(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_0

    .line 826657
    invoke-static {v0}, LX/53A;->g(LX/53A;)V

    .line 826658
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 826659
    iget-object v0, p0, LX/537;->b:LX/53A;

    .line 826660
    sget-object v1, LX/53A;->i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->decrementAndGet(Ljava/lang/Object;)J

    .line 826661
    iget-object v0, p0, LX/537;->a:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Object;)V

    .line 826662
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 826663
    sget-object v0, LX/537;->d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826664
    iget-object v0, p0, LX/537;->b:LX/53A;

    invoke-virtual {v0, p1}, LX/53A;->a(Ljava/lang/Throwable;)V

    .line 826665
    :cond_0
    return-void
.end method
