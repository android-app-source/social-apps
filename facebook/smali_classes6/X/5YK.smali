.class public final LX/5YK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 941360
    const/4 v13, 0x0

    .line 941361
    const/4 v12, 0x0

    .line 941362
    const-wide/16 v10, 0x0

    .line 941363
    const/4 v9, 0x0

    .line 941364
    const/4 v8, 0x0

    .line 941365
    const/4 v7, 0x0

    .line 941366
    const/4 v6, 0x0

    .line 941367
    const/4 v5, 0x0

    .line 941368
    const/4 v4, 0x0

    .line 941369
    const/4 v3, 0x0

    .line 941370
    const/4 v2, 0x0

    .line 941371
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 941372
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 941373
    const/4 v2, 0x0

    .line 941374
    :goto_0
    return v2

    .line 941375
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_9

    .line 941376
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 941377
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 941378
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 941379
    const-string v7, "can_stop_sending_location"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 941380
    const/4 v2, 0x1

    .line 941381
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v15, v6

    move v6, v2

    goto :goto_1

    .line 941382
    :cond_1
    const-string v7, "coordinate"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 941383
    invoke-static/range {p0 .. p1}, LX/5YI;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 941384
    :cond_2
    const-string v7, "expiration_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 941385
    const/4 v2, 0x1

    .line 941386
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 941387
    :cond_3
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 941388
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 941389
    :cond_4
    const-string v7, "location_title"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 941390
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 941391
    :cond_5
    const-string v7, "offline_threading_id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 941392
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 941393
    :cond_6
    const-string v7, "sender"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 941394
    invoke-static/range {p0 .. p1}, LX/5YJ;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 941395
    :cond_7
    const-string v7, "should_show_eta"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 941396
    const/4 v2, 0x1

    .line 941397
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v9, v7

    goto/16 :goto_1

    .line 941398
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 941399
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 941400
    if-eqz v6, :cond_a

    .line 941401
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 941402
    :cond_a
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 941403
    if-eqz v3, :cond_b

    .line 941404
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 941405
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 941406
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 941407
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 941408
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 941409
    if-eqz v8, :cond_c

    .line 941410
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 941411
    :cond_c
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v12

    move v15, v13

    move v12, v8

    move v13, v9

    move v9, v5

    move v8, v2

    move/from16 v17, v6

    move v6, v4

    move-wide v4, v10

    move v11, v7

    move/from16 v10, v17

    goto/16 :goto_1
.end method
