.class public final enum LX/6OP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6OP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6OP;

.field public static final enum HIGH:LX/6OP;

.field public static final enum LOW:LX/6OP;

.field public static final enum MEDIUM:LX/6OP;

.field public static final enum UNKNOWN:LX/6OP;

.field public static final enum VERY_LOW:LX/6OP;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085118
    new-instance v0, LX/6OP;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, LX/6OP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OP;->HIGH:LX/6OP;

    .line 1085119
    new-instance v0, LX/6OP;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/6OP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OP;->MEDIUM:LX/6OP;

    .line 1085120
    new-instance v0, LX/6OP;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4}, LX/6OP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OP;->LOW:LX/6OP;

    .line 1085121
    new-instance v0, LX/6OP;

    const-string v1, "VERY_LOW"

    invoke-direct {v0, v1, v5}, LX/6OP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OP;->VERY_LOW:LX/6OP;

    .line 1085122
    new-instance v0, LX/6OP;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/6OP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OP;->UNKNOWN:LX/6OP;

    .line 1085123
    const/4 v0, 0x5

    new-array v0, v0, [LX/6OP;

    sget-object v1, LX/6OP;->HIGH:LX/6OP;

    aput-object v1, v0, v2

    sget-object v1, LX/6OP;->MEDIUM:LX/6OP;

    aput-object v1, v0, v3

    sget-object v1, LX/6OP;->LOW:LX/6OP;

    aput-object v1, v0, v4

    sget-object v1, LX/6OP;->VERY_LOW:LX/6OP;

    aput-object v1, v0, v5

    sget-object v1, LX/6OP;->UNKNOWN:LX/6OP;

    aput-object v1, v0, v6

    sput-object v0, LX/6OP;->$VALUES:[LX/6OP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1085117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6OP;
    .locals 1

    .prologue
    .line 1085115
    const-class v0, LX/6OP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OP;

    return-object v0
.end method

.method public static values()[LX/6OP;
    .locals 1

    .prologue
    .line 1085116
    sget-object v0, LX/6OP;->$VALUES:[LX/6OP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6OP;

    return-object v0
.end method
