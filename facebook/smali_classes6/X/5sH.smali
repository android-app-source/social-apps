.class public abstract LX/5sH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/5sM;",
            "Landroid/view/animation/Interpolator;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/5sI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field private d:Landroid/view/animation/Interpolator;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 1012734
    sget-object v0, LX/5sM;->LINEAR:LX/5sM;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sget-object v2, LX/5sM;->EASE_IN:LX/5sM;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sget-object v4, LX/5sM;->EASE_OUT:LX/5sM;

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sget-object v6, LX/5sM;->EASE_IN_EASE_OUT:LX/5sM;

    new-instance v7, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sget-object v8, LX/5sM;->SPRING:LX/5sM;

    new-instance v9, LX/5sW;

    invoke-direct {v9}, LX/5sW;-><init>()V

    invoke-static/range {v0 .. v9}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LX/5sH;->c:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1012735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/5sM;)Landroid/view/animation/Interpolator;
    .locals 3

    .prologue
    .line 1012730
    sget-object v0, LX/5sH;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Interpolator;

    .line 1012731
    if-nez v0, :cond_0

    .line 1012732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing interpolator for type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012733
    :cond_0
    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/view/View;IIII)Landroid/view/animation/Animation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final a(LX/5pG;I)V
    .locals 3

    .prologue
    .line 1012719
    const-string v0, "property"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "property"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5sI;->fromString(Ljava/lang/String;)LX/5sI;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/5sH;->a:LX/5sI;

    .line 1012720
    const-string v0, "duration"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "duration"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result p2

    :cond_0
    iput p2, p0, LX/5sH;->b:I

    .line 1012721
    const-string v0, "delay"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "delay"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, LX/5sH;->e:I

    .line 1012722
    const-string v0, "type"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1012723
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing interpolation type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012724
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1012725
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1012726
    :cond_3
    const-string v0, "type"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5sM;->fromString(Ljava/lang/String;)LX/5sM;

    move-result-object v0

    invoke-static {v0}, LX/5sH;->a(LX/5sM;)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, LX/5sH;->d:Landroid/view/animation/Interpolator;

    .line 1012727
    invoke-virtual {p0}, LX/5sH;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1012728
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid layout animation : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012729
    :cond_4
    return-void
.end method

.method public abstract a()Z
.end method

.method public final b(Landroid/view/View;IIII)Landroid/view/animation/Animation;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1012711
    invoke-virtual {p0}, LX/5sH;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1012712
    const/4 v0, 0x0

    .line 1012713
    :cond_0
    :goto_0
    return-object v0

    .line 1012714
    :cond_1
    invoke-virtual/range {p0 .. p5}, LX/5sH;->a(Landroid/view/View;IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1012715
    if-eqz v0, :cond_0

    .line 1012716
    iget v1, p0, LX/5sH;->b:I

    mul-int/lit8 v1, v1, 0x1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1012717
    iget v1, p0, LX/5sH;->e:I

    mul-int/lit8 v1, v1, 0x1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1012718
    iget-object v1, p0, LX/5sH;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1012706
    iput-object v1, p0, LX/5sH;->a:LX/5sI;

    .line 1012707
    iput v0, p0, LX/5sH;->b:I

    .line 1012708
    iput v0, p0, LX/5sH;->e:I

    .line 1012709
    iput-object v1, p0, LX/5sH;->d:Landroid/view/animation/Interpolator;

    .line 1012710
    return-void
.end method
