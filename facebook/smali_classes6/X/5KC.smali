.class public final LX/5KC;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/5KE;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/5KD;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 898318
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "binder"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "snapDelegate"

    aput-object v2, v0, v1

    sput-object v0, LX/5KC;->b:[Ljava/lang/String;

    .line 898319
    sput v3, LX/5KC;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 898316
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 898317
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/5KC;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/5KC;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/5KC;LX/1De;IILX/5KD;)V
    .locals 1

    .prologue
    .line 898312
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 898313
    iput-object p4, p0, LX/5KC;->a:LX/5KD;

    .line 898314
    iget-object v0, p0, LX/5KC;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 898315
    return-void
.end method


# virtual methods
.method public final a(LX/1OX;)LX/5KC;
    .locals 1

    .prologue
    .line 898310
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->f:LX/1OX;

    .line 898311
    return-object p0
.end method

.method public final a(LX/1Of;)LX/5KC;
    .locals 1

    .prologue
    .line 898308
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->b:LX/1Of;

    .line 898309
    return-object p0
.end method

.method public final a(LX/25S;)LX/5KC;
    .locals 2

    .prologue
    .line 898280
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->c:LX/25S;

    .line 898281
    iget-object v0, p0, LX/5KC;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898282
    return-object p0
.end method

.method public final a(LX/3x6;)LX/5KC;
    .locals 1

    .prologue
    .line 898306
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->e:LX/3x6;

    .line 898307
    return-object p0
.end method

.method public final a(LX/5Je;)LX/5KC;
    .locals 2

    .prologue
    .line 898303
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->a:LX/5Je;

    .line 898304
    iget-object v0, p0, LX/5KC;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898305
    return-object p0
.end method

.method public final a(LX/5K7;)LX/5KC;
    .locals 1

    .prologue
    .line 898301
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-object p1, v0, LX/5KD;->d:LX/5K7;

    .line 898302
    return-object p0
.end method

.method public final a(Z)LX/5KC;
    .locals 1

    .prologue
    .line 898299
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput-boolean p1, v0, LX/5KD;->i:Z

    .line 898300
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 898295
    invoke-super {p0}, LX/1X5;->a()V

    .line 898296
    const/4 v0, 0x0

    iput-object v0, p0, LX/5KC;->a:LX/5KD;

    .line 898297
    sget-object v0, LX/5KE;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 898298
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/5KE;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 898285
    iget-object v1, p0, LX/5KC;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/5KC;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/5KC;->c:I

    if-ge v1, v2, :cond_2

    .line 898286
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 898287
    :goto_0
    sget v2, LX/5KC;->c:I

    if-ge v0, v2, :cond_1

    .line 898288
    iget-object v2, p0, LX/5KC;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 898289
    sget-object v2, LX/5KC;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 898290
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 898291
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898292
    :cond_2
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    .line 898293
    invoke-virtual {p0}, LX/5KC;->a()V

    .line 898294
    return-object v0
.end method

.method public final h(I)LX/5KC;
    .locals 1

    .prologue
    .line 898283
    iget-object v0, p0, LX/5KC;->a:LX/5KD;

    iput p1, v0, LX/5KD;->j:I

    .line 898284
    return-object p0
.end method
