.class public LX/6LE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6LC;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/6JT;

.field private final c:Landroid/os/Handler;

.field public final d:LX/6LG;

.field public volatile e:LX/6LB;

.field public f:Landroid/view/Surface;

.field public g:Landroid/media/MediaCodec;

.field private h:Landroid/media/MediaFormat;

.field private volatile i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1078175
    const-class v0, LX/6LC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6LE;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6LG;LX/6JT;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1078176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078177
    iput-object p1, p0, LX/6LE;->d:LX/6LG;

    .line 1078178
    iput-object p2, p0, LX/6LE;->b:LX/6JT;

    .line 1078179
    iput-object p3, p0, LX/6LE;->c:Landroid/os/Handler;

    .line 1078180
    sget-object v0, LX/6LB;->STOPPED:LX/6LB;

    iput-object v0, p0, LX/6LE;->e:LX/6LB;

    .line 1078181
    return-void
.end method

.method public static a(LX/6LG;Z)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 1078164
    const-string v0, "video/avc"

    iget v1, p0, LX/6LG;->a:I

    iget v2, p0, LX/6LG;->b:I

    invoke-static {v0, v1, v2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 1078165
    const-string v1, "color-format"

    const v2, 0x7f000789

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078166
    const-string v1, "bitrate"

    iget v2, p0, LX/6LG;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078167
    const-string v1, "frame-rate"

    iget v2, p0, LX/6LG;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078168
    const-string v1, "i-frame-interval"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078169
    const-string v1, "channel-count"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078170
    const-string v1, "max-input-size"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078171
    if-eqz p1, :cond_0

    .line 1078172
    const-string v1, "profile"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078173
    const-string v1, "level"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1078174
    :cond_0
    return-object v0
.end method

.method public static a$redex0(LX/6LE;Z)V
    .locals 14

    .prologue
    .line 1078118
    :try_start_0
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1078119
    new-instance v1, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v1}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1078120
    :cond_0
    :goto_0
    iget-object v2, p0, LX/6LE;->e:LX/6LB;

    sget-object v3, LX/6LB;->STARTED:LX/6LB;

    if-eq v2, v3, :cond_1

    if-eqz p1, :cond_2

    .line 1078121
    :cond_1
    iget-object v2, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    .line 1078122
    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 1078123
    if-eqz p1, :cond_0

    .line 1078124
    :cond_2
    :goto_1
    return-void

    .line 1078125
    :cond_3
    const/4 v3, -0x3

    if-ne v2, v3, :cond_4

    .line 1078126
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0

    .line 1078127
    :cond_4
    const/4 v3, -0x2

    if-ne v2, v3, :cond_5

    .line 1078128
    iget-object v2, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v2

    iput-object v2, p0, LX/6LE;->h:Landroid/media/MediaFormat;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1078129
    :catch_0
    move-exception v0

    .line 1078130
    iget-object v1, p0, LX/6LE;->b:LX/6JT;

    invoke-virtual {v1, v0}, LX/6JT;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1078131
    :cond_5
    if-gez v2, :cond_6

    .line 1078132
    :try_start_1
    iget-object v0, p0, LX/6LE;->b:LX/6JT;

    new-instance v1, Ljava/io/IOException;

    const-string v3, "Unexpected result from encoder.dequeueOutputBuffer: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/6JT;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1078133
    :cond_6
    aget-object v3, v0, v2

    .line 1078134
    if-nez v3, :cond_7

    .line 1078135
    iget-object v0, p0, LX/6LE;->b:LX/6JT;

    new-instance v1, Ljava/io/IOException;

    const-string v3, "encoderOutputBuffer %d was null"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/6JT;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1078136
    :cond_7
    iget v4, v1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v4

    iget v5, v1, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v4, v5}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 1078137
    iget v4, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_8

    .line 1078138
    const/4 v4, 0x2

    iput v4, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    .line 1078139
    :cond_8
    iget-object v4, p0, LX/6LE;->b:LX/6JT;

    .line 1078140
    iget-object v6, v4, LX/6JT;->a:LX/6Jc;

    iget-boolean v6, v6, LX/6Jc;->l:Z

    if-nez v6, :cond_9

    .line 1078141
    :goto_2
    iget-object v3, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1078142
    iget v2, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_2

    goto/16 :goto_0

    .line 1078143
    :cond_9
    :try_start_2
    iget-object v6, v4, LX/6JT;->a:LX/6Jc;

    iget-object v6, v6, LX/6Jc;->f:LX/6L8;

    const-wide/16 v9, 0x0

    .line 1078144
    iget-boolean v7, v6, LX/6L8;->f:Z

    if-nez v7, :cond_b

    .line 1078145
    invoke-static {v6}, LX/6L8;->b(LX/6L8;)V

    .line 1078146
    iget-boolean v7, v6, LX/6L8;->f:Z

    if-nez v7, :cond_b
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1078147
    :cond_a
    :goto_3
    :try_start_3
    goto :goto_2

    .line 1078148
    :catch_1
    move-exception v6

    .line 1078149
    invoke-virtual {v4, v6}, LX/6JT;->a(Ljava/lang/Exception;)V

    goto :goto_2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 1078150
    :cond_b
    iget v7, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v7, v7, 0x2

    if-nez v7, :cond_a

    .line 1078151
    iget v7, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v7, v7, 0x2

    if-nez v7, :cond_c

    .line 1078152
    iget-wide v7, v6, LX/6L8;->j:J

    cmp-long v7, v7, v9

    if-nez v7, :cond_c

    .line 1078153
    iget-wide v7, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v7, v6, LX/6L8;->j:J

    .line 1078154
    :cond_c
    iget-wide v7, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    cmp-long v7, v7, v9

    if-gez v7, :cond_d

    .line 1078155
    sget-object v7, LX/6L8;->a:Ljava/lang/String;

    const-string v8, "Video PTS negative - current pts %d last pts %d "

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-wide v11, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-wide v11, v6, LX/6L8;->k:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078156
    iget-wide v7, v6, LX/6L8;->k:J

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    iput-wide v7, v6, LX/6L8;->k:J

    .line 1078157
    :goto_4
    iget-wide v7, v6, LX/6L8;->k:J

    iget-wide v9, v6, LX/6L8;->j:J

    sub-long v11, v7, v9

    .line 1078158
    iget v9, v1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v10, v1, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget v13, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    move-object v8, v1

    invoke-virtual/range {v8 .. v13}, Landroid/media/MediaCodec$BufferInfo;->set(IIJI)V

    .line 1078159
    iget-object v7, v6, LX/6L8;->b:LX/6L9;

    .line 1078160
    iget-object v8, v7, LX/6L9;->a:Landroid/media/MediaMuxer;

    iget v9, v7, LX/6L9;->c:I

    invoke-virtual {v8, v9, v3, v1}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1078161
    const/4 v8, 0x1

    iput-boolean v8, v7, LX/6L9;->e:Z

    .line 1078162
    goto :goto_3

    .line 1078163
    :cond_d
    iget-wide v7, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v7, v6, LX/6L8;->k:J

    goto :goto_4
.end method

.method public static declared-synchronized e(LX/6LE;LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1078108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6LE;->e:LX/6LB;

    sget-object v1, LX/6LB;->PREPARED:LX/6LB;

    if-eq v0, v1, :cond_0

    .line 1078109
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prepare() must be called before starting video encoding. Current state is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6LE;->e:LX/6LB;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078110
    :goto_0
    monitor-exit p0

    return-void

    .line 1078111
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1078112
    :try_start_2
    sget-object v0, LX/6LB;->STARTED:LX/6LB;

    iput-object v0, p0, LX/6LE;->e:LX/6LB;

    .line 1078113
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1078114
    iget-object v0, p0, LX/6LE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$4;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$4;-><init>(LX/6LE;)V

    const v2, -0x7da0dac9

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1078115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1078116
    :catch_0
    move-exception v0

    .line 1078117
    :try_start_3
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static f(LX/6LE;LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1078182
    iget-boolean v0, p0, LX/6LE;->i:Z

    if-eqz v0, :cond_0

    .line 1078183
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/6LE;->a$redex0(LX/6LE;Z)V

    .line 1078184
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/6LE;->f:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 1078185
    iget-object v0, p0, LX/6LE;->f:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1078186
    :cond_1
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    if-eqz v0, :cond_3

    .line 1078187
    iget-boolean v0, p0, LX/6LE;->i:Z

    if-eqz v0, :cond_2

    .line 1078188
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 1078189
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1078190
    :cond_2
    iget-object v0, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078191
    :cond_3
    sget-object v0, LX/6LB;->STOPPED:LX/6LB;

    iput-object v0, p0, LX/6LE;->e:LX/6LB;

    .line 1078192
    iput-object v2, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    .line 1078193
    iput-object v2, p0, LX/6LE;->f:Landroid/view/Surface;

    .line 1078194
    iput-object v2, p0, LX/6LE;->h:Landroid/media/MediaFormat;

    .line 1078195
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1078196
    :goto_0
    return-void

    .line 1078197
    :catch_0
    move-exception v0

    .line 1078198
    :try_start_1
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1078199
    sget-object v0, LX/6LB;->STOPPED:LX/6LB;

    iput-object v0, p0, LX/6LE;->e:LX/6LB;

    .line 1078200
    iput-object v2, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    .line 1078201
    iput-object v2, p0, LX/6LE;->f:Landroid/view/Surface;

    .line 1078202
    iput-object v2, p0, LX/6LE;->h:Landroid/media/MediaFormat;

    goto :goto_0

    .line 1078203
    :catchall_0
    move-exception v0

    sget-object v1, LX/6LB;->STOPPED:LX/6LB;

    iput-object v1, p0, LX/6LE;->e:LX/6LB;

    .line 1078204
    iput-object v2, p0, LX/6LE;->g:Landroid/media/MediaCodec;

    .line 1078205
    iput-object v2, p0, LX/6LE;->f:Landroid/view/Surface;

    .line 1078206
    iput-object v2, p0, LX/6LE;->h:Landroid/media/MediaFormat;

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/Surface;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1078107
    iget-object v0, p0, LX/6LE;->f:Landroid/view/Surface;

    return-object v0
.end method

.method public final a(LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1078105
    iget-object v0, p0, LX/6LE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;-><init>(LX/6LE;LX/6JU;Landroid/os/Handler;)V

    const v2, -0x13ea2a86

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1078106
    return-void
.end method

.method public final b()Landroid/media/MediaFormat;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1078104
    iget-object v0, p0, LX/6LE;->h:Landroid/media/MediaFormat;

    return-object v0
.end method

.method public final b(LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1078097
    iget-object v0, p0, LX/6LE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$2;-><init>(LX/6LE;LX/6JU;Landroid/os/Handler;)V

    const v2, 0x58e58903

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1078098
    return-void
.end method

.method public final declared-synchronized c(LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1078099
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6LE;->e:LX/6LB;

    sget-object v1, LX/6LB;->STARTED:LX/6LB;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/6LE;->i:Z

    .line 1078100
    sget-object v0, LX/6LB;->STOP_IN_PROGRESS:LX/6LB;

    iput-object v0, p0, LX/6LE;->e:LX/6LB;

    .line 1078101
    iget-object v0, p0, LX/6LE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$3;-><init>(LX/6LE;LX/6JU;Landroid/os/Handler;)V

    const v2, 0x597ccf82

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078102
    monitor-exit p0

    return-void

    .line 1078103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
