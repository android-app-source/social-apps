.class public LX/5JV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5JT;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5JW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 897029
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5JV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/5JW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 897030
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 897031
    iput-object p1, p0, LX/5JV;->b:LX/0Ot;

    .line 897032
    return-void
.end method

.method public static a(LX/0QB;)LX/5JV;
    .locals 4

    .prologue
    .line 897033
    const-class v1, LX/5JV;

    monitor-enter v1

    .line 897034
    :try_start_0
    sget-object v0, LX/5JV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 897035
    sput-object v2, LX/5JV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 897036
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897037
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 897038
    new-instance v3, LX/5JV;

    const/16 p0, 0x1942

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/5JV;-><init>(LX/0Ot;)V

    .line 897039
    move-object v0, v3

    .line 897040
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 897041
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/5JV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 897042
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 897043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 897044
    check-cast p2, LX/5JU;

    .line 897045
    iget-object v0, p0, LX/5JV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5JW;

    iget v1, p2, LX/5JU;->a:I

    iget v2, p2, LX/5JU;->b:I

    iget-object v3, p2, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    .line 897046
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    iget-object p2, v0, LX/5JW;->a:LX/1vg;

    invoke-virtual {p2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p2

    invoke-virtual {p2, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 897047
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897048
    invoke-static {}, LX/1dS;->b()V

    .line 897049
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/5JT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 897050
    new-instance v1, LX/5JU;

    invoke-direct {v1, p0}, LX/5JU;-><init>(LX/5JV;)V

    .line 897051
    sget-object v2, LX/5JV;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5JT;

    .line 897052
    if-nez v2, :cond_0

    .line 897053
    new-instance v2, LX/5JT;

    invoke-direct {v2}, LX/5JT;-><init>()V

    .line 897054
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/5JT;->a$redex0(LX/5JT;LX/1De;IILX/5JU;)V

    .line 897055
    move-object v1, v2

    .line 897056
    move-object v0, v1

    .line 897057
    return-object v0
.end method
