.class public LX/69l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/69l;


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0W3;

.field private final c:LX/4Vd;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0W3;LX/4Vd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058253
    iput-object p1, p0, LX/69l;->a:LX/0Uh;

    .line 1058254
    iput-object p2, p0, LX/69l;->b:LX/0W3;

    .line 1058255
    iput-object p3, p0, LX/69l;->c:LX/4Vd;

    .line 1058256
    return-void
.end method

.method public static a(LX/0QB;)LX/69l;
    .locals 6

    .prologue
    .line 1058257
    sget-object v0, LX/69l;->d:LX/69l;

    if-nez v0, :cond_1

    .line 1058258
    const-class v1, LX/69l;

    monitor-enter v1

    .line 1058259
    :try_start_0
    sget-object v0, LX/69l;->d:LX/69l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1058260
    if-eqz v2, :cond_0

    .line 1058261
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1058262
    new-instance p0, LX/69l;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/4Vd;->a(LX/0QB;)LX/4Vd;

    move-result-object v5

    check-cast v5, LX/4Vd;

    invoke-direct {p0, v3, v4, v5}, LX/69l;-><init>(LX/0Uh;LX/0W3;LX/4Vd;)V

    .line 1058263
    move-object v0, p0

    .line 1058264
    sput-object v0, LX/69l;->d:LX/69l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058265
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1058266
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1058267
    :cond_1
    sget-object v0, LX/69l;->d:LX/69l;

    return-object v0

    .line 1058268
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1058269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1058270
    iget-object v1, p0, LX/69l;->b:LX/0W3;

    sget-wide v2, LX/0X5;->dC:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/69l;->a:LX/0Uh;

    const/16 v2, 0x36d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/69l;->c:LX/4Vd;

    const/4 v2, 0x1

    .line 1058271
    iget-object v3, v1, LX/4Vd;->a:LX/0Uh;

    const/16 p0, 0x3f6

    invoke-virtual {v3, p0, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    move v1, v2

    .line 1058272
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
