.class public final LX/5Hb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 890884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 5

    .prologue
    .line 890875
    if-nez p0, :cond_0

    .line 890876
    const/4 v0, 0x0

    .line 890877
    :goto_0
    return-object v0

    .line 890878
    :cond_0
    new-instance v0, LX/4X8;

    invoke-direct {v0}, LX/4X8;-><init>()V

    .line 890879
    invoke-interface {p0}, LX/1k1;->a()D

    move-result-wide v2

    .line 890880
    iput-wide v2, v0, LX/4X8;->b:D

    .line 890881
    invoke-interface {p0}, LX/1k1;->b()D

    move-result-wide v2

    .line 890882
    iput-wide v2, v0, LX/4X8;->c:D

    .line 890883
    invoke-virtual {v0}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/580;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 8

    .prologue
    .line 890830
    if-nez p0, :cond_0

    .line 890831
    const/4 v0, 0x0

    .line 890832
    :goto_0
    return-object v0

    .line 890833
    :cond_0
    new-instance v0, LX/4XR;

    invoke-direct {v0}, LX/4XR;-><init>()V

    .line 890834
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, -0x1dbebddb

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 890835
    iput-object v1, v0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 890836
    invoke-interface {p0}, LX/57x;->D()Z

    move-result v1

    .line 890837
    iput-boolean v1, v0, LX/4XR;->bo:Z

    .line 890838
    invoke-interface {p0}, LX/580;->H()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/5Hb;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 890839
    iput-object v1, v0, LX/4XR;->cl:Lcom/facebook/graphql/model/GraphQLPage;

    .line 890840
    invoke-interface {p0}, LX/57x;->E()I

    move-result v1

    .line 890841
    iput v1, v0, LX/4XR;->dr:I

    .line 890842
    invoke-interface {p0}, LX/57x;->m()Ljava/lang/String;

    move-result-object v1

    .line 890843
    iput-object v1, v0, LX/4XR;->fO:Ljava/lang/String;

    .line 890844
    invoke-interface {p0}, LX/57x;->F()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v1

    .line 890845
    if-nez v1, :cond_1

    .line 890846
    const/4 v2, 0x0

    .line 890847
    :goto_1
    move-object v1, v2

    .line 890848
    iput-object v1, v0, LX/4XR;->gj:Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    .line 890849
    invoke-interface {p0}, LX/580;->I()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v1

    invoke-static {v1}, LX/5Hb;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v1

    .line 890850
    iput-object v1, v0, LX/4XR;->hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    .line 890851
    invoke-interface {p0}, LX/580;->J()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/5Hb;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 890852
    iput-object v1, v0, LX/4XR;->jz:Lcom/facebook/graphql/model/GraphQLPage;

    .line 890853
    invoke-interface {p0}, LX/57x;->G()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v1

    .line 890854
    if-nez v1, :cond_2

    .line 890855
    const/4 v3, 0x0

    .line 890856
    :goto_2
    move-object v1, v3

    .line 890857
    iput-object v1, v0, LX/4XR;->qa:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 890858
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    goto :goto_0

    .line 890859
    :cond_1
    new-instance v2, LX/4Y8;

    invoke-direct {v2}, LX/4Y8;-><init>()V

    .line 890860
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;->a()Z

    move-result v3

    .line 890861
    iput-boolean v3, v2, LX/4Y8;->b:Z

    .line 890862
    new-instance v3, Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;-><init>(LX/4Y8;)V

    .line 890863
    move-object v2, v3

    .line 890864
    goto :goto_1

    .line 890865
    :cond_2
    new-instance v3, LX/4WT;

    invoke-direct {v3}, LX/4WT;-><init>()V

    .line 890866
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->a()D

    move-result-wide v5

    .line 890867
    iput-wide v5, v3, LX/4WT;->b:D

    .line 890868
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->b()D

    move-result-wide v5

    .line 890869
    iput-wide v5, v3, LX/4WT;->c:D

    .line 890870
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->c()D

    move-result-wide v5

    .line 890871
    iput-wide v5, v3, LX/4WT;->d:D

    .line 890872
    invoke-virtual {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;->d()D

    move-result-wide v5

    .line 890873
    iput-wide v5, v3, LX/4WT;->e:D

    .line 890874
    invoke-virtual {v3}, LX/4WT;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v3

    goto :goto_2
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 8

    .prologue
    .line 890598
    if-nez p0, :cond_0

    .line 890599
    const/4 v0, 0x0

    .line 890600
    :goto_0
    return-object v0

    .line 890601
    :cond_0
    new-instance v0, LX/4XY;

    invoke-direct {v0}, LX/4XY;-><init>()V

    .line 890602
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 890603
    iput-object v1, v0, LX/4XY;->N:Ljava/lang/String;

    .line 890604
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 890605
    iput-object v1, v0, LX/4XY;->ag:Ljava/lang/String;

    .line 890606
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->d()LX/1k1;

    move-result-object v1

    invoke-static {v1}, LX/5Hb;->a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 890607
    iput-object v1, v0, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 890608
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->aQ_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 890609
    if-nez v1, :cond_1

    .line 890610
    const/4 v3, 0x0

    .line 890611
    :goto_1
    move-object v1, v3

    .line 890612
    iput-object v1, v0, LX/4XY;->aM:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 890613
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 890614
    iput-object v1, v0, LX/4XY;->aT:Ljava/lang/String;

    .line 890615
    invoke-virtual {v0}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    goto :goto_0

    .line 890616
    :cond_1
    new-instance v3, LX/4WT;

    invoke-direct {v3}, LX/4WT;-><init>()V

    .line 890617
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 890618
    iput-wide v5, v3, LX/4WT;->b:D

    .line 890619
    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 890620
    iput-wide v5, v3, LX/4WT;->c:D

    .line 890621
    const/4 v4, 0x2

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 890622
    iput-wide v5, v3, LX/4WT;->d:D

    .line 890623
    const/4 v4, 0x3

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v5

    .line 890624
    iput-wide v5, v3, LX/4WT;->e:D

    .line 890625
    invoke-virtual {v3}, LX/4WT;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v3

    goto :goto_1
.end method

.method private static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;
    .locals 14

    .prologue
    .line 890646
    if-nez p0, :cond_0

    .line 890647
    const/4 v0, 0x0

    .line 890648
    :goto_0
    return-object v0

    .line 890649
    :cond_0
    new-instance v2, LX/4YB;

    invoke-direct {v2}, LX/4YB;-><init>()V

    .line 890650
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 890651
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 890652
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 890653
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;

    .line 890654
    if-nez v0, :cond_3

    .line 890655
    const/4 v4, 0x0

    .line 890656
    :goto_2
    move-object v0, v4

    .line 890657
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 890658
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 890659
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 890660
    iput-object v0, v2, LX/4YB;->b:LX/0Px;

    .line 890661
    :cond_2
    invoke-virtual {v2}, LX/4YB;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    goto :goto_0

    .line 890662
    :cond_3
    new-instance v4, LX/4Y9;

    invoke-direct {v4}, LX/4Y9;-><init>()V

    .line 890663
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 890664
    iput-object v5, v4, LX/4Y9;->b:Ljava/lang/String;

    .line 890665
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;

    move-result-object v5

    .line 890666
    if-nez v5, :cond_4

    .line 890667
    const/4 v6, 0x0

    .line 890668
    :goto_3
    move-object v5, v6

    .line 890669
    iput-object v5, v4, LX/4Y9;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 890670
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->d()LX/176;

    move-result-object v5

    const/4 v8, 0x0

    .line 890671
    if-nez v5, :cond_9

    .line 890672
    const/4 v6, 0x0

    .line 890673
    :goto_4
    move-object v5, v6

    .line 890674
    iput-object v5, v4, LX/4Y9;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 890675
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel;->e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v5

    .line 890676
    if-nez v5, :cond_11

    .line 890677
    const/4 v6, 0x0

    .line 890678
    :goto_5
    move-object v5, v6

    .line 890679
    iput-object v5, v4, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 890680
    invoke-virtual {v4}, LX/4Y9;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v4

    goto :goto_2

    .line 890681
    :cond_4
    new-instance v6, LX/4XY;

    invoke-direct {v6}, LX/4XY;-><init>()V

    .line 890682
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->b()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 890683
    if-nez v7, :cond_5

    .line 890684
    const/4 v9, 0x0

    .line 890685
    :goto_6
    move-object v7, v9

    .line 890686
    iput-object v7, v6, LX/4XY;->j:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 890687
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->e()Ljava/lang/String;

    move-result-object v7

    .line 890688
    iput-object v7, v6, LX/4XY;->F:Ljava/lang/String;

    .line 890689
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->aK_()LX/0Px;

    move-result-object v7

    .line 890690
    iput-object v7, v6, LX/4XY;->G:LX/0Px;

    .line 890691
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->aL_()Ljava/lang/String;

    move-result-object v7

    .line 890692
    iput-object v7, v6, LX/4XY;->ag:Ljava/lang/String;

    .line 890693
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->j()LX/1k1;

    move-result-object v7

    invoke-static {v7}, LX/5Hb;->a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    .line 890694
    iput-object v7, v6, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 890695
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 890696
    iput-object v7, v6, LX/4XY;->aT:Ljava/lang/String;

    .line 890697
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->c()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 890698
    if-nez v7, :cond_6

    .line 890699
    const/4 v9, 0x0

    .line 890700
    :goto_7
    move-object v7, v9

    .line 890701
    iput-object v7, v6, LX/4XY;->ba:Lcom/facebook/graphql/model/GraphQLRating;

    .line 890702
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->l()Ljava/lang/String;

    move-result-object v7

    .line 890703
    iput-object v7, v6, LX/4XY;->br:Ljava/lang/String;

    .line 890704
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->d()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 890705
    if-nez v7, :cond_7

    .line 890706
    const/4 v9, 0x0

    .line 890707
    :goto_8
    move-object v7, v9

    .line 890708
    iput-object v7, v6, LX/4XY;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 890709
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->m()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v7

    .line 890710
    iput-object v7, v6, LX/4XY;->bt:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 890711
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->n()Ljava/lang/String;

    move-result-object v7

    .line 890712
    iput-object v7, v6, LX/4XY;->bx:Ljava/lang/String;

    .line 890713
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$PlaceRecommendationPageModel;->o()LX/1Fb;

    move-result-object v7

    .line 890714
    if-nez v7, :cond_8

    .line 890715
    const/4 v8, 0x0

    .line 890716
    :goto_9
    move-object v7, v8

    .line 890717
    iput-object v7, v6, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 890718
    invoke-virtual {v6}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    goto/16 :goto_3

    .line 890719
    :cond_5
    new-instance v9, LX/4Z0;

    invoke-direct {v9}, LX/4Z0;-><init>()V

    .line 890720
    const/4 v10, 0x0

    invoke-virtual {v8, v7, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    .line 890721
    iput-object v10, v9, LX/4Z0;->h:Ljava/lang/String;

    .line 890722
    invoke-virtual {v9}, LX/4Z0;->a()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v9

    goto :goto_6

    .line 890723
    :cond_6
    new-instance v9, LX/4YW;

    invoke-direct {v9}, LX/4YW;-><init>()V

    .line 890724
    const/4 v10, 0x0

    invoke-virtual {v8, v7, v10}, LX/15i;->l(II)D

    move-result-wide v11

    .line 890725
    iput-wide v11, v9, LX/4YW;->d:D

    .line 890726
    invoke-virtual {v9}, LX/4YW;->a()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    goto :goto_7

    .line 890727
    :cond_7
    new-instance v9, LX/173;

    invoke-direct {v9}, LX/173;-><init>()V

    .line 890728
    const/4 v10, 0x0

    invoke-virtual {v8, v7, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    .line 890729
    iput-object v10, v9, LX/173;->f:Ljava/lang/String;

    .line 890730
    invoke-virtual {v9}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    goto :goto_8

    .line 890731
    :cond_8
    new-instance v8, LX/2dc;

    invoke-direct {v8}, LX/2dc;-><init>()V

    .line 890732
    invoke-interface {v7}, LX/1Fb;->a()I

    move-result v9

    .line 890733
    iput v9, v8, LX/2dc;->c:I

    .line 890734
    invoke-interface {v7}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v9

    .line 890735
    iput-object v9, v8, LX/2dc;->h:Ljava/lang/String;

    .line 890736
    invoke-interface {v7}, LX/1Fb;->c()I

    move-result v9

    .line 890737
    iput v9, v8, LX/2dc;->i:I

    .line 890738
    invoke-virtual {v8}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    goto :goto_9

    .line 890739
    :cond_9
    new-instance v9, LX/173;

    invoke-direct {v9}, LX/173;-><init>()V

    .line 890740
    invoke-interface {v5}, LX/176;->c()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 890741
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    move v7, v8

    .line 890742
    :goto_a
    invoke-interface {v5}, LX/176;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_a

    .line 890743
    invoke-interface {v5}, LX/176;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1jz;

    .line 890744
    if-nez v6, :cond_e

    .line 890745
    const/4 v11, 0x0

    .line 890746
    :goto_b
    move-object v6, v11

    .line 890747
    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 890748
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_a

    .line 890749
    :cond_a
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 890750
    iput-object v6, v9, LX/173;->b:LX/0Px;

    .line 890751
    :cond_b
    invoke-interface {v5}, LX/176;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 890752
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 890753
    :goto_c
    invoke-interface {v5}, LX/176;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_c

    .line 890754
    invoke-interface {v5}, LX/176;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1W5;

    .line 890755
    if-nez v6, :cond_f

    .line 890756
    const/4 v10, 0x0

    .line 890757
    :goto_d
    move-object v6, v10

    .line 890758
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 890759
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 890760
    :cond_c
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 890761
    iput-object v6, v9, LX/173;->e:LX/0Px;

    .line 890762
    :cond_d
    invoke-interface {v5}, LX/176;->a()Ljava/lang/String;

    move-result-object v6

    .line 890763
    iput-object v6, v9, LX/173;->f:Ljava/lang/String;

    .line 890764
    invoke-virtual {v9}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto/16 :goto_4

    .line 890765
    :cond_e
    new-instance v11, LX/4Vo;

    invoke-direct {v11}, LX/4Vo;-><init>()V

    .line 890766
    invoke-interface {v6}, LX/1jz;->a()I

    move-result v12

    .line 890767
    iput v12, v11, LX/4Vo;->b:I

    .line 890768
    invoke-interface {v6}, LX/1jz;->b()I

    move-result v12

    .line 890769
    iput v12, v11, LX/4Vo;->c:I

    .line 890770
    invoke-interface {v6}, LX/1jz;->c()I

    move-result v12

    .line 890771
    iput v12, v11, LX/4Vo;->d:I

    .line 890772
    invoke-virtual {v11}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v11

    goto :goto_b

    .line 890773
    :cond_f
    new-instance v10, LX/4W6;

    invoke-direct {v10}, LX/4W6;-><init>()V

    .line 890774
    invoke-interface {v6}, LX/1W5;->a()LX/171;

    move-result-object v11

    .line 890775
    if-nez v11, :cond_10

    .line 890776
    const/4 v12, 0x0

    .line 890777
    :goto_e
    move-object v11, v12

    .line 890778
    iput-object v11, v10, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 890779
    invoke-interface {v6}, LX/1W5;->b()I

    move-result v11

    .line 890780
    iput v11, v10, LX/4W6;->c:I

    .line 890781
    invoke-interface {v6}, LX/1W5;->c()I

    move-result v11

    .line 890782
    iput v11, v10, LX/4W6;->d:I

    .line 890783
    invoke-virtual {v10}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v10

    goto :goto_d

    .line 890784
    :cond_10
    new-instance v12, LX/170;

    invoke-direct {v12}, LX/170;-><init>()V

    .line 890785
    invoke-interface {v11}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 890786
    iput-object v13, v12, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 890787
    invoke-interface {v11}, LX/171;->c()LX/0Px;

    move-result-object v13

    .line 890788
    iput-object v13, v12, LX/170;->b:LX/0Px;

    .line 890789
    invoke-interface {v11}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v13

    .line 890790
    iput-object v13, v12, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 890791
    invoke-interface {v11}, LX/171;->e()Ljava/lang/String;

    move-result-object v13

    .line 890792
    iput-object v13, v12, LX/170;->o:Ljava/lang/String;

    .line 890793
    invoke-interface {v11}, LX/171;->v_()Ljava/lang/String;

    move-result-object v13

    .line 890794
    iput-object v13, v12, LX/170;->A:Ljava/lang/String;

    .line 890795
    invoke-interface {v11}, LX/171;->w_()Ljava/lang/String;

    move-result-object v13

    .line 890796
    iput-object v13, v12, LX/170;->X:Ljava/lang/String;

    .line 890797
    invoke-interface {v11}, LX/171;->j()Ljava/lang/String;

    move-result-object v13

    .line 890798
    iput-object v13, v12, LX/170;->Y:Ljava/lang/String;

    .line 890799
    invoke-virtual {v12}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v12

    goto :goto_e

    .line 890800
    :cond_11
    new-instance v8, LX/4YA;

    invoke-direct {v8}, LX/4YA;-><init>()V

    .line 890801
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_13

    .line 890802
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 890803
    const/4 v6, 0x0

    move v7, v6

    :goto_f
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_12

    .line 890804
    invoke-virtual {v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel;

    .line 890805
    if-nez v6, :cond_14

    .line 890806
    const/4 v10, 0x0

    .line 890807
    :goto_10
    move-object v6, v10

    .line 890808
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 890809
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_f

    .line 890810
    :cond_12
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 890811
    iput-object v6, v8, LX/4YA;->b:LX/0Px;

    .line 890812
    :cond_13
    invoke-virtual {v8}, LX/4YA;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v6

    goto/16 :goto_5

    .line 890813
    :cond_14
    new-instance v10, LX/4Vu;

    invoke-direct {v10}, LX/4Vu;-><init>()V

    .line 890814
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel;->b()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel$AuthorModel;

    move-result-object v11

    .line 890815
    if-nez v11, :cond_15

    .line 890816
    const/4 v12, 0x0

    .line 890817
    :goto_11
    move-object v11, v12

    .line 890818
    iput-object v11, v10, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 890819
    invoke-virtual {v6}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel;->c()Ljava/lang/String;

    move-result-object v11

    .line 890820
    iput-object v11, v10, LX/4Vu;->q:Ljava/lang/String;

    .line 890821
    invoke-virtual {v10}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v10

    goto :goto_10

    .line 890822
    :cond_15
    new-instance v12, LX/3dL;

    invoke-direct {v12}, LX/3dL;-><init>()V

    .line 890823
    invoke-virtual {v11}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel$AuthorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 890824
    iput-object v13, v12, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 890825
    invoke-virtual {v11}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel$AuthorModel;->c()Ljava/lang/String;

    move-result-object v13

    .line 890826
    iput-object v13, v12, LX/3dL;->E:Ljava/lang/String;

    .line 890827
    invoke-virtual {v11}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel$AuthorModel;->d()Ljava/lang/String;

    move-result-object v13

    .line 890828
    iput-object v13, v12, LX/3dL;->ag:Ljava/lang/String;

    .line 890829
    invoke-virtual {v12}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v12

    goto :goto_11
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 6

    .prologue
    .line 890626
    if-nez p0, :cond_0

    .line 890627
    const/4 v0, 0x0

    .line 890628
    :goto_0
    return-object v0

    .line 890629
    :cond_0
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    .line 890630
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 890631
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 890632
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 890633
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;

    .line 890634
    if-nez v0, :cond_3

    .line 890635
    const/4 v4, 0x0

    .line 890636
    :goto_2
    move-object v0, v4

    .line 890637
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 890638
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 890639
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 890640
    iput-object v0, v2, LX/23u;->k:LX/0Px;

    .line 890641
    :cond_2
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 890642
    :cond_3
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 890643
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;->a()LX/580;

    move-result-object v5

    invoke-static {v5}, LX/5Hb;->a(LX/580;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    .line 890644
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 890645
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto :goto_2
.end method
