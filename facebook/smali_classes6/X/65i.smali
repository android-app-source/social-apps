.class public final LX/65i;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic d:Z


# instance fields
.field public a:J

.field public b:J

.field public final c:LX/65d;

.field public final e:I

.field public final f:LX/65c;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/65e;

.field public final j:LX/65h;

.field public final k:LX/65h;

.field public l:LX/65X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048409
    const-class v0, LX/65i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/65i;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILX/65c;ZZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/65c;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/high16 v2, 0x10000

    .line 1048389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048390
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/65i;->a:J

    .line 1048391
    new-instance v0, LX/65h;

    invoke-direct {v0, p0}, LX/65h;-><init>(LX/65i;)V

    iput-object v0, p0, LX/65i;->j:LX/65h;

    .line 1048392
    new-instance v0, LX/65h;

    invoke-direct {v0, p0}, LX/65h;-><init>(LX/65i;)V

    iput-object v0, p0, LX/65i;->k:LX/65h;

    .line 1048393
    const/4 v0, 0x0

    iput-object v0, p0, LX/65i;->l:LX/65X;

    .line 1048394
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048395
    :cond_0
    if-nez p5, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048396
    :cond_1
    iput p1, p0, LX/65i;->e:I

    .line 1048397
    iput-object p2, p0, LX/65i;->f:LX/65c;

    .line 1048398
    iget-object v0, p2, LX/65c;->f:LX/663;

    .line 1048399
    invoke-virtual {v0, v2}, LX/663;->f(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/65i;->b:J

    .line 1048400
    new-instance v0, LX/65e;

    iget-object v1, p2, LX/65c;->e:LX/663;

    .line 1048401
    invoke-virtual {v1, v2}, LX/663;->f(I)I

    move-result v1

    int-to-long v2, v1

    invoke-direct {v0, p0, v2, v3}, LX/65e;-><init>(LX/65i;J)V

    iput-object v0, p0, LX/65i;->i:LX/65e;

    .line 1048402
    new-instance v0, LX/65d;

    invoke-direct {v0, p0}, LX/65d;-><init>(LX/65i;)V

    iput-object v0, p0, LX/65i;->c:LX/65d;

    .line 1048403
    iget-object v0, p0, LX/65i;->i:LX/65e;

    .line 1048404
    iput-boolean p4, v0, LX/65e;->g:Z

    .line 1048405
    iget-object v0, p0, LX/65i;->c:LX/65d;

    .line 1048406
    iput-boolean p3, v0, LX/65d;->e:Z

    .line 1048407
    iput-object p5, p0, LX/65i;->g:Ljava/util/List;

    .line 1048408
    return-void
.end method

.method private d(LX/65X;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1048376
    sget-boolean v1, LX/65i;->d:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048377
    :cond_0
    monitor-enter p0

    .line 1048378
    :try_start_0
    iget-object v1, p0, LX/65i;->l:LX/65X;

    if-eqz v1, :cond_1

    .line 1048379
    monitor-exit p0

    .line 1048380
    :goto_0
    return v0

    .line 1048381
    :cond_1
    iget-object v1, p0, LX/65i;->i:LX/65e;

    iget-boolean v1, v1, LX/65e;->g:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/65i;->c:LX/65d;

    iget-boolean v1, v1, LX/65d;->e:Z

    if-eqz v1, :cond_2

    .line 1048382
    monitor-exit p0

    goto :goto_0

    .line 1048383
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1048384
    :cond_2
    :try_start_1
    iput-object p1, p0, LX/65i;->l:LX/65X;

    .line 1048385
    const v0, 0x719b58b5

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1048386
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048387
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1}, LX/65c;->b(I)LX/65i;

    .line 1048388
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static j(LX/65i;)V
    .locals 2

    .prologue
    .line 1048364
    sget-boolean v0, LX/65i;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048365
    :cond_0
    monitor-enter p0

    .line 1048366
    :try_start_0
    iget-object v0, p0, LX/65i;->i:LX/65e;

    iget-boolean v0, v0, LX/65e;->g:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/65i;->i:LX/65e;

    iget-boolean v0, v0, LX/65e;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/65i;->c:LX/65d;

    iget-boolean v0, v0, LX/65d;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/65i;->c:LX/65d;

    iget-boolean v0, v0, LX/65d;->d:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 1048367
    :goto_0
    invoke-virtual {p0}, LX/65i;->b()Z

    move-result v1

    .line 1048368
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048369
    if-eqz v0, :cond_4

    .line 1048370
    sget-object v0, LX/65X;->CANCEL:LX/65X;

    invoke-virtual {p0, v0}, LX/65i;->a(LX/65X;)V

    .line 1048371
    :cond_2
    :goto_1
    return-void

    .line 1048372
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1048373
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1048374
    :cond_4
    if-nez v1, :cond_2

    .line 1048375
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1}, LX/65c;->b(I)LX/65i;

    goto :goto_1
.end method

.method public static k(LX/65i;)V
    .locals 2

    .prologue
    .line 1048303
    iget-object v0, p0, LX/65i;->c:LX/65d;

    iget-boolean v0, v0, LX/65d;->d:Z

    if-eqz v0, :cond_0

    .line 1048304
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048305
    :cond_0
    iget-object v0, p0, LX/65i;->c:LX/65d;

    iget-boolean v0, v0, LX/65d;->e:Z

    if-eqz v0, :cond_1

    .line 1048306
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048307
    :cond_1
    iget-object v0, p0, LX/65i;->l:LX/65X;

    if-eqz v0, :cond_2

    .line 1048308
    new-instance v0, LX/667;

    iget-object v1, p0, LX/65i;->l:LX/65X;

    invoke-direct {v0, v1}, LX/667;-><init>(LX/65X;)V

    throw v0

    .line 1048309
    :cond_2
    return-void
.end method

.method public static l(LX/65i;)V
    .locals 1

    .prologue
    .line 1048361
    const v0, 0x75edea01

    :try_start_0
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1048362
    return-void

    .line 1048363
    :catch_0
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1048358
    iget-wide v0, p0, LX/65i;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/65i;->b:J

    .line 1048359
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const v0, 0x5b99ade1

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1048360
    :cond_0
    return-void
.end method

.method public final a(LX/65X;)V
    .locals 2

    .prologue
    .line 1048355
    invoke-direct {p0, p1}, LX/65i;->d(LX/65X;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048356
    :goto_0
    return-void

    .line 1048357
    :cond_0
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1, p1}, LX/65c;->b(ILX/65X;)V

    goto :goto_0
.end method

.method public final a(LX/671;I)V
    .locals 4

    .prologue
    .line 1048352
    sget-boolean v0, LX/65i;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048353
    :cond_0
    iget-object v0, p0, LX/65i;->i:LX/65e;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, LX/65e;->a(LX/671;J)V

    .line 1048354
    return-void
.end method

.method public final a(Ljava/util/List;LX/65k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;",
            "LX/65k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1048280
    sget-boolean v0, LX/65i;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048281
    :cond_0
    const/4 v1, 0x0

    .line 1048282
    const/4 v0, 0x1

    .line 1048283
    monitor-enter p0

    .line 1048284
    :try_start_0
    iget-object v2, p0, LX/65i;->h:Ljava/util/List;

    if-nez v2, :cond_3

    .line 1048285
    invoke-virtual {p2}, LX/65k;->failIfHeadersAbsent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1048286
    sget-object v1, LX/65X;->PROTOCOL_ERROR:LX/65X;

    .line 1048287
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048288
    if-eqz v1, :cond_5

    .line 1048289
    invoke-virtual {p0, v1}, LX/65i;->b(LX/65X;)V

    .line 1048290
    :cond_1
    :goto_1
    return-void

    .line 1048291
    :cond_2
    :try_start_1
    iput-object p1, p0, LX/65i;->h:Ljava/util/List;

    .line 1048292
    invoke-virtual {p0}, LX/65i;->b()Z

    move-result v0

    .line 1048293
    const v2, 0x456f16b2

    invoke-static {p0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1048294
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1048295
    :cond_3
    :try_start_2
    invoke-virtual {p2}, LX/65k;->failIfHeadersPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1048296
    sget-object v1, LX/65X;->STREAM_IN_USE:LX/65X;

    goto :goto_0

    .line 1048297
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1048298
    iget-object v3, p0, LX/65i;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1048299
    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1048300
    iput-object v2, p0, LX/65i;->h:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1048301
    :cond_5
    if-nez v0, :cond_1

    .line 1048302
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1}, LX/65c;->b(I)LX/65i;

    goto :goto_1
.end method

.method public final b(LX/65X;)V
    .locals 2

    .prologue
    .line 1048310
    invoke-direct {p0, p1}, LX/65i;->d(LX/65X;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048311
    :goto_0
    return-void

    .line 1048312
    :cond_0
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1, p1}, LX/65c;->a(ILX/65X;)V

    goto :goto_0
.end method

.method public final declared-synchronized b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1048313
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/65i;->l:LX/65X;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 1048314
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1048315
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/65i;->i:LX/65e;

    iget-boolean v1, v1, LX/65e;->g:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/65i;->i:LX/65e;

    iget-boolean v1, v1, LX/65e;->f:Z

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, LX/65i;->c:LX/65d;

    .line 1048316
    iget-boolean v1, v1, LX/65d;->e:Z

    if-nez v1, :cond_3

    iget-object v1, p0, LX/65i;->c:LX/65d;

    iget-boolean v1, v1, LX/65d;->d:Z

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, LX/65i;->h:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 1048317
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1048318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/65X;)V
    .locals 1

    .prologue
    .line 1048319
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65i;->l:LX/65X;

    if-nez v0, :cond_0

    .line 1048320
    iput-object p1, p0, LX/65i;->l:LX/65X;

    .line 1048321
    const v0, -0x23c6ceab

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048322
    :cond_0
    monitor-exit p0

    return-void

    .line 1048323
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1048324
    iget v0, p0, LX/65i;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1048325
    :goto_0
    iget-object v3, p0, LX/65i;->f:LX/65c;

    iget-boolean v3, v3, LX/65c;->b:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1048326
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1048327
    goto :goto_1
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1048328
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65i;->j:LX/65h;

    invoke-virtual {v0}, LX/65g;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1048329
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/65i;->h:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65i;->l:LX/65X;

    if-nez v0, :cond_0

    .line 1048330
    invoke-static {p0}, LX/65i;->l(LX/65i;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048331
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, LX/65i;->j:LX/65h;

    invoke-virtual {v1}, LX/65h;->b()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1048332
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1048333
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/65i;->j:LX/65h;

    invoke-virtual {v0}, LX/65h;->b()V

    .line 1048334
    iget-object v0, p0, LX/65i;->h:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/65i;->h:Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit p0

    return-object v0

    .line 1048335
    :cond_1
    new-instance v0, LX/667;

    iget-object v1, p0, LX/65i;->l:LX/65X;

    invoke-direct {v0, v1}, LX/667;-><init>(LX/65X;)V

    throw v0
.end method

.method public final h()LX/65J;
    .locals 2

    .prologue
    .line 1048336
    monitor-enter p0

    .line 1048337
    :try_start_0
    iget-object v0, p0, LX/65i;->h:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/65i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048338
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the sink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048339
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048340
    iget-object v0, p0, LX/65i;->c:LX/65d;

    return-object v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1048341
    sget-boolean v0, LX/65i;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048342
    :cond_0
    monitor-enter p0

    .line 1048343
    :try_start_0
    iget-object v0, p0, LX/65i;->i:LX/65e;

    const/4 v1, 0x1

    .line 1048344
    iput-boolean v1, v0, LX/65e;->g:Z

    .line 1048345
    invoke-virtual {p0}, LX/65i;->b()Z

    move-result v0

    .line 1048346
    const v1, -0x5535a3eb

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1048347
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048348
    if-nez v0, :cond_1

    .line 1048349
    iget-object v0, p0, LX/65i;->f:LX/65c;

    iget v1, p0, LX/65i;->e:I

    invoke-virtual {v0, v1}, LX/65c;->b(I)LX/65i;

    .line 1048350
    :cond_1
    return-void

    .line 1048351
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
