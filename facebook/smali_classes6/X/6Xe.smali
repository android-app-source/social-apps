.class public final LX/6Xe;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Lorg/json/JSONObject;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108694
    const/4 v0, 0x0

    sput-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 1108672
    sget-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 1108673
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "platform.installdata"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v0

    .line 1108674
    sget-object v1, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1108675
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    sput-object v1, LX/6Xe;->a:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1108676
    :goto_0
    sget-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 1108677
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    .line 1108678
    :cond_0
    sget-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    return-object v0

    :catch_0
    goto :goto_0

    .line 1108679
    :catch_1
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1108687
    invoke-static {p2}, LX/6Xe;->a(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1108688
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1108689
    :try_start_0
    const-string v2, "com.facebook.platform.APPLINK_ARGS"

    invoke-virtual {v1, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1108690
    const-string v2, "com.facebook.platform.APPLINK_TAP_TIME_UTC"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1108691
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1108692
    :goto_0
    invoke-static {p2}, LX/6Xe;->b(Landroid/content/Context;)V

    .line 1108693
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1108680
    sget-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 1108681
    sget-object v0, LX/6Xe;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1108682
    sget-object v1, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 1108683
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "platform.installdata"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/1t3;->a([BLjava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1108684
    :goto_0
    return-void

    .line 1108685
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "platform.installdata"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 1108686
    :catch_0
    goto :goto_0
.end method
