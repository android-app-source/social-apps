.class public LX/6UT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1101635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101636
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1101615
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;

    .line 1101616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1101617
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "watcher_id"

    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101618
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "watch_like_events"

    .line 1101619
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1101620
    move-object v1, v1

    .line 1101621
    const-string v2, "POST"

    .line 1101622
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1101623
    move-object v1, v1

    .line 1101624
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/watch_like_events"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1101625
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1101626
    move-object v1, v1

    .line 1101627
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1101628
    move-object v0, v1

    .line 1101629
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1101630
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1101631
    move-object v0, v0

    .line 1101632
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1101633
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1101634
    const/4 v0, 0x0

    return-object v0
.end method
