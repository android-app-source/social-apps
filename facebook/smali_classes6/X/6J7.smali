.class public final LX/6J7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6JF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1075336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075337
    return-void
.end method

.method public static a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;
    .locals 2
    .param p2    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1075344
    invoke-static {}, LX/6J7;->a()LX/6J9;

    move-result-object v0

    .line 1075345
    sget-object v1, LX/6J9;->CAMERA1:LX/6J9;

    if-ne v0, v1, :cond_0

    if-eq v0, p3, :cond_0

    .line 1075346
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported api level requested"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075347
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, LX/6J7;->b(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6KF;)LX/6Ia;
    .locals 1
    .param p2    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1075348
    invoke-static {}, LX/6J7;->a()LX/6J9;

    move-result-object v0

    invoke-static {p0, p1, p2, v0, p3}, LX/6J7;->b(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v0

    return-object v0
.end method

.method public static a()LX/6J9;
    .locals 2

    .prologue
    .line 1075343
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    sget-object v0, LX/6J9;->CAMERA1:LX/6J9;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6J9;->CAMERA2:LX/6J9;

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;
    .locals 2
    .param p2    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1075338
    sget-object v0, LX/6J9;->CAMERA2:LX/6J9;

    if-ne p3, v0, :cond_0

    .line 1075339
    iget-boolean v0, p4, LX/6KF;->a:Z

    move v0, v0

    .line 1075340
    if-nez v0, :cond_0

    new-instance v0, LX/6Iu;

    invoke-direct {v0, p0, p1, p2}, LX/6Iu;-><init>(Landroid/content/Context;LX/6JF;LX/6KN;)V

    .line 1075341
    :goto_0
    new-instance v1, LX/6J6;

    invoke-direct {v1, v0}, LX/6J6;-><init>(LX/6Ia;)V

    return-object v1

    .line 1075342
    :cond_0
    new-instance v0, LX/6Ib;

    invoke-direct {v0, p1, p2}, LX/6Ib;-><init>(LX/6JF;LX/6KN;)V

    goto :goto_0
.end method
