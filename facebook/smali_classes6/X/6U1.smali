.class public final LX/6U1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 1099868
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1099869
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1099870
    :goto_0
    return v7

    .line 1099871
    :cond_0
    const-string v12, "total_tip_amount"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1099872
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    .line 1099873
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 1099874
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1099875
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1099876
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1099877
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1099878
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1099879
    :cond_2
    const-string v12, "unique_tip_giver_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1099880
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v1

    goto :goto_1

    .line 1099881
    :cond_3
    const-string v12, "video_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1099882
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1099883
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1099884
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1099885
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 1099886
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1099887
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1099888
    :cond_6
    if-eqz v6, :cond_7

    .line 1099889
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9, v7}, LX/186;->a(III)V

    .line 1099890
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1099891
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_0

    :cond_8
    move v6, v7

    move v0, v7

    move v8, v7

    move v9, v7

    move-wide v2, v4

    move v10, v7

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1099892
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1099893
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1099894
    if-eqz v0, :cond_0

    .line 1099895
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099896
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1099897
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1099898
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 1099899
    const-string v2, "total_tip_amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099900
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1099901
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 1099902
    if-eqz v0, :cond_2

    .line 1099903
    const-string v1, "unique_tip_giver_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099904
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1099905
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1099906
    if-eqz v0, :cond_3

    .line 1099907
    const-string v1, "video_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099908
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1099909
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1099910
    return-void
.end method
