.class public final LX/5Bi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 47

    .prologue
    .line 865054
    const/16 v43, 0x0

    .line 865055
    const/16 v42, 0x0

    .line 865056
    const/16 v41, 0x0

    .line 865057
    const/16 v40, 0x0

    .line 865058
    const/16 v39, 0x0

    .line 865059
    const/16 v38, 0x0

    .line 865060
    const/16 v37, 0x0

    .line 865061
    const/16 v36, 0x0

    .line 865062
    const/16 v35, 0x0

    .line 865063
    const/16 v34, 0x0

    .line 865064
    const/16 v33, 0x0

    .line 865065
    const/16 v32, 0x0

    .line 865066
    const/16 v31, 0x0

    .line 865067
    const/16 v30, 0x0

    .line 865068
    const/16 v29, 0x0

    .line 865069
    const/16 v28, 0x0

    .line 865070
    const/16 v27, 0x0

    .line 865071
    const/16 v26, 0x0

    .line 865072
    const/16 v25, 0x0

    .line 865073
    const/16 v24, 0x0

    .line 865074
    const/16 v23, 0x0

    .line 865075
    const/16 v22, 0x0

    .line 865076
    const/16 v21, 0x0

    .line 865077
    const/16 v20, 0x0

    .line 865078
    const/16 v19, 0x0

    .line 865079
    const/16 v18, 0x0

    .line 865080
    const/16 v17, 0x0

    .line 865081
    const/16 v16, 0x0

    .line 865082
    const/4 v15, 0x0

    .line 865083
    const/4 v14, 0x0

    .line 865084
    const/4 v13, 0x0

    .line 865085
    const/4 v12, 0x0

    .line 865086
    const/4 v11, 0x0

    .line 865087
    const/4 v10, 0x0

    .line 865088
    const/4 v9, 0x0

    .line 865089
    const/4 v8, 0x0

    .line 865090
    const/4 v7, 0x0

    .line 865091
    const/4 v6, 0x0

    .line 865092
    const/4 v5, 0x0

    .line 865093
    const/4 v4, 0x0

    .line 865094
    const/4 v3, 0x0

    .line 865095
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1

    .line 865096
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 865097
    const/4 v3, 0x0

    .line 865098
    :goto_0
    return v3

    .line 865099
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 865100
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v44

    sget-object v45, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    if-eq v0, v1, :cond_1f

    .line 865101
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v44

    .line 865102
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 865103
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1

    if-eqz v44, :cond_1

    .line 865104
    const-string v45, "__type__"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-nez v45, :cond_2

    const-string v45, "__typename"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_3

    .line 865105
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v43

    goto :goto_1

    .line 865106
    :cond_3
    const-string v45, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_4

    .line 865107
    const/4 v14, 0x1

    .line 865108
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 865109
    :cond_4
    const-string v45, "can_see_voice_switcher"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_5

    .line 865110
    const/4 v13, 0x1

    .line 865111
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 865112
    :cond_5
    const-string v45, "can_viewer_comment"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_6

    .line 865113
    const/4 v12, 0x1

    .line 865114
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 865115
    :cond_6
    const-string v45, "can_viewer_comment_with_photo"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_7

    .line 865116
    const/4 v11, 0x1

    .line 865117
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 865118
    :cond_7
    const-string v45, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_8

    .line 865119
    const/4 v10, 0x1

    .line 865120
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto/16 :goto_1

    .line 865121
    :cond_8
    const-string v45, "can_viewer_comment_with_video"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_9

    .line 865122
    const/4 v9, 0x1

    .line 865123
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 865124
    :cond_9
    const-string v45, "can_viewer_like"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_a

    .line 865125
    const/4 v8, 0x1

    .line 865126
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 865127
    :cond_a
    const-string v45, "can_viewer_react"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_b

    .line 865128
    const/4 v7, 0x1

    .line 865129
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 865130
    :cond_b
    const-string v45, "can_viewer_subscribe"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_c

    .line 865131
    const/4 v6, 0x1

    .line 865132
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 865133
    :cond_c
    const-string v45, "comments"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_d

    .line 865134
    invoke-static/range {p0 .. p1}, LX/5Bg;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 865135
    :cond_d
    const-string v45, "comments_mirroring_domain"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_e

    .line 865136
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 865137
    :cond_e
    const-string v45, "does_viewer_like"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_f

    .line 865138
    const/4 v5, 0x1

    .line 865139
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 865140
    :cond_f
    const-string v45, "id"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_10

    .line 865141
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 865142
    :cond_10
    const-string v45, "important_reactors"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_11

    .line 865143
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 865144
    :cond_11
    const-string v45, "is_viewer_subscribed"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_12

    .line 865145
    const/4 v4, 0x1

    .line 865146
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 865147
    :cond_12
    const-string v45, "legacy_api_post_id"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_13

    .line 865148
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 865149
    :cond_13
    const-string v45, "like_sentence"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_14

    .line 865150
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 865151
    :cond_14
    const-string v45, "likers"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_15

    .line 865152
    invoke-static/range {p0 .. p1}, LX/5Bf;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 865153
    :cond_15
    const-string v45, "reactors"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_16

    .line 865154
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 865155
    :cond_16
    const-string v45, "remixable_photo_uri"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_17

    .line 865156
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 865157
    :cond_17
    const-string v45, "seen_by"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_18

    .line 865158
    invoke-static/range {p0 .. p1}, LX/5Bh;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 865159
    :cond_18
    const-string v45, "supported_reactions"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_19

    .line 865160
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 865161
    :cond_19
    const-string v45, "top_reactions"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_1a

    .line 865162
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 865163
    :cond_1a
    const-string v45, "viewer_acts_as_page"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_1b

    .line 865164
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 865165
    :cond_1b
    const-string v45, "viewer_acts_as_person"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_1c

    .line 865166
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 865167
    :cond_1c
    const-string v45, "viewer_does_not_like_sentence"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_1d

    .line 865168
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 865169
    :cond_1d
    const-string v45, "viewer_feedback_reaction_key"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_1e

    .line 865170
    const/4 v3, 0x1

    .line 865171
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 865172
    :cond_1e
    const-string v45, "viewer_likes_sentence"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_0

    .line 865173
    invoke-static/range {p0 .. p1}, LX/412;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 865174
    :cond_1f
    const/16 v44, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 865175
    const/16 v44, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 865176
    if-eqz v14, :cond_20

    .line 865177
    const/4 v14, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 865178
    :cond_20
    if-eqz v13, :cond_21

    .line 865179
    const/4 v13, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 865180
    :cond_21
    if-eqz v12, :cond_22

    .line 865181
    const/4 v12, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 865182
    :cond_22
    if-eqz v11, :cond_23

    .line 865183
    const/4 v11, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 865184
    :cond_23
    if-eqz v10, :cond_24

    .line 865185
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 865186
    :cond_24
    if-eqz v9, :cond_25

    .line 865187
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 865188
    :cond_25
    if-eqz v8, :cond_26

    .line 865189
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 865190
    :cond_26
    if-eqz v7, :cond_27

    .line 865191
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 865192
    :cond_27
    if-eqz v6, :cond_28

    .line 865193
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 865194
    :cond_28
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 865195
    const/16 v6, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 865196
    if-eqz v5, :cond_29

    .line 865197
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 865198
    :cond_29
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 865199
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 865200
    if-eqz v4, :cond_2a

    .line 865201
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 865202
    :cond_2a
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865203
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865204
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865205
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865206
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865207
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865208
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865209
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865210
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865211
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865212
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 865213
    if-eqz v3, :cond_2b

    .line 865214
    const/16 v3, 0x1b

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1, v4}, LX/186;->a(III)V

    .line 865215
    :cond_2b
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 865216
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
