.class public final LX/5lf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 999112
    const/4 v8, 0x0

    .line 999113
    const/4 v7, 0x0

    .line 999114
    const/4 v6, 0x0

    .line 999115
    const/4 v5, 0x0

    .line 999116
    const/4 v4, 0x0

    .line 999117
    const-wide/16 v2, 0x0

    .line 999118
    const/4 v1, 0x0

    .line 999119
    const/4 v0, 0x0

    .line 999120
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_1

    .line 999121
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 999122
    const/4 v0, 0x0

    .line 999123
    :goto_0
    return v0

    .line 999124
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 999125
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 999126
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 999127
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 999128
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 999129
    const-string v10, "can_viewer_remove_tag"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 999130
    const/4 v1, 0x1

    .line 999131
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 999132
    :cond_2
    const-string v10, "external_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 999133
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 999134
    :cond_3
    const-string v10, "location"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 999135
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 999136
    :cond_4
    const-string v10, "subtitle"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 999137
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 999138
    :cond_5
    const-string v10, "tagger"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 999139
    invoke-static {p0, p1}, LX/5le;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 999140
    :cond_6
    const-string v10, "time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 999141
    const/4 v0, 0x1

    .line 999142
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 999143
    :cond_7
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 999144
    if-eqz v1, :cond_8

    .line 999145
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 999146
    :cond_8
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 999147
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 999148
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 999149
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 999150
    if-eqz v0, :cond_9

    .line 999151
    const/4 v1, 0x5

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 999152
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 999153
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 999154
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999155
    if-eqz v0, :cond_0

    .line 999156
    const-string v1, "can_viewer_remove_tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999157
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999158
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999159
    if-eqz v0, :cond_1

    .line 999160
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999161
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999162
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999163
    if-eqz v0, :cond_2

    .line 999164
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999165
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 999166
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999167
    if-eqz v0, :cond_3

    .line 999168
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999169
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999170
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999171
    if-eqz v0, :cond_4

    .line 999172
    const-string v1, "tagger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999173
    invoke-static {p0, v0, p2}, LX/5le;->a(LX/15i;ILX/0nX;)V

    .line 999174
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 999175
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 999176
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999177
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 999178
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 999179
    return-void
.end method
