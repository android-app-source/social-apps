.class public LX/5Op;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLComment;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 909440
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v2

    .line 909441
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 909442
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 909443
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v3, :cond_0

    .line 909444
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 909445
    :goto_1
    return-object v0

    .line 909446
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 909447
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLFeedback;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 909448
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v2

    .line 909449
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 909450
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 909451
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v3, :cond_0

    .line 909452
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 909453
    :goto_1
    return-object v0

    .line 909454
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 909455
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
