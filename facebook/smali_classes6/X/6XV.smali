.class public LX/6XV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1108443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108444
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 6

    .prologue
    .line 1108445
    const/4 v0, 0x0

    .line 1108446
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1108447
    new-instance v0, LX/4W3;

    invoke-direct {v0}, LX/4W3;-><init>()V

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1108448
    iput-object v1, v0, LX/4W3;->g:Ljava/lang/String;

    .line 1108449
    move-object v0, v0

    .line 1108450
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    .line 1108451
    iput-object v1, v0, LX/4W3;->d:Ljava/lang/String;

    .line 1108452
    move-object v0, v0

    .line 1108453
    invoke-virtual {v0}, LX/4W3;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    .line 1108454
    :cond_0
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-static {v1}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1108455
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-static {v2}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1108456
    new-instance v3, LX/4XR;

    invoke-direct {v3}, LX/4XR;-><init>()V

    .line 1108457
    iput-object v1, v3, LX/4XR;->fm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1108458
    move-object v1, v3

    .line 1108459
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x261131e8

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1108460
    iput-object v3, v1, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1108461
    move-object v1, v1

    .line 1108462
    iput-object v2, v1, LX/4XR;->jM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1108463
    move-object v1, v1

    .line 1108464
    iput-object v0, v1, LX/4XR;->hr:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 1108465
    move-object v0, v1

    .line 1108466
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1108467
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1108468
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 1108469
    iput-object v1, v2, LX/39x;->p:LX/0Px;

    .line 1108470
    move-object v1, v2

    .line 1108471
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    .line 1108472
    iput-object v2, v1, LX/39x;->t:Ljava/lang/String;

    .line 1108473
    move-object v1, v1

    .line 1108474
    iput-object v0, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1108475
    move-object v0, v1

    .line 1108476
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0
.end method
