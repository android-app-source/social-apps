.class public final LX/5Lo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 903047
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 903048
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 903049
    :goto_0
    return v1

    .line 903050
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 903051
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 903052
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 903053
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 903054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 903055
    const-string v8, "previewTemplateAtPlace"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 903056
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 903057
    :cond_2
    const-string v8, "previewTemplateNoTags"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 903058
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 903059
    :cond_3
    const-string v8, "previewTemplateWithPeople"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 903060
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 903061
    :cond_4
    const-string v8, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 903062
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 903063
    :cond_5
    const-string v8, "previewTemplateWithPerson"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 903064
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 903065
    :cond_6
    const-string v8, "previewTemplateWithPersonAtPlace"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 903066
    invoke-static {p0, p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 903067
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 903068
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 903069
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 903070
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 903071
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 903072
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 903073
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 903074
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 903020
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 903021
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903022
    if-eqz v0, :cond_0

    .line 903023
    const-string v1, "previewTemplateAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903024
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903025
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903026
    if-eqz v0, :cond_1

    .line 903027
    const-string v1, "previewTemplateNoTags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903028
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903029
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903030
    if-eqz v0, :cond_2

    .line 903031
    const-string v1, "previewTemplateWithPeople"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903032
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903033
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903034
    if-eqz v0, :cond_3

    .line 903035
    const-string v1, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903036
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903037
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903038
    if-eqz v0, :cond_4

    .line 903039
    const-string v1, "previewTemplateWithPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903040
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903041
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 903042
    if-eqz v0, :cond_5

    .line 903043
    const-string v1, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903044
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903045
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 903046
    return-void
.end method
