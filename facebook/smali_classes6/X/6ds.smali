.class public LX/6ds;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6ds;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cw;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6cw;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1116589
    const-string v15, "threads"

    const/16 v16, 0xc6

    new-instance v2, LX/6dj;

    invoke-direct {v2}, LX/6dj;-><init>()V

    new-instance v3, LX/6dY;

    invoke-direct {v3}, LX/6dY;-><init>()V

    new-instance v4, LX/6da;

    invoke-direct {v4}, LX/6da;-><init>()V

    new-instance v5, LX/6dr;

    invoke-direct {v5}, LX/6dr;-><init>()V

    new-instance v6, LX/6dg;

    invoke-direct {v6}, LX/6dg;-><init>()V

    new-instance v7, LX/6dp;

    invoke-direct {v7}, LX/6dp;-><init>()V

    new-instance v8, LX/6dc;

    invoke-direct {v8}, LX/6dc;-><init>()V

    new-instance v9, LX/6dl;

    invoke-direct {v9}, LX/6dl;-><init>()V

    new-instance v10, LX/6di;

    invoke-direct {v10}, LX/6di;-><init>()V

    new-instance v11, LX/6dn;

    invoke-direct {v11}, LX/6dn;-><init>()V

    new-instance v12, LX/6dW;

    invoke-direct {v12}, LX/6dW;-><init>()V

    new-instance v13, LX/6dU;

    invoke-direct {v13}, LX/6dU;-><init>()V

    const/4 v14, 0x1

    new-array v14, v14, [LX/0Tz;

    const/16 v17, 0x0

    new-instance v18, LX/6de;

    invoke-direct/range {v18 .. v18}, LX/6de;-><init>()V

    aput-object v18, v14, v17

    invoke-static/range {v2 .. v14}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1116590
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/6ds;->a:LX/0Ot;

    .line 1116591
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/6ds;->b:LX/0Ot;

    .line 1116592
    return-void
.end method

.method private static E(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116593
    const-string v1, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "last_read_timestamp_ms"

    const-string v4, "INTEGER"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x6eb92658

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x2f2f1d34

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116594
    const-string v1, "UPDATE %s SET %s = %s WHERE unread = ?"

    const-string v2, "threads"

    const-string v3, "last_read_timestamp_ms"

    const-string v4, "timestamp_ms"

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1116595
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const v3, -0x6e73bccd

    invoke-static {v3}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const v1, -0x7d1b1ddb

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116596
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116597
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "participants"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "participants_flat_buffer"

    const-string v11, "BLOB"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "former_participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "former_participants_flat_buffer"

    const-string v13, "BLOB"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "senders"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x17

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "senders_flat_buffer"

    const-string v17, "BLOB"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender_flat_buffer"

    const-string v17, "BLOB"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "mute_until_flat_buffer"

    const-string v17, "BLOB"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "draft_flat_buffer"

    const-string v17, "BLOB"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x16

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116598
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116599
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, -0x65076f96

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0xe25eeb8

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116600
    return-void
.end method

.method private static L(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116601
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116602
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "participants"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "former_participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "senders"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet_sender"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x14

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "unread_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "cannot_reply_reason"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "custom_like_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116603
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116604
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x3a1d3b97

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x7ba1fcc1

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116605
    return-void
.end method

.method private static V(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 25

    .prologue
    .line 1116606
    new-instance v16, LX/0U1;

    const-string v3, "folder"

    const-string v4, "TEXT"

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116607
    new-instance v3, LX/0U1;

    const-string v4, "unread_count"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "unseen_count"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_seen_time"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_action_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-static {v0, v3, v4, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v17

    .line 1116608
    new-instance v18, LX/0su;

    invoke-static/range {v16 .. v16}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, LX/0su;-><init>(LX/0Px;)V

    .line 1116609
    new-instance v3, LX/0U1;

    const-string v4, "thread_key"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116610
    new-instance v4, LX/0U1;

    const-string v5, "timestamp_ms"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-static {v0, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v19

    .line 1116611
    new-instance v20, LX/0su;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, LX/0su;-><init>(LX/0Px;)V

    .line 1116612
    new-instance v4, LX/0U1;

    const-string v5, "legacy_thread_id"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "refetch_action_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "last_visible_action_id"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "sequence_id"

    const-string v10, "INTEGER"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "name"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "former_participants"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "bot_participants"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v13, LX/0U1;

    const-string v14, "senders"

    const-string v15, "TEXT"

    invoke-direct {v13, v14, v15}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v14, LX/0U1;

    const-string v15, "snippet"

    const-string v21, "TEXT"

    move-object/from16 v0, v21

    invoke-direct {v14, v15, v0}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v15, 0x16

    new-array v15, v15, [LX/0U1;

    const/16 v21, 0x0

    new-instance v22, LX/0U1;

    const-string v23, "snippet_sender"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x1

    new-instance v22, LX/0U1;

    const-string v23, "admin_snippet"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x2

    new-instance v22, LX/0U1;

    const-string v23, "timestamp_ms"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x3

    new-instance v22, LX/0U1;

    const-string v23, "last_read_timestamp_ms"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x4

    new-instance v22, LX/0U1;

    const-string v23, "unread_message_count"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x5

    new-instance v22, LX/0U1;

    const-string v23, "last_fetch_time_ms"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x6

    new-instance v22, LX/0U1;

    const-string v23, "pic_hash"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x7

    new-instance v22, LX/0U1;

    const-string v23, "pic"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x8

    new-instance v22, LX/0U1;

    const-string v23, "can_reply_to"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x9

    new-instance v22, LX/0U1;

    const-string v23, "cannot_reply_reason"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xa

    new-instance v22, LX/0U1;

    const-string v23, "mute_until"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xb

    new-instance v22, LX/0U1;

    const-string v23, "is_subscribed"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xc

    new-instance v22, LX/0U1;

    const-string v23, "folder"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xd

    new-instance v22, LX/0U1;

    const-string v23, "draft"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xe

    new-instance v22, LX/0U1;

    const-string v23, "has_missed_call"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0xf

    new-instance v22, LX/0U1;

    const-string v23, "me_bubble_color"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x10

    new-instance v22, LX/0U1;

    const-string v23, "other_bubble_color"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x11

    new-instance v22, LX/0U1;

    const-string v23, "wallpaper_color"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x12

    new-instance v22, LX/0U1;

    const-string v23, "last_fetch_action_id"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x13

    new-instance v22, LX/0U1;

    const-string v23, "initial_fetch_complete"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x14

    new-instance v22, LX/0U1;

    const-string v23, "custom_like_emoji"

    const-string v24, "TEXT"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    const/16 v21, 0x15

    new-instance v22, LX/0U1;

    const-string v23, "message_lifetime"

    const-string v24, "INTEGER"

    invoke-direct/range {v22 .. v24}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v15, v21

    invoke-static/range {v3 .. v15}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1116613
    new-instance v5, LX/0su;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v5, v3}, LX/0su;-><init>(LX/0Px;)V

    .line 1116614
    const-string v3, ":0123456789"

    invoke-static {v3}, LX/0U1;->f(Ljava/lang/String;)LX/0QK;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-static {v0, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 1116615
    const-string v6, "folder_counts"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v6, v1, v3, v2}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0sv;)V

    .line 1116616
    const-string v6, "folders"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v6, v1, v3, v2}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0sv;)V

    .line 1116617
    sget-object v6, LX/6da;->c:Ljava/lang/String;

    const v7, 0x8084e7c

    invoke-static {v7}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v6, -0x79e478b1

    invoke-static {v6}, LX/03h;->a(I)V

    .line 1116618
    const-string v6, "threads"

    move-object/from16 v0, p0

    invoke-static {v0, v6, v4, v3, v5}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0sv;)V

    .line 1116619
    sget-object v3, LX/6dr;->c:Ljava/lang/String;

    const v4, -0x3b47d678

    invoke-static {v4}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v3, 0x9dbbd2b

    invoke-static {v3}, LX/03h;->a(I)V

    .line 1116620
    return-void
.end method

.method private static Y(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116554
    const-string v1, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "outgoing_message_lifetime"

    const-string v4, "INTEGER"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const v2, -0x4ca07642

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x76dd1507

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116555
    const-string v1, "UPDATE %s SET %s = %s"

    const-string v2, "threads"

    const-string v3, "outgoing_message_lifetime"

    const-string v4, "message_lifetime"

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1116556
    const v2, -0x3c6d9e49

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x7dd25ffd

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116557
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116558
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "participants"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "former_participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "bot_participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "senders"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x16

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "unread_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "cannot_reply_reason"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "custom_like_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "outgoing_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116559
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116560
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, -0x5a3ea9e9

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x7cf0e6a5    # 1.0006634E37f

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116561
    return-void
.end method

.method public static a(LX/0QB;)LX/6ds;
    .locals 5

    .prologue
    .line 1116621
    sget-object v0, LX/6ds;->c:LX/6ds;

    if-nez v0, :cond_1

    .line 1116622
    const-class v1, LX/6ds;

    monitor-enter v1

    .line 1116623
    :try_start_0
    sget-object v0, LX/6ds;->c:LX/6ds;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1116624
    if-eqz v2, :cond_0

    .line 1116625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1116626
    new-instance v3, LX/6ds;

    const/16 v4, 0x2743

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/6ds;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1116627
    move-object v0, v3

    .line 1116628
    sput-object v0, LX/6ds;->c:LX/6ds;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1116629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1116630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1116631
    :cond_1
    sget-object v0, LX/6ds;->c:LX/6ds;

    return-object v0

    .line 1116632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1116633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1116634
    const-string v0, "UPDATE %s SET %s=(SELECT p.%s FROM %s p WHERE %s=REPLACE(p.%s,\'%s\',\'\'))"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "threads"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/2Iu;->b:LX/0U1;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "properties"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, LX/6dq;->a:LX/0U1;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, LX/2Iu;->a:LX/0U1;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x5dbfc87b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x756b8ce8

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116635
    const-string v0, "DELETE FROM %s WHERE key LIKE \'%s%%\'"

    const-string v1, "properties"

    invoke-static {v0, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x61114f88

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x73d1d322

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116636
    return-void
.end method

.method private static aB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12

    .prologue
    .line 1117150
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117151
    new-instance v1, LX/0U1;

    const-string v2, "user_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117152
    const-string v8, "thread_participants"

    const-string v9, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "email"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "type"

    const-string v5, "TEXT NOT NULL"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "is_admin"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_read_receipt_time"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_delivered_receipt_time"

    const-string v10, "INTEGER"

    invoke-direct {v6, v7, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v10, "last_delivered_receipt_id"

    const-string v11, "TEXT"

    invoke-direct {v7, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v0 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    const-string v7, "ON DELETE CASCADE"

    move-object v1, p0

    move-object v2, v8

    move-object v3, v9

    invoke-static/range {v1 .. v7}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1117153
    new-instance v4, LX/0U1;

    const-string v1, "event_reminder_key"

    const-string v2, "TEXT"

    invoke-direct {v4, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117154
    const-string v1, "event_reminders"

    const-string v2, "threads"

    new-instance v3, LX/0U1;

    const-string v5, "event_reminder_timestamp"

    const-string v6, "INTEGER"

    invoke-direct {v3, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "event_reminder_title"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v4, v3, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    const-string v6, "ON DELETE CASCADE"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1117155
    return-void
.end method

.method private static aT(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116637
    new-instance v1, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116638
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "text"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "sender"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "is_not_forwardable"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "timestamp_ms"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "timestamp_sent_ms"

    const-string v10, "INTEGER"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "attachments"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "shares"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "sticker_id"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "msg_type"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x2f

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "affected_users"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "coordinates"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "offline_threading_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "channel_source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "send_channel"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "is_non_authoritative"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "pending_send_media_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "sent_share_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "client_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "send_error"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "send_error_message"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "send_error_number"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "send_error_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "send_error_error_url"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "publicity"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "send_queue_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "payment_transaction"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "payment_request"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "has_unavailable_attachment"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "app_attribution"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "content_app_attribution"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x16

    new-instance v15, LX/0U1;

    const-string v16, "xma"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x17

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_type"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x18

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_theme_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x19

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_icon_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_nickname"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_target_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_color_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_emoji_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_nickname_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x20

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_bot_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x21

    new-instance v15, LX/0U1;

    const-string v16, "message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x22

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_event"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x23

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_server_info_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x24

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_is_video_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x25

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_ride_provider_name"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x26

    new-instance v15, LX/0U1;

    const-string v16, "is_sponsored"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x27

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_ad_properties"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x28

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_game_score_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x29

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_event_reminder_properties"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2a

    new-instance v15, LX/0U1;

    const-string v16, "commerce_message_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2b

    new-instance v15, LX/0U1;

    const-string v16, "customizations"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2c

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_joinable_event_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2d

    new-instance v15, LX/0U1;

    const-string v16, "metadata_at_text_ranges"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2e

    new-instance v15, LX/0U1;

    const-string v16, "platform_metadata"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116639
    const-string v3, "messages"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116640
    sget-object v1, LX/6dg;->c:Ljava/lang/String;

    const v2, 0x62415116

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x11ea1900

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116641
    sget-object v1, LX/6dg;->d:Ljava/lang/String;

    const v2, 0x6192dba6

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0xcb83a25

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116642
    sget-object v1, LX/6dg;->e:Ljava/lang/String;

    const v2, 0x1b4ab972

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x11dcf9bc

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116643
    return-void
.end method

.method private static aW(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14

    .prologue
    .line 1116644
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116645
    new-instance v1, LX/0U1;

    const-string v2, "user_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116646
    new-instance v4, LX/0U1;

    const-string v2, "type"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v4, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116647
    const-string v10, "thread_participants"

    const-string v11, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "email"

    const-string v5, "TEXT"

    invoke-direct {v2, v3, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v5, "phone"

    const-string v6, "TEXT"

    invoke-direct {v3, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "is_admin"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_read_receipt_time"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "last_delivered_receipt_time"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "last_delivered_receipt_id"

    const-string v12, "TEXT"

    invoke-direct {v8, v9, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v12, "request_timestamp_ms"

    const-string v13, "INTEGER"

    invoke-direct {v9, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v0, v1, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    const-string v6, "ON DELETE CASCADE"

    move-object v0, p0

    move-object v1, v10

    move-object v2, v11

    invoke-static/range {v0 .. v6}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1116648
    return-void
.end method

.method private static ak(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7

    .prologue
    .line 1116649
    const-string v0, "SELECT msg_id, xma FROM messages WHERE xma IS NOT NULL;"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1116650
    if-eqz v6, :cond_3

    .line 1116651
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1116652
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1116653
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1116654
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1116655
    const-string v1, "story_attachment"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_4

    .line 1116656
    :cond_1
    :goto_0
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1116657
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1116658
    :cond_3
    return-void

    .line 1116659
    :cond_4
    :try_start_3
    const-string v1, "story_attachment"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1116660
    const-string v3, "properties"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1116661
    const-string v3, "properties"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1116662
    const-string v4, "properties"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 1116663
    const-string v4, "attachment_properties"

    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1116664
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1116665
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1116666
    const-string v3, "xma"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116667
    const-string v1, "messages"

    const-string v3, "msg_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/database/sqlite/SQLiteDatabase;->updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1116668
    :catch_0
    move-exception v0

    .line 1116669
    :try_start_4
    const-string v1, "ThreadsDbSchemaPart"

    const-string v2, "JSONException"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1116670
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1116671
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static al(LX/6ds;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116672
    new-instance v8, LX/0su;

    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, LX/0U1;

    const-string v3, "user_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v8, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 1116673
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, LX/0U1;

    const-string v3, "user_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "email"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "type"

    const-string v6, "TEXT NOT NULL"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_read_receipt_time"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_delivered_receipt_time"

    const-string v9, "INTEGER"

    invoke-direct {v6, v7, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v9, "last_delivered_receipt_id"

    const-string v10, "TEXT"

    invoke-direct {v7, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v1 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1116674
    const-string v2, "thread_participants"

    invoke-static {v8}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v2, v1, v3}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x739eba7a

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5e14b4c9

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116675
    move-object/from16 v0, p0

    iget-object v1, v0, LX/6ds;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6cw;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1116676
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116677
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "senders"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "snippet"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "snippet_sender"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "admin_snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "timestamp_ms"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x16

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "approx_total_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "unread_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "cannot_reply_reason"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "custom_like_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "outgoing_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "custom_nicknames"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "invite_uri"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116678
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p1

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116679
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x6717a580

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x78e42fe4

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116680
    return-void
.end method

.method private static az(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1116681
    new-instance v6, LX/0lB;

    invoke-direct {v6, v1}, LX/0lB;-><init>(LX/0lp;)V

    .line 1116682
    new-instance v0, LX/0lp;

    invoke-direct {v0, v6}, LX/0lp;-><init>(LX/0lD;)V

    .line 1116683
    const-string v0, "SELECT msg_id, xma FROM messages WHERE xma IS NOT NULL;"

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1116684
    if-eqz v7, :cond_3

    .line 1116685
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1116686
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1116687
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1116688
    :try_start_1
    invoke-virtual {v6, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1116689
    const-string v0, "story_attachment"

    invoke-virtual {v2, v0}, LX/0lF;->c(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_4

    .line 1116690
    :cond_1
    :goto_0
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1116691
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1116692
    :cond_3
    return-void

    .line 1116693
    :cond_4
    :try_start_3
    const-string v0, "story_attachment"

    invoke-virtual {v2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    .line 1116694
    const-string v1, "large_media"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "media"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1116695
    const-string v1, "media"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    check-cast v1, LX/0m9;

    .line 1116696
    const-string v3, "large_media"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 1116697
    const-string v4, "large_media"

    invoke-virtual {v0, v4}, LX/0m9;->g(Ljava/lang/String;)LX/0lF;

    .line 1116698
    const-string v0, "image"

    invoke-virtual {v3, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1116699
    const-string v0, "image"

    invoke-virtual {v3, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1116700
    const-string v3, "imageLarge"

    invoke-virtual {v1, v3, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1116701
    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1116702
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1116703
    const-string v1, "xma"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116704
    const-string v1, "messages"

    const-string v3, "msg_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v5, v4, v0

    const/4 v5, 0x4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/database/sqlite/SQLiteDatabase;->updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1116705
    :catch_0
    move-exception v0

    .line 1116706
    :try_start_4
    const-string v1, "ThreadsDbSchemaPart"

    const-string v2, "JSONException"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1116707
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1116708
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;II)I
    .locals 11

    .prologue
    const/16 v2, 0x5a

    const/16 v0, 0x42

    .line 1116709
    add-int/lit8 v1, p2, 0x1

    .line 1116710
    const/16 v3, 0x3e

    if-ne p2, v3, :cond_0

    .line 1116711
    invoke-static {p1}, LX/6ds;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v0

    .line 1116712
    :goto_0
    return p3

    .line 1116713
    :cond_0
    if-eq p2, v0, :cond_84

    .line 1116714
    const/16 v0, 0x43

    if-ne p2, v0, :cond_1

    .line 1116715
    const-string v0, "ALTER TABLE threads ADD COLUMN num_unread INTEGER"

    const v2, -0x4f48017a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x1afd2363

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116716
    move p3, v1

    goto :goto_0

    .line 1116717
    :cond_1
    const/16 v0, 0x44

    if-ne p2, v0, :cond_2

    .line 1116718
    const-string v0, "ALTER TABLE threads ADD COLUMN last_visitied_ms INTEGER"

    const v2, -0x32936d04

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xd7ee45e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116719
    const-string v0, "ALTER TABLE threads ADD COLUMN last_visitied_ms_type INTEGER"

    const v2, -0x4a8973ec

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6c2a64fd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116720
    move p3, v1

    goto :goto_0

    .line 1116721
    :cond_2
    const/16 v0, 0x45

    if-ne p2, v0, :cond_3

    .line 1116722
    const-string v0, "folder"

    sget-object v2, LX/6ek;->NONE:LX/6ek;

    iget-object v2, v2, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 1116723
    const-string v2, "threads"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1116724
    move p3, v1

    goto :goto_0

    .line 1116725
    :cond_3
    const/16 v0, 0x46

    if-ne p2, v0, :cond_4

    .line 1116726
    const-string v0, "ALTER TABLE messages ADD COLUMN payment_transaction TEXT"

    const v2, -0x7d3ee406

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x452ba534

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116727
    move p3, v1

    goto :goto_0

    .line 1116728
    :cond_4
    const/16 v0, 0x47

    if-ne p2, v0, :cond_5

    .line 1116729
    invoke-static {p1}, LX/6ds;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto :goto_0

    .line 1116730
    :cond_5
    const/16 v0, 0x48

    if-ne p2, v0, :cond_6

    .line 1116731
    invoke-static {p1}, LX/6ds;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116732
    :cond_6
    const/16 v0, 0x49

    if-ne p2, v0, :cond_7

    .line 1116733
    invoke-static {p1}, LX/6ds;->k(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116734
    :cond_7
    const/16 v0, 0x4a

    if-ne p2, v0, :cond_8

    .line 1116735
    const-string v0, "ALTER TABLE messages ADD COLUMN has_unavailable_attachment INTEGER"

    const v2, 0x3cfd2918

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7b2949c3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116736
    move p3, v1

    goto/16 :goto_0

    .line 1116737
    :cond_8
    const/16 v0, 0x4b

    if-ne p2, v0, :cond_9

    .line 1116738
    invoke-static {p1}, LX/6ds;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116739
    :cond_9
    const/16 v0, 0x4c

    if-ne p2, v0, :cond_a

    .line 1116740
    const-string v0, "ALTER TABLE messages ADD COLUMN send_error_error_url STRING"

    const v2, -0x478381a1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x19af6091

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116741
    move p3, v1

    goto/16 :goto_0

    .line 1116742
    :cond_a
    const/16 v0, 0x4d

    if-ne p2, v0, :cond_b

    .line 1116743
    const-string v0, "ALTER TABLE messages ADD COLUMN app_attribution TEXT"

    const v2, -0xd8cd9e1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4b4bf380    # 1.3366144E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116744
    move p3, v1

    goto/16 :goto_0

    .line 1116745
    :cond_b
    const/16 v0, 0x4e

    if-ne p2, v0, :cond_c

    .line 1116746
    invoke-static {p1}, LX/6ds;->p(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116747
    :cond_c
    const/16 v0, 0x4f

    if-ne p2, v0, :cond_d

    .line 1116748
    invoke-static {p1}, LX/6ds;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116749
    :cond_d
    const/16 v0, 0x50

    if-ne p2, v0, :cond_e

    .line 1116750
    invoke-static {p1}, LX/6ds;->r(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116751
    :cond_e
    const/16 v0, 0x51

    if-ne p2, v0, :cond_f

    .line 1116752
    const-string v0, "ALTER TABLE messages ADD COLUMN content_app_attribution TEXT"

    const v2, -0x1b116c92

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x4b3cacb0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116753
    move p3, v1

    goto/16 :goto_0

    .line 1116754
    :cond_f
    const/16 v0, 0x52

    if-ne p2, v0, :cond_10

    .line 1116755
    invoke-static {p1}, LX/6ds;->t(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116756
    :cond_10
    const/16 v0, 0x53

    if-ne p2, v0, :cond_11

    .line 1116757
    const-string v0, "UPDATE messages SET msg_id = replace(msg_id, \'m_mid\', \'mid\') WHERE msg_id LIKE \'m_mid%\'"

    const v2, 0x123acc4b

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xfbb3093

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116758
    move p3, v1

    goto/16 :goto_0

    .line 1116759
    :cond_11
    const/16 v0, 0x54

    if-ne p2, v0, :cond_12

    .line 1116760
    const-string v0, "group_clusters"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x6a4854f7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x431d9d44

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116761
    move p3, v1

    goto/16 :goto_0

    .line 1116762
    :cond_12
    const/16 v0, 0x55

    if-ne p2, v0, :cond_13

    .line 1116763
    const-string v0, "ALTER TABLE messages ADD COLUMN xma TEXT"

    const v2, 0x40501eec

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x5f4acf7e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116764
    move p3, v1

    goto/16 :goto_0

    .line 1116765
    :cond_13
    const/16 v0, 0x56

    if-ne p2, v0, :cond_14

    .line 1116766
    const-string v0, "ALTER TABLE thread_users ADD is_commerce INTEGER"

    const v2, 0x438f592b

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x59e4b0f1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116767
    move p3, v1

    goto/16 :goto_0

    .line 1116768
    :cond_14
    const/16 v0, 0x57

    if-ne p2, v0, :cond_15

    .line 1116769
    const-string v0, "ALTER TABLE messages ADD COLUMN is_not_forwardable INTEGER"

    const v2, 0x626a33ab

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x497aca06

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116770
    move p3, v1

    goto/16 :goto_0

    .line 1116771
    :cond_15
    const/16 v0, 0x58

    if-ne p2, v0, :cond_16

    move p3, v2

    .line 1116772
    goto/16 :goto_0

    .line 1116773
    :cond_16
    const/16 v0, 0x59

    if-ne p2, v0, :cond_17

    .line 1116774
    invoke-static {p1}, LX/6ds;->z(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116775
    :cond_17
    if-ne p2, v2, :cond_18

    .line 1116776
    const-string v0, "ALTER TABLE thread_users ADD is_partial INTEGER"

    const v2, 0x5fcddf29

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x314bc785

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116777
    const-string v0, "ALTER TABLE thread_users ADD user_rank REAL"

    const v2, -0x481c2034

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xf99247c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116778
    move p3, v1

    goto/16 :goto_0

    .line 1116779
    :cond_18
    const/16 v0, 0x5b

    if-ne p2, v0, :cond_19

    .line 1116780
    const-string v0, "ALTER TABLE threads ADD COLUMN participants_flat_buffer BLOB"

    const v2, -0x6184ab7e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3a422e1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116781
    const-string v0, "ALTER TABLE threads ADD COLUMN former_participants_flat_buffer BLOB"

    const v2, -0x141bdb92

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x57b4d8b5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116782
    const-string v0, "ALTER TABLE threads ADD COLUMN senders_flat_buffer BLOB"

    const v2, -0x76372830

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xa0a30a2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116783
    const-string v0, "ALTER TABLE threads ADD COLUMN snippet_sender_flat_buffer BLOB"

    const v2, -0x18acbcdf

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7dd9986d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116784
    const-string v0, "ALTER TABLE threads ADD COLUMN mute_until_flat_buffer BLOB"

    const v2, -0x2980e15a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x44e44ca7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116785
    const-string v0, "ALTER TABLE threads ADD COLUMN draft_flat_buffer BLOB"

    const v2, -0x67de2b2

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7aff72e9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116786
    move p3, v1

    goto/16 :goto_0

    .line 1116787
    :cond_19
    const/16 v0, 0x5c

    if-ne p2, v0, :cond_1a

    .line 1116788
    const-string v0, "ALTER TABLE threads ADD COLUMN me_bubble_color INTEGER"

    const v2, -0x3b3c68d7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3e6613f5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116789
    const-string v0, "ALTER TABLE threads ADD COLUMN other_bubble_color INTEGER"

    const v2, -0x7f4db4c2

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x43f1c8c2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116790
    const-string v0, "ALTER TABLE threads ADD COLUMN wallpaper_color INTEGER"

    const v2, 0x5ea85532

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x47945749

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116791
    move p3, v1

    goto/16 :goto_0

    .line 1116792
    :cond_1a
    const/16 v0, 0x5d

    if-ne p2, v0, :cond_1b

    .line 1116793
    const-string v0, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "last_fetch_action_id"

    const-string p0, "INTEGER DEFAULT -1"

    invoke-direct {v2, v3, p0}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x1fd15a05

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7d4a5e7b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116794
    const-string v0, "threads"

    new-instance v2, LX/0U1;

    const-string v3, "initial_fetch_complete"

    const-string p0, "INTEGER"

    invoke-direct {v2, v3, p0}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v0

    const v2, -0x5cdda38d

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x1cc51eda

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116795
    const-string v0, "/sync/last_thread_fetch_action_id/"

    const-string v2, "last_fetch_action_id"

    invoke-static {p1, v0, v2}, LX/6ds;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1116796
    const-string v0, "thread_initial_fetch_complete/"

    const-string v2, "initial_fetch_complete"

    invoke-static {p1, v0, v2}, LX/6ds;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1116797
    move p3, v1

    goto/16 :goto_0

    .line 1116798
    :cond_1b
    const/16 v0, 0x5e

    if-ne p2, v0, :cond_1c

    .line 1116799
    invoke-static {p1}, LX/6ds;->E(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116800
    :cond_1c
    const/16 v0, 0x5f

    if-ne p2, v0, :cond_1d

    .line 1116801
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_type INTEGER"

    const v2, -0x1a363940

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xd9f7d5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116802
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_theme_color INTEGER"

    const v2, 0x222c6c69

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3070e02f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116803
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_icon_emoji TEXT"

    const v2, 0x550acfef

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x781e1da5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116804
    move p3, v1

    goto/16 :goto_0

    .line 1116805
    :cond_1d
    const/16 v0, 0x60

    if-ne p2, v0, :cond_1e

    .line 1116806
    const-string v0, "ALTER TABLE threads ADD COLUMN custom_like_emoji TEXT"

    const v2, -0x68187170

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x73b4102e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116807
    move p3, v1

    goto/16 :goto_0

    .line 1116808
    :cond_1e
    const/16 v0, 0x61

    if-ne p2, v0, :cond_1f

    .line 1116809
    const-string v4, "threads"

    new-instance v5, LX/0U1;

    const-string v6, "unread_message_count"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4, v5}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x1a3fd7e5

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v4, -0x42c0de30

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1116810
    const-string v4, "UPDATE %s SET %s = ? WHERE %s < %s"

    const-string v5, "threads"

    const-string v6, "unread_message_count"

    const-string v7, "last_read_timestamp_ms"

    const-string v8, "timestamp_ms"

    invoke-static {v4, v5, v6, v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1116811
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const v6, 0x588195a6

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const v4, -0xdcc2337

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1116812
    move p3, v1

    goto/16 :goto_0

    .line 1116813
    :cond_1f
    const/16 v0, 0x62

    if-ne p2, v0, :cond_20

    .line 1116814
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_blocked_by_viewer INTEGER"

    const v2, 0x4ce7870e    # 1.2138712E8f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x15629fb

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116815
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_message_blocked_by_viewer INTEGER"

    const v2, -0x356f30ed    # -4745097.5f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x205e0540

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116816
    move p3, v1

    goto/16 :goto_0

    .line 1116817
    :cond_20
    const/16 v0, 0x63

    if-ne p2, v0, :cond_21

    .line 1116818
    const-string v0, "ALTER TABLE thread_users ADD COLUMN commerce_page_type TEXT"

    const v2, -0x67883d5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2f6905cf

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116819
    move p3, v1

    goto/16 :goto_0

    .line 1116820
    :cond_21
    const/16 v0, 0x64

    if-ne p2, v0, :cond_22

    .line 1116821
    const-string v0, "ALTER TABLE threads ADD COLUMN cannot_reply_reason TEXT"

    const v2, 0x6e2e23f3

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2ca20763

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116822
    move p3, v1

    goto/16 :goto_0

    .line 1116823
    :cond_22
    const/16 v0, 0x65

    if-ne p2, v0, :cond_23

    .line 1116824
    invoke-static {p1}, LX/6ds;->L(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116825
    :cond_23
    const/16 v0, 0x66

    if-ne p2, v0, :cond_24

    .line 1116826
    const-string v0, "ALTER TABLE threads ADD COLUMN message_lifetime INTEGER"

    const v2, -0x567bd3f0

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x20162627

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116827
    move p3, v1

    goto/16 :goto_0

    .line 1116828
    :cond_24
    const/16 v0, 0x67

    if-ne p2, v0, :cond_25

    .line 1116829
    const-string v0, "ALTER TABLE thread_users ADD COLUMN can_viewer_message INTEGER"

    const v2, 0x47cd1d7a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3e139665

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116830
    move p3, v1

    goto/16 :goto_0

    .line 1116831
    :cond_25
    const/16 v0, 0x68

    if-ne p2, v0, :cond_26

    .line 1116832
    const-string v0, "ALTER TABLE messages ADD COLUMN message_lifetime INTEGER"

    const v2, 0x77eee1af

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7cdf7ec2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116833
    move p3, v1

    goto/16 :goto_0

    .line 1116834
    :cond_26
    const/16 v0, 0x69

    if-ne p2, v0, :cond_27

    .line 1116835
    const-string v0, "ALTER TABLE thread_users ADD COLUMN commerce_page_settings TEXT"

    const v2, -0x30ff6ee

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7c95e9b5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116836
    move p3, v1

    goto/16 :goto_0

    .line 1116837
    :cond_27
    const/16 v0, 0x6a

    if-ne p2, v0, :cond_28

    .line 1116838
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_message_lifetime INTEGER"

    const v2, -0x124f65cb

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x370ecf37

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116839
    move p3, v1

    goto/16 :goto_0

    .line 1116840
    :cond_28
    const/16 v0, 0x6b

    if-ne p2, v0, :cond_29

    .line 1116841
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_friend INTEGER"

    const v2, -0x1402a7ae

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x45c2a433

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116842
    move p3, v1

    goto/16 :goto_0

    .line 1116843
    :cond_29
    const/16 v0, 0x6c

    if-ne p2, v0, :cond_2a

    .line 1116844
    const-string v0, "ALTER TABLE thread_users ADD COLUMN last_fetch_time INTEGER"

    const v2, -0x2c3d2dc5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4db0e1f0    # 3.70949632E8f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116845
    move p3, v1

    goto/16 :goto_0

    .line 1116846
    :cond_2a
    const/16 v0, 0x6d

    if-ne p2, v0, :cond_2b

    .line 1116847
    const-string v0, "ALTER TABLE thread_users ADD COLUMN montage_thread_fbid TEXT"

    const v2, -0x712f47ab

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x56977f01

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116848
    const-string v0, "ALTER TABLE thread_users ADD COLUMN can_see_viewer_montage_thread INTEGER"

    const v2, 0x4dcb9b1d    # 4.26992544E8f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x15594b09

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116849
    move p3, v1

    goto/16 :goto_0

    .line 1116850
    :cond_2b
    const/16 v0, 0x6e

    if-ne p2, v0, :cond_2c

    .line 1116851
    const-string v0, "ALTER TABLE threads ADD COLUMN bot_participants TEXT"

    const v2, -0x3c4c2ceb

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x339b3d9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116852
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_messenger_bot INTEGER"

    const v2, -0x66b55204

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x523c0b91

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116853
    move p3, v1

    goto/16 :goto_0

    .line 1116854
    :cond_2c
    const/16 v0, 0x6f

    if-ne p2, v0, :cond_2d

    .line 1116855
    invoke-static {p1}, LX/6ds;->V(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116856
    :cond_2d
    const/16 v0, 0x70

    if-ne p2, v0, :cond_2e

    .line 1116857
    const-string v0, "ALTER TABLE messages ADD send_channel TEXT"

    const v2, -0x27926db1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5bc46682

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116858
    move p3, v1

    goto/16 :goto_0

    .line 1116859
    :cond_2e
    const/16 v0, 0x71

    if-ne p2, v0, :cond_2f

    .line 1116860
    const-string v0, "ALTER TABLE messages ADD send_error_number INTEGER"

    const v2, 0x5cce39f2

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x172e29ba

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116861
    move p3, v1

    goto/16 :goto_0

    .line 1116862
    :cond_2f
    const/16 v0, 0x72

    if-ne p2, v0, :cond_30

    .line 1116863
    invoke-static {p1}, LX/6ds;->Y(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116864
    :cond_30
    const/16 v0, 0x73

    if-ne p2, v0, :cond_31

    .line 1116865
    const-string v0, "ALTER TABLE threads ADD COLUMN approx_total_message_count INTEGER"

    const v2, -0x3e39d194

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2e349a81

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116866
    const-string v0, "UPDATE threads SET approx_total_message_count=1"

    const v2, 0x38cd833f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7397f301

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116867
    move p3, v1

    goto/16 :goto_0

    .line 1116868
    :cond_31
    const/16 v0, 0x74

    if-ne p2, v0, :cond_32

    .line 1116869
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_journey_color_choices TEXT"

    const v2, -0xa5d2b82

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6631e189

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116870
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_journey_emoji_choices TEXT"

    const v2, 0x65059a60

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x35442ec4    # -6154398.0f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116871
    move p3, v1

    goto/16 :goto_0

    .line 1116872
    :cond_32
    const/16 v0, 0x75

    if-ne p2, v0, :cond_33

    .line 1116873
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_journey_nickname_choices TEXT"

    const v2, -0x1783512b

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x74d8fcea

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116874
    move p3, v1

    goto/16 :goto_0

    .line 1116875
    :cond_33
    const/16 v0, 0x76

    if-ne p2, v0, :cond_34

    .line 1116876
    const-string v0, "ALTER TABLE threads ADD COLUMN custom_nicknames TEXT"

    const v2, 0x1e7d8f61

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6636824b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116877
    move p3, v1

    goto/16 :goto_0

    .line 1116878
    :cond_34
    const/16 v0, 0x77

    if-ne p2, v0, :cond_35

    .line 1116879
    const-string v0, "ALTER TABLE threads ADD COLUMN invite_uri TEXT"

    const v2, 0x7ea621ed    # 1.10414E38f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x81ffe84

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116880
    move p3, v1

    goto/16 :goto_0

    .line 1116881
    :cond_35
    const/16 v0, 0x78

    if-ne p2, v0, :cond_36

    .line 1116882
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_messenger_promotion_blocked_by_viewer INTEGER"

    const v2, 0x2813db9a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x59ef1a48

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116883
    move p3, v1

    goto/16 :goto_0

    .line 1116884
    :cond_36
    const/16 v0, 0x79

    if-ne p2, v0, :cond_37

    .line 1116885
    const-string v0, "ALTER TABLE messages ADD COLUMN payment_request TEXT"

    const v2, 0x3552b2cb

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x23fd58d3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116886
    move p3, v1

    goto/16 :goto_0

    .line 1116887
    :cond_37
    const/16 v0, 0x7a

    if-ne p2, v0, :cond_38

    .line 1116888
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_rtc_event TEXT"

    const v2, -0x792b47c6

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2028ff40

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116889
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_rtc_server_info_data TEXT"

    const v2, -0x55eb127

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xb3fed77

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116890
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_rtc_is_video_call INTEGER"

    const v2, 0x5ed6da05

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6b55fd31

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116891
    move p3, v1

    goto/16 :goto_0

    .line 1116892
    :cond_38
    const/16 v0, 0x7b

    if-ne p2, v0, :cond_39

    .line 1116893
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_target_id TEXT"

    const v2, -0x45b51888    # -7.74018E-4f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x282f9fc2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116894
    move p3, v1

    goto/16 :goto_0

    .line 1116895
    :cond_39
    const/16 v0, 0x7c

    if-ne p2, v0, :cond_3a

    .line 1116896
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_nickname TEXT"

    const v2, 0x71e127c3

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6a370844

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116897
    move p3, v1

    goto/16 :goto_0

    .line 1116898
    :cond_3a
    const/16 v0, 0x7d

    if-ne p2, v0, :cond_3b

    .line 1116899
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_journey_bot_choices TEXT"

    const v2, 0x31d20422

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x50e43241

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116900
    move p3, v1

    goto/16 :goto_0

    .line 1116901
    :cond_3b
    const/16 v0, 0x7e

    if-ne p2, v0, :cond_3c

    .line 1116902
    invoke-static {p1}, LX/6ds;->ak(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116903
    :cond_3c
    const/16 v0, 0x7f

    if-ne p2, v0, :cond_3e

    .line 1116904
    :try_start_0
    invoke-static {p0, p1}, LX/6ds;->al(LX/6ds;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move p3, v1

    .line 1116905
    goto/16 :goto_0

    .line 1116906
    :catch_0
    move-exception v1

    .line 1116907
    iget-object v0, p0, LX/6ds;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "DbThreadParticipantsUtilParticipantsColumnUpgradeFail"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1116908
    :cond_3d
    const-string v0, "users"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x16779b36

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x662aca81

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116909
    const-string v0, "friends"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x67e464e0

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x34b58060    # -1.3270944E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116910
    const-string v0, "archived_sms_mms_threads"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x5e3338d9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x357555ab    # -4543786.5f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116911
    const-string v0, "unread_mms_sms_threads"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x3fb7afa7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x55e0485c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116912
    const-string v0, "group_clusters"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x37ce1cfa

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2de3131f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116913
    const-string v0, "properties"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x63c5e762

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2606afe2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116914
    const-string v0, "folder_counts"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x5e259260

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2980b493

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116915
    const-string v0, "folders"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x47ec2c11

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x77a860f6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116916
    const-string v0, "threads"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x51f7de43

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x117af9a8

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116917
    const-string v0, "messages"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x4b41994

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x49cb07f8

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116918
    const-string v0, "thread_users"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x631a13da

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3411aff7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116919
    const-string v0, "group_conversations"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x749f7bf1

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x13f1d84f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116920
    const-string v0, "pinned_threads"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x526913be

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3975daad

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116921
    const-string v0, "ranked_threads"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x55a55838

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x56a95b45

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116922
    const-string v0, "thread_participants"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x5f142d2d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x823f7b9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116923
    const-string v0, "message_reactions"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x8b3cd46

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x657e3938

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116924
    const-string v0, "event_reminders"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x75cb3720

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x43343dae

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116925
    const-string v0, "event_reminder_members"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x19499e8e

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5d0b850c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116926
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .line 1116927
    :cond_3e
    const/16 v0, 0x80

    if-ne p2, v0, :cond_3f

    .line 1116928
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_ride_provider_name TEXT"

    const v2, 0x1e1abf40

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7f72db00

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116929
    move p3, v1

    goto/16 :goto_0

    .line 1116930
    :cond_3f
    const/16 v0, 0x81

    if-ne p2, v0, :cond_40

    .line 1116931
    const-string v0, "ALTER TABLE thread_users ADD COLUMN username TEXT"

    const v2, -0x3d5066a7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7826e02a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116932
    move p3, v1

    goto/16 :goto_0

    .line 1116933
    :cond_40
    const/16 v0, 0x82

    if-ne p2, v0, :cond_41

    .line 1116934
    const-string v0, "ALTER TABLE thread_participants ADD COLUMN is_admin INTEGER"

    const v2, 0x1f1eca29

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x1fc6e838

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116935
    move p3, v1

    goto/16 :goto_0

    .line 1116936
    :cond_41
    const/16 v0, 0x83

    if-ne p2, v0, :cond_42

    .line 1116937
    const-string v0, "ALTER TABLE group_conversations ADD COLUMN group_chat_rank FLOAT"

    const v2, 0x1e2d0a82

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x4cc72f83

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116938
    move p3, v1

    goto/16 :goto_0

    .line 1116939
    :cond_42
    const/16 v0, 0x84

    if-ne p2, v0, :cond_43

    .line 1116940
    const-string v0, "ALTER TABLE threads ADD COLUMN is_last_message_sponsored INTEGER"

    const v2, 0x14a83301

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7655c1d5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116941
    move p3, v1

    goto/16 :goto_0

    .line 1116942
    :cond_43
    const/16 v0, 0x85

    if-ne p2, v0, :cond_44

    .line 1116943
    const-string v0, "ALTER TABLE messages ADD COLUMN is_sponsored INTEGER"

    const v2, -0xf2d4601

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x20107dce

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116944
    move p3, v1

    goto/16 :goto_0

    .line 1116945
    :cond_44
    const/16 v0, 0x86

    if-ne p2, v0, :cond_45

    .line 1116946
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_ad_properties TEXT"

    const v2, -0x38d2d1aa

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x4040624

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116947
    move p3, v1

    goto/16 :goto_0

    .line 1116948
    :cond_45
    const/16 v0, 0x87

    if-ne p2, v0, :cond_46

    .line 1116949
    const-string v0, "ALTER TABLE threads ADD COLUMN group_chat_rank FLOAT"

    const v2, -0x401e4342

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x305764d9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116950
    move p3, v1

    goto/16 :goto_0

    .line 1116951
    :cond_46
    const/16 v0, 0x88

    if-ne p2, v0, :cond_47

    .line 1116952
    new-instance v0, LX/0su;

    new-instance v2, LX/0U1;

    const-string v3, "event_reminder_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0su;-><init>(LX/0Px;)V

    .line 1116953
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "event_reminder_key"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "event_reminder_timestamp"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116954
    const-string v3, "event_reminders"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v3, v2, v0}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v2, -0x2f2d505c

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x74a93c97

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116955
    move p3, v1

    goto/16 :goto_0

    .line 1116956
    :cond_47
    const/16 v0, 0x89

    if-ne p2, v0, :cond_48

    .line 1116957
    const-string v0, "ALTER TABLE threads ADD COLUMN game_data TEXT"

    const v2, 0x4ded5b7c    # 4.97774464E8f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x21b3a6c6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116958
    move p3, v1

    goto/16 :goto_0

    .line 1116959
    :cond_48
    const/16 v0, 0x8a

    if-ne p2, v0, :cond_49

    .line 1116960
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_game_score_data TEXT"

    const v2, -0x71bea6d7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x93fd664

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116961
    move p3, v1

    goto/16 :goto_0

    .line 1116962
    :cond_49
    const/16 v0, 0x8b

    if-ne p2, v0, :cond_4a

    .line 1116963
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_thread_event_reminder_properties TEXT"

    const v2, 0x33fbae89

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x245958c3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116964
    move p3, v1

    goto/16 :goto_0

    .line 1116965
    :cond_4a
    const/16 v0, 0x8c

    if-ne p2, v0, :cond_4b

    .line 1116966
    const-string v0, "ALTER TABLE thread_users ADD COLUMN user_custom_tags TEXT"

    const v2, -0x6a3d85a5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x570c3b1e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116967
    move p3, v1

    goto/16 :goto_0

    .line 1116968
    :cond_4b
    const/16 v0, 0x8d

    if-ne p2, v0, :cond_4c

    .line 1116969
    invoke-static {p1}, LX/6ds;->az(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116970
    :cond_4c
    const/16 v0, 0x8e

    if-ne p2, v0, :cond_4d

    .line 1116971
    const-string v0, "ALTER TABLE event_reminders ADD COLUMN event_reminder_title TEXT"

    const v2, 0x2de9ac16

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x30822bfd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116972
    move p3, v1

    goto/16 :goto_0

    .line 1116973
    :cond_4d
    const/16 v0, 0x8f

    if-ne p2, v0, :cond_4e

    .line 1116974
    invoke-static {p1}, LX/6ds;->aB(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1116975
    :cond_4e
    const/16 v0, 0x90

    if-ne p2, v0, :cond_4f

    .line 1116976
    const-string v0, "ALTER TABLE threads ADD COLUMN group_type TEXT DEFAULT \'private\'"

    const v2, -0x22ac8be9

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3d4e6d9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116977
    const-string v0, "ALTER TABLE threads ADD COLUMN requires_approval INTEGER DEFAULT 0"

    const v2, -0x6726cbf1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x69c3a81a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116978
    move p3, v1

    goto/16 :goto_0

    .line 1116979
    :cond_4f
    const/16 v0, 0x91

    if-ne p2, v0, :cond_50

    .line 1116980
    const-string v0, "ALTER TABLE threads ADD COLUMN rtc_call_info TEXT"

    const v2, -0x5c7b2251

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x53506ac0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116981
    move p3, v1

    goto/16 :goto_0

    .line 1116982
    :cond_50
    const/16 v0, 0x92

    if-ne p2, v0, :cond_51

    .line 1116983
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_receiving_subscription_messages INTEGER"

    const v2, -0x137310eb

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x73492501

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116984
    move p3, v1

    goto/16 :goto_0

    .line 1116985
    :cond_51
    const/16 v0, 0x93

    if-ne p2, v0, :cond_52

    .line 1116986
    const-string v0, "ALTER TABLE messages ADD COLUMN commerce_message_type TEXT"

    const v2, -0x19405e51

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x782c441e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116987
    move p3, v1

    goto/16 :goto_0

    .line 1116988
    :cond_52
    const/16 v0, 0x94

    if-ne p2, v0, :cond_53

    .line 1116989
    const-string v0, "ALTER TABLE threads ADD COLUMN last_message_commerce_message_type TEXT"

    const v2, -0x6bde4757

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x1feed26b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116990
    move p3, v1

    goto/16 :goto_0

    .line 1116991
    :cond_53
    const/16 v0, 0x95

    if-ne p2, v0, :cond_54

    .line 1116992
    new-instance v0, LX/0U1;

    const-string v2, "event_reminder_key"

    const-string v3, "TEXT"

    invoke-direct {v0, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116993
    new-instance v2, LX/0U1;

    const-string v3, "user_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116994
    new-instance v3, LX/0su;

    invoke-static {v0, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0su;-><init>(LX/0Px;)V

    .line 1116995
    new-instance v4, LX/0U1;

    const-string v5, "member_guest_status"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1116996
    const-string v2, "event_reminder_members"

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v2, v0, v3}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x3730d23e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x1de52fa8

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116997
    move p3, v1

    goto/16 :goto_0

    .line 1116998
    :cond_54
    const/16 v0, 0x96

    if-ne p2, v0, :cond_55

    .line 1116999
    const-string v0, "ALTER TABLE thread_participants ADD COLUMN phone TEXT"

    const v2, -0x3144234e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4efb27de

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117000
    move p3, v1

    goto/16 :goto_0

    .line 1117001
    :cond_55
    const/16 v0, 0x97

    if-ne p2, v0, :cond_56

    .line 1117002
    const-string v0, "ALTER TABLE messages ADD COLUMN customizations TEXT"

    const v2, -0x256a1079

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x48c6b911

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117003
    move p3, v1

    goto/16 :goto_0

    .line 1117004
    :cond_56
    const/16 v0, 0x98

    if-ne p2, v0, :cond_57

    .line 1117005
    const-string v0, "ALTER TABLE threads ADD COLUMN is_thread_queue_enabled INTEGER"

    const v2, 0x17af3ec0

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x31c346ed

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117006
    move p3, v1

    goto/16 :goto_0

    .line 1117007
    :cond_57
    const/16 v0, 0x99

    if-ne p2, v0, :cond_58

    .line 1117008
    const-string v0, "ALTER TABLE event_reminders ADD COLUMN allows_rsvp INTEGER"

    const v2, -0x6f02d8c1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x199bd78c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117009
    move p3, v1

    goto/16 :goto_0

    .line 1117010
    :cond_58
    const/16 v0, 0x9a

    if-ne p2, v0, :cond_59

    .line 1117011
    const-string v0, "ALTER TABLE messages ADD COLUMN quick_reply TEXT"

    const v2, 0x59f004f0

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x5d6a90dd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117012
    move p3, v1

    goto/16 :goto_0

    .line 1117013
    :cond_59
    const/16 v0, 0x9b

    if-ne p2, v0, :cond_5a

    .line 1117014
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_joinable_event_type TEXT"

    const v2, 0x460b6396

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6ae35295

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117015
    move p3, v1

    goto/16 :goto_0

    .line 1117016
    :cond_5a
    const/16 v0, 0x9c

    if-ne p2, v0, :cond_5b

    .line 1117017
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_messenger_platform_bot INTEGER"

    const v2, 0x135c733c

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x69a2a226

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117018
    move p3, v1

    goto/16 :goto_0

    .line 1117019
    :cond_5b
    const/16 v0, 0x9d

    if-ne p2, v0, :cond_5c

    .line 1117020
    const-string v0, "ALTER TABLE thread_users ADD COLUMN user_call_to_actions TEXT"

    const v2, 0x7097c56d

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3498a2a6    # -1.5162714E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117021
    move p3, v1

    goto/16 :goto_0

    .line 1117022
    :cond_5c
    const/16 v0, 0x9e

    if-ne p2, v0, :cond_5d

    .line 1117023
    const-string v0, "ALTER TABLE thread_participants ADD COLUMN request_timestamp_ms INTEGER"

    const v2, 0x1e92f2c7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2df5cb2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117024
    move p3, v1

    goto/16 :goto_0

    .line 1117025
    :cond_5d
    const/16 v0, 0x9f

    if-ne p2, v0, :cond_5e

    .line 1117026
    const-string v0, "ALTER TABLE messages ADD COLUMN metadata_at_text_ranges TEXT"

    const v2, 0x6689832c

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x16441118

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117027
    move p3, v1

    goto/16 :goto_0

    .line 1117028
    :cond_5e
    const/16 v0, 0xa0

    if-ne p2, v0, :cond_5f

    .line 1117029
    const-string v0, "ALTER TABLE messages ADD COLUMN platform_metadata TEXT"

    const v2, 0x766a251f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6d121241

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117030
    move p3, v1

    goto/16 :goto_0

    .line 1117031
    :cond_5f
    const/16 v0, 0xa1

    if-ne p2, v0, :cond_60

    .line 1117032
    invoke-static {p1}, LX/6ds;->aT(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117033
    :cond_60
    const/16 v0, 0xa2

    if-ne p2, v0, :cond_61

    .line 1117034
    const-string v0, "ALTER TABLE thread_users ADD COLUMN structured_menu_call_to_actions TEXT"

    const v2, -0xaeb0adf

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x20caf8dc

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117035
    move p3, v1

    goto/16 :goto_0

    .line 1117036
    :cond_61
    const/16 v0, 0xa3

    if-ne p2, v0, :cond_62

    .line 1117037
    const-string v0, "ALTER TABLE thread_users ADD COLUMN current_country_code TEXT"

    const v2, -0x7defdb58

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6b44a911

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117038
    const-string v0, "ALTER TABLE thread_users ADD COLUMN home_country_code TEXT"

    const v2, 0x7f50f8f3

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x56cc3a65

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117039
    move p3, v1

    goto/16 :goto_0

    .line 1117040
    :cond_62
    const/16 v0, 0xa4

    if-ne p2, v0, :cond_63

    .line 1117041
    invoke-static {p1}, LX/6ds;->aW(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117042
    :cond_63
    const/16 v0, 0xa5

    if-ne p2, v0, :cond_64

    .line 1117043
    const-string v0, "ALTER TABLE event_reminders ADD COLUMN event_reminder_type TEXT"

    const v2, -0x21b2268

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x487c87c7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117044
    move p3, v1

    goto/16 :goto_0

    .line 1117045
    :cond_64
    const/16 v0, 0xa6

    if-ne p2, v0, :cond_65

    .line 1117046
    const-string v0, "ALTER TABLE thread_participants ADD COLUMN sms_participant_fbid TEXT"

    const v2, 0x1981f485

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x74ed8eee

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117047
    move p3, v1

    goto/16 :goto_0

    .line 1117048
    :cond_65
    const/16 v0, 0xa7

    if-ne p2, v0, :cond_66

    .line 1117049
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_is_joinable_promo INTEGER"

    const v2, 0x67079979

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4c3a1eb5    # 4.8790228E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117050
    move p3, v1

    goto/16 :goto_0

    .line 1117051
    :cond_66
    const/16 v0, 0xa8

    if-ne p2, v0, :cond_67

    .line 1117052
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_agent_intent_id TEXT"

    const v2, -0x74df1408

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x44034651

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117053
    move p3, v1

    goto/16 :goto_0

    .line 1117054
    :cond_67
    const/16 v0, 0xa9

    if-ne p2, v0, :cond_68

    .line 1117055
    const-string v0, "ALTER TABLE threads ADD COLUMN group_description TEXT"

    const v2, -0x709cd5a5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x19e9bab5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117056
    move p3, v1

    goto/16 :goto_0

    .line 1117057
    :cond_68
    const/16 v0, 0xaa

    if-ne p2, v0, :cond_69

    .line 1117058
    const-string v0, "ALTER TABLE messages ADD COLUMN montage_reply_message_id TEXT"

    const v2, 0x157de6e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x46f78607

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117059
    move p3, v1

    goto/16 :goto_0

    .line 1117060
    :cond_69
    const/16 v0, 0xab

    if-ne p2, v0, :cond_6a

    .line 1117061
    const-string v0, "ALTER TABLE thread_users ADD COLUMN estimated_folder TEXT"

    const v2, 0x2114b7fb

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3589f858

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117062
    move p3, v1

    goto/16 :goto_0

    .line 1117063
    :cond_6a
    const/16 v0, 0xac

    if-ne p2, v0, :cond_6b

    .line 1117064
    const-string v0, "ALTER TABLE threads ADD COLUMN media_preview TEXT"

    const v2, -0x5df984a7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7e2f12af

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117065
    move p3, v1

    goto/16 :goto_0

    .line 1117066
    :cond_6b
    const/16 v0, 0xad

    if-ne p2, v0, :cond_6c

    .line 1117067
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_instant_game_id TEXT"

    const v2, 0x6292fc9e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6936d488

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117068
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_instant_game_update_type TEXT"

    const v2, 0x2486bcb1

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x377ab79e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117069
    move p3, v1

    goto/16 :goto_0

    .line 1117070
    :cond_6c
    const/16 v0, 0xae

    if-ne p2, v0, :cond_6d

    .line 1117071
    const-string v0, "ALTER TABLE threads ADD COLUMN booking_requests TEXT"

    const v2, -0x3c962dba

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7110b6ad

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117072
    move p3, v1

    goto/16 :goto_0

    .line 1117073
    :cond_6d
    const/16 v0, 0xaf

    if-ne p2, v0, :cond_6e

    .line 1117074
    const-string v0, "ALTER TABLE thread_users ADD COLUMN extension_resume_url TEXT"

    const v2, 0xbf90a86

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x428c9859

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117075
    const-string v0, "ALTER TABLE thread_users ADD COLUMN structured_menu_badge_count INTEGER"

    const v2, -0x4e061630

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x37910d50    # -244682.75f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117076
    move p3, v1

    goto/16 :goto_0

    .line 1117077
    :cond_6e
    const/16 v0, 0xb0

    if-ne p2, v0, :cond_6f

    .line 1117078
    const-string v0, "ALTER TABLE messages ADD COLUMN admin_text_booking_request_id TEXT"

    const v2, -0x44e664b9

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x6b6fbaba

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117079
    move p3, v1

    goto/16 :goto_0

    .line 1117080
    :cond_6f
    const/16 v0, 0xb1

    if-ne p2, v0, :cond_70

    .line 1117081
    const-string v0, "ALTER TABLE thread_users ADD COLUMN extension_resume_text TEXT"

    const v2, -0x70102f9a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xb7f0db3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117082
    const-string v0, "ALTER TABLE thread_users ADD COLUMN extension_payment_policy TEXT"

    const v2, 0x424222ce

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x290a5262

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117083
    move p3, v1

    goto/16 :goto_0

    .line 1117084
    :cond_70
    const/16 v0, 0xb2

    if-ne p2, v0, :cond_71

    .line 1117085
    const-string v0, "ALTER TABLE messages ADD COLUMN generic_admin_message_extensible_data TEXT"

    const v2, -0x185ea722

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6bc36f36

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117086
    move p3, v1

    goto/16 :goto_0

    .line 1117087
    :cond_71
    const/16 v0, 0xb3

    if-ne p2, v0, :cond_72

    .line 1117088
    invoke-static {p1}, LX/6ds;->bl(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117089
    :cond_72
    const/16 v0, 0xb4

    if-ne p2, v0, :cond_73

    .line 1117090
    invoke-static {p1}, LX/6ds;->bm(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117091
    :cond_73
    const/16 v0, 0xb5

    if-ne p2, v0, :cond_74

    .line 1117092
    const-string v0, "ALTER TABLE thread_users ADD COLUMN does_accept_user_feedback INTEGER"

    const v2, 0x11b4586c

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x466a7735

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117093
    move p3, v1

    goto/16 :goto_0

    .line 1117094
    :cond_74
    const/16 v0, 0xb6

    if-ne p2, v0, :cond_75

    .line 1117095
    const-string v0, "ALTER TABLE thread_users ADD COLUMN is_vc_endpoint INTEGER"

    const v2, 0x6d70f355

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x59ed9916

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117096
    move p3, v1

    goto/16 :goto_0

    .line 1117097
    :cond_75
    const/16 v0, 0xb7

    if-ne p2, v0, :cond_76

    .line 1117098
    const-string v0, "ALTER TABLE threads ADD COLUMN last_call_ms INTEGER"

    const v2, -0x38220e5c

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x775a68d0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117099
    move p3, v1

    goto/16 :goto_0

    .line 1117100
    :cond_76
    const/16 v0, 0xb8

    if-ne p2, v0, :cond_77

    .line 1117101
    const-string v0, "DELETE FROM thread_participants WHERE type = \'FORMER_PARTICIPANT\'"

    const v2, -0x4314e95e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x782278fb

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117102
    move p3, v1

    goto/16 :goto_0

    .line 1117103
    :cond_77
    const/16 v0, 0xb9

    if-ne p2, v0, :cond_78

    .line 1117104
    new-instance v0, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v0, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117105
    new-instance v2, LX/0U1;

    const-string v3, "reaction"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117106
    new-instance v3, LX/0U1;

    const-string v4, "user_key"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117107
    invoke-static {v0, v3, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1117108
    new-instance v4, LX/0su;

    invoke-static {v0, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v4, v0}, LX/0su;-><init>(LX/0Px;)V

    .line 1117109
    const-string v0, "message_reactions"

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v2, -0x41f7b04a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5813fe7

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117110
    move p3, v1

    goto/16 :goto_0

    .line 1117111
    :cond_78
    const/16 v0, 0xba

    if-ne p2, v0, :cond_79

    .line 1117112
    const-string v0, "ALTER TABLE thread_users ADD COLUMN extension_properties TEXT"

    const v2, 0x76591600

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x24161c08

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117113
    move p3, v1

    goto/16 :goto_0

    .line 1117114
    :cond_79
    const/16 v0, 0xbb

    if-ne p2, v0, :cond_7a

    .line 1117115
    new-instance v4, LX/0U1;

    const-string v5, "msg_id"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117116
    new-instance v7, LX/0U1;

    const-string v5, "reaction"

    const-string v6, "TEXT"

    invoke-direct {v7, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117117
    new-instance v8, LX/0U1;

    const-string v5, "user_key"

    const-string v6, "TEXT"

    invoke-direct {v8, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117118
    const-string v5, "message_reactions"

    const-string v6, "messages"

    invoke-static {v4, v8, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-static {v4, v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    const-string v10, "ON DELETE CASCADE"

    move-object v4, p1

    invoke-static/range {v4 .. v10}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1117119
    move p3, v1

    goto/16 :goto_0

    .line 1117120
    :cond_7a
    const/16 v0, 0xbc

    if-ne p2, v0, :cond_7b

    .line 1117121
    const-string v0, "ALTER TABLE threads ADD COLUMN is_discoverable INTEGER"

    const v2, 0x53400236

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x1d5ff1d6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117122
    move p3, v1

    goto/16 :goto_0

    .line 1117123
    :cond_7b
    const/16 v0, 0xbd

    if-ne p2, v0, :cond_7c

    .line 1117124
    invoke-static {p1}, LX/6ds;->bv(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117125
    :cond_7c
    const/16 v0, 0xbe

    if-ne p2, v0, :cond_7d

    .line 1117126
    const-string v0, "ALTER TABLE thread_participants ADD COLUMN last_read_receipt_watermark_time INTEGER"

    const v2, -0x69686bf5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5ce71069

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117127
    const-string v0, "UPDATE thread_participants SET last_read_receipt_watermark_time=last_read_receipt_time"

    const v2, -0x1e9fba15

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5d0f27be

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117128
    move p3, v1

    goto/16 :goto_0

    .line 1117129
    :cond_7d
    const/16 v0, 0xbf

    if-ne p2, v0, :cond_7e

    .line 1117130
    const-string v0, "DELETE FROM properties WHERE key IN (\'sync_token\', \'last_sequence_id\')"

    const v2, 0x5a449dda

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3f671aff

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117131
    move p3, v1

    goto/16 :goto_0

    .line 1117132
    :cond_7e
    const/16 v0, 0xc0

    if-ne p2, v0, :cond_7f

    .line 1117133
    const-string v0, "ALTER TABLE threads ADD COLUMN last_sponsored_message_call_to_action TEXT"

    const v2, 0x62d75bb4

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x14d0e9bc

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117134
    move p3, v1

    goto/16 :goto_0

    .line 1117135
    :cond_7f
    const/16 v0, 0xc1

    if-ne p2, v0, :cond_80

    .line 1117136
    const-string v0, "ALTER TABLE thread_users ADD COLUMN viewer_connection_status TEXT"

    const v2, 0x69375d3

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x902b259

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117137
    move p3, v1

    goto/16 :goto_0

    .line 1117138
    :cond_80
    const/16 v0, 0xc2

    if-ne p2, v0, :cond_81

    .line 1117139
    invoke-static {p1}, LX/6ds;->bA(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v1

    goto/16 :goto_0

    .line 1117140
    :cond_81
    const/16 v0, 0xc3

    if-ne p2, v0, :cond_82

    .line 1117141
    const-string v0, "ALTER TABLE event_reminders ADD COLUMN event_reminder_location_name TEXT"

    const v2, -0x137bb08b

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x1766cbdd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117142
    move p3, v1

    goto/16 :goto_0

    .line 1117143
    :cond_82
    const/16 v0, 0xc4

    if-ne p2, v0, :cond_83

    .line 1117144
    const-string v0, "ALTER TABLE threads ADD COLUMN montage_thread_key TEXT"

    const v2, 0x253998f5

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x68ad4f5e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117145
    sget-object v0, LX/6dr;->d:Ljava/lang/String;

    const v2, 0x16bcc970

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x5246eae4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117146
    move p3, v1

    goto/16 :goto_0

    .line 1117147
    :cond_83
    const/16 v0, 0xc5

    if-ne p2, v0, :cond_3d

    .line 1117148
    const-string v0, "ALTER TABLE messages ADD COLUMN profile_ranges TEXT"

    const v2, 0x46b32094

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xe04425a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1117149
    move p3, v1

    goto/16 :goto_0

    :cond_84
    move p3, v1

    goto/16 :goto_0
.end method

.method private static bA(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116575
    const-string v1, "ALTER TABLE threads ADD COLUMN missed_call_status INTEGER DEFAULT 0"

    const v2, 0x78e1ed85

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x3b4f34fb

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116576
    const-string v1, "UPDATE threads SET missed_call_status = 1 WHERE has_missed_call = 1"

    const v2, -0x33338237    # -1.0721236E8f

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x6f15839e

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116577
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116578
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "senders"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "snippet"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "snippet_sender"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "admin_snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "timestamp_ms"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x24

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "approx_total_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "unread_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "cannot_reply_reason"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "missed_call_status"

    const-string v17, "INTEGER DEFAULT 0"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "custom_like_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "outgoing_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "custom_nicknames"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "invite_uri"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x16

    new-instance v15, LX/0U1;

    const-string v16, "is_last_message_sponsored"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x17

    new-instance v15, LX/0U1;

    const-string v16, "group_chat_rank"

    const-string v17, "FLOAT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x18

    new-instance v15, LX/0U1;

    const-string v16, "game_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x19

    new-instance v15, LX/0U1;

    const-string v16, "is_joinable"

    const-string v17, "INTEGER DEFAULT 0"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    new-instance v15, LX/0U1;

    const-string v16, "requires_approval"

    const-string v17, "INTEGER DEFAULT 0"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    new-instance v15, LX/0U1;

    const-string v16, "rtc_call_info"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    new-instance v15, LX/0U1;

    const-string v16, "last_message_commerce_message_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    new-instance v15, LX/0U1;

    const-string v16, "is_thread_queue_enabled"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    new-instance v15, LX/0U1;

    const-string v16, "group_description"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    new-instance v15, LX/0U1;

    const-string v16, "media_preview"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x20

    new-instance v15, LX/0U1;

    const-string v16, "booking_requests"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x21

    new-instance v15, LX/0U1;

    const-string v16, "last_call_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x22

    new-instance v15, LX/0U1;

    const-string v16, "is_discoverable"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x23

    new-instance v15, LX/0U1;

    const-string v16, "last_sponsored_message_call_to_action"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116579
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116580
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x6bae6a6a

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x3e2c4905

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116581
    return-void
.end method

.method private static bl(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116582
    new-instance v1, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116583
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "text"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "sender"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "is_not_forwardable"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "timestamp_ms"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "timestamp_sent_ms"

    const-string v10, "INTEGER"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "attachments"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "shares"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "sticker_id"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "msg_type"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x34

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "affected_users"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "coordinates"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "offline_threading_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "channel_source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "send_channel"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "is_non_authoritative"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "pending_send_media_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "sent_share_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "client_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "send_error"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "send_error_message"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "send_error_number"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "send_error_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "send_error_error_url"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "publicity"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "send_queue_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "payment_transaction"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "payment_request"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "has_unavailable_attachment"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "app_attribution"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "content_app_attribution"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x16

    new-instance v15, LX/0U1;

    const-string v16, "xma"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x17

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_type"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x18

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_theme_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x19

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_icon_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_nickname"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_target_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_color_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_emoji_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_nickname_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x20

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_journey_bot_choices"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x21

    new-instance v15, LX/0U1;

    const-string v16, "message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x22

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_event"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x23

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_server_info_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x24

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_rtc_is_video_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x25

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_ride_provider_name"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x26

    new-instance v15, LX/0U1;

    const-string v16, "is_sponsored"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x27

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_ad_properties"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x28

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_game_score_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x29

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_thread_event_reminder_properties"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2a

    new-instance v15, LX/0U1;

    const-string v16, "commerce_message_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2b

    new-instance v15, LX/0U1;

    const-string v16, "customizations"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2c

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_joinable_event_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2d

    new-instance v15, LX/0U1;

    const-string v16, "metadata_at_text_ranges"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2e

    new-instance v15, LX/0U1;

    const-string v16, "platform_metadata"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x2f

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_is_joinable_promo"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x30

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_agent_intent_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x31

    new-instance v15, LX/0U1;

    const-string v16, "montage_reply_message_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x32

    new-instance v15, LX/0U1;

    const-string v16, "admin_text_booking_request_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x33

    new-instance v15, LX/0U1;

    const-string v16, "generic_admin_message_extensible_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116584
    const-string v3, "messages"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116585
    sget-object v1, LX/6dg;->c:Ljava/lang/String;

    const v2, 0x75f4db09

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x2288bcee

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116586
    sget-object v1, LX/6dg;->d:Ljava/lang/String;

    const v2, 0x29e02f12

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x48228bf5

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116587
    sget-object v1, LX/6dg;->e:Ljava/lang/String;

    const v2, 0x473f29af

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x497ec9c0

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116588
    return-void
.end method

.method private static bm(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116491
    new-instance v1, LX/0U1;

    const-string v2, "user_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116492
    new-instance v2, LX/0U1;

    const-string v3, "first_name"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "last_name"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "username"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "name"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "is_messenger_user"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "profile_pic_square"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "profile_type"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "is_commerce"

    const-string v11, "INTEGER"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "commerce_page_type"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "is_partial"

    const-string v13, "INTEGER"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "user_rank"

    const-string v14, "REAL"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x15

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "is_blocked_by_viewer"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "is_message_blocked_by_viewer"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "can_viewer_message"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "commerce_page_settings"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "is_friend"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "montage_thread_fbid"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "can_see_viewer_montage_thread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "is_messenger_bot"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_messenger_promotion_blocked_by_viewer"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "user_custom_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "is_receiving_subscription_messages"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "is_messenger_platform_bot"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "user_call_to_actions"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "structured_menu_call_to_actions"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "current_country_code"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "home_country_code"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "extension_resume_url"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "extension_resume_text"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "extension_payment_policy"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "structured_menu_badge_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116493
    const-string v3, "thread_users"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116494
    return-void
.end method

.method private static bv(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116495
    const-string v1, "ALTER TABLE threads ADD COLUMN is_joinable INTEGER DEFAULT 0"

    const v2, -0x2a2e4ff4

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x3dc6eab1

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116496
    const-string v1, "UPDATE threads SET is_joinable = 1 WHERE group_type = \'hidden\'"

    const v2, 0x432c4d7c

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x6bb59993

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116497
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116498
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "senders"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "snippet"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "snippet_sender"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "admin_snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "timestamp_ms"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x23

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "last_read_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "approx_total_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "unread_message_count"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "cannot_reply_reason"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "me_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "other_bubble_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "wallpaper_color"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x10

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_action_id"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x11

    new-instance v15, LX/0U1;

    const-string v16, "initial_fetch_complete"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x12

    new-instance v15, LX/0U1;

    const-string v16, "custom_like_emoji"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x13

    new-instance v15, LX/0U1;

    const-string v16, "outgoing_message_lifetime"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x14

    new-instance v15, LX/0U1;

    const-string v16, "custom_nicknames"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x15

    new-instance v15, LX/0U1;

    const-string v16, "invite_uri"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x16

    new-instance v15, LX/0U1;

    const-string v16, "is_last_message_sponsored"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x17

    new-instance v15, LX/0U1;

    const-string v16, "group_chat_rank"

    const-string v17, "FLOAT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x18

    new-instance v15, LX/0U1;

    const-string v16, "game_data"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x19

    new-instance v15, LX/0U1;

    const-string v16, "is_joinable"

    const-string v17, "INTEGER DEFAULT 0"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    new-instance v15, LX/0U1;

    const-string v16, "requires_approval"

    const-string v17, "INTEGER DEFAULT 0"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    new-instance v15, LX/0U1;

    const-string v16, "rtc_call_info"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    new-instance v15, LX/0U1;

    const-string v16, "last_message_commerce_message_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    new-instance v15, LX/0U1;

    const-string v16, "is_thread_queue_enabled"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    new-instance v15, LX/0U1;

    const-string v16, "group_description"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    new-instance v15, LX/0U1;

    const-string v16, "media_preview"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x20

    new-instance v15, LX/0U1;

    const-string v16, "booking_requests"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x21

    new-instance v15, LX/0U1;

    const-string v16, "last_call_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x22

    new-instance v15, LX/0U1;

    const-string v16, "is_discoverable"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116499
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116500
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x40af2d4b

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x7c006e7a

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116501
    return-void
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116502
    const-string v1, "ALTER TABLE messages ADD COLUMN send_queue_type TEXT"

    const v2, -0x74b1d7c0

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x21a32747

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116503
    const-string v1, "ranked_threads"

    sget-object v2, LX/6dl;->b:LX/0Px;

    sget-object v3, LX/6dl;->a:LX/0sv;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const v2, -0x5a07ac4d

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x403e957b

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116504
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116505
    new-instance v2, LX/0U1;

    const-string v3, "thread_fbid"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "legacy_thread_id"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "refetch_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_visible_action_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "sequence_id"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "name"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "former_participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "object_participants"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "senders"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xd

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116506
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116507
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x4bcbe381    # 2.6724098E7f

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x451f5a42

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116508
    return-void
.end method

.method private static i(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116509
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    const-string v5, "msg_type"

    sget-object v6, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v6, v6, LX/2uW;->dbKeyValue:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "msg_type"

    sget-object v6, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v6, v6, LX/2uW;->dbKeyValue:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "pending_attachment_fbid NOT NULL"

    invoke-static {v3}, LX/0uu;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 1116510
    const-string v2, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1116511
    new-instance v1, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116512
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "legacy_thread_id"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "text"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sender"

    const-string v8, "TEXT"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "timestamp_ms"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "timestamp_sent_ms"

    const-string v10, "INTEGER"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "attachments"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "shares"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "sticker_id"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "msg_type"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xf

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "affected_users"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "coordinates"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "offline_threading_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "channel_source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "is_non_authoritative"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pending_send_media_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "pending_shares"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "client_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "send_error"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "send_error_message"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "send_error_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "publicity"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "send_queue_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "payment_transaction"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116513
    const-string v3, "messages"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116514
    sget-object v1, LX/6dg;->c:Ljava/lang/String;

    const v2, 0x3e41ffcf

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x3ef5c454

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116515
    sget-object v1, LX/6dg;->d:Ljava/lang/String;

    const v2, 0x24ed7407

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x5e18f337

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116516
    sget-object v1, LX/6dg;->e:Ljava/lang/String;

    const v2, 0x1f3cab8e

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x13a15400

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116517
    return-void
.end method

.method private static j(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116518
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    const-string v5, "msg_type"

    sget-object v6, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v6, v6, LX/2uW;->dbKeyValue:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "msg_type"

    sget-object v6, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v6, v6, LX/2uW;->dbKeyValue:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "pending_shares NOT NULL"

    invoke-static {v3}, LX/0uu;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 1116519
    const-string v2, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1116520
    new-instance v1, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116521
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "legacy_thread_id"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "text"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sender"

    const-string v8, "TEXT"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "timestamp_ms"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "timestamp_sent_ms"

    const-string v10, "INTEGER"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "attachments"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "shares"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "sticker_id"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "msg_type"

    const-string v14, "INTEGER"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xe

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "affected_users"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "coordinates"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "offline_threading_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "channel_source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "is_non_authoritative"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pending_send_media_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "client_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "send_error"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "send_error_message"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "send_error_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "publicity"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "send_queue_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "payment_transaction"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116522
    const-string v3, "messages"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116523
    sget-object v1, LX/6dg;->c:Ljava/lang/String;

    const v2, 0x51b54040

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0xd37bc86

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116524
    sget-object v1, LX/6dg;->d:Ljava/lang/String;

    const v2, 0x7a403328

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x2aa6a570

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116525
    sget-object v1, LX/6dg;->e:Ljava/lang/String;

    const v2, -0x3cf1a972

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x5c8604d2

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116526
    const-string v1, "ALTER TABLE messages ADD COLUMN sent_share_attachment TEXT"

    const v2, -0x3f1e46bc

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x2ef9310d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116527
    return-void
.end method

.method private static k(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1116528
    const-string v0, "ALTER TABLE threads ADD COLUMN has_missed_call INTEGER"

    const v1, 0x30ffe706

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3e35c779

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116529
    return-void
.end method

.method private static m(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116530
    new-instance v1, LX/0U1;

    const-string v2, "msg_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116531
    new-instance v2, LX/0U1;

    const-string v3, "thread_key"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "text"

    const-string v6, "TEXT"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "sender"

    const-string v7, "TEXT"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "timestamp_ms"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "timestamp_sent_ms"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "attachments"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "shares"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "sticker_id"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "msg_type"

    const-string v13, "INTEGER"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "affected_users"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xf

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "coordinates"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "offline_threading_id"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "channel_source"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "is_non_authoritative"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pending_send_media_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "sent_share_attachment"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "client_tags"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "send_error"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "send_error_message"

    const-string v17, "STRING"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "send_error_timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "publicity"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "send_queue_type"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "payment_transaction"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "has_unavailable_attachment"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116532
    const-string v3, "messages"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116533
    sget-object v1, LX/6dg;->c:Ljava/lang/String;

    const v2, -0x199601fb

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0xc898b1

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116534
    sget-object v1, LX/6dg;->d:Ljava/lang/String;

    const v2, 0x13be0212

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5ee218c2

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116535
    sget-object v1, LX/6dg;->e:Ljava/lang/String;

    const v2, -0x2f380811

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x3cf32530

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116536
    return-void
.end method

.method private static p(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1116537
    const-string v0, "ALTER TABLE thread_users ADD COLUMN profile_type TEXT"

    const v1, 0x6afd1c4b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5cc5dba9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116538
    return-void
.end method

.method private static q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116539
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116540
    new-instance v2, LX/0U1;

    const-string v3, "thread_fbid"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "legacy_thread_id"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "refetch_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_visible_action_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "sequence_id"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "name"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "former_participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "senders"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0x10

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "num_unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xd

    new-instance v15, LX/0U1;

    const-string v16, "last_visitied_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xe

    new-instance v15, LX/0U1;

    const-string v16, "last_visitied_ms_type"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xf

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116541
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116542
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x48d4c70e

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x42b3810d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116543
    return-void
.end method

.method private static r(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116544
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116545
    new-instance v2, LX/0U1;

    const-string v3, "thread_fbid"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "legacy_thread_id"

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "refetch_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "last_visible_action_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "sequence_id"

    const-string v9, "INTEGER"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "name"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "former_participants"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "senders"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xd

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "snippet_sender"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xc

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116546
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116547
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, -0x1f843bef

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x30f9336

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116548
    return-void
.end method

.method private static t(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116549
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116550
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "participants"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "former_participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "senders"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet_sender"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xc

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116551
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116552
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x2ca60938

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x500d4943

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116553
    return-void
.end method

.method private static z(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18

    .prologue
    .line 1116562
    new-instance v1, LX/0U1;

    const-string v2, "thread_key"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116563
    new-instance v2, LX/0U1;

    const-string v3, "legacy_thread_id"

    const-string v4, "TEXT"

    invoke-direct {v2, v3, v4}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/0U1;

    const-string v4, "action_id"

    const-string v5, "INTEGER"

    invoke-direct {v3, v4, v5}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, LX/0U1;

    const-string v5, "refetch_action_id"

    const-string v6, "INTEGER"

    invoke-direct {v4, v5, v6}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, LX/0U1;

    const-string v6, "last_visible_action_id"

    const-string v7, "INTEGER"

    invoke-direct {v5, v6, v7}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, LX/0U1;

    const-string v7, "sequence_id"

    const-string v8, "INTEGER"

    invoke-direct {v6, v7, v8}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, LX/0U1;

    const-string v8, "name"

    const-string v9, "TEXT"

    invoke-direct {v7, v8, v9}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, LX/0U1;

    const-string v9, "participants"

    const-string v10, "TEXT"

    invoke-direct {v8, v9, v10}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, LX/0U1;

    const-string v10, "former_participants"

    const-string v11, "TEXT"

    invoke-direct {v9, v10, v11}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, LX/0U1;

    const-string v11, "senders"

    const-string v12, "TEXT"

    invoke-direct {v10, v11, v12}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, LX/0U1;

    const-string v12, "snippet"

    const-string v13, "TEXT"

    invoke-direct {v11, v12, v13}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, LX/0U1;

    const-string v13, "snippet_sender"

    const-string v14, "TEXT"

    invoke-direct {v12, v13, v14}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v13, 0xc

    new-array v13, v13, [LX/0U1;

    const/4 v14, 0x0

    new-instance v15, LX/0U1;

    const-string v16, "admin_snippet"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, LX/0U1;

    const-string v16, "timestamp_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, LX/0U1;

    const-string v16, "last_fetch_time_ms"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x3

    new-instance v15, LX/0U1;

    const-string v16, "unread"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x4

    new-instance v15, LX/0U1;

    const-string v16, "pic_hash"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x5

    new-instance v15, LX/0U1;

    const-string v16, "pic"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x6

    new-instance v15, LX/0U1;

    const-string v16, "can_reply_to"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/4 v14, 0x7

    new-instance v15, LX/0U1;

    const-string v16, "mute_until"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x8

    new-instance v15, LX/0U1;

    const-string v16, "is_subscribed"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0x9

    new-instance v15, LX/0U1;

    const-string v16, "folder"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xa

    new-instance v15, LX/0U1;

    const-string v16, "draft"

    const-string v17, "TEXT"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    const/16 v14, 0xb

    new-instance v15, LX/0U1;

    const-string v16, "has_missed_call"

    const-string v17, "INTEGER"

    invoke-direct/range {v15 .. v17}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116564
    const-string v3, "threads"

    new-instance v4, LX/0su;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/0su;-><init>(LX/0Px;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2, v4}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116565
    sget-object v1, LX/6dr;->c:Ljava/lang/String;

    const v2, 0x3bbc709f

    invoke-static {v2}, LX/03h;->a(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x5f792347

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1116566
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1116567
    :goto_0
    if-ge p2, p3, :cond_0

    .line 1116568
    invoke-direct {p0, p1, p2, p3}, LX/6ds;->b(Landroid/database/sqlite/SQLiteDatabase;II)I

    move-result p2

    goto :goto_0

    .line 1116569
    :cond_0
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 1116570
    return-void
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 1116571
    invoke-super {p0, p1}, LX/0Tv;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1116572
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1116573
    const-string v0, "PRAGMA foreign_keys=ON;"

    const v1, -0x13b4d5ba

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x76be07ca

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116574
    :cond_0
    return-void
.end method
