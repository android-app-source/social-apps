.class public LX/5iB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 982269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z
    .locals 1
    .param p0    # Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 982268
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ")",
            "LX/0Px",
            "<",
            "LX/362;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982274
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z
    .locals 1

    .prologue
    .line 982272
    invoke-static {p0}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v0

    .line 982273
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z
    .locals 1

    .prologue
    .line 982275
    invoke-static {p0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->isRotated()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z
    .locals 2

    .prologue
    .line 982271
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z
    .locals 1

    .prologue
    .line 982270
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
