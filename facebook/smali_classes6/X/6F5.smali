.class public LX/6F5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6F4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6F4",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/6ul;

.field private final b:LX/6F6;


# direct methods
.method public constructor <init>(LX/6ul;LX/6F6;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066974
    iput-object p1, p0, LX/6F5;->a:LX/6ul;

    .line 1066975
    iput-object p2, p0, LX/6F5;->b:LX/6F6;

    .line 1066976
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 1

    .prologue
    .line 1066977
    iget-object v0, p0, LX/6F5;->a:LX/6ul;

    invoke-virtual {v0, p1}, LX/6ul;->a(LX/6qh;)V

    .line 1066978
    return-void
.end method

.method public final a(Lcom/facebook/payments/confirmation/ConfirmationData;)V
    .locals 7

    .prologue
    .line 1066979
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1066980
    iget-object v0, p0, LX/6F5;->b:LX/6F6;

    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->e:Ljava/lang/String;

    .line 1066981
    iget-object v2, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    const-string v3, "onConfirmationClose"

    invoke-virtual {v2, v3}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1066982
    :goto_0
    return-void

    .line 1066983
    :cond_0
    iget-object v2, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    iget-object v3, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "confirmationClose"

    .line 1066984
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 1066985
    :try_start_0
    const-string v5, "paymentId"

    invoke-virtual {v6, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066986
    :goto_1
    move-object v5, v6

    .line 1066987
    invoke-static {v3, v4, v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1066988
    :catch_0
    move-exception v5

    .line 1066989
    const-string p0, "paymentsCheckout"

    const-string p1, "Exception serializing return params!"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v5, p1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public bridge synthetic onClick(Lcom/facebook/payments/confirmation/ConfirmationData;LX/6uH;)V
    .locals 1

    .prologue
    .line 1066990
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1066991
    iget-object v0, p0, LX/6F5;->a:LX/6ul;

    invoke-virtual {v0, p1, p2}, LX/6ul;->onClick(Lcom/facebook/payments/confirmation/SimpleConfirmationData;LX/6uH;)V

    .line 1066992
    return-void
.end method
