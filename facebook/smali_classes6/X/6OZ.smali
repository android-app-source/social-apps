.class public final enum LX/6OZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6OZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6OZ;

.field public static final enum CONTACT_IMPORTER:LX/6OZ;

.field public static final enum CONTINUOUS_SYNC:LX/6OZ;

.field public static final enum QUICK_PROMOTION:LX/6OZ;


# instance fields
.field public final contactsFlow:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085269
    new-instance v0, LX/6OZ;

    const-string v1, "CONTACT_IMPORTER"

    invoke-direct {v0, v1, v2, v2}, LX/6OZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6OZ;->CONTACT_IMPORTER:LX/6OZ;

    .line 1085270
    new-instance v0, LX/6OZ;

    const-string v1, "CONTINUOUS_SYNC"

    invoke-direct {v0, v1, v3, v3}, LX/6OZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6OZ;->CONTINUOUS_SYNC:LX/6OZ;

    .line 1085271
    new-instance v0, LX/6OZ;

    const-string v1, "QUICK_PROMOTION"

    invoke-direct {v0, v1, v4, v4}, LX/6OZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6OZ;->QUICK_PROMOTION:LX/6OZ;

    .line 1085272
    const/4 v0, 0x3

    new-array v0, v0, [LX/6OZ;

    sget-object v1, LX/6OZ;->CONTACT_IMPORTER:LX/6OZ;

    aput-object v1, v0, v2

    sget-object v1, LX/6OZ;->CONTINUOUS_SYNC:LX/6OZ;

    aput-object v1, v0, v3

    sget-object v1, LX/6OZ;->QUICK_PROMOTION:LX/6OZ;

    aput-object v1, v0, v4

    sput-object v0, LX/6OZ;->$VALUES:[LX/6OZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1085266
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1085267
    iput p3, p0, LX/6OZ;->contactsFlow:I

    .line 1085268
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6OZ;
    .locals 1

    .prologue
    .line 1085273
    const-class v0, LX/6OZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OZ;

    return-object v0
.end method

.method public static values()[LX/6OZ;
    .locals 1

    .prologue
    .line 1085265
    sget-object v0, LX/6OZ;->$VALUES:[LX/6OZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6OZ;

    return-object v0
.end method
