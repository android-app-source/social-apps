.class public LX/6bO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2MV;

.field private final b:LX/0Uh;

.field private final c:LX/7TG;

.field private final d:LX/1Er;

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7TD;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/io/File;


# direct methods
.method public constructor <init>(LX/0Uh;LX/2MV;LX/1Er;LX/7TG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1113554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113555
    iput-object v0, p0, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1113556
    iput-object v0, p0, LX/6bO;->f:Ljava/io/File;

    .line 1113557
    iput-object p1, p0, LX/6bO;->b:LX/0Uh;

    .line 1113558
    iput-object p2, p0, LX/6bO;->a:LX/2MV;

    .line 1113559
    iput-object p4, p0, LX/6bO;->c:LX/7TG;

    .line 1113560
    iput-object p3, p0, LX/6bO;->d:LX/1Er;

    .line 1113561
    return-void
.end method

.method private static a(LX/60x;III)J
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1113546
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    move p1, v0

    .line 1113547
    :cond_0
    const/4 v1, -0x2

    if-ne p2, v1, :cond_1

    .line 1113548
    iget-wide v2, p0, LX/60x;->a:J

    long-to-int p2, v2

    .line 1113549
    :cond_1
    sub-int v1, p2, p1

    .line 1113550
    iget v2, p0, LX/60x;->g:I

    if-lez v2, :cond_2

    .line 1113551
    iget v0, p0, LX/60x;->g:I

    .line 1113552
    :cond_2
    add-int/2addr v0, p3

    div-int/lit8 v0, v0, 0x8

    div-int/lit16 v1, v1, 0x3e8

    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 1113553
    return-wide v0
.end method

.method private static a(Ljava/io/File;Ljava/io/File;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/2Md;)LX/7TH;
    .locals 4

    .prologue
    .line 1113519
    iget-object v0, p2, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1113520
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v1

    .line 1113521
    iput-object p0, v1, LX/7TI;->a:Ljava/io/File;

    .line 1113522
    move-object v1, v1

    .line 1113523
    iput-object p1, v1, LX/7TI;->b:Ljava/io/File;

    .line 1113524
    move-object v1, v1

    .line 1113525
    iput-object p3, v1, LX/7TI;->c:LX/2Md;

    .line 1113526
    move-object v1, v1

    .line 1113527
    iget-object v2, p2, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->d:LX/60y;

    .line 1113528
    iput-object v2, v1, LX/7TI;->h:LX/60y;

    .line 1113529
    move-object v1, v1

    .line 1113530
    if-eqz v0, :cond_3

    .line 1113531
    iget-boolean v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    if-eqz v2, :cond_0

    .line 1113532
    iget v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    .line 1113533
    iput v2, v1, LX/7TI;->f:I

    .line 1113534
    move-object v2, v1

    .line 1113535
    iget v3, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    .line 1113536
    iput v3, v2, LX/7TI;->g:I

    .line 1113537
    :cond_0
    iget v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    if-eqz v2, :cond_1

    .line 1113538
    iget v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    invoke-virtual {v1, v2}, LX/7TI;->c(I)LX/7TI;

    .line 1113539
    :cond_1
    iget-boolean v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    if-eqz v2, :cond_2

    .line 1113540
    const/4 v2, 0x1

    .line 1113541
    iput-boolean v2, v1, LX/7TI;->i:Z

    .line 1113542
    :cond_2
    iget-object v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    if-eqz v2, :cond_3

    .line 1113543
    iget-object v0, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    .line 1113544
    iput-object v0, v1, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1113545
    :cond_3
    invoke-virtual {v1}, LX/7TI;->o()LX/7TH;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/photos/base/media/VideoItem;Ljava/io/File;LX/2Mf;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)V
    .locals 52

    .prologue
    .line 1113417
    invoke-static/range {p1 .. p1}, LX/6bO;->b(Lcom/facebook/photos/base/media/VideoItem;)Ljava/io/File;

    move-result-object v4

    .line 1113418
    const-string v5, "Transcoded File Path cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113419
    const/4 v12, 0x0

    .line 1113420
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-static {v4, v0, v1, v2}, LX/6bO;->a(Ljava/io/File;Ljava/io/File;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/2Md;)LX/7TH;

    move-result-object v13

    .line 1113421
    :try_start_0
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    .line 1113422
    if-nez v4, :cond_0

    .line 1113423
    invoke-static {}, Lcom/facebook/media/transcode/video/VideoEditConfig;->a()Lcom/facebook/media/transcode/video/VideoEditConfig;

    move-result-object v4

    .line 1113424
    :cond_0
    invoke-virtual/range {p3 .. p3}, LX/2Mf;->a()LX/2Mg;

    move-result-object v5

    iget v5, v5, LX/2Mg;->b:I

    iget-boolean v6, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    iget v7, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    iget v8, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    iget-boolean v9, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    iget v10, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    iget-object v11, v4, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    move-object/from16 v4, p5

    invoke-virtual/range {v4 .. v11}, LX/6b7;->a(IZIIZILandroid/graphics/RectF;)V

    .line 1113425
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6bO;->c:LX/7TG;

    invoke-virtual {v4, v13}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/7T9; {:try_start_0 .. :try_end_0} :catch_3

    .line 1113426
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const v5, -0x64ccea0b

    invoke-static {v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7TD;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/7T9; {:try_start_1 .. :try_end_1} :catch_3

    .line 1113427
    :try_start_2
    iget-object v0, v4, LX/7TD;->m:LX/7TE;

    move-object/from16 v51, v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/7T9; {:try_start_2 .. :try_end_2} :catch_3

    .line 1113428
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1113429
    const-wide/16 v8, 0x1

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    .line 1113430
    new-instance v4, LX/7T9;

    const-string v5, "empty resized file"

    invoke-direct {v4, v5}, LX/7T9;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/7T9; {:try_start_3 .. :try_end_3} :catch_4

    .line 1113431
    :catch_0
    move-exception v4

    .line 1113432
    invoke-virtual/range {p5 .. p5}, LX/6b7;->e()V

    .line 1113433
    throw v4

    .line 1113434
    :catch_1
    move-exception v4

    .line 1113435
    :try_start_4
    invoke-virtual {v4}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    .line 1113436
    instance-of v4, v5, LX/7T9;

    if-eqz v4, :cond_4

    .line 1113437
    move-object v0, v5

    check-cast v0, LX/7T9;

    move-object v4, v0

    .line 1113438
    invoke-virtual {v4}, LX/7T9;->b()LX/7TE;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch LX/7T9; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v4

    .line 1113439
    :goto_0
    :try_start_5
    new-instance v6, LX/7T9;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Resizing video failed. Reason: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v5}, LX/7T9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch LX/7T9; {:try_start_5 .. :try_end_5} :catch_2

    .line 1113440
    :catch_2
    move-exception v37

    .line 1113441
    :goto_1
    if-nez v4, :cond_1

    .line 1113442
    new-instance v4, LX/7TE;

    invoke-direct {v4}, LX/7TE;-><init>()V

    .line 1113443
    :cond_1
    invoke-virtual/range {v37 .. v37}, LX/7T9;->a()Z

    move-result v6

    iget-boolean v7, v4, LX/7TE;->a:Z

    iget-boolean v8, v4, LX/7TE;->b:Z

    iget-boolean v9, v4, LX/7TE;->c:Z

    iget-boolean v10, v4, LX/7TE;->d:Z

    iget-boolean v11, v4, LX/7TE;->e:Z

    iget-boolean v12, v4, LX/7TE;->f:Z

    iget-boolean v13, v4, LX/7TE;->g:Z

    iget-wide v14, v4, LX/7TE;->h:J

    iget-wide v0, v4, LX/7TE;->i:J

    move-wide/from16 v16, v0

    iget-wide v0, v4, LX/7TE;->j:J

    move-wide/from16 v18, v0

    iget-wide v0, v4, LX/7TE;->k:J

    move-wide/from16 v20, v0

    iget-wide v0, v4, LX/7TE;->l:J

    move-wide/from16 v22, v0

    iget-wide v0, v4, LX/7TE;->m:J

    move-wide/from16 v24, v0

    iget-wide v0, v4, LX/7TE;->n:J

    move-wide/from16 v26, v0

    iget-wide v0, v4, LX/7TE;->o:J

    move-wide/from16 v28, v0

    iget-wide v0, v4, LX/7TE;->p:J

    move-wide/from16 v30, v0

    iget-wide v0, v4, LX/7TE;->q:J

    move-wide/from16 v32, v0

    iget-object v0, v4, LX/7TE;->r:Ljava/lang/String;

    move-object/from16 v34, v0

    iget-object v0, v4, LX/7TE;->s:Ljava/lang/String;

    move-object/from16 v35, v0

    iget-object v0, v4, LX/7TE;->t:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v5, p5

    invoke-virtual/range {v5 .. v37}, LX/6b7;->a(ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1113444
    if-eqz p2, :cond_2

    .line 1113445
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    .line 1113446
    :cond_2
    throw v37

    .line 1113447
    :cond_3
    :try_start_6
    const-string v6, "mp4"

    iget v7, v4, LX/7TD;->d:I

    iget v8, v4, LX/7TD;->e:I

    iget v9, v4, LX/7TD;->f:I

    iget v10, v4, LX/7TD;->g:I

    iget v11, v4, LX/7TD;->h:I

    iget v12, v4, LX/7TD;->i:I

    iget v13, v4, LX/7TD;->j:I

    iget v14, v4, LX/7TD;->k:I

    iget-wide v15, v4, LX/7TD;->l:J

    iget-wide v0, v4, LX/7TD;->b:J

    move-wide/from16 v17, v0

    iget-wide v0, v4, LX/7TD;->c:J

    move-wide/from16 v19, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->a:Z

    move/from16 v21, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->b:Z

    move/from16 v22, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->c:Z

    move/from16 v23, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->d:Z

    move/from16 v24, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->e:Z

    move/from16 v25, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->f:Z

    move/from16 v26, v0

    move-object/from16 v0, v51

    iget-boolean v0, v0, LX/7TE;->g:Z

    move/from16 v27, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->h:J

    move-wide/from16 v28, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->i:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->j:J

    move-wide/from16 v32, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->k:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->l:J

    move-wide/from16 v36, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->m:J

    move-wide/from16 v38, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->n:J

    move-wide/from16 v40, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->o:J

    move-wide/from16 v42, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->p:J

    move-wide/from16 v44, v0

    move-object/from16 v0, v51

    iget-wide v0, v0, LX/7TE;->q:J

    move-wide/from16 v46, v0

    move-object/from16 v0, v51

    iget-object v0, v0, LX/7TE;->r:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v51

    iget-object v0, v0, LX/7TE;->s:Ljava/lang/String;

    move-object/from16 v49, v0

    move-object/from16 v0, v51

    iget-object v0, v0, LX/7TE;->t:Ljava/lang/String;

    move-object/from16 v50, v0

    move-object/from16 v5, p5

    invoke-virtual/range {v5 .. v50}, LX/6b7;->a(Ljava/lang/String;IIIIIIIIJJJZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch LX/7T9; {:try_start_6 .. :try_end_6} :catch_4

    .line 1113448
    return-void

    .line 1113449
    :catch_3
    move-exception v37

    move-object v4, v12

    goto/16 :goto_1

    :catch_4
    move-exception v37

    move-object/from16 v4, v51

    goto/16 :goto_1

    :cond_4
    move-object v4, v12

    goto/16 :goto_0
.end method

.method private static a(JLX/60x;LX/2Mf;LX/6b7;)Z
    .locals 10

    .prologue
    .line 1113497
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Video Metadata cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113498
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Resizing Policy cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113499
    invoke-static {p4}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1113500
    const-string v1, "original_file_size"

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113501
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START:LX/74R;

    invoke-static {p4, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1113502
    invoke-virtual {p3}, LX/2Mf;->a()LX/2Mg;

    move-result-object v0

    iget v7, v0, LX/2Mg;->b:I

    .line 1113503
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-static {p2, v0, v1, v7}, LX/6bO;->a(LX/60x;III)J

    move-result-wide v4

    .line 1113504
    long-to-float v0, v4

    long-to-float v1, p0

    div-float/2addr v0, v1

    .line 1113505
    sub-long v2, p0, v4

    .line 1113506
    const v1, 0x3f4ccccd    # 0.8f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const-wide/32 v0, 0x500000

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    const/4 v6, 0x1

    .line 1113507
    :goto_2
    iget v8, p2, LX/60x;->g:I

    move-object v1, p4

    move-wide v2, p0

    .line 1113508
    invoke-static {v1}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1113509
    const-string v9, "original_file_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v9, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113510
    const-string v9, "estimated_resized_file_size"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v9, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113511
    const-string v9, "attempt_video_resize"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v9, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113512
    const-string v9, "estimated_video_bit_rate"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v9, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113513
    const-string v9, "estimated_audio_bit_rate"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v9, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113514
    sget-object v9, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS:LX/74R;

    invoke-static {v1, v9, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1113515
    return v6

    .line 1113516
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1113517
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1113518
    :cond_2
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/6bO;
    .locals 5

    .prologue
    .line 1113495
    new-instance v4, LX/6bO;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v1

    check-cast v1, LX/2MV;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v2

    check-cast v2, LX/1Er;

    invoke-static {p0}, LX/7TG;->a(LX/0QB;)LX/7TG;

    move-result-object v3

    check-cast v3, LX/7TG;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6bO;-><init>(LX/0Uh;LX/2MV;LX/1Er;LX/7TG;)V

    .line 1113496
    return-object v4
.end method

.method public static b(Lcom/facebook/photos/base/media/VideoItem;)Ljava/io/File;
    .locals 3

    .prologue
    .line 1113490
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    .line 1113491
    const-string v1, "Input File Path cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113492
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1113493
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v0

    const-string v2, "Input file is not a file"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113494
    return-object v1
.end method

.method public static b(LX/6bO;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1113489
    iget-object v1, p0, LX/6bO;->b:LX/0Uh;

    const/16 v2, 0x2c2

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6bO;->b:LX/0Uh;

    const/16 v2, 0x2ca

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static c(LX/6bO;Lcom/facebook/photos/base/media/VideoItem;)LX/60x;
    .locals 2

    .prologue
    .line 1113485
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113486
    :try_start_0
    iget-object v0, p0, LX/6bO;->a:LX/2MV;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1113487
    :goto_0
    return-object v0

    .line 1113488
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)LX/6bA;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1113450
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Media Transcode Parmeters cannot be null"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113451
    instance-of v0, p2, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;

    const-string v2, "Mediatranscode Parameters not instance of videotranscode parameters"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move-object v4, p2

    .line 1113452
    check-cast v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;

    .line 1113453
    iget-boolean v0, v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->a:Z

    if-eqz v0, :cond_8

    .line 1113454
    iget v0, v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->b:I

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    const-string v2, "Specified Transcoding"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1113455
    iget v0, v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->b:I

    mul-int/lit16 v0, v0, 0x3e8

    .line 1113456
    invoke-static {v0}, LX/2Mf;->a(I)LX/2Mf;

    move-result-object v0

    .line 1113457
    :goto_2
    move-object v3, v0

    .line 1113458
    iget-object v0, v4, Lcom/facebook/media/transcode/video/VideoTranscodeParameters;->c:Lcom/facebook/media/transcode/video/VideoEditConfig;

    if-eqz v0, :cond_1

    move v7, v1

    .line 1113459
    :goto_3
    invoke-static {p0, p1}, LX/6bO;->c(LX/6bO;Lcom/facebook/photos/base/media/VideoItem;)LX/60x;

    move-result-object v0

    .line 1113460
    invoke-static {p0}, LX/6bO;->b(LX/6bO;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v0, :cond_9

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 1113461
    if-nez v2, :cond_3

    .line 1113462
    invoke-virtual {p3, v6}, LX/6b7;->a(Z)V

    .line 1113463
    if-eqz v7, :cond_2

    .line 1113464
    new-instance v0, LX/7T9;

    const-string v1, "Transcoding not supported for this videoItem"

    invoke-direct {v0, v1}, LX/7T9;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v6

    .line 1113465
    goto :goto_0

    :cond_1
    move v7, v6

    .line 1113466
    goto :goto_3

    .line 1113467
    :cond_2
    new-instance v0, LX/6bA;

    invoke-direct {v0, v6, p1}, LX/6bA;-><init>(ZLcom/facebook/ipc/media/MediaItem;)V

    .line 1113468
    :goto_5
    return-object v0

    .line 1113469
    :cond_3
    if-eqz v7, :cond_5

    .line 1113470
    invoke-virtual {p3, v1}, LX/6b7;->a(Z)V

    .line 1113471
    :cond_4
    iget-object v0, p0, LX/6bO;->d:LX/1Er;

    const-string v1, "video_transcode"

    const-string v2, "mp4"

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/6bO;->f:Ljava/io/File;

    .line 1113472
    :try_start_0
    iget-object v2, p0, LX/6bO;->f:Ljava/io/File;

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/6bO;->a(Lcom/facebook/photos/base/media/VideoItem;Ljava/io/File;LX/2Mf;Lcom/facebook/media/transcode/video/VideoTranscodeParameters;LX/6b7;)V

    .line 1113473
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    iget-object v1, p0, LX/6bO;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/74m;->c(Ljava/lang/String;)LX/74m;

    move-result-object v0

    const-string v1, "video/mp4"

    invoke-virtual {v0, v1}, LX/74m;->d(Ljava/lang/String;)LX/74m;

    move-result-object v0

    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v1

    .line 1113474
    new-instance v0, LX/6bA;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, LX/6bA;-><init>(ZLcom/facebook/ipc/media/MediaItem;)V
    :try_end_0
    .catch LX/7T9; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 1113475
    :catch_0
    move-exception v0

    .line 1113476
    if-eqz v7, :cond_6

    .line 1113477
    throw v0

    .line 1113478
    :cond_5
    invoke-static {p1}, LX/6bO;->b(Lcom/facebook/photos/base/media/VideoItem;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 1113479
    invoke-static {v8, v9, v0, v3, p3}, LX/6bO;->a(JLX/60x;LX/2Mf;LX/6b7;)Z

    move-result v0

    .line 1113480
    if-nez v0, :cond_4

    .line 1113481
    new-instance v0, LX/6bA;

    invoke-direct {v0, v6, p1}, LX/6bA;-><init>(ZLcom/facebook/ipc/media/MediaItem;)V

    goto :goto_5

    .line 1113482
    :cond_6
    new-instance v0, LX/6bA;

    invoke-direct {v0, v6, p1}, LX/6bA;-><init>(ZLcom/facebook/ipc/media/MediaItem;)V

    goto :goto_5

    .line 1113483
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1113484
    :cond_8
    invoke-static {}, LX/2Mf;->b()LX/2Mf;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto :goto_4
.end method
