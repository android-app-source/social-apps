.class public LX/6CS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6CP;


# direct methods
.method public constructor <init>(LX/6CP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063839
    iput-object p1, p0, LX/6CS;->a:LX/6CP;

    .line 1063840
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063841
    const-string v0, "purchase_complete"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 3

    .prologue
    .line 1063842
    check-cast p1, Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;

    .line 1063843
    iget-object v0, p0, LX/6CS;->a:LX/6CP;

    .line 1063844
    const-string v1, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1063845
    new-instance v2, LX/6CQ;

    invoke-direct {v2, p0, p1}, LX/6CQ;-><init>(LX/6CS;Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;)V

    invoke-virtual {v0, v1, v2}, LX/6CP;->a(Ljava/lang/String;LX/6CO;)V

    .line 1063846
    return-void
.end method
