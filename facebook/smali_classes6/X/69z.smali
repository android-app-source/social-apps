.class public LX/69z;
.super LX/3dG;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public b:LX/20j;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1058464
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, LX/3dG;-><init>([Ljava/lang/String;)V

    .line 1058465
    iput-object p2, p0, LX/69z;->c:Ljava/lang/String;

    .line 1058466
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 1058467
    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/3dG;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1058468
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v0, p0, LX/69z;->b:LX/20j;

    iget-object v1, p0, LX/69z;->c:Ljava/lang/String;

    .line 1058469
    if-eqz p1, :cond_2

    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result p0

    if-nez p0, :cond_3

    .line 1058470
    :cond_2
    :goto_1
    move-object p1, p1

    .line 1058471
    goto :goto_0

    :cond_3
    new-instance p0, LX/6PP;

    invoke-direct {p0, v0}, LX/6PP;-><init>(LX/20j;)V

    invoke-static {v0, p1, p0, v1}, LX/20j;->a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;LX/0QK;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058472
    const-string v0, "DeleteCommentCacheVisitor"

    return-object v0
.end method
