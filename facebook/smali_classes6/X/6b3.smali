.class public final LX/6b3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/listitem/FigListItem;

.field public final synthetic b:Lcom/facebook/fig/textinput/FigEditText;

.field public final synthetic c:Landroid/widget/ViewFlipper;

.field public final synthetic d:Landroid/widget/LinearLayout;

.field public final synthetic e:LX/3BS;


# direct methods
.method public constructor <init>(LX/3BS;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/fig/textinput/FigEditText;Landroid/widget/ViewFlipper;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 1112873
    iput-object p1, p0, LX/6b3;->e:LX/3BS;

    iput-object p2, p0, LX/6b3;->a:Lcom/facebook/fig/listitem/FigListItem;

    iput-object p3, p0, LX/6b3;->b:Lcom/facebook/fig/textinput/FigEditText;

    iput-object p4, p0, LX/6b3;->c:Landroid/widget/ViewFlipper;

    iput-object p5, p0, LX/6b3;->d:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x71d9e24f

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1112874
    sget-object v0, LX/3BS;->d:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31V;

    .line 1112875
    if-nez v0, :cond_0

    .line 1112876
    const v0, 0x47df21e3

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1112877
    :goto_0
    return-void

    .line 1112878
    :cond_0
    iget-object v2, p0, LX/6b3;->e:LX/3BS;

    iget-object v3, v0, LX/31V;->a:Ljava/lang/String;

    .line 1112879
    iput-object v3, v2, LX/3BS;->l:Ljava/lang/String;

    .line 1112880
    iget-object v2, p0, LX/6b3;->e:LX/3BS;

    const-string v3, "category"

    .line 1112881
    iput-object v3, v2, LX/3BS;->k:Ljava/lang/String;

    .line 1112882
    iget-object v2, p0, LX/6b3;->a:Lcom/facebook/fig/listitem/FigListItem;

    iget v3, v0, LX/31V;->b:I

    invoke-virtual {v2, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 1112883
    iget-object v2, p0, LX/6b3;->b:Lcom/facebook/fig/textinput/FigEditText;

    iget v0, v0, LX/31V;->c:I

    invoke-virtual {v2, v0}, Lcom/facebook/fig/textinput/FigEditText;->setHint(I)V

    .line 1112884
    iget-object v0, p0, LX/6b3;->c:Landroid/widget/ViewFlipper;

    iget-object v2, p0, LX/6b3;->c:Landroid/widget/ViewFlipper;

    iget-object v3, p0, LX/6b3;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1112885
    iget-object v0, p0, LX/6b3;->b:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->requestFocus()Z

    .line 1112886
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1112887
    iget-object v2, p0, LX/6b3;->b:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1112888
    const v0, -0xb258988

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
