.class public final LX/6Yy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1110771
    new-instance v0, LX/0U1;

    const-string v1, "tagged_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->a:LX/0U1;

    .line 1110772
    new-instance v0, LX/0U1;

    const-string v1, "box_left"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->b:LX/0U1;

    .line 1110773
    new-instance v0, LX/0U1;

    const-string v1, "box_top"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->c:LX/0U1;

    .line 1110774
    new-instance v0, LX/0U1;

    const-string v1, "box_right"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->d:LX/0U1;

    .line 1110775
    new-instance v0, LX/0U1;

    const-string v1, "box_bottom"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->e:LX/0U1;

    .line 1110776
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->f:LX/0U1;

    .line 1110777
    new-instance v0, LX/0U1;

    const-string v1, "is_prefilled"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->g:LX/0U1;

    .line 1110778
    new-instance v0, LX/0U1;

    const-string v1, "created"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->h:LX/0U1;

    .line 1110779
    new-instance v0, LX/0U1;

    const-string v1, "text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->i:LX/0U1;

    .line 1110780
    new-instance v0, LX/0U1;

    const-string v1, "first_name"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->j:LX/0U1;

    .line 1110781
    new-instance v0, LX/0U1;

    const-string v1, "image_hash"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6Yy;->k:LX/0U1;

    return-void
.end method
