.class public LX/6NA;
.super LX/6Mz;
.source ""


# instance fields
.field private final d:LX/3fx;


# direct methods
.method public constructor <init>(LX/3fx;LX/2J0;Landroid/database/Cursor;)V
    .locals 0
    .param p3    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082682
    invoke-direct {p0, p2, p3}, LX/6Mz;-><init>(LX/2J0;Landroid/database/Cursor;)V

    .line 1082683
    iput-object p1, p0, LX/6NA;->d:LX/3fx;

    .line 1082684
    return-void
.end method


# virtual methods
.method public final a(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082685
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082686
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1082687
    :goto_0
    return-void

    .line 1082688
    :cond_0
    const-string v1, "parseSmsAddress"

    const v2, 0xdc64587

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1082689
    :try_start_0
    iget-object v1, p0, LX/6NA;->d:LX/3fx;

    .line 1082690
    new-instance v2, LX/7Hx;

    invoke-direct {v2, v1, v0}, LX/7Hx;-><init>(LX/3fx;Ljava/lang/String;)V

    invoke-virtual {v2}, LX/7Hx;->a()Z

    move-result v2

    move v1, v2

    .line 1082691
    if-eqz v1, :cond_1

    .line 1082692
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082693
    iget-object v2, p0, LX/6NA;->d:LX/3fx;

    invoke-virtual {v2, v0, v1}, LX/3fx;->a(Ljava/lang/String;I)Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    .line 1082694
    if-eqz v0, :cond_1

    .line 1082695
    new-instance v2, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1082696
    iget-object v3, v0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v0, v3

    .line 1082697
    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/facebook/contacts/model/PhonebookPhoneNumber;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1, v2}, LX/6NG;->a(Lcom/facebook/contacts/model/PhonebookPhoneNumber;)LX/6NG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1082698
    :cond_1
    const v0, -0x771b1bf9

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x6807d53d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082699
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082700
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1082701
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082702
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082703
    new-instance v3, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/contacts/model/PhonebookEmailAddress;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1, v3}, LX/6NG;->a(Lcom/facebook/contacts/model/PhonebookEmailAddress;)LX/6NG;

    .line 1082704
    :cond_0
    return-void
.end method

.method public final c(LX/6NG;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1082705
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data1"

    invoke-static {v0, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082706
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data2"

    invoke-static {v0, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082707
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "data3"

    invoke-static {v0, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082708
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "is_super_primary"

    invoke-static {v0, v5}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    .line 1082709
    if-nez v2, :cond_2

    move v0, v1

    .line 1082710
    :goto_0
    iget-object v6, p1, LX/6NG;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1082711
    if-nez v6, :cond_3

    .line 1082712
    :goto_1
    if-gt v0, v1, :cond_0

    if-eqz v5, :cond_1

    if-lez v0, :cond_1

    .line 1082713
    :cond_0
    iput-object v2, p1, LX/6NG;->b:Ljava/lang/String;

    .line 1082714
    iput-object v3, p1, LX/6NG;->c:Ljava/lang/String;

    .line 1082715
    iput-object v4, p1, LX/6NG;->d:Ljava/lang/String;

    .line 1082716
    :cond_1
    return-void

    .line 1082717
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 1082718
    :cond_3
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_1
.end method
