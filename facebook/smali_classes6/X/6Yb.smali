.class public abstract LX/6Yb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

.field private final b:LX/7Y7;

.field private final c:LX/4g9;

.field private final d:LX/7YP;


# direct methods
.method public constructor <init>(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/7Y7;LX/4g9;LX/7YP;)V
    .locals 0

    .prologue
    .line 1110266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1110267
    iput-object p1, p0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110268
    iput-object p2, p0, LX/6Yb;->b:LX/7Y7;

    .line 1110269
    iput-object p3, p0, LX/6Yb;->c:LX/4g9;

    .line 1110270
    iput-object p4, p0, LX/6Yb;->d:LX/7YP;

    .line 1110271
    return-void
.end method

.method public static c(LX/6Yb;)V
    .locals 2

    .prologue
    .line 1110272
    iget-object v0, p0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    new-instance v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    invoke-direct {v1}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1110273
    iget-object v0, p0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->BUY_FAILURE:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    .line 1110274
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()LX/6Y4;
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x3acf61db

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1110275
    invoke-virtual {p0}, LX/6Yb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/6Yb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/6Ya;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 1110276
    iget-object v2, p0, LX/6Yb;->c:LX/4g9;

    sget-object v3, LX/4g6;->c:LX/4g6;

    invoke-virtual {v2, v3, v1}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110277
    iget-object v1, p0, LX/6Yb;->d:LX/7YP;

    new-instance v2, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/6Yb;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LX/6Yb;->b()LX/6Y4;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V

    .line 1110278
    iget-object v3, v1, LX/7YP;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    const/4 v4, 0x1

    const-string v5, "upsell"

    invoke-virtual {v3, v4, v5}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1110279
    new-instance v5, LX/7YO;

    invoke-direct {v5, v1, v2}, LX/7YO;-><init>(LX/7YP;Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;)V

    iget-object v3, v1, LX/7YP;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 1110280
    iget-object v2, p0, LX/6Yb;->b:LX/7Y7;

    new-instance v3, LX/6Yo;

    invoke-direct {v3, p0}, LX/6Yo;-><init>(LX/6Yb;)V

    invoke-virtual {v2, v1, v3}, LX/7Y7;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1110281
    const v1, 0x3f944ffe

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
