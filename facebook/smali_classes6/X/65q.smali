.class public final LX/65q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65Y;


# instance fields
.field public final a:LX/65l;

.field private final b:LX/671;

.field private final c:LX/65o;

.field private final d:Z


# direct methods
.method public constructor <init>(LX/671;IZ)V
    .locals 2

    .prologue
    .line 1048892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048893
    iput-object p1, p0, LX/65q;->b:LX/671;

    .line 1048894
    iput-boolean p3, p0, LX/65q;->d:Z

    .line 1048895
    new-instance v0, LX/65o;

    iget-object v1, p0, LX/65q;->b:LX/671;

    invoke-direct {v0, v1}, LX/65o;-><init>(LX/671;)V

    iput-object v0, p0, LX/65q;->c:LX/65o;

    .line 1048896
    new-instance v0, LX/65l;

    iget-object v1, p0, LX/65q;->c:LX/65o;

    invoke-direct {v0, p2, v1}, LX/65l;-><init>(ILX/65D;)V

    iput-object v0, p0, LX/65q;->a:LX/65l;

    .line 1048897
    return-void
.end method

.method private a(ISBI)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ISBI)",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1048882
    iget-object v0, p0, LX/65q;->c:LX/65o;

    iget-object v1, p0, LX/65q;->c:LX/65o;

    iput p1, v1, LX/65o;->d:I

    iput p1, v0, LX/65o;->a:I

    .line 1048883
    iget-object v0, p0, LX/65q;->c:LX/65o;

    iput-short p2, v0, LX/65o;->e:S

    .line 1048884
    iget-object v0, p0, LX/65q;->c:LX/65o;

    iput-byte p3, v0, LX/65o;->b:B

    .line 1048885
    iget-object v0, p0, LX/65q;->c:LX/65o;

    iput p4, v0, LX/65o;->c:I

    .line 1048886
    iget-object v0, p0, LX/65q;->a:LX/65l;

    invoke-virtual {v0}, LX/65l;->a()V

    .line 1048887
    iget-object v0, p0, LX/65q;->a:LX/65l;

    .line 1048888
    new-instance v1, Ljava/util/ArrayList;

    iget-object p0, v0, LX/65l;->e:Ljava/util/List;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1048889
    iget-object p0, v0, LX/65l;->e:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 1048890
    move-object v0, v1

    .line 1048891
    return-object v0
.end method

.method private a(Lokhttp3/internal/framed/FramedConnection$Reader;I)V
    .locals 1

    .prologue
    .line 1048879
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    .line 1048880
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    .line 1048881
    return-void
.end method

.method private a(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1048867
    if-nez p4, :cond_0

    const-string v0, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048868
    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    .line 1048869
    :goto_0
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 1048870
    :goto_1
    and-int/lit8 v3, p3, 0x20

    if-eqz v3, :cond_1

    .line 1048871
    invoke-direct {p0, p1, p4}, LX/65q;->a(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    .line 1048872
    add-int/lit8 p2, p2, -0x5

    .line 1048873
    :cond_1
    invoke-static {p2, p3, v0}, LX/65t;->b(IBS)I

    move-result v3

    .line 1048874
    invoke-direct {p0, v3, v0, p3, p4}, LX/65q;->a(ISBI)Ljava/util/List;

    move-result-object v4

    .line 1048875
    sget-object v5, LX/65k;->HTTP_20_HEADERS:LX/65k;

    move-object v0, p1

    move v3, p4

    invoke-virtual/range {v0 .. v5}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZZILjava/util/List;LX/65k;)V

    .line 1048876
    return-void

    :cond_2
    move v2, v1

    .line 1048877
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1048878
    goto :goto_1
.end method

.method private a(Lokhttp3/internal/framed/FramedConnection$Reader;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1048863
    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_PRIORITY length: %d != 5"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048864
    :cond_0
    if-nez p3, :cond_1

    const-string v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048865
    :cond_1
    invoke-direct {p0, p1, p3}, LX/65q;->a(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    .line 1048866
    return-void
.end method

.method private b(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1048852
    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_0

    move v2, v1

    .line 1048853
    :goto_0
    and-int/lit8 v3, p3, 0x20

    if-eqz v3, :cond_1

    .line 1048854
    :goto_1
    if-eqz v1, :cond_2

    .line 1048855
    const-string v1, "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    move v2, v0

    .line 1048856
    goto :goto_0

    :cond_1
    move v1, v0

    .line 1048857
    goto :goto_1

    .line 1048858
    :cond_2
    and-int/lit8 v1, p3, 0x8

    if-eqz v1, :cond_3

    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 1048859
    :cond_3
    invoke-static {p2, p3, v0}, LX/65t;->b(IBS)I

    move-result v1

    .line 1048860
    iget-object v3, p0, LX/65q;->b:LX/671;

    invoke-virtual {p1, v2, p4, v3, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZILX/671;I)V

    .line 1048861
    iget-object v1, p0, LX/65q;->b:LX/671;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, LX/671;->f(J)V

    .line 1048862
    return-void
.end method

.method private b(Lokhttp3/internal/framed/FramedConnection$Reader;II)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1048844
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_RST_STREAM length: %d != 4"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048845
    :cond_0
    if-nez p3, :cond_1

    const-string v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048846
    :cond_1
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    .line 1048847
    invoke-static {v0}, LX/65X;->fromHttp2(I)LX/65X;

    move-result-object v1

    .line 1048848
    if-nez v1, :cond_2

    .line 1048849
    const-string v1, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048850
    :cond_2
    invoke-virtual {p1, p3, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ILX/65X;)V

    .line 1048851
    return-void
.end method

.method private c(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1048758
    if-eqz p4, :cond_0

    const-string v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048759
    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    .line 1048760
    if-eqz p2, :cond_6

    const-string v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048761
    :cond_1
    rem-int/lit8 v0, p2, 0x6

    if-eqz v0, :cond_2

    const-string v0, "TYPE_SETTINGS length %% 6 != 0: %s"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048762
    :cond_2
    new-instance v3, LX/663;

    invoke-direct {v3}, LX/663;-><init>()V

    move v1, v2

    .line 1048763
    :goto_0
    if-ge v1, p2, :cond_5

    .line 1048764
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->i()S

    move-result v0

    .line 1048765
    iget-object v4, p0, LX/65q;->b:LX/671;

    invoke-interface {v4}, LX/671;->j()I

    move-result v4

    .line 1048766
    packed-switch v0, :pswitch_data_0

    .line 1048767
    :cond_3
    :goto_1
    :pswitch_0
    invoke-virtual {v3, v0, v2, v4}, LX/663;->a(III)LX/663;

    .line 1048768
    add-int/lit8 v0, v1, 0x6

    move v1, v0

    goto :goto_0

    .line 1048769
    :pswitch_1
    if-eqz v4, :cond_3

    if-eq v4, v6, :cond_3

    .line 1048770
    const-string v0, "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048771
    :pswitch_2
    const/4 v0, 0x4

    .line 1048772
    goto :goto_1

    .line 1048773
    :pswitch_3
    const/4 v0, 0x7

    .line 1048774
    if-gez v4, :cond_3

    .line 1048775
    const-string v0, "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048776
    :pswitch_4
    const/16 v5, 0x4000

    if-lt v4, v5, :cond_4

    const v5, 0xffffff

    if-le v4, v5, :cond_3

    .line 1048777
    :cond_4
    const-string v0, "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048778
    :cond_5
    invoke-virtual {p1, v2, v3}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZLX/663;)V

    .line 1048779
    :cond_6
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c(Lokhttp3/internal/framed/FramedConnection$Reader;II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1048831
    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    const-string v0, "TYPE_GOAWAY length < 8: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048832
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048833
    :cond_1
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v1

    .line 1048834
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    .line 1048835
    add-int/lit8 v2, p2, -0x8

    .line 1048836
    invoke-static {v0}, LX/65X;->fromHttp2(I)LX/65X;

    move-result-object v3

    .line 1048837
    if-nez v3, :cond_2

    .line 1048838
    const-string v1, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048839
    :cond_2
    sget-object v0, LX/673;->b:LX/673;

    .line 1048840
    if-lez v2, :cond_3

    .line 1048841
    iget-object v0, p0, LX/65q;->b:LX/671;

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, LX/671;->c(J)LX/673;

    move-result-object v0

    .line 1048842
    :cond_3
    invoke-virtual {p1, v1, v0}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ILX/673;)V

    .line 1048843
    return-void
.end method

.method private d(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1048822
    if-nez p4, :cond_0

    .line 1048823
    const-string v1, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048824
    :cond_0
    and-int/lit8 v1, p3, 0x8

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    .line 1048825
    :cond_1
    iget-object v1, p0, LX/65q;->b:LX/671;

    invoke-interface {v1}, LX/671;->j()I

    move-result v1

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    .line 1048826
    add-int/lit8 v2, p2, -0x4

    .line 1048827
    invoke-static {v2, p3, v0}, LX/65t;->b(IBS)I

    move-result v2

    .line 1048828
    invoke-direct {p0, v2, v0, p3, p4}, LX/65q;->a(ISBI)Ljava/util/List;

    move-result-object v0

    .line 1048829
    iget-object v2, p1, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v2, v1, v0}, LX/65c;->a$redex0(LX/65c;ILjava/util/List;)V

    .line 1048830
    return-void
.end method

.method private d(Lokhttp3/internal/framed/FramedConnection$Reader;II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1048817
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048818
    :cond_0
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v0, v2

    .line 1048819
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-string v2, "windowSizeIncrement was 0"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048820
    :cond_1
    invoke-virtual {p1, p3, v0, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(IJ)V

    .line 1048821
    return-void
.end method

.method private e(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1048809
    const/16 v2, 0x8

    if-eq p2, v2, :cond_0

    const-string v2, "TYPE_PING length != 8: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048810
    :cond_0
    if-eqz p4, :cond_1

    const-string v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048811
    :cond_1
    iget-object v2, p0, LX/65q;->b:LX/671;

    invoke-interface {v2}, LX/671;->j()I

    move-result v2

    .line 1048812
    iget-object v3, p0, LX/65q;->b:LX/671;

    invoke-interface {v3}, LX/671;->j()I

    move-result v3

    .line 1048813
    and-int/lit8 v4, p3, 0x1

    if-eqz v4, :cond_2

    .line 1048814
    :goto_0
    invoke-virtual {p1, v0, v2, v3}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZII)V

    .line 1048815
    return-void

    :cond_2
    move v0, v1

    .line 1048816
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1048803
    iget-boolean v0, p0, LX/65q;->d:Z

    if-eqz v0, :cond_1

    .line 1048804
    :cond_0
    return-void

    .line 1048805
    :cond_1
    iget-object v0, p0, LX/65q;->b:LX/671;

    sget-object v1, LX/65t;->b:LX/673;

    invoke-virtual {v1}, LX/673;->e()I

    move-result v1

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, LX/671;->c(J)LX/673;

    move-result-object v0

    .line 1048806
    sget-object v1, LX/65t;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/65t;->a:Ljava/util/logging/Logger;

    const-string v2, "<< CONNECTION %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/673;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 1048807
    :cond_2
    sget-object v1, LX/65t;->b:LX/673;

    invoke-virtual {v1, v0}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1048808
    const-string v1, "Expected a connection header but was %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/673;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public final a(Lokhttp3/internal/framed/FramedConnection$Reader;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1048782
    :try_start_0
    iget-object v2, p0, LX/65q;->b:LX/671;

    const-wide/16 v4, 0x9

    invoke-interface {v2, v4, v5}, LX/671;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1048783
    iget-object v2, p0, LX/65q;->b:LX/671;

    invoke-static {v2}, LX/65t;->b(LX/671;)I

    move-result v2

    .line 1048784
    if-ltz v2, :cond_0

    const/16 v3, 0x4000

    if-le v2, v3, :cond_1

    .line 1048785
    :cond_0
    const-string v3, "FRAME_SIZE_ERROR: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v0}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048786
    :catch_0
    move v0, v1

    .line 1048787
    :goto_0
    return v0

    .line 1048788
    :cond_1
    iget-object v1, p0, LX/65q;->b:LX/671;

    invoke-interface {v1}, LX/671;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 1048789
    iget-object v3, p0, LX/65q;->b:LX/671;

    invoke-interface {v3}, LX/671;->h()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 1048790
    iget-object v4, p0, LX/65q;->b:LX/671;

    invoke-interface {v4}, LX/671;->j()I

    move-result v4

    const v5, 0x7fffffff

    and-int/2addr v4, v5

    .line 1048791
    sget-object v5, LX/65t;->a:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-object v5, LX/65t;->a:Ljava/util/logging/Logger;

    invoke-static {v0, v4, v2, v1, v3}, LX/65p;->a(ZIIBB)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 1048792
    :cond_2
    packed-switch v1, :pswitch_data_0

    .line 1048793
    iget-object v1, p0, LX/65q;->b:LX/671;

    int-to-long v2, v2

    invoke-interface {v1, v2, v3}, LX/671;->f(J)V

    goto :goto_0

    .line 1048794
    :pswitch_0
    invoke-direct {p0, p1, v2, v3, v4}, LX/65q;->b(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V

    goto :goto_0

    .line 1048795
    :pswitch_1
    invoke-direct {p0, p1, v2, v3, v4}, LX/65q;->a(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V

    goto :goto_0

    .line 1048796
    :pswitch_2
    invoke-direct {p0, p1, v2, v4}, LX/65q;->a(Lokhttp3/internal/framed/FramedConnection$Reader;II)V

    goto :goto_0

    .line 1048797
    :pswitch_3
    invoke-direct {p0, p1, v2, v4}, LX/65q;->b(Lokhttp3/internal/framed/FramedConnection$Reader;II)V

    goto :goto_0

    .line 1048798
    :pswitch_4
    invoke-direct {p0, p1, v2, v3, v4}, LX/65q;->c(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V

    goto :goto_0

    .line 1048799
    :pswitch_5
    invoke-direct {p0, p1, v2, v3, v4}, LX/65q;->d(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V

    goto :goto_0

    .line 1048800
    :pswitch_6
    invoke-direct {p0, p1, v2, v3, v4}, LX/65q;->e(Lokhttp3/internal/framed/FramedConnection$Reader;IBI)V

    goto :goto_0

    .line 1048801
    :pswitch_7
    invoke-direct {p0, p1, v2, v4}, LX/65q;->c(Lokhttp3/internal/framed/FramedConnection$Reader;II)V

    goto :goto_0

    .line 1048802
    :pswitch_8
    invoke-direct {p0, p1, v2, v4}, LX/65q;->d(Lokhttp3/internal/framed/FramedConnection$Reader;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1048780
    iget-object v0, p0, LX/65q;->b:LX/671;

    invoke-interface {v0}, LX/65D;->close()V

    .line 1048781
    return-void
.end method
