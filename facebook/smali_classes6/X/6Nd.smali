.class public LX/6Nd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public final a:LX/6NX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1083730
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6Nd;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/6NX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1083727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083728
    iput-object p1, p0, LX/6Nd;->a:LX/6NX;

    .line 1083729
    return-void
.end method

.method public static a(LX/0QB;)LX/6Nd;
    .locals 7

    .prologue
    .line 1083692
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1083693
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1083694
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1083695
    if-nez v1, :cond_0

    .line 1083696
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1083697
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1083698
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1083699
    sget-object v1, LX/6Nd;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1083700
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1083701
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1083702
    :cond_1
    if-nez v1, :cond_4

    .line 1083703
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1083704
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1083705
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1083706
    new-instance p0, LX/6Nd;

    invoke-static {v0}, LX/6NX;->a(LX/0QB;)LX/6NX;

    move-result-object v1

    check-cast v1, LX/6NX;

    invoke-direct {p0, v1}, LX/6Nd;-><init>(LX/6NX;)V

    .line 1083707
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1083708
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1083709
    if-nez v1, :cond_2

    .line 1083710
    sget-object v0, LX/6Nd;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Nd;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1083711
    :goto_1
    if-eqz v0, :cond_3

    .line 1083712
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1083713
    :goto_3
    check-cast v0, LX/6Nd;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1083714
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1083715
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1083716
    :catchall_1
    move-exception v0

    .line 1083717
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1083718
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1083719
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1083720
    :cond_2
    :try_start_8
    sget-object v0, LX/6Nd;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Nd;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/graphql/Contact;)V
    .locals 4

    .prologue
    .line 1083721
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cannot save contact without an FBID"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083722
    invoke-static {p1}, LX/6NW;->a(Lcom/facebook/contacts/graphql/Contact;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1083723
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    .line 1083724
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 1083725
    iget-object v0, p0, LX/6Nd;->a:LX/6NX;

    sget-object v2, LX/6Nb;->SAVE_CONTACT:LX/6Nb;

    invoke-virtual {v0, v2}, LX/6NX;->a(LX/6Nb;)Lcom/facebook/omnistore/Collection;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/omnistore/Collection;->saveObject(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 1083726
    return-void
.end method
