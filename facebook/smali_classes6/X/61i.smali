.class public LX/61i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61h;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:LX/5Ox;

.field private b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

.field private c:Lcom/facebook/ffmpeg/FFMpegAVStream;

.field private d:Lcom/facebook/ffmpeg/FFMpegAVStream;

.field private e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;


# direct methods
.method public constructor <init>(LX/5Ox;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040546
    iput-object p1, p0, LX/61i;->a:LX/5Ox;

    .line 1040547
    new-instance v0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-direct {v0}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;-><init>()V

    iput-object v0, p0, LX/61i;->e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    .line 1040548
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1040543
    iget-object v0, p0, LX/61i;->b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    invoke-virtual {v0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->b()V

    .line 1040544
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1040541
    iget-object v0, p0, LX/61i;->c:Lcom/facebook/ffmpeg/FFMpegAVStream;

    invoke-virtual {v0, p1}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(I)V

    .line 1040542
    return-void
.end method

.method public final a(LX/60z;)V
    .locals 3

    .prologue
    .line 1040536
    :try_start_0
    iget-object v0, p0, LX/61i;->e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-interface {p1}, LX/60z;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(Landroid/media/MediaCodec$BufferInfo;)V

    .line 1040537
    iget-object v0, p0, LX/61i;->d:Lcom/facebook/ffmpeg/FFMpegAVStream;

    iget-object v1, p0, LX/61i;->e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-interface {p1}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Lcom/facebook/ffmpeg/FFMpegBadDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040538
    return-void

    .line 1040539
    :catch_0
    move-exception v0

    .line 1040540
    new-instance v1, LX/61n;

    invoke-direct {v1, v0}, LX/61n;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 1040534
    iget-object v0, p0, LX/61i;->b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    invoke-static {p1}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->a(Landroid/media/MediaFormat;)Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;

    move-result-object v0

    iput-object v0, p0, LX/61i;->d:Lcom/facebook/ffmpeg/FFMpegAVStream;

    .line 1040535
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1040532
    iget-object v0, p0, LX/61i;->a:LX/5Ox;

    invoke-virtual {v0, p1}, LX/5Ox;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a()Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    move-result-object v0

    iput-object v0, p0, LX/61i;->b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    .line 1040533
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1040530
    iget-object v0, p0, LX/61i;->b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    invoke-virtual {v0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->c()V

    .line 1040531
    return-void
.end method

.method public final b(LX/60z;)V
    .locals 3

    .prologue
    .line 1040523
    :try_start_0
    iget-object v0, p0, LX/61i;->e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-interface {p1}, LX/60z;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(Landroid/media/MediaCodec$BufferInfo;)V

    .line 1040524
    iget-object v0, p0, LX/61i;->c:Lcom/facebook/ffmpeg/FFMpegAVStream;

    iget-object v1, p0, LX/61i;->e:Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-interface {p1}, LX/60z;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Lcom/facebook/ffmpeg/FFMpegBadDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040525
    return-void

    .line 1040526
    :catch_0
    move-exception v0

    .line 1040527
    new-instance v1, LX/61n;

    invoke-direct {v1, v0}, LX/61n;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 1040528
    iget-object v0, p0, LX/61i;->b:Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    invoke-static {p1}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->a(Landroid/media/MediaFormat;)Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;

    move-result-object v0

    iput-object v0, p0, LX/61i;->c:Lcom/facebook/ffmpeg/FFMpegAVStream;

    .line 1040529
    return-void
.end method
