.class public LX/5qS;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AnimationsDebugModule"
.end annotation


# instance fields
.field private a:LX/5qX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/5qT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;LX/5qT;)V
    .locals 0

    .prologue
    .line 1009285
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1009286
    iput-object p2, p0, LX/5qS;->b:LX/5qT;

    .line 1009287
    return-void
.end method

.method private static h()V
    .locals 2

    .prologue
    .line 1009282
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1009283
    new-instance v0, LX/5p9;

    const-string v1, "Animation debugging is not supported in API <16"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009284
    :cond_0
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 1009254
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    if-eqz v0, :cond_0

    .line 1009255
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    invoke-virtual {v0}, LX/5qX;->b()V

    .line 1009256
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qS;->a:LX/5qX;

    .line 1009257
    :cond_0
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009281
    const-string v0, "AnimationsDebugModule"

    return-object v0
.end method

.method public startRecordingFps()V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1009272
    iget-object v0, p0, LX/5qS;->b:LX/5qT;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5qS;->b:LX/5qT;

    invoke-interface {v0}, LX/5qT;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1009273
    :cond_0
    :goto_0
    return-void

    .line 1009274
    :cond_1
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    if-eqz v0, :cond_2

    .line 1009275
    new-instance v0, LX/5p9;

    const-string v1, "Already recording FPS!"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009276
    :cond_2
    invoke-static {}, LX/5qS;->h()V

    .line 1009277
    new-instance v0, LX/5qX;

    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v1

    .line 1009278
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 1009279
    invoke-direct {v0, v1, v2}, LX/5qX;-><init>(Landroid/view/Choreographer;LX/5pX;)V

    iput-object v0, p0, LX/5qS;->a:LX/5qX;

    .line 1009280
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    invoke-virtual {v0}, LX/5qX;->a()V

    goto :goto_0
.end method

.method public stopRecordingFps(D)V
    .locals 11
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1009258
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    if-nez v0, :cond_0

    .line 1009259
    :goto_0
    return-void

    .line 1009260
    :cond_0
    invoke-static {}, LX/5qS;->h()V

    .line 1009261
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    invoke-virtual {v0}, LX/5qX;->b()V

    .line 1009262
    iget-object v0, p0, LX/5qS;->a:LX/5qX;

    double-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LX/5qX;->a(J)LX/5qW;

    move-result-object v0

    .line 1009263
    if-nez v0, :cond_1

    .line 1009264
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1009265
    const-string v1, "Unable to get FPS info"

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 1009266
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qS;->a:LX/5qX;

    goto :goto_0

    .line 1009267
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "FPS: %.2f, %d frames (%d expected)"

    new-array v3, v6, [Ljava/lang/Object;

    iget-wide v4, v0, LX/5qW;->e:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v9

    iget v4, v0, LX/5qW;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    iget v4, v0, LX/5qW;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1009268
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "JS FPS: %.2f, %d frames (%d expected)"

    new-array v4, v6, [Ljava/lang/Object;

    iget-wide v6, v0, LX/5qW;->f:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v9

    iget v5, v0, LX/5qW;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    iget v5, v0, LX/5qW;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1009269
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nTotal Time MS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d"

    new-array v4, v8, [Ljava/lang/Object;

    iget v0, v0, LX/5qW;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1009270
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1009271
    invoke-static {v1, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method
