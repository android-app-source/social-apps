.class public final LX/6X5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1107418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107419
    new-instance v0, LX/4XB;

    invoke-direct {v0}, LX/4XB;-><init>()V

    .line 1107420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->j()Ljava/lang/String;

    move-result-object v1

    .line 1107421
    iput-object v1, v0, LX/4XB;->b:Ljava/lang/String;

    .line 1107422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 1107423
    iput-object v1, v0, LX/4XB;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1107424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107425
    iput-object v1, v0, LX/4XB;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->m()I

    move-result v1

    .line 1107427
    iput v1, v0, LX/4XB;->e:I

    .line 1107428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    .line 1107429
    iput-object v1, v0, LX/4XB;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 1107430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->o()Ljava/lang/String;

    move-result-object v1

    .line 1107431
    iput-object v1, v0, LX/4XB;->g:Ljava/lang/String;

    .line 1107432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->p()I

    move-result v1

    .line 1107433
    iput v1, v0, LX/4XB;->j:I

    .line 1107434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->q()Z

    move-result v1

    .line 1107435
    iput-boolean v1, v0, LX/4XB;->l:Z

    .line 1107436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->r()Z

    move-result v1

    .line 1107437
    iput-boolean v1, v0, LX/4XB;->m:Z

    .line 1107438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aR()Z

    move-result v1

    .line 1107439
    iput-boolean v1, v0, LX/4XB;->n:Z

    .line 1107440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->s()Z

    move-result v1

    .line 1107441
    iput-boolean v1, v0, LX/4XB;->o:Z

    .line 1107442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->t()Z

    move-result v1

    .line 1107443
    iput-boolean v1, v0, LX/4XB;->p:Z

    .line 1107444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->u()Z

    move-result v1

    .line 1107445
    iput-boolean v1, v0, LX/4XB;->q:Z

    .line 1107446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->v()Z

    move-result v1

    .line 1107447
    iput-boolean v1, v0, LX/4XB;->r:Z

    .line 1107448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->w()Z

    move-result v1

    .line 1107449
    iput-boolean v1, v0, LX/4XB;->s:Z

    .line 1107450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aU()Z

    move-result v1

    .line 1107451
    iput-boolean v1, v0, LX/4XB;->t:Z

    .line 1107452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aV()Z

    move-result v1

    .line 1107453
    iput-boolean v1, v0, LX/4XB;->u:Z

    .line 1107454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->x()Z

    move-result v1

    .line 1107455
    iput-boolean v1, v0, LX/4XB;->v:Z

    .line 1107456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->y()Z

    move-result v1

    .line 1107457
    iput-boolean v1, v0, LX/4XB;->w:Z

    .line 1107458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107459
    iput-object v1, v0, LX/4XB;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->A()J

    move-result-wide v2

    .line 1107461
    iput-wide v2, v0, LX/4XB;->A:J

    .line 1107462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107463
    iput-object v1, v0, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->C()Ljava/lang/String;

    move-result-object v1

    .line 1107465
    iput-object v1, v0, LX/4XB;->C:Ljava/lang/String;

    .line 1107466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107467
    iput-object v1, v0, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v1

    .line 1107469
    iput-object v1, v0, LX/4XB;->F:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 1107470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1107471
    iput-object v1, v0, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1107472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    .line 1107473
    iput-object v1, v0, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 1107474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->H()Z

    move-result v1

    .line 1107475
    iput-boolean v1, v0, LX/4XB;->K:Z

    .line 1107476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->I()I

    move-result v1

    .line 1107477
    iput v1, v0, LX/4XB;->N:I

    .line 1107478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->J()I

    move-result v1

    .line 1107479
    iput v1, v0, LX/4XB;->O:I

    .line 1107480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    .line 1107481
    iput-object v1, v0, LX/4XB;->T:Ljava/lang/String;

    .line 1107482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107483
    iput-object v1, v0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107485
    iput-object v1, v0, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107487
    iput-object v1, v0, LX/4XB;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107489
    iput-object v1, v0, LX/4XB;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107491
    iput-object v1, v0, LX/4XB;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107493
    iput-object v1, v0, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107495
    iput-object v1, v0, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107497
    iput-object v1, v0, LX/4XB;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107499
    iput-object v1, v0, LX/4XB;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107501
    iput-object v1, v0, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107503
    iput-object v1, v0, LX/4XB;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 1107505
    iput-object v1, v0, LX/4XB;->ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 1107506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aa()Z

    move-result v1

    .line 1107507
    iput-boolean v1, v0, LX/4XB;->am:Z

    .line 1107508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ab()Z

    move-result v1

    .line 1107509
    iput-boolean v1, v0, LX/4XB;->an:Z

    .line 1107510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ac()Z

    move-result v1

    .line 1107511
    iput-boolean v1, v0, LX/4XB;->ao:Z

    .line 1107512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ad()Z

    move-result v1

    .line 1107513
    iput-boolean v1, v0, LX/4XB;->at:Z

    .line 1107514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ae()Z

    move-result v1

    .line 1107515
    iput-boolean v1, v0, LX/4XB;->az:Z

    .line 1107516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107517
    iput-object v1, v0, LX/4XB;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107519
    iput-object v1, v0, LX/4XB;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107521
    iput-object v1, v0, LX/4XB;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 1107523
    iput-object v1, v0, LX/4XB;->aD:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1107524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v1

    .line 1107525
    iput-object v1, v0, LX/4XB;->aG:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 1107526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107527
    iput-object v1, v0, LX/4XB;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107529
    iput-object v1, v0, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->an()Ljava/lang/String;

    move-result-object v1

    .line 1107531
    iput-object v1, v0, LX/4XB;->aL:Ljava/lang/String;

    .line 1107532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107533
    iput-object v1, v0, LX/4XB;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ap()Ljava/lang/String;

    move-result-object v1

    .line 1107535
    iput-object v1, v0, LX/4XB;->aO:Ljava/lang/String;

    .line 1107536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107537
    iput-object v1, v0, LX/4XB;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ar()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107539
    iput-object v1, v0, LX/4XB;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1107541
    iput-object v1, v0, LX/4XB;->aS:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1107542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aO()Ljava/lang/String;

    move-result-object v1

    .line 1107543
    iput-object v1, v0, LX/4XB;->aT:Ljava/lang/String;

    .line 1107544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->au()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 1107545
    iput-object v1, v0, LX/4XB;->aU:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1107546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107547
    iput-object v1, v0, LX/4XB;->aV:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v1

    .line 1107549
    iput-object v1, v0, LX/4XB;->aW:LX/0Px;

    .line 1107550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aP()Ljava/lang/String;

    move-result-object v1

    .line 1107551
    iput-object v1, v0, LX/4XB;->aX:Ljava/lang/String;

    .line 1107552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ax()Ljava/lang/String;

    move-result-object v1

    .line 1107553
    iput-object v1, v0, LX/4XB;->aZ:Ljava/lang/String;

    .line 1107554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ay()I

    move-result v1

    .line 1107555
    iput v1, v0, LX/4XB;->bb:I

    .line 1107556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->az()Ljava/lang/String;

    move-result-object v1

    .line 1107557
    iput-object v1, v0, LX/4XB;->bc:Ljava/lang/String;

    .line 1107558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107559
    iput-object v1, v0, LX/4XB;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aB()Ljava/lang/String;

    move-result-object v1

    .line 1107561
    iput-object v1, v0, LX/4XB;->bg:Ljava/lang/String;

    .line 1107562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1107563
    iput-object v1, v0, LX/4XB;->bh:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1107564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aT()Ljava/lang/String;

    move-result-object v1

    .line 1107565
    iput-object v1, v0, LX/4XB;->bi:Ljava/lang/String;

    .line 1107566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v1

    .line 1107567
    iput-object v1, v0, LX/4XB;->bj:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 1107568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107569
    iput-object v1, v0, LX/4XB;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aG()Z

    move-result v1

    .line 1107571
    iput-boolean v1, v0, LX/4XB;->br:Z

    .line 1107572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aH()Z

    move-result v1

    .line 1107573
    iput-boolean v1, v0, LX/4XB;->bs:Z

    .line 1107574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107575
    iput-object v1, v0, LX/4XB;->bC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    .line 1107577
    iput-object v1, v0, LX/4XB;->bG:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 1107578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aK()I

    move-result v1

    .line 1107579
    iput v1, v0, LX/4XB;->bK:I

    .line 1107580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107581
    iput-object v1, v0, LX/4XB;->bN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    .line 1107583
    iput-object v1, v0, LX/4XB;->bP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 1107584
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x4984e12

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1107585
    iput-object v1, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1107586
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    return-object v0
.end method
