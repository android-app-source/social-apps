.class public final LX/5Jp;
.super LX/3me;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 897755
    invoke-direct {p0}, LX/3me;-><init>()V

    .line 897756
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, LX/5Jp;->a:I

    .line 897757
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 897758
    iget v0, p0, LX/5Jp;->a:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 897759
    if-ltz p1, :cond_0

    .line 897760
    iget-object v0, p0, LX/3me;->a:LX/3mY;

    move-object v0, v0

    .line 897761
    invoke-virtual {v0}, LX/3mY;->e()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 897762
    :cond_0
    :goto_0
    return-void

    .line 897763
    :cond_1
    invoke-virtual {p0}, LX/5Jp;->a()I

    move-result v0

    .line 897764
    const/4 v1, 0x0

    sub-int v2, p1, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 897765
    add-int/2addr v0, p1

    .line 897766
    iget-object v2, p0, LX/3me;->a:LX/3mY;

    move-object v2, v2

    .line 897767
    invoke-virtual {v2}, LX/3mY;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 897768
    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 897769
    invoke-virtual {p0, v1, v0, p2}, LX/3me;->a(III)V

    goto :goto_0
.end method
