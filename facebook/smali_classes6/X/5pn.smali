.class public LX/5pn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5pi;

.field public final b:LX/5pi;


# direct methods
.method public constructor <init>(LX/5pi;LX/5pi;)V
    .locals 0

    .prologue
    .line 1008516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008517
    iput-object p1, p0, LX/5pn;->a:LX/5pi;

    .line 1008518
    iput-object p2, p0, LX/5pn;->b:LX/5pi;

    .line 1008519
    return-void
.end method

.method public synthetic constructor <init>(LX/5pi;LX/5pi;B)V
    .locals 0

    .prologue
    .line 1008520
    invoke-direct {p0, p1, p2}, LX/5pn;-><init>(LX/5pi;LX/5pi;)V

    return-void
.end method

.method public static c()LX/5pn;
    .locals 4

    .prologue
    .line 1008521
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    const-string v0, "native_modules"

    const-wide/32 v2, 0x1e8480

    invoke-static {v0, v2, v3}, LX/5pi;->a(Ljava/lang/String;J)LX/5pi;

    move-result-object v0

    .line 1008522
    :goto_0
    invoke-static {}, LX/5pn;->d()LX/5pm;

    move-result-object v1

    const-string v2, "js"

    invoke-static {v2}, LX/5pi;->a(Ljava/lang/String;)LX/5pi;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5pm;->b(LX/5pi;)LX/5pm;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/5pm;->a(LX/5pi;)LX/5pm;

    move-result-object v0

    invoke-virtual {v0}, LX/5pm;->a()LX/5pn;

    move-result-object v0

    return-object v0

    .line 1008523
    :cond_0
    const-string v0, "native_modules"

    invoke-static {v0}, LX/5pi;->a(Ljava/lang/String;)LX/5pi;

    move-result-object v0

    goto :goto_0
.end method

.method private static d()LX/5pm;
    .locals 1

    .prologue
    .line 1008524
    new-instance v0, LX/5pm;

    invoke-direct {v0}, LX/5pm;-><init>()V

    return-object v0
.end method
