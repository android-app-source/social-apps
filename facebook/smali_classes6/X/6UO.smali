.class public final LX/6UO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1101510
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1101511
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1101512
    :goto_0
    return v1

    .line 1101513
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v12, :cond_5

    .line 1101514
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1101515
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1101516
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v9, :cond_0

    .line 1101517
    const-string v12, "length"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1101518
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1101519
    :cond_1
    const-string v12, "start_time"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1101520
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v10

    move v6, v7

    goto :goto_1

    .line 1101521
    :cond_2
    const-string v12, "tip_jar_transactions"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1101522
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1101523
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v12, :cond_3

    .line 1101524
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v12, :cond_3

    .line 1101525
    invoke-static {p0, p1}, LX/6UN;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1101526
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1101527
    :cond_3
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1101528
    goto :goto_1

    .line 1101529
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1101530
    :cond_5
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1101531
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1101532
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1101533
    :cond_6
    if-eqz v6, :cond_7

    move-object v0, p1

    move v1, v7

    move-wide v2, v10

    .line 1101534
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1101535
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1101536
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v10, v4

    move-wide v2, v4

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1101537
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101538
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1101539
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1101540
    const-string v2, "length"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101541
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1101542
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1101543
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1101544
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101545
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1101546
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1101547
    if-eqz v0, :cond_3

    .line 1101548
    const-string v1, "tip_jar_transactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101549
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1101550
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1101551
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/6UN;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1101552
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1101553
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1101554
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101555
    return-void
.end method
