.class public LX/6Nf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6Nf;


# instance fields
.field public final a:LX/2Iv;

.field public final b:LX/3Lo;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/6Ne;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Iv;LX/3Lo;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1083736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083737
    iput-object p1, p0, LX/6Nf;->a:LX/2Iv;

    .line 1083738
    iput-object p2, p0, LX/6Nf;->b:LX/3Lo;

    .line 1083739
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/6Nf;->c:Ljava/util/Map;

    .line 1083740
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LX/6Nf;->d:Ljava/lang/ThreadLocal;

    .line 1083741
    return-void
.end method

.method public static a(LX/0QB;)LX/6Nf;
    .locals 5

    .prologue
    .line 1083742
    sget-object v0, LX/6Nf;->e:LX/6Nf;

    if-nez v0, :cond_1

    .line 1083743
    const-class v1, LX/6Nf;

    monitor-enter v1

    .line 1083744
    :try_start_0
    sget-object v0, LX/6Nf;->e:LX/6Nf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1083745
    if-eqz v2, :cond_0

    .line 1083746
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1083747
    new-instance p0, LX/6Nf;

    invoke-static {v0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v3

    check-cast v3, LX/2Iv;

    invoke-static {v0}, LX/3Lo;->a(LX/0QB;)LX/3Lo;

    move-result-object v4

    check-cast v4, LX/3Lo;

    invoke-direct {p0, v3, v4}, LX/6Nf;-><init>(LX/2Iv;LX/3Lo;)V

    .line 1083748
    move-object v0, p0

    .line 1083749
    sput-object v0, LX/6Nf;->e:LX/6Nf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1083750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1083751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1083752
    :cond_1
    sget-object v0, LX/6Nf;->e:LX/6Nf;

    return-object v0

    .line 1083753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1083754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6Nq;)Lcom/facebook/user/model/PicSquareUrlWithSize;
    .locals 3
    .param p0    # LX/6Nq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1083755
    if-nez p0, :cond_0

    .line 1083756
    const/4 v0, 0x0

    .line 1083757
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, LX/6Nq;->b()I

    move-result v1

    invoke-virtual {p0}, LX/6Nq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/user/model/User;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1083758
    invoke-static {p2}, LX/6Ni;->a(Ljava/nio/ByteBuffer;)LX/6Ni;

    move-result-object v5

    .line 1083759
    iget-object v0, p0, LX/6Nf;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ne;

    .line 1083760
    if-nez v0, :cond_3

    .line 1083761
    new-instance v0, LX/6Ne;

    invoke-direct {v0}, LX/6Ne;-><init>()V

    .line 1083762
    iget-object v1, p0, LX/6Nf;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    move-object v3, v0

    .line 1083763
    :goto_0
    iget-object v0, v3, LX/6Ne;->a:LX/6Ng;

    invoke-virtual {v5, v0}, LX/6Ni;->a(LX/6Ng;)LX/6Ng;

    move-result-object v0

    const-string v1, "Represented profile must not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ng;

    .line 1083764
    invoke-virtual {v0}, LX/6Ng;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FBID must not be null"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1083765
    iget-object v2, v3, LX/6Ne;->b:LX/6Nm;

    invoke-virtual {v5, v2}, LX/6Ni;->a(LX/6Nm;)LX/6Nm;

    move-result-object v2

    invoke-static {v2}, LX/6NW;->a(LX/6Nm;)Lcom/facebook/user/model/Name;

    move-result-object v2

    const-string v6, "User must have name"

    invoke-static {v2, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/Name;

    .line 1083766
    new-instance v6, LX/0XI;

    invoke-direct {v6}, LX/0XI;-><init>()V

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v6, v7, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    .line 1083767
    iput-object v2, v1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1083768
    move-object v1, v1

    .line 1083769
    iget-object v6, p0, LX/6Nf;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6Ne;

    iget-object v6, v6, LX/6Ne;->c:LX/6Nq;

    .line 1083770
    invoke-virtual {v5, v6}, LX/6Ni;->a(LX/6Nq;)LX/6Nq;

    move-result-object v7

    invoke-static {v7}, LX/6Nf;->a(LX/6Nq;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v7

    .line 1083771
    invoke-virtual {v5, v6}, LX/6Ni;->b(LX/6Nq;)LX/6Nq;

    move-result-object v8

    invoke-static {v8}, LX/6Nf;->a(LX/6Nq;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v8

    .line 1083772
    invoke-virtual {v5, v6}, LX/6Ni;->c(LX/6Nq;)LX/6Nq;

    move-result-object v6

    invoke-static {v6}, LX/6Nf;->a(LX/6Nq;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object p2

    .line 1083773
    if-nez v7, :cond_4

    if-nez v8, :cond_4

    if-nez p2, :cond_4

    const/4 v6, 0x0

    :goto_1
    move-object v6, v6

    .line 1083774
    iput-object v6, v1, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1083775
    move-object v1, v1

    .line 1083776
    invoke-virtual {v0}, LX/6Ng;->e()F

    move-result v6

    .line 1083777
    iput v6, v1, LX/0XI;->t:F

    .line 1083778
    move-object v1, v1

    .line 1083779
    invoke-virtual {v0}, LX/6Ng;->q()Ljava/lang/String;

    move-result-object v6

    .line 1083780
    iput-object v6, v1, LX/0XI;->l:Ljava/lang/String;

    .line 1083781
    move-object v1, v1

    .line 1083782
    invoke-virtual {v0}, LX/6Ng;->h()Z

    move-result v6

    invoke-static {v6}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v6

    .line 1083783
    iput-object v6, v1, LX/0XI;->u:LX/03R;

    .line 1083784
    move-object v1, v1

    .line 1083785
    invoke-virtual {v0}, LX/6Ng;->i()Z

    move-result v6

    .line 1083786
    iput-boolean v6, v1, LX/0XI;->z:Z

    .line 1083787
    move-object v1, v1

    .line 1083788
    invoke-virtual {v0}, LX/6Ng;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/6NW;->a(J)J

    move-result-wide v6

    .line 1083789
    iput-wide v6, v1, LX/0XI;->D:J

    .line 1083790
    move-object v1, v1

    .line 1083791
    invoke-virtual {v5}, LX/6Ni;->i()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/6NW;->a(J)J

    move-result-wide v6

    .line 1083792
    iput-wide v6, v1, LX/0XI;->E:J

    .line 1083793
    move-object v6, v1

    .line 1083794
    invoke-virtual {v0}, LX/6Ng;->m()B

    move-result v1

    const/4 v7, 0x2

    if-ne v1, v7, :cond_2

    const/4 v1, 0x1

    .line 1083795
    :goto_2
    iput-boolean v1, v6, LX/0XI;->F:Z

    .line 1083796
    move-object v1, v6

    .line 1083797
    invoke-virtual {v0}, LX/6Ng;->o()Z

    move-result v4

    .line 1083798
    iput-boolean v4, v1, LX/0XI;->M:Z

    .line 1083799
    move-object v1, v1

    .line 1083800
    iget-object v4, v3, LX/6Ne;->b:LX/6Nm;

    invoke-virtual {v5, v4}, LX/6Ni;->b(LX/6Nm;)LX/6Nm;

    move-result-object v4

    invoke-static {v4}, LX/6NW;->a(LX/6Nm;)Lcom/facebook/user/model/Name;

    move-result-object v4

    .line 1083801
    iget-object v6, p0, LX/6Nf;->c:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1083802
    if-eqz v6, :cond_5

    .line 1083803
    :goto_3
    move-object v2, v6

    .line 1083804
    iput-object v2, v1, LX/0XI;->s:Ljava/lang/String;

    .line 1083805
    move-object v1, v1

    .line 1083806
    invoke-virtual {v5}, LX/6Ni;->m()Z

    move-result v2

    .line 1083807
    iput-boolean v2, v1, LX/0XI;->aa:Z

    .line 1083808
    move-object v1, v1

    .line 1083809
    invoke-virtual {v5}, LX/6Ni;->n()B

    move-result v2

    invoke-static {v2}, LX/6NW;->a(B)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    .line 1083810
    invoke-static {v2}, LX/6Ok;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/0XK;

    move-result-object v2

    .line 1083811
    invoke-virtual {v1, v2}, LX/0XI;->a(LX/0XK;)LX/0XI;

    .line 1083812
    iget-object v2, v3, LX/6Ne;->d:LX/6Nl;

    invoke-virtual {v0, v2}, LX/6Ng;->a(LX/6Nl;)LX/6Nl;

    move-result-object v0

    .line 1083813
    if-eqz v0, :cond_0

    .line 1083814
    invoke-virtual {v0}, LX/6Nl;->b()I

    move-result v2

    invoke-virtual {v0}, LX/6Nl;->a()I

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/0XI;->a(II)LX/0XI;

    .line 1083815
    :cond_0
    iget-object v0, v3, LX/6Ne;->c:LX/6Nq;

    invoke-virtual {v5, v0}, LX/6Ni;->a(LX/6Nq;)LX/6Nq;

    move-result-object v0

    .line 1083816
    if-eqz v0, :cond_1

    .line 1083817
    invoke-virtual {v0}, LX/6Nq;->a()Ljava/lang/String;

    move-result-object v0

    .line 1083818
    iput-object v0, v1, LX/0XI;->n:Ljava/lang/String;

    .line 1083819
    :cond_1
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0

    :cond_2
    move v1, v4

    .line 1083820
    goto :goto_2

    :cond_3
    move-object v3, v0

    goto/16 :goto_0

    :cond_4
    new-instance v6, Lcom/facebook/user/model/PicSquare;

    invoke-direct {v6, v7, v8, p2}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    goto/16 :goto_1

    .line 1083821
    :cond_5
    new-instance v6, LX/3hD;

    invoke-direct {v6}, LX/3hD;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v7

    .line 1083822
    iput-object v7, v6, LX/3hD;->a:Ljava/lang/String;

    .line 1083823
    move-object v6, v6

    .line 1083824
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v7

    .line 1083825
    iput-object v7, v6, LX/3hD;->b:Ljava/lang/String;

    .line 1083826
    move-object v6, v6

    .line 1083827
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v7

    .line 1083828
    iput-object v7, v6, LX/3hD;->c:Ljava/lang/String;

    .line 1083829
    move-object v6, v6

    .line 1083830
    if-eqz v4, :cond_6

    .line 1083831
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v7

    .line 1083832
    iput-object v7, v6, LX/3hD;->d:Ljava/lang/String;

    .line 1083833
    move-object v7, v6

    .line 1083834
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v8

    .line 1083835
    iput-object v8, v7, LX/3hD;->e:Ljava/lang/String;

    .line 1083836
    move-object v7, v7

    .line 1083837
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v8

    .line 1083838
    iput-object v8, v7, LX/3hD;->f:Ljava/lang/String;

    .line 1083839
    :cond_6
    iget-object v7, p0, LX/6Nf;->b:LX/3Lo;

    iget-object v8, p0, LX/6Nf;->a:LX/2Iv;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual {v6}, LX/3hD;->a()LX/3hE;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, LX/3Lo;->a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v6

    .line 1083840
    iget-object v7, p0, LX/6Nf;->c:Ljava/util/Map;

    invoke-interface {v7, p1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1083841
    iget-object v0, p0, LX/6Nf;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1083842
    return-void
.end method
