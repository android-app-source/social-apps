.class public final LX/6BC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0uk;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/0TF;

.field public final synthetic e:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic f:Ljava/util/concurrent/Executor;

.field public final synthetic g:Z

.field public final synthetic h:LX/0TF;

.field public final synthetic i:Ljava/util/concurrent/Executor;

.field public final synthetic j:Lcom/facebook/api/prefetch/GraphQLPrefetchController;


# direct methods
.method public constructor <init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;ZLjava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;ZLX/0TF;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1061751
    iput-object p1, p0, LX/6BC;->j:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iput-object p2, p0, LX/6BC;->a:LX/0uk;

    iput-boolean p3, p0, LX/6BC;->b:Z

    iput-object p4, p0, LX/6BC;->c:Ljava/lang/String;

    iput-object p5, p0, LX/6BC;->d:LX/0TF;

    iput-object p6, p0, LX/6BC;->e:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p7, p0, LX/6BC;->f:Ljava/util/concurrent/Executor;

    iput-boolean p8, p0, LX/6BC;->g:Z

    iput-object p9, p0, LX/6BC;->h:LX/0TF;

    iput-object p10, p0, LX/6BC;->i:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1061752
    iget-object v0, p0, LX/6BC;->d:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1061753
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1061754
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1061755
    iget-object v0, p0, LX/6BC;->j:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/6BC;->a:LX/0uk;

    invoke-virtual {v0, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;)LX/1Ep;

    move-result-object v6

    .line 1061756
    if-nez p1, :cond_2

    .line 1061757
    iget-boolean v0, p0, LX/6BC;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6BC;->j:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/6BC;->a:LX/0uk;

    iget-object v2, p0, LX/6BC;->c:Ljava/lang/String;

    iget-object v3, p0, LX/6BC;->d:LX/0TF;

    iget-object v4, p0, LX/6BC;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p0, LX/6BC;->f:Ljava/util/concurrent/Executor;

    .line 1061758
    invoke-static/range {v0 .. v5}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)Z

    move-result v7

    move v0, v7

    .line 1061759
    if-eqz v0, :cond_0

    .line 1061760
    :goto_0
    return-void

    .line 1061761
    :cond_0
    iget-object v0, p0, LX/6BC;->c:Ljava/lang/String;

    invoke-virtual {v6, v0}, LX/1Ep;->b(Ljava/lang/String;)V

    .line 1061762
    :goto_1
    iget-boolean v0, p0, LX/6BC;->g:Z

    if-eqz v0, :cond_1

    .line 1061763
    iget-object v0, p0, LX/6BC;->j:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/6BC;->a:LX/0uk;

    iget-object v2, p0, LX/6BC;->c:Ljava/lang/String;

    iget-object v3, p0, LX/6BC;->h:LX/0TF;

    iget-object v4, p0, LX/6BC;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p0, LX/6BC;->i:Ljava/util/concurrent/Executor;

    .line 1061764
    invoke-static/range {v0 .. v5}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 1061765
    :cond_1
    iget-object v0, p0, LX/6BC;->d:LX/0TF;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/graphql/executor/GraphQLResult;Z)LX/6BE;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 1061766
    :cond_2
    iget-object v0, p0, LX/6BC;->a:LX/0uk;

    invoke-virtual {v0}, LX/0uk;->e()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/6BC;->a:LX/0uk;

    invoke-virtual {v0, p1}, LX/0uk;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1061767
    :cond_3
    iget-object v0, p0, LX/6BC;->c:Ljava/lang/String;

    invoke-virtual {v6, v0}, LX/1Ep;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 1061768
    :cond_4
    iget-object v0, p0, LX/6BC;->c:Ljava/lang/String;

    .line 1061769
    sget-object v1, LX/1Eq;->CACHE_HIT:LX/1Eq;

    invoke-static {v6, v1, v0}, LX/1Ep;->a(LX/1Ep;LX/1Eq;Ljava/lang/String;)V

    .line 1061770
    iget-object v0, p0, LX/6BC;->d:LX/0TF;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/graphql/executor/GraphQLResult;Z)LX/6BE;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
