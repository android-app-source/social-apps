.class public final LX/6Fo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:LX/6Ft;


# direct methods
.method public constructor <init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1068964
    iput-object p1, p0, LX/6Fo;->e:LX/6Ft;

    iput-object p2, p0, LX/6Fo;->a:Ljava/io/File;

    iput-object p3, p0, LX/6Fo;->b:Ljava/io/File;

    iput-object p4, p0, LX/6Fo;->c:Ljava/util/Map;

    iput-object p5, p0, LX/6Fo;->d:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1068965
    iget-object v0, p0, LX/6Fo;->e:LX/6Ft;

    iget-object v0, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aV:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 1068966
    iget-object v2, p0, LX/6Fo;->e:LX/6Ft;

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/6Fo;->a:Ljava/io/File;

    .line 1068967
    :goto_0
    :try_start_0
    const-string v3, "traces.txt"

    invoke-static {v0, v3}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;

    move-result-object v3

    .line 1068968
    new-instance v4, Ljava/io/File;

    const-string v5, "/data/anr/traces.txt"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v3}, LX/6G3;->a(Ljava/io/File;LX/6FR;)V

    .line 1068969
    iget-object v4, v3, LX/6FR;->b:Landroid/net/Uri;

    move-object v3, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068970
    :goto_1
    move-object v2, v3

    .line 1068971
    if-eqz v2, :cond_0

    .line 1068972
    if-eqz v1, :cond_2

    iget-object v0, p0, LX/6Fo;->c:Ljava/util/Map;

    :goto_2
    const-string v1, "traces.txt"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1068973
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 1068974
    :cond_1
    iget-object v0, p0, LX/6Fo;->b:Ljava/io/File;

    goto :goto_0

    .line 1068975
    :cond_2
    iget-object v0, p0, LX/6Fo;->d:Ljava/util/Map;

    goto :goto_2

    .line 1068976
    :catch_0
    move-exception v3

    .line 1068977
    const-string v4, "BugReportWriter"

    const-string v5, "Unable to copy traces file"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068978
    iget-object v3, v2, LX/6Ft;->q:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v3, v4}, LX/6GZ;->a(LX/6GY;)V

    .line 1068979
    const/4 v3, 0x0

    goto :goto_1
.end method
