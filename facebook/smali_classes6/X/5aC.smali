.class public final LX/5aC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 954039
    const/4 v9, 0x0

    .line 954040
    const/4 v8, 0x0

    .line 954041
    const/4 v7, 0x0

    .line 954042
    const/4 v6, 0x0

    .line 954043
    const/4 v5, 0x0

    .line 954044
    const/4 v4, 0x0

    .line 954045
    const-wide/16 v2, 0x0

    .line 954046
    const/4 v1, 0x0

    .line 954047
    const/4 v0, 0x0

    .line 954048
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 954049
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954050
    const/4 v0, 0x0

    .line 954051
    :goto_0
    return v0

    .line 954052
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954053
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 954054
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 954055
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954056
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 954057
    const-string v11, "allows_rsvp"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 954058
    const/4 v1, 0x1

    .line 954059
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 954060
    :cond_2
    const-string v11, "event_reminder_members"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 954061
    invoke-static {p0, p1}, LX/5aB;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 954062
    :cond_3
    const-string v11, "event_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 954063
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 954064
    :cond_4
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 954065
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 954066
    :cond_5
    const-string v11, "lightweight_event_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 954067
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 954068
    :cond_6
    const-string v11, "location_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 954069
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 954070
    :cond_7
    const-string v11, "time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 954071
    const/4 v0, 0x1

    .line 954072
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto/16 :goto_1

    .line 954073
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 954074
    if-eqz v1, :cond_9

    .line 954075
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 954076
    :cond_9
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 954077
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 954078
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 954079
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 954080
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 954081
    if-eqz v0, :cond_a

    .line 954082
    const/4 v1, 0x6

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 954083
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x4

    .line 954084
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954085
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 954086
    if-eqz v0, :cond_0

    .line 954087
    const-string v1, "allows_rsvp"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954088
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 954089
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954090
    if-eqz v0, :cond_1

    .line 954091
    const-string v1, "event_reminder_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954092
    invoke-static {p0, v0, p2, p3}, LX/5aB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954093
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954094
    if-eqz v0, :cond_2

    .line 954095
    const-string v1, "event_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954096
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954097
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954098
    if-eqz v0, :cond_3

    .line 954099
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954100
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954101
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 954102
    if-eqz v0, :cond_4

    .line 954103
    const-string v0, "lightweight_event_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954104
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954105
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954106
    if-eqz v0, :cond_5

    .line 954107
    const-string v1, "location_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954108
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954109
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 954110
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 954111
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954112
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 954113
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954114
    return-void
.end method
