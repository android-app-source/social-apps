.class public LX/68p;
.super LX/67m;
.source ""

# interfaces
.implements LX/67q;


# instance fields
.field private final A:F

.field private final B:F

.field private final C:F

.field private final o:Landroid/graphics/Paint;

.field private final p:Landroid/graphics/Path;

.field private final q:LX/31i;

.field public final r:LX/68u;

.field private s:F

.field private t:F

.field private final u:F

.field private v:F

.field private w:Z

.field private final x:F

.field private final y:F

.field private final z:F


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1056534
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1056535
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68p;->o:Landroid/graphics/Paint;

    .line 1056536
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/68p;->p:Landroid/graphics/Path;

    .line 1056537
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/68p;->q:LX/31i;

    .line 1056538
    const/4 v0, 0x3

    iput v0, p0, LX/68p;->j:I

    .line 1056539
    iput v2, p0, LX/68p;->k:F

    .line 1056540
    const/high16 v0, 0x41000000    # 8.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->u:F

    .line 1056541
    const/high16 v0, 0x41300000    # 11.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->y:F

    .line 1056542
    iget v0, p0, LX/68p;->y:F

    const/high16 v1, 0x3fc00000    # 1.5f

    add-float/2addr v0, v1

    iput v0, p0, LX/68p;->x:F

    .line 1056543
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->z:F

    .line 1056544
    const/high16 v0, 0x41200000    # 10.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->A:F

    .line 1056545
    const/high16 v0, 0x41c00000    # 24.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->B:F

    .line 1056546
    iget v0, p0, LX/68p;->B:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iput v0, p0, LX/68p;->C:F

    .line 1056547
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v2, v0}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/68p;->r:LX/68u;

    .line 1056548
    iget-object v0, p0, LX/68p;->r:LX/68u;

    const/4 v1, -0x1

    .line 1056549
    iput v1, v0, LX/68u;->x:I

    .line 1056550
    iget-object v0, p0, LX/68p;->r:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67q;)V

    .line 1056551
    iget-object v0, p0, LX/68p;->r:LX/68u;

    const-wide/16 v2, 0x834

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1056552
    return-void
.end method


# virtual methods
.method public final a(LX/68u;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3e800000    # 0.25f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1056553
    iget v0, p1, LX/68u;->C:F

    move v0, v0

    .line 1056554
    iput v0, p0, LX/68p;->s:F

    .line 1056555
    iget v0, p0, LX/68p;->s:F

    iget v1, p0, LX/68p;->t:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1056556
    iget-boolean v0, p0, LX/68p;->w:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/68p;->w:Z

    .line 1056557
    :cond_0
    iget-boolean v0, p0, LX/68p;->w:Z

    if-eqz v0, :cond_2

    .line 1056558
    iget v0, p0, LX/68p;->s:F

    sub-float v0, v2, v0

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    iput v0, p0, LX/68p;->v:F

    .line 1056559
    :goto_1
    iget v0, p0, LX/68p;->s:F

    iput v0, p0, LX/68p;->t:F

    .line 1056560
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1056561
    return-void

    .line 1056562
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1056563
    :cond_2
    iget v0, p0, LX/68p;->s:F

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    iput v0, p0, LX/68p;->v:F

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const v12, -0xc4a863

    const/high16 v11, 0x437f0000    # 255.0f

    const/high16 v10, 0x40000000    # 2.0f

    .line 1056564
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->e()Landroid/location/Location;

    move-result-object v0

    .line 1056565
    if-eqz v0, :cond_0

    .line 1056566
    iget v1, p0, LX/68p;->B:F

    iget v2, p0, LX/68p;->C:F

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 1056567
    iget-object v1, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {v1, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056568
    iget-object v1, p0, LX/68p;->o:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, LX/68p;->s:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v11

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056569
    iget-object v1, p0, LX/67m;->f:LX/31h;

    iget-object v2, p0, LX/68p;->q:LX/31i;

    invoke-virtual {v1, v2}, LX/31h;->a(LX/31i;)V

    .line 1056570
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, LX/31h;->d(D)F

    move-result v1

    float-to-double v2, v1

    .line 1056571
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, LX/31h;->b(D)F

    move-result v1

    float-to-double v4, v1

    .line 1056572
    iget-object v1, p0, LX/68p;->q:LX/31i;

    iget-wide v8, v1, LX/31i;->c:D

    sub-double/2addr v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 1056573
    iget-object v1, p0, LX/67m;->f:LX/31h;

    int-to-double v8, v6

    add-double/2addr v2, v8

    iget-object v6, p0, LX/67m;->c:[F

    invoke-virtual/range {v1 .. v6}, LX/31h;->a(DD[F)V

    .line 1056574
    iget-object v1, p0, LX/67m;->c:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 1056575
    iget-object v2, p0, LX/67m;->c:[F

    aget v2, v2, v13

    .line 1056576
    iget v3, p0, LX/68p;->s:F

    mul-float/2addr v3, v7

    iget-object v4, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056577
    iget-object v3, p0, LX/68p;->o:Landroid/graphics/Paint;

    const v4, -0x333334

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056578
    iget v3, p0, LX/68p;->x:F

    iget-object v4, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056579
    iget-object v3, p0, LX/68p;->o:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056580
    iget v3, p0, LX/68p;->y:F

    iget-object v4, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056581
    iget-object v3, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {v3, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056582
    iget-object v3, p0, LX/68p;->o:Landroid/graphics/Paint;

    iget v4, p0, LX/68p;->v:F

    mul-float/2addr v4, v11

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056583
    iget v3, p0, LX/68p;->v:F

    iget v4, p0, LX/68p;->u:F

    mul-float/2addr v3, v4

    iget-object v4, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056584
    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1056585
    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->save(I)I

    .line 1056586
    iget-object v3, p0, LX/67m;->f:LX/31h;

    invoke-virtual {v3}, LX/31h;->b()F

    move-result v3

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    add-float/2addr v0, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1056587
    iget v0, p0, LX/68p;->z:F

    div-float/2addr v0, v10

    sub-float v0, v1, v0

    .line 1056588
    iget v1, p0, LX/68p;->x:F

    sub-float v1, v2, v1

    .line 1056589
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1056590
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1056591
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    iget v3, p0, LX/68p;->z:F

    div-float/2addr v3, v10

    add-float/2addr v3, v0

    iget v4, p0, LX/68p;->A:F

    sub-float v4, v1, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1056592
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    iget v3, p0, LX/68p;->z:F

    add-float/2addr v3, v0

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1056593
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    const/high16 v3, 0x3f000000    # 0.5f

    iget v4, p0, LX/68p;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    const/high16 v4, 0x3e800000    # 0.25f

    iget v5, p0, LX/68p;->A:F

    mul-float/2addr v4, v5

    sub-float v4, v1, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1056594
    iget-object v2, p0, LX/68p;->p:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1056595
    iget-object v0, p0, LX/68p;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1056596
    iget-object v0, p0, LX/68p;->p:Landroid/graphics/Path;

    iget-object v1, p0, LX/68p;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1056597
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1056598
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1056599
    iget-object v0, p0, LX/68p;->r:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1056600
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1056601
    return-void
.end method
