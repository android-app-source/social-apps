.class public final LX/557;
.super Landroid/view/View$AccessibilityDelegate;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/0wL;


# direct methods
.method public constructor <init>(LX/0wL;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 829028
    iput-object p1, p0, LX/557;->c:LX/0wL;

    iput p2, p0, LX/557;->a:I

    iput-object p3, p0, LX/557;->b:Landroid/view/View;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 829029
    iget v0, p0, LX/557;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 829030
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    const v1, 0x7f0d0094

    iget-object v2, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isClickable()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 829031
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    const v1, 0x7f0d0095

    iget-object v2, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isFocusable()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 829032
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    const v1, 0x7f0d0096

    iget-object v2, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isLongClickable()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 829033
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    const v1, 0x7f0d0097

    iget-object v2, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 829034
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 829035
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 829036
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setLongClickable(Z)V

    .line 829037
    iget-object v0, p0, LX/557;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 829038
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 829039
    :cond_0
    return-void
.end method

.method public final onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 829040
    iget v0, p0, LX/557;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    if-nez p1, :cond_1

    .line 829041
    :cond_0
    const/4 v0, 0x0

    .line 829042
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 829043
    const v0, 0x8000

    if-eq p2, v0, :cond_0

    const/16 v0, 0x80

    if-eq p2, v0, :cond_0

    .line 829044
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    .line 829045
    :cond_0
    return-void
.end method
