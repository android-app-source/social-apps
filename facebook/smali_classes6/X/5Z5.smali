.class public final LX/5Z5;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 944023
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;

    const v0, 0x3c6c07c8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "MoreThreadsQuery"

    const-string v6, "5fccb1e31c8e5b1fa7bb75447e1df94e"

    const-string v7, "viewer"

    const-string v8, "10155265581926729"

    const-string v9, "10155259696861729"

    const-string v0, "actor_id"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 944024
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 944025
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 944026
    sparse-switch v0, :sswitch_data_0

    .line 944027
    :goto_0
    return-object p1

    .line 944028
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 944029
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 944030
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 944031
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 944032
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 944033
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 944034
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 944035
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 944036
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 944037
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 944038
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 944039
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 944040
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 944041
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 944042
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 944043
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 944044
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 944045
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 944046
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 944047
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 944048
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 944049
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 944050
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 944051
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 944052
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 944053
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 944054
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_a
        -0x723fdf0b -> :sswitch_0
        -0x7226305f -> :sswitch_19
        -0x5f54881d -> :sswitch_5
        -0x5bcbf522 -> :sswitch_16
        -0x56855c4a -> :sswitch_14
        -0x4c47f2a9 -> :sswitch_13
        -0x4450092f -> :sswitch_f
        -0x39e54905 -> :sswitch_1a
        -0x3155dbb7 -> :sswitch_3
        -0x26451294 -> :sswitch_2
        -0x1b236af7 -> :sswitch_18
        -0x179abbec -> :sswitch_17
        -0x10df6d66 -> :sswitch_15
        -0xf820fe3 -> :sswitch_9
        -0x786d0bb -> :sswitch_d
        -0x3224078 -> :sswitch_e
        -0x132889c -> :sswitch_12
        -0x8d30fe -> :sswitch_c
        0x8da57ae -> :sswitch_6
        0xe0e2e5a -> :sswitch_8
        0x19ec4b2a -> :sswitch_1
        0x2f1911b0 -> :sswitch_10
        0x3349e8c0 -> :sswitch_11
        0x5af48aaa -> :sswitch_4
        0x5ba7488b -> :sswitch_b
        0x69308369 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 944055
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 944056
    :goto_1
    return v0

    .line 944057
    :pswitch_1
    const-string v2, "25"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 944058
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944059
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x641
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
