.class public final LX/5Yh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 942099
    const/4 v14, 0x0

    .line 942100
    const/4 v13, 0x0

    .line 942101
    const/4 v12, 0x0

    .line 942102
    const/4 v11, 0x0

    .line 942103
    const/4 v10, 0x0

    .line 942104
    const/4 v7, 0x0

    .line 942105
    const-wide/16 v8, 0x0

    .line 942106
    const/4 v6, 0x0

    .line 942107
    const/4 v5, 0x0

    .line 942108
    const/4 v4, 0x0

    .line 942109
    const/4 v3, 0x0

    .line 942110
    const/4 v2, 0x0

    .line 942111
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 942112
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 942113
    const/4 v2, 0x0

    .line 942114
    :goto_0
    return v2

    .line 942115
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v3, v0, :cond_c

    .line 942116
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 942117
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 942118
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 942119
    const-string v16, "booking_status"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 942120
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 942121
    :cond_1
    const-string v16, "id"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 942122
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 942123
    :cond_2
    const-string v16, "page"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 942124
    invoke-static/range {p0 .. p1}, LX/5Yd;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 942125
    :cond_3
    const-string v16, "product_item"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 942126
    invoke-static/range {p0 .. p1}, LX/5Ye;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 942127
    :cond_4
    const-string v16, "service_general_info"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 942128
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto/16 :goto_1

    .line 942129
    :cond_5
    const-string v16, "special_request"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 942130
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto/16 :goto_1

    .line 942131
    :cond_6
    const-string v16, "start_time"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 942132
    const/4 v2, 0x1

    .line 942133
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 942134
    :cond_7
    const-string v16, "status"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 942135
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 942136
    :cond_8
    const-string v16, "suggested_time_range"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 942137
    invoke-static/range {p0 .. p1}, LX/5Yf;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 942138
    :cond_9
    const-string v16, "user"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 942139
    invoke-static/range {p0 .. p1}, LX/5Yg;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 942140
    :cond_a
    const-string v16, "user_availability"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 942141
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 942142
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 942143
    :cond_c
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 942144
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 942145
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 942146
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 942147
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 942148
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 942149
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 942150
    if-eqz v2, :cond_d

    .line 942151
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 942152
    :cond_d
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 942153
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 942154
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 942155
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 942156
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v6

    move v6, v7

    move v7, v10

    move v10, v5

    move-wide/from16 v18, v8

    move v9, v4

    move v8, v3

    move-wide/from16 v4, v18

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 942157
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 942158
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 942159
    if-eqz v0, :cond_0

    .line 942160
    const-string v0, "booking_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942161
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942162
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942163
    if-eqz v0, :cond_1

    .line 942164
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942165
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942166
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 942167
    if-eqz v0, :cond_2

    .line 942168
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942169
    invoke-static {p0, v0, p2, p3}, LX/5Yd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 942170
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 942171
    if-eqz v0, :cond_3

    .line 942172
    const-string v1, "product_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942173
    invoke-static {p0, v0, p2, p3}, LX/5Ye;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 942174
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942175
    if-eqz v0, :cond_4

    .line 942176
    const-string v1, "service_general_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942177
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942178
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942179
    if-eqz v0, :cond_5

    .line 942180
    const-string v1, "special_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942181
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942182
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 942183
    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 942184
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942185
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 942186
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942187
    if-eqz v0, :cond_7

    .line 942188
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942189
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942190
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 942191
    if-eqz v0, :cond_8

    .line 942192
    const-string v1, "suggested_time_range"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942193
    invoke-static {p0, v0, p2}, LX/5Yf;->a(LX/15i;ILX/0nX;)V

    .line 942194
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 942195
    if-eqz v0, :cond_9

    .line 942196
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942197
    invoke-static {p0, v0, p2}, LX/5Yg;->a(LX/15i;ILX/0nX;)V

    .line 942198
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 942199
    if-eqz v0, :cond_a

    .line 942200
    const-string v1, "user_availability"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 942201
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 942202
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 942203
    return-void
.end method
