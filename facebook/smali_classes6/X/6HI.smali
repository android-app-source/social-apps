.class public LX/6HI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public c:Landroid/hardware/Camera;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private final h:LX/6HF;

.field private final i:LX/6HU;

.field public j:Landroid/hardware/Camera$Parameters;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 1071597
    const-class v0, LX/6HI;

    sput-object v0, LX/6HI;->d:Ljava/lang/Class;

    .line 1071598
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, LX/6HI;->a:Ljava/util/HashMap;

    .line 1071599
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, LX/6HI;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/6HU;)V
    .locals 3

    .prologue
    .line 1071600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071601
    const-string v0, "auto"

    iput-object v0, p0, LX/6HI;->f:Ljava/lang/String;

    .line 1071602
    const-string v0, "auto"

    iput-object v0, p0, LX/6HI;->g:Ljava/lang/String;

    .line 1071603
    iput-object p2, p0, LX/6HI;->h:LX/6HF;

    .line 1071604
    iput-object p3, p0, LX/6HI;->i:LX/6HU;

    .line 1071605
    sget-object v0, LX/6HI;->a:Ljava/util/HashMap;

    const-string v1, "on"

    const v2, 0x7f0201bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071606
    sget-object v0, LX/6HI;->a:Ljava/util/HashMap;

    const-string v1, "torch"

    const v2, 0x7f0201bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071607
    sget-object v0, LX/6HI;->a:Ljava/util/HashMap;

    const-string v1, "off"

    const v2, 0x7f0201be

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071608
    sget-object v0, LX/6HI;->a:Ljava/util/HashMap;

    const-string v1, "auto"

    const v2, 0x7f0201bc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071609
    sget-object v0, LX/6HI;->b:Ljava/util/HashMap;

    const-string v1, "on"

    const v2, 0x7f0201c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071610
    sget-object v0, LX/6HI;->b:Ljava/util/HashMap;

    const-string v1, "torch"

    const v2, 0x7f0201c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071611
    sget-object v0, LX/6HI;->b:Ljava/util/HashMap;

    const-string v1, "off"

    const v2, 0x7f0201bf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071612
    sget-object v0, LX/6HI;->b:Ljava/util/HashMap;

    const-string v1, "auto"

    const v2, 0x7f0201bd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071613
    sget-object v0, LX/6Ho;->b:LX/0Tn;

    const-string v1, "auto"

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6HI;->g:Ljava/lang/String;

    .line 1071614
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/hardware/Camera$Parameters;Z)Landroid/hardware/Camera$Parameters;
    .locals 1

    .prologue
    .line 1071615
    invoke-virtual {p2, p1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 1071616
    iput-object p1, p0, LX/6HI;->g:Ljava/lang/String;

    .line 1071617
    iget-object v0, p0, LX/6HI;->h:LX/6HF;

    invoke-interface {v0, p1, p3}, LX/6HF;->a(Ljava/lang/String;Z)V

    .line 1071618
    return-object p2
.end method


# virtual methods
.method public final a(Landroid/hardware/Camera;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1071619
    iput-object p1, p0, LX/6HI;->c:Landroid/hardware/Camera;

    .line 1071620
    iget-object v0, p0, LX/6HI;->c:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    .line 1071621
    const/4 v0, 0x0

    iput-object v0, p0, LX/6HI;->e:Ljava/util/List;

    .line 1071622
    :cond_0
    :goto_0
    return-void

    .line 1071623
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6HI;->e:Ljava/util/List;

    .line 1071624
    iget-object v0, p0, LX/6HI;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    .line 1071625
    iget-object v0, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v0

    .line 1071626
    if-eqz v0, :cond_0

    .line 1071627
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1071628
    sget-object v2, LX/6HI;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1071629
    iget-object v2, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1071630
    :cond_3
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1071631
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    iget-object v1, p0, LX/6HI;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1071632
    iget-object v0, p0, LX/6HI;->g:Ljava/lang/String;

    iget-object v1, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0, v1, v3}, LX/6HI;->a(Ljava/lang/String;Landroid/hardware/Camera$Parameters;Z)Landroid/hardware/Camera$Parameters;

    .line 1071633
    :goto_2
    :try_start_0
    iget-object v0, p0, LX/6HI;->c:Landroid/hardware/Camera;

    iget-object v1, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1071634
    :catch_0
    move-exception v0

    .line 1071635
    iget-object v1, p0, LX/6HI;->h:LX/6HF;

    const-string v2, "setCamera/setParameters failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1071636
    :cond_4
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1071637
    const-string v0, "auto"

    iget-object v1, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0, v1, v3}, LX/6HI;->a(Ljava/lang/String;Landroid/hardware/Camera$Parameters;Z)Landroid/hardware/Camera$Parameters;

    goto :goto_2

    .line 1071638
    :cond_5
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-direct {p0, v0, v1, v3}, LX/6HI;->a(Ljava/lang/String;Landroid/hardware/Camera$Parameters;Z)Landroid/hardware/Camera$Parameters;

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1071639
    iget-object v2, p0, LX/6HI;->c:Landroid/hardware/Camera;

    if-nez v2, :cond_1

    .line 1071640
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1071641
    iget-object v2, p0, LX/6HI;->c:Landroid/hardware/Camera;

    if-nez v2, :cond_1

    .line 1071642
    :cond_0
    :goto_0
    return v0

    .line 1071643
    :cond_1
    iget-object v2, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1071644
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1071645
    if-eq v2, v1, :cond_2

    if-nez v2, :cond_0

    .line 1071646
    :cond_2
    iget-object v0, p0, LX/6HI;->i:LX/6HU;

    .line 1071647
    iget-boolean v3, v0, LX/6HU;->s:Z

    if-eqz v3, :cond_6

    iget-object v3, v0, LX/6HU;->n:LX/6Ha;

    const/4 v4, 0x1

    .line 1071648
    iget p2, v3, LX/6Ha;->c:I

    if-eq p2, v4, :cond_3

    iget p2, v3, LX/6Ha;->c:I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_7

    :cond_3
    :goto_1
    move v3, v4

    .line 1071649
    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 1071650
    if-eqz v0, :cond_4

    .line 1071651
    iget-object v0, p0, LX/6HI;->c:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    .line 1071652
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1071653
    if-nez v2, :cond_5

    .line 1071654
    sget-object v2, LX/6HI;->b:Ljava/util/HashMap;

    iget-object v3, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_4
    :goto_3
    move v0, v1

    .line 1071655
    goto :goto_0

    .line 1071656
    :cond_5
    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    rem-int v2, v0, v2

    .line 1071657
    iget-object v0, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v3, v1}, LX/6HI;->a(Ljava/lang/String;Landroid/hardware/Camera$Parameters;Z)Landroid/hardware/Camera$Parameters;

    .line 1071658
    sget-object v0, LX/6HI;->a:Ljava/util/HashMap;

    iget-object v4, p0, LX/6HI;->e:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1071659
    :try_start_0
    iget-object v0, p0, LX/6HI;->c:Landroid/hardware/Camera;

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1071660
    :catch_0
    move-exception v0

    .line 1071661
    iget-object v2, p0, LX/6HI;->h:LX/6HF;

    const-string v3, "onTouch/setParameters failed"

    invoke-interface {v2, v3, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    :cond_7
    const/4 v4, 0x0

    goto :goto_1
.end method
