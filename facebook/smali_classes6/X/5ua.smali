.class public LX/5ua;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Landroid/graphics/RectF;


# instance fields
.field public c:LX/5uZ;

.field public d:LX/5uh;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:F

.field public j:F

.field public final k:Landroid/graphics/RectF;

.field public final l:Landroid/graphics/RectF;

.field public final m:Landroid/graphics/RectF;

.field public final n:Landroid/graphics/Matrix;

.field public final o:Landroid/graphics/Matrix;

.field private final p:Landroid/graphics/Matrix;

.field public final q:[F

.field private final r:Landroid/graphics/RectF;

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1019731
    const-class v0, LX/5ua;

    sput-object v0, LX/5ua;->a:Ljava/lang/Class;

    .line 1019732
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/5ua;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(LX/5uZ;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1019711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1019712
    const/4 v0, 0x0

    iput-object v0, p0, LX/5ua;->d:LX/5uh;

    .line 1019713
    iput-boolean v1, p0, LX/5ua;->e:Z

    .line 1019714
    iput-boolean v1, p0, LX/5ua;->f:Z

    .line 1019715
    iput-boolean v2, p0, LX/5ua;->g:Z

    .line 1019716
    iput-boolean v2, p0, LX/5ua;->h:Z

    .line 1019717
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/5ua;->i:F

    .line 1019718
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/5ua;->j:F

    .line 1019719
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ua;->k:Landroid/graphics/RectF;

    .line 1019720
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ua;->l:Landroid/graphics/RectF;

    .line 1019721
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ua;->m:Landroid/graphics/RectF;

    .line 1019722
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/5ua;->n:Landroid/graphics/Matrix;

    .line 1019723
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    .line 1019724
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/5ua;->p:Landroid/graphics/Matrix;

    .line 1019725
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, LX/5ua;->q:[F

    .line 1019726
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/5ua;->r:Landroid/graphics/RectF;

    .line 1019727
    iput-object p1, p0, LX/5ua;->c:LX/5uZ;

    .line 1019728
    iget-object v0, p0, LX/5ua;->c:LX/5uZ;

    .line 1019729
    iput-object p0, v0, LX/5uZ;->b:LX/5ua;

    .line 1019730
    return-void
.end method

.method private static a(FFFFF)F
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1019697
    sub-float v0, p1, p0

    sub-float v1, p3, p2

    .line 1019698
    sub-float v2, p4, p2

    sub-float v3, p3, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    mul-float/2addr v2, v4

    .line 1019699
    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 1019700
    add-float v0, p1, p0

    div-float/2addr v0, v4

    sub-float v0, p4, v0

    .line 1019701
    :goto_0
    return v0

    .line 1019702
    :cond_0
    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1019703
    add-float v0, p2, p3

    div-float/2addr v0, v4

    cmpg-float v0, p4, v0

    if-gez v0, :cond_1

    .line 1019704
    sub-float v0, p2, p0

    goto :goto_0

    .line 1019705
    :cond_1
    sub-float v0, p3, p1

    goto :goto_0

    .line 1019706
    :cond_2
    cmpl-float v0, p0, p2

    if-lez v0, :cond_3

    .line 1019707
    sub-float v0, p2, p0

    goto :goto_0

    .line 1019708
    :cond_3
    cmpg-float v0, p1, p3

    if-gez v0, :cond_4

    .line 1019709
    sub-float v0, p3, p1

    goto :goto_0

    .line 1019710
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(II)Z
    .locals 1

    .prologue
    .line 1019696
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/5ua;Landroid/graphics/Matrix;FFI)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1019687
    const/4 v1, 0x4

    invoke-static {p4, v1}, LX/5ua;->a(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1019688
    :cond_0
    :goto_0
    return v0

    .line 1019689
    :cond_1
    invoke-direct {p0, p1}, LX/5ua;->c(Landroid/graphics/Matrix;)F

    move-result v1

    .line 1019690
    iget v2, p0, LX/5ua;->i:F

    iget v3, p0, LX/5ua;->j:F

    .line 1019691
    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result p0

    invoke-static {p0, v3}, Ljava/lang/Math;->min(FF)F

    move-result p0

    move v2, p0

    .line 1019692
    cmpl-float v3, v2, v1

    if-eqz v3, :cond_0

    .line 1019693
    div-float v0, v2, v1

    .line 1019694
    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1019695
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/5ua;Landroid/graphics/Matrix;I)Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1019674
    const/4 v0, 0x3

    invoke-static {p2, v0}, LX/5ua;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 1019675
    :goto_0
    return v0

    .line 1019676
    :cond_0
    iget-object v4, p0, LX/5ua;->r:Landroid/graphics/RectF;

    .line 1019677
    iget-object v0, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v4, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1019678
    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1019679
    invoke-static {p2, v3}, LX/5ua;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, v4, Landroid/graphics/RectF;->left:F

    iget v5, v4, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    invoke-static {v0, v5, v6, v7, v8}, LX/5ua;->a(FFFFF)F

    move-result v0

    .line 1019680
    :goto_1
    const/4 v5, 0x2

    invoke-static {p2, v5}, LX/5ua;->a(II)Z

    move-result v5

    if-eqz v5, :cond_3

    iget v5, v4, Landroid/graphics/RectF;->top:F

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-static {v5, v4, v6, v7, v8}, LX/5ua;->a(FFFFF)F

    move-result v4

    .line 1019681
    :goto_2
    cmpl-float v5, v0, v1

    if-nez v5, :cond_1

    cmpl-float v1, v4, v1

    if-eqz v1, :cond_4

    .line 1019682
    :cond_1
    invoke-virtual {p1, v0, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v0, v3

    .line 1019683
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1019684
    goto :goto_1

    :cond_3
    move v4, v1

    .line 1019685
    goto :goto_2

    :cond_4
    move v0, v2

    .line 1019686
    goto :goto_0
.end method

.method private c(Landroid/graphics/Matrix;)F
    .locals 2

    .prologue
    .line 1019672
    iget-object v0, p0, LX/5ua;->q:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1019673
    iget-object v0, p0, LX/5ua;->q:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public static c(LX/5ua;)V
    .locals 3

    .prologue
    .line 1019666
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/5ua;->m:Landroid/graphics/RectF;

    iget-object v2, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1019667
    iget-object v0, p0, LX/5ua;->d:LX/5uh;

    if-eqz v0, :cond_0

    .line 1019668
    iget-boolean v0, p0, LX/5ua;->e:Z

    move v0, v0

    .line 1019669
    if-eqz v0, :cond_0

    .line 1019670
    iget-object v0, p0, LX/5ua;->d:LX/5uh;

    iget-object v1, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, LX/5uh;->a(Landroid/graphics/Matrix;)V

    .line 1019671
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1019656
    iget-object v1, p0, LX/5ua;->q:[F

    .line 1019657
    iget v0, p1, Landroid/graphics/PointF;->x:F

    aput v0, v1, v2

    .line 1019658
    iget v0, p1, Landroid/graphics/PointF;->y:F

    aput v0, v1, v5

    .line 1019659
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    iget-object v3, p0, LX/5ua;->p:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1019660
    iget-object v0, p0, LX/5ua;->p:Landroid/graphics/Matrix;

    move-object v3, v1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapPoints([FI[FII)V

    .line 1019661
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    .line 1019662
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x0

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x0

    aget v4, v1, v4

    iget-object p1, p0, LX/5ua;->l:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, p1

    iget-object p1, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result p1

    div-float/2addr v4, p1

    aput v4, v1, v3

    .line 1019663
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v4, v1, v4

    iget-object p1, p0, LX/5ua;->l:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, p1

    iget-object p1, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    div-float/2addr v4, p1

    aput v4, v1, v3

    .line 1019664
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1019665
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    aget v2, v1, v2

    aget v1, v1, v5

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1019733
    iget-object v0, p0, LX/5ua;->c:LX/5uZ;

    .line 1019734
    iget-object v1, v0, LX/5uZ;->a:LX/5uY;

    invoke-virtual {v1}, LX/5uY;->b()V

    .line 1019735
    iget-object v0, p0, LX/5ua;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1019736
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1019737
    invoke-static {p0}, LX/5ua;->c(LX/5ua;)V

    .line 1019738
    return-void
.end method

.method public a(FLandroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 6

    .prologue
    .line 1019653
    iget-object v1, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    const/4 v5, 0x7

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, LX/5ua;->a(Landroid/graphics/Matrix;FLandroid/graphics/PointF;Landroid/graphics/PointF;I)Z

    .line 1019654
    invoke-static {p0}, LX/5ua;->c(LX/5ua;)V

    .line 1019655
    return-void
.end method

.method public a(LX/5uZ;)V
    .locals 2

    .prologue
    .line 1019647
    iget-object v0, p0, LX/5ua;->n:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1019648
    const p1, 0x3a83126f    # 0.001f

    .line 1019649
    iget-object v0, p0, LX/5ua;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, p1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, LX/5ua;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, p1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, LX/5ua;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, LX/5ua;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, LX/5ua;->k:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, p1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1019650
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/5ua;->s:Z

    .line 1019651
    return-void

    .line 1019652
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 3

    .prologue
    .line 1019645
    sget-object v0, LX/5ua;->b:Landroid/graphics/RectF;

    iget-object v1, p0, LX/5ua;->m:Landroid/graphics/RectF;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1019646
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;FLandroid/graphics/PointF;Landroid/graphics/PointF;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1019631
    iget-object v0, p0, LX/5ua;->q:[F

    .line 1019632
    iget v1, p3, Landroid/graphics/PointF;->x:F

    aput v1, v0, v6

    .line 1019633
    iget v1, p3, Landroid/graphics/PointF;->y:F

    aput v1, v0, v5

    .line 1019634
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    .line 1019635
    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x0

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x0

    aget v3, v0, v3

    iget-object v4, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    iget-object v4, p0, LX/5ua;->l:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    aput v3, v0, v2

    .line 1019636
    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    aget v3, v0, v3

    iget-object v4, p0, LX/5ua;->l:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    mul-float/2addr v3, v4

    iget-object v4, p0, LX/5ua;->l:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    aput v3, v0, v2

    .line 1019637
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1019638
    :cond_0
    iget v1, p4, Landroid/graphics/PointF;->x:F

    aget v2, v0, v6

    sub-float/2addr v1, v2

    .line 1019639
    iget v2, p4, Landroid/graphics/PointF;->y:F

    aget v3, v0, v5

    sub-float/2addr v2, v3

    .line 1019640
    aget v3, v0, v6

    aget v4, v0, v5

    invoke-virtual {p1, p2, p2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 1019641
    aget v3, v0, v6

    aget v0, v0, v5

    invoke-static {p0, p1, v3, v0, p5}, LX/5ua;->a(LX/5ua;Landroid/graphics/Matrix;FFI)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1019642
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1019643
    invoke-static {p0, p1, p5}, LX/5ua;->b(LX/5ua;Landroid/graphics/Matrix;I)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1019644
    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1019583
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 1019584
    iget-boolean v0, p0, LX/5ua;->e:Z

    if-eqz v0, :cond_1

    .line 1019585
    iget-object v0, p0, LX/5ua;->c:LX/5uZ;

    .line 1019586
    iget-object v1, v0, LX/5uZ;->a:LX/5uY;

    .line 1019587
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1019588
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x1

    move v1, v2

    .line 1019589
    move v0, v1

    .line 1019590
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1019591
    :pswitch_1
    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x2

    if-ge v2, v3, :cond_3

    .line 1019592
    iget-object v3, v1, LX/5uY;->d:[I

    aget v3, v3, v2

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    .line 1019593
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 1019594
    iget-object v4, v1, LX/5uY;->g:[F

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    aput v5, v4, v2

    .line 1019595
    iget-object v4, v1, LX/5uY;->h:[F

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    aput v3, v4, v2

    .line 1019596
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1019597
    :cond_3
    iget-boolean v2, v1, LX/5uY;->a:Z

    if-nez v2, :cond_4

    iget v2, v1, LX/5uY;->b:I

    if-lez v2, :cond_4

    .line 1019598
    const/4 v2, 0x1

    move v2, v2

    .line 1019599
    if-eqz v2, :cond_4

    .line 1019600
    invoke-static {v1}, LX/5uY;->j(LX/5uY;)V

    .line 1019601
    :cond_4
    iget-boolean v2, v1, LX/5uY;->a:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/5uY;->i:LX/5uZ;

    if-eqz v2, :cond_0

    .line 1019602
    iget-object v2, v1, LX/5uY;->i:LX/5uZ;

    .line 1019603
    iget-object v3, v2, LX/5uZ;->b:LX/5ua;

    if-eqz v3, :cond_5

    .line 1019604
    iget-object v3, v2, LX/5uZ;->b:LX/5ua;

    invoke-virtual {v3, v2}, LX/5ua;->b(LX/5uZ;)V

    .line 1019605
    :cond_5
    goto :goto_0

    .line 1019606
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 1019607
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 1019608
    const/4 v4, 0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x6

    if-ne v3, v4, :cond_7

    .line 1019609
    :cond_6
    add-int/lit8 v2, v2, -0x1

    .line 1019610
    :cond_7
    move v2, v2

    .line 1019611
    iput v2, v1, LX/5uY;->c:I

    .line 1019612
    invoke-static {v1}, LX/5uY;->k(LX/5uY;)V

    .line 1019613
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 1019614
    iput v2, v1, LX/5uY;->b:I

    .line 1019615
    :goto_3
    const/4 v3, 0x2

    if-ge v2, v3, :cond_9

    .line 1019616
    invoke-static {p1, v2}, LX/5uY;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1019617
    if-ne v3, v0, :cond_8

    .line 1019618
    iget-object v3, v1, LX/5uY;->d:[I

    aput v0, v3, v2

    .line 1019619
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1019620
    :cond_8
    iget-object v4, v1, LX/5uY;->d:[I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    aput v5, v4, v2

    .line 1019621
    iget-object v4, v1, LX/5uY;->g:[F

    iget-object v5, v1, LX/5uY;->e:[F

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result p0

    aput p0, v5, v2

    aput p0, v4, v2

    .line 1019622
    iget-object v4, v1, LX/5uY;->h:[F

    iget-object v5, v1, LX/5uY;->f:[F

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    aput v3, v5, v2

    aput v3, v4, v2

    .line 1019623
    iget v3, v1, LX/5uY;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/5uY;->b:I

    goto :goto_4

    .line 1019624
    :cond_9
    iget v2, v1, LX/5uY;->b:I

    if-lez v2, :cond_0

    .line 1019625
    const/4 v2, 0x1

    move v2, v2

    .line 1019626
    if-eqz v2, :cond_0

    .line 1019627
    invoke-static {v1}, LX/5uY;->j(LX/5uY;)V

    goto/16 :goto_0

    .line 1019628
    :pswitch_3
    const/4 v2, 0x0

    iput v2, v1, LX/5uY;->c:I

    .line 1019629
    invoke-static {v1}, LX/5uY;->k(LX/5uY;)V

    .line 1019630
    invoke-virtual {v1}, LX/5uY;->b()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public b(LX/5uZ;)V
    .locals 13

    .prologue
    .line 1019497
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    const/4 v1, 0x7

    .line 1019498
    iget-object v2, p0, LX/5ua;->c:LX/5uZ;

    .line 1019499
    iget-object v3, p0, LX/5ua;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1019500
    iget-boolean v3, p0, LX/5ua;->f:Z

    if-eqz v3, :cond_0

    .line 1019501
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1019502
    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019503
    iget v7, v6, LX/5uY;->b:I

    move v6, v7

    .line 1019504
    const/4 v7, 0x2

    if-ge v6, v7, :cond_4

    .line 1019505
    const/4 v6, 0x0

    .line 1019506
    :goto_0
    move v3, v6

    .line 1019507
    const v4, 0x42652ee1

    mul-float/2addr v3, v4

    .line 1019508
    invoke-virtual {v2}, LX/5uZ;->f()F

    move-result v4

    invoke-virtual {v2}, LX/5uZ;->g()F

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1019509
    :cond_0
    iget-boolean v3, p0, LX/5ua;->g:Z

    if-eqz v3, :cond_1

    .line 1019510
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1019511
    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019512
    iget v7, v6, LX/5uY;->b:I

    move v6, v7

    .line 1019513
    const/4 v7, 0x2

    if-ge v6, v7, :cond_5

    .line 1019514
    const/high16 v6, 0x3f800000    # 1.0f

    .line 1019515
    :goto_1
    move v3, v6

    .line 1019516
    invoke-virtual {v2}, LX/5uZ;->f()F

    move-result v4

    invoke-virtual {v2}, LX/5uZ;->g()F

    move-result v5

    invoke-virtual {v0, v3, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1019517
    :cond_1
    invoke-virtual {v2}, LX/5uZ;->f()F

    move-result v3

    invoke-virtual {v2}, LX/5uZ;->g()F

    move-result v4

    invoke-static {p0, v0, v3, v4, v1}, LX/5ua;->a(LX/5ua;Landroid/graphics/Matrix;FFI)Z

    move-result v3

    or-int/lit8 v3, v3, 0x0

    .line 1019518
    iget-boolean v4, p0, LX/5ua;->h:Z

    if-eqz v4, :cond_2

    .line 1019519
    invoke-virtual {v2}, LX/5uZ;->h()F

    move-result v4

    .line 1019520
    iget-object v5, v2, LX/5uZ;->a:LX/5uY;

    .line 1019521
    iget-object v6, v5, LX/5uY;->h:[F

    move-object v5, v6

    .line 1019522
    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019523
    iget v7, v6, LX/5uY;->b:I

    move v6, v7

    .line 1019524
    invoke-static {v5, v6}, LX/5uZ;->a([FI)F

    move-result v5

    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019525
    iget-object v7, v6, LX/5uY;->f:[F

    move-object v6, v7

    .line 1019526
    iget-object v7, v2, LX/5uZ;->a:LX/5uY;

    .line 1019527
    iget v2, v7, LX/5uY;->b:I

    move v7, v2

    .line 1019528
    invoke-static {v6, v7}, LX/5uZ;->a([FI)F

    move-result v6

    sub-float/2addr v5, v6

    move v2, v5

    .line 1019529
    invoke-virtual {v0, v4, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1019530
    :cond_2
    invoke-static {p0, v0, v1}, LX/5ua;->b(LX/5ua;Landroid/graphics/Matrix;I)Z

    move-result v2

    or-int/2addr v2, v3

    .line 1019531
    move v0, v2

    .line 1019532
    invoke-static {p0}, LX/5ua;->c(LX/5ua;)V

    .line 1019533
    if-eqz v0, :cond_3

    .line 1019534
    iget-object v1, p0, LX/5ua;->c:LX/5uZ;

    invoke-virtual {v1}, LX/5uZ;->e()V

    .line 1019535
    :cond_3
    iput-boolean v0, p0, LX/5ua;->s:Z

    .line 1019536
    return-void

    .line 1019537
    :cond_4
    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019538
    iget-object v7, v6, LX/5uY;->e:[F

    move-object v6, v7

    .line 1019539
    aget v6, v6, v10

    iget-object v7, v2, LX/5uZ;->a:LX/5uY;

    .line 1019540
    iget-object v8, v7, LX/5uY;->e:[F

    move-object v7, v8

    .line 1019541
    aget v7, v7, v11

    sub-float/2addr v6, v7

    .line 1019542
    iget-object v7, v2, LX/5uZ;->a:LX/5uY;

    .line 1019543
    iget-object v8, v7, LX/5uY;->f:[F

    move-object v7, v8

    .line 1019544
    aget v7, v7, v10

    iget-object v8, v2, LX/5uZ;->a:LX/5uY;

    .line 1019545
    iget-object v9, v8, LX/5uY;->f:[F

    move-object v8, v9

    .line 1019546
    aget v8, v8, v11

    sub-float/2addr v7, v8

    .line 1019547
    iget-object v8, v2, LX/5uZ;->a:LX/5uY;

    .line 1019548
    iget-object v9, v8, LX/5uY;->g:[F

    move-object v8, v9

    .line 1019549
    aget v8, v8, v10

    iget-object v9, v2, LX/5uZ;->a:LX/5uY;

    .line 1019550
    iget-object v12, v9, LX/5uY;->g:[F

    move-object v9, v12

    .line 1019551
    aget v9, v9, v11

    sub-float/2addr v8, v9

    .line 1019552
    iget-object v9, v2, LX/5uZ;->a:LX/5uY;

    .line 1019553
    iget-object v12, v9, LX/5uY;->h:[F

    move-object v9, v12

    .line 1019554
    aget v9, v9, v10

    iget-object v10, v2, LX/5uZ;->a:LX/5uY;

    .line 1019555
    iget-object v12, v10, LX/5uY;->h:[F

    move-object v10, v12

    .line 1019556
    aget v10, v10, v11

    sub-float/2addr v9, v10

    .line 1019557
    float-to-double v10, v7

    float-to-double v6, v6

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v6, v6

    .line 1019558
    float-to-double v10, v9

    float-to-double v8, v8

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v7, v8

    .line 1019559
    sub-float v6, v7, v6

    goto/16 :goto_0

    .line 1019560
    :cond_5
    iget-object v6, v2, LX/5uZ;->a:LX/5uY;

    .line 1019561
    iget-object v7, v6, LX/5uY;->e:[F

    move-object v6, v7

    .line 1019562
    aget v6, v6, v10

    iget-object v7, v2, LX/5uZ;->a:LX/5uY;

    .line 1019563
    iget-object v8, v7, LX/5uY;->e:[F

    move-object v7, v8

    .line 1019564
    aget v7, v7, v11

    sub-float/2addr v6, v7

    .line 1019565
    iget-object v7, v2, LX/5uZ;->a:LX/5uY;

    .line 1019566
    iget-object v8, v7, LX/5uY;->f:[F

    move-object v7, v8

    .line 1019567
    aget v7, v7, v10

    iget-object v8, v2, LX/5uZ;->a:LX/5uY;

    .line 1019568
    iget-object v9, v8, LX/5uY;->f:[F

    move-object v8, v9

    .line 1019569
    aget v8, v8, v11

    sub-float/2addr v7, v8

    .line 1019570
    iget-object v8, v2, LX/5uZ;->a:LX/5uY;

    .line 1019571
    iget-object v9, v8, LX/5uY;->g:[F

    move-object v8, v9

    .line 1019572
    aget v8, v8, v10

    iget-object v9, v2, LX/5uZ;->a:LX/5uY;

    .line 1019573
    iget-object v12, v9, LX/5uY;->g:[F

    move-object v9, v12

    .line 1019574
    aget v9, v9, v11

    sub-float/2addr v8, v9

    .line 1019575
    iget-object v9, v2, LX/5uZ;->a:LX/5uY;

    .line 1019576
    iget-object v12, v9, LX/5uY;->h:[F

    move-object v9, v12

    .line 1019577
    aget v9, v9, v10

    iget-object v10, v2, LX/5uZ;->a:LX/5uY;

    .line 1019578
    iget-object v12, v10, LX/5uY;->h:[F

    move-object v10, v12

    .line 1019579
    aget v10, v10, v11

    sub-float/2addr v9, v10

    .line 1019580
    float-to-double v10, v6

    float-to-double v6, v7

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    double-to-float v6, v6

    .line 1019581
    float-to-double v10, v8

    float-to-double v8, v9

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v8

    double-to-float v7, v8

    .line 1019582
    div-float v6, v7, v6

    goto/16 :goto_1
.end method

.method public final b(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 1019494
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1019495
    invoke-static {p0}, LX/5ua;->c(LX/5ua;)V

    .line 1019496
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1019490
    iput-boolean p1, p0, LX/5ua;->e:Z

    .line 1019491
    if-nez p1, :cond_0

    .line 1019492
    invoke-virtual {p0}, LX/5ua;->a()V

    .line 1019493
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 7

    .prologue
    .line 1019479
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    const v1, 0x3a83126f    # 0.001f

    const/4 v2, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 1019480
    iget-object v3, p0, LX/5ua;->q:[F

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1019481
    iget-object v3, p0, LX/5ua;->q:[F

    aget v4, v3, v2

    sub-float/2addr v4, v6

    aput v4, v3, v2

    .line 1019482
    iget-object v3, p0, LX/5ua;->q:[F

    const/4 v4, 0x4

    aget v5, v3, v4

    sub-float/2addr v5, v6

    aput v5, v3, v4

    .line 1019483
    iget-object v3, p0, LX/5ua;->q:[F

    const/16 v4, 0x8

    aget v5, v3, v4

    sub-float/2addr v5, v6

    aput v5, v3, v4

    move v3, v2

    .line 1019484
    :goto_0
    const/16 v4, 0x9

    if-ge v3, v4, :cond_1

    .line 1019485
    iget-object v4, p0, LX/5ua;->q:[F

    aget v4, v4, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v1

    if-lez v4, :cond_0

    .line 1019486
    :goto_1
    move v0, v2

    .line 1019487
    return v0

    .line 1019488
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1019489
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 1019478
    iget-object v0, p0, LX/5ua;->o:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, LX/5ua;->c(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method
