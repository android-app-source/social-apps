.class public LX/6V5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6V2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6V2",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6Uv;

.field private final b:LX/6V4;


# direct methods
.method public constructor <init>(LX/6Uv;LX/6V4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103403
    iput-object p1, p0, LX/6V5;->a:LX/6Uv;

    .line 1103404
    iput-object p2, p0, LX/6V5;->b:LX/6V4;

    .line 1103405
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1103406
    const-class v0, Landroid/view/View;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1103407
    check-cast p1, Landroid/view/View;

    .line 1103408
    const-string v0, "class_name"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103409
    const-string v0, "id"

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v1, v2}, LX/6V4;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103410
    const-string v0, "visibility"

    .line 1103411
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 1103412
    sparse-switch v1, :sswitch_data_0

    .line 1103413
    const-string v1, "Unknown"

    :goto_0
    move-object v1, v1

    .line 1103414
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103415
    const-string v0, "local_bounds"

    .line 1103416
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1103417
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1103418
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1103419
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 1103420
    sub-int v5, v3, v1

    .line 1103421
    sub-int v6, v4, v2

    .line 1103422
    const-string v7, "[l:%d t:%d, r:%d b:%d] [w:%d, h:%d]"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v9

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1103423
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103424
    const-string v0, "padding"

    .line 1103425
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 1103426
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 1103427
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    .line 1103428
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1103429
    const-string v5, "[l:%d t:%d, r:%d b:%d]"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v5, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1103430
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103431
    const-string v0, "background"

    .line 1103432
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1103433
    if-nez v2, :cond_0

    .line 1103434
    const-string v1, "null"

    .line 1103435
    :goto_1
    move-object v1, v1

    .line 1103436
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103437
    const-string v0, "layout_params"

    iget-object v1, p0, LX/6V5;->a:LX/6Uv;

    .line 1103438
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1103439
    if-nez v2, :cond_2

    .line 1103440
    const/4 v2, 0x0

    .line 1103441
    :goto_2
    move-object v1, v2

    .line 1103442
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1103443
    return-void

    .line 1103444
    :sswitch_0
    const-string v1, "VISIBLE"

    goto/16 :goto_0

    .line 1103445
    :sswitch_1
    const-string v1, "INVISIBLE"

    goto/16 :goto_0

    .line 1103446
    :sswitch_2
    const-string v1, "GONE"

    goto/16 :goto_0

    .line 1103447
    :cond_0
    instance-of v1, v2, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 1103448
    check-cast v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 1103449
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1103450
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1103451
    :cond_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1103452
    const-string v3, "type"

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103453
    const-string v3, "height"

    iget v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v5}, LX/6Uv;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103454
    const-string v3, "width"

    iget v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v5}, LX/6Uv;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103455
    instance-of v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v3, :cond_3

    move-object v3, v2

    .line 1103456
    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1103457
    const-string v5, "margins"

    .line 1103458
    new-instance v6, Landroid/os/Bundle;

    const/4 v7, 0x4

    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(I)V

    .line 1103459
    const-string v7, "left"

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103460
    const-string v7, "top"

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103461
    const-string v7, "right"

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103462
    const-string v7, "bottom"

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103463
    move-object v3, v6

    .line 1103464
    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1103465
    :cond_3
    instance-of v3, v2, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v3, :cond_4

    move-object v3, v2

    .line 1103466
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1103467
    const-string v5, "gravity"

    iget v6, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-static {v6}, LX/6Uv;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103468
    const-string v5, "weight"

    iget v3, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1103469
    :cond_4
    instance-of v3, v2, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v3, :cond_8

    move-object v3, v2

    .line 1103470
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1103471
    const-string v5, "rules"

    .line 1103472
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1103473
    invoke-virtual {v3}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v9

    .line 1103474
    const/4 v6, 0x0

    :goto_3
    array-length v7, v9

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_7

    .line 1103475
    aget v7, v9, v6

    if-eqz v7, :cond_5

    .line 1103476
    sget-object v7, LX/6Uv;->a:[Ljava/lang/String;

    aget-object p0, v7, v6

    .line 1103477
    aget v7, v9, v6

    const/4 p1, -0x1

    if-ne v7, p1, :cond_6

    const-string v7, "true"

    .line 1103478
    :goto_4
    invoke-virtual {v8, p0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103479
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1103480
    :cond_6
    iget-object v7, v1, LX/6Uv;->d:Landroid/content/Context;

    aget p1, v9, v6

    invoke-static {v7, p1}, LX/6V4;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 1103481
    :cond_7
    invoke-virtual {v8}, Landroid/os/Bundle;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    const/4 v6, 0x0

    :goto_5
    move-object v3, v6

    .line 1103482
    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1103483
    :cond_8
    instance-of v3, v2, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v3, :cond_9

    .line 1103484
    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 1103485
    const-string v3, "gravity"

    iget v5, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-static {v5}, LX/6Uv;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103486
    :cond_9
    move-object v2, v4

    .line 1103487
    goto/16 :goto_2

    :cond_a
    move-object v6, v8

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method
