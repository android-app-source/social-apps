.class public final enum LX/610;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/610;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/610;

.field public static final enum BUFFERS:LX/610;

.field public static final enum SURFACE:LX/610;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1039486
    new-instance v0, LX/610;

    const-string v1, "BUFFERS"

    invoke-direct {v0, v1, v2}, LX/610;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/610;->BUFFERS:LX/610;

    .line 1039487
    new-instance v0, LX/610;

    const-string v1, "SURFACE"

    invoke-direct {v0, v1, v3}, LX/610;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/610;->SURFACE:LX/610;

    .line 1039488
    const/4 v0, 0x2

    new-array v0, v0, [LX/610;

    sget-object v1, LX/610;->BUFFERS:LX/610;

    aput-object v1, v0, v2

    sget-object v1, LX/610;->SURFACE:LX/610;

    aput-object v1, v0, v3

    sput-object v0, LX/610;->$VALUES:[LX/610;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1039490
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/610;
    .locals 1

    .prologue
    .line 1039491
    const-class v0, LX/610;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/610;

    return-object v0
.end method

.method public static values()[LX/610;
    .locals 1

    .prologue
    .line 1039489
    sget-object v0, LX/610;->$VALUES:[LX/610;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/610;

    return-object v0
.end method
