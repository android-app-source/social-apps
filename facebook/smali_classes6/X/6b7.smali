.class public LX/6b7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:LX/0cX;


# direct methods
.method public constructor <init>(LX/4gF;Ljava/lang/String;Ljava/lang/String;LX/0Zb;LX/0cX;)V
    .locals 4
    .param p1    # LX/4gF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1112964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112965
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Invalid waterfall ID"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1112966
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "Invalid source tag"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1112967
    sget-object v0, LX/4gF;->UNKNOWN:LX/4gF;

    if-eq p1, v0, :cond_3

    :goto_2
    const-string v0, "Must be known media type"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1112968
    sget-object v0, LX/4gF;->PHOTO:LX/4gF;

    if-ne p1, v0, :cond_4

    .line 1112969
    const-string v0, "photo"

    iput-object v0, p0, LX/6b7;->d:Ljava/lang/String;

    .line 1112970
    :cond_0
    :goto_3
    iput-object p2, p0, LX/6b7;->b:Ljava/lang/String;

    .line 1112971
    iput-object p3, p0, LX/6b7;->c:Ljava/lang/String;

    .line 1112972
    iput-object p4, p0, LX/6b7;->a:LX/0Zb;

    .line 1112973
    iput-object p5, p0, LX/6b7;->e:LX/0cX;

    .line 1112974
    return-void

    :cond_1
    move v0, v2

    .line 1112975
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1112976
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1112977
    goto :goto_2

    .line 1112978
    :cond_4
    sget-object v0, LX/4gF;->VIDEO:LX/4gF;

    if-ne p1, v0, :cond_0

    .line 1112979
    const-string v0, "video"

    iput-object v0, p0, LX/6b7;->d:Ljava/lang/String;

    goto :goto_3
.end method

.method private static a(IIIILjava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1112980
    if-ltz p0, :cond_0

    .line 1112981
    const-string v0, "segment_id"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112982
    const-string v0, "segment_type"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112983
    const-string v0, "segment_start_time"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112984
    const-string v0, "segment_end_time"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112985
    :cond_0
    return-void
.end method

.method public static a(LX/6b7;LX/74R;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1112986
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/74R;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1112987
    const-string v0, "composer"

    .line 1112988
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1112989
    iget-object v0, p0, LX/6b7;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1112990
    iget-object v0, p0, LX/6b7;->b:Ljava/lang/String;

    .line 1112991
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1112992
    :cond_0
    if-eqz p2, :cond_1

    .line 1112993
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1112994
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1112995
    :cond_1
    iget-object v0, p0, LX/6b7;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1112996
    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/Exception;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1112997
    if-nez p1, :cond_1

    .line 1112998
    :cond_0
    :goto_0
    return-void

    .line 1112999
    :cond_1
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0cX;->a(Ljava/lang/Exception;Z)LX/73z;

    move-result-object v0

    .line 1113000
    invoke-virtual {v0}, LX/73z;->c()Ljava/lang/String;

    move-result-object v1

    .line 1113001
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1113002
    const-string v2, "ex_type"

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113003
    :cond_2
    invoke-virtual {v0}, LX/73z;->d()Ljava/lang/String;

    move-result-object v1

    .line 1113004
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1113005
    const-string v2, "ex_msg"

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113006
    :cond_3
    invoke-virtual {v0}, LX/73z;->e()I

    move-result v1

    .line 1113007
    if-eq v1, v3, :cond_4

    .line 1113008
    const-string v2, "ex_code"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113009
    :cond_4
    invoke-virtual {v0}, LX/73z;->f()I

    move-result v1

    .line 1113010
    if-eq v1, v3, :cond_5

    .line 1113011
    const-string v2, "http_status_code"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113012
    :cond_5
    invoke-virtual {v0}, LX/73z;->g()Ljava/lang/String;

    move-result-object v1

    .line 1113013
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1113014
    const-string v2, "error_type"

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113015
    :cond_6
    invoke-virtual {v0}, LX/73z;->b()Ljava/lang/String;

    move-result-object v0

    .line 1113016
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1113017
    const-string v1, "ex_inner_msg"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZZZZJJJJJJJJJJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1113043
    const-string v2, "trans_is_audio_track"

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113044
    const-string v2, "trans_is_init_complete"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113045
    const-string v2, "trans_audio_track_is_set"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113046
    const-string v2, "trans_video_track_is_set"

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113047
    const-string v2, "trans_got_first_audio"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113048
    const-string v2, "trans_got_first_video"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113049
    const-string v2, "trans_audio_video_track_reset"

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113050
    const-string v2, "trans_start_time_us"

    invoke-static {p7, p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113051
    const-string v2, "trans_end_time_us"

    invoke-static {p9, p10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113052
    const-string v2, "trans_adjusted_end_time_us"

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113053
    const-string v2, "trans_sync_start_time_us"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113054
    const-string v2, "trans_first_video_time_us"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113055
    const-string v2, "trans_last_video_time_us"

    invoke-static/range {p17 .. p18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113056
    const-string v2, "trans_first_audio_time_us"

    invoke-static/range {p19 .. p20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113057
    const-string v2, "trans_last_audio_time_us"

    invoke-static/range {p21 .. p22}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113058
    const-string v2, "trans_num_video_samples_muxed"

    invoke-static/range {p23 .. p24}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113059
    const-string v2, "trans_num_audio_samples_muxed"

    invoke-static/range {p25 .. p26}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113060
    invoke-static/range {p27 .. p27}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1113061
    const-string v2, "trans_num_exception"

    move-object/from16 v0, p30

    move-object/from16 v1, p27

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113062
    :cond_0
    invoke-static/range {p28 .. p28}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1113063
    const-string v2, "trans_num_exception_cause"

    move-object/from16 v0, p30

    move-object/from16 v1, p28

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113064
    :cond_1
    invoke-static/range {p29 .. p29}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1113065
    const-string v2, "trans_num_call_stack"

    move-object/from16 v0, p30

    move-object/from16 v1, p29

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113066
    :cond_2
    return-void
.end method

.method public static f(LX/6b7;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113018
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1113019
    const-string v1, "version"

    const-string v2, "m1.0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113020
    const-string v1, "media_type"

    iget-object v2, p0, LX/6b7;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113021
    const-string v1, "client_tag"

    iget-object v2, p0, LX/6b7;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113022
    return-object v0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 2

    .prologue
    .line 1113023
    invoke-static {p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1113024
    invoke-static {p1, p2, p3, p4, v0}, LX/6b7;->a(IIIILjava/util/Map;)V

    .line 1113025
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_CANCEL:LX/74R;

    invoke-static {p0, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1113026
    return-void
.end method

.method public final a(IIIIIIIIJJZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 38

    .prologue
    .line 1113027
    invoke-static/range {p0 .. p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v37

    .line 1113028
    const-string v6, "source_width"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113029
    const-string v6, "source_height"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113030
    const-string v6, "source_bit_rate"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113031
    const-string v6, "source_frame_rate"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113032
    const-string v6, "target_width"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113033
    const-string v6, "target_height"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113034
    const-wide/16 v6, 0x0

    cmp-long v6, p9, v6

    if-lez v6, :cond_0

    .line 1113035
    const-string v6, "original_file_size"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113036
    :cond_0
    const-string v6, "bytes"

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113037
    const-string v6, "target_bit_rate"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113038
    const-string v6, "target_frame_rate"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113039
    move/from16 v0, p43

    move/from16 v1, p44

    move/from16 v2, p45

    move/from16 v3, p46

    move-object/from16 v4, v37

    invoke-static {v0, v1, v2, v3, v4}, LX/6b7;->a(IIIILjava/util/Map;)V

    move/from16 v7, p13

    move/from16 v8, p14

    move/from16 v9, p15

    move/from16 v10, p16

    move/from16 v11, p17

    move/from16 v12, p18

    move/from16 v13, p19

    move-wide/from16 v14, p20

    move-wide/from16 v16, p22

    move-wide/from16 v18, p24

    move-wide/from16 v20, p26

    move-wide/from16 v22, p28

    move-wide/from16 v24, p30

    move-wide/from16 v26, p32

    move-wide/from16 v28, p34

    move-wide/from16 v30, p36

    move-wide/from16 v32, p38

    move-object/from16 v34, p40

    move-object/from16 v35, p41

    move-object/from16 v36, p42

    .line 1113040
    invoke-static/range {v7 .. v37}, LX/6b7;->a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1113041
    sget-object v6, LX/74R;->MEDIA_UPLOAD_PROCESS_SUCCESS:LX/74R;

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-static {v0, v6, v1}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1113042
    return-void
.end method

.method public final a(IZIIZILandroid/graphics/RectF;)V
    .locals 12

    .prologue
    .line 1112962
    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, -0x1

    const/4 v11, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v11}, LX/6b7;->a(IZIIZILandroid/graphics/RectF;IIII)V

    .line 1112963
    return-void
.end method

.method public final a(IZIIZILandroid/graphics/RectF;IIII)V
    .locals 3

    .prologue
    .line 1112904
    invoke-static {p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1112905
    const-string v1, "specified_transcode_bit_rate"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112906
    const-string v1, "is_video_trim"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112907
    if-eqz p2, :cond_0

    .line 1112908
    const-string v1, "video_trim_start_time_ms"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112909
    const-string v1, "video_trim_end_time_ms"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112910
    :cond_0
    const-string v1, "is_video_muted"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112911
    const-string v1, "video_output_rotation_angle"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112912
    if-eqz p7, :cond_1

    .line 1112913
    const-string v1, "video_crop_rectangle"

    const/16 p2, 0x2c

    .line 1112914
    if-nez p7, :cond_2

    .line 1112915
    const/4 v2, 0x0

    .line 1112916
    :goto_0
    move-object v2, v2

    .line 1112917
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112918
    :cond_1
    invoke-static {p8, p9, p10, p11, v0}, LX/6b7;->a(IIIILjava/util/Map;)V

    .line 1112919
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_START:LX/74R;

    invoke-static {p0, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1112920
    return-void

    .line 1112921
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1112922
    iget p1, p7, Landroid/graphics/RectF;->left:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1112923
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1112924
    iget p1, p7, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1112925
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1112926
    iget p1, p7, Landroid/graphics/RectF;->right:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1112927
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1112928
    iget p1, p7, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1112929
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IIIIIIIIJJJZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 47

    .prologue
    .line 1112960
    const/16 v43, -0x1

    const/16 v44, -0x1

    const/16 v45, -0x1

    const/16 v46, -0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move-wide/from16 v9, p12

    move-wide/from16 v11, p14

    move/from16 v13, p16

    move/from16 v14, p17

    move/from16 v15, p18

    move/from16 v16, p19

    move/from16 v17, p20

    move/from16 v18, p21

    move/from16 v19, p22

    move-wide/from16 v20, p23

    move-wide/from16 v22, p25

    move-wide/from16 v24, p27

    move-wide/from16 v26, p29

    move-wide/from16 v28, p31

    move-wide/from16 v30, p33

    move-wide/from16 v32, p35

    move-wide/from16 v34, p37

    move-wide/from16 v36, p39

    move-wide/from16 v38, p41

    move-object/from16 v40, p43

    move-object/from16 v41, p44

    move-object/from16 v42, p45

    invoke-virtual/range {v0 .. v46}, LX/6b7;->a(IIIIIIIIJJZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    .line 1112961
    return-void
.end method

.method public final a(Ljava/lang/String;JJILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1112952
    invoke-static {p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1112953
    const-string v1, "upload_session_id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112954
    const-string v1, "chunk_offset"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112955
    const-string v1, "auto_retry_count"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112956
    const-string v1, "bytes"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112957
    const-string v1, "video_chunk_id"

    invoke-interface {v0, v1, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112958
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS:LX/74R;

    invoke-static {p0, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1112959
    return-void
.end method

.method public final a(Ljava/lang/String;JJLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1112945
    invoke-static {p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1112946
    const-string v1, "upload_session_id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112947
    const-string v1, "chunk_offset"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112948
    const-string v1, "chunk_size"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112949
    const-string v1, "video_chunk_id"

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112950
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_START:LX/74R;

    invoke-static {p0, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1112951
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1112941
    invoke-static {p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v0

    .line 1112942
    const-string v1, "attempt_video_resize"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112943
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED:LX/74R;

    invoke-static {p0, v1, v0}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1112944
    return-void
.end method

.method public final a(ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/Exception;)V
    .locals 39

    .prologue
    .line 1112934
    invoke-static/range {p0 .. p0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v37

    .line 1112935
    const-string v6, "video_resize_codec_init_error"

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p8

    move-wide/from16 v14, p9

    move-wide/from16 v16, p11

    move-wide/from16 v18, p13

    move-wide/from16 v20, p15

    move-wide/from16 v22, p17

    move-wide/from16 v24, p19

    move-wide/from16 v26, p21

    move-wide/from16 v28, p23

    move-wide/from16 v30, p25

    move-wide/from16 v32, p27

    move-object/from16 v34, p29

    move-object/from16 v35, p30

    move-object/from16 v36, p31

    .line 1112936
    invoke-static/range {v7 .. v37}, LX/6b7;->a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1112937
    move-object/from16 v1, v37

    move-object/from16 v2, p36

    invoke-static {v1, v2}, LX/6b7;->a(Ljava/util/Map;Ljava/lang/Exception;)V

    .line 1112938
    move/from16 v0, p32

    move/from16 v1, p33

    move/from16 v2, p34

    move/from16 v3, p35

    move-object/from16 v4, v37

    invoke-static {v0, v1, v2, v3, v4}, LX/6b7;->a(IIIILjava/util/Map;)V

    .line 1112939
    sget-object v6, LX/74R;->MEDIA_UPLOAD_PROCESS_FAILURE:LX/74R;

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-static {v0, v6, v1}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1112940
    return-void
.end method

.method public final a(ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 39

    .prologue
    .line 1112932
    const/16 v33, -0x1

    const/16 v34, -0x1

    const/16 v35, -0x1

    const/16 v36, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p6

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v37, p32

    invoke-virtual/range {v1 .. v37}, LX/6b7;->a(ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/Exception;)V

    .line 1112933
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1112930
    invoke-virtual {p0, v0, v0, v0, v0}, LX/6b7;->a(IIII)V

    .line 1112931
    return-void
.end method
