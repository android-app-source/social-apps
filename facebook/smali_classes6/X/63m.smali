.class public final LX/63m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1hs;


# direct methods
.method public constructor <init>(LX/1hs;)V
    .locals 0

    .prologue
    .line 1043641
    iput-object p1, p0, LX/63m;->a:LX/1hs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1043638
    iget-object v0, p0, LX/63m;->a:LX/1hs;

    const/4 v1, 0x0

    .line 1043639
    iput-object v1, v0, LX/1hs;->b:LX/0P1;

    .line 1043640
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1043623
    check-cast p1, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;

    .line 1043624
    if-eqz p1, :cond_2

    .line 1043625
    iget-object v0, p0, LX/63m;->a:LX/1hs;

    const/4 v4, 0x0

    .line 1043626
    invoke-virtual {p1}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->j()LX/0Px;

    move-result-object v6

    .line 1043627
    new-instance v7, LX/0P2;

    invoke-direct {v7}, LX/0P2;-><init>()V

    .line 1043628
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_1

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;

    .line 1043629
    invoke-virtual {v1}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    new-array p0, v2, [Ljava/util/regex/Pattern;

    move v3, v4

    .line 1043630
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 1043631
    invoke-virtual {v1}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    aput-object v2, p0, v3

    .line 1043632
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1043633
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1043634
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 1043635
    :cond_1
    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v1, v1

    .line 1043636
    iput-object v1, v0, LX/1hs;->b:LX/0P1;

    .line 1043637
    :cond_2
    return-void
.end method
