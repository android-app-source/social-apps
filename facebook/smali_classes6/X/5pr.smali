.class public final LX/5pr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1008589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008590
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/5pr;->a:Ljava/util/Map;

    .line 1008591
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5pr;->b:Z

    .line 1008592
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1008588
    invoke-direct {p0}, LX/5pr;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "LX/5pr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1008584
    iget-boolean v0, p0, LX/5pr;->b:Z

    if-nez v0, :cond_0

    .line 1008585
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Underlying map has already been built"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008586
    :cond_0
    iget-object v0, p0, LX/5pr;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008587
    return-object p0
.end method

.method public final a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1008580
    iget-boolean v0, p0, LX/5pr;->b:Z

    if-nez v0, :cond_0

    .line 1008581
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Underlying map has already been built"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008582
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5pr;->b:Z

    .line 1008583
    iget-object v0, p0, LX/5pr;->a:Ljava/util/Map;

    return-object v0
.end method
