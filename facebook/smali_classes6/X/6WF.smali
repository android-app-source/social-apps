.class public abstract LX/6WF;
.super Lcom/facebook/resources/ui/FbButton;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1104702
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;)V

    .line 1104703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1104666
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1104667
    return-void
.end method


# virtual methods
.method public setAllCaps(Z)V
    .locals 2

    .prologue
    .line 1104704
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104705
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setAllCaps"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104706
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setAllCaps(Z)V

    .line 1104707
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1104708
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104709
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundDrawable"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104710
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1104711
    return-void
.end method

.method public setBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 1104712
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104713
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundTintList"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104714
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 1104715
    return-void
.end method

.method public setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 1104716
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104717
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setBackgroundTintMode"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104718
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 1104719
    return-void
.end method

.method public setCompoundDrawablePadding(I)V
    .locals 2

    .prologue
    .line 1104720
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104721
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablePadding"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104722
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablePadding(I)V

    .line 1104723
    return-void
.end method

.method public setCompoundDrawableTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 1104724
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104725
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawableTintMode"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104726
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawableTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 1104727
    return-void
.end method

.method public final setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1104728
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104729
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawables"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104730
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1104731
    return-void
.end method

.method public final setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1104732
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104733
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCompoundDrawablesRelative"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104734
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1104735
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 2

    .prologue
    .line 1104736
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104737
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setCursorVisible"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104738
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setCursorVisible(Z)V

    .line 1104739
    return-void
.end method

.method public setEms(I)V
    .locals 2

    .prologue
    .line 1104740
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104741
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setEms"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104742
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setEms(I)V

    .line 1104743
    return-void
.end method

.method public setGravity(I)V
    .locals 2

    .prologue
    .line 1104744
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104745
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setGravity"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104746
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setGravity(I)V

    .line 1104747
    return-void
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 1104748
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104749
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104750
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setHeight(I)V

    .line 1104751
    return-void
.end method

.method public setHighlightColor(I)V
    .locals 2

    .prologue
    .line 1104752
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104753
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setHighlightColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104754
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setHighlightColor(I)V

    .line 1104755
    return-void
.end method

.method public setIncludeFontPadding(Z)V
    .locals 2

    .prologue
    .line 1104694
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104695
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setIncludeFontPadding"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104696
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setIncludeFontPadding(Z)V

    .line 1104697
    return-void
.end method

.method public setLines(I)V
    .locals 2

    .prologue
    .line 1104698
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104699
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104700
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setLines(I)V

    .line 1104701
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 2

    .prologue
    .line 1104635
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104636
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104637
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMaxHeight(I)V

    .line 1104638
    return-void
.end method

.method public setMaxLines(I)V
    .locals 2

    .prologue
    .line 1104639
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104640
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104641
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMaxLines(I)V

    .line 1104642
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 2

    .prologue
    .line 1104643
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104644
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMaxWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104645
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMaxWidth(I)V

    .line 1104646
    return-void
.end method

.method public setMinHeight(I)V
    .locals 2

    .prologue
    .line 1104647
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104648
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinHeight"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104649
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMinHeight(I)V

    .line 1104650
    return-void
.end method

.method public setMinLines(I)V
    .locals 2

    .prologue
    .line 1104651
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104652
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinLines"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104653
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMinLines(I)V

    .line 1104654
    return-void
.end method

.method public setMinWidth(I)V
    .locals 2

    .prologue
    .line 1104655
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104656
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setMinWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104657
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setMinWidth(I)V

    .line 1104658
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 1

    .prologue
    .line 1104659
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-nez v0, :cond_0

    .line 1104660
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbButton;->setPadding(IIII)V

    .line 1104661
    :cond_0
    return-void
.end method

.method public setTextAppearance(I)V
    .locals 2

    .prologue
    .line 1104662
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104663
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextAppearance"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104664
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(I)V

    .line 1104665
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 1104631
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104632
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextAppearance"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104633
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 1104634
    return-void
.end method

.method public setTextColor(I)V
    .locals 2

    .prologue
    .line 1104668
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104669
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104670
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 1104671
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 1104672
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104673
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextColor"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104674
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1104675
    return-void
.end method

.method public setTextScaleX(F)V
    .locals 2

    .prologue
    .line 1104676
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104677
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextScaleX"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104678
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTextScaleX(F)V

    .line 1104679
    return-void
.end method

.method public final setTextSize(IF)V
    .locals 2

    .prologue
    .line 1104680
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104681
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setTextSize"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104682
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbButton;->setTextSize(IF)V

    .line 1104683
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1104684
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-nez v0, :cond_0

    .line 1104685
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1104686
    :cond_0
    return-void
.end method

.method public final setTypeface(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 1104687
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-nez v0, :cond_0

    .line 1104688
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbButton;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1104689
    :cond_0
    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1104690
    iget-boolean v0, p0, LX/6WF;->a:Z

    if-eqz v0, :cond_0

    .line 1104691
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setWidth"

    invoke-static {v0, v1}, LX/0zj;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1104692
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setWidth(I)V

    .line 1104693
    return-void
.end method
