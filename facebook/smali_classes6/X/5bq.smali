.class public final LX/5bq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 960017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 960018
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 960019
    :goto_0
    return v1

    .line 960020
    :cond_0
    const-string v11, "is_structured_menu_type_message_hidden"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 960021
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    .line 960022
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 960023
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 960024
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 960025
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 960026
    const-string v11, "cart"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 960027
    invoke-static {p0, p1}, LX/5bo;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 960028
    :cond_2
    const-string v11, "payment_privacy_policy_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 960029
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 960030
    :cond_3
    const-string v11, "resume_url_cta"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 960031
    invoke-static {p0, p1}, LX/5bp;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 960032
    :cond_4
    const-string v11, "structured_menu_badge"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 960033
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 960034
    :cond_5
    const-string v11, "top_level_ctas"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 960035
    invoke-static {p0, p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 960036
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 960037
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 960038
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 960039
    if-eqz v3, :cond_8

    .line 960040
    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 960041
    :cond_8
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 960042
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 960043
    if-eqz v0, :cond_9

    .line 960044
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 960045
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 960046
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 960047
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960048
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 960049
    if-eqz v0, :cond_3

    .line 960050
    const-string v1, "cart"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960051
    const/4 v1, 0x0

    .line 960052
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960053
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 960054
    if-eqz v1, :cond_0

    .line 960055
    const-string v3, "cart_item_count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960056
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 960057
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 960058
    if-eqz v1, :cond_1

    .line 960059
    const-string v3, "cart_native_url"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960060
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960061
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 960062
    if-eqz v1, :cond_2

    .line 960063
    const-string v3, "cart_url"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960064
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960065
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960066
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 960067
    if-eqz v0, :cond_4

    .line 960068
    const-string v1, "is_structured_menu_type_message_hidden"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960069
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 960070
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 960071
    if-eqz v0, :cond_5

    .line 960072
    const-string v1, "payment_privacy_policy_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960073
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960074
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960075
    if-eqz v0, :cond_8

    .line 960076
    const-string v1, "resume_url_cta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960077
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 960078
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 960079
    if-eqz v1, :cond_6

    .line 960080
    const-string v3, "banner_text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960081
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960082
    :cond_6
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 960083
    if-eqz v1, :cond_7

    .line 960084
    const-string v3, "native_uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960085
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 960086
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960087
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 960088
    if-eqz v0, :cond_9

    .line 960089
    const-string v1, "structured_menu_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960090
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 960091
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 960092
    if-eqz v0, :cond_a

    .line 960093
    const-string v1, "top_level_ctas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 960094
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 960095
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 960096
    return-void
.end method
