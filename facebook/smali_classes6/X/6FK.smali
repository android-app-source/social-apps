.class public final LX/6FK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6FF;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public final synthetic b:Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

.field public final synthetic c:LX/6FL;


# direct methods
.method public constructor <init>(LX/6FL;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1067647
    iput-object p1, p0, LX/6FK;->c:LX/6FL;

    iput-object p2, p0, LX/6FK;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    iput-object p3, p0, LX/6FK;->b:Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1067648
    iget-object v0, p0, LX/6FK;->b:Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED:LX/6Cy;

    invoke-virtual {v1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067649
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;)V
    .locals 3

    .prologue
    .line 1067650
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1067651
    new-instance v1, LX/0Ed;

    invoke-direct {v1}, LX/0Ed;-><init>()V

    const-string v2, "tokenized_card"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1067652
    iput-object v2, v1, LX/0Ed;->a:Ljava/lang/String;

    .line 1067653
    move-object v1, v1

    .line 1067654
    const-string v2, "tokenized_cvv"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1067655
    iput-object v2, v1, LX/0Ed;->b:Ljava/lang/String;

    .line 1067656
    move-object v1, v1

    .line 1067657
    const-string v2, "token_expiry_month"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1067658
    iput-object v2, v1, LX/0Ed;->c:Ljava/lang/String;

    .line 1067659
    move-object v1, v1

    .line 1067660
    const-string v2, "token_expiry_year"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1067661
    iput-object v0, v1, LX/0Ed;->d:Ljava/lang/String;

    .line 1067662
    move-object v0, v1

    .line 1067663
    iget-object v1, p0, LX/6FK;->a:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1067664
    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object p1

    .line 1067665
    invoke-static {p1}, LX/47j;->a(LX/0am;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1067666
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->i()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1067667
    iput-object v1, v0, LX/0Ed;->e:Ljava/lang/String;

    .line 1067668
    move-object v0, v0

    .line 1067669
    new-instance v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;

    invoke-direct {v1, v0}, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;-><init>(LX/0Ed;)V

    move-object v0, v1

    .line 1067670
    iget-object v1, p0, LX/6FK;->b:Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    iget-object v2, p0, LX/6FK;->b:Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    invoke-virtual {v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v2

    .line 1067671
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1067672
    const-string p1, "callbackID"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067673
    const-string p1, "cardToken"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1067674
    move-object v0, p0

    .line 1067675
    invoke-virtual {v1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1067676
    return-void

    .line 1067677
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
