.class public final enum LX/5RS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5RS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5RS;

.field public static final enum CENTER:LX/5RS;

.field public static final enum LEFT:LX/5RS;

.field public static final enum RIGHT:LX/5RS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 915523
    new-instance v0, LX/5RS;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/5RS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RS;->LEFT:LX/5RS;

    .line 915524
    new-instance v0, LX/5RS;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/5RS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RS;->CENTER:LX/5RS;

    .line 915525
    new-instance v0, LX/5RS;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LX/5RS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5RS;->RIGHT:LX/5RS;

    .line 915526
    const/4 v0, 0x3

    new-array v0, v0, [LX/5RS;

    sget-object v1, LX/5RS;->LEFT:LX/5RS;

    aput-object v1, v0, v2

    sget-object v1, LX/5RS;->CENTER:LX/5RS;

    aput-object v1, v0, v3

    sget-object v1, LX/5RS;->RIGHT:LX/5RS;

    aput-object v1, v0, v4

    sput-object v0, LX/5RS;->$VALUES:[LX/5RS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 915522
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValue(Ljava/lang/String;)LX/5RS;
    .locals 5

    .prologue
    .line 915515
    invoke-static {}, LX/5RS;->values()[LX/5RS;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 915516
    invoke-virtual {v0}, LX/5RS;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 915517
    :goto_1
    return-object v0

    .line 915518
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 915519
    :cond_1
    sget-object v0, LX/5RS;->LEFT:LX/5RS;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5RS;
    .locals 1

    .prologue
    .line 915521
    const-class v0, LX/5RS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5RS;

    return-object v0
.end method

.method public static values()[LX/5RS;
    .locals 1

    .prologue
    .line 915520
    sget-object v0, LX/5RS;->$VALUES:[LX/5RS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5RS;

    return-object v0
.end method
