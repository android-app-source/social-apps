.class public LX/6AH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058843
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6AH;->a:Ljava/lang/String;

    .line 1058844
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058841
    iget-object v0, p0, LX/6AH;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1058834
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/4Yr;

    .line 1058835
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AH;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1058836
    :cond_0
    :goto_0
    return-void

    .line 1058837
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1058838
    invoke-virtual {p2, v0}, LX/4Yr;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058840
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058839
    const-string v0, "SocialSearchRemoveExplicitPlacelistMutatingVisitor"

    return-object v0
.end method
