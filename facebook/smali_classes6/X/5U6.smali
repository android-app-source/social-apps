.class public final LX/5U6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 923894
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 923895
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923896
    :goto_0
    return v1

    .line 923897
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923898
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 923899
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 923900
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 923901
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 923902
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 923903
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 923904
    :cond_3
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 923905
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 923906
    :cond_4
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 923907
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 923908
    :cond_5
    const-string v8, "profile_pic_large"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 923909
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 923910
    :cond_6
    const-string v8, "profile_pic_medium"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 923911
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 923912
    :cond_7
    const-string v8, "profile_pic_small"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 923913
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 923914
    :cond_8
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 923915
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 923916
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 923917
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 923918
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 923919
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 923920
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 923921
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 923922
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 923923
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 923924
    if-eqz v0, :cond_0

    .line 923925
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923926
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 923927
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 923928
    if-eqz v0, :cond_1

    .line 923929
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923930
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923931
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 923932
    if-eqz v0, :cond_2

    .line 923933
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923934
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923935
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923936
    if-eqz v0, :cond_3

    .line 923937
    const-string v1, "profile_pic_large"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923938
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 923939
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923940
    if-eqz v0, :cond_4

    .line 923941
    const-string v1, "profile_pic_medium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923942
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 923943
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923944
    if-eqz v0, :cond_5

    .line 923945
    const-string v1, "profile_pic_small"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923946
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 923947
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 923948
    return-void
.end method
