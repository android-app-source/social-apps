.class public LX/6WH;
.super LX/6WF;
.source ""


# static fields
.field private static a:Landroid/util/SparseIntArray;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Landroid/content/res/ColorStateList;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1105182
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 1105183
    sput-object v0, LX/6WH;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0e02e5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105184
    sget-object v0, LX/6WH;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0e02e7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105185
    return-void
.end method

.method private a(I[I)V
    .locals 6
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1105167
    invoke-virtual {p0}, LX/6WH;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1105168
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 1105169
    :goto_0
    if-ge v0, v3, :cond_4

    .line 1105170
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 1105171
    const/16 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1105172
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, LX/6WH;->c:I

    .line 1105173
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1105174
    :cond_1
    const/16 v5, 0x0

    if-ne v4, v5, :cond_2

    .line 1105175
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {p0, v4}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1105176
    :cond_2
    const/16 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 1105177
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, LX/6WH;->d:I

    goto :goto_1

    .line 1105178
    :cond_3
    const/16 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 1105179
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    iput-object v4, p0, LX/6WH;->e:Landroid/content/res/ColorStateList;

    goto :goto_1

    .line 1105180
    :cond_4
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105181
    return-void
.end method

.method public static a(LX/6WH;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1105161
    if-eqz p1, :cond_0

    .line 1105162
    iget v0, p0, LX/6WH;->d:I

    iget v1, p0, LX/6WH;->d:I

    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1105163
    iget-object v0, p0, LX/6WH;->e:Landroid/content/res/ColorStateList;

    invoke-static {p1, v0}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 1105164
    invoke-virtual {p0}, LX/6WH;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1105165
    invoke-virtual {p0, p1, v2, v2, v2}, LX/6WH;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1105166
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    .line 1105157
    const/16 v0, 0x22

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1105158
    if-nez v0, :cond_0

    .line 1105159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "%s doesn\'t support the supplied type: 0x%X"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1105160
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getGlyph(LX/6WH;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1105150
    invoke-virtual {p0}, LX/6WH;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1105152
    invoke-super {p0}, LX/6WF;->drawableStateChanged()V

    .line 1105153
    invoke-static {p0}, LX/6WH;->getGlyph(LX/6WH;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1105154
    if-eqz v0, :cond_0

    .line 1105155
    invoke-static {p0}, LX/6WH;->getGlyph(LX/6WH;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, LX/6WH;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1105156
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1105151
    iget v0, p0, LX/6WH;->b:I

    return v0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x45cf997f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1105147
    invoke-super {p0, p1, p2}, LX/6WF;->onMeasure(II)V

    .line 1105148
    iget v1, p0, LX/6WH;->c:I

    iget v2, p0, LX/6WH;->c:I

    invoke-virtual {p0, v1, v2}, LX/6WH;->setMeasuredDimension(II)V

    .line 1105149
    const/16 v1, 0x2d

    const v2, -0x22baaaa6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setGlyph(I)V
    .locals 2

    .prologue
    .line 1105136
    if-lez p1, :cond_0

    invoke-virtual {p0}, LX/6WH;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    const/4 p1, 0x0

    .line 1105137
    const/4 v1, 0x0

    .line 1105138
    iput-boolean v1, p0, LX/6WF;->a:Z

    .line 1105139
    if-eqz v0, :cond_1

    .line 1105140
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1105141
    invoke-static {p0, v1}, LX/6WH;->a(LX/6WH;Landroid/graphics/drawable/Drawable;)V

    .line 1105142
    :goto_1
    const/4 v1, 0x1

    .line 1105143
    iput-boolean v1, p0, LX/6WF;->a:Z

    .line 1105144
    return-void

    .line 1105145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1105146
    :cond_1
    invoke-virtual {p0, p1, p1, p1, p1}, LX/6WH;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 1105134
    const-string v0, ""

    invoke-super {p0, v0, p2}, LX/6WF;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1105135
    return-void
.end method

.method public setType(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1105120
    const/4 v0, 0x0

    .line 1105121
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1105122
    invoke-direct {p0, p1}, LX/6WH;->b(I)V

    .line 1105123
    iput p1, p0, LX/6WH;->b:I

    .line 1105124
    sget-object v0, LX/6WH;->a:Landroid/util/SparseIntArray;

    and-int/lit8 v1, p1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigGlyphButtonAttrs:[I

    invoke-direct {p0, v0, v1}, LX/6WH;->a(I[I)V

    .line 1105125
    sget-object v0, LX/6WH;->a:Landroid/util/SparseIntArray;

    and-int/lit16 v1, p1, 0xff0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigGlyphButtonAttrs:[I

    invoke-direct {p0, v0, v1}, LX/6WH;->a(I[I)V

    .line 1105126
    invoke-static {p0}, LX/6WH;->getGlyph(LX/6WH;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/6WH;->a(LX/6WH;Landroid/graphics/drawable/Drawable;)V

    .line 1105127
    iget v0, p0, LX/6WH;->c:I

    iget v1, p0, LX/6WH;->d:I

    sub-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x1

    .line 1105128
    invoke-virtual {p0, v0, v0, v0, v0}, LX/6WH;->setPadding(IIII)V

    .line 1105129
    const/4 v0, 0x1

    .line 1105130
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1105131
    invoke-virtual {p0}, LX/6WH;->requestLayout()V

    .line 1105132
    invoke-virtual {p0}, LX/6WH;->invalidate()V

    .line 1105133
    return-void
.end method
