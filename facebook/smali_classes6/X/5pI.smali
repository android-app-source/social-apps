.class public LX/5pI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pG;
.implements LX/5pH;


# instance fields
.field public final a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1007992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007993
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    .line 1007994
    return-void
.end method

.method private varargs constructor <init>([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1008002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008003
    array-length v0, p1

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 1008004
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must provide the same number of keys and values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008005
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    .line 1008006
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 1008007
    iget-object v1, p0, LX/5pI;->a:Ljava/util/Map;

    aget-object v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p1, v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008008
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 1008009
    :cond_1
    return-void
.end method

.method public static varargs a([Ljava/lang/Object;)LX/5pI;
    .locals 1

    .prologue
    .line 1008001
    new-instance v0, LX/5pI;

    invoke-direct {v0, p0}, LX/5pI;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/5pI;
    .locals 1

    .prologue
    .line 1008000
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pI;

    return-object v0
.end method

.method private d(Ljava/lang/String;)LX/5pE;
    .locals 1

    .prologue
    .line 1007999
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pE;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/5pG;
    .locals 1

    .prologue
    .line 1007998
    invoke-direct {p0, p1}, LX/5pI;->c(Ljava/lang/String;)LX/5pI;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;
    .locals 1

    .prologue
    .line 1007997
    new-instance v0, LX/5pF;

    invoke-direct {v0, p0}, LX/5pF;-><init>(LX/5pI;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/5pD;)V
    .locals 1

    .prologue
    .line 1007995
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007996
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5pH;)V
    .locals 1

    .prologue
    .line 1007990
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007991
    return-void
.end method

.method public final synthetic b(Ljava/lang/String;)LX/5pC;
    .locals 1

    .prologue
    .line 1007989
    invoke-direct {p0, p1}, LX/5pI;->d(Ljava/lang/String;)LX/5pE;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1007982
    if-ne p0, p1, :cond_1

    .line 1007983
    :cond_0
    :goto_0
    return v0

    .line 1007984
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1007985
    :cond_3
    check-cast p1, LX/5pI;

    .line 1007986
    iget-object v2, p0, LX/5pI;->a:Ljava/util/Map;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/5pI;->a:Ljava/util/Map;

    iget-object v3, p1, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1007987
    goto :goto_0

    .line 1007988
    :cond_4
    iget-object v2, p1, LX/5pI;->a:Ljava/util/Map;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final getBoolean(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1007981
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getDouble(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 1008010
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getInt(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1007950
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007951
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;
    .locals 4

    .prologue
    .line 1007952
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1007953
    if-nez v0, :cond_0

    .line 1007954
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Null:Lcom/facebook/react/bridge/ReadableType;

    .line 1007955
    :goto_0
    return-object v0

    .line 1007956
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 1007957
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007958
    :cond_1
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1007959
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007960
    :cond_2
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1007961
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Boolean:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007962
    :cond_3
    instance-of v1, v0, LX/5pG;

    if-eqz v1, :cond_4

    .line 1007963
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Map:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007964
    :cond_4
    instance-of v1, v0, LX/5pC;

    if-eqz v1, :cond_5

    .line 1007965
    sget-object v0, Lcom/facebook/react/bridge/ReadableType;->Array:Lcom/facebook/react/bridge/ReadableType;

    goto :goto_0

    .line 1007966
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for key "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "contained in JavaOnlyMap"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final hasKey(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1007967
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1007980
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNull(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1007968
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final putBoolean(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1007969
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007970
    return-void
.end method

.method public final putDouble(Ljava/lang/String;D)V
    .locals 2

    .prologue
    .line 1007971
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007972
    return-void
.end method

.method public final putInt(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1007973
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007974
    return-void
.end method

.method public final putNull(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1007975
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007976
    return-void
.end method

.method public final putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1007977
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007978
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007979
    iget-object v0, p0, LX/5pI;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
