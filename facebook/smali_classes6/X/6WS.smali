.class public LX/6WS;
.super LX/5OM;
.source ""


# static fields
.field private static final l:LX/0wT;

.field public static final m:LX/0wT;


# instance fields
.field public n:LX/0wc;

.field public o:LX/0wW;

.field public p:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    .line 1106032
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/6WS;->l:LX/0wT;

    .line 1106033
    const-wide/high16 v0, 0x4054000000000000L    # 80.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/6WS;->m:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1106034
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 1106035
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1106036
    invoke-direct {p0, p1}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1106037
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 1106038
    const v0, 0x7f0e0254

    .line 1106039
    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object p1, p0, LX/0ht;->l:Landroid/content/Context;

    iget-object p2, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-static {p2, v0}, LX/0ht;->a(Landroid/content/Context;I)I

    move-result p2

    invoke-direct {v2, p1, p2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, LX/0ht;->l:Landroid/content/Context;

    .line 1106040
    invoke-static {p0}, LX/0ht;->b$redex0(LX/0ht;)V

    .line 1106041
    invoke-static {v1}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v0

    check-cast v0, LX/0wc;

    iput-object v0, p0, LX/6WS;->n:LX/0wc;

    .line 1106042
    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, LX/6WS;->o:LX/0wW;

    .line 1106043
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/6WS;->p:Landroid/graphics/drawable/Drawable;

    .line 1106044
    return-void
.end method

.method private s()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1106045
    iget-object v0, p0, LX/6WS;->o:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/6WS;->l:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1106046
    iput-boolean v2, v0, LX/0wd;->c:Z

    .line 1106047
    move-object v0, v0

    .line 1106048
    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    .line 1106049
    new-instance v1, LX/6WR;

    invoke-direct {v1, p0}, LX/6WR;-><init>(LX/6WS;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1106050
    new-instance v1, LX/6WQ;

    invoke-direct {v1, p0}, LX/6WQ;-><init>(LX/6WS;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1106051
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 1106052
    iput-object v0, v1, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    .line 1106053
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1106054
    invoke-direct {p0}, LX/6WS;->s()V

    .line 1106055
    invoke-super {p0, p1}, LX/5OM;->a(Landroid/view/View;)V

    .line 1106056
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1106057
    invoke-direct {p0}, LX/6WS;->s()V

    .line 1106058
    invoke-super {p0}, LX/5OM;->d()V

    .line 1106059
    return-void
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 1106060
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const/4 v2, 0x0

    .line 1106061
    iget-object v0, p0, LX/6WS;->o:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/6WS;->m:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1106062
    iput-boolean v2, v0, LX/0wd;->c:Z

    .line 1106063
    move-object v0, v0

    .line 1106064
    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    .line 1106065
    new-instance v1, LX/6WR;

    invoke-direct {v1, p0}, LX/6WR;-><init>(LX/6WS;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1106066
    new-instance v1, LX/6WQ;

    invoke-direct {v1, p0}, LX/6WQ;-><init>(LX/6WS;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1106067
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 1106068
    iput-object v0, v1, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    .line 1106069
    invoke-super {p0}, LX/5OM;->l()V

    .line 1106070
    return-void
.end method
