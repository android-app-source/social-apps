.class public LX/6EV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

.field private final c:LX/0aG;

.field public final d:LX/0lC;

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method private constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;LX/0aG;LX/0lC;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066207
    iput-object p1, p0, LX/6EV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1066208
    iput-object p2, p0, LX/6EV;->b:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    .line 1066209
    iput-object p3, p0, LX/6EV;->c:LX/0aG;

    .line 1066210
    iput-object p4, p0, LX/6EV;->d:LX/0lC;

    .line 1066211
    iput-object p5, p0, LX/6EV;->e:Ljava/util/concurrent/Executor;

    .line 1066212
    return-void
.end method

.method public static b(LX/0QB;)LX/6EV;
    .locals 6

    .prologue
    .line 1066204
    new-instance v0, LX/6EV;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->b(LX/0QB;)Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v5}, LX/6EV;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;LX/0aG;LX/0lC;Ljava/util/concurrent/Executor;)V

    .line 1066205
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MF;
    .locals 6

    .prologue
    .line 1066203
    iget-object v0, p0, LX/6EV;->c:LX/0aG;

    const v5, -0x66b2ed0c

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1066179
    iget-object v0, p0, LX/6EV;->b:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    const-string v1, "me"

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/util/Pair;

    const/4 v3, 0x0

    new-instance v4, Landroid/util/Pair;

    const-string v5, "fields"

    .line 1066180
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1066181
    sget-object v7, LX/6D6;->EMAIL:LX/6D6;

    invoke-virtual {v7}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1066182
    sget-object v7, LX/6D6;->EMAIL:LX/6D6;

    invoke-virtual {v7}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066183
    :cond_0
    sget-object v7, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v7}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1066184
    const-string v7, "name"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066185
    const-string v7, "first_name"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066186
    const-string v7, "last_name"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066187
    const-string v7, "picture"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066188
    :cond_1
    sget-object v7, LX/6D6;->USER_MOBILE_PHONE:LX/6D6;

    invoke-virtual {v7}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1066189
    const-string v7, "verified_mobile_phone"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1066190
    :cond_2
    const-string v7, ","

    invoke-static {v7, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 1066191
    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 1066192
    iget-object v3, v0, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->d:LX/6D1;

    const-string v4, "v2.7"

    invoke-virtual {v3, v4}, LX/6D1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1066193
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "https"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "graph.facebook.com"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "access_token"

    invoke-virtual {v3, v4, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    .line 1066194
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 1066195
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v4, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 1066196
    :cond_3
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1066197
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1066198
    const-string v3, "browser_extension_async_graph_api_get"

    invoke-static {v0, v3, v4}, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->a(Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)LX/15D;

    move-result-object v3

    .line 1066199
    iget-object v4, v0, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v4, v3}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v3

    .line 1066200
    iget-object v4, v3, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v3, v4

    .line 1066201
    move-object v0, v3

    .line 1066202
    new-instance v1, LX/6EU;

    invoke-direct {v1, p0}, LX/6EU;-><init>(LX/6EV;)V

    iget-object v2, p0, LX/6EV;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
