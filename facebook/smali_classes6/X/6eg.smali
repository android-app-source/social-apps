.class public LX/6eg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1118073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;
    .locals 2

    .prologue
    .line 1118074
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    .line 1118075
    invoke-virtual {v0}, LX/5zj;->isQuickCamSource()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1118076
    sget-object v0, LX/5dX;->QUICKCAM:LX/5dX;

    .line 1118077
    :goto_0
    return-object v0

    .line 1118078
    :cond_0
    invoke-virtual {v0}, LX/5zj;->isVideoStickerSource()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1118079
    sget-object v0, LX/5dX;->VIDEO_STICKER:LX/5dX;

    goto :goto_0

    .line 1118080
    :cond_1
    invoke-virtual {v0}, LX/5zj;->isVideoMailSource()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1118081
    sget-object v0, LX/5dX;->VIDEO_MAIL:LX/5dX;

    goto :goto_0

    .line 1118082
    :cond_2
    sget-object v0, LX/5dX;->VIDEO_ATTACHMENT:LX/5dX;

    goto :goto_0
.end method
