.class public final LX/6HJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$ShutterCallback;


# instance fields
.field public final synthetic a:LX/6HU;


# direct methods
.method public constructor <init>(LX/6HU;)V
    .locals 0

    .prologue
    .line 1071686
    iput-object p1, p0, LX/6HJ;->a:LX/6HU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShutter()V
    .locals 7

    .prologue
    .line 1071662
    iget-object v0, p0, LX/6HJ;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1071663
    iget-object v0, p0, LX/6HJ;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->b()V

    .line 1071664
    iget-object v0, p0, LX/6HJ;->a:LX/6HU;

    iget-boolean v0, v0, LX/6HU;->A:Z

    if-eqz v0, :cond_1

    .line 1071665
    iget-object v0, p0, LX/6HJ;->a:LX/6HU;

    const/4 p0, 0x0

    .line 1071666
    iget-object v1, v0, LX/6HU;->g:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 1071667
    iget-boolean v2, v0, LX/6HU;->z:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    .line 1071668
    :goto_0
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v4

    .line 1071669
    iget-boolean v3, v0, LX/6HU;->z:Z

    if-eqz v3, :cond_0

    .line 1071670
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    .line 1071671
    invoke-virtual {v1, v2, v3, p0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1071672
    :cond_0
    iget-object v3, v0, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1071673
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "android.resource"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const v6, 0x7f07000a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const v6, 0x7f07000a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const v6, 0x7f07000a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1071674
    new-instance v5, Landroid/media/MediaPlayer;

    invoke-direct {v5}, Landroid/media/MediaPlayer;-><init>()V

    .line 1071675
    invoke-virtual {v5, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1071676
    :try_start_0
    iget-object v6, v0, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v5, v6, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1071677
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071678
    new-instance v3, LX/6HL;

    invoke-direct {v3, v0, v1, v2, v4}, LX/6HL;-><init>(LX/6HU;Landroid/media/AudioManager;II)V

    invoke-virtual {v5, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1071679
    new-instance v3, LX/6HM;

    invoke-direct {v3, v0, v1, v2, v4}, LX/6HM;-><init>(LX/6HU;Landroid/media/AudioManager;II)V

    invoke-virtual {v5, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1071680
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->start()V

    .line 1071681
    :cond_1
    :goto_1
    return-void

    .line 1071682
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 1071683
    :catch_0
    move-exception v3

    .line 1071684
    invoke-virtual {v1, v2, v4, p0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1071685
    iget-object v1, v0, LX/6HU;->B:LX/6HF;

    const-string v2, "playShutterSound media player error"

    invoke-interface {v1, v2, v3}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method
