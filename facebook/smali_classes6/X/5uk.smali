.class public LX/5uk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/5ul;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/saved/server/SavedEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/5ul;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/5ul;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/saved/server/SavedEventListener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1020065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020066
    iput-object p1, p0, LX/5uk;->a:LX/0Or;

    .line 1020067
    iput-object p2, p0, LX/5uk;->b:LX/5ul;

    .line 1020068
    iput-object p3, p0, LX/5uk;->c:Ljava/util/Set;

    .line 1020069
    return-void
.end method

.method public static a(LX/0QB;)LX/5uk;
    .locals 8

    .prologue
    .line 1020070
    const-class v1, LX/5uk;

    monitor-enter v1

    .line 1020071
    :try_start_0
    sget-object v0, LX/5uk;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1020072
    sput-object v2, LX/5uk;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1020073
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020074
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1020075
    new-instance v4, LX/5uk;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/5ul;->b(LX/0QB;)LX/5ul;

    move-result-object v3

    check-cast v3, LX/5ul;

    .line 1020076
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance p0, LX/5uj;

    invoke-direct {p0, v0}, LX/5uj;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 1020077
    invoke-direct {v4, v5, v3, v6}, LX/5uk;-><init>(LX/0Or;LX/5ul;Ljava/util/Set;)V

    .line 1020078
    move-object v0, v4

    .line 1020079
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1020080
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/5uk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1020081
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1020082
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1020083
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1020084
    const-string v1, "update_saved_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1020085
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1020086
    const-string v1, "update_saved_state_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/server/UpdateSavedStateParams;

    .line 1020087
    iget-object v1, p0, LX/5uk;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 1020088
    iget-object v2, p0, LX/5uk;->b:LX/5ul;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1020089
    if-eqz v0, :cond_2

    .line 1020090
    iget-object v0, p0, LX/5uk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNw;

    .line 1020091
    invoke-virtual {v0}, LX/BNw;->a()V

    goto :goto_0

    .line 1020092
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1020093
    :goto_1
    move-object v0, v0

    .line 1020094
    return-object v0

    .line 1020095
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method
