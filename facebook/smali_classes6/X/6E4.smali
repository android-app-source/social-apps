.class public LX/6E4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6E3;


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;)V
    .locals 1

    .prologue
    .line 1065794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065795
    iget-object v0, p1, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/6E4;->a:Ljava/lang/String;

    .line 1065796
    iget-object v0, p1, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->c:Landroid/net/Uri;

    iput-object v0, p0, LX/6E4;->b:Landroid/net/Uri;

    .line 1065797
    return-void
.end method


# virtual methods
.method public final b()LX/6so;
    .locals 1

    .prologue
    .line 1065791
    sget-object v0, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1065792
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1065793
    const/4 v0, 0x0

    return v0
.end method
