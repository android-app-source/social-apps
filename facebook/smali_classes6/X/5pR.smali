.class public final enum LX/5pR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5pR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5pR;

.field public static final enum CRITICAL:LX/5pR;

.field public static final enum MODERATE:LX/5pR;

.field public static final enum UI_HIDDEN:LX/5pR;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1008139
    new-instance v0, LX/5pR;

    const-string v1, "UI_HIDDEN"

    invoke-direct {v0, v1, v2}, LX/5pR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pR;->UI_HIDDEN:LX/5pR;

    .line 1008140
    new-instance v0, LX/5pR;

    const-string v1, "MODERATE"

    invoke-direct {v0, v1, v3}, LX/5pR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pR;->MODERATE:LX/5pR;

    .line 1008141
    new-instance v0, LX/5pR;

    const-string v1, "CRITICAL"

    invoke-direct {v0, v1, v4}, LX/5pR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pR;->CRITICAL:LX/5pR;

    .line 1008142
    const/4 v0, 0x3

    new-array v0, v0, [LX/5pR;

    sget-object v1, LX/5pR;->UI_HIDDEN:LX/5pR;

    aput-object v1, v0, v2

    sget-object v1, LX/5pR;->MODERATE:LX/5pR;

    aput-object v1, v0, v3

    sget-object v1, LX/5pR;->CRITICAL:LX/5pR;

    aput-object v1, v0, v4

    sput-object v0, LX/5pR;->$VALUES:[LX/5pR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1008143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5pR;
    .locals 1

    .prologue
    .line 1008144
    const-class v0, LX/5pR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5pR;

    return-object v0
.end method

.method public static values()[LX/5pR;
    .locals 1

    .prologue
    .line 1008145
    sget-object v0, LX/5pR;->$VALUES:[LX/5pR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5pR;

    return-object v0
.end method
