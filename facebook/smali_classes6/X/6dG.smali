.class public LX/6dG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "LX/6dE;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1115665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115666
    iput-object p1, p0, LX/6dG;->a:Landroid/database/Cursor;

    .line 1115667
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1115668
    iget-object v0, p0, LX/6dG;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1115669
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/6dE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1115670
    new-instance v0, LX/6dF;

    iget-object v1, p0, LX/6dG;->a:Landroid/database/Cursor;

    invoke-direct {v0, v1}, LX/6dF;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method
