.class public final LX/5K3;
.super LX/1OM;
.source ""

# interfaces
.implements LX/3mh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/5K2;",
        ">;",
        "LX/3mh;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/5Je;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/5Je;)V
    .locals 0

    .prologue
    .line 898156
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 898157
    iput-object p1, p0, LX/5K3;->a:Landroid/content/Context;

    .line 898158
    iput-object p2, p0, LX/5K3;->b:LX/5Je;

    .line 898159
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 4

    .prologue
    .line 898154
    const-wide/16 v2, -0x1

    move-wide v0, v2

    .line 898155
    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 898153
    new-instance v0, LX/5K2;

    new-instance v1, Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/5K3;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/5K2;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 898140
    check-cast p1, LX/5K2;

    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 898141
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 898142
    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 898143
    iget-object v1, p0, LX/5K3;->b:LX/5Je;

    invoke-virtual {v1, p2}, LX/3mY;->e(I)I

    move-result v1

    invoke-static {v1}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 898144
    :goto_0
    iget-object v5, p0, LX/5K3;->b:LX/5Je;

    invoke-virtual {v5, p2}, LX/3mY;->f(I)I

    move-result v5

    invoke-static {v5}, LX/1mh;->a(I)I

    move-result v5

    if-nez v5, :cond_1

    .line 898145
    :goto_1
    if-eqz v4, :cond_2

    .line 898146
    iput v1, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 898147
    iput v2, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 898148
    :goto_2
    iget-object v1, p0, LX/5K3;->b:LX/5Je;

    invoke-virtual {v1, p2}, LX/3mY;->d(I)LX/1dV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 898149
    return-void

    :cond_0
    move v1, v3

    .line 898150
    goto :goto_0

    :cond_1
    move v2, v3

    .line 898151
    goto :goto_1

    .line 898152
    :cond_2
    new-instance v3, LX/1a3;

    invoke-direct {v3, v1, v2}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/facebook/components/ComponentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public final a_(II)V
    .locals 0

    .prologue
    .line 898138
    invoke-virtual {p0, p1, p2}, LX/1OM;->c(II)V

    .line 898139
    return-void
.end method

.method public final bG_()V
    .locals 0

    .prologue
    .line 898136
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 898137
    return-void
.end method

.method public final c_(I)V
    .locals 0

    .prologue
    .line 898129
    invoke-virtual {p0, p1}, LX/1OM;->j_(I)V

    .line 898130
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 898134
    invoke-virtual {p0, p1}, LX/1OM;->i_(I)V

    .line 898135
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 898132
    invoke-virtual {p0, p1}, LX/1OM;->d(I)V

    .line 898133
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 898131
    iget-object v0, p0, LX/5K3;->b:LX/5Je;

    invoke-virtual {v0}, LX/3mY;->e()I

    move-result v0

    return v0
.end method
