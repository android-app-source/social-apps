.class public final LX/54f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/54f;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0vR;

.field public final b:Ljava/lang/Long;

.field public final c:I


# direct methods
.method public constructor <init>(LX/0vR;Ljava/lang/Long;I)V
    .locals 0

    .prologue
    .line 828208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828209
    iput-object p1, p0, LX/54f;->a:LX/0vR;

    .line 828210
    iput-object p2, p0, LX/54f;->b:Ljava/lang/Long;

    .line 828211
    iput p3, p0, LX/54f;->c:I

    .line 828212
    return-void
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 828213
    check-cast p1, LX/54f;

    .line 828214
    iget-object v0, p0, LX/54f;->b:Ljava/lang/Long;

    iget-object v1, p1, LX/54f;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    .line 828215
    if-nez v0, :cond_0

    .line 828216
    iget v0, p0, LX/54f;->c:I

    iget v1, p1, LX/54f;->c:I

    .line 828217
    if-ge v0, v1, :cond_1

    const/4 p0, -0x1

    :goto_0
    move v0, p0

    .line 828218
    :cond_0
    return v0

    :cond_1
    if-ne v0, v1, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    const/4 p0, 0x1

    goto :goto_0
.end method
