.class public final LX/6cN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2UY;


# direct methods
.method public constructor <init>(LX/2UY;)V
    .locals 0

    .prologue
    .line 1114314
    iput-object p1, p0, LX/6cN;->a:LX/2UY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1114315
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 1114316
    :try_start_0
    iget-object v0, p0, LX/6cN;->a:LX/2UY;

    iget-object v0, v0, LX/2UY;->b:LX/2UZ;

    invoke-virtual {v0}, LX/2UZ;->a()V

    .line 1114317
    iget-object v0, p0, LX/6cN;->a:LX/2UY;

    iget-object v1, p0, LX/6cN;->a:LX/2UY;

    iget-object v1, v1, LX/2UY;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 1114318
    iput-wide v2, v0, LX/2UY;->f:J

    .line 1114319
    iget-object v0, p0, LX/6cN;->a:LX/2UY;

    const-wide/16 v2, 0x0

    .line 1114320
    iput-wide v2, v0, LX/2UY;->g:J

    .line 1114321
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V
    :try_end_0
    .catch LX/6cP; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1114322
    :catch_0
    move-exception v0

    .line 1114323
    iget-object v1, p0, LX/6cN;->a:LX/2UY;

    iget-wide v2, v1, LX/2UY;->g:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_0

    .line 1114324
    iget-object v1, p0, LX/6cN;->a:LX/2UY;

    const-wide/32 v2, 0xea60

    .line 1114325
    iput-wide v2, v1, LX/2UY;->g:J

    .line 1114326
    :goto_0
    iget-object v1, p0, LX/6cN;->a:LX/2UY;

    iget-object v2, p0, LX/6cN;->a:LX/2UY;

    iget-object v2, v2, LX/2UY;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, p0, LX/6cN;->a:LX/2UY;

    iget-wide v4, v4, LX/2UY;->g:J

    add-long/2addr v2, v4

    .line 1114327
    iput-wide v2, v1, LX/2UY;->f:J

    .line 1114328
    const-string v1, "ClockSkewCheckBackgroundTask"

    const-string v2, "Failed to check clock skew, scheduling next retry time to %d ms later"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/6cN;->a:LX/2UY;

    iget-wide v6, v5, LX/2UY;->g:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1114329
    throw v0

    .line 1114330
    :cond_0
    iget-object v1, p0, LX/6cN;->a:LX/2UY;

    iget-object v2, p0, LX/6cN;->a:LX/2UY;

    iget-wide v2, v2, LX/2UY;->g:J

    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x1b77400

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 1114331
    iput-wide v2, v1, LX/2UY;->g:J

    .line 1114332
    goto :goto_0
.end method
