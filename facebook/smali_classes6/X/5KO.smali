.class public LX/5KO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/5KN;

.field private b:LX/1De;

.field private c:Lcom/facebook/java2js/JSContext;


# direct methods
.method public constructor <init>(LX/5KN;LX/1De;Lcom/facebook/java2js/JSContext;)V
    .locals 0
    .param p2    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/java2js/JSContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 898595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898596
    iput-object p2, p0, LX/5KO;->b:LX/1De;

    .line 898597
    iput-object p3, p0, LX/5KO;->c:Lcom/facebook/java2js/JSContext;

    .line 898598
    iput-object p1, p0, LX/5KO;->a:LX/5KN;

    .line 898599
    return-void
.end method


# virtual methods
.method public nativeComponentFactory(Ljava/lang/String;Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)LX/1X5;
    .locals 3
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 898600
    iget-object v0, p0, LX/5KO;->a:LX/5KN;

    iget-object v1, p0, LX/5KO;->b:LX/1De;

    new-instance v2, LX/5KI;

    invoke-direct {v2, p5, p3}, LX/5KI;-><init>(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)V

    invoke-interface {v0, v1, p1, p2, v2}, LX/5KN;->a(LX/1De;Ljava/lang/String;Lcom/facebook/java2js/JSValue;LX/5KI;)LX/1X5;

    move-result-object v0

    .line 898601
    if-eqz v0, :cond_0

    .line 898602
    return-object v0

    .line 898603
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized component "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for factory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5KO;->a:LX/5KN;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public refComponentFactory(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
    .locals 0
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 898604
    return-object p1
.end method

.method public statefulComponentFactory(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
    .locals 2
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    .line 898605
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Haven\'t been implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
