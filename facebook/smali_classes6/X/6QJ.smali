.class public final LX/6QJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/6QK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6QL;


# direct methods
.method public constructor <init>(LX/6QL;)V
    .locals 0

    .prologue
    .line 1087272
    iput-object p1, p0, LX/6QJ;->a:LX/6QL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1087273
    check-cast p1, LX/6QK;

    check-cast p2, LX/6QK;

    .line 1087274
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    if-nez v0, :cond_1

    .line 1087275
    :cond_0
    const/4 v0, -0x1

    .line 1087276
    :goto_0
    return v0

    .line 1087277
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p2, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    if-nez v0, :cond_3

    .line 1087278
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1087279
    :cond_3
    sget-object v0, LX/3lo;->a:LX/3lo;

    move-object v0, v0

    .line 1087280
    iget-object v1, p1, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    iget-object v2, p2, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/3lo;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/3lo;

    move-result-object v0

    invoke-virtual {v0}, LX/3lo;->b()I

    move-result v0

    goto :goto_0
.end method
