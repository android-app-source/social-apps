.class public final LX/5uc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/5ue;


# direct methods
.method public constructor <init>(LX/5ue;)V
    .locals 0

    .prologue
    .line 1019773
    iput-object p1, p0, LX/5uc;->a:LX/5ue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 1019774
    iget-object v1, p0, LX/5uc;->a:LX/5ue;

    iget-object v0, p0, LX/5uc;->a:LX/5ue;

    .line 1019775
    iget-object v2, v0, LX/5ub;->f:Landroid/graphics/Matrix;

    move-object v2, v2

    .line 1019776
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1019777
    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x9

    if-ge v3, v4, :cond_0

    .line 1019778
    iget-object v4, v1, LX/5ub;->d:[F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v0

    iget-object p1, v1, LX/5ub;->b:[F

    aget p1, p1, v3

    mul-float/2addr v5, p1

    iget-object p1, v1, LX/5ub;->c:[F

    aget p1, p1, v3

    mul-float/2addr p1, v0

    add-float/2addr v5, p1

    aput v5, v4, v3

    .line 1019779
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1019780
    :cond_0
    iget-object v3, v1, LX/5ub;->d:[F

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->setValues([F)V

    .line 1019781
    iget-object v0, p0, LX/5uc;->a:LX/5ue;

    iget-object v1, p0, LX/5uc;->a:LX/5ue;

    .line 1019782
    iget-object v2, v1, LX/5ub;->f:Landroid/graphics/Matrix;

    move-object v1, v2

    .line 1019783
    invoke-static {v0, v1}, LX/5ue;->a(LX/5ue;Landroid/graphics/Matrix;)V

    .line 1019784
    return-void
.end method
