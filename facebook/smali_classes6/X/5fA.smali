.class public final LX/5fA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/5fl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/5fQ;


# direct methods
.method public constructor <init>(LX/5fQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 971004
    iput-object p1, p0, LX/5fA;->b:LX/5fQ;

    iput-object p2, p0, LX/5fA;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 971005
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fA;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971006
    invoke-virtual {v0}, LX/5fS;->v()V

    .line 971007
    iget-object v1, p0, LX/5fA;->b:LX/5fQ;

    invoke-virtual {v0}, LX/5fS;->j()Z

    move-result v2

    .line 971008
    iput-boolean v2, v1, LX/5fQ;->D:Z

    .line 971009
    iget-object v1, p0, LX/5fA;->b:LX/5fQ;

    invoke-virtual {v0}, LX/5fS;->c()Ljava/lang/String;

    move-result-object v2

    .line 971010
    iput-object v2, v1, LX/5fQ;->C:Ljava/lang/String;

    .line 971011
    invoke-virtual {v0}, LX/5fS;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/5fS;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 971012
    const-string v1, "torch"

    invoke-virtual {v0, v1}, LX/5fS;->a(Ljava/lang/String;)V

    .line 971013
    :cond_0
    iget-object v1, p0, LX/5fA;->b:LX/5fQ;

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/5fQ;->c(LX/5fQ;Z)V

    .line 971014
    iget-object v1, p0, LX/5fA;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 971015
    :try_start_0
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v2, v2, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->unlock()V

    .line 971016
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    new-instance v3, Landroid/media/MediaRecorder;

    invoke-direct {v3}, Landroid/media/MediaRecorder;-><init>()V

    .line 971017
    iput-object v3, v2, LX/5fQ;->z:Landroid/media/MediaRecorder;

    .line 971018
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v2, v2, LX/5fQ;->z:Landroid/media/MediaRecorder;

    iget-object v3, p0, LX/5fA;->b:LX/5fQ;

    iget-object v3, v3, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 971019
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v2, v2, LX/5fQ;->z:Landroid/media/MediaRecorder;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 971020
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v2, v2, LX/5fQ;->z:Landroid/media/MediaRecorder;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 971021
    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v2, v2, LX/5fQ;->i:LX/5fM;

    invoke-static {v2}, LX/5fQ;->b(LX/5fM;)I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v2

    .line 971022
    invoke-virtual {v0}, LX/5fS;->r()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 971023
    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iput v3, v2, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    .line 971024
    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, v2, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    .line 971025
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->j:LX/5fO;

    sget-object v3, LX/5fO;->HIGH:LX/5fO;

    invoke-virtual {v0, v3}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 971026
    const v0, 0x4c4b40

    iput v0, v2, Landroid/media/CamcorderProfile;->videoBitRate:I

    .line 971027
    :cond_1
    :goto_0
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    .line 971028
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    iget-object v3, p0, LX/5fA;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 971029
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v3, p0, LX/5fA;->b:LX/5fQ;

    iget v3, v3, LX/5fQ;->c:I

    invoke-static {v0, v3}, LX/5fQ;->e$redex0(LX/5fQ;I)I

    move-result v0

    .line 971030
    iget-object v3, p0, LX/5fA;->b:LX/5fQ;

    new-instance v4, LX/5fl;

    iget v5, v2, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    iget-object v6, p0, LX/5fA;->a:Ljava/lang/String;

    invoke-direct {v4, v5, v2, v6, v0}, LX/5fl;-><init>(IILjava/lang/String;I)V

    .line 971031
    iput-object v4, v3, LX/5fQ;->B:LX/5fl;

    .line 971032
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    iget-object v2, p0, LX/5fA;->b:LX/5fQ;

    iget-object v3, p0, LX/5fA;->b:LX/5fQ;

    iget v3, v3, LX/5fQ;->c:I

    invoke-static {v2, v3}, LX/5fQ;->e$redex0(LX/5fQ;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 971033
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V

    .line 971034
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->z:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    .line 971035
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 971036
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->B:LX/5fl;

    return-object v0

    .line 971037
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->j:LX/5fO;

    sget-object v3, LX/5fO;->MEDIUM:LX/5fO;

    invoke-virtual {v0, v3}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 971038
    const v0, 0x2dc6c0

    iput v0, v2, Landroid/media/CamcorderProfile;->videoBitRate:I

    goto :goto_0

    .line 971039
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 971040
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/5fA;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->j:LX/5fO;

    sget-object v3, LX/5fO;->LOW:LX/5fO;

    invoke-virtual {v0, v3}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 971041
    const v0, 0xf4240

    iput v0, v2, Landroid/media/CamcorderProfile;->videoBitRate:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
