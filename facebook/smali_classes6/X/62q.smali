.class public final LX/62q;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/refreshableview/RefreshableViewItem;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V
    .locals 0

    .prologue
    .line 1042495
    iput-object p1, p0, LX/62q;->a:Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;B)V
    .locals 0

    .prologue
    .line 1042496
    invoke-direct {p0, p1}, LX/62q;-><init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1042497
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1042498
    const v1, 0x3eb33333    # 0.35f

    mul-float/2addr v1, v0

    add-float/2addr v1, v3

    .line 1042499
    iget-object v2, p0, LX/62q;->a:Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    iget-object v2, v2, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1042500
    iget-object v2, p0, LX/62q;->a:Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    iget-object v2, v2, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1042501
    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 1042502
    iget-object v0, p0, LX/62q;->a:Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    iget-object v0, v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1042503
    :cond_0
    return-void
.end method
