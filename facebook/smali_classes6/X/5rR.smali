.class public LX/5rR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1011767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/util/Map;
    .locals 7

    .prologue
    .line 1011768
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topChange"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onChange"

    const-string v5, "captured"

    const-string v6, "onChangeCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topSelect"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onSelect"

    const-string v5, "captured"

    const-string v6, "onSelectCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/5sF;->START:LX/5sF;

    invoke-virtual {v1}, LX/5sF;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onTouchStart"

    const-string v5, "captured"

    const-string v6, "onTouchStartCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/5sF;->MOVE:LX/5sF;

    invoke-virtual {v1}, LX/5sF;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onTouchMove"

    const-string v5, "captured"

    const-string v6, "onTouchMoveCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    sget-object v1, LX/5sF;->END:LX/5sF;

    invoke-virtual {v1}, LX/5sF;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onTouchEnd"

    const-string v5, "captured"

    const-string v6, "onTouchEndCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/util/Map;
    .locals 4

    .prologue
    .line 1011769
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topContentSizeChange"

    const-string v2, "registrationName"

    const-string v3, "onContentSizeChange"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topLayout"

    const-string v2, "registrationName"

    const-string v3, "onLayout"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topLoadingError"

    const-string v2, "registrationName"

    const-string v3, "onLoadingError"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topLoadingFinish"

    const-string v2, "registrationName"

    const-string v3, "onLoadingFinish"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topLoadingStart"

    const-string v2, "registrationName"

    const-string v3, "onLoadingStart"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topSelectionChange"

    const-string v2, "registrationName"

    const-string v3, "onSelectionChange"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topMessage"

    const-string v2, "registrationName"

    const-string v3, "onMessage"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static c()Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1011770
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 1011771
    const-string v6, "UIView"

    const-string v7, "ContentMode"

    const-string v0, "ScaleAspectFit"

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ScaleAspectFill"

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "ScaleAspectCenter"

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v7, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v10, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011772
    sget-object v0, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v9, v0

    .line 1011773
    sget-object v0, LX/5ql;->b:Landroid/util/DisplayMetrics;

    move-object v11, v0

    .line 1011774
    const-string v12, "Dimensions"

    const-string v13, "windowPhysicalPixels"

    const-string v0, "width"

    iget v1, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "height"

    iget v3, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "scale"

    iget v5, v9, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, "fontScale"

    iget v7, v9, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const-string v8, "densityDpi"

    iget v9, v9, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v14

    const-string v15, "screenPhysicalPixels"

    const-string v0, "width"

    iget v1, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "height"

    iget v3, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "scale"

    iget v5, v11, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, "fontScale"

    iget v7, v11, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const-string v8, "densityDpi"

    iget v9, v11, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v13, v14, v15, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v10, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011775
    const-string v8, "StyleConstants"

    const-string v9, "PointerEventsValues"

    const-string v0, "none"

    sget-object v1, LX/5r3;->NONE:LX/5r3;

    invoke-virtual {v1}, LX/5r3;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "boxNone"

    sget-object v3, LX/5r3;->BOX_NONE:LX/5r3;

    invoke-virtual {v3}, LX/5r3;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "boxOnly"

    sget-object v5, LX/5r3;->BOX_ONLY:LX/5r3;

    invoke-virtual {v5}, LX/5r3;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "unspecified"

    sget-object v7, LX/5r3;->AUTO:LX/5r3;

    invoke-virtual {v7}, LX/5r3;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v9, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v10, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011776
    const-string v0, "PopupMenu"

    const-string v1, "dismissed"

    const-string v2, "dismissed"

    const-string v3, "itemSelected"

    const-string v4, "itemSelected"

    invoke-static {v1, v2, v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011777
    const-string v0, "AccessibilityEventTypes"

    const-string v1, "typeWindowStateChanged"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "typeViewClicked"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011778
    return-object v10
.end method
