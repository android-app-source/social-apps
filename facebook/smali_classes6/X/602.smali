.class public final LX/602;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;


# direct methods
.method public constructor <init>(Lcom/facebook/uicontrib/datepicker/DatePickerRow;)V
    .locals 0

    .prologue
    .line 1036291
    iput-object p1, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, 0x646e1491

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1036292
    iget-object v1, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-boolean v1, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    if-nez v1, :cond_0

    .line 1036293
    iget-object v1, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036294
    iget-object v1, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->e:LX/5zx;

    iget-object v2, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getId()I

    move-result v2

    invoke-interface {v1, v2}, LX/5zx;->a(I)V

    .line 1036295
    :cond_0
    iget-object v1, p0, LX/602;->a:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->performClick()Z

    .line 1036296
    const v1, 0x45ee97e7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
