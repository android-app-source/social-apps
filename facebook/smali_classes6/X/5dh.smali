.class public final LX/5dh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5dg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5dg",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 965992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
    .locals 2

    .prologue
    .line 965993
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;

    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 965994
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 965995
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;

    return-object v0
.end method
