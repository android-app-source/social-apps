.class public final LX/5Zq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 953196
    const/4 v8, 0x0

    .line 953197
    const/4 v7, 0x0

    .line 953198
    const/4 v6, 0x0

    .line 953199
    const/4 v3, 0x0

    .line 953200
    const-wide/16 v4, 0x0

    .line 953201
    const/4 v2, 0x0

    .line 953202
    const/4 v1, 0x0

    .line 953203
    const/4 v0, 0x0

    .line 953204
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 953205
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 953206
    const/4 v0, 0x0

    .line 953207
    :goto_0
    return v0

    .line 953208
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_8

    .line 953209
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 953210
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 953211
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 953212
    const-string v10, "booking_status"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 953213
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 953214
    :cond_1
    const-string v10, "id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 953215
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 953216
    :cond_2
    const-string v10, "page"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 953217
    invoke-static {p0, p1}, LX/5Zn;->a(LX/15w;LX/186;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 953218
    :cond_3
    const-string v10, "product_item"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 953219
    invoke-static {p0, p1}, LX/5Zo;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 953220
    :cond_4
    const-string v10, "start_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 953221
    const/4 v0, 0x1

    .line 953222
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 953223
    :cond_5
    const-string v10, "status"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 953224
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 953225
    :cond_6
    const-string v10, "user"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 953226
    invoke-static {p0, p1}, LX/5Zp;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 953227
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 953228
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 953229
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 953230
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 953231
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 953232
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 953233
    if-eqz v0, :cond_9

    .line 953234
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 953235
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 953236
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 953237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v8

    move v8, v7

    move v7, v2

    move v12, v3

    move-wide v2, v4

    move v5, v6

    move v4, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 953238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 953239
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 953240
    if-eqz v0, :cond_0

    .line 953241
    const-string v0, "booking_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953242
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953243
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 953244
    if-eqz v0, :cond_1

    .line 953245
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953246
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953247
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953248
    if-eqz v0, :cond_2

    .line 953249
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953250
    invoke-static {p0, v0, p2}, LX/5Zn;->a(LX/15i;ILX/0nX;)V

    .line 953251
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953252
    if-eqz v0, :cond_3

    .line 953253
    const-string v1, "product_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953254
    invoke-static {p0, v0, p2, p3}, LX/5Zo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 953255
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 953256
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 953257
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953258
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 953259
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 953260
    if-eqz v0, :cond_5

    .line 953261
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953262
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 953263
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 953264
    if-eqz v0, :cond_6

    .line 953265
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 953266
    invoke-static {p0, v0, p2}, LX/5Zp;->a(LX/15i;ILX/0nX;)V

    .line 953267
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 953268
    return-void
.end method
