.class public abstract LX/5r0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/5r0;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:I


# instance fields
.field public b:Z

.field public c:I

.field public d:J

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1010801
    const/4 v0, 0x0

    sput v0, LX/5r0;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1010798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010799
    sget v0, LX/5r0;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LX/5r0;->a:I

    iput v0, p0, LX/5r0;->e:I

    .line 1010800
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1010794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010795
    sget v0, LX/5r0;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LX/5r0;->a:I

    iput v0, p0, LX/5r0;->e:I

    .line 1010796
    invoke-virtual {p0, p1}, LX/5r0;->a(I)V

    .line 1010797
    return-void
.end method


# virtual methods
.method public a(LX/5r0;)LX/5r0;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1010791
    iget-wide v4, p0, LX/5r0;->d:J

    move-wide v0, v4

    .line 1010792
    iget-wide v4, p1, LX/5r0;->d:J

    move-wide v2, v4

    .line 1010793
    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 1010790
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1010781
    iput p1, p0, LX/5r0;->c:I

    .line 1010782
    invoke-static {}, LX/5pv;->c()J

    move-result-wide v0

    iput-wide v0, p0, LX/5r0;->d:J

    .line 1010783
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5r0;->b:Z

    .line 1010784
    return-void
.end method

.method public abstract a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1010789
    const/4 v0, 0x1

    return v0
.end method

.method public f()S
    .locals 1

    .prologue
    .line 1010788
    const/4 v0, 0x0

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1010785
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5r0;->b:Z

    .line 1010786
    invoke-virtual {p0}, LX/5r0;->a()V

    .line 1010787
    return-void
.end method
