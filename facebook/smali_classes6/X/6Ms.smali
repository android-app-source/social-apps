.class public LX/6Ms;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1082132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/contacts/graphql/Contact;)LX/0Py;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/graphql/Contact;",
            ")",
            "LX/0Py",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1082122
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1082123
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    .line 1082124
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v1

    .line 1082125
    if-eqz v1, :cond_1

    .line 1082126
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v2, v3, v1}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1082127
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    .line 1082128
    if-eqz v1, :cond_2

    .line 1082129
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    invoke-direct {v2, v3, v1}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1082130
    :cond_2
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    .line 1082131
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
