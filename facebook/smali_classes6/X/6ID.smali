.class public final enum LX/6ID;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ID;

.field public static final enum NO_CONSTRAINTS:LX/6ID;

.field public static final enum SMALLER_THAN_OR_EQUAL_TO:LX/6ID;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1073521
    new-instance v0, LX/6ID;

    const-string v1, "NO_CONSTRAINTS"

    invoke-direct {v0, v1, v2}, LX/6ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ID;->NO_CONSTRAINTS:LX/6ID;

    .line 1073522
    new-instance v0, LX/6ID;

    const-string v1, "SMALLER_THAN_OR_EQUAL_TO"

    invoke-direct {v0, v1, v3}, LX/6ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ID;->SMALLER_THAN_OR_EQUAL_TO:LX/6ID;

    .line 1073523
    const/4 v0, 0x2

    new-array v0, v0, [LX/6ID;

    sget-object v1, LX/6ID;->NO_CONSTRAINTS:LX/6ID;

    aput-object v1, v0, v2

    sget-object v1, LX/6ID;->SMALLER_THAN_OR_EQUAL_TO:LX/6ID;

    aput-object v1, v0, v3

    sput-object v0, LX/6ID;->$VALUES:[LX/6ID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1073524
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ID;
    .locals 1

    .prologue
    .line 1073525
    const-class v0, LX/6ID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ID;

    return-object v0
.end method

.method public static values()[LX/6ID;
    .locals 1

    .prologue
    .line 1073526
    sget-object v0, LX/6ID;->$VALUES:[LX/6ID;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ID;

    return-object v0
.end method
