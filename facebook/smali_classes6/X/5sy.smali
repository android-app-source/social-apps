.class public final LX/5sy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1014634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 1014606
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1014607
    iget-object v1, p0, LX/5sy;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1014608
    iget-object v3, p0, LX/5sy;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1014609
    iget-object v5, p0, LX/5sy;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1014610
    iget-object v6, p0, LX/5sy;->g:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1014611
    iget-object v7, p0, LX/5sy;->h:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1014612
    iget-object v8, p0, LX/5sy;->i:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1014613
    iget-object v9, p0, LX/5sy;->j:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1014614
    iget-object v10, p0, LX/5sy;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1014615
    const/16 v11, 0xb

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1014616
    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 1014617
    iget-boolean v1, p0, LX/5sy;->b:Z

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1014618
    const/4 v1, 0x2

    iget-boolean v11, p0, LX/5sy;->c:Z

    invoke-virtual {v0, v1, v11}, LX/186;->a(IZ)V

    .line 1014619
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1014620
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1014621
    const/4 v1, 0x5

    iget-boolean v3, p0, LX/5sy;->f:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1014622
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1014623
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1014624
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1014625
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1014626
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1014627
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1014628
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1014629
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1014630
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1014631
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1014632
    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;-><init>(LX/15i;)V

    .line 1014633
    return-object v1
.end method
