.class public LX/54V;
.super LX/52s;
.source ""


# static fields
.field public static final b:LX/53o;


# instance fields
.field public final a:LX/54T;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828039
    new-instance v0, LX/53o;

    const-string v1, "RxComputationThreadPool-"

    invoke-direct {v0, v1}, LX/53o;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/54V;->b:LX/53o;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828040
    invoke-direct {p0}, LX/52s;-><init>()V

    .line 828041
    new-instance v0, LX/54T;

    invoke-direct {v0}, LX/54T;-><init>()V

    iput-object v0, p0, LX/54V;->a:LX/54T;

    .line 828042
    return-void
.end method


# virtual methods
.method public final a()LX/52r;
    .locals 8

    .prologue
    .line 828043
    new-instance v0, LX/54S;

    iget-object v1, p0, LX/54V;->a:LX/54T;

    .line 828044
    iget-object v2, v1, LX/54T;->b:[LX/54U;

    iget-wide v4, v1, LX/54T;->c:J

    const-wide/16 v6, 0x1

    add-long/2addr v6, v4

    iput-wide v6, v1, LX/54T;->c:J

    iget v3, v1, LX/54T;->a:I

    int-to-long v6, v3

    rem-long/2addr v4, v6

    long-to-int v3, v4

    aget-object v2, v2, v3

    move-object v1, v2

    .line 828045
    invoke-direct {v0, v1}, LX/54S;-><init>(LX/54U;)V

    return-object v0
.end method
