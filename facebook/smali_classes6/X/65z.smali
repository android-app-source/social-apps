.class public LX/65z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/677;

.field public b:I

.field public final c:LX/671;


# direct methods
.method public constructor <init>(LX/671;)V
    .locals 3

    .prologue
    .line 1049122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049123
    new-instance v0, LX/65x;

    invoke-direct {v0, p0, p1}, LX/65x;-><init>(LX/65z;LX/65D;)V

    .line 1049124
    new-instance v1, LX/65y;

    invoke-direct {v1, p0}, LX/65y;-><init>(LX/65z;)V

    .line 1049125
    new-instance v2, LX/677;

    invoke-direct {v2, v0, v1}, LX/677;-><init>(LX/65D;Ljava/util/zip/Inflater;)V

    iput-object v2, p0, LX/65z;->a:LX/677;

    .line 1049126
    iget-object v0, p0, LX/65z;->a:LX/677;

    invoke-static {v0}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    iput-object v0, p0, LX/65z;->c:LX/671;

    .line 1049127
    return-void
.end method

.method private b()LX/673;
    .locals 4

    .prologue
    .line 1049128
    iget-object v0, p0, LX/65z;->c:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    .line 1049129
    iget-object v1, p0, LX/65z;->c:LX/671;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, LX/671;->c(J)LX/673;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1049130
    iget v0, p0, LX/65z;->b:I

    if-lez v0, :cond_0

    .line 1049131
    iget-object v0, p0, LX/65z;->a:LX/677;

    invoke-virtual {v0}, LX/677;->b()Z

    .line 1049132
    iget v0, p0, LX/65z;->b:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "compressedLimit > 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/65z;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049133
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1049134
    iget v0, p0, LX/65z;->b:I

    add-int/2addr v0, p1

    iput v0, p0, LX/65z;->b:I

    .line 1049135
    iget-object v0, p0, LX/65z;->c:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v1

    .line 1049136
    if-gez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "numberOfPairs < 0: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049137
    :cond_0
    const/16 v0, 0x400

    if-le v1, v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "numberOfPairs > 1024: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049138
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1049139
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 1049140
    invoke-direct {p0}, LX/65z;->b()LX/673;

    move-result-object v3

    invoke-virtual {v3}, LX/673;->d()LX/673;

    move-result-object v3

    .line 1049141
    invoke-direct {p0}, LX/65z;->b()LX/673;

    move-result-object v4

    .line 1049142
    invoke-virtual {v3}, LX/673;->e()I

    move-result v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "name.size == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049143
    :cond_2
    new-instance v5, LX/65j;

    invoke-direct {v5, v3, v4}, LX/65j;-><init>(LX/673;LX/673;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1049145
    :cond_3
    invoke-direct {p0}, LX/65z;->c()V

    .line 1049146
    return-object v2
.end method
