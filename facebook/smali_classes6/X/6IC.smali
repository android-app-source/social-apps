.class public final enum LX/6IC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6IC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6IC;

.field public static final enum BOTTOM_LEFT:LX/6IC;

.field public static final enum BOTTOM_RIGHT:LX/6IC;

.field public static final enum TOP_LEFT:LX/6IC;

.field public static final enum TOP_RIGHT:LX/6IC;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1073519
    new-instance v0, LX/6IC;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v2}, LX/6IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    new-instance v0, LX/6IC;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, LX/6IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    new-instance v0, LX/6IC;

    const-string v1, "BOTTOM_LEFT"

    invoke-direct {v0, v1, v4}, LX/6IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    new-instance v0, LX/6IC;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v5}, LX/6IC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073520
    const/4 v0, 0x4

    new-array v0, v0, [LX/6IC;

    sget-object v1, LX/6IC;->TOP_LEFT:LX/6IC;

    aput-object v1, v0, v2

    sget-object v1, LX/6IC;->TOP_RIGHT:LX/6IC;

    aput-object v1, v0, v3

    sget-object v1, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    aput-object v1, v0, v4

    sget-object v1, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    aput-object v1, v0, v5

    sput-object v0, LX/6IC;->$VALUES:[LX/6IC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1073518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6IC;
    .locals 1

    .prologue
    .line 1073517
    const-class v0, LX/6IC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6IC;

    return-object v0
.end method

.method public static values()[LX/6IC;
    .locals 1

    .prologue
    .line 1073516
    sget-object v0, LX/6IC;->$VALUES:[LX/6IC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6IC;

    return-object v0
.end method
