.class public LX/6D7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1064622
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "browserextensions/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1064623
    sput-object v0, LX/6D7;->a:LX/0Tn;

    const-string v1, "location/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6D7;->b:LX/0Tn;

    .line 1064624
    sget-object v0, LX/6D7;->a:LX/0Tn;

    const-string v1, "accesstoken/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6D7;->c:LX/0Tn;

    .line 1064625
    sget-object v0, LX/6D7;->a:LX/0Tn;

    const-string v1, "expires/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6D7;->d:LX/0Tn;

    .line 1064626
    sget-object v0, LX/6D7;->a:LX/0Tn;

    const-string v1, "autofill/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6D7;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1064628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064629
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064627
    sget-object v0, LX/6D7;->a:LX/0Tn;

    sget-object v1, LX/6D7;->b:LX/0Tn;

    sget-object v2, LX/6D7;->c:LX/0Tn;

    sget-object v3, LX/6D7;->d:LX/0Tn;

    sget-object v4, LX/6D7;->e:LX/0Tn;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
