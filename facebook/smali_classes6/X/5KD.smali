.class public final LX/5KD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5KE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/5Je;

.field public b:LX/1Of;

.field public c:LX/25S;

.field public d:LX/5K7;

.field public e:LX/3x6;

.field public f:LX/1OX;

.field public g:LX/1Of;

.field public h:Z

.field public i:Z

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 898355
    invoke-static {}, LX/5KE;->q()LX/5KE;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 898356
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5KD;->h:Z

    .line 898357
    const/4 v0, 0x0

    iput v0, p0, LX/5KD;->j:I

    .line 898358
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 898359
    const-string v0, "SnapRecycler"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/5KE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 898360
    check-cast p1, LX/5KD;

    .line 898361
    iget-object v0, p1, LX/5KD;->g:LX/1Of;

    iput-object v0, p0, LX/5KD;->g:LX/1Of;

    .line 898362
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 898320
    if-ne p0, p1, :cond_1

    .line 898321
    :cond_0
    :goto_0
    return v0

    .line 898322
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 898323
    goto :goto_0

    .line 898324
    :cond_3
    check-cast p1, LX/5KD;

    .line 898325
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 898326
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 898327
    if-eq v2, v3, :cond_0

    .line 898328
    iget-object v2, p0, LX/5KD;->a:LX/5Je;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5KD;->a:LX/5Je;

    iget-object v3, p1, LX/5KD;->a:LX/5Je;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 898329
    goto :goto_0

    .line 898330
    :cond_5
    iget-object v2, p1, LX/5KD;->a:LX/5Je;

    if-nez v2, :cond_4

    .line 898331
    :cond_6
    iget-object v2, p0, LX/5KD;->b:LX/1Of;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/5KD;->b:LX/1Of;

    iget-object v3, p1, LX/5KD;->b:LX/1Of;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 898332
    goto :goto_0

    .line 898333
    :cond_8
    iget-object v2, p1, LX/5KD;->b:LX/1Of;

    if-nez v2, :cond_7

    .line 898334
    :cond_9
    iget-object v2, p0, LX/5KD;->c:LX/25S;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/5KD;->c:LX/25S;

    iget-object v3, p1, LX/5KD;->c:LX/25S;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 898335
    goto :goto_0

    .line 898336
    :cond_b
    iget-object v2, p1, LX/5KD;->c:LX/25S;

    if-nez v2, :cond_a

    .line 898337
    :cond_c
    iget-object v2, p0, LX/5KD;->d:LX/5K7;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/5KD;->d:LX/5K7;

    iget-object v3, p1, LX/5KD;->d:LX/5K7;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 898338
    goto :goto_0

    .line 898339
    :cond_e
    iget-object v2, p1, LX/5KD;->d:LX/5K7;

    if-nez v2, :cond_d

    .line 898340
    :cond_f
    iget-object v2, p0, LX/5KD;->e:LX/3x6;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/5KD;->e:LX/3x6;

    iget-object v3, p1, LX/5KD;->e:LX/3x6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 898341
    goto :goto_0

    .line 898342
    :cond_11
    iget-object v2, p1, LX/5KD;->e:LX/3x6;

    if-nez v2, :cond_10

    .line 898343
    :cond_12
    iget-object v2, p0, LX/5KD;->f:LX/1OX;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/5KD;->f:LX/1OX;

    iget-object v3, p1, LX/5KD;->f:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 898344
    goto/16 :goto_0

    .line 898345
    :cond_14
    iget-object v2, p1, LX/5KD;->f:LX/1OX;

    if-nez v2, :cond_13

    .line 898346
    :cond_15
    iget-boolean v2, p0, LX/5KD;->h:Z

    iget-boolean v3, p1, LX/5KD;->h:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 898347
    goto/16 :goto_0

    .line 898348
    :cond_16
    iget-boolean v2, p0, LX/5KD;->i:Z

    iget-boolean v3, p1, LX/5KD;->i:Z

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 898349
    goto/16 :goto_0

    .line 898350
    :cond_17
    iget v2, p0, LX/5KD;->j:I

    iget v3, p1, LX/5KD;->j:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 898351
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 898352
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/5KD;

    .line 898353
    const/4 v1, 0x0

    iput-object v1, v0, LX/5KD;->g:LX/1Of;

    .line 898354
    return-object v0
.end method
