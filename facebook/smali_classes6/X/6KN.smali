.class public LX/6KN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/0Zb;

.field private final c:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final d:LX/6Kb;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public g:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Zb;Lcom/facebook/common/perftest/PerfTestConfig;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1076628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076629
    iput-object p1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1076630
    iput-object p2, p0, LX/6KN;->b:LX/0Zb;

    .line 1076631
    iput-object p3, p0, LX/6KN;->c:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 1076632
    new-instance v0, LX/6Kb;

    invoke-direct {v0}, LX/6Kb;-><init>()V

    iput-object v0, p0, LX/6KN;->d:LX/6Kb;

    .line 1076633
    iput-object p4, p0, LX/6KN;->e:Ljava/lang/String;

    .line 1076634
    iput-object p5, p0, LX/6KN;->f:Ljava/lang/String;

    .line 1076635
    const/16 p3, 0xa

    .line 1076636
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, p3}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    .line 1076637
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0x9

    const p2, 0xac0001

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076638
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const p1, 0xac0010

    invoke-virtual {v0, p3, p1}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076639
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x1

    const p2, 0xac0004

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076640
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x2

    const p2, 0xac0005

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076641
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x7

    const p2, 0xac000c

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076642
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0x8

    const p2, 0xac000f

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076643
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x3

    const p2, 0xac0007

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076644
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x4

    const p2, 0xac0008

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076645
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x5

    const p2, 0xac0009

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076646
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/4 p1, 0x6

    const p2, 0xac000a

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076647
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0xb

    const p2, 0xac0006

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076648
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0xc

    const p2, 0xac0012

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076649
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0xd

    const p2, 0xac0013

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076650
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0xe

    const p2, 0xac0003

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076651
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0xf

    const p2, 0xac000b

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076652
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    const/16 p1, 0x10

    const p2, 0xac0014

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1076653
    return-void
.end method

.method private static a(Ljava/util/List;)LX/0lF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 1076624
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 1076625
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JR;

    .line 1076626
    invoke-virtual {v0}, LX/6JR;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 1076627
    :cond_0
    return-object v1
.end method

.method private c(I)I
    .locals 4

    .prologue
    .line 1076620
    iget-object v0, p0, LX/6KN;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 1076621
    if-nez v0, :cond_0

    .line 1076622
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not part of FbCameraLogger.FbCameraEvent"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1076623
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 1076618
    iget-object v0, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/6KN;->c(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1076619
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1076654
    invoke-direct {p0, p1}, LX/6KN;->c(I)I

    move-result v0

    .line 1076655
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1076656
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "product_name:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076657
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "product_session_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6KN;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076658
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "media_pipeline_session_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076659
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "camera_lib_type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/6KU;->CAMERA_CORE:LX/6KU;

    invoke-virtual {v3}, LX/6KU;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076660
    return-void
.end method

.method public final a(ILjava/lang/String;LX/6J9;)V
    .locals 4

    .prologue
    .line 1076610
    invoke-direct {p0, p1}, LX/6KN;->c(I)I

    move-result v0

    .line 1076611
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1076612
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "product_name:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076613
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "product_session_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6KN;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076614
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "camera_session_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076615
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "camera_api_level:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, LX/6J9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076616
    iget-object v1, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "camera_lib_type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/6KU;->CAMERA_CORE:LX/6KU;

    invoke-virtual {v3}, LX/6KU;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1076617
    return-void
.end method

.method public final a(LX/6JF;LX/6IP;)V
    .locals 3

    .prologue
    .line 1076601
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "supported_camera_sizes_report"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1076602
    const-string v1, "product_name"

    iget-object v2, p0, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076603
    const-string v1, "product_session_id"

    iget-object v2, p0, LX/6KN;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076604
    const-string v1, "camera_facing"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076605
    const-string v1, "supported_preview_sizes"

    invoke-interface {p2}, LX/6IP;->c()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/6KN;->a(Ljava/util/List;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076606
    const-string v1, "supported_photo_sizes"

    invoke-interface {p2}, LX/6IP;->d()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/6KN;->a(Ljava/util/List;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076607
    const-string v1, "supported_video_sizes"

    invoke-interface {p2}, LX/6IP;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/6KN;->a(Ljava/util/List;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1076608
    iget-object v1, p0, LX/6KN;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1076609
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1076599
    iget-object v0, p0, LX/6KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, p1}, LX/6KN;->c(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1076600
    return-void
.end method
