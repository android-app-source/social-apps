.class public final LX/5JU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5JV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Landroid/widget/ImageView$ScaleType;

.field public final synthetic d:LX/5JV;


# direct methods
.method public constructor <init>(LX/5JV;)V
    .locals 1

    .prologue
    .line 897025
    iput-object p1, p0, LX/5JU;->d:LX/5JV;

    .line 897026
    move-object v0, p1

    .line 897027
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 897028
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897024
    const-string v0, "GlyphColorizerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897009
    if-ne p0, p1, :cond_1

    .line 897010
    :cond_0
    :goto_0
    return v0

    .line 897011
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 897012
    goto :goto_0

    .line 897013
    :cond_3
    check-cast p1, LX/5JU;

    .line 897014
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 897015
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 897016
    if-eq v2, v3, :cond_0

    .line 897017
    iget v2, p0, LX/5JU;->a:I

    iget v3, p1, LX/5JU;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 897018
    goto :goto_0

    .line 897019
    :cond_4
    iget v2, p0, LX/5JU;->b:I

    iget v3, p1, LX/5JU;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 897020
    goto :goto_0

    .line 897021
    :cond_5
    iget-object v2, p0, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    iget-object v3, p1, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView$ScaleType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 897022
    goto :goto_0

    .line 897023
    :cond_6
    iget-object v2, p1, LX/5JU;->c:Landroid/widget/ImageView$ScaleType;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
