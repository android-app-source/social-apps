.class public final enum LX/6J9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6J9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6J9;

.field public static final enum CAMERA1:LX/6J9;

.field public static final enum CAMERA2:LX/6J9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1075364
    new-instance v0, LX/6J9;

    const-string v1, "CAMERA1"

    invoke-direct {v0, v1, v2}, LX/6J9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J9;->CAMERA1:LX/6J9;

    .line 1075365
    new-instance v0, LX/6J9;

    const-string v1, "CAMERA2"

    invoke-direct {v0, v1, v3}, LX/6J9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J9;->CAMERA2:LX/6J9;

    .line 1075366
    const/4 v0, 0x2

    new-array v0, v0, [LX/6J9;

    sget-object v1, LX/6J9;->CAMERA1:LX/6J9;

    aput-object v1, v0, v2

    sget-object v1, LX/6J9;->CAMERA2:LX/6J9;

    aput-object v1, v0, v3

    sput-object v0, LX/6J9;->$VALUES:[LX/6J9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1075367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6J9;
    .locals 1

    .prologue
    .line 1075368
    const-class v0, LX/6J9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6J9;

    return-object v0
.end method

.method public static values()[LX/6J9;
    .locals 1

    .prologue
    .line 1075369
    sget-object v0, LX/6J9;->$VALUES:[LX/6J9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6J9;

    return-object v0
.end method
