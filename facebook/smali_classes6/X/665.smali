.class public final LX/665;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65Z;


# instance fields
.field private final a:LX/670;

.field private final b:LX/672;

.field public final c:LX/670;

.field private final d:Z

.field private e:Z


# direct methods
.method public constructor <init>(LX/670;Z)V
    .locals 3

    .prologue
    .line 1049399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049400
    iput-object p1, p0, LX/665;->a:LX/670;

    .line 1049401
    iput-boolean p2, p0, LX/665;->d:Z

    .line 1049402
    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0}, Ljava/util/zip/Deflater;-><init>()V

    .line 1049403
    sget-object v1, LX/666;->a:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/Deflater;->setDictionary([B)V

    .line 1049404
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    iput-object v1, p0, LX/665;->b:LX/672;

    .line 1049405
    new-instance v1, LX/674;

    iget-object v2, p0, LX/665;->b:LX/672;

    invoke-direct {v1, v2, v0}, LX/674;-><init>(LX/65J;Ljava/util/zip/Deflater;)V

    invoke-static {v1}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v0

    iput-object v0, p0, LX/665;->c:LX/670;

    .line 1049406
    return-void
.end method

.method private a(IILX/672;I)V
    .locals 4

    .prologue
    .line 1049391
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049392
    :cond_0
    int-to-long v0, p4

    const-wide/32 v2, 0xffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1049393
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FRAME_TOO_LARGE max size is 16Mib: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049394
    :cond_1
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049395
    iget-object v0, p0, LX/665;->a:LX/670;

    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x18

    const v2, 0xffffff

    and-int/2addr v2, p4

    or-int/2addr v1, v2

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049396
    if-lez p4, :cond_2

    .line 1049397
    iget-object v0, p0, LX/665;->a:LX/670;

    int-to-long v2, p4

    invoke-interface {v0, p3, v2, v3}, LX/65J;->a_(LX/672;J)V

    .line 1049398
    :cond_2
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 0

    .prologue
    .line 1049390
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final a(IILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1049303
    return-void
.end method

.method public final declared-synchronized a(IJ)V
    .locals 4

    .prologue
    .line 1049381
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049382
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 1049383
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "windowSizeIncrement must be between 1 and 0x7fffffff: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049384
    :cond_2
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, -0x7ffcfff7

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049385
    iget-object v0, p0, LX/665;->a:LX/670;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049386
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0, p1}, LX/670;->f(I)LX/670;

    .line 1049387
    iget-object v0, p0, LX/665;->a:LX/670;

    long-to-int v1, p2

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049388
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049389
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ILX/65X;)V
    .locals 2

    .prologue
    .line 1049373
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049374
    :cond_0
    :try_start_1
    iget v0, p2, LX/65X;->spdyRstCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1049375
    :cond_1
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, -0x7ffcfffd

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049376
    iget-object v0, p0, LX/665;->a:LX/670;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049377
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049378
    iget-object v0, p0, LX/665;->a:LX/670;

    iget v1, p2, LX/65X;->spdyRstCode:I

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049379
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049380
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ILX/65X;[B)V
    .locals 2

    .prologue
    .line 1049364
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049365
    :cond_0
    :try_start_1
    iget v0, p2, LX/65X;->spdyGoAwayCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1049366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "errorCode.spdyGoAwayCode == -1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049367
    :cond_1
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, -0x7ffcfff9

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049368
    iget-object v0, p0, LX/665;->a:LX/670;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049369
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0, p1}, LX/670;->f(I)LX/670;

    .line 1049370
    iget-object v0, p0, LX/665;->a:LX/670;

    iget v1, p2, LX/65X;->spdyGoAwayCode:I

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049371
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049372
    monitor-exit p0

    return-void
.end method

.method public final a(LX/663;)V
    .locals 0

    .prologue
    .line 1049363
    return-void
.end method

.method public final declared-synchronized a(ZII)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1049354
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/665;->e:Z

    if-eqz v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049355
    :cond_0
    :try_start_1
    iget-boolean v3, p0, LX/665;->d:Z

    and-int/lit8 v2, p2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-eq v3, v2, :cond_2

    .line 1049356
    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "payload != reply"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v2, v1

    .line 1049357
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 1049358
    :cond_3
    iget-object v0, p0, LX/665;->a:LX/670;

    const v1, -0x7ffcfffa

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049359
    iget-object v0, p0, LX/665;->a:LX/670;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049360
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0, p2}, LX/670;->f(I)LX/670;

    .line 1049361
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049362
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(ZILX/672;I)V
    .locals 1

    .prologue
    .line 1049350
    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 1049351
    :goto_0
    :try_start_0
    invoke-direct {p0, p2, v0, p3, p4}, LX/665;->a(IILX/672;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1049352
    monitor-exit p0

    return-void

    .line 1049353
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZZIILjava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZII",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v6, 0x7fffffff

    const/4 v0, 0x0

    .line 1049326
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/665;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049327
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/665;->c:LX/670;

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v2}, LX/670;->f(I)LX/670;

    .line 1049328
    const/4 v1, 0x0

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 1049329
    invoke-interface {p5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/65j;

    iget-object v1, v1, LX/65j;->h:LX/673;

    .line 1049330
    iget-object v4, p0, LX/665;->c:LX/670;

    invoke-virtual {v1}, LX/673;->e()I

    move-result v5

    invoke-interface {v4, v5}, LX/670;->f(I)LX/670;

    .line 1049331
    iget-object v4, p0, LX/665;->c:LX/670;

    invoke-interface {v4, v1}, LX/670;->b(LX/673;)LX/670;

    .line 1049332
    invoke-interface {p5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/65j;

    iget-object v1, v1, LX/65j;->i:LX/673;

    .line 1049333
    iget-object v4, p0, LX/665;->c:LX/670;

    invoke-virtual {v1}, LX/673;->e()I

    move-result v5

    invoke-interface {v4, v5}, LX/670;->f(I)LX/670;

    .line 1049334
    iget-object v4, p0, LX/665;->c:LX/670;

    invoke-interface {v4, v1}, LX/670;->b(LX/673;)LX/670;

    .line 1049335
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1049336
    :cond_1
    iget-object v1, p0, LX/665;->c:LX/670;

    invoke-interface {v1}, LX/670;->flush()V

    .line 1049337
    const-wide/16 v2, 0xa

    iget-object v1, p0, LX/665;->b:LX/672;

    .line 1049338
    iget-wide v7, v1, LX/672;->b:J

    move-wide v4, v7

    .line 1049339
    add-long/2addr v2, v4

    long-to-int v2, v2

    .line 1049340
    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz p2, :cond_2

    const/4 v0, 0x2

    :cond_2
    or-int/2addr v0, v1

    .line 1049341
    iget-object v1, p0, LX/665;->a:LX/670;

    const v3, -0x7ffcffff

    invoke-interface {v1, v3}, LX/670;->f(I)LX/670;

    .line 1049342
    iget-object v1, p0, LX/665;->a:LX/670;

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const v3, 0xffffff

    and-int/2addr v2, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, LX/670;->f(I)LX/670;

    .line 1049343
    iget-object v0, p0, LX/665;->a:LX/670;

    and-int v1, p3, v6

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049344
    iget-object v0, p0, LX/665;->a:LX/670;

    and-int v1, p4, v6

    invoke-interface {v0, v1}, LX/670;->f(I)LX/670;

    .line 1049345
    iget-object v0, p0, LX/665;->a:LX/670;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/670;->g(I)LX/670;

    .line 1049346
    iget-object v0, p0, LX/665;->a:LX/670;

    iget-object v1, p0, LX/665;->b:LX/672;

    invoke-interface {v0, v1}, LX/670;->a(LX/65D;)J

    .line 1049347
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049348
    monitor-exit p0

    return-void

    :cond_3
    move v1, v0

    .line 1049349
    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1049323
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049324
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049325
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(LX/663;)V
    .locals 5

    .prologue
    const v4, 0xffffff

    .line 1049309
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/665;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1049310
    :cond_0
    :try_start_1
    invoke-virtual {p1}, LX/663;->b()I

    move-result v0

    .line 1049311
    mul-int/lit8 v1, v0, 0x8

    add-int/lit8 v1, v1, 0x4

    .line 1049312
    iget-object v2, p0, LX/665;->a:LX/670;

    const v3, -0x7ffcfffc

    invoke-interface {v2, v3}, LX/670;->f(I)LX/670;

    .line 1049313
    iget-object v2, p0, LX/665;->a:LX/670;

    and-int/2addr v1, v4

    or-int/lit8 v1, v1, 0x0

    invoke-interface {v2, v1}, LX/670;->f(I)LX/670;

    .line 1049314
    iget-object v1, p0, LX/665;->a:LX/670;

    invoke-interface {v1, v0}, LX/670;->f(I)LX/670;

    .line 1049315
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-gt v0, v1, :cond_2

    .line 1049316
    invoke-virtual {p1, v0}, LX/663;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1049317
    invoke-virtual {p1, v0}, LX/663;->c(I)I

    move-result v1

    .line 1049318
    iget-object v2, p0, LX/665;->a:LX/670;

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    and-int v3, v0, v4

    or-int/2addr v1, v3

    invoke-interface {v2, v1}, LX/670;->f(I)LX/670;

    .line 1049319
    iget-object v1, p0, LX/665;->a:LX/670;

    invoke-virtual {p1, v0}, LX/663;->b(I)I

    move-result v2

    invoke-interface {v1, v2}, LX/670;->f(I)LX/670;

    .line 1049320
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1049321
    :cond_2
    iget-object v0, p0, LX/665;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049322
    monitor-exit p0

    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1049308
    const/16 v0, 0x3fff

    return v0
.end method

.method public final declared-synchronized close()V
    .locals 2

    .prologue
    .line 1049304
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/665;->e:Z

    .line 1049305
    iget-object v0, p0, LX/665;->a:LX/670;

    iget-object v1, p0, LX/665;->c:LX/670;

    invoke-static {v0, v1}, LX/65A;->a(Ljava/io/Closeable;Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1049306
    monitor-exit p0

    return-void

    .line 1049307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
