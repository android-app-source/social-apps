.class public final enum LX/6J0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6J0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6J0;

.field public static final enum CLOSED:LX/6J0;

.field public static final enum CLOSE_IN_PROGRESS:LX/6J0;

.field public static final enum OPENED:LX/6J0;

.field public static final enum OPEN_IN_PROGRESS:LX/6J0;

.field public static final enum PREVIEW:LX/6J0;

.field public static final enum PREVIEW_IN_PROGRESS:LX/6J0;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1075191
    new-instance v0, LX/6J0;

    const-string v1, "OPEN_IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->OPEN_IN_PROGRESS:LX/6J0;

    .line 1075192
    new-instance v0, LX/6J0;

    const-string v1, "OPENED"

    invoke-direct {v0, v1, v4}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->OPENED:LX/6J0;

    .line 1075193
    new-instance v0, LX/6J0;

    const-string v1, "PREVIEW_IN_PROGRESS"

    invoke-direct {v0, v1, v5}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    .line 1075194
    new-instance v0, LX/6J0;

    const-string v1, "PREVIEW"

    invoke-direct {v0, v1, v6}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->PREVIEW:LX/6J0;

    .line 1075195
    new-instance v0, LX/6J0;

    const-string v1, "CLOSE_IN_PROGRESS"

    invoke-direct {v0, v1, v7}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->CLOSE_IN_PROGRESS:LX/6J0;

    .line 1075196
    new-instance v0, LX/6J0;

    const-string v1, "CLOSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6J0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6J0;->CLOSED:LX/6J0;

    .line 1075197
    const/4 v0, 0x6

    new-array v0, v0, [LX/6J0;

    sget-object v1, LX/6J0;->OPEN_IN_PROGRESS:LX/6J0;

    aput-object v1, v0, v3

    sget-object v1, LX/6J0;->OPENED:LX/6J0;

    aput-object v1, v0, v4

    sget-object v1, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    aput-object v1, v0, v5

    sget-object v1, LX/6J0;->PREVIEW:LX/6J0;

    aput-object v1, v0, v6

    sget-object v1, LX/6J0;->CLOSE_IN_PROGRESS:LX/6J0;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6J0;->CLOSED:LX/6J0;

    aput-object v2, v0, v1

    sput-object v0, LX/6J0;->$VALUES:[LX/6J0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1075198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6J0;
    .locals 1

    .prologue
    .line 1075190
    const-class v0, LX/6J0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6J0;

    return-object v0
.end method

.method public static values()[LX/6J0;
    .locals 1

    .prologue
    .line 1075189
    sget-object v0, LX/6J0;->$VALUES:[LX/6J0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6J0;

    return-object v0
.end method
