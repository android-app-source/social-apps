.class public LX/68m;
.super LX/68l;
.source ""


# static fields
.field private static x:LX/68s;

.field private static final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68m;",
            ">;"
        }
    .end annotation
.end field

.field public static z:Landroid/graphics/Bitmap;


# instance fields
.field public final A:LX/68n;

.field public final B:LX/68Y;

.field private C:Z

.field public D:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1056432
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, LX/68m;->y:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(LX/680;LX/68n;)V
    .locals 16

    .prologue
    .line 1056406
    new-instance v2, LX/69D;

    invoke-direct {v2}, LX/69D;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/69D;->a(LX/68U;)LX/69D;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/69D;->a(Z)LX/69D;

    move-result-object v2

    invoke-static {}, LX/68m;->v()LX/68s;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, LX/68l;-><init>(LX/680;LX/69D;LX/68s;)V

    .line 1056407
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/68m;->D:I

    .line 1056408
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/68m;->j:I

    .line 1056409
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/68m;->q:D

    .line 1056410
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/68m;->A:LX/68n;

    .line 1056411
    new-instance v2, LX/68Y;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, LX/68Y;-><init>(LX/680;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/68m;->B:LX/68Y;

    .line 1056412
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->e:LX/680;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68m;->B:LX/68Y;

    invoke-virtual {v2, v3}, LX/680;->a(LX/67m;)LX/67m;

    .line 1056413
    new-instance v2, LX/68k;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/68k;-><init>(LX/68m;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/68m;->p:LX/68i;

    .line 1056414
    sget-object v2, LX/68m;->z:Landroid/graphics/Bitmap;

    if-nez v2, :cond_3

    .line 1056415
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->e:LX/680;

    invoke-virtual {v2}, LX/680;->d()I

    move-result v15

    .line 1056416
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->e:LX/680;

    invoke-virtual {v2}, LX/680;->f()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1056417
    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v15, v15, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, LX/68m;->z:Landroid/graphics/Bitmap;

    .line 1056418
    new-instance v2, Landroid/graphics/Canvas;

    sget-object v4, LX/68m;->z:Landroid/graphics/Bitmap;

    invoke-direct {v2, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1056419
    const/16 v4, 0x140

    if-lt v3, v4, :cond_1

    const/16 v3, 0x20

    move v14, v3

    .line 1056420
    :goto_0
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1056421
    const v3, -0x6e685d

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056422
    const/4 v3, 0x0

    :goto_1
    int-to-float v4, v15

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_3

    .line 1056423
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_0

    int-to-float v4, v15

    cmpl-float v4, v3, v4

    if-nez v4, :cond_2

    :cond_0
    const/16 v4, 0x2c

    :goto_2
    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056424
    const/4 v4, 0x0

    int-to-float v6, v15

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056425
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v9, v3, v4

    const/4 v10, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v11, v3, v4

    int-to-float v12, v15

    move-object v8, v2

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056426
    const/4 v9, 0x0

    int-to-float v11, v15

    move-object v8, v2

    move v10, v3

    move v12, v3

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056427
    const/4 v9, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v10, v3, v4

    int-to-float v11, v15

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v12, v3, v4

    move-object v8, v2

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056428
    int-to-float v4, v14

    add-float/2addr v3, v4

    goto :goto_1

    .line 1056429
    :cond_1
    const/16 v3, 0x10

    move v14, v3

    goto :goto_0

    .line 1056430
    :cond_2
    const/16 v4, 0x12

    goto :goto_2

    .line 1056431
    :cond_3
    return-void
.end method

.method private static b([I)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1056393
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1056394
    if-nez v4, :cond_0

    .line 1056395
    aput v2, p0, v2

    .line 1056396
    aput v2, p0, v5

    .line 1056397
    :goto_0
    return-void

    .line 1056398
    :cond_0
    const-wide v0, 0x3ff999999999999aL    # 1.6

    int-to-double v6, v4

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    sub-double/2addr v0, v6

    const-wide v6, 0x3ff199999999999aL    # 1.1

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    move v1, v2

    move v3, v2

    .line 1056399
    :goto_1
    if-ge v1, v4, :cond_1

    .line 1056400
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68m;

    iget v0, v0, LX/68P;->t:I

    add-int/2addr v3, v0

    .line 1056401
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1056402
    :cond_1
    int-to-double v0, v3

    mul-double/2addr v0, v6

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 1056403
    sub-int v1, v0, v3

    add-int/lit8 v1, v1, -0x1

    .line 1056404
    aput v0, p0, v2

    .line 1056405
    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    aput v0, p0, v5

    goto :goto_0
.end method

.method private static v()LX/68s;
    .locals 1

    .prologue
    .line 1056390
    sget-object v0, LX/68m;->x:LX/68s;

    if-nez v0, :cond_0

    .line 1056391
    new-instance v0, LX/68s;

    invoke-direct {v0}, LX/68s;-><init>()V

    sput-object v0, LX/68m;->x:LX/68s;

    .line 1056392
    :cond_0
    sget-object v0, LX/68m;->x:LX/68s;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1056367
    invoke-static {}, LX/31U;->a()J

    move-result-wide v0

    .line 1056368
    iget-object v2, p0, LX/68m;->B:LX/68Y;

    const/4 v3, 0x0

    iput v3, v2, LX/68Y;->o:I

    .line 1056369
    invoke-super {p0, p1}, LX/68l;->a(Landroid/graphics/Canvas;)V

    .line 1056370
    sget-object v2, LX/31U;->j:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, LX/31U;->a(J)V

    .line 1056371
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1056386
    invoke-super {p0, p1}, LX/68l;->a(Z)V

    .line 1056387
    iget-object v1, p0, LX/68m;->B:LX/68Y;

    iget-boolean v0, p0, LX/68m;->C:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/67m;->a(Z)V

    .line 1056388
    return-void

    .line 1056389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([I)V
    .locals 1

    .prologue
    .line 1056433
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1056434
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056435
    :cond_0
    invoke-static {p1}, LX/68m;->b([I)V

    .line 1056436
    return-void
.end method

.method public final b(III)LX/69C;
    .locals 2

    .prologue
    .line 1056381
    invoke-super {p0, p1, p2, p3}, LX/68l;->b(III)LX/69C;

    move-result-object v0

    .line 1056382
    if-eqz v0, :cond_0

    .line 1056383
    invoke-virtual {v0, p1, p2, p3}, LX/69C;->a(III)LX/69C;

    .line 1056384
    iget v1, p0, LX/68m;->D:I

    invoke-static {v0, v1}, LX/3BU;->a(LX/69C;I)I

    move-result v1

    iput v1, v0, LX/69C;->h:I

    .line 1056385
    :cond_0
    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1056372
    invoke-super {p0}, LX/68l;->b()V

    .line 1056373
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1056374
    iget-object v3, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v3

    .line 1056375
    const/high16 v3, 0x437a0000    # 250.0f

    iget v4, p0, LX/67m;->d:F

    mul-float/2addr v3, v4

    .line 1056376
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/68m;->C:Z

    .line 1056377
    iget-object v0, p0, LX/68m;->B:LX/68Y;

    iget-boolean v3, p0, LX/68m;->C:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, LX/67m;->i:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, LX/67m;->a(Z)V

    .line 1056378
    return-void

    :cond_0
    move v0, v2

    .line 1056379
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1056380
    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1056366
    const/4 v0, 0x1

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1056363
    invoke-super {p0}, LX/68l;->l()V

    .line 1056364
    iget-object v0, p0, LX/68m;->B:LX/68Y;

    invoke-virtual {v0}, LX/67m;->l()V

    .line 1056365
    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1056349
    iget-object v0, p0, LX/68m;->A:LX/68n;

    invoke-virtual {v0}, LX/68n;->a()V

    .line 1056350
    iput v2, p0, LX/68m;->t:I

    .line 1056351
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1056352
    sget-object v0, LX/68m;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1056353
    invoke-virtual {p0, v2}, LX/68P;->b(Z)V

    .line 1056354
    :cond_0
    iget-object v0, p0, LX/68P;->w:[I

    invoke-static {v0}, LX/68m;->b([I)V

    .line 1056355
    iget-object v0, p0, LX/68P;->o:LX/68s;

    iget-object v1, p0, LX/68P;->w:[I

    aget v1, v1, v2

    .line 1056356
    iput v1, v0, LX/68s;->e:I

    .line 1056357
    move-object v0, v0

    .line 1056358
    iget-object v1, p0, LX/68P;->w:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 1056359
    iput v1, v0, LX/68s;->f:I

    .line 1056360
    move-object v0, v0

    .line 1056361
    invoke-virtual {v0}, LX/68s;->b()V

    .line 1056362
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 1056347
    const-wide v0, 0x3ff3333333333333L    # 1.2

    iput-wide v0, p0, LX/68m;->q:D

    .line 1056348
    return-void
.end method
