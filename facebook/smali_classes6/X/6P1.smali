.class public abstract LX/6P1;
.super LX/3dH;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/3d4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d4",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/3d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d2",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1085988
    invoke-direct {p0}, LX/3dH;-><init>()V

    .line 1085989
    new-instance v0, LX/6Pg;

    invoke-direct {v0, p0}, LX/6Pg;-><init>(LX/6P1;)V

    iput-object v0, p0, LX/6P1;->b:LX/3d4;

    .line 1085990
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085991
    new-instance v0, LX/3d2;

    const-class v1, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/6P1;->b:LX/3d4;

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/6P1;->f:LX/3d2;

    .line 1085992
    iput-object p1, p0, LX/6P1;->g:Ljava/lang/String;

    .line 1085993
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1085994
    iget-object v0, p0, LX/6P1;->f:LX/3d2;

    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085995
    iget-object v0, p0, LX/6P1;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
