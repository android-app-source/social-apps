.class public final enum LX/6Qo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Qo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Qo;

.field public static final enum Any:LX/6Qo;

.field public static final enum WifiForce:LX/6Qo;

.field public static final enum WifiOnly:LX/6Qo;


# instance fields
.field private final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1088008
    new-instance v0, LX/6Qo;

    const-string v1, "WifiOnly"

    invoke-direct {v0, v1, v2, v2}, LX/6Qo;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Qo;->WifiOnly:LX/6Qo;

    .line 1088009
    new-instance v0, LX/6Qo;

    const-string v1, "Any"

    invoke-direct {v0, v1, v3, v3}, LX/6Qo;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Qo;->Any:LX/6Qo;

    .line 1088010
    new-instance v0, LX/6Qo;

    const-string v1, "WifiForce"

    invoke-direct {v0, v1, v4, v4}, LX/6Qo;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Qo;->WifiForce:LX/6Qo;

    .line 1088011
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Qo;

    sget-object v1, LX/6Qo;->WifiOnly:LX/6Qo;

    aput-object v1, v0, v2

    sget-object v1, LX/6Qo;->Any:LX/6Qo;

    aput-object v1, v0, v3

    sget-object v1, LX/6Qo;->WifiForce:LX/6Qo;

    aput-object v1, v0, v4

    sput-object v0, LX/6Qo;->$VALUES:[LX/6Qo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1088019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1088020
    iput p3, p0, LX/6Qo;->mValue:I

    .line 1088021
    return-void
.end method

.method public static fromValue(I)LX/6Qo;
    .locals 1

    .prologue
    .line 1088015
    packed-switch p0, :pswitch_data_0

    .line 1088016
    sget-object v0, LX/6Qo;->WifiOnly:LX/6Qo;

    :goto_0
    return-object v0

    .line 1088017
    :pswitch_0
    sget-object v0, LX/6Qo;->WifiForce:LX/6Qo;

    goto :goto_0

    .line 1088018
    :pswitch_1
    sget-object v0, LX/6Qo;->Any:LX/6Qo;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Qo;
    .locals 1

    .prologue
    .line 1088014
    const-class v0, LX/6Qo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Qo;

    return-object v0
.end method

.method public static values()[LX/6Qo;
    .locals 1

    .prologue
    .line 1088013
    sget-object v0, LX/6Qo;->$VALUES:[LX/6Qo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Qo;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1088012
    iget v0, p0, LX/6Qo;->mValue:I

    return v0
.end method
