.class public LX/54y;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field public a:F

.field public final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/RectF;

.field private final d:Landroid/graphics/Rect;

.field public e:F

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(IF)V
    .locals 2

    .prologue
    .line 828732
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 828733
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/54y;->f:Z

    .line 828734
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/54y;->g:Z

    .line 828735
    iput p2, p0, LX/54y;->a:F

    .line 828736
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/54y;->b:Landroid/graphics/Paint;

    .line 828737
    iget-object v0, p0, LX/54y;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 828738
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/54y;->c:Landroid/graphics/RectF;

    .line 828739
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/54y;->d:Landroid/graphics/Rect;

    .line 828740
    return-void
.end method

.method public static a(LX/54y;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 828722
    if-nez p1, :cond_0

    .line 828723
    invoke-virtual {p0}, LX/54y;->getBounds()Landroid/graphics/Rect;

    move-result-object p1

    .line 828724
    :cond_0
    iget-object v0, p0, LX/54y;->c:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 828725
    iget-object v0, p0, LX/54y;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 828726
    iget-boolean v0, p0, LX/54y;->f:Z

    if-eqz v0, :cond_1

    .line 828727
    iget v0, p0, LX/54y;->e:F

    iget v1, p0, LX/54y;->a:F

    iget-boolean v2, p0, LX/54y;->g:Z

    invoke-static {v0, v1, v2}, LX/54z;->a(FFZ)F

    move-result v0

    .line 828728
    iget v1, p0, LX/54y;->e:F

    iget v2, p0, LX/54y;->a:F

    iget-boolean v3, p0, LX/54y;->g:Z

    invoke-static {v1, v2, v3}, LX/54z;->b(FFZ)F

    move-result v1

    .line 828729
    iget-object v2, p0, LX/54y;->d:Landroid/graphics/Rect;

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 828730
    iget-object v0, p0, LX/54y;->c:Landroid/graphics/RectF;

    iget-object v1, p0, LX/54y;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 828731
    :cond_1
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 828720
    iget-object v0, p0, LX/54y;->c:Landroid/graphics/RectF;

    iget v1, p0, LX/54y;->a:F

    iget v2, p0, LX/54y;->a:F

    iget-object v3, p0, LX/54y;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 828721
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 828719
    const/4 v0, -0x3

    return v0
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
    .locals 2

    .prologue
    .line 828717
    iget-object v0, p0, LX/54y;->d:Landroid/graphics/Rect;

    iget v1, p0, LX/54y;->a:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Outline;->setRoundRect(Landroid/graphics/Rect;F)V

    .line 828718
    return-void
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 828714
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 828715
    invoke-static {p0, p1}, LX/54y;->a(LX/54y;Landroid/graphics/Rect;)V

    .line 828716
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 828712
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 828713
    return-void
.end method
