.class public final LX/5Ca;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 868319
    const-class v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    const v0, 0x3ea7c1ce

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FeedbackReactors"

    const-string v6, "dbacf1c00399846a353a64ce8ef1179a"

    const-string v7, "node"

    const-string v8, "10155192969636729"

    const-string v9, "10155259087041729"

    .line 868320
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 868321
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 868322
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 868323
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 868324
    sparse-switch v0, :sswitch_data_0

    .line 868325
    :goto_0
    return-object p1

    .line 868326
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 868327
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 868328
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 868329
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 868330
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 868331
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 868332
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 868333
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 868334
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 868335
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x622aa070 -> :sswitch_6
        -0x4da3e3eb -> :sswitch_2
        0x107ed14 -> :sswitch_7
        0x5684d8c -> :sswitch_3
        0x21a83aac -> :sswitch_8
        0x291d8de0 -> :sswitch_4
        0x52a11695 -> :sswitch_9
        0x613be1d1 -> :sswitch_1
        0x725bf414 -> :sswitch_0
        0x7895daa9 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 868336
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 868337
    :goto_1
    return v0

    .line 868338
    :pswitch_0
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const-string v2, "9"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 868339
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 868340
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 868341
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x37
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
