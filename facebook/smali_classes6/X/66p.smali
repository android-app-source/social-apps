.class public final LX/66p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public final synthetic a:LX/66q;


# direct methods
.method public constructor <init>(LX/66q;)V
    .locals 0

    .prologue
    .line 1050998
    iput-object p1, p0, LX/66p;->a:LX/66q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 9

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, -0x1

    .line 1050999
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051000
    :cond_0
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->f:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051001
    :cond_1
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-wide v0, v0, LX/66q;->i:J

    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-wide v2, v2, LX/66q;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 1051002
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->j:Z

    if-eqz v0, :cond_2

    move-wide v1, v4

    .line 1051003
    :goto_0
    return-wide v1

    .line 1051004
    :cond_2
    iget-object v0, p0, LX/66p;->a:LX/66q;

    invoke-static {v0}, LX/66q;->e(LX/66q;)V

    .line 1051005
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget v0, v0, LX/66q;->g:I

    if-eqz v0, :cond_3

    .line 1051006
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected continuation opcode. Got: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget v2, v2, LX/66q;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051007
    :cond_3
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->j:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-wide v0, v0, LX/66q;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    move-wide v1, v4

    .line 1051008
    goto :goto_0

    .line 1051009
    :cond_4
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-wide v0, v0, LX/66q;->h:J

    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-wide v2, v2, LX/66q;->i:J

    sub-long/2addr v0, v2

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1051010
    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-boolean v2, v2, LX/66q;->l:Z

    if-eqz v2, :cond_7

    .line 1051011
    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-object v2, v2, LX/66q;->n:[B

    array-length v2, v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1051012
    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-object v2, v2, LX/66q;->b:LX/671;

    iget-object v3, p0, LX/66p;->a:LX/66q;

    iget-object v3, v3, LX/66q;->n:[B

    long-to-int v0, v0

    invoke-interface {v2, v3, v6, v0}, LX/671;->a([BII)I

    move-result v0

    int-to-long v1, v0

    .line 1051013
    cmp-long v0, v1, v4

    if-nez v0, :cond_5

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051014
    :cond_5
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v0, v0, LX/66q;->n:[B

    iget-object v3, p0, LX/66p;->a:LX/66q;

    iget-object v3, v3, LX/66q;->m:[B

    iget-object v4, p0, LX/66p;->a:LX/66q;

    iget-wide v4, v4, LX/66q;->i:J

    invoke-static/range {v0 .. v5}, LX/66n;->a([BJ[BJ)V

    .line 1051015
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v0, v0, LX/66q;->n:[B

    long-to-int v3, v1

    invoke-virtual {p1, v0, v6, v3}, LX/672;->b([BII)LX/672;

    .line 1051016
    :cond_6
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v3, p0, LX/66p;->a:LX/66q;

    iget-wide v4, v3, LX/66q;->i:J

    add-long/2addr v4, v1

    .line 1051017
    iput-wide v4, v0, LX/66q;->i:J

    .line 1051018
    goto/16 :goto_0

    .line 1051019
    :cond_7
    iget-object v2, p0, LX/66p;->a:LX/66q;

    iget-object v2, v2, LX/66q;->b:LX/671;

    invoke-interface {v2, p1, v0, v1}, LX/65D;->a(LX/672;J)J

    move-result-wide v1

    .line 1051020
    cmp-long v0, v1, v4

    if-nez v0, :cond_6

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1051021
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v0, v0, LX/66q;->b:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 6

    .prologue
    .line 1051022
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->f:Z

    if-eqz v0, :cond_1

    .line 1051023
    :cond_0
    return-void

    .line 1051024
    :cond_1
    iget-object v0, p0, LX/66p;->a:LX/66q;

    const/4 v1, 0x1

    .line 1051025
    iput-boolean v1, v0, LX/66q;->f:Z

    .line 1051026
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->e:Z

    if-nez v0, :cond_0

    .line 1051027
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v0, v0, LX/66q;->b:LX/671;

    iget-object v1, p0, LX/66p;->a:LX/66q;

    iget-wide v2, v1, LX/66q;->h:J

    iget-object v1, p0, LX/66p;->a:LX/66q;

    iget-wide v4, v1, LX/66q;->i:J

    sub-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    .line 1051028
    :goto_0
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-boolean v0, v0, LX/66q;->j:Z

    if-nez v0, :cond_0

    .line 1051029
    iget-object v0, p0, LX/66p;->a:LX/66q;

    invoke-static {v0}, LX/66q;->e(LX/66q;)V

    .line 1051030
    iget-object v0, p0, LX/66p;->a:LX/66q;

    iget-object v0, v0, LX/66q;->b:LX/671;

    iget-object v1, p0, LX/66p;->a:LX/66q;

    iget-wide v2, v1, LX/66q;->h:J

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    goto :goto_0
.end method
