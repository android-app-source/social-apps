.class public final LX/61r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;"
        }
    .end annotation
.end field

.field public final k:[I

.field private final l:Z


# direct methods
.method public constructor <init>(ZLX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1040737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040738
    iput v0, p0, LX/61r;->a:I

    .line 1040739
    iput v0, p0, LX/61r;->b:I

    .line 1040740
    iput v0, p0, LX/61r;->c:I

    .line 1040741
    iput v0, p0, LX/61r;->d:I

    .line 1040742
    iput v0, p0, LX/61r;->e:I

    .line 1040743
    iput v0, p0, LX/61r;->f:I

    .line 1040744
    iput v0, p0, LX/61r;->g:I

    .line 1040745
    iput-boolean v1, p0, LX/61r;->h:Z

    .line 1040746
    iput-boolean v1, p0, LX/61r;->i:Z

    .line 1040747
    iput-object p2, p0, LX/61r;->j:LX/0Px;

    .line 1040748
    iput-boolean p1, p0, LX/61r;->l:Z

    .line 1040749
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/61r;->k:[I

    .line 1040750
    return-void
.end method

.method public static a(LX/61r;LX/1Cw;)I
    .locals 1

    .prologue
    .line 1040736
    iget-boolean v0, p0, LX/61r;->l:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, LX/62C;

    if-nez v0, :cond_0

    const v0, 0xf4240

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    goto :goto_0
.end method

.method public static h(LX/61r;)I
    .locals 1

    .prologue
    .line 1040786
    iget-object v0, p0, LX/61r;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cw;
    .locals 2

    .prologue
    .line 1040785
    iget-object v0, p0, LX/61r;->j:LX/0Px;

    iget v1, p0, LX/61r;->a:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1040778
    iput v0, p0, LX/61r;->a:I

    .line 1040779
    iput v0, p0, LX/61r;->d:I

    .line 1040780
    iput v0, p0, LX/61r;->e:I

    .line 1040781
    invoke-virtual {p0}, LX/61r;->a()LX/1Cw;

    move-result-object v0

    .line 1040782
    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v1

    iput v1, p0, LX/61r;->b:I

    .line 1040783
    invoke-static {p0, v0}, LX/61r;->a(LX/61r;LX/1Cw;)I

    move-result v0

    iput v0, p0, LX/61r;->c:I

    .line 1040784
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1040777
    iget v0, p0, LX/61r;->a:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/61r;->a:I

    invoke-static {p0}, LX/61r;->h(LX/61r;)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1040767
    iget v0, p0, LX/61r;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/61r;->a:I

    .line 1040768
    invoke-virtual {p0}, LX/61r;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1040769
    const/4 v0, 0x0

    .line 1040770
    :goto_0
    return v0

    .line 1040771
    :cond_0
    iget v0, p0, LX/61r;->d:I

    iget v1, p0, LX/61r;->b:I

    add-int/2addr v0, v1

    iput v0, p0, LX/61r;->d:I

    .line 1040772
    iget v0, p0, LX/61r;->e:I

    iget v1, p0, LX/61r;->c:I

    add-int/2addr v0, v1

    iput v0, p0, LX/61r;->e:I

    .line 1040773
    invoke-virtual {p0}, LX/61r;->a()LX/1Cw;

    move-result-object v0

    .line 1040774
    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v1

    iput v1, p0, LX/61r;->b:I

    .line 1040775
    invoke-static {p0, v0}, LX/61r;->a(LX/61r;LX/1Cw;)I

    move-result v0

    iput v0, p0, LX/61r;->c:I

    .line 1040776
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1040757
    iget v0, p0, LX/61r;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/61r;->a:I

    .line 1040758
    invoke-virtual {p0}, LX/61r;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1040759
    const/4 v0, 0x0

    .line 1040760
    :goto_0
    return v0

    .line 1040761
    :cond_0
    invoke-virtual {p0}, LX/61r;->a()LX/1Cw;

    move-result-object v0

    .line 1040762
    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v1

    iput v1, p0, LX/61r;->b:I

    .line 1040763
    invoke-static {p0, v0}, LX/61r;->a(LX/61r;LX/1Cw;)I

    move-result v0

    iput v0, p0, LX/61r;->c:I

    .line 1040764
    iget v0, p0, LX/61r;->d:I

    iget v1, p0, LX/61r;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/61r;->d:I

    .line 1040765
    iget v0, p0, LX/61r;->e:I

    iget v1, p0, LX/61r;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/61r;->e:I

    .line 1040766
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1040752
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/61r;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1040753
    iget-object v0, p0, LX/61r;->j:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    iget-object v2, p0, LX/61r;->k:[I

    aget v2, v2, v1

    if-eq v0, v2, :cond_0

    .line 1040754
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LX/61r;->j:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " changed its itemCount without calling notifyDataSetChanged"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1040755
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1040756
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1040751
    const-class v0, LX/61r;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "index"

    iget v2, p0, LX/61r;->a:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "localItemCount"

    iget v2, p0, LX/61r;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "localViewTypeCount"

    iget v2, p0, LX/61r;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "positionOffset"

    iget v2, p0, LX/61r;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "viewTypeOffset"

    iget v2, p0, LX/61r;->e:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "totalItemCount"

    iget v2, p0, LX/61r;->f:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "totalViewTypeCount"

    iget v2, p0, LX/61r;->g:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "totalAllItemsEnabled"

    iget-boolean v2, p0, LX/61r;->h:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "totalHasStableIds"

    iget-boolean v2, p0, LX/61r;->i:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
