.class public final LX/5C0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 866164
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 866165
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 866166
    :goto_0
    return v1

    .line 866167
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 866168
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 866169
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 866170
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 866171
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 866172
    const-string v5, "aggregated_ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 866173
    invoke-static {p0, p1}, LX/5Bz;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 866174
    :cond_2
    const-string v5, "ranges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 866175
    invoke-static {p0, p1}, LX/4ao;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 866176
    :cond_3
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 866177
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 866178
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 866179
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 866180
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 866181
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 866182
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 866123
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 866124
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866125
    if-eqz v0, :cond_6

    .line 866126
    const-string v1, "aggregated_ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866127
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 866128
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 866129
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v5, 0x0

    .line 866130
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 866131
    invoke-virtual {p0, v2, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 866132
    if-eqz v3, :cond_0

    .line 866133
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866134
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 866135
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 866136
    if-eqz v3, :cond_1

    .line 866137
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866138
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 866139
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 866140
    if-eqz v3, :cond_2

    .line 866141
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866142
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 866143
    :cond_2
    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 866144
    if-eqz v3, :cond_4

    .line 866145
    const-string v4, "sample_entities"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866146
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 866147
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 866148
    invoke-virtual {p0, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {p0, v5, p2}, LX/5By;->a(LX/15i;ILX/0nX;)V

    .line 866149
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 866150
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 866151
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 866152
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 866153
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 866154
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866155
    if-eqz v0, :cond_7

    .line 866156
    const-string v1, "ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866157
    invoke-static {p0, v0, p2, p3}, LX/4ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866158
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 866159
    if-eqz v0, :cond_8

    .line 866160
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866161
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866162
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 866163
    return-void
.end method
