.class public LX/5zr;
.super LX/3hi;
.source ""


# instance fields
.field private final b:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1035888
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 1035889
    iput p1, p0, LX/5zr;->b:I

    .line 1035890
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FZ;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 1035880
    iget v0, p0, LX/5zr;->b:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/5zr;->b:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2, v0, v1}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    move-object v1, v0

    .line 1035881
    :goto_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1035882
    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1035883
    iget v3, p0, LX/5zr;->b:I

    int-to-float v3, v3

    int-to-float v4, v0

    div-float/2addr v4, v6

    int-to-float v0, v0

    div-float/2addr v0, v6

    invoke-virtual {v2, v3, v4, v0}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1035884
    const/4 v0, 0x0

    invoke-virtual {v2, p1, v5, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1035885
    return-object v1

    .line 1035886
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p2, v0, v1}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035887
    const-string v0, "MediaResource Overlay Image Orientation"

    return-object v0
.end method
