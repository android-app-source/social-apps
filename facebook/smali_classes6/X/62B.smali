.class public final LX/62B;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:LX/62C;

.field private final b:LX/1Cw;

.field private c:Z


# direct methods
.method public constructor <init>(LX/62C;LX/1Cw;)V
    .locals 1

    .prologue
    .line 1041240
    iput-object p1, p0, LX/62B;->a:LX/62C;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 1041241
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/62B;->c:Z

    .line 1041242
    iput-object p2, p0, LX/62B;->b:LX/1Cw;

    .line 1041243
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 1041244
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/62B;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041245
    monitor-exit p0

    return-void

    .line 1041246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onChanged()V
    .locals 2

    .prologue
    .line 1041247
    iget-boolean v0, p0, LX/62B;->c:Z

    if-eqz v0, :cond_0

    .line 1041248
    iget-object v0, p0, LX/62B;->a:LX/62C;

    iget-object v1, p0, LX/62B;->b:LX/1Cw;

    invoke-static {v0, v1}, LX/62C;->b(LX/62C;LX/1Cw;)V

    .line 1041249
    :cond_0
    return-void
.end method

.method public final onInvalidated()V
    .locals 2

    .prologue
    .line 1041250
    iget-object v0, p0, LX/62B;->a:LX/62C;

    const v1, -0x1e76209f

    invoke-static {v0, v1}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    .line 1041251
    return-void
.end method
