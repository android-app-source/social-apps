.class public final enum LX/5ns;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5ns;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5ns;

.field public static final enum NEWSFEED:LX/5ns;

.field public static final enum PERMALINK:LX/5ns;

.field public static final enum TIMELINE:LX/5ns;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1004756
    new-instance v0, LX/5ns;

    const-string v1, "NEWSFEED"

    const-string v2, "fb4a_feed"

    invoke-direct {v0, v1, v3, v2}, LX/5ns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ns;->NEWSFEED:LX/5ns;

    .line 1004757
    new-instance v0, LX/5ns;

    const-string v1, "TIMELINE"

    const-string v2, "fb4a_timeline"

    invoke-direct {v0, v1, v4, v2}, LX/5ns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ns;->TIMELINE:LX/5ns;

    .line 1004758
    new-instance v0, LX/5ns;

    const-string v1, "PERMALINK"

    const-string v2, "fb4a_permalink"

    invoke-direct {v0, v1, v5, v2}, LX/5ns;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5ns;->PERMALINK:LX/5ns;

    .line 1004759
    const/4 v0, 0x3

    new-array v0, v0, [LX/5ns;

    sget-object v1, LX/5ns;->NEWSFEED:LX/5ns;

    aput-object v1, v0, v3

    sget-object v1, LX/5ns;->TIMELINE:LX/5ns;

    aput-object v1, v0, v4

    sget-object v1, LX/5ns;->PERMALINK:LX/5ns;

    aput-object v1, v0, v5

    sput-object v0, LX/5ns;->$VALUES:[LX/5ns;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004760
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004761
    iput-object p3, p0, LX/5ns;->name:Ljava/lang/String;

    .line 1004762
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5ns;
    .locals 1

    .prologue
    .line 1004763
    const-class v0, LX/5ns;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5ns;

    return-object v0
.end method

.method public static values()[LX/5ns;
    .locals 1

    .prologue
    .line 1004764
    sget-object v0, LX/5ns;->$VALUES:[LX/5ns;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5ns;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004765
    iget-object v0, p0, LX/5ns;->name:Ljava/lang/String;

    return-object v0
.end method
