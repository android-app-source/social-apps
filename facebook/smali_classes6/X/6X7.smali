.class public final LX/6X7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1107588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLVideo;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1107589
    new-instance v0, LX/4XB;

    invoke-direct {v0}, LX/4XB;-><init>()V

    .line 1107590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107591
    iput-object v1, v0, LX/4XB;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v1

    .line 1107593
    iput v1, v0, LX/4XB;->e:I

    .line 1107594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    .line 1107595
    iput-object v1, v0, LX/4XB;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 1107596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->o()Ljava/lang/String;

    move-result-object v1

    .line 1107597
    iput-object v1, v0, LX/4XB;->g:Ljava/lang/String;

    .line 1107598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bi()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v1

    .line 1107599
    iput-object v1, v0, LX/4XB;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 1107600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->p()J

    move-result-wide v2

    .line 1107601
    iput-wide v2, v0, LX/4XB;->i:J

    .line 1107602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v1

    .line 1107603
    iput v1, v0, LX/4XB;->j:I

    .line 1107604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 1107605
    iput-object v1, v0, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1107606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v1

    .line 1107607
    iput-boolean v1, v0, LX/4XB;->m:Z

    .line 1107608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->t()Z

    move-result v1

    .line 1107609
    iput-boolean v1, v0, LX/4XB;->s:Z

    .line 1107610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->u()Z

    move-result v1

    .line 1107611
    iput-boolean v1, v0, LX/4XB;->t:Z

    .line 1107612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->v()Ljava/lang/String;

    move-result-object v1

    .line 1107613
    iput-object v1, v0, LX/4XB;->x:Ljava/lang/String;

    .line 1107614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v1

    .line 1107615
    iput-object v1, v0, LX/4XB;->z:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 1107616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->w()J

    move-result-wide v2

    .line 1107617
    iput-wide v2, v0, LX/4XB;->A:J

    .line 1107618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1107619
    iput-object v1, v0, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1107620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bk()Z

    move-result v1

    .line 1107621
    iput-boolean v1, v0, LX/4XB;->D:Z

    .line 1107622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->y()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1107623
    iput-object v1, v0, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1107624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1107625
    iput-object v1, v0, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1107626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bl()D

    move-result-wide v2

    .line 1107627
    iput-wide v2, v0, LX/4XB;->I:D

    .line 1107628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    .line 1107629
    iput-object v1, v0, LX/4XB;->J:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 1107630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->A()Z

    move-result v1

    .line 1107631
    iput-boolean v1, v0, LX/4XB;->L:Z

    .line 1107632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->B()Z

    move-result v1

    .line 1107633
    iput-boolean v1, v0, LX/4XB;->M:Z

    .line 1107634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->C()I

    move-result v1

    .line 1107635
    iput v1, v0, LX/4XB;->N:I

    .line 1107636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v1

    .line 1107637
    iput v1, v0, LX/4XB;->O:I

    .line 1107638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bo()Ljava/lang/String;

    move-result-object v1

    .line 1107639
    iput-object v1, v0, LX/4XB;->P:Ljava/lang/String;

    .line 1107640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bp()Ljava/lang/String;

    move-result-object v1

    .line 1107641
    iput-object v1, v0, LX/4XB;->Q:Ljava/lang/String;

    .line 1107642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->E()Ljava/lang/String;

    move-result-object v1

    .line 1107643
    iput-object v1, v0, LX/4XB;->R:Ljava/lang/String;

    .line 1107644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v1

    .line 1107645
    iput v1, v0, LX/4XB;->S:I

    .line 1107646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v1

    .line 1107647
    iput-object v1, v0, LX/4XB;->T:Ljava/lang/String;

    .line 1107648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107649
    iput-object v1, v0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107651
    iput-object v1, v0, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107652
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107653
    iput-object v1, v0, LX/4XB;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107655
    iput-object v1, v0, LX/4XB;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107657
    iput-object v1, v0, LX/4XB;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107659
    iput-object v1, v0, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107661
    iput-object v1, v0, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107663
    iput-object v1, v0, LX/4XB;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107665
    iput-object v1, v0, LX/4XB;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107667
    iput-object v1, v0, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107669
    iput-object v1, v0, LX/4XB;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107670
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->S()I

    move-result v1

    .line 1107671
    iput v1, v0, LX/4XB;->ah:I

    .line 1107672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->T()I

    move-result v1

    .line 1107673
    iput v1, v0, LX/4XB;->ai:I

    .line 1107674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->U()I

    move-result v1

    .line 1107675
    iput v1, v0, LX/4XB;->aj:I

    .line 1107676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    .line 1107677
    iput-object v1, v0, LX/4XB;->ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 1107678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->W()LX/0Px;

    move-result-object v1

    .line 1107679
    iput-object v1, v0, LX/4XB;->al:LX/0Px;

    .line 1107680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->X()Z

    move-result v1

    .line 1107681
    iput-boolean v1, v0, LX/4XB;->am:Z

    .line 1107682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Y()Z

    move-result v1

    .line 1107683
    iput-boolean v1, v0, LX/4XB;->ao:Z

    .line 1107684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Z()Z

    move-result v1

    .line 1107685
    iput-boolean v1, v0, LX/4XB;->ap:Z

    .line 1107686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bn()Z

    move-result v1

    .line 1107687
    iput-boolean v1, v0, LX/4XB;->aq:Z

    .line 1107688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v1

    .line 1107689
    iput-boolean v1, v0, LX/4XB;->ar:Z

    .line 1107690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ab()Z

    move-result v1

    .line 1107691
    iput-boolean v1, v0, LX/4XB;->as:Z

    .line 1107692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ac()Z

    move-result v1

    .line 1107693
    iput-boolean v1, v0, LX/4XB;->at:Z

    .line 1107694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bt()Z

    move-result v1

    .line 1107695
    iput-boolean v1, v0, LX/4XB;->au:Z

    .line 1107696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ad()Z

    move-result v1

    .line 1107697
    iput-boolean v1, v0, LX/4XB;->av:Z

    .line 1107698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ae()Z

    move-result v1

    .line 1107699
    iput-boolean v1, v0, LX/4XB;->aw:Z

    .line 1107700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v1

    .line 1107701
    iput-boolean v1, v0, LX/4XB;->ax:Z

    .line 1107702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v1

    .line 1107703
    iput-boolean v1, v0, LX/4XB;->ay:Z

    .line 1107704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107705
    iput-object v1, v0, LX/4XB;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107707
    iput-object v1, v0, LX/4XB;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107709
    iput-object v1, v0, LX/4XB;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ak()I

    move-result v1

    .line 1107711
    iput v1, v0, LX/4XB;->aE:I

    .line 1107712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->al()I

    move-result v1

    .line 1107713
    iput v1, v0, LX/4XB;->aF:I

    .line 1107714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->am()I

    move-result v1

    .line 1107715
    iput v1, v0, LX/4XB;->aH:I

    .line 1107716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107717
    iput-object v1, v0, LX/4XB;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bs()I

    move-result v1

    .line 1107719
    iput v1, v0, LX/4XB;->aJ:I

    .line 1107720
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107721
    iput-object v1, v0, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107722
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ap()Ljava/lang/String;

    move-result-object v1

    .line 1107723
    iput-object v1, v0, LX/4XB;->aL:Ljava/lang/String;

    .line 1107724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107725
    iput-object v1, v0, LX/4XB;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ar()Ljava/lang/String;

    move-result-object v1

    .line 1107727
    iput-object v1, v0, LX/4XB;->aO:Ljava/lang/String;

    .line 1107728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107729
    iput-object v1, v0, LX/4XB;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107731
    iput-object v1, v0, LX/4XB;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bm()D

    move-result-wide v2

    .line 1107733
    iput-wide v2, v0, LX/4XB;->aR:D

    .line 1107734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->au()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1107735
    iput-object v1, v0, LX/4XB;->aS:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1107736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->av()I

    move-result v1

    .line 1107737
    iput v1, v0, LX/4XB;->aY:I

    .line 1107738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aw()Ljava/lang/String;

    move-result-object v1

    .line 1107739
    iput-object v1, v0, LX/4XB;->aZ:Ljava/lang/String;

    .line 1107740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ax()I

    move-result v1

    .line 1107741
    iput v1, v0, LX/4XB;->ba:I

    .line 1107742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v1

    .line 1107743
    iput v1, v0, LX/4XB;->bb:I

    .line 1107744
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v1

    .line 1107745
    iput-object v1, v0, LX/4XB;->bc:Ljava/lang/String;

    .line 1107746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aA()Ljava/lang/String;

    move-result-object v1

    .line 1107747
    iput-object v1, v0, LX/4XB;->bd:Ljava/lang/String;

    .line 1107748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107749
    iput-object v1, v0, LX/4XB;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107750
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aC()I

    move-result v1

    .line 1107751
    iput v1, v0, LX/4XB;->bf:I

    .line 1107752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aD()Ljava/lang/String;

    move-result-object v1

    .line 1107753
    iput-object v1, v0, LX/4XB;->bg:Ljava/lang/String;

    .line 1107754
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aE()Ljava/lang/String;

    move-result-object v1

    .line 1107755
    iput-object v1, v0, LX/4XB;->bk:Ljava/lang/String;

    .line 1107756
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aF()Ljava/lang/String;

    move-result-object v1

    .line 1107757
    iput-object v1, v0, LX/4XB;->bl:Ljava/lang/String;

    .line 1107758
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1107759
    iput-object v1, v0, LX/4XB;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1107760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107761
    iput-object v1, v0, LX/4XB;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107762
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bq()Ljava/lang/String;

    move-result-object v1

    .line 1107763
    iput-object v1, v0, LX/4XB;->bo:Ljava/lang/String;

    .line 1107764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aI()Z

    move-result v1

    .line 1107765
    iput-boolean v1, v0, LX/4XB;->bp:Z

    .line 1107766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aJ()Z

    move-result v1

    .line 1107767
    iput-boolean v1, v0, LX/4XB;->bq:Z

    .line 1107768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aK()Z

    move-result v1

    .line 1107769
    iput-boolean v1, v0, LX/4XB;->bs:Z

    .line 1107770
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aL()Z

    move-result v1

    .line 1107771
    iput-boolean v1, v0, LX/4XB;->bt:Z

    .line 1107772
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aM()Z

    move-result v1

    .line 1107773
    iput-boolean v1, v0, LX/4XB;->bu:Z

    .line 1107774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aN()D

    move-result-wide v2

    .line 1107775
    iput-wide v2, v0, LX/4XB;->bv:D

    .line 1107776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aO()D

    move-result-wide v2

    .line 1107777
    iput-wide v2, v0, LX/4XB;->bw:D

    .line 1107778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aP()Ljava/lang/String;

    move-result-object v1

    .line 1107779
    iput-object v1, v0, LX/4XB;->bx:Ljava/lang/String;

    .line 1107780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aQ()Ljava/lang/String;

    move-result-object v1

    .line 1107781
    iput-object v1, v0, LX/4XB;->by:Ljava/lang/String;

    .line 1107782
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aR()I

    move-result v1

    .line 1107783
    iput v1, v0, LX/4XB;->bz:I

    .line 1107784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->br()Ljava/lang/String;

    move-result-object v1

    .line 1107785
    iput-object v1, v0, LX/4XB;->bA:Ljava/lang/String;

    .line 1107786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aS()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 1107787
    iput-object v1, v0, LX/4XB;->bB:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1107788
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107789
    iput-object v1, v0, LX/4XB;->bC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 1107791
    iput-object v1, v0, LX/4XB;->bD:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 1107792
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 1107793
    iput-object v1, v0, LX/4XB;->bE:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 1107794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aW()Z

    move-result v1

    .line 1107795
    iput-boolean v1, v0, LX/4XB;->bF:Z

    .line 1107796
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aX()I

    move-result v1

    .line 1107797
    iput v1, v0, LX/4XB;->bH:I

    .line 1107798
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v1

    .line 1107799
    iput-object v1, v0, LX/4XB;->bI:LX/0Px;

    .line 1107800
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v1

    .line 1107801
    iput-object v1, v0, LX/4XB;->bJ:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 1107802
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bc()I

    move-result v1

    .line 1107803
    iput v1, v0, LX/4XB;->bK:I

    .line 1107804
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v1

    .line 1107805
    iput-object v1, v0, LX/4XB;->bL:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 1107806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v1

    .line 1107807
    iput-object v1, v0, LX/4XB;->bM:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 1107808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1107809
    iput-object v1, v0, LX/4XB;->bN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1107810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v1

    .line 1107811
    iput v1, v0, LX/4XB;->bO:I

    .line 1107812
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x4ed245b

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1107813
    iput-object v1, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1107814
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    return-object v0
.end method
