.class public final enum LX/6Wn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Wn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Wn;

.field public static final enum APPLICATION_RELEASE:LX/6Wn;

.field public static final enum CONNECTION:LX/6Wn;

.field public static final enum EVENT:LX/6Wn;

.field public static final enum EVENT_MEMBER:LX/6Wn;

.field public static final enum FRIEND:LX/6Wn;

.field public static final enum FRIEND_REQUEST:LX/6Wn;

.field public static final enum GEO_REGION:LX/6Wn;

.field public static final enum GROUP_MEMBER:LX/6Wn;

.field public static final enum PAGE:LX/6Wn;

.field public static final enum PLACE:LX/6Wn;

.field public static final enum PLACE_TO_SUGGESTED_ACTIVITY:LX/6Wn;

.field public static final enum PROFILE:LX/6Wn;

.field public static final enum PROFILE_PIC:LX/6Wn;

.field public static final enum SQUARE_PROFILE_PIC:LX/6Wn;

.field public static final enum USER:LX/6Wn;

.field public static final enum USER_SETTINGS:LX/6Wn;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1106496
    new-instance v0, LX/6Wn;

    const-string v1, "APPLICATION_RELEASE"

    const-string v2, "application_release"

    invoke-direct {v0, v1, v4, v2}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->APPLICATION_RELEASE:LX/6Wn;

    .line 1106497
    new-instance v0, LX/6Wn;

    const-string v1, "CONNECTION"

    const-string v2, "connection"

    invoke-direct {v0, v1, v5, v2}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->CONNECTION:LX/6Wn;

    .line 1106498
    new-instance v0, LX/6Wn;

    const-string v1, "EVENT"

    const-string v2, "event"

    invoke-direct {v0, v1, v6, v2}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->EVENT:LX/6Wn;

    .line 1106499
    new-instance v0, LX/6Wn;

    const-string v1, "EVENT_MEMBER"

    const-string v2, "event_member"

    invoke-direct {v0, v1, v7, v2}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->EVENT_MEMBER:LX/6Wn;

    .line 1106500
    new-instance v0, LX/6Wn;

    const-string v1, "FRIEND"

    const-string v2, "friend"

    invoke-direct {v0, v1, v8, v2}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->FRIEND:LX/6Wn;

    .line 1106501
    new-instance v0, LX/6Wn;

    const-string v1, "FRIEND_REQUEST"

    const/4 v2, 0x5

    const-string v3, "friend_request"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->FRIEND_REQUEST:LX/6Wn;

    .line 1106502
    new-instance v0, LX/6Wn;

    const-string v1, "GEO_REGION"

    const/4 v2, 0x6

    const-string v3, "geo_region"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->GEO_REGION:LX/6Wn;

    .line 1106503
    new-instance v0, LX/6Wn;

    const-string v1, "GROUP_MEMBER"

    const/4 v2, 0x7

    const-string v3, "group_member"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->GROUP_MEMBER:LX/6Wn;

    .line 1106504
    new-instance v0, LX/6Wn;

    const-string v1, "PAGE"

    const/16 v2, 0x8

    const-string v3, "page"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->PAGE:LX/6Wn;

    .line 1106505
    new-instance v0, LX/6Wn;

    const-string v1, "PLACE"

    const/16 v2, 0x9

    const-string v3, "place"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->PLACE:LX/6Wn;

    .line 1106506
    new-instance v0, LX/6Wn;

    const-string v1, "PLACE_TO_SUGGESTED_ACTIVITY"

    const/16 v2, 0xa

    const-string v3, "place_to_suggested_activity"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->PLACE_TO_SUGGESTED_ACTIVITY:LX/6Wn;

    .line 1106507
    new-instance v0, LX/6Wn;

    const-string v1, "PROFILE"

    const/16 v2, 0xb

    const-string v3, "profile"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->PROFILE:LX/6Wn;

    .line 1106508
    new-instance v0, LX/6Wn;

    const-string v1, "PROFILE_PIC"

    const/16 v2, 0xc

    const-string v3, "profile_pic"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->PROFILE_PIC:LX/6Wn;

    .line 1106509
    new-instance v0, LX/6Wn;

    const-string v1, "SQUARE_PROFILE_PIC"

    const/16 v2, 0xd

    const-string v3, "square_profile_pic"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->SQUARE_PROFILE_PIC:LX/6Wn;

    .line 1106510
    new-instance v0, LX/6Wn;

    const-string v1, "USER"

    const/16 v2, 0xe

    const-string v3, "user"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->USER:LX/6Wn;

    .line 1106511
    new-instance v0, LX/6Wn;

    const-string v1, "USER_SETTINGS"

    const/16 v2, 0xf

    const-string v3, "user_settings"

    invoke-direct {v0, v1, v2, v3}, LX/6Wn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Wn;->USER_SETTINGS:LX/6Wn;

    .line 1106512
    const/16 v0, 0x10

    new-array v0, v0, [LX/6Wn;

    sget-object v1, LX/6Wn;->APPLICATION_RELEASE:LX/6Wn;

    aput-object v1, v0, v4

    sget-object v1, LX/6Wn;->CONNECTION:LX/6Wn;

    aput-object v1, v0, v5

    sget-object v1, LX/6Wn;->EVENT:LX/6Wn;

    aput-object v1, v0, v6

    sget-object v1, LX/6Wn;->EVENT_MEMBER:LX/6Wn;

    aput-object v1, v0, v7

    sget-object v1, LX/6Wn;->FRIEND:LX/6Wn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6Wn;->FRIEND_REQUEST:LX/6Wn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6Wn;->GEO_REGION:LX/6Wn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6Wn;->GROUP_MEMBER:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6Wn;->PAGE:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6Wn;->PLACE:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6Wn;->PLACE_TO_SUGGESTED_ACTIVITY:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6Wn;->PROFILE:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6Wn;->PROFILE_PIC:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6Wn;->SQUARE_PROFILE_PIC:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6Wn;->USER:LX/6Wn;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6Wn;->USER_SETTINGS:LX/6Wn;

    aput-object v2, v0, v1

    sput-object v0, LX/6Wn;->$VALUES:[LX/6Wn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1106513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1106514
    iput-object p3, p0, LX/6Wn;->mName:Ljava/lang/String;

    .line 1106515
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Wn;
    .locals 1

    .prologue
    .line 1106516
    const-class v0, LX/6Wn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Wn;

    return-object v0
.end method

.method public static values()[LX/6Wn;
    .locals 1

    .prologue
    .line 1106517
    sget-object v0, LX/6Wn;->$VALUES:[LX/6Wn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Wn;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1106518
    iget-object v0, p0, LX/6Wn;->mName:Ljava/lang/String;

    return-object v0
.end method
