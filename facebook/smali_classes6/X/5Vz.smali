.class public final LX/5Vz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 931583
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 931584
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 931585
    :goto_0
    return v1

    .line 931586
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 931587
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 931588
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 931589
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 931590
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 931591
    const-string v7, "attachments"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 931592
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 931593
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 931594
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 931595
    invoke-static {p0, p1}, LX/5Vx;->b(LX/15w;LX/186;)I

    move-result v6

    .line 931596
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 931597
    :cond_2
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 931598
    goto :goto_1

    .line 931599
    :cond_3
    const-string v7, "section_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 931600
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 931601
    :cond_4
    const-string v7, "see_more_button"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 931602
    invoke-static {p0, p1}, LX/5Vy;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 931603
    :cond_5
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 931604
    const/4 v6, 0x0

    .line 931605
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_c

    .line 931606
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 931607
    :goto_3
    move v2, v6

    .line 931608
    goto :goto_1

    .line 931609
    :cond_6
    const-string v7, "tracking_data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 931610
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 931611
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 931612
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 931613
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 931614
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 931615
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 931616
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 931617
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 931618
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 931619
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 931620
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 931621
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 931622
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_a

    if-eqz v7, :cond_a

    .line 931623
    const-string v8, "text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 931624
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 931625
    :cond_b
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 931626
    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 931627
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_c
    move v2, v6

    goto :goto_4
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 931628
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 931629
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 931630
    if-eqz v0, :cond_1

    .line 931631
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931632
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 931633
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 931634
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/5Vx;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 931635
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 931636
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 931637
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 931638
    if-eqz v0, :cond_2

    .line 931639
    const-string v0, "section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931640
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931641
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 931642
    if-eqz v0, :cond_a

    .line 931643
    const-string v1, "see_more_button"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931644
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 931645
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 931646
    if-eqz v1, :cond_8

    .line 931647
    const-string v2, "query"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931648
    const/4 v3, 0x0

    .line 931649
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 931650
    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v2

    .line 931651
    if-eqz v2, :cond_3

    .line 931652
    const-string v2, "list_type"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931653
    invoke-virtual {p0, v1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931654
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 931655
    if-eqz v2, :cond_7

    .line 931656
    const-string v3, "page_info"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931657
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 931658
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 931659
    if-eqz v3, :cond_4

    .line 931660
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931661
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931662
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 931663
    if-eqz v3, :cond_5

    .line 931664
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931665
    invoke-virtual {p2, v3}, LX/0nX;->a(Z)V

    .line 931666
    :cond_5
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 931667
    if-eqz v3, :cond_6

    .line 931668
    const-string v1, "start_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931669
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931670
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 931671
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 931672
    :cond_8
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 931673
    if-eqz v1, :cond_9

    .line 931674
    const-string v2, "title"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931675
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931676
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 931677
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 931678
    if-eqz v0, :cond_c

    .line 931679
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931680
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 931681
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 931682
    if-eqz v1, :cond_b

    .line 931683
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931684
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931685
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 931686
    :cond_c
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 931687
    if-eqz v0, :cond_d

    .line 931688
    const-string v1, "tracking_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 931689
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 931690
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 931691
    return-void
.end method
