.class public final LX/6X0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1106734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1106735
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x20151104

    if-eq v0, v1, :cond_1

    .line 1106736
    :cond_0
    const/4 v0, 0x0

    .line 1106737
    :goto_0
    return-object v0

    .line 1106738
    :cond_1
    new-instance v0, LX/4WV;

    invoke-direct {v0}, LX/4WV;-><init>()V

    .line 1106739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1106740
    iput-object v1, v0, LX/4WV;->b:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1106741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106742
    iput-object v1, v0, LX/4WV;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106743
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106744
    iput-object v1, v0, LX/4WV;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106745
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v1

    .line 1106746
    iput-object v1, v0, LX/4WV;->e:Ljava/lang/String;

    .line 1106747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v1

    .line 1106748
    iput-object v1, v0, LX/4WV;->f:LX/0Px;

    .line 1106749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v1

    .line 1106750
    iput-object v1, v0, LX/4WV;->g:LX/0Px;

    .line 1106751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->v()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v1

    .line 1106752
    iput-object v1, v0, LX/4WV;->h:Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    .line 1106753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106754
    iput-object v1, v0, LX/4WV;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106755
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    .line 1106756
    iput-object v1, v0, LX/4WV;->k:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 1106757
    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;-><init>(LX/4WV;)V

    .line 1106758
    move-object v0, v1

    .line 1106759
    goto :goto_0
.end method
