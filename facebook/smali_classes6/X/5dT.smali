.class public final enum LX/5dT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5dT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5dT;

.field public static final enum NONQUICKCAM:LX/5dT;

.field public static final enum QUICKCAM:LX/5dT;


# instance fields
.field public final apiStringValue:Ljava/lang/String;

.field public final intValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 965687
    new-instance v0, LX/5dT;

    const-string v1, "NONQUICKCAM"

    const-string v2, "FILE_ATTACHMENT"

    invoke-direct {v0, v1, v4, v3, v2}, LX/5dT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dT;->NONQUICKCAM:LX/5dT;

    .line 965688
    new-instance v0, LX/5dT;

    const-string v1, "QUICKCAM"

    const-string v2, "MESSENGER_CAM"

    invoke-direct {v0, v1, v3, v5, v2}, LX/5dT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5dT;->QUICKCAM:LX/5dT;

    .line 965689
    new-array v0, v5, [LX/5dT;

    sget-object v1, LX/5dT;->NONQUICKCAM:LX/5dT;

    aput-object v1, v0, v4

    sget-object v1, LX/5dT;->QUICKCAM:LX/5dT;

    aput-object v1, v0, v3

    sput-object v0, LX/5dT;->$VALUES:[LX/5dT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 965683
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 965684
    iput p3, p0, LX/5dT;->intValue:I

    .line 965685
    iput-object p4, p0, LX/5dT;->apiStringValue:Ljava/lang/String;

    .line 965686
    return-void
.end method

.method public static fromIntVal(I)LX/5dT;
    .locals 1

    .prologue
    .line 965678
    sget-object v0, LX/5dT;->QUICKCAM:LX/5dT;

    iget v0, v0, LX/5dT;->intValue:I

    if-ne p0, v0, :cond_0

    .line 965679
    sget-object v0, LX/5dT;->QUICKCAM:LX/5dT;

    .line 965680
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/5dT;->NONQUICKCAM:LX/5dT;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5dT;
    .locals 1

    .prologue
    .line 965682
    const-class v0, LX/5dT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5dT;

    return-object v0
.end method

.method public static values()[LX/5dT;
    .locals 1

    .prologue
    .line 965681
    sget-object v0, LX/5dT;->$VALUES:[LX/5dT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5dT;

    return-object v0
.end method
