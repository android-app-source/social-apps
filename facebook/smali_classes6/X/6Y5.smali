.class public LX/6Y5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Landroid/text/Spannable;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Landroid/view/View$OnClickListener;

.field public k:Ljava/lang/String;

.field public l:Landroid/view/View$OnClickListener;

.field public m:Ljava/lang/String;

.field public n:Landroid/view/View$OnClickListener;

.field private o:Z

.field public p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field public q:Z

.field private r:Z

.field public s:I

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1109211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109212
    iput v1, p0, LX/6Y5;->s:I

    .line 1109213
    const-string v0, ""

    iput-object v0, p0, LX/6Y5;->t:Ljava/lang/String;

    .line 1109214
    iput-boolean v1, p0, LX/6Y5;->r:Z

    .line 1109215
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)LX/6Y5;
    .locals 1

    .prologue
    .line 1109230
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/6Y5;->r:Z

    .line 1109231
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6Y5;
    .locals 1

    .prologue
    .line 1109232
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/6Y5;->a(Ljava/lang/String;Z)LX/6Y5;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;
    .locals 0

    .prologue
    .line 1109224
    iput-object p1, p0, LX/6Y5;->i:Ljava/lang/String;

    .line 1109225
    iput-object p2, p0, LX/6Y5;->j:Landroid/view/View$OnClickListener;

    .line 1109226
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)LX/6Y5;
    .locals 0

    .prologue
    .line 1109227
    iput-object p1, p0, LX/6Y5;->a:Ljava/lang/String;

    .line 1109228
    iput-boolean p2, p0, LX/6Y5;->b:Z

    .line 1109229
    return-object p0
.end method

.method public final b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;
    .locals 0

    .prologue
    .line 1109221
    iput-object p1, p0, LX/6Y5;->k:Ljava/lang/String;

    .line 1109222
    iput-object p2, p0, LX/6Y5;->l:Landroid/view/View$OnClickListener;

    .line 1109223
    return-object p0
.end method

.method public final c(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;
    .locals 1

    .prologue
    .line 1109217
    iput-object p1, p0, LX/6Y5;->m:Ljava/lang/String;

    .line 1109218
    iput-object p2, p0, LX/6Y5;->n:Landroid/view/View$OnClickListener;

    .line 1109219
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Y5;->o:Z

    .line 1109220
    return-object p0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1109216
    iget-boolean v0, p0, LX/6Y5;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
