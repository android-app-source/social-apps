.class public LX/6F6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6F6;


# instance fields
.field public final a:LX/0W3;

.field public b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

.field public c:LX/6Ez;

.field public d:LX/6Ew;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1067002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067003
    iput-object p1, p0, LX/6F6;->a:LX/0W3;

    .line 1067004
    return-void
.end method

.method public static a(LX/0QB;)LX/6F6;
    .locals 4

    .prologue
    .line 1067005
    sget-object v0, LX/6F6;->e:LX/6F6;

    if-nez v0, :cond_1

    .line 1067006
    const-class v1, LX/6F6;

    monitor-enter v1

    .line 1067007
    :try_start_0
    sget-object v0, LX/6F6;->e:LX/6F6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1067008
    if-eqz v2, :cond_0

    .line 1067009
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1067010
    new-instance p0, LX/6F6;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/6F6;-><init>(LX/0W3;)V

    .line 1067011
    move-object v0, p0

    .line 1067012
    sput-object v0, LX/6F6;->e:LX/6F6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1067013
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1067014
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1067015
    :cond_1
    sget-object v0, LX/6F6;->e:LX/6F6;

    return-object v0

    .line 1067016
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1067017
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Cy;)V
    .locals 2

    .prologue
    .line 1067018
    iget-object v0, p0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067019
    return-void
.end method

.method public final a(LX/6Ew;Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 1067020
    iget-object v0, p0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    const-string v1, "handleShippingAddress"

    invoke-virtual {v0, v1}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1067021
    :goto_0
    return-void

    .line 1067022
    :cond_0
    iput-object p1, p0, LX/6F6;->d:LX/6Ew;

    .line 1067023
    iget-object v0, p0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    iget-object v1, p0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "shipping"

    .line 1067024
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1067025
    :try_start_0
    const-string v3, "shippingAddress"

    invoke-virtual {v4, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1067026
    :goto_1
    move-object v3, v4

    .line 1067027
    invoke-static {v1, v2, v3}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1067028
    :catch_0
    move-exception v3

    .line 1067029
    const-string v5, "paymentsCheckout"

    const-string p0, "Exception serializing return params!"

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v5, v3, p0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
