.class public LX/6A3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "LX/4WJ;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/20i;

.field private final b:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/20i;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)V
    .locals 1
    .param p2    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058509
    iput-object p1, p0, LX/6A3;->a:LX/20i;

    .line 1058510
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    iput-object v0, p0, LX/6A3;->b:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    .line 1058511
    invoke-virtual {p2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6A3;->c:Ljava/lang/String;

    .line 1058512
    invoke-virtual {p2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6A3;->d:Ljava/lang/String;

    .line 1058513
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058514
    iget-object v0, p0, LX/6A3;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 6

    .prologue
    .line 1058515
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    check-cast p2, LX/4WJ;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1058516
    iget-object v2, p0, LX/6A3;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1058517
    :goto_0
    return-void

    .line 1058518
    :cond_0
    invoke-static {p1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1058519
    iget-object v3, p0, LX/6A3;->b:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    invoke-static {v3}, LX/20j;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)I

    move-result v3

    .line 1058520
    if-ne v3, v0, :cond_2

    :goto_1
    invoke-virtual {p2, v0}, LX/4WJ;->a(Z)V

    .line 1058521
    invoke-static {v3}, LX/20j;->a(I)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    .line 1058522
    iget-object v4, p2, LX/40T;->a:LX/4VK;

    const-string v5, "viewer_feedback_reaction"

    invoke-virtual {v4, v5, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058523
    iget-object v0, p0, LX/6A3;->b:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->m()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6A3;->b:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->m()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;->a()I

    move-result v1

    :cond_1
    iget-object v0, p0, LX/6A3;->a:LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {p1, v2, v3, v1, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;IIILcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    .line 1058524
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v4, "reactors"

    invoke-virtual {v1, v4, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058525
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-static {v0, v2, v3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;II)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    .line 1058526
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "top_reactions"

    invoke-virtual {v1, v2, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058527
    iget-object v0, p2, LX/40T;->a:LX/4VK;

    .line 1058528
    invoke-static {v0}, LX/4VK;->d(LX/4VK;)LX/16f;

    move-result-object v1

    .line 1058529
    instance-of p2, v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p2}, LX/0PB;->checkState(Z)V

    .line 1058530
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 p2, 0x0

    invoke-static {v1, p2}, LX/3dS;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;)V

    .line 1058531
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1058532
    goto :goto_1
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058533
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058534
    const-string v0, "ReactionsMutatingVisitor"

    return-object v0
.end method
