.class public final LX/6YT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6YQ;


# instance fields
.field private final a:LX/0yH;


# direct methods
.method public constructor <init>(LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1110208
    iput-object p1, p0, LX/6YT;->a:LX/0yH;

    .line 1110209
    return-void
.end method


# virtual methods
.method public final a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;
    .locals 6

    .prologue
    .line 1110210
    new-instance v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    invoke-direct {v1}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>()V

    .line 1110211
    iget-object v0, p1, LX/3J6;->b:Ljava/lang/String;

    .line 1110212
    iput-object v0, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    .line 1110213
    iget-object v0, p1, LX/3J6;->c:Ljava/lang/String;

    .line 1110214
    iput-object v0, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    .line 1110215
    sget-object v2, LX/6YN;->FETCH_UPSELL:LX/6YN;

    .line 1110216
    iget-object v0, p0, LX/6YT;->a:LX/0yH;

    sget-object v3, LX/0yY;->ZERO_BALANCE_WEBVIEW:LX/0yY;

    invoke-virtual {v0, v3}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1110217
    sget-object v2, LX/6YN;->ZERO_BALANCE_SPINNER:LX/6YN;

    .line 1110218
    :cond_0
    const/4 v3, 0x0

    sget-object v5, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    move-object v0, p3

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/0yY;Ljava/lang/Object;LX/6YN;ILjava/lang/Object;LX/4g1;)Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    move-result-object v0

    return-object v0
.end method
