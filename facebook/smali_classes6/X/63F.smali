.class public LX/63F;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1043006
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 1043007
    iput p1, p0, LX/63F;->b:I

    .line 1043008
    const/4 v0, 0x0

    iput v0, p0, LX/63F;->c:I

    .line 1043009
    iput p2, p0, LX/63F;->a:I

    .line 1043010
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 8

    .prologue
    .line 1043011
    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v6

    move-object v1, p0

    move-object/from16 v2, p9

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, LX/63F;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v1

    .line 1043012
    new-instance v2, Landroid/graphics/RectF;

    int-to-float v3, p6

    int-to-float v1, v1

    add-float/2addr v1, p5

    move/from16 v0, p8

    int-to-float v4, v0

    invoke-direct {v2, p5, v3, v1, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1043013
    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    .line 1043014
    iget v3, p0, LX/63F;->a:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1043015
    iget v3, p0, LX/63F;->b:I

    int-to-float v3, v3

    iget v4, p0, LX/63F;->b:I

    int-to-float v4, v4

    move-object/from16 v0, p9

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1043016
    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1043017
    iget v1, p0, LX/63F;->c:I

    int-to-float v1, v1

    add-float v5, p5, v1

    int-to-float v6, p7

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 1043018
    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    .line 1043019
    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, LX/63F;->c:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method
