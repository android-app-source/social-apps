.class public LX/6J6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ia;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/6Ia;

.field public final c:LX/6J2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075335
    const-class v0, LX/6J6;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6J6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Ia;)V
    .locals 2

    .prologue
    .line 1075331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075332
    iput-object p1, p0, LX/6J6;->b:LX/6Ia;

    .line 1075333
    new-instance v0, LX/6J2;

    iget-object v1, p0, LX/6J6;->b:LX/6Ia;

    invoke-direct {v0, v1}, LX/6J2;-><init>(LX/6Ia;)V

    iput-object v0, p0, LX/6J6;->c:LX/6J2;

    .line 1075334
    return-void
.end method


# virtual methods
.method public final a()LX/6IP;
    .locals 1

    .prologue
    .line 1075330
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    return-object v0
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 1075328
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1, p2}, LX/6Ia;->a(FF)V

    .line 1075329
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1075326
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1}, LX/6Ia;->a(I)V

    .line 1075327
    return-void
.end method

.method public final a(ILX/6Jd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/cameracore/camerasdk/common/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1075324
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1, p2}, LX/6Ia;->a(ILX/6Jd;)V

    .line 1075325
    return-void
.end method

.method public final a(LX/6Ik;)V
    .locals 2

    .prologue
    .line 1075312
    iget-object v0, p0, LX/6J6;->c:LX/6J2;

    new-instance v1, LX/6J3;

    invoke-direct {v1, p0, p1}, LX/6J3;-><init>(LX/6J6;LX/6Ik;)V

    .line 1075313
    invoke-static {}, LX/6J2;->d()V

    .line 1075314
    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->OPENED:LX/6J0;

    if-eq p0, p1, :cond_0

    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->OPEN_IN_PROGRESS:LX/6J0;

    if-eq p0, p1, :cond_0

    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->PREVIEW:LX/6J0;

    if-eq p0, p1, :cond_0

    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    if-ne p0, p1, :cond_3

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move p0, p0

    .line 1075315
    if-eqz p0, :cond_1

    .line 1075316
    invoke-static {v0}, LX/6J2;->h(LX/6J2;)V

    .line 1075317
    invoke-interface {v1}, LX/6Ik;->a()V

    .line 1075318
    sget-object p0, LX/6J1;->NONE:LX/6J1;

    iput-object p0, v0, LX/6J2;->e:LX/6J1;

    .line 1075319
    :goto_1
    return-void

    .line 1075320
    :cond_1
    invoke-static {v0}, LX/6J2;->e(LX/6J2;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1075321
    iput-object v1, v0, LX/6J2;->c:LX/6Ik;

    .line 1075322
    sget-object p0, LX/6J1;->OPEN:LX/6J1;

    iput-object p0, v0, LX/6J2;->e:LX/6J1;

    goto :goto_1

    .line 1075323
    :cond_2
    invoke-static {v0, v1}, LX/6J2;->c(LX/6J2;LX/6Ik;)V

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(LX/6Ik;LX/6JC;)V
    .locals 2

    .prologue
    .line 1075309
    iget-object v0, p0, LX/6J6;->c:LX/6J2;

    sget-object v1, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    invoke-virtual {v0, v1}, LX/6J2;->a(LX/6J0;)V

    .line 1075310
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    new-instance v1, LX/6J5;

    invoke-direct {v1, p0, p1}, LX/6J5;-><init>(LX/6J6;LX/6Ik;)V

    invoke-interface {v0, v1, p2}, LX/6Ia;->a(LX/6Ik;LX/6JC;)V

    .line 1075311
    return-void
.end method

.method public final a(LX/6JB;)V
    .locals 1

    .prologue
    .line 1075307
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1}, LX/6Ia;->a(LX/6JB;)V

    .line 1075308
    return-void
.end method

.method public final a(LX/6Jj;I)V
    .locals 1

    .prologue
    .line 1075305
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1, p2}, LX/6Ia;->a(LX/6Jj;I)V

    .line 1075306
    return-void
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 1

    .prologue
    .line 1075303
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0, p1, p2}, LX/6Ia;->a(Ljava/io/File;LX/6JG;)V

    .line 1075304
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1075276
    sget-object v0, LX/6JE;->a:LX/6JE;

    invoke-virtual {p0, v0}, LX/6J6;->b(LX/6Ik;)V

    .line 1075277
    return-void
.end method

.method public final b(LX/6Ik;)V
    .locals 2

    .prologue
    .line 1075291
    iget-object v0, p0, LX/6J6;->c:LX/6J2;

    new-instance v1, LX/6J4;

    invoke-direct {v1, p0, p1}, LX/6J4;-><init>(LX/6J6;LX/6Ik;)V

    .line 1075292
    invoke-static {}, LX/6J2;->d()V

    .line 1075293
    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->CLOSED:LX/6J0;

    if-eq p0, p1, :cond_0

    iget-object p0, v0, LX/6J2;->a:LX/6J0;

    sget-object p1, LX/6J0;->CLOSE_IN_PROGRESS:LX/6J0;

    if-ne p0, p1, :cond_3

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move p0, p0

    .line 1075294
    if-eqz p0, :cond_1

    .line 1075295
    invoke-static {v0}, LX/6J2;->h(LX/6J2;)V

    .line 1075296
    invoke-interface {v1}, LX/6Ik;->a()V

    .line 1075297
    sget-object p0, LX/6J1;->NONE:LX/6J1;

    iput-object p0, v0, LX/6J2;->e:LX/6J1;

    .line 1075298
    :goto_1
    return-void

    .line 1075299
    :cond_1
    invoke-static {v0}, LX/6J2;->e(LX/6J2;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1075300
    iput-object v1, v0, LX/6J2;->b:LX/6Ik;

    .line 1075301
    sget-object p0, LX/6J1;->CLOSE:LX/6J1;

    iput-object p0, v0, LX/6J2;->e:LX/6J1;

    goto :goto_1

    .line 1075302
    :cond_2
    invoke-static {v0, v1}, LX/6J2;->d(LX/6J2;LX/6Ik;)V

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1075288
    iget-object v0, p0, LX/6J6;->c:LX/6J2;

    .line 1075289
    iget-object v1, v0, LX/6J2;->a:LX/6J0;

    move-object v0, v1

    .line 1075290
    sget-object v1, LX/6J0;->OPENED:LX/6J0;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/6J0;->PREVIEW:LX/6J0;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/6JF;
    .locals 1

    .prologue
    .line 1075287
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->d()LX/6JF;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1075285
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->e()V

    .line 1075286
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1075284
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->f()Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1075283
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1075282
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->h()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1075281
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->i()I

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1075279
    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->j()V

    .line 1075280
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1075278
    invoke-virtual {p0}, LX/6J6;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6J6;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
