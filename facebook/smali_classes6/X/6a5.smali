.class public LX/6a5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zm;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zm;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111908
    iput-object p1, p0, LX/6a5;->a:LX/0Zm;

    .line 1111909
    iput-object p2, p0, LX/6a5;->b:LX/0Zb;

    .line 1111910
    return-void
.end method

.method public static d(LX/6a5;Ljava/lang/String;)LX/0oG;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1111911
    iget-object v1, p0, LX/6a5;->a:LX/0Zm;

    invoke-virtual {v1, p1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1111912
    :cond_0
    :goto_0
    return-object v0

    .line 1111913
    :cond_1
    iget-object v1, p0, LX/6a5;->b:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1111914
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1111915
    const-string v0, "oxygen_map"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-object v0, v1

    .line 1111916
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1111917
    const-string v0, "generic_map_ls_upsell_launched"

    invoke-static {p0, v0}, LX/6a5;->d(LX/6a5;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 1111918
    if-nez v0, :cond_0

    .line 1111919
    :goto_0
    return-void

    .line 1111920
    :cond_0
    const-string v1, "mechanism"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111921
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1111922
    const-string v0, "generic_map_ls_upsell_result"

    invoke-static {p0, v0}, LX/6a5;->d(LX/6a5;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 1111923
    if-nez v0, :cond_0

    .line 1111924
    :goto_0
    return-void

    .line 1111925
    :cond_0
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111926
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1111927
    const-string v0, "generic_map_runtime_permission_result"

    invoke-static {p0, v0}, LX/6a5;->d(LX/6a5;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 1111928
    if-nez v0, :cond_0

    .line 1111929
    :goto_0
    return-void

    .line 1111930
    :cond_0
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111931
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
