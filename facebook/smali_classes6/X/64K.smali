.class public final LX/64K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/64L;


# direct methods
.method public constructor <init>(LX/64L;)V
    .locals 0

    .prologue
    .line 1044459
    iput-object p1, p0, LX/64K;->a:LX/64L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1044460
    iget-object v0, p0, LX/64K;->a:LX/64L;

    .line 1044461
    new-instance v1, Ljava/util/HashSet;

    iget-object p0, v0, LX/64L;->c:Ljava/util/Set;

    invoke-direct {v1, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/64J;

    .line 1044462
    invoke-interface {v1, p1}, LX/64J;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1044463
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1044464
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;

    .line 1044465
    iget-object v0, p0, LX/64K;->a:LX/64L;

    .line 1044466
    new-instance v1, Ljava/util/HashSet;

    iget-object p0, v0, LX/64L;->c:Ljava/util/Set;

    invoke-direct {v1, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/64J;

    .line 1044467
    invoke-interface {v1, p1}, LX/64J;->a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;)V

    goto :goto_0

    .line 1044468
    :cond_0
    return-void
.end method
