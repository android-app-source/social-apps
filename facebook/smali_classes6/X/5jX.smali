.class public final LX/5jX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 987885
    const/4 v12, 0x0

    .line 987886
    const/4 v11, 0x0

    .line 987887
    const/4 v10, 0x0

    .line 987888
    const/4 v9, 0x0

    .line 987889
    const/4 v8, 0x0

    .line 987890
    const/4 v7, 0x0

    .line 987891
    const/4 v6, 0x0

    .line 987892
    const/4 v5, 0x0

    .line 987893
    const/4 v4, 0x0

    .line 987894
    const/4 v3, 0x0

    .line 987895
    const/4 v2, 0x0

    .line 987896
    const/4 v1, 0x0

    .line 987897
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 987898
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 987899
    const/4 v1, 0x0

    .line 987900
    :goto_0
    return v1

    .line 987901
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 987902
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_8

    .line 987903
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 987904
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 987905
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 987906
    const-string v14, "animation_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 987907
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto :goto_1

    .line 987908
    :cond_2
    const-string v14, "asset_image"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 987909
    invoke-static/range {p0 .. p1}, LX/5jd;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 987910
    :cond_3
    const-string v14, "col_of_sprites"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 987911
    const/4 v5, 0x1

    .line 987912
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 987913
    :cond_4
    const-string v14, "frame_rate"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 987914
    const/4 v4, 0x1

    .line 987915
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 987916
    :cond_5
    const-string v14, "num_of_sprites"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 987917
    const/4 v3, 0x1

    .line 987918
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 987919
    :cond_6
    const-string v14, "play_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 987920
    const/4 v2, 0x1

    .line 987921
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 987922
    :cond_7
    const-string v14, "row_of_sprites"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 987923
    const/4 v1, 0x1

    .line 987924
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 987925
    :cond_8
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 987926
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 987927
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 987928
    if-eqz v5, :cond_9

    .line 987929
    const/4 v5, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10, v11}, LX/186;->a(III)V

    .line 987930
    :cond_9
    if-eqz v4, :cond_a

    .line 987931
    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9, v5}, LX/186;->a(III)V

    .line 987932
    :cond_a
    if-eqz v3, :cond_b

    .line 987933
    const/4 v3, 0x4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v4}, LX/186;->a(III)V

    .line 987934
    :cond_b
    if-eqz v2, :cond_c

    .line 987935
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 987936
    :cond_c
    if-eqz v1, :cond_d

    .line 987937
    const/4 v1, 0x6

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 987938
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 987939
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 987940
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 987941
    if-eqz v0, :cond_0

    .line 987942
    const-string v0, "animation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987943
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 987944
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 987945
    if-eqz v0, :cond_1

    .line 987946
    const-string v1, "asset_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987947
    invoke-static {p0, v0, p2}, LX/5jd;->a(LX/15i;ILX/0nX;)V

    .line 987948
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 987949
    if-eqz v0, :cond_2

    .line 987950
    const-string v1, "col_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987951
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 987952
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 987953
    if-eqz v0, :cond_3

    .line 987954
    const-string v1, "frame_rate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987955
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 987956
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 987957
    if-eqz v0, :cond_4

    .line 987958
    const-string v1, "num_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987959
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 987960
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 987961
    if-eqz v0, :cond_5

    .line 987962
    const-string v1, "play_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987963
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 987964
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 987965
    if-eqz v0, :cond_6

    .line 987966
    const-string v1, "row_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 987967
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 987968
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 987969
    return-void
.end method
