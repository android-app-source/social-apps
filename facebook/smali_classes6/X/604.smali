.class public LX/604;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:Lcom/facebook/uicontrib/datepicker/Period;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/facebook/uicontrib/datepicker/Period;)V
    .locals 1

    .prologue
    .line 1036397
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1036398
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1036399
    sput-object v0, LX/604;->a:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    iget-object p2, p2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {p2}, Ljava/text/DateFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;

    move-result-object p2

    invoke-virtual {p2}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 1036400
    iput p3, p0, LX/604;->b:I

    .line 1036401
    iput-object p4, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036402
    invoke-static {p0}, LX/604;->b(LX/604;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/604;->a(LX/604;Ljava/util/List;)V

    .line 1036403
    return-void
.end method

.method public static a(LX/604;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036404
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1036405
    invoke-virtual {p0, v0}, LX/604;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 1036406
    :cond_0
    return-void
.end method

.method public static b(LX/604;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1036407
    iget v0, p0, LX/604;->b:I

    if-nez v0, :cond_1

    .line 1036408
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1036409
    :cond_0
    :goto_0
    return-object v0

    .line 1036410
    :cond_1
    sget-object v0, LX/604;->a:Ljava/util/List;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1036411
    iget v1, p0, LX/604;->b:I

    iget-object v2, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v2

    if-lt v1, v2, :cond_2

    iget v1, p0, LX/604;->b:I

    iget-object v2, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 1036412
    :cond_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 1036413
    :cond_3
    iget v1, p0, LX/604;->b:I

    iget-object v2, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 1036414
    const/4 v1, 0x0

    iget-object v2, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1036415
    :cond_4
    iget v1, p0, LX/604;->b:I

    iget-object v2, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1036416
    iget-object v1, p0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)I
    .locals 2

    .prologue
    .line 1036417
    sget-object v0, LX/604;->a:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/604;->getPosition(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
