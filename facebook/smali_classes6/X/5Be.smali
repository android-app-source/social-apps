.class public final LX/5Be;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 864919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 864920
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 864921
    :goto_0
    return v1

    .line 864922
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 864923
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 864924
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 864925
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 864926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 864927
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 864928
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 864929
    :cond_3
    const-string v8, "friendship_status"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 864930
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 864931
    :cond_4
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 864932
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 864933
    :cond_5
    const-string v8, "mutual_friends"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 864934
    invoke-static {p0, p1}, LX/57s;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 864935
    :cond_6
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 864936
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 864937
    :cond_7
    const-string v8, "profile_picture"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 864938
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 864939
    :cond_8
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 864940
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 864941
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 864942
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 864943
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 864944
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 864945
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 864946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 864947
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 864948
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 864949
    if-eqz v0, :cond_0

    .line 864950
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864951
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 864952
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 864953
    if-eqz v0, :cond_1

    .line 864954
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864955
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864956
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 864957
    if-eqz v0, :cond_2

    .line 864958
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864959
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864960
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 864961
    if-eqz v0, :cond_3

    .line 864962
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864963
    invoke-static {p0, v0, p2}, LX/57s;->a(LX/15i;ILX/0nX;)V

    .line 864964
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 864965
    if-eqz v0, :cond_4

    .line 864966
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864967
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 864968
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 864969
    if-eqz v0, :cond_5

    .line 864970
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 864971
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 864972
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 864973
    return-void
.end method
