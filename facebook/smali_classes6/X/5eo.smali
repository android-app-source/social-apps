.class public final LX/5eo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 969888
    const/4 v10, 0x0

    .line 969889
    const/4 v9, 0x0

    .line 969890
    const/4 v8, 0x0

    .line 969891
    const/4 v7, 0x0

    .line 969892
    const/4 v6, 0x0

    .line 969893
    const/4 v5, 0x0

    .line 969894
    const/4 v4, 0x0

    .line 969895
    const/4 v3, 0x0

    .line 969896
    const/4 v2, 0x0

    .line 969897
    const/4 v1, 0x0

    .line 969898
    const/4 v0, 0x0

    .line 969899
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 969900
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 969901
    const/4 v0, 0x0

    .line 969902
    :goto_0
    return v0

    .line 969903
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 969904
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 969905
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 969906
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 969907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 969908
    const-string v12, "can_viewer_react"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 969909
    const/4 v1, 0x1

    .line 969910
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 969911
    :cond_2
    const-string v12, "important_reactors"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 969912
    invoke-static {p0, p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 969913
    :cond_3
    const-string v12, "likers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 969914
    invoke-static {p0, p1}, LX/5em;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 969915
    :cond_4
    const-string v12, "reactors"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 969916
    invoke-static {p0, p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 969917
    :cond_5
    const-string v12, "supported_reactions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 969918
    invoke-static {p0, p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 969919
    :cond_6
    const-string v12, "top_level_comments"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 969920
    invoke-static {p0, p1}, LX/5en;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 969921
    :cond_7
    const-string v12, "top_reactions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 969922
    invoke-static {p0, p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 969923
    :cond_8
    const-string v12, "viewer_acts_as_person"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 969924
    invoke-static {p0, p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 969925
    :cond_9
    const-string v12, "viewer_feedback_reaction_key"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 969926
    const/4 v0, 0x1

    .line 969927
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    goto/16 :goto_1

    .line 969928
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 969929
    if-eqz v1, :cond_b

    .line 969930
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 969931
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 969932
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 969933
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 969934
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 969935
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 969936
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 969937
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 969938
    if-eqz v0, :cond_c

    .line 969939
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 969940
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 969941
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 969942
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 969943
    if-eqz v0, :cond_0

    .line 969944
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969945
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 969946
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969947
    if-eqz v0, :cond_1

    .line 969948
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969949
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 969950
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969951
    if-eqz v0, :cond_2

    .line 969952
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969953
    invoke-static {p0, v0, p2}, LX/5em;->a(LX/15i;ILX/0nX;)V

    .line 969954
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969955
    if-eqz v0, :cond_3

    .line 969956
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969957
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 969958
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969959
    if-eqz v0, :cond_4

    .line 969960
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969961
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 969962
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969963
    if-eqz v0, :cond_5

    .line 969964
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969965
    invoke-static {p0, v0, p2}, LX/5en;->a(LX/15i;ILX/0nX;)V

    .line 969966
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969967
    if-eqz v0, :cond_6

    .line 969968
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969969
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 969970
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 969971
    if-eqz v0, :cond_7

    .line 969972
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969973
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 969974
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 969975
    if-eqz v0, :cond_8

    .line 969976
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 969977
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 969978
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 969979
    return-void
.end method
