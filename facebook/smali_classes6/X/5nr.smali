.class public final enum LX/5nr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nr;

.field public static final enum ACCEPTED:LX/5nr;

.field public static final enum CLOSED:LX/5nr;

.field public static final enum DECLINED:LX/5nr;

.field public static final enum DISMISSED:LX/5nr;

.field public static final enum EXPOSED:LX/5nr;

.field public static final enum HOLDOUT:LX/5nr;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004749
    new-instance v0, LX/5nr;

    const-string v1, "EXPOSED"

    const-string v2, "client_suggested"

    invoke-direct {v0, v1, v4, v2}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->EXPOSED:LX/5nr;

    .line 1004750
    new-instance v0, LX/5nr;

    const-string v1, "ACCEPTED"

    const-string v2, "moved"

    invoke-direct {v0, v1, v5, v2}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->ACCEPTED:LX/5nr;

    .line 1004751
    new-instance v0, LX/5nr;

    const-string v1, "DECLINED"

    const-string v2, "declined"

    invoke-direct {v0, v1, v6, v2}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->DECLINED:LX/5nr;

    .line 1004752
    new-instance v0, LX/5nr;

    const-string v1, "CLOSED"

    const-string v2, "closed"

    invoke-direct {v0, v1, v7, v2}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->CLOSED:LX/5nr;

    .line 1004753
    new-instance v0, LX/5nr;

    const-string v1, "DISMISSED"

    const-string v2, "dismissed"

    invoke-direct {v0, v1, v8, v2}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->DISMISSED:LX/5nr;

    .line 1004754
    new-instance v0, LX/5nr;

    const-string v1, "HOLDOUT"

    const/4 v2, 0x5

    const-string v3, "holdout"

    invoke-direct {v0, v1, v2, v3}, LX/5nr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nr;->HOLDOUT:LX/5nr;

    .line 1004755
    const/4 v0, 0x6

    new-array v0, v0, [LX/5nr;

    sget-object v1, LX/5nr;->EXPOSED:LX/5nr;

    aput-object v1, v0, v4

    sget-object v1, LX/5nr;->ACCEPTED:LX/5nr;

    aput-object v1, v0, v5

    sget-object v1, LX/5nr;->DECLINED:LX/5nr;

    aput-object v1, v0, v6

    sget-object v1, LX/5nr;->CLOSED:LX/5nr;

    aput-object v1, v0, v7

    sget-object v1, LX/5nr;->DISMISSED:LX/5nr;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nr;->HOLDOUT:LX/5nr;

    aput-object v2, v0, v1

    sput-object v0, LX/5nr;->$VALUES:[LX/5nr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004746
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004747
    iput-object p3, p0, LX/5nr;->eventName:Ljava/lang/String;

    .line 1004748
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nr;
    .locals 1

    .prologue
    .line 1004745
    const-class v0, LX/5nr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nr;

    return-object v0
.end method

.method public static values()[LX/5nr;
    .locals 1

    .prologue
    .line 1004744
    sget-object v0, LX/5nr;->$VALUES:[LX/5nr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nr;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004743
    iget-object v0, p0, LX/5nr;->eventName:Ljava/lang/String;

    return-object v0
.end method
