.class public final LX/5Hy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 892179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 892180
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892181
    :goto_0
    return v1

    .line 892182
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892183
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 892184
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 892185
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 892186
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 892187
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 892188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 892189
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 892190
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 892191
    const/4 v3, 0x0

    .line 892192
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 892193
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892194
    :goto_3
    move v2, v3

    .line 892195
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 892196
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 892197
    goto :goto_1

    .line 892198
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 892199
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 892200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 892201
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892202
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 892203
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 892204
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 892205
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 892206
    const-string v5, "recommending_comments"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 892207
    const/4 v4, 0x0

    .line 892208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_d

    .line 892209
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892210
    :goto_5
    move v2, v4

    .line 892211
    goto :goto_4

    .line 892212
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 892213
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 892214
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    .line 892215
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892216
    :cond_a
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_c

    .line 892217
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 892218
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 892219
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 892220
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 892221
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 892222
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_b

    .line 892223
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_b

    .line 892224
    invoke-static {p0, p1}, LX/5Hx;->b(LX/15w;LX/186;)I

    move-result v5

    .line 892225
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 892226
    :cond_b
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 892227
    goto :goto_6

    .line 892228
    :cond_c
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 892229
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 892230
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_d
    move v2, v4

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 892231
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 892232
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 892233
    if-eqz v0, :cond_4

    .line 892234
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 892235
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 892236
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 892237
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 892238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 892239
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 892240
    if-eqz v3, :cond_2

    .line 892241
    const-string p1, "recommending_comments"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 892242
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 892243
    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1}, LX/15i;->g(II)I

    move-result p1

    .line 892244
    if-eqz p1, :cond_1

    .line 892245
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 892246
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 892247
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 892248
    invoke-virtual {p0, p1, v2}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/5Hx;->a(LX/15i;ILX/0nX;)V

    .line 892249
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 892250
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 892251
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 892252
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 892253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 892254
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 892255
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 892256
    return-void
.end method
