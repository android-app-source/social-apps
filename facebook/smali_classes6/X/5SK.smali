.class public final enum LX/5SK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5SK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5SK;

.field public static final enum CROP:LX/5SK;

.field public static final enum TRIM:LX/5SK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 917819
    new-instance v0, LX/5SK;

    const-string v1, "TRIM"

    invoke-direct {v0, v1, v2}, LX/5SK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SK;->TRIM:LX/5SK;

    .line 917820
    new-instance v0, LX/5SK;

    const-string v1, "CROP"

    invoke-direct {v0, v1, v3}, LX/5SK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SK;->CROP:LX/5SK;

    .line 917821
    const/4 v0, 0x2

    new-array v0, v0, [LX/5SK;

    sget-object v1, LX/5SK;->TRIM:LX/5SK;

    aput-object v1, v0, v2

    sget-object v1, LX/5SK;->CROP:LX/5SK;

    aput-object v1, v0, v3

    sput-object v0, LX/5SK;->$VALUES:[LX/5SK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 917818
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5SK;
    .locals 1

    .prologue
    .line 917816
    const-class v0, LX/5SK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SK;

    return-object v0
.end method

.method public static values()[LX/5SK;
    .locals 1

    .prologue
    .line 917817
    sget-object v0, LX/5SK;->$VALUES:[LX/5SK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5SK;

    return-object v0
.end method
