.class public LX/5pX;
.super Landroid/content/ContextWrapper;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/5pQ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/5on;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/341;

.field private d:Lcom/facebook/react/bridge/CatalystInstance;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/view/LayoutInflater;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/react/bridge/queue/MessageQueueThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0o1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1008239
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 1008240
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 1008241
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/5pX;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 1008242
    sget-object v0, LX/341;->BEFORE_CREATE:LX/341;

    iput-object v0, p0, LX/5pX;->c:LX/341;

    .line 1008243
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/react/bridge/CatalystInstance;
    .locals 1

    .prologue
    .line 1008244
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/CatalystInstance;

    return-object v0
.end method

.method public final a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">(",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008245
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-nez v0, :cond_0

    .line 1008246
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to access a JS module before the React instance was fully set up. Calls to ReactContext#getJSModule should only happen once initialize() has been called on your native module."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008247
    :cond_0
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/bridge/CatalystInstance;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008248
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-nez v0, :cond_0

    .line 1008249
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to access a JS module before the React instance was fully set up. Calls to ReactContext#getJSModule should only happen once initialize() has been called on your native module."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008250
    :cond_0
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/CatalystInstance;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5on;)V
    .locals 1

    .prologue
    .line 1008251
    iget-object v0, p0, LX/5pX;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 1008252
    return-void
.end method

.method public a(LX/5pQ;)V
    .locals 2

    .prologue
    .line 1008272
    iget-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 1008273
    invoke-virtual {p0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008274
    sget-object v0, LX/5pa;->a:[I

    iget-object v1, p0, LX/5pX;->c:LX/341;

    invoke-virtual {v1}, LX/341;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1008275
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unhandled lifecycle state."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008276
    :pswitch_0
    new-instance v0, Lcom/facebook/react/bridge/ReactContext$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/react/bridge/ReactContext$1;-><init>(LX/5pX;LX/5pQ;)V

    invoke-virtual {p0, v0}, LX/5pX;->a(Ljava/lang/Runnable;)V

    .line 1008277
    :cond_0
    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1008253
    invoke-static {}, LX/5pe;->b()V

    .line 1008254
    sget-object v0, LX/341;->RESUMED:LX/341;

    iput-object v0, p0, LX/5pX;->c:LX/341;

    .line 1008255
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    .line 1008256
    sget-object v0, LX/341;->RESUMED:LX/341;

    iput-object v0, p0, LX/5pX;->c:LX/341;

    .line 1008257
    iget-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pQ;

    .line 1008258
    invoke-interface {v0}, LX/5pQ;->bM_()V

    goto :goto_0

    .line 1008259
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1008260
    iget-object v0, p0, LX/5pX;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5on;

    .line 1008261
    invoke-interface {v0, p2, p3, p4}, LX/5on;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 1008262
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/react/bridge/CatalystInstance;)V
    .locals 2

    .prologue
    .line 1008214
    if-nez p1, :cond_0

    .line 1008215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CatalystInstance cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008216
    :cond_0
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-eqz v0, :cond_1

    .line 1008217
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ReactContext has been already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008218
    :cond_1
    iput-object p1, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    .line 1008219
    invoke-interface {p1}, Lcom/facebook/react/bridge/CatalystInstance;->e()LX/5pk;

    move-result-object v0

    .line 1008220
    invoke-interface {v0}, LX/5pk;->a()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v1

    iput-object v1, p0, LX/5pX;->f:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    .line 1008221
    invoke-interface {v0}, LX/5pk;->b()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v1

    iput-object v1, p0, LX/5pX;->g:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    .line 1008222
    invoke-interface {v0}, LX/5pk;->c()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v0

    iput-object v0, p0, LX/5pX;->h:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    .line 1008223
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1008263
    iget-object v0, p0, LX/5pX;->f:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->runOnQueue(Ljava/lang/Runnable;)V

    .line 1008264
    return-void
.end method

.method public final a(Ljava/lang/RuntimeException;)V
    .locals 1

    .prologue
    .line 1008265
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0}, Lcom/facebook/react/bridge/CatalystInstance;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/5pX;->i:LX/0o1;

    if-eqz v0, :cond_0

    .line 1008266
    iget-object v0, p0, LX/5pX;->i:LX/0o1;

    invoke-interface {v0, p1}, LX/0o1;->a(Ljava/lang/Exception;)V

    return-void

    .line 1008267
    :cond_0
    throw p1
.end method

.method public final a(Landroid/content/Intent;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 1008268
    invoke-virtual {p0}, LX/5pX;->j()Landroid/app/Activity;

    move-result-object v0

    .line 1008269
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008270
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 1008271
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Class;)LX/5p4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/5p4;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008234
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-nez v0, :cond_0

    .line 1008235
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call native module before CatalystInstance has been set!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008236
    :cond_0
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/CatalystInstance;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/5pQ;)V
    .locals 1

    .prologue
    .line 1008237
    iget-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 1008238
    return-void
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1008232
    iget-object v0, p0, LX/5pX;->g:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->runOnQueue(Ljava/lang/Runnable;)V

    .line 1008233
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1008231
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0}, Lcom/facebook/react/bridge/CatalystInstance;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1008226
    invoke-static {}, LX/5pe;->b()V

    .line 1008227
    sget-object v0, LX/341;->BEFORE_RESUME:LX/341;

    iput-object v0, p0, LX/5pX;->c:LX/341;

    .line 1008228
    iget-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pQ;

    .line 1008229
    invoke-interface {v0}, LX/5pQ;->bN_()V

    goto :goto_0

    .line 1008230
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1008224
    iget-object v0, p0, LX/5pX;->h:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->runOnQueue(Ljava/lang/Runnable;)V

    .line 1008225
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1008208
    invoke-static {}, LX/5pe;->b()V

    .line 1008209
    sget-object v0, LX/341;->BEFORE_CREATE:LX/341;

    iput-object v0, p0, LX/5pX;->c:LX/341;

    .line 1008210
    iget-object v0, p0, LX/5pX;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pQ;

    .line 1008211
    invoke-interface {v0}, LX/5pQ;->bO_()V

    goto :goto_0

    .line 1008212
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    .line 1008213
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1008204
    invoke-static {}, LX/5pe;->b()V

    .line 1008205
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    if-eqz v0, :cond_0

    .line 1008206
    iget-object v0, p0, LX/5pX;->d:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-interface {v0}, Lcom/facebook/react/bridge/CatalystInstance;->b()V

    .line 1008207
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1008203
    iget-object v0, p0, LX/5pX;->f:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->isOnThread()Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1008201
    iget-object v0, p0, LX/5pX;->g:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->assertIsOnThread()V

    .line 1008202
    return-void
.end method

.method public final getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1008196
    const-string v0, "layout_inflater"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1008197
    iget-object v0, p0, LX/5pX;->e:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 1008198
    invoke-virtual {p0}, LX/5pX;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/5pX;->e:Landroid/view/LayoutInflater;

    .line 1008199
    :cond_0
    iget-object v0, p0, LX/5pX;->e:Landroid/view/LayoutInflater;

    .line 1008200
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/5pX;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1008195
    iget-object v0, p0, LX/5pX;->g:Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/queue/MessageQueueThread;

    invoke-interface {v0}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->isOnThread()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 1008194
    iget-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Landroid/app/Activity;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1008191
    iget-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 1008192
    const/4 v0, 0x0

    .line 1008193
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/5pX;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0
.end method
