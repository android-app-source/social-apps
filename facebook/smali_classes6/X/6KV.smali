.class public final enum LX/6KV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6KV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6KV;

.field public static final enum PURPLE_RAIN:LX/6KV;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1076751
    new-instance v0, LX/6KV;

    const-string v1, "PURPLE_RAIN"

    const-string v2, "PurpleRain"

    invoke-direct {v0, v1, v3, v2}, LX/6KV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6KV;->PURPLE_RAIN:LX/6KV;

    .line 1076752
    const/4 v0, 0x1

    new-array v0, v0, [LX/6KV;

    sget-object v1, LX/6KV;->PURPLE_RAIN:LX/6KV;

    aput-object v1, v0, v3

    sput-object v0, LX/6KV;->$VALUES:[LX/6KV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1076755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1076756
    iput-object p3, p0, LX/6KV;->mName:Ljava/lang/String;

    .line 1076757
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6KV;
    .locals 1

    .prologue
    .line 1076758
    const-class v0, LX/6KV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6KV;

    return-object v0
.end method

.method public static values()[LX/6KV;
    .locals 1

    .prologue
    .line 1076754
    sget-object v0, LX/6KV;->$VALUES:[LX/6KV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6KV;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1076753
    iget-object v0, p0, LX/6KV;->mName:Ljava/lang/String;

    return-object v0
.end method
