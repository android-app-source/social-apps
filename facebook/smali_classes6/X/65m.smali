.class public final LX/65m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:[LX/65j;

.field public d:I

.field public e:I

.field public f:I

.field private final g:LX/672;

.field public h:I

.field public i:Z


# direct methods
.method private constructor <init>(ILX/672;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1048595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048596
    const v0, 0x7fffffff

    iput v0, p0, LX/65m;->h:I

    .line 1048597
    const/16 v0, 0x8

    new-array v0, v0, [LX/65j;

    iput-object v0, p0, LX/65m;->c:[LX/65j;

    .line 1048598
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/65m;->d:I

    .line 1048599
    iput v1, p0, LX/65m;->e:I

    .line 1048600
    iput v1, p0, LX/65m;->f:I

    .line 1048601
    iput p1, p0, LX/65m;->a:I

    .line 1048602
    iput p1, p0, LX/65m;->b:I

    .line 1048603
    iput-object p2, p0, LX/65m;->g:LX/672;

    .line 1048604
    return-void
.end method

.method public constructor <init>(LX/672;)V
    .locals 1

    .prologue
    .line 1048605
    const/16 v0, 0x1000

    invoke-direct {p0, v0, p1}, LX/65m;-><init>(ILX/672;)V

    .line 1048606
    return-void
.end method

.method private a(III)V
    .locals 3

    .prologue
    .line 1048607
    if-ge p1, p2, :cond_0

    .line 1048608
    iget-object v0, p0, LX/65m;->g:LX/672;

    or-int v1, p3, p1

    invoke-virtual {v0, v1}, LX/672;->b(I)LX/672;

    .line 1048609
    :goto_0
    return-void

    .line 1048610
    :cond_0
    iget-object v0, p0, LX/65m;->g:LX/672;

    or-int v1, p3, p2

    invoke-virtual {v0, v1}, LX/672;->b(I)LX/672;

    .line 1048611
    sub-int v0, p1, p2

    .line 1048612
    :goto_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 1048613
    and-int/lit8 v1, v0, 0x7f

    .line 1048614
    iget-object v2, p0, LX/65m;->g:LX/672;

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v2, v1}, LX/672;->b(I)LX/672;

    .line 1048615
    ushr-int/lit8 v0, v0, 0x7

    .line 1048616
    goto :goto_1

    .line 1048617
    :cond_1
    iget-object v1, p0, LX/65m;->g:LX/672;

    invoke-virtual {v1, v0}, LX/672;->b(I)LX/672;

    goto :goto_0
.end method

.method public static a(LX/65m;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1048618
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1048619
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/65m;->d:I

    .line 1048620
    iput v2, p0, LX/65m;->e:I

    .line 1048621
    iput v2, p0, LX/65m;->f:I

    .line 1048622
    return-void
.end method

.method private a(LX/673;)V
    .locals 3

    .prologue
    .line 1048623
    invoke-virtual {p1}, LX/673;->e()I

    move-result v0

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LX/65m;->a(III)V

    .line 1048624
    iget-object v0, p0, LX/65m;->g:LX/672;

    invoke-virtual {v0, p1}, LX/672;->a(LX/673;)LX/672;

    .line 1048625
    return-void
.end method

.method public static b(LX/65m;I)I
    .locals 6

    .prologue
    .line 1048626
    const/4 v1, 0x0

    .line 1048627
    if-lez p1, :cond_1

    .line 1048628
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, LX/65m;->d:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    .line 1048629
    iget-object v2, p0, LX/65m;->c:[LX/65j;

    aget-object v2, v2, v0

    iget v2, v2, LX/65j;->j:I

    sub-int/2addr p1, v2

    .line 1048630
    iget v2, p0, LX/65m;->f:I

    iget-object v3, p0, LX/65m;->c:[LX/65j;

    aget-object v3, v3, v0

    iget v3, v3, LX/65j;->j:I

    sub-int/2addr v2, v3

    iput v2, p0, LX/65m;->f:I

    .line 1048631
    iget v2, p0, LX/65m;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/65m;->e:I

    .line 1048632
    add-int/lit8 v1, v1, 0x1

    .line 1048633
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1048634
    :cond_0
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    iget v2, p0, LX/65m;->d:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, LX/65m;->c:[LX/65j;

    iget v4, p0, LX/65m;->d:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, LX/65m;->e:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1048635
    iget-object v0, p0, LX/65m;->c:[LX/65j;

    iget v2, p0, LX/65m;->d:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, LX/65m;->d:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v1

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 1048636
    iget v0, p0, LX/65m;->d:I

    add-int/2addr v0, v1

    iput v0, p0, LX/65m;->d:I

    .line 1048637
    :cond_1
    return v1
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x20

    const/16 v2, 0x1f

    const/4 v3, 0x0

    .line 1048638
    iget-boolean v0, p0, LX/65m;->i:Z

    if-eqz v0, :cond_1

    .line 1048639
    iget v0, p0, LX/65m;->h:I

    iget v1, p0, LX/65m;->b:I

    if-ge v0, v1, :cond_0

    .line 1048640
    iget v0, p0, LX/65m;->h:I

    invoke-direct {p0, v0, v2, v4}, LX/65m;->a(III)V

    .line 1048641
    :cond_0
    iput-boolean v3, p0, LX/65m;->i:Z

    .line 1048642
    const v0, 0x7fffffff

    iput v0, p0, LX/65m;->h:I

    .line 1048643
    iget v0, p0, LX/65m;->b:I

    invoke-direct {p0, v0, v2, v4}, LX/65m;->a(III)V

    .line 1048644
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_4

    .line 1048645
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    .line 1048646
    iget-object v1, v0, LX/65j;->h:LX/673;

    invoke-virtual {v1}, LX/673;->d()LX/673;

    move-result-object v5

    .line 1048647
    iget-object v6, v0, LX/65j;->i:LX/673;

    .line 1048648
    sget-object v1, LX/65n;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1048649
    if-eqz v1, :cond_2

    .line 1048650
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0xf

    invoke-direct {p0, v0, v1, v3}, LX/65m;->a(III)V

    .line 1048651
    invoke-direct {p0, v6}, LX/65m;->a(LX/673;)V

    .line 1048652
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1048653
    :cond_2
    iget-object v1, p0, LX/65m;->c:[LX/65j;

    invoke-static {v1, v0}, LX/65A;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 1048654
    const/4 v7, -0x1

    if-eq v1, v7, :cond_3

    .line 1048655
    iget v0, p0, LX/65m;->d:I

    sub-int v0, v1, v0

    sget-object v1, LX/65n;->a:[LX/65j;

    array-length v1, v1

    add-int/2addr v0, v1

    const/16 v1, 0x7f

    const/16 v5, 0x80

    invoke-direct {p0, v0, v1, v5}, LX/65m;->a(III)V

    goto :goto_1

    .line 1048656
    :cond_3
    iget-object v1, p0, LX/65m;->g:LX/672;

    const/16 v7, 0x40

    invoke-virtual {v1, v7}, LX/672;->b(I)LX/672;

    .line 1048657
    invoke-direct {p0, v5}, LX/65m;->a(LX/673;)V

    .line 1048658
    invoke-direct {p0, v6}, LX/65m;->a(LX/673;)V

    .line 1048659
    iget v1, v0, LX/65j;->j:I

    .line 1048660
    iget v5, p0, LX/65m;->b:I

    if-le v1, v5, :cond_5

    .line 1048661
    invoke-static {p0}, LX/65m;->a(LX/65m;)V

    .line 1048662
    :goto_2
    goto :goto_1

    .line 1048663
    :cond_4
    return-void

    .line 1048664
    :cond_5
    iget v5, p0, LX/65m;->f:I

    add-int/2addr v5, v1

    iget v6, p0, LX/65m;->b:I

    sub-int/2addr v5, v6

    .line 1048665
    invoke-static {p0, v5}, LX/65m;->b(LX/65m;I)I

    .line 1048666
    iget v5, p0, LX/65m;->e:I

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, LX/65m;->c:[LX/65j;

    array-length v6, v6

    if-le v5, v6, :cond_6

    .line 1048667
    iget-object v5, p0, LX/65m;->c:[LX/65j;

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    new-array v5, v5, [LX/65j;

    .line 1048668
    iget-object v6, p0, LX/65m;->c:[LX/65j;

    const/4 v7, 0x0

    iget-object v8, p0, LX/65m;->c:[LX/65j;

    array-length v8, v8

    iget-object v9, p0, LX/65m;->c:[LX/65j;

    array-length v9, v9

    invoke-static {v6, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1048669
    iget-object v6, p0, LX/65m;->c:[LX/65j;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, LX/65m;->d:I

    .line 1048670
    iput-object v5, p0, LX/65m;->c:[LX/65j;

    .line 1048671
    :cond_6
    iget v5, p0, LX/65m;->d:I

    add-int/lit8 v6, v5, -0x1

    iput v6, p0, LX/65m;->d:I

    .line 1048672
    iget-object v6, p0, LX/65m;->c:[LX/65j;

    aput-object v0, v6, v5

    .line 1048673
    iget v5, p0, LX/65m;->e:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, LX/65m;->e:I

    .line 1048674
    iget v5, p0, LX/65m;->f:I

    add-int/2addr v1, v5

    iput v1, p0, LX/65m;->f:I

    goto :goto_2
.end method
