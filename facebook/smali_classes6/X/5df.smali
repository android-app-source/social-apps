.class public LX/5df;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 965984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;
    .locals 6

    .prologue
    .line 965985
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 965986
    :goto_0
    return-object p0

    .line 965987
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    instance-of v0, v0, LX/0m9;

    if-eqz v0, :cond_1

    .line 965988
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    invoke-virtual {v0}, LX/0lF;->d()LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    move-object v5, v0

    .line 965989
    :goto_1
    invoke-virtual {v5, p1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 965990
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    iget-object v3, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;-><init>(Ljava/lang/String;LX/5dw;Ljava/lang/String;Ljava/lang/String;LX/0lF;)V

    move-object p0, v0

    goto :goto_0

    .line 965991
    :cond_1
    new-instance v5, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/0m9;-><init>(LX/0mC;)V

    goto :goto_1
.end method
