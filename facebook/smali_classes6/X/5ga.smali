.class public final LX/5ga;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 975284
    const-class v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;

    const v0, 0x23dab47e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "AlbumMetadataQuery"

    const-string v6, "460c4ed59008edcd2a3c3f83f466c2b5"

    const-string v7, "album"

    const-string v8, "10155156451446729"

    const/4 v9, 0x0

    .line 975285
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 975286
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 975287
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 975288
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 975289
    sparse-switch v0, :sswitch_data_0

    .line 975290
    :goto_0
    return-object p1

    .line 975291
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 975292
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 975293
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 975294
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 975295
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 975296
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 975297
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_3
        -0x39da4793 -> :sswitch_1
        -0x1b3da4a0 -> :sswitch_0
        0x683094a -> :sswitch_6
        0x1918b88b -> :sswitch_2
        0x73a026b5 -> :sswitch_4
        0x7e07ec78 -> :sswitch_5
    .end sparse-switch
.end method
