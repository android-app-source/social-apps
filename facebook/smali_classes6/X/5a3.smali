.class public final LX/5a3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 953744
    const/4 v13, 0x0

    .line 953745
    const/4 v12, 0x0

    .line 953746
    const/4 v11, 0x0

    .line 953747
    const/4 v10, 0x0

    .line 953748
    const/4 v9, 0x0

    .line 953749
    const/4 v8, 0x0

    .line 953750
    const/4 v7, 0x0

    .line 953751
    const/4 v6, 0x0

    .line 953752
    const/4 v5, 0x0

    .line 953753
    const/4 v4, 0x0

    .line 953754
    const/4 v3, 0x0

    .line 953755
    const/4 v2, 0x0

    .line 953756
    const/4 v1, 0x0

    .line 953757
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 953758
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 953759
    const/4 v1, 0x0

    .line 953760
    :goto_0
    return v1

    .line 953761
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 953762
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 953763
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 953764
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 953765
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 953766
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 953767
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 953768
    :cond_3
    const-string v15, "best_description"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 953769
    invoke-static/range {p0 .. p1}, LX/5a0;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 953770
    :cond_4
    const-string v15, "commerce_page_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 953771
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    goto :goto_1

    .line 953772
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 953773
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 953774
    :cond_6
    const-string v15, "is_messenger_user"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 953775
    const/4 v1, 0x1

    .line 953776
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 953777
    :cond_7
    const-string v15, "messenger_welcome_page_context_banner"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 953778
    invoke-static/range {p0 .. p1}, LX/5a1;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 953779
    :cond_8
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 953780
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 953781
    :cond_9
    const-string v15, "profile_pic_large"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 953782
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 953783
    :cond_a
    const-string v15, "profile_pic_medium"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 953784
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 953785
    :cond_b
    const-string v15, "profile_pic_small"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 953786
    invoke-static/range {p0 .. p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 953787
    :cond_c
    const-string v15, "responsiveness_context"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 953788
    invoke-static/range {p0 .. p1}, LX/5a2;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 953789
    :cond_d
    const-string v15, "username"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 953790
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 953791
    :cond_e
    const/16 v14, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 953792
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 953793
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 953794
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 953795
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 953796
    if-eqz v1, :cond_f

    .line 953797
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->a(IZ)V

    .line 953798
    :cond_f
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 953799
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 953800
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 953801
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 953802
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 953803
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 953804
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 953805
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
