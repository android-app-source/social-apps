.class public final LX/5LB;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 899778
    const-class v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    const v0, 0x398633bc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchTaggableObjectsQuery"

    const-string v6, "a386116842f21fb96a5ea2aad8e7380e"

    const-string v7, "node"

    const-string v8, "10155069966116729"

    const-string v9, "10155259088361729"

    .line 899779
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 899780
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 899781
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 899782
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 899783
    sparse-switch v0, :sswitch_data_0

    .line 899784
    :goto_0
    return-object p1

    .line 899785
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 899786
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 899787
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 899788
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 899789
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 899790
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 899791
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 899792
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 899793
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 899794
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 899795
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 899796
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 899797
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 899798
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 899799
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 899800
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 899801
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 899802
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 899803
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x789f2a94 -> :sswitch_3
        -0x7713b599 -> :sswitch_a
        -0x6e761353 -> :sswitch_8
        -0x6b0fbb02 -> :sswitch_b
        -0x69f19a9a -> :sswitch_2
        -0x51484e72 -> :sswitch_0
        -0x50cab1c8 -> :sswitch_d
        -0x4ae70342 -> :sswitch_c
        -0x41a91745 -> :sswitch_6
        -0x2f1f8925 -> :sswitch_1
        0x23640cb -> :sswitch_7
        0x1893e7dc -> :sswitch_f
        0x215ea710 -> :sswitch_e
        0x291d8de0 -> :sswitch_12
        0x53b4a6f3 -> :sswitch_9
        0x54df6484 -> :sswitch_4
        0x6ecd2753 -> :sswitch_5
        0x7c660157 -> :sswitch_10
        0x7d405224 -> :sswitch_11
    .end sparse-switch
.end method
