.class public final enum LX/5MM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5MM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5MM;

.field public static final enum CCR:LX/5MM;

.field public static final enum COR:LX/5MM;

.field public static final enum EQ:LX/5MM;

.field public static final enum GT:LX/5MM;

.field public static final enum GTE:LX/5MM;

.field public static final enum LT:LX/5MM;

.field public static final enum LTE:LX/5MM;

.field public static final enum NEQ:LX/5MM;

.field public static final enum OCR:LX/5MM;

.field public static final enum OOR:LX/5MM;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 905434
    new-instance v0, LX/5MM;

    const-string v1, "OOR"

    invoke-direct {v0, v1, v3}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->OOR:LX/5MM;

    .line 905435
    new-instance v0, LX/5MM;

    const-string v1, "CCR"

    invoke-direct {v0, v1, v4}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->CCR:LX/5MM;

    .line 905436
    new-instance v0, LX/5MM;

    const-string v1, "OCR"

    invoke-direct {v0, v1, v5}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->OCR:LX/5MM;

    .line 905437
    new-instance v0, LX/5MM;

    const-string v1, "COR"

    invoke-direct {v0, v1, v6}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->COR:LX/5MM;

    .line 905438
    new-instance v0, LX/5MM;

    const-string v1, "LT"

    invoke-direct {v0, v1, v7}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->LT:LX/5MM;

    .line 905439
    new-instance v0, LX/5MM;

    const-string v1, "LTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->LTE:LX/5MM;

    .line 905440
    new-instance v0, LX/5MM;

    const-string v1, "EQ"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->EQ:LX/5MM;

    .line 905441
    new-instance v0, LX/5MM;

    const-string v1, "NEQ"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->NEQ:LX/5MM;

    .line 905442
    new-instance v0, LX/5MM;

    const-string v1, "GT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->GT:LX/5MM;

    .line 905443
    new-instance v0, LX/5MM;

    const-string v1, "GTE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/5MM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5MM;->GTE:LX/5MM;

    .line 905444
    const/16 v0, 0xa

    new-array v0, v0, [LX/5MM;

    sget-object v1, LX/5MM;->OOR:LX/5MM;

    aput-object v1, v0, v3

    sget-object v1, LX/5MM;->CCR:LX/5MM;

    aput-object v1, v0, v4

    sget-object v1, LX/5MM;->OCR:LX/5MM;

    aput-object v1, v0, v5

    sget-object v1, LX/5MM;->COR:LX/5MM;

    aput-object v1, v0, v6

    sget-object v1, LX/5MM;->LT:LX/5MM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5MM;->LTE:LX/5MM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5MM;->EQ:LX/5MM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5MM;->NEQ:LX/5MM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5MM;->GT:LX/5MM;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5MM;->GTE:LX/5MM;

    aput-object v2, v0, v1

    sput-object v0, LX/5MM;->$VALUES:[LX/5MM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 905445
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5MM;
    .locals 1

    .prologue
    .line 905433
    const-class v0, LX/5MM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5MM;

    return-object v0
.end method

.method public static values()[LX/5MM;
    .locals 1

    .prologue
    .line 905432
    sget-object v0, LX/5MM;->$VALUES:[LX/5MM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5MM;

    return-object v0
.end method
