.class public LX/6Iu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ia;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/6JO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Landroid/hardware/camera2/CameraCaptureSession;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Landroid/hardware/camera2/CaptureRequest$Builder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Z

.field public E:Z

.field public F:Z

.field private G:Ljava/lang/String;

.field public H:LX/6Ih;

.field public final I:Landroid/hardware/camera2/CameraDevice$StateCallback;

.field public final J:LX/6Ik;

.field public final K:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

.field private final L:LX/6Ik;

.field public M:LX/6JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final N:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

.field public O:LX/6JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final P:Landroid/media/ImageReader$OnImageAvailableListener;

.field public final Q:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

.field private final R:Landroid/media/ImageReader$OnImageAvailableListener;

.field public b:I

.field public c:I

.field public final d:LX/6JF;

.field public final e:LX/6Iv;

.field public volatile f:Z

.field public g:Ljava/lang/String;

.field public h:Landroid/hardware/camera2/CameraManager;

.field public i:Landroid/hardware/camera2/CameraDevice;

.field public volatile j:LX/6Ik;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Landroid/media/ImageReader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/Surface;",
            ">;"
        }
    .end annotation
.end field

.field public o:I

.field public p:Landroid/media/MediaRecorder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/6KN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field private s:[Landroid/hardware/camera2/params/MeteringRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I

.field public u:LX/6Jj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/media/ImageReader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/6If;

.field public x:LX/6JB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/6JC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/6JC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1074946
    const-class v0, LX/6Iu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Iu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6JF;LX/6KN;)V
    .locals 2
    .param p3    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1074819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1074820
    iput v0, p0, LX/6Iu;->b:I

    .line 1074821
    iput v0, p0, LX/6Iu;->c:I

    .line 1074822
    iput-boolean v0, p0, LX/6Iu;->r:Z

    .line 1074823
    iput-boolean v0, p0, LX/6Iu;->D:Z

    .line 1074824
    iput-boolean v0, p0, LX/6Iu;->E:Z

    .line 1074825
    iput-boolean v0, p0, LX/6Iu;->F:Z

    .line 1074826
    new-instance v0, LX/6Ij;

    invoke-direct {v0, p0}, LX/6Ij;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->I:Landroid/hardware/camera2/CameraDevice$StateCallback;

    .line 1074827
    new-instance v0, LX/6Im;

    invoke-direct {v0, p0}, LX/6Im;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->J:LX/6Ik;

    .line 1074828
    new-instance v0, LX/6In;

    invoke-direct {v0, p0}, LX/6In;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->K:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    .line 1074829
    new-instance v0, LX/6Io;

    invoke-direct {v0, p0}, LX/6Io;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->L:LX/6Ik;

    .line 1074830
    new-instance v0, LX/6Ip;

    invoke-direct {v0, p0}, LX/6Ip;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->N:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    .line 1074831
    new-instance v0, LX/6Iq;

    invoke-direct {v0, p0}, LX/6Iq;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->P:Landroid/media/ImageReader$OnImageAvailableListener;

    .line 1074832
    new-instance v0, LX/6Ir;

    invoke-direct {v0, p0}, LX/6Ir;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->Q:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    .line 1074833
    new-instance v0, LX/6Is;

    invoke-direct {v0, p0}, LX/6Is;-><init>(LX/6Iu;)V

    iput-object v0, p0, LX/6Iu;->R:Landroid/media/ImageReader$OnImageAvailableListener;

    .line 1074834
    if-nez p1, :cond_0

    .line 1074835
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074836
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    .line 1074837
    iput-object p2, p0, LX/6Iu;->d:LX/6JF;

    .line 1074838
    iput-object p3, p0, LX/6Iu;->q:LX/6KN;

    .line 1074839
    new-instance v0, LX/6Iv;

    const-string v1, "CameraBackgroundThread"

    invoke-direct {v0, v1}, LX/6Iv;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/6Iu;->e:LX/6Iv;

    .line 1074840
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 1074841
    iget-boolean v0, p0, LX/6Iu;->f:Z

    if-nez v0, :cond_0

    .line 1074842
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera is not open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074843
    :cond_0
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    if-nez v0, :cond_1

    .line 1074844
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera Settings is Null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074845
    :cond_1
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    .line 1074846
    iget-object v1, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v1

    .line 1074847
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1074848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera Settings: Output Surface is Empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074849
    :cond_2
    return-void
.end method

.method public static D(LX/6Iu;)V
    .locals 2

    .prologue
    .line 1074850
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    if-nez v0, :cond_0

    .line 1074851
    :goto_0
    return-void

    .line 1074852
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraCaptureSession;->stopRepeating()V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074853
    :goto_1
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    const v1, 0x35e9926c

    invoke-static {v0, v1}, LX/0J1;->a(Landroid/hardware/camera2/CameraCaptureSession;I)V

    .line 1074854
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public static a(LX/6Iu;LX/6JC;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1074855
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074856
    iget-object v0, p0, LX/6Iu;->A:LX/6JO;

    .line 1074857
    if-nez v0, :cond_0

    .line 1074858
    iget-object v0, p1, LX/6JC;->b:LX/6JO;

    .line 1074859
    :cond_0
    if-nez v0, :cond_1

    .line 1074860
    iget-object v0, p0, LX/6Iu;->H:LX/6Ih;

    invoke-virtual {v0}, LX/6Ih;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/6J8;->a(Ljava/util/List;)LX/6JO;

    move-result-object v0

    .line 1074861
    :cond_1
    sget-object v1, LX/6Id;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    move-object v0, v1

    .line 1074862
    if-eqz v0, :cond_2

    .line 1074863
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074864
    :cond_2
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_REGIONS:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v1, p0, LX/6Iu;->s:[Landroid/hardware/camera2/params/MeteringRectangle;

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074865
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_REGIONS:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v1, p0, LX/6Iu;->s:[Landroid/hardware/camera2/params/MeteringRectangle;

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074866
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074867
    iget-object v0, p1, LX/6JC;->a:LX/6JN;

    .line 1074868
    sget-object v1, LX/6Id;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    move-object v0, v1

    .line 1074869
    if-eqz v0, :cond_4

    .line 1074870
    const/4 v1, 0x5

    invoke-static {p0, v1}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074871
    iput-boolean v2, p0, LX/6Iu;->E:Z

    .line 1074872
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1074873
    const/4 v1, 0x6

    invoke-static {p0, v1}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074874
    iput-boolean v2, p0, LX/6Iu;->F:Z

    .line 1074875
    :cond_3
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074876
    :cond_4
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v1, p0, LX/6Iu;->w:LX/6If;

    .line 1074877
    iget-object p1, v1, LX/6If;->a:Landroid/graphics/Rect;

    move-object v1, p1

    .line 1074878
    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074879
    iget-object v0, p0, LX/6Iu;->H:LX/6Ih;

    .line 1074880
    iget-object v1, v0, LX/6Ih;->g:LX/6Ig;

    if-nez v1, :cond_5

    .line 1074881
    const/4 p1, 0x1

    const/4 v4, 0x0

    .line 1074882
    sget-object v1, LX/6Ig;->NONE:LX/6Ig;

    iput-object v1, v0, LX/6Ih;->g:LX/6Ig;

    .line 1074883
    iget-object v1, v0, LX/6Ih;->a:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 1074884
    if-eqz v1, :cond_9

    move v3, v4

    .line 1074885
    :goto_0
    array-length p0, v1

    if-ge v3, p0, :cond_9

    .line 1074886
    aget p0, v1, v3

    if-ne p0, p1, :cond_8

    .line 1074887
    sget-object v1, LX/6Ig;->OPTICAL:LX/6Ig;

    iput-object v1, v0, LX/6Ih;->g:LX/6Ig;

    .line 1074888
    :cond_5
    :goto_1
    iget-object v1, v0, LX/6Ih;->g:LX/6Ig;

    move-object v0, v1

    .line 1074889
    sget-object v1, LX/6Ig;->OPTICAL:LX/6Ig;

    if-ne v0, v1, :cond_7

    .line 1074890
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->LENS_OPTICAL_STABILIZATION_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074891
    :cond_6
    :goto_2
    return-void

    .line 1074892
    :cond_7
    sget-object v1, LX/6Ig;->SOFTWARE:LX/6Ig;

    if-ne v0, v1, :cond_6

    .line 1074893
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_VIDEO_STABILIZATION_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_2

    .line 1074894
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1074895
    :cond_9
    iget-object v1, v0, LX/6Ih;->a:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 1074896
    if-eqz v1, :cond_5

    .line 1074897
    :goto_3
    array-length v3, v1

    if-ge v4, v3, :cond_5

    .line 1074898
    aget v3, v1, v4

    if-ne v3, p1, :cond_a

    .line 1074899
    sget-object v1, LX/6Ig;->SOFTWARE:LX/6Ig;

    iput-object v1, v0, LX/6Ih;->g:LX/6Ig;

    goto :goto_1

    .line 1074900
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method private a(Ljava/io/File;LX/6JG;LX/6JC;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1074901
    if-nez p1, :cond_0

    .line 1074902
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "File to save video is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074903
    :cond_0
    if-nez p3, :cond_1

    .line 1074904
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Capture Settings is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074905
    :cond_1
    if-nez p2, :cond_2

    .line 1074906
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Capturecallback is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074907
    :cond_2
    invoke-direct {p0}, LX/6Iu;->A()V

    .line 1074908
    iget v0, p0, LX/6Iu;->b:I

    if-eqz v0, :cond_3

    .line 1074909
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Preview is not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074910
    :cond_3
    invoke-virtual {p0}, LX/6Iu;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1074911
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is a video already being recorded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074912
    :cond_4
    iput-object p1, p0, LX/6Iu;->m:Ljava/io/File;

    .line 1074913
    iput-object p2, p0, LX/6Iu;->O:LX/6JG;

    .line 1074914
    invoke-static {p0, v4}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074915
    :try_start_0
    invoke-static {p0}, LX/6Iu;->D(LX/6Iu;)V

    .line 1074916
    iget-object v0, p0, LX/6Iu;->m:Ljava/io/File;

    invoke-static {v0}, LX/6JM;->a(Ljava/io/File;)V

    .line 1074917
    const/4 v3, 0x2

    .line 1074918
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    if-nez v0, :cond_5

    .line 1074919
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    .line 1074920
    :cond_5
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 1074921
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v3}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 1074922
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 1074923
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    iget-object v1, p0, LX/6Iu;->m:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 1074924
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    const v1, 0x989680

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 1074925
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    .line 1074926
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    iget-object v1, p0, LX/6Iu;->x:LX/6JB;

    iget v1, v1, LX/6JB;->c:I

    iget-object v2, p0, LX/6Iu;->x:LX/6JB;

    iget v2, v2, LX/6JB;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    .line 1074927
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v3}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 1074928
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 1074929
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    iget v1, p0, LX/6Iu;->o:I

    iget v2, p0, LX/6Iu;->k:I

    iget-object v3, p0, LX/6Iu;->d:LX/6JF;

    invoke-static {v1, v2, v3}, LX/6Ie;->a(IILX/6JF;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 1074930
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V

    .line 1074931
    iget-object v0, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 1074932
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/6Iu;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1074933
    iget-object v1, p0, LX/6Iu;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1074934
    iget-object v1, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074935
    iget-object v1, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-static {v0, v1}, LX/6Iu;->a(Ljava/util/List;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074936
    iget-object v1, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-static {p0, p3, v1}, LX/6Iu;->a(LX/6Iu;LX/6JC;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074937
    iget-object v1, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    new-instance v2, LX/6Ii;

    invoke-direct {v2, p0}, LX/6Ii;-><init>(LX/6Iu;)V

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074938
    :goto_0
    return-void

    .line 1074939
    :catch_0
    move-exception v0

    .line 1074940
    invoke-static {p0, v4}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074941
    new-instance v1, LX/6JJ;

    const-string v2, "Start session failed"

    invoke-direct {v1, v2, v0}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v1}, LX/6JG;->a(LX/6JJ;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/Surface;",
            ">;",
            "Landroid/hardware/camera2/CaptureRequest$Builder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1074942
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1074943
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 1074944
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1074945
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1074808
    iget-object v0, p0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$14;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$14;-><init>(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1074809
    return-void
.end method

.method public static a$redex0(LX/6Iu;Landroid/hardware/camera2/CaptureResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x5

    .line 1074947
    iget v0, p0, LX/6Iu;->b:I

    packed-switch v0, :pswitch_data_0

    .line 1074948
    :cond_0
    :goto_0
    return-void

    .line 1074949
    :pswitch_0
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AF_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1074950
    if-eqz v0, :cond_5

    .line 1074951
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v3, v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v2, v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 1074952
    :cond_1
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1074953
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1074954
    :cond_2
    invoke-direct {p0}, LX/6Iu;->t()V

    goto :goto_0

    .line 1074955
    :cond_3
    const/4 v0, 0x2

    iput v0, p0, LX/6Iu;->b:I

    .line 1074956
    iget-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_PRECAPTURE_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074957
    :try_start_0
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, LX/6Iu;->K:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074958
    :goto_1
    goto :goto_0

    .line 1074959
    :pswitch_1
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1074960
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v2, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1074961
    :cond_4
    const/4 v0, 0x3

    iput v0, p0, LX/6Iu;->b:I

    goto :goto_0

    .line 1074962
    :pswitch_2
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1074963
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 1074964
    :cond_5
    invoke-direct {p0}, LX/6Iu;->t()V

    goto/16 :goto_0

    .line 1074965
    :catch_0
    move-exception v0

    .line 1074966
    const-string v1, "Precapture trigger failed"

    invoke-static {p0, v1, v0}, LX/6Iu;->a$redex0(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1074967
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1074810
    iget-object v0, p0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$15;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$15;-><init>(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1074811
    return-void
.end method

.method public static a$redex0(LX/6Iu;Ljava/util/List;LX/6JC;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/Surface;",
            ">;",
            "LX/6JC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1074968
    iget-object v0, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 1074969
    iget-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-static {p0, p2, v0}, LX/6Iu;->a(LX/6Iu;LX/6JC;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074970
    iget-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-static {p1, v0}, LX/6Iu;->a(Ljava/util/List;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074971
    invoke-static {p0}, LX/6Iu;->s(LX/6Iu;)V

    .line 1074972
    return-void
.end method

.method public static b$redex0(LX/6Iu;I)V
    .locals 3

    .prologue
    .line 1074973
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    if-eqz v0, :cond_0

    .line 1074974
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    iget-object v1, p0, LX/6Iu;->G:Ljava/lang/String;

    sget-object v2, LX/6J9;->CAMERA2:LX/6J9;

    invoke-virtual {v0, p1, v1, v2}, LX/6KN;->a(ILjava/lang/String;LX/6J9;)V

    .line 1074975
    :cond_0
    return-void
.end method

.method public static c$redex0(LX/6Iu;I)V
    .locals 1

    .prologue
    .line 1074976
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    if-eqz v0, :cond_0

    .line 1074977
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->a(I)V

    .line 1074978
    :cond_0
    return-void
.end method

.method public static c$redex0(LX/6Iu;LX/6Ik;)V
    .locals 6
    .param p0    # LX/6Iu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x8

    .line 1074979
    iget-boolean v0, p0, LX/6Iu;->f:Z

    if-nez v0, :cond_0

    .line 1074980
    :goto_0
    return-void

    .line 1074981
    :cond_0
    if-nez p1, :cond_1

    .line 1074982
    iget-object p1, p0, LX/6Iu;->J:LX/6Ik;

    .line 1074983
    :cond_1
    invoke-static {p0, v1}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074984
    :try_start_0
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v0, :cond_2

    .line 1074985
    invoke-static {p0, p1}, LX/6Iu;->e(LX/6Iu;LX/6Ik;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1074986
    :catch_0
    move-exception v0

    .line 1074987
    invoke-static {p0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074988
    new-instance v1, LX/6JJ;

    const-string v2, "Failed to start preview"

    invoke-direct {v1, v2, v0}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p1, v1}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1074989
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/6Iu;->l:Landroid/media/ImageReader;

    if-nez v0, :cond_3

    .line 1074990
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    iget v0, v0, LX/6JB;->a:I

    iget-object v2, p0, LX/6Iu;->x:LX/6JB;

    iget v2, v2, LX/6JB;->b:I

    const/16 v3, 0x100

    const/4 v4, 0x2

    invoke-static {v0, v2, v3, v4}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->l:Landroid/media/ImageReader;

    .line 1074991
    iget-object v0, p0, LX/6Iu;->l:Landroid/media/ImageReader;

    iget-object v2, p0, LX/6Iu;->P:Landroid/media/ImageReader$OnImageAvailableListener;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    .line 1074992
    :cond_3
    iget-object v0, p0, LX/6Iu;->u:LX/6Jj;

    if-eqz v0, :cond_4

    .line 1074993
    invoke-static {p0}, LX/6Iu;->p(LX/6Iu;)V

    .line 1074994
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1074995
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    .line 1074996
    iget-object v4, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v4

    .line 1074997
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1074998
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    .line 1074999
    iget-object v4, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v4

    .line 1075000
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JA;

    .line 1075001
    iget-object v4, v0, LX/6JA;->a:Landroid/graphics/SurfaceTexture;

    .line 1075002
    iget v5, v0, LX/6JA;->b:I

    iget v0, v0, LX/6JA;->c:I

    invoke-virtual {v4, v5, v0}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 1075003
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, v4}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 1075004
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075005
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1075006
    :cond_5
    iget-object v0, p0, LX/6Iu;->u:LX/6Jj;

    if-eqz v0, :cond_6

    .line 1075007
    invoke-static {p0}, LX/6Iu;->p(LX/6Iu;)V

    .line 1075008
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075009
    :cond_6
    move-object v0, v3

    .line 1075010
    iput-object v0, p0, LX/6Iu;->n:Ljava/util/List;

    .line 1075011
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/6Iu;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1075012
    iget-object v2, p0, LX/6Iu;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1075013
    iget-object v2, p0, LX/6Iu;->l:Landroid/media/ImageReader;

    invoke-virtual {v2}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075014
    iget-object v2, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    new-instance v3, LX/6It;

    invoke-direct {v3, p0, p1}, LX/6It;-><init>(LX/6Iu;LX/6Ik;)V

    iget-object v4, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v4}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1075015
    goto/16 :goto_0
.end method

.method public static d$redex0(LX/6Iu;I)V
    .locals 1

    .prologue
    .line 1075016
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    if-eqz v0, :cond_0

    .line 1075017
    iget-object v0, p0, LX/6Iu;->q:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->b(I)V

    .line 1075018
    :cond_0
    return-void
.end method

.method public static e(LX/6Iu;LX/6Ik;)V
    .locals 2

    .prologue
    .line 1075019
    const/4 v0, 0x0

    iput v0, p0, LX/6Iu;->b:I

    .line 1075020
    iget-object v0, p0, LX/6Iu;->n:Ljava/util/List;

    iget-object v1, p0, LX/6Iu;->y:LX/6JC;

    invoke-static {p0, v0, v1}, LX/6Iu;->a$redex0(LX/6Iu;Ljava/util/List;LX/6JC;)V

    .line 1075021
    iget-object v0, p0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$10;

    invoke-direct {v1, p0, p1}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$10;-><init>(LX/6Iu;LX/6Ik;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1075022
    return-void
.end method

.method public static o(LX/6Iu;)V
    .locals 1

    .prologue
    .line 1075023
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    if-eqz v0, :cond_0

    .line 1075024
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->close()V

    .line 1075025
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    .line 1075026
    :cond_0
    return-void
.end method

.method public static p(LX/6Iu;)V
    .locals 4

    .prologue
    .line 1075027
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->getImageFormat()I

    move-result v0

    iget v1, p0, LX/6Iu;->t:I

    if-eq v0, v1, :cond_0

    .line 1075028
    invoke-static {p0}, LX/6Iu;->o(LX/6Iu;)V

    .line 1075029
    :cond_0
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    if-nez v0, :cond_1

    .line 1075030
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    .line 1075031
    iget-object v1, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v1

    .line 1075032
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JA;

    .line 1075033
    iget v1, v0, LX/6JA;->b:I

    iget v0, v0, LX/6JA;->c:I

    iget v2, p0, LX/6Iu;->t:I

    const/4 v3, 0x2

    invoke-static {v1, v0, v2, v3}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    .line 1075034
    iget-object v0, p0, LX/6Iu;->v:Landroid/media/ImageReader;

    iget-object v1, p0, LX/6Iu;->R:Landroid/media/ImageReader$OnImageAvailableListener;

    iget-object v2, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v2}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    .line 1075035
    :cond_1
    return-void
.end method

.method public static q(LX/6Iu;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1075036
    iget-object v0, p0, LX/6Iu;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1075037
    iget-object v0, p0, LX/6Iu;->g:Ljava/lang/String;

    .line 1075038
    :goto_0
    return-object v0

    .line 1075039
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1075040
    iget-object v0, p0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    .line 1075041
    sget-object v5, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1075042
    if-eqz v0, :cond_2

    iget-object v5, p0, LX/6Iu;->d:LX/6JF;

    .line 1075043
    sget-object v6, LX/6JF;->FRONT:LX/6JF;

    if-ne v5, v6, :cond_3

    const/4 v6, 0x0

    :goto_2
    move v5, v6

    .line 1075044
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1075045
    iput-object v4, p0, LX/6Iu;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075046
    :cond_1
    iget-object v0, p0, LX/6Iu;->g:Ljava/lang/String;

    goto :goto_0

    .line 1075047
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1075048
    :catch_0
    move-exception v0

    .line 1075049
    new-instance v1, LX/6JJ;

    const-string v2, "Failed to get charcterstics for cameraId"

    invoke-direct {v1, v2, v0}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    const/4 v6, 0x1

    goto :goto_2
.end method

.method public static s(LX/6Iu;)V
    .locals 6

    .prologue
    .line 1075050
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, LX/6Iu;->K:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    .line 1075051
    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 1075052
    invoke-static {}, LX/0Bx;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1075053
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraCaptureSession;->getDevice()Landroid/hardware/camera2/CameraDevice;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result p0

    .line 1075054
    invoke-static {p0}, LX/0Bx;->c(I)V

    .line 1075055
    :cond_0
    return-void
.end method

.method private t()V
    .locals 7

    .prologue
    .line 1075056
    iget v0, p0, LX/6Iu;->c:I

    packed-switch v0, :pswitch_data_0

    .line 1075057
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/6Iu;->c:I

    .line 1075058
    return-void

    .line 1075059
    :pswitch_0
    const/4 v0, 0x4

    iput v0, p0, LX/6Iu;->b:I

    .line 1075060
    :try_start_0
    iget-object v0, p0, LX/6Iu;->l:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iget-object v1, p0, LX/6Iu;->z:LX/6JC;

    .line 1075061
    iget-object v2, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v2

    .line 1075062
    invoke-static {p0, v1, v2}, LX/6Iu;->a(LX/6Iu;LX/6JC;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1075063
    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 1075064
    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->JPEG_ORIENTATION:Landroid/hardware/camera2/CaptureRequest$Key;

    iget v4, p0, LX/6Iu;->o:I

    iget v5, p0, LX/6Iu;->k:I

    iget-object v6, p0, LX/6Iu;->d:LX/6JF;

    invoke-static {v4, v5, v6}, LX/6Ie;->a(IILX/6JF;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1075065
    move-object v0, v2

    .line 1075066
    iget-object v1, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraCaptureSession;->stopRepeating()V

    .line 1075067
    iget-object v1, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v0

    iget-object v2, p0, LX/6Iu;->N:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075068
    :goto_1
    goto :goto_0

    .line 1075069
    :pswitch_1
    const/4 v1, 0x4

    invoke-static {p0, v1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1075070
    iget-object v1, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v1}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$11;

    invoke-direct {v2, p0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$11;-><init>(LX/6Iu;)V

    const-wide/16 v3, 0x1388

    const v5, -0x2e291a00

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1075071
    goto :goto_0

    .line 1075072
    :catch_0
    move-exception v0

    .line 1075073
    const-string v1, "Capture still picture failed"

    invoke-static {p0, v1, v0}, LX/6Iu;->a$redex0(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1075074
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static x(LX/6Iu;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1074812
    iget-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074813
    :try_start_0
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, LX/6Iu;->K:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1074814
    invoke-static {p0, v4}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    .line 1074815
    :goto_0
    return-void

    .line 1074816
    :catch_0
    move-exception v0

    .line 1074817
    :try_start_1
    const-string v1, "Capture failed"

    invoke-static {p0, v1, v0}, LX/6Iu;->a$redex0(LX/6Iu;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1074818
    invoke-static {p0, v4}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0, v4}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/6IP;
    .locals 2

    .prologue
    .line 1074773
    invoke-virtual {p0}, LX/6Iu;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074774
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera must be open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074775
    :cond_0
    iget-object v0, p0, LX/6Iu;->H:LX/6Ih;

    return-object v0
.end method

.method public final a(FF)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1074750
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/6Iu;->D:Z

    .line 1074751
    const/4 v0, 0x4

    invoke-static {p0, v0}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074752
    iget v0, p0, LX/6Iu;->o:I

    iget v1, p0, LX/6Iu;->k:I

    iget-object v2, p0, LX/6Iu;->d:LX/6JF;

    invoke-static {v0, v1, v2}, LX/6Ie;->a(IILX/6JF;)I

    move-result v0

    iget-object v1, p0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    iget-object v2, p0, LX/6Iu;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v1

    iget-object v2, p0, LX/6Iu;->d:LX/6JF;

    invoke-static {v0, v1, v2, p1, p2}, LX/6Iw;->a(ILandroid/hardware/camera2/CameraCharacteristics;LX/6JF;FF)[Landroid/hardware/camera2/params/MeteringRectangle;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->s:[Landroid/hardware/camera2/params/MeteringRectangle;

    .line 1074753
    const/4 v2, 0x1

    .line 1074754
    iget v0, p0, LX/6Iu;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074755
    :goto_0
    return-void

    .line 1074756
    :catch_0
    move-exception v0

    .line 1074757
    const-string v1, "Failed to set focus point"

    invoke-static {p0, v3, v1, v0}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1074758
    invoke-static {p0, v3}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074759
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Iu;->D:Z

    goto :goto_0

    .line 1074760
    :cond_0
    :try_start_1
    sget-object v0, LX/6JO;->AUTO:LX/6JO;

    iput-object v0, p0, LX/6Iu;->A:LX/6JO;

    .line 1074761
    iput v2, p0, LX/6Iu;->b:I

    .line 1074762
    iput v2, p0, LX/6Iu;->c:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1074763
    :try_start_2
    iget-object v0, p0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    .line 1074764
    iget-object v1, p0, LX/6Iu;->n:Ljava/util/List;

    invoke-static {v1, v0}, LX/6Iu;->a(Ljava/util/List;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074765
    iget-object v1, p0, LX/6Iu;->y:LX/6JC;

    invoke-static {p0, v1, v0}, LX/6Iu;->a(LX/6Iu;LX/6JC;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 1074766
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074767
    iget-object v1, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v2

    iget-object p1, p0, LX/6Iu;->Q:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object p2, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {p2}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object p2

    invoke-virtual {v1, v2, p1, p2}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 1074768
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074769
    iput-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 1074770
    invoke-static {p0}, LX/6Iu;->s(LX/6Iu;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1074771
    :catch_1
    move-exception v0

    .line 1074772
    const/4 v1, 0x4

    const-string v2, "Failed to start auto focus"

    invoke-static {p0, v1, v2, v0}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1074742
    iget-object v0, p0, LX/6Iu;->w:LX/6If;

    invoke-virtual {v0, p1}, LX/6If;->a(I)V

    .line 1074743
    :try_start_0
    iget-object v0, p0, LX/6Iu;->C:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v2, p0, LX/6Iu;->w:LX/6If;

    .line 1074744
    iget-object p1, v2, LX/6If;->a:Landroid/graphics/Rect;

    move-object v2, p1

    .line 1074745
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 1074746
    invoke-static {p0}, LX/6Iu;->s(LX/6Iu;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074747
    :goto_0
    return-void

    .line 1074748
    :catch_0
    move-exception v0

    .line 1074749
    const/4 v1, 0x4

    const-string v2, "Failed to set zoom level"

    invoke-static {p0, v1, v2, v0}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(ILX/6Jd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/cameracore/camerasdk/common/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1074739
    iput p1, p0, LX/6Iu;->k:I

    .line 1074740
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/6Jd;->a(Ljava/lang/Object;)V

    .line 1074741
    return-void
.end method

.method public final a(LX/6Ik;)V
    .locals 4

    .prologue
    .line 1074723
    invoke-virtual {p0}, LX/6Iu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074724
    const/4 v0, 0x1

    const-string v1, "Camera is already in use"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1074725
    :goto_0
    return-void

    .line 1074726
    :cond_0
    if-nez p1, :cond_1

    .line 1074727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Statecallback is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074728
    :cond_1
    iput-object p1, p0, LX/6Iu;->j:LX/6Ik;

    .line 1074729
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6Iu;->G:Ljava/lang/String;

    .line 1074730
    const/4 p1, 0x1

    .line 1074731
    iget-boolean v0, p0, LX/6Iu;->f:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/6Iu;->j:LX/6Ik;

    if-nez v0, :cond_3

    .line 1074732
    :cond_2
    :goto_1
    goto :goto_0

    .line 1074733
    :cond_3
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074734
    iget-object v0, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v0}, LX/6Iv;->a()V

    .line 1074735
    iget-object v0, p0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    invoke-static {p0}, LX/6Iu;->q(LX/6Iu;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/6Iu;->I:Landroid/hardware/camera2/CameraDevice$StateCallback;

    iget-object v3, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v3}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1074736
    :catch_0
    move-exception v0

    .line 1074737
    invoke-static {p0, p1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074738
    const/4 v1, 0x4

    const-string v2, "Couldn\'t open camera"

    invoke-static {p0, v1, v2, v0}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(LX/6Ik;LX/6JC;)V
    .locals 2

    .prologue
    .line 1074715
    if-nez p1, :cond_0

    .line 1074716
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Preview callback is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074717
    :cond_0
    if-nez p2, :cond_1

    .line 1074718
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Capture settings is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074719
    :cond_1
    invoke-direct {p0}, LX/6Iu;->A()V

    .line 1074720
    iput-object p2, p0, LX/6Iu;->y:LX/6JC;

    .line 1074721
    invoke-static {p0, p1}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    .line 1074722
    return-void
.end method

.method public final a(LX/6JB;)V
    .locals 2

    .prologue
    .line 1074709
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    if-eqz v0, :cond_0

    .line 1074710
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Settings have already been set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074711
    :cond_0
    iput-object p1, p0, LX/6Iu;->x:LX/6JB;

    .line 1074712
    invoke-direct {p0}, LX/6Iu;->A()V

    .line 1074713
    iget v0, p1, LX/6JB;->e:I

    iput v0, p0, LX/6Iu;->k:I

    .line 1074714
    return-void
.end method

.method public final a(LX/6Jj;I)V
    .locals 3

    .prologue
    .line 1074697
    iget v0, p0, LX/6Iu;->b:I

    if-eqz v0, :cond_0

    .line 1074698
    new-instance v0, LX/6JJ;

    const-string v1, "Cannot set FrameCallback while in preview"

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074699
    :cond_0
    iget-object v0, p0, LX/6Iu;->x:LX/6JB;

    if-nez v0, :cond_1

    .line 1074700
    new-instance v0, LX/6JJ;

    const-string v1, "Camera settings has to specified first"

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074701
    :cond_1
    if-nez p1, :cond_2

    .line 1074702
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Iu;->u:LX/6Jj;

    .line 1074703
    const/16 v0, 0x23

    iput v0, p0, LX/6Iu;->t:I

    .line 1074704
    :goto_0
    return-void

    .line 1074705
    :cond_2
    invoke-virtual {p0}, LX/6Iu;->a()LX/6IP;

    move-result-object v0

    invoke-interface {v0}, LX/6IP;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1074706
    new-instance v0, LX/6JJ;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Frame callback format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074707
    :cond_3
    iput-object p1, p0, LX/6Iu;->u:LX/6Jj;

    .line 1074708
    iput p2, p0, LX/6Iu;->t:I

    goto :goto_0
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 1

    .prologue
    .line 1074695
    iget-object v0, p0, LX/6Iu;->y:LX/6JC;

    invoke-direct {p0, p1, p2, v0}, LX/6Iu;->a(Ljava/io/File;LX/6JG;LX/6JC;)V

    .line 1074696
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1074693
    sget-object v0, LX/6JE;->a:LX/6JE;

    invoke-virtual {p0, v0}, LX/6Iu;->b(LX/6Ik;)V

    .line 1074694
    return-void
.end method

.method public final b(LX/6Ik;)V
    .locals 3

    .prologue
    .line 1074776
    iget-boolean v0, p0, LX/6Iu;->f:Z

    if-nez v0, :cond_0

    .line 1074777
    :goto_0
    return-void

    .line 1074778
    :cond_0
    iget-object v0, p0, LX/6Iu;->e:LX/6Iv;

    invoke-virtual {v0}, LX/6Iv;->c()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;

    invoke-direct {v1, p0, p1}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;-><init>(LX/6Iu;LX/6Ik;)V

    const v2, 0x2f22acab

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1074779
    iget-boolean v0, p0, LX/6Iu;->f:Z

    return v0
.end method

.method public final d()LX/6JF;
    .locals 1

    .prologue
    .line 1074780
    iget-object v0, p0, LX/6Iu;->d:LX/6JF;

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1074781
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    .line 1074782
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaRecorder is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074783
    :cond_0
    invoke-virtual {p0}, LX/6Iu;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1074784
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Video recording was not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074785
    :cond_1
    const/16 v0, 0xb

    invoke-static {p0, v0}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074786
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Iu;->r:Z

    .line 1074787
    invoke-static {p0}, LX/6Iu;->D(LX/6Iu;)V

    .line 1074788
    invoke-virtual {p0}, LX/6Iu;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1074789
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 1074790
    :cond_2
    iget-object v0, p0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 1074791
    iput-object v1, p0, LX/6Iu;->m:Ljava/io/File;

    .line 1074792
    iget-object v0, p0, LX/6Iu;->O:LX/6JG;

    if-eqz v0, :cond_3

    .line 1074793
    iget-object v0, p0, LX/6Iu;->O:LX/6JG;

    invoke-interface {v0}, LX/6JG;->b()V

    .line 1074794
    iput-object v1, p0, LX/6Iu;->O:LX/6JG;

    .line 1074795
    :cond_3
    iget-object v0, p0, LX/6Iu;->L:LX/6Ik;

    invoke-static {p0, v0}, LX/6Iu;->c$redex0(LX/6Iu;LX/6Ik;)V

    .line 1074796
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1074797
    iget-boolean v0, p0, LX/6Iu;->r:Z

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1074798
    iget v0, p0, LX/6Iu;->k:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1074799
    iget v0, p0, LX/6Iu;->k:I

    mul-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1074800
    iget-object v0, p0, LX/6Iu;->w:LX/6If;

    .line 1074801
    iget p0, v0, LX/6If;->g:I

    move v0, p0

    .line 1074802
    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1074803
    invoke-virtual {p0}, LX/6Iu;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074804
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    const v1, -0x634d8d33

    invoke-static {v0, v1}, LX/0J1;->a(Landroid/hardware/camera2/CameraCaptureSession;I)V

    .line 1074805
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    .line 1074806
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1074807
    iget-object v0, p0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v0, :cond_0

    iget v0, p0, LX/6Iu;->b:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/6Iu;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
