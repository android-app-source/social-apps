.class public LX/5fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 972029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5fS;)V
    .locals 0

    .prologue
    .line 972030
    return-void
.end method

.method public final a(LX/5fS;II)V
    .locals 8

    .prologue
    .line 972031
    invoke-virtual {p1}, LX/5fS;->C()Ljava/util/List;

    move-result-object v0

    .line 972032
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    div-float v4, v1, v2

    .line 972033
    const/4 v3, 0x0

    .line 972034
    const/4 v1, 0x0

    .line 972035
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972036
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v2, v6

    .line 972037
    cmpg-float v2, v4, v2

    if-gez v2, :cond_0

    .line 972038
    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    int-to-float v6, v6

    mul-float/2addr v2, v6

    float-to-int v2, v2

    .line 972039
    :goto_1
    if-le v2, v3, :cond_2

    move v1, v2

    :goto_2
    move v3, v1

    move-object v1, v0

    .line 972040
    goto :goto_0

    .line 972041
    :cond_0
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v2, v2

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    int-to-float v6, v6

    div-float/2addr v6, v4

    mul-float/2addr v2, v6

    float-to-int v2, v2

    goto :goto_1

    .line 972042
    :cond_1
    iget v0, v1, Landroid/hardware/Camera$Size;->width:I

    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, v0, v1}, LX/5fS;->a(II)V

    .line 972043
    return-void

    :cond_2
    move-object v0, v1

    move v1, v3

    goto :goto_2
.end method

.method public a(LX/5fS;LX/5fO;LX/5fO;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 972044
    invoke-virtual {p1}, LX/5fS;->B()Ljava/util/List;

    move-result-object v0

    .line 972045
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {p1}, LX/5fS;->C()Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 972046
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 972047
    if-eqz v0, :cond_1

    .line 972048
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972049
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 972050
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 972051
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 972052
    :cond_2
    new-instance v0, LX/5fg;

    invoke-direct {v0, p0}, LX/5fg;-><init>(LX/5fj;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 972053
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    .line 972054
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    move-object v1, v0

    .line 972055
    :goto_1
    sget-object v0, LX/5fO;->HIGH:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 972056
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    move-object v1, v0

    .line 972057
    :cond_3
    :goto_2
    invoke-virtual {p1}, LX/5fS;->D()Ljava/util/List;

    move-result-object v0

    .line 972058
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 972059
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 972060
    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 972061
    :cond_4
    new-instance v0, LX/5fh;

    invoke-direct {v0, p0}, LX/5fh;-><init>(LX/5fj;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 972062
    sget-object v0, LX/5fO;->HIGH:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 972063
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972064
    :cond_5
    :goto_3
    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, v2, v1}, LX/5fS;->a(II)V

    .line 972065
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p1, v1, v0}, LX/5fS;->b(II)V

    .line 972066
    return-void

    .line 972067
    :cond_6
    sget-object v0, LX/5fO;->MEDIUM:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 972068
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972069
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v4

    div-int/lit8 v4, v0, 0x2

    .line 972070
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v7, v0

    move-object v0, v1

    move v1, v7

    .line 972071
    :cond_7
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_8

    .line 972072
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972073
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    if-gt v5, v4, :cond_7

    :cond_8
    move-object v1, v0

    .line 972074
    goto :goto_2

    :cond_9
    sget-object v0, LX/5fO;->LOW:LX/5fO;

    invoke-virtual {p3, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 972075
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972076
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v4

    div-int/lit8 v4, v0, 0x3

    .line 972077
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v7, v0

    move-object v0, v1

    move v1, v7

    .line 972078
    :cond_a
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_b

    .line 972079
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972080
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    if-gt v5, v4, :cond_a

    :cond_b
    move-object v1, v0

    goto/16 :goto_2

    .line 972081
    :cond_c
    sget-object v0, LX/5fO;->MEDIUM:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 972082
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v7, v0

    move-object v0, v2

    move v2, v7

    .line 972083
    :cond_d
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_5

    .line 972084
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972085
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v4, v5

    const/high16 v5, 0x200000

    if-gt v4, v5, :cond_d

    goto/16 :goto_3

    .line 972086
    :cond_e
    sget-object v0, LX/5fO;->LOW:LX/5fO;

    invoke-virtual {p2, v0}, LX/5fO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 972087
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v7, v0

    move-object v0, v2

    move v2, v7

    .line 972088
    :cond_f
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_5

    .line 972089
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 972090
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v4, v5

    const/high16 v5, 0x100000

    if-gt v4, v5, :cond_f

    goto/16 :goto_3

    :cond_10
    move-object v0, v2

    goto/16 :goto_3

    :cond_11
    move-object v1, v2

    goto/16 :goto_1
.end method

.method public final b(LX/5fS;)V
    .locals 0

    .prologue
    .line 972091
    return-void
.end method
