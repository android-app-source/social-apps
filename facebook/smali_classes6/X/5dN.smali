.class public LX/5dN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Lcom/facebook/messaging/model/attachment/ImageData;

.field public h:Lcom/facebook/messaging/model/attachment/VideoData;

.field public i:Lcom/facebook/messaging/model/attachment/AudioData;

.field public j:Ljava/lang/String;

.field public k:[B

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 965594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965595
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/5dN;->a:Ljava/lang/String;

    .line 965596
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/5dN;->b:Ljava/lang/String;

    .line 965597
    return-void
.end method


# virtual methods
.method public final a(I)LX/5dN;
    .locals 0

    .prologue
    .line 965606
    iput p1, p0, LX/5dN;->f:I

    .line 965607
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/attachment/ImageData;)LX/5dN;
    .locals 0

    .prologue
    .line 965608
    iput-object p1, p0, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 965609
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5dN;
    .locals 0

    .prologue
    .line 965602
    iput-object p1, p0, LX/5dN;->c:Ljava/lang/String;

    .line 965603
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5dN;
    .locals 0

    .prologue
    .line 965604
    iput-object p1, p0, LX/5dN;->d:Ljava/lang/String;

    .line 965605
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/5dN;
    .locals 0

    .prologue
    .line 965600
    iput-object p1, p0, LX/5dN;->e:Ljava/lang/String;

    .line 965601
    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 965599
    iget-object v0, p0, LX/5dN;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/messaging/model/attachment/Attachment;
    .locals 1

    .prologue
    .line 965598
    new-instance v0, Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/attachment/Attachment;-><init>(LX/5dN;)V

    return-object v0
.end method
