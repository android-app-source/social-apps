.class public final LX/690;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:F

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/68w;",
            ">;>;"
        }
    .end annotation
.end field

.field public static c:Landroid/content/Context;

.field private static d:J

.field private static final e:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1057167
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, LX/690;->a:F

    .line 1057168
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, LX/690;->b:Ljava/util/HashMap;

    .line 1057169
    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sput v0, LX/690;->e:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1057170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1057171
    return-void
.end method

.method public static a(F)LX/68w;
    .locals 2

    .prologue
    .line 1057172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hue_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/68y;

    invoke-direct {v1, p0}, LX/68y;-><init>(F)V

    invoke-static {v0, v1}, LX/690;->a(Ljava/lang/String;LX/68x;)LX/68w;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)LX/68w;
    .locals 2

    .prologue
    .line 1057173
    sget-object v0, LX/690;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1057174
    const/4 v0, 0x0

    .line 1057175
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resource_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/68z;

    invoke-direct {v1, p0}, LX/68z;-><init>(I)V

    invoke-static {v0, v1}, LX/690;->a(Ljava/lang/String;LX/68x;)LX/68w;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;)LX/68w;
    .locals 3

    .prologue
    .line 1057176
    new-instance v0, LX/68w;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, LX/68w;-><init>(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;LX/68x;)LX/68w;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1057177
    sget-object v0, LX/690;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1057178
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68w;

    .line 1057179
    :goto_0
    if-nez v0, :cond_5

    .line 1057180
    invoke-interface {p1}, LX/68x;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1057181
    if-nez v2, :cond_2

    .line 1057182
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    .line 1057183
    goto :goto_0

    .line 1057184
    :cond_2
    new-instance v0, LX/68w;

    invoke-direct {v0, v2}, LX/68w;-><init>(Landroid/graphics/Bitmap;)V

    .line 1057185
    sget-object v1, LX/690;->b:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1057186
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1057187
    sget-wide v4, LX/690;->d:J

    const-wide/32 v6, 0x927c0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    sget-wide v4, LX/690;->d:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 1057188
    :cond_3
    sput-wide v2, LX/690;->d:J

    .line 1057189
    sget-object v0, LX/690;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1057190
    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1057191
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1057192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1057193
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1057194
    sput-object p0, LX/690;->c:Landroid/content/Context;

    .line 1057195
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, LX/690;->a:F

    .line 1057196
    return-void
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 1057197
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 1057198
    mul-float v1, v7, p4

    sub-float v1, p3, v1

    .line 1057199
    invoke-virtual {v0, p2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1057200
    new-instance v2, Landroid/graphics/RectF;

    sub-float v3, p2, p4

    sub-float v4, v1, p4

    add-float v5, p2, p4

    add-float v6, v1, p4

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v3, 0x41f00000    # 30.0f

    const/high16 v4, -0x3c900000    # -240.0f

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 1057201
    mul-float v2, p4, v7

    add-float/2addr v2, v1

    invoke-virtual {v0, p2, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057202
    sget v2, LX/690;->e:F

    mul-float/2addr v2, p4

    add-float/2addr v2, p2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v3, p4

    add-float/2addr v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1057203
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1057204
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1057205
    return-void
.end method
