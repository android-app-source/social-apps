.class public final LX/69m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/29L;


# direct methods
.method public constructor <init>(LX/29L;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1058273
    iput-object p1, p0, LX/69m;->c:LX/29L;

    iput-object p2, p0, LX/69m;->a:Ljava/lang/String;

    iput-object p3, p0, LX/69m;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1058289
    const-string v0, "RealtimePrivacyHandler"

    const-string v1, "FBFeedPrivacyInvalidationQuery query failed, removing"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1058290
    iget-object v0, p0, LX/69m;->c:LX/29L;

    iget-object v1, p0, LX/69m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/29L;->a(Ljava/lang/String;)V

    .line 1058291
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1058274
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1058275
    if-nez p1, :cond_1

    .line 1058276
    const-string v0, "RealtimePrivacyHandler"

    const-string v1, "received null GraphQLResult"

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "received null GraphQLResult"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1058277
    :cond_0
    :goto_0
    return-void

    .line 1058278
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1058279
    if-nez v0, :cond_0

    .line 1058280
    iget-object v0, p0, LX/69m;->c:LX/29L;

    iget-object v1, p0, LX/69m;->a:Ljava/lang/String;

    iget-object v2, p0, LX/69m;->b:Ljava/lang/String;

    const-string v3, "realtime_privacy_livequery_invalidation"

    .line 1058281
    invoke-static {v0, v1, v3}, LX/29L;->b(LX/29L;Ljava/lang/String;Ljava/lang/String;)V

    .line 1058282
    iget-object v4, v0, LX/29L;->a:LX/69l;

    .line 1058283
    iget-object v5, v4, LX/69l;->b:LX/0W3;

    sget-wide v7, LX/0X5;->dB:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    move v4, v5

    .line 1058284
    if-eqz v4, :cond_2

    .line 1058285
    const-string v4, "realtime_privacy_invalidation_remove"

    invoke-static {v0, v1, v4}, LX/29L;->b(LX/29L;Ljava/lang/String;Ljava/lang/String;)V

    .line 1058286
    iget-object v4, v0, LX/29L;->c:LX/18A;

    invoke-virtual {v4, v2}, LX/18A;->a(Ljava/lang/String;)V

    .line 1058287
    :goto_1
    goto :goto_0

    .line 1058288
    :cond_2
    invoke-virtual {v0, v2}, LX/29L;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
