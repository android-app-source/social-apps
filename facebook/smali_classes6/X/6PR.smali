.class public final enum LX/6PR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6PR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6PR;

.field public static final enum ADD:LX/6PR;

.field public static final enum ADD_OR_EDIT:LX/6PR;

.field public static final enum EDIT:LX/6PR;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1086152
    new-instance v0, LX/6PR;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, LX/6PR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6PR;->ADD:LX/6PR;

    .line 1086153
    new-instance v0, LX/6PR;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v3}, LX/6PR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6PR;->EDIT:LX/6PR;

    .line 1086154
    new-instance v0, LX/6PR;

    const-string v1, "ADD_OR_EDIT"

    invoke-direct {v0, v1, v4}, LX/6PR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6PR;->ADD_OR_EDIT:LX/6PR;

    .line 1086155
    const/4 v0, 0x3

    new-array v0, v0, [LX/6PR;

    sget-object v1, LX/6PR;->ADD:LX/6PR;

    aput-object v1, v0, v2

    sget-object v1, LX/6PR;->EDIT:LX/6PR;

    aput-object v1, v0, v3

    sget-object v1, LX/6PR;->ADD_OR_EDIT:LX/6PR;

    aput-object v1, v0, v4

    sput-object v0, LX/6PR;->$VALUES:[LX/6PR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1086156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6PR;
    .locals 1

    .prologue
    .line 1086157
    const-class v0, LX/6PR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6PR;

    return-object v0
.end method

.method public static values()[LX/6PR;
    .locals 1

    .prologue
    .line 1086158
    sget-object v0, LX/6PR;->$VALUES:[LX/6PR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6PR;

    return-object v0
.end method
