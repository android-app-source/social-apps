.class public LX/5rC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5pG;


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 0

    .prologue
    .line 1010941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010942
    iput-object p1, p0, LX/5rC;->a:LX/5pG;

    .line 1010943
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;D)D
    .locals 2

    .prologue
    .line 1010940
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;F)F
    .locals 2

    .prologue
    .line 1010939
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float p2, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 1010938
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1010937
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 1010936
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1010931
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1010935
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/5pC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1010934
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)LX/5pG;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1010933
    iget-object v0, p0, LX/5rC;->a:LX/5pG;

    invoke-interface {v0, p1}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1010932
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/5rC;->a:LX/5pG;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
