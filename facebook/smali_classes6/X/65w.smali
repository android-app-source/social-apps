.class public abstract LX/65w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field private final a:LX/65D;


# direct methods
.method public constructor <init>(LX/65D;)V
    .locals 2

    .prologue
    .line 1049102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049103
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049104
    :cond_0
    iput-object p1, p0, LX/65w;->a:LX/65D;

    .line 1049105
    return-void
.end method


# virtual methods
.method public a(LX/672;J)J
    .locals 2

    .prologue
    .line 1049101
    iget-object v0, p0, LX/65w;->a:LX/65D;

    invoke-interface {v0, p1, p2, p3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1049106
    iget-object v0, p0, LX/65w;->a:LX/65D;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 1049099
    iget-object v0, p0, LX/65w;->a:LX/65D;

    invoke-interface {v0}, LX/65D;->close()V

    .line 1049100
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1049098
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/65w;->a:LX/65D;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
