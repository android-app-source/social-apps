.class public final LX/6IK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/6IL;

.field private b:F

.field private c:Z


# direct methods
.method public constructor <init>(LX/6IL;F)V
    .locals 1

    .prologue
    .line 1073790
    iput-object p1, p0, LX/6IK;->a:LX/6IL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073791
    iput p2, p0, LX/6IK;->b:F

    .line 1073792
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6IK;->c:Z

    .line 1073793
    return-void
.end method

.method public static a$redex0(LX/6IK;)V
    .locals 2

    .prologue
    .line 1073794
    iget-boolean v0, p0, LX/6IK;->c:Z

    if-nez v0, :cond_0

    .line 1073795
    iget-object v0, p0, LX/6IK;->a:LX/6IL;

    iget-object v0, v0, LX/6IL;->a:Landroid/view/View;

    iget v1, p0, LX/6IK;->b:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 1073796
    iget-object v0, p0, LX/6IK;->a:LX/6IL;

    invoke-static {v0}, LX/6IL;->c(LX/6IL;)V

    .line 1073797
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6IK;->c:Z

    .line 1073798
    :cond_0
    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1073799
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6IK;->c:Z

    .line 1073800
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1073801
    invoke-static {p0}, LX/6IK;->a$redex0(LX/6IK;)V

    .line 1073802
    iget-object v0, p0, LX/6IK;->a:LX/6IL;

    const/4 v1, 0x0

    .line 1073803
    iput-object v1, v0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    .line 1073804
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1073805
    invoke-static {p0}, LX/6IK;->a$redex0(LX/6IK;)V

    .line 1073806
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1073807
    return-void
.end method
