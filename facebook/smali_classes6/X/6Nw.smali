.class public LX/6Nw;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchContactParams;",
        "Lcom/facebook/contacts/server/FetchContactResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/3fb;

.field public final c:LX/3fc;


# direct methods
.method public constructor <init>(LX/3fb;LX/3fc;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084088
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 1084089
    iput-object p1, p0, LX/6Nw;->b:LX/3fb;

    .line 1084090
    iput-object p2, p0, LX/6Nw;->c:LX/3fc;

    .line 1084091
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1084078
    check-cast p1, Lcom/facebook/contacts/server/FetchContactParams;

    .line 1084079
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchContactParams;->a:Lcom/facebook/user/model/UserKey;

    move-object v5, v0

    .line 1084080
    const/4 v1, 0x0

    .line 1084081
    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v2, :cond_0

    .line 1084082
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactByProfileIdQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactByProfileIdQueryModel;

    .line 1084083
    if-eqz v0, :cond_1

    .line 1084084
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactByProfileIdQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    move-result-object v0

    .line 1084085
    :goto_0
    iget-object v1, p0, LX/6Nw;->b:LX/3fb;

    invoke-virtual {v1, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v0

    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v4

    .line 1084086
    new-instance v0, Lcom/facebook/contacts/server/FetchContactResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/contacts/server/FetchContactResult;-><init>(LX/0ta;JLcom/facebook/contacts/graphql/Contact;Lcom/facebook/user/model/UserKey;)V

    return-object v0

    .line 1084087
    :cond_0
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1084092
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 1084063
    check-cast p1, Lcom/facebook/contacts/server/FetchContactParams;

    .line 1084064
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchContactParams;->a:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1084065
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v1

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v1

    sget-object v2, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1084066
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v1

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v1, v2, :cond_2

    .line 1084067
    new-instance v1, LX/6M9;

    invoke-direct {v1}, LX/6M9;-><init>()V

    move-object v1, v1

    .line 1084068
    iget-object v2, p0, LX/6Nw;->c:LX/3fc;

    invoke-virtual {v2, v1}, LX/3fc;->a(LX/0gW;)V

    .line 1084069
    iget-object v2, p0, LX/6Nw;->c:LX/3fc;

    invoke-virtual {v2, v1}, LX/3fc;->c(LX/0gW;)V

    .line 1084070
    const-string v2, "profile_id"

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1084071
    :goto_1
    move-object v0, v1

    .line 1084072
    return-object v0

    .line 1084073
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1084074
    :cond_2
    new-instance v1, LX/6MA;

    invoke-direct {v1}, LX/6MA;-><init>()V

    move-object v1, v1

    .line 1084075
    const-string v2, "contact_id"

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1084076
    iget-object v2, p0, LX/6Nw;->c:LX/3fc;

    invoke-virtual {v2, v1}, LX/3fc;->a(LX/0gW;)V

    .line 1084077
    iget-object v2, p0, LX/6Nw;->c:LX/3fc;

    invoke-virtual {v2, v1}, LX/3fc;->c(LX/0gW;)V

    goto :goto_1
.end method
