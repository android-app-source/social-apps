.class public final LX/5Er;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 36

    .prologue
    .line 883945
    const/16 v29, 0x0

    .line 883946
    const/16 v28, 0x0

    .line 883947
    const-wide/16 v26, 0x0

    .line 883948
    const/16 v25, 0x0

    .line 883949
    const/16 v24, 0x0

    .line 883950
    const/16 v23, 0x0

    .line 883951
    const/16 v22, 0x0

    .line 883952
    const/16 v21, 0x0

    .line 883953
    const/16 v20, 0x0

    .line 883954
    const/16 v19, 0x0

    .line 883955
    const/16 v18, 0x0

    .line 883956
    const/16 v17, 0x0

    .line 883957
    const/16 v16, 0x0

    .line 883958
    const/4 v13, 0x0

    .line 883959
    const-wide/16 v14, 0x0

    .line 883960
    const/4 v12, 0x0

    .line 883961
    const/4 v11, 0x0

    .line 883962
    const/4 v10, 0x0

    .line 883963
    const/4 v9, 0x0

    .line 883964
    const/4 v8, 0x0

    .line 883965
    const/4 v7, 0x0

    .line 883966
    const/4 v6, 0x0

    .line 883967
    const/4 v5, 0x0

    .line 883968
    const/4 v4, 0x0

    .line 883969
    const/4 v3, 0x0

    .line 883970
    const/4 v2, 0x0

    .line 883971
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1c

    .line 883972
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 883973
    const/4 v2, 0x0

    .line 883974
    :goto_0
    return v2

    .line 883975
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_15

    .line 883976
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 883977
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 883978
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 883979
    const-string v7, "can_viewer_change_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 883980
    const/4 v2, 0x1

    .line 883981
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v31, v6

    move v6, v2

    goto :goto_1

    .line 883982
    :cond_1
    const-string v7, "connection_style"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 883983
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v30, v2

    goto :goto_1

    .line 883984
    :cond_2
    const-string v7, "end_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 883985
    const/4 v2, 0x1

    .line 883986
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 883987
    :cond_3
    const-string v7, "event_cover_photo"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 883988
    invoke-static/range {p0 .. p1}, LX/5En;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 883989
    :cond_4
    const-string v7, "event_members"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 883990
    invoke-static/range {p0 .. p1}, LX/5Eo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 883991
    :cond_5
    const-string v7, "event_place"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 883992
    invoke-static/range {p0 .. p1}, LX/5Fk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 883993
    :cond_6
    const-string v7, "event_watchers"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 883994
    invoke-static/range {p0 .. p1}, LX/5Ep;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 883995
    :cond_7
    const-string v7, "friendEventMembersFirst3"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 883996
    invoke-static/range {p0 .. p1}, LX/5Eu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 883997
    :cond_8
    const-string v7, "friendEventWatchersFirst3"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 883998
    invoke-static/range {p0 .. p1}, LX/5Ex;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 883999
    :cond_9
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 884000
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 884001
    :cond_a
    const-string v7, "is_all_day"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 884002
    const/4 v2, 0x1

    .line 884003
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v11, v2

    move/from16 v22, v7

    goto/16 :goto_1

    .line 884004
    :cond_b
    const-string v7, "is_canceled"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 884005
    const/4 v2, 0x1

    .line 884006
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v10, v2

    move/from16 v21, v7

    goto/16 :goto_1

    .line 884007
    :cond_c
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 884008
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 884009
    :cond_d
    const-string v7, "social_context"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 884010
    invoke-static/range {p0 .. p1}, LX/5Eq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 884011
    :cond_e
    const-string v7, "start_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 884012
    const/4 v2, 0x1

    .line 884013
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 884014
    :cond_f
    const-string v7, "timezone"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 884015
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 884016
    :cond_10
    const-string v7, "url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 884017
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 884018
    :cond_11
    const-string v7, "viewer_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 884019
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 884020
    :cond_12
    const-string v7, "viewer_has_pending_invite"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 884021
    const/4 v2, 0x1

    .line 884022
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v13, v7

    goto/16 :goto_1

    .line 884023
    :cond_13
    const-string v7, "viewer_watch_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 884024
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 884025
    :cond_14
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 884026
    :cond_15
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 884027
    if-eqz v6, :cond_16

    .line 884028
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 884029
    :cond_16
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884030
    if-eqz v3, :cond_17

    .line 884031
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 884032
    :cond_17
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884033
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884034
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884035
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884036
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884037
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884038
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884039
    if-eqz v11, :cond_18

    .line 884040
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 884041
    :cond_18
    if-eqz v10, :cond_19

    .line 884042
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 884043
    :cond_19
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884044
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884045
    if-eqz v9, :cond_1a

    .line 884046
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 884047
    :cond_1a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 884048
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 884049
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 884050
    if-eqz v8, :cond_1b

    .line 884051
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 884052
    :cond_1b
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 884053
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1c
    move/from16 v30, v28

    move/from16 v31, v29

    move/from16 v28, v24

    move/from16 v29, v25

    move/from16 v24, v20

    move/from16 v25, v21

    move/from16 v20, v16

    move/from16 v21, v17

    move/from16 v16, v12

    move/from16 v17, v13

    move v12, v8

    move v13, v9

    move v8, v2

    move v9, v3

    move v3, v6

    move v6, v7

    move/from16 v33, v19

    move-wide/from16 v34, v14

    move v15, v11

    move v14, v10

    move v10, v4

    move v11, v5

    move-wide/from16 v4, v26

    move/from16 v26, v22

    move/from16 v27, v23

    move/from16 v22, v18

    move/from16 v23, v33

    move-wide/from16 v18, v34

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x13

    const/16 v3, 0x11

    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 884054
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884055
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 884056
    if-eqz v0, :cond_0

    .line 884057
    const-string v1, "can_viewer_change_guest_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884058
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 884059
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 884060
    if-eqz v0, :cond_1

    .line 884061
    const-string v0, "connection_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884062
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884063
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 884064
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 884065
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884066
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 884067
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884068
    if-eqz v0, :cond_3

    .line 884069
    const-string v1, "event_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884070
    invoke-static {p0, v0, p2, p3}, LX/5En;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884071
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884072
    if-eqz v0, :cond_4

    .line 884073
    const-string v1, "event_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884074
    invoke-static {p0, v0, p2}, LX/5Eo;->a(LX/15i;ILX/0nX;)V

    .line 884075
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884076
    if-eqz v0, :cond_5

    .line 884077
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884078
    invoke-static {p0, v0, p2, p3}, LX/5Fk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884079
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884080
    if-eqz v0, :cond_6

    .line 884081
    const-string v1, "event_watchers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884082
    invoke-static {p0, v0, p2}, LX/5Ep;->a(LX/15i;ILX/0nX;)V

    .line 884083
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884084
    if-eqz v0, :cond_7

    .line 884085
    const-string v1, "friendEventMembersFirst3"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884086
    invoke-static {p0, v0, p2, p3}, LX/5Eu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884087
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884088
    if-eqz v0, :cond_8

    .line 884089
    const-string v1, "friendEventWatchersFirst3"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884090
    invoke-static {p0, v0, p2, p3}, LX/5Ex;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 884091
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884092
    if-eqz v0, :cond_9

    .line 884093
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884094
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884095
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 884096
    if-eqz v0, :cond_a

    .line 884097
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884098
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 884099
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 884100
    if-eqz v0, :cond_b

    .line 884101
    const-string v1, "is_canceled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884102
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 884103
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884104
    if-eqz v0, :cond_c

    .line 884105
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884106
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884107
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 884108
    if-eqz v0, :cond_d

    .line 884109
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884110
    invoke-static {p0, v0, p2}, LX/5Eq;->a(LX/15i;ILX/0nX;)V

    .line 884111
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 884112
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 884113
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884114
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 884115
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884116
    if-eqz v0, :cond_f

    .line 884117
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884118
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884119
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 884120
    if-eqz v0, :cond_10

    .line 884121
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884122
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884123
    :cond_10
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 884124
    if-eqz v0, :cond_11

    .line 884125
    const-string v0, "viewer_guest_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884126
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884127
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 884128
    if-eqz v0, :cond_12

    .line 884129
    const-string v1, "viewer_has_pending_invite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884130
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 884131
    :cond_12
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 884132
    if-eqz v0, :cond_13

    .line 884133
    const-string v0, "viewer_watch_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884134
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884135
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884136
    return-void
.end method
