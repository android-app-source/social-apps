.class public final LX/5ld;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 998881
    const/16 v31, 0x0

    .line 998882
    const/16 v30, 0x0

    .line 998883
    const/16 v29, 0x0

    .line 998884
    const/16 v28, 0x0

    .line 998885
    const/16 v27, 0x0

    .line 998886
    const/16 v26, 0x0

    .line 998887
    const/16 v25, 0x0

    .line 998888
    const/16 v24, 0x0

    .line 998889
    const/16 v23, 0x0

    .line 998890
    const/16 v22, 0x0

    .line 998891
    const/16 v21, 0x0

    .line 998892
    const/16 v20, 0x0

    .line 998893
    const/16 v19, 0x0

    .line 998894
    const/16 v18, 0x0

    .line 998895
    const/16 v17, 0x0

    .line 998896
    const/16 v16, 0x0

    .line 998897
    const/4 v15, 0x0

    .line 998898
    const/4 v14, 0x0

    .line 998899
    const/4 v13, 0x0

    .line 998900
    const/4 v12, 0x0

    .line 998901
    const/4 v11, 0x0

    .line 998902
    const/4 v10, 0x0

    .line 998903
    const/4 v9, 0x0

    .line 998904
    const/4 v8, 0x0

    .line 998905
    const/4 v7, 0x0

    .line 998906
    const/4 v6, 0x0

    .line 998907
    const/4 v5, 0x0

    .line 998908
    const/4 v4, 0x0

    .line 998909
    const/4 v3, 0x0

    .line 998910
    const/4 v2, 0x0

    .line 998911
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    .line 998912
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 998913
    const/4 v2, 0x0

    .line 998914
    :goto_0
    return v2

    .line 998915
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 998916
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_15

    .line 998917
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v32

    .line 998918
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 998919
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    if-eqz v32, :cond_1

    .line 998920
    const-string v33, "can_see_voice_switcher"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_2

    .line 998921
    const/4 v11, 0x1

    .line 998922
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto :goto_1

    .line 998923
    :cond_2
    const-string v33, "can_viewer_comment"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_3

    .line 998924
    const/4 v10, 0x1

    .line 998925
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto :goto_1

    .line 998926
    :cond_3
    const-string v33, "can_viewer_comment_with_photo"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_4

    .line 998927
    const/4 v9, 0x1

    .line 998928
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto :goto_1

    .line 998929
    :cond_4
    const-string v33, "can_viewer_comment_with_video"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_5

    .line 998930
    const/4 v8, 0x1

    .line 998931
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 998932
    :cond_5
    const-string v33, "can_viewer_like"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_6

    .line 998933
    const/4 v7, 0x1

    .line 998934
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 998935
    :cond_6
    const-string v33, "can_viewer_react"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_7

    .line 998936
    const/4 v6, 0x1

    .line 998937
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 998938
    :cond_7
    const-string v33, "can_viewer_subscribe"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_8

    .line 998939
    const/4 v5, 0x1

    .line 998940
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 998941
    :cond_8
    const-string v33, "does_viewer_like"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_9

    .line 998942
    const/4 v4, 0x1

    .line 998943
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 998944
    :cond_9
    const-string v33, "id"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_a

    .line 998945
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 998946
    :cond_a
    const-string v33, "important_reactors"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_b

    .line 998947
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 998948
    :cond_b
    const-string v33, "is_viewer_subscribed"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_c

    .line 998949
    const/4 v3, 0x1

    .line 998950
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 998951
    :cond_c
    const-string v33, "legacy_api_post_id"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 998952
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 998953
    :cond_d
    const-string v33, "likers"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_e

    .line 998954
    invoke-static/range {p0 .. p1}, LX/5lb;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 998955
    :cond_e
    const-string v33, "reactors"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_f

    .line 998956
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 998957
    :cond_f
    const-string v33, "remixable_photo_uri"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_10

    .line 998958
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 998959
    :cond_10
    const-string v33, "supported_reactions"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_11

    .line 998960
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 998961
    :cond_11
    const-string v33, "top_level_comments"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_12

    .line 998962
    invoke-static/range {p0 .. p1}, LX/5lc;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 998963
    :cond_12
    const-string v33, "top_reactions"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_13

    .line 998964
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 998965
    :cond_13
    const-string v33, "viewer_acts_as_person"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_14

    .line 998966
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 998967
    :cond_14
    const-string v33, "viewer_feedback_reaction_key"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 998968
    const/4 v2, 0x1

    .line 998969
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 998970
    :cond_15
    const/16 v32, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 998971
    if-eqz v11, :cond_16

    .line 998972
    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 998973
    :cond_16
    if-eqz v10, :cond_17

    .line 998974
    const/4 v10, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 998975
    :cond_17
    if-eqz v9, :cond_18

    .line 998976
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 998977
    :cond_18
    if-eqz v8, :cond_19

    .line 998978
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 998979
    :cond_19
    if-eqz v7, :cond_1a

    .line 998980
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 998981
    :cond_1a
    if-eqz v6, :cond_1b

    .line 998982
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 998983
    :cond_1b
    if-eqz v5, :cond_1c

    .line 998984
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 998985
    :cond_1c
    if-eqz v4, :cond_1d

    .line 998986
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 998987
    :cond_1d
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 998988
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 998989
    if-eqz v3, :cond_1e

    .line 998990
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 998991
    :cond_1e
    const/16 v3, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 998992
    const/16 v3, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 998993
    const/16 v3, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 998994
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 998995
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 998996
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 998997
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 998998
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 998999
    if-eqz v2, :cond_1f

    .line 999000
    const/16 v2, 0x13

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v3}, LX/186;->a(III)V

    .line 999001
    :cond_1f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 999002
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 999003
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 999004
    if-eqz v0, :cond_0

    .line 999005
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999006
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999007
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999008
    if-eqz v0, :cond_1

    .line 999009
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999010
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999011
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999012
    if-eqz v0, :cond_2

    .line 999013
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999014
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999015
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999016
    if-eqz v0, :cond_3

    .line 999017
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999018
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999019
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999020
    if-eqz v0, :cond_4

    .line 999021
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999022
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999023
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999024
    if-eqz v0, :cond_5

    .line 999025
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999026
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999027
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999028
    if-eqz v0, :cond_6

    .line 999029
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999030
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999031
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999032
    if-eqz v0, :cond_7

    .line 999033
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999034
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999035
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999036
    if-eqz v0, :cond_8

    .line 999037
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999038
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999039
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999040
    if-eqz v0, :cond_9

    .line 999041
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999042
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 999043
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 999044
    if-eqz v0, :cond_a

    .line 999045
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999046
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 999047
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999048
    if-eqz v0, :cond_b

    .line 999049
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999050
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999051
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999052
    if-eqz v0, :cond_c

    .line 999053
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999054
    invoke-static {p0, v0, p2}, LX/5lb;->a(LX/15i;ILX/0nX;)V

    .line 999055
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999056
    if-eqz v0, :cond_d

    .line 999057
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999058
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 999059
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 999060
    if-eqz v0, :cond_e

    .line 999061
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999062
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 999063
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999064
    if-eqz v0, :cond_f

    .line 999065
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999066
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 999067
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999068
    if-eqz v0, :cond_10

    .line 999069
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999070
    invoke-static {p0, v0, p2}, LX/5lc;->a(LX/15i;ILX/0nX;)V

    .line 999071
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999072
    if-eqz v0, :cond_11

    .line 999073
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999074
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 999075
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 999076
    if-eqz v0, :cond_12

    .line 999077
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999078
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 999079
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 999080
    if-eqz v0, :cond_13

    .line 999081
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 999082
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 999083
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 999084
    return-void
.end method
