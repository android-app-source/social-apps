.class public LX/5Qo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/5Qo;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 913794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913795
    return-void
.end method

.method public static a(LX/0QB;)LX/5Qo;
    .locals 3

    .prologue
    .line 913796
    sget-object v0, LX/5Qo;->a:LX/5Qo;

    if-nez v0, :cond_1

    .line 913797
    const-class v1, LX/5Qo;

    monitor-enter v1

    .line 913798
    :try_start_0
    sget-object v0, LX/5Qo;->a:LX/5Qo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 913799
    if-eqz v2, :cond_0

    .line 913800
    :try_start_1
    new-instance v0, LX/5Qo;

    invoke-direct {v0}, LX/5Qo;-><init>()V

    .line 913801
    move-object v0, v0

    .line 913802
    sput-object v0, LX/5Qo;->a:LX/5Qo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 913803
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 913804
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 913805
    :cond_1
    sget-object v0, LX/5Qo;->a:LX/5Qo;

    return-object v0

    .line 913806
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 913807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 913808
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
