.class public final LX/5CQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 867754
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 867755
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 867756
    :goto_0
    return v6

    .line 867757
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 867758
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 867759
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 867760
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 867761
    const-string v10, "scale"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 867762
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v8, v7

    move v7, v1

    goto :goto_1

    .line 867763
    :cond_1
    const-string v10, "value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 867764
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 867765
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 867766
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 867767
    if-eqz v7, :cond_4

    .line 867768
    invoke-virtual {p1, v6, v8, v6}, LX/186;->a(III)V

    .line 867769
    :cond_4
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 867770
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 867771
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_6
    move v0, v6

    move v7, v6

    move-wide v2, v4

    move v8, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 867772
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 867773
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 867774
    if-eqz v0, :cond_0

    .line 867775
    const-string v1, "scale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867776
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 867777
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 867778
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 867779
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867780
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 867781
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 867782
    return-void
.end method
