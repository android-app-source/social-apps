.class public final enum LX/5hJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5hJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5hJ;

.field public static final enum ALBUM_MEDIASET:LX/5hJ;

.field public static final enum MEDIASET:LX/5hJ;

.field public static final enum PHOTOS_TAKEN_HERE:LX/5hJ;

.field public static final enum PHOTOS_TAKEN_OF:LX/5hJ;

.field public static final enum POSTED_PHOTOS:LX/5hJ;

.field public static final enum TAGGED_MEDIASET:LX/5hJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 978313
    new-instance v0, LX/5hJ;

    const-string v1, "TAGGED_MEDIASET"

    invoke-direct {v0, v1, v3}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->TAGGED_MEDIASET:LX/5hJ;

    .line 978314
    new-instance v0, LX/5hJ;

    const-string v1, "POSTED_PHOTOS"

    invoke-direct {v0, v1, v4}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->POSTED_PHOTOS:LX/5hJ;

    .line 978315
    new-instance v0, LX/5hJ;

    const-string v1, "PHOTOS_TAKEN_OF"

    invoke-direct {v0, v1, v5}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->PHOTOS_TAKEN_OF:LX/5hJ;

    .line 978316
    new-instance v0, LX/5hJ;

    const-string v1, "PHOTOS_TAKEN_HERE"

    invoke-direct {v0, v1, v6}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->PHOTOS_TAKEN_HERE:LX/5hJ;

    .line 978317
    new-instance v0, LX/5hJ;

    const-string v1, "MEDIASET"

    invoke-direct {v0, v1, v7}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->MEDIASET:LX/5hJ;

    .line 978318
    new-instance v0, LX/5hJ;

    const-string v1, "ALBUM_MEDIASET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5hJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5hJ;->ALBUM_MEDIASET:LX/5hJ;

    .line 978319
    const/4 v0, 0x6

    new-array v0, v0, [LX/5hJ;

    sget-object v1, LX/5hJ;->TAGGED_MEDIASET:LX/5hJ;

    aput-object v1, v0, v3

    sget-object v1, LX/5hJ;->POSTED_PHOTOS:LX/5hJ;

    aput-object v1, v0, v4

    sget-object v1, LX/5hJ;->PHOTOS_TAKEN_OF:LX/5hJ;

    aput-object v1, v0, v5

    sget-object v1, LX/5hJ;->PHOTOS_TAKEN_HERE:LX/5hJ;

    aput-object v1, v0, v6

    sget-object v1, LX/5hJ;->MEDIASET:LX/5hJ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5hJ;->ALBUM_MEDIASET:LX/5hJ;

    aput-object v2, v0, v1

    sput-object v0, LX/5hJ;->$VALUES:[LX/5hJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 978310
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5hJ;
    .locals 1

    .prologue
    .line 978312
    const-class v0, LX/5hJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5hJ;

    return-object v0
.end method

.method public static values()[LX/5hJ;
    .locals 1

    .prologue
    .line 978311
    sget-object v0, LX/5hJ;->$VALUES:[LX/5hJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5hJ;

    return-object v0
.end method
