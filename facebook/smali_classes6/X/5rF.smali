.class public LX/5rF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/react/uimanager/ReactShadowNode;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/util/SparseBooleanArray;

.field private final c:LX/5pu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1011079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011080
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/5rF;->a:Landroid/util/SparseArray;

    .line 1011081
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    .line 1011082
    new-instance v0, LX/5pu;

    invoke-direct {v0}, LX/5pu;-><init>()V

    iput-object v0, p0, LX/5rF;->c:LX/5pu;

    .line 1011083
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1011084
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011085
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1011073
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011074
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011075
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View with tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not registered as a root view"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011076
    :cond_0
    iget-object v0, p0, LX/5rF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 1011077
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 1011078
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 3

    .prologue
    .line 1011069
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 1011070
    iget-object v1, p0, LX/5rF;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1011071
    iget-object v1, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1011072
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1011086
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011087
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011088
    new-instance v0, LX/5qo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to remove root node "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " without using removeRootNode!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011089
    :cond_0
    iget-object v0, p0, LX/5rF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 1011090
    return-void
.end method

.method public final b(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 2

    .prologue
    .line 1011064
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011065
    iget-object v0, p0, LX/5rF;->a:Landroid/util/SparseArray;

    .line 1011066
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 1011067
    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1011068
    return-void
.end method

.method public final c(I)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 1011062
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011063
    iget-object v0, p0, LX/5rF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    return-object v0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 1011058
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011059
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 1011060
    iget-object v0, p0, LX/5rF;->c:LX/5pu;

    invoke-virtual {v0}, LX/5pu;->a()V

    .line 1011061
    iget-object v0, p0, LX/5rF;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    return v0
.end method
