.class public LX/6KD;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/6KD;

.field public static b:LX/6KB;

.field private static final c:LX/6KC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1076481
    new-instance v0, LX/6KC;

    invoke-direct {v0}, LX/6KC;-><init>()V

    sput-object v0, LX/6KD;->c:LX/6KC;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1076479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076480
    return-void
.end method

.method public static a(LX/6KT;)LX/6KD;
    .locals 8
    .param p0    # LX/6KT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1076483
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1076484
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "API not called on UI Thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076485
    :cond_0
    sget-object v0, LX/6KD;->a:LX/6KD;

    if-nez v0, :cond_2

    .line 1076486
    new-instance v0, LX/6KD;

    invoke-direct {v0}, LX/6KD;-><init>()V

    sput-object v0, LX/6KD;->a:LX/6KD;

    .line 1076487
    new-instance v0, LX/6KA;

    invoke-direct {v0}, LX/6KA;-><init>()V

    new-instance v1, LX/6KE;

    invoke-direct {v1}, LX/6KE;-><init>()V

    const/4 v3, 0x0

    .line 1076488
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1076489
    new-instance v4, LX/6KG;

    const-string v5, "*"

    const-string v6, "HTC One M9"

    const-string v7, "HTC"

    invoke-direct {v4, v5, v6, v7}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076490
    new-instance v4, LX/6KG;

    const-string v5, "*"

    const-string v6, "X9009"

    const-string v7, "Oppo"

    invoke-direct {v4, v5, v6, v7}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076491
    new-instance v4, LX/6KG;

    const-string v5, "*"

    const-string v6, "SM-N910"

    const-string v7, "Samsung"

    invoke-direct {v4, v5, v6, v7}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076492
    new-instance v4, LX/6KG;

    const-string v5, "5.0.2"

    const-string v6, "SM-G92"

    const-string v7, "Samsung"

    invoke-direct {v4, v5, v6, v7}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076493
    new-instance v4, LX/6KG;

    const-string v5, "5.0"

    const-string v6, ""

    const-string v7, ""

    invoke-direct {v4, v5, v6, v7}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076494
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 1076495
    :goto_0
    move v2, v2

    .line 1076496
    iput-boolean v2, v1, LX/6KE;->a:Z

    .line 1076497
    move-object v1, v1

    .line 1076498
    invoke-virtual {v1}, LX/6KE;->a()LX/6KF;

    move-result-object v1

    .line 1076499
    iput-object v1, v0, LX/6KA;->a:LX/6KF;

    .line 1076500
    move-object v0, v0

    .line 1076501
    new-instance v1, LX/6KJ;

    invoke-direct {v1}, LX/6KJ;-><init>()V

    .line 1076502
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1076503
    new-instance v3, LX/6KG;

    const-string v4, "7.0"

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6}, LX/6KG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076504
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6KG;

    .line 1076505
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v5, "7.0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1076506
    const/4 v4, 0x1

    .line 1076507
    :goto_1
    move v2, v4

    .line 1076508
    if-eqz v2, :cond_1

    .line 1076509
    const-string v2, "baseline"

    .line 1076510
    :goto_2
    move-object v2, v2

    .line 1076511
    iput-object v2, v1, LX/6KJ;->a:Ljava/lang/String;

    .line 1076512
    move-object v1, v1

    .line 1076513
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_9

    const/4 v2, 0x1

    :goto_3
    move v2, v2

    .line 1076514
    iput-boolean v2, v1, LX/6KJ;->b:Z

    .line 1076515
    move-object v1, v1

    .line 1076516
    invoke-virtual {v1}, LX/6KJ;->a()LX/6KK;

    move-result-object v1

    .line 1076517
    iput-object v1, v0, LX/6KA;->b:LX/6KK;

    .line 1076518
    move-object v0, v0

    .line 1076519
    invoke-virtual {v0}, LX/6KA;->a()LX/6KB;

    move-result-object v0

    move-object v0, v0

    .line 1076520
    sput-object v0, LX/6KD;->b:LX/6KB;

    .line 1076521
    if-eqz p0, :cond_2

    .line 1076522
    sget-object v0, LX/6KD;->c:LX/6KC;

    invoke-virtual {p0, v0}, LX/6KT;->a(LX/6KC;)V

    .line 1076523
    :cond_2
    sget-object v0, LX/6KD;->a:LX/6KD;

    return-object v0

    .line 1076524
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6KG;

    .line 1076525
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v6, "5.0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1076526
    const/4 v5, 0x1

    .line 1076527
    :goto_4
    move v2, v5

    .line 1076528
    if-eqz v2, :cond_4

    .line 1076529
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 1076530
    goto/16 :goto_0

    :cond_6
    invoke-static {v2}, LX/6KH;->c(LX/6KG;)Z

    move-result v5

    goto :goto_4

    :cond_7
    const-string v2, "high"

    goto :goto_2

    :cond_8
    invoke-static {v2}, LX/6KH;->c(LX/6KG;)Z

    move-result v4

    goto :goto_1

    :cond_9
    const/4 v2, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()LX/6KB;
    .locals 1

    .prologue
    .line 1076482
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/6KD;->b:LX/6KB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
