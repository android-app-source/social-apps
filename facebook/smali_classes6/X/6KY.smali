.class public LX/6KY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/61B;

.field public b:LX/61G;

.field public c:LX/6KI;

.field public d:LX/61H;

.field public e:Z


# direct methods
.method public constructor <init>(LX/61B;)V
    .locals 2

    .prologue
    .line 1076787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076788
    iput-object p1, p0, LX/6KY;->a:LX/61B;

    .line 1076789
    instance-of v0, p1, LX/61G;

    if-eqz v0, :cond_0

    .line 1076790
    check-cast p1, LX/61G;

    iput-object p1, p0, LX/6KY;->b:LX/61G;

    .line 1076791
    iget-object v0, p0, LX/6KY;->b:LX/61G;

    new-instance v1, LX/6KX;

    invoke-direct {v1, p0}, LX/6KX;-><init>(LX/6KY;)V

    invoke-interface {v0, v1}, LX/61G;->a(LX/61H;)V

    .line 1076792
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6KY;->e:Z

    .line 1076793
    new-instance v0, LX/6KI;

    invoke-direct {v0}, LX/6KI;-><init>()V

    iput-object v0, p0, LX/6KY;->c:LX/6KI;

    .line 1076794
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1076785
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    invoke-interface {v0}, LX/61B;->b()V

    .line 1076786
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1076783
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    invoke-interface {v0, p1, p2}, LX/61B;->a(II)V

    .line 1076784
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 1

    .prologue
    .line 1076780
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    invoke-interface {v0, p1}, LX/61B;->a(LX/5Pc;)V

    .line 1076781
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6KY;->e:Z

    .line 1076782
    return-void
.end method

.method public final a(ZLX/5Pc;)V
    .locals 1

    .prologue
    .line 1076767
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    instance-of v0, v0, LX/61C;

    if-eqz v0, :cond_0

    .line 1076768
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    check-cast v0, LX/61C;

    invoke-interface {v0, p1, p2}, LX/61C;->a(ZLX/5Pc;)V

    .line 1076769
    :cond_0
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1076775
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 1076776
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, LX/61B;->a([F[F[FJ)V

    .line 1076777
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 1076778
    iget-object v2, p0, LX/6KY;->c:LX/6KI;

    sub-long/2addr v0, v6

    invoke-virtual {v2, v0, v1}, LX/6KI;->a(J)V

    .line 1076779
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1076774
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    invoke-interface {v0}, LX/61B;->c()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1076773
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    instance-of v0, v0, LX/61C;

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1076770
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    instance-of v0, v0, LX/61C;

    if-eqz v0, :cond_0

    .line 1076771
    iget-object v0, p0, LX/6KY;->a:LX/61B;

    check-cast v0, LX/61C;

    invoke-interface {v0}, LX/61C;->a()Z

    move-result v0

    .line 1076772
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
