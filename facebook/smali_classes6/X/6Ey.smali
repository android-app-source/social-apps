.class public final LX/6Ey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ex;


# instance fields
.field public final synthetic a:LX/6Ez;


# direct methods
.method public constructor <init>(LX/6Ez;)V
    .locals 0

    .prologue
    .line 1066890
    iput-object p1, p0, LX/6Ey;->a:LX/6Ez;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1066888
    iget-object v0, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v0, v0, LX/6Ez;->g:LX/6Ex;

    invoke-interface {v0}, LX/6Ex;->a()V

    .line 1066889
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 12

    .prologue
    .line 1066832
    iget-object v0, p0, LX/6Ey;->a:LX/6Ez;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;->a()Ljava/lang/String;

    move-result-object v1

    .line 1066833
    iput-object v1, v0, LX/6Ez;->h:Ljava/lang/String;

    .line 1066834
    move-object v0, p1

    .line 1066835
    check-cast v0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    .line 1066836
    iget-object v1, v0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    move-object v0, v1

    .line 1066837
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1066838
    iget-object v1, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v1, v1, LX/6Ez;->e:LX/6Et;

    iget-object v2, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v2, v2, LX/6Ez;->h:Ljava/lang/String;

    iget-object v3, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v3, v3, LX/6Ez;->f:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1066839
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1066840
    :try_start_0
    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v5

    .line 1066841
    iget-object v6, v5, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    invoke-static {v6}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v6

    .line 1066842
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1066843
    invoke-static {}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->newBuilder()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;

    move-result-object v7

    .line 1066844
    iget-object v8, v6, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v8, v8

    .line 1066845
    invoke-virtual {v8}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->setAmount(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;

    move-result-object v7

    .line 1066846
    iget-object v8, v6, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v6, v8

    .line 1066847
    invoke-virtual {v7, v6}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->setCurrency(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    move-result-object v6

    .line 1066848
    invoke-static {}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->newBuilder()Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;->setTotal_currency_amount(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;)Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    move-result-object v6

    .line 1066849
    new-instance v7, Lorg/json/JSONObject;

    iget-object v8, v1, LX/6Et;->b:LX/0lB;

    invoke-virtual {v8, v6}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v5, v7

    .line 1066850
    const/4 v7, 0x0

    .line 1066851
    invoke-static {}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->newBuilder()Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    move-result-object v6

    invoke-static {v3}, LX/6Et;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->setShippingAddress(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    move-result-object v8

    .line 1066852
    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v6

    .line 1066853
    invoke-static {v6}, LX/47j;->a(LX/0am;)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v6, v7

    :goto_0
    invoke-virtual {v8, v6}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->setShippingOption(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    .line 1066854
    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object v6

    .line 1066855
    invoke-static {v6}, LX/47j;->a(LX/0am;)Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v6, v7

    :goto_1
    invoke-virtual {v8, v6}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->setContactEmail(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    .line 1066856
    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->m()LX/0am;

    move-result-object v6

    .line 1066857
    invoke-static {v6}, LX/47j;->a(LX/0am;)Z

    move-result v9

    if-eqz v9, :cond_5

    :goto_2
    invoke-virtual {v8, v7}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->setContactPhone(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    .line 1066858
    new-instance v6, Lorg/json/JSONObject;

    iget-object v7, v1, LX/6Et;->b:LX/0lB;

    invoke-virtual {v8}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1066859
    invoke-static {v0}, LX/6Et;->a(LX/0lF;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1066860
    const-string v7, "payment_data"

    new-instance v8, Lorg/json/JSONObject;

    iget-object v9, v1, LX/6Et;->b:LX/0lB;

    const-string v10, "payment_data"

    invoke-virtual {v0, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1066861
    :cond_0
    move-object v6, v6

    .line 1066862
    const-string v7, "order_info"

    invoke-virtual {v4, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1066863
    const-string v5, "collected_purchase_info"

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1066864
    const-string v5, "payment_id"

    invoke-virtual {v4, v5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066865
    :goto_3
    move-object v1, v4

    .line 1066866
    if-nez v1, :cond_1

    .line 1066867
    iget-object v2, p0, LX/6Ey;->a:LX/6Ez;

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/6Ez;->b$redex0(LX/6Ez;Ljava/lang/String;)V

    .line 1066868
    :cond_1
    iget-object v2, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v2, v2, LX/6Ez;->a:LX/6F6;

    iget-object v3, p0, LX/6Ey;->a:LX/6Ez;

    .line 1066869
    iget-object v4, v2, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    const-string v5, "handleChargeRequest"

    invoke-virtual {v4, v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1066870
    :goto_4
    invoke-static {v0}, LX/6Et;->a(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1066871
    iget-object v0, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v0, v0, LX/6Ez;->g:LX/6Ex;

    invoke-interface {v0, p1}, LX/6Ex;->a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 1066872
    :cond_2
    return-void

    .line 1066873
    :catch_0
    move-exception v4

    .line 1066874
    sget-object v5, LX/6Et;->a:Ljava/lang/String;

    const-string v6, "Exception serializing getPayloadForHandleChargeRequest!"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v4, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1066875
    const/4 v4, 0x0

    goto :goto_3

    .line 1066876
    :cond_3
    :try_start_1
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-interface {v6}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1066877
    :cond_4
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v6}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 1066878
    :cond_5
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v6}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1066879
    :cond_6
    iput-object v3, v2, LX/6F6;->c:LX/6Ez;

    .line 1066880
    iget-object v4, v2, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    iget-object v5, v2, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {v5}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v5

    const-string v6, "chargeRequest"

    .line 1066881
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 1066882
    :try_start_2
    const-string v7, "chargeRequest"

    invoke-virtual {v8, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1066883
    :goto_5
    move-object v7, v8

    .line 1066884
    invoke-static {v5, v6, v7}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1066885
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/facebook/browserextensions/common/payments/PaymentsCheckoutDataHandler$1;

    invoke-direct {v5, v2}, Lcom/facebook/browserextensions/common/payments/PaymentsCheckoutDataHandler$1;-><init>(LX/6F6;)V

    const-wide/16 v6, 0x3e8

    iget-object v8, v2, LX/6F6;->a:LX/0W3;

    sget-wide v10, LX/0X5;->fv:J

    const/16 v9, 0x3c

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JI)I

    move-result v8

    int-to-long v8, v8

    mul-long/2addr v6, v8

    const v8, -0xb8b2401

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_4

    .line 1066886
    :catch_1
    move-exception v7

    .line 1066887
    const-string v9, "paymentsCheckout"

    const-string v10, "Exception serializing return params!"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v7, v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1066828
    iget-object v0, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v0, v0, LX/6Ez;->g:LX/6Ex;

    invoke-interface {v0, p1}, LX/6Ex;->a(Ljava/lang/String;)V

    .line 1066829
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1066830
    iget-object v0, p0, LX/6Ey;->a:LX/6Ez;

    iget-object v0, v0, LX/6Ez;->g:LX/6Ex;

    invoke-interface {v0, p1}, LX/6Ex;->a(Ljava/lang/Throwable;)V

    .line 1066831
    return-void
.end method
