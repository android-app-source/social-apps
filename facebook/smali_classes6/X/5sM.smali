.class public final enum LX/5sM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5sM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5sM;

.field public static final enum EASE_IN:LX/5sM;

.field public static final enum EASE_IN_EASE_OUT:LX/5sM;

.field public static final enum EASE_OUT:LX/5sM;

.field public static final enum LINEAR:LX/5sM;

.field public static final enum SPRING:LX/5sM;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1012775
    new-instance v0, LX/5sM;

    const-string v1, "LINEAR"

    const-string v2, "linear"

    invoke-direct {v0, v1, v3, v2}, LX/5sM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sM;->LINEAR:LX/5sM;

    .line 1012776
    new-instance v0, LX/5sM;

    const-string v1, "EASE_IN"

    const-string v2, "easeIn"

    invoke-direct {v0, v1, v4, v2}, LX/5sM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sM;->EASE_IN:LX/5sM;

    .line 1012777
    new-instance v0, LX/5sM;

    const-string v1, "EASE_OUT"

    const-string v2, "easeOut"

    invoke-direct {v0, v1, v5, v2}, LX/5sM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sM;->EASE_OUT:LX/5sM;

    .line 1012778
    new-instance v0, LX/5sM;

    const-string v1, "EASE_IN_EASE_OUT"

    const-string v2, "easeInEaseOut"

    invoke-direct {v0, v1, v6, v2}, LX/5sM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sM;->EASE_IN_EASE_OUT:LX/5sM;

    .line 1012779
    new-instance v0, LX/5sM;

    const-string v1, "SPRING"

    const-string v2, "spring"

    invoke-direct {v0, v1, v7, v2}, LX/5sM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sM;->SPRING:LX/5sM;

    .line 1012780
    const/4 v0, 0x5

    new-array v0, v0, [LX/5sM;

    sget-object v1, LX/5sM;->LINEAR:LX/5sM;

    aput-object v1, v0, v3

    sget-object v1, LX/5sM;->EASE_IN:LX/5sM;

    aput-object v1, v0, v4

    sget-object v1, LX/5sM;->EASE_OUT:LX/5sM;

    aput-object v1, v0, v5

    sget-object v1, LX/5sM;->EASE_IN_EASE_OUT:LX/5sM;

    aput-object v1, v0, v6

    sget-object v1, LX/5sM;->SPRING:LX/5sM;

    aput-object v1, v0, v7

    sput-object v0, LX/5sM;->$VALUES:[LX/5sM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1012764
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1012765
    iput-object p3, p0, LX/5sM;->mName:Ljava/lang/String;

    .line 1012766
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/5sM;
    .locals 5

    .prologue
    .line 1012770
    invoke-static {}, LX/5sM;->values()[LX/5sM;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1012771
    invoke-virtual {v3}, LX/5sM;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1012772
    return-object v3

    .line 1012773
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012774
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported interpolation type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5sM;
    .locals 1

    .prologue
    .line 1012769
    const-class v0, LX/5sM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5sM;

    return-object v0
.end method

.method public static values()[LX/5sM;
    .locals 1

    .prologue
    .line 1012768
    sget-object v0, LX/5sM;->$VALUES:[LX/5sM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5sM;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012767
    iget-object v0, p0, LX/5sM;->mName:Ljava/lang/String;

    return-object v0
.end method
