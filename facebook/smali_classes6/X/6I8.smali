.class public final LX/6I8;
.super LX/6I7;
.source ""


# instance fields
.field public final synthetic a:LX/6IA;


# direct methods
.method public constructor <init>(LX/6IA;LX/03V;)V
    .locals 0

    .prologue
    .line 1073136
    iput-object p1, p0, LX/6I8;->a:LX/6IA;

    .line 1073137
    invoke-direct {p0, p2}, LX/6I7;-><init>(LX/03V;)V

    .line 1073138
    return-void
.end method


# virtual methods
.method public final a(LX/6II;)V
    .locals 6

    .prologue
    .line 1073139
    iget-object v0, p0, LX/6I8;->a:LX/6IA;

    const/4 p0, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    .line 1073140
    sget-object v1, LX/6Hw;->a:[I

    invoke-virtual {p1}, LX/6II;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1073141
    :goto_0
    iget-object v1, v0, LX/6IA;->Z:LX/6I6;

    invoke-virtual {v1}, LX/6I6;->a()V

    .line 1073142
    return-void

    .line 1073143
    :pswitch_0
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setClickable(Z)V

    .line 1073144
    iget-object v1, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1073145
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1073146
    iget-object v1, v0, LX/6IA;->m:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 1073147
    :pswitch_1
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 1073148
    iget-object v1, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1073149
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1073150
    iget-object v1, v0, LX/6IA;->m:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 1073151
    :pswitch_2
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 1073152
    iget-object v1, v0, LX/6IA;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1073153
    iget-object v1, v0, LX/6IA;->l:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1073154
    iget-object v1, v0, LX/6IA;->m:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
