.class public LX/5jL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:F

.field public final b:F

.field public final c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public final d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public final e:F

.field public final f:F


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;)V
    .locals 2

    .prologue
    .line 986359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986360
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->a:F

    .line 986361
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;->c()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->b:F

    .line 986362
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jL;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 986363
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jL;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 986364
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->e:F

    .line 986365
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;->d()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->f:F

    .line 986366
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;)V
    .locals 2

    .prologue
    .line 986367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986368
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->a:F

    .line 986369
    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;->c()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->b:F

    .line 986370
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jL;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 986371
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    iput-object v0, p0, LX/5jL;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 986372
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->e:F

    .line 986373
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;->d()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/5jL;->f:F

    .line 986374
    return-void
.end method
