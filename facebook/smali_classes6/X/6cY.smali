.class public LX/6cY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6cX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6cX",
        "<",
        "LX/6mi;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1sq;

.field private b:LX/6mi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1114491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114492
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    iput-object v0, p0, LX/6cY;->a:LX/1sq;

    return-void
.end method


# virtual methods
.method public final a([B)V
    .locals 8

    .prologue
    .line 1114493
    iget-object v0, p0, LX/6cY;->a:LX/1sq;

    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 1114494
    :try_start_0
    invoke-static {v0}, LX/3ll;->b(LX/1su;)LX/3ll;

    .line 1114495
    const/4 v4, 0x0

    .line 1114496
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 1114497
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 1114498
    iget-byte v6, v5, LX/1sw;->b:B

    if-eqz v6, :cond_1

    .line 1114499
    iget-short v6, v5, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_0

    .line 1114500
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {v0, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1114501
    :pswitch_0
    iget-byte v6, v5, LX/1sw;->b:B

    const/16 v7, 0xa

    if-ne v6, v7, :cond_0

    .line 1114502
    invoke-virtual {v0}, LX/1su;->n()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1114503
    :cond_0
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {v0, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1114504
    :cond_1
    invoke-virtual {v0}, LX/1su;->e()V

    .line 1114505
    new-instance v5, LX/6mi;

    invoke-direct {v5, v4}, LX/6mi;-><init>(Ljava/lang/Long;)V

    .line 1114506
    move-object v0, v5

    .line 1114507
    iput-object v0, p0, LX/6cY;->b:LX/6mi;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 1114508
    :goto_1
    return-void

    .line 1114509
    :catch_0
    move-exception v0

    .line 1114510
    const-string v1, "TimeSyncMqttResponseProcessorCallback"

    const-string v2, "setPayload failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1114511
    iget-object v0, p0, LX/6cY;->b:LX/6mi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1114512
    iget-object v0, p0, LX/6cY;->b:LX/6mi;

    return-object v0
.end method
