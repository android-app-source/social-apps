.class public final LX/57E;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 841044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 841045
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841046
    :goto_0
    return v1

    .line 841047
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 841048
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 841049
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 841050
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 841051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 841052
    const-string v7, "default_group_name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 841053
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 841054
    :cond_2
    const-string v7, "default_visibility"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 841055
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 841056
    :cond_3
    const-string v7, "suggested_members"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 841057
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 841058
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_4

    .line 841059
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_4

    .line 841060
    invoke-static {p0, p1}, LX/57D;->b(LX/15w;LX/186;)I

    move-result v6

    .line 841061
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 841062
    :cond_4
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 841063
    goto :goto_1

    .line 841064
    :cond_5
    const-string v7, "suggestion_identifier"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 841065
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 841066
    :cond_6
    const-string v7, "suggestion_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 841067
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto/16 :goto_1

    .line 841068
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 841069
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 841070
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 841071
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 841072
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 841073
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 841074
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 841075
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 841076
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841077
    if-eqz v0, :cond_0

    .line 841078
    const-string v1, "default_group_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841079
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841080
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 841081
    if-eqz v0, :cond_1

    .line 841082
    const-string v0, "default_visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841083
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841084
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 841085
    if-eqz v0, :cond_3

    .line 841086
    const-string v1, "suggested_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841087
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 841088
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 841089
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/57D;->a(LX/15i;ILX/0nX;)V

    .line 841090
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 841091
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 841092
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 841093
    if-eqz v0, :cond_4

    .line 841094
    const-string v1, "suggestion_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841095
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841096
    :cond_4
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 841097
    if-eqz v0, :cond_5

    .line 841098
    const-string v0, "suggestion_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 841099
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 841100
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 841101
    return-void
.end method
