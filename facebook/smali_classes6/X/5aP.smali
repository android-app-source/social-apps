.class public final LX/5aP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 954556
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 954557
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954558
    :goto_0
    return v1

    .line 954559
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954560
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 954561
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 954562
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954563
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 954564
    const-string v4, "text_with_entities"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 954565
    const/4 v3, 0x0

    .line 954566
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 954567
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954568
    :goto_2
    move v2, v3

    .line 954569
    goto :goto_1

    .line 954570
    :cond_2
    const-string v4, "voters"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 954571
    invoke-static {p0, p1}, LX/5aO;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 954572
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 954573
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 954574
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 954575
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 954576
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954577
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 954578
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 954579
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 954581
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 954582
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 954583
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 954584
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 954585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 954586
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954587
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954588
    if-eqz v0, :cond_1

    .line 954589
    const-string v1, "text_with_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954590
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954591
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 954592
    if-eqz v1, :cond_0

    .line 954593
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954594
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954595
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954596
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954597
    if-eqz v0, :cond_2

    .line 954598
    const-string v1, "voters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954599
    invoke-static {p0, v0, p2, p3}, LX/5aO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954600
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954601
    return-void
.end method
