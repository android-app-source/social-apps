.class public final LX/6Yj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Yk;


# direct methods
.method public constructor <init>(LX/6Yk;)V
    .locals 0

    .prologue
    .line 1110479
    iput-object p1, p0, LX/6Yj;->a:LX/6Yk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1110461
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    invoke-static {v0}, LX/6Yk;->h(LX/6Yk;)V

    .line 1110462
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1110463
    check-cast p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    .line 1110464
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    if-nez v0, :cond_0

    .line 1110465
    :goto_0
    return-void

    .line 1110466
    :cond_0
    if-eqz p1, :cond_1

    .line 1110467
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1110468
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1110469
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v0, v0

    .line 1110470
    if-eqz v0, :cond_1

    .line 1110471
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v0, v0

    .line 1110472
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1110473
    :cond_1
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    invoke-static {v0}, LX/6Yk;->h(LX/6Yk;)V

    goto :goto_0

    .line 1110474
    :cond_2
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110475
    iput-object p1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    .line 1110476
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    iget-object v0, v0, LX/6Yk;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1110477
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->USE_DATA_OR_STAY_IN_FREE:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0

    .line 1110478
    :cond_3
    iget-object v0, p0, LX/6Yj;->a:LX/6Yk;

    iget-object v0, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->PROMOS_LIST:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0
.end method
