.class public LX/5KB;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source ""


# instance fields
.field public c:Lcom/facebook/components/ComponentView;

.field public d:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 898269
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;)V

    .line 898270
    new-instance v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    .line 898271
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/5KA;

    invoke-direct {v1, p0}, LX/5KA;-><init>(LX/5KB;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setChildDrawingOrderCallback(LX/3x4;)V

    .line 898272
    iget-object v0, p0, LX/5KB;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, LX/5KB;->addView(Landroid/view/View;)V

    .line 898273
    new-instance v0, Lcom/facebook/components/ComponentView;

    invoke-virtual {p0}, LX/5KB;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    .line 898274
    iget-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 898275
    iget-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {p0, v0}, LX/5KB;->addView(Landroid/view/View;)V

    .line 898276
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 898267
    iget-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    .line 898268
    return-void
.end method

.method public final isLayoutRequested()Z
    .locals 1

    .prologue
    .line 898277
    invoke-virtual {p0}, LX/5KB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 898278
    invoke-virtual {p0}, LX/5KB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v0

    .line 898279
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isLayoutRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 898258
    invoke-super/range {p0 .. p5}, Landroid/support/v4/widget/SwipeRefreshLayout;->onLayout(ZIIII)V

    .line 898259
    iget-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 898260
    :goto_0
    return-void

    .line 898261
    :cond_0
    invoke-virtual {p0}, LX/5KB;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p2

    .line 898262
    invoke-virtual {p0}, LX/5KB;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p3

    .line 898263
    iget-object v2, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    iget-object v3, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {v3}, Lcom/facebook/components/ComponentView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {v4}, Lcom/facebook/components/ComponentView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/facebook/components/ComponentView;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 898264
    invoke-super {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->onMeasure(II)V

    .line 898265
    iget-object v0, p0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {p0, v0, p1, p2}, LX/5KB;->measureChild(Landroid/view/View;II)V

    .line 898266
    return-void
.end method
