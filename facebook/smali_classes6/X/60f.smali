.class public final LX/60f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1038802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1038801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;->getViolationFromString(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1038800
    new-array v0, p1, [Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;

    return-object v0
.end method
