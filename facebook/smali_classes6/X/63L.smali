.class public LX/63L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0h5;


# instance fields
.field private final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0ew;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/3u1;

.field private final c:LX/63Y;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/63W;

.field private f:LX/63J;


# direct methods
.method private constructor <init>(LX/0am;LX/3u1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/0ew;",
            ">;",
            "LX/3u1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1043120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043121
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1043122
    iput-object v0, p0, LX/63L;->d:LX/0Px;

    .line 1043123
    iput-object p1, p0, LX/63L;->a:LX/0am;

    .line 1043124
    iput-object p2, p0, LX/63L;->b:LX/3u1;

    .line 1043125
    new-instance v0, LX/63Y;

    invoke-direct {v0}, LX/63Y;-><init>()V

    iput-object v0, p0, LX/63L;->c:LX/63Y;

    .line 1043126
    return-void
.end method

.method public constructor <init>(LX/0ew;LX/3u1;)V
    .locals 1

    .prologue
    .line 1043127
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/63L;-><init>(LX/0am;LX/3u1;)V

    .line 1043128
    return-void
.end method

.method public constructor <init>(LX/3u1;)V
    .locals 1

    .prologue
    .line 1043129
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/63L;-><init>(LX/0am;LX/3u1;)V

    .line 1043130
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 1043132
    iget-object v0, p0, LX/63L;->d:LX/0Px;

    invoke-static {p1, v0}, LX/63Y;->a(Landroid/view/Menu;Ljava/util/List;)V

    .line 1043133
    iget-object v0, p0, LX/63L;->c:LX/63Y;

    iget-object v1, p0, LX/63L;->d:LX/0Px;

    iget-object v2, p0, LX/63L;->e:LX/63W;

    invoke-virtual {v0, p1, v1, v2}, LX/63Y;->a(Landroid/view/Menu;Ljava/util/List;LX/63W;)V

    .line 1043134
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1043131
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1043138
    iget-object v0, p0, LX/63L;->f:LX/63J;

    .line 1043139
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    if-eqz v0, :cond_2

    .line 1043140
    invoke-interface {v0}, LX/63J;->a()V

    .line 1043141
    const/4 v1, 0x1

    .line 1043142
    :goto_0
    move v0, v1

    .line 1043143
    if-eqz v0, :cond_0

    .line 1043144
    const/4 v0, 0x1

    .line 1043145
    :goto_1
    return v0

    .line 1043146
    :cond_0
    iget-object v0, p0, LX/63L;->e:LX/63W;

    if-nez v0, :cond_1

    .line 1043147
    const/4 v0, 0x0

    goto :goto_1

    .line 1043148
    :cond_1
    iget-object v0, p0, LX/63L;->d:LX/0Px;

    iget-object v1, p0, LX/63L;->e:LX/63W;

    invoke-static {p1, v0, v1}, LX/63Y;->a(Landroid/view/MenuItem;Ljava/util/List;LX/63W;)Z

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d_(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1043135
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    invoke-virtual {v0}, LX/3u1;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1043136
    invoke-virtual {p0, v0}, LX/63L;->setCustomTitleView(Landroid/view/View;)V

    .line 1043137
    return-object v0
.end method

.method public final setButtonSpecs(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1043113
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/63L;->d:LX/0Px;

    .line 1043114
    iget-object v0, p0, LX/63L;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1043115
    iget-object v0, p0, LX/63L;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    invoke-interface {v0}, LX/0ew;->iA_()V

    .line 1043116
    :cond_0
    return-void
.end method

.method public final setCustomTitleView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1043117
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    const/16 v1, 0x12

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, LX/3u1;->a(II)V

    .line 1043118
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    invoke-virtual {v0, p1}, LX/3u1;->a(Landroid/view/View;)V

    .line 1043119
    return-void
.end method

.method public final setHasBackButton(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1043108
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    .line 1043109
    :goto_0
    iget-object v2, p0, LX/63L;->b:LX/3u1;

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :cond_0
    or-int/2addr v1, v0

    or-int/lit8 v0, v0, 0x4

    invoke-virtual {v2, v1, v0}, LX/3u1;->a(II)V

    .line 1043110
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3u1;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1043111
    return-void

    :cond_1
    move v0, v1

    .line 1043112
    goto :goto_0
.end method

.method public final setHasFbLogo(Z)V
    .locals 0

    .prologue
    .line 1043107
    return-void
.end method

.method public final setOnBackPressedListener(LX/63J;)V
    .locals 0

    .prologue
    .line 1043105
    iput-object p1, p0, LX/63L;->f:LX/63J;

    .line 1043106
    return-void
.end method

.method public final setOnToolbarButtonListener(LX/63W;)V
    .locals 0

    .prologue
    .line 1043103
    iput-object p1, p0, LX/63L;->e:LX/63W;

    .line 1043104
    return-void
.end method

.method public final setShowDividers(Z)V
    .locals 0

    .prologue
    .line 1043102
    return-void
.end method

.method public final setTitle(I)V
    .locals 3

    .prologue
    .line 1043099
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    const/16 v1, 0xa

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, LX/3u1;->a(II)V

    .line 1043100
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    invoke-virtual {v0, p1}, LX/3u1;->b(I)V

    .line 1043101
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1043096
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    const/16 v1, 0xa

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, LX/3u1;->a(II)V

    .line 1043097
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    invoke-virtual {v0, p1}, LX/3u1;->a(Ljava/lang/CharSequence;)V

    .line 1043098
    return-void
.end method

.method public final setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1043093
    iget-object v0, p0, LX/63L;->b:LX/3u1;

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, LX/3u1;->d(I)V

    .line 1043094
    new-instance v0, LX/63K;

    invoke-direct {v0, p0, p1}, LX/63K;-><init>(LX/63L;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, LX/63L;->setOnBackPressedListener(LX/63J;)V

    .line 1043095
    return-void
.end method
