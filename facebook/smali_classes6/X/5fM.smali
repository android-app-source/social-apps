.class public final enum LX/5fM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5fM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5fM;

.field public static final enum BACK:LX/5fM;

.field public static final enum FRONT:LX/5fM;


# instance fields
.field private mInfoId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 971198
    new-instance v0, LX/5fM;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2, v3}, LX/5fM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fM;->FRONT:LX/5fM;

    .line 971199
    new-instance v0, LX/5fM;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3, v2}, LX/5fM;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5fM;->BACK:LX/5fM;

    .line 971200
    const/4 v0, 0x2

    new-array v0, v0, [LX/5fM;

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    aput-object v1, v0, v2

    sget-object v1, LX/5fM;->BACK:LX/5fM;

    aput-object v1, v0, v3

    sput-object v0, LX/5fM;->$VALUES:[LX/5fM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 971201
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 971202
    iput p3, p0, LX/5fM;->mInfoId:I

    .line 971203
    return-void
.end method

.method public static fromId(I)LX/5fM;
    .locals 5

    .prologue
    .line 971204
    invoke-static {}, LX/5fM;->values()[LX/5fM;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 971205
    iget v4, v0, LX/5fM;->mInfoId:I

    if-ne v4, p0, :cond_0

    .line 971206
    :goto_1
    return-object v0

    .line 971207
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 971208
    :cond_1
    sget-object v0, LX/5fM;->BACK:LX/5fM;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5fM;
    .locals 1

    .prologue
    .line 971209
    const-class v0, LX/5fM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5fM;

    return-object v0
.end method

.method public static values()[LX/5fM;
    .locals 1

    .prologue
    .line 971210
    sget-object v0, LX/5fM;->$VALUES:[LX/5fM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5fM;

    return-object v0
.end method


# virtual methods
.method public final getInfoId()I
    .locals 1

    .prologue
    .line 971211
    iget v0, p0, LX/5fM;->mInfoId:I

    return v0
.end method
