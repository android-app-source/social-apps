.class public abstract LX/5Je;
.super LX/3mY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Landroid/support/v7/widget/RecyclerView$LayoutManager;",
        "R:",
        "LX/5K5;",
        ">",
        "LX/3mY",
        "<",
        "Landroid/support/v7/widget/RecyclerView;",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1OR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field private final b:LX/5K3;

.field private final c:LX/1OX;

.field public d:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1OR;LX/5K5;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "T",
            "L;",
            "TR;)V"
        }
    .end annotation

    .prologue
    .line 897387
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/5Je;-><init>(Landroid/content/Context;LX/1OR;LX/5K5;Landroid/os/Looper;)V

    .line 897388
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/1OR;LX/5K5;Landroid/os/Looper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "T",
            "L;",
            "TR;",
            "Landroid/os/Looper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 897389
    invoke-direct {p0, p1, p4, p3}, LX/3mY;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/3me;)V

    .line 897390
    iput-object p2, p0, LX/5Je;->a:LX/1OR;

    .line 897391
    new-instance v0, LX/5K3;

    invoke-direct {v0, p1, p0}, LX/5K3;-><init>(Landroid/content/Context;LX/5Je;)V

    iput-object v0, p0, LX/5Je;->b:LX/5K3;

    .line 897392
    new-instance v0, LX/5K4;

    invoke-direct {v0, p0}, LX/5K4;-><init>(LX/5Je;)V

    iput-object v0, p0, LX/5Je;->c:LX/1OX;

    .line 897393
    iget-object v0, p0, LX/5Je;->b:LX/5K3;

    .line 897394
    iput-object v0, p0, LX/3mY;->h:LX/3mh;

    .line 897395
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 897396
    iput-object p1, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    .line 897397
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    if-nez v0, :cond_2

    .line 897398
    iget-object v0, p0, LX/5Je;->a:LX/1OR;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 897399
    :cond_0
    iget-object v0, p0, LX/5Je;->b:LX/5K3;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 897400
    iget-object v0, p0, LX/5Je;->c:LX/1OX;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 897401
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 897402
    :cond_1
    :goto_0
    return-void

    .line 897403
    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    iget-object v1, p0, LX/5Je;->a:LX/1OR;

    if-eq v0, v1, :cond_0

    .line 897404
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The LayoutManager used in the Binder constructor must be the same one assigned to the RecyclerView associated to that Binder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 897405
    :cond_3
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 897406
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    .line 897407
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, LX/5KB;

    if-eqz v1, :cond_4

    .line 897408
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, LX/5KB;

    .line 897409
    :goto_1
    move-object v0, v1

    .line 897410
    if-eqz v0, :cond_1

    .line 897411
    instance-of v1, p0, LX/5Jl;

    if-eqz v1, :cond_1

    .line 897412
    new-instance v1, LX/5KG;

    new-instance v2, LX/1De;

    iget-object p1, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v2, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, p0, v0}, LX/5KG;-><init>(LX/1De;LX/5Je;LX/5KB;)V

    .line 897413
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 897414
    return-void
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 897415
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->a()V

    .line 897416
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 897417
    iget-object v0, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 897418
    iput-object v1, p0, LX/5Je;->d:Landroid/support/v7/widget/RecyclerView;

    .line 897419
    return-void
.end method

.method public synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 897420
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, p1}, LX/5Je;->a(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 897421
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, LX/3mY;->a(III)V

    .line 897422
    return-void
.end method

.method public synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 897423
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, p1}, LX/5Je;->b(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public k()LX/1OR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()T",
            "L;"
        }
    .end annotation

    .prologue
    .line 897424
    iget-object v0, p0, LX/5Je;->a:LX/1OR;

    return-object v0
.end method
