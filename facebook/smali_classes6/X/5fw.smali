.class public LX/5fw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 972459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 972460
    sget-object v0, LX/5fy;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 972461
    invoke-static {p0}, LX/5fy;->c(Landroid/content/Context;)V

    .line 972462
    :cond_0
    sget-object v0, LX/5fy;->a:LX/0Px;

    move-object v0, v0

    .line 972463
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 972464
    :goto_0
    return-object v0

    .line 972465
    :cond_1
    sget-object v0, LX/5fx;->b:LX/0Px;

    move-object v0, v0

    .line 972466
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 972467
    sget-object v0, LX/5fy;->b:LX/0P1;

    if-nez v0, :cond_0

    .line 972468
    invoke-static {p0}, LX/5fy;->c(Landroid/content/Context;)V

    .line 972469
    :cond_0
    sget-object v0, LX/5fy;->b:LX/0P1;

    move-object v0, v0

    .line 972470
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 972471
    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 972472
    :goto_0
    return-object v0

    .line 972473
    :cond_1
    sget-object v0, LX/5fx;->a:LX/0P1;

    move-object v0, v0

    .line 972474
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 972475
    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 972476
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported currency "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
