.class public abstract LX/556;
.super LX/3t3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "LX/3t3;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/reflect/Field;


# instance fields
.field public final a:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public final c:Landroid/view/accessibility/AccessibilityManager;

.field public d:I

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 829023
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "mPrivateFlags2"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 829024
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 829025
    :goto_0
    sput-object v0, LX/556;->b:Ljava/lang/reflect/Field;

    .line 829026
    return-void

    .line 829027
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    const/high16 v0, -0x80000000

    .line 829014
    invoke-direct {p0}, LX/3t3;-><init>()V

    .line 829015
    iput v0, p0, LX/556;->d:I

    .line 829016
    iput v0, p0, LX/556;->e:I

    .line 829017
    if-nez p1, :cond_0

    .line 829018
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 829019
    :cond_0
    iput-object p1, p0, LX/556;->a:Landroid/view/View;

    .line 829020
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 829021
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    .line 829022
    return-void
.end method

.method public static a(LX/556;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 828999
    const/high16 v1, -0x80000000

    if-eq p1, v1, :cond_0

    iget-object v1, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 829000
    :cond_0
    :goto_0
    return v0

    .line 829001
    :cond_1
    iget-object v1, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 829002
    if-eqz v1, :cond_0

    .line 829003
    packed-switch p1, :pswitch_data_0

    .line 829004
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 829005
    invoke-static {v0}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v2

    .line 829006
    iget-object v3, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v2, v3, p1}, LX/2fO;->a(Landroid/view/View;I)V

    .line 829007
    move-object v0, v0

    .line 829008
    :goto_1
    move-object v0, v0

    .line 829009
    iget-object v2, p0, LX/556;->a:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0

    .line 829010
    :pswitch_0
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 829011
    iget-object v2, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 829012
    move-object v0, v0

    .line 829013
    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 828993
    iget v0, p0, LX/556;->e:I

    if-ne v0, p1, :cond_0

    .line 828994
    :goto_0
    return-void

    .line 828995
    :cond_0
    iget v0, p0, LX/556;->e:I

    .line 828996
    iput p1, p0, LX/556;->e:I

    .line 828997
    const/16 v1, 0x80

    invoke-static {p0, p1, v1}, LX/556;->a(LX/556;II)Z

    .line 828998
    const/16 v1, 0x100

    invoke-static {p0, v0, v1}, LX/556;->a(LX/556;II)Z

    goto :goto_0
.end method

.method public static e(LX/556;I)Z
    .locals 1

    .prologue
    .line 828936
    iget v0, p0, LX/556;->d:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/556;I)Z
    .locals 1

    .prologue
    .line 828987
    invoke-static {p0, p1}, LX/556;->e(LX/556;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828988
    const/high16 v0, -0x80000000

    iput v0, p0, LX/556;->d:I

    .line 828989
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 828990
    const/high16 v0, 0x10000

    invoke-static {p0, p1, v0}, LX/556;->a(LX/556;II)Z

    .line 828991
    const/4 v0, 0x1

    .line 828992
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(FF)I
.end method

.method public final a(I)LX/3sp;
    .locals 2

    .prologue
    .line 828971
    packed-switch p1, :pswitch_data_0

    .line 828972
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    .line 828973
    sget-object v1, LX/3sp;->a:LX/3sg;

    invoke-interface {v1, v0, p1}, LX/3sg;->a(Landroid/view/View;I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/3sp;->c(Ljava/lang/Object;)LX/3sp;

    move-result-object v1

    move-object v0, v1

    .line 828974
    invoke-virtual {p0, p1, v0}, LX/556;->a(ILX/3sp;)V

    .line 828975
    iget v1, p0, LX/556;->d:I

    if-ne v1, p1, :cond_0

    .line 828976
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3sp;->d(Z)V

    .line 828977
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, LX/3sp;->a(I)V

    .line 828978
    :goto_0
    move-object v0, v0

    .line 828979
    :goto_1
    return-object v0

    .line 828980
    :pswitch_0
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    invoke-static {v0}, LX/3sp;->a(Landroid/view/View;)LX/3sp;

    move-result-object v0

    .line 828981
    iget-object v1, p0, LX/556;->a:Landroid/view/View;

    invoke-static {v1, v0}, LX/0vv;->a(Landroid/view/View;LX/3sp;)V

    .line 828982
    invoke-virtual {p0, v0}, LX/556;->a(LX/3sp;)V

    .line 828983
    move-object v0, v0

    .line 828984
    goto :goto_1

    .line 828985
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3sp;->d(Z)V

    .line 828986
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, LX/3sp;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public abstract a(ILX/3sp;)V
.end method

.method public abstract a(LX/3sp;)V
.end method

.method public final a(IILandroid/os/Bundle;)Z
    .locals 4

    .prologue
    .line 828945
    packed-switch p1, :pswitch_data_0

    .line 828946
    sparse-switch p2, :sswitch_data_0

    .line 828947
    const/4 v0, 0x0

    move v0, v0

    .line 828948
    :goto_0
    move v0, v0

    .line 828949
    :goto_1
    return v0

    .line 828950
    :pswitch_0
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    invoke-static {v0, p2, p3}, LX/0vv;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    .line 828951
    const/16 v1, 0x40

    if-ne p2, v1, :cond_0

    if-eqz v0, :cond_0

    .line 828952
    sget-object v1, LX/556;->b:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_0

    .line 828953
    :try_start_0
    sget-object v1, LX/556;->b:Ljava/lang/reflect/Field;

    iget-object v2, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 828954
    sget-object v2, LX/556;->b:Ljava/lang/reflect/Field;

    iget-object v3, p0, LX/556;->a:Landroid/view/View;

    const/high16 p1, 0x4000000

    or-int/2addr v1, p1

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 828955
    :cond_0
    :goto_2
    move v0, v0

    .line 828956
    goto :goto_1

    .line 828957
    :sswitch_0
    sparse-switch p2, :sswitch_data_1

    .line 828958
    const/4 v0, 0x0

    :goto_3
    move v0, v0

    .line 828959
    goto :goto_0

    .line 828960
    :sswitch_1
    const/4 v0, 0x0

    .line 828961
    iget-object p2, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result p2

    if-nez p2, :cond_2

    .line 828962
    :cond_1
    :goto_4
    move v0, v0

    .line 828963
    goto :goto_3

    .line 828964
    :sswitch_2
    invoke-static {p0, p1}, LX/556;->g(LX/556;I)Z

    move-result v0

    goto :goto_3

    .line 828965
    :cond_2
    invoke-static {p0, p1}, LX/556;->e(LX/556;I)Z

    move-result p2

    if-nez p2, :cond_1

    .line 828966
    iget v0, p0, LX/556;->d:I

    invoke-static {p0, v0}, LX/556;->g(LX/556;I)Z

    .line 828967
    iput p1, p0, LX/556;->d:I

    .line 828968
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 828969
    const v0, 0x8000

    invoke-static {p0, p1, v0}, LX/556;->a(LX/556;II)Z

    .line 828970
    const/4 v0, 0x1

    goto :goto_4

    :catch_0
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x40 -> :sswitch_1
        0x80 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    .line 828937
    iget-object v2, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/556;->c:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 828938
    :cond_1
    :goto_0
    return v0

    .line 828939
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 828940
    goto :goto_0

    .line 828941
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/556;->a(FF)I

    move-result v2

    .line 828942
    invoke-direct {p0, v2}, LX/556;->b(I)V

    .line 828943
    if-ne v2, v4, :cond_1

    move v0, v1

    goto :goto_0

    .line 828944
    :pswitch_2
    invoke-direct {p0, v4}, LX/556;->b(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
