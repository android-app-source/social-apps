.class public final LX/5aL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 954415
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 954416
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 954417
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 954418
    const/4 v2, 0x0

    .line 954419
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 954420
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954421
    :goto_1
    move v1, v2

    .line 954422
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 954423
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 954424
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954425
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 954426
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 954427
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954428
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 954429
    const-string v5, "nickname_suggestions"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 954430
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_2

    .line 954431
    :cond_3
    const-string v5, "participant_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 954432
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 954433
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 954434
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 954435
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 954436
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 954437
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 954438
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 954439
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 954440
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954441
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 954442
    if-eqz v2, :cond_0

    .line 954443
    const-string v2, "nickname_suggestions"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954444
    invoke-virtual {p0, v1, p3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 954445
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 954446
    if-eqz v2, :cond_1

    .line 954447
    const-string p3, "participant_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954448
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954449
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954450
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 954451
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 954452
    return-void
.end method
