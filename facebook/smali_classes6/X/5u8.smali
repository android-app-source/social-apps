.class public final LX/5u8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 14

    .prologue
    .line 1018010
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1018011
    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 1018012
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_6

    .line 1018013
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1018014
    :goto_0
    move v1, v3

    .line 1018015
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1018016
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1018017
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 1018018
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1018019
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1018020
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1018021
    const-string v12, "creation_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1018022
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v2, v8

    goto :goto_1

    .line 1018023
    :cond_1
    const-string v12, "creator"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1018024
    invoke-static {p0, v0}, LX/5u6;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1018025
    :cond_2
    const-string v12, "story"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1018026
    invoke-static {p0, v0}, LX/5u7;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1018027
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1018028
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1018029
    if-eqz v2, :cond_5

    move-object v2, v0

    .line 1018030
    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1018031
    :cond_5
    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 1018032
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1018033
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_0

    :cond_6
    move v2, v3

    move v9, v3

    move v10, v3

    move-wide v4, v6

    goto :goto_1
.end method
