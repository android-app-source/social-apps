.class public LX/5MN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5MG;


# static fields
.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/5MM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field public b:LX/5MM;

.field public c:D

.field public d:D


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 905446
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "oor"

    sget-object v2, LX/5MM;->OOR:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "ccr"

    sget-object v2, LX/5MM;->CCR:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "ocr"

    sget-object v2, LX/5MM;->OCR:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "cor"

    sget-object v2, LX/5MM;->COR:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "lt"

    sget-object v2, LX/5MM;->LT:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "lte"

    sget-object v2, LX/5MM;->LTE:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "eq"

    sget-object v2, LX/5MM;->EQ:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "neq"

    sget-object v2, LX/5MM;->NEQ:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "gt"

    sget-object v2, LX/5MM;->GT:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "gte"

    sget-object v2, LX/5MM;->GTE:LX/5MM;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/5MN;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5MM;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5MM;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 905447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905448
    iput-object p1, p0, LX/5MN;->a:Ljava/lang/String;

    .line 905449
    iput-object p2, p0, LX/5MN;->b:LX/5MM;

    .line 905450
    sget-object v0, LX/5ML;->a:[I

    iget-object v1, p0, LX/5MN;->b:LX/5MM;

    invoke-virtual {v1}, LX/5MM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 905451
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 905452
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905453
    :pswitch_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 905454
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905455
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/5MN;->c:D

    .line 905456
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/5MN;->d:D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 905457
    iget-wide v0, p0, LX/5MN;->c:D

    iget-wide v2, p0, LX/5MN;->d:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    .line 905458
    new-instance v0, LX/5MH;

    const-string v1, "Bad values order"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905459
    :catch_0
    new-instance v0, LX/5MH;

    const-string v1, "Bad number format"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905460
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/5MN;->c:D
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 905461
    :cond_2
    return-void

    .line 905462
    :catch_1
    new-instance v0, LX/5MH;

    const-string v1, "Bad number format"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905463
    iget-object v0, p0, LX/5MN;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0uE;)Z
    .locals 7

    .prologue
    .line 905464
    invoke-virtual {p1}, LX/0uE;->d()D

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 905465
    sget-object v4, LX/5ML;->a:[I

    iget-object v5, p0, LX/5MN;->b:LX/5MM;

    invoke-virtual {v5}, LX/5MM;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    move v2, v3

    .line 905466
    :cond_0
    :goto_0
    move v0, v2

    .line 905467
    return v0

    .line 905468
    :pswitch_0
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-lez v4, :cond_1

    iget-wide v4, p0, LX/5MN;->d:D

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_0

    :cond_1
    move v2, v3

    goto :goto_0

    .line 905469
    :pswitch_1
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    iget-wide v4, p0, LX/5MN;->d:D

    cmpg-double v4, v0, v4

    if-lez v4, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0

    .line 905470
    :pswitch_2
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-lez v4, :cond_3

    iget-wide v4, p0, LX/5MN;->d:D

    cmpg-double v4, v0, v4

    if-lez v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0

    .line 905471
    :pswitch_3
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_4

    iget-wide v4, p0, LX/5MN;->d:D

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_0

    :cond_4
    move v2, v3

    goto :goto_0

    .line 905472
    :pswitch_4
    iget-wide v4, p0, LX/5MN;->c:D

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 905473
    :pswitch_5
    iget-wide v4, p0, LX/5MN;->c:D

    cmpg-double v4, v0, v4

    if-lez v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 905474
    :pswitch_6
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 905475
    :pswitch_7
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 905476
    :pswitch_8
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-gtz v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 905477
    :pswitch_9
    iget-wide v4, p0, LX/5MN;->c:D

    cmpl-double v4, v0, v4

    if-gez v4, :cond_0

    move v2, v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
