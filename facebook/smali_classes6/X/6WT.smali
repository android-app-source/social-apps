.class public LX/6WT;
.super LX/1cr;
.source ""


# instance fields
.field private final b:Landroid/graphics/Rect;

.field private c:LX/6WN;

.field public d:Z


# direct methods
.method public constructor <init>(LX/6WN;)V
    .locals 1

    .prologue
    .line 1106268
    invoke-direct {p0, p1}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 1106269
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/6WT;->b:Landroid/graphics/Rect;

    .line 1106270
    iput-object p1, p0, LX/6WT;->c:LX/6WN;

    .line 1106271
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6WT;->d:Z

    .line 1106272
    return-void
.end method


# virtual methods
.method public a(FF)I
    .locals 7

    .prologue
    .line 1106273
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1106274
    invoke-virtual {p0, v3}, LX/6WT;->a(Ljava/util/List;)V

    .line 1106275
    const/high16 v2, -0x80000000

    .line 1106276
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1106277
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1106278
    if-ltz v0, :cond_0

    iget-object v4, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v4}, LX/6WN;->getNumDraweeControllers()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 1106279
    iget-object v4, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v4, v0}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 1106280
    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1106281
    :goto_1
    return v0

    .line 1106282
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public a(ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1106283
    iget-object v0, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v0, p1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1106284
    if-eqz p2, :cond_0

    .line 1106285
    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1106286
    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 2

    .prologue
    .line 1106287
    invoke-virtual {p0, p1}, LX/6WT;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1106288
    :cond_0
    :goto_0
    return-void

    .line 1106289
    :cond_1
    iget-object v0, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v0, p1}, LX/6WN;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 1106290
    iget-object v1, p0, LX/6WT;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1}, LX/6WT;->a(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1106291
    iget-object v1, p0, LX/6WT;->b:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 1106292
    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1106293
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1106294
    iget-boolean v0, p0, LX/6WT;->d:Z

    if-eqz v0, :cond_0

    .line 1106295
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/3sp;->f(Z)V

    .line 1106296
    const-class v0, Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1106297
    iget-object v0, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v0, p1}, LX/6WN;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 1106298
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1106299
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1106300
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v1}, LX/6WN;->getNumDraweeControllers()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1106301
    iget-object v1, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v1, v0}, LX/6WN;->c(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1106302
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106303
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1106304
    :cond_1
    return-void
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 1106305
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v0, p1}, LX/6WN;->c(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1106306
    :cond_0
    const/4 v0, 0x0

    .line 1106307
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, LX/1cr;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 1106308
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/6WT;->c:LX/6WN;

    invoke-virtual {v0}, LX/6WN;->getNumDraweeControllers()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1106309
    const/4 v0, 0x0

    return v0
.end method
