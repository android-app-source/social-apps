.class public LX/5pq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:[J

.field public b:I


# direct methods
.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 1008555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008556
    new-array v0, p1, [J

    iput-object v0, p0, LX/5pq;->a:[J

    .line 1008557
    const/4 v0, 0x0

    iput v0, p0, LX/5pq;->b:I

    .line 1008558
    return-void
.end method

.method public static a(I)LX/5pq;
    .locals 1

    .prologue
    .line 1008559
    new-instance v0, LX/5pq;

    invoke-direct {v0, p0}, LX/5pq;-><init>(I)V

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1008560
    iget v0, p0, LX/5pq;->b:I

    iget-object v1, p0, LX/5pq;->a:[J

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 1008561
    iget v0, p0, LX/5pq;->b:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/5pq;->b:I

    int-to-double v2, v1

    const-wide v4, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1008562
    new-array v0, v0, [J

    .line 1008563
    iget-object v1, p0, LX/5pq;->a:[J

    iget v2, p0, LX/5pq;->b:I

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1008564
    iput-object v0, p0, LX/5pq;->a:[J

    .line 1008565
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IJ)V
    .locals 4

    .prologue
    .line 1008566
    iget v0, p0, LX/5pq;->b:I

    if-lt p1, v0, :cond_0

    .line 1008567
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/5pq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008568
    :cond_0
    iget-object v0, p0, LX/5pq;->a:[J

    aput-wide p2, v0, p1

    .line 1008569
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1008570
    invoke-direct {p0}, LX/5pq;->b()V

    .line 1008571
    iget-object v0, p0, LX/5pq;->a:[J

    iget v1, p0, LX/5pq;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/5pq;->b:I

    aput-wide p1, v0, v1

    .line 1008572
    return-void
.end method

.method public final b(I)J
    .locals 3

    .prologue
    .line 1008573
    iget v0, p0, LX/5pq;->b:I

    if-lt p1, v0, :cond_0

    .line 1008574
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/5pq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008575
    :cond_0
    iget-object v0, p0, LX/5pq;->a:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1008576
    iget v0, p0, LX/5pq;->b:I

    if-le p1, v0, :cond_0

    .line 1008577
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to drop "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items from array of length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/5pq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008578
    :cond_0
    iget v0, p0, LX/5pq;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/5pq;->b:I

    .line 1008579
    return-void
.end method
