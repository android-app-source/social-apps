.class public LX/6Ra;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1090091
    const-string v0, "cccc"

    sput-object v0, LX/6Ra;->a:Ljava/lang/String;

    .line 1090092
    const-string v0, "ccc"

    sput-object v0, LX/6Ra;->b:Ljava/lang/String;

    .line 1090093
    const-string v0, "LLLL"

    sput-object v0, LX/6Ra;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1090094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;
    .locals 2

    .prologue
    .line 1090095
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 1090096
    const/4 v0, 0x2

    invoke-static {v0, p0}, Ljava/text/SimpleDateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 1090097
    :goto_0
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090098
    return-object v0

    .line 1090099
    :cond_0
    const-string v0, "EEEMMMd"

    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1090100
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090101
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090102
    move-object v0, v1

    .line 1090103
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1090104
    sput-object p0, LX/6Ra;->b:Ljava/lang/String;

    .line 1090105
    sput-object p1, LX/6Ra;->a:Ljava/lang/String;

    .line 1090106
    sput-object p2, LX/6Ra;->c:Ljava/lang/String;

    .line 1090107
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;
    .locals 2

    .prologue
    .line 1090108
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1090109
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090110
    :goto_0
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090111
    return-object v0

    .line 1090112
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h a"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method
