.class public final LX/5II;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 894445
    const-class v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    const v0, 0x22c9bc23

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "AddPlaceListItemToCommentMutation"

    const-string v6, "67a964ec5868da1401527ce3c6f846f7"

    const-string v7, "comment_add_place"

    const-string v8, "0"

    const-string v9, "10155256824086729"

    const/4 v10, 0x0

    .line 894446
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 894447
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 894448
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 894449
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 894450
    sparse-switch v0, :sswitch_data_0

    .line 894451
    :goto_0
    return-object p1

    .line 894452
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 894453
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_1
        0x5fb57ca -> :sswitch_0
    .end sparse-switch
.end method
