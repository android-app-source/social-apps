.class public final LX/5bN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 958000
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 958001
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 958002
    :goto_0
    return v1

    .line 958003
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 958004
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 958005
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 958006
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 958007
    const-string v10, "approval_mode"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 958008
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 958009
    :cond_1
    const-string v10, "approval_requests"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 958010
    invoke-static {p0, p1}, LX/5bL;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 958011
    :cond_2
    const-string v10, "discoverable_mode"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 958012
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 958013
    :cond_3
    const-string v10, "joinable_mode"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 958014
    const/4 v9, 0x0

    .line 958015
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_f

    .line 958016
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 958017
    :goto_2
    move v5, v9

    .line 958018
    goto :goto_1

    .line 958019
    :cond_4
    const-string v10, "thread_admins"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 958020
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 958021
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_5

    .line 958022
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_5

    .line 958023
    invoke-static {p0, p1}, LX/5bM;->b(LX/15w;LX/186;)I

    move-result v9

    .line 958024
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 958025
    :cond_5
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 958026
    goto/16 :goto_1

    .line 958027
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 958028
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 958029
    if-eqz v3, :cond_8

    .line 958030
    invoke-virtual {p1, v1, v8, v1}, LX/186;->a(III)V

    .line 958031
    :cond_8
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 958032
    if-eqz v0, :cond_9

    .line 958033
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 958034
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 958035
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 958036
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1

    .line 958037
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 958038
    :cond_c
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_e

    .line 958039
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 958040
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 958041
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 958042
    const-string v12, "link"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 958043
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_4

    .line 958044
    :cond_d
    const-string v12, "mode"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 958045
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 958046
    :cond_e
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 958047
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 958048
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 958049
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_2

    :cond_f
    move v5, v9

    move v10, v9

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 958050
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 958051
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958052
    if-eqz v0, :cond_0

    .line 958053
    const-string v1, "approval_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958054
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958055
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958056
    if-eqz v0, :cond_1

    .line 958057
    const-string v1, "approval_requests"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958058
    invoke-static {p0, v0, p2, p3}, LX/5bL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 958059
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958060
    if-eqz v0, :cond_2

    .line 958061
    const-string v1, "discoverable_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958062
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958063
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958064
    if-eqz v0, :cond_5

    .line 958065
    const-string v1, "joinable_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958066
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 958067
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 958068
    if-eqz v1, :cond_3

    .line 958069
    const-string v2, "link"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958070
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958071
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 958072
    if-eqz v1, :cond_4

    .line 958073
    const-string v2, "mode"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958074
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958075
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 958076
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958077
    if-eqz v0, :cond_7

    .line 958078
    const-string v1, "thread_admins"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958079
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 958080
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 958081
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/5bM;->a(LX/15i;ILX/0nX;)V

    .line 958082
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 958083
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 958084
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 958085
    return-void
.end method
