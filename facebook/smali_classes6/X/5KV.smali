.class public final LX/5KV;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5KV;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5KT;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5KW;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 898706
    const/4 v0, 0x0

    sput-object v0, LX/5KV;->a:LX/5KV;

    .line 898707
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5KV;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 898703
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 898704
    new-instance v0, LX/5KW;

    invoke-direct {v0}, LX/5KW;-><init>()V

    iput-object v0, p0, LX/5KV;->c:LX/5KW;

    .line 898705
    return-void
.end method

.method public static a(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/java2js/JSValue;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 898702
    const v0, 0x7195e3cf

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/5KV;
    .locals 2

    .prologue
    .line 898674
    const-class v1, LX/5KV;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5KV;->a:LX/5KV;

    if-nez v0, :cond_0

    .line 898675
    new-instance v0, LX/5KV;

    invoke-direct {v0}, LX/5KV;-><init>()V

    sput-object v0, LX/5KV;->a:LX/5KV;

    .line 898676
    :cond_0
    sget-object v0, LX/5KV;->a:LX/5KV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 898677
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 898686
    check-cast p2, LX/5KU;

    .line 898687
    iget-object v0, p2, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    .line 898688
    const-string v1, "selected"

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->getBooleanProperty(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    .line 898689
    const-string v2, "imageSet"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 898690
    const-string v3, "title"

    invoke-virtual {v0, v3}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 898691
    const-string p0, "onPress"

    invoke-virtual {v0, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p0

    .line 898692
    invoke-virtual {v2}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    .line 898693
    :goto_0
    if-eqz v1, :cond_1

    .line 898694
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-static {p1, p0}, LX/5KV;->a(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1dQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 898695
    :goto_1
    move-object v0, v1

    .line 898696
    return-object v0

    .line 898697
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v2, v1}, LX/5KW;->a(Lcom/facebook/java2js/JSValue;Z)LX/1dc;

    move-result-object v1

    goto :goto_0

    .line 898698
    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/high16 p2, 0x41600000    # 14.0f

    invoke-virtual {v3, p2}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 898699
    if-eqz v2, :cond_2

    .line 898700
    invoke-interface {v3, v1}, LX/1Di;->a(LX/1dc;)LX/1Di;

    .line 898701
    :cond_2
    invoke-static {p1, p0}, LX/5KV;->a(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1dQ;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 898678
    invoke-static {}, LX/1dS;->b()V

    .line 898679
    iget v0, p1, LX/1dQ;->b:I

    .line 898680
    packed-switch v0, :pswitch_data_0

    .line 898681
    :goto_0
    return-object v2

    .line 898682
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Lcom/facebook/java2js/JSValue;

    .line 898683
    invoke-virtual {v0}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v1

    if-nez v1, :cond_0

    .line 898684
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->callAsFunction([Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    .line 898685
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7195e3cf
        :pswitch_0
    .end packed-switch
.end method
