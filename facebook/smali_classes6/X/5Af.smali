.class public final LX/5Af;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 859057
    const/16 v39, 0x0

    .line 859058
    const/16 v38, 0x0

    .line 859059
    const/16 v37, 0x0

    .line 859060
    const/16 v36, 0x0

    .line 859061
    const/16 v35, 0x0

    .line 859062
    const/16 v34, 0x0

    .line 859063
    const/16 v33, 0x0

    .line 859064
    const/16 v32, 0x0

    .line 859065
    const/16 v31, 0x0

    .line 859066
    const/16 v30, 0x0

    .line 859067
    const/16 v29, 0x0

    .line 859068
    const/16 v28, 0x0

    .line 859069
    const/16 v27, 0x0

    .line 859070
    const/16 v26, 0x0

    .line 859071
    const/16 v25, 0x0

    .line 859072
    const/16 v24, 0x0

    .line 859073
    const/16 v23, 0x0

    .line 859074
    const/16 v22, 0x0

    .line 859075
    const/16 v21, 0x0

    .line 859076
    const/16 v20, 0x0

    .line 859077
    const/16 v19, 0x0

    .line 859078
    const/16 v18, 0x0

    .line 859079
    const/16 v17, 0x0

    .line 859080
    const/16 v16, 0x0

    .line 859081
    const/4 v15, 0x0

    .line 859082
    const/4 v14, 0x0

    .line 859083
    const/4 v13, 0x0

    .line 859084
    const/4 v12, 0x0

    .line 859085
    const/4 v11, 0x0

    .line 859086
    const/4 v10, 0x0

    .line 859087
    const/4 v9, 0x0

    .line 859088
    const/4 v8, 0x0

    .line 859089
    const/4 v7, 0x0

    .line 859090
    const/4 v6, 0x0

    .line 859091
    const/4 v5, 0x0

    .line 859092
    const/4 v4, 0x0

    .line 859093
    const/4 v3, 0x0

    .line 859094
    const/4 v2, 0x0

    .line 859095
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1

    .line 859096
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 859097
    const/4 v2, 0x0

    .line 859098
    :goto_0
    return v2

    .line 859099
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 859100
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1b

    .line 859101
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v40

    .line 859102
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 859103
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    if-eqz v40, :cond_1

    .line 859104
    const-string v41, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_2

    .line 859105
    const/4 v13, 0x1

    .line 859106
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 859107
    :cond_2
    const-string v41, "can_see_voice_switcher"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_3

    .line 859108
    const/4 v12, 0x1

    .line 859109
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 859110
    :cond_3
    const-string v41, "can_viewer_comment"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_4

    .line 859111
    const/4 v11, 0x1

    .line 859112
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 859113
    :cond_4
    const-string v41, "can_viewer_comment_with_photo"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_5

    .line 859114
    const/4 v10, 0x1

    .line 859115
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 859116
    :cond_5
    const-string v41, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_6

    .line 859117
    const/4 v9, 0x1

    .line 859118
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 859119
    :cond_6
    const-string v41, "can_viewer_comment_with_video"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_7

    .line 859120
    const/4 v8, 0x1

    .line 859121
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto :goto_1

    .line 859122
    :cond_7
    const-string v41, "can_viewer_like"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_8

    .line 859123
    const/4 v7, 0x1

    .line 859124
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 859125
    :cond_8
    const-string v41, "can_viewer_react"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 859126
    const/4 v6, 0x1

    .line 859127
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 859128
    :cond_9
    const-string v41, "can_viewer_subscribe"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_a

    .line 859129
    const/4 v5, 0x1

    .line 859130
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 859131
    :cond_a
    const-string v41, "comments_mirroring_domain"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 859132
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 859133
    :cond_b
    const-string v41, "does_viewer_like"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 859134
    const/4 v4, 0x1

    .line 859135
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 859136
    :cond_c
    const-string v41, "id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_d

    .line 859137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 859138
    :cond_d
    const-string v41, "important_reactors"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_e

    .line 859139
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 859140
    :cond_e
    const-string v41, "is_viewer_subscribed"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_f

    .line 859141
    const/4 v3, 0x1

    .line 859142
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 859143
    :cond_f
    const-string v41, "legacy_api_post_id"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_10

    .line 859144
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 859145
    :cond_10
    const-string v41, "likers"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_11

    .line 859146
    invoke-static/range {p0 .. p1}, LX/5Ac;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 859147
    :cond_11
    const-string v41, "reactors"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_12

    .line 859148
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 859149
    :cond_12
    const-string v41, "real_time_activity_info"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_13

    .line 859150
    invoke-static/range {p0 .. p1}, LX/5Ab;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 859151
    :cond_13
    const-string v41, "remixable_photo_uri"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_14

    .line 859152
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 859153
    :cond_14
    const-string v41, "reshares"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_15

    .line 859154
    invoke-static/range {p0 .. p1}, LX/5Ad;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 859155
    :cond_15
    const-string v41, "supported_reactions"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_16

    .line 859156
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 859157
    :cond_16
    const-string v41, "top_level_comments"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_17

    .line 859158
    invoke-static/range {p0 .. p1}, LX/5Ae;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 859159
    :cond_17
    const-string v41, "top_reactions"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_18

    .line 859160
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 859161
    :cond_18
    const-string v41, "viewer_acts_as_page"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_19

    .line 859162
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 859163
    :cond_19
    const-string v41, "viewer_acts_as_person"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_1a

    .line 859164
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 859165
    :cond_1a
    const-string v41, "viewer_feedback_reaction_key"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_0

    .line 859166
    const/4 v2, 0x1

    .line 859167
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 859168
    :cond_1b
    const/16 v40, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 859169
    if-eqz v13, :cond_1c

    .line 859170
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 859171
    :cond_1c
    if-eqz v12, :cond_1d

    .line 859172
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 859173
    :cond_1d
    if-eqz v11, :cond_1e

    .line 859174
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 859175
    :cond_1e
    if-eqz v10, :cond_1f

    .line 859176
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 859177
    :cond_1f
    if-eqz v9, :cond_20

    .line 859178
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 859179
    :cond_20
    if-eqz v8, :cond_21

    .line 859180
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 859181
    :cond_21
    if-eqz v7, :cond_22

    .line 859182
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 859183
    :cond_22
    if-eqz v6, :cond_23

    .line 859184
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 859185
    :cond_23
    if-eqz v5, :cond_24

    .line 859186
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 859187
    :cond_24
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 859188
    if-eqz v4, :cond_25

    .line 859189
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 859190
    :cond_25
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 859191
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 859192
    if-eqz v3, :cond_26

    .line 859193
    const/16 v3, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 859194
    :cond_26
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859195
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859196
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859197
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859198
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859199
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859200
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859201
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859202
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859203
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 859204
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 859205
    if-eqz v2, :cond_27

    .line 859206
    const/16 v2, 0x19

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 859207
    :cond_27
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 859208
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 859209
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 859210
    if-eqz v0, :cond_0

    .line 859211
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859212
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859213
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859214
    if-eqz v0, :cond_1

    .line 859215
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859216
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859217
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859218
    if-eqz v0, :cond_2

    .line 859219
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859220
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859221
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859222
    if-eqz v0, :cond_3

    .line 859223
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859224
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859225
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859226
    if-eqz v0, :cond_4

    .line 859227
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859228
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859229
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859230
    if-eqz v0, :cond_5

    .line 859231
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859232
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859233
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859234
    if-eqz v0, :cond_6

    .line 859235
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859236
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859237
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859238
    if-eqz v0, :cond_7

    .line 859239
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859240
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859241
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859242
    if-eqz v0, :cond_8

    .line 859243
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859244
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859245
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 859246
    if-eqz v0, :cond_9

    .line 859247
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859248
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 859249
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859250
    if-eqz v0, :cond_a

    .line 859251
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859252
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859253
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 859254
    if-eqz v0, :cond_b

    .line 859255
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859256
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 859257
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859258
    if-eqz v0, :cond_c

    .line 859259
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859260
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 859261
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 859262
    if-eqz v0, :cond_d

    .line 859263
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859264
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 859265
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 859266
    if-eqz v0, :cond_e

    .line 859267
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859268
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 859269
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859270
    if-eqz v0, :cond_f

    .line 859271
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859272
    invoke-static {p0, v0, p2}, LX/5Ac;->a(LX/15i;ILX/0nX;)V

    .line 859273
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859274
    if-eqz v0, :cond_10

    .line 859275
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859276
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 859277
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859278
    if-eqz v0, :cond_11

    .line 859279
    const-string v1, "real_time_activity_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859280
    invoke-static {p0, v0, p2, p3}, LX/5Ab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 859281
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 859282
    if-eqz v0, :cond_12

    .line 859283
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859284
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 859285
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859286
    if-eqz v0, :cond_13

    .line 859287
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859288
    invoke-static {p0, v0, p2}, LX/5Ad;->a(LX/15i;ILX/0nX;)V

    .line 859289
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859290
    if-eqz v0, :cond_14

    .line 859291
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859292
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 859293
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859294
    if-eqz v0, :cond_15

    .line 859295
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859296
    invoke-static {p0, v0, p2}, LX/5Ae;->a(LX/15i;ILX/0nX;)V

    .line 859297
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859298
    if-eqz v0, :cond_16

    .line 859299
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859300
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 859301
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859302
    if-eqz v0, :cond_17

    .line 859303
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859304
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 859305
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 859306
    if-eqz v0, :cond_18

    .line 859307
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859308
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 859309
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 859310
    if-eqz v0, :cond_19

    .line 859311
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 859312
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 859313
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 859314
    return-void
.end method
