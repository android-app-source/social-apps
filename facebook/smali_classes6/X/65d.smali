.class public final LX/65d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# static fields
.field public static final synthetic a:Z


# instance fields
.field public final synthetic b:LX/65i;

.field private final c:LX/672;

.field public d:Z

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048102
    const-class v0, LX/65i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/65d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/65i;)V
    .locals 1

    .prologue
    .line 1048103
    iput-object p1, p0, LX/65d;->b:LX/65i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048104
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/65d;->c:LX/672;

    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    .line 1048080
    iget-object v1, p0, LX/65d;->b:LX/65i;

    monitor-enter v1

    .line 1048081
    :try_start_0
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->k:LX/65h;

    invoke-virtual {v0}, LX/65g;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1048082
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-wide v2, v0, LX/65i;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    iget-boolean v0, p0, LX/65d;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/65d;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->l:LX/65X;

    if-nez v0, :cond_0

    .line 1048083
    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, LX/65i;->l(LX/65i;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048084
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v2, p0, LX/65d;->b:LX/65i;

    iget-object v2, v2, LX/65i;->k:LX/65h;

    invoke-virtual {v2}, LX/65h;->b()V

    throw v0

    .line 1048085
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1048086
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->k:LX/65h;

    invoke-virtual {v0}, LX/65h;->b()V

    .line 1048087
    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, LX/65i;->k(LX/65i;)V

    .line 1048088
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-wide v2, v0, LX/65i;->b:J

    iget-object v0, p0, LX/65d;->c:LX/672;

    .line 1048089
    iget-wide v6, v0, LX/672;->b:J

    move-wide v4, v6

    .line 1048090
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 1048091
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-wide v2, v0, LX/65i;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, LX/65i;->b:J

    .line 1048092
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1048093
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->k:LX/65h;

    invoke-virtual {v0}, LX/65g;->c()V

    .line 1048094
    :try_start_4
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->f:LX/65c;

    iget-object v1, p0, LX/65d;->b:LX/65i;

    iget v1, v1, LX/65i;->e:I

    if-eqz p1, :cond_1

    iget-object v2, p0, LX/65d;->c:LX/672;

    .line 1048095
    iget-wide v6, v2, LX/672;->b:J

    move-wide v2, v6

    .line 1048096
    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, LX/65d;->c:LX/672;

    invoke-virtual/range {v0 .. v5}, LX/65c;->a(IZLX/672;J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1048097
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->k:LX/65h;

    invoke-virtual {v0}, LX/65h;->b()V

    .line 1048098
    return-void

    .line 1048099
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1048100
    :catchall_2
    move-exception v0

    iget-object v1, p0, LX/65d;->b:LX/65i;

    iget-object v1, v1, LX/65i;->k:LX/65h;

    invoke-virtual {v1}, LX/65h;->b()V

    throw v0
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1048101
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->k:LX/65h;

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 6

    .prologue
    .line 1048073
    sget-boolean v0, LX/65d;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048074
    :cond_0
    iget-object v0, p0, LX/65d;->c:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->a_(LX/672;J)V

    .line 1048075
    :goto_0
    iget-object v0, p0, LX/65d;->c:LX/672;

    .line 1048076
    iget-wide v4, v0, LX/672;->b:J

    move-wide v0, v4

    .line 1048077
    const-wide/16 v2, 0x4000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 1048078
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/65d;->a(Z)V

    goto :goto_0

    .line 1048079
    :cond_1
    return-void
.end method

.method public final close()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 1048052
    sget-boolean v0, LX/65d;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048053
    :cond_0
    iget-object v1, p0, LX/65d;->b:LX/65i;

    monitor-enter v1

    .line 1048054
    :try_start_0
    iget-boolean v0, p0, LX/65d;->d:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 1048055
    :goto_0
    return-void

    .line 1048056
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048057
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->c:LX/65d;

    iget-boolean v0, v0, LX/65d;->e:Z

    if-nez v0, :cond_3

    .line 1048058
    iget-object v0, p0, LX/65d;->c:LX/672;

    .line 1048059
    iget-wide v6, v0, LX/672;->b:J

    move-wide v0, v6

    .line 1048060
    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 1048061
    :goto_1
    iget-object v0, p0, LX/65d;->c:LX/672;

    .line 1048062
    iget-wide v6, v0, LX/672;->b:J

    move-wide v0, v6

    .line 1048063
    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 1048064
    invoke-direct {p0, v2}, LX/65d;->a(Z)V

    goto :goto_1

    .line 1048065
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1048066
    :cond_2
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->f:LX/65c;

    iget-object v1, p0, LX/65d;->b:LX/65i;

    iget v1, v1, LX/65i;->e:I

    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, LX/65c;->a(IZLX/672;J)V

    .line 1048067
    :cond_3
    iget-object v1, p0, LX/65d;->b:LX/65i;

    monitor-enter v1

    .line 1048068
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/65d;->d:Z

    .line 1048069
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1048070
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->f:LX/65c;

    invoke-virtual {v0}, LX/65c;->c()V

    .line 1048071
    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, LX/65i;->j(LX/65i;)V

    goto :goto_0

    .line 1048072
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final flush()V
    .locals 6

    .prologue
    .line 1048041
    sget-boolean v0, LX/65d;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048042
    :cond_0
    iget-object v1, p0, LX/65d;->b:LX/65i;

    monitor-enter v1

    .line 1048043
    :try_start_0
    iget-object v0, p0, LX/65d;->b:LX/65i;

    invoke-static {v0}, LX/65i;->k(LX/65i;)V

    .line 1048044
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048045
    :goto_0
    iget-object v0, p0, LX/65d;->c:LX/672;

    .line 1048046
    iget-wide v4, v0, LX/672;->b:J

    move-wide v0, v4

    .line 1048047
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1048048
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/65d;->a(Z)V

    .line 1048049
    iget-object v0, p0, LX/65d;->b:LX/65i;

    iget-object v0, v0, LX/65i;->f:LX/65c;

    invoke-virtual {v0}, LX/65c;->c()V

    goto :goto_0

    .line 1048050
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1048051
    :cond_1
    return-void
.end method
