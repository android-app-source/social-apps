.class public final LX/54p;
.super Landroid/widget/RelativeLayout$LayoutParams;
.source ""

# interfaces
.implements LX/54o;


# instance fields
.field private a:LX/2tR;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    .line 828341
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 828342
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/high16 v5, -0x40800000    # -1.0f

    .line 828343
    const/4 v0, 0x0

    .line 828344
    sget-object v1, LX/JG0;->PercentLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 828345
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828346
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_1

    .line 828347
    const-string v0, "PercentLayout"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "percent width: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828349
    :cond_0
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    .line 828350
    iput v2, v0, LX/2tR;->a:F

    .line 828351
    :cond_1
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828352
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_3

    .line 828353
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 828354
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent height: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828355
    :cond_2
    if-eqz v0, :cond_13

    .line 828356
    :goto_0
    iput v2, v0, LX/2tR;->b:F

    .line 828357
    :cond_3
    const/16 v2, 0x2

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828358
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_5

    .line 828359
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 828360
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828361
    :cond_4
    if-eqz v0, :cond_14

    .line 828362
    :goto_1
    iput v2, v0, LX/2tR;->c:F

    .line 828363
    iput v2, v0, LX/2tR;->d:F

    .line 828364
    iput v2, v0, LX/2tR;->e:F

    .line 828365
    iput v2, v0, LX/2tR;->f:F

    .line 828366
    :cond_5
    const/16 v2, 0x3

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828367
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_7

    .line 828368
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 828369
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent left margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828370
    :cond_6
    if-eqz v0, :cond_15

    .line 828371
    :goto_2
    iput v2, v0, LX/2tR;->c:F

    .line 828372
    :cond_7
    const/16 v2, 0x4

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828373
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_9

    .line 828374
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 828375
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent top margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828376
    :cond_8
    if-eqz v0, :cond_16

    .line 828377
    :goto_3
    iput v2, v0, LX/2tR;->d:F

    .line 828378
    :cond_9
    const/16 v2, 0x5

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828379
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_b

    .line 828380
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 828381
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent right margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828382
    :cond_a
    if-eqz v0, :cond_17

    .line 828383
    :goto_4
    iput v2, v0, LX/2tR;->e:F

    .line 828384
    :cond_b
    const/16 v2, 0x6

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828385
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_d

    .line 828386
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 828387
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent bottom margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828388
    :cond_c
    if-eqz v0, :cond_18

    .line 828389
    :goto_5
    iput v2, v0, LX/2tR;->f:F

    .line 828390
    :cond_d
    const/16 v2, 0x7

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828391
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_f

    .line 828392
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 828393
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent start margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828394
    :cond_e
    if-eqz v0, :cond_19

    .line 828395
    :goto_6
    iput v2, v0, LX/2tR;->g:F

    .line 828396
    :cond_f
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    .line 828397
    cmpl-float v3, v2, v5

    if-eqz v3, :cond_11

    .line 828398
    const-string v3, "PercentLayout"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 828399
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "percent end margin: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 828400
    :cond_10
    if-eqz v0, :cond_1a

    .line 828401
    :goto_7
    iput v2, v0, LX/2tR;->h:F

    .line 828402
    :cond_11
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 828403
    const-string v1, "PercentLayout"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 828404
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "constructed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828405
    :cond_12
    move-object v0, v0

    .line 828406
    iput-object v0, p0, LX/54p;->a:LX/2tR;

    .line 828407
    return-void

    .line 828408
    :cond_13
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_0

    .line 828409
    :cond_14
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_1

    .line 828410
    :cond_15
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_2

    .line 828411
    :cond_16
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_3

    .line 828412
    :cond_17
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_4

    .line 828413
    :cond_18
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto/16 :goto_5

    .line 828414
    :cond_19
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto :goto_6

    .line 828415
    :cond_1a
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    goto :goto_7
.end method


# virtual methods
.method public final a()LX/2tR;
    .locals 1

    .prologue
    .line 828416
    iget-object v0, p0, LX/54p;->a:LX/2tR;

    if-nez v0, :cond_0

    .line 828417
    new-instance v0, LX/2tR;

    invoke-direct {v0}, LX/2tR;-><init>()V

    iput-object v0, p0, LX/54p;->a:LX/2tR;

    .line 828418
    :cond_0
    iget-object v0, p0, LX/54p;->a:LX/2tR;

    return-object v0
.end method

.method public final setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .locals 2

    .prologue
    .line 828419
    const/4 v1, 0x0

    .line 828420
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 828421
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 828422
    return-void
.end method
