.class public LX/6Po;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/6Po;


# instance fields
.field public final b:Landroid/view/WindowManager;

.field public final c:Landroid/content/Context;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/1Ml;

.field private final g:Ljava/lang/Runnable;

.field public h:LX/6Pq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1086430
    const-class v0, LX/6Po;

    sput-object v0, LX/6Po;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowManager;Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ml;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086438
    new-instance v0, Lcom/facebook/debug/debugoverlay/DebugOverlayController$1;

    invoke-direct {v0, p0}, Lcom/facebook/debug/debugoverlay/DebugOverlayController$1;-><init>(LX/6Po;)V

    iput-object v0, p0, LX/6Po;->g:Ljava/lang/Runnable;

    .line 1086439
    iput-object p1, p0, LX/6Po;->b:Landroid/view/WindowManager;

    .line 1086440
    iput-object p2, p0, LX/6Po;->c:Landroid/content/Context;

    .line 1086441
    iput-object p3, p0, LX/6Po;->d:Landroid/os/Handler;

    .line 1086442
    iput-object p4, p0, LX/6Po;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1086443
    iput-object p5, p0, LX/6Po;->f:LX/1Ml;

    .line 1086444
    return-void
.end method

.method public static a(LX/0QB;)LX/6Po;
    .locals 9

    .prologue
    .line 1086445
    sget-object v0, LX/6Po;->i:LX/6Po;

    if-nez v0, :cond_1

    .line 1086446
    const-class v1, LX/6Po;

    monitor-enter v1

    .line 1086447
    :try_start_0
    sget-object v0, LX/6Po;->i:LX/6Po;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1086448
    if-eqz v2, :cond_0

    .line 1086449
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1086450
    new-instance v3, LX/6Po;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    invoke-direct/range {v3 .. v8}, LX/6Po;-><init>(Landroid/view/WindowManager;Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ml;)V

    .line 1086451
    move-object v0, v3

    .line 1086452
    sput-object v0, LX/6Po;->i:LX/6Po;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1086453
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1086454
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1086455
    :cond_1
    sget-object v0, LX/6Po;->i:LX/6Po;

    return-object v0

    .line 1086456
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1086457
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Pr;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1086431
    iget-object v0, p0, LX/6Po;->f:LX/1Ml;

    invoke-virtual {v0}, LX/1Ml;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086432
    :cond_0
    :goto_0
    return-void

    .line 1086433
    :cond_1
    iget-object v0, p0, LX/6Po;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/6Ps;->a(LX/6Pr;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086434
    iget-object v0, p0, LX/6Po;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/6Po;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1086435
    iget-object v0, p0, LX/6Po;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/debug/debugoverlay/DebugOverlayController$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/debug/debugoverlay/DebugOverlayController$2;-><init>(LX/6Po;LX/6Pr;Ljava/lang/String;)V

    const v2, -0x109bd7d2

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1086436
    iget-object v0, p0, LX/6Po;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/6Po;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    const v4, 0x7adf7757

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
