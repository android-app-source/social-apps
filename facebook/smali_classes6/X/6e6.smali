.class public LX/6e6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6e6;


# instance fields
.field public final a:LX/1zU;

.field public final b:LX/0lC;


# direct methods
.method public constructor <init>(LX/1zU;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117470
    iput-object p1, p0, LX/6e6;->a:LX/1zU;

    .line 1117471
    iput-object p2, p0, LX/6e6;->b:LX/0lC;

    .line 1117472
    return-void
.end method

.method public static a(LX/0QB;)LX/6e6;
    .locals 5

    .prologue
    .line 1117473
    sget-object v0, LX/6e6;->c:LX/6e6;

    if-nez v0, :cond_1

    .line 1117474
    const-class v1, LX/6e6;

    monitor-enter v1

    .line 1117475
    :try_start_0
    sget-object v0, LX/6e6;->c:LX/6e6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1117476
    if-eqz v2, :cond_0

    .line 1117477
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1117478
    new-instance p0, LX/6e6;

    invoke-static {v0}, LX/1zU;->a(LX/0QB;)LX/1zU;

    move-result-object v3

    check-cast v3, LX/1zU;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-direct {p0, v3, v4}, LX/6e6;-><init>(LX/1zU;LX/0lC;)V

    .line 1117479
    move-object v0, p0

    .line 1117480
    sput-object v0, LX/6e6;->c:LX/6e6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1117481
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1117482
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1117483
    :cond_1
    sget-object v0, LX/6e6;->c:LX/6e6;

    return-object v0

    .line 1117484
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1117485
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
