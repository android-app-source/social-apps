.class public interface abstract LX/580;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/57x;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "PlaceListAttachmentTarget"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract H()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract I()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getListItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
