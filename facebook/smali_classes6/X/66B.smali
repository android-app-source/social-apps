.class public final LX/66B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field public final synthetic a:LX/66H;

.field private final b:LX/675;

.field private c:Z


# direct methods
.method public constructor <init>(LX/66H;)V
    .locals 2

    .prologue
    .line 1049582
    iput-object p1, p0, LX/66B;->a:LX/66H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049583
    new-instance v0, LX/675;

    iget-object v1, p0, LX/66B;->a:LX/66H;

    iget-object v1, v1, LX/66H;->d:LX/670;

    invoke-interface {v1}, LX/65J;->a()LX/65f;

    move-result-object v1

    invoke-direct {v0, v1}, LX/675;-><init>(LX/65f;)V

    iput-object v0, p0, LX/66B;->b:LX/675;

    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1049581
    iget-object v0, p0, LX/66B;->b:LX/675;

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 2

    .prologue
    .line 1049557
    iget-boolean v0, p0, LX/66B;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049558
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 1049559
    :goto_0
    return-void

    .line 1049560
    :cond_1
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    invoke-interface {v0, p2, p3}, LX/670;->j(J)LX/670;

    .line 1049561
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1049562
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    invoke-interface {v0, p1, p2, p3}, LX/65J;->a_(LX/672;J)V

    .line 1049563
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    goto :goto_0
.end method

.method public final declared-synchronized close()V
    .locals 3

    .prologue
    .line 1049568
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/66B;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1049569
    :goto_0
    monitor-exit p0

    return-void

    .line 1049570
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/66B;->c:Z

    .line 1049571
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    const-string v1, "0\r\n\r\n"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1049572
    iget-object v1, p0, LX/66B;->b:LX/675;

    .line 1049573
    iget-object v2, v1, LX/675;->a:LX/65f;

    move-object v2, v2

    .line 1049574
    sget-object v0, LX/65f;->b:LX/65f;

    invoke-virtual {v1, v0}, LX/675;->a(LX/65f;)LX/675;

    .line 1049575
    invoke-virtual {v2}, LX/65f;->f()LX/65f;

    .line 1049576
    invoke-virtual {v2}, LX/65f;->bL_()LX/65f;

    .line 1049577
    iget-object v0, p0, LX/66B;->a:LX/66H;

    const/4 v1, 0x3

    .line 1049578
    iput v1, v0, LX/66H;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1049579
    goto :goto_0

    .line 1049580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized flush()V
    .locals 1

    .prologue
    .line 1049564
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/66B;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1049565
    :goto_0
    monitor-exit p0

    return-void

    .line 1049566
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/66B;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1049567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
