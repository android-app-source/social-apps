.class public final LX/5hv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 981112
    const/16 v26, 0x0

    .line 981113
    const/16 v25, 0x0

    .line 981114
    const/16 v24, 0x0

    .line 981115
    const/16 v23, 0x0

    .line 981116
    const/16 v22, 0x0

    .line 981117
    const/16 v21, 0x0

    .line 981118
    const/16 v20, 0x0

    .line 981119
    const/16 v19, 0x0

    .line 981120
    const/16 v18, 0x0

    .line 981121
    const/16 v17, 0x0

    .line 981122
    const/16 v16, 0x0

    .line 981123
    const/4 v15, 0x0

    .line 981124
    const/4 v14, 0x0

    .line 981125
    const/4 v13, 0x0

    .line 981126
    const/4 v12, 0x0

    .line 981127
    const/4 v11, 0x0

    .line 981128
    const/4 v10, 0x0

    .line 981129
    const/4 v9, 0x0

    .line 981130
    const/4 v8, 0x0

    .line 981131
    const/4 v7, 0x0

    .line 981132
    const/4 v6, 0x0

    .line 981133
    const/4 v5, 0x0

    .line 981134
    const/4 v4, 0x0

    .line 981135
    const/4 v3, 0x0

    .line 981136
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 981137
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 981138
    const/4 v3, 0x0

    .line 981139
    :goto_0
    return v3

    .line 981140
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 981141
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_12

    .line 981142
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 981143
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 981144
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 981145
    const-string v28, "__type__"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_2

    const-string v28, "__typename"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 981146
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v26

    goto :goto_1

    .line 981147
    :cond_3
    const-string v28, "bitrate"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 981148
    const/4 v10, 0x1

    .line 981149
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto :goto_1

    .line 981150
    :cond_4
    const-string v28, "creation_story"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 981151
    invoke-static/range {p0 .. p1}, LX/5hu;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 981152
    :cond_5
    const-string v28, "hdBitrate"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 981153
    const/4 v9, 0x1

    .line 981154
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v23

    goto :goto_1

    .line 981155
    :cond_6
    const-string v28, "height"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 981156
    const/4 v8, 0x1

    .line 981157
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    goto :goto_1

    .line 981158
    :cond_7
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 981159
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 981160
    :cond_8
    const-string v28, "image"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 981161
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 981162
    :cond_9
    const-string v28, "is_playable"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 981163
    const/4 v7, 0x1

    .line 981164
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto/16 :goto_1

    .line 981165
    :cond_a
    const-string v28, "is_video_broadcast"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 981166
    const/4 v6, 0x1

    .line 981167
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto/16 :goto_1

    .line 981168
    :cond_b
    const-string v28, "playableUrlHdString"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 981169
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 981170
    :cond_c
    const-string v28, "playable_duration_in_ms"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 981171
    const/4 v5, 0x1

    .line 981172
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 981173
    :cond_d
    const-string v28, "playable_url"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 981174
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 981175
    :cond_e
    const-string v28, "playlist"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 981176
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 981177
    :cond_f
    const-string v28, "preferredPlayableUrlString"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 981178
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 981179
    :cond_10
    const-string v28, "supports_time_slices"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 981180
    const/4 v4, 0x1

    .line 981181
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 981182
    :cond_11
    const-string v28, "width"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 981183
    const/4 v3, 0x1

    .line 981184
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 981185
    :cond_12
    const/16 v27, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 981186
    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 981187
    if-eqz v10, :cond_13

    .line 981188
    const/4 v10, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v10, v1, v2}, LX/186;->a(III)V

    .line 981189
    :cond_13
    const/4 v10, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 981190
    if-eqz v9, :cond_14

    .line 981191
    const/4 v9, 0x3

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 981192
    :cond_14
    if-eqz v8, :cond_15

    .line 981193
    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 981194
    :cond_15
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 981195
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 981196
    if-eqz v7, :cond_16

    .line 981197
    const/4 v7, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 981198
    :cond_16
    if-eqz v6, :cond_17

    .line 981199
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 981200
    :cond_17
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 981201
    if-eqz v5, :cond_18

    .line 981202
    const/16 v5, 0xa

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 981203
    :cond_18
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 981204
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 981205
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 981206
    if-eqz v4, :cond_19

    .line 981207
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 981208
    :cond_19
    if-eqz v3, :cond_1a

    .line 981209
    const/16 v3, 0xf

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11, v4}, LX/186;->a(III)V

    .line 981210
    :cond_1a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 981045
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 981046
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 981047
    if-eqz v0, :cond_0

    .line 981048
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981049
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 981050
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 981051
    if-eqz v0, :cond_1

    .line 981052
    const-string v1, "bitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981053
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981054
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981055
    if-eqz v0, :cond_2

    .line 981056
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981057
    invoke-static {p0, v0, p2, p3}, LX/5hu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 981058
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 981059
    if-eqz v0, :cond_3

    .line 981060
    const-string v1, "hdBitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981061
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981062
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 981063
    if-eqz v0, :cond_4

    .line 981064
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981065
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981066
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981067
    if-eqz v0, :cond_5

    .line 981068
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981069
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981070
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981071
    if-eqz v0, :cond_6

    .line 981072
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981073
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 981074
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981075
    if-eqz v0, :cond_7

    .line 981076
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981077
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981078
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981079
    if-eqz v0, :cond_8

    .line 981080
    const-string v1, "is_video_broadcast"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981081
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981082
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981083
    if-eqz v0, :cond_9

    .line 981084
    const-string v1, "playableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981085
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981086
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 981087
    if-eqz v0, :cond_a

    .line 981088
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981089
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981090
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981091
    if-eqz v0, :cond_b

    .line 981092
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981093
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981094
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981095
    if-eqz v0, :cond_c

    .line 981096
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981097
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981098
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981099
    if-eqz v0, :cond_d

    .line 981100
    const-string v1, "preferredPlayableUrlString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981101
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981102
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981103
    if-eqz v0, :cond_e

    .line 981104
    const-string v1, "supports_time_slices"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981105
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981106
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 981107
    if-eqz v0, :cond_f

    .line 981108
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981109
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981110
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 981111
    return-void
.end method
