.class public LX/61I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([F)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/high16 v2, -0x41000000    # -0.5f

    const/4 v1, 0x0

    .line 1039779
    invoke-static {p0, v1, v0, v0, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1039780
    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p0, v1, v4, v0, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1039781
    invoke-static {p0, v1, v2, v2, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1039782
    return-void
.end method

.method public static a([FI)V
    .locals 7

    .prologue
    const/high16 v0, 0x3f000000    # 0.5f

    const/high16 v6, -0x41000000    # -0.5f

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1039783
    if-nez p1, :cond_0

    .line 1039784
    :goto_0
    return-void

    .line 1039785
    :cond_0
    invoke-static {p0, v1, v0, v0, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1039786
    int-to-float v2, p1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1039787
    invoke-static {p0, v1, v6, v6, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    goto :goto_0
.end method

.method public static a([FLandroid/graphics/RectF;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1039788
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    const/4 v2, 0x0

    invoke-static {p0, v3, v0, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1039789
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p0, v3, v0, v1, v2}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1039790
    return-void
.end method
