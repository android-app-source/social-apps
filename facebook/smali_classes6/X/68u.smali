.class public LX/68u;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:LX/68t;

.field public static final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Landroid/view/animation/Interpolator;

.field private static final i:LX/68r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68r",
            "<",
            "LX/68u;",
            ">;"
        }
    .end annotation
.end field

.field public static w:J


# instance fields
.field public A:Ljava/lang/Object;

.field public B:Landroid/view/animation/Interpolator;

.field public C:F

.field public D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67p;",
            ">;"
        }
    .end annotation
.end field

.field public E:[F

.field public F:[F

.field public G:I

.field public H:F

.field public I:Z

.field public j:J

.field public k:J

.field private l:Z

.field private m:I

.field public n:F

.field private o:Z

.field private p:J

.field public q:I

.field public r:Z

.field public s:Z

.field public t:Z

.field private u:J

.field public v:J

.field public x:I

.field private y:I

.field public z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1057061
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/68u;->a:Ljava/lang/Object;

    .line 1057062
    const/4 v0, 0x0

    sput-object v0, LX/68u;->b:LX/68t;

    .line 1057063
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/68u;->c:Ljava/util/ArrayList;

    .line 1057064
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/68u;->d:Ljava/util/ArrayList;

    .line 1057065
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/68u;->e:Ljava/util/ArrayList;

    .line 1057066
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/68u;->f:Ljava/util/ArrayList;

    .line 1057067
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/68u;->g:Ljava/util/ArrayList;

    .line 1057068
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, LX/68u;->h:Landroid/view/animation/Interpolator;

    .line 1057069
    new-instance v0, LX/68r;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/68r;-><init>(I)V

    sput-object v0, LX/68u;->i:LX/68r;

    .line 1057070
    const-wide/16 v0, 0xa

    sput-wide v0, LX/68u;->w:J

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1057040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1057041
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/68u;->k:J

    .line 1057042
    iput-boolean v2, p0, LX/68u;->l:Z

    .line 1057043
    iput v2, p0, LX/68u;->m:I

    .line 1057044
    const/4 v0, 0x0

    iput v0, p0, LX/68u;->n:F

    .line 1057045
    iput-boolean v2, p0, LX/68u;->o:Z

    .line 1057046
    iput v2, p0, LX/68u;->q:I

    .line 1057047
    iput-boolean v2, p0, LX/68u;->r:Z

    .line 1057048
    iput-boolean v2, p0, LX/68u;->s:Z

    .line 1057049
    iput-boolean v2, p0, LX/68u;->t:Z

    .line 1057050
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, LX/68u;->u:J

    .line 1057051
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/68u;->v:J

    .line 1057052
    iput v2, p0, LX/68u;->x:I

    .line 1057053
    iput v3, p0, LX/68u;->y:I

    .line 1057054
    iput-object v5, p0, LX/68u;->z:Ljava/util/ArrayList;

    .line 1057055
    sget-object v0, LX/68u;->h:Landroid/view/animation/Interpolator;

    iput-object v0, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    .line 1057056
    iput-object v5, p0, LX/68u;->D:Ljava/util/ArrayList;

    .line 1057057
    new-array v0, v4, [F

    iput-object v0, p0, LX/68u;->E:[F

    .line 1057058
    new-array v0, v4, [F

    iput-object v0, p0, LX/68u;->F:[F

    .line 1057059
    iput-boolean v3, p0, LX/68u;->I:Z

    .line 1057060
    return-void
.end method

.method public static a(FF)LX/68u;
    .locals 5

    .prologue
    .line 1057029
    sget-object v0, LX/68u;->i:LX/68r;

    invoke-virtual {v0}, LX/68r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1057030
    if-nez v0, :cond_0

    .line 1057031
    new-instance v0, LX/68u;

    invoke-direct {v0}, LX/68u;-><init>()V

    .line 1057032
    :cond_0
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1057033
    const/4 v1, 0x2

    iput v1, v0, LX/68u;->G:I

    .line 1057034
    iget-object v1, v0, LX/68u;->E:[F

    const/4 v2, 0x0

    aput v2, v1, v3

    .line 1057035
    iget-object v1, v0, LX/68u;->F:[F

    aput p0, v1, v3

    .line 1057036
    iget-object v1, v0, LX/68u;->E:[F

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v4

    .line 1057037
    iget-object v1, v0, LX/68u;->F:[F

    aput p1, v1, v4

    .line 1057038
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/68u;->t:Z

    .line 1057039
    return-object v0
.end method

.method public static a(II)LX/68u;
    .locals 5

    .prologue
    .line 1057018
    sget-object v0, LX/68u;->i:LX/68r;

    invoke-virtual {v0}, LX/68r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68u;

    .line 1057019
    if-nez v0, :cond_0

    .line 1057020
    new-instance v0, LX/68u;

    invoke-direct {v0}, LX/68u;-><init>()V

    .line 1057021
    :cond_0
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1057022
    const/4 v1, 0x2

    iput v1, v0, LX/68u;->G:I

    .line 1057023
    iget-object v1, v0, LX/68u;->E:[F

    const/4 v2, 0x0

    aput v2, v1, v3

    .line 1057024
    iget-object v1, v0, LX/68u;->F:[F

    int-to-float v2, p0

    aput v2, v1, v3

    .line 1057025
    iget-object v1, v0, LX/68u;->E:[F

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v4

    .line 1057026
    iget-object v1, v0, LX/68u;->F:[F

    int-to-float v2, p1

    aput v2, v1, v4

    .line 1057027
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/68u;->t:Z

    .line 1057028
    return-object v0
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1056990
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1056991
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animators may only be run on Looper threads"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1056992
    :cond_0
    iput-boolean p1, p0, LX/68u;->l:Z

    .line 1056993
    iput v1, p0, LX/68u;->m:I

    .line 1056994
    iput v1, p0, LX/68u;->q:I

    .line 1056995
    iput-boolean v6, p0, LX/68u;->s:Z

    .line 1056996
    iput-boolean v1, p0, LX/68u;->o:Z

    .line 1056997
    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056998
    iget-wide v2, p0, LX/68u;->v:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 1056999
    iget-boolean v7, p0, LX/68u;->t:Z

    if-eqz v7, :cond_1

    iget v7, p0, LX/68u;->q:I

    if-nez v7, :cond_4

    .line 1057000
    :cond_1
    const-wide/16 v7, 0x0

    .line 1057001
    :goto_0
    move-wide v2, v7

    .line 1057002
    const/4 v11, 0x1

    .line 1057003
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v9

    .line 1057004
    iget v7, p0, LX/68u;->q:I

    if-eq v7, v11, :cond_2

    .line 1057005
    iput-wide v2, p0, LX/68u;->k:J

    .line 1057006
    iget-boolean v7, p0, LX/68u;->t:Z

    if-eqz v7, :cond_5

    const/4 v7, 0x2

    :goto_1
    iput v7, p0, LX/68u;->q:I

    .line 1057007
    :cond_2
    sub-long v7, v9, v2

    iput-wide v7, p0, LX/68u;->j:J

    .line 1057008
    iput-boolean v11, p0, LX/68u;->t:Z

    .line 1057009
    invoke-static {p0, v9, v10}, LX/68u;->d(LX/68u;J)Z

    .line 1057010
    iput-boolean v6, p0, LX/68u;->r:Z

    .line 1057011
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1057012
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_3

    .line 1057013
    iget-object v3, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1057014
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1057015
    :cond_3
    invoke-static {}, LX/68u;->p()LX/68t;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/68t;->sendEmptyMessage(I)Z

    .line 1057016
    return-void

    :cond_4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, LX/68u;->j:J

    sub-long/2addr v7, v9

    goto :goto_0

    .line 1057017
    :cond_5
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static c(LX/68u;J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1056982
    iget-boolean v1, p0, LX/68u;->o:Z

    if-nez v1, :cond_1

    .line 1056983
    iput-boolean v0, p0, LX/68u;->o:Z

    .line 1056984
    iput-wide p1, p0, LX/68u;->p:J

    .line 1056985
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1056986
    :cond_1
    iget-wide v2, p0, LX/68u;->p:J

    sub-long v2, p1, v2

    .line 1056987
    iget-wide v4, p0, LX/68u;->v:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1056988
    iget-wide v4, p0, LX/68u;->v:J

    sub-long/2addr v2, v4

    sub-long v2, p1, v2

    iput-wide v2, p0, LX/68u;->j:J

    .line 1056989
    iput v0, p0, LX/68u;->q:I

    goto :goto_0
.end method

.method public static d(LX/68u;J)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1056929
    iget v2, p0, LX/68u;->q:I

    if-nez v2, :cond_0

    .line 1056930
    iput v0, p0, LX/68u;->q:I

    .line 1056931
    iget-wide v2, p0, LX/68u;->k:J

    cmp-long v2, v2, v6

    if-gez v2, :cond_1

    .line 1056932
    iput-wide p1, p0, LX/68u;->j:J

    .line 1056933
    :cond_0
    :goto_0
    iget v2, p0, LX/68u;->q:I

    packed-switch v2, :pswitch_data_0

    .line 1056934
    :goto_1
    return v1

    .line 1056935
    :cond_1
    iget-wide v2, p0, LX/68u;->k:J

    sub-long v2, p1, v2

    iput-wide v2, p0, LX/68u;->j:J

    .line 1056936
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/68u;->k:J

    goto :goto_0

    .line 1056937
    :pswitch_0
    iget-wide v2, p0, LX/68u;->u:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    iget-wide v2, p0, LX/68u;->j:J

    sub-long v2, p1, v2

    long-to-float v2, v2

    iget-wide v6, p0, LX/68u;->u:J

    long-to-float v3, v6

    div-float/2addr v2, v3

    .line 1056938
    :goto_2
    cmpl-float v3, v2, v4

    if-ltz v3, :cond_b

    .line 1056939
    iget v3, p0, LX/68u;->m:I

    iget v5, p0, LX/68u;->x:I

    if-lt v3, v5, :cond_2

    iget v3, p0, LX/68u;->x:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_a

    .line 1056940
    :cond_2
    iget-object v3, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 1056941
    iget-object v3, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_4

    .line 1056942
    iget-object v6, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1056943
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    move v2, v4

    .line 1056944
    goto :goto_2

    .line 1056945
    :cond_4
    iget v3, p0, LX/68u;->y:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_6

    .line 1056946
    iget-boolean v3, p0, LX/68u;->l:Z

    if-eqz v3, :cond_5

    move v0, v1

    :cond_5
    iput-boolean v0, p0, LX/68u;->l:Z

    .line 1056947
    :cond_6
    iget v0, p0, LX/68u;->m:I

    float-to-int v3, v2

    add-int/2addr v0, v3

    iput v0, p0, LX/68u;->m:I

    .line 1056948
    rem-float v0, v2, v4

    .line 1056949
    iget-wide v2, p0, LX/68u;->j:J

    iget-wide v6, p0, LX/68u;->u:J

    add-long/2addr v2, v6

    iput-wide v2, p0, LX/68u;->j:J

    .line 1056950
    :goto_4
    iget-boolean v2, p0, LX/68u;->l:Z

    if-eqz v2, :cond_7

    .line 1056951
    sub-float v0, v4, v0

    .line 1056952
    :cond_7
    iput v0, p0, LX/68u;->n:F

    .line 1056953
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1056954
    iget v3, p0, LX/68u;->G:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 1056955
    iget-boolean v3, p0, LX/68u;->I:Z

    if-eqz v3, :cond_8

    .line 1056956
    iput-boolean v6, p0, LX/68u;->I:Z

    .line 1056957
    iget-object v3, p0, LX/68u;->F:[F

    aget v2, v3, v2

    iget-object v3, p0, LX/68u;->F:[F

    aget v3, v3, v6

    sub-float/2addr v2, v3

    iput v2, p0, LX/68u;->H:F

    .line 1056958
    :cond_8
    iget-object v2, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    .line 1056959
    iget-object v3, p0, LX/68u;->F:[F

    aget v3, v3, v6

    iget v4, p0, LX/68u;->H:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    .line 1056960
    :goto_5
    move v2, v2

    .line 1056961
    iput v2, p0, LX/68u;->C:F

    .line 1056962
    iget-object v2, p0, LX/68u;->z:Ljava/util/ArrayList;

    if-eqz v2, :cond_9

    .line 1056963
    const/4 v2, 0x0

    iget-object v3, p0, LX/68u;->z:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_6
    if-ge v3, v4, :cond_9

    .line 1056964
    iget-object v2, p0, LX/68u;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/67q;

    invoke-interface {v2, p0}, LX/67q;->a(LX/68u;)V

    .line 1056965
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 1056966
    :cond_9
    goto/16 :goto_1

    .line 1056967
    :cond_a
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_4

    .line 1056968
    :cond_c
    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_d

    .line 1056969
    iget-object v3, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    .line 1056970
    iget-object v4, p0, LX/68u;->E:[F

    aget v4, v4, v6

    sub-float/2addr v3, v4

    iget-object v4, p0, LX/68u;->E:[F

    aget v4, v4, v2

    iget-object v5, p0, LX/68u;->E:[F

    aget v5, v5, v6

    sub-float/2addr v4, v5

    div-float/2addr v3, v4

    .line 1056971
    iget-object v4, p0, LX/68u;->F:[F

    aget v4, v4, v6

    iget-object v5, p0, LX/68u;->F:[F

    aget v2, v5, v2

    iget-object v5, p0, LX/68u;->F:[F

    aget v5, v5, v6

    sub-float/2addr v2, v5

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    goto :goto_5

    .line 1056972
    :cond_d
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_f

    .line 1056973
    iget-object v2, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    .line 1056974
    iget-object v3, p0, LX/68u;->E:[F

    iget v4, p0, LX/68u;->G:I

    add-int/lit8 v4, v4, -0x2

    aget v3, v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, LX/68u;->E:[F

    iget v4, p0, LX/68u;->G:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    iget-object v4, p0, LX/68u;->E:[F

    iget v5, p0, LX/68u;->G:I

    add-int/lit8 v5, v5, -0x2

    aget v4, v4, v5

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    .line 1056975
    iget-object v3, p0, LX/68u;->F:[F

    iget v4, p0, LX/68u;->G:I

    add-int/lit8 v4, v4, -0x2

    aget v3, v3, v4

    iget-object v4, p0, LX/68u;->F:[F

    iget v5, p0, LX/68u;->G:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    iget-object v5, p0, LX/68u;->F:[F

    iget v6, p0, LX/68u;->G:I

    add-int/lit8 v6, v6, -0x2

    aget v5, v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    goto/16 :goto_5

    .line 1056976
    :cond_e
    add-int/lit8 v2, v2, 0x1

    :cond_f
    iget v3, p0, LX/68u;->G:I

    if-ge v2, v3, :cond_10

    .line 1056977
    iget-object v3, p0, LX/68u;->E:[F

    aget v3, v3, v2

    cmpg-float v3, v0, v3

    if-gez v3, :cond_e

    .line 1056978
    iget-object v3, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    .line 1056979
    iget-object v4, p0, LX/68u;->E:[F

    add-int/lit8 v5, v2, -0x1

    aget v4, v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, LX/68u;->E:[F

    aget v4, v4, v2

    iget-object v5, p0, LX/68u;->E:[F

    add-int/lit8 v6, v2, -0x1

    aget v5, v5, v6

    sub-float/2addr v4, v5

    div-float/2addr v3, v4

    .line 1056980
    iget-object v4, p0, LX/68u;->F:[F

    add-int/lit8 v5, v2, -0x1

    aget v4, v4, v5

    iget-object v5, p0, LX/68u;->F:[F

    aget v5, v5, v2

    iget-object v6, p0, LX/68u;->F:[F

    add-int/lit8 v2, v2, -0x1

    aget v2, v6, v2

    sub-float v2, v5, v2

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    goto/16 :goto_5

    .line 1056981
    :cond_10
    iget-object v2, p0, LX/68u;->F:[F

    iget v3, p0, LX/68u;->G:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static p()LX/68t;
    .locals 3

    .prologue
    .line 1056905
    sget-object v1, LX/68u;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1056906
    :try_start_0
    sget-object v0, LX/68u;->b:LX/68t;

    if-nez v0, :cond_0

    .line 1056907
    new-instance v0, LX/68t;

    invoke-direct {v0}, LX/68t;-><init>()V

    sput-object v0, LX/68u;->b:LX/68t;

    .line 1056908
    :cond_0
    sget-object v0, LX/68u;->b:LX/68t;

    monitor-exit v1

    return-object v0

    .line 1056909
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static r(LX/68u;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1056917
    sget-object v1, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1056918
    sget-object v1, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1056919
    sget-object v1, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1056920
    iput v0, p0, LX/68u;->q:I

    .line 1056921
    iput-boolean v0, p0, LX/68u;->s:Z

    .line 1056922
    iget-boolean v1, p0, LX/68u;->r:Z

    if-eqz v1, :cond_0

    .line 1056923
    iput-boolean v0, p0, LX/68u;->r:Z

    .line 1056924
    iget-object v1, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1056925
    iget-object v1, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1056926
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67p;

    invoke-interface {v0, p0}, LX/67p;->b(LX/68u;)V

    .line 1056927
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1056928
    :cond_0
    return-void
.end method

.method public static s(LX/68u;)V
    .locals 4

    .prologue
    .line 1056910
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/68u;->t:Z

    .line 1056911
    sget-object v0, LX/68u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056912
    iget-wide v0, p0, LX/68u;->v:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1056913
    const/4 v0, 0x0

    iget-object v1, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1056914
    iget-object v2, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1056915
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1056916
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)LX/68u;
    .locals 3

    .prologue
    .line 1057071
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1057072
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Animators cannot have negative duration: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1057073
    :cond_0
    iput-wide p1, p0, LX/68u;->u:J

    .line 1057074
    return-object p0
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1056853
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1056854
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056855
    :cond_0
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1056856
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056857
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/68u;->A:Ljava/lang/Object;

    .line 1056858
    iput-wide v4, p0, LX/68u;->j:J

    .line 1056859
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/68u;->k:J

    .line 1056860
    iput-boolean v2, p0, LX/68u;->l:Z

    .line 1056861
    iput v2, p0, LX/68u;->m:I

    .line 1056862
    iput v3, p0, LX/68u;->n:F

    .line 1056863
    iput-boolean v2, p0, LX/68u;->o:Z

    .line 1056864
    iput-wide v4, p0, LX/68u;->p:J

    .line 1056865
    iput v2, p0, LX/68u;->q:I

    .line 1056866
    iput-boolean v2, p0, LX/68u;->r:Z

    .line 1056867
    iput-boolean v2, p0, LX/68u;->s:Z

    .line 1056868
    iput-boolean v2, p0, LX/68u;->t:Z

    .line 1056869
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, LX/68u;->u:J

    .line 1056870
    iput-wide v4, p0, LX/68u;->v:J

    .line 1056871
    iput v2, p0, LX/68u;->x:I

    .line 1056872
    iput v6, p0, LX/68u;->y:I

    .line 1056873
    sget-object v0, LX/68u;->h:Landroid/view/animation/Interpolator;

    iput-object v0, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    .line 1056874
    iput v3, p0, LX/68u;->C:F

    .line 1056875
    iput-boolean v6, p0, LX/68u;->I:Z

    .line 1056876
    sget-object v0, LX/68u;->i:LX/68r;

    invoke-virtual {v0, p0}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1056877
    return-void
.end method

.method public final a(LX/67p;)V
    .locals 1

    .prologue
    .line 1056878
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1056879
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    .line 1056880
    :cond_0
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056881
    return-void
.end method

.method public final a(LX/67q;)V
    .locals 1

    .prologue
    .line 1056882
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1056883
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    .line 1056884
    :cond_0
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056885
    return-void
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 1056886
    if-eqz p1, :cond_0

    .line 1056887
    iput-object p1, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    .line 1056888
    :goto_0
    return-void

    .line 1056889
    :cond_0
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, LX/68u;->B:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1056890
    iget v0, p0, LX/68u;->C:F

    float-to-int v0, v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1056891
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1056892
    :goto_0
    return-void

    .line 1056893
    :cond_0
    iget-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056894
    const/4 v0, 0x0

    iput-object v0, p0, LX/68u;->z:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1056895
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/68u;->a(Z)V

    .line 1056896
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1056897
    iget v0, p0, LX/68u;->q:I

    if-nez v0, :cond_0

    sget-object v0, LX/68u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/68u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1056898
    :cond_0
    iget-boolean v0, p0, LX/68u;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1056899
    const/4 v0, 0x0

    iget-object v1, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1056900
    iget-object v0, p0, LX/68u;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67p;

    invoke-interface {v0, p0}, LX/67p;->c(LX/68u;)V

    .line 1056901
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1056902
    :cond_1
    invoke-static {p0}, LX/68u;->r(LX/68u;)V

    .line 1056903
    :cond_2
    return-void
.end method

.method public final h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1056904
    iget v1, p0, LX/68u;->q:I

    if-eq v1, v0, :cond_0

    iget-boolean v1, p0, LX/68u;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
