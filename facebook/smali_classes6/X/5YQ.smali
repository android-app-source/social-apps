.class public final LX/5YQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 941545
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 941546
    const/4 v11, 0x0

    .line 941547
    const/4 v10, 0x0

    .line 941548
    const/4 v9, 0x0

    .line 941549
    const/4 v8, 0x0

    .line 941550
    const/4 v7, 0x0

    .line 941551
    const/4 v6, 0x0

    .line 941552
    const-wide/16 v4, 0x0

    .line 941553
    const/4 v3, 0x0

    .line 941554
    const/4 v2, 0x0

    .line 941555
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_1

    .line 941556
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 941557
    const/4 v2, 0x0

    .line 941558
    :goto_0
    move v1, v2

    .line 941559
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 941560
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 941561
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 941562
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, v13, :cond_8

    .line 941563
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 941564
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 941565
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_1

    if-eqz v12, :cond_1

    .line 941566
    const-string v13, "event_title"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 941567
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 941568
    :cond_2
    const-string v13, "firstThreeMembers"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 941569
    invoke-static {p0, v0}, LX/5YP;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 941570
    :cond_3
    const-string v13, "id"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 941571
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 941572
    :cond_4
    const-string v13, "lightweight_event_status"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 941573
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 941574
    :cond_5
    const-string v13, "lightweight_event_type"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 941575
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 941576
    :cond_6
    const-string v13, "seconds_to_notify_before"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 941577
    const/4 v3, 0x1

    .line 941578
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 941579
    :cond_7
    const-string v13, "time"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 941580
    const/4 v2, 0x1

    .line 941581
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 941582
    :cond_8
    const/4 v12, 0x7

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 941583
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 941584
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 941585
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 941586
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 941587
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 941588
    if-eqz v3, :cond_9

    .line 941589
    const/4 v3, 0x5

    const/4 v7, 0x0

    invoke-virtual {v0, v3, v6, v7}, LX/186;->a(III)V

    .line 941590
    :cond_9
    if-eqz v2, :cond_a

    .line 941591
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 941592
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
