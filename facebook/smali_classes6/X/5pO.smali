.class public final enum LX/5pO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5pO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5pO;

.field public static final enum ARRAY:LX/5pO;

.field public static final enum EMPTY_ARRAY:LX/5pO;

.field public static final enum EMPTY_OBJECT:LX/5pO;

.field public static final enum OBJECT:LX/5pO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1008067
    new-instance v0, LX/5pO;

    const-string v1, "EMPTY_OBJECT"

    invoke-direct {v0, v1, v2}, LX/5pO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pO;->EMPTY_OBJECT:LX/5pO;

    .line 1008068
    new-instance v0, LX/5pO;

    const-string v1, "OBJECT"

    invoke-direct {v0, v1, v3}, LX/5pO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pO;->OBJECT:LX/5pO;

    .line 1008069
    new-instance v0, LX/5pO;

    const-string v1, "EMPTY_ARRAY"

    invoke-direct {v0, v1, v4}, LX/5pO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pO;->EMPTY_ARRAY:LX/5pO;

    .line 1008070
    new-instance v0, LX/5pO;

    const-string v1, "ARRAY"

    invoke-direct {v0, v1, v5}, LX/5pO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5pO;->ARRAY:LX/5pO;

    .line 1008071
    const/4 v0, 0x4

    new-array v0, v0, [LX/5pO;

    sget-object v1, LX/5pO;->EMPTY_OBJECT:LX/5pO;

    aput-object v1, v0, v2

    sget-object v1, LX/5pO;->OBJECT:LX/5pO;

    aput-object v1, v0, v3

    sget-object v1, LX/5pO;->EMPTY_ARRAY:LX/5pO;

    aput-object v1, v0, v4

    sget-object v1, LX/5pO;->ARRAY:LX/5pO;

    aput-object v1, v0, v5

    sput-object v0, LX/5pO;->$VALUES:[LX/5pO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1008066
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5pO;
    .locals 1

    .prologue
    .line 1008064
    const-class v0, LX/5pO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5pO;

    return-object v0
.end method

.method public static values()[LX/5pO;
    .locals 1

    .prologue
    .line 1008065
    sget-object v0, LX/5pO;->$VALUES:[LX/5pO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5pO;

    return-object v0
.end method
