.class public final LX/5BO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 863567
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 863568
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 863569
    :goto_0
    return v1

    .line 863570
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 863571
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 863572
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 863573
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 863574
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 863575
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 863576
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 863577
    invoke-static {p0, p1}, LX/5BN;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 863578
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 863579
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 863580
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 863581
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 863582
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 863583
    if-eqz v3, :cond_5

    .line 863584
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 863585
    :cond_5
    if-eqz v0, :cond_6

    .line 863586
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 863587
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 863588
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 863589
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 863590
    if-eqz v0, :cond_0

    .line 863591
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863592
    invoke-static {p0, v0, p2}, LX/5BN;->a(LX/15i;ILX/0nX;)V

    .line 863593
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 863594
    if-eqz v0, :cond_1

    .line 863595
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863596
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 863597
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 863598
    if-eqz v0, :cond_2

    .line 863599
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863600
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 863601
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 863602
    return-void
.end method
