.class public final LX/6eJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1117788
    iput-object p1, p0, LX/6eJ;->b:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    iput-object p2, p0, LX/6eJ;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1117787
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1117783
    check-cast p1, Ljava/lang/Long;

    .line 1117784
    if-eqz p1, :cond_0

    .line 1117785
    iget-object v0, p0, LX/6eJ;->b:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    iget-object v1, p0, LX/6eJ;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a$redex0(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;Ljava/lang/String;J)V

    .line 1117786
    :cond_0
    return-void
.end method
