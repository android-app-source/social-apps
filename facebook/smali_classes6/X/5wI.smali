.class public final LX/5wI;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1023843
    const-class v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    const v0, -0x147e6f12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "UserTimelineSelfQuery"

    const-string v6, "9adaa7683ccaf6b38ffba4cd639a4c6a"

    const-string v7, "viewer"

    const-string v8, "10155259384041729"

    const-string v9, "10155260036846729"

    .line 1023844
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1023845
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1023846
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1023847
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1023848
    sparse-switch v0, :sswitch_data_0

    .line 1023849
    :goto_0
    return-object p1

    .line 1023850
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1023851
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1023852
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1023853
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1023854
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1023855
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1023856
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1023857
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1023858
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1023859
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1023860
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1023861
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1023862
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1023863
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1023864
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1023865
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1023866
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1023867
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1023868
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1023869
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1023870
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1023871
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1023872
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1023873
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1023874
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1023875
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1023876
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1023877
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1023878
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1023879
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1023880
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1023881
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76a93de2 -> :sswitch_19
        -0x6b335858 -> :sswitch_13
        -0x6ad64185 -> :sswitch_8
        -0x6a24640d -> :sswitch_11
        -0x680de62a -> :sswitch_c
        -0x647507c2 -> :sswitch_1d
        -0x6326fdb3 -> :sswitch_f
        -0x5c5a7b55 -> :sswitch_16
        -0x595e5196 -> :sswitch_14
        -0x49bc39fd -> :sswitch_3
        -0x4496acc9 -> :sswitch_b
        -0x2a0a3d40 -> :sswitch_15
        -0x1b87b280 -> :sswitch_10
        -0x19a96e0d -> :sswitch_1c
        -0x12efdeb3 -> :sswitch_e
        -0x93a55fc -> :sswitch_1
        -0x318ed33 -> :sswitch_4
        0xc076c1 -> :sswitch_6
        0x1312dd1 -> :sswitch_2
        0x759c0e8 -> :sswitch_1e
        0xa1fa812 -> :sswitch_12
        0x14658929 -> :sswitch_a
        0x155c0cca -> :sswitch_7
        0x214100e0 -> :sswitch_d
        0x291d8de0 -> :sswitch_1b
        0x2d750402 -> :sswitch_9
        0x3052e0ff -> :sswitch_0
        0x48619927 -> :sswitch_5
        0x5a19c1ec -> :sswitch_1f
        0x5db1e22c -> :sswitch_18
        0x6c6f579a -> :sswitch_1a
        0x73a026b5 -> :sswitch_17
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1023882
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1023883
    :goto_1
    return v0

    .line 1023884
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 1023885
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1023886
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1023887
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1023888
    :pswitch_3
    const-string v0, "contain-fit"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x34 -> :sswitch_2
        0x61f -> :sswitch_1
        0x626 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
