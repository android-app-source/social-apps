.class public final enum LX/6YD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6YD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6YD;

.field public static final enum ERROR:LX/6YD;

.field public static final enum MAYBE_SUCCESS:LX/6YD;

.field public static final enum SUCCESS:LX/6YD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1109417
    new-instance v0, LX/6YD;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/6YD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YD;->SUCCESS:LX/6YD;

    .line 1109418
    new-instance v0, LX/6YD;

    const-string v1, "MAYBE_SUCCESS"

    invoke-direct {v0, v1, v3}, LX/6YD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YD;->MAYBE_SUCCESS:LX/6YD;

    .line 1109419
    new-instance v0, LX/6YD;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/6YD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YD;->ERROR:LX/6YD;

    .line 1109420
    const/4 v0, 0x3

    new-array v0, v0, [LX/6YD;

    sget-object v1, LX/6YD;->SUCCESS:LX/6YD;

    aput-object v1, v0, v2

    sget-object v1, LX/6YD;->MAYBE_SUCCESS:LX/6YD;

    aput-object v1, v0, v3

    sget-object v1, LX/6YD;->ERROR:LX/6YD;

    aput-object v1, v0, v4

    sput-object v0, LX/6YD;->$VALUES:[LX/6YD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1109410
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromStatus(Ljava/lang/String;)LX/6YD;
    .locals 1

    .prologue
    .line 1109411
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1109412
    sget-object v0, LX/6YD;->ERROR:LX/6YD;

    .line 1109413
    :goto_0
    return-object v0

    .line 1109414
    :pswitch_0
    sget-object v0, LX/6YD;->SUCCESS:LX/6YD;

    goto :goto_0

    .line 1109415
    :pswitch_1
    sget-object v0, LX/6YD;->MAYBE_SUCCESS:LX/6YD;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1109416
    :catch_0
    sget-object v0, LX/6YD;->ERROR:LX/6YD;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/6YD;
    .locals 1

    .prologue
    .line 1109409
    const-class v0, LX/6YD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6YD;

    return-object v0
.end method

.method public static values()[LX/6YD;
    .locals 1

    .prologue
    .line 1109408
    sget-object v0, LX/6YD;->$VALUES:[LX/6YD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6YD;

    return-object v0
.end method
