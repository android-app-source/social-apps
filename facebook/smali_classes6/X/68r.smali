.class public LX/68r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "LX/68r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:[Ljava/lang/Object;

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1056606
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, LX/68r;->a:Ljava/util/Vector;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1056640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1056641
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LX/68r;->b:[Ljava/lang/Object;

    .line 1056642
    sget-object v0, LX/68r;->a:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1056643
    return-void
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 1056635
    sget-object v2, LX/68r;->a:Ljava/util/Vector;

    monitor-enter v2

    .line 1056636
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/68r;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1056637
    sget-object v0, LX/68r;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68r;

    invoke-virtual {v0}, LX/68r;->d()V

    .line 1056638
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1056639
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 1056644
    new-instance v0, Lcom/facebook/android/maps/internal/SynchronizedPool$1;

    invoke-direct {v0}, Lcom/facebook/android/maps/internal/SynchronizedPool$1;-><init>()V

    invoke-static {v0}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1056645
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 1056623
    monitor-enter p0

    :try_start_0
    const/4 v1, 0x0

    .line 1056624
    move v0, v1

    :goto_0
    iget v2, p0, LX/68r;->c:I

    if-ge v0, v2, :cond_0

    .line 1056625
    iget-object v2, p0, LX/68r;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_2

    .line 1056626
    const/4 v1, 0x1

    .line 1056627
    :cond_0
    move v0, v1

    .line 1056628
    if-nez v0, :cond_1

    iget v0, p0, LX/68r;->c:I

    iget-object v1, p0, LX/68r;->b:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1056629
    iget-object v0, p0, LX/68r;->b:[Ljava/lang/Object;

    iget v1, p0, LX/68r;->c:I

    aput-object p1, v0, v1

    .line 1056630
    iget v0, p0, LX/68r;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/68r;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1056631
    const/4 v0, 0x1

    .line 1056632
    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1056633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1056634
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized c()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1056616
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/68r;->c:I

    if-lez v1, :cond_0

    .line 1056617
    iget v0, p0, LX/68r;->c:I

    add-int/lit8 v1, v0, -0x1

    .line 1056618
    iget-object v0, p0, LX/68r;->b:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 1056619
    iget-object v2, p0, LX/68r;->b:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 1056620
    iget v1, p0, LX/68r;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/68r;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1056621
    :cond_0
    monitor-exit p0

    return-object v0

    .line 1056622
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1056607
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/68r;->b:[Ljava/lang/Object;

    array-length v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1056608
    iget-object v0, p0, LX/68r;->b:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 1056609
    if-eqz v0, :cond_0

    instance-of v3, v0, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    .line 1056610
    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1056611
    :cond_0
    iget-object v0, p0, LX/68r;->b:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v0, v1

    .line 1056612
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1056613
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, LX/68r;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1056614
    monitor-exit p0

    return-void

    .line 1056615
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
