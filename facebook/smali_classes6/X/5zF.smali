.class public final LX/5zF;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1034049
    const-class v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;

    const v0, -0x7ff53085

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "TimelineInfoReviewQuestionSkipMutation"

    const-string v6, "749f438aece2b949866090042bdef20b"

    const-string v7, "profile_question_skip"

    const-string v8, "0"

    const-string v9, "10155156452291729"

    const/4 v10, 0x0

    .line 1034050
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1034051
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1034052
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1034053
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1034054
    sparse-switch v0, :sswitch_data_0

    .line 1034055
    :goto_0
    return-object p1

    .line 1034056
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1034057
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1034058
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1034059
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1034060
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e761353 -> :sswitch_4
        0x1b893 -> :sswitch_1
        0x5fb57ca -> :sswitch_0
        0x683094a -> :sswitch_2
        0x76508296 -> :sswitch_3
    .end sparse-switch
.end method
