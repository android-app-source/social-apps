.class public LX/6A4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;",
        "LX/5ke;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)V
    .locals 1

    .prologue
    .line 1058564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058565
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    iput-object v0, p0, LX/6A4;->a:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    .line 1058566
    invoke-virtual {p1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6A4;->b:Ljava/lang/String;

    .line 1058567
    invoke-virtual {p1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6A4;->c:Ljava/lang/String;

    .line 1058568
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058563
    iget-object v0, p0, LX/6A4;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 10

    .prologue
    .line 1058535
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    check-cast p2, LX/5ke;

    .line 1058536
    iget-object v0, p0, LX/6A4;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058537
    :goto_0
    return-void

    .line 1058538
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->w()I

    move-result v0

    if-nez v0, :cond_3

    .line 1058539
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1058540
    :goto_1
    move-object v0, v0

    .line 1058541
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1058542
    iget-object v1, p0, LX/6A4;->a:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    invoke-static {v1}, LX/20j;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)I

    move-result v1

    .line 1058543
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    .line 1058544
    invoke-static {v2}, LX/5k9;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v3

    invoke-static {v3, v0, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;II)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v3

    .line 1058545
    const/4 v6, 0x0

    .line 1058546
    if-nez v3, :cond_4

    .line 1058547
    :cond_2
    :goto_2
    move-object v3, v6

    .line 1058548
    invoke-static {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v3

    move-object v0, v3

    .line 1058549
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "top_reactions"

    invoke-virtual {v1, v2, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058550
    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->w()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 1058551
    :cond_4
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1058552
    invoke-static {v4, v3}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)I

    move-result v5

    .line 1058553
    if-eqz v5, :cond_2

    .line 1058554
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1058555
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1058556
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1058557
    new-instance v4, LX/15i;

    const/4 v8, 0x1

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1058558
    instance-of v5, v3, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v5, :cond_5

    .line 1058559
    const-string v5, "PhotosMetadataConversionHelper.getReactionsCountFieldsTopReactions"

    invoke-virtual {v4, v5, v3}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1058560
    :cond_5
    new-instance v6, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {v6, v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;-><init>(LX/15i;)V

    goto :goto_2
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058562
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058561
    const-string v0, "ReactionsSimpleMediaMutatingVisitor"

    return-object v0
.end method
