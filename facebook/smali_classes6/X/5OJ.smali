.class public LX/5OJ;
.super LX/5OG;
.source ""

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field public c:Landroid/view/MenuItem;

.field public d:LX/5OG;

.field private e:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 908397
    invoke-direct {p0, p1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 908398
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908407
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    .line 908408
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    iput-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    .line 908409
    :cond_0
    if-eqz p1, :cond_1

    .line 908410
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 908411
    :cond_1
    if-eqz p2, :cond_2

    .line 908412
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 908413
    :cond_2
    const v0, -0x530f0fe8

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908414
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 0

    .prologue
    .line 908404
    invoke-virtual {p0}, LX/5OJ;->clearHeader()V

    .line 908405
    invoke-super {p0}, LX/5OG;->clear()V

    .line 908406
    return-void
.end method

.method public final clearHeader()V
    .locals 1

    .prologue
    .line 908402
    const/4 v0, 0x0

    iput-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    .line 908403
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 908400
    invoke-super {p0}, LX/5OG;->getCount()I

    move-result v0

    .line 908401
    iget-object v1, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908399
    iget-object v0, p0, LX/5OJ;->c:Landroid/view/MenuItem;

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908375
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 908376
    if-nez p1, :cond_0

    .line 908377
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    .line 908378
    :goto_0
    return-object v0

    .line 908379
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 908380
    :cond_1
    invoke-super {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 908396
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 908415
    iget-object v0, p0, LX/5OG;->a:LX/5OE;

    if-nez v0, :cond_0

    .line 908416
    :goto_0
    return-void

    .line 908417
    :cond_0
    iget-object v0, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    iget-object v0, p0, LX/5OG;->b:LX/5OF;

    if-eqz v0, :cond_1

    .line 908418
    iget-object v0, p0, LX/5OG;->b:LX/5OF;

    .line 908419
    iget-object v1, p0, LX/5OJ;->d:LX/5OG;

    move-object v1, v1

    .line 908420
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/5OF;->a(LX/5OG;Z)V

    goto :goto_0

    .line 908421
    :cond_1
    invoke-super/range {p0 .. p5}, LX/5OG;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908393
    iget-object v0, p0, LX/5OG;->c:Landroid/content/Context;

    move-object v0, v0

    .line 908394
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/5OJ;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 908395
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908391
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/5OJ;->a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 908392
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908388
    iget-object v0, p0, LX/5OG;->c:Landroid/content/Context;

    move-object v0, v0

    .line 908389
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/5OJ;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 908390
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908386
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5OJ;->a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 908387
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908385
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908384
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908383
    return-object p0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 908381
    invoke-super {p0}, LX/5OG;->size()I

    move-result v0

    .line 908382
    iget-object v1, p0, LX/5OJ;->e:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method
