.class public final LX/5El;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 883764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 883765
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 883766
    :goto_0
    return v1

    .line 883767
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_7

    .line 883768
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 883769
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 883770
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 883771
    const-string v12, "height"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 883772
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v10, v4

    move v4, v2

    goto :goto_1

    .line 883773
    :cond_1
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 883774
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 883775
    :cond_2
    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 883776
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 883777
    :cond_3
    const-string v12, "is_playable"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 883778
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 883779
    :cond_4
    const-string v12, "playable_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 883780
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 883781
    :cond_5
    const-string v12, "width"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 883782
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 883783
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 883784
    :cond_7
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 883785
    if-eqz v4, :cond_8

    .line 883786
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 883787
    :cond_8
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 883788
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 883789
    if-eqz v3, :cond_9

    .line 883790
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 883791
    :cond_9
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 883792
    if-eqz v0, :cond_a

    .line 883793
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 883794
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 883795
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883796
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883797
    if-eqz v0, :cond_0

    .line 883798
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883799
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883800
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883801
    if-eqz v0, :cond_1

    .line 883802
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883803
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883804
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883805
    if-eqz v0, :cond_2

    .line 883806
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883807
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 883808
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 883809
    if-eqz v0, :cond_3

    .line 883810
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883811
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 883812
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883813
    if-eqz v0, :cond_4

    .line 883814
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883815
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883816
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 883817
    if-eqz v0, :cond_5

    .line 883818
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883819
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883820
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883821
    return-void
.end method
