.class public LX/61t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vf;


# instance fields
.field public final a:LX/61r;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field private e:Z


# direct methods
.method public constructor <init>(ZLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "LX/1Cw;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 1040884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040885
    iput v0, p0, LX/61t;->c:I

    .line 1040886
    iput v0, p0, LX/61t;->d:I

    .line 1040887
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/61t;->e:Z

    .line 1040888
    iput-object p2, p0, LX/61t;->b:LX/0Px;

    .line 1040889
    new-instance v0, LX/61r;

    invoke-direct {v0, p1, p2}, LX/61r;-><init>(ZLX/0Px;)V

    iput-object v0, p0, LX/61t;->a:LX/61r;

    .line 1040890
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1040883
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->e:I

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1040800
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->f:I

    if-lt p1, v0, :cond_0

    .line 1040801
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/61t;->a(LX/61s;)V

    .line 1040802
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->f:I

    if-lt p1, v0, :cond_2

    .line 1040803
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from totalCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/61t;->a:LX/61r;

    iget v2, v2, LX/61r;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/61t;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040804
    :cond_2
    iget v0, p0, LX/61t;->c:I

    if-ltz v0, :cond_3

    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1040805
    :cond_3
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->b()V

    .line 1040806
    :cond_4
    :goto_0
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1040807
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->d:I

    sub-int v0, p1, v0

    .line 1040808
    if-gez v0, :cond_5

    .line 1040809
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->e()Z

    goto :goto_0

    .line 1040810
    :cond_5
    iget-object v1, p0, LX/61t;->a:LX/61r;

    iget v1, v1, LX/61r;->b:I

    if-lt v0, v1, :cond_6

    .line 1040811
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->d()Z

    goto :goto_0

    .line 1040812
    :cond_6
    iput p1, p0, LX/61t;->c:I

    .line 1040813
    iput v0, p0, LX/61t;->d:I

    .line 1040814
    return-void

    .line 1040815
    :cond_7
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->g()V

    .line 1040816
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Could not find valid position in adapters."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " Ensure adapters are only accessed from the UI thread.\nPosition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/61t;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1040817
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(LX/1Cw;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1040875
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->b()V

    .line 1040876
    iput v1, p0, LX/61t;->d:I

    .line 1040877
    iput v1, p0, LX/61t;->c:I

    .line 1040878
    :goto_0
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->a()LX/1Cw;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 1040879
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1040880
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adapter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040881
    :cond_0
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->d:I

    iput v0, p0, LX/61t;->c:I

    goto :goto_0

    .line 1040882
    :cond_1
    return-void
.end method

.method public final a(LX/61s;)V
    .locals 8
    .param p1    # LX/61s;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 1040847
    iput v1, p0, LX/61t;->c:I

    .line 1040848
    iput v1, p0, LX/61t;->d:I

    .line 1040849
    iget-object v0, p0, LX/61t;->a:LX/61r;

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1040850
    iput v3, v0, LX/61r;->a:I

    .line 1040851
    iput v3, v0, LX/61r;->d:I

    .line 1040852
    iput v3, v0, LX/61r;->e:I

    .line 1040853
    iput-boolean v5, v0, LX/61r;->h:Z

    .line 1040854
    iput-boolean v5, v0, LX/61r;->i:Z

    move v2, v3

    .line 1040855
    :goto_0
    invoke-static {v0}, LX/61r;->h(LX/61r;)I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 1040856
    iput v2, v0, LX/61r;->a:I

    .line 1040857
    invoke-virtual {v0}, LX/61r;->a()LX/1Cw;

    move-result-object v6

    .line 1040858
    if-eqz p1, :cond_0

    .line 1040859
    invoke-interface {p1, v6, v2}, LX/61s;->a(LX/1Cw;I)V

    .line 1040860
    :cond_0
    invoke-interface {v6}, LX/1Cw;->getCount()I

    move-result v4

    iput v4, v0, LX/61r;->b:I

    .line 1040861
    iget-object v4, v0, LX/61r;->k:[I

    invoke-interface {v6}, LX/1Cw;->getCount()I

    move-result v7

    aput v7, v4, v2

    .line 1040862
    invoke-static {v0, v6}, LX/61r;->a(LX/61r;LX/1Cw;)I

    move-result v4

    iput v4, v0, LX/61r;->c:I

    .line 1040863
    iget-boolean v4, v0, LX/61r;->h:Z

    if-eqz v4, :cond_1

    invoke-interface {v6}, LX/1Cw;->areAllItemsEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    iput-boolean v4, v0, LX/61r;->h:Z

    .line 1040864
    iget-boolean v4, v0, LX/61r;->i:Z

    if-eqz v4, :cond_2

    invoke-interface {v6}, LX/1Cw;->hasStableIds()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_2
    iput-boolean v4, v0, LX/61r;->i:Z

    .line 1040865
    iget v4, v0, LX/61r;->d:I

    iget v6, v0, LX/61r;->b:I

    add-int/2addr v4, v6

    iput v4, v0, LX/61r;->d:I

    .line 1040866
    iget v4, v0, LX/61r;->e:I

    iget v6, v0, LX/61r;->c:I

    add-int/2addr v4, v6

    iput v4, v0, LX/61r;->e:I

    .line 1040867
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v4, v3

    .line 1040868
    goto :goto_1

    :cond_2
    move v4, v3

    .line 1040869
    goto :goto_2

    .line 1040870
    :cond_3
    iget v2, v0, LX/61r;->d:I

    iput v2, v0, LX/61r;->f:I

    .line 1040871
    iget v2, v0, LX/61r;->e:I

    iput v2, v0, LX/61r;->g:I

    .line 1040872
    iput v1, p0, LX/61t;->c:I

    .line 1040873
    iput v1, p0, LX/61t;->d:I

    .line 1040874
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1040831
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->g:I

    if-lt p1, v0, :cond_1

    .line 1040832
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from totalCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/61t;->a:LX/61r;

    iget v2, v2, LX/61r;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040833
    :cond_1
    iget v0, p0, LX/61t;->c:I

    if-ltz v0, :cond_2

    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1040834
    :cond_2
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->b()V

    .line 1040835
    :cond_3
    :goto_0
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1040836
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->e:I

    sub-int v0, p1, v0

    .line 1040837
    if-gez v0, :cond_4

    .line 1040838
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->e()Z

    goto :goto_0

    .line 1040839
    :cond_4
    iget-object v1, p0, LX/61t;->a:LX/61r;

    iget v1, v1, LX/61r;->c:I

    if-lt v0, v1, :cond_5

    .line 1040840
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->d()Z

    goto :goto_0

    .line 1040841
    :cond_5
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->b:I

    if-lez v0, :cond_7

    .line 1040842
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->d:I

    iput v0, p0, LX/61t;->c:I

    .line 1040843
    const/4 v0, 0x0

    iput v0, p0, LX/61t;->d:I

    .line 1040844
    :cond_6
    :goto_1
    return-void

    .line 1040845
    :cond_7
    iput v2, p0, LX/61t;->c:I

    .line 1040846
    iput v2, p0, LX/61t;->d:I

    goto :goto_1
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1040830
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->e:I

    sub-int v0, p1, v0

    return v0
.end method

.method public final d()LX/1Cw;
    .locals 1

    .prologue
    .line 1040829
    iget-object v0, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v0}, LX/61r;->a()LX/1Cw;

    move-result-object v0

    return-object v0
.end method

.method public final dispose()V
    .locals 4

    .prologue
    .line 1040821
    iget-object v0, p0, LX/61t;->a:LX/61r;

    .line 1040822
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-static {v0}, LX/61r;->h(LX/61r;)I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 1040823
    iget-object v1, v0, LX/61r;->j:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cw;

    .line 1040824
    instance-of v3, v1, LX/0Vf;

    if-eqz v3, :cond_0

    .line 1040825
    check-cast v1, LX/0Vf;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 1040826
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1040827
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/61t;->e:Z

    .line 1040828
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1040820
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->f:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1040819
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget v0, v0, LX/61r;->g:I

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1040818
    iget-object v0, p0, LX/61t;->a:LX/61r;

    iget-boolean v0, v0, LX/61r;->i:Z

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1040799
    iget-boolean v0, p0, LX/61t;->e:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1040787
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1040788
    const-string v0, " CurrentViewTypeOffset: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/61t;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CurrentPosition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1040789
    iget v1, p0, LX/61t;->c:I

    move v1, v1

    .line 1040790
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LocalPosition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1040791
    iget v1, p0, LX/61t;->d:I

    move v1, v1

    .line 1040792
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/61t;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ViewTypeCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/61t;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AreAllItemsEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1040793
    iget-object v1, p0, LX/61t;->a:LX/61r;

    iget-boolean v1, v1, LX/61r;->h:Z

    move v1, v1

    .line 1040794
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " HasStableIds: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/61t;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n Cursor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/61t;->a:LX/61r;

    invoke-virtual {v1}, LX/61r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nAdapters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1040795
    iget-object v0, p0, LX/61t;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/61t;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Adapter;

    .line 1040796
    const-string v4, "\n    Class: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " HasStableIds: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ViewTypeCount: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Landroid/widget/Adapter;->getViewTypeCount()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1040797
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1040798
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
