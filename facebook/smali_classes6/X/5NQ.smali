.class public final LX/5NQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1pk;


# direct methods
.method public constructor <init>(LX/1pk;)V
    .locals 0

    .prologue
    .line 906300
    iput-object p1, p0, LX/5NQ;->a:LX/1pk;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 906298
    iget-object v0, p0, LX/5NQ;->a:LX/1pk;

    iget-object v0, v0, LX/1pk;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "dialtone"

    const-string v2, "photo cap result fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 906299
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 906273
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    .line 906274
    if-eqz p1, :cond_0

    .line 906275
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 906276
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 906277
    iget-object v0, p0, LX/5NQ;->a:LX/1pk;

    iget-object v0, v0, LX/1pk;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "dialtone"

    const-string v2, "photo cap result is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 906278
    :goto_1
    return-void

    .line 906279
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 906280
    check-cast v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;

    invoke-virtual {v0}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 906281
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 906282
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 906283
    check-cast v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;

    invoke-virtual {v0}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/5NQ;->a:LX/1pk;

    const/4 v10, 0x0

    .line 906284
    iget-object v3, v2, LX/1pk;->d:LX/1pl;

    .line 906285
    iget-wide v11, v3, LX/1pl;->h:J

    move-wide v3, v11

    .line 906286
    invoke-virtual {v1, v0, v10}, LX/15i;->k(II)J

    move-result-wide v5

    .line 906287
    iget-object v7, v2, LX/1pk;->d:LX/1pl;

    invoke-virtual {v7, v1, v0}, LX/1pl;->b(LX/15i;I)V

    .line 906288
    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-eqz v7, :cond_4

    cmp-long v3, v5, v3

    if-lez v3, :cond_4

    .line 906289
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->j(II)I

    move-result v4

    .line 906290
    iget-object v3, v2, LX/1pk;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0kL;

    new-instance v5, LX/27k;

    iget-object v6, v2, LX/1pk;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f002b

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 906291
    const-string v3, "dialtone_photocapping_cap_reminder_toast_impression"

    .line 906292
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "dialtone"

    .line 906293
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 906294
    move-object v5, v4

    .line 906295
    const-string v6, "carrier_id"

    iget-object v4, v2, LX/1pk;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v7}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v4, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906296
    iget-object v4, v2, LX/1pk;->j:LX/0Zb;

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 906297
    :cond_4
    goto/16 :goto_1
.end method
