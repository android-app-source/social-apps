.class public final LX/5Jx;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Jx;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Jv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Jy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 897943
    const/4 v0, 0x0

    sput-object v0, LX/5Jx;->a:LX/5Jx;

    .line 897944
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Jx;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 897940
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 897941
    new-instance v0, LX/5Jy;

    invoke-direct {v0}, LX/5Jy;-><init>()V

    iput-object v0, p0, LX/5Jx;->c:LX/5Jy;

    .line 897942
    return-void
.end method

.method public static declared-synchronized q()LX/5Jx;
    .locals 2

    .prologue
    .line 897936
    const-class v1, LX/5Jx;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Jx;->a:LX/5Jx;

    if-nez v0, :cond_0

    .line 897937
    new-instance v0, LX/5Jx;

    invoke-direct {v0}, LX/5Jx;-><init>()V

    sput-object v0, LX/5Jx;->a:LX/5Jx;

    .line 897938
    :cond_0
    sget-object v0, LX/5Jx;->a:LX/5Jx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 897939
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897934
    invoke-static {}, LX/1dS;->b()V

    .line 897935
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/16 v0, 0x1e

    const v1, 0x2ca5f0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 897930
    check-cast p6, LX/5Jw;

    .line 897931
    iget-wide v2, p6, LX/5Jw;->a:D

    .line 897932
    double-to-float v5, v2

    invoke-static {p3, p4, v5, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 897933
    const/16 v1, 0x1f

    const v2, -0x5e592042

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897928
    new-instance v0, Lcom/facebook/components/ComponentView;

    invoke-direct {v0, p1}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    move-object v0, v0

    .line 897929
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 897927
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 897918
    check-cast p3, LX/5Jw;

    .line 897919
    check-cast p2, Lcom/facebook/components/ComponentView;

    iget-object v0, p3, LX/5Jw;->b:LX/1X1;

    .line 897920
    invoke-static {p1, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object p0

    invoke-virtual {p0}, LX/1me;->b()LX/1dV;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897921
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 897926
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 897923
    check-cast p2, Lcom/facebook/components/ComponentView;

    .line 897924
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897925
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 897922
    const/16 v0, 0xf

    return v0
.end method
