.class public final LX/6Is;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;


# instance fields
.field public final synthetic a:LX/6Iu;

.field private b:LX/6JQ;


# direct methods
.method public constructor <init>(LX/6Iu;)V
    .locals 0

    .prologue
    .line 1074642
    iput-object p1, p0, LX/6Is;->a:LX/6Iu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onImageAvailable(Landroid/media/ImageReader;)V
    .locals 7

    .prologue
    .line 1074631
    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireLatestImage()Landroid/media/Image;

    move-result-object v1

    .line 1074632
    if-eqz v1, :cond_2

    .line 1074633
    iget-object v0, p0, LX/6Is;->b:LX/6JQ;

    if-nez v0, :cond_0

    .line 1074634
    new-instance v0, LX/6JQ;

    invoke-virtual {v1}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v2

    array-length v2, v2

    invoke-direct {v0, v2}, LX/6JQ;-><init>(I)V

    iput-object v0, p0, LX/6Is;->b:LX/6JQ;

    .line 1074635
    :cond_0
    invoke-virtual {v1}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v2

    .line 1074636
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 1074637
    iget-object v3, p0, LX/6Is;->b:LX/6JQ;

    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    aget-object v5, v2, v0

    invoke-virtual {v5}, Landroid/media/Image$Plane;->getPixelStride()I

    move-result v5

    aget-object v6, v2, v0

    invoke-virtual {v6}, Landroid/media/Image$Plane;->getRowStride()I

    move-result v6

    invoke-virtual {v3, v0, v4, v5, v6}, LX/6JQ;->a(ILjava/nio/ByteBuffer;II)V

    .line 1074638
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1074639
    :cond_1
    iget-object v0, p0, LX/6Is;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->u:LX/6Jj;

    iget-object v2, p0, LX/6Is;->b:LX/6JQ;

    invoke-virtual {v0, v2}, LX/6Jj;->a(LX/6JQ;)V

    .line 1074640
    invoke-virtual {v1}, Landroid/media/Image;->close()V

    .line 1074641
    :cond_2
    return-void
.end method
