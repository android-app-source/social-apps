.class public final LX/6Wy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1106627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLActor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1106628
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-eq v0, v1, :cond_1

    .line 1106629
    :cond_0
    const/4 v0, 0x0

    .line 1106630
    :goto_0
    return-object v0

    .line 1106631
    :cond_1
    new-instance v0, LX/4XY;

    invoke-direct {v0}, LX/4XY;-><init>()V

    .line 1106632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    .line 1106633
    iput-object v1, v0, LX/4XY;->j:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 1106634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v1

    .line 1106635
    iput-object v1, v0, LX/4XY;->k:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 1106636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->p()Z

    move-result v1

    .line 1106637
    iput-boolean v1, v0, LX/4XY;->u:Z

    .line 1106638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->s()Z

    move-result v1

    .line 1106639
    iput-boolean v1, v0, LX/4XY;->z:Z

    .line 1106640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->u()Z

    move-result v1

    .line 1106641
    iput-boolean v1, v0, LX/4XY;->A:Z

    .line 1106642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v1

    .line 1106643
    iput-object v1, v0, LX/4XY;->G:LX/0Px;

    .line 1106644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    .line 1106645
    iput-object v1, v0, LX/4XY;->O:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 1106646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->A()LX/0Px;

    move-result-object v1

    .line 1106647
    iput-object v1, v0, LX/4XY;->R:LX/0Px;

    .line 1106648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 1106649
    iput-object v1, v0, LX/4XY;->ag:Ljava/lang/String;

    .line 1106650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->I()Z

    move-result v1

    .line 1106651
    iput-boolean v1, v0, LX/4XY;->am:Z

    .line 1106652
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->M()Z

    move-result v1

    .line 1106653
    iput-boolean v1, v0, LX/4XY;->aq:Z

    .line 1106654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->N()Z

    move-result v1

    .line 1106655
    iput-boolean v1, v0, LX/4XY;->au:Z

    .line 1106656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v1

    .line 1106657
    iput-boolean v1, v0, LX/4XY;->aE:Z

    .line 1106658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aW()Z

    move-result v1

    .line 1106659
    iput-boolean v1, v0, LX/4XY;->aF:Z

    .line 1106660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v1

    .line 1106661
    iput-object v1, v0, LX/4XY;->aI:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 1106662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v1

    .line 1106663
    iput-object v1, v0, LX/4XY;->aJ:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 1106664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 1106665
    iput-object v1, v0, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1106666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Z()Ljava/lang/String;

    move-result-object v1

    .line 1106667
    iput-object v1, v0, LX/4XY;->aR:Ljava/lang/String;

    .line 1106668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 1106669
    iput-object v1, v0, LX/4XY;->aT:Ljava/lang/String;

    .line 1106670
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ac()LX/0Px;

    move-result-object v1

    .line 1106671
    iput-object v1, v0, LX/4XY;->aU:LX/0Px;

    .line 1106672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->be()Z

    move-result v1

    .line 1106673
    iput-boolean v1, v0, LX/4XY;->aX:Z

    .line 1106674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    .line 1106675
    iput-object v1, v0, LX/4XY;->be:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 1106676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106677
    iput-object v1, v0, LX/4XY;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106679
    iput-object v1, v0, LX/4XY;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aV()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    .line 1106681
    iput-object v1, v0, LX/4XY;->bt:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1106682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1106683
    iput-object v1, v0, LX/4XY;->bw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 1106684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106685
    iput-object v1, v0, LX/4XY;->bF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    .line 1106687
    iput-object v1, v0, LX/4XY;->bM:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1106688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106689
    iput-object v1, v0, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->al()Z

    move-result v1

    .line 1106691
    iput-boolean v1, v0, LX/4XY;->bR:Z

    .line 1106692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v1

    .line 1106693
    iput-object v1, v0, LX/4XY;->bS:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 1106694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->an()Ljava/lang/String;

    move-result-object v1

    .line 1106695
    iput-object v1, v0, LX/4XY;->ca:Ljava/lang/String;

    .line 1106696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ao()Ljava/lang/String;

    move-result-object v1

    .line 1106697
    iput-object v1, v0, LX/4XY;->cb:Ljava/lang/String;

    .line 1106698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    .line 1106699
    iput-object v1, v0, LX/4XY;->cf:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1106700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v1

    .line 1106701
    iput-object v1, v0, LX/4XY;->cr:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 1106702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106703
    iput-object v1, v0, LX/4XY;->cv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106705
    iput-object v1, v0, LX/4XY;->cw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106707
    iput-object v1, v0, LX/4XY;->cx:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 1106709
    iput-object v1, v0, LX/4XY;->cz:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1106710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v1

    .line 1106711
    iput v1, v0, LX/4XY;->cP:I

    .line 1106712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v1

    .line 1106713
    iput-object v1, v0, LX/4XY;->cR:Ljava/lang/String;

    .line 1106714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aA()Ljava/lang/String;

    move-result-object v1

    .line 1106715
    iput-object v1, v0, LX/4XY;->cS:Ljava/lang/String;

    .line 1106716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v1

    .line 1106717
    iput-boolean v1, v0, LX/4XY;->cU:Z

    .line 1106718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v1

    .line 1106719
    iput-boolean v1, v0, LX/4XY;->cV:Z

    .line 1106720
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1106721
    iput-object v1, v0, LX/4XY;->cX:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1106722
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v1

    .line 1106723
    iput-boolean v1, v0, LX/4XY;->cZ:Z

    .line 1106724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v1

    .line 1106725
    iput-boolean v1, v0, LX/4XY;->da:Z

    .line 1106726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106727
    iput-object v1, v0, LX/4XY;->de:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106729
    iput-object v1, v0, LX/4XY;->df:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aI()Z

    move-result v1

    .line 1106731
    iput-boolean v1, v0, LX/4XY;->dg:Z

    .line 1106732
    invoke-virtual {v0}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    goto/16 :goto_0
.end method
