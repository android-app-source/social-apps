.class public LX/62V;
.super LX/1P0;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1041700
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/62V;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1041698
    invoke-direct {p0, p1}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 1041699
    return-void
.end method

.method private a(LX/1Od;III)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1041682
    invoke-virtual {p1, p2}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v2

    .line 1041683
    if-eqz v2, :cond_3

    .line 1041684
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 1041685
    iget v3, p0, LX/1P1;->j:I

    move v3, v3

    .line 1041686
    if-ne v3, v5, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v3

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p3, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v3

    .line 1041687
    iget v4, p0, LX/1P1;->j:I

    move v4, v4

    .line 1041688
    if-ne v4, v5, :cond_0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p4

    :cond_0
    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v1

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v4

    add-int/2addr v1, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v4

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p4, v1, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1041689
    invoke-virtual {v2, v3, v1}, Landroid/view/View;->measure(II)V

    .line 1041690
    sget-object v1, LX/62V;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v1}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1041691
    iget v1, p0, LX/1P1;->j:I

    move v1, v1

    .line 1041692
    if-ne v1, v5, :cond_2

    .line 1041693
    invoke-static {v2}, LX/1OR;->h(Landroid/view/View;)I

    move-result v1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    .line 1041694
    :goto_1
    invoke-virtual {p1, v2}, LX/1Od;->a(Landroid/view/View;)V

    .line 1041695
    :goto_2
    return v0

    .line 1041696
    :cond_1
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p3

    goto :goto_0

    .line 1041697
    :cond_2
    invoke-static {v2}, LX/1OR;->g(Landroid/view/View;)I

    move-result v1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1041663
    iget v0, p0, LX/1P1;->j:I

    move v0, v0

    .line 1041664
    if-ne v0, v4, :cond_0

    move v0, p4

    .line 1041665
    :goto_0
    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_1

    .line 1041666
    invoke-super {p0, p1, p2, p3, p4}, LX/1P0;->a(LX/1Od;LX/1Ok;II)V

    .line 1041667
    :goto_1
    return-void

    :cond_0
    move v0, p3

    .line 1041668
    goto :goto_0

    .line 1041669
    :cond_1
    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_3

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    :goto_2
    move v2, v1

    .line 1041670
    :goto_3
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1041671
    invoke-direct {p0, p1, v1, p3, p4}, LX/62V;->a(LX/1Od;III)I

    move-result v3

    .line 1041672
    add-int/2addr v2, v3

    .line 1041673
    if-le v2, v0, :cond_4

    move v2, v0

    .line 1041674
    :cond_2
    iget v1, p0, LX/1P1;->j:I

    move v1, v1

    .line 1041675
    if-ne v1, v4, :cond_5

    .line 1041676
    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v1

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 1041677
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, LX/1OR;->e(II)V

    goto :goto_1

    .line 1041678
    :cond_3
    const v0, 0x7fffffff

    goto :goto_2

    .line 1041679
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1041680
    :cond_5
    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v1

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 1041681
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/1OR;->e(II)V

    goto :goto_1
.end method
