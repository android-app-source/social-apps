.class public final LX/59z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 855377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 855378
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 855379
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 855380
    invoke-static {p0, p1}, LX/59z;->b(LX/15w;LX/186;)I

    move-result v1

    .line 855381
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 855382
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 855383
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 855384
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 855385
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/59z;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 855386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 855387
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 855388
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 855389
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 855390
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 855391
    :goto_0
    return v1

    .line 855392
    :cond_0
    const-string v10, "width"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 855393
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 855394
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_8

    .line 855395
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 855396
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 855397
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 855398
    const-string v10, "cdn_uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 855399
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 855400
    :cond_2
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 855401
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 855402
    :cond_3
    const-string v10, "projection_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 855403
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 855404
    :cond_4
    const-string v10, "spherical_metadata"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 855405
    invoke-static {p0, p1}, LX/59x;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 855406
    :cond_5
    const-string v10, "tiles"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 855407
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 855408
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_6

    .line 855409
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_6

    .line 855410
    invoke-static {p0, p1}, LX/59y;->b(LX/15w;LX/186;)I

    move-result v9

    .line 855411
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 855412
    :cond_6
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 855413
    goto/16 :goto_1

    .line 855414
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 855415
    :cond_8
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 855416
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 855417
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 855418
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 855419
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 855420
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 855421
    if-eqz v0, :cond_9

    .line 855422
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 855423
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 855424
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 855425
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 855426
    if-eqz v0, :cond_0

    .line 855427
    const-string v1, "cdn_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855428
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 855429
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 855430
    if-eqz v0, :cond_1

    .line 855431
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855432
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 855433
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 855434
    if-eqz v0, :cond_2

    .line 855435
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855436
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 855437
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 855438
    if-eqz v0, :cond_d

    .line 855439
    const-string v1, "spherical_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855440
    const/4 v5, 0x0

    const-wide/16 v7, 0x0

    .line 855441
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 855442
    invoke-virtual {p0, v0, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855443
    if-eqz v3, :cond_3

    .line 855444
    const-string v4, "cropped_area_image_height_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855445
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855446
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855447
    if-eqz v3, :cond_4

    .line 855448
    const-string v4, "cropped_area_image_width_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855449
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855450
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p0, v0, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855451
    if-eqz v3, :cond_5

    .line 855452
    const-string v4, "cropped_area_left_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855453
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855454
    :cond_5
    const/4 v3, 0x3

    invoke-virtual {p0, v0, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855455
    if-eqz v3, :cond_6

    .line 855456
    const-string v4, "cropped_area_top_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855457
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855458
    :cond_6
    const/4 v3, 0x4

    invoke-virtual {p0, v0, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855459
    if-eqz v3, :cond_7

    .line 855460
    const-string v4, "full_pano_height_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855461
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855462
    :cond_7
    const/4 v3, 0x5

    invoke-virtual {p0, v0, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 855463
    if-eqz v3, :cond_8

    .line 855464
    const-string v4, "full_pano_width_pixels"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855465
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 855466
    :cond_8
    const/4 v3, 0x6

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 855467
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_9

    .line 855468
    const-string v5, "initial_view_heading_degrees"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855469
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 855470
    :cond_9
    const/4 v3, 0x7

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 855471
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_a

    .line 855472
    const-string v5, "initial_view_pitch_degrees"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855473
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 855474
    :cond_a
    const/16 v3, 0x8

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 855475
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_b

    .line 855476
    const-string v5, "initial_view_roll_degrees"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855477
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 855478
    :cond_b
    const/16 v3, 0x9

    invoke-virtual {p0, v0, v3, v7, v8}, LX/15i;->a(IID)D

    move-result-wide v3

    .line 855479
    cmpl-double v5, v3, v7

    if-eqz v5, :cond_c

    .line 855480
    const-string v5, "initial_view_vertical_fov_degrees"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855481
    invoke-virtual {p2, v3, v4}, LX/0nX;->a(D)V

    .line 855482
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 855483
    :cond_d
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 855484
    if-eqz v0, :cond_f

    .line 855485
    const-string v1, "tiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855486
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 855487
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_e

    .line 855488
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/59y;->a(LX/15i;ILX/0nX;)V

    .line 855489
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 855490
    :cond_e
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 855491
    :cond_f
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 855492
    if-eqz v0, :cond_10

    .line 855493
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 855494
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 855495
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 855496
    return-void
.end method
