.class public LX/5pi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/5pi;


# instance fields
.field public final b:LX/5ph;

.field public final c:Ljava/lang/String;

.field public final d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1008477
    new-instance v0, LX/5pi;

    sget-object v1, LX/5ph;->MAIN_UI:LX/5ph;

    const-string v2, "main_ui"

    invoke-direct {v0, v1, v2}, LX/5pi;-><init>(LX/5ph;Ljava/lang/String;)V

    sput-object v0, LX/5pi;->a:LX/5pi;

    return-void
.end method

.method private constructor <init>(LX/5ph;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1008468
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/5pi;-><init>(LX/5ph;Ljava/lang/String;J)V

    .line 1008469
    return-void
.end method

.method private constructor <init>(LX/5ph;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 1008470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008471
    iput-object p1, p0, LX/5pi;->b:LX/5ph;

    .line 1008472
    iput-object p2, p0, LX/5pi;->c:Ljava/lang/String;

    .line 1008473
    iput-wide p3, p0, LX/5pi;->d:J

    .line 1008474
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/5pi;
    .locals 2

    .prologue
    .line 1008475
    new-instance v0, LX/5pi;

    sget-object v1, LX/5ph;->NEW_BACKGROUND:LX/5ph;

    invoke-direct {v0, v1, p0}, LX/5pi;-><init>(LX/5ph;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;J)LX/5pi;
    .locals 3

    .prologue
    .line 1008476
    new-instance v0, LX/5pi;

    sget-object v1, LX/5ph;->NEW_BACKGROUND:LX/5ph;

    invoke-direct {v0, v1, p0, p1, p2}, LX/5pi;-><init>(LX/5ph;Ljava/lang/String;J)V

    return-object v0
.end method
