.class public LX/68s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/69C;

.field public b:LX/69C;

.field private c:LX/69C;

.field public d:I

.field public e:I

.field public f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1056804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1056805
    iput v0, p0, LX/68s;->d:I

    .line 1056806
    iput v0, p0, LX/68s;->e:I

    .line 1056807
    const/4 v0, 0x0

    iput v0, p0, LX/68s;->f:I

    .line 1056808
    invoke-static {p0}, LX/68s;->d(LX/68s;)V

    .line 1056809
    return-void
.end method

.method private static a(IIII)I
    .locals 2

    .prologue
    .line 1056800
    sub-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    .line 1056801
    shr-int v1, p0, v0

    and-int/lit8 v1, v1, 0x1

    .line 1056802
    shr-int v0, p1, v0

    and-int/lit8 v0, v0, 0x1

    .line 1056803
    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method

.method private a(LX/69C;I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1056785
    if-gez p2, :cond_1

    .line 1056786
    const-string v0, "<snip>"

    .line 1056787
    :cond_0
    :goto_0
    return-object v0

    .line 1056788
    :cond_1
    if-nez p1, :cond_2

    .line 1056789
    const-string v0, "\n{x}"

    goto :goto_0

    .line 1056790
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/69C;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1056791
    iget-object v3, p1, LX/69C;->i:[LX/69C;

    array-length v4, v3

    const/4 v0, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    .line 1056792
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v5, p2, -0x1

    invoke-direct {p0, v2, v5}, LX/68s;->a(LX/69C;I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\n"

    const-string v6, "\n-"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1056793
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_1
.end method

.method private b(LX/69C;)V
    .locals 1

    .prologue
    .line 1056779
    if-nez p1, :cond_0

    .line 1056780
    :goto_0
    return-void

    .line 1056781
    :cond_0
    invoke-direct {p0, p1}, LX/68s;->c(LX/69C;)V

    .line 1056782
    iget-object v0, p0, LX/68s;->a:LX/69C;

    iput-object p1, v0, LX/69C;->j:LX/69C;

    .line 1056783
    iget-object v0, p0, LX/68s;->a:LX/69C;

    iput-object v0, p1, LX/69C;->k:LX/69C;

    .line 1056784
    iput-object p1, p0, LX/68s;->a:LX/69C;

    goto :goto_0
.end method

.method private c(LX/69C;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1056768
    iget-object v0, p0, LX/68s;->b:LX/69C;

    if-ne p1, v0, :cond_0

    .line 1056769
    iget-object v0, p1, LX/69C;->j:LX/69C;

    iput-object v0, p0, LX/68s;->b:LX/69C;

    .line 1056770
    :cond_0
    iget-object v0, p0, LX/68s;->a:LX/69C;

    if-ne p1, v0, :cond_1

    .line 1056771
    iget-object v0, p1, LX/69C;->k:LX/69C;

    iput-object v0, p0, LX/68s;->a:LX/69C;

    .line 1056772
    :cond_1
    iget-object v0, p1, LX/69C;->j:LX/69C;

    if-eqz v0, :cond_2

    .line 1056773
    iget-object v0, p1, LX/69C;->j:LX/69C;

    iget-object v1, p1, LX/69C;->k:LX/69C;

    iput-object v1, v0, LX/69C;->k:LX/69C;

    .line 1056774
    :cond_2
    iget-object v0, p1, LX/69C;->k:LX/69C;

    if-eqz v0, :cond_3

    .line 1056775
    iget-object v0, p1, LX/69C;->k:LX/69C;

    iget-object v1, p1, LX/69C;->j:LX/69C;

    iput-object v1, v0, LX/69C;->j:LX/69C;

    .line 1056776
    :cond_3
    iput-object v2, p1, LX/69C;->j:LX/69C;

    .line 1056777
    iput-object v2, p1, LX/69C;->k:LX/69C;

    .line 1056778
    return-void
.end method

.method public static d(LX/68s;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1056794
    invoke-static {}, LX/69C;->a()LX/69C;

    move-result-object v0

    iput-object v0, p0, LX/68s;->c:LX/69C;

    .line 1056795
    iget-object v0, p0, LX/68s;->c:LX/69C;

    invoke-virtual {v0, v1, v1, v1}, LX/69C;->a(III)LX/69C;

    .line 1056796
    iget-object v0, p0, LX/68s;->c:LX/69C;

    iput-object v0, p0, LX/68s;->b:LX/69C;

    .line 1056797
    iget-object v0, p0, LX/68s;->c:LX/69C;

    iput-object v0, p0, LX/68s;->a:LX/69C;

    .line 1056798
    iput v1, p0, LX/68s;->g:I

    .line 1056799
    return-void
.end method


# virtual methods
.method public final a(I)LX/68s;
    .locals 0

    .prologue
    .line 1056766
    iput p1, p0, LX/68s;->e:I

    .line 1056767
    return-object p0
.end method

.method public final a(IIILX/68i;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 1056734
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1056735
    iput v0, p4, LX/68i;->h:I

    .line 1056736
    iput-object v3, p4, LX/68i;->a:LX/69C;

    .line 1056737
    iput-object v3, p4, LX/68i;->b:LX/69C;

    .line 1056738
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 1056739
    iget-object v2, p4, LX/68i;->c:[LX/69C;

    aput-object v3, v2, v0

    .line 1056740
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1056741
    :cond_0
    iput p1, p4, LX/68i;->e:I

    .line 1056742
    iput p2, p4, LX/68i;->f:I

    .line 1056743
    iput p3, p4, LX/68i;->g:I

    .line 1056744
    iget-object v0, p0, LX/68s;->c:LX/69C;

    .line 1056745
    :goto_1
    iget v2, v0, LX/69C;->e:I

    if-ge v2, p3, :cond_2

    .line 1056746
    iget-object v2, v0, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1056747
    if-eqz v2, :cond_1

    .line 1056748
    iput-object v0, p4, LX/68i;->b:LX/69C;

    .line 1056749
    :cond_1
    iget v2, v0, LX/69C;->e:I

    invoke-static {p1, p2, p3, v2}, LX/68s;->a(IIII)I

    move-result v2

    .line 1056750
    iget-object v3, v0, LX/69C;->i:[LX/69C;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 1056751
    iget-object v0, v0, LX/69C;->i:[LX/69C;

    aget-object v0, v0, v2

    goto :goto_1

    .line 1056752
    :cond_2
    iget v2, v0, LX/69C;->e:I

    if-ne v2, p3, :cond_3

    iget v2, v0, LX/69C;->f:I

    if-ne v2, p1, :cond_3

    iget v2, v0, LX/69C;->g:I

    if-ne v2, p2, :cond_3

    .line 1056753
    iget-object v2, v0, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1056754
    if-eqz v2, :cond_5

    .line 1056755
    iput-object v0, p4, LX/68i;->a:LX/69C;

    .line 1056756
    :cond_3
    iget-object v0, p4, LX/68i;->a:LX/69C;

    if-eqz v0, :cond_6

    .line 1056757
    iget-object v0, p4, LX/68i;->a:LX/69C;

    invoke-direct {p0, v0}, LX/68s;->b(LX/69C;)V

    .line 1056758
    :cond_4
    :goto_2
    return-void

    .line 1056759
    :cond_5
    iget v2, v0, LX/69C;->l:I

    iput v2, p4, LX/68i;->h:I

    .line 1056760
    iget-object v2, v0, LX/69C;->i:[LX/69C;

    iget-object v3, p4, LX/68i;->c:[LX/69C;

    invoke-static {v2, v1, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1056761
    :goto_3
    if-ge v1, v4, :cond_3

    .line 1056762
    iget-object v2, v0, LX/69C;->i:[LX/69C;

    aget-object v2, v2, v1

    invoke-direct {p0, v2}, LX/68s;->b(LX/69C;)V

    .line 1056763
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1056764
    :cond_6
    iget-object v0, p4, LX/68i;->b:LX/69C;

    if-eqz v0, :cond_4

    .line 1056765
    iget-object v0, p4, LX/68i;->b:LX/69C;

    invoke-direct {p0, v0}, LX/68s;->b(LX/69C;)V

    goto :goto_2
.end method

.method public final a(LX/69C;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1056686
    iget-object v0, p1, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1056687
    if-eqz v0, :cond_0

    .line 1056688
    iget v0, p0, LX/68s;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/68s;->g:I

    .line 1056689
    :cond_0
    iget-object v1, p0, LX/68s;->c:LX/69C;

    .line 1056690
    iget v0, p1, LX/69C;->f:I

    iget v2, p1, LX/69C;->g:I

    iget v3, p1, LX/69C;->e:I

    iget v4, v1, LX/69C;->e:I

    invoke-static {v0, v2, v3, v4}, LX/68s;->a(IIII)I

    move-result v0

    .line 1056691
    :goto_0
    iget-object v2, v1, LX/69C;->i:[LX/69C;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/69C;->i:[LX/69C;

    aget-object v2, v2, v0

    const/4 v3, 0x0

    .line 1056692
    iget v4, p1, LX/69C;->e:I

    iget v6, v2, LX/69C;->e:I

    if-gt v4, v6, :cond_9

    .line 1056693
    :cond_1
    :goto_1
    move v2, v3

    .line 1056694
    if-eqz v2, :cond_2

    .line 1056695
    iget-object v1, v1, LX/69C;->i:[LX/69C;

    aget-object v1, v1, v0

    .line 1056696
    iget v0, p1, LX/69C;->f:I

    iget v2, p1, LX/69C;->g:I

    iget v3, p1, LX/69C;->e:I

    iget v4, v1, LX/69C;->e:I

    invoke-static {v0, v2, v3, v4}, LX/68s;->a(IIII)I

    move-result v0

    goto :goto_0

    .line 1056697
    :cond_2
    iget-object v2, v1, LX/69C;->i:[LX/69C;

    aget-object v7, v2, v0

    .line 1056698
    if-nez v7, :cond_3

    .line 1056699
    iget-object v1, v1, LX/69C;->i:[LX/69C;

    aput-object p1, v1, v0

    .line 1056700
    :goto_2
    invoke-direct {p0, p1}, LX/68s;->b(LX/69C;)V

    .line 1056701
    invoke-virtual {p0}, LX/68s;->b()V

    .line 1056702
    return-void

    .line 1056703
    :cond_3
    iget v2, p1, LX/69C;->e:I

    iget v3, v7, LX/69C;->e:I

    if-ge v2, v3, :cond_4

    .line 1056704
    iget-object v1, v1, LX/69C;->i:[LX/69C;

    aput-object p1, v1, v0

    .line 1056705
    iget-object v0, p1, LX/69C;->i:[LX/69C;

    iget v1, v7, LX/69C;->f:I

    iget v2, v7, LX/69C;->g:I

    iget v3, v7, LX/69C;->e:I

    iget v4, p1, LX/69C;->e:I

    invoke-static {v1, v2, v3, v4}, LX/68s;->a(IIII)I

    move-result v1

    aput-object v7, v0, v1

    goto :goto_2

    .line 1056706
    :cond_4
    iget v2, v7, LX/69C;->e:I

    iget v3, p1, LX/69C;->e:I

    if-ne v2, v3, :cond_6

    iget v2, v7, LX/69C;->f:I

    iget v3, p1, LX/69C;->f:I

    if-ne v2, v3, :cond_6

    iget v2, v7, LX/69C;->g:I

    iget v3, p1, LX/69C;->g:I

    if-ne v2, v3, :cond_6

    .line 1056707
    invoke-direct {p0, v7}, LX/68s;->c(LX/69C;)V

    .line 1056708
    iget-object v2, v7, LX/69C;->i:[LX/69C;

    iget-object v3, p1, LX/69C;->i:[LX/69C;

    const/4 v4, 0x4

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1056709
    iget-object v1, v1, LX/69C;->i:[LX/69C;

    aput-object p1, v1, v0

    .line 1056710
    iget-object v0, v7, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1056711
    if-eqz v0, :cond_5

    .line 1056712
    iget v0, p0, LX/68s;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/68s;->g:I

    .line 1056713
    :cond_5
    invoke-virtual {v7}, LX/69C;->c()V

    goto :goto_2

    .line 1056714
    :cond_6
    iget v2, p1, LX/69C;->e:I

    iget v3, v7, LX/69C;->e:I

    sub-int/2addr v2, v3

    .line 1056715
    iget v3, p1, LX/69C;->f:I

    shr-int v6, v3, v2

    .line 1056716
    iget v3, p1, LX/69C;->g:I

    shr-int v5, v3, v2

    .line 1056717
    iget v4, v7, LX/69C;->f:I

    .line 1056718
    iget v3, v7, LX/69C;->g:I

    .line 1056719
    iget v2, v7, LX/69C;->e:I

    .line 1056720
    :goto_3
    if-ne v6, v4, :cond_7

    if-eq v5, v3, :cond_8

    .line 1056721
    :cond_7
    shr-int/lit8 v6, v6, 0x1

    .line 1056722
    shr-int/lit8 v5, v5, 0x1

    .line 1056723
    shr-int/lit8 v4, v4, 0x1

    .line 1056724
    shr-int/lit8 v3, v3, 0x1

    .line 1056725
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 1056726
    :cond_8
    invoke-static {}, LX/69C;->a()LX/69C;

    move-result-object v3

    .line 1056727
    invoke-virtual {v3, v6, v5, v2}, LX/69C;->a(III)LX/69C;

    .line 1056728
    iget-object v1, v1, LX/69C;->i:[LX/69C;

    aput-object v3, v1, v0

    .line 1056729
    iget-object v0, v3, LX/69C;->i:[LX/69C;

    iget v1, v7, LX/69C;->f:I

    iget v4, v7, LX/69C;->g:I

    iget v5, v7, LX/69C;->e:I

    invoke-static {v1, v4, v5, v2}, LX/68s;->a(IIII)I

    move-result v1

    aput-object v7, v0, v1

    .line 1056730
    iget-object v0, v3, LX/69C;->i:[LX/69C;

    iget v1, p1, LX/69C;->f:I

    iget v4, p1, LX/69C;->g:I

    iget v5, p1, LX/69C;->e:I

    invoke-static {v1, v4, v5, v2}, LX/68s;->a(IIII)I

    move-result v1

    aput-object p1, v0, v1

    .line 1056731
    invoke-direct {p0, v3}, LX/68s;->b(LX/69C;)V

    goto/16 :goto_2

    .line 1056732
    :cond_9
    iget v4, p1, LX/69C;->e:I

    iget v6, v2, LX/69C;->e:I

    sub-int/2addr v4, v6

    .line 1056733
    iget v6, p1, LX/69C;->f:I

    shr-int/2addr v6, v4

    iget v7, v2, LX/69C;->f:I

    if-ne v6, v7, :cond_1

    iget v6, p1, LX/69C;->g:I

    shr-int v4, v6, v4

    iget v6, v2, LX/69C;->g:I

    if-ne v4, v6, :cond_1

    const/4 v3, 0x1

    goto/16 :goto_1
.end method

.method public final b(I)LX/68s;
    .locals 0

    .prologue
    .line 1056684
    iput p1, p0, LX/68s;->f:I

    .line 1056685
    return-object p0
.end method

.method public final b()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1056646
    invoke-static {}, LX/31U;->a()J

    move-result-wide v10

    .line 1056647
    :try_start_0
    iget v0, p0, LX/68s;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v6, :cond_0

    .line 1056648
    sget-object v0, LX/31U;->f:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    .line 1056649
    :goto_0
    return-void

    .line 1056650
    :cond_0
    :try_start_1
    iget v0, p0, LX/68s;->e:I

    iget v1, p0, LX/68s;->f:I

    sub-int v1, v0, v1

    iget v0, p0, LX/68s;->d:I

    if-eq v0, v6, :cond_1

    iget v0, p0, LX/68s;->d:I

    shl-int/lit8 v0, v0, 0x1

    shl-int v0, v3, v0

    :goto_1
    add-int/2addr v1, v0

    .line 1056651
    iget v0, p0, LX/68s;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v0, v1, :cond_2

    .line 1056652
    sget-object v0, LX/31U;->f:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    goto :goto_0

    :cond_1
    move v0, v5

    .line 1056653
    goto :goto_1

    .line 1056654
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/68s;->b:LX/69C;

    :goto_2
    iget v2, p0, LX/68s;->g:I

    if-le v2, v1, :cond_4

    if-eqz v0, :cond_4

    .line 1056655
    iget-object v2, v0, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1056656
    if-eqz v2, :cond_3

    iget v2, v0, LX/69C;->e:I

    iget v4, p0, LX/68s;->d:I

    if-eq v2, v4, :cond_3

    iget v2, v0, LX/69C;->l:I

    if-nez v2, :cond_3

    .line 1056657
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/69C;->a(Landroid/graphics/Bitmap;)V

    .line 1056658
    iget v2, p0, LX/68s;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/68s;->g:I

    .line 1056659
    :cond_3
    iget-object v0, v0, LX/69C;->j:LX/69C;

    goto :goto_2

    .line 1056660
    :cond_4
    iget-object v0, p0, LX/68s;->b:LX/69C;

    move-object v8, v0

    move v4, v5

    .line 1056661
    :goto_3
    if-eqz v8, :cond_9

    move v7, v5

    .line 1056662
    :goto_4
    if-ge v7, v13, :cond_8

    .line 1056663
    iget-object v0, v8, LX/69C;->i:[LX/69C;

    aget-object v9, v0, v7

    .line 1056664
    if-eqz v9, :cond_a

    iget v0, v9, LX/69C;->l:I

    if-nez v0, :cond_a

    .line 1056665
    iget-object v0, v9, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1056666
    if-nez v0, :cond_a

    move v1, v5

    move v0, v6

    move v2, v5

    .line 1056667
    :goto_5
    if-ge v1, v13, :cond_6

    .line 1056668
    iget-object v12, v9, LX/69C;->i:[LX/69C;

    aget-object v12, v12, v1

    if-eqz v12, :cond_5

    .line 1056669
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v1

    .line 1056670
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1056671
    :cond_6
    if-ne v2, v3, :cond_7

    .line 1056672
    iget-object v1, v8, LX/69C;->i:[LX/69C;

    iget-object v2, v9, LX/69C;->i:[LX/69C;

    aget-object v0, v2, v0

    aput-object v0, v1, v7

    .line 1056673
    invoke-direct {p0, v9}, LX/68s;->c(LX/69C;)V

    .line 1056674
    invoke-virtual {v9}, LX/69C;->c()V

    move v0, v3

    .line 1056675
    :goto_6
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v4, v0

    goto :goto_4

    .line 1056676
    :cond_7
    if-nez v2, :cond_a

    .line 1056677
    iget-object v0, v8, LX/69C;->i:[LX/69C;

    const/4 v1, 0x0

    aput-object v1, v0, v7

    .line 1056678
    invoke-direct {p0, v9}, LX/68s;->c(LX/69C;)V

    .line 1056679
    invoke-virtual {v9}, LX/69C;->c()V

    move v0, v3

    goto :goto_6

    .line 1056680
    :cond_8
    iget-object v0, v8, LX/69C;->j:LX/69C;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v8, v0

    goto :goto_3

    .line 1056681
    :cond_9
    if-nez v4, :cond_4

    .line 1056682
    sget-object v0, LX/31U;->f:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, LX/31U;->f:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    throw v0

    :cond_a
    move v0, v4

    goto :goto_6
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1056683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/68s;->c:LX/69C;

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2}, LX/68s;->a(LX/69C;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
