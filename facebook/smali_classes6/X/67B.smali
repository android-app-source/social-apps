.class public final LX/67B;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/logging/Logger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1052379
    const-class v0, LX/67B;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/67B;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1052377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052378
    return-void
.end method

.method private static a(Ljava/io/InputStream;LX/65f;)LX/65D;
    .locals 2

    .prologue
    .line 1052355
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052356
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052357
    :cond_1
    new-instance v0, LX/679;

    invoke-direct {v0, p1, p0}, LX/679;-><init>(LX/65f;Ljava/io/InputStream;)V

    return-object v0
.end method

.method private static a(Ljava/io/OutputStream;LX/65f;)LX/65J;
    .locals 2

    .prologue
    .line 1052374
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "out == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052375
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052376
    :cond_1
    new-instance v0, LX/678;

    invoke-direct {v0, p1, p0}, LX/678;-><init>(LX/65f;Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static a(Ljava/net/Socket;)LX/65J;
    .locals 2

    .prologue
    .line 1052369
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "socket == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052370
    :cond_0
    invoke-static {p0}, LX/67B;->c(Ljava/net/Socket;)LX/65g;

    move-result-object v0

    .line 1052371
    invoke-virtual {p0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-static {v1, v0}, LX/67B;->a(Ljava/io/OutputStream;LX/65f;)LX/65J;

    move-result-object v1

    .line 1052372
    new-instance p0, LX/66w;

    invoke-direct {p0, v0, v1}, LX/66w;-><init>(LX/65g;LX/65J;)V

    move-object v0, p0

    .line 1052373
    return-object v0
.end method

.method public static a(LX/65J;)LX/670;
    .locals 1

    .prologue
    .line 1052368
    new-instance v0, LX/67C;

    invoke-direct {v0, p0}, LX/67C;-><init>(LX/65J;)V

    return-object v0
.end method

.method public static a(LX/65D;)LX/671;
    .locals 1

    .prologue
    .line 1052367
    new-instance v0, LX/67E;

    invoke-direct {v0, p0}, LX/67E;-><init>(LX/65D;)V

    return-object v0
.end method

.method public static a(Ljava/lang/AssertionError;)Z
    .locals 2

    .prologue
    .line 1052364
    invoke-virtual {p0}, Ljava/lang/AssertionError;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1052365
    invoke-virtual {p0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getsockname failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1052366
    goto :goto_0
.end method

.method public static b(Ljava/net/Socket;)LX/65D;
    .locals 2

    .prologue
    .line 1052359
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "socket == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052360
    :cond_0
    invoke-static {p0}, LX/67B;->c(Ljava/net/Socket;)LX/65g;

    move-result-object v0

    .line 1052361
    invoke-virtual {p0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1, v0}, LX/67B;->a(Ljava/io/InputStream;LX/65f;)LX/65D;

    move-result-object v1

    .line 1052362
    new-instance p0, LX/66x;

    invoke-direct {p0, v0, v1}, LX/66x;-><init>(LX/65g;LX/65D;)V

    move-object v0, p0

    .line 1052363
    return-object v0
.end method

.method private static c(Ljava/net/Socket;)LX/65g;
    .locals 1

    .prologue
    .line 1052358
    new-instance v0, LX/67A;

    invoke-direct {v0, p0}, LX/67A;-><init>(Ljava/net/Socket;)V

    return-object v0
.end method
