.class public LX/5qV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pU;
.implements LX/5qU;


# instance fields
.field private final a:LX/5pq;

.field private final b:LX/5pq;

.field private final c:LX/5pq;

.field private final d:LX/5pq;

.field private volatile e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 1009342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009343
    invoke-static {v1}, LX/5pq;->a(I)LX/5pq;

    move-result-object v0

    iput-object v0, p0, LX/5qV;->a:LX/5pq;

    .line 1009344
    invoke-static {v1}, LX/5pq;->a(I)LX/5pq;

    move-result-object v0

    iput-object v0, p0, LX/5qV;->b:LX/5pq;

    .line 1009345
    invoke-static {v1}, LX/5pq;->a(I)LX/5pq;

    move-result-object v0

    iput-object v0, p0, LX/5qV;->c:LX/5pq;

    .line 1009346
    invoke-static {v1}, LX/5pq;->a(I)LX/5pq;

    move-result-object v0

    iput-object v0, p0, LX/5qV;->d:LX/5pq;

    .line 1009347
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5qV;->e:Z

    return-void
.end method

.method private static a(LX/5pq;J)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1009330
    iget v0, p0, LX/5pq;->b:I

    move v3, v0

    .line 1009331
    move v2, v1

    move v0, v1

    .line 1009332
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1009333
    invoke-virtual {p0, v2}, LX/5pq;->b(I)J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-gez v4, :cond_0

    .line 1009334
    add-int/lit8 v0, v0, 0x1

    .line 1009335
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1009336
    :cond_1
    if-lez v0, :cond_3

    .line 1009337
    :goto_1
    sub-int v2, v3, v0

    if-ge v1, v2, :cond_2

    .line 1009338
    add-int v2, v1, v0

    invoke-virtual {p0, v2}, LX/5pq;->b(I)J

    move-result-wide v4

    invoke-virtual {p0, v1, v4, v5}, LX/5pq;->a(IJ)V

    .line 1009339
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1009340
    :cond_2
    invoke-virtual {p0, v0}, LX/5pq;->c(I)V

    .line 1009341
    :cond_3
    return-void
.end method

.method private static a(LX/5pq;JJ)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1009322
    move v0, v1

    .line 1009323
    :goto_0
    iget v2, p0, LX/5pq;->b:I

    move v2, v2

    .line 1009324
    if-ge v0, v2, :cond_0

    .line 1009325
    invoke-virtual {p0, v0}, LX/5pq;->b(I)J

    move-result-wide v2

    .line 1009326
    cmp-long v4, v2, p1

    if-ltz v4, :cond_1

    cmp-long v2, v2, p3

    if-gez v2, :cond_1

    .line 1009327
    const/4 v1, 0x1

    .line 1009328
    :cond_0
    return v1

    .line 1009329
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/5pq;JJ)J
    .locals 7

    .prologue
    .line 1009288
    const-wide/16 v2, -0x1

    .line 1009289
    const/4 v0, 0x0

    .line 1009290
    :goto_0
    iget v1, p0, LX/5pq;->b:I

    move v1, v1

    .line 1009291
    if-ge v0, v1, :cond_2

    .line 1009292
    invoke-virtual {p0, v0}, LX/5pq;->b(I)J

    move-result-wide v4

    .line 1009293
    cmp-long v1, v4, p1

    if-ltz v1, :cond_1

    cmp-long v1, v4, p3

    if-gez v1, :cond_1

    move-wide v2, v4

    .line 1009294
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1009295
    :cond_1
    cmp-long v1, v4, p3

    if-ltz v1, :cond_0

    .line 1009296
    :cond_2
    return-wide v2
.end method

.method private b(JJ)Z
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    .line 1009317
    iget-object v0, p0, LX/5qV;->a:LX/5pq;

    invoke-static {v0, p1, p2, p3, p4}, LX/5qV;->b(LX/5pq;JJ)J

    move-result-wide v0

    .line 1009318
    iget-object v2, p0, LX/5qV;->b:LX/5pq;

    invoke-static {v2, p1, p2, p3, p4}, LX/5qV;->b(LX/5pq;JJ)J

    move-result-wide v2

    .line 1009319
    cmp-long v4, v0, v6

    if-nez v4, :cond_0

    cmp-long v4, v2, v6

    if-nez v4, :cond_0

    .line 1009320
    iget-boolean v0, p0, LX/5qV;->e:Z

    .line 1009321
    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 1009348
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qV;->a:LX/5pq;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/5pq;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009349
    monitor-exit p0

    return-void

    .line 1009350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JJ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1009306
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/5qV;->d:LX/5pq;

    invoke-static {v1, p1, p2, p3, p4}, LX/5qV;->a(LX/5pq;JJ)Z

    move-result v1

    .line 1009307
    invoke-direct {p0, p1, p2, p3, p4}, LX/5qV;->b(JJ)Z

    move-result v2

    .line 1009308
    if-eqz v1, :cond_1

    .line 1009309
    :cond_0
    :goto_0
    iget-object v1, p0, LX/5qV;->a:LX/5pq;

    invoke-static {v1, p3, p4}, LX/5qV;->a(LX/5pq;J)V

    .line 1009310
    iget-object v1, p0, LX/5qV;->b:LX/5pq;

    invoke-static {v1, p3, p4}, LX/5qV;->a(LX/5pq;J)V

    .line 1009311
    iget-object v1, p0, LX/5qV;->c:LX/5pq;

    invoke-static {v1, p3, p4}, LX/5qV;->a(LX/5pq;J)V

    .line 1009312
    iget-object v1, p0, LX/5qV;->d:LX/5pq;

    invoke-static {v1, p3, p4}, LX/5qV;->a(LX/5pq;J)V

    .line 1009313
    iput-boolean v2, p0, LX/5qV;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009314
    monitor-exit p0

    return v0

    .line 1009315
    :cond_1
    if-eqz v2, :cond_2

    :try_start_1
    iget-object v1, p0, LX/5qV;->c:LX/5pq;

    invoke-static {v1, p1, p2, p3, p4}, LX/5qV;->a(LX/5pq;JJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1009316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 1009303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qV;->b:LX/5pq;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/5pq;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009304
    monitor-exit p0

    return-void

    .line 1009305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 1009300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qV;->c:LX/5pq;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/5pq;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009301
    monitor-exit p0

    return-void

    .line 1009302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 1009297
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qV;->d:LX/5pq;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/5pq;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009298
    monitor-exit p0

    return-void

    .line 1009299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
