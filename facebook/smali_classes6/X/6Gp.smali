.class public final LX/6Gp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;)V
    .locals 0

    .prologue
    .line 1070959
    iput-object p1, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1070960
    iget-object v0, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1070961
    iget-object v0, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->p:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1070962
    sget-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->s:Ljava/lang/Class;

    const-string v1, "Saving the bitmap failed, could not generate Uri."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1070963
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1070964
    check-cast p1, Landroid/net/Uri;

    .line 1070965
    iget-object v0, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->v:LX/6Gq;

    if-eqz v0, :cond_0

    .line 1070966
    iget-object v0, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->v:LX/6Gq;

    invoke-virtual {v0, p1}, LX/6Gq;->a(Landroid/net/Uri;)V

    .line 1070967
    :cond_0
    iget-object v0, p0, LX/6Gp;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1070968
    return-void
.end method
