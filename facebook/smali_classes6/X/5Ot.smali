.class public LX/5Ot;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 909520
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/5Ot;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 909521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909522
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 909523
    iput-object v0, p0, LX/5Ot;->a:LX/0Ot;

    .line 909524
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 909525
    iput-object v0, p0, LX/5Ot;->b:LX/0Ot;

    .line 909526
    return-void
.end method

.method public static a(LX/0QB;)LX/5Ot;
    .locals 8

    .prologue
    .line 909527
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 909528
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 909529
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 909530
    if-nez v1, :cond_0

    .line 909531
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 909532
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 909533
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 909534
    sget-object v1, LX/5Ot;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 909535
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 909536
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 909537
    :cond_1
    if-nez v1, :cond_4

    .line 909538
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 909539
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 909540
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 909541
    new-instance v1, LX/5Ot;

    invoke-direct {v1}, LX/5Ot;-><init>()V

    .line 909542
    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x79a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 909543
    iput-object v7, v1, LX/5Ot;->a:LX/0Ot;

    iput-object p0, v1, LX/5Ot;->b:LX/0Ot;

    .line 909544
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 909545
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 909546
    if-nez v1, :cond_2

    .line 909547
    sget-object v0, LX/5Ot;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ot;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 909548
    :goto_1
    if-eqz v0, :cond_3

    .line 909549
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 909550
    :goto_3
    check-cast v0, LX/5Ot;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 909551
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 909552
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 909553
    :catchall_1
    move-exception v0

    .line 909554
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 909555
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 909556
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 909557
    :cond_2
    :try_start_8
    sget-object v0, LX/5Ot;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ot;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
