.class public final LX/6Zh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/6Zi;


# direct methods
.method public constructor <init>(LX/6Zi;)V
    .locals 0

    .prologue
    .line 1111329
    iput-object p1, p0, LX/6Zh;->a:LX/6Zi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1111330
    iget-object v0, p0, LX/6Zh;->a:LX/6Zi;

    iget-object v0, v0, LX/6Zi;->c:LX/6aJ;

    .line 1111331
    const-string v1, "oxygen_map_here_upsell_dialog_map_image_download_failure"

    .line 1111332
    iget-object v2, v0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v2, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1111333
    :cond_0
    :goto_0
    return-void

    .line 1111334
    :cond_1
    iget-object v2, v0, LX/6aJ;->a:LX/0Zb;

    const/4 p0, 0x0

    invoke-interface {v2, v1, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1111335
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1111336
    const-string v2, "oxygen_map"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string p0, "reason"

    invoke-virtual {v2, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111337
    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1111338
    iget-object v0, p0, LX/6Zh;->a:LX/6Zi;

    iget-object v0, v0, LX/6Zi;->c:LX/6aJ;

    .line 1111339
    const-string v1, "oxygen_map_here_upsell_dialog_map_image_download_success"

    .line 1111340
    iget-object v2, v0, LX/6aJ;->b:LX/0Zm;

    invoke-virtual {v2, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1111341
    :cond_0
    :goto_0
    return-void

    .line 1111342
    :cond_1
    iget-object v2, v0, LX/6aJ;->a:LX/0Zb;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1111343
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1111344
    const-string v2, "oxygen_map"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "query_type"

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "load_time"

    invoke-virtual {v2, v3, p2, p3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1111345
    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_0
.end method
