.class public LX/6Pb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "LX/4WJ;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1086252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086253
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pb;->a:Ljava/lang/String;

    .line 1086254
    iput-boolean p2, p0, LX/6Pb;->b:Z

    .line 1086255
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086251
    iget-object v0, p0, LX/6Pb;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086257
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    check-cast p2, LX/4WJ;

    .line 1086258
    iget-object v0, p0, LX/6Pb;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6Pb;->b:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1086259
    :cond_0
    :goto_0
    return-void

    .line 1086260
    :cond_1
    iget-boolean v0, p0, LX/6Pb;->b:Z

    .line 1086261
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "is_viewer_subscribed"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086262
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086263
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086256
    const-string v0, "SetNotifyMeMutatingVisitor"

    return-object v0
.end method
