.class public LX/6V4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6V4;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/6V4;
    .locals 3

    .prologue
    .line 1103390
    sget-object v0, LX/6V4;->a:LX/6V4;

    if-nez v0, :cond_1

    .line 1103391
    const-class v1, LX/6V4;

    monitor-enter v1

    .line 1103392
    :try_start_0
    sget-object v0, LX/6V4;->a:LX/6V4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1103393
    if-eqz v2, :cond_0

    .line 1103394
    :try_start_1
    new-instance v0, LX/6V4;

    invoke-direct {v0}, LX/6V4;-><init>()V

    .line 1103395
    move-object v0, v0

    .line 1103396
    sput-object v0, LX/6V4;->a:LX/6V4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103397
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103398
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103399
    :cond_1
    sget-object v0, LX/6V4;->a:LX/6V4;

    return-object v0

    .line 1103400
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1103384
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1103385
    const-string v0, "NoId"

    .line 1103386
    :goto_0
    return-object v0

    .line 1103387
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1103388
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1103389
    :catch_0
    const-string v0, "IdNotFound"

    goto :goto_1
.end method
