.class public LX/6PV;
.super LX/6PU;
.source ""


# instance fields
.field private final a:LX/03V;

.field private final b:LX/6Ou;

.field private final c:LX/6Ot;

.field private final d:Z


# direct methods
.method public constructor <init>(LX/03V;LX/6Ou;LX/6Ot;Ljava/lang/String;Z)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086192
    invoke-direct {p0, p4}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086193
    iput-object p1, p0, LX/6PV;->a:LX/03V;

    .line 1086194
    iput-object p2, p0, LX/6PV;->b:LX/6Ou;

    .line 1086195
    iput-object p3, p0, LX/6PV;->c:LX/6Ot;

    .line 1086196
    iput-boolean p5, p0, LX/6PV;->d:Z

    .line 1086197
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 3

    .prologue
    .line 1086198
    invoke-static {p1}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1086199
    if-nez v0, :cond_0

    .line 1086200
    iget-object v0, p0, LX/6PV;->a:LX/03V;

    const-string v1, "null_GroupCommerceItemAttachment"

    const-string v2, "ProductItem is null on story after product availability change. t7270764."

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1086201
    :goto_0
    return-void

    .line 1086202
    :cond_0
    iget-boolean v1, p0, LX/6PV;->d:Z

    invoke-static {v0, v1}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1086203
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-static {v1, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4Yr;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086204
    const-string v0, "ProductAvailabilityMutatingVisitor"

    return-object v0
.end method
