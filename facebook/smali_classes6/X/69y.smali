.class public LX/69y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "LX/4WJ;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLComment;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1058460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058461
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, LX/69y;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1058462
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/69y;->b:Ljava/lang/String;

    .line 1058463
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058459
    iget-object v0, p0, LX/69y;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1058452
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    check-cast p2, LX/4WJ;

    .line 1058453
    iget-object v0, p0, LX/69y;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1058454
    :cond_0
    :goto_0
    return-void

    .line 1058455
    :cond_1
    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058456
    iget-object v0, p0, LX/69y;->a:Lcom/facebook/graphql/model/GraphQLComment;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Z)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4WJ;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058458
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058457
    const-string v0, "AddCommentMutatingVisitor"

    return-object v0
.end method
