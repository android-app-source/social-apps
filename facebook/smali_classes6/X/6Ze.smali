.class public LX/6Ze;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6ZZ;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111316
    iput-object p1, p0, LX/6Ze;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1111317
    iput-object p2, p0, LX/6Ze;->b:Landroid/content/Context;

    .line 1111318
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ze;
    .locals 3

    .prologue
    .line 1111312
    new-instance v2, LX/6Ze;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/6Ze;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    .line 1111313
    move-object v0, v2

    .line 1111314
    return-object v0
.end method


# virtual methods
.method public final a(LX/6ZY;)V
    .locals 2

    .prologue
    .line 1111307
    sget-object v0, LX/6Zd;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1111308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal location upsell dialog result."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1111309
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1111310
    iget-object v1, p0, LX/6Ze;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, p0, LX/6Ze;->b:Landroid/content/Context;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1111311
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
