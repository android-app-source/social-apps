.class public LX/6NW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1083516
    const-class v0, LX/6NW;

    sput-object v0, LX/6NW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1083496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083497
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)B
    .locals 2

    .prologue
    .line 1083498
    sget-object v0, LX/6NV;->d:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1083499
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1083500
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 1083501
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)B
    .locals 3
    .param p0    # Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1083502
    if-nez p0, :cond_0

    .line 1083503
    :goto_0
    return v0

    .line 1083504
    :cond_0
    sget-object v1, LX/6NV;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1083505
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1083506
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1083507
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1083508
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1083509
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)B
    .locals 3
    .param p0    # Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1083510
    if-nez p0, :cond_0

    .line 1083511
    :goto_0
    return v0

    .line 1083512
    :cond_0
    sget-object v1, LX/6NV;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1083513
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1083514
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1083515
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/0eX;II)I
    .locals 2

    .prologue
    .line 1083517
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1083518
    :cond_0
    const/4 v0, 0x0

    .line 1083519
    :goto_0
    return v0

    .line 1083520
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/0eX;->b(I)V

    .line 1083521
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->b(III)V

    .line 1083522
    const/4 v0, 0x0

    .line 1083523
    invoke-virtual {p0, v0, p2, v0}, LX/0eX;->b(III)V

    .line 1083524
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 1083525
    move v0, v0

    .line 1083526
    move v0, v0

    .line 1083527
    goto :goto_0
.end method

.method private static a(LX/0eX;LX/0Px;)I
    .locals 8
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0eX;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1083528
    if-nez p1, :cond_0

    move v0, v1

    .line 1083529
    :goto_0
    return v0

    .line 1083530
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    .line 1083531
    new-array v4, v3, [I

    move v2, v1

    .line 1083532
    :goto_1
    if-ge v2, v3, :cond_1

    .line 1083533
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;

    .line 1083534
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel$PrimaryFieldModel;

    move-result-object v0

    const/4 v5, 0x0

    .line 1083535
    if-nez v0, :cond_3

    .line 1083536
    :goto_2
    move v0, v5

    .line 1083537
    invoke-static {p0, v1, v0}, LX/6Nj;->a(LX/0eX;ZI)I

    move-result v0

    aput v0, v4, v2

    .line 1083538
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1083539
    :cond_1
    const/4 v1, 0x4

    .line 1083540
    array-length v0, v4

    invoke-virtual {p0, v1, v0, v1}, LX/0eX;->a(III)V

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-ltz v0, :cond_2

    aget v1, v4, v0

    invoke-virtual {p0, v1}, LX/0eX;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, LX/0eX;->b()I

    move-result v0

    move v0, v0

    .line 1083541
    goto :goto_0

    .line 1083542
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel$PrimaryFieldModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1083543
    const/4 v0, 0x0

    .line 1083544
    if-nez v6, :cond_4

    .line 1083545
    :goto_4
    move v6, v0

    .line 1083546
    invoke-static {p0, v5, v5, v6, v5}, LX/6Nk;->a(LX/0eX;IIII)I

    move-result v5

    goto :goto_2

    :cond_4
    invoke-virtual {v7, v6, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v0

    .line 1083547
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/0eX;->b(I)V

    .line 1083548
    const/4 v7, 0x0

    .line 1083549
    invoke-virtual {p0, v7, v0, v7}, LX/0eX;->c(III)V

    .line 1083550
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v7

    .line 1083551
    move v7, v7

    .line 1083552
    move v0, v7

    .line 1083553
    goto :goto_4
.end method

.method private static a(LX/0eX;LX/2RU;)I
    .locals 2

    .prologue
    .line 1083554
    sget-object v0, LX/6NV;->c:[I

    invoke-virtual {p1}, LX/2RU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1083555
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1083556
    :pswitch_0
    const-string v0, "User"

    invoke-virtual {p0, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, LX/6Np;->a(LX/0eX;I)I

    move-result v0

    goto :goto_0

    .line 1083557
    :pswitch_1
    const-string v0, "Page"

    invoke-virtual {p0, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, LX/6Np;->a(LX/0eX;I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/0eX;Lcom/facebook/contacts/graphql/Contact;)I
    .locals 22

    .prologue
    .line 1083558
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v2

    .line 1083559
    if-nez v2, :cond_0

    .line 1083560
    const/4 v2, 0x0

    .line 1083561
    :goto_0
    return v2

    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6NW;->a(LX/0eX;LX/2RU;)I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->D()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->C()I

    move-result v5

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, LX/6NW;->a(LX/0eX;II)I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->G()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6NW;->b(LX/0eX;Ljava/lang/String;)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->n()F

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->q()Z

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->r()LX/03R;

    move-result-object v2

    const/4 v10, 0x0

    invoke-virtual {v2, v10}, LX/03R;->asBoolean(Z)Z

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->t()J

    move-result-wide v12

    invoke-static {v12, v13}, LX/6NW;->b(J)J

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->u()Z

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-static {v2}, LX/6NW;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)B

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-static {v2}, LX/6NW;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)B

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->z()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6NW;->b(LX/0eX;LX/0Px;)I

    move-result v18

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->E()Z

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->J()F

    move-result v20

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/Contact;->K()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v21

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v21}, LX/6Ng;->a(LX/0eX;IIIIFFZZZJZBBIIZFI)I

    move-result v2

    goto/16 :goto_0
.end method

.method private static a(LX/0eX;Lcom/facebook/user/model/Name;)I
    .locals 9
    .param p1    # Lcom/facebook/user/model/Name;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 1083562
    if-nez p1, :cond_1

    .line 1083563
    :cond_0
    :goto_0
    return v1

    .line 1083564
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v5

    .line 1083565
    if-eqz v5, :cond_0

    .line 1083566
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v6

    .line 1083567
    new-array v2, v8, [I

    .line 1083568
    if-nez v6, :cond_4

    move v4, v3

    .line 1083569
    :goto_1
    if-ltz v4, :cond_7

    .line 1083570
    invoke-virtual {v5, v1, v4}, Ljava/lang/String;->codePointCount(II)I

    move-result v4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v1, v7}, Ljava/lang/String;->codePointCount(II)I

    move-result v6

    invoke-static {p0, v0, v4, v6}, LX/6Nn;->a(LX/0eX;BII)I

    move-result v4

    aput v4, v2, v1

    .line 1083571
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v4

    .line 1083572
    if-nez v4, :cond_5

    .line 1083573
    :goto_3
    if-ltz v3, :cond_2

    .line 1083574
    const/4 v6, 0x3

    invoke-virtual {v5, v1, v3}, Ljava/lang/String;->codePointCount(II)I

    move-result v3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v1, v7}, Ljava/lang/String;->codePointCount(II)I

    move-result v4

    invoke-static {p0, v6, v3, v4}, LX/6Nn;->a(LX/0eX;BII)I

    move-result v3

    aput v3, v2, v0

    .line 1083575
    add-int/lit8 v0, v0, 0x1

    .line 1083576
    :cond_2
    if-eq v0, v8, :cond_6

    .line 1083577
    invoke-static {v2, v1, v0}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v0

    .line 1083578
    :goto_4
    const/4 v2, 0x4

    .line 1083579
    array-length v1, v0

    invoke-virtual {p0, v2, v1, v2}, LX/0eX;->a(III)V

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    :goto_5
    if-ltz v1, :cond_3

    aget v2, v0, v1

    invoke-virtual {p0, v2}, LX/0eX;->a(I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    :cond_3
    invoke-virtual {p0}, LX/0eX;->b()I

    move-result v1

    move v0, v1

    .line 1083580
    invoke-static {p0, v5}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v1

    .line 1083581
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, LX/0eX;->b(I)V

    .line 1083582
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v1, v3}, LX/0eX;->c(III)V

    .line 1083583
    const/4 v2, 0x0

    .line 1083584
    invoke-virtual {p0, v2, v0, v2}, LX/0eX;->c(III)V

    .line 1083585
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v2

    .line 1083586
    move v2, v2

    .line 1083587
    move v1, v2

    .line 1083588
    goto :goto_0

    .line 1083589
    :cond_4
    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1083590
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public static a(LX/0eX;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1083482
    if-nez p1, :cond_0

    .line 1083483
    const/4 v0, 0x0

    .line 1083484
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/0eX;Ljava/lang/String;I)I
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1083485
    if-nez p1, :cond_0

    .line 1083486
    const/4 v0, 0x0

    .line 1083487
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v0

    .line 1083488
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/0eX;->b(I)V

    .line 1083489
    const/4 v1, 0x1

    const/4 p1, 0x0

    invoke-virtual {p0, v1, p2, p1}, LX/0eX;->b(III)V

    .line 1083490
    const/4 v1, 0x0

    .line 1083491
    invoke-virtual {p0, v1, v0, v1}, LX/0eX;->c(III)V

    .line 1083492
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v1

    .line 1083493
    move v1, v1

    .line 1083494
    move v0, v1

    .line 1083495
    goto :goto_0
.end method

.method public static a(J)J
    .locals 2

    .prologue
    .line 1083203
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method public static a(Ljava/nio/ByteBuffer;)Lcom/facebook/contacts/graphql/Contact;
    .locals 8

    .prologue
    .line 1083214
    invoke-static {p0}, LX/6Ni;->a(Ljava/nio/ByteBuffer;)LX/6Ni;

    move-result-object v0

    .line 1083215
    new-instance v1, LX/3hB;

    invoke-direct {v1}, LX/3hB;-><init>()V

    .line 1083216
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_b

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v2}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 1083217
    iput-object v2, v1, LX/3hB;->a:Ljava/lang/String;

    .line 1083218
    move-object v1, v1

    .line 1083219
    const/4 v2, 0x6

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_c

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v2}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 1083220
    iput-object v2, v1, LX/3hB;->c:Ljava/lang/String;

    .line 1083221
    move-object v1, v1

    .line 1083222
    new-instance v2, LX/6Nm;

    invoke-direct {v2}, LX/6Nm;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->a(LX/6Nm;)LX/6Nm;

    move-result-object v2

    move-object v2, v2

    .line 1083223
    invoke-static {v2}, LX/6NW;->a(LX/6Nm;)Lcom/facebook/user/model/Name;

    move-result-object v2

    .line 1083224
    iput-object v2, v1, LX/3hB;->d:Lcom/facebook/user/model/Name;

    .line 1083225
    move-object v1, v1

    .line 1083226
    new-instance v2, LX/6Nm;

    invoke-direct {v2}, LX/6Nm;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->b(LX/6Nm;)LX/6Nm;

    move-result-object v2

    move-object v2, v2

    .line 1083227
    invoke-static {v2}, LX/6NW;->a(LX/6Nm;)Lcom/facebook/user/model/Name;

    move-result-object v2

    .line 1083228
    iput-object v2, v1, LX/3hB;->h:Lcom/facebook/user/model/Name;

    .line 1083229
    move-object v1, v1

    .line 1083230
    const/4 v2, 0x0

    .line 1083231
    const/16 v3, 0x10

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v2, v2

    .line 1083232
    iput-boolean v2, v1, LX/3hB;->z:Z

    .line 1083233
    move-object v1, v1

    .line 1083234
    invoke-virtual {v0}, LX/6Ni;->n()B

    move-result v2

    invoke-static {v2}, LX/6NW;->a(B)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    .line 1083235
    iput-object v2, v1, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 1083236
    move-object v1, v1

    .line 1083237
    invoke-virtual {v0}, LX/6Ni;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/6NW;->a(J)J

    move-result-wide v2

    .line 1083238
    iput-wide v2, v1, LX/3hB;->D:J

    .line 1083239
    move-object v1, v1

    .line 1083240
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1083241
    const/16 v2, 0x12

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v0, v2}, LX/0eW;->d(I)I

    move-result v2

    :goto_2
    move v4, v2

    .line 1083242
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_2

    .line 1083243
    new-instance v5, LX/6Nj;

    invoke-direct {v5}, LX/6Nj;-><init>()V

    .line 1083244
    const/16 v6, 0x12

    invoke-virtual {v0, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {v0, v6}, LX/0eW;->e(I)I

    move-result v6

    mul-int/lit8 p0, v2, 0x4

    add-int/2addr v6, p0

    invoke-virtual {v0, v6}, LX/0eW;->b(I)I

    move-result v6

    iget-object p0, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6, p0}, LX/6Nj;->a(ILjava/nio/ByteBuffer;)LX/6Nj;

    move-result-object v6

    :goto_4
    move-object v5, v6

    .line 1083245
    move-object v5, v5

    .line 1083246
    invoke-static {v5}, LX/6NW;->a(LX/6Nj;)Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;

    move-result-object v5

    .line 1083247
    if-eqz v5, :cond_1

    .line 1083248
    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1083249
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1083250
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 1083251
    iput-object v2, v1, LX/3hB;->B:LX/0Px;

    .line 1083252
    move-object v1, v1

    .line 1083253
    invoke-static {v0}, LX/6NW;->b(LX/6Ni;)LX/0Px;

    move-result-object v2

    .line 1083254
    iput-object v2, v1, LX/3hB;->q:LX/0Px;

    .line 1083255
    move-object v1, v1

    .line 1083256
    invoke-virtual {v0}, LX/6Ni;->m()Z

    move-result v2

    .line 1083257
    iput-boolean v2, v1, LX/3hB;->O:Z

    .line 1083258
    move-object v1, v1

    .line 1083259
    new-instance v2, LX/6Nq;

    invoke-direct {v2}, LX/6Nq;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->a(LX/6Nq;)LX/6Nq;

    move-result-object v2

    move-object v2, v2

    .line 1083260
    if-eqz v2, :cond_3

    .line 1083261
    invoke-virtual {v2}, LX/6Nq;->a()Ljava/lang/String;

    move-result-object v3

    .line 1083262
    iput-object v3, v1, LX/3hB;->i:Ljava/lang/String;

    .line 1083263
    move-object v3, v1

    .line 1083264
    invoke-virtual {v2}, LX/6Nq;->b()I

    move-result v2

    .line 1083265
    iput v2, v3, LX/3hB;->l:I

    .line 1083266
    :cond_3
    new-instance v2, LX/6Nq;

    invoke-direct {v2}, LX/6Nq;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->b(LX/6Nq;)LX/6Nq;

    move-result-object v2

    move-object v2, v2

    .line 1083267
    if-eqz v2, :cond_4

    .line 1083268
    invoke-virtual {v2}, LX/6Nq;->a()Ljava/lang/String;

    move-result-object v3

    .line 1083269
    iput-object v3, v1, LX/3hB;->j:Ljava/lang/String;

    .line 1083270
    move-object v3, v1

    .line 1083271
    invoke-virtual {v2}, LX/6Nq;->b()I

    move-result v2

    .line 1083272
    iput v2, v3, LX/3hB;->m:I

    .line 1083273
    :cond_4
    new-instance v2, LX/6Nq;

    invoke-direct {v2}, LX/6Nq;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->c(LX/6Nq;)LX/6Nq;

    move-result-object v2

    move-object v2, v2

    .line 1083274
    if-eqz v2, :cond_5

    .line 1083275
    invoke-virtual {v2}, LX/6Nq;->a()Ljava/lang/String;

    move-result-object v3

    .line 1083276
    iput-object v3, v1, LX/3hB;->k:Ljava/lang/String;

    .line 1083277
    move-object v3, v1

    .line 1083278
    invoke-virtual {v2}, LX/6Nq;->b()I

    move-result v2

    .line 1083279
    iput v2, v3, LX/3hB;->n:I

    .line 1083280
    :cond_5
    new-instance v2, LX/6Ng;

    invoke-direct {v2}, LX/6Ng;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ni;->a(LX/6Ng;)LX/6Ng;

    move-result-object v2

    move-object v0, v2

    .line 1083281
    if-eqz v0, :cond_a

    .line 1083282
    invoke-virtual {v0}, LX/6Ng;->a()Ljava/lang/String;

    move-result-object v2

    .line 1083283
    iput-object v2, v1, LX/3hB;->b:Ljava/lang/String;

    .line 1083284
    move-object v2, v1

    .line 1083285
    invoke-virtual {v0}, LX/6Ng;->e()F

    move-result v3

    .line 1083286
    iput v3, v2, LX/3hB;->o:F

    .line 1083287
    move-object v2, v2

    .line 1083288
    const/16 v3, 0xe

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_f

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    :goto_5
    move v3, v3

    .line 1083289
    iput v3, v2, LX/3hB;->p:F

    .line 1083290
    move-object v2, v2

    .line 1083291
    const/4 v3, 0x0

    .line 1083292
    const/16 v4, 0x10

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_6

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eqz v4, :cond_6

    const/4 v3, 0x1

    :cond_6
    move v3, v3

    .line 1083293
    iput-boolean v3, v2, LX/3hB;->s:Z

    .line 1083294
    move-object v2, v2

    .line 1083295
    const/16 v3, 0x1a

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_10

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    :goto_6
    move v3, v3

    .line 1083296
    packed-switch v3, :pswitch_data_0

    .line 1083297
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    :goto_7
    move-object v3, v4

    .line 1083298
    iput-object v3, v2, LX/3hB;->u:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1083299
    move-object v2, v2

    .line 1083300
    invoke-virtual {v0}, LX/6Ng;->m()B

    move-result v3

    .line 1083301
    packed-switch v3, :pswitch_data_1

    .line 1083302
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :goto_8
    move-object v3, v4

    .line 1083303
    iput-object v3, v2, LX/3hB;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1083304
    move-object v2, v2

    .line 1083305
    invoke-virtual {v0}, LX/6Ng;->h()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 1083306
    iput-object v3, v2, LX/3hB;->v:LX/03R;

    .line 1083307
    move-object v2, v2

    .line 1083308
    invoke-virtual {v0}, LX/6Ng;->i()Z

    move-result v3

    .line 1083309
    iput-boolean v3, v2, LX/3hB;->w:Z

    .line 1083310
    move-object v2, v2

    .line 1083311
    invoke-virtual {v0}, LX/6Ng;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/6NW;->a(J)J

    move-result-wide v4

    .line 1083312
    iput-wide v4, v2, LX/3hB;->x:J

    .line 1083313
    move-object v2, v2

    .line 1083314
    const/4 v3, 0x0

    .line 1083315
    const/16 v4, 0x18

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_7

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eqz v4, :cond_7

    const/4 v3, 0x1

    :cond_7
    move v3, v3

    .line 1083316
    iput-boolean v3, v2, LX/3hB;->y:Z

    .line 1083317
    move-object v2, v2

    .line 1083318
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {v0, v3}, LX/0eW;->d(I)I

    move-result v3

    :goto_9
    move v4, v3

    .line 1083319
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1083320
    const/4 v3, 0x0

    :goto_a
    if-ge v3, v4, :cond_8

    .line 1083321
    const/16 v6, 0x20

    invoke-virtual {v0, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_12

    invoke-virtual {v0, v6}, LX/0eW;->e(I)I

    move-result v6

    mul-int/lit8 p0, v3, 0x4

    add-int/2addr v6, p0

    invoke-virtual {v0, v6}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v6

    :goto_b
    move-object v6, v6

    .line 1083322
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1083323
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 1083324
    :cond_8
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 1083325
    iput-object v3, v2, LX/3hB;->C:LX/0Px;

    .line 1083326
    move-object v2, v2

    .line 1083327
    invoke-virtual {v0}, LX/6Ng;->o()Z

    move-result v3

    .line 1083328
    iput-boolean v3, v2, LX/3hB;->H:Z

    .line 1083329
    move-object v2, v2

    .line 1083330
    new-instance v3, LX/6Np;

    invoke-direct {v3}, LX/6Np;-><init>()V

    .line 1083331
    const/4 v4, 0x6

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_13

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, LX/0eW;->b(I)I

    move-result v4

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083332
    iput v4, v3, LX/6Np;->a:I

    iput-object v5, v3, LX/6Np;->b:Ljava/nio/ByteBuffer;

    move-object v4, v3

    .line 1083333
    :goto_c
    move-object v3, v4

    .line 1083334
    move-object v3, v3

    .line 1083335
    if-nez v3, :cond_14

    .line 1083336
    sget-object v4, LX/2RU;->UNMATCHED:LX/2RU;

    .line 1083337
    :goto_d
    move-object v3, v4

    .line 1083338
    iput-object v3, v2, LX/3hB;->A:LX/2RU;

    .line 1083339
    move-object v2, v2

    .line 1083340
    const/16 v3, 0x24

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_18

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    :goto_e
    move v3, v3

    .line 1083341
    iput v3, v2, LX/3hB;->L:F

    .line 1083342
    move-object v2, v2

    .line 1083343
    invoke-virtual {v0}, LX/6Ng;->q()Ljava/lang/String;

    move-result-object v3

    .line 1083344
    iput-object v3, v2, LX/3hB;->M:Ljava/lang/String;

    .line 1083345
    new-instance v2, LX/6Nl;

    invoke-direct {v2}, LX/6Nl;-><init>()V

    invoke-virtual {v0, v2}, LX/6Ng;->a(LX/6Nl;)LX/6Nl;

    move-result-object v2

    move-object v2, v2

    .line 1083346
    if-eqz v2, :cond_9

    .line 1083347
    invoke-virtual {v2}, LX/6Nl;->b()I

    move-result v3

    invoke-virtual {v2}, LX/6Nl;->a()I

    move-result v2

    invoke-virtual {v1, v3, v2}, LX/3hB;->a(II)LX/3hB;

    .line 1083348
    :cond_9
    new-instance v2, LX/6Nh;

    invoke-direct {v2}, LX/6Nh;-><init>()V

    .line 1083349
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_19

    iget v4, v0, LX/0eW;->a:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083350
    iput v3, v2, LX/6Nh;->a:I

    iput-object v4, v2, LX/6Nh;->b:Ljava/nio/ByteBuffer;

    move-object v3, v2

    .line 1083351
    :goto_f
    move-object v2, v3

    .line 1083352
    move-object v0, v2

    .line 1083353
    if-eqz v0, :cond_a

    .line 1083354
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_1a

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v2}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v2

    :goto_10
    move-object v0, v2

    .line 1083355
    iput-object v0, v1, LX/3hB;->G:Ljava/lang/String;

    .line 1083356
    :cond_a
    invoke-virtual {v1}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    return-object v0

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 1083357
    :pswitch_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto/16 :goto_7

    .line 1083358
    :pswitch_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto/16 :goto_7

    .line 1083359
    :pswitch_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto/16 :goto_7

    .line 1083360
    :pswitch_3
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto/16 :goto_8

    .line 1083361
    :pswitch_4
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto/16 :goto_8

    .line 1083362
    :pswitch_5
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto/16 :goto_8

    .line 1083363
    :pswitch_6
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto/16 :goto_8

    .line 1083364
    :pswitch_7
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto/16 :goto_8

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_b

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 1083365
    :cond_14
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_17

    iget v5, v3, LX/0eW;->a:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v4

    :goto_11
    move-object v4, v4

    .line 1083366
    const-string v5, "User"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1083367
    sget-object v4, LX/2RU;->USER:LX/2RU;

    goto/16 :goto_d

    .line 1083368
    :cond_15
    const-string v5, "Page"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1083369
    sget-object v4, LX/2RU;->PAGE:LX/2RU;

    goto/16 :goto_d

    .line 1083370
    :cond_16
    sget-object v5, LX/6NW;->a:Ljava/lang/Class;

    const-string v6, "Malformed contact type name: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v4, v7, p0

    invoke-static {v5, v6, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1083371
    sget-object v4, LX/2RU;->UNMATCHED:LX/2RU;

    goto/16 :goto_d

    :cond_17
    const/4 v4, 0x0

    goto :goto_11

    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_e

    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_f

    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_10

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(LX/6Nj;)Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;
    .locals 8
    .param p0    # LX/6Nj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1083372
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/6MO;

    invoke-direct {v0}, LX/6MO;-><init>()V

    invoke-virtual {p0}, LX/6Nj;->b()LX/6Nk;

    move-result-object v1

    .line 1083373
    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 1083374
    iput-object v1, v0, LX/6MO;->a:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel$PrimaryFieldModel;

    .line 1083375
    move-object v0, v0

    .line 1083376
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1083377
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1083378
    iget-object v3, v0, LX/6MO;->a:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel$PrimaryFieldModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1083379
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1083380
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1083381
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1083382
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1083383
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1083384
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1083385
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1083386
    new-instance v3, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;

    invoke-direct {v3, v2}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;-><init>(LX/15i;)V

    .line 1083387
    move-object v0, v3

    .line 1083388
    goto :goto_0

    .line 1083389
    :cond_1
    new-instance v2, LX/6Nr;

    invoke-direct {v2}, LX/6Nr;-><init>()V

    .line 1083390
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_2

    iget v4, v1, LX/0eW;->a:I

    add-int/2addr v3, v4

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object v4, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083391
    iput v3, v2, LX/6Nr;->a:I

    iput-object v4, v2, LX/6Nr;->b:Ljava/nio/ByteBuffer;

    move-object v3, v2

    .line 1083392
    :goto_2
    move-object v2, v3

    .line 1083393
    move-object v2, v2

    .line 1083394
    const/4 v3, 0x0

    .line 1083395
    if-nez v2, :cond_3

    const/4 v4, 0x0

    :goto_3
    invoke-static {v4, v3}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v3

    move-object v2, v3

    .line 1083396
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1083397
    new-instance v4, LX/6MP;

    invoke-direct {v4}, LX/6MP;-><init>()V

    invoke-virtual {v4, v3, v2}, LX/6MP;->a(LX/15i;I)LX/6MP;

    move-result-object v2

    invoke-virtual {v2}, LX/6MP;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel$PrimaryFieldModel;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 1083398
    :cond_3
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_4

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v2, v4}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v4

    :goto_4
    move-object v4, v4

    .line 1083399
    new-instance p0, LX/186;

    const/16 v1, 0x400

    invoke-direct {p0, v1}, LX/186;-><init>(I)V

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    const v3, 0x15136e12

    invoke-static {p0, v3}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    goto :goto_4
.end method

.method public static a(B)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 1

    .prologue
    .line 1083400
    packed-switch p0, :pswitch_data_0

    .line 1083401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    :goto_0
    return-object v0

    .line 1083402
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->NO_CONNECTION:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0

    .line 1083403
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/6Nm;)Lcom/facebook/user/model/Name;
    .locals 9
    .param p0    # LX/6Nm;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1083404
    if-nez p0, :cond_0

    .line 1083405
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v1, v1}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083406
    :goto_0
    return-object v0

    .line 1083407
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_3

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v3, v0

    .line 1083408
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_2
    move v4, v0

    .line 1083409
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_3
    if-ge v2, v4, :cond_2

    .line 1083410
    new-instance v5, LX/6Nn;

    invoke-direct {v5}, LX/6Nn;-><init>()V

    .line 1083411
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {p0, v6}, LX/0eW;->e(I)I

    move-result v6

    mul-int/lit8 v7, v2, 0x4

    add-int/2addr v6, v7

    invoke-virtual {p0, v6}, LX/0eW;->b(I)I

    move-result v6

    iget-object v7, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083412
    iput v6, v5, LX/6Nn;->a:I

    iput-object v7, v5, LX/6Nn;->b:Ljava/nio/ByteBuffer;

    move-object v6, v5

    .line 1083413
    :goto_4
    move-object v5, v6

    .line 1083414
    move-object v5, v5

    .line 1083415
    if-eqz v5, :cond_1

    .line 1083416
    const/4 v6, 0x4

    invoke-virtual {v5, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_6

    iget-object v7, v5, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v8, v5, LX/0eW;->a:I

    add-int/2addr v6, v8

    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    :goto_5
    move v6, v6

    .line 1083417
    packed-switch v6, :pswitch_data_0

    .line 1083418
    :cond_1
    :goto_6
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1083419
    :pswitch_1
    invoke-virtual {v5}, LX/6Nn;->b()I

    move-result v1

    invoke-virtual {v5}, LX/6Nn;->c()I

    move-result v5

    invoke-static {v3, v1, v5}, LX/6NW;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    .line 1083420
    :pswitch_2
    invoke-virtual {v5}, LX/6Nn;->b()I

    move-result v0

    invoke-virtual {v5}, LX/6Nn;->c()I

    move-result v5

    invoke-static {v3, v0, v5}, LX/6NW;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 1083421
    :cond_2
    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-direct {v2, v1, v0, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_4

    :cond_6
    const/4 v6, 0x0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1083422
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v0

    .line 1083423
    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/contacts/graphql/Contact;)Ljava/nio/ByteBuffer;
    .locals 20

    .prologue
    .line 1083424
    invoke-static/range {p0 .. p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083425
    new-instance v3, LX/0eX;

    const/16 v2, 0x200

    invoke-direct {v3, v2}, LX/0eX;-><init>(I)V

    .line 1083426
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    invoke-static {v3, v0}, LX/6NW;->a(LX/0eX;Lcom/facebook/contacts/graphql/Contact;)I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->a(LX/0eX;Lcom/facebook/user/model/Name;)I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->a(LX/0eX;Lcom/facebook/user/model/Name;)I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->c(LX/0eX;LX/0Px;)I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->v()Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->B()LX/0Px;

    move-result-object v2

    invoke-static {v3, v2}, LX/6NW;->a(LX/0eX;LX/0Px;)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->w()J

    move-result-wide v12

    invoke-static {v12, v13}, LX/6NW;->b(J)J

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->j()I

    move-result v14

    invoke-static {v3, v2, v14}, LX/6NW;->a(LX/0eX;Ljava/lang/String;I)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->k()I

    move-result v15

    invoke-static {v3, v2, v15}, LX/6NW;->a(LX/0eX;Ljava/lang/String;I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->l()I

    move-result v16

    move/from16 v0, v16

    invoke-static {v3, v2, v0}, LX/6NW;->a(LX/0eX;Ljava/lang/String;I)I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->M()Z

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->u()Z

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/Contact;->N()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    invoke-static {v2}, LX/6NW;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)B

    move-result v19

    invoke-static/range {v3 .. v19}, LX/6Ni;->a(LX/0eX;IIIIIIZIJIIIZZB)I

    move-result v2

    .line 1083427
    invoke-static {v3, v2}, LX/6Ni;->a(LX/0eX;I)V

    .line 1083428
    invoke-virtual {v3}, LX/0eX;->d()Ljava/nio/ByteBuffer;

    move-result-object v2

    return-object v2
.end method

.method private static b(LX/0eX;LX/0Px;)I
    .locals 4
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0eX;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1083204
    if-nez p1, :cond_0

    .line 1083205
    :goto_0
    return v0

    .line 1083206
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    .line 1083207
    new-array v3, v2, [I

    move v1, v0

    .line 1083208
    :goto_1
    if-ge v1, v2, :cond_1

    .line 1083209
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v0

    aput v0, v3, v1

    .line 1083210
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1083211
    :cond_1
    const/4 v1, 0x4

    .line 1083212
    array-length v0, v3

    invoke-virtual {p0, v1, v0, v1}, LX/0eX;->a(III)V

    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_2

    aget v1, v3, v0

    invoke-virtual {p0, v1}, LX/0eX;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, LX/0eX;->b()I

    move-result v0

    move v0, v0

    .line 1083213
    goto :goto_0
.end method

.method private static b(LX/0eX;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1083429
    if-nez p1, :cond_0

    .line 1083430
    const/4 v0, 0x0

    .line 1083431
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v0

    .line 1083432
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/0eX;->b(I)V

    .line 1083433
    const/4 p1, 0x0

    .line 1083434
    invoke-virtual {p0, p1, v0, p1}, LX/0eX;->c(III)V

    .line 1083435
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result p1

    .line 1083436
    move p1, p1

    .line 1083437
    move v0, p1

    .line 1083438
    goto :goto_0
.end method

.method private static b(J)J
    .locals 2

    .prologue
    .line 1083439
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private static b(LX/6Ni;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Ni;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1083440
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1083441
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_0
    move v8, v0

    .line 1083442
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_2

    .line 1083443
    new-instance v0, LX/6Nj;

    invoke-direct {v0}, LX/6Nj;-><init>()V

    .line 1083444
    const/16 v1, 0xe

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, v1}, LX/0eW;->e(I)I

    move-result v1

    mul-int/lit8 v2, v6, 0x4

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, LX/6Nj;->a(ILjava/nio/ByteBuffer;)LX/6Nj;

    move-result-object v1

    :goto_2
    move-object v0, v1

    .line 1083445
    move-object v5, v0

    .line 1083446
    if-eqz v5, :cond_1

    .line 1083447
    invoke-virtual {v5}, LX/6Nj;->b()LX/6Nk;

    move-result-object v2

    .line 1083448
    if-eqz v2, :cond_1

    .line 1083449
    new-instance v0, LX/6No;

    invoke-direct {v0}, LX/6No;-><init>()V

    .line 1083450
    const/16 v1, 0xa

    invoke-virtual {v2, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_5

    iget v3, v2, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v3, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083451
    iput v1, v0, LX/6No;->a:I

    iput-object v3, v0, LX/6No;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1083452
    :goto_3
    move-object v0, v1

    .line 1083453
    move-object v4, v0

    .line 1083454
    if-eqz v4, :cond_1

    .line 1083455
    new-instance v0, Lcom/facebook/contacts/graphql/ContactPhone;

    .line 1083456
    const/4 v1, 0x4

    invoke-virtual {v2, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_6

    iget v3, v2, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object v1, v1

    .line 1083457
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_7

    iget v9, v2, LX/0eW;->a:I

    add-int/2addr v3, v9

    invoke-virtual {v2, v3}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v3

    :goto_5
    move-object v2, v3

    .line 1083458
    const/4 v3, 0x4

    invoke-virtual {v4, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_8

    iget v9, v4, LX/0eW;->a:I

    add-int/2addr v3, v9

    invoke-virtual {v4, v3}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v3

    :goto_6
    move-object v3, v3

    .line 1083459
    const/4 v9, 0x6

    invoke-virtual {v4, v9}, LX/0eW;->a(I)I

    move-result v9

    if-eqz v9, :cond_9

    iget v10, v4, LX/0eW;->a:I

    add-int/2addr v9, v10

    invoke-virtual {v4, v9}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v9

    :goto_7
    move-object v4, v9

    .line 1083460
    const/4 v9, 0x0

    .line 1083461
    const/4 v10, 0x4

    invoke-virtual {v5, v10}, LX/0eW;->a(I)I

    move-result v10

    if-eqz v10, :cond_0

    iget-object v11, v5, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v12, v5, LX/0eW;->a:I

    add-int/2addr v10, v12

    invoke-virtual {v11, v10}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x1

    :cond_0
    move v5, v9

    .line 1083462
    invoke-direct/range {v0 .. v5}, Lcom/facebook/contacts/graphql/ContactPhone;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1083463
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    .line 1083464
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    goto :goto_5

    :cond_8
    const/4 v3, 0x0

    goto :goto_6

    :cond_9
    const/4 v9, 0x0

    goto :goto_7
.end method

.method private static c(LX/0eX;LX/0Px;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0eX;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1083465
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    .line 1083466
    new-array v4, v3, [I

    move v1, v2

    .line 1083467
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1083468
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactPhone;

    .line 1083469
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->e()Z

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/6NW;->a(LX/0eX;Ljava/lang/String;)I

    move-result v0

    .line 1083470
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/0eX;->b(I)V

    .line 1083471
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v0, v10}, LX/0eX;->c(III)V

    .line 1083472
    const/4 v9, 0x0

    .line 1083473
    invoke-virtual {p0, v9, v8, v9}, LX/0eX;->c(III)V

    .line 1083474
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v9

    .line 1083475
    move v9, v9

    .line 1083476
    move v0, v9

    .line 1083477
    invoke-static {p0, v6, v7, v2, v0}, LX/6Nk;->a(LX/0eX;IIII)I

    move-result v0

    invoke-static {p0, v5, v0}, LX/6Nj;->a(LX/0eX;ZI)I

    move-result v0

    aput v0, v4, v1

    .line 1083478
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1083479
    :cond_0
    const/4 v1, 0x4

    .line 1083480
    array-length v0, v4

    invoke-virtual {p0, v1, v0, v1}, LX/0eX;->a(III)V

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    aget v1, v4, v0

    invoke-virtual {p0, v1}, LX/0eX;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, LX/0eX;->b()I

    move-result v0

    move v0, v0

    .line 1083481
    return v0
.end method
