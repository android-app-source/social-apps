.class public final LX/5Z3;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 943976
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    const v0, -0x3d72a56d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "MessagesQuery"

    const-string v6, "0f94a55c9268e1c9efeb211a5c6092ec"

    const-string v7, "messages"

    const-string v8, "10155265581916729"

    const/4 v9, 0x0

    .line 943977
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 943978
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 943979
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 943980
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 943981
    sparse-switch v0, :sswitch_data_0

    .line 943982
    :goto_0
    return-object p1

    .line 943983
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 943984
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 943985
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 943986
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 943987
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 943988
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 943989
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 943990
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 943991
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 943992
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 943993
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 943994
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 943995
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 943996
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 943997
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_5
        -0x5f54881d -> :sswitch_3
        -0x5bcbf522 -> :sswitch_d
        -0x56855c4a -> :sswitch_b
        -0x4c47f2a9 -> :sswitch_a
        -0x3fb4303b -> :sswitch_0
        -0x39e54905 -> :sswitch_e
        -0x786d0bb -> :sswitch_8
        -0x3224078 -> :sswitch_9
        -0x8d30fe -> :sswitch_7
        0x8da57ae -> :sswitch_4
        0x19ec4b2a -> :sswitch_1
        0x3349e8c0 -> :sswitch_c
        0x5af48aaa -> :sswitch_2
        0x5ba7488b -> :sswitch_6
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 943998
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueries$MessagesQueryString$1;

    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueries$MessagesQueryString$1;-><init>(LX/5Z3;Ljava/lang/Class;)V

    return-object v0
.end method
