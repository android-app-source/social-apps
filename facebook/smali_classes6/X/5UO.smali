.class public final LX/5UO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 924793
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 924794
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 924795
    :goto_0
    return v1

    .line 924796
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 924797
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 924798
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 924799
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 924800
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 924801
    const-string v4, "action_links"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 924802
    invoke-static {p0, p1}, LX/5UI;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 924803
    :cond_2
    const-string v4, "show_at"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 924804
    invoke-static {p0, p1}, LX/5UL;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 924805
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 924806
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 924807
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 924808
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 924809
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 924810
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 924811
    if-eqz v0, :cond_0

    .line 924812
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924813
    invoke-static {p0, v0, p2, p3}, LX/5UI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 924814
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 924815
    if-eqz v0, :cond_1

    .line 924816
    const-string v1, "show_at"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924817
    invoke-static {p0, v0, p2}, LX/5UL;->a(LX/15i;ILX/0nX;)V

    .line 924818
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 924819
    return-void
.end method
