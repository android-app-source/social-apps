.class public LX/63G;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1043051
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1043052
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x2a71dad7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1043053
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043054
    :goto_0
    const v1, 0x5d3142ab

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 1043055
    :catch_0
    invoke-virtual {p0}, LX/63G;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/63G;->setText(Ljava/lang/CharSequence;)V

    .line 1043056
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V

    goto :goto_0
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 1043057
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043058
    :goto_0
    return-void

    .line 1043059
    :catch_0
    invoke-virtual {p0}, LX/63G;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/63G;->setText(Ljava/lang/CharSequence;)V

    .line 1043060
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V

    goto :goto_0
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 1043061
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043062
    :goto_0
    return-void

    .line 1043063
    :catch_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/63G;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
