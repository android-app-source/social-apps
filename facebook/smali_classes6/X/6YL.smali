.class public LX/6YL;
.super LX/6YK;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6YJ;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1109717
    invoke-direct {p0, p1}, LX/6YK;-><init>(Landroid/content/Context;)V

    .line 1109718
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6YL;->a:Ljava/util/List;

    .line 1109719
    return-void
.end method


# virtual methods
.method public final a(LX/6Y7;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1109720
    iget-object v0, p0, LX/6YL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1109721
    invoke-virtual {p0}, LX/6YL;->removeAllViews()V

    .line 1109722
    const/4 v0, 0x0

    .line 1109723
    invoke-virtual {p1}, LX/6Y7;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Y6;

    .line 1109724
    iget-object v3, v0, LX/6Y6;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1109725
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1109726
    iget-object v3, v0, LX/6Y6;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1109727
    new-instance v6, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/6YL;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1109728
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109729
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    invoke-virtual {p0}, LX/6YL;->getContext()Landroid/content/Context;

    move-result-object v9

    const/high16 v10, 0x42400000    # 48.0f

    invoke-static {v9, v10}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1109730
    invoke-virtual {p0}, LX/6YL;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b06dd

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1109731
    const/4 v8, 0x0

    invoke-virtual {p0}, LX/6YL;->getContext()Landroid/content/Context;

    move-result-object v9

    const/high16 v10, 0x40800000    # 4.0f

    invoke-static {v9, v10}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v9

    invoke-virtual {v6, v7, v8, v7, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1109732
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1109733
    invoke-virtual {p0}, LX/6YL;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0a27

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1109734
    const/16 v7, 0x50

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 1109735
    move-object v3, v6

    .line 1109736
    invoke-virtual {p0, v3}, LX/6YL;->addView(Landroid/view/View;)V

    .line 1109737
    :cond_0
    invoke-virtual {p0}, LX/6YK;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/6YL;->addView(Landroid/view/View;)V

    .line 1109738
    new-instance v3, LX/6YJ;

    invoke-virtual {p0}, LX/6YL;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, LX/6YJ;-><init>(Landroid/content/Context;)V

    .line 1109739
    const/4 v8, 0x0

    .line 1109740
    iget-object v6, v0, LX/6Y6;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1109741
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1109742
    iget-object v6, v3, LX/6YJ;->a:Landroid/widget/TextView;

    .line 1109743
    iget-object v7, v0, LX/6Y6;->b:Ljava/lang/String;

    move-object v7, v7

    .line 1109744
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109745
    iget-object v6, v3, LX/6YJ;->a:Landroid/widget/TextView;

    .line 1109746
    iget-object v7, v0, LX/6Y6;->b:Ljava/lang/String;

    move-object v7, v7

    .line 1109747
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109748
    iget-object v6, v3, LX/6YJ;->a:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1109749
    :cond_1
    iget-object v6, v0, LX/6Y6;->c:Ljava/lang/String;

    move-object v6, v6

    .line 1109750
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1109751
    iget-object v6, v3, LX/6YJ;->b:Landroid/widget/TextView;

    .line 1109752
    iget-object v7, v0, LX/6Y6;->c:Ljava/lang/String;

    move-object v7, v7

    .line 1109753
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109754
    iget-object v6, v3, LX/6YJ;->b:Landroid/widget/TextView;

    .line 1109755
    iget-object v7, v0, LX/6Y6;->c:Ljava/lang/String;

    move-object v7, v7

    .line 1109756
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109757
    iget-object v6, v3, LX/6YJ;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1109758
    :cond_2
    iget-object v6, v3, LX/6YJ;->c:Landroid/widget/TextView;

    .line 1109759
    iget-object v7, v0, LX/6Y6;->e:Landroid/os/Parcelable;

    move-object v7, v7

    .line 1109760
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1109761
    iget-object v6, v3, LX/6YJ;->d:Landroid/widget/ImageButton;

    .line 1109762
    iget-object v7, v0, LX/6Y6;->e:Landroid/os/Parcelable;

    move-object v7, v7

    .line 1109763
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1109764
    iget-boolean v6, v0, LX/6Y6;->f:Z

    move v6, v6

    .line 1109765
    if-eqz v6, :cond_6

    .line 1109766
    iget-object v6, v3, LX/6YJ;->d:Landroid/widget/ImageButton;

    invoke-virtual {v6, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1109767
    :goto_1
    invoke-virtual {v3, v8}, LX/6YJ;->setVisibility(I)V

    .line 1109768
    iget-object v0, p0, LX/6YL;->b:Landroid/view/View$OnClickListener;

    .line 1109769
    iget-object v6, v3, LX/6YJ;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1109770
    iget-object v6, v3, LX/6YJ;->d:Landroid/widget/ImageButton;

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1109771
    iget-object v0, p0, LX/6YL;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1109772
    invoke-virtual {p0, v3}, LX/6YL;->addView(Landroid/view/View;)V

    .line 1109773
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_0

    .line 1109774
    :cond_3
    if-eqz v0, :cond_5

    .line 1109775
    iget-object v1, p1, LX/6Y7;->b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object v1, v1

    .line 1109776
    if-eqz v1, :cond_4

    .line 1109777
    invoke-virtual {p0}, LX/6YL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b06e0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/6YL;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b06f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v1, v2, v3}, LX/6YJ;->setPadding(IIII)V

    .line 1109778
    new-instance v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;

    invoke-virtual {p0}, LX/6YL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;-><init>(Landroid/content/Context;)V

    .line 1109779
    iget-object v1, p1, LX/6Y7;->b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object v1, v1

    .line 1109780
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->setCheckListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1109781
    iget-boolean v1, p1, LX/6Y7;->c:Z

    move v1, v1

    .line 1109782
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->setChecked(Z)V

    .line 1109783
    invoke-virtual {p0, v0}, LX/6YL;->addView(Landroid/view/View;)V

    .line 1109784
    :cond_4
    invoke-virtual {p0}, LX/6YK;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6YL;->addView(Landroid/view/View;)V

    .line 1109785
    :cond_5
    invoke-virtual {p0, v2}, LX/6YL;->setVisibility(I)V

    .line 1109786
    return-void

    .line 1109787
    :cond_6
    iget-object v6, v3, LX/6YJ;->c:Landroid/widget/TextView;

    .line 1109788
    iget-object v7, v0, LX/6Y6;->d:Ljava/lang/String;

    move-object v7, v7

    .line 1109789
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109790
    iget-object v6, v3, LX/6YJ;->c:Landroid/widget/TextView;

    .line 1109791
    iget-object v7, v0, LX/6Y6;->d:Ljava/lang/String;

    move-object v7, v7

    .line 1109792
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109793
    iget-object v6, v3, LX/6YJ;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
