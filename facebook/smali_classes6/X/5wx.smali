.class public final LX/5wx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1026918
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1026919
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1026920
    :goto_0
    return v1

    .line 1026921
    :cond_0
    const-string v10, "modified_time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1026922
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 1026923
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 1026924
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1026925
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1026926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1026927
    const-string v10, "album"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1026928
    invoke-static {p0, p1}, LX/5ww;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1026929
    :cond_2
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1026930
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1026931
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1026932
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1026933
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1026934
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 1026935
    if-eqz v0, :cond_5

    .line 1026936
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1026937
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move-wide v2, v4

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1026938
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1026939
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1026940
    if-eqz v0, :cond_0

    .line 1026941
    const-string v1, "album"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1026942
    invoke-static {p0, v0, p2}, LX/5ww;->a(LX/15i;ILX/0nX;)V

    .line 1026943
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1026944
    if-eqz v0, :cond_1

    .line 1026945
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1026946
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1026947
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1026948
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 1026949
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1026950
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1026951
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1026952
    return-void
.end method
