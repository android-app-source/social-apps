.class public final LX/5aq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 956273
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_4

    .line 956274
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956275
    :goto_0
    return v1

    .line 956276
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 956277
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 956278
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956279
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_0

    if-eqz v4, :cond_0

    .line 956280
    const-string v5, "has_next_page"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 956281
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 956282
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 956283
    :cond_2
    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 956284
    if-eqz v0, :cond_3

    .line 956285
    invoke-virtual {p1, v1, v3}, LX/186;->a(IZ)V

    .line 956286
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 956287
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956288
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 956289
    if-eqz v0, :cond_0

    .line 956290
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956291
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 956292
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956293
    return-void
.end method
