.class public final LX/5OW;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/0ht;


# direct methods
.method public constructor <init>(LX/0ht;)V
    .locals 0

    .prologue
    .line 909062
    iput-object p1, p0, LX/5OW;->a:LX/0ht;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0wd;)V
    .locals 1

    .prologue
    .line 909063
    iget-object v0, p0, LX/5OW;->a:LX/0ht;

    .line 909064
    iget-boolean p0, v0, LX/0ht;->r:Z

    move p0, p0

    .line 909065
    if-nez p0, :cond_1

    .line 909066
    :cond_0
    :goto_0
    return-void

    .line 909067
    :cond_1
    invoke-static {}, LX/0ht;->r()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 909068
    iget-object p0, v0, LX/0ht;->g:LX/5OY;

    invoke-virtual {p0}, LX/5OY;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup;

    iget-object p1, v0, LX/0ht;->g:LX/5OY;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 909069
    :goto_1
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/0ht;->r:Z

    .line 909070
    invoke-virtual {v0}, LX/0ht;->h()Landroid/view/View;

    move-result-object p1

    .line 909071
    const/4 p0, 0x0

    .line 909072
    if-eqz p1, :cond_2

    .line 909073
    iget-object p0, v0, LX/0ht;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 909074
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p0

    .line 909075
    :cond_2
    invoke-static {v0, p0}, LX/0ht;->a(LX/0ht;Landroid/view/ViewTreeObserver;)V

    .line 909076
    iget-object p0, v0, LX/0ht;->H:LX/2dD;

    if-eqz p0, :cond_0

    .line 909077
    iget-object p0, v0, LX/0ht;->H:LX/2dD;

    invoke-interface {p0, v0}, LX/2dD;->a(LX/0ht;)Z

    goto :goto_0

    .line 909078
    :cond_3
    :try_start_0
    iget-object p0, v0, LX/0ht;->m:Landroid/view/WindowManager;

    iget-object p1, v0, LX/0ht;->g:LX/5OY;

    invoke-interface {p0, p1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    goto :goto_1
.end method
