.class public LX/6EF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0lC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/6EV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1065957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065958
    return-void
.end method

.method public static b(LX/0QB;)LX/6EF;
    .locals 4

    .prologue
    .line 1065959
    new-instance v3, LX/6EF;

    invoke-direct {v3}, LX/6EF;-><init>()V

    .line 1065960
    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {p0}, LX/6EV;->b(LX/0QB;)LX/6EV;

    move-result-object v2

    check-cast v2, LX/6EV;

    .line 1065961
    iput-object v0, v3, LX/6EF;->a:Ljava/util/concurrent/Executor;

    iput-object v1, v3, LX/6EF;->b:LX/0lC;

    iput-object v2, v3, LX/6EF;->c:LX/6EV;

    .line 1065962
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1065963
    const/4 v5, 0x0

    .line 1065964
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1065965
    sget-object v2, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v2}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1065966
    sget-object v2, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v2}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1065967
    :cond_0
    new-instance v2, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v3, p1

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1065968
    const-string v3, "authorize_instant_experience_oepration_param"

    invoke-virtual {v8, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1065969
    iget-object v2, p0, LX/6EF;->c:LX/6EV;

    const-string v3, "authorize_instant_experience_operation_type"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v2, v3, v8, v4, v5}, LX/6EV;->a(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    move-object v0, v2

    .line 1065970
    new-instance v1, LX/6EE;

    invoke-direct {v1, p0, p1, p2}, LX/6EE;-><init>(LX/6EF;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
