.class public final LX/5XH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D

.field public e:D

.field public f:D

.field public g:D

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 934835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;
    .locals 7

    .prologue
    .line 934836
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 934837
    iget-object v1, p0, LX/5XH;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 934838
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 934839
    const/4 v1, 0x0

    iget-wide v2, p0, LX/5XH;->a:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934840
    const/4 v1, 0x1

    iget-wide v2, p0, LX/5XH;->b:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934841
    const/4 v1, 0x2

    iget-wide v2, p0, LX/5XH;->c:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934842
    const/4 v1, 0x3

    iget-wide v2, p0, LX/5XH;->d:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934843
    const/4 v1, 0x4

    iget-wide v2, p0, LX/5XH;->e:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934844
    const/4 v1, 0x5

    iget-wide v2, p0, LX/5XH;->f:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934845
    const/4 v1, 0x6

    iget-wide v2, p0, LX/5XH;->g:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 934846
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 934847
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 934848
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 934849
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 934850
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 934851
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 934852
    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;-><init>(LX/15i;)V

    .line 934853
    return-object v1
.end method
