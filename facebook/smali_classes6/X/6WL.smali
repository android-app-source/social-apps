.class public final LX/6WL;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/fig/mediagrid/FigMediaGrid;


# direct methods
.method public constructor <init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;)V
    .locals 0

    .prologue
    .line 1105672
    iput-object p1, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;B)V
    .locals 0

    .prologue
    .line 1105673
    invoke-direct {p0, p1}, LX/6WL;-><init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1105674
    iget-object v0, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v0, v0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->e:LX/6WM;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1105675
    iget-object v1, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v1, v1, Lcom/facebook/fig/mediagrid/FigMediaGrid;->e:LX/6WM;

    if-nez v1, :cond_1

    .line 1105676
    :cond_0
    :goto_0
    return v0

    .line 1105677
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v2, v1

    .line 1105678
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v3, v1

    .line 1105679
    iget-object v1, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v1, v1, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v1}, LX/6WP;->e()I

    move-result v4

    move v1, v0

    .line 1105680
    :goto_1
    if-ge v1, v4, :cond_0

    .line 1105681
    iget-object v5, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-static {v5, v1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1105682
    iget-object v0, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-static {v0, v1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->b(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1105683
    iget-object v2, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v2, v2, Lcom/facebook/fig/mediagrid/FigMediaGrid;->e:LX/6WM;

    iget-object v3, p0, LX/6WL;->a:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-static {v3, v1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->c(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)LX/1aZ;

    move-result-object v3

    invoke-interface {v2, v1, v3, v0}, LX/6WM;->a(ILX/1aZ;Landroid/graphics/Rect;)V

    .line 1105684
    const/4 v0, 0x1

    goto :goto_0

    .line 1105685
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
