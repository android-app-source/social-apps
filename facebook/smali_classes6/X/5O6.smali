.class public LX/5O6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 907947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907948
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/5O6;->a:Landroid/graphics/Paint;

    .line 907949
    iget-object v0, p0, LX/5O6;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 907950
    iget-object v0, p0, LX/5O6;->a:Landroid/graphics/Paint;

    const v1, 0x7f0a059a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 907951
    iget-object v0, p0, LX/5O6;->a:Landroid/graphics/Paint;

    const v1, 0x7f0b0033

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 907952
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 907953
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/5O6;->a(Landroid/graphics/Canvas;F)V

    .line 907954
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;F)V
    .locals 1

    .prologue
    .line 907955
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/5O6;->a(Landroid/graphics/Canvas;FI)V

    .line 907956
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;FI)V
    .locals 6

    .prologue
    .line 907957
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 907958
    int-to-float v1, p3

    sub-int/2addr v0, p3

    int-to-float v3, v0

    iget-object v5, p0, LX/5O6;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, p2

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 907959
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 907960
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/5O6;->b(Landroid/graphics/Canvas;F)V

    .line 907961
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;F)V
    .locals 6

    .prologue
    .line 907962
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    .line 907963
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    .line 907964
    const/4 v1, 0x0

    int-to-float v2, v0

    iget-object v4, p0, LX/5O6;->a:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    sub-float/2addr v2, v4

    sub-float/2addr v2, p2

    int-to-float v3, v3

    int-to-float v0, v0

    iget-object v4, p0, LX/5O6;->a:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    sub-float/2addr v0, v4

    sub-float v4, v0, p2

    iget-object v5, p0, LX/5O6;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 907965
    return-void
.end method
