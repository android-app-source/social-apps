.class public LX/5Mp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/196;


# instance fields
.field public final a:LX/19D;

.field public final b:LX/5Mo;

.field private final c:LX/19A;

.field public final d:Z

.field public e:LX/0Jb;

.field public f:Z


# direct methods
.method public constructor <init>(LX/197;LX/19A;LX/03R;)V
    .locals 2
    .param p3    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 905719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905720
    new-instance v0, LX/5Mo;

    invoke-direct {v0}, LX/5Mo;-><init>()V

    iput-object v0, p0, LX/5Mp;->b:LX/5Mo;

    .line 905721
    iput-boolean v1, p0, LX/5Mp;->f:Z

    .line 905722
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/197;->a(Ljava/lang/Boolean;)LX/19D;

    move-result-object v0

    iput-object v0, p0, LX/5Mp;->a:LX/19D;

    .line 905723
    iget-object v0, p0, LX/5Mp;->a:LX/19D;

    .line 905724
    iput-object p0, v0, LX/19D;->f:LX/196;

    .line 905725
    iput-object p2, p0, LX/5Mp;->c:LX/19A;

    .line 905726
    invoke-virtual {p3, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/5Mp;->d:Z

    .line 905727
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 13

    .prologue
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    .line 905728
    iget-boolean v0, p0, LX/5Mp;->f:Z

    if-nez v0, :cond_0

    .line 905729
    :goto_0
    return-void

    .line 905730
    :cond_0
    iget-object v0, p0, LX/5Mp;->c:LX/19A;

    invoke-virtual {v0}, LX/19A;->a()I

    move-result v2

    .line 905731
    const/4 v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 905732
    int-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 905733
    iget-object v0, p0, LX/5Mp;->b:LX/5Mo;

    int-to-float v1, v3

    .line 905734
    iget v4, v0, LX/5Mo;->a:F

    add-float/2addr v4, v1

    iput v4, v0, LX/5Mo;->a:F

    .line 905735
    iget-object v4, p0, LX/5Mp;->b:LX/5Mo;

    const/4 v0, 0x4

    if-lt v3, v0, :cond_1

    int-to-double v0, v3

    mul-double/2addr v0, v8

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v8

    .line 905736
    :goto_1
    iget v10, v4, LX/5Mo;->b:F

    float-to-double v10, v10

    add-double/2addr v10, v0

    double-to-float v10, v10

    iput v10, v4, LX/5Mo;->b:F

    .line 905737
    iget-object v0, p0, LX/5Mp;->b:LX/5Mo;

    add-int/lit8 v1, v3, 0x1

    mul-int/2addr v1, v2

    int-to-float v1, v1

    .line 905738
    iget v2, v0, LX/5Mo;->c:F

    add-float/2addr v2, v1

    iput v2, v0, LX/5Mo;->c:F

    .line 905739
    goto :goto_0

    .line 905740
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method
