.class public LX/6Qc;
.super Landroid/database/ContentObserver;
.source ""


# instance fields
.field private a:LX/2v6;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:J

.field public i:LX/162;

.field public j:Z


# direct methods
.method public constructor <init>(LX/2v6;Landroid/os/Handler;Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;)V
    .locals 2

    .prologue
    .line 1087749
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1087750
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Qc;->j:Z

    .line 1087751
    iput-object p1, p0, LX/6Qc;->a:LX/2v6;

    .line 1087752
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->b:Ljava/lang/String;

    .line 1087753
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->packageName:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->c:Ljava/lang/String;

    .line 1087754
    iget-wide v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->updateId:J

    iput-wide v0, p0, LX/6Qc;->h:J

    .line 1087755
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appName:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->d:Ljava/lang/String;

    .line 1087756
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appId:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->g:Ljava/lang/String;

    .line 1087757
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appLaunchUrl:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->e:Ljava/lang/String;

    .line 1087758
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->iconUrl:Ljava/lang/String;

    iput-object v0, p0, LX/6Qc;->f:Ljava/lang/String;

    .line 1087759
    iget-object v0, p3, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->trackingCodes:LX/162;

    iput-object v0, p0, LX/6Qc;->i:LX/162;

    .line 1087760
    return-void
.end method

.method public constructor <init>(LX/2v6;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/162;)V
    .locals 1

    .prologue
    .line 1087737
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1087738
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Qc;->j:Z

    .line 1087739
    iput-object p1, p0, LX/6Qc;->a:LX/2v6;

    .line 1087740
    iput-object p3, p0, LX/6Qc;->b:Ljava/lang/String;

    .line 1087741
    iput-object p4, p0, LX/6Qc;->c:Ljava/lang/String;

    .line 1087742
    iput-wide p9, p0, LX/6Qc;->h:J

    .line 1087743
    iput-object p5, p0, LX/6Qc;->d:Ljava/lang/String;

    .line 1087744
    iput-object p8, p0, LX/6Qc;->g:Ljava/lang/String;

    .line 1087745
    iput-object p6, p0, LX/6Qc;->e:Ljava/lang/String;

    .line 1087746
    iput-object p7, p0, LX/6Qc;->f:Ljava/lang/String;

    .line 1087747
    iput-object p11, p0, LX/6Qc;->i:LX/162;

    .line 1087748
    return-void
.end method

.method private h()V
    .locals 7

    .prologue
    .line 1087735
    iget-object v1, p0, LX/6Qc;->a:LX/2v6;

    iget-wide v2, p0, LX/6Qc;->h:J

    iget-object v4, p0, LX/6Qc;->b:Ljava/lang/String;

    iget-object v5, p0, LX/6Qc;->c:Ljava/lang/String;

    move-object v6, p0

    invoke-virtual/range {v1 .. v6}, LX/2v6;->a(JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V

    .line 1087736
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 1087733
    iput-boolean p1, p0, LX/6Qc;->j:Z

    .line 1087734
    return-void
.end method

.method public final d()Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;
    .locals 4

    .prologue
    .line 1087723
    new-instance v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    invoke-direct {v0}, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;-><init>()V

    .line 1087724
    iget-object v1, p0, LX/6Qc;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appId:Ljava/lang/String;

    .line 1087725
    iget-object v1, p0, LX/6Qc;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appLaunchUrl:Ljava/lang/String;

    .line 1087726
    iget-object v1, p0, LX/6Qc;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->appName:Ljava/lang/String;

    .line 1087727
    iget-object v1, p0, LX/6Qc;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->iconUrl:Ljava/lang/String;

    .line 1087728
    iget-object v1, p0, LX/6Qc;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->packageName:Ljava/lang/String;

    .line 1087729
    iget-object v1, p0, LX/6Qc;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    .line 1087730
    iget-object v1, p0, LX/6Qc;->i:LX/162;

    iput-object v1, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->trackingCodes:LX/162;

    .line 1087731
    iget-wide v2, p0, LX/6Qc;->h:J

    iput-wide v2, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->updateId:J

    .line 1087732
    return-object v0
.end method

.method public final deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 1087717
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    .line 1087722
    iget-wide v0, p0, LX/6Qc;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onChange(Z)V
    .locals 0

    .prologue
    .line 1087720
    invoke-direct {p0}, LX/6Qc;->h()V

    .line 1087721
    return-void
.end method

.method public final onChange(ZLandroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1087718
    invoke-direct {p0}, LX/6Qc;->h()V

    .line 1087719
    return-void
.end method
