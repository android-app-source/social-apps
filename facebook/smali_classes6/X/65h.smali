.class public final LX/65h;
.super LX/65g;
.source ""


# instance fields
.field public final synthetic a:LX/65i;


# direct methods
.method public constructor <init>(LX/65i;)V
    .locals 0

    .prologue
    .line 1048271
    iput-object p1, p0, LX/65h;->a:LX/65i;

    invoke-direct {p0}, LX/65g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 1048272
    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    .line 1048273
    if-eqz p1, :cond_0

    .line 1048274
    invoke-virtual {v0, p1}, Ljava/net/SocketTimeoutException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1048275
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1048276
    iget-object v0, p0, LX/65h;->a:LX/65i;

    sget-object v1, LX/65X;->CANCEL:LX/65X;

    invoke-virtual {v0, v1}, LX/65i;->b(LX/65X;)V

    .line 1048277
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1048278
    invoke-virtual {p0}, LX/65g;->bI_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/65h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048279
    :cond_0
    return-void
.end method
