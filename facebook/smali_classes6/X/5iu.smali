.class public final LX/5iu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 985122
    const/4 v7, 0x0

    .line 985123
    const/4 v6, 0x0

    .line 985124
    const/4 v5, 0x0

    .line 985125
    const/4 v4, 0x0

    .line 985126
    const/4 v1, 0x0

    .line 985127
    const-wide/16 v2, 0x0

    .line 985128
    const/4 v0, 0x0

    .line 985129
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_1

    .line 985130
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 985131
    const/4 v0, 0x0

    .line 985132
    :goto_0
    return v0

    .line 985133
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 985134
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 985135
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 985136
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 985137
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 985138
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 985139
    invoke-static {p0, p1}, LX/5iq;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 985140
    :cond_2
    const-string v9, "image_landscape_size"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 985141
    invoke-static {p0, p1}, LX/5ip;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 985142
    :cond_3
    const-string v9, "image_portrait_size"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 985143
    invoke-static {p0, p1}, LX/5ir;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 985144
    :cond_4
    const-string v9, "landscape_anchoring"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 985145
    invoke-static {p0, p1}, LX/5is;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 985146
    :cond_5
    const-string v9, "portrait_anchoring"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 985147
    invoke-static {p0, p1}, LX/5it;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 985148
    :cond_6
    const-string v9, "rotation_degree"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 985149
    const/4 v0, 0x1

    .line 985150
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    goto :goto_1

    .line 985151
    :cond_7
    const/4 v8, 0x6

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 985152
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 985153
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 985154
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 985155
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 985156
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v1}, LX/186;->b(II)V

    .line 985157
    if-eqz v0, :cond_8

    .line 985158
    const/4 v1, 0x5

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985159
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 985160
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985161
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985162
    if-eqz v0, :cond_0

    .line 985163
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985164
    invoke-static {p0, v0, p2}, LX/5iq;->a(LX/15i;ILX/0nX;)V

    .line 985165
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985166
    if-eqz v0, :cond_1

    .line 985167
    const-string v1, "image_landscape_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985168
    invoke-static {p0, v0, p2}, LX/5ip;->a(LX/15i;ILX/0nX;)V

    .line 985169
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985170
    if-eqz v0, :cond_2

    .line 985171
    const-string v1, "image_portrait_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985172
    invoke-static {p0, v0, p2}, LX/5ir;->a(LX/15i;ILX/0nX;)V

    .line 985173
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985174
    if-eqz v0, :cond_3

    .line 985175
    const-string v1, "landscape_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985176
    invoke-static {p0, v0, p2}, LX/5is;->a(LX/15i;ILX/0nX;)V

    .line 985177
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985178
    if-eqz v0, :cond_4

    .line 985179
    const-string v1, "portrait_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985180
    invoke-static {p0, v0, p2}, LX/5it;->a(LX/15i;ILX/0nX;)V

    .line 985181
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985182
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_5

    .line 985183
    const-string v2, "rotation_degree"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985184
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985185
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985186
    return-void
.end method
