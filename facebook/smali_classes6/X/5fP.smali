.class public final LX/5fP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$OnZoomChangeListener;


# instance fields
.field public final synthetic a:LX/5fQ;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5fQ;)V
    .locals 2

    .prologue
    .line 971230
    iput-object p1, p0, LX/5fP;->a:LX/5fQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 971231
    invoke-virtual {p1}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971232
    new-instance v0, LX/5fN;

    const-string v1, "Failed to create a zoom controller."

    invoke-direct {v0, p1, v1}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971233
    :cond_0
    iget-object v0, p1, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0}, LX/5fS;->q()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/5fP;->b:Ljava/util/List;

    .line 971234
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 971235
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    invoke-virtual {v0}, LX/5fQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 971236
    new-instance v0, LX/5fN;

    iget-object v1, p0, LX/5fP;->a:LX/5fQ;

    const-string v2, "Zoom controller failed to set the zoom level."

    invoke-direct {v0, v1, v2}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    throw v0

    .line 971237
    :cond_0
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fP;->a:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971238
    invoke-virtual {v0}, LX/5fS;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 971239
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->startSmoothZoom(I)V

    .line 971240
    :cond_1
    :goto_0
    return-void

    .line 971241
    :cond_2
    invoke-virtual {v0, p1}, LX/5fS;->b(I)V

    .line 971242
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->t:LX/ALT;

    if-eqz v0, :cond_1

    .line 971243
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v1, v0, LX/5fQ;->t:LX/ALT;

    iget-object v0, p0, LX/5fP;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p0, LX/5fP;->b:Ljava/util/List;

    iget-object v3, p0, LX/5fP;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 971244
    invoke-virtual {v1, v2, v0}, LX/ALT;->a(II)V

    goto :goto_0
.end method

.method public final onZoomChange(IZLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 971245
    if-eqz p2, :cond_0

    .line 971246
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fP;->a:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5fS;->b(I)V

    .line 971247
    :cond_0
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->t:LX/ALT;

    if-eqz v0, :cond_1

    .line 971248
    iget-object v0, p0, LX/5fP;->a:LX/5fQ;

    iget-object v1, v0, LX/5fQ;->t:LX/ALT;

    iget-object v0, p0, LX/5fP;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p0, LX/5fP;->b:Ljava/util/List;

    iget-object v3, p0, LX/5fP;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/ALT;->a(II)V

    .line 971249
    :cond_1
    return-void
.end method
