.class public final LX/5Z0;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 943936
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel;

    const v0, -0x71a70ca4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "BusinessNameSearchQuery"

    const-string v6, "ead39d7f455412ebd2f0fd65a4f3c471"

    const-string v7, "entities_named"

    const-string v8, "10155163126131729"

    const-string v9, "10155259086326729"

    .line 943937
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 943938
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 943939
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 943940
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 943941
    sparse-switch v0, :sswitch_data_0

    .line 943942
    :goto_0
    return-object p1

    .line 943943
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 943944
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 943945
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 943946
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 943947
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 943948
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 943949
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5c793396 -> :sswitch_3
        -0x5174b82c -> :sswitch_0
        -0x2db065ce -> :sswitch_1
        -0x132889c -> :sswitch_6
        0x2f1911b0 -> :sswitch_4
        0x3349e8c0 -> :sswitch_5
        0x69308369 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 943950
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 943951
    :goto_1
    return v0

    .line 943952
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 943953
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
