.class public final LX/5aI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 954315
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 954316
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 954317
    :goto_0
    return v1

    .line 954318
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 954319
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 954320
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 954321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 954322
    const-string v9, "rank"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 954323
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 954324
    :cond_1
    const-string v9, "score"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 954325
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 954326
    :cond_2
    const-string v9, "score_int"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 954327
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 954328
    :cond_3
    const-string v9, "user"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 954329
    invoke-static {p0, p1}, LX/5aH;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 954330
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 954331
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 954332
    if-eqz v3, :cond_6

    .line 954333
    invoke-virtual {p1, v1, v7, v1}, LX/186;->a(III)V

    .line 954334
    :cond_6
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 954335
    if-eqz v0, :cond_7

    .line 954336
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 954337
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 954338
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 954339
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954340
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 954341
    if-eqz v0, :cond_0

    .line 954342
    const-string v1, "rank"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954343
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 954344
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954345
    if-eqz v0, :cond_1

    .line 954346
    const-string v1, "score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954347
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954348
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 954349
    if-eqz v0, :cond_2

    .line 954350
    const-string v1, "score_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954351
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 954352
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954353
    if-eqz v0, :cond_3

    .line 954354
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954355
    invoke-static {p0, v0, p2}, LX/5aH;->a(LX/15i;ILX/0nX;)V

    .line 954356
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954357
    return-void
.end method
