.class public final enum LX/6Ob;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Ob;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Ob;

.field public static final enum FAIL:LX/6Ob;

.field public static final enum RETRY:LX/6Ob;

.field public static final enum SUCCESS:LX/6Ob;

.field public static final enum UNKNOWN:LX/6Ob;


# instance fields
.field public final statusCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085300
    new-instance v0, LX/6Ob;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2, v2}, LX/6Ob;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Ob;->SUCCESS:LX/6Ob;

    .line 1085301
    new-instance v0, LX/6Ob;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3, v3}, LX/6Ob;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Ob;->FAIL:LX/6Ob;

    .line 1085302
    new-instance v0, LX/6Ob;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v4, v4}, LX/6Ob;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Ob;->RETRY:LX/6Ob;

    .line 1085303
    new-instance v0, LX/6Ob;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5, v5}, LX/6Ob;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6Ob;->UNKNOWN:LX/6Ob;

    .line 1085304
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Ob;

    sget-object v1, LX/6Ob;->SUCCESS:LX/6Ob;

    aput-object v1, v0, v2

    sget-object v1, LX/6Ob;->FAIL:LX/6Ob;

    aput-object v1, v0, v3

    sget-object v1, LX/6Ob;->RETRY:LX/6Ob;

    aput-object v1, v0, v4

    sget-object v1, LX/6Ob;->UNKNOWN:LX/6Ob;

    aput-object v1, v0, v5

    sput-object v0, LX/6Ob;->$VALUES:[LX/6Ob;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1085305
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1085306
    iput p3, p0, LX/6Ob;->statusCode:I

    .line 1085307
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Ob;
    .locals 1

    .prologue
    .line 1085308
    const-class v0, LX/6Ob;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Ob;

    return-object v0
.end method

.method public static values()[LX/6Ob;
    .locals 1

    .prologue
    .line 1085309
    sget-object v0, LX/6Ob;->$VALUES:[LX/6Ob;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Ob;

    return-object v0
.end method
