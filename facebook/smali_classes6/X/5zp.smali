.class public final LX/5zp;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Landroid/graphics/Canvas;

.field public final synthetic b:Landroid/graphics/Rect;

.field public final synthetic c:LX/47d;

.field public final synthetic d:Z

.field public final synthetic e:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic f:Lcom/facebook/ui/media/attachments/MediaResourceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResourceHelper;Landroid/graphics/Canvas;Landroid/graphics/Rect;LX/47d;ZLcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1035865
    iput-object p1, p0, LX/5zp;->f:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    iput-object p2, p0, LX/5zp;->a:Landroid/graphics/Canvas;

    iput-object p3, p0, LX/5zp;->b:Landroid/graphics/Rect;

    iput-object p4, p0, LX/5zp;->c:LX/47d;

    iput-boolean p5, p0, LX/5zp;->d:Z

    iput-object p6, p0, LX/5zp;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1035866
    iget-object v1, p0, LX/5zp;->a:Landroid/graphics/Canvas;

    iget-object v3, p0, LX/5zp;->b:Landroid/graphics/Rect;

    iget-object v4, p0, LX/5zp;->c:LX/47d;

    iget-boolean v5, p0, LX/5zp;->d:Z

    move-object v2, p1

    .line 1035867
    const/high16 v0, 0x40000000    # 2.0f

    .line 1035868
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 1035869
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 1035870
    invoke-static {v4}, LX/47e;->a(LX/47d;)I

    move-result v6

    .line 1035871
    if-eqz v5, :cond_0

    neg-int v6, v6

    int-to-float v6, v6

    :goto_0
    int-to-float p1, v7

    div-float/2addr p1, v0

    int-to-float v7, v7

    div-float/2addr v7, v0

    invoke-virtual {v1, v6, p1, v7}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1035872
    const/4 v6, 0x0

    new-instance v7, Landroid/graphics/Paint;

    const/4 p1, 0x2

    invoke-direct {v7, p1}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v1, v2, v6, v3, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1035873
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 1035874
    iget-object v0, p0, LX/5zp;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x48b4d8e9

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1035875
    return-void

    .line 1035876
    :cond_0
    int-to-float v6, v6

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1035877
    iget-object v0, p0, LX/5zp;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 1035878
    return-void
.end method
