.class public final LX/5xF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1027745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLTimelineSectionsConnection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1027746
    if-nez p1, :cond_0

    .line 1027747
    const/4 v0, 0x0

    .line 1027748
    :goto_0
    return-object v0

    .line 1027749
    :cond_0
    new-instance v3, LX/4ZF;

    invoke-direct {v3}, LX/4ZF;-><init>()V

    .line 1027750
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineSectionBasicFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    .line 1027751
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1027752
    :goto_2
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineSectionBasicFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1027753
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineSectionBasicFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5xL;

    .line 1027754
    if-nez v0, :cond_6

    .line 1027755
    const/4 v5, 0x0

    .line 1027756
    :goto_5
    move-object v0, v5

    .line 1027757
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1027758
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1027759
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1027760
    goto :goto_1

    .line 1027761
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1027762
    goto :goto_3

    .line 1027763
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1027764
    goto :goto_4

    .line 1027765
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1027766
    iput-object v0, v3, LX/4ZF;->b:LX/0Px;

    .line 1027767
    :cond_5
    invoke-virtual {v3}, LX/4ZF;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    goto :goto_0

    .line 1027768
    :cond_6
    new-instance v5, LX/4ZC;

    invoke-direct {v5}, LX/4ZC;-><init>()V

    .line 1027769
    invoke-interface {v0}, LX/5xL;->b()Ljava/lang/String;

    move-result-object v6

    .line 1027770
    iput-object v6, v5, LX/4ZC;->b:Ljava/lang/String;

    .line 1027771
    invoke-interface {v0}, LX/5xL;->c()Ljava/lang/String;

    move-result-object v6

    .line 1027772
    iput-object v6, v5, LX/4ZC;->c:Ljava/lang/String;

    .line 1027773
    invoke-interface {v0}, LX/5xL;->e()I

    move-result v6

    .line 1027774
    iput v6, v5, LX/4ZC;->f:I

    .line 1027775
    invoke-virtual {v5}, LX/4ZC;->a()Lcom/facebook/graphql/model/GraphQLTimelineSection;

    move-result-object v5

    goto :goto_5
.end method

.method public static a(Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;)Lcom/facebook/graphql/model/GraphQLUser;
    .locals 13
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1027776
    if-nez p0, :cond_0

    .line 1027777
    const/4 v0, 0x0

    .line 1027778
    :goto_0
    return-object v0

    .line 1027779
    :cond_0
    new-instance v0, LX/33O;

    invoke-direct {v0}, LX/33O;-><init>()V

    .line 1027780
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionsConnectionFieldsModel;

    move-result-object v1

    .line 1027781
    if-nez v1, :cond_1

    .line 1027782
    const/4 v2, 0x0

    .line 1027783
    :goto_1
    move-object v1, v2

    .line 1027784
    iput-object v1, v0, LX/33O;->L:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 1027785
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v2, v1}, LX/5xF;->a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v1

    .line 1027786
    iput-object v1, v0, LX/33O;->bz:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 1027787
    invoke-virtual {v0}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    goto :goto_0

    .line 1027788
    :cond_1
    new-instance v4, LX/4ZF;

    invoke-direct {v4}, LX/4ZF;-><init>()V

    .line 1027789
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1027790
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1027791
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 1027792
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;

    .line 1027793
    if-nez v2, :cond_4

    .line 1027794
    const/4 v6, 0x0

    .line 1027795
    :goto_3
    move-object v2, v6

    .line 1027796
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1027797
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1027798
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1027799
    iput-object v2, v4, LX/4ZF;->b:LX/0Px;

    .line 1027800
    :cond_3
    invoke-virtual {v4}, LX/4ZF;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v2

    goto :goto_1

    .line 1027801
    :cond_4
    new-instance v6, LX/4ZC;

    invoke-direct {v6}, LX/4ZC;-><init>()V

    .line 1027802
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 1027803
    iput-object v7, v6, LX/4ZC;->b:Ljava/lang/String;

    .line 1027804
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1027805
    iput-object v7, v6, LX/4ZC;->c:Ljava/lang/String;

    .line 1027806
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->d()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v7

    .line 1027807
    if-nez v7, :cond_5

    .line 1027808
    const/4 v8, 0x0

    .line 1027809
    :goto_4
    move-object v7, v8

    .line 1027810
    iput-object v7, v6, LX/4ZC;->d:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 1027811
    invoke-virtual {v6}, LX/4ZC;->a()Lcom/facebook/graphql/model/GraphQLTimelineSection;

    move-result-object v6

    goto :goto_3

    .line 1027812
    :cond_5
    new-instance v10, LX/4ZD;

    invoke-direct {v10}, LX/4ZD;-><init>()V

    .line 1027813
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 1027814
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1027815
    const/4 v8, 0x0

    move v9, v8

    :goto_5
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_6

    .line 1027816
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel$EdgesModel;

    .line 1027817
    if-nez v8, :cond_8

    .line 1027818
    const/4 v12, 0x0

    .line 1027819
    :goto_6
    move-object v8, v12

    .line 1027820
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1027821
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_5

    .line 1027822
    :cond_6
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1027823
    iput-object v8, v10, LX/4ZD;->b:LX/0Px;

    .line 1027824
    :cond_7
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1027825
    if-nez v8, :cond_9

    .line 1027826
    const/4 v11, 0x0

    .line 1027827
    :goto_7
    move-object v8, v11

    .line 1027828
    iput-object v8, v10, LX/4ZD;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1027829
    invoke-virtual {v10}, LX/4ZD;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v8

    goto :goto_4

    .line 1027830
    :cond_8
    new-instance v12, LX/4ZE;

    invoke-direct {v12}, LX/4ZE;-><init>()V

    .line 1027831
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 1027832
    iput-object v2, v12, LX/4ZE;->c:Lcom/facebook/graphql/model/FeedUnit;

    .line 1027833
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1027834
    iput-object v2, v12, LX/4ZE;->d:Ljava/lang/String;

    .line 1027835
    invoke-virtual {v12}, LX/4ZE;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    move-result-object v12

    goto :goto_6

    .line 1027836
    :cond_9
    new-instance v11, LX/17L;

    invoke-direct {v11}, LX/17L;-><init>()V

    .line 1027837
    const/4 v12, 0x0

    invoke-virtual {v9, v8, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    .line 1027838
    iput-object v12, v11, LX/17L;->c:Ljava/lang/String;

    .line 1027839
    const/4 v12, 0x1

    invoke-virtual {v9, v8, v12}, LX/15i;->h(II)Z

    move-result v12

    .line 1027840
    iput-boolean v12, v11, LX/17L;->d:Z

    .line 1027841
    invoke-virtual {v11}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v11

    goto :goto_7
.end method
