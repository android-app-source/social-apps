.class public abstract LX/5SN;
.super Lcom/facebook/jni/HybridClassBase;
.source ""


# instance fields
.field private final mClassName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 917965
    invoke-direct {p0}, Lcom/facebook/jni/HybridClassBase;-><init>()V

    .line 917966
    iput-object p1, p0, LX/5SN;->mClassName:Ljava/lang/String;

    .line 917967
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 8

    .prologue
    .line 917968
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 917969
    invoke-super {p0}, Lcom/facebook/jni/HybridClassBase;->finalize()V

    .line 917970
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 917971
    const-wide v2, 0xb2d05e00L

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 917972
    const-string v2, "HybridClassBaseWithTimeoutDetection"

    const-string v3, "Finalize call of %d ms seconds detected in %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/32 v6, 0xf4240

    div-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v1, p0, LX/5SN;->mClassName:Ljava/lang/String;

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 917973
    :cond_0
    return-void
.end method
