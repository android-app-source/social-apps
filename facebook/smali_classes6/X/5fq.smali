.class public LX/5fq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field public final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(ZZLandroid/widget/CompoundButton$OnCheckedChangeListener;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 972248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972249
    iput-boolean p1, p0, LX/5fq;->a:Z

    .line 972250
    iput-boolean p2, p0, LX/5fq;->b:Z

    .line 972251
    iput-object p3, p0, LX/5fq;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 972252
    iput-object p4, p0, LX/5fq;->d:Landroid/view/View$OnClickListener;

    .line 972253
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 972254
    instance-of v1, p1, LX/5fq;

    if-nez v1, :cond_1

    .line 972255
    :cond_0
    :goto_0
    return v0

    .line 972256
    :cond_1
    check-cast p1, LX/5fq;

    .line 972257
    iget-boolean v1, p0, LX/5fq;->a:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, LX/5fq;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/5fq;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, LX/5fq;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/5fq;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v2, p1, LX/5fq;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/5fq;->d:Landroid/view/View$OnClickListener;

    iget-object v2, p1, LX/5fq;->d:Landroid/view/View$OnClickListener;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 972258
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/5fq;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/5fq;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/5fq;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/5fq;->d:Landroid/view/View$OnClickListener;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
