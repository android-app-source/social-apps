.class public final LX/535;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0vL",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826634
    if-nez p1, :cond_0

    .line 826635
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "iterable must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 826636
    :cond_0
    iput-object p1, p0, LX/535;->a:Ljava/lang/Iterable;

    .line 826637
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 826638
    check-cast p1, LX/0zZ;

    .line 826639
    iget-object v0, p0, LX/535;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 826640
    new-instance v1, LX/534;

    invoke-direct {v1, p1, v0}, LX/534;-><init>(LX/0zZ;Ljava/util/Iterator;)V

    invoke-virtual {p1, v1}, LX/0zZ;->a(LX/52p;)V

    .line 826641
    return-void
.end method
