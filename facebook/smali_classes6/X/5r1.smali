.class public LX/5r1;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/5r1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5r1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1010802
    new-instance v0, LX/0Zi;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5r1;->a:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1010803
    invoke-direct {p0}, LX/5r0;-><init>()V

    .line 1010804
    return-void
.end method

.method public static a(IIIII)LX/5r1;
    .locals 6

    .prologue
    .line 1010805
    sget-object v0, LX/5r1;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r1;

    .line 1010806
    if-nez v0, :cond_0

    .line 1010807
    new-instance v0, LX/5r1;

    invoke-direct {v0}, LX/5r1;-><init>()V

    :cond_0
    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 1010808
    invoke-direct/range {v0 .. v5}, LX/5r1;->b(IIIII)V

    .line 1010809
    return-object v0
.end method

.method private b(IIIII)V
    .locals 0

    .prologue
    .line 1010810
    invoke-super {p0, p1}, LX/5r0;->a(I)V

    .line 1010811
    iput p2, p0, LX/5r1;->b:I

    .line 1010812
    iput p3, p0, LX/5r1;->c:I

    .line 1010813
    iput p4, p0, LX/5r1;->d:I

    .line 1010814
    iput p5, p0, LX/5r1;->e:I

    .line 1010815
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1010816
    sget-object v0, LX/5r1;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1010817
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 4

    .prologue
    .line 1010818
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 1010819
    const-string v1, "x"

    iget v2, p0, LX/5r1;->b:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1010820
    const-string v1, "y"

    iget v2, p0, LX/5r1;->c:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1010821
    const-string v1, "width"

    iget v2, p0, LX/5r1;->d:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1010822
    const-string v1, "height"

    iget v2, p0, LX/5r1;->e:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1010823
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 1010824
    const-string v2, "layout"

    invoke-interface {v1, v2, v0}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 1010825
    const-string v0, "target"

    .line 1010826
    iget v2, p0, LX/5r0;->c:I

    move v2, v2

    .line 1010827
    invoke-interface {v1, v0, v2}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1010828
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 1010829
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2, v1}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 1010830
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1010831
    const-string v0, "topLayout"

    return-object v0
.end method
