.class public LX/6Ou;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/6Os;

.field public final c:LX/2fl;

.field public final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085833
    const-class v0, LX/6Ou;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Ou;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6Os;LX/2fl;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085835
    iput-object p1, p0, LX/6Ou;->b:LX/6Os;

    .line 1085836
    iput-object p2, p0, LX/6Ou;->c:LX/2fl;

    .line 1085837
    iput-object p3, p0, LX/6Ou;->d:LX/03V;

    .line 1085838
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ou;
    .locals 1

    .prologue
    .line 1085681
    invoke-static {p0}, LX/6Ou;->b(LX/0QB;)LX/6Ou;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;
    .locals 8
    .param p0    # Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1085801
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1085802
    if-eqz p0, :cond_1

    .line 1085803
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1085804
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    .line 1085805
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1085806
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 1085807
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1085808
    :cond_0
    invoke-static {v0}, LX/4Y9;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/4Y9;

    move-result-object v1

    new-instance v6, LX/4YA;

    invoke-direct {v6}, LX/4YA;-><init>()V

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {v7, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v7

    invoke-virtual {v7, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v7

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 1085809
    iput-object v7, v6, LX/4YA;->b:LX/0Px;

    .line 1085810
    move-object v6, v6

    .line 1085811
    invoke-virtual {v6}, LX/4YA;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v6

    .line 1085812
    iput-object v6, v1, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 1085813
    move-object v1, v1

    .line 1085814
    invoke-virtual {v1}, LX/4Y9;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v1

    move-object v0, v1

    .line 1085815
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1085816
    const/4 v0, 0x1

    goto :goto_1

    .line 1085817
    :cond_1
    if-nez v1, :cond_2

    .line 1085818
    new-instance v0, LX/4Y9;

    invoke-direct {v0}, LX/4Y9;-><init>()V

    .line 1085819
    iput-object p1, v0, LX/4Y9;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085820
    move-object v0, v0

    .line 1085821
    new-instance v1, LX/4YA;

    invoke-direct {v1}, LX/4YA;-><init>()V

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1085822
    iput-object v2, v1, LX/4YA;->b:LX/0Px;

    .line 1085823
    move-object v1, v1

    .line 1085824
    invoke-virtual {v1}, LX/4YA;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v1

    .line 1085825
    iput-object v1, v0, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 1085826
    move-object v0, v0

    .line 1085827
    invoke-virtual {v0}, LX/4Y9;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v0

    move-object v0, v0

    .line 1085828
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1085829
    :cond_2
    new-instance v0, LX/4YB;

    invoke-direct {v0}, LX/4YB;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1085830
    iput-object v1, v0, LX/4YB;->b:LX/0Px;

    .line 1085831
    move-object v0, v0

    .line 1085832
    invoke-virtual {v0}, LX/4YB;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuestionOption;Z)Lcom/facebook/graphql/model/GraphQLQuestionOption;
    .locals 4

    .prologue
    .line 1085772
    new-instance v0, LX/4YQ;

    invoke-direct {v0}, LX/4YQ;-><init>()V

    .line 1085773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1085774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YQ;->b:Ljava/lang/String;

    .line 1085775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4YQ;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1085776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YQ;->d:Ljava/lang/String;

    .line 1085777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/4YQ;->e:Z

    .line 1085778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4YQ;->f:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 1085779
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1085780
    move-object v1, v0

    .line 1085781
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1085782
    :goto_0
    iput-boolean v0, v1, LX/4YQ;->e:Z

    .line 1085783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    .line 1085784
    new-instance v2, LX/4YS;

    invoke-direct {v2}, LX/4YS;-><init>()V

    .line 1085785
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1085786
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v3

    iput v3, v2, LX/4YS;->b:I

    .line 1085787
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->j()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/4YS;->c:LX/0Px;

    .line 1085788
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    iput-object v3, v2, LX/4YS;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1085789
    invoke-static {v2, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1085790
    move-object v0, v2

    .line 1085791
    if-eqz p1, :cond_1

    .line 1085792
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 1085793
    iput v2, v0, LX/4YS;->b:I

    .line 1085794
    :goto_1
    invoke-virtual {v0}, LX/4YS;->a()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    .line 1085795
    iput-object v0, v1, LX/4YQ;->f:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 1085796
    invoke-virtual {v1}, LX/4YQ;->a()Lcom/facebook/graphql/model/GraphQLQuestionOption;

    move-result-object v0

    return-object v0

    .line 1085797
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1085798
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 1085799
    iput v2, v0, LX/4YS;->b:I

    .line 1085800
    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1085751
    invoke-static {p0}, LX/4YT;->a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;)LX/4YT;

    move-result-object v0

    .line 1085752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;->a()LX/0Px;

    move-result-object v1

    .line 1085753
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1085754
    new-instance v3, LX/4YQ;

    invoke-direct {v3}, LX/4YQ;-><init>()V

    .line 1085755
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 1085756
    iput-object p1, v4, LX/173;->f:Ljava/lang/String;

    .line 1085757
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 1085758
    iput-object v4, v3, LX/4YQ;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1085759
    iput-object p2, v3, LX/4YQ;->b:Ljava/lang/String;

    .line 1085760
    iput-boolean v5, v3, LX/4YQ;->e:Z

    .line 1085761
    new-instance v4, LX/4YS;

    invoke-direct {v4}, LX/4YS;-><init>()V

    .line 1085762
    iput v5, v4, LX/4YS;->b:I

    .line 1085763
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 1085764
    iput-object v5, v4, LX/4YS;->c:LX/0Px;

    .line 1085765
    invoke-virtual {v4}, LX/4YS;->a()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v4

    .line 1085766
    iput-object v4, v3, LX/4YQ;->f:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 1085767
    invoke-virtual {v3}, LX/4YQ;->a()Lcom/facebook/graphql/model/GraphQLQuestionOption;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1085768
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1085769
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1085770
    iput-object v1, v0, LX/4YT;->b:LX/0Px;

    .line 1085771
    invoke-virtual {v0}, LX/4YT;->a()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4

    .prologue
    .line 1085839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 1085840
    :goto_0
    return-object p1

    .line 1085841
    :cond_0
    invoke-static {p1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 1085842
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->disjoint(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1085843
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1085844
    iput-object p2, v1, LX/4XR;->py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1085845
    move-object v1, v1

    .line 1085846
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 1085847
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1085848
    :cond_1
    invoke-static {p0}, LX/4W8;->a(Lcom/facebook/graphql/model/GraphQLEvent;)LX/4W8;

    move-result-object v1

    .line 1085849
    iput-object p2, v1, LX/4W8;->bz:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1085850
    move-object v1, v1

    .line 1085851
    invoke-virtual {v1}, LX/4W8;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    .line 1085852
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2, v1}, LX/2fl;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLEvent;)LX/0Px;

    move-result-object v1

    .line 1085853
    iput-object v1, v0, LX/39x;->b:LX/0Px;

    .line 1085854
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4

    .prologue
    .line 1085735
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 1085736
    :goto_0
    return-object p1

    .line 1085737
    :cond_0
    invoke-static {p1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 1085738
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->disjoint(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1085739
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1085740
    iput-object p2, v1, LX/4XR;->pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1085741
    move-object v1, v1

    .line 1085742
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 1085743
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1085744
    :cond_1
    invoke-static {p0}, LX/4W8;->a(Lcom/facebook/graphql/model/GraphQLEvent;)LX/4W8;

    move-result-object v1

    .line 1085745
    iput-object p2, v1, LX/4W8;->bG:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1085746
    move-object v1, v1

    .line 1085747
    invoke-virtual {v1}, LX/4W8;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    .line 1085748
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2, v1}, LX/2fl;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLEvent;)LX/0Px;

    move-result-object v1

    .line 1085749
    iput-object v1, v0, LX/39x;->b:LX/0Px;

    .line 1085750
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3

    .prologue
    .line 1085727
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1085728
    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    invoke-static {v2, p1, p2}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    .line 1085729
    iput-object v2, v1, LX/4XR;->hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    .line 1085730
    move-object v1, v1

    .line 1085731
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 1085732
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1085733
    move-object v0, v0

    .line 1085734
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2

    .prologue
    .line 1085717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1085718
    invoke-static {v0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1085719
    :goto_0
    iput-boolean v0, v1, LX/4XR;->hd:Z

    .line 1085720
    move-object v0, v1

    .line 1085721
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1085722
    invoke-static {p0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    .line 1085723
    iput-object v0, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1085724
    move-object v0, v1

    .line 1085725
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0

    .line 1085726
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6Ou;
    .locals 4

    .prologue
    .line 1085715
    new-instance v3, LX/6Ou;

    invoke-static {p0}, LX/6Os;->b(LX/0QB;)LX/6Os;

    move-result-object v0

    check-cast v0, LX/6Os;

    invoke-static {p0}, LX/2fl;->a(LX/0QB;)LX/2fl;

    move-result-object v1

    check-cast v1, LX/2fl;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/6Ou;-><init>(LX/6Os;LX/2fl;LX/03V;)V

    .line 1085716
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;
    .locals 12

    .prologue
    .line 1085682
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1085683
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1085684
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    .line 1085685
    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1085686
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1085687
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1085688
    :cond_1
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1085689
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;->a()LX/0Px;

    move-result-object v9

    .line 1085690
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v7

    if-ne v7, v6, :cond_4

    .line 1085691
    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1085692
    iget-object v5, p0, LX/6Ou;->d:LX/03V;

    sget-object v6, LX/6Ou;->a:Ljava/lang/String;

    const-string v7, "Did not find a matching comment to remove"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085693
    :cond_2
    const/4 v5, 0x0

    .line 1085694
    :goto_2
    move-object v0, v5

    .line 1085695
    if-eqz v0, :cond_0

    .line 1085696
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1085697
    :cond_3
    new-instance v0, LX/4YB;

    invoke-direct {v0}, LX/4YB;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1085698
    iput-object v1, v0, LX/4YB;->b:LX/0Px;

    .line 1085699
    move-object v0, v0

    .line 1085700
    invoke-virtual {v0}, LX/4YB;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    return-object v0

    .line 1085701
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1085702
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    move v8, v5

    move v7, v5

    :goto_3
    if-ge v8, v11, :cond_6

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1085703
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    move v5, v6

    .line 1085704
    :goto_4
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v5

    goto :goto_3

    .line 1085705
    :cond_5
    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v5, v7

    goto :goto_4

    .line 1085706
    :cond_6
    if-nez v7, :cond_7

    .line 1085707
    iget-object v5, p0, LX/6Ou;->d:LX/03V;

    sget-object v6, LX/6Ou;->a:Ljava/lang/String;

    const-string v7, "Did not find a matching comment to remove"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085708
    :cond_7
    invoke-static {v0}, LX/4Y9;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/4Y9;

    move-result-object v5

    new-instance v6, LX/4YA;

    invoke-direct {v6}, LX/4YA;-><init>()V

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 1085709
    iput-object v7, v6, LX/4YA;->b:LX/0Px;

    .line 1085710
    move-object v6, v6

    .line 1085711
    invoke-virtual {v6}, LX/4YA;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v6

    .line 1085712
    iput-object v6, v5, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 1085713
    move-object v5, v5

    .line 1085714
    invoke-virtual {v5}, LX/4Y9;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v5

    goto :goto_2
.end method
