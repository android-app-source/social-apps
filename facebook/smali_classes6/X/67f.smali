.class public final LX/67f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/67h;

.field private b:I


# direct methods
.method public constructor <init>(LX/67h;)V
    .locals 1

    .prologue
    .line 1053286
    iput-object p1, p0, LX/67f;->a:LX/67h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1053287
    const/4 v0, 0x0

    iput v0, p0, LX/67f;->b:I

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 1053288
    iget v0, p0, LX/67f;->b:I

    iget-object v1, p0, LX/67f;->a:LX/67h;

    iget v1, v1, LX/67h;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1053289
    iget-object v0, p0, LX/67f;->a:LX/67h;

    iget-object v0, v0, LX/67h;->a:[LX/682;

    iget v1, p0, LX/67f;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/67f;->b:I

    aget-object v0, v0, v1

    .line 1053290
    iget-object v1, v0, LX/682;->d:LX/67m;

    move-object v0, v1

    .line 1053291
    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 1053292
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
