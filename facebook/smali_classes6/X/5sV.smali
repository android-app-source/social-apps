.class public LX/5sV;
.super Landroid/view/animation/Animation;
.source ""

# interfaces
.implements LX/5sL;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:F

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 1012879
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1012880
    iput-object p1, p0, LX/5sV;->a:Landroid/view/View;

    .line 1012881
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    iput v0, p0, LX/5sV;->b:F

    .line 1012882
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    iput v0, p0, LX/5sV;->c:F

    .line 1012883
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, LX/5sV;->f:I

    .line 1012884
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, LX/5sV;->g:I

    .line 1012885
    int-to-float v0, p2

    iget v1, p0, LX/5sV;->b:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/5sV;->d:F

    .line 1012886
    int-to-float v0, p3

    iget v1, p0, LX/5sV;->c:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/5sV;->e:F

    .line 1012887
    iget v0, p0, LX/5sV;->f:I

    sub-int v0, p4, v0

    iput v0, p0, LX/5sV;->h:I

    .line 1012888
    iget v0, p0, LX/5sV;->g:I

    sub-int v0, p5, v0

    iput v0, p0, LX/5sV;->i:I

    .line 1012889
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 7

    .prologue
    .line 1012890
    iget v0, p0, LX/5sV;->b:F

    iget v1, p0, LX/5sV;->d:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 1012891
    iget v1, p0, LX/5sV;->c:F

    iget v2, p0, LX/5sV;->e:F

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    .line 1012892
    iget v2, p0, LX/5sV;->f:I

    int-to-float v2, v2

    iget v3, p0, LX/5sV;->h:I

    int-to-float v3, v3

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    .line 1012893
    iget v3, p0, LX/5sV;->g:I

    int-to-float v3, v3

    iget v4, p0, LX/5sV;->i:I

    int-to-float v4, v4

    mul-float/2addr v4, p1

    add-float/2addr v3, v4

    .line 1012894
    iget-object v4, p0, LX/5sV;->a:Landroid/view/View;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1012895
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1012896
    const/4 v0, 0x1

    return v0
.end method
