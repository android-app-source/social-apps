.class public final LX/6Fw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/bugreporter/BugReport;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/0P1;

.field public final synthetic e:LX/0Rf;

.field public final synthetic f:LX/6Fb;

.field public final synthetic g:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;Ljava/util/List;Landroid/net/Uri;Landroid/content/Context;LX/0P1;LX/0Rf;LX/6Fb;)V
    .locals 0

    .prologue
    .line 1069496
    iput-object p1, p0, LX/6Fw;->g:LX/6G2;

    iput-object p2, p0, LX/6Fw;->a:Ljava/util/List;

    iput-object p3, p0, LX/6Fw;->b:Landroid/net/Uri;

    iput-object p4, p0, LX/6Fw;->c:Landroid/content/Context;

    iput-object p5, p0, LX/6Fw;->d:LX/0P1;

    iput-object p6, p0, LX/6Fw;->e:LX/0Rf;

    iput-object p7, p0, LX/6Fw;->f:LX/6Fb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1069482
    iget-object v1, p0, LX/6Fw;->a:Ljava/util/List;

    .line 1069483
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1069484
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1069485
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1069486
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1069487
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1069488
    :cond_0
    move-object v2, v3

    .line 1069489
    move-object v1, v2

    .line 1069490
    iget-object v2, p0, LX/6Fw;->b:Landroid/net/Uri;

    .line 1069491
    const/4 v3, 0x0

    .line 1069492
    if-nez v2, :cond_1

    .line 1069493
    :cond_1
    move-object v3, v3

    .line 1069494
    move-object v2, v3

    .line 1069495
    iget-object v0, p0, LX/6Fw;->g:LX/6G2;

    iget-object v0, v0, LX/6G2;->l:LX/6Ft;

    iget-object v3, p0, LX/6Fw;->c:Landroid/content/Context;

    iget-object v4, p0, LX/6Fw;->d:LX/0P1;

    iget-object v5, p0, LX/6Fw;->e:LX/0Rf;

    iget-object v6, p0, LX/6Fw;->f:LX/6Fb;

    invoke-virtual/range {v0 .. v6}, LX/6Ft;->a(Ljava/util/List;Landroid/os/Bundle;Landroid/content/Context;LX/0P1;LX/0Rf;LX/6Fb;)LX/6FU;

    move-result-object v0

    invoke-virtual {v0}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v0

    return-object v0
.end method
