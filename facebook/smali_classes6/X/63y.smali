.class public final LX/63y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel;",
        ">;",
        "Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1ht;


# direct methods
.method public constructor <init>(LX/1ht;)V
    .locals 0

    .prologue
    .line 1044064
    iput-object p1, p0, LX/63y;->a:LX/1ht;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1044065
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1044066
    if-eqz p1, :cond_0

    .line 1044067
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1044068
    if-eqz v0, :cond_0

    .line 1044069
    const/4 v0, 0x0

    .line 1044070
    sput-boolean v0, LX/1ht;->d:Z

    .line 1044071
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1044072
    check-cast v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel;->a()Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;

    move-result-object v0

    .line 1044073
    iget-object v1, p0, LX/63y;->a:LX/1ht;

    invoke-virtual {v0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->a()I

    move-result v2

    .line 1044074
    iget-object v3, v1, LX/1ht;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0df;->w:LX/0Tn;

    int-to-long v5, v2

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 1044075
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
