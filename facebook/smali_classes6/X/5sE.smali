.class public LX/5sE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1012639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012640
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1012641
    iget-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1012642
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1012643
    iget-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 1012644
    if-ne v0, v2, :cond_0

    .line 1012645
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to increment non-existent cookie"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012646
    :cond_0
    iget-object v1, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v2, p1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 1012647
    return-void
.end method

.method public final c(J)S
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1012648
    iget-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 1012649
    if-ne v0, v2, :cond_0

    .line 1012650
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to get non-existent cookie"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012651
    :cond_0
    const v1, 0xffff

    and-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public final d(J)V
    .locals 3

    .prologue
    .line 1012652
    iget-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 1012653
    return-void
.end method

.method public final e(J)Z
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1012654
    iget-object v0, p0, LX/5sE;->a:Landroid/util/SparseIntArray;

    long-to-int v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 1012655
    if-ne v0, v2, :cond_0

    .line 1012656
    const/4 v0, 0x0

    .line 1012657
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
