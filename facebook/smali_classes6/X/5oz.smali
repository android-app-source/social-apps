.class public final LX/5oz;
.super LX/5or;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5or",
        "<",
        "Lcom/facebook/react/bridge/Callback;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1007646
    invoke-direct {p0}, LX/5or;-><init>()V

    return-void
.end method

.method private static b(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;I)Lcom/facebook/react/bridge/Callback;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1007647
    invoke-virtual {p2, p3}, Lcom/facebook/react/bridge/ReadableNativeArray;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007648
    const/4 v0, 0x0

    .line 1007649
    :goto_0
    return-object v0

    .line 1007650
    :cond_0
    invoke-virtual {p2, p3}, Lcom/facebook/react/bridge/ReadableNativeArray;->getDouble(I)D

    move-result-wide v0

    double-to-int v1, v0

    .line 1007651
    new-instance v0, LX/5p6;

    invoke-direct {v0, p0, p1, v1}, LX/5p6;-><init>(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;I)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1007652
    invoke-static {p1, p2, p3, p4}, LX/5oz;->b(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;I)Lcom/facebook/react/bridge/Callback;

    move-result-object v0

    return-object v0
.end method
