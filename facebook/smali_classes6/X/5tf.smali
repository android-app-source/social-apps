.class public final LX/5tf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1016562
    const/4 v7, 0x0

    .line 1016563
    const/4 v6, 0x0

    .line 1016564
    const/4 v3, 0x0

    .line 1016565
    const-wide/16 v4, 0x0

    .line 1016566
    const/4 v2, 0x0

    .line 1016567
    const/4 v1, 0x0

    .line 1016568
    const/4 v0, 0x0

    .line 1016569
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 1016570
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1016571
    const/4 v0, 0x0

    .line 1016572
    :goto_0
    return v0

    .line 1016573
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1016574
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1016575
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1016576
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1016577
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1016578
    const-string v9, "histogram"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1016579
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1016580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_2

    .line 1016581
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_2

    .line 1016582
    invoke-static {p0, p1}, LX/5te;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1016583
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1016584
    :cond_2
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 1016585
    goto :goto_1

    .line 1016586
    :cond_3
    const-string v9, "rating_count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1016587
    const/4 v4, 0x1

    .line 1016588
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 1016589
    :cond_4
    const-string v9, "scale"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1016590
    const/4 v1, 0x1

    .line 1016591
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 1016592
    :cond_5
    const-string v9, "value"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1016593
    const/4 v0, 0x1

    .line 1016594
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    goto :goto_1

    .line 1016595
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1016596
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1016597
    if-eqz v4, :cond_7

    .line 1016598
    const/4 v4, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1, v4, v6, v7}, LX/186;->a(III)V

    .line 1016599
    :cond_7
    if-eqz v1, :cond_8

    .line 1016600
    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v5, v4}, LX/186;->a(III)V

    .line 1016601
    :cond_8
    if-eqz v0, :cond_9

    .line 1016602
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1016603
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v11, v2

    move-wide v12, v4

    move v4, v11

    move v5, v3

    move-wide v2, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1016604
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1016605
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1016606
    if-eqz v0, :cond_1

    .line 1016607
    const-string v1, "histogram"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016608
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1016609
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1016610
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2}, LX/5te;->a(LX/15i;ILX/0nX;)V

    .line 1016611
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1016612
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1016613
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1016614
    if-eqz v0, :cond_2

    .line 1016615
    const-string v1, "rating_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016616
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1016617
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1016618
    if-eqz v0, :cond_3

    .line 1016619
    const-string v1, "scale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016620
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1016621
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1016622
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 1016623
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1016624
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1016625
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1016626
    return-void
.end method
