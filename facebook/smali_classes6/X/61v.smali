.class public LX/61v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Cw;


# instance fields
.field private final a:LX/1Cw;


# direct methods
.method public constructor <init>(LX/1Cw;)V
    .locals 0

    .prologue
    .line 1040913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040914
    iput-object p1, p0, LX/61v;->a:LX/1Cw;

    .line 1040915
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1040912
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1040910
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1040911
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1040909
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1040908
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1040907
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1040906
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1040905
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1040894
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1, p2, p3}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1040904
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1040903
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1040902
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1040901
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 1040899
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1040900
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1040897
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1040898
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1040895
    iget-object v0, p0, LX/61v;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1040896
    return-void
.end method
