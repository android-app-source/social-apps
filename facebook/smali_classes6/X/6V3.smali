.class public LX/6V3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/6V0;

.field private final c:LX/1Er;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/6V0;LX/1Er;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103374
    iput-object p1, p0, LX/6V3;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1103375
    iput-object p2, p0, LX/6V3;->b:LX/6V0;

    .line 1103376
    iput-object p3, p0, LX/6V3;->c:LX/1Er;

    .line 1103377
    return-void
.end method

.method public static b(LX/6V3;Landroid/view/View;)Ljava/io/File;
    .locals 4

    .prologue
    .line 1103378
    iget-object v0, p0, LX/6V3;->c:LX/1Er;

    const-string v1, "view_hierarchy"

    const-string v2, ".json"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 1103379
    iget-object v1, p0, LX/6V3;->b:LX/6V0;

    sget-object v2, LX/6Uz;->ALL:LX/6Uz;

    sget-object v3, LX/6Uy;->PRETTY:LX/6Uy;

    .line 1103380
    invoke-virtual {v1, p1, v2}, LX/6V0;->a(Landroid/view/View;LX/6Uz;)Landroid/os/Bundle;

    move-result-object p0

    .line 1103381
    invoke-virtual {v1, v0, v3, p0}, LX/6V0;->a(Ljava/io/File;LX/6Uy;Landroid/os/Bundle;)Z

    .line 1103382
    return-object v0
.end method
