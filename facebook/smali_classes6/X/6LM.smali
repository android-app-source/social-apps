.class public final enum LX/6LM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6LM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6LM;

.field public static final enum THREADLIST:LX/6LM;

.field public static final enum THREADLIST_CHAT_HEADS:LX/6LM;

.field public static final enum THREADVIEW:LX/6LM;

.field public static final enum THREADVIEW_CHAT_HEADS:LX/6LM;

.field public static final enum UNKNOWN:LX/6LM;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1078316
    new-instance v0, LX/6LM;

    const-string v1, "THREADLIST"

    invoke-direct {v0, v1, v2}, LX/6LM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LM;->THREADLIST:LX/6LM;

    .line 1078317
    new-instance v0, LX/6LM;

    const-string v1, "THREADLIST_CHAT_HEADS"

    invoke-direct {v0, v1, v3}, LX/6LM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LM;->THREADLIST_CHAT_HEADS:LX/6LM;

    .line 1078318
    new-instance v0, LX/6LM;

    const-string v1, "THREADVIEW"

    invoke-direct {v0, v1, v4}, LX/6LM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LM;->THREADVIEW:LX/6LM;

    .line 1078319
    new-instance v0, LX/6LM;

    const-string v1, "THREADVIEW_CHAT_HEADS"

    invoke-direct {v0, v1, v5}, LX/6LM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LM;->THREADVIEW_CHAT_HEADS:LX/6LM;

    .line 1078320
    new-instance v0, LX/6LM;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/6LM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6LM;->UNKNOWN:LX/6LM;

    .line 1078321
    const/4 v0, 0x5

    new-array v0, v0, [LX/6LM;

    sget-object v1, LX/6LM;->THREADLIST:LX/6LM;

    aput-object v1, v0, v2

    sget-object v1, LX/6LM;->THREADLIST_CHAT_HEADS:LX/6LM;

    aput-object v1, v0, v3

    sget-object v1, LX/6LM;->THREADVIEW:LX/6LM;

    aput-object v1, v0, v4

    sget-object v1, LX/6LM;->THREADVIEW_CHAT_HEADS:LX/6LM;

    aput-object v1, v0, v5

    sget-object v1, LX/6LM;->UNKNOWN:LX/6LM;

    aput-object v1, v0, v6

    sput-object v0, LX/6LM;->$VALUES:[LX/6LM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1078322
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6LM;
    .locals 1

    .prologue
    .line 1078323
    const-class v0, LX/6LM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6LM;

    return-object v0
.end method

.method public static values()[LX/6LM;
    .locals 1

    .prologue
    .line 1078324
    sget-object v0, LX/6LM;->$VALUES:[LX/6LM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6LM;

    return-object v0
.end method
