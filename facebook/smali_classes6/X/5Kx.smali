.class public final LX/5Kx;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5Ky;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:LX/5KI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 899327
    invoke-static {}, LX/5Ky;->q()LX/5Ky;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 899328
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 899329
    const-string v0, "CSTouchableWithoutFeedback"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 899330
    if-ne p0, p1, :cond_1

    .line 899331
    :cond_0
    :goto_0
    return v0

    .line 899332
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 899333
    goto :goto_0

    .line 899334
    :cond_3
    check-cast p1, LX/5Kx;

    .line 899335
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 899336
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 899337
    if-eq v2, v3, :cond_0

    .line 899338
    iget-object v2, p0, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 899339
    goto :goto_0

    .line 899340
    :cond_5
    iget-object v2, p1, LX/5Kx;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 899341
    :cond_6
    iget-object v2, p0, LX/5Kx;->b:LX/5KI;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/5Kx;->b:LX/5KI;

    iget-object v3, p1, LX/5Kx;->b:LX/5KI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 899342
    goto :goto_0

    .line 899343
    :cond_7
    iget-object v2, p1, LX/5Kx;->b:LX/5KI;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
