.class public LX/6Xp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/interstitial/logging/LogInterstitialParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6Xp;


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1108900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108901
    iput-object p1, p0, LX/6Xp;->a:LX/0lC;

    .line 1108902
    return-void
.end method

.method public static a(LX/0QB;)LX/6Xp;
    .locals 4

    .prologue
    .line 1108903
    sget-object v0, LX/6Xp;->b:LX/6Xp;

    if-nez v0, :cond_1

    .line 1108904
    const-class v1, LX/6Xp;

    monitor-enter v1

    .line 1108905
    :try_start_0
    sget-object v0, LX/6Xp;->b:LX/6Xp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1108906
    if-eqz v2, :cond_0

    .line 1108907
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1108908
    new-instance p0, LX/6Xp;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/6Xp;-><init>(LX/0lC;)V

    .line 1108909
    move-object v0, p0

    .line 1108910
    sput-object v0, LX/6Xp;->b:LX/6Xp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1108911
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1108912
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1108913
    :cond_1
    sget-object v0, LX/6Xp;->b:LX/6Xp;

    return-object v0

    .line 1108914
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1108915
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1108916
    check-cast p1, Lcom/facebook/interstitial/logging/LogInterstitialParams;

    .line 1108917
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1108918
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108919
    const-string v0, "me/interstitials/%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/facebook/interstitial/logging/LogInterstitialParams;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1108920
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "log_event"

    iget-object v2, p1, Lcom/facebook/interstitial/logging/LogInterstitialParams;->b:LX/6Xs;

    invoke-virtual {v2}, LX/6Xs;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108921
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "log_data"

    iget-object v2, p0, LX/6Xp;->a:LX/0lC;

    iget-object v5, p1, Lcom/facebook/interstitial/logging/LogInterstitialParams;->c:LX/0P1;

    invoke-virtual {v2, v5}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108922
    new-instance v0, LX/14N;

    const-string v1, "LogInterstitialMethod"

    const-string v2, "POST"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1108923
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1108924
    const/4 v0, 0x0

    return-object v0
.end method
