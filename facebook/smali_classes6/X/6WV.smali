.class public LX/6WV;
.super LX/6WU;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public b:I

.field public c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1106354
    invoke-direct {p0}, LX/6WU;-><init>()V

    .line 1106355
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/6WV;->a:Landroid/content/res/Resources;

    .line 1106356
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 1106357
    iget-object p1, p0, LX/6WU;->d:LX/1nq;

    invoke-virtual {p1, v0}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 1106358
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/6WU;->a:Z

    .line 1106359
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 1106360
    iput p1, p0, LX/6WV;->b:I

    .line 1106361
    iget-object v0, p0, LX/6WV;->a:Landroid/content/res/Resources;

    const v1, 0x7f081e1e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6WU;->a(Ljava/lang/CharSequence;)V

    .line 1106362
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1106363
    iget v0, p0, LX/6WV;->b:I

    if-lez v0, :cond_1

    .line 1106364
    iget-object v0, p0, LX/6WV;->c:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1106365
    iget-object v0, p0, LX/6WU;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1106366
    iget-object v1, p0, LX/6WV;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1106367
    :cond_0
    invoke-super {p0, p1}, LX/6WU;->a(Landroid/graphics/Canvas;)V

    .line 1106368
    :cond_1
    return-void
.end method
