.class public LX/5Pd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Pc;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 910952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910953
    iput-object p1, p0, LX/5Pd;->a:Landroid/content/res/Resources;

    .line 910954
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 910955
    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v0, p0, LX/5Pd;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 910956
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 910957
    const/16 v2, 0x800

    invoke-static {v2}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v2

    .line 910958
    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/InputStreamReader;->read(Ljava/nio/CharBuffer;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 910959
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    .line 910960
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 910961
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 910962
    :catch_0
    move-exception v0

    .line 910963
    :try_start_1
    invoke-static {v0}, LX/64P;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 910964
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/64Q;->a(Ljava/io/Closeable;)V

    throw v0

    .line 910965
    :cond_0
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 910966
    invoke-static {v1}, LX/64Q;->a(Ljava/io/Closeable;)V

    return-object v0
.end method


# virtual methods
.method public final a(II)LX/5Pb;
    .locals 3

    .prologue
    .line 910967
    new-instance v0, LX/5Pb;

    invoke-direct {p0, p1}, LX/5Pd;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2}, LX/5Pd;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/5Pb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(IIZ)LX/5Pb;
    .locals 2

    .prologue
    .line 910968
    invoke-direct {p0, p2}, LX/5Pd;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 910969
    invoke-direct {p0, p1}, LX/5Pd;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 910970
    invoke-virtual {p0, v1, v0, p3}, LX/5Pd;->a(Ljava/lang/String;Ljava/lang/String;Z)LX/5Pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)LX/5Pb;
    .locals 1

    .prologue
    .line 910971
    if-nez p3, :cond_0

    .line 910972
    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 p0, 0x0

    aget-object v0, v0, p0

    .line 910973
    const-string p0, "#extension GL_OES_EGL_image_external : require"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    const-string p3, "Fragment shader\'s first line must be:\nimport com.google.common.base.Preconditions;"

    invoke-static {p0, p3}, LX/64O;->b(ZLjava/lang/Object;)V

    .line 910974
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 910975
    const-string p0, "samplerExternalOES"

    const-string p3, "sampler2D"

    invoke-virtual {v0, p0, p3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 910976
    move-object p2, v0

    .line 910977
    :cond_0
    new-instance v0, LX/5Pb;

    invoke-direct {v0, p1, p2}, LX/5Pb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 910978
    iget-object v0, p0, LX/5Pd;->a:Landroid/content/res/Resources;

    return-object v0
.end method
