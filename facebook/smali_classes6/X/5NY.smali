.class public final LX/5NY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/drawingview/DrawingView;


# direct methods
.method public constructor <init>(Lcom/facebook/drawingview/DrawingView;)V
    .locals 0

    .prologue
    .line 906431
    iput-object p1, p0, LX/5NY;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 906432
    iget-object v0, p0, LX/5NY;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-static {v0}, Lcom/facebook/drawingview/DrawingView;->d(Lcom/facebook/drawingview/DrawingView;)V

    .line 906433
    iget-object v0, p0, LX/5NY;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 906434
    return-void
.end method
