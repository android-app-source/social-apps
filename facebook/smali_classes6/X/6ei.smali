.class public final synthetic LX/6ei;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1118104
    invoke-static {}, LX/2uW;->values()[LX/2uW;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6ei;->a:[I

    :try_start_0
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->ADD_MEMBERS:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_17

    :goto_0
    :try_start_1
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->REMOVE_MEMBERS:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_16

    :goto_1
    :try_start_2
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->SET_NAME:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_15

    :goto_2
    :try_start_3
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->SET_IMAGE:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_14

    :goto_3
    :try_start_4
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->REMOVED_IMAGE:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_13

    :goto_4
    :try_start_5
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->VIDEO_CALL:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_12

    :goto_5
    :try_start_6
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_11

    :goto_6
    :try_start_7
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->INCOMING_CALL:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_10

    :goto_7
    :try_start_8
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->MISSED_CALL:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_f

    :goto_8
    :try_start_9
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->OUTGOING_CALL:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_e

    :goto_9
    :try_start_a
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->CALL_LOG:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_d

    :goto_a
    :try_start_b
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->TELEPHONE_CALL_LOG:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_c

    :goto_b
    :try_start_c
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->SMS_LOG:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_b

    :goto_c
    :try_start_d
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->REGULAR:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_a

    :goto_d
    :try_start_e
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->PENDING_SEND:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_9

    :goto_e
    :try_start_f
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->FAILED_SEND:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_8

    :goto_f
    :try_start_10
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_7

    :goto_10
    :try_start_11
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_6

    :goto_11
    :try_start_12
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->P2P_PAYMENT:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_5

    :goto_12
    :try_start_13
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->P2P_PAYMENT_CANCELED:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_4

    :goto_13
    :try_start_14
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->P2P_PAYMENT_GROUP:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_3

    :goto_14
    :try_start_15
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->COMMERCE_LINK:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_2

    :goto_15
    :try_start_16
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->COMMERCE_UNLINK:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_1

    :goto_16
    :try_start_17
    sget-object v0, LX/6ei;->a:[I

    sget-object v1, LX/2uW;->ACTIVITY_REPLY:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_0

    :goto_17
    return-void

    :catch_0
    goto :goto_17

    :catch_1
    goto :goto_16

    :catch_2
    goto :goto_15

    :catch_3
    goto :goto_14

    :catch_4
    goto :goto_13

    :catch_5
    goto :goto_12

    :catch_6
    goto :goto_11

    :catch_7
    goto :goto_10

    :catch_8
    goto :goto_f

    :catch_9
    goto :goto_e

    :catch_a
    goto/16 :goto_d

    :catch_b
    goto/16 :goto_c

    :catch_c
    goto/16 :goto_b

    :catch_d
    goto/16 :goto_a

    :catch_e
    goto/16 :goto_9

    :catch_f
    goto/16 :goto_8

    :catch_10
    goto/16 :goto_7

    :catch_11
    goto/16 :goto_6

    :catch_12
    goto/16 :goto_5

    :catch_13
    goto/16 :goto_4

    :catch_14
    goto/16 :goto_3

    :catch_15
    goto/16 :goto_2

    :catch_16
    goto/16 :goto_1

    :catch_17
    goto/16 :goto_0
.end method
