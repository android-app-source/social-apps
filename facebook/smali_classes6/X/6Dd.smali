.class public final LX/6Dd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

.field public final synthetic c:LX/6De;


# direct methods
.method public constructor <init>(LX/6De;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1065358
    iput-object p1, p0, LX/6Dd;->c:LX/6De;

    iput-object p2, p0, LX/6Dd;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/6Dd;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1065359
    iget-object v0, p0, LX/6Dd;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1065360
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065361
    check-cast p1, Ljava/util/HashMap;

    .line 1065362
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1065363
    if-nez p1, :cond_0

    .line 1065364
    iget-object v0, p0, LX/6Dd;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x1f883a9f

    invoke-static {v0, v2, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1065365
    :goto_0
    return-void

    .line 1065366
    :cond_0
    iget-object v0, p0, LX/6Dd;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;->h()Ljava/util/HashMap;

    move-result-object v0

    .line 1065367
    iget-object v1, p0, LX/6Dd;->c:LX/6De;

    iget-object v1, v1, LX/6De;->f:LX/6DS;

    .line 1065368
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1065369
    invoke-static {v0}, LX/6DS;->b(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065370
    invoke-virtual {v1, v0}, LX/6DS;->c(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065371
    invoke-static {v0}, LX/6DS;->d(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065372
    invoke-static {v0}, LX/6DS;->e(Ljava/util/Map;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065373
    move-object v0, v3

    .line 1065374
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1065375
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1065376
    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1065377
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1065378
    invoke-virtual {v0, v5}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1065379
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 1065380
    :goto_2
    move-object v5, v5

    .line 1065381
    move-object v1, v5

    .line 1065382
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1065383
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1065384
    :cond_3
    iget-object v0, p0, LX/6Dd;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x77b18b77

    invoke-static {v0, v2, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_2
.end method
