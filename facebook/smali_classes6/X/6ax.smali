.class public LX/6ax;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "LX/698;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/6ax;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "LX/7cA;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/6ax;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/698;

.field public final b:LX/7cA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1112806
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/6ax;->c:Ljava/util/WeakHashMap;

    .line 1112807
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/6ax;->d:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(LX/698;)V
    .locals 1

    .prologue
    .line 1112802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112803
    iput-object p1, p0, LX/6ax;->a:LX/698;

    .line 1112804
    const/4 v0, 0x0

    iput-object v0, p0, LX/6ax;->b:LX/7cA;

    .line 1112805
    return-void
.end method

.method private constructor <init>(LX/7cA;)V
    .locals 1

    .prologue
    .line 1112753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112754
    iput-object p1, p0, LX/6ax;->b:LX/7cA;

    .line 1112755
    const/4 v0, 0x0

    iput-object v0, p0, LX/6ax;->a:LX/698;

    .line 1112756
    return-void
.end method

.method public static a(LX/698;)LX/6ax;
    .locals 3

    .prologue
    .line 1112795
    sget-object v0, LX/6ax;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1112796
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1112797
    :goto_0
    if-nez v0, :cond_0

    .line 1112798
    new-instance v0, LX/6ax;

    invoke-direct {v0, p0}, LX/6ax;-><init>(LX/698;)V

    .line 1112799
    sget-object v1, LX/6ax;->c:Ljava/util/WeakHashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112800
    :cond_0
    return-object v0

    .line 1112801
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    goto :goto_0
.end method

.method public static a(LX/7cA;)LX/6ax;
    .locals 3

    .prologue
    .line 1112788
    sget-object v0, LX/6ax;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1112789
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1112790
    :goto_0
    if-nez v0, :cond_0

    .line 1112791
    new-instance v0, LX/6ax;

    invoke-direct {v0, p0}, LX/6ax;-><init>(LX/7cA;)V

    .line 1112792
    sget-object v1, LX/6ax;->d:Ljava/util/WeakHashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112793
    :cond_0
    return-object v0

    .line 1112794
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/android/maps/model/LatLng;
    .locals 1

    .prologue
    .line 1112785
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112786
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0}, LX/67m;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 1112787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0}, LX/7cA;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, LX/6as;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 1112781
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112782
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0, p1, p2}, LX/698;->f(FF)V

    .line 1112783
    :goto_0
    return-void

    .line 1112784
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0, p1, p2}, LX/7cA;->a(FF)V

    goto :goto_0
.end method

.method public final a(LX/68w;)V
    .locals 2

    .prologue
    .line 1112777
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112778
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0, p1}, LX/698;->a(LX/68w;)V

    .line 1112779
    :goto_0
    return-void

    .line 1112780
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-static {p1}, LX/6as;->a(LX/68w;)LX/7c6;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7cA;->a(LX/7c6;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 1112773
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112774
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0, p1}, LX/698;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1112775
    :goto_0
    return-void

    .line 1112776
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-static {p1}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7cA;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1112769
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112770
    iget-object v0, p0, LX/6ax;->a:LX/698;

    .line 1112771
    iget-object p0, v0, LX/698;->D:Ljava/lang/String;

    move-object v0, p0

    .line 1112772
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0}, LX/7cA;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1112765
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112766
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0}, LX/67m;->l()V

    .line 1112767
    :goto_0
    return-void

    .line 1112768
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0}, LX/7cA;->a()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1112761
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112762
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0}, LX/698;->q()V

    .line 1112763
    :goto_0
    return-void

    .line 1112764
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0}, LX/7cA;->d()V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1112757
    iget-object v0, p0, LX/6ax;->a:LX/698;

    if-eqz v0, :cond_0

    .line 1112758
    iget-object v0, p0, LX/6ax;->a:LX/698;

    invoke-virtual {v0}, LX/698;->p()V

    .line 1112759
    :goto_0
    return-void

    .line 1112760
    :cond_0
    iget-object v0, p0, LX/6ax;->b:LX/7cA;

    invoke-virtual {v0}, LX/7cA;->e()V

    goto :goto_0
.end method
