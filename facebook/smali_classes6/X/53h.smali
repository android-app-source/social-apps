.class public final LX/53h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0za;"
    }
.end annotation


# static fields
.field public static c:I

.field public static final d:I

.field public static final e:LX/53d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53d",
            "<",
            "LX/53h;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final f:LX/53f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53f",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/53g;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 827409
    new-instance v0, LX/53e;

    invoke-direct {v0}, LX/53e;-><init>()V

    sput-object v0, LX/53h;->e:LX/53d;

    .line 827410
    const/16 v0, 0x100

    sput v0, LX/53h;->c:I

    .line 827411
    sget-boolean v0, LX/53k;->a:Z

    move v0, v0

    .line 827412
    if-eqz v0, :cond_0

    .line 827413
    const/16 v0, 0x8

    sput v0, LX/53h;->c:I

    .line 827414
    :cond_0
    const-string v0, "rx.indexed-ring-buffer.size"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 827415
    if-eqz v1, :cond_1

    .line 827416
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/53h;->c:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 827417
    :cond_1
    :goto_0
    sget v0, LX/53h;->c:I

    sput v0, LX/53h;->d:I

    return-void

    .line 827418
    :catch_0
    move-exception v0

    .line 827419
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to set \'rx.indexed-ring-buffer.size\' with value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " => "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 827403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827404
    new-instance v0, LX/53f;

    invoke-direct {v0}, LX/53f;-><init>()V

    iput-object v0, p0, LX/53h;->f:LX/53f;

    .line 827405
    new-instance v0, LX/53g;

    invoke-direct {v0}, LX/53g;-><init>()V

    iput-object v0, p0, LX/53h;->g:LX/53g;

    .line 827406
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 827407
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/53h;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 827408
    return-void
.end method

.method private a(LX/4V7;II)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4V7",
            "<-TE;",
            "Ljava/lang/Boolean;",
            ">;II)I"
        }
    .end annotation

    .prologue
    .line 827387
    iget-object v0, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    .line 827388
    iget-object v0, p0, LX/53h;->f:LX/53f;

    .line 827389
    sget v1, LX/53h;->d:I

    if-lt p2, v1, :cond_5

    .line 827390
    invoke-static {p0, p2}, LX/53h;->c(LX/53h;I)LX/53f;

    move-result-object v0

    .line 827391
    sget v1, LX/53h;->d:I

    rem-int v1, p2, v1

    move-object v3, v0

    move v0, v1

    move v1, p2

    .line 827392
    :goto_0
    if-eqz v3, :cond_4

    move v2, v1

    move v1, v0

    .line 827393
    :goto_1
    sget v0, LX/53h;->d:I

    if-ge v1, v0, :cond_3

    .line 827394
    if-ge v2, v4, :cond_0

    if-lt v2, p3, :cond_1

    .line 827395
    :cond_0
    :goto_2
    return v2

    .line 827396
    :cond_1
    iget-object v0, v3, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 827397
    if-eqz v0, :cond_2

    .line 827398
    invoke-interface {p1, v0}, LX/4V7;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 827399
    if-eqz v0, :cond_0

    .line 827400
    :cond_2
    add-int/lit8 v0, v1, 0x1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 827401
    :cond_3
    iget-object v0, v3, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/53f;

    .line 827402
    const/4 p2, 0x0

    move-object v3, v0

    move v1, v2

    move v0, p2

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_2

    :cond_5
    move-object v3, v0

    move v1, p2

    move v0, p2

    goto :goto_0
.end method

.method private static b(LX/53h;I)LX/53g;
    .locals 5

    .prologue
    .line 827374
    sget v0, LX/53h;->d:I

    if-ge p1, v0, :cond_1

    .line 827375
    iget-object v0, p0, LX/53h;->g:LX/53g;

    .line 827376
    :cond_0
    return-object v0

    .line 827377
    :cond_1
    sget v0, LX/53h;->d:I

    div-int v3, p1, v0

    .line 827378
    iget-object v1, p0, LX/53h;->g:LX/53g;

    .line 827379
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    if-ge v1, v3, :cond_0

    .line 827380
    iget-object v2, v0, LX/53g;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 827381
    iget-object v2, v0, LX/53g;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/53g;

    .line 827382
    :cond_2
    :goto_1
    move-object v2, v2

    .line 827383
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 827384
    :cond_3
    new-instance v2, LX/53g;

    invoke-direct {v2}, LX/53g;-><init>()V

    .line 827385
    iget-object v4, v0, LX/53g;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 p0, 0x0

    invoke-virtual {v4, p0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 827386
    iget-object v2, v0, LX/53g;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/53g;

    goto :goto_1
.end method

.method public static c(LX/53h;I)LX/53f;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/53f",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 827361
    sget v0, LX/53h;->d:I

    if-ge p1, v0, :cond_1

    .line 827362
    iget-object v0, p0, LX/53h;->f:LX/53f;

    .line 827363
    :cond_0
    return-object v0

    .line 827364
    :cond_1
    sget v0, LX/53h;->d:I

    div-int v3, p1, v0

    .line 827365
    iget-object v1, p0, LX/53h;->f:LX/53f;

    .line 827366
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    if-ge v1, v3, :cond_0

    .line 827367
    iget-object v2, v0, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 827368
    iget-object v2, v0, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/53f;

    .line 827369
    :cond_2
    :goto_1
    move-object v2, v2

    .line 827370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 827371
    :cond_3
    new-instance v2, LX/53f;

    invoke-direct {v2}, LX/53f;-><init>()V

    .line 827372
    iget-object v4, v0, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 p0, 0x0

    invoke-virtual {v4, p0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 827373
    iget-object v2, v0, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/53f;

    goto :goto_1
.end method

.method public static declared-synchronized d(LX/53h;I)V
    .locals 2

    .prologue
    .line 827420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/53h;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 827421
    sget v1, LX/53h;->d:I

    if-ge v0, v1, :cond_0

    .line 827422
    iget-object v1, p0, LX/53h;->g:LX/53g;

    invoke-virtual {v1, v0, p1}, LX/53g;->b(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827423
    :goto_0
    monitor-exit p0

    return-void

    .line 827424
    :cond_0
    :try_start_1
    sget v1, LX/53h;->d:I

    rem-int v1, v0, v1

    .line 827425
    invoke-static {p0, v0}, LX/53h;->b(LX/53h;I)LX/53g;

    move-result-object v0

    invoke-virtual {v0, v1, p1}, LX/53g;->b(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 827426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/53h;)I
    .locals 3

    .prologue
    .line 827350
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/53h;->f(LX/53h;)I

    move-result v0

    .line 827351
    if-ltz v0, :cond_2

    .line 827352
    sget v1, LX/53h;->d:I

    if-ge v0, v1, :cond_1

    .line 827353
    iget-object v1, p0, LX/53h;->g:LX/53g;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, LX/53g;->a(II)I

    move-result v0

    .line 827354
    :goto_0
    iget-object v1, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 827355
    iget-object v1, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827356
    :cond_0
    :goto_1
    monitor-exit p0

    return v0

    .line 827357
    :cond_1
    :try_start_1
    sget v1, LX/53h;->d:I

    rem-int v1, v0, v1

    .line 827358
    invoke-static {p0, v0}, LX/53h;->b(LX/53h;I)LX/53g;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/53g;->a(II)I

    move-result v0

    goto :goto_0

    .line 827359
    :cond_2
    iget-object v0, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 827360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized f(LX/53h;)I
    .locals 3

    .prologue
    .line 827344
    monitor-enter p0

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/53h;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 827345
    if-lez v0, :cond_1

    .line 827346
    iget-object v1, p0, LX/53h;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 827347
    add-int/lit8 v0, v0, -0x1

    .line 827348
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 827349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/4V7;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4V7",
            "<-TE;",
            "Ljava/lang/Boolean;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 827339
    iget-object v1, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-direct {p0, p1, p2, v1}, LX/53h;->a(LX/4V7;II)I

    move-result v1

    .line 827340
    if-lez p2, :cond_1

    iget-object v2, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 827341
    invoke-direct {p0, p1, v0, p2}, LX/53h;->a(LX/4V7;II)I

    move-result v0

    .line 827342
    :cond_0
    :goto_0
    return v0

    .line 827343
    :cond_1
    iget-object v2, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-eq v1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 827326
    const/4 v1, 0x0

    .line 827327
    iget-object v0, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    .line 827328
    iget-object v0, p0, LX/53h;->f:LX/53f;

    move-object v3, v0

    move v0, v1

    .line 827329
    :goto_0
    if-eqz v3, :cond_1

    move v2, v0

    move v0, v1

    .line 827330
    :goto_1
    sget v5, LX/53h;->d:I

    if-ge v0, v5, :cond_0

    .line 827331
    if-ge v2, v4, :cond_1

    .line 827332
    iget-object v5, v3, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 827333
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 827334
    :cond_0
    iget-object v0, v3, LX/53f;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/53f;

    move-object v3, v0

    move v0, v2

    goto :goto_0

    .line 827335
    :cond_1
    iget-object v0, p0, LX/53h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 827336
    iget-object v0, p0, LX/53h;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 827337
    sget-object v0, LX/53h;->e:LX/53d;

    invoke-virtual {v0, p0}, LX/53d;->a(Ljava/lang/Object;)V

    .line 827338
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827325
    const/4 v0, 0x0

    return v0
.end method
