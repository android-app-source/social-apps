.class public final LX/54P;
.super LX/52r;
.source ""


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/54P;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:I

.field private final c:LX/54i;

.field private final d:LX/54Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828002
    const-class v0, LX/54P;

    const-string v1, "a"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/54P;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/54Q;)V
    .locals 1

    .prologue
    .line 827998
    invoke-direct {p0}, LX/52r;-><init>()V

    .line 827999
    new-instance v0, LX/54i;

    invoke-direct {v0}, LX/54i;-><init>()V

    iput-object v0, p0, LX/54P;->c:LX/54i;

    .line 828000
    iput-object p1, p0, LX/54P;->d:LX/54Q;

    .line 828001
    return-void
.end method


# virtual methods
.method public final a(LX/0vR;)LX/0za;
    .locals 3

    .prologue
    .line 827983
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, LX/54P;->a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 2

    .prologue
    .line 827992
    iget-object v0, p0, LX/54P;->c:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827993
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 827994
    :goto_0
    return-object v0

    .line 827995
    :cond_0
    iget-object v0, p0, LX/54P;->d:LX/54Q;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/53a;->b(LX/0vR;JLjava/util/concurrent/TimeUnit;)Lrx/internal/schedulers/ScheduledAction;

    move-result-object v0

    .line 827996
    iget-object v1, p0, LX/54P;->c:LX/54i;

    invoke-virtual {v1, v0}, LX/54i;->a(LX/0za;)V

    .line 827997
    iget-object v1, p0, LX/54P;->c:LX/54i;

    invoke-virtual {v0, v1}, Lrx/internal/schedulers/ScheduledAction;->a(LX/54i;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 827985
    sget-object v0, LX/54P;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827986
    sget-object v0, LX/54O;->d:LX/54O;

    iget-object v1, p0, LX/54P;->d:LX/54Q;

    .line 827987
    invoke-static {}, LX/54O;->d()J

    move-result-wide v3

    iget-wide v5, v0, LX/54O;->a:J

    add-long/2addr v3, v5

    .line 827988
    iput-wide v3, v1, LX/54Q;->b:J

    .line 827989
    iget-object v3, v0, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 827990
    :cond_0
    iget-object v0, p0, LX/54P;->c:LX/54i;

    invoke-virtual {v0}, LX/54i;->b()V

    .line 827991
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827984
    iget-object v0, p0, LX/54P;->c:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    return v0
.end method
