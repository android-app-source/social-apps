.class public final enum LX/5Gs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Gs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Gs;

.field public static final enum LOAD_AFTER:LX/5Gs;

.field public static final enum LOAD_BEFORE:LX/5Gs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 889359
    new-instance v0, LX/5Gs;

    const-string v1, "LOAD_AFTER"

    invoke-direct {v0, v1, v2}, LX/5Gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    .line 889360
    new-instance v0, LX/5Gs;

    const-string v1, "LOAD_BEFORE"

    invoke-direct {v0, v1, v3}, LX/5Gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    .line 889361
    const/4 v0, 0x2

    new-array v0, v0, [LX/5Gs;

    sget-object v1, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    aput-object v1, v0, v2

    sget-object v1, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    aput-object v1, v0, v3

    sput-object v0, LX/5Gs;->$VALUES:[LX/5Gs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 889358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Gs;
    .locals 1

    .prologue
    .line 889356
    const-class v0, LX/5Gs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Gs;

    return-object v0
.end method

.method public static values()[LX/5Gs;
    .locals 1

    .prologue
    .line 889357
    sget-object v0, LX/5Gs;->$VALUES:[LX/5Gs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Gs;

    return-object v0
.end method
