.class public final enum LX/6KU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6KU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6KU;

.field public static final enum CAMERA_CORE:LX/6KU;

.field public static final enum FACECAST:LX/6KU;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1076746
    new-instance v0, LX/6KU;

    const-string v1, "FACECAST"

    invoke-direct {v0, v1, v2}, LX/6KU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6KU;->FACECAST:LX/6KU;

    .line 1076747
    new-instance v0, LX/6KU;

    const-string v1, "CAMERA_CORE"

    invoke-direct {v0, v1, v3}, LX/6KU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6KU;->CAMERA_CORE:LX/6KU;

    .line 1076748
    const/4 v0, 0x2

    new-array v0, v0, [LX/6KU;

    sget-object v1, LX/6KU;->FACECAST:LX/6KU;

    aput-object v1, v0, v2

    sget-object v1, LX/6KU;->CAMERA_CORE:LX/6KU;

    aput-object v1, v0, v3

    sput-object v0, LX/6KU;->$VALUES:[LX/6KU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1076745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6KU;
    .locals 1

    .prologue
    .line 1076750
    const-class v0, LX/6KU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6KU;

    return-object v0
.end method

.method public static values()[LX/6KU;
    .locals 1

    .prologue
    .line 1076749
    sget-object v0, LX/6KU;->$VALUES:[LX/6KU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6KU;

    return-object v0
.end method
