.class public final enum LX/5P4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5P4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5P4;

.field public static final enum CI_FRIENDS_CENTER:LX/5P4;

.field public static final enum CI_NUX:LX/5P4;

.field public static final enum FEED:LX/5P4;

.field public static final enum JEWEL:LX/5P4;

.field public static final enum NUX:LX/5P4;

.field public static final enum UNKNOWN:LX/5P4;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 909878
    new-instance v0, LX/5P4;

    const-string v1, "CI_FRIENDS_CENTER"

    const-string v2, "ci_friends_center"

    invoke-direct {v0, v1, v4, v2}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->CI_FRIENDS_CENTER:LX/5P4;

    .line 909879
    new-instance v0, LX/5P4;

    const-string v1, "CI_NUX"

    const-string v2, "ci_nux"

    invoke-direct {v0, v1, v5, v2}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->CI_NUX:LX/5P4;

    .line 909880
    new-instance v0, LX/5P4;

    const-string v1, "FEED"

    const-string v2, "mobile_top_of_feed"

    invoke-direct {v0, v1, v6, v2}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->FEED:LX/5P4;

    .line 909881
    new-instance v0, LX/5P4;

    const-string v1, "JEWEL"

    const-string v2, "mobile_jewel"

    invoke-direct {v0, v1, v7, v2}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->JEWEL:LX/5P4;

    .line 909882
    new-instance v0, LX/5P4;

    const-string v1, "NUX"

    const-string v2, "nux"

    invoke-direct {v0, v1, v8, v2}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->NUX:LX/5P4;

    .line 909883
    new-instance v0, LX/5P4;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3}, LX/5P4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P4;->UNKNOWN:LX/5P4;

    .line 909884
    const/4 v0, 0x6

    new-array v0, v0, [LX/5P4;

    sget-object v1, LX/5P4;->CI_FRIENDS_CENTER:LX/5P4;

    aput-object v1, v0, v4

    sget-object v1, LX/5P4;->CI_NUX:LX/5P4;

    aput-object v1, v0, v5

    sget-object v1, LX/5P4;->FEED:LX/5P4;

    aput-object v1, v0, v6

    sget-object v1, LX/5P4;->JEWEL:LX/5P4;

    aput-object v1, v0, v7

    sget-object v1, LX/5P4;->NUX:LX/5P4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5P4;->UNKNOWN:LX/5P4;

    aput-object v2, v0, v1

    sput-object v0, LX/5P4;->$VALUES:[LX/5P4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 909885
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 909886
    iput-object p3, p0, LX/5P4;->value:Ljava/lang/String;

    .line 909887
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5P4;
    .locals 1

    .prologue
    .line 909888
    const-class v0, LX/5P4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5P4;

    return-object v0
.end method

.method public static values()[LX/5P4;
    .locals 1

    .prologue
    .line 909889
    sget-object v0, LX/5P4;->$VALUES:[LX/5P4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5P4;

    return-object v0
.end method
