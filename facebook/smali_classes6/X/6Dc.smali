.class public final LX/6Dc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

.field public final synthetic c:Landroid/os/Bundle;

.field public final synthetic d:LX/6De;


# direct methods
.method public constructor <init>(LX/6De;Ljava/util/ArrayList;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1065357
    iput-object p1, p0, LX/6Dc;->d:LX/6De;

    iput-object p2, p0, LX/6Dc;->a:Ljava/util/ArrayList;

    iput-object p3, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    iput-object p4, p0, LX/6Dc;->c:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 1065323
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v0, v0, LX/6De;->g:LX/03V;

    sget-object v1, LX/6De;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1065324
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v0, v0, LX/6De;->l:LX/6Cz;

    iget-object v1, p0, LX/6Dc;->c:Landroid/os/Bundle;

    sget-object v2, LX/6De;->a:Ljava/lang/String;

    const-string v3, "Failed to fetch graphQL results for autofill data"

    iget-object v4, p0, LX/6Dc;->c:Landroid/os/Bundle;

    const-string v5, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/6Dc;->c:Landroid/os/Bundle;

    const-string v6, "JS_BRIDGE_AD_ID"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/6Cz;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1065325
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065326
    check-cast p1, Ljava/util/List;

    .line 1065327
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1065328
    :cond_0
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v0, v0, LX/6De;->l:LX/6Cz;

    iget-object v1, p0, LX/6Dc;->a:Ljava/util/ArrayList;

    iget-object v2, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    const-string v3, "browser_extensions_save_autofill_no_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6Cz;->b(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065329
    :goto_0
    return-void

    .line 1065330
    :cond_1
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v0, v0, LX/6De;->k:LX/0W3;

    sget-wide v2, LX/0X5;->ft:J

    const-string v1, "save_dialog"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1065331
    iget-object v1, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, v1, LX/6De;->l:LX/6Cz;

    iget-object v2, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    const-string v3, "browser_extensions_save_autofill_dialog_shown"

    invoke-virtual {v1, p1, v2, v3}, LX/6Cz;->a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 1065332
    const-string v1, "expandable_banner_with_details"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1065333
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    check-cast p1, Ljava/util/ArrayList;

    invoke-static {v0, v1, p1}, LX/6De;->a(LX/6De;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object v0

    .line 1065334
    const-string v1, "EXTRA_AUTOFILL_SAVE_BANNER_IS_EXPANDED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1065335
    iget-object v1, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, v1, LX/6De;->i:LX/1Bf;

    iget-object v2, p0, LX/6Dc;->d:LX/6De;

    iget-object v2, v2, LX/6De;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/1Bf;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1065336
    :cond_2
    const-string v1, "expanded_banner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1065337
    iget-object v0, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    check-cast p1, Ljava/util/ArrayList;

    invoke-static {v0, v1, p1}, LX/6De;->a(LX/6De;Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;Ljava/util/ArrayList;)Landroid/os/Bundle;

    move-result-object v0

    .line 1065338
    const-string v1, "EXTRA_AUTOFILL_SAVE_BANNER_IS_EXPANDED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1065339
    iget-object v1, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, v1, LX/6De;->i:LX/1Bf;

    iget-object v2, p0, LX/6Dc;->d:LX/6De;

    iget-object v2, v2, LX/6De;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/1Bf;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1065340
    :cond_3
    new-instance v0, LX/6Da;

    iget-object v1, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, v1, LX/6De;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6Da;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    .line 1065341
    const-string v2, "JS_BRIDGE_APP_NAME"

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v1, v2

    .line 1065342
    iput-object v1, v0, LX/6Da;->b:Ljava/lang/String;

    .line 1065343
    move-object v0, v0

    .line 1065344
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, LX/6Da;->c:Ljava/util/ArrayList;

    .line 1065345
    move-object v0, v0

    .line 1065346
    iget-object v1, p0, LX/6Dc;->b:Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    .line 1065347
    iput-object v1, v0, LX/6Da;->d:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 1065348
    move-object v0, v0

    .line 1065349
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/6Da;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1065350
    move-object v1, v1

    .line 1065351
    const-string v2, "app_name"

    iget-object v3, v0, LX/6Da;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1065352
    const-string v2, "autofill_data"

    iget-object v3, v0, LX/6Da;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1065353
    const-string v2, "js_bridge_call"

    iget-object v3, v0, LX/6Da;->d:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1065354
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1065355
    move-object v0, v1

    .line 1065356
    iget-object v1, p0, LX/6Dc;->d:LX/6De;

    iget-object v1, v1, LX/6De;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6Dc;->d:LX/6De;

    iget-object v2, v2, LX/6De;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0
.end method
