.class public final enum LX/5L2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5L2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5L2;

.field public static final enum ON_AAA_ONLY_ME_COMPLETED:LX/5L2;

.field public static final enum ON_AAA_TUX_COMPLETED:LX/5L2;

.field public static final enum ON_DATASET_CHANGE:LX/5L2;

.field public static final enum ON_DESTROY_VIEW:LX/5L2;

.field public static final enum ON_FACE_DETECTION_COMPLETE:LX/5L2;

.field public static final enum ON_FIRST_DRAW:LX/5L2;

.field public static final enum ON_INLINE_SPROUTS_STATE_CHANGE:LX/5L2;

.field public static final enum ON_KEYBOARD_STATE_CHANGED:LX/5L2;

.field public static final enum ON_NEWCOMER_AUDIENCE_SELECTOR_COMPLETED:LX/5L2;

.field public static final enum ON_PAUSE:LX/5L2;

.field public static final enum ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY:LX/5L2;

.field public static final enum ON_PRIVACY_FETCHED:LX/5L2;

.field public static final enum ON_RESUME:LX/5L2;

.field public static final enum ON_SCROLL_CHANGED:LX/5L2;

.field public static final enum ON_STATUS_TEXT_CHANGED:LX/5L2;

.field public static final enum ON_USER_CANCEL:LX/5L2;

.field public static final enum ON_USER_POST:LX/5L2;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 899384
    new-instance v0, LX/5L2;

    const-string v1, "ON_DATASET_CHANGE"

    invoke-direct {v0, v1, v3}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    .line 899385
    new-instance v0, LX/5L2;

    const-string v1, "ON_RESUME"

    invoke-direct {v0, v1, v4}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_RESUME:LX/5L2;

    .line 899386
    new-instance v0, LX/5L2;

    const-string v1, "ON_SCROLL_CHANGED"

    invoke-direct {v0, v1, v5}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_SCROLL_CHANGED:LX/5L2;

    .line 899387
    new-instance v0, LX/5L2;

    const-string v1, "ON_STATUS_TEXT_CHANGED"

    invoke-direct {v0, v1, v6}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_STATUS_TEXT_CHANGED:LX/5L2;

    .line 899388
    new-instance v0, LX/5L2;

    const-string v1, "ON_AAA_TUX_COMPLETED"

    invoke-direct {v0, v1, v7}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_AAA_TUX_COMPLETED:LX/5L2;

    .line 899389
    new-instance v0, LX/5L2;

    const-string v1, "ON_AAA_ONLY_ME_COMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_AAA_ONLY_ME_COMPLETED:LX/5L2;

    .line 899390
    new-instance v0, LX/5L2;

    const-string v1, "ON_NEWCOMER_AUDIENCE_SELECTOR_COMPLETED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_NEWCOMER_AUDIENCE_SELECTOR_COMPLETED:LX/5L2;

    .line 899391
    new-instance v0, LX/5L2;

    const-string v1, "ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY:LX/5L2;

    .line 899392
    new-instance v0, LX/5L2;

    const-string v1, "ON_FACE_DETECTION_COMPLETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_FACE_DETECTION_COMPLETE:LX/5L2;

    .line 899393
    new-instance v0, LX/5L2;

    const-string v1, "ON_FIRST_DRAW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    .line 899394
    new-instance v0, LX/5L2;

    const-string v1, "ON_PAUSE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_PAUSE:LX/5L2;

    .line 899395
    new-instance v0, LX/5L2;

    const-string v1, "ON_DESTROY_VIEW"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    .line 899396
    new-instance v0, LX/5L2;

    const-string v1, "ON_PRIVACY_FETCHED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_PRIVACY_FETCHED:LX/5L2;

    .line 899397
    new-instance v0, LX/5L2;

    const-string v1, "ON_USER_POST"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_USER_POST:LX/5L2;

    .line 899398
    new-instance v0, LX/5L2;

    const-string v1, "ON_USER_CANCEL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_USER_CANCEL:LX/5L2;

    .line 899399
    new-instance v0, LX/5L2;

    const-string v1, "ON_INLINE_SPROUTS_STATE_CHANGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_INLINE_SPROUTS_STATE_CHANGE:LX/5L2;

    .line 899400
    new-instance v0, LX/5L2;

    const-string v1, "ON_KEYBOARD_STATE_CHANGED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/5L2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5L2;->ON_KEYBOARD_STATE_CHANGED:LX/5L2;

    .line 899401
    const/16 v0, 0x11

    new-array v0, v0, [LX/5L2;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    aput-object v1, v0, v3

    sget-object v1, LX/5L2;->ON_RESUME:LX/5L2;

    aput-object v1, v0, v4

    sget-object v1, LX/5L2;->ON_SCROLL_CHANGED:LX/5L2;

    aput-object v1, v0, v5

    sget-object v1, LX/5L2;->ON_STATUS_TEXT_CHANGED:LX/5L2;

    aput-object v1, v0, v6

    sget-object v1, LX/5L2;->ON_AAA_TUX_COMPLETED:LX/5L2;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5L2;->ON_AAA_ONLY_ME_COMPLETED:LX/5L2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5L2;->ON_NEWCOMER_AUDIENCE_SELECTOR_COMPLETED:LX/5L2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5L2;->ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5L2;->ON_FACE_DETECTION_COMPLETE:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5L2;->ON_PAUSE:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5L2;->ON_PRIVACY_FETCHED:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/5L2;->ON_USER_POST:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/5L2;->ON_USER_CANCEL:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/5L2;->ON_INLINE_SPROUTS_STATE_CHANGE:LX/5L2;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/5L2;->ON_KEYBOARD_STATE_CHANGED:LX/5L2;

    aput-object v2, v0, v1

    sput-object v0, LX/5L2;->$VALUES:[LX/5L2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 899402
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5L2;
    .locals 1

    .prologue
    .line 899403
    const-class v0, LX/5L2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5L2;

    return-object v0
.end method

.method public static values()[LX/5L2;
    .locals 1

    .prologue
    .line 899404
    sget-object v0, LX/5L2;->$VALUES:[LX/5L2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5L2;

    return-object v0
.end method
