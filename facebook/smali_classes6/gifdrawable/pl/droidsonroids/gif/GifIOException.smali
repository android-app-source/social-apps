.class public Lgifdrawable/pl/droidsonroids/gif/GifIOException;
.super Ljava/io/IOException;
.source ""


# static fields
.field private static final serialVersionUID:J = 0xbdbbd5fa1b9L


# instance fields
.field public final reason:LX/52k;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 826403
    invoke-static {p1}, LX/52k;->fromCode(I)LX/52k;

    move-result-object v0

    invoke-direct {p0, v0}, Lgifdrawable/pl/droidsonroids/gif/GifIOException;-><init>(LX/52k;)V

    .line 826404
    return-void
.end method

.method public constructor <init>(LX/52k;)V
    .locals 1

    .prologue
    .line 826405
    invoke-virtual {p1}, LX/52k;->getFormattedDescription()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 826406
    iput-object p1, p0, Lgifdrawable/pl/droidsonroids/gif/GifIOException;->reason:LX/52k;

    .line 826407
    return-void
.end method
