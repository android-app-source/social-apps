.class public final Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;
.super LX/52r;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/54i;

.field public final c:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lrx/schedulers/ExecutorScheduler$ExecutorAction;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 828108
    invoke-direct {p0}, LX/52r;-><init>()V

    .line 828109
    iput-object p1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->a:Ljava/util/concurrent/Executor;

    .line 828110
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 828111
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 828112
    new-instance v0, LX/54i;

    invoke-direct {v0}, LX/54i;-><init>()V

    iput-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    .line 828113
    return-void
.end method


# virtual methods
.method public final a(LX/0vR;)LX/0za;
    .locals 3

    .prologue
    .line 828094
    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 828095
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 828096
    :cond_0
    :goto_0
    return-object v0

    .line 828097
    :cond_1
    new-instance v0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;

    iget-object v1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    invoke-direct {v0, p1, v1}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;-><init>(LX/0vR;LX/54i;)V

    .line 828098
    iget-object v1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    invoke-virtual {v1, v0}, LX/54i;->a(LX/0za;)V

    .line 828099
    iget-object v1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 828100
    iget-object v1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    if-nez v1, :cond_0

    .line 828101
    :try_start_0
    iget-object v1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->a:Ljava/util/concurrent/Executor;

    const v2, -0x703e3016

    invoke-static {v1, p0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 828102
    :catch_0
    move-exception v1

    .line 828103
    iget-object v2, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    invoke-virtual {v2, v0}, LX/54i;->b(LX/0za;)V

    .line 828104
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 828105
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 828106
    invoke-virtual {v0}, LX/0vB;->b()LX/0vD;

    .line 828107
    throw v1
.end method

.method public final a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 4

    .prologue
    .line 828075
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 828076
    invoke-virtual {p0, p1}, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->a(LX/0vR;)LX/0za;

    move-result-object v0

    .line 828077
    :goto_0
    return-object v0

    .line 828078
    :cond_0
    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 828079
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 828080
    goto :goto_0

    .line 828081
    :cond_1
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->a:Ljava/util/concurrent/Executor;

    instance-of v0, v0, Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_2

    .line 828082
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->a:Ljava/util/concurrent/Executor;

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    .line 828083
    :goto_1
    new-instance v1, LX/54k;

    invoke-direct {v1}, LX/54k;-><init>()V

    .line 828084
    :try_start_0
    new-instance v2, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker$1;

    invoke-direct {v2, p0, v1, p1}, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker$1;-><init>(Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;LX/54k;LX/0vR;)V

    invoke-interface {v0, v2, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 828085
    new-instance v2, LX/54n;

    invoke-direct {v2, v0}, LX/54n;-><init>(Ljava/util/concurrent/Future;)V

    move-object v0, v2

    .line 828086
    invoke-virtual {v1, v0}, LX/54k;->a(LX/0za;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 828087
    goto :goto_0

    .line 828088
    :cond_2
    sget-object v0, LX/54X;->b:LX/54X;

    iget-object v0, v0, LX/54X;->c:Ljava/util/concurrent/ScheduledExecutorService;

    move-object v0, v0

    .line 828089
    goto :goto_1

    .line 828090
    :catch_0
    move-exception v0

    .line 828091
    sget-object v1, LX/0vB;->b:LX/0vB;

    move-object v1, v1

    .line 828092
    invoke-virtual {v1}, LX/0vB;->b()LX/0vD;

    .line 828093
    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 828069
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    invoke-virtual {v0}, LX/54i;->b()V

    .line 828070
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828074
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->b:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    return v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 828071
    :cond_0
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;

    invoke-virtual {v0}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->run()V

    .line 828072
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorSchedulerWorker;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-gtz v0, :cond_0

    .line 828073
    return-void
.end method
