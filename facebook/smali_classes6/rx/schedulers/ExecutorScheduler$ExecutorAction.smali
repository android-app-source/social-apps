.class public final Lrx/schedulers/ExecutorScheduler$ExecutorAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;
.implements LX/0za;


# static fields
.field public static final d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "Lrx/schedulers/ExecutorScheduler$ExecutorAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0vR;

.field public final b:LX/54i;

.field public volatile c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828046
    const-class v0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;

    const-string v1, "c"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/0vR;LX/54i;)V
    .locals 0

    .prologue
    .line 828047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828048
    iput-object p1, p0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->a:LX/0vR;

    .line 828049
    iput-object p2, p0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->b:LX/54i;

    .line 828050
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 828051
    sget-object v0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->d:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828052
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->b:LX/54i;

    invoke-virtual {v0, p0}, LX/54i;->b(LX/0za;)V

    .line 828053
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828054
    iget v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->c:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 828055
    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828056
    :goto_0
    return-void

    .line 828057
    :cond_0
    :try_start_0
    iget-object v0, p0, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->a:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828058
    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->b()V

    goto :goto_0

    .line 828059
    :catch_0
    move-exception v0

    .line 828060
    :try_start_1
    sget-object v1, LX/0vB;->b:LX/0vB;

    move-object v1, v1

    .line 828061
    invoke-virtual {v1}, LX/0vB;->b()LX/0vD;

    .line 828062
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 828063
    invoke-virtual {v1}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 828064
    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lrx/schedulers/ExecutorScheduler$ExecutorAction;->b()V

    throw v0
.end method
