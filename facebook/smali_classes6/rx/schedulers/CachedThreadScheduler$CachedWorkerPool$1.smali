.class public final Lrx/schedulers/CachedThreadScheduler$CachedWorkerPool$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/54O;


# direct methods
.method public constructor <init>(LX/54O;)V
    .locals 0

    .prologue
    .line 827965
    iput-object p1, p0, Lrx/schedulers/CachedThreadScheduler$CachedWorkerPool$1;->a:LX/54O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 827966
    iget-object v0, p0, Lrx/schedulers/CachedThreadScheduler$CachedWorkerPool$1;->a:LX/54O;

    .line 827967
    iget-object v1, v0, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 827968
    invoke-static {}, LX/54O;->d()J

    move-result-wide v3

    .line 827969
    iget-object v1, v0, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/54Q;

    .line 827970
    iget-wide v7, v1, LX/54Q;->b:J

    move-wide v5, v7

    .line 827971
    cmp-long v5, v5, v3

    if-gtz v5, :cond_1

    .line 827972
    iget-object v5, v0, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 827973
    invoke-virtual {v1}, LX/53a;->b()V

    goto :goto_0

    .line 827974
    :cond_1
    return-void
.end method
