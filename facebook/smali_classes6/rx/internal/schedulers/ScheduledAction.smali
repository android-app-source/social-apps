.class public final Lrx/internal/schedulers/ScheduledAction;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source ""

# interfaces
.implements Ljava/lang/Runnable;
.implements LX/0za;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Ljava/lang/Thread;",
        ">;",
        "Ljava/lang/Runnable;",
        "LX/0za;"
    }
.end annotation


# instance fields
.field public final action:LX/0vR;

.field public final cancel:LX/54i;


# direct methods
.method public constructor <init>(LX/0vR;)V
    .locals 1

    .prologue
    .line 827270
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 827271
    iput-object p1, p0, Lrx/internal/schedulers/ScheduledAction;->action:LX/0vR;

    .line 827272
    new-instance v0, LX/54i;

    invoke-direct {v0}, LX/54i;-><init>()V

    iput-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    .line 827273
    return-void
.end method


# virtual methods
.method public final a(LX/54i;)V
    .locals 2

    .prologue
    .line 827274
    iget-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    new-instance v1, LX/53c;

    invoke-direct {v1, p0, p1}, LX/53c;-><init>(LX/0za;LX/54i;)V

    invoke-virtual {v0, v1}, LX/54i;->a(LX/0za;)V

    .line 827275
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 827276
    iget-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 827277
    iget-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    invoke-virtual {v0}, LX/54i;->b()V

    .line 827278
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 827279
    iget-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->cancel:LX/54i;

    invoke-virtual {v0}, LX/54i;->c()Z

    move-result v0

    return v0
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 827280
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/internal/schedulers/ScheduledAction;->lazySet(Ljava/lang/Object;)V

    .line 827281
    iget-object v0, p0, Lrx/internal/schedulers/ScheduledAction;->action:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827282
    invoke-virtual {p0}, Lrx/internal/schedulers/ScheduledAction;->b()V

    .line 827283
    :goto_0
    return-void

    .line 827284
    :catch_0
    move-exception v0

    .line 827285
    :try_start_1
    instance-of v1, v0, LX/531;

    if-eqz v1, :cond_0

    .line 827286
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Exception thrown on Scheduler.Worker thread. Add `onError` handling."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 827287
    :goto_1
    sget-object v1, LX/0vB;->b:LX/0vB;

    move-object v1, v1

    .line 827288
    invoke-virtual {v1}, LX/0vB;->b()LX/0vD;

    .line 827289
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 827290
    invoke-virtual {v1}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 827291
    invoke-virtual {p0}, Lrx/internal/schedulers/ScheduledAction;->b()V

    goto :goto_0

    .line 827292
    :cond_0
    :try_start_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fatal Exception thrown on Scheduler.Worker thread."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    goto :goto_1

    .line 827293
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lrx/internal/schedulers/ScheduledAction;->b()V

    throw v0
.end method
