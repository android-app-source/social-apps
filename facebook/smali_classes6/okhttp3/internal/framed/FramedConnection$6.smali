.class public final Lokhttp3/internal/framed/FramedConnection$6;
.super Lokhttp3/internal/NamedRunnable;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic c:LX/672;

.field public final synthetic d:I

.field public final synthetic e:Z

.field public final synthetic f:LX/65c;


# direct methods
.method public varargs constructor <init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILX/672;IZ)V
    .locals 0

    .prologue
    .line 1047695
    iput-object p1, p0, Lokhttp3/internal/framed/FramedConnection$6;->f:LX/65c;

    iput p4, p0, Lokhttp3/internal/framed/FramedConnection$6;->a:I

    iput-object p5, p0, Lokhttp3/internal/framed/FramedConnection$6;->c:LX/672;

    iput p6, p0, Lokhttp3/internal/framed/FramedConnection$6;->d:I

    iput-boolean p7, p0, Lokhttp3/internal/framed/FramedConnection$6;->e:Z

    invoke-direct {p0, p2, p3}, Lokhttp3/internal/NamedRunnable;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 1047696
    :try_start_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$6;->f:LX/65c;

    iget-object v0, v0, LX/65c;->u:LX/661;

    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$6;->c:LX/672;

    iget v2, p0, Lokhttp3/internal/framed/FramedConnection$6;->d:I

    invoke-interface {v0, v1, v2}, LX/661;->a(LX/671;I)Z

    move-result v0

    .line 1047697
    if-eqz v0, :cond_0

    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$6;->f:LX/65c;

    iget-object v1, v1, LX/65c;->i:LX/65Z;

    iget v2, p0, Lokhttp3/internal/framed/FramedConnection$6;->a:I

    sget-object v3, LX/65X;->CANCEL:LX/65X;

    invoke-interface {v1, v2, v3}, LX/65Z;->a(ILX/65X;)V

    .line 1047698
    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lokhttp3/internal/framed/FramedConnection$6;->e:Z

    if-eqz v0, :cond_2

    .line 1047699
    :cond_1
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$6;->f:LX/65c;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047700
    :try_start_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$6;->f:LX/65c;

    iget-object v0, v0, LX/65c;->x:Ljava/util/Set;

    iget v2, p0, Lokhttp3/internal/framed/FramedConnection$6;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1047701
    monitor-exit v1

    .line 1047702
    :cond_2
    :goto_0
    return-void

    .line 1047703
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    goto :goto_0
.end method
