.class public final Lokhttp3/internal/framed/FramedConnection$Reader;
.super Lokhttp3/internal/NamedRunnable;
.source ""


# instance fields
.field public final a:LX/65Y;

.field public final synthetic c:LX/65c;


# direct methods
.method public constructor <init>(LX/65c;LX/65Y;)V
    .locals 4

    .prologue
    .line 1047855
    iput-object p1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    .line 1047856
    const-string v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, LX/65c;->o:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lokhttp3/internal/NamedRunnable;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1047857
    iput-object p2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    .line 1047858
    return-void
.end method


# virtual methods
.method public final a(IJ)V
    .locals 4

    .prologue
    .line 1047843
    if-nez p1, :cond_1

    .line 1047844
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    monitor-enter v1

    .line 1047845
    :try_start_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-wide v2, v0, LX/65c;->d:J

    add-long/2addr v2, p2

    iput-wide v2, v0, LX/65c;->d:J

    .line 1047846
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    const v2, -0x23597962

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1047847
    monitor-exit v1

    .line 1047848
    :cond_0
    :goto_0
    return-void

    .line 1047849
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1047850
    :cond_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v0, p1}, LX/65c;->a(I)LX/65i;

    move-result-object v1

    .line 1047851
    if-eqz v1, :cond_0

    .line 1047852
    monitor-enter v1

    .line 1047853
    :try_start_1
    invoke-virtual {v1, p2, p3}, LX/65i;->a(J)V

    .line 1047854
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public final a(ILX/65X;)V
    .locals 8

    .prologue
    .line 1047836
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p1}, LX/65c;->d(LX/65c;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047837
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    .line 1047838
    iget-object v7, v0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lokhttp3/internal/framed/FramedConnection$7;

    const-string v3, "OkHttp %s Push Reset[%s]"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, v0, LX/65c;->o:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, v0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lokhttp3/internal/framed/FramedConnection$7;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILX/65X;)V

    const v2, 0x1cfc723a

    invoke-static {v7, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047839
    :cond_0
    :goto_0
    return-void

    .line 1047840
    :cond_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v0, p1}, LX/65c;->b(I)LX/65i;

    move-result-object v0

    .line 1047841
    if-eqz v0, :cond_0

    .line 1047842
    invoke-virtual {v0, p2}, LX/65i;->c(LX/65X;)V

    goto :goto_0
.end method

.method public final a(ILX/673;)V
    .locals 5

    .prologue
    .line 1047820
    invoke-virtual {p2}, LX/673;->e()I

    .line 1047821
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    monitor-enter v1

    .line 1047822
    :try_start_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v0, v0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v2, v2, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [LX/65i;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65i;

    .line 1047823
    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    const/4 v3, 0x1

    .line 1047824
    iput-boolean v3, v2, LX/65c;->r:Z

    .line 1047825
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047826
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 1047827
    iget v4, v3, LX/65i;->e:I

    move v4, v4

    .line 1047828
    if-le v4, p1, :cond_0

    invoke-virtual {v3}, LX/65i;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1047829
    sget-object v4, LX/65X;->REFUSED_STREAM:LX/65X;

    invoke-virtual {v3, v4}, LX/65i;->c(LX/65X;)V

    .line 1047830
    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    .line 1047831
    iget p2, v3, LX/65i;->e:I

    move v3, p2

    .line 1047832
    invoke-virtual {v4, v3}, LX/65c;->b(I)LX/65i;

    .line 1047833
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1047834
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1047835
    :cond_1
    return-void
.end method

.method public final a(ZII)V
    .locals 12

    .prologue
    .line 1047859
    if-eqz p1, :cond_1

    .line 1047860
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p2}, LX/65c;->c$redex0(LX/65c;I)LX/660;

    move-result-object v0

    .line 1047861
    if-eqz v0, :cond_0

    .line 1047862
    invoke-virtual {v0}, LX/660;->b()V

    .line 1047863
    :cond_0
    :goto_0
    return-void

    .line 1047864
    :cond_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1047865
    sget-object v11, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lokhttp3/internal/framed/FramedConnection$3;

    const-string v5, "OkHttp %s ping %08x%08x"

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v7, v0, LX/65c;->o:Ljava/lang/String;

    aput-object v7, v6, v4

    const/4 v4, 0x1

    .line 1047866
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    move-object v4, v0

    move v7, v1

    move v8, p2

    move v9, p3

    move-object v10, v2

    invoke-direct/range {v3 .. v10}, Lokhttp3/internal/framed/FramedConnection$3;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ZIILX/660;)V

    .line 1047867
    const v4, 0x55647c7b

    invoke-static {v11, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047868
    goto :goto_0
.end method

.method public final a(ZILX/671;I)V
    .locals 2

    .prologue
    .line 1047810
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p2}, LX/65c;->d(LX/65c;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047811
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p2, p3, p4, p1}, LX/65c;->a$redex0(LX/65c;ILX/671;IZ)V

    .line 1047812
    :cond_0
    :goto_0
    return-void

    .line 1047813
    :cond_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v0, p2}, LX/65c;->a(I)LX/65i;

    move-result-object v0

    .line 1047814
    if-nez v0, :cond_2

    .line 1047815
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    sget-object v1, LX/65X;->INVALID_STREAM:LX/65X;

    invoke-virtual {v0, p2, v1}, LX/65c;->a(ILX/65X;)V

    .line 1047816
    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, LX/671;->f(J)V

    goto :goto_0

    .line 1047817
    :cond_2
    invoke-virtual {v0, p3, p4}, LX/65i;->a(LX/671;I)V

    .line 1047818
    if-eqz p1, :cond_0

    .line 1047819
    invoke-virtual {v0}, LX/65i;->i()V

    goto :goto_0
.end method

.method public final a(ZLX/663;)V
    .locals 13

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 1047775
    const/4 v0, 0x0

    .line 1047776
    iget-object v8, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    monitor-enter v8

    .line 1047777
    :try_start_0
    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v2, v2, LX/65c;->f:LX/663;

    const/high16 v3, 0x10000

    invoke-virtual {v2, v3}, LX/663;->f(I)I

    move-result v2

    .line 1047778
    if-eqz p1, :cond_0

    iget-object v3, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v3, v3, LX/65c;->f:LX/663;

    const/4 v5, 0x0

    .line 1047779
    iput v5, v3, LX/663;->c:I

    iput v5, v3, LX/663;->b:I

    iput v5, v3, LX/663;->a:I

    .line 1047780
    iget-object v4, v3, LX/663;->d:[I

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([II)V

    .line 1047781
    :cond_0
    iget-object v3, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v3, v3, LX/65c;->f:LX/663;

    .line 1047782
    const/4 v4, 0x0

    :goto_0
    const/16 v5, 0xa

    if-ge v4, v5, :cond_2

    .line 1047783
    invoke-virtual {p2, v4}, LX/663;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1047784
    invoke-virtual {p2, v4}, LX/663;->c(I)I

    move-result v5

    invoke-virtual {p2, v4}, LX/663;->b(I)I

    move-result v9

    invoke-virtual {v3, v4, v5, v9}, LX/663;->a(III)LX/663;

    .line 1047785
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1047786
    :cond_2
    iget-object v3, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    .line 1047787
    iget-object v4, v3, LX/65c;->a:LX/64x;

    move-object v3, v4

    .line 1047788
    sget-object v4, LX/64x;->HTTP_2:LX/64x;

    if-ne v3, v4, :cond_3

    .line 1047789
    sget-object v3, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lokhttp3/internal/framed/FramedConnection$Reader$3;

    const-string v5, "OkHttp %s ACK Settings"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v11, v11, LX/65c;->o:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-direct {v4, p0, v5, v9, p2}, Lokhttp3/internal/framed/FramedConnection$Reader$3;-><init>(Lokhttp3/internal/framed/FramedConnection$Reader;Ljava/lang/String;[Ljava/lang/Object;LX/663;)V

    const v5, -0x5a655c88

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047790
    :cond_3
    iget-object v3, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v3, v3, LX/65c;->f:LX/663;

    const/high16 v4, 0x10000

    invoke-virtual {v3, v4}, LX/663;->f(I)I

    move-result v3

    .line 1047791
    const/4 v4, -0x1

    if-eq v3, v4, :cond_7

    if-eq v3, v2, :cond_7

    .line 1047792
    sub-int v2, v3, v2

    int-to-long v2, v2

    .line 1047793
    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-boolean v4, v4, LX/65c;->w:Z

    if-nez v4, :cond_4

    .line 1047794
    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v4, v2, v3}, LX/65c;->a(J)V

    .line 1047795
    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    const/4 v5, 0x1

    .line 1047796
    iput-boolean v5, v4, LX/65c;->w:Z

    .line 1047797
    :cond_4
    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v4, v4, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1047798
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v0, v0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v4, v4, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [LX/65i;

    invoke-interface {v0, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65i;

    move-wide v4, v2

    move-object v2, v0

    .line 1047799
    :goto_1
    sget-object v0, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lokhttp3/internal/framed/FramedConnection$Reader$2;

    const-string v9, "OkHttp %s settings"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v12, v12, LX/65c;->o:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-direct {v3, p0, v9, v10}, Lokhttp3/internal/framed/FramedConnection$Reader$2;-><init>(Lokhttp3/internal/framed/FramedConnection$Reader;Ljava/lang/String;[Ljava/lang/Object;)V

    const v9, -0x62f10264

    invoke-static {v0, v3, v9}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047800
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047801
    if-eqz v2, :cond_5

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    .line 1047802
    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_5

    aget-object v1, v2, v0

    .line 1047803
    monitor-enter v1

    .line 1047804
    :try_start_1
    invoke-virtual {v1, v4, v5}, LX/65i;->a(J)V

    .line 1047805
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1047806
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1047807
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1047808
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1047809
    :cond_5
    return-void

    :cond_6
    move-wide v4, v2

    move-object v2, v0

    goto :goto_1

    :cond_7
    move-object v2, v0

    move-wide v4, v6

    goto :goto_1
.end method

.method public final a(ZZILjava/util/List;LX/65k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZI",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;",
            "LX/65k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1047732
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p3}, LX/65c;->d(LX/65c;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047733
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v0, p3, p4, p2}, LX/65c;->a$redex0(LX/65c;ILjava/util/List;Z)V

    .line 1047734
    :cond_0
    :goto_0
    return-void

    .line 1047735
    :cond_1
    iget-object v6, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    monitor-enter v6

    .line 1047736
    :try_start_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-boolean v0, v0, LX/65c;->r:Z

    if-eqz v0, :cond_2

    monitor-exit v6

    goto :goto_0

    .line 1047737
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1047738
    :cond_2
    :try_start_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v0, p3}, LX/65c;->a(I)LX/65i;

    move-result-object v0

    .line 1047739
    if-nez v0, :cond_6

    .line 1047740
    invoke-virtual {p5}, LX/65k;->failIfStreamAbsent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1047741
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    sget-object v1, LX/65X;->INVALID_STREAM:LX/65X;

    invoke-virtual {v0, p3, v1}, LX/65c;->a(ILX/65X;)V

    .line 1047742
    monitor-exit v6

    goto :goto_0

    .line 1047743
    :cond_3
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget v0, v0, LX/65c;->p:I

    if-gt p3, v0, :cond_4

    monitor-exit v6

    goto :goto_0

    .line 1047744
    :cond_4
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget v1, v1, LX/65c;->q:I

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_5

    monitor-exit v6

    goto :goto_0

    .line 1047745
    :cond_5
    new-instance v0, LX/65i;

    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    move v1, p3

    move v3, p1

    move v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/65i;-><init>(ILX/65c;ZZLjava/util/List;)V

    .line 1047746
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    .line 1047747
    iput p3, v1, LX/65c;->p:I

    .line 1047748
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v1, v1, LX/65c;->n:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047749
    sget-object v1, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lokhttp3/internal/framed/FramedConnection$Reader$1;

    const-string v3, "OkHttp %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v7, v7, LX/65c;->o:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lokhttp3/internal/framed/FramedConnection$Reader$1;-><init>(Lokhttp3/internal/framed/FramedConnection$Reader;Ljava/lang/String;[Ljava/lang/Object;LX/65i;)V

    const v0, 0x1a42f60d

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047750
    monitor-exit v6

    goto :goto_0

    .line 1047751
    :cond_6
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047752
    invoke-virtual {p5}, LX/65k;->failIfStreamPresent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1047753
    sget-object v1, LX/65X;->PROTOCOL_ERROR:LX/65X;

    invoke-virtual {v0, v1}, LX/65i;->b(LX/65X;)V

    .line 1047754
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-virtual {v0, p3}, LX/65c;->b(I)LX/65i;

    goto/16 :goto_0

    .line 1047755
    :cond_7
    invoke-virtual {v0, p4, p5}, LX/65i;->a(Ljava/util/List;LX/65k;)V

    .line 1047756
    if-eqz p2, :cond_0

    invoke-virtual {v0}, LX/65i;->i()V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1047757
    sget-object v0, LX/65X;->INTERNAL_ERROR:LX/65X;

    .line 1047758
    sget-object v2, LX/65X;->INTERNAL_ERROR:LX/65X;

    .line 1047759
    :try_start_0
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-boolean v1, v1, LX/65c;->b:Z

    if-nez v1, :cond_0

    .line 1047760
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    invoke-interface {v1}, LX/65Y;->a()V

    .line 1047761
    :cond_0
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    invoke-interface {v1, p0}, LX/65Y;->a(Lokhttp3/internal/framed/FramedConnection$Reader;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047762
    sget-object v0, LX/65X;->NO_ERROR:LX/65X;

    .line 1047763
    sget-object v1, LX/65X;->CANCEL:LX/65X;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047764
    :try_start_1
    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v2, v0, v1}, LX/65c;->a$redex0(LX/65c;LX/65X;LX/65X;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1047765
    :goto_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    invoke-static {v0}, LX/65A;->a(Ljava/io/Closeable;)V

    .line 1047766
    :goto_1
    return-void

    .line 1047767
    :catch_0
    :try_start_2
    sget-object v1, LX/65X;->PROTOCOL_ERROR:LX/65X;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1047768
    :try_start_3
    sget-object v0, LX/65X;->PROTOCOL_ERROR:LX/65X;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1047769
    :try_start_4
    iget-object v2, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v2, v1, v0}, LX/65c;->a$redex0(LX/65c;LX/65X;LX/65X;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1047770
    :goto_2
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    invoke-static {v0}, LX/65A;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 1047771
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 1047772
    :goto_3
    :try_start_5
    iget-object v3, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    invoke-static {v3, v1, v2}, LX/65c;->a$redex0(LX/65c;LX/65X;LX/65X;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 1047773
    :goto_4
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader;->a:LX/65Y;

    invoke-static {v1}, LX/65A;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    goto :goto_4

    .line 1047774
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_0
.end method
