.class public final Lokhttp3/internal/framed/FramedConnection$Reader$1;
.super Lokhttp3/internal/NamedRunnable;
.source ""


# instance fields
.field public final synthetic a:LX/65i;

.field public final synthetic c:Lokhttp3/internal/framed/FramedConnection$Reader;


# direct methods
.method public varargs constructor <init>(Lokhttp3/internal/framed/FramedConnection$Reader;Ljava/lang/String;[Ljava/lang/Object;LX/65i;)V
    .locals 0

    .prologue
    .line 1047718
    iput-object p1, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->c:Lokhttp3/internal/framed/FramedConnection$Reader;

    iput-object p4, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->a:LX/65i;

    invoke-direct {p0, p2, p3}, Lokhttp3/internal/NamedRunnable;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    .prologue
    .line 1047719
    :try_start_0
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->c:Lokhttp3/internal/framed/FramedConnection$Reader;

    iget-object v0, v0, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v0, v0, LX/65c;->m:LX/65R;

    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->a:LX/65i;

    invoke-virtual {v0, v1}, LX/65R;->a(LX/65i;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047720
    :goto_0
    return-void

    .line 1047721
    :catch_0
    move-exception v0

    .line 1047722
    sget-object v1, LX/66Y;->a:LX/66Y;

    move-object v1, v1

    .line 1047723
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FramedConnection.Listener failure for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->c:Lokhttp3/internal/framed/FramedConnection$Reader;

    iget-object v4, v4, Lokhttp3/internal/framed/FramedConnection$Reader;->c:LX/65c;

    iget-object v4, v4, LX/65c;->o:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/66Y;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1047724
    :try_start_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$Reader$1;->a:LX/65i;

    sget-object v1, LX/65X;->PROTOCOL_ERROR:LX/65X;

    invoke-virtual {v0, v1}, LX/65i;->a(LX/65X;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1047725
    :catch_1
    goto :goto_0
.end method
