.class public final Lokhttp3/internal/framed/FramedConnection$5;
.super Lokhttp3/internal/NamedRunnable;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:Z

.field public final synthetic e:LX/65c;


# direct methods
.method public varargs constructor <init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V
    .locals 0

    .prologue
    .line 1047686
    iput-object p1, p0, Lokhttp3/internal/framed/FramedConnection$5;->e:LX/65c;

    iput p4, p0, Lokhttp3/internal/framed/FramedConnection$5;->a:I

    iput-object p5, p0, Lokhttp3/internal/framed/FramedConnection$5;->c:Ljava/util/List;

    iput-boolean p6, p0, Lokhttp3/internal/framed/FramedConnection$5;->d:Z

    invoke-direct {p0, p2, p3}, Lokhttp3/internal/NamedRunnable;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 1047687
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$5;->e:LX/65c;

    iget-object v0, v0, LX/65c;->u:LX/661;

    invoke-interface {v0}, LX/661;->b()Z

    move-result v0

    .line 1047688
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$5;->e:LX/65c;

    iget-object v1, v1, LX/65c;->i:LX/65Z;

    iget v2, p0, Lokhttp3/internal/framed/FramedConnection$5;->a:I

    sget-object v3, LX/65X;->CANCEL:LX/65X;

    invoke-interface {v1, v2, v3}, LX/65Z;->a(ILX/65X;)V

    .line 1047689
    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lokhttp3/internal/framed/FramedConnection$5;->d:Z

    if-eqz v0, :cond_2

    .line 1047690
    :cond_1
    iget-object v1, p0, Lokhttp3/internal/framed/FramedConnection$5;->e:LX/65c;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047691
    :try_start_1
    iget-object v0, p0, Lokhttp3/internal/framed/FramedConnection$5;->e:LX/65c;

    iget-object v0, v0, LX/65c;->x:Ljava/util/Set;

    iget v2, p0, Lokhttp3/internal/framed/FramedConnection$5;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1047692
    monitor-exit v1

    .line 1047693
    :cond_2
    :goto_0
    return-void

    .line 1047694
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    goto :goto_0
.end method
