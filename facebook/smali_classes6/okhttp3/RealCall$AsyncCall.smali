.class public final Lokhttp3/RealCall$AsyncCall;
.super Lokhttp3/internal/NamedRunnable;
.source ""


# instance fields
.field public final synthetic a:LX/64y;

.field private final c:LX/64X;


# direct methods
.method public constructor <init>(LX/64y;LX/64X;)V
    .locals 4

    .prologue
    .line 1046204
    iput-object p1, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    .line 1046205
    const-string v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, LX/64y;->e()LX/64q;

    move-result-object v3

    invoke-virtual {v3}, LX/64q;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lokhttp3/internal/NamedRunnable;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1046206
    iput-object p2, p0, Lokhttp3/RealCall$AsyncCall;->c:LX/64X;

    .line 1046207
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1046208
    iget-object v0, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    iget-object v0, v0, LX/64y;->a:LX/650;

    .line 1046209
    iget-object p0, v0, LX/650;->a:LX/64q;

    move-object v0, p0

    .line 1046210
    iget-object p0, v0, LX/64q;->e:Ljava/lang/String;

    move-object v0, p0

    .line 1046211
    return-object v0
.end method

.method public final b()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 1046212
    const/4 v2, 0x0

    .line 1046213
    :try_start_0
    iget-object v0, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    const/4 v7, 0x0

    .line 1046214
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1046215
    iget-object v5, v0, LX/64y;->b:LX/64w;

    .line 1046216
    iget-object v8, v5, LX/64w;->e:Ljava/util/List;

    move-object v5, v8

    .line 1046217
    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1046218
    iget-object v5, v0, LX/64y;->c:LX/66R;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046219
    new-instance v5, LX/668;

    iget-object v8, v0, LX/64y;->b:LX/64w;

    .line 1046220
    iget-object v9, v8, LX/64w;->h:LX/64g;

    move-object v8, v9

    .line 1046221
    invoke-direct {v5, v8}, LX/668;-><init>(LX/64g;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046222
    new-instance v5, LX/65F;

    iget-object v8, v0, LX/64y;->b:LX/64w;

    .line 1046223
    iget-object v9, v8, LX/64w;->i:LX/64U;

    if-eqz v9, :cond_3

    iget-object v9, v8, LX/64w;->i:LX/64U;

    iget-object v9, v9, LX/64U;->a:LX/65O;

    :goto_0
    move-object v8, v9

    .line 1046224
    invoke-direct {v5, v8}, LX/65F;-><init>(LX/65O;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046225
    new-instance v5, LX/65P;

    iget-object v8, v0, LX/64y;->b:LX/64w;

    invoke-direct {v5, v8}, LX/65P;-><init>(LX/64w;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046226
    iget-object v5, v0, LX/64y;->c:LX/66R;

    .line 1046227
    iget-boolean v8, v5, LX/66R;->c:Z

    move v5, v8

    .line 1046228
    if-nez v5, :cond_0

    .line 1046229
    iget-object v5, v0, LX/64y;->b:LX/64w;

    .line 1046230
    iget-object v8, v5, LX/64w;->f:Ljava/util/List;

    move-object v5, v8

    .line 1046231
    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1046232
    :cond_0
    new-instance v5, LX/669;

    iget-object v8, v0, LX/64y;->c:LX/66R;

    .line 1046233
    iget-boolean v9, v8, LX/66R;->c:Z

    move v8, v9

    .line 1046234
    invoke-direct {v5, v8}, LX/669;-><init>(Z)V

    .line 1046235
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046236
    new-instance v5, LX/66O;

    const/4 v10, 0x0

    iget-object v11, v0, LX/64y;->a:LX/650;

    move-object v8, v7

    move-object v9, v7

    invoke-direct/range {v5 .. v11}, LX/66O;-><init>(Ljava/util/List;LX/65W;LX/66G;LX/65S;ILX/650;)V

    .line 1046237
    iget-object v6, v0, LX/64y;->a:LX/650;

    invoke-virtual {v5, v6}, LX/66O;->a(LX/650;)LX/655;

    move-result-object v5

    move-object v0, v5

    .line 1046238
    iget-object v3, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    iget-object v3, v3, LX/64y;->c:LX/66R;

    .line 1046239
    iget-boolean v4, v3, LX/66R;->d:Z

    move v2, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046240
    if-eqz v2, :cond_1

    .line 1046241
    :try_start_1
    iget-object v0, p0, Lokhttp3/RealCall$AsyncCall;->c:LX/64X;

    iget-object v2, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Canceled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2, v3}, LX/64X;->a(LX/64y;Ljava/io/IOException;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1046242
    :goto_1
    iget-object v0, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    iget-object v0, v0, LX/64y;->b:LX/64w;

    .line 1046243
    iget-object v1, v0, LX/64w;->a:LX/64i;

    move-object v0, v1

    .line 1046244
    invoke-virtual {v0, p0}, LX/64i;->b(Lokhttp3/RealCall$AsyncCall;)V

    .line 1046245
    :goto_2
    return-void

    .line 1046246
    :cond_1
    :try_start_2
    iget-object v2, p0, Lokhttp3/RealCall$AsyncCall;->c:LX/64X;

    iget-object v3, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    invoke-interface {v2, v3, v0}, LX/64X;->a(LX/64y;LX/655;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1046247
    :catch_0
    move-exception v0

    .line 1046248
    :goto_3
    if-eqz v1, :cond_2

    .line 1046249
    :try_start_3
    sget-object v1, LX/66Y;->a:LX/66Y;

    move-object v1, v1

    .line 1046250
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Callback failure for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    .line 1046251
    iget-object v5, v4, LX/64y;->c:LX/66R;

    .line 1046252
    iget-boolean v6, v5, LX/66R;->d:Z

    move v5, v6

    .line 1046253
    if-eqz v5, :cond_4

    const-string v5, "canceled call"

    .line 1046254
    :goto_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, LX/64y;->e()LX/64q;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 1046255
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/66Y;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1046256
    :goto_5
    iget-object v0, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    iget-object v0, v0, LX/64y;->b:LX/64w;

    .line 1046257
    iget-object v1, v0, LX/64w;->a:LX/64i;

    move-object v0, v1

    .line 1046258
    invoke-virtual {v0, p0}, LX/64i;->b(Lokhttp3/RealCall$AsyncCall;)V

    goto :goto_2

    .line 1046259
    :cond_2
    :try_start_4
    iget-object v1, p0, Lokhttp3/RealCall$AsyncCall;->c:LX/64X;

    iget-object v2, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    invoke-interface {v1, v2, v0}, LX/64X;->a(LX/64y;Ljava/io/IOException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5

    .line 1046260
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lokhttp3/RealCall$AsyncCall;->a:LX/64y;

    iget-object v1, v1, LX/64y;->b:LX/64w;

    .line 1046261
    iget-object v2, v1, LX/64w;->a:LX/64i;

    move-object v1, v2

    .line 1046262
    invoke-virtual {v1, p0}, LX/64i;->b(Lokhttp3/RealCall$AsyncCall;)V

    throw v0

    .line 1046263
    :catch_1
    move-exception v0

    move v1, v2

    goto :goto_3

    :cond_3
    :try_start_5
    iget-object v9, v8, LX/64w;->j:LX/65O;

    goto/16 :goto_0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1046264
    :cond_4
    const-string v5, "call"

    goto :goto_4
.end method
