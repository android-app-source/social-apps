.class public final Lcom/facebook/feed/analytics/LongClickTracker$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/Date;

.field public final synthetic b:D

.field public final synthetic c:D

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/0lF;

.field public final synthetic f:Ljava/util/Map;

.field public final synthetic g:LX/1Ay;


# direct methods
.method public constructor <init>(LX/1Ay;Ljava/util/Date;DDLjava/lang/String;LX/0lF;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 1103960
    iput-object p1, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->g:LX/1Ay;

    iput-object p2, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->a:Ljava/util/Date;

    iput-wide p3, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->b:D

    iput-wide p5, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->c:D

    iput-object p7, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->d:Ljava/lang/String;

    iput-object p8, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->e:LX/0lF;

    iput-object p9, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->f:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v2, 0x0

    .line 1103961
    iget-object v0, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->a:Ljava/util/Date;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->b:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 1103962
    :goto_0
    return-void

    .line 1103963
    :cond_0
    iget-wide v0, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 1103964
    iget-wide v0, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->b:D

    div-double/2addr v0, v4

    .line 1103965
    :goto_1
    iget-wide v2, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->c:D

    iget-object v4, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->e:LX/0lF;

    iget-object v6, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->f:Ljava/util/Map;

    .line 1103966
    invoke-static {v5}, LX/17Q;->F(LX/0lF;)Z

    move-result v7

    if-nez v7, :cond_1

    if-nez v4, :cond_4

    .line 1103967
    :cond_1
    const/4 v7, 0x0

    .line 1103968
    :cond_2
    :goto_2
    move-object v0, v7

    .line 1103969
    iget-object v1, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->g:LX/1Ay;

    iget-object v1, v1, LX/1Ay;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1103970
    :cond_3
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1103971
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/feed/analytics/LongClickTracker$1;->a:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-double v0, v0

    div-double/2addr v0, v4

    goto :goto_1

    .line 1103972
    :cond_4
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "client_long_click"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "tracking"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "URL"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "web_view_time"

    invoke-virtual {v7, v8, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "native_newsfeed"

    .line 1103973
    iput-object v8, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1103974
    move-object v7, v7

    .line 1103975
    const-wide/16 v9, 0x0

    cmpl-double v8, v2, v9

    if-lez v8, :cond_2

    .line 1103976
    const-string v8, "page_load_time"

    invoke-virtual {v7, v8, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2
.end method
