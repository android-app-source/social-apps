.class public Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<TP;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*TE;*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*TE;*>;)V"
        }
    .end annotation

    .prologue
    .line 909421
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 909422
    iput-object p1, p0, Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 909423
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 909424
    iget-object v0, p0, Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 909425
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)Z"
        }
    .end annotation

    .prologue
    .line 909426
    iget-object v0, p0, Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-interface {v0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
