.class public Lcom/facebook/feed/platformads/AppInstallService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/6VX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/content/pm/PackageManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1104189
    const-class v0, Lcom/facebook/feed/platformads/AppInstallService;

    sput-object v0, Lcom/facebook/feed/platformads/AppInstallService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1104186
    sget-object v0, Lcom/facebook/feed/platformads/AppInstallService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1104187
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feed/platformads/AppInstallService;->setIntentRedelivery(Z)V

    .line 1104188
    return-void
.end method

.method private static a(Lcom/facebook/feed/platformads/AppInstallService;LX/0Zb;LX/6VX;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 1104129
    iput-object p1, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/feed/platformads/AppInstallService;->c:LX/6VX;

    iput-object p3, p0, Lcom/facebook/feed/platformads/AppInstallService;->d:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private a(Lcom/facebook/feed/platformads/TrackedPackage;)V
    .locals 4

    .prologue
    .line 1104178
    iget-object v0, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    if-eqz v0, :cond_0

    .line 1104179
    iget-object v0, p1, Lcom/facebook/feed/platformads/TrackedPackage;->trackingCodes:LX/162;

    iget-object v1, p1, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feed/platformads/AppInstallService;->d:Landroid/content/pm/PackageManager;

    iget-object v3, p1, Lcom/facebook/feed/platformads/TrackedPackage;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1104180
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "store_conversion_v2"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "installer_package_name"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "native_newsfeed"

    .line 1104181
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1104182
    move-object v3, v3

    .line 1104183
    move-object v0, v3

    .line 1104184
    iget-object v1, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1104185
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/feed/platformads/AppInstallService;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v2}, LX/6VX;->a(LX/0QB;)LX/6VX;

    move-result-object v1

    check-cast v1, LX/6VX;

    invoke-static {v2}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManager;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feed/platformads/AppInstallService;->a(Lcom/facebook/feed/platformads/AppInstallService;LX/0Zb;LX/6VX;Landroid/content/pm/PackageManager;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1104173
    iget-object v0, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    if-eqz v0, :cond_0

    .line 1104174
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "package_uninstalled"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "package_name"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1104175
    move-object v0, v0

    .line 1104176
    iget-object v1, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1104177
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1104156
    iget-object v0, p0, Lcom/facebook/feed/platformads/AppInstallService;->c:LX/6VX;

    .line 1104157
    invoke-static {v0}, LX/6VX;->c(LX/6VX;)V

    .line 1104158
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 1104159
    iget-object v1, v0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1104160
    iget-object v1, v0, LX/6VX;->f:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/platformads/TrackedPackage;

    .line 1104161
    iget-object v3, v1, Lcom/facebook/feed/platformads/TrackedPackage;->trackUntil:Ljava/util/Date;

    invoke-virtual {v3, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1104162
    :goto_0
    move-object v0, v1

    .line 1104163
    if-nez v0, :cond_1

    .line 1104164
    :cond_0
    :goto_1
    return-void

    .line 1104165
    :cond_1
    const-string v1, "install"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1104166
    invoke-direct {p0, v0}, Lcom/facebook/feed/platformads/AppInstallService;->a(Lcom/facebook/feed/platformads/TrackedPackage;)V

    goto :goto_1

    .line 1104167
    :cond_2
    const-string v1, "uninstall"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1104168
    invoke-direct {p0, v0}, Lcom/facebook/feed/platformads/AppInstallService;->b(Lcom/facebook/feed/platformads/TrackedPackage;)V

    .line 1104169
    iget-object v1, p0, Lcom/facebook/feed/platformads/AppInstallService;->c:LX/6VX;

    .line 1104170
    iget-object v2, v1, LX/6VX;->f:Ljava/util/Map;

    iget-object v3, v0, Lcom/facebook/feed/platformads/TrackedPackage;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104171
    invoke-static {v1, v0}, LX/6VX;->b(LX/6VX;Lcom/facebook/feed/platformads/TrackedPackage;)V

    .line 1104172
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/feed/platformads/TrackedPackage;)V
    .locals 3

    .prologue
    .line 1104148
    iget-object v0, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    if-eqz v0, :cond_0

    .line 1104149
    iget-object v0, p1, Lcom/facebook/feed/platformads/TrackedPackage;->trackingCodes:LX/162;

    iget-object v1, p1, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    .line 1104150
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "third_party_app_uninstall"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v2, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p1, "native_newsfeed"

    .line 1104151
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1104152
    move-object v2, v2

    .line 1104153
    move-object v0, v2

    .line 1104154
    iget-object v1, p0, Lcom/facebook/feed/platformads/AppInstallService;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1104155
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1104143
    const-string v0, "uninstall"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/platformads/AppInstallService;->c:LX/6VX;

    .line 1104144
    iget-object v1, v0, LX/6VX;->c:LX/0Uh;

    const/16 v2, 0x5a9

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1104145
    if-eqz v0, :cond_0

    .line 1104146
    invoke-direct {p0, p2}, Lcom/facebook/feed/platformads/AppInstallService;->a(Ljava/lang/String;)V

    .line 1104147
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x5c627275

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1104133
    if-nez p1, :cond_0

    .line 1104134
    const/16 v1, 0x25

    const v2, -0x1ef9e37d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1104135
    :goto_0
    return-void

    .line 1104136
    :cond_0
    const-string v1, "package_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1104137
    const-string v2, "action_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1104138
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1104139
    :cond_1
    const v1, -0x7d4971c

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0

    .line 1104140
    :cond_2
    invoke-direct {p0, v2, v1}, Lcom/facebook/feed/platformads/AppInstallService;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104141
    invoke-direct {p0, v2, v1}, Lcom/facebook/feed/platformads/AppInstallService;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104142
    const v1, -0x9c5f79d

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x666f0429

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1104130
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1104131
    invoke-static {p0, p0}, Lcom/facebook/feed/platformads/AppInstallService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1104132
    const/16 v1, 0x25

    const v2, -0xbe50abe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
