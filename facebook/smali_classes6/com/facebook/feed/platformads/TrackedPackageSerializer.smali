.class public Lcom/facebook/feed/platformads/TrackedPackageSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/platformads/TrackedPackage;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1104297
    const-class v0, Lcom/facebook/feed/platformads/TrackedPackage;

    new-instance v1, Lcom/facebook/feed/platformads/TrackedPackageSerializer;

    invoke-direct {v1}, Lcom/facebook/feed/platformads/TrackedPackageSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1104298
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1104312
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/platformads/TrackedPackage;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1104306
    if-nez p0, :cond_0

    .line 1104307
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1104308
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1104309
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/platformads/TrackedPackageSerializer;->b(Lcom/facebook/feed/platformads/TrackedPackage;LX/0nX;LX/0my;)V

    .line 1104310
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1104311
    return-void
.end method

.method private static b(Lcom/facebook/feed/platformads/TrackedPackage;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1104300
    const-string v0, "appLaunchUrl"

    iget-object v1, p0, Lcom/facebook/feed/platformads/TrackedPackage;->appLaunchUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1104301
    const-string v0, "fbid"

    iget-object v1, p0, Lcom/facebook/feed/platformads/TrackedPackage;->fbid:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1104302
    const-string v0, "packageName"

    iget-object v1, p0, Lcom/facebook/feed/platformads/TrackedPackage;->packageName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1104303
    const-string v0, "trackUntil"

    iget-object v1, p0, Lcom/facebook/feed/platformads/TrackedPackage;->trackUntil:Ljava/util/Date;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1104304
    const-string v0, "trackingCodes"

    iget-object v1, p0, Lcom/facebook/feed/platformads/TrackedPackage;->trackingCodes:LX/162;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;LX/0gT;)V

    .line 1104305
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1104299
    check-cast p1, Lcom/facebook/feed/platformads/TrackedPackage;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/platformads/TrackedPackageSerializer;->a(Lcom/facebook/feed/platformads/TrackedPackage;LX/0nX;LX/0my;)V

    return-void
.end method
