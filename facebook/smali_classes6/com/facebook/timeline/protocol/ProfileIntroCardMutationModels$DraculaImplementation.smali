.class public final Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1030985
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1030986
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1031017
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1031018
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1030988
    if-nez p1, :cond_0

    move v0, v1

    .line 1030989
    :goto_0
    return v0

    .line 1030990
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1030991
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1030992
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1030993
    const v2, 0x6d395d39

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1030994
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1030995
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1030996
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1030997
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1030998
    const v2, -0x7fac355

    const/4 v5, 0x0

    .line 1030999
    if-nez v0, :cond_1

    move v4, v5

    .line 1031000
    :goto_1
    move v0, v4

    .line 1031001
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1031002
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1031003
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1031004
    :sswitch_2
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;

    .line 1031005
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1031006
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1031007
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1031008
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1031009
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 1031010
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1031011
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1031012
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1031013
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1031014
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1031015
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1031016
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x77e12d43 -> :sswitch_0
        -0x7fac355 -> :sswitch_2
        0x6d395d39 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1030987
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1030929
    if-eqz p0, :cond_0

    .line 1030930
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1030931
    if-eq v0, p0, :cond_0

    .line 1030932
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1030933
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1030969
    sparse-switch p2, :sswitch_data_0

    .line 1030970
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1030971
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1030972
    const v1, 0x6d395d39

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1030973
    :goto_0
    return-void

    .line 1030974
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1030975
    const v1, -0x7fac355

    .line 1030976
    if-eqz v0, :cond_0

    .line 1030977
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1030978
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1030979
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1030980
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1030981
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1030982
    :cond_0
    goto :goto_0

    .line 1030983
    :sswitch_2
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;

    .line 1030984
    invoke-static {v0, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x77e12d43 -> :sswitch_0
        -0x7fac355 -> :sswitch_2
        0x6d395d39 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1030963
    if-eqz p1, :cond_0

    .line 1030964
    invoke-static {p0, p1, p2}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;

    move-result-object v1

    .line 1030965
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;

    .line 1030966
    if-eq v0, v1, :cond_0

    .line 1030967
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1030968
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1030962
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1030960
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1030961
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1031019
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1031020
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1031021
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a:LX/15i;

    .line 1031022
    iput p2, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->b:I

    .line 1031023
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1030959
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1030958
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1030955
    iget v0, p0, LX/1vt;->c:I

    .line 1030956
    move v0, v0

    .line 1030957
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1030952
    iget v0, p0, LX/1vt;->c:I

    .line 1030953
    move v0, v0

    .line 1030954
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1030949
    iget v0, p0, LX/1vt;->b:I

    .line 1030950
    move v0, v0

    .line 1030951
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030946
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1030947
    move-object v0, v0

    .line 1030948
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1030937
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1030938
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1030939
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1030940
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1030941
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1030942
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1030943
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1030944
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1030945
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1030934
    iget v0, p0, LX/1vt;->c:I

    .line 1030935
    move v0, v0

    .line 1030936
    return v0
.end method
