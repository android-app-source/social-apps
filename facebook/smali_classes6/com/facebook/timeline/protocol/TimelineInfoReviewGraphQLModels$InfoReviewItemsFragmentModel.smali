.class public final Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6256ac13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033710
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033711
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1033708
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033709
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1033705
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033706
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033707
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1033697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033698
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1033699
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1033700
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1033701
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1033702
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1033703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033704
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1033712
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033713
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1033714
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1033715
    if-eqz v1, :cond_2

    .line 1033716
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    .line 1033717
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1033718
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1033719
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    .line 1033720
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1033721
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    .line 1033722
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    .line 1033723
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033724
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033696
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1033694
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->e:Ljava/util/List;

    .line 1033695
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1033691
    new-instance v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;-><init>()V

    .line 1033692
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033693
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1033690
    const v0, 0x1c125c61

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1033687
    const v0, -0x37395d29

    return v0
.end method

.method public final j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033688
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    .line 1033689
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    return-object v0
.end method
