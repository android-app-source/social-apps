.class public final Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1025933
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1025934
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1025932
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1025910
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1025911
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1025912
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1025913
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1025914
    if-eqz v2, :cond_0

    .line 1025915
    const-string p0, "actor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1025916
    invoke-static {v1, v2, p1, p2}, LX/5wz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1025917
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1025918
    if-eqz v2, :cond_1

    .line 1025919
    const-string p0, "currently_processing_profile_video_content_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1025920
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1025921
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1025922
    if-eqz v2, :cond_2

    .line 1025923
    const-string p0, "profile_wizard_nux"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1025924
    invoke-static {v1, v2, p1, p2}, LX/5x0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1025925
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1025926
    if-eqz v2, :cond_3

    .line 1025927
    const-string p0, "profile_wizard_refresher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1025928
    invoke-static {v1, v2, p1, p2}, LX/5x1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1025929
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1025930
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1025931
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel$Serializer;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
