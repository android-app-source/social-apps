.class public final Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xdb3aaf8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1027668
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1027669
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1027670
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1027671
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1027680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1027681
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1027682
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1027683
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1027684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1027685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1027672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1027673
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1027674
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    .line 1027675
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1027676
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;

    .line 1027677
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    .line 1027678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1027679
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1027657
    new-instance v0, LX/5xA;

    invoke-direct {v0, p1}, LX/5xA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1027666
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    .line 1027667
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1027664
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1027665
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1027663
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1027660
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;-><init>()V

    .line 1027661
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1027662
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1027659
    const v0, -0x592b72b3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1027658
    const v0, 0x285feb

    return v0
.end method
