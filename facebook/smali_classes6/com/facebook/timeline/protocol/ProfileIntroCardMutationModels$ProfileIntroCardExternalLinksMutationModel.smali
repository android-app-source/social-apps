.class public final Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6f859ddd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031237
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031236
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1031213
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1031214
    return-void
.end method

.method private a()Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031234
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->e:Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->e:Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    .line 1031235
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->e:Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1031228
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031229
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->a()Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1031230
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1031231
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1031232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031233
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1031220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031221
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->a()Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1031222
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->a()Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    .line 1031223
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->a()Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1031224
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;

    .line 1031225
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;->e:Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel$ProfileIntroCardModel;

    .line 1031226
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031227
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1031217
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;-><init>()V

    .line 1031218
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1031219
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1031216
    const v0, 0x6c03e391

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1031215
    const v0, -0x60c56542

    return v0
.end method
