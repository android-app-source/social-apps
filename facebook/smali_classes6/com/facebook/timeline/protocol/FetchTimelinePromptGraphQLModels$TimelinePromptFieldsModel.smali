.class public final Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xa25ddfe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1027582
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1027583
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1027584
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1027585
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1027586
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    .line 1027587
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1027588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1027589
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1027590
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1027591
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1027592
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1027593
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1027594
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1027595
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1027596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1027597
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1027598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1027599
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1027600
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    .line 1027601
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1027602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    .line 1027603
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    .line 1027604
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1027605
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1027606
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1027607
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;-><init>()V

    .line 1027608
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1027609
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1027610
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->f:Ljava/lang/String;

    .line 1027611
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1027612
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->g:Ljava/lang/String;

    .line 1027613
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1027614
    const v0, -0x94a374f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1027615
    const v0, -0x1afc0edb

    return v0
.end method
