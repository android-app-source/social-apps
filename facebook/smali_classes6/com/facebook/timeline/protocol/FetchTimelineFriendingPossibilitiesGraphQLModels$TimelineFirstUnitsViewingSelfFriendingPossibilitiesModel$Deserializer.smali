.class public final Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1023694
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel;

    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1023695
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1023696
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1023697
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1023698
    const/4 v2, 0x0

    .line 1023699
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1023700
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1023701
    :goto_0
    move v1, v2

    .line 1023702
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1023703
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1023704
    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/FetchTimelineFriendingPossibilitiesGraphQLModels$TimelineFirstUnitsViewingSelfFriendingPossibilitiesModel;-><init>()V

    .line 1023705
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1023706
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1023707
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1023708
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1023709
    :cond_0
    return-object v1

    .line 1023710
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1023711
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1023712
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1023713
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1023714
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1023715
    const-string v4, "friending_possibilities"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1023716
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1023717
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_9

    .line 1023718
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1023719
    :goto_2
    move v1, v3

    .line 1023720
    goto :goto_1

    .line 1023721
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1023722
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1023723
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1023724
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_7

    .line 1023725
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1023726
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1023727
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v6, :cond_5

    .line 1023728
    const-string p0, "total_possibility_count"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1023729
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v4

    goto :goto_3

    .line 1023730
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1023731
    :cond_7
    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1023732
    if-eqz v1, :cond_8

    .line 1023733
    invoke-virtual {v0, v3, v5, v3}, LX/186;->a(III)V

    .line 1023734
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v5, v3

    goto :goto_3
.end method
