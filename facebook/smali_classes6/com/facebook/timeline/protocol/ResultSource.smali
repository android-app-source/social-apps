.class public final enum Lcom/facebook/timeline/protocol/ResultSource;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/timeline/protocol/ResultSource;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/timeline/protocol/ResultSource;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/protocol/ResultSource;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

.field public static final enum GRAPH_CURSOR_DB:Lcom/facebook/timeline/protocol/ResultSource;

.field public static final enum SERVER:Lcom/facebook/timeline/protocol/ResultSource;

.field public static final enum UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1033165
    new-instance v0, Lcom/facebook/timeline/protocol/ResultSource;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/facebook/timeline/protocol/ResultSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033166
    new-instance v0, Lcom/facebook/timeline/protocol/ResultSource;

    const-string v1, "DISK_CACHE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/timeline/protocol/ResultSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033167
    new-instance v0, Lcom/facebook/timeline/protocol/ResultSource;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/timeline/protocol/ResultSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033168
    new-instance v0, Lcom/facebook/timeline/protocol/ResultSource;

    const-string v1, "GRAPH_CURSOR_DB"

    invoke-direct {v0, v1, v5}, Lcom/facebook/timeline/protocol/ResultSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->GRAPH_CURSOR_DB:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033169
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/timeline/protocol/ResultSource;

    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->GRAPH_CURSOR_DB:Lcom/facebook/timeline/protocol/ResultSource;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->$VALUES:[Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033170
    new-instance v0, LX/5yp;

    invoke-direct {v0}, LX/5yp;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protocol/ResultSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1033153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1033154
    return-void
.end method

.method public static fromGraphQLResultDataFreshness(LX/0ta;)Lcom/facebook/timeline/protocol/ResultSource;
    .locals 3

    .prologue
    .line 1033155
    sget-object v0, LX/5yq;->a:[I

    invoke-virtual {p0}, LX/0ta;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1033156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected freshness result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1033157
    :pswitch_0
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1033158
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

    goto :goto_0

    .line 1033159
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Should not get null data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/timeline/protocol/ResultSource;
    .locals 1

    .prologue
    .line 1033160
    const-class v0, Lcom/facebook/timeline/protocol/ResultSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ResultSource;

    return-object v0
.end method

.method public static values()[Lcom/facebook/timeline/protocol/ResultSource;
    .locals 1

    .prologue
    .line 1033161
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->$VALUES:[Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/timeline/protocol/ResultSource;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1033162
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1033163
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ResultSource;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1033164
    return-void
.end method
