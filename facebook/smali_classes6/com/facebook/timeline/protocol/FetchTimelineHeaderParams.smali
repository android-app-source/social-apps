.class public Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:I

.field public final c:I

.field public final d:Z

.field public final e:Z

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1027474
    new-instance v0, LX/5x5;

    invoke-direct {v0}, LX/5x5;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JIIZZLX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIIZZ",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1027475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1027476
    iput-wide p1, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    .line 1027477
    iput p3, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->b:I

    .line 1027478
    iput p4, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->c:I

    .line 1027479
    iput-boolean p5, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->d:Z

    .line 1027480
    iput-boolean p6, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->e:Z

    .line 1027481
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->f:LX/0am;

    .line 1027482
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1027483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1027484
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    .line 1027485
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->b:I

    .line 1027486
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->c:I

    .line 1027487
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->d:Z

    .line 1027488
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->e:Z

    .line 1027489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->f:LX/0am;

    .line 1027490
    return-void

    :cond_0
    move v0, v2

    .line 1027491
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1027492
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1027493
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1027494
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profile"

    iget-wide v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "lowResWidth"

    iget v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "highResWidth"

    iget v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fetchBioSuggestion"

    iget-boolean v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->d:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fetchFavPhotoSuggestions"

    iget-boolean v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->e:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topContextItemType"

    iget-object v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->f:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1027495
    iget-wide v4, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1027496
    iget v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1027497
    iget v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1027498
    iget-boolean v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1027499
    iget-boolean v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1027500
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1027501
    return-void

    :cond_0
    move v0, v2

    .line 1027502
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1027503
    goto :goto_1
.end method
