.class public final Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e67de9a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1024933
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1024932
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1024930
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1024931
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024928
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    .line 1024929
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1024916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1024917
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1024918
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1024919
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1024920
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1024921
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1024922
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1024923
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1024924
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1024925
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1024926
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1024927
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1024908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1024909
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1024910
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    .line 1024911
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1024912
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;

    .line 1024913
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    .line 1024914
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1024915
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024905
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1024906
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1024907
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1024895
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;-><init>()V

    .line 1024896
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1024897
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024904
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024902
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->g:Ljava/lang/String;

    .line 1024903
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024900
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->h:Ljava/lang/String;

    .line 1024901
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1024899
    const v0, 0x295dbbe0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1024898
    const v0, -0x6829c9fb

    return v0
.end method
