.class public final Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f1c90cf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032695
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032696
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1032697
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1032698
    return-void
.end method

.method private a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileQuestions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1032699
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    .line 1032700
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1032701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032702
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1032703
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1032704
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1032705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032706
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1032707
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032708
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1032709
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    .line 1032710
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1032711
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;

    .line 1032712
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    .line 1032713
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032714
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1032715
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel;-><init>()V

    .line 1032716
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1032717
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1032718
    const v0, 0x39b00d18

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1032719
    const v0, -0x6747e1ce

    return v0
.end method
