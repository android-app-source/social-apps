.class public final Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7c244cf9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1025767
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1025766
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1025764
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1025765
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1025755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1025756
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x33d5023d

    invoke-static {v1, v0, v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1025757
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x657b17f1

    invoke-static {v2, v1, v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1025758
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1025759
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1025760
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1025761
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1025762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1025763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1025739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1025740
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1025741
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x33d5023d

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1025742
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1025743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    .line 1025744
    iput v3, v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->e:I

    move-object v1, v0

    .line 1025745
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1025746
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x657b17f1

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1025747
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1025748
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    .line 1025749
    iput v3, v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->g:I

    move-object v1, v0

    .line 1025750
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1025751
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1025752
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1025753
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1025754
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1025737
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1025738
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1025732
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1025733
    const/4 v0, 0x0

    const v1, 0x33d5023d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->e:I

    .line 1025734
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->f:Z

    .line 1025735
    const/4 v0, 0x2

    const v1, -0x657b17f1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->g:I

    .line 1025736
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1025730
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1025731
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1025727
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;-><init>()V

    .line 1025728
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1025729
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1025723
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1025724
    iget-boolean v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->f:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1025726
    const v0, 0x2b781c45

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1025725
    const v0, -0x4db3e963

    return v0
.end method
