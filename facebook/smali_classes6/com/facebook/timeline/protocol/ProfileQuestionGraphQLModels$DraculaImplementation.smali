.class public final Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1031742
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1031743
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1031740
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1031741
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1031726
    if-nez p1, :cond_0

    .line 1031727
    :goto_0
    return v0

    .line 1031728
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1031729
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1031730
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1031731
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1031732
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1031733
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1031734
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1031735
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1031736
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1031737
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1031738
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1031739
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x735c0e20 -> :sswitch_1
        0x397793f5 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1031725
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1031722
    sparse-switch p0, :sswitch_data_0

    .line 1031723
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1031724
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x735c0e20 -> :sswitch_0
        0x397793f5 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1031688
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1031720
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->b(I)V

    .line 1031721
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1031715
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1031716
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1031717
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1031718
    iput p2, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->b:I

    .line 1031719
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1031714
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1031713
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1031710
    iget v0, p0, LX/1vt;->c:I

    .line 1031711
    move v0, v0

    .line 1031712
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1031707
    iget v0, p0, LX/1vt;->c:I

    .line 1031708
    move v0, v0

    .line 1031709
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1031704
    iget v0, p0, LX/1vt;->b:I

    .line 1031705
    move v0, v0

    .line 1031706
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031701
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1031702
    move-object v0, v0

    .line 1031703
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1031692
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1031693
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1031694
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1031695
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1031696
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1031697
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1031698
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1031699
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1031700
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1031689
    iget v0, p0, LX/1vt;->c:I

    .line 1031690
    move v0, v0

    .line 1031691
    return v0
.end method
