.class public final Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1029989
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1029990
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1029991
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1029992
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1029993
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/5xs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1029994
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1029995
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel$Serializer;->a(Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;LX/0nX;LX/0my;)V

    return-void
.end method
