.class public final Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1030557
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1030558
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1030580
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1030581
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1030563
    if-nez p1, :cond_0

    .line 1030564
    :goto_0
    return v0

    .line 1030565
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1030566
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1030567
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1030568
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1030569
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1030570
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1030571
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1030572
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1030573
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1030574
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1030575
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1030576
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1030577
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1030578
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1030579
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x68447cb2 -> :sswitch_0
        -0xf2644b -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1030562
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1030559
    sparse-switch p0, :sswitch_data_0

    .line 1030560
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1030561
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x68447cb2 -> :sswitch_0
        -0xf2644b -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1030556
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1030554
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->b(I)V

    .line 1030555
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1030582
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1030583
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1030584
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1030585
    iput p2, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->b:I

    .line 1030586
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1030553
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1030552
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1030549
    iget v0, p0, LX/1vt;->c:I

    .line 1030550
    move v0, v0

    .line 1030551
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1030528
    iget v0, p0, LX/1vt;->c:I

    .line 1030529
    move v0, v0

    .line 1030530
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1030546
    iget v0, p0, LX/1vt;->b:I

    .line 1030547
    move v0, v0

    .line 1030548
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030543
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1030544
    move-object v0, v0

    .line 1030545
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1030534
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1030535
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1030536
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1030537
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1030538
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1030539
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1030540
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1030541
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1030542
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1030531
    iget v0, p0, LX/1vt;->c:I

    .line 1030532
    move v0, v0

    .line 1030533
    return v0
.end method
