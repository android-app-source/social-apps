.class public final Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6754432e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1029732
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1029731
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1029729
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1029730
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1029723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1029724
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1029725
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1029726
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1029727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1029728
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1029715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1029716
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1029717
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 1029718
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1029719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;

    .line 1029720
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 1029721
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1029722
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1029733
    new-instance v0, LX/5xh;

    invoke-direct {v0, p1}, LX/5xh;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1029713
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 1029714
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1029711
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1029712
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1029710
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1029707
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetQueryModel;-><init>()V

    .line 1029708
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1029709
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1029706
    const v0, 0x5f0613ac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1029705
    const v0, 0x285feb

    return v0
.end method
