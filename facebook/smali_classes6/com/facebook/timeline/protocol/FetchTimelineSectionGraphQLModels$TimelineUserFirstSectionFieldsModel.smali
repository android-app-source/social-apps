.class public final Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x661f98df
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028940
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028939
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1028937
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1028938
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028910
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->g:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->g:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    .line 1028911
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->g:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1028927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028928
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1028929
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1028930
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1028931
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1028932
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1028933
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1028934
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1028935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028936
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1028919
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028920
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1028921
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    .line 1028922
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1028923
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;

    .line 1028924
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->g:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    .line 1028925
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028926
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028941
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1028916
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;-><init>()V

    .line 1028917
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1028918
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028914
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->e:Ljava/lang/String;

    .line 1028915
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028912
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->f:Ljava/lang/String;

    .line 1028913
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028909
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstSectionFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineUserFirstUnitsConnectionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1028908
    const v0, -0x2a9e7113

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1028907
    const v0, 0x4357df44

    return v0
.end method
