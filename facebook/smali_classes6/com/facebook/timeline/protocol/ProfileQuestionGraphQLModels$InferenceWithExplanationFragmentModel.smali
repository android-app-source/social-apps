.class public final Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x61e8e376
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031952
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031951
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1031949
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1031950
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1031946
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1031947
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1031948
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1031878
    if-nez p0, :cond_0

    .line 1031879
    const/4 p0, 0x0

    .line 1031880
    :goto_0
    return-object p0

    .line 1031881
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    if-eqz v0, :cond_1

    .line 1031882
    check-cast p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    goto :goto_0

    .line 1031883
    :cond_1
    new-instance v0, LX/5yS;

    invoke-direct {v0}, LX/5yS;-><init>()V

    .line 1031884
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->a()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5yS;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031885
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v1

    iput-object v1, v0, LX/5yS;->b:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    .line 1031886
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->b()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5yS;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031887
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1031888
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1031889
    iget-object v3, v0, LX/5yS;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1031890
    iget-object v5, v0, LX/5yS;->b:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1031891
    iget-object v7, v0, LX/5yS;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1031892
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1031893
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1031894
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1031895
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1031896
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1031897
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1031898
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1031899
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1031900
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1031901
    new-instance v3, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;-><init>(LX/15i;)V

    .line 1031902
    move-object p0, v3

    .line 1031903
    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031944
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031945
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031942
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    .line 1031943
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031940
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031941
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1031930
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031931
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1031932
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1031933
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1031934
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1031935
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1031936
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1031937
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1031938
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031939
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1031912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031913
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1031914
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031915
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1031916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1031917
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031918
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1031919
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    .line 1031920
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1031921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1031922
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    .line 1031923
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1031924
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031925
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1031926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1031927
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1031928
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031929
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031911
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031910
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1031907
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;-><init>()V

    .line 1031908
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1031909
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031906
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->k()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1031905
    const v0, 0xafbd053

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1031904
    const v0, 0x70b1144a

    return v0
.end method
