.class public final Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1031352
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;

    new-instance v1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1031353
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1031354
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1031355
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1031356
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1031357
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1031358
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1031359
    if-eqz v2, :cond_4

    .line 1031360
    const-string v3, "profile_intro_card"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1031361
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1031362
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1031363
    if-eqz v3, :cond_3

    .line 1031364
    const-string v4, "featured_photos"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1031365
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1031366
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1031367
    if-eqz v4, :cond_2

    .line 1031368
    const-string p0, "edges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1031369
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1031370
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1031371
    invoke-virtual {v1, v4, p0}, LX/15i;->q(II)I

    move-result v0

    .line 1031372
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1031373
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1031374
    if-eqz v2, :cond_0

    .line 1031375
    const-string v3, "node"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1031376
    invoke-static {v1, v2, p1}, LX/5yJ;->a(LX/15i;ILX/0nX;)V

    .line 1031377
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1031378
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1031379
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1031380
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1031381
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1031382
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1031383
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1031384
    check-cast p1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Serializer;->a(Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
