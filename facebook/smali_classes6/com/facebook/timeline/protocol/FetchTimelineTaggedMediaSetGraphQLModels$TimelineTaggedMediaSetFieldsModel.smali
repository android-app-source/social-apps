.class public final Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x72bffec3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1029663
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1029662
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1029660
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1029661
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1029658
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    .line 1029659
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1029638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1029639
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1029640
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1029641
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1029642
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1029643
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1029650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1029651
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1029652
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    .line 1029653
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1029654
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 1029655
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    .line 1029656
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1029657
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1029649
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1029646
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;-><init>()V

    .line 1029647
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1029648
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1029645
    const v0, 0x26941382

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1029644
    const v0, 0x28ddea18

    return v0
.end method
