.class public final Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47a080c6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1034315
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1034316
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1034317
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1034318
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1034319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1034320
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1034321
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1034322
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1034323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1034324
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1034325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1034326
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1034327
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    .line 1034328
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1034329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;

    .line 1034330
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->e:Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    .line 1034331
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1034332
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1034333
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->e:Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->e:Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    .line 1034334
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;->e:Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel$ViewerModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1034335
    new-instance v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSkipMutationModel;-><init>()V

    .line 1034336
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1034337
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1034338
    const v0, -0x7ff53085

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1034339
    const v0, -0x4dba1281

    return v0
.end method
