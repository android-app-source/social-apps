.class public final Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x548548d5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033317
    const-class v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033318
    const-class v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1033319
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033320
    return-void
.end method

.method private a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033321
    iget-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    .line 1033322
    iget-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1033323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033324
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1033325
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1033326
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1033327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033328
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1033329
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033330
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1033331
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    .line 1033332
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1033333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;

    .line 1033334
    iput-object v0, v1, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    .line 1033335
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033336
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1033337
    new-instance v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel;-><init>()V

    .line 1033338
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033339
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1033340
    const v0, -0x476059bd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1033341
    const v0, -0x6bba82df

    return v0
.end method
