.class public final Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d0cb530
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032156
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032155
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1032153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1032154
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1032150
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1032151
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1032152
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1032130
    if-nez p0, :cond_0

    .line 1032131
    const/4 p0, 0x0

    .line 1032132
    :goto_0
    return-object p0

    .line 1032133
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1032134
    check-cast p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    goto :goto_0

    .line 1032135
    :cond_1
    new-instance v0, LX/5yY;

    invoke-direct {v0}, LX/5yY;-><init>()V

    .line 1032136
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/5yY;->a:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1032137
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1032138
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1032139
    iget-object v3, v0, LX/5yY;->a:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1032140
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1032141
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1032142
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1032143
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1032144
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1032145
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1032146
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1032147
    new-instance v3, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;-><init>(LX/15i;)V

    .line 1032148
    move-object p0, v3

    .line 1032149
    goto :goto_0
.end method

.method private j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1032128
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1032129
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1032122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032123
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1032124
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1032125
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1032126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032127
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1032114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032115
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1032116
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1032117
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1032118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    .line 1032119
    iput-object v0, v1, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->e:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    .line 1032120
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032121
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1032108
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;->j()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1032111
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel$InferencesModel$EdgesModel;-><init>()V

    .line 1032112
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1032113
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1032110
    const v0, -0x44b028a2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1032109
    const v0, -0x468853a

    return v0
.end method
