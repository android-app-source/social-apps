.class public final Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52a40232
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032681
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1032653
    const-class v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1032654
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1032655
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1032656
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->f:Ljava/util/List;

    .line 1032657
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1032658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032659
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1032660
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1032661
    iget v1, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1032662
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1032663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032664
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1032665
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1032666
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1032667
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1032668
    if-eqz v1, :cond_0

    .line 1032669
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    .line 1032670
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->f:Ljava/util/List;

    .line 1032671
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1032672
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1032673
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1032674
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;->e:I

    .line 1032675
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1032676
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionQueryModel$ProfileQuestionsModel;-><init>()V

    .line 1032677
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1032678
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1032679
    const v0, 0x5632ed85

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1032680
    const v0, 0x7712dfc2

    return v0
.end method
