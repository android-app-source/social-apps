.class public final Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031351
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1031350
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1031348
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1031349
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031346
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1031347
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1031340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031341
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1031342
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1031343
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1031344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1031337
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1031338
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1031339
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1031331
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1031334
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$ProfileIntroCardModel$FeaturedPhotosModel$EdgesModel$NodeModel;-><init>()V

    .line 1031335
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1031336
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1031333
    const v0, 0xb3c990b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1031332
    const v0, 0x4984e12

    return v0
.end method
