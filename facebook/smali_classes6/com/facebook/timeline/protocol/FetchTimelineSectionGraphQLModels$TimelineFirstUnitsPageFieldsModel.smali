.class public final Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x337e8ea
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028536
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028535
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1028533
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1028534
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028531
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    .line 1028532
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1028523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028524
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1028525
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x48b1a19a

    invoke-static {v2, v1, v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionListGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineSectionListGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1028526
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1028527
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1028528
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1028529
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028530
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1028508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028509
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1028510
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    .line 1028511
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1028512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;

    .line 1028513
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    .line 1028514
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1028515
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x48b1a19a

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionListGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/FetchTimelineSectionListGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1028516
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1028517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;

    .line 1028518
    iput v3, v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->f:I

    move-object v1, v0

    .line 1028519
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028520
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1028521
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1028522
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1028507
    new-instance v0, LX/5xN;

    invoke-direct {v0, p1}, LX/5xN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028537
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1028504
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1028505
    const/4 v0, 0x1

    const v1, 0x48b1a19a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->f:I

    .line 1028506
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1028502
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1028503
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1028501
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028499
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1028500
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1028496
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;-><init>()V

    .line 1028497
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1028498
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1028495
    const v0, 0x5893c9bd    # 1.29995735E15f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1028494
    const v0, 0x25d6af

    return v0
.end method
