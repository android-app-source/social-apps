.class public final Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x188da04d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033313
    const-class v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033316
    const-class v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1033314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033315
    return-void
.end method

.method private a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033305
    iget-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    .line 1033306
    iget-object v0, p0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1033307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033308
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1033309
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1033310
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1033311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033312
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1033297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033298
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1033299
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    .line 1033300
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->a()Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1033301
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    .line 1033302
    iput-object v0, v1, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;->e:Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel$ActorModel;

    .line 1033303
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033304
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1033294
    new-instance v0, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/RevertTemporaryMutationModels$RevertTemporaryMutationModel$ViewerModel;-><init>()V

    .line 1033295
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033296
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1033293
    const v0, 0x51684b26

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1033292
    const v0, -0x6747e1ce

    return v0
.end method
