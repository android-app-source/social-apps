.class public final Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1031238
    const-class v0, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;

    new-instance v1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1031239
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1031240
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1031241
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1031242
    const/4 v2, 0x0

    .line 1031243
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1031244
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031245
    :goto_0
    move v1, v2

    .line 1031246
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1031247
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1031248
    new-instance v1, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;

    invoke-direct {v1}, Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;-><init>()V

    .line 1031249
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1031250
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1031251
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1031252
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1031253
    :cond_0
    return-object v1

    .line 1031254
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031255
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1031256
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1031257
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1031258
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1031259
    const-string v4, "profile_intro_card"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1031260
    const/4 v3, 0x0

    .line 1031261
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1031262
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031263
    :goto_2
    move v1, v3

    .line 1031264
    goto :goto_1

    .line 1031265
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1031266
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1031267
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1031268
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031269
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1031270
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1031271
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1031272
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1031273
    const-string v5, "featured_photos"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1031274
    const/4 v4, 0x0

    .line 1031275
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_d

    .line 1031276
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031277
    :goto_4
    move v1, v4

    .line 1031278
    goto :goto_3

    .line 1031279
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1031280
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1031281
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 1031282
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031283
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_c

    .line 1031284
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1031285
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1031286
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 1031287
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1031288
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1031289
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_b

    .line 1031290
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_b

    .line 1031291
    const/4 v6, 0x0

    .line 1031292
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_11

    .line 1031293
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031294
    :goto_7
    move v5, v6

    .line 1031295
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1031296
    :cond_b
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1031297
    goto :goto_5

    .line 1031298
    :cond_c
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1031299
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1031300
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v1, v4

    goto :goto_5

    .line 1031301
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1031302
    :cond_f
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_10

    .line 1031303
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1031304
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1031305
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v7, :cond_f

    .line 1031306
    const-string p0, "node"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1031307
    invoke-static {p1, v0}, LX/5yJ;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_8

    .line 1031308
    :cond_10
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1031309
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1031310
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_7

    :cond_11
    move v5, v6

    goto :goto_8
.end method
