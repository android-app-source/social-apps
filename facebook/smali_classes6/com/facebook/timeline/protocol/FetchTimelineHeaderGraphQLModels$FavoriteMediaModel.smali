.class public final Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d2042b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1024213
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1024214
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1024215
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1024216
    return-void
.end method

.method private j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024217
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 1024218
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024219
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    .line 1024220
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1024221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1024222
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1024223
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1024224
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1024225
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1024226
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1024227
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1024228
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1024229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1024230
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1024231
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 1024232
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1024233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    .line 1024234
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->e:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 1024235
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1024236
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    .line 1024237
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1024238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    .line 1024239
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->f:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    .line 1024240
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1024241
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1024242
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;->k()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1024243
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;-><init>()V

    .line 1024244
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1024245
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1024246
    const v0, -0x540c78e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1024247
    const v0, -0x362d10a0    # -1727980.0f

    return v0
.end method
