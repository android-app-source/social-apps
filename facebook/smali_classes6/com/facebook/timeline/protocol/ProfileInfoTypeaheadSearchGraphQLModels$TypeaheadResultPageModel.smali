.class public final Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3f402e41
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1030775
    const-class v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1030774
    const-class v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1030772
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1030773
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1030758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1030759
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x68447cb2

    invoke-static {v1, v0, v2}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1030760
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1030761
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1030762
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1030763
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0xf2644b

    invoke-static {v5, v4, v6}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1030764
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1030765
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1030766
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1030767
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1030768
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1030769
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1030770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1030771
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1030742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1030743
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1030744
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x68447cb2

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1030745
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1030746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    .line 1030747
    iput v3, v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e:I

    move-object v1, v0

    .line 1030748
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1030749
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xf2644b

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1030750
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->ck_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1030751
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    .line 1030752
    iput v3, v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->i:I

    move-object v1, v0

    .line 1030753
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1030754
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1030755
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1030756
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1030757
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1030741
    new-instance v0, LX/5y7;

    invoke-direct {v0, p1}, LX/5y7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030740
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1030736
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1030737
    const/4 v0, 0x0

    const v1, -0x68447cb2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e:I

    .line 1030738
    const/4 v0, 0x4

    const v1, -0xf2644b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->i:I

    .line 1030739
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1030733
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1030734
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1030735
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1030718
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->f:Ljava/util/List;

    .line 1030719
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1030720
    new-instance v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;-><init>()V

    .line 1030721
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1030722
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030723
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->g:Ljava/lang/String;

    .line 1030724
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final ck_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030725
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1030726
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030727
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->h:Ljava/lang/String;

    .line 1030728
    iget-object v0, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1030729
    const v0, 0x6acc0249

    return v0
.end method

.method public final e()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1030730
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1030731
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1030732
    const v0, 0x25d6af

    return v0
.end method
