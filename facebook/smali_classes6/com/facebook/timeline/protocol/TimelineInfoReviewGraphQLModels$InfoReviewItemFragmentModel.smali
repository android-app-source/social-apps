.class public final Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x46201fd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033665
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1033664
    const-class v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1033662
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033663
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1033659
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1033660
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033661
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;)Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1033641
    if-nez p0, :cond_0

    .line 1033642
    const/4 p0, 0x0

    .line 1033643
    :goto_0
    return-object p0

    .line 1033644
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    if-eqz v0, :cond_1

    .line 1033645
    check-cast p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    goto :goto_0

    .line 1033646
    :cond_1
    new-instance v0, LX/5z6;

    invoke-direct {v0}, LX/5z6;-><init>()V

    .line 1033647
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1033648
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->c()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1033649
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->c:Ljava/lang/String;

    .line 1033650
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/5z6;->d:Z

    .line 1033651
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->cn_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->e:Ljava/lang/String;

    .line 1033652
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;)Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 1033653
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->co_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->g:Ljava/lang/String;

    .line 1033654
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->h:Ljava/lang/String;

    .line 1033655
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033656
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->j:Ljava/lang/String;

    .line 1033657
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5z6;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033658
    invoke-virtual {v0}, LX/5z6;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    move-result-object p0

    goto :goto_0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033639
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1033640
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileQuestion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033637
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 1033638
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033635
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033636
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033633
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033634
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 1033608
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033609
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1033610
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1033611
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1033612
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->cn_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1033613
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1033614
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->co_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1033615
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1033616
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1033617
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1033618
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1033619
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1033620
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1033621
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1033622
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1033623
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1033624
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1033625
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1033626
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1033627
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1033628
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1033629
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1033630
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1033631
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1033585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1033586
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1033587
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1033588
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1033589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 1033590
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1033591
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1033592
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 1033593
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1033594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 1033595
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 1033596
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1033597
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033598
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1033599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 1033600
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033601
    :cond_2
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1033602
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033603
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1033604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 1033605
    iput-object v0, v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1033606
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1033607
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033584
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1033581
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1033582
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->h:Z

    .line 1033583
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033578
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1033579
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1033580
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1033557
    new-instance v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;-><init>()V

    .line 1033558
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1033559
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033577
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final cn_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033575
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->i:Ljava/lang/String;

    .line 1033576
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final co_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033573
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k:Ljava/lang/String;

    .line 1033574
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033571
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->g:Ljava/lang/String;

    .line 1033572
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1033570
    const v0, -0x47f5e4b0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1033568
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1033569
    iget-boolean v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1033567
    const v0, 0x79809a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033565
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->l:Ljava/lang/String;

    .line 1033566
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033564
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033562
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n:Ljava/lang/String;

    .line 1033563
    iget-object v0, p0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic m()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033561
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileQuestion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1033560
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->p()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v0

    return-object v0
.end method
