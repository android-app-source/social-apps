.class public final Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x18af0d21
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028769
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1028770
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1028771
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1028772
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1028773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028774
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v0

    .line 1028775
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1028776
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1028777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028778
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1028779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1028780
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1028781
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1028782
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1028783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;

    .line 1028784
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 1028785
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1028786
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028787
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->e:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x0

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 1028788
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->e:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1028789
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;-><init>()V

    .line 1028790
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1028791
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1028792
    const v0, -0x2194f8b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1028793
    const v0, -0x381ae918

    return v0
.end method
