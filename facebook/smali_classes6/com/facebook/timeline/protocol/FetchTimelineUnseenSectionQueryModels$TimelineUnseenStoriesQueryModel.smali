.class public final Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x37bc2d2a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1030049
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1030048
    const-class v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1030046
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1030047
    return-void
.end method

.method private j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030026
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    iput-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    .line 1030027
    iget-object v0, p0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1030040
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1030041
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1030042
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1030043
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1030044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1030045
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1030032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1030033
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1030034
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    .line 1030035
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1030036
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;

    .line 1030037
    iput-object v0, v1, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->e:Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    .line 1030038
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1030039
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1030050
    new-instance v0, LX/5xq;

    invoke-direct {v0, p1}, LX/5xq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1030031
    invoke-direct {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->j()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1030029
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1030030
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1030028
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1030023
    new-instance v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;-><init>()V

    .line 1030024
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1030025
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1030022
    const v0, 0x4b5e25bb    # 1.4558651E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1030021
    const v0, 0x285feb

    return v0
.end method
