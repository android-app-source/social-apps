.class public final Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1022423
    const-class v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel;

    new-instance v1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1022424
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1022425
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1022426
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1022427
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1022428
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1022429
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1022430
    if-eqz v2, :cond_0

    .line 1022431
    const-string p0, "album"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022432
    invoke-static {v1, v2, p1}, LX/5vc;->a(LX/15i;ILX/0nX;)V

    .line 1022433
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1022434
    if-eqz v2, :cond_1

    .line 1022435
    const-string p0, "image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022436
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1022437
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1022438
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1022439
    check-cast p1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$Serializer;->a(Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
