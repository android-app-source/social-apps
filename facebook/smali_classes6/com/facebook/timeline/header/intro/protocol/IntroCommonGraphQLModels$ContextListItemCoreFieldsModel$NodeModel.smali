.class public final Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5vV;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ef4c172
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1022190
    const-class v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1022191
    const-class v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1022192
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1022193
    return-void
.end method

.method private j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022194
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->f:Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->f:Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    .line 1022195
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->f:Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022196
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1022197
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1022198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1022199
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1022200
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1022201
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1022202
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1022203
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1022204
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1022205
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1022206
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1022207
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1022208
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1022209
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1022210
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1022211
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1022212
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    .line 1022213
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1022214
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    .line 1022215
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->f:Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    .line 1022216
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1022217
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1022218
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1022219
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    .line 1022220
    iput-object v0, v1, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1022221
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1022222
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1022188
    new-instance v0, LX/5vY;

    invoke-direct {v0, p1}, LX/5vY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022189
    invoke-virtual {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1022186
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1022187
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1022185
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022182
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1022183
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1022184
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1022179
    new-instance v0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;-><init>()V

    .line 1022180
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1022181
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022178
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->j()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022176
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->g:Ljava/lang/String;

    .line 1022177
    iget-object v0, p0, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1022175
    const v0, -0x33558ab7    # -8.936916E7f

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022174
    invoke-direct {p0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1022173
    const v0, 0x252222

    return v0
.end method
