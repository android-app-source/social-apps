.class public final Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1034850
    const-class v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    new-instance v1, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1034851
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1034848
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1034852
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1034853
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0xe

    const/16 v5, 0xd

    const/4 v4, 0x7

    .line 1034854
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1034855
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034856
    if-eqz v2, :cond_0

    .line 1034857
    const-string v3, "can_viewer_act_as_memorial_contact"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034858
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034859
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034860
    if-eqz v2, :cond_1

    .line 1034861
    const-string v3, "can_viewer_block"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034862
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034863
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034864
    if-eqz v2, :cond_2

    .line 1034865
    const-string v3, "can_viewer_message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034866
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034867
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034868
    if-eqz v2, :cond_3

    .line 1034869
    const-string v3, "can_viewer_poke"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034870
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034871
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034872
    if-eqz v2, :cond_4

    .line 1034873
    const-string v3, "can_viewer_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034874
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034875
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034876
    if-eqz v2, :cond_5

    .line 1034877
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034878
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034879
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1034880
    if-eqz v2, :cond_6

    .line 1034881
    const-string v3, "friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034882
    invoke-static {v1, v2, p1}, LX/5zW;->a(LX/15i;ILX/0nX;)V

    .line 1034883
    :cond_6
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1034884
    if-eqz v2, :cond_7

    .line 1034885
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034886
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034887
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1034888
    if-eqz v2, :cond_8

    .line 1034889
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034890
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034891
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034892
    if-eqz v2, :cond_9

    .line 1034893
    const-string v3, "is_followed_by_everyone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034894
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034895
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034896
    if-eqz v2, :cond_a

    .line 1034897
    const-string v3, "is_viewer_coworker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034898
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034899
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1034900
    if-eqz v2, :cond_b

    .line 1034901
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034902
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034903
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1034904
    if-eqz v2, :cond_c

    .line 1034905
    const-string v3, "posted_item_privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034906
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1034907
    :cond_c
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1034908
    if-eqz v2, :cond_d

    .line 1034909
    const-string v2, "secondary_subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034910
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034911
    :cond_d
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1034912
    if-eqz v2, :cond_e

    .line 1034913
    const-string v2, "subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034914
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034915
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1034916
    if-eqz v2, :cond_f

    .line 1034917
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034918
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1034919
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1034920
    if-eqz v2, :cond_10

    .line 1034921
    const-string v3, "viewer_can_see_profile_insights"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1034922
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1034923
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1034924
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1034849
    check-cast p1, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel$Serializer;->a(Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
