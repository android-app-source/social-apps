.class public final Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5vo;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x761c0a79
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1023131
    const-class v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1023110
    const-class v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1023111
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1023112
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1023113
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1023114
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1023115
    return-void
.end method

.method public static a(LX/5vo;)Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;
    .locals 2

    .prologue
    .line 1023116
    if-nez p0, :cond_0

    .line 1023117
    const/4 p0, 0x0

    .line 1023118
    :goto_0
    return-object p0

    .line 1023119
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    if-eqz v0, :cond_1

    .line 1023120
    check-cast p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    goto :goto_0

    .line 1023121
    :cond_1
    new-instance v0, LX/5vr;

    invoke-direct {v0}, LX/5vr;-><init>()V

    .line 1023122
    invoke-interface {p0}, LX/5vo;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1023123
    invoke-interface {p0}, LX/5vo;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->b:Ljava/lang/String;

    .line 1023124
    invoke-interface {p0}, LX/5vo;->c()LX/1f8;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a(LX/1f8;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1023125
    invoke-interface {p0}, LX/5vo;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->d:Ljava/lang/String;

    .line 1023126
    invoke-interface {p0}, LX/5vo;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023127
    invoke-interface {p0}, LX/5vo;->aj_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023128
    invoke-interface {p0}, LX/5vo;->ai_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023129
    invoke-interface {p0}, LX/5vo;->j()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5vr;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023130
    invoke-virtual {v0}, LX/5vr;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    move-result-object p0

    goto :goto_0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023107
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1023108
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023132
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023133
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023134
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023135
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023136
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023137
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023138
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023139
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1023140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1023141
    invoke-virtual {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1023142
    invoke-virtual {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1023143
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1023144
    invoke-virtual {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1023145
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1023146
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1023147
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1023148
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1023149
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1023150
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1023151
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1023152
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1023153
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1023154
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1023155
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1023156
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1023157
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1023158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1023159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1023160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1023161
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1023162
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1023163
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1023164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 1023165
    iput-object v0, v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1023166
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1023167
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023168
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1023169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 1023170
    iput-object v0, v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023171
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1023172
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023173
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1023174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 1023175
    iput-object v0, v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023176
    :cond_2
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1023177
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023178
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1023179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 1023180
    iput-object v0, v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023181
    :cond_3
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1023182
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023183
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1023184
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 1023185
    iput-object v0, v1, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1023186
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1023187
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1023188
    new-instance v0, LX/5vs;

    invoke-direct {v0, p1}, LX/5vs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023109
    invoke-virtual {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1023087
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1023088
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1023091
    return-void
.end method

.method public final synthetic ai_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023092
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aj_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023093
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023094
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1023095
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1023096
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1023097
    new-instance v0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;-><init>()V

    .line 1023098
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1023099
    return-object v0
.end method

.method public final synthetic c()LX/1f8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023100
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023089
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->h:Ljava/lang/String;

    .line 1023090
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1023101
    const v0, 0x1716c1eb

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023102
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1023103
    const v0, 0x4984e12

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023104
    invoke-direct {p0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1023105
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->f:Ljava/lang/String;

    .line 1023106
    iget-object v0, p0, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method
