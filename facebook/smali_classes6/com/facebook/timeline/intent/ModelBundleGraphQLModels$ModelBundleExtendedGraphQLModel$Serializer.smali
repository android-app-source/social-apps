.class public final Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1022712
    const-class v0, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    new-instance v1, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1022713
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1022711
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1022682
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1022683
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    const/4 v3, 0x0

    .line 1022684
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1022685
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1022686
    if-eqz v2, :cond_0

    .line 1022687
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022688
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1022689
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1022690
    if-eqz v2, :cond_1

    .line 1022691
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022692
    invoke-static {v1, v2, p1, p2}, LX/5wq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1022693
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1022694
    if-eqz v2, :cond_2

    .line 1022695
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022696
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022697
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1022698
    if-eqz v2, :cond_3

    .line 1022699
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022700
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022701
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1022702
    if-eqz v2, :cond_4

    .line 1022703
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022704
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022705
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1022706
    if-eqz v2, :cond_5

    .line 1022707
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022708
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1022709
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1022710
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1022681
    check-cast p1, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel$Serializer;->a(Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
