.class public final Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1022848
    const-class v0, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;

    new-instance v1, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1022849
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1022850
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1022851
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1022852
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 1022853
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1022854
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1022855
    if-eqz v2, :cond_0

    .line 1022856
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022857
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1022858
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1022859
    if-eqz v2, :cond_1

    .line 1022860
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022861
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022862
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1022863
    if-eqz v2, :cond_2

    .line 1022864
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022865
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022866
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1022867
    if-eqz v2, :cond_3

    .line 1022868
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022869
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1022870
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1022871
    if-eqz v2, :cond_4

    .line 1022872
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1022873
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1022874
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1022875
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1022876
    check-cast p1, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel$Serializer;->a(Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
