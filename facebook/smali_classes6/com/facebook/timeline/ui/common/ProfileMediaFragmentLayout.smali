.class public Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1034524
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034525
    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034526
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1034527
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1034528
    invoke-direct {p0, p1, p2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1034513
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1034514
    invoke-direct {p0, p1, p2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034515
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1034516
    if-nez p2, :cond_0

    .line 1034517
    iput v2, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a:I

    .line 1034518
    iput v2, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b:I

    .line 1034519
    :goto_0
    return-void

    .line 1034520
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->ProfileMediaFragmentLayout:[I

    invoke-virtual {v0, p2, v1, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1034521
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a:I

    .line 1034522
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b:I

    .line 1034523
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1034509
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1034510
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v3, :cond_0

    :goto_0
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->measureChild(Landroid/view/View;II)V

    .line 1034511
    return-void

    :cond_0
    move p2, v1

    .line 1034512
    goto :goto_0

    :cond_1
    move p3, v1

    goto :goto_1
.end method

.method private static b(Landroid/view/View;II)V
    .locals 2

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 1034507
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1034508
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 8

    .prologue
    .line 1034486
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1034487
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1034488
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1034489
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a(Landroid/view/View;II)V

    .line 1034490
    invoke-direct {p0, v2, p1, p2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a(Landroid/view/View;II)V

    .line 1034491
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1034492
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1034493
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 1034494
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1034495
    add-int v7, v5, v6

    add-int/2addr v7, v4

    .line 1034496
    if-gt v7, v3, :cond_0

    .line 1034497
    sub-int/2addr v3, v7

    .line 1034498
    iget v5, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a:I

    mul-int/2addr v5, v3

    iget v6, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a:I

    iget v7, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b:I

    add-int/2addr v6, v7

    div-int/2addr v5, v6

    .line 1034499
    iget v6, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b:I

    mul-int/2addr v3, v6

    iget v6, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->a:I

    iget v7, p0, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b:I

    add-int/2addr v6, v7

    div-int/2addr v3, v6

    .line 1034500
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v5, v7

    invoke-static {v0, v6, v5}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b(Landroid/view/View;II)V

    .line 1034501
    invoke-static {v1, v4, v4}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b(Landroid/view/View;II)V

    .line 1034502
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v3

    invoke-static {v2, v0, v1}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b(Landroid/view/View;II)V

    .line 1034503
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->setMeasuredDimension(II)V

    .line 1034504
    return-void

    .line 1034505
    :cond_0
    sub-int v0, v3, v5

    sub-int/2addr v0, v6

    .line 1034506
    invoke-static {v1, v0, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->b(Landroid/view/View;II)V

    goto :goto_0
.end method
