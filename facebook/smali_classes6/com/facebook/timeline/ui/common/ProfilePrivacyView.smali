.class public Lcom/facebook/timeline/ui/common/ProfilePrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field public b:LX/0hs;

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1034543
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1034544
    invoke-direct {p0, p1}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a(Landroid/content/Context;)V

    .line 1034545
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1034546
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034547
    invoke-direct {p0, p1}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a(Landroid/content/Context;)V

    .line 1034548
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1034549
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1034550
    invoke-direct {p0, p1}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a(Landroid/content/Context;)V

    .line 1034551
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1034552
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->setWillNotDraw(Z)V

    .line 1034553
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 1034554
    if-eqz v0, :cond_0

    const v0, 0x7f0813d5

    :goto_0
    iput v0, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->c:I

    .line 1034555
    new-instance v0, LX/5zQ;

    invoke-direct {v0, p0, p1}, LX/5zQ;-><init>(Lcom/facebook/timeline/ui/common/ProfilePrivacyView;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1034556
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a:Landroid/graphics/Paint;

    .line 1034557
    iget-object v0, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1034558
    iget-object v0, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1034559
    return-void

    .line 1034560
    :cond_0
    const v0, 0x7f0813d4

    goto :goto_0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1034561
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1034562
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1034563
    return-void
.end method

.method public setFixedPrivacyToolTipDescriptionResource(I)V
    .locals 0

    .prologue
    .line 1034564
    iput p1, p0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->c:I

    .line 1034565
    return-void
.end method

.method public setPrivacyIcon(I)V
    .locals 1

    .prologue
    .line 1034566
    const v0, 0x7f0d276b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1034567
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1034568
    return-void
.end method

.method public setPrivacyText(I)V
    .locals 1

    .prologue
    .line 1034569
    const v0, 0x7f0d276c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1034570
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1034571
    return-void
.end method
