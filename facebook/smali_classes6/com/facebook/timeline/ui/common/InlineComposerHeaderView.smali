.class public Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1034458
    const-class v0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    const-string v1, "inline_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1034459
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034460
    invoke-direct {p0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a()V

    .line 1034461
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1034462
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1034463
    invoke-direct {p0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a()V

    .line 1034464
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1034465
    const v0, 0x7f030635

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1034466
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setOrientation(I)V

    .line 1034467
    const v0, 0x7f0d111b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1034468
    const v0, 0x7f0d111c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1034469
    const v0, 0x7f0d111e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1034470
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1034471
    invoke-virtual {p0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1034472
    const v1, 0x7f0a015a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1034473
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1034474
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1034475
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1034476
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1034477
    iget-object v1, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1034478
    invoke-direct {p0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->b()V

    .line 1034479
    return-void
.end method

.method public setComposerHintText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1034480
    iget-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1034481
    return-void
.end method

.method public setHeaderSectionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1034482
    iget-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1034483
    return-void
.end method

.method public setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1034484
    iget-object v0, p0, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1034485
    return-void
.end method
