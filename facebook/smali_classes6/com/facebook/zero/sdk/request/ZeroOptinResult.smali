.class public Lcom/facebook/zero/sdk/request/ZeroOptinResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/sdk/request/ZeroOptinResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "status"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1044295
    const-class v0, Lcom/facebook/zero/sdk/request/ZeroOptinResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1044284
    new-instance v0, LX/64C;

    invoke-direct {v0}, LX/64C;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1044292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->mStatus:Ljava/lang/String;

    .line 1044294
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1044289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044290
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->mStatus:Ljava/lang/String;

    .line 1044291
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1044288
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1044287
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1044285
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->mStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1044286
    return-void
.end method
