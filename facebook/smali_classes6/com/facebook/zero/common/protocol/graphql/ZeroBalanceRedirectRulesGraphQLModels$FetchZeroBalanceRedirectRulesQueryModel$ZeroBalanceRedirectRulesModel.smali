.class public final Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x35f02156
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1043834
    const-class v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1043812
    const-class v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1043832
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1043833
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1043830
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1043831
    iget v0, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1043823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1043824
    invoke-virtual {p0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1043825
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1043826
    iget v1, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1043827
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1043828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1043829
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1043835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1043836
    invoke-virtual {p0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1043837
    invoke-virtual {p0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1043838
    if-eqz v1, :cond_0

    .line 1043839
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;

    .line 1043840
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->f:Ljava/util/List;

    .line 1043841
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1043842
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1043820
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1043821
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->e:I

    .line 1043822
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1043817
    new-instance v0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;

    invoke-direct {v0}, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;-><init>()V

    .line 1043818
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1043819
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1043816
    const v0, 0x1e4e56

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1043815
    const v0, -0x48ab243f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1043813
    iget-object v0, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel$ZeroIpPoolHostRegexesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->f:Ljava/util/List;

    .line 1043814
    iget-object v0, p0, Lcom/facebook/zero/common/protocol/graphql/ZeroBalanceRedirectRulesGraphQLModels$FetchZeroBalanceRedirectRulesQueryModel$ZeroBalanceRedirectRulesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
