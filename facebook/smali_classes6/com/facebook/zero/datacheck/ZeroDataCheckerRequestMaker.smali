.class public Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile h:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0TD;

.field public final e:LX/644;

.field private final f:LX/64A;

.field public final g:LX/640;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1044146
    const-class v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    sput-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->a:Ljava/lang/Class;

    .line 1044147
    const-class v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0TD;LX/644;LX/64A;LX/640;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0TD;",
            "LX/644;",
            "LX/64A;",
            "LX/640;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044116
    iput-object p1, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->c:LX/0Ot;

    .line 1044117
    iput-object p2, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->d:LX/0TD;

    .line 1044118
    iput-object p3, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->e:LX/644;

    .line 1044119
    iput-object p4, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->f:LX/64A;

    .line 1044120
    iput-object p5, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->g:LX/640;

    .line 1044121
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;
    .locals 10

    .prologue
    .line 1044122
    sget-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->h:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    if-nez v0, :cond_1

    .line 1044123
    const-class v1, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    monitor-enter v1

    .line 1044124
    :try_start_0
    sget-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->h:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1044125
    if-eqz v2, :cond_0

    .line 1044126
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1044127
    new-instance v3, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    .line 1044128
    new-instance v6, LX/644;

    invoke-direct {v6}, LX/644;-><init>()V

    .line 1044129
    move-object v6, v6

    .line 1044130
    move-object v6, v6

    .line 1044131
    check-cast v6, LX/644;

    .line 1044132
    new-instance v7, LX/64A;

    invoke-direct {v7}, LX/64A;-><init>()V

    .line 1044133
    move-object v7, v7

    .line 1044134
    move-object v7, v7

    .line 1044135
    check-cast v7, LX/64A;

    .line 1044136
    new-instance v9, LX/640;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v8

    check-cast v8, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {v9, v8}, LX/640;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 1044137
    move-object v8, v9

    .line 1044138
    check-cast v8, LX/640;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;-><init>(LX/0Ot;LX/0TD;LX/644;LX/64A;LX/640;)V

    .line 1044139
    move-object v0, v3

    .line 1044140
    sput-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->h:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1044141
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1044142
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1044143
    :cond_1
    sget-object v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->h:Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    return-object v0

    .line 1044144
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1044145
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;
    .locals 2
    .param p1    # LX/0e6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 1044114
    iget-object v0, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    sget-object v1, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, p2, p3, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044112
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 1044113
    iget-object v1, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->d:LX/0TD;

    new-instance v2, LX/641;

    invoke-direct {v2, p0, v0}, LX/641;-><init>(Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;LX/14U;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1044109
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 1044110
    sget-object v1, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v0, v1}, LX/14U;->a(LX/14V;)V

    .line 1044111
    iget-object v1, p0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->d:LX/0TD;

    new-instance v2, LX/642;

    invoke-direct {v2, p0, v0}, LX/642;-><init>(Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;LX/14U;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
