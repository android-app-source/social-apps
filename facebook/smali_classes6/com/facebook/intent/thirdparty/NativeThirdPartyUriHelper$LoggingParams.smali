.class public final Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Landroid/os/Bundle;

.field public c:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108698
    new-instance v0, LX/6Xf;

    invoke-direct {v0}, LX/6Xf;-><init>()V

    sput-object v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1108699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108700
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->a:Z

    .line 1108701
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    .line 1108702
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    .line 1108703
    return-void

    .line 1108704
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1108705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108706
    iput-boolean p1, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->a:Z

    .line 1108707
    iput-object p2, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    .line 1108708
    iput-object p3, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    .line 1108709
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1108710
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1108711
    iget-boolean v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1108712
    iget-object v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 1108713
    iget-object v0, p0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 1108714
    return-void

    .line 1108715
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
