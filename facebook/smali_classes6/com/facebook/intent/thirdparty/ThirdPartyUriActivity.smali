.class public Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/17d;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1108839
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1108840
    const-string v0, "extra_app_name"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1108841
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1108842
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 1108843
    :cond_0
    return-object p2
.end method

.method private a(LX/6Xg;)V
    .locals 4

    .prologue
    .line 1108844
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance v1, LX/6Xl;

    invoke-direct {v1, p0}, LX/6Xl;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f081a0d

    new-instance v2, LX/6Xk;

    invoke-direct {v2, p0}, LX/6Xk;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 1108845
    iget-object v1, p1, LX/6Xg;->a:Landroid/content/Intent;

    move-object v1, v1

    .line 1108846
    if-eqz v1, :cond_0

    .line 1108847
    const v1, 0x7f081a10

    invoke-virtual {p0, v1}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1108848
    iget-object v2, p1, LX/6Xg;->a:Landroid/content/Intent;

    move-object v2, v2

    .line 1108849
    const v3, 0x7f081a11

    invoke-virtual {p0, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1108850
    const v2, 0x7f081a0e

    invoke-virtual {v0, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081a0f

    new-instance v3, LX/6Xm;

    invoke-direct {v3, p0, p1}, LX/6Xm;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;LX/6Xg;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1108851
    :goto_0
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1108852
    :goto_1
    return-void

    .line 1108853
    :cond_0
    iget-object v1, p1, LX/6Xg;->b:Landroid/content/Intent;

    move-object v1, v1

    .line 1108854
    if-eqz v1, :cond_1

    .line 1108855
    const v1, 0x7f081a14

    invoke-virtual {p0, v1}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1108856
    iget-object v2, p1, LX/6Xg;->b:Landroid/content/Intent;

    move-object v2, v2

    .line 1108857
    const v3, 0x7f081a15

    invoke-virtual {p0, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1108858
    const v2, 0x7f081a12

    invoke-virtual {v0, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081a13

    new-instance v3, LX/6Xn;

    invoke-direct {v3, p0, p1}, LX/6Xn;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;LX/6Xg;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_0

    .line 1108859
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->finish()V

    goto :goto_1
.end method

.method private static a(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/17d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1108838
    iput-object p1, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->q:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->r:LX/17d;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x97

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v1

    check-cast v1, LX/17d;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/17d;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108774
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081a07

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v2, LX/6Xj;

    invoke-direct {v2, p0, p2}, LX/6Xj;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;Ljava/util/List;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/6Xi;

    invoke-direct {v1, p0}, LX/6Xi;-><init>(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1108775
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1108776
    return-void
.end method

.method public static c(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1108817
    const-string v0, "extra_app_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1108818
    invoke-virtual {p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_logging_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;

    .line 1108819
    const/4 v1, 0x0

    .line 1108820
    if-eqz v0, :cond_3

    .line 1108821
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 1108822
    iget-boolean v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->a:Z

    if-eqz v1, :cond_0

    .line 1108823
    const-string v1, "sponsored"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108824
    :cond_0
    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1108825
    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1108826
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->b:Landroid/os/Bundle;

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 1108827
    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1108828
    :catch_0
    move-exception v1

    .line 1108829
    sget-object v4, LX/17d;->a:Ljava/lang/Class;

    const-string v5, "Error parsing extra logging parameters"

    invoke-static {v4, v5, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1108830
    :cond_1
    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1108831
    iget-object v1, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1108832
    iget-object v4, v0, Lcom/facebook/intent/thirdparty/NativeThirdPartyUriHelper$LoggingParams;->c:Landroid/os/Bundle;

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1108833
    :cond_2
    move-object v0, v2

    .line 1108834
    :goto_2
    iget-object v1, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->q:LX/0Ot;

    invoke-static {p0, p1, v1, v0}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;Ljava/util/Map;)V

    .line 1108835
    iget-object v0, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1108836
    invoke-virtual {p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->finish()V

    .line 1108837
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1108777
    invoke-static {p0, p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1108778
    iget-object v0, p0, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->r:LX/17d;

    invoke-virtual {p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1108779
    invoke-static {v1}, LX/17d;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1108780
    const/4 v2, 0x0

    .line 1108781
    :goto_0
    move-object v0, v2

    .line 1108782
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1108783
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1108784
    iget-object v3, v0, LX/6Xg;->c:Landroid/content/Intent;

    move-object v3, v3

    .line 1108785
    if-eqz v3, :cond_1

    .line 1108786
    const v3, 0x7f081a08

    invoke-virtual {p0, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108787
    iget-object v3, v0, LX/6Xg;->c:Landroid/content/Intent;

    move-object v3, v3

    .line 1108788
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108789
    iget-object v3, v0, LX/6Xg;->a:Landroid/content/Intent;

    move-object v3, v3

    .line 1108790
    if-eqz v3, :cond_2

    .line 1108791
    const v3, 0x7f081a09

    invoke-virtual {p0, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1108792
    iget-object v4, v0, LX/6Xg;->a:Landroid/content/Intent;

    move-object v4, v4

    .line 1108793
    const v5, 0x7f081a0a

    invoke-virtual {p0, v5}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1108794
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108795
    iget-object v3, v0, LX/6Xg;->a:Landroid/content/Intent;

    move-object v0, v3

    .line 1108796
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108797
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1108798
    invoke-virtual {p0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->finish()V

    .line 1108799
    :goto_2
    return-void

    .line 1108800
    :cond_1
    invoke-direct {p0, v0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(LX/6Xg;)V

    goto :goto_2

    .line 1108801
    :cond_2
    iget-object v3, v0, LX/6Xg;->b:Landroid/content/Intent;

    move-object v3, v3

    .line 1108802
    if-eqz v3, :cond_0

    .line 1108803
    const v3, 0x7f081a0b

    invoke-virtual {p0, v3}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1108804
    iget-object v4, v0, LX/6Xg;->b:Landroid/content/Intent;

    move-object v4, v4

    .line 1108805
    const v5, 0x7f081a0c

    invoke-virtual {p0, v5}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1108806
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1108807
    iget-object v3, v0, LX/6Xg;->b:Landroid/content/Intent;

    move-object v0, v3

    .line 1108808
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1108809
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_4

    .line 1108810
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->c(Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;Landroid/content/Intent;)V

    goto :goto_2

    .line 1108811
    :cond_4
    invoke-direct {p0, v1, v2}, Lcom/facebook/intent/thirdparty/ThirdPartyUriActivity;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_2

    .line 1108812
    :cond_5
    invoke-static {v0, v1}, LX/17d;->e(LX/17d;Landroid/net/Uri;)LX/31z;

    move-result-object v2

    .line 1108813
    invoke-static {p0, v1, v2}, LX/17d;->d(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v3

    .line 1108814
    invoke-static {p0, v1, v2}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v4

    .line 1108815
    invoke-static {p0, v1, v2}, LX/17d;->b(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v5

    .line 1108816
    new-instance v2, LX/6Xg;

    invoke-direct {v2, v3, v4, v5}, LX/6Xg;-><init>(Landroid/content/Intent;Landroid/content/Intent;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
