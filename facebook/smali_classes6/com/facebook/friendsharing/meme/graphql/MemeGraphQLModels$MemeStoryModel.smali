.class public final Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x30323c26
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 910279
    const-class v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 910278
    const-class v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 910276
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 910277
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 910270
    iput-object p1, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->f:Ljava/util/List;

    .line 910271
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 910272
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 910273
    if-eqz v0, :cond_0

    .line 910274
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 910275
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 910260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 910261
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 910262
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 910263
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 910264
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 910265
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 910266
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 910267
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 910268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 910269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 910247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 910248
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 910249
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 910250
    if-eqz v1, :cond_0

    .line 910251
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;

    .line 910252
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->e:Ljava/util/List;

    .line 910253
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 910254
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 910255
    if-eqz v1, :cond_1

    .line 910256
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;

    .line 910257
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->f:Ljava/util/List;

    .line 910258
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 910259
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 910246
    new-instance v0, LX/5P9;

    invoke-direct {v0, p1}, LX/5P9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 910228
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 910244
    invoke-virtual {p2}, LX/18L;->a()V

    .line 910245
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 910241
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 910242
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->a(Ljava/util/List;)V

    .line 910243
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 910240
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 910238
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->e:Ljava/util/List;

    .line 910239
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 910235
    new-instance v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;-><init>()V

    .line 910236
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 910237
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 910233
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->f:Ljava/util/List;

    .line 910234
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 910231
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->g:Ljava/lang/String;

    .line 910232
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 910230
    const v0, 0x3029a850

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 910229
    const v0, 0x4c808d5

    return v0
.end method
