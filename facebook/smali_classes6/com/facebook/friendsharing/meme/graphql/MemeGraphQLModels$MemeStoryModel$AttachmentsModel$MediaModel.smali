.class public final Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x65492645
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 910157
    const-class v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 910139
    const-class v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 910158
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 910159
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 910170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 910171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 910172
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 910160
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->f:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->f:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    .line 910161
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->f:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 910162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 910163
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 910164
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 910165
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 910166
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 910167
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 910168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 910169
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 910148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 910149
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 910150
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    .line 910151
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 910152
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    .line 910153
    iput-object v0, v1, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->f:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    .line 910154
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 910155
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 910156
    new-instance v0, LX/5P8;

    invoke-direct {v0, p1}, LX/5P8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 910147
    invoke-direct {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->k()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 910145
    invoke-virtual {p2}, LX/18L;->a()V

    .line 910146
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 910144
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 910141
    new-instance v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;-><init>()V

    .line 910142
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 910143
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 910140
    const v0, -0x6a31b802

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 910138
    const v0, 0x46c7fc4

    return v0
.end method
