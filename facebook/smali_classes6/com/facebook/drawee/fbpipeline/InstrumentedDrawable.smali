.class public Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;
.super LX/1ah;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;LX/0Zb;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V
    .locals 1

    .prologue
    .line 906389
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 906390
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->d:Z

    .line 906391
    iput-object p2, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->a:LX/0Zb;

    .line 906392
    iput-object p3, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    .line 906393
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 906394
    iget-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->d:Z

    if-nez v0, :cond_0

    .line 906395
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->d:Z

    .line 906396
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 906397
    invoke-virtual {p0, v0}, LX/1ah;->a(Landroid/graphics/RectF;)V

    .line 906398
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    .line 906399
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    .line 906400
    invoke-virtual {p0, v0}, LX/1ah;->b(Landroid/graphics/RectF;)V

    .line 906401
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    .line 906402
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    .line 906403
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->getIntrinsicWidth()I

    move-result v4

    .line 906404
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->getIntrinsicHeight()I

    move-result v5

    .line 906405
    iget-object v6, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->a:LX/0Zb;

    const-string v7, "android_instrumented_drawable"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 906406
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 906407
    const-string v7, "view_width"

    invoke-virtual {v6, v7, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906408
    const-string v1, "view_height"

    invoke-virtual {v6, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906409
    const-string v1, "scaled_width"

    invoke-virtual {v6, v1, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906410
    const-string v1, "scaled_height"

    invoke-virtual {v6, v1, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906411
    const-string v0, "image_width"

    invoke-virtual {v6, v0, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906412
    const-string v0, "image_height"

    invoke-virtual {v6, v0, v5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 906413
    const-string v0, "calling_class"

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    .line 906414
    iget-object v2, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v1, v2

    .line 906415
    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 906416
    const-string v0, "analytics_tag"

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 906417
    const-string v0, "module_tag"

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 906418
    const-string v0, "feature_tag"

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;->c:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 906419
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 906420
    :cond_0
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 906421
    return-void
.end method
