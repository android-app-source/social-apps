.class public Lcom/facebook/media/transcode/video/VideoEditConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/media/transcode/video/VideoEditConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:Landroid/graphics/RectF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113399
    new-instance v0, LX/6bL;

    invoke-direct {v0}, LX/6bL;-><init>()V

    sput-object v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1113365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(LX/6bM;)V
    .locals 1

    .prologue
    .line 1113375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113376
    iget-boolean v0, p1, LX/6bM;->a:Z

    move v0, v0

    .line 1113377
    iput-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    .line 1113378
    iget v0, p1, LX/6bM;->b:I

    move v0, v0

    .line 1113379
    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    .line 1113380
    iget v0, p1, LX/6bM;->c:I

    move v0, v0

    .line 1113381
    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    .line 1113382
    iget v0, p1, LX/6bM;->d:I

    move v0, v0

    .line 1113383
    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    .line 1113384
    iget-boolean v0, p1, LX/6bM;->e:Z

    move v0, v0

    .line 1113385
    iput-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    .line 1113386
    iget-object v0, p1, LX/6bM;->f:Landroid/graphics/RectF;

    move-object v0, v0

    .line 1113387
    iput-object v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    .line 1113388
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1113389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    .line 1113391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    .line 1113392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    .line 1113393
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    .line 1113394
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    .line 1113395
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    .line 1113396
    return-void

    :cond_0
    move v0, v2

    .line 1113397
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1113398
    goto :goto_1
.end method

.method public static a()Lcom/facebook/media/transcode/video/VideoEditConfig;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1113367
    new-instance v0, Lcom/facebook/media/transcode/video/VideoEditConfig;

    invoke-direct {v0}, Lcom/facebook/media/transcode/video/VideoEditConfig;-><init>()V

    .line 1113368
    iput-boolean v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    .line 1113369
    const/4 v1, -0x1

    iput v1, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    .line 1113370
    const/4 v1, -0x2

    iput v1, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    .line 1113371
    iput v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    .line 1113372
    iput-boolean v2, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    .line 1113373
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    .line 1113374
    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113366
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1113356
    iget-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113357
    iget v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113358
    iget v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113359
    iget v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113360
    iget-boolean v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113361
    iget-object v0, p0, Lcom/facebook/media/transcode/video/VideoEditConfig;->f:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113362
    return-void

    :cond_0
    move v0, v2

    .line 1113363
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1113364
    goto :goto_1
.end method
