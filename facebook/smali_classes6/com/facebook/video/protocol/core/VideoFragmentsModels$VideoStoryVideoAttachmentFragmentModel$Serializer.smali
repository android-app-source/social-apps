.class public final Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1037887
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel;

    new-instance v1, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1037888
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1037886
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel;LX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1037700
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1037701
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v11, 0x2b

    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 1037702
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1037703
    invoke-virtual {v1, v0, v5, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1037704
    cmp-long v4, v2, v8

    if-eqz v4, :cond_0

    .line 1037705
    const-string v4, "best_effort_time_taken"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037706
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1037707
    :cond_0
    invoke-virtual {v1, v0, v10}, LX/15i;->g(II)I

    move-result v2

    .line 1037708
    if-eqz v2, :cond_1

    .line 1037709
    const-string v2, "broadcast_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037710
    invoke-virtual {v1, v0, v10}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037711
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037712
    if-eqz v2, :cond_2

    .line 1037713
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037714
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037715
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037716
    if-eqz v2, :cond_3

    .line 1037717
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037718
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037719
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037720
    if-eqz v2, :cond_4

    .line 1037721
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037722
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037723
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1037724
    if-eqz v2, :cond_5

    .line 1037725
    const-string v3, "captions_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037726
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037727
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1037728
    cmp-long v4, v2, v8

    if-eqz v4, :cond_6

    .line 1037729
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037730
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1037731
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037732
    if-eqz v2, :cond_7

    .line 1037733
    const-string v3, "creation_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037734
    invoke-static {v1, v2, p1, p2}, LX/60W;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1037735
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037736
    if-eqz v2, :cond_8

    .line 1037737
    const-string v3, "enable_focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037738
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037739
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1037740
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_9

    .line 1037741
    const-string v4, "focus_width_degrees"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037742
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1037743
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037744
    if-eqz v2, :cond_a

    .line 1037745
    const-string v3, "guided_tour"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037746
    invoke-static {v1, v2, p1, p2}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1037747
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037748
    if-eqz v2, :cond_b

    .line 1037749
    const-string v3, "has_viewer_viewed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037750
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037751
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037752
    if-eqz v2, :cond_c

    .line 1037753
    const-string v3, "has_viewer_watched_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037754
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037755
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037756
    if-eqz v2, :cond_d

    .line 1037757
    const-string v3, "height"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037758
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037759
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1037760
    if-eqz v2, :cond_e

    .line 1037761
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037762
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037763
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037764
    if-eqz v2, :cond_f

    .line 1037765
    const-string v3, "initial_view_heading_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037766
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037767
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037768
    if-eqz v2, :cond_10

    .line 1037769
    const-string v3, "initial_view_pitch_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037770
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037771
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037772
    if-eqz v2, :cond_11

    .line 1037773
    const-string v3, "initial_view_roll_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037774
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037775
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037776
    if-eqz v2, :cond_12

    .line 1037777
    const-string v3, "instream_video_ad_breaks"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037778
    invoke-static {v1, v2, p1, p2}, LX/60X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1037779
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037780
    if-eqz v2, :cond_13

    .line 1037781
    const-string v3, "is_eligible_for_commercial_break"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037782
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037783
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037784
    if-eqz v2, :cond_14

    .line 1037785
    const-string v3, "is_live_audio_format"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037786
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037787
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037788
    if-eqz v2, :cond_15

    .line 1037789
    const-string v3, "is_live_streaming"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037790
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037791
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037792
    if-eqz v2, :cond_16

    .line 1037793
    const-string v3, "is_looping"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037794
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037795
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037796
    if-eqz v2, :cond_17

    .line 1037797
    const-string v3, "is_rtc_broadcast"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037798
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037799
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037800
    if-eqz v2, :cond_18

    .line 1037801
    const-string v3, "is_save_primary_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037802
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037803
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037804
    if-eqz v2, :cond_19

    .line 1037805
    const-string v3, "is_spherical"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037806
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037807
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037808
    if-eqz v2, :cond_1a

    .line 1037809
    const-string v3, "is_video_broadcast"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037810
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037811
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037812
    if-eqz v2, :cond_1b

    .line 1037813
    const-string v3, "live_viewer_count_read_only"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037814
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037815
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037816
    if-eqz v2, :cond_1c

    .line 1037817
    const-string v3, "loop_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037818
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037819
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1037820
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1d

    .line 1037821
    const-string v4, "off_focus_level"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037822
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1037823
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037824
    if-eqz v2, :cond_1e

    .line 1037825
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037826
    invoke-static {v1, v2, p1, p2}, LX/60b;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1037827
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037828
    if-eqz v2, :cond_1f

    .line 1037829
    const-string v3, "play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037830
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037831
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1037832
    if-eqz v2, :cond_20

    .line 1037833
    const-string v3, "projection_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037834
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037835
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037836
    if-eqz v2, :cond_21

    .line 1037837
    const-string v3, "publisher_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037838
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1037839
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037840
    if-eqz v2, :cond_22

    .line 1037841
    const-string v3, "show_video_channel_subscribe_button"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037842
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037843
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1037844
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_23

    .line 1037845
    const-string v4, "sphericalFullscreenAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037846
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1037847
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1037848
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_24

    .line 1037849
    const-string v4, "sphericalInlineAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037850
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1037851
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1037852
    if-eqz v2, :cond_25

    .line 1037853
    const-string v3, "sphericalPlayableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037854
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037855
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1037856
    if-eqz v2, :cond_26

    .line 1037857
    const-string v3, "sphericalPlayableUrlSdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037858
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1037859
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037860
    if-eqz v2, :cond_27

    .line 1037861
    const-string v3, "sphericalPreferredFov"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037862
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037863
    :cond_27
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037864
    if-eqz v2, :cond_28

    .line 1037865
    const-string v3, "streaming_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037866
    invoke-static {v1, v2, p1}, LX/60R;->a(LX/15i;ILX/0nX;)V

    .line 1037867
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1037868
    if-eqz v2, :cond_29

    .line 1037869
    const-string v3, "streaming_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037870
    invoke-static {v1, v2, p1}, LX/60R;->a(LX/15i;ILX/0nX;)V

    .line 1037871
    :cond_29
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1037872
    if-eqz v2, :cond_2a

    .line 1037873
    const-string v3, "supports_time_slices"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037874
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1037875
    :cond_2a
    invoke-virtual {v1, v0, v11}, LX/15i;->g(II)I

    move-result v2

    .line 1037876
    if-eqz v2, :cond_2b

    .line 1037877
    const-string v2, "video_captions_locales"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037878
    invoke-virtual {v1, v0, v11}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1037879
    :cond_2b
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1037880
    if-eqz v2, :cond_2c

    .line 1037881
    const-string v3, "width"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1037882
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1037883
    :cond_2c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1037884
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1037885
    check-cast p1, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$Serializer;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
