.class public final Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55936f9f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1037327
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1037321
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1037322
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1037323
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1037324
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1037325
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1037326
    return-void
.end method

.method public static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;
    .locals 8

    .prologue
    .line 1037328
    if-nez p0, :cond_0

    .line 1037329
    const/4 p0, 0x0

    .line 1037330
    :goto_0
    return-object p0

    .line 1037331
    :cond_0
    instance-of v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    if-eqz v0, :cond_1

    .line 1037332
    check-cast p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    goto :goto_0

    .line 1037333
    :cond_1
    new-instance v0, LX/60J;

    invoke-direct {v0}, LX/60J;-><init>()V

    .line 1037334
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->a()I

    move-result v1

    iput v1, v0, LX/60J;->a:I

    .line 1037335
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->b()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v1

    iput-object v1, v0, LX/60J;->b:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 1037336
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->c()I

    move-result v1

    iput v1, v0, LX/60J;->c:I

    .line 1037337
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->d()I

    move-result v1

    iput v1, v0, LX/60J;->d:I

    .line 1037338
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e()I

    move-result v1

    iput v1, v0, LX/60J;->e:I

    .line 1037339
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1037340
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1037341
    iget-object v3, v0, LX/60J;->b:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1037342
    const/4 v5, 0x5

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1037343
    iget v5, v0, LX/60J;->a:I

    invoke-virtual {v2, v7, v5, v7}, LX/186;->a(III)V

    .line 1037344
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1037345
    const/4 v3, 0x2

    iget v5, v0, LX/60J;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1037346
    const/4 v3, 0x3

    iget v5, v0, LX/60J;->d:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1037347
    const/4 v3, 0x4

    iget v5, v0, LX/60J;->e:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1037348
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1037349
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1037350
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1037351
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1037352
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1037353
    new-instance v3, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;-><init>(LX/15i;)V

    .line 1037354
    move-object p0, v3

    .line 1037355
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1037356
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1037357
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1037308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1037309
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->b()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1037310
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1037311
    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1037312
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1037313
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1037314
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->h:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1037315
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->i:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1037316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1037317
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1037318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1037319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1037320
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1037302
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1037303
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e:I

    .line 1037304
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->g:I

    .line 1037305
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->h:I

    .line 1037306
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->i:I

    .line 1037307
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037300
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->f:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->f:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 1037301
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->f:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1037297
    new-instance v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    invoke-direct {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;-><init>()V

    .line 1037298
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1037299
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1037289
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1037290
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->g:I

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1037295
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1037296
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1037294
    const v0, -0x72b811c9

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1037292
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1037293
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->i:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1037291
    const v0, 0xaf49346

    return v0
.end method
