.class public final Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1036800
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1036799
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1036797
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1036798
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1036794
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1036795
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1036796
    return-void
.end method

.method public static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .locals 8

    .prologue
    .line 1036770
    if-nez p0, :cond_0

    .line 1036771
    const/4 p0, 0x0

    .line 1036772
    :goto_0
    return-object p0

    .line 1036773
    :cond_0
    instance-of v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    if-eqz v0, :cond_1

    .line 1036774
    check-cast p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    goto :goto_0

    .line 1036775
    :cond_1
    new-instance v0, LX/60A;

    invoke-direct {v0}, LX/60A;-><init>()V

    .line 1036776
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->a()I

    move-result v1

    iput v1, v0, LX/60A;->a:I

    .line 1036777
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/60A;->b:Ljava/lang/String;

    .line 1036778
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->c()I

    move-result v1

    iput v1, v0, LX/60A;->c:I

    .line 1036779
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1036780
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1036781
    iget-object v3, v0, LX/60A;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1036782
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1036783
    iget v5, v0, LX/60A;->a:I

    invoke-virtual {v2, v7, v5, v7}, LX/186;->a(III)V

    .line 1036784
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1036785
    const/4 v3, 0x2

    iget v5, v0, LX/60A;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1036786
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1036787
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1036788
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1036789
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1036790
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1036791
    new-instance v3, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;-><init>(LX/15i;)V

    .line 1036792
    move-object p0, v3

    .line 1036793
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1036768
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1036769
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1036760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1036761
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1036762
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1036763
    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1036764
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1036765
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1036766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1036767
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1036757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1036758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1036759
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1036744
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1036745
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->e:I

    .line 1036746
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->g:I

    .line 1036747
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1036754
    new-instance v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-direct {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;-><init>()V

    .line 1036755
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1036756
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036752
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->f:Ljava/lang/String;

    .line 1036753
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1036750
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1036751
    iget v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1036749
    const v0, -0x5e545800

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1036748
    const v0, 0x34d85a39

    return v0
.end method
