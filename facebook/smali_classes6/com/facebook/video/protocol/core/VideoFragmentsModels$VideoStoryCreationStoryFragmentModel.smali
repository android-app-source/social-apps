.class public final Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3aab9d7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1037200
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1037201
    const-class v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1037202
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1037203
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1037204
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1037205
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1037206
    return-void
.end method

.method public static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1037176
    if-nez p0, :cond_0

    .line 1037177
    const/4 p0, 0x0

    .line 1037178
    :goto_0
    return-object p0

    .line 1037179
    :cond_0
    instance-of v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    if-eqz v0, :cond_1

    .line 1037180
    check-cast p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    goto :goto_0

    .line 1037181
    :cond_1
    new-instance v3, LX/60G;

    invoke-direct {v3}, LX/60G;-><init>()V

    .line 1037182
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1037183
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1037184
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;

    invoke-static {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1037185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1037186
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/60G;->a:LX/0Px;

    .line 1037187
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1037188
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1037189
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;

    invoke-static {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1037190
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1037191
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/60G;->b:LX/0Px;

    .line 1037192
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/60G;->c:Ljava/lang/String;

    .line 1037193
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e()J

    move-result-wide v0

    iput-wide v0, v3, LX/60G;->d:J

    .line 1037194
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bm_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/60G;->e:Ljava/lang/String;

    .line 1037195
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bn_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v0

    iput-object v0, v3, LX/60G;->f:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    .line 1037196
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/60G;->g:Ljava/lang/String;

    .line 1037197
    invoke-virtual {v3}, LX/60G;->a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1037207
    iput-object p1, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->f:Ljava/util/List;

    .line 1037208
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1037209
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1037210
    if-eqz v0, :cond_0

    .line 1037211
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 1037212
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037213
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    .line 1037214
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1037215
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1037216
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1037217
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1037218
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1037219
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bm_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1037220
    invoke-direct {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1037221
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1037222
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1037223
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1037224
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1037225
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1037226
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1037227
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1037228
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1037229
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1037230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1037231
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1037232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1037233
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1037234
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1037235
    if-eqz v1, :cond_0

    .line 1037236
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    .line 1037237
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e:Ljava/util/List;

    .line 1037238
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1037239
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1037240
    if-eqz v1, :cond_1

    .line 1037241
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    .line 1037242
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->f:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 1037243
    invoke-direct {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1037244
    invoke-direct {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    .line 1037245
    invoke-direct {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1037246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    .line 1037247
    iput-object v0, v1, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    .line 1037248
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1037249
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1037250
    new-instance v0, LX/60H;

    invoke-direct {v0, p1}, LX/60H;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037251
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bm_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1037252
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1037253
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->h:J

    .line 1037254
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1037198
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1037199
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1037154
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1037155
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->a(Ljava/util/List;)V

    .line 1037156
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1037159
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1037160
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e:Ljava/util/List;

    .line 1037161
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1037162
    new-instance v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    invoke-direct {v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;-><init>()V

    .line 1037163
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1037164
    return-object v0
.end method

.method public final bm_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037165
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->i:Ljava/lang/String;

    .line 1037166
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic bn_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037167
    invoke-direct {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1037157
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->f:Ljava/util/List;

    .line 1037158
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037168
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->g:Ljava/lang/String;

    .line 1037169
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1037170
    const v0, -0x3a339a51

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1037171
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1037172
    iget-wide v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->h:J

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1037173
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1037174
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k:Ljava/lang/String;

    .line 1037175
    iget-object v0, p0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method
