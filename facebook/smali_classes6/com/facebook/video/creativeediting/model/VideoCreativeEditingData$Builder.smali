.class public final Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Z

.field public j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1036538
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1036534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036535
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)V
    .locals 1

    .prologue
    .line 1036509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036510
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1036511
    instance-of v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-eqz v0, :cond_0

    .line 1036512
    check-cast p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1036513
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1036514
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->b:Ljava/lang/String;

    .line 1036515
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->c:LX/0Px;

    .line 1036516
    iget-boolean v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->d:Z

    .line 1036517
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->e:Ljava/lang/String;

    .line 1036518
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 1036519
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 1036520
    iget v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    iput v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->h:I

    .line 1036521
    iget-boolean v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->i:Z

    .line 1036522
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1036523
    :goto_0
    return-void

    .line 1036524
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1036525
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->b:Ljava/lang/String;

    .line 1036526
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getGLRendererConfigs()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->c:LX/0Px;

    .line 1036527
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->d:Z

    .line 1036528
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getMsqrdMaskId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->e:Ljava/lang/String;

    .line 1036529
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 1036530
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 1036531
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->h:I

    .line 1036532
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->i:Z

    .line 1036533
    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 2

    .prologue
    .line 1036508
    new-instance v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-direct {v0, p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;-><init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;)V

    return-object v0
.end method

.method public setCropRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/PersistableRect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "crop_rect"
    .end annotation

    .prologue
    .line 1036506
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1036507
    return-object p0
.end method

.method public setDisplayUri(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_uri"
    .end annotation

    .prologue
    .line 1036504
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->b:Ljava/lang/String;

    .line 1036505
    return-object p0
.end method

.method public setGLRendererConfigs(LX/0Px;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "g_l_renderer_configs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;)",
            "Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 1036536
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->c:LX/0Px;

    .line 1036537
    return-object p0
.end method

.method public setIsVideoMuted(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_video_muted"
    .end annotation

    .prologue
    .line 1036502
    iput-boolean p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->d:Z

    .line 1036503
    return-object p0
.end method

.method public setMsqrdMaskId(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "msqrd_mask_id"
    .end annotation

    .prologue
    .line 1036500
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->e:Ljava/lang/String;

    .line 1036501
    return-object p0
.end method

.method public setOverlayId(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "overlay_id"
    .end annotation

    .prologue
    .line 1036498
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 1036499
    return-object p0
.end method

.method public setOverlayUri(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "overlay_uri"
    .end annotation

    .prologue
    .line 1036496
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 1036497
    return-object p0
.end method

.method public setRotationAngle(I)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation_angle"
    .end annotation

    .prologue
    .line 1036494
    iput p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->h:I

    .line 1036495
    return-object p0
.end method

.method public setShouldFlipHorizontally(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_flip_horizontally"
    .end annotation

    .prologue
    .line 1036492
    iput-boolean p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->i:Z

    .line 1036493
    return-object p0
.end method

.method public setVideoTrimParams(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_trim_params"
    .end annotation

    .prologue
    .line 1036490
    iput-object p1, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1036491
    return-object p0
.end method
