.class public Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:I

.field public final i:Z

.field public final j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1036667
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1036666
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1036665
    new-instance v0, LX/607;

    invoke-direct {v0}, LX/607;-><init>()V

    sput-object v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1036632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036633
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1036634
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1036635
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1036636
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    .line 1036637
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1036638
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    .line 1036639
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_4

    move v0, v3

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    .line 1036640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1036641
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    .line 1036642
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1036643
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    .line 1036644
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 1036645
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    .line 1036646
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    .line 1036647
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_8

    :goto_7
    iput-boolean v3, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    .line 1036648
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 1036649
    iput-object v5, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1036650
    :goto_8
    return-void

    .line 1036651
    :cond_0
    sget-object v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    goto :goto_0

    .line 1036652
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    goto :goto_1

    .line 1036653
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/videocodec/effects/common/GLRendererConfig;

    move v1, v2

    .line 1036654
    :goto_9
    array-length v0, v4

    if-ge v1, v0, :cond_3

    .line 1036655
    const-class v0, Lcom/facebook/videocodec/effects/common/GLRendererConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/common/GLRendererConfig;

    .line 1036656
    aput-object v0, v4, v1

    .line 1036657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1036658
    :cond_3
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1036659
    goto :goto_3

    .line 1036660
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    goto :goto_4

    .line 1036661
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    goto :goto_5

    .line 1036662
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    goto :goto_6

    :cond_8
    move v3, v2

    .line 1036663
    goto :goto_7

    .line 1036664
    :cond_9
    sget-object v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    goto :goto_8
.end method

.method public constructor <init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;)V
    .locals 1

    .prologue
    .line 1036620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036621
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1036622
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    .line 1036623
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    .line 1036624
    iget-boolean v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    .line 1036625
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    .line 1036626
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    .line 1036627
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    .line 1036628
    iget v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    .line 1036629
    iget-boolean v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    .line 1036630
    iget-object v0, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1036631
    return-void
.end method

.method public static a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 2

    .prologue
    .line 1036591
    new-instance v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;-><init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;
    .locals 2

    .prologue
    .line 1036619
    new-instance v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    invoke-direct {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1036618
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1036593
    if-ne p0, p1, :cond_1

    .line 1036594
    :cond_0
    :goto_0
    return v0

    .line 1036595
    :cond_1
    instance-of v2, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1036596
    goto :goto_0

    .line 1036597
    :cond_2
    check-cast p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1036598
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1036599
    goto :goto_0

    .line 1036600
    :cond_3
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1036601
    goto :goto_0

    .line 1036602
    :cond_4
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1036603
    goto :goto_0

    .line 1036604
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    iget-boolean v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1036605
    goto :goto_0

    .line 1036606
    :cond_6
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1036607
    goto :goto_0

    .line 1036608
    :cond_7
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1036609
    goto :goto_0

    .line 1036610
    :cond_8
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1036611
    goto :goto_0

    .line 1036612
    :cond_9
    iget v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    iget v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1036613
    goto :goto_0

    .line 1036614
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    iget-boolean v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1036615
    goto :goto_0

    .line 1036616
    :cond_b
    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iget-object v3, p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1036617
    goto :goto_0
.end method

.method public getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "crop_rect"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036592
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-object v0
.end method

.method public getDisplayUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036668
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getGLRendererConfigs()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "g_l_renderer_configs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036545
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    return-object v0
.end method

.method public getMsqrdMaskId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "msqrd_mask_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036546
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getOverlayId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "overlay_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036547
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getOverlayUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "overlay_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036548
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getRotationAngle()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation_angle"
    .end annotation

    .prologue
    .line 1036549
    iget v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    return v0
.end method

.method public getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_trim_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036550
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1036551
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isVideoMuted()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_video_muted"
    .end annotation

    .prologue
    .line 1036552
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    return v0
.end method

.method public shouldFlipHorizontally()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_flip_horizontally"
    .end annotation

    .prologue
    .line 1036553
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1036554
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    if-nez v0, :cond_1

    .line 1036555
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036556
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1036557
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036558
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    if-nez v0, :cond_3

    .line 1036559
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036560
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->d:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036561
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1036562
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036563
    :goto_3
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1036564
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036565
    :goto_4
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1036566
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036567
    :goto_5
    iget v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036568
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->i:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036569
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    if-nez v0, :cond_9

    .line 1036570
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036571
    :goto_7
    return-void

    .line 1036572
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036573
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1036574
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036575
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1036576
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036577
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036578
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_8
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/common/GLRendererConfig;

    .line 1036579
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1036580
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_4
    move v0, v2

    .line 1036581
    goto :goto_2

    .line 1036582
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036583
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 1036584
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036585
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    .line 1036586
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036587
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    move v0, v2

    .line 1036588
    goto :goto_6

    .line 1036589
    :cond_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1036590
    iget-object v0, p0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->j:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_7
.end method
