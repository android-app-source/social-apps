.class public final Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1036539
    new-instance v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;->a:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1036540
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1036541
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 1

    .prologue
    .line 1036542
    sget-object v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;->a:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    .line 1036543
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1036544
    invoke-static {p1, p2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    return-object v0
.end method
