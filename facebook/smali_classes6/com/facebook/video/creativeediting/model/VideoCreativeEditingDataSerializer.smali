.class public Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1036670
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    new-instance v1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;

    invoke-direct {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1036671
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1036672
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1036673
    if-nez p0, :cond_0

    .line 1036674
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1036675
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1036676
    invoke-static {p0, p1, p2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;->b(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/0nX;LX/0my;)V

    .line 1036677
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1036678
    return-void
.end method

.method private static b(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1036679
    const-string v0, "crop_rect"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1036680
    const-string v0, "display_uri"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036681
    const-string v0, "g_l_renderer_configs"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getGLRendererConfigs()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1036682
    const-string v0, "is_video_muted"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1036683
    const-string v0, "msqrd_mask_id"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getMsqrdMaskId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036684
    const-string v0, "overlay_id"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036685
    const-string v0, "overlay_uri"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036686
    const-string v0, "rotation_angle"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1036687
    const-string v0, "should_flip_horizontally"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1036688
    const-string v0, "video_trim_params"

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1036689
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1036690
    check-cast p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingDataSerializer;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/0nX;LX/0my;)V

    return-void
.end method
