.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039153
    new-instance v0, LX/60n;

    invoke-direct {v0}, LX/60n;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1039154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->a:Ljava/lang/String;

    .line 1039156
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->b:Z

    .line 1039157
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->c:Z

    .line 1039158
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 1039148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039149
    iput-object p1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->a:Ljava/lang/String;

    .line 1039150
    iput-boolean p2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->b:Z

    .line 1039151
    iput-boolean p3, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->c:Z

    .line 1039152
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039143
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039144
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039145
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1039146
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1039147
    return-void
.end method
