.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1039009
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    new-instance v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1039010
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039011
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039012
    if-nez p0, :cond_0

    .line 1039013
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1039014
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1039015
    invoke-static {p0, p1, p2}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;->b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;LX/0nX;LX/0my;)V

    .line 1039016
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1039017
    return-void
.end method

.method private static b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1039018
    const-string v0, "sample_rate"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->sampleRate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039019
    const-string v0, "bit_rate"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039020
    const-string v0, "channels"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->channels:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039021
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039022
    check-cast p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;LX/0nX;LX/0my;)V

    return-void
.end method
