.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final allowBFrames:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_b_frames"
    .end annotation
.end field

.field public final bitRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bit_rate"
    .end annotation
.end field

.field public final frameRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_rate"
    .end annotation
.end field

.field public final height:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field public final iFrameInterval:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "iframe_interval"
    .end annotation
.end field

.field public final videoProfile:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_profile"
    .end annotation
.end field

.field public final width:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039382
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039381
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039380
    new-instance v0, LX/60q;

    invoke-direct {v0}, LX/60q;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1039383
    new-instance v0, LX/60r;

    invoke-direct {v0}, LX/60r;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;-><init>(LX/60r;)V

    .line 1039384
    return-void
.end method

.method public constructor <init>(LX/60r;)V
    .locals 1

    .prologue
    .line 1039371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039372
    iget v0, p1, LX/60r;->a:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    .line 1039373
    iget v0, p1, LX/60r;->b:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    .line 1039374
    iget v0, p1, LX/60r;->c:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    .line 1039375
    iget v0, p1, LX/60r;->d:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    .line 1039376
    iget-boolean v0, p1, LX/60r;->e:Z

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->allowBFrames:Z

    .line 1039377
    iget-object v0, p1, LX/60r;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    .line 1039378
    iget v0, p1, LX/60r;->g:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    .line 1039379
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1039362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039363
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    .line 1039364
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    .line 1039365
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    .line 1039366
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    .line 1039367
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->allowBFrames:Z

    .line 1039368
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    .line 1039369
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    .line 1039370
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039361
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039353
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039354
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039355
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039356
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039357
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->allowBFrames:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1039358
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039359
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039360
    return-void
.end method
