.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;


# instance fields
.field public final audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audio_only_video_streaming_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audio_streaming_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final broadcastId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "broadcast_id"
    .end annotation
.end field

.field public final broadcastInterruptionLimitInSeconds:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "broadcaster_interruption_limit_in_seconds"
    .end annotation
.end field

.field public final clientRenderingDurationMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_rendering_duration_ms"
    .end annotation
.end field

.field public final commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "commercial_break_settings"
    .end annotation
.end field

.field public final mAudioOnlyFormatBitRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audio_only_format_stream_bit_rate"
    .end annotation
.end field

.field public final mIsDiskRecordingEnabled:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_disk_recording_enabled"
    .end annotation
.end field

.field public final mRawJsonConfig:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "raw_json_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final maxBroadcastDurationSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_broadcast_duration"
    .end annotation
.end field

.field public final minBroadcastDurationSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "min_broadacst_duration"
    .end annotation
.end field

.field public final rtmpPublishUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rtmp_publish_url"
    .end annotation
.end field

.field public final sendStreamInterruptedIntervalInSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "send_stream_interrupted_interval"
    .end annotation
.end field

.field public final speedTestTimeoutSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "speed_test_timeout_seconds"
    .end annotation
.end field

.field public final videoId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_id"
    .end annotation
.end field

.field public final videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_streaming_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039256
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039255
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039251
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->a:Ljava/lang/String;

    .line 1039252
    new-instance v0, LX/60o;

    invoke-direct {v0}, LX/60o;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1039253
    new-instance v0, LX/60p;

    invoke-direct {v0}, LX/60p;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;-><init>(LX/60p;)V

    .line 1039254
    return-void
.end method

.method public constructor <init>(LX/60p;)V
    .locals 2

    .prologue
    .line 1039257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039258
    iget-object v0, p1, LX/60p;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;

    .line 1039259
    iget-object v0, p1, LX/60p;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    .line 1039260
    iget-object v0, p1, LX/60p;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 1039261
    iget-wide v0, p1, LX/60p;->e:J

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->minBroadcastDurationSeconds:J

    .line 1039262
    iget-wide v0, p1, LX/60p;->f:J

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->maxBroadcastDurationSeconds:J

    .line 1039263
    iget-wide v0, p1, LX/60p;->g:J

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->speedTestTimeoutSeconds:J

    .line 1039264
    iget-wide v0, p1, LX/60p;->h:J

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    .line 1039265
    iget-object v0, p1, LX/60p;->i:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039266
    iget-object v0, p1, LX/60p;->j:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039267
    iget-object v0, p1, LX/60p;->k:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1039268
    iget-object v0, p1, LX/60p;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    .line 1039269
    iget-boolean v0, p1, LX/60p;->m:Z

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mIsDiskRecordingEnabled:Z

    .line 1039270
    iget-wide v0, p1, LX/60p;->n:J

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->clientRenderingDurationMs:J

    .line 1039271
    iget v0, p1, LX/60p;->o:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastInterruptionLimitInSeconds:I

    .line 1039272
    iget-object v0, p1, LX/60p;->p:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1039273
    iget v0, p1, LX/60p;->a:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mAudioOnlyFormatBitRate:I

    .line 1039274
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1039213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;

    .line 1039215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    .line 1039216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 1039217
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->minBroadcastDurationSeconds:J

    .line 1039218
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->maxBroadcastDurationSeconds:J

    .line 1039219
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->speedTestTimeoutSeconds:J

    .line 1039220
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    .line 1039221
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039222
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1039223
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1039224
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    .line 1039225
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mIsDiskRecordingEnabled:Z

    .line 1039226
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->clientRenderingDurationMs:J

    .line 1039227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastInterruptionLimitInSeconds:I

    .line 1039228
    const-class v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1039229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mAudioOnlyFormatBitRate:I

    .line 1039230
    return-void

    .line 1039231
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 4

    .prologue
    .line 1039207
    const/4 v0, 0x0

    .line 1039208
    :try_start_0
    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1039209
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1039210
    :cond_0
    :goto_0
    return-object v0

    .line 1039211
    :catch_0
    move-exception v1

    .line 1039212
    sget-object v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->a:Ljava/lang/String;

    const-string v3, "Failed to read the broadcast configuration"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039232
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1039233
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039234
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039235
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039236
    iget-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->minBroadcastDurationSeconds:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1039237
    iget-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->maxBroadcastDurationSeconds:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1039238
    iget-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->speedTestTimeoutSeconds:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1039239
    iget-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1039240
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1039241
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1039242
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1039243
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039244
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mIsDiskRecordingEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1039245
    iget-wide v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->clientRenderingDurationMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1039246
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastInterruptionLimitInSeconds:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039247
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1039248
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mAudioOnlyFormatBitRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039249
    return-void

    .line 1039250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
