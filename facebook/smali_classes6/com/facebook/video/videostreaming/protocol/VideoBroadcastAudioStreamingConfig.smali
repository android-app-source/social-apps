.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bitRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bit_rate"
    .end annotation
.end field

.field public final channels:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "channels"
    .end annotation
.end field

.field public final sampleRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sample_rate"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1038983
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1038982
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1038981
    new-instance v0, LX/60j;

    invoke-direct {v0}, LX/60j;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1038984
    new-instance v0, LX/60k;

    invoke-direct {v0}, LX/60k;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;-><init>(LX/60k;)V

    .line 1038985
    return-void
.end method

.method public constructor <init>(LX/60k;)V
    .locals 1

    .prologue
    .line 1038966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038967
    iget v0, p1, LX/60k;->a:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->sampleRate:I

    .line 1038968
    iget v0, p1, LX/60k;->b:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    .line 1038969
    iget v0, p1, LX/60k;->c:I

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->channels:I

    .line 1038970
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1038976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038977
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->sampleRate:I

    .line 1038978
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    .line 1038979
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->channels:I

    .line 1038980
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1038971
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1038972
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->sampleRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038973
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->bitRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038974
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;->channels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038975
    return-void
.end method
