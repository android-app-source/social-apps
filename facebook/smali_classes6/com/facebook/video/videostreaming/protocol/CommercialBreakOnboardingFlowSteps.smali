.class public final enum Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INTRODUCTION:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

.field public static final enum LEGAL_AGREEMENT:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

.field public static final enum PAYMENT_ONBOARDING:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

.field public static final enum TUTORIAL:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

.field public static final enum UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1038826
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    const-string v1, "INTRODUCTION"

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->INTRODUCTION:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038827
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    const-string v1, "TUTORIAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->TUTORIAL:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038828
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    const-string v1, "LEGAL_AGREEMENT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->LEGAL_AGREEMENT:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038829
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    const-string v1, "PAYMENT_ONBOARDING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->PAYMENT_ONBOARDING:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038830
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038831
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->INTRODUCTION:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->TUTORIAL:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->LEGAL_AGREEMENT:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->PAYMENT_ONBOARDING:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->$VALUES:[Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1038832
    new-instance v0, LX/60g;

    invoke-direct {v0}, LX/60g;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1038825
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getOnboardingFlowStepsFromString(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;
    .locals 1

    .prologue
    .line 1038823
    :try_start_0
    invoke-static {p0}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->valueOf(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1038824
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;
    .locals 1

    .prologue
    .line 1038822
    const-class v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    return-object v0
.end method

.method public static values()[Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;
    .locals 1

    .prologue
    .line 1038818
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->$VALUES:[Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1038821
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1038819
    invoke-virtual {p0}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1038820
    return-void
.end method
