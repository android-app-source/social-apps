.class public final enum Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

.field public static final enum AGREE_LEGAL_TERMS:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum UNKNOWN:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1038945
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    const-string v1, "AGREE_LEGAL_TERMS"

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->AGREE_LEGAL_TERMS:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1038946
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1038947
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->AGREE_LEGAL_TERMS:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->$VALUES:[Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1038948
    new-instance v0, LX/60i;

    invoke-direct {v0}, LX/60i;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1038944
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getActionFromString(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;
    .locals 1

    .prologue
    .line 1038937
    :try_start_0
    invoke-static {p0}, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->valueOf(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1038938
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;
    .locals 1

    .prologue
    .line 1038943
    const-class v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    return-object v0
.end method

.method public static values()[Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;
    .locals 1

    .prologue
    .line 1038942
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->$VALUES:[Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1038941
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1038939
    invoke-virtual {p0}, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1038940
    return-void
.end method
