.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1039412
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    new-instance v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1039413
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039414
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039415
    if-nez p0, :cond_0

    .line 1039416
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1039417
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1039418
    invoke-static {p0, p1, p2}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;->b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;LX/0nX;LX/0my;)V

    .line 1039419
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1039420
    return-void
.end method

.method private static b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1039421
    const-string v0, "width"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->width:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039422
    const-string v0, "height"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->height:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039423
    const-string v0, "bit_rate"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->bitRate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039424
    const-string v0, "frame_rate"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->frameRate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039425
    const-string v0, "allow_b_frames"

    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->allowBFrames:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1039426
    const-string v0, "video_profile"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->videoProfile:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039427
    const-string v0, "iframe_interval"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;->iFrameInterval:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039428
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039429
    check-cast p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfigSerializer;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;LX/0nX;LX/0my;)V

    return-void
.end method
