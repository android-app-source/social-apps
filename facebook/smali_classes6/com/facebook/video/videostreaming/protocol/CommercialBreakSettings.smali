.class public Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettingsDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

.field private static final d:Ljava/lang/String;


# instance fields
.field public final allowPublicBroadcastsOnly:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_public_broadcasts_only"
    .end annotation
.end field

.field public final broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "broadcaster_viewer_count_category"
    .end annotation
.end field

.field public final broadcasterViolations:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "broadcaster_violations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;",
            ">;"
        }
    .end annotation
.end field

.field public final commercialBreakLengthMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "commercial_break_length_ms"
    .end annotation
.end field

.field public final currencyCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency_code"
    .end annotation
.end field

.field public final firstCommercialEligibleSecs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_commercial_eligible_secs"
    .end annotation
.end field

.field public final isEligible:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_eligible"
    .end annotation
.end field

.field public final isOnboarded:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_onboarded"
    .end annotation
.end field

.field public final onboardingFlowSteps:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "onboarding_flow_steps"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;",
            ">;"
        }
    .end annotation
.end field

.field public final payoutEstimateFactorHigh:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "payout_estimate_factor_high"
    .end annotation
.end field

.field public final payoutEstimateFactorLow:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "payout_estimate_factor_low"
    .end annotation
.end field

.field public final timeBetweenCommercialsEligibleSecs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "time_between_commercials_eligible_secs"
    .end annotation
.end field

.field public final viewerCountThreshold:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewer_count_threshold"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1038880
    const-class v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettingsDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1038873
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1038874
    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->a:Ljava/util/List;

    .line 1038875
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1038876
    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->b:Ljava/util/List;

    .line 1038877
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    .line 1038878
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->d:Ljava/lang/String;

    .line 1038879
    new-instance v0, LX/60h;

    invoke-direct {v0}, LX/60h;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/16 v0, 0x12c

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 1038858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038859
    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    .line 1038860
    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    .line 1038861
    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    .line 1038862
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    .line 1038863
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    .line 1038864
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    .line 1038865
    iput v2, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    .line 1038866
    iput v2, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    .line 1038867
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    .line 1038868
    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    .line 1038869
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    .line 1038870
    sget-object v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->currencyCode:Ljava/lang/String;

    .line 1038871
    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->allowPublicBroadcastsOnly:Z

    .line 1038872
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1038881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038882
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    .line 1038883
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    .line 1038884
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    .line 1038885
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    .line 1038886
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    .line 1038887
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    .line 1038888
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    sget-object v3, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViolationType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1038889
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    .line 1038890
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    .line 1038891
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    .line 1038892
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    sget-object v3, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1038893
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    .line 1038894
    const-class v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    .line 1038895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->currencyCode:Ljava/lang/String;

    .line 1038896
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->allowPublicBroadcastsOnly:Z

    .line 1038897
    return-void

    :cond_0
    move v0, v2

    .line 1038898
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1038899
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1038900
    goto :goto_2
.end method

.method public static a(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .locals 2

    .prologue
    .line 1038855
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    const-class v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-virtual {v0, p0, v1}, LX/0lC;->a(LX/0lG;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1038856
    :goto_0
    return-object v0

    .line 1038857
    :catch_0
    new-instance v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-direct {v0}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1038854
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1038853
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isEligible: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nfirstCommercialEligibleSecs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ntimeBetweenCommercialsEligibleSecs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nviewerCountThreshold: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncommercialBreakLengthMs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nbroadcasterViolations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\npayoutEstimateFactorLow: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\npayoutEstimateFactorHigh: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nonboardingFlowSteps: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nisOnboarded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nbroadcasterViewerCountCategory: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncurrencyCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->currencyCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nallowPublicBroadcastsOnly: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->allowPublicBroadcastsOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1038836
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1038837
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038838
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038839
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038840
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1038841
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1038842
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1038843
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1038844
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1038845
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1038846
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1038847
    iget-object v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1038848
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->allowPublicBroadcastsOnly:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1038849
    return-void

    :cond_0
    move v0, v2

    .line 1038850
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1038851
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1038852
    goto :goto_2
.end method
