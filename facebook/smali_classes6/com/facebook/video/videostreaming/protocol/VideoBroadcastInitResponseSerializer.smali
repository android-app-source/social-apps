.class public Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1039336
    const-class v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    new-instance v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;

    invoke-direct {v1}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1039337
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039335
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039329
    if-nez p0, :cond_0

    .line 1039330
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1039331
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1039332
    invoke-static {p0, p1, p2}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;->b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0nX;LX/0my;)V

    .line 1039333
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1039334
    return-void
.end method

.method private static b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1039312
    const-string v0, "rtmp_publish_url"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039313
    const-string v0, "video_id"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039314
    const-string v0, "broadcast_id"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039315
    const-string v0, "min_broadacst_duration"

    iget-wide v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->minBroadcastDurationSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039316
    const-string v0, "max_broadcast_duration"

    iget-wide v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->maxBroadcastDurationSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039317
    const-string v0, "speed_test_timeout_seconds"

    iget-wide v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->speedTestTimeoutSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039318
    const-string v0, "send_stream_interrupted_interval"

    iget-wide v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039319
    const-string v0, "video_streaming_config"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039320
    const-string v0, "audio_only_video_streaming_config"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioOnlyVideoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039321
    const-string v0, "audio_streaming_config"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039322
    const-string v0, "raw_json_config"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mRawJsonConfig:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039323
    const-string v0, "is_disk_recording_enabled"

    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mIsDiskRecordingEnabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1039324
    const-string v0, "client_rendering_duration_ms"

    iget-wide v2, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->clientRenderingDurationMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039325
    const-string v0, "broadcaster_interruption_limit_in_seconds"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastInterruptionLimitInSeconds:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039326
    const-string v0, "commercial_break_settings"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039327
    const-string v0, "audio_only_format_stream_bit_rate"

    iget v1, p0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mAudioOnlyFormatBitRate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1039328
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039311
    check-cast p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponseSerializer;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0nX;LX/0my;)V

    return-void
.end method
