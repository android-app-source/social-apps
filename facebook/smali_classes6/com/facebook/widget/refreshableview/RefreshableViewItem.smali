.class public Lcom/facebook/widget/refreshableview/RefreshableViewItem;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0wT;

.field private static final c:LX/0wT;

.field private static final d:Landroid/view/animation/Interpolator;


# instance fields
.field public e:Landroid/widget/ImageView;

.field private f:Lcom/facebook/widget/FacebookProgressCircleView;

.field private g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0wW;

.field public j:LX/0wd;

.field private k:LX/0wd;

.field private l:LX/62s;

.field private m:F

.field private n:I

.field public o:LX/62r;

.field private p:I

.field private q:LX/61q;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    .line 1042579
    const-class v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    sput-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a:Ljava/lang/Class;

    .line 1042580
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    invoke-static {v2, v3, v0, v1}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b:LX/0wT;

    .line 1042581
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v2, v3, v0, v1}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->c:LX/0wT;

    .line 1042582
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->d:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1042583
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1042584
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->p:I

    .line 1042585
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e()V

    .line 1042586
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1042587
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042588
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->p:I

    .line 1042589
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e()V

    .line 1042590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1042591
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042592
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->p:I

    .line 1042593
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e()V

    .line 1042594
    return-void
.end method

.method private a(Landroid/view/View;)I
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1042595
    if-nez p1, :cond_0

    .line 1042596
    sget-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a:Ljava/lang/Class;

    const-string v1, "No background color set for PTR fragment"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1042597
    iget v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->n:I

    .line 1042598
    :goto_0
    return v0

    .line 1042599
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1042600
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1042601
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    .line 1042602
    :cond_2
    instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_3

    .line 1042603
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    goto :goto_0

    .line 1042604
    :cond_3
    sget-object v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a:Ljava/lang/Class;

    const-string v1, "Non color drawables not supported for PTR backgrounds"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1042605
    iget v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->n:I

    goto :goto_0
.end method

.method private static a(I)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 1042606
    const v0, 0xffffff

    and-int/2addr v0, p0

    or-int/lit8 v0, v0, 0x0

    .line 1042607
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v0, 0x1

    aput p0, v3, v0

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v1
.end method

.method private a(F)V
    .locals 6

    .prologue
    const v1, 0x44ed8000    # 1900.0f

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 1042608
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    .line 1042609
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 1042610
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1042611
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    if-nez v0, :cond_2

    .line 1042612
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1042613
    iget v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->m:F

    div-float v0, p1, v0

    .line 1042614
    cmpl-float v3, v0, v1

    if-lez v3, :cond_1

    move v0, v1

    .line 1042615
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v3

    const-wide/16 v4, 0x7d0

    sget-object v3, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->d:Landroid/view/animation/Interpolator;

    .line 1042616
    iput v0, v1, LX/62s;->f:F

    .line 1042617
    iput-wide v4, v1, LX/62s;->g:J

    .line 1042618
    iput-object v3, v1, LX/62s;->h:Landroid/view/animation/Interpolator;

    .line 1042619
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    const/4 v1, 0x0

    .line 1042620
    iput v1, v0, LX/62s;->c:F

    .line 1042621
    iput v1, v0, LX/62s;->d:F

    .line 1042622
    iput v1, v0, LX/62s;->e:F

    .line 1042623
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1042624
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1042625
    invoke-direct {p0, p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setAnimationRestartListeners(Landroid/view/View;)V

    move v0, v2

    .line 1042626
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1042627
    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->setAnimationRestartListeners(Landroid/view/View;)V

    .line 1042628
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1042629
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1042630
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->setVisibility(I)V

    .line 1042631
    :cond_3
    return-void
.end method

.method private a(LX/0wW;LX/61q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1042670
    iput-object p1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->i:LX/0wW;

    .line 1042671
    iput-object p2, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->q:LX/61q;

    .line 1042672
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {v1}, LX/61q;->a(LX/0QB;)LX/61q;

    move-result-object v1

    check-cast v1, LX/61q;

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(LX/0wW;LX/61q;)V

    return-void
.end method

.method private static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1042632
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 1042633
    if-eqz v0, :cond_0

    .line 1042634
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 1042635
    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 1042636
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    .line 1042637
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1042638
    const-class v0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;

    invoke-static {v0, p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1042639
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f()V

    .line 1042640
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 1042641
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010492

    const v2, 0x7f030d7d

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    .line 1042642
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1042643
    invoke-virtual {v1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1042644
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->q:LX/61q;

    invoke-virtual {v0}, LX/61q;->a()I

    move-result v0

    const/16 v2, 0x3c

    if-eq v0, v2, :cond_0

    .line 1042645
    const v0, 0x7f0d2110

    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1042646
    const v2, 0x7f0310a5

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1042647
    const v1, 0x7f0d27a6

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    .line 1042648
    :cond_0
    const v0, 0x7f0d213c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    .line 1042649
    const v0, 0x7f0d2111

    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    .line 1042650
    const v0, 0x7f0d2112

    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleView;

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    .line 1042651
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->n:I

    .line 1042652
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->m:F

    .line 1042653
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    if-nez v0, :cond_1

    .line 1042654
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->i:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/62q;

    invoke-direct {v1, p0}, LX/62q;-><init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    .line 1042655
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1042656
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->i:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->c:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/62p;

    invoke-direct {v1, p0}, LX/62p;-><init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    .line 1042657
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    if-nez v0, :cond_3

    .line 1042658
    new-instance v0, LX/62s;

    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const-wide/16 v2, 0x320

    invoke-direct {v0, v1, v2, v3}, LX/62s;-><init>(Landroid/graphics/drawable/Drawable;J)V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    .line 1042659
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/62s;->setRepeatMode(I)V

    .line 1042660
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/62s;->setRepeatCount(I)V

    .line 1042661
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/62s;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1042662
    :cond_3
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g()V

    .line 1042663
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1042571
    const v0, 0x7f0d2114

    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1042572
    const v1, 0x7f0d2113

    invoke-virtual {p0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1042573
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1042574
    :cond_0
    :goto_0
    return-void

    .line 1042575
    :cond_1
    invoke-direct {p0, p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(Landroid/view/View;)I

    move-result v2

    .line 1042576
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1042577
    invoke-static {v2}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1042578
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgressBarColor(I)V

    goto :goto_0
.end method

.method public static h(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V
    .locals 2

    .prologue
    .line 1042664
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1042665
    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 1042666
    :cond_0
    :goto_0
    return-void

    .line 1042667
    :cond_1
    check-cast v0, Landroid/app/Activity;

    .line 1042668
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042669
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b()V

    goto :goto_0
.end method

.method private setAnimationRestartListeners(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1042504
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1042505
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getAnimationRestartListener()Landroid/view/animation/Animation$AnimationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1042506
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 1042528
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b()V

    .line 1042529
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1042530
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    if-eqz v0, :cond_0

    .line 1042531
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->setVisibility(I)V

    .line 1042532
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1042533
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1042534
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1042535
    :cond_1
    return-void
.end method

.method public final a(IF)V
    .locals 4

    .prologue
    .line 1042536
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    invoke-virtual {v0}, LX/62s;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    invoke-virtual {v0}, LX/62s;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1042537
    :cond_0
    :goto_0
    return-void

    .line 1042538
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 1042539
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    mul-int/lit8 v1, p1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 1042540
    const/16 v0, 0x64

    if-lt p1, v0, :cond_0

    .line 1042541
    invoke-direct {p0, p2}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->a(F)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1042542
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042543
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V

    .line 1042544
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1042507
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    if-eqz v0, :cond_0

    .line 1042508
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    .line 1042509
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->f:Lcom/facebook/widget/FacebookProgressCircleView;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 1042510
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1042511
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1042512
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    if-eqz v0, :cond_2

    .line 1042513
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 1042514
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->j:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1042515
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    if-eqz v0, :cond_3

    .line 1042516
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 1042517
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1042518
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1042519
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1042520
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 1042521
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    if-eqz v0, :cond_5

    .line 1042522
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->setVisibility(I)V

    .line 1042523
    :cond_5
    invoke-static {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b(Landroid/view/View;)V

    move v0, v1

    .line 1042524
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 1042525
    invoke-virtual {p0, v0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b(Landroid/view/View;)V

    .line 1042526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1042527
    :cond_6
    return-void
.end method

.method public getAnimationRestartListener()Landroid/view/animation/Animation$AnimationListener;
    .locals 1

    .prologue
    .line 1042545
    new-instance v0, LX/62o;

    invoke-direct {v0, p0}, LX/62o;-><init>(Lcom/facebook/widget/refreshableview/RefreshableViewItem;)V

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xb0af5ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1042546
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1042547
    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->o:LX/62r;

    sget-object v2, LX/62r;->LOADING:LX/62r;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    invoke-virtual {v1}, LX/62s;->hasStarted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1042548
    iget-object v1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->l:LX/62s;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1042549
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x43376cf5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x46074932

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1042550
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1042551
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->b()V

    .line 1042552
    const/16 v1, 0x2d

    const v2, 0x5cd6fe11

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1042553
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1042554
    invoke-super {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1042555
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1042556
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1042557
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g()V

    .line 1042558
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1042559
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1042560
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->g()V

    .line 1042561
    return-void
.end method

.method public setDirection(I)V
    .locals 1

    .prologue
    .line 1042562
    iget v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->p:I

    if-ne p1, v0, :cond_0

    .line 1042563
    :goto_0
    return-void

    .line 1042564
    :cond_0
    iput p1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->p:I

    goto :goto_0
.end method

.method public setErrorVerticalPadding(I)V
    .locals 2

    .prologue
    .line 1042565
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042566
    invoke-virtual {p0}, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1042567
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(II)V

    .line 1042568
    :cond_0
    return-void
.end method

.method public setState(LX/62r;)V
    .locals 0

    .prologue
    .line 1042569
    iput-object p1, p0, Lcom/facebook/widget/refreshableview/RefreshableViewItem;->o:LX/62r;

    .line 1042570
    return-void
.end method
