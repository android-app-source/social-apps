.class public Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;
.super LX/62l;
.source ""


# instance fields
.field public f:I

.field private g:Landroid/graphics/Rect;

.field private h:Landroid/graphics/Rect;

.field private i:LX/0g8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1042459
    invoke-direct {p0, p1}, LX/62l;-><init>(Landroid/content/Context;)V

    .line 1042460
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->g:Landroid/graphics/Rect;

    .line 1042461
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->h:Landroid/graphics/Rect;

    .line 1042462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1042455
    invoke-direct {p0, p1, p2}, LX/62l;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042456
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->g:Landroid/graphics/Rect;

    .line 1042457
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->h:Landroid/graphics/Rect;

    .line 1042458
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1042451
    invoke-direct {p0, p1, p2, p3}, LX/62l;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042452
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->g:Landroid/graphics/Rect;

    .line 1042453
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->h:Landroid/graphics/Rect;

    .line 1042454
    return-void
.end method

.method private getCount()I
    .locals 1

    .prologue
    .line 1042450
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->s()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method private getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 1042449
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    goto :goto_0
.end method

.method private getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 1042448
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->r()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v0

    goto :goto_0
.end method

.method private getScrollingViewProxy()LX/0g8;
    .locals 1

    .prologue
    .line 1042443
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i:LX/0g8;

    if-eqz v0, :cond_0

    .line 1042444
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i:LX/0g8;

    .line 1042445
    :goto_0
    return-object v0

    .line 1042446
    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/62E;->a(Landroid/view/View;)LX/0g8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i:LX/0g8;

    .line 1042447
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i:LX/0g8;

    goto :goto_0
.end method

.method private getTopVisibilityThreshold()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1042437
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v1

    .line 1042438
    if-nez v1, :cond_1

    .line 1042439
    :cond_0
    :goto_0
    return v0

    .line 1042440
    :cond_1
    invoke-static {v1}, LX/62E;->a(Landroid/view/View;)LX/0g8;

    move-result-object v1

    .line 1042441
    if-eqz v1, :cond_0

    .line 1042442
    invoke-interface {v1}, LX/0g8;->j()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/0g8;->g()I

    move-result v0

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1042463
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    .line 1042464
    instance-of v1, v0, LX/1Zi;

    if-eqz v1, :cond_1

    .line 1042465
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->j()V

    .line 1042466
    :cond_0
    :goto_0
    return-void

    .line 1042467
    :cond_1
    instance-of v0, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_0

    .line 1042468
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->k()V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1042435
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1Zi;

    new-instance v1, LX/62i;

    invoke-direct {v1, p0}, LX/62i;-><init>(Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;)V

    invoke-interface {v0, v1}, LX/1Zi;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1042436
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1042433
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62j;

    invoke-direct {v1, p0}, LX/62j;-><init>(Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1042434
    return-void
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1042426
    iget v0, p0, LX/62l;->d:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v0, v1

    .line 1042427
    :goto_0
    return v0

    .line 1042428
    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1042429
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getFirstVisiblePosition()I

    move-result v3

    if-lez v3, :cond_1

    move v0, v2

    .line 1042430
    goto :goto_0

    .line 1042431
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1042432
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getTopVisibilityThreshold()I

    move-result v3

    if-lt v0, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private m()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1042415
    iget v0, p0, LX/62l;->d:F

    const/4 v3, 0x0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    .line 1042416
    :goto_0
    return v0

    .line 1042417
    :cond_0
    invoke-virtual {p0}, LX/62l;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1042418
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getLastVisiblePosition()I

    move-result v3

    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_1

    move v0, v2

    .line 1042419
    goto :goto_0

    .line 1042420
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1042421
    if-nez v3, :cond_2

    move v0, v2

    .line 1042422
    goto :goto_0

    .line 1042423
    :cond_2
    iget-object v4, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1042424
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->g:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1042425
    iget-object v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1042402
    iget v0, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->f:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(F)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 1042408
    iget v2, p0, LX/62l;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    .line 1042409
    :cond_0
    :goto_0
    return v0

    .line 1042410
    :cond_1
    iget v2, p0, LX/62l;->c:I

    if-nez v2, :cond_3

    .line 1042411
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->l()Z

    move-result v2

    if-eqz v2, :cond_2

    cmpl-float v2, p1, v3

    if-gtz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1042412
    :cond_3
    iget v2, p0, LX/62l;->c:I

    if-ne v2, v0, :cond_5

    .line 1042413
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    cmpg-float v2, p1, v3

    if-ltz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 1042414
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown direction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/62l;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1042407
    iget v1, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->f:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x349e4f2b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1042405
    invoke-direct {p0}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i()V

    .line 1042406
    const/16 v1, 0x2d

    const v2, -0x5d29cb15

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setScrollingViewProxy(LX/0g8;)V
    .locals 0

    .prologue
    .line 1042403
    iput-object p1, p0, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->i:LX/0g8;

    .line 1042404
    return-void
.end method
