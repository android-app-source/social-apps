.class public Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/1lD;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1041610
    new-instance v0, LX/62P;

    invoke-direct {v0}, LX/62P;-><init>()V

    sput-object v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/62Q;)V
    .locals 1

    .prologue
    .line 1041604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041605
    iget-object v0, p1, LX/62Q;->a:LX/1lD;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1041606
    iget-object v0, p1, LX/62Q;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    .line 1041607
    iget-object v0, p1, LX/62Q;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->c:Ljava/lang/String;

    .line 1041608
    iget v0, p1, LX/62Q;->d:I

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->d:I

    .line 1041609
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1041598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041599
    invoke-static {}, LX/1lD;->values()[LX/1lD;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1041600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    .line 1041601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->c:Ljava/lang/String;

    .line 1041602
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->d:I

    .line 1041603
    return-void
.end method

.method public static newBuilder()LX/62Q;
    .locals 1

    .prologue
    .line 1041591
    new-instance v0, LX/62Q;

    invoke-direct {v0}, LX/62Q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1041597
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1041592
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    invoke-virtual {v0}, LX/1lD;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1041593
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1041594
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1041595
    iget v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1041596
    return-void
.end method
