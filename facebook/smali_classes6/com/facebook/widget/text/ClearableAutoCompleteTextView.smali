.class public Lcom/facebook/widget/text/ClearableAutoCompleteTextView;
.super Lcom/facebook/resources/ui/FbAutoCompleteTextView;
.source ""


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1042850
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042851
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1042848
    const v0, 0x101006b

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042849
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1042845
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042846
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042847
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1042839
    sget-object v0, LX/03r;->BetterEditTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1042840
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 1042841
    iget-object v1, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 1042842
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02081c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    .line 1042843
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1042844
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1042834
    iget-object v0, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1042835
    :goto_0
    return-void

    .line 1042836
    :cond_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1042837
    invoke-direct {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b()V

    goto :goto_0

    .line 1042838
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->c()V

    goto :goto_0
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1042806
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1042807
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v2, 0x2

    aget-object v0, v0, v2

    .line 1042808
    :goto_0
    iget-object v2, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    if-ne v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1042809
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1042810
    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1042828
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1042829
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getCompoundPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v0, v1

    .line 1042830
    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1042831
    goto :goto_0

    .line 1042832
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getCompoundPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    .line 1042833
    goto :goto_1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1042826
    iget-object v0, p0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setEndDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1042827
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1042824
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setEndDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1042825
    return-void
.end method

.method private setEndDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1042817
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v1

    .line 1042818
    invoke-virtual {p0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1042819
    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1042820
    :goto_0
    if-eqz v1, :cond_0

    const/4 v1, 0x2

    aget-object p1, v2, v1

    .line 1042821
    :cond_0
    const/4 v1, 0x1

    aget-object v1, v2, v1

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1042822
    return-void

    .line 1042823
    :cond_1
    const/4 v0, 0x0

    aget-object v0, v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1042814
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 1042815
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->a(Ljava/lang/CharSequence;)V

    .line 1042816
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, -0x4826e552

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1042811
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1042812
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1042813
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0xe7ba1d3

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
