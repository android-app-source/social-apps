.class public Lcom/facebook/widget/text/BetterButton;
.super Lcom/facebook/resources/ui/FbButton;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1042744
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/text/BetterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042745
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1042746
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042747
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042748
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1042749
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042750
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042751
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1042752
    sget-object v0, LX/03r;->BetterButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1042753
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1042754
    const/16 v2, 0x1

    sget-object v3, LX/0xr;->BUILTIN:LX/0xr;

    invoke-virtual {v3}, LX/0xr;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 1042755
    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterButton;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 1042756
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1042757
    new-instance v1, LX/0wJ;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1042758
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1042759
    return-void
.end method
