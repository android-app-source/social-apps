.class public Lcom/facebook/widget/text/TextViewWithFallback;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1043081
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043082
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3e3e433f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1043083
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1043084
    invoke-virtual {p0}, Lcom/facebook/widget/text/TextViewWithFallback;->getLineCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/text/TextViewWithFallback;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/widget/text/TextViewWithFallback;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1043085
    iget-object v1, p0, Lcom/facebook/widget/text/TextViewWithFallback;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/TextViewWithFallback;->setText(Ljava/lang/CharSequence;)V

    .line 1043086
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1043087
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x51917734

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFallbackText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1043088
    iput-object p1, p0, Lcom/facebook/widget/text/TextViewWithFallback;->a:Ljava/lang/CharSequence;

    .line 1043089
    return-void
.end method
