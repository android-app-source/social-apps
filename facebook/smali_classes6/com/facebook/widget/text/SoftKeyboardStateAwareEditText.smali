.class public Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public b:LX/Dyh;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1043043
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1043044
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    .line 1043045
    invoke-virtual {p0, p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1043046
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1043039
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043040
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    .line 1043041
    invoke-virtual {p0, p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1043042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1043047
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1043048
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    .line 1043049
    invoke-virtual {p0, p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1043050
    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1043031
    iget-object v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    if-nez v2, :cond_1

    .line 1043032
    :cond_0
    :goto_0
    return-void

    .line 1043033
    :cond_1
    if-eqz p2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-nez v2, :cond_3

    .line 1043034
    iget-object v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    invoke-virtual {v2}, LX/Dyh;->a()V

    .line 1043035
    iget-boolean v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-nez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 1043036
    :cond_3
    if-nez p2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-eqz v2, :cond_0

    .line 1043037
    iget-object v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    invoke-virtual {v2}, LX/Dyh;->b()V

    .line 1043038
    iget-boolean v2, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-nez v2, :cond_4

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1043020
    iget-object v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    if-nez v0, :cond_0

    .line 1043021
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1043022
    :goto_0
    return v0

    .line 1043023
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-eqz v0, :cond_1

    .line 1043024
    iget-object v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    invoke-virtual {v0}, LX/Dyh;->b()V

    .line 1043025
    iget-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->c:Z

    .line 1043026
    invoke-virtual {p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->clearFocus()V

    .line 1043027
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1043028
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setOnSoftKeyboardVisibleListener(LX/Dyh;)V
    .locals 0

    .prologue
    .line 1043029
    iput-object p1, p0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    .line 1043030
    return-void
.end method
