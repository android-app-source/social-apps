.class public Lcom/facebook/widget/text/FakeCursorHook;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/636;
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Landroid/graphics/Paint;

.field public c:Z

.field public d:Z

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 2

    .prologue
    .line 1042924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1042925
    iput-object p1, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1042926
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->b:Landroid/graphics/Paint;

    .line 1042927
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->b:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1042928
    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    .line 1042929
    iget v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    if-gtz v0, :cond_0

    .line 1042930
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    .line 1042931
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1042932
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1042933
    iget-boolean v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    if-nez v0, :cond_0

    .line 1042934
    :goto_0
    return-void

    .line 1042935
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->e:I

    .line 1042936
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1042937
    iget-boolean v1, v0, Lcom/facebook/widget/text/BetterTextView;->f:Z

    move v0, v1

    .line 1042938
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iput v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    .line 1042939
    iget v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    int-to-float v1, v0

    const/high16 v2, 0x41a00000    # 20.0f

    iget v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    int-to-float v3, v0

    iget v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->e:I

    add-int/lit8 v0, v0, -0x14

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/widget/text/FakeCursorHook;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1042940
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1042941
    const/4 v0, 0x0

    return v0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 1042942
    iget-boolean v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    .line 1042943
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    const/16 v2, 0x14

    iget v3, p0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    iget v4, p0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/facebook/widget/text/FakeCursorHook;->e:I

    add-int/lit8 v4, v4, -0x14

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/widget/text/BetterTextView;->invalidate(IIII)V

    .line 1042944
    iget-object v0, p0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1042945
    return-void

    .line 1042946
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
