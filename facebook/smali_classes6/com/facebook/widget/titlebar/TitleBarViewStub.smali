.class public Lcom/facebook/widget/titlebar/TitleBarViewStub;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Boolean;

.field private final c:Ljava/lang/Boolean;

.field private d:LX/01T;

.field private e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1043474
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043475
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1043472
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1043473
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1043423
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1043424
    const-class v0, Lcom/facebook/widget/titlebar/TitleBarViewStub;

    invoke-static {v0, p0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1043425
    sget-object v0, LX/03r;->TitleBarViewStub:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1043426
    const/16 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a:Ljava/lang/String;

    .line 1043427
    const/16 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->b:Ljava/lang/Boolean;

    .line 1043428
    const/16 v1, 0x4

    invoke-static {v0, v1}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->c:Ljava/lang/Boolean;

    .line 1043429
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1043430
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->setWillNotDraw(Z)V

    .line 1043431
    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1043471
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1043448
    invoke-virtual {p0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1043449
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 1043450
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewStub must have a non-null ViewGroup viewParent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1043451
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 1043452
    invoke-virtual {p0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1043453
    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->d:LX/01T;

    sget-object v3, LX/01T;->FB4A:LX/01T;

    if-ne v2, v3, :cond_3

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1043454
    const v2, 0x7f031508

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1043455
    :goto_0
    instance-of v1, v2, LX/0h5;

    if-eqz v1, :cond_4

    move-object v1, v2

    .line 1043456
    check-cast v1, LX/0h5;

    .line 1043457
    :goto_1
    iget-object v3, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1043458
    iget-object v3, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1043459
    :cond_1
    iget-object v3, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->b:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    .line 1043460
    iget-object v3, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v1, v3}, LX/0h5;->setHasBackButton(Z)V

    .line 1043461
    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 1043462
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 1043463
    invoke-virtual {p0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1043464
    if-eqz v3, :cond_5

    .line 1043465
    invoke-virtual {v0, v2, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1043466
    :goto_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->e:Ljava/lang/ref/WeakReference;

    .line 1043467
    return-void

    .line 1043468
    :cond_3
    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 1043469
    :cond_4
    const v1, 0x7f0d00bc

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    goto :goto_1

    .line 1043470
    :cond_5
    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_2
.end method

.method private a(LX/01T;Ljava/lang/Integer;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/widget/titlebar/TitleBarResourceId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1043445
    iput-object p1, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->d:LX/01T;

    .line 1043446
    iput-object p2, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->f:Ljava/lang/Integer;

    .line 1043447
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;

    invoke-static {v1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-static {}, LX/0sM;->a()Ljava/lang/Integer;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a(LX/01T;Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1043444
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1043443
    return-void
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1043441
    invoke-virtual {p0, v0, v0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->setMeasuredDimension(II)V

    .line 1043442
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 1043432
    iget-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 1043433
    iget-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarViewStub;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1043434
    if-eqz v0, :cond_1

    .line 1043435
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1043436
    :cond_0
    :goto_0
    return-void

    .line 1043437
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setVisibility called on un-referenced view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1043438
    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1043439
    if-eqz p1, :cond_3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1043440
    :cond_3
    invoke-direct {p0}, Lcom/facebook/widget/titlebar/TitleBarViewStub;->a()V

    goto :goto_0
.end method
