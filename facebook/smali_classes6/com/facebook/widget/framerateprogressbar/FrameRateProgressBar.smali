.class public Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;
.super Landroid/widget/ProgressBar;
.source ""


# instance fields
.field public a:LX/61q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0TL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:J

.field private d:Z

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1040692
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1040693
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1040694
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1040695
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1040696
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1040697
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->c:J

    .line 1040698
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->d:Z

    .line 1040699
    new-instance v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar$1;-><init>(Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;)V

    iput-object v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->e:Ljava/lang/Runnable;

    .line 1040700
    const-class v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-static {v0, p0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1040701
    iget-object v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->b:LX/0TL;

    invoke-virtual {v0}, LX/0TL;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1040702
    invoke-direct {p0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a()V

    .line 1040703
    :cond_0
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1040704
    iget-object v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a:LX/61q;

    invoke-virtual {v0}, LX/61q;->a()I

    move-result v0

    .line 1040705
    const/16 v1, 0x3c

    if-eq v0, v1, :cond_0

    if-lez v0, :cond_0

    .line 1040706
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->d:Z

    .line 1040707
    const-wide/16 v2, 0x3e8

    int-to-long v0, v0

    div-long v0, v2, v0

    iput-wide v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->c:J

    .line 1040708
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;LX/61q;LX/0TL;)V
    .locals 0

    .prologue
    .line 1040709
    iput-object p1, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a:LX/61q;

    iput-object p2, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->b:LX/0TL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-static {v1}, LX/61q;->a(LX/0QB;)LX/61q;

    move-result-object v0

    check-cast v0, LX/61q;

    invoke-static {v1}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v1

    check-cast v1, LX/0TL;

    invoke-static {p0, v0, v1}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->a(Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;LX/61q;LX/0TL;)V

    return-void
.end method

.method public static b(Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1040710
    invoke-super {p0}, Landroid/widget/ProgressBar;->postInvalidateOnAnimation()V

    .line 1040711
    return-void
.end method


# virtual methods
.method public final postInvalidateOnAnimation()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1040712
    iget-boolean v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->d:Z

    if-eqz v0, :cond_0

    .line 1040713
    iget-object v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1040714
    iget-object v0, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->e:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->c:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1040715
    :goto_0
    return-void

    .line 1040716
    :cond_0
    invoke-super {p0}, Landroid/widget/ProgressBar;->postInvalidateOnAnimation()V

    goto :goto_0
.end method
