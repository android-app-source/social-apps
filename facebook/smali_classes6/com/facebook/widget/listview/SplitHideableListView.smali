.class public Lcom/facebook/widget/listview/SplitHideableListView;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# instance fields
.field public final a:Ljava/lang/Runnable;

.field public final b:Ljava/lang/Runnable;

.field private c:LX/2qr;

.field private d:LX/2qr;

.field private e:LX/2qr;

.field private f:Landroid/view/View;

.field private g:F

.field public h:LX/62K;

.field private i:Landroid/view/WindowManager;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field public o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1041517
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    .line 1041518
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$1;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->a:Ljava/lang/Runnable;

    .line 1041519
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$2;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$2;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->b:Ljava/lang/Runnable;

    .line 1041520
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041521
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/listview/SplitHideableListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041522
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1041511
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041512
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$1;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->a:Ljava/lang/Runnable;

    .line 1041513
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$2;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$2;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->b:Ljava/lang/Runnable;

    .line 1041514
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041515
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/SplitHideableListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041516
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1041505
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1041506
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$1;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->a:Ljava/lang/Runnable;

    .line 1041507
    new-instance v0, Lcom/facebook/widget/listview/SplitHideableListView$2;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/SplitHideableListView$2;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->b:Ljava/lang/Runnable;

    .line 1041508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041509
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/SplitHideableListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041510
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 1041490
    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 1041491
    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->i:Landroid/view/WindowManager;

    .line 1041492
    sget-object v0, LX/03r;->SplitHideableListView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1041493
    const/16 v0, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->n:Z

    .line 1041494
    const/16 v0, 0x0

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->j:I

    .line 1041495
    const/16 v0, 0x1

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->k:I

    .line 1041496
    const/16 v0, 0x2

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    .line 1041497
    const/16 v0, 0x3

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->m:I

    .line 1041498
    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->j:I

    if-ne v0, v5, :cond_0

    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-eq v0, v5, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v4, "must specify listStartHeight or headerStartHeight"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1041499
    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->n:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->k:I

    if-ne v0, v5, :cond_1

    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->m:I

    if-eq v0, v5, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "must specify listHideThreshold or headerHideThreshold, or disableScrollHideList"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1041500
    invoke-direct {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->k()V

    .line 1041501
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1041502
    new-instance v0, LX/62H;

    invoke-direct {v0, p0}, LX/62H;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1041503
    return-void

    :cond_3
    move v0, v1

    .line 1041504
    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1041485
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    .line 1041486
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1041487
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041488
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1041489
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1041482
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->c:LX/2qr;

    invoke-virtual {v0}, LX/2qr;->reset()V

    .line 1041483
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->c:LX/2qr;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1041484
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1041475
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1041476
    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->i:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1041477
    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-ne v1, v3, :cond_0

    .line 1041478
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->j:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    .line 1041479
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->n:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->m:I

    if-ne v1, v3, :cond_1

    .line 1041480
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->k:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->m:I

    .line 1041481
    :cond_1
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1041463
    new-instance v0, LX/2qr;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    iget v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    invoke-direct {v0, v1, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->c:LX/2qr;

    .line 1041464
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->c:LX/2qr;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/2qr;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1041465
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->c:LX/2qr;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, LX/2qr;->setDuration(J)V

    .line 1041466
    new-instance v0, LX/2qr;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    iget v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    invoke-direct {v0, v1, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    .line 1041467
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/2qr;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1041468
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, LX/2qr;->setDuration(J)V

    .line 1041469
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    new-instance v1, LX/62I;

    invoke-direct {v1, p0}, LX/62I;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    invoke-virtual {v0, v1}, LX/2qr;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1041470
    new-instance v0, LX/2qr;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    .line 1041471
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/2qr;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1041472
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    const v1, 0x3e6b851f    # 0.23f

    iget v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/2qr;->setDuration(J)V

    .line 1041473
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    new-instance v1, LX/62J;

    invoke-direct {v1, p0}, LX/62J;-><init>(Lcom/facebook/widget/listview/SplitHideableListView;)V

    invoke-virtual {v0, v1}, LX/2qr;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1041474
    return-void
.end method

.method public static m(Lcom/facebook/widget/listview/SplitHideableListView;)V
    .locals 1

    .prologue
    .line 1041458
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->h:LX/62K;

    if-eqz v0, :cond_1

    .line 1041459
    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1041460
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    .line 1041461
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->h:LX/62K;

    invoke-interface {v0}, LX/62K;->a()V

    .line 1041462
    :cond_1
    return-void
.end method


# virtual methods
.method public final e()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 1041523
    iget-boolean v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    if-eqz v2, :cond_0

    .line 1041524
    :goto_0
    return-wide v0

    .line 1041525
    :cond_0
    iget-object v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3e6b851f    # 0.23f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1041526
    iget-object v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p0, v3, v2}, Lcom/facebook/widget/listview/SplitHideableListView;->smoothScrollBy(II)V

    .line 1041527
    iget-object v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->clearAnimation()V

    .line 1041528
    iget-object v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    if-nez v3, :cond_1

    .line 1041529
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    goto :goto_0

    .line 1041530
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    invoke-virtual {v0}, LX/2qr;->reset()V

    .line 1041531
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1041532
    int-to-long v0, v2

    iget-object v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->d:LX/2qr;

    invoke-virtual {v2}, LX/2qr;->getDuration()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final f()J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 1041450
    iget-boolean v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    if-nez v2, :cond_0

    .line 1041451
    :goto_0
    return-wide v0

    .line 1041452
    :cond_0
    iget-object v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 1041453
    iget-object v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    if-nez v2, :cond_1

    .line 1041454
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    goto :goto_0

    .line 1041455
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    invoke-virtual {v0}, LX/2qr;->reset()V

    .line 1041456
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1041457
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->e:LX/2qr;

    invoke-virtual {v0}, LX/2qr;->getDuration()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1041445
    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    if-eqz v0, :cond_0

    .line 1041446
    :goto_0
    return-void

    .line 1041447
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    .line 1041448
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041449
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1041440
    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    if-nez v0, :cond_0

    .line 1041441
    :goto_0
    return-void

    .line 1041442
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    .line 1041443
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041444
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1041375
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1041376
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1041377
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->k()V

    .line 1041378
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    if-nez v0, :cond_1

    .line 1041379
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041380
    :cond_1
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1041437
    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    if-eqz v0, :cond_0

    .line 1041438
    const/4 v0, 0x0

    .line 1041439
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1041431
    if-eqz p1, :cond_0

    .line 1041432
    invoke-direct {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->l()V

    .line 1041433
    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1041434
    invoke-direct {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->i()V

    .line 1041435
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/listview/BetterListView;->onLayout(ZIIII)V

    .line 1041436
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    const v2, 0x4d2bcbb

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1041394
    iget-object v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    if-eqz v3, :cond_1

    .line 1041395
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x1bdbad87

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1041396
    :goto_0
    return v0

    .line 1041397
    :cond_1
    iget-boolean v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->o:Z

    if-eqz v3, :cond_2

    .line 1041398
    const v1, 0x3357e937

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1041399
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1041400
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x7790a26f

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1041401
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1041402
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->g:F

    goto :goto_1

    .line 1041403
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1041404
    iget-boolean v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->n:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->m:I

    if-lt v0, v1, :cond_4

    .line 1041405
    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->e()J

    goto :goto_1

    .line 1041406
    :cond_4
    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-le v0, v1, :cond_3

    .line 1041407
    invoke-direct {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->j()V

    goto :goto_1

    .line 1041408
    :pswitch_2
    iget-object v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-nez v3, :cond_3

    .line 1041409
    iget v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->g:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 1041410
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    iput v3, p0, Lcom/facebook/widget/listview/SplitHideableListView;->g:F

    .line 1041411
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    iget v4, p0, Lcom/facebook/widget/listview/SplitHideableListView;->g:F

    sub-float/2addr v3, v4

    .line 1041412
    invoke-virtual {p0}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v4

    .line 1041413
    int-to-float v5, v4

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v3, v6

    sub-float/2addr v5, v6

    int-to-float v4, v4

    div-float v4, v5, v4

    .line 1041414
    iget v5, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    int-to-float v5, v5

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    .line 1041415
    iget v4, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    int-to-float v4, v4

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    .line 1041416
    iget-boolean v4, p0, Lcom/facebook/widget/listview/SplitHideableListView;->n:Z

    if-eqz v4, :cond_6

    iget v4, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-gt v3, v4, :cond_3

    .line 1041417
    :cond_6
    iget-object v4, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 1041418
    iget-object v5, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iput v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041419
    iget-object v5, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->requestLayout()V

    .line 1041420
    iget-object v5, p0, Lcom/facebook/widget/listview/SplitHideableListView;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v5}, Lcom/facebook/widget/listview/SplitHideableListView;->post(Ljava/lang/Runnable;)Z

    .line 1041421
    iget v5, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-le v3, v5, :cond_8

    .line 1041422
    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-ne v4, v0, :cond_7

    .line 1041423
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1041424
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1041425
    invoke-super {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1041426
    :cond_7
    const v0, -0x14ce99c7

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    .line 1041427
    :cond_8
    iget v1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    if-ne v3, v1, :cond_3

    if-ge v3, v4, :cond_3

    .line 1041428
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1041429
    invoke-virtual {v1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1041430
    invoke-super {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setExpandableHeader(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1041383
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1041384
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 1041385
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1041386
    goto :goto_0

    .line 1041387
    :cond_2
    if-nez p1, :cond_3

    .line 1041388
    iget-object v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/SplitHideableListView;->removeHeaderView(Landroid/view/View;)Z

    .line 1041389
    :goto_2
    iput-object p1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->f:Landroid/view/View;

    goto :goto_1

    .line 1041390
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-boolean v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->p:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1041391
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 1041392
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/widget/listview/SplitHideableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    goto :goto_2

    .line 1041393
    :cond_4
    iget v0, p0, Lcom/facebook/widget/listview/SplitHideableListView;->l:I

    goto :goto_3
.end method

.method public setOnSplitHeightChangedListener(LX/62K;)V
    .locals 0

    .prologue
    .line 1041381
    iput-object p1, p0, Lcom/facebook/widget/listview/SplitHideableListView;->h:LX/62K;

    .line 1041382
    return-void
.end method
