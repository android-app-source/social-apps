.class public final Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/61z;


# direct methods
.method public constructor <init>(LX/61z;)V
    .locals 0

    .prologue
    .line 1040916
    iput-object p1, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1040917
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget v0, v0, LX/61z;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 1040918
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v0, v0, LX/61z;->a:LX/620;

    invoke-static {v0, p0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1040919
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v0, v0, LX/61z;->a:LX/620;

    iget-object v0, v0, LX/620;->C:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1040920
    iget-object v2, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-wide v2, v2, LX/61z;->b:J

    sub-long v2, v0, v2

    .line 1040921
    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    long-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget v3, v3, LX/61z;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1040922
    iget-object v3, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    .line 1040923
    iput-wide v0, v3, LX/61z;->b:J

    .line 1040924
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v0, v0, LX/61z;->a:LX/620;

    iget-object v1, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v1, v1, LX/61z;->a:LX/620;

    iget v1, v1, LX/620;->p:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v6, v1}, LX/620;->pointToPosition(II)I

    move-result v0

    .line 1040925
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1040926
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v0, v0, LX/61z;->a:LX/620;

    iget-object v1, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v1, v1, LX/61z;->a:LX/620;

    iget v1, v1, LX/620;->p:I

    div-int/lit8 v1, v1, 0x2

    iget-object v3, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v3, v3, LX/61z;->a:LX/620;

    invoke-virtual {v3}, LX/620;->getDividerHeight()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x40

    invoke-virtual {v0, v6, v1}, LX/620;->pointToPosition(II)I

    move-result v0

    .line 1040927
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v1, v1, LX/61z;->a:LX/620;

    iget-object v3, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v3, v3, LX/61z;->a:LX/620;

    invoke-virtual {v3}, LX/620;->getFirstVisiblePosition()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v1, v3}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1040928
    if-eqz v1, :cond_1

    .line 1040929
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1040930
    iget-object v3, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v3, v3, LX/61z;->a:LX/620;

    sub-int/2addr v1, v2

    invoke-virtual {v3, v0, v1}, LX/620;->setSelectionFromTop(II)V

    .line 1040931
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    iget-object v0, v0, LX/61z;->a:LX/620;

    invoke-static {v0}, LX/620;->f(LX/620;)V

    .line 1040932
    :cond_1
    :goto_0
    return-void

    .line 1040933
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/listview/DragSortListView$ScrollHandler$1;->a:LX/61z;

    .line 1040934
    iput-boolean v6, v0, LX/61z;->c:Z

    .line 1040935
    goto :goto_0
.end method
