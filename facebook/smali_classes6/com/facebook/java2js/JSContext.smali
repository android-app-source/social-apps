.class public Lcom/facebook/java2js/JSContext;
.super LX/5SN;
.source ""


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sContextCache:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/java2js/JSContext;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mGlobalObject:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/java2js/JSValue;",
            ">;"
        }
    .end annotation
.end field

.field public mJSGlobalContextRef:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final mNullJSValue:Lcom/facebook/java2js/JSValue;

.field private final mSoftErrorReporter:LX/5L0;

.field public final mUndefinedJSValue:Lcom/facebook/java2js/JSValue;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 918015
    const-string v0, "reactnativejnifb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 918016
    const-string v0, "java2js"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 918017
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    sput-object v0, Lcom/facebook/java2js/JSContext;->sContextCache:LX/0tf;

    .line 918018
    const-class v0, Lcom/facebook/java2js/JSContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/java2js/JSContext;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5L0;)V
    .locals 7

    .prologue
    .line 917991
    sget-object v0, Lcom/facebook/java2js/JSContext;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/5SN;-><init>(Ljava/lang/String;)V

    .line 917992
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/java2js/JSContext;->mGlobalObject:Ljava/lang/ref/WeakReference;

    .line 917993
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 917994
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 917995
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSContext;->initHybrid(Ljava/lang/String;)V

    .line 917996
    iput-object p2, p0, Lcom/facebook/java2js/JSContext;->mSoftErrorReporter:LX/5L0;

    .line 917997
    sget-object v1, Lcom/facebook/java2js/JSContext;->sContextCache:LX/0tf;

    monitor-enter v1

    .line 917998
    :try_start_0
    sget-object v0, Lcom/facebook/java2js/JSContext;->sContextCache:LX/0tf;

    iget-wide v2, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3, v4}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 917999
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 918000
    iget-wide v5, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v5

    .line 918001
    invoke-static {v0, v1}, Lcom/facebook/java2js/JSValue;->makeNullInternal(J)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/java2js/JSContext;->mNullJSValue:Lcom/facebook/java2js/JSValue;

    .line 918002
    iget-wide v5, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v5

    .line 918003
    invoke-static {v0, v1}, Lcom/facebook/java2js/JSValue;->makeUndefinedInternal(J)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/java2js/JSContext;->mUndefinedJSValue:Lcom/facebook/java2js/JSValue;

    .line 918004
    return-void

    .line 918005
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private native callModuleMethodNative(Ljava/lang/String;Ljava/lang/String;[Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
.end method

.method public static getFromJSGlobalContextRefLong(J)Lcom/facebook/java2js/JSContext;
    .locals 4

    .prologue
    .line 918007
    sget-object v1, Lcom/facebook/java2js/JSContext;->sContextCache:LX/0tf;

    monitor-enter v1

    .line 918008
    :try_start_0
    sget-object v0, Lcom/facebook/java2js/JSContext;->sContextCache:LX/0tf;

    invoke-virtual {v0, p0, p1}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 918009
    if-eqz v0, :cond_0

    .line 918010
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/java2js/JSContext;

    .line 918011
    if-eqz v0, :cond_0

    .line 918012
    monitor-exit v1

    return-object v0

    .line 918013
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Context does not exist"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918014
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private native getGlobalObjectNative()Lcom/facebook/java2js/JSValue;
.end method

.method public static native getMemoryMetrics()Lcom/facebook/java2js/JscMemoryMetrics;
.end method

.method private native initHybrid(Ljava/lang/String;)V
.end method


# virtual methods
.method public final varargs callModuleMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918006
    invoke-static {p0, p3}, Lcom/facebook/java2js/JSValue;->toJSValues(Lcom/facebook/java2js/JSContext;[Ljava/lang/Object;)[Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/java2js/JSContext;->callModuleMethodNative(Ljava/lang/String;Ljava/lang/String;[Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public native evaluateSourceCode(Landroid/content/res/AssetManager;Ljava/lang/String;)V
.end method

.method public native evaluateSourceCode(Ljava/io/File;)V
.end method

.method public finalize()V
    .locals 4

    .prologue
    .line 917974
    :try_start_0
    invoke-super {p0}, LX/5SN;->finalize()V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 917975
    return-void

    .line 917976
    :catch_0
    move-exception v0

    .line 917977
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TimeoutException in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/facebook/java2js/JSContext;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public native garbageCollect()V
.end method

.method public final declared-synchronized getGlobalObject()Lcom/facebook/java2js/JSValue;
    .locals 2

    .prologue
    .line 917978
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mGlobalObject:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/java2js/JSValue;

    .line 917979
    if-nez v0, :cond_0

    .line 917980
    invoke-direct {p0}, Lcom/facebook/java2js/JSContext;->getGlobalObjectNative()Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 917981
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/facebook/java2js/JSContext;->mGlobalObject:Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 917982
    :cond_0
    monitor-exit p0

    return-object v0

    .line 917983
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNullJSValue()Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 917984
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mNullJSValue:Lcom/facebook/java2js/JSValue;

    return-object v0
.end method

.method public getUndefinedJSValue()Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 917985
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mUndefinedJSValue:Lcom/facebook/java2js/JSValue;

    return-object v0
.end method

.method public ref()J
    .locals 2

    .prologue
    .line 917986
    iget-wide v0, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    return-wide v0
.end method

.method public reportSoftError(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 917987
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mSoftErrorReporter:LX/5L0;

    if-eqz v0, :cond_0

    .line 917988
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mSoftErrorReporter:LX/5L0;

    .line 917989
    iget-object v1, v0, LX/5L0;->a:LX/0dl;

    iget-object v1, v1, LX/0dl;->d:LX/03V;

    const-string p0, "componentscript"

    invoke-virtual {v1, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 917990
    :cond_0
    return-void
.end method
