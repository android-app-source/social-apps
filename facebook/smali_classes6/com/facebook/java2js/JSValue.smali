.class public final Lcom/facebook/java2js/JSValue;
.super LX/5SN;
.source ""


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static performanceListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5SR;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBool:Z

.field private mJSContext:Lcom/facebook/java2js/JSContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mJSGlobalContextRef:J

.field private final mJSValueType:LX/5SU;

.field private mJavaObject:Ljava/lang/Object;

.field private mNumber:D

.field private mString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 918235
    const-class v0, Lcom/facebook/java2js/JSValue;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/java2js/JSValue;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(JLX/5SU;)V
    .locals 1

    .prologue
    .line 918230
    sget-object v0, Lcom/facebook/java2js/JSValue;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/5SN;-><init>(Ljava/lang/String;)V

    .line 918231
    iput-wide p1, p0, Lcom/facebook/java2js/JSValue;->mJSGlobalContextRef:J

    .line 918232
    iput-object p3, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    .line 918233
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-static {v0}, Lcom/facebook/java2js/JSValue;->onValueCreated(LX/5SU;)V

    .line 918234
    return-void
.end method

.method public static addPerformanceListener(LX/5SR;)V
    .locals 1

    .prologue
    .line 918226
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 918227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    .line 918228
    :cond_0
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 918229
    return-void
.end method

.method private native callAsFunctionNative(Lcom/facebook/java2js/JSValue;[Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
.end method

.method private checkCanHaveProperties()V
    .locals 3

    .prologue
    .line 918223
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isFunction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918224
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to access properties on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918225
    :cond_0
    return-void
.end method

.method private native getBooleanPropertyAtIndexNative(I)Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method private native getBooleanPropertyNative(Ljava/lang/String;)Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method private getJSContext()Lcom/facebook/java2js/JSContext;
    .locals 2

    .prologue
    .line 918220
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSContext:Lcom/facebook/java2js/JSContext;

    if-nez v0, :cond_0

    .line 918221
    iget-wide v0, p0, Lcom/facebook/java2js/JSValue;->mJSGlobalContextRef:J

    invoke-static {v0, v1}, Lcom/facebook/java2js/JSContext;->getFromJSGlobalContextRefLong(J)Lcom/facebook/java2js/JSContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSContext:Lcom/facebook/java2js/JSContext;

    .line 918222
    :cond_0
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSContext:Lcom/facebook/java2js/JSContext;

    return-object v0
.end method

.method private static getJavaProperty(JLjava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 918193
    invoke-static {p0, p1}, Lcom/facebook/java2js/JSContext;->getFromJSGlobalContextRefLong(J)Lcom/facebook/java2js/JSContext;

    move-result-object v2

    .line 918194
    const-string v0, "toJSON"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918195
    new-instance v0, LX/5ST;

    invoke-direct {v0, v2, p2}, LX/5ST;-><init>(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)V

    invoke-static {v2, v0}, Lcom/facebook/java2js/JSValue;->makeFunction(Lcom/facebook/java2js/JSContext;Lcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 918196
    :goto_0
    return-object v0

    .line 918197
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 918198
    sget-object v1, LX/5SO;->sMethodCache:LX/0aq;

    invoke-virtual {v1, v0}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/026;

    .line 918199
    if-nez v1, :cond_5

    .line 918200
    new-instance v6, LX/026;

    invoke-direct {v6}, LX/026;-><init>()V

    .line 918201
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v7

    array-length v8, v7

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v8, :cond_4

    aget-object p0, v7, v5

    .line 918202
    const-class v1, Lcom/facebook/java2js/annotation/JSExport;

    invoke-virtual {p0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/facebook/java2js/annotation/JSExport;

    .line 918203
    if-eqz v1, :cond_2

    .line 918204
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/facebook/java2js/annotation/JSExport;->as()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    .line 918205
    :goto_2
    new-instance p1, LX/5SO;

    invoke-interface {v1}, Lcom/facebook/java2js/annotation/JSExport;->mode()LX/5SV;

    move-result-object v1

    invoke-direct {p1, p0, v1}, LX/5SO;-><init>(Ljava/lang/reflect/Method;LX/5SV;)V

    invoke-virtual {v6, v3, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918206
    :cond_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 918207
    :cond_3
    invoke-interface {v1}, Lcom/facebook/java2js/annotation/JSExport;->as()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 918208
    :cond_4
    move-object v1, v6

    .line 918209
    sget-object v3, LX/5SO;->sMethodCache:LX/0aq;

    invoke-virtual {v3, v0, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918210
    :cond_5
    move-object v0, v1

    .line 918211
    invoke-virtual {v0, p3}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SO;

    .line 918212
    if-eqz v0, :cond_6

    .line 918213
    sget-object v1, LX/5SQ;->$SwitchMap$com$facebook$java2js$annotation$JSExport$Mode:[I

    iget-object v3, v0, LX/5SO;->mode:LX/5SV;

    invoke-virtual {v3}, LX/5SV;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 918214
    :cond_6
    const-string v1, "Failed to read %s on class %s"

    if-eqz p2, :cond_7

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v1, p3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSContext;->reportSoftError(Ljava/lang/String;)V

    .line 918215
    invoke-static {v2}, Lcom/facebook/java2js/JSValue;->makeUndefined(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    goto/16 :goto_0

    .line 918216
    :pswitch_0
    iget-object v0, v0, LX/5SO;->method:Ljava/lang/reflect/Method;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    goto/16 :goto_0

    .line 918217
    :pswitch_1
    new-instance v1, Lcom/facebook/java2js/UncacheablePropertyReadResult;

    iget-object v0, v0, LX/5SO;->method:Ljava/lang/reflect/Method;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0, p2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/java2js/UncacheablePropertyReadResult;-><init>(Lcom/facebook/java2js/JSValue;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 918218
    :pswitch_2
    new-instance v1, LX/5SS;

    iget-object v0, v0, LX/5SO;->method:Ljava/lang/reflect/Method;

    invoke-direct {v1, v2, p2, v0}, LX/5SS;-><init>(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    invoke-static {v2, v1}, Lcom/facebook/java2js/JSValue;->makeFunction(Lcom/facebook/java2js/JSContext;Lcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    goto/16 :goto_0

    .line 918219
    :cond_7
    const-string v0, "(null)"

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private native getNumberPropertyAtIndexNative(I)D
.end method

.method private native getNumberPropertyNative(Ljava/lang/String;)D
.end method

.method private native getPropertyAtIndexNative(I)Lcom/facebook/java2js/JSValue;
.end method

.method private native getPropertyNamesNative()[Ljava/lang/String;
.end method

.method private native getPropertyNative(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;
.end method

.method private native getStringPropertyAtIndexNative(I)Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method private native getStringPropertyNative(Ljava/lang/String;)Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method private native hasPropertyNative(Ljava/lang/String;)Z
.end method

.method private native initArrayHybrid(J)V
.end method

.method private native initBooleanHybrid(JJZ)V
.end method

.method private native initFunctionHybrid(JJLcom/facebook/java2js/Invokable;)V
.end method

.method private native initNullHybrid(J)V
.end method

.method private native initNumberHybrid(JJD)V
.end method

.method private native initObjectHybrid(JJLjava/lang/Object;)V
.end method

.method private native initStringHybrid(JJLjava/lang/String;)V
.end method

.method private native initUndefinedHybrid(J)V
.end method

.method public static make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 2

    .prologue
    .line 918280
    if-nez p1, :cond_0

    .line 918281
    invoke-static {p0}, Lcom/facebook/java2js/JSValue;->makeNull(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    .line 918282
    :goto_0
    return-object p1

    .line 918283
    :cond_0
    instance-of v0, p1, Lcom/facebook/java2js/JSValue;

    if-eqz v0, :cond_1

    .line 918284
    check-cast p1, Lcom/facebook/java2js/JSValue;

    goto :goto_0

    .line 918285
    :cond_1
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 918286
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/java2js/JSValue;->makeBoolean(Lcom/facebook/java2js/JSContext;Z)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    goto :goto_0

    .line 918287
    :cond_2
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_3

    .line 918288
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/facebook/java2js/JSValue;->makeNumber(Lcom/facebook/java2js/JSContext;D)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    goto :goto_0

    .line 918289
    :cond_3
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 918290
    check-cast p1, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/facebook/java2js/JSValue;->makeString(Lcom/facebook/java2js/JSContext;Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    goto :goto_0

    .line 918291
    :cond_4
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 918292
    check-cast p1, Ljava/util/Map;

    invoke-static {p0, p1}, Lcom/facebook/java2js/JSValue;->makeObjectFromMap(Lcom/facebook/java2js/JSContext;Ljava/util/Map;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    goto :goto_0

    .line 918293
    :cond_5
    invoke-static {p0, p1}, Lcom/facebook/java2js/JSValue;->makeObject(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    goto :goto_0
.end method

.method public static makeArray(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 918294
    new-instance v0, Lcom/facebook/java2js/JSValue;

    .line 918295
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v2, v4

    .line 918296
    sget-object v1, LX/5SU;->JSARRAY:LX/5SU;

    invoke-direct {v0, v2, v3, v1}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918297
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v2, v4

    .line 918298
    invoke-direct {v0, v2, v3}, Lcom/facebook/java2js/JSValue;->initArrayHybrid(J)V

    .line 918299
    return-object v0
.end method

.method public static makeBoolean(Lcom/facebook/java2js/JSContext;Z)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 918300
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v4

    .line 918301
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/facebook/java2js/JSValue;->makeBooleanInternal(JJZ)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method private static makeBooleanInternal(JJZ)Lcom/facebook/java2js/JSValue;
    .locals 8

    .prologue
    .line 918302
    new-instance v1, Lcom/facebook/java2js/JSValue;

    sget-object v0, LX/5SU;->JSBOOLEAN:LX/5SU;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918303
    iput-boolean p4, v1, Lcom/facebook/java2js/JSValue;->mBool:Z

    move-wide v2, p0

    move-wide v4, p2

    move v6, p4

    .line 918304
    invoke-direct/range {v1 .. v6}, Lcom/facebook/java2js/JSValue;->initBooleanHybrid(JJZ)V

    .line 918305
    return-object v1
.end method

.method public static makeFunction(Lcom/facebook/java2js/JSContext;Lcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 918306
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v4

    .line 918307
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/facebook/java2js/JSValue;->makeFunctionInternal(JJLcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method private static makeFunctionInternal(JJLcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;
    .locals 8

    .prologue
    .line 918308
    new-instance v1, Lcom/facebook/java2js/JSValue;

    sget-object v0, LX/5SU;->JSFUNCTION:LX/5SU;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    .line 918309
    invoke-direct/range {v1 .. v6}, Lcom/facebook/java2js/JSValue;->initFunctionHybrid(JJLcom/facebook/java2js/Invokable;)V

    .line 918310
    return-object v1
.end method

.method public static makeNull(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918311
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mNullJSValue:Lcom/facebook/java2js/JSValue;

    move-object v0, v0

    .line 918312
    return-object v0
.end method

.method public static makeNullInternal(J)Lcom/facebook/java2js/JSValue;
    .locals 2

    .prologue
    .line 918275
    new-instance v0, Lcom/facebook/java2js/JSValue;

    sget-object v1, LX/5SU;->JSNULL:LX/5SU;

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918276
    invoke-direct {v0, p0, p1}, Lcom/facebook/java2js/JSValue;->initNullHybrid(J)V

    .line 918277
    return-object v0
.end method

.method public static makeNumber(Lcom/facebook/java2js/JSContext;D)Lcom/facebook/java2js/JSValue;
    .locals 9

    .prologue
    .line 918278
    iget-wide v7, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v7

    .line 918279
    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/java2js/JSValue;->makeNumberInternal(JJD)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method private static makeNumberInternal(JJD)Lcom/facebook/java2js/JSValue;
    .locals 8

    .prologue
    .line 918271
    new-instance v1, Lcom/facebook/java2js/JSValue;

    sget-object v0, LX/5SU;->JSNUMBER:LX/5SU;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918272
    iput-wide p4, v1, Lcom/facebook/java2js/JSValue;->mNumber:D

    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    .line 918273
    invoke-direct/range {v1 .. v7}, Lcom/facebook/java2js/JSValue;->initNumberHybrid(JJD)V

    .line 918274
    return-object v1
.end method

.method public static makeObject(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918270
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/java2js/JSValue;->makeObject(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public static makeObject(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 918268
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v4

    .line 918269
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/facebook/java2js/JSValue;->makeObjectInternal(JJLjava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public static makeObjectFromMap(Lcom/facebook/java2js/JSContext;Ljava/util/Map;)Lcom/facebook/java2js/JSValue;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/java2js/JSContext;",
            "Ljava/util/Map",
            "<**>;)",
            "Lcom/facebook/java2js/JSValue;"
        }
    .end annotation

    .prologue
    .line 918259
    invoke-static {p0}, Lcom/facebook/java2js/JSValue;->makeObject(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 918260
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 918261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 918262
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 918263
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/java2js/JSValue;->setPropertyAtIndex(ILjava/lang/Object;)V

    goto :goto_0

    .line 918264
    :cond_0
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 918265
    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 918266
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid key of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918267
    :cond_2
    return-object v2
.end method

.method private static makeObjectInternal(JJLjava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 8

    .prologue
    .line 918255
    new-instance v1, Lcom/facebook/java2js/JSValue;

    sget-object v0, LX/5SU;->JSOBJECT:LX/5SU;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918256
    iput-object p4, v1, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    .line 918257
    invoke-direct/range {v1 .. v6}, Lcom/facebook/java2js/JSValue;->initObjectHybrid(JJLjava/lang/Object;)V

    .line 918258
    return-object v1
.end method

.method public static makeString(Lcom/facebook/java2js/JSContext;Ljava/lang/String;)Lcom/facebook/java2js/JSValue;
    .locals 6

    .prologue
    .line 918253
    iget-wide v4, p0, Lcom/facebook/java2js/JSContext;->mJSGlobalContextRef:J

    move-wide v0, v4

    .line 918254
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/facebook/java2js/JSValue;->makeStringInternal(JJLjava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method private static makeStringInternal(JJLjava/lang/String;)Lcom/facebook/java2js/JSValue;
    .locals 8

    .prologue
    .line 918249
    new-instance v1, Lcom/facebook/java2js/JSValue;

    sget-object v0, LX/5SU;->JSSTRING:LX/5SU;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918250
    iput-object p4, v1, Lcom/facebook/java2js/JSValue;->mString:Ljava/lang/String;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    .line 918251
    invoke-direct/range {v1 .. v6}, Lcom/facebook/java2js/JSValue;->initStringHybrid(JJLjava/lang/String;)V

    .line 918252
    return-object v1
.end method

.method public static makeUndefined(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918247
    iget-object v0, p0, Lcom/facebook/java2js/JSContext;->mUndefinedJSValue:Lcom/facebook/java2js/JSValue;

    move-object v0, v0

    .line 918248
    return-object v0
.end method

.method public static makeUndefinedInternal(J)Lcom/facebook/java2js/JSValue;
    .locals 2

    .prologue
    .line 918244
    new-instance v0, Lcom/facebook/java2js/JSValue;

    sget-object v1, LX/5SU;->JSUNDEFINED:LX/5SU;

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/java2js/JSValue;-><init>(JLX/5SU;)V

    .line 918245
    invoke-direct {v0, p0, p1}, Lcom/facebook/java2js/JSValue;->initUndefinedHybrid(J)V

    .line 918246
    return-object v0
.end method

.method private static onValueCreated(LX/5SU;)V
    .locals 3

    .prologue
    .line 918239
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 918240
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SR;

    .line 918241
    invoke-interface {v0, p0}, LX/5SR;->onValueCreated(LX/5SU;)V

    .line 918242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 918243
    :cond_0
    return-void
.end method

.method public static removePerformanceListener(LX/5SR;)V
    .locals 1

    .prologue
    .line 918236
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 918237
    sget-object v0, Lcom/facebook/java2js/JSValue;->performanceListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 918238
    :cond_0
    return-void
.end method

.method private native setBooleanPropertyAtIndexNative(IZ)V
.end method

.method private native setBooleanPropertyNative(Ljava/lang/String;Z)V
.end method

.method private native setNumberPropertyAtIndexNative(ID)V
.end method

.method private native setNumberPropertyNative(Ljava/lang/String;D)V
.end method

.method private native setPropertyAtIndexNative(ILcom/facebook/java2js/JSValue;)V
.end method

.method private native setPropertyNative(Ljava/lang/String;Lcom/facebook/java2js/JSValue;)V
.end method

.method private native setPropertyNative(Ljava/lang/String;Lcom/facebook/java2js/JSValue;I)V
.end method

.method private native setStringPropertyAtIndexNative(ILjava/lang/String;)V
.end method

.method private native setStringPropertyNative(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private toInternal(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 918098
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isNull()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 918099
    :cond_0
    const/4 p0, 0x0

    .line 918100
    :cond_1
    :goto_0
    return-object p0

    .line 918101
    :cond_2
    const-class v0, Ljava/lang/Boolean;

    if-eq p1, v0, :cond_3

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_4

    .line 918102
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asBoolean()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    goto :goto_0

    .line 918103
    :cond_4
    const-class v0, Ljava/lang/Double;

    if-eq p1, v0, :cond_5

    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_5

    const-class v0, Ljava/lang/Number;

    if-ne p1, v0, :cond_6

    .line 918104
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    goto :goto_0

    .line 918105
    :cond_6
    const-class v0, Ljava/lang/Integer;

    if-eq p1, v0, :cond_7

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_8

    .line 918106
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    .line 918107
    :cond_8
    const-class v0, Ljava/lang/Long;

    if-eq p1, v0, :cond_9

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_a

    .line 918108
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v0

    double-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_0

    .line 918109
    :cond_a
    const-class v0, Ljava/lang/Float;

    if-eq p1, v0, :cond_b

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_c

    .line 918110
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    goto :goto_0

    .line 918111
    :cond_c
    const-class v0, Ljava/lang/String;

    if-ne p1, v0, :cond_d

    .line 918112
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 918113
    :cond_d
    const-class v0, Lcom/facebook/java2js/JSValue;

    if-eq p1, v0, :cond_1

    .line 918114
    const-class v0, Ljava/lang/Object;

    if-ne p1, v0, :cond_11

    .line 918115
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isBoolean()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 918116
    iget-boolean v0, p0, Lcom/facebook/java2js/JSValue;->mBool:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    goto :goto_0

    .line 918117
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isNumber()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 918118
    iget-wide v0, p0, Lcom/facebook/java2js/JSValue;->mNumber:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    goto/16 :goto_0

    .line 918119
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isString()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 918120
    iget-object p0, p0, Lcom/facebook/java2js/JSValue;->mString:Ljava/lang/String;

    goto/16 :goto_0

    .line 918121
    :cond_10
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 918122
    iget-object p0, p0, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    goto/16 :goto_0

    .line 918123
    :cond_11
    iget-object p0, p0, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static toJSValues(Lcom/facebook/java2js/JSContext;[Ljava/lang/Object;)[Lcom/facebook/java2js/JSValue;
    .locals 3

    .prologue
    .line 918093
    array-length v0, p1

    new-array v1, v0, [Lcom/facebook/java2js/JSValue;

    .line 918094
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 918095
    aget-object v2, p1, v0

    invoke-static {p0, v2}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    aput-object v2, v1, v0

    .line 918096
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918097
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final asArray(Ljava/lang/Class;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 918085
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918086
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSOBJECT:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918087
    :cond_0
    const-string v0, "length"

    invoke-virtual {p0, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/java2js/JSValue;->asNumber()D

    move-result-wide v0

    double-to-int v2, v0

    .line 918088
    invoke-static {p1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 918089
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 918090
    invoke-virtual {p0, v1}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 918091
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 918092
    :cond_1
    return-object v0
.end method

.method public final asBoolean()Z
    .locals 3

    .prologue
    .line 918082
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isBoolean()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918083
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSBOOLEAN:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918084
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/java2js/JSValue;->mBool:Z

    return v0
.end method

.method public final asJavaObject()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 918044
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918045
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSOBJECT:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918046
    :cond_0
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    return-object v0
.end method

.method public final asMap(Ljava/lang/Class;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 918073
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918074
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSOBJECT:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918075
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 918076
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->getPropertyNamesNative()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 918077
    invoke-virtual {p0, v4}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918078
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918079
    :cond_1
    return-object v1
.end method

.method public final asNumber()D
    .locals 3

    .prologue
    .line 918070
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isNumber()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918071
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSNUMBER:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918072
    :cond_0
    iget-wide v0, p0, Lcom/facebook/java2js/JSValue;->mNumber:D

    return-wide v0
.end method

.method public final asString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 918067
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isString()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918068
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSSTRING:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918069
    :cond_0
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mString:Ljava/lang/String;

    return-object v0
.end method

.method public final varargs callAsFunction([Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 3

    .prologue
    .line 918064
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isFunction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918065
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSFUNCTION:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918066
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->getJSContext()Lcom/facebook/java2js/JSContext;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/java2js/JSValue;->toJSValues(Lcom/facebook/java2js/JSContext;[Ljava/lang/Object;)[Lcom/facebook/java2js/JSValue;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/java2js/JSValue;->callAsFunctionNative(Lcom/facebook/java2js/JSValue;[Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public final finalize()V
    .locals 4

    .prologue
    .line 918060
    :try_start_0
    invoke-super {p0}, LX/5SN;->finalize()V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 918061
    return-void

    .line 918062
    :catch_0
    move-exception v0

    .line 918063
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TimeoutException in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v3, Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final getBooleanProperty(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918058
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918059
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getBooleanPropertyNative(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final getBooleanPropertyAtIndex(I)Ljava/lang/Boolean;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918056
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918057
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getBooleanPropertyAtIndexNative(I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final getJavaObject()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918053
    invoke-virtual {p0}, Lcom/facebook/java2js/JSValue;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 918054
    new-instance v0, LX/5SP;

    sget-object v1, LX/5SU;->JSOBJECT:LX/5SU;

    iget-object v2, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v0, v1, v2}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v0

    .line 918055
    :cond_0
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJavaObject:Ljava/lang/Object;

    return-object v0
.end method

.method public final getNumberProperty(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 918051
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918052
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getNumberPropertyNative(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getNumberPropertyAtIndex(I)D
    .locals 2

    .prologue
    .line 918049
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918050
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getNumberPropertyAtIndexNative(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918047
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918048
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getPropertyNative(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public final getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;
    .locals 1

    .prologue
    .line 918080
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918081
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndexNative(I)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public final getStringProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918177
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918178
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getStringPropertyNative(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getStringPropertyAtIndex(I)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918191
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918192
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->getStringPropertyAtIndexNative(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hasProperty(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 918189
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918190
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->hasPropertyNative(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final varargs invokeMethod(Ljava/lang/String;[Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;
    .locals 3

    .prologue
    .line 918185
    invoke-virtual {p0, p1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 918186
    iget-object v1, v0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v1}, LX/5SU;->isFunction()Z

    move-result v1

    if-nez v1, :cond_0

    .line 918187
    new-instance v1, LX/5SP;

    sget-object v2, LX/5SU;->JSFUNCTION:LX/5SU;

    iget-object v0, v0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-direct {v1, v2, v0}, LX/5SP;-><init>(LX/5SU;LX/5SU;)V

    throw v1

    .line 918188
    :cond_0
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->getJSContext()Lcom/facebook/java2js/JSContext;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/facebook/java2js/JSValue;->toJSValues(Lcom/facebook/java2js/JSContext;[Ljava/lang/Object;)[Lcom/facebook/java2js/JSValue;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/facebook/java2js/JSValue;->callAsFunctionNative(Lcom/facebook/java2js/JSValue;[Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    return-object v0
.end method

.method public final isArray()Z
    .locals 1

    .prologue
    .line 918184
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isArray()Z

    move-result v0

    return v0
.end method

.method public final isBoolean()Z
    .locals 1

    .prologue
    .line 918183
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isBoolean()Z

    move-result v0

    return v0
.end method

.method public final isFunction()Z
    .locals 1

    .prologue
    .line 918182
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isFunction()Z

    move-result v0

    return v0
.end method

.method public final isNull()Z
    .locals 1

    .prologue
    .line 918181
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isNull()Z

    move-result v0

    return v0
.end method

.method public final isNumber()Z
    .locals 1

    .prologue
    .line 918180
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isNumber()Z

    move-result v0

    return v0
.end method

.method public final isObject()Z
    .locals 1

    .prologue
    .line 918179
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isObject()Z

    move-result v0

    return v0
.end method

.method public final native isStrictEqual(Lcom/facebook/java2js/JSValue;)Z
.end method

.method public final isString()Z
    .locals 1

    .prologue
    .line 918124
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isString()Z

    move-result v0

    return v0
.end method

.method public final isUndefined()Z
    .locals 1

    .prologue
    .line 918176
    iget-object v0, p0, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v0}, LX/5SU;->isUndefined()Z

    move-result v0

    return v0
.end method

.method public final setBooleanProperty(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 918173
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918174
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setBooleanPropertyNative(Ljava/lang/String;Z)V

    .line 918175
    return-void
.end method

.method public final setBooleanPropertyAtIndex(IZ)V
    .locals 0

    .prologue
    .line 918170
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918171
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setBooleanPropertyAtIndexNative(IZ)V

    .line 918172
    return-void
.end method

.method public final setNumberProperty(Ljava/lang/String;D)V
    .locals 0

    .prologue
    .line 918167
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918168
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/java2js/JSValue;->setNumberPropertyNative(Ljava/lang/String;D)V

    .line 918169
    return-void
.end method

.method public final setNumberPropertyAtIndex(ID)V
    .locals 0

    .prologue
    .line 918164
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918165
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/java2js/JSValue;->setNumberPropertyAtIndexNative(ID)V

    .line 918166
    return-void
.end method

.method public final setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 918148
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918149
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 918150
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setBooleanProperty(Ljava/lang/String;Z)V

    .line 918151
    :goto_0
    return-void

    .line 918152
    :cond_0
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 918153
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/java2js/JSValue;->setNumberProperty(Ljava/lang/String;D)V

    goto :goto_0

    .line 918154
    :cond_1
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 918155
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setStringProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 918156
    :cond_2
    instance-of v0, p2, Lcom/facebook/java2js/JSValue;

    if-eqz v0, :cond_3

    .line 918157
    check-cast p2, Lcom/facebook/java2js/JSValue;

    .line 918158
    sget-object v0, LX/5SQ;->$SwitchMap$com$facebook$java2js$JSValueType:[I

    iget-object v1, p2, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v1}, LX/5SU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 918159
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setPropertyNative(Ljava/lang/String;Lcom/facebook/java2js/JSValue;)V

    goto :goto_0

    .line 918160
    :pswitch_0
    iget-boolean v0, p2, Lcom/facebook/java2js/JSValue;->mBool:Z

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setBooleanProperty(Ljava/lang/String;Z)V

    goto :goto_0

    .line 918161
    :pswitch_1
    iget-wide v0, p2, Lcom/facebook/java2js/JSValue;->mNumber:D

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/java2js/JSValue;->setNumberProperty(Ljava/lang/String;D)V

    goto :goto_0

    .line 918162
    :pswitch_2
    iget-object v0, p2, Lcom/facebook/java2js/JSValue;->mString:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setStringProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 918163
    :cond_3
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->getJSContext()Lcom/facebook/java2js/JSContext;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setPropertyNative(Ljava/lang/String;Lcom/facebook/java2js/JSValue;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setPropertyAtIndex(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 918132
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918133
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 918134
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setBooleanPropertyAtIndex(IZ)V

    .line 918135
    :goto_0
    return-void

    .line 918136
    :cond_0
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 918137
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/java2js/JSValue;->setNumberPropertyAtIndex(ID)V

    goto :goto_0

    .line 918138
    :cond_1
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 918139
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setStringPropertyAtIndex(ILjava/lang/String;)V

    goto :goto_0

    .line 918140
    :cond_2
    instance-of v0, p2, Lcom/facebook/java2js/JSValue;

    if-eqz v0, :cond_3

    .line 918141
    check-cast p2, Lcom/facebook/java2js/JSValue;

    .line 918142
    sget-object v0, LX/5SQ;->$SwitchMap$com$facebook$java2js$JSValueType:[I

    iget-object v1, p2, Lcom/facebook/java2js/JSValue;->mJSValueType:LX/5SU;

    invoke-virtual {v1}, LX/5SU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 918143
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setPropertyAtIndexNative(ILcom/facebook/java2js/JSValue;)V

    goto :goto_0

    .line 918144
    :pswitch_0
    iget-boolean v0, p2, Lcom/facebook/java2js/JSValue;->mBool:Z

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setBooleanPropertyAtIndex(IZ)V

    goto :goto_0

    .line 918145
    :pswitch_1
    iget-wide v0, p2, Lcom/facebook/java2js/JSValue;->mNumber:D

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/java2js/JSValue;->setNumberPropertyAtIndex(ID)V

    goto :goto_0

    .line 918146
    :pswitch_2
    iget-object v0, p2, Lcom/facebook/java2js/JSValue;->mString:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setStringPropertyAtIndex(ILjava/lang/String;)V

    goto :goto_0

    .line 918147
    :cond_3
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->getJSContext()Lcom/facebook/java2js/JSContext;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/java2js/JSValue;->setPropertyAtIndexNative(ILcom/facebook/java2js/JSValue;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setStringProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 918129
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918130
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setStringPropertyNative(Ljava/lang/String;Ljava/lang/String;)V

    .line 918131
    return-void
.end method

.method public final setStringPropertyAtIndex(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 918126
    invoke-direct {p0}, Lcom/facebook/java2js/JSValue;->checkCanHaveProperties()V

    .line 918127
    invoke-direct {p0, p1, p2}, Lcom/facebook/java2js/JSValue;->setStringPropertyAtIndexNative(ILjava/lang/String;)V

    .line 918128
    return-void
.end method

.method public final to(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 918125
    invoke-direct {p0, p1}, Lcom/facebook/java2js/JSValue;->toInternal(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final native toJSON()Ljava/lang/String;
.end method
