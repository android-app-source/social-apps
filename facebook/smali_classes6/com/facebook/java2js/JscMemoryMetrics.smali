.class public Lcom/facebook/java2js/JscMemoryMetrics;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public mBlockBytes:J

.field public mJscCount:J

.field public mMallocBytes:J


# direct methods
.method public constructor <init>(JJJ)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 918334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 918335
    iput-wide p1, p0, Lcom/facebook/java2js/JscMemoryMetrics;->mJscCount:J

    .line 918336
    iput-wide p3, p0, Lcom/facebook/java2js/JscMemoryMetrics;->mMallocBytes:J

    .line 918337
    iput-wide p5, p0, Lcom/facebook/java2js/JscMemoryMetrics;->mBlockBytes:J

    .line 918338
    return-void
.end method
