.class public Lcom/facebook/fbui/badges/BadgeIconView;
.super Lcom/facebook/fbui/glyph/GlyphView;
.source ""


# instance fields
.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:Landroid/text/Layout;

.field private final g:LX/1nq;

.field private final h:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1101640
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/badges/BadgeIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1101641
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1101702
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/badges/BadgeIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1101703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1101686
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1101687
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    .line 1101688
    sget-object v0, LX/03r;->BadgeIconView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1101689
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1101690
    const/16 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    .line 1101691
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->d:I

    .line 1101692
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->e:I

    .line 1101693
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1101694
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    .line 1101695
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    .line 1101696
    iput-boolean v3, v0, LX/1nq;->e:Z

    .line 1101697
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    .line 1101698
    iput-boolean v3, v0, LX/1nq;->f:Z

    .line 1101699
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, LX/1nq;->a(I)LX/1nq;

    .line 1101700
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/badges/BadgeIconView;->setBadgeTextAppearance(I)V

    .line 1101701
    return-void
.end method

.method private static a(Landroid/text/Layout;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1101680
    if-nez p0, :cond_0

    .line 1101681
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 1101682
    :goto_1
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1101683
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1101684
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1101685
    goto :goto_0
.end method


# virtual methods
.method public getBadgeText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1101679
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1101669
    invoke-super {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1101670
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 1101671
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1101672
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    if-eqz v0, :cond_1

    .line 1101673
    invoke-virtual {p0}, Lcom/facebook/fbui/badges/BadgeIconView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->d:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 1101674
    invoke-virtual {p0}, Lcom/facebook/fbui/badges/BadgeIconView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->e:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    .line 1101675
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1101676
    iget-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    invoke-virtual {v2, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1101677
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1101678
    :cond_1
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 1101660
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/glyph/GlyphView;->onLayout(ZIIII)V

    .line 1101661
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 1101662
    :cond_0
    :goto_0
    return-void

    .line 1101663
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    invoke-static {v0}, Lcom/facebook/fbui/badges/BadgeIconView;->a(Landroid/text/Layout;)I

    move-result v0

    .line 1101664
    iget-object v1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    .line 1101665
    sub-int v2, p4, p2

    div-int/lit8 v2, v2, 0x2

    .line 1101666
    sub-int v3, p5, p3

    div-int/lit8 v3, v3, 0x2

    .line 1101667
    iget-object v4, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1101668
    iget-object v4, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/facebook/fbui/badges/BadgeIconView;->d:I

    add-int/2addr v5, v2

    iget v6, p0, Lcom/facebook/fbui/badges/BadgeIconView;->e:I

    sub-int v6, v3, v6

    sub-int v1, v6, v1

    iget-object v6, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v6

    iget-object v6, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v6

    iget v6, p0, Lcom/facebook/fbui/badges/BadgeIconView;->d:I

    add-int/2addr v2, v6

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/badges/BadgeIconView;->e:I

    sub-int v2, v3, v2

    invoke-virtual {v4, v5, v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1101655
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/glyph/GlyphView;->onMeasure(II)V

    .line 1101656
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1101657
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    .line 1101658
    :goto_0
    return-void

    .line 1101659
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->f:Landroid/text/Layout;

    goto :goto_0
.end method

.method public setBadgeBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1101651
    iput-object p1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    .line 1101652
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1101653
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1101654
    :cond_0
    return-void
.end method

.method public setBadgeText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1101645
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->b:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1101646
    :goto_0
    return-void

    .line 1101647
    :cond_0
    iput-object p1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->b:Ljava/lang/CharSequence;

    .line 1101648
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    iget-object v1, p0, Lcom/facebook/fbui/badges/BadgeIconView;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1101649
    invoke-virtual {p0}, Lcom/facebook/fbui/badges/BadgeIconView;->invalidate()V

    .line 1101650
    invoke-virtual {p0}, Lcom/facebook/fbui/badges/BadgeIconView;->requestLayout()V

    goto :goto_0
.end method

.method public setBadgeTextAppearance(I)V
    .locals 2

    .prologue
    .line 1101643
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->g:LX/1nq;

    invoke-virtual {p0}, Lcom/facebook/fbui/badges/BadgeIconView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/5OZ;->a(LX/1nq;Landroid/content/Context;I)V

    .line 1101644
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1101642
    iget-object v0, p0, Lcom/facebook/fbui/badges/BadgeIconView;->c:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
