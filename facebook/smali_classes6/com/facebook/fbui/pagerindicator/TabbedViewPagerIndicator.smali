.class public Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;
.super Landroid/widget/HorizontalScrollView;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field private final a:Landroid/database/DataSetObserver;

.field public b:Landroid/support/v4/view/ViewPager;

.field public c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

.field public d:LX/0gG;

.field public e:LX/6Ua;

.field private f:F

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field public k:Z

.field public l:LX/0hc;

.field public m:LX/6Uh;

.field public n:LX/6Ui;

.field public o:Landroid/text/method/TransformationMethod;

.field public p:LX/0wL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102629
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102630
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102550
    const v0, 0x7f01018d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102551
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1102552
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102553
    new-instance v0, LX/6Ug;

    invoke-direct {v0, p0}, LX/6Ug;-><init>(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;)V

    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a:Landroid/database/DataSetObserver;

    .line 1102554
    const-class v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-static {v0, p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1102555
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setHorizontalScrollBarEnabled(Z)V

    .line 1102556
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getTabsContainerResource()I

    move-result v0

    .line 1102557
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 1102558
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->addView(Landroid/view/View;)V

    .line 1102559
    sget-object v0, LX/03r;->TabbedViewPagerIndicator:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1102560
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1102561
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setUnderlineColor(I)V

    .line 1102562
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1102563
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setUnderlineHeight(I)V

    .line 1102564
    const/16 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1102565
    if-eqz v1, :cond_1

    .line 1102566
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1102567
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1102568
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1102569
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 1102570
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1102571
    if-lez v1, :cond_0

    .line 1102572
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1102573
    :cond_0
    :goto_0
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1102574
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setTabLayout(I)V

    .line 1102575
    const/16 v1, 0x4

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->j:Z

    .line 1102576
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    .line 1102577
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1102578
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 1102579
    iput-boolean v1, v2, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->h:Z

    .line 1102580
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1102581
    return-void

    .line 1102582
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1102583
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(IF)V
    .locals 3

    .prologue
    .line 1102584
    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h:I

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1102585
    :cond_0
    :goto_0
    return-void

    .line 1102586
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102587
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1102588
    invoke-direct {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getDividerWidth()I

    move-result v2

    .line 1102589
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 1102590
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->f(I)I

    move-result v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    add-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->scrollTo(II)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-static {v1}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->o:Landroid/text/method/TransformationMethod;

    invoke-static {v1}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v0

    check-cast v0, LX/0wL;

    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->p:LX/0wL;

    return-void
.end method

.method public static e(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V
    .locals 2

    .prologue
    .line 1102591
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->f(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->smoothScrollTo(II)V

    .line 1102592
    return-void
.end method

.method private f(I)I
    .locals 5

    .prologue
    .line 1102593
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1102594
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getDividerWidth()I

    move-result v0

    .line 1102595
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getWidth()I

    move-result v2

    .line 1102596
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    .line 1102597
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 1102598
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getPaddingLeft()I

    move-result v1

    add-int/2addr v1, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    return v0

    .line 1102599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 1102676
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102677
    if-nez v0, :cond_0

    .line 1102678
    const/4 v0, 0x0

    .line 1102679
    :goto_0
    return-object v0

    .line 1102680
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0800c5

    .line 1102681
    :goto_1
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-eqz v2, :cond_2

    .line 1102682
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    invoke-virtual {v0, p1}, LX/6UZ;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1102683
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    add-int/lit8 v4, p1, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    invoke-virtual {v4}, LX/0gG;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1102684
    :cond_1
    const v1, 0x7f0800c6

    goto :goto_1

    .line 1102685
    :cond_2
    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    invoke-virtual {v2, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1102686
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    .line 1102687
    :cond_3
    instance-of v2, v0, Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_4

    .line 1102688
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    .line 1102689
    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method private getDividerWidth()I
    .locals 1

    .prologue
    .line 1102600
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1102601
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v3

    .line 1102602
    if-ltz p1, :cond_0

    if-lt p1, v3, :cond_1

    .line 1102603
    :cond_0
    :goto_0
    return-void

    .line 1102604
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102605
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getScrollX()I

    move-result v4

    .line 1102606
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getWidth()I

    move-result v5

    .line 1102607
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    .line 1102608
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 1102609
    if-lez p1, :cond_4

    .line 1102610
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102611
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    move v2, v0

    .line 1102612
    :goto_1
    add-int/lit8 v0, v3, -0x1

    if-ge p1, v0, :cond_3

    .line 1102613
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102614
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1102615
    :goto_2
    sub-int v2, v6, v2

    .line 1102616
    add-int v3, v6, v7

    add-int/2addr v0, v3

    .line 1102617
    if-ge v2, v4, :cond_2

    .line 1102618
    invoke-virtual {p0, v2, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->scrollTo(II)V

    goto :goto_0

    .line 1102619
    :cond_2
    add-int v2, v4, v5

    if-le v0, v2, :cond_0

    .line 1102620
    sub-int/2addr v0, v5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->scrollTo(II)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method private setLastSelectTabIndex(I)V
    .locals 2

    .prologue
    .line 1102621
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    iget v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102622
    if-eqz v0, :cond_0

    .line 1102623
    iget v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g:I

    invoke-direct {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102624
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102625
    if-eqz v0, :cond_1

    .line 1102626
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102627
    :cond_1
    iput p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g:I

    .line 1102628
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 1102516
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setPageSelection(I)V

    .line 1102517
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 1102518
    iget-boolean v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->g:Z

    move v0, v1

    .line 1102519
    if-nez v0, :cond_4

    .line 1102520
    :cond_0
    new-instance v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$2;-><init>(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->post(Ljava/lang/Runnable;)Z

    .line 1102521
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setLastSelectTabIndex(I)V

    .line 1102522
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102523
    if-eqz v0, :cond_2

    .line 1102524
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->p:LX/0wL;

    invoke-virtual {v0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1102525
    invoke-static {v1, p0, v0}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1102526
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    if-eqz v0, :cond_3

    .line 1102527
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 1102528
    :cond_3
    return-void

    .line 1102529
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h:I

    if-nez v0, :cond_5

    .line 1102530
    invoke-static {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    .line 1102531
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->c()V

    goto :goto_0

    .line 1102532
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h:I

    if-nez v0, :cond_1

    :cond_6
    iget-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->i:Z

    if-nez v0, :cond_1

    .line 1102533
    invoke-static {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 1102631
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-nez v0, :cond_0

    .line 1102632
    :goto_0
    return-void

    .line 1102633
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v2

    .line 1102634
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 1102635
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102636
    instance-of v3, v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    if-eqz v3, :cond_1

    .line 1102637
    iget-object v3, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    invoke-virtual {v3, v1}, LX/6Ua;->a(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1102638
    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 1102639
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1102640
    :cond_2
    new-instance v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$3;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$3;-><init>(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 5

    .prologue
    .line 1102641
    int-to-float v0, p1

    add-float v3, v0, p2

    .line 1102642
    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 1102643
    add-int/lit8 v1, p1, 0x1

    .line 1102644
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    move v2, v1

    move v1, p1

    .line 1102645
    :goto_0
    const/4 v4, 0x0

    cmpl-float v4, p2, v4

    if-lez v4, :cond_0

    .line 1102646
    iget-boolean v4, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    if-nez v4, :cond_3

    .line 1102647
    invoke-static {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    .line 1102648
    :cond_0
    :goto_1
    iput v3, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->f:F

    .line 1102649
    iget-object v3, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v3, v2, v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->a(IIF)V

    .line 1102650
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    if-eqz v0, :cond_1

    .line 1102651
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 1102652
    :cond_1
    return-void

    .line 1102653
    :cond_2
    add-int/lit8 v0, p1, 0x1

    move v1, v0

    move v2, p1

    move v0, p2

    .line 1102654
    goto :goto_0

    .line 1102655
    :cond_3
    iget-boolean v4, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->i:Z

    if-eqz v4, :cond_0

    .line 1102656
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(IF)V

    goto :goto_1
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1102657
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    if-nez v0, :cond_0

    .line 1102658
    :goto_0
    return-void

    .line 1102659
    :cond_0
    const/4 v0, 0x0

    .line 1102660
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    instance-of v1, v1, LX/6Uf;

    if-eqz v1, :cond_1

    .line 1102661
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    check-cast v0, LX/6Uf;

    .line 1102662
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->removeAllViews()V

    .line 1102663
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v2

    .line 1102664
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 1102665
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c(I)Landroid/view/View;

    move-result-object v3

    .line 1102666
    invoke-direct {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->g(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102667
    if-eqz v0, :cond_2

    .line 1102668
    invoke-interface {v0}, LX/6Uf;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setId(I)V

    .line 1102669
    :cond_2
    new-instance v4, LX/6Uj;

    invoke-direct {v4, p0, v1}, LX/6Uj;-><init>(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1102670
    new-instance v4, LX/6Uk;

    invoke-direct {v4, p0, v1}, LX/6Uk;-><init>(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1102671
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1102672
    :cond_3
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->b()V

    .line 1102673
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    .line 1102674
    iget v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->a:I

    move v0, v1

    .line 1102675
    invoke-direct {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setLastSelectTabIndex(I)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1102534
    iput p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->h:I

    .line 1102535
    if-ne p1, v0, :cond_2

    .line 1102536
    iput-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->i:Z

    .line 1102537
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    if-eqz v0, :cond_1

    .line 1102538
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1102539
    :cond_1
    return-void

    .line 1102540
    :cond_2
    if-nez p1, :cond_0

    .line 1102541
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->i:Z

    goto :goto_0
.end method

.method public c(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1102542
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1102543
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->a(Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v1

    .line 1102544
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-eqz v0, :cond_0

    instance-of v0, v1, Lcom/facebook/fbui/widget/text/BadgeTextView;

    if-eqz v0, :cond_0

    .line 1102545
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    invoke-virtual {v0, p1}, LX/6Ua;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v0, v1

    .line 1102546
    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 1102547
    :cond_0
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1102548
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->o:Landroid/text/method/TransformationMethod;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1102549
    :cond_1
    return-object v1
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1102430
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1102431
    :cond_0
    const/4 v0, 0x0

    .line 1102432
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getBadgePagerAdapter()LX/6Ua;
    .locals 1

    .prologue
    .line 1102433
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    return-object v0
.end method

.method public getPagerAdapter()LX/0gG;
    .locals 1

    .prologue
    .line 1102434
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    return-object v0
.end method

.method public getTabsContainer()Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;
    .locals 1

    .prologue
    .line 1102435
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    return-object v0
.end method

.method public getTabsContainerResource()I
    .locals 1

    .prologue
    .line 1102436
    const v0, 0x7f03061b

    return v0
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x643b778b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1102437
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    .line 1102438
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-eqz v1, :cond_0

    .line 1102439
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/6UZ;->b(Landroid/database/DataSetObserver;)V

    .line 1102440
    iput-object v4, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    .line 1102441
    :cond_0
    iput-object v4, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    .line 1102442
    const/16 v1, 0x2d

    const v2, -0x5bb21687

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1102443
    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    .line 1102444
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    if-nez v0, :cond_1

    .line 1102445
    :cond_0
    :goto_0
    return-void

    .line 1102446
    :cond_1
    iget v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->f:F

    float-to-int v0, v0

    .line 1102447
    if-eqz p1, :cond_0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1102448
    invoke-static {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e(Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;I)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1102449
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillViewport(Z)V

    .line 1102450
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 1102451
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getMeasuredWidth()I

    move-result v3

    if-ge v0, v3, :cond_3

    iget-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->j:Z

    if-eqz v0, :cond_3

    .line 1102452
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillViewport(Z)V

    .line 1102453
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v4

    move v3, v1

    .line 1102454
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1102455
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1102456
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1102457
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1102458
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1102459
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1102460
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1102461
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    move v3, v1

    .line 1102462
    :goto_1
    if-ge v3, v4, :cond_4

    .line 1102463
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102464
    instance-of v5, v0, Lcom/facebook/resources/ui/FbTextView;

    if-eqz v5, :cond_1

    .line 1102465
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1102466
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v2

    .line 1102467
    :goto_2
    if-eqz v0, :cond_3

    .line 1102468
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setFillViewport(Z)V

    .line 1102469
    :goto_3
    if-ge v1, v4, :cond_2

    .line 1102470
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1102471
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1102472
    const/4 v3, -0x2

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1102473
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1102474
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1102475
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1102476
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1102477
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 1102478
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public setCenterSelectedTab(Z)V
    .locals 1

    .prologue
    .line 1102479
    iget-boolean v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    if-eq v0, p1, :cond_0

    .line 1102480
    iput-boolean p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->k:Z

    .line 1102481
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->requestLayout()V

    .line 1102482
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->invalidate()V

    .line 1102483
    :cond_0
    return-void
.end method

.method public setFillParentWidth(Z)V
    .locals 0

    .prologue
    .line 1102484
    iput-boolean p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->j:Z

    .line 1102485
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 0

    .prologue
    .line 1102486
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 1102487
    return-void
.end method

.method public setOnTabClickListener(LX/6Uh;)V
    .locals 0

    .prologue
    .line 1102488
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 1102489
    return-void
.end method

.method public setOnTabLongClickListener(LX/6Ui;)V
    .locals 0

    .prologue
    .line 1102490
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->n:LX/6Ui;

    .line 1102491
    return-void
.end method

.method public setTitleTransformationMethod(Landroid/text/method/TransformationMethod;)V
    .locals 0

    .prologue
    .line 1102492
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->o:Landroid/text/method/TransformationMethod;

    .line 1102493
    return-void
.end method

.method public setUnderlineColor(I)V
    .locals 1

    .prologue
    .line 1102494
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getUnderlineColor()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1102495
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setUnderlineColor(I)V

    .line 1102496
    :cond_0
    return-void
.end method

.method public setUnderlineHeight(I)V
    .locals 1

    .prologue
    .line 1102497
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setUnderlineHeight(I)V

    .line 1102498
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 3

    .prologue
    .line 1102499
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_1

    .line 1102500
    :cond_0
    :goto_0
    return-void

    .line 1102501
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 1102502
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1102503
    :cond_2
    iput-object p1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    .line 1102504
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1102505
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 1102506
    if-eqz v0, :cond_0

    .line 1102507
    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    .line 1102508
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-eqz v1, :cond_3

    .line 1102509
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    iget-object v2, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/6UZ;->b(Landroid/database/DataSetObserver;)V

    .line 1102510
    :cond_3
    instance-of v1, v0, LX/6Ub;

    if-eqz v1, :cond_4

    .line 1102511
    check-cast v0, LX/6Ub;

    invoke-interface {v0}, LX/6Ub;->a()LX/6Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    .line 1102512
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    if-eqz v0, :cond_4

    .line 1102513
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a:Landroid/database/DataSetObserver;

    .line 1102514
    iget-object v2, v0, LX/6UZ;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v2, v1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 1102515
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    goto :goto_0
.end method
