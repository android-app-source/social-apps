.class public Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;
.super Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102862
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102863
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102860
    const v0, 0x7f01018d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102861
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1102845
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102846
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1102864
    invoke-super {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a()V

    .line 1102865
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    move-object v0, v0

    .line 1102866
    if-nez v0, :cond_1

    .line 1102867
    :cond_0
    return-void

    .line 1102868
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v0, v0

    .line 1102869
    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildCount()I

    move-result v2

    .line 1102870
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1102871
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v0, v0

    .line 1102872
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102873
    instance-of v3, v0, Lcom/facebook/fbui/badges/BadgeIconView;

    if-eqz v3, :cond_2

    .line 1102874
    iget-object v3, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    move-object v3, v3

    .line 1102875
    invoke-virtual {v3, v1}, LX/6Ua;->a(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1102876
    check-cast v0, Lcom/facebook/fbui/badges/BadgeIconView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/badges/BadgeIconView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 1102877
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 1102847
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    move-object v0, v0

    .line 1102848
    invoke-virtual {v0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1102849
    check-cast v0, LX/K31;

    .line 1102850
    iget-object v2, v0, LX/K31;->a:[LX/K2y;

    aget-object v2, v2, p1

    iget v2, v2, LX/K2y;->tabIconResId:I

    move v2, v2

    .line 1102851
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v0, v0

    .line 1102852
    check-cast v0, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;->a(Ljava/lang/CharSequence;I)Landroid/view/View;

    move-result-object v1

    .line 1102853
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->e:LX/6Ua;

    move-object v2, v0

    .line 1102854
    if-eqz v2, :cond_0

    instance-of v0, v1, Lcom/facebook/fbui/badges/BadgeIconView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1102855
    check-cast v0, Lcom/facebook/fbui/badges/BadgeIconView;

    .line 1102856
    invoke-virtual {v2, p1}, LX/6UZ;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/badges/BadgeIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102857
    invoke-virtual {v2, p1}, LX/6Ua;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/badges/BadgeIconView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 1102858
    goto :goto_0

    .line 1102859
    :cond_0
    :goto_0
    return-object v1
.end method

.method public getTabsContainerResource()I
    .locals 1

    .prologue
    .line 1102844
    const v0, 0x7f030619

    return v0
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1102838
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 1102839
    if-eqz v0, :cond_0

    .line 1102840
    instance-of v0, v0, LX/K31;

    if-nez v0, :cond_0

    .line 1102841
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter should be an instance of IconPagerAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1102842
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1102843
    return-void
.end method
