.class public Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;
.super Lcom/facebook/fbui/widget/text/BadgeTextView;
.source ""

# interfaces
.implements LX/6Ud;


# instance fields
.field private a:F

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102405
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102406
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102407
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102408
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1102409
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102410
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102411
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1102412
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1102413
    sget-object v1, LX/03r;->ColoredTabProgressListenerBadgeTextView:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1102414
    const/16 v2, 0x0

    const v3, 0x7f0a00fc

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1102415
    iput v2, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    .line 1102416
    const/16 v2, 0x1

    const v3, 0x7f0a00d2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1102417
    iput v0, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    .line 1102418
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1102419
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    .line 1102420
    invoke-static {p1}, LX/6Uc;->a(F)F

    move-result v0

    .line 1102421
    iget v1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->a:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_0

    .line 1102422
    :goto_0
    return-void

    .line 1102423
    :cond_0
    iget v1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    iget v2, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    invoke-static {p1, v1, v2}, LX/6Uc;->a(FII)I

    move-result v1

    .line 1102424
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->setTextColor(I)V

    .line 1102425
    iput v0, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->a:F

    goto :goto_0
.end method

.method public setSelectedColor(I)V
    .locals 0

    .prologue
    .line 1102426
    iput p1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    .line 1102427
    return-void
.end method

.method public setUnselectedColor(I)V
    .locals 0

    .prologue
    .line 1102428
    iput p1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    .line 1102429
    return-void
.end method
