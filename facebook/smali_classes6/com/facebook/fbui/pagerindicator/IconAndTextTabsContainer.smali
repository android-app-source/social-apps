.class public Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;
.super Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102806
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102808
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1102810
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator$IconTabsContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102811
    sget-object v0, LX/03r;->IconAndTextTabsContainer:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1102812
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->a:I

    .line 1102813
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1102814
    const v0, 0x7f030616

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->setTabLayout(I)V

    .line 1102815
    return-void
.end method


# virtual methods
.method public final a(LX/0gG;I)Landroid/view/View;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1102816
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;->d()Landroid/view/View;

    move-result-object v2

    .line 1102817
    instance-of v0, v2, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    instance-of v0, p1, LX/6Ue;

    if-nez v0, :cond_1

    .line 1102818
    :cond_0
    new-instance v0, Landroid/view/InflateException;

    const-string v1, "Tab layout should be a subclass of TextView and pager adapter should be a subclass of IconAndTextPagerAdapter"

    invoke-direct {v0, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, p1

    .line 1102819
    check-cast v0, LX/6Ue;

    move-object v1, v2

    .line 1102820
    check-cast v1, Landroid/widget/TextView;

    .line 1102821
    invoke-virtual {p1, p2}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1102822
    invoke-interface {v0, p2}, LX/6Ue;->A_(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1102823
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v3, ""

    .line 1102824
    :cond_2
    invoke-interface {v0, v1, p2}, LX/6Ue;->a(Landroid/widget/TextView;I)V

    .line 1102825
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1102826
    if-eqz v4, :cond_3

    .line 1102827
    invoke-virtual {p0}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1102828
    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1102829
    iget v6, p0, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->a:I

    aput-object v4, v5, v6

    .line 1102830
    aget-object v4, v5, v7

    aget-object v6, v5, v8

    aget-object v7, v5, v9

    aget-object v5, v5, v10

    invoke-virtual {v1, v4, v6, v7, v5}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1102831
    :cond_3
    :goto_0
    invoke-interface {v0, p2}, LX/6Ue;->k_(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1102832
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v3, v0

    :cond_4
    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1102833
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->addView(Landroid/view/View;)V

    .line 1102834
    return-object v2

    .line 1102835
    :cond_5
    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1102836
    iget v6, p0, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->a:I

    aput-object v4, v5, v6

    .line 1102837
    aget-object v4, v5, v7

    aget-object v6, v5, v8

    aget-object v7, v5, v9

    aget-object v5, v5, v10

    invoke-virtual {v1, v4, v6, v7, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
