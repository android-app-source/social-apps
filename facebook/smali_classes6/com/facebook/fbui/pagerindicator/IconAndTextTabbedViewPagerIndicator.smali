.class public Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;
.super Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102705
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102706
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102703
    const v0, 0x7f01018d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102704
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1102701
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102702
    return-void
.end method


# virtual methods
.method public c(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1102690
    iget-object v0, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->c:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator$TabsContainer;

    move-object v0, v0

    .line 1102691
    check-cast v0, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;

    .line 1102692
    iget-object v1, p0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->d:LX/0gG;

    move-object v1, v1

    .line 1102693
    invoke-virtual {v0, v1, p1}, Lcom/facebook/fbui/pagerindicator/IconAndTextTabsContainer;->a(LX/0gG;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getTabsContainerResource()I
    .locals 1

    .prologue
    .line 1102700
    const v0, 0x7f030617

    return v0
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1102694
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 1102695
    if-eqz v0, :cond_0

    .line 1102696
    instance-of v0, v0, LX/6Ue;

    if-nez v0, :cond_0

    .line 1102697
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter should be an instance of IconAndTextPagerAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1102698
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1102699
    return-void
.end method
