.class public Lcom/facebook/fbui/widget/text/BadgeTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Landroid/text/TextPaint;

.field private b:Landroid/content/res/ColorStateList;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/text/Layout;

.field public h:Ljava/lang/CharSequence;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/Rect;

.field private k:LX/5Oe;

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 909388
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 909389
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 909356
    const v0, 0x7f010218

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 909357
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 909359
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 909360
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    .line 909361
    sget-object v0, LX/03r;->BadgeTextView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 909362
    const/16 v0, 0x1

    const v2, 0x7f0e0289

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 909363
    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 909364
    const/16 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 909365
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeBackground(Landroid/graphics/drawable/Drawable;)V

    .line 909366
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 909367
    if-lez v0, :cond_0

    .line 909368
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 909369
    :goto_0
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 909370
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgePadding(I)V

    .line 909371
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 909372
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeYOffset(I)V

    .line 909373
    const/16 v0, 0x5

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 909374
    if-nez v0, :cond_1

    sget-object v0, LX/5Oe;->WITH_TEXT:LX/5Oe;

    :goto_1
    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    .line 909375
    const/16 v0, 0x6

    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->l:I

    .line 909376
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 909377
    return-void

    .line 909378
    :cond_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 909379
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 909380
    :cond_1
    sget-object v0, LX/5Oe;->AWAY_FROM_TEXT:LX/5Oe;

    goto :goto_1
.end method

.method private static a()Z
    .locals 1

    .prologue
    .line 909381
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBadgeHeight()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 909382
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    if-nez v1, :cond_1

    .line 909383
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private getBadgeWidth()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 909384
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    if-nez v1, :cond_1

    .line 909385
    :cond_0
    :goto_0
    return v0

    .line 909386
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    .line 909387
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    invoke-virtual {v2, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    float-to-int v0, v0

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v6, 0x0

    .line 909210
    sget-object v0, LX/03r;->BadgeTextViewBadgeAppearance:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 909211
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    .line 909212
    const/16 v1, 0x0

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 909213
    const/16 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 909214
    const/16 v3, 0x4

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    .line 909215
    const/16 v4, 0x5

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 909216
    const/16 v5, 0x6

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    .line 909217
    const/16 v6, 0x1

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 909218
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 909219
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 909220
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    iget-object v7, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {v7}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 909221
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 909222
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, v5, v3, v4, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 909223
    if-eq v6, v8, :cond_1

    .line 909224
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    invoke-static {v6}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 909225
    :cond_1
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 3

    .prologue
    .line 909395
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->drawableStateChanged()V

    .line 909396
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getDrawableState()[I

    move-result-object v0

    .line 909397
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 909398
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->b:Landroid/content/res/ColorStateList;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 909399
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 909400
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 909401
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 909402
    :cond_1
    return-void
.end method

.method public getBadgeBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 909390
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getBadgeGravity()I
    .locals 1

    .prologue
    .line 909394
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->l:I

    return v0
.end method

.method public getBadgePadding()I
    .locals 1

    .prologue
    .line 909393
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    return v0
.end method

.method public getBadgePlacement()LX/5Oe;
    .locals 1

    .prologue
    .line 909392
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    return-object v0
.end method

.method public getBadgeText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 909391
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getBadgeYOffset()I
    .locals 1

    .prologue
    .line 909358
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->d:I

    return v0
.end method

.method public getCompoundPaddingLeft()I
    .locals 3

    .prologue
    .line 909351
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v0

    .line 909352
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeWidth()I

    move-result v1

    .line 909353
    invoke-static {}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a()Z

    move-result v2

    if-nez v2, :cond_0

    if-lez v1, :cond_0

    .line 909354
    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    add-int/2addr v0, v1

    .line 909355
    :cond_0
    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 3

    .prologue
    .line 909346
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v0

    .line 909347
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeWidth()I

    move-result v1

    .line 909348
    invoke-static {}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    if-lez v1, :cond_0

    .line 909349
    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    add-int/2addr v0, v1

    .line 909350
    :cond_0
    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x3

    const/4 v5, 0x0

    .line 909299
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 909300
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    if-nez v0, :cond_1

    .line 909301
    :cond_0
    :goto_0
    return-void

    .line 909302
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    float-to-int v0, v0

    .line 909303
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeWidth()I

    move-result v1

    .line 909304
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeHeight()I

    move-result v2

    .line 909305
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getWidth()I

    move-result v3

    .line 909306
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getHeight()I

    move-result v4

    .line 909307
    iput v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    .line 909308
    sub-int v5, v4, v2

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    .line 909309
    iget v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->l:I

    and-int/lit8 v5, v5, 0x70

    .line 909310
    const/16 v6, 0x30

    if-ne v5, v6, :cond_4

    .line 909311
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getPaddingTop()I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    .line 909312
    :cond_2
    :goto_1
    iget v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    iget v4, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->d:I

    add-int/2addr v2, v4

    iput v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    .line 909313
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v2

    .line 909314
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v4

    .line 909315
    add-int v5, v2, v4

    .line 909316
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getScrollX()I

    move-result v6

    .line 909317
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getScrollY()I

    move-result v7

    .line 909318
    int-to-float v6, v6

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 909319
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getGravity()I

    move-result v6

    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v7

    invoke-static {v6, v7}, LX/1uf;->a(II)I

    move-result v6

    .line 909320
    and-int/lit8 v6, v6, 0x7

    .line 909321
    invoke-static {}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 909322
    iget-object v7, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    sget-object v8, LX/5Oe;->WITH_TEXT:LX/5Oe;

    if-ne v7, v8, :cond_7

    .line 909323
    if-ne v6, v9, :cond_5

    .line 909324
    add-int/2addr v0, v2

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    .line 909325
    :cond_3
    :goto_2
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 909326
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 909327
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 909328
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 909329
    :cond_4
    const/16 v6, 0x50

    if-ne v5, v6, :cond_2

    .line 909330
    sub-int v2, v4, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    goto :goto_1

    .line 909331
    :cond_5
    if-ne v6, v10, :cond_6

    .line 909332
    sub-int v0, v3, v4

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909333
    :cond_6
    sub-int/2addr v3, v5

    sub-int/2addr v3, v0

    sub-int v1, v3, v1

    iget v3, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    sub-int/2addr v1, v3

    .line 909334
    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    add-int/2addr v0, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909335
    :cond_7
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    sget-object v2, LX/5Oe;->AWAY_FROM_TEXT:LX/5Oe;

    if-ne v0, v2, :cond_3

    .line 909336
    sub-int v0, v3, v4

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909337
    :cond_8
    iget-object v7, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    sget-object v8, LX/5Oe;->WITH_TEXT:LX/5Oe;

    if-ne v7, v8, :cond_b

    .line 909338
    if-ne v6, v9, :cond_9

    .line 909339
    iput v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909340
    :cond_9
    if-ne v6, v10, :cond_a

    .line 909341
    sub-int v0, v3, v0

    sub-int/2addr v0, v4

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909342
    :cond_a
    sub-int/2addr v3, v5

    sub-int v0, v3, v0

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    sub-int/2addr v0, v1

    .line 909343
    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto :goto_2

    .line 909344
    :cond_b
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    sget-object v1, LX/5Oe;->AWAY_FROM_TEXT:LX/5Oe;

    if-ne v0, v1, :cond_3

    .line 909345
    iput v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    goto/16 :goto_2
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v9, 0x0

    const/16 v0, 0x2c

    const v1, 0x1f0bdbf8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 909285
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909286
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 909287
    const/16 v0, 0x2d

    const v1, -0x5826eb7a

    invoke-static {v2, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 909288
    :goto_0
    return-void

    .line 909289
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 909290
    if-eqz v3, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 909291
    :cond_1
    const v3, 0x7fffffff

    .line 909292
    :cond_2
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->a:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    .line 909293
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeWidth()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeHeight()I

    move-result v2

    invoke-virtual {v0, v9, v9, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 909294
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 909295
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 909296
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 909297
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setMeasuredDimension(II)V

    .line 909298
    :cond_3
    const v0, -0x26a0b61c

    invoke-static {v0, v8}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const v0, 0x7a8da0f2

    invoke-static {v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 909261
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 909262
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x2194c60b

    invoke-static {v3, v3, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 909263
    :goto_0
    return v0

    .line 909264
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 909265
    instance-of v0, v0, Landroid/text/Spanned;

    if-nez v0, :cond_1

    .line 909266
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x6a5504b2

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 909267
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 909268
    if-eq v3, v1, :cond_2

    if-eqz v3, :cond_2

    .line 909269
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x6029a6f9

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 909270
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getScrollX()I

    move-result v4

    add-int/2addr v0, v4

    .line 909271
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getScrollY()I

    move-result v5

    add-int/2addr v4, v5

    .line 909272
    iget v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->e:I

    sub-int/2addr v0, v5

    .line 909273
    iget v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->f:I

    sub-int v5, v4, v5

    .line 909274
    if-ltz v0, :cond_3

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeWidth()I

    move-result v6

    if-gt v0, v6, :cond_3

    if-ltz v5, :cond_3

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getBadgeHeight()I

    move-result v6

    if-le v5, v6, :cond_4

    .line 909275
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, -0x5aebe151

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 909276
    :cond_4
    iget-object v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    invoke-virtual {v5, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v4

    .line 909277
    iget-object v5, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    int-to-float v0, v0

    invoke-virtual {v5, v4, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v4

    .line 909278
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 909279
    check-cast v0, Landroid/text/Spanned;

    const-class v5, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v4, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 909280
    array-length v4, v0

    if-eqz v4, :cond_6

    .line 909281
    if-ne v3, v1, :cond_5

    .line 909282
    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 909283
    :cond_5
    const v0, 0x3aa6ad48

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    .line 909284
    :cond_6
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x4941579b

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method

.method public setBadgeBackground(I)V
    .locals 1

    .prologue
    .line 909258
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeBackground(Landroid/graphics/drawable/Drawable;)V

    .line 909259
    return-void

    .line 909260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBadgeBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 909252
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    .line 909253
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 909254
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    .line 909255
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    .line 909256
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->i:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 909257
    return-void
.end method

.method public setBadgeGravity(I)V
    .locals 1

    .prologue
    .line 909247
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->l:I

    if-eq v0, p1, :cond_0

    .line 909248
    iput p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->l:I

    .line 909249
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->invalidate()V

    .line 909250
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->requestLayout()V

    .line 909251
    :cond_0
    return-void
.end method

.method public setBadgePadding(I)V
    .locals 1

    .prologue
    .line 909242
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    if-eq v0, p1, :cond_0

    .line 909243
    iput p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->c:I

    .line 909244
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->requestLayout()V

    .line 909245
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->invalidate()V

    .line 909246
    :cond_0
    return-void
.end method

.method public setBadgePlacement(LX/5Oe;)V
    .locals 1

    .prologue
    .line 909237
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    if-eq v0, p1, :cond_0

    .line 909238
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->k:LX/5Oe;

    .line 909239
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->invalidate()V

    .line 909240
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->requestLayout()V

    .line 909241
    :cond_0
    return-void
.end method

.method public setBadgeText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 909231
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 909232
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    .line 909233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->g:Landroid/text/Layout;

    .line 909234
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->invalidate()V

    .line 909235
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->requestLayout()V

    .line 909236
    :cond_0
    return-void
.end method

.method public setBadgeYOffset(I)V
    .locals 1

    .prologue
    .line 909226
    iget v0, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->d:I

    if-eq v0, p1, :cond_0

    .line 909227
    iput p1, p0, Lcom/facebook/fbui/widget/text/BadgeTextView;->d:I

    .line 909228
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->requestLayout()V

    .line 909229
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->invalidate()V

    .line 909230
    :cond_0
    return-void
.end method
