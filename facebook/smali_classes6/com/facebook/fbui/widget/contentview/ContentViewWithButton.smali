.class public Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# instance fields
.field private final j:Landroid/graphics/Rect;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1103920
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1103921
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1103918
    const v0, 0x7f01020d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1103919
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, -0x2

    const/4 v4, 0x0

    .line 1103881
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1103882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->j:Landroid/graphics/Rect;

    .line 1103883
    sget-object v0, LX/03r;->ContentViewWithButton:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1103884
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-nez v1, :cond_0

    .line 1103885
    new-instance v1, Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;)V

    .line 1103886
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablePadding(I)V

    .line 1103887
    iput-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->a:Landroid/view/View;

    .line 1103888
    new-instance v1, LX/6VC;

    invoke-direct {v1, v3, v3}, LX/6VC;-><init>(II)V

    .line 1103889
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/6VC;->b:Z

    .line 1103890
    const/16 v2, 0x9

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, v1, LX/6VC;->d:I

    .line 1103891
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1103892
    :cond_0
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1103893
    if-eqz v1, :cond_1

    .line 1103894
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1103895
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1103896
    if-lez v1, :cond_2

    .line 1103897
    invoke-static {}, LX/6VG;->values()[LX/6VG;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTheme(LX/6VG;)V

    .line 1103898
    :cond_2
    const/16 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1103899
    if-eqz v1, :cond_3

    .line 1103900
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1103901
    :cond_3
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1103902
    if-lez v1, :cond_4

    .line 1103903
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonText(I)V

    .line 1103904
    :goto_0
    const/16 v1, 0x7

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1103905
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTextAppearance(I)V

    .line 1103906
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1103907
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1103908
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1103909
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1103910
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1103911
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setDividerPadding(I)V

    .line 1103912
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1103913
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setDividerThickness(I)V

    .line 1103914
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1103915
    return-void

    .line 1103916
    :cond_4
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1103917
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1103873
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_1

    .line 1103874
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1103875
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1103876
    :goto_0
    return-void

    .line 1103877
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1103878
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1103879
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1103880
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 1103870
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1103871
    :cond_0
    const/4 v0, 0x0

    .line 1103872
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getActionButtonDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1103866
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103867
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1103868
    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    .line 1103869
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionButtonPadding()I
    .locals 1

    .prologue
    .line 1103865
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getAuxViewPadding()I

    move-result v0

    return v0
.end method

.method public getActionButtonText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1103862
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103863
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1103864
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAuxViewPadding()I
    .locals 2

    .prologue
    .line 1103859
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getAuxViewPadding()I

    move-result v0

    .line 1103860
    invoke-direct {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1103861
    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1103858
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerPadding()I
    .locals 1

    .prologue
    .line 1103857
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->m:I

    return v0
.end method

.method public getDividerThickness()I
    .locals 1

    .prologue
    .line 1103856
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1103847
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1103848
    invoke-direct {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1103849
    :goto_0
    return-void

    .line 1103850
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6VC;

    .line 1103851
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1103852
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v1, v0

    iget v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    sub-int/2addr v0, v1

    .line 1103853
    :goto_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->j:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceTop()I

    move-result v2

    iget v3, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->m:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceBottom()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->m:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1103854
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->j:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1103855
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1103922
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1103923
    return-void
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1103791
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1103792
    return-void
.end method

.method public setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1103795
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103796
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    .line 1103797
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1103798
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1103799
    return-void
.end method

.method public setActionButtonPadding(I)V
    .locals 0

    .prologue
    .line 1103793
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1103794
    return-void
.end method

.method public setActionButtonResource(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1103800
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103801
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void

    .line 1103802
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActionButtonText(I)V
    .locals 2

    .prologue
    .line 1103803
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103804
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void

    .line 1103805
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActionButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1103806
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103807
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 1103808
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActionButtonTextAppearance(I)V
    .locals 2

    .prologue
    .line 1103809
    if-lez p1, :cond_0

    .line 1103810
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1103811
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1103812
    :cond_0
    return-void

    .line 1103813
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action button is not an instanceof TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setActionButtonTheme(LX/6VG;)V
    .locals 2

    .prologue
    .line 1103814
    sget-object v0, LX/6VG;->GRAY:LX/6VG;

    if-ne p1, v0, :cond_0

    .line 1103815
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02079d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1103816
    const v0, 0x7f0e014f

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTextAppearance(I)V

    .line 1103817
    :goto_0
    return-void

    .line 1103818
    :cond_0
    sget-object v0, LX/6VG;->BLUE:LX/6VG;

    if-ne p1, v0, :cond_1

    .line 1103819
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02079a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1103820
    const v0, 0x7f0e0153

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTextAppearance(I)V

    goto :goto_0

    .line 1103821
    :cond_1
    sget-object v0, LX/6VG;->SPECIAL:LX/6VG;

    if-ne p1, v0, :cond_2

    .line 1103822
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0207a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1103823
    const v0, 0x7f0e0157

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTextAppearance(I)V

    goto :goto_0

    .line 1103824
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1103825
    const v0, 0x7f0e013e

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTextAppearance(I)V

    goto :goto_0
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1103826
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_1

    .line 1103827
    iput-object p1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    .line 1103828
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 1103829
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    .line 1103830
    :goto_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->k:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setWillNotDraw(Z)V

    .line 1103831
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->requestLayout()V

    .line 1103832
    :cond_1
    return-void

    .line 1103833
    :cond_2
    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    goto :goto_0
.end method

.method public setDividerPadding(I)V
    .locals 1

    .prologue
    .line 1103834
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->m:I

    if-eq v0, p1, :cond_0

    .line 1103835
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->m:I

    .line 1103836
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->requestLayout()V

    .line 1103837
    :cond_0
    return-void
.end method

.method public setDividerThickness(I)V
    .locals 1

    .prologue
    .line 1103838
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    if-eq v0, p1, :cond_0

    .line 1103839
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->l:I

    .line 1103840
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->requestLayout()V

    .line 1103841
    :cond_0
    return-void
.end method

.method public setEnableActionButton(Z)V
    .locals 1

    .prologue
    .line 1103842
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1103843
    return-void
.end method

.method public setShowActionButton(Z)V
    .locals 2

    .prologue
    .line 1103844
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1103845
    return-void

    .line 1103846
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
