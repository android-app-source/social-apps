.class public Lcom/facebook/fbui/widget/contentview/CheckedContentView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final j:[I

.field private static final k:[I


# instance fields
.field private l:Z

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:LX/6V9;

.field public o:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1103518
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->j:[I

    .line 1103519
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009f

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->k:[I

    return-void

    .line 1103520
    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1103623
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1103624
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1103621
    const v0, 0x7f010215

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1103622
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1103609
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1103610
    iput-boolean v3, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    .line 1103611
    sget-object v0, LX/03r;->CheckedContentView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1103612
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1103613
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(I)V

    .line 1103614
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1103615
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 1103616
    const/16 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1103617
    invoke-static {}, LX/6V9;->values()[LX/6V9;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkPosition(LX/6V9;)V

    .line 1103618
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkPadding(I)V

    .line 1103619
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1103620
    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1103605
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->drawableStateChanged()V

    .line 1103606
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1103607
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1103608
    :cond_0
    return-void
.end method

.method public getCheckMarkDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1103604
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getCheckMarkPadding()I
    .locals 1

    .prologue
    .line 1103603
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->o:I

    return v0
.end method

.method public getCheckMarkPosition()LX/6V9;
    .locals 1

    .prologue
    .line 1103602
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    return-object v0
.end method

.method public getSpaceLeft()I
    .locals 4

    .prologue
    .line 1103593
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSpaceLeft()I

    move-result v0

    .line 1103594
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 1103595
    :cond_0
    :goto_0
    return v0

    .line 1103596
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    .line 1103597
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v3, LX/6V9;->START:LX/6V9;

    if-eq v2, v3, :cond_3

    :cond_2
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v2, LX/6V9;->END:LX/6V9;

    if-ne v1, v2, :cond_0

    .line 1103598
    :cond_3
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1103599
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1103600
    iget v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->o:I

    move v2, v2

    .line 1103601
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getSpaceRight()I
    .locals 4

    .prologue
    .line 1103584
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSpaceRight()I

    move-result v0

    .line 1103585
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 1103586
    :cond_0
    :goto_0
    return v0

    .line 1103587
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    .line 1103588
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v3, LX/6V9;->END:LX/6V9;

    if-eq v2, v3, :cond_3

    :cond_2
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v2, LX/6V9;->START:LX/6V9;

    if-ne v1, v2, :cond_0

    .line 1103589
    :cond_3
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1103590
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1103591
    iget v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->o:I

    move v2, v2

    .line 1103592
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1103583
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    return v0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 1103579
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->jumpDrawablesToCurrentState()V

    .line 1103580
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1103581
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1103582
    :cond_0
    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 1103625
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 1103626
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->j:[I

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->mergeDrawableStates([I[I)[I

    .line 1103627
    return-object v1

    .line 1103628
    :cond_0
    sget-object v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->k:[I

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1103567
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1103568
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 1103569
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1103570
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v0

    .line 1103571
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v3, LX/6V9;->START:LX/6V9;

    if-eq v2, v3, :cond_1

    :cond_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    sget-object v2, LX/6V9;->END:LX/6V9;

    if-ne v0, v2, :cond_3

    .line 1103572
    :cond_1
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSpaceLeft()I

    move-result v0

    .line 1103573
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    .line 1103574
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1103575
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1103576
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1103577
    :cond_2
    return-void

    .line 1103578
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSpaceRight()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1103564
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1103565
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 1103566
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1103560
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1103561
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 1103562
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 1103563
    return-void
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 1103554
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1103555
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    if-eqz v0, :cond_1

    const v0, 0x7f081346

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1103556
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1103557
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1103558
    return-void

    .line 1103559
    :cond_1
    const v0, 0x7f081347

    goto :goto_0
.end method

.method public setCheckMarkDrawable(I)V
    .locals 1

    .prologue
    .line 1103551
    if-lez p1, :cond_0

    .line 1103552
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1103553
    :cond_0
    return-void
.end method

.method public setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1103543
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_1

    .line 1103544
    iput-object p1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    .line 1103545
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 1103546
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1103547
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1103548
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->invalidate()V

    .line 1103549
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setWillNotDraw(Z)V

    .line 1103550
    return-void
.end method

.method public setCheckMarkPadding(I)V
    .locals 1

    .prologue
    .line 1103539
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->o:I

    if-eq v0, p1, :cond_0

    .line 1103540
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->o:I

    .line 1103541
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->requestLayout()V

    .line 1103542
    :cond_0
    return-void
.end method

.method public setCheckMarkPosition(LX/6V9;)V
    .locals 1

    .prologue
    .line 1103534
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    if-eq v0, p1, :cond_0

    .line 1103535
    iput-object p1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->n:LX/6V9;

    .line 1103536
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->invalidate()V

    .line 1103537
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->requestLayout()V

    .line 1103538
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1103529
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    if-eq v0, p1, :cond_0

    .line 1103530
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    .line 1103531
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->refreshDrawableState()V

    .line 1103532
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->invalidate()V

    .line 1103533
    :cond_0
    return-void
.end method

.method public setPressed(Z)V
    .locals 0

    .prologue
    .line 1103525
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPressed(Z)V

    .line 1103526
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->refreshDrawableState()V

    .line 1103527
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->invalidate()V

    .line 1103528
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1103522
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 1103523
    return-void

    .line 1103524
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1103521
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->m:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
