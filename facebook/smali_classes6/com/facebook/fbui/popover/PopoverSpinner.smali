.class public Lcom/facebook/fbui/popover/PopoverSpinner;
.super Landroid/widget/AdapterView;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/view/View;

.field public d:Landroid/widget/ListAdapter;

.field public e:I

.field private f:LX/5ON;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 908807
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 908808
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 908717
    const v0, 0x7f01024d

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908718
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 908795
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908796
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setFocusable(Z)V

    .line 908797
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setClickable(Z)V

    .line 908798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    .line 908799
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->g:Z

    .line 908800
    new-instance v0, LX/5ON;

    invoke-direct {v0, p1}, LX/5ON;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    .line 908801
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    invoke-virtual {v0, v1}, LX/0ht;->e(Z)V

    .line 908802
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    new-instance v1, LX/5OP;

    invoke-direct {v1, p0}, LX/5OP;-><init>(Lcom/facebook/fbui/popover/PopoverSpinner;)V

    .line 908803
    iput-object v1, v0, LX/5ON;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 908804
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    new-instance v1, LX/5OQ;

    invoke-direct {v1, p0}, LX/5OQ;-><init>(Lcom/facebook/fbui/popover/PopoverSpinner;)V

    .line 908805
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 908806
    return-void
.end method


# virtual methods
.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 908794
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 908792
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    move-object v0, v0

    .line 908793
    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 908791
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 908790
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelection()I

    move-result v0

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 908786
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelection()I

    move-result v1

    .line 908787
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 908788
    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-interface {v2, v1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 908789
    :cond_0
    return-object v0
.end method

.method public getSelection()I
    .locals 1

    .prologue
    .line 908783
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 908784
    :cond_0
    const/4 v0, -0x1

    .line 908785
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->e:I

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x46ea5278

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 908809
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 908810
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    .line 908811
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 908812
    if-eqz v1, :cond_0

    .line 908813
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 908814
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x745dfb1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 908759
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 908760
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 908761
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    .line 908762
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->removeAllViewsInLayout()V

    .line 908763
    :goto_0
    return-void

    .line 908764
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelection()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    .line 908765
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 908766
    if-nez v0, :cond_2

    .line 908767
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 908768
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->detachAllViewsFromParent()V

    .line 908769
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->hasFocus()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 908770
    iget v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->a:I

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 908771
    iget v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->b:I

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 908772
    iget-object v3, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    .line 908773
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 908774
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 908775
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 908776
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingTop()I

    move-result v2

    .line 908777
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingBottom()I

    move-result v3

    .line 908778
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v4, v1

    sub-int/2addr v4, v2

    sub-int v3, v4, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 908779
    add-int/2addr v1, v2

    .line 908780
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingLeft()I

    move-result v3

    .line 908781
    add-int/2addr v0, v3

    .line 908782
    iget-object v4, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->c:Landroid/view/View;

    invoke-virtual {v4, v3, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 908742
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelection()I

    move-result v1

    .line 908743
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 908744
    iget-object v2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 908745
    if-eqz v2, :cond_1

    .line 908746
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 908747
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 908748
    :cond_0
    invoke-virtual {p0, v2, p1, p2}, Lcom/facebook/fbui/popover/PopoverSpinner;->measureChild(Landroid/view/View;II)V

    .line 908749
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 908750
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 908751
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 908752
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 908753
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 908754
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 908755
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setMeasuredDimension(II)V

    .line 908756
    iput p1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->a:I

    .line 908757
    iput p2, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->b:I

    .line 908758
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final performClick()Z
    .locals 2

    .prologue
    .line 908737
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    .line 908738
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 908739
    if-nez v0, :cond_0

    .line 908740
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    invoke-virtual {v0, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 908741
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 908736
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 908730
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->d:Landroid/widget/ListAdapter;

    .line 908731
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    .line 908732
    iput-object p1, v0, LX/5ON;->m:Landroid/widget/ListAdapter;

    .line 908733
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->requestLayout()V

    .line 908734
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->invalidate()V

    .line 908735
    return-void
.end method

.method public setMaxRows(F)V
    .locals 1

    .prologue
    .line 908727
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    .line 908728
    iput p1, v0, LX/5ON;->o:F

    .line 908729
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 2

    .prologue
    .line 908726
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setOnItemClickListener cannot be used with a spinner."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setPopoverDimAmount(F)V
    .locals 1

    .prologue
    .line 908724
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->f:LX/5ON;

    invoke-virtual {v0, p1}, LX/0ht;->b(F)V

    .line 908725
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 908719
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->e:I

    if-eq v0, p1, :cond_0

    .line 908720
    iput p1, p0, Lcom/facebook/fbui/popover/PopoverSpinner;->e:I

    .line 908721
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->requestLayout()V

    .line 908722
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->invalidate()V

    .line 908723
    :cond_0
    return-void
.end method
