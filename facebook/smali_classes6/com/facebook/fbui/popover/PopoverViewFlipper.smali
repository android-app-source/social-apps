.class public Lcom/facebook/fbui/popover/PopoverViewFlipper;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final a:LX/0wT;

.field private static final b:LX/0wT;


# instance fields
.field public c:LX/0wc;

.field private d:LX/0wW;

.field private e:LX/5OT;

.field private f:LX/5OU;

.field public g:LX/0wd;

.field public h:LX/0wd;

.field private i:LX/0wd;

.field public j:I

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:LX/5OS;

.field public n:LX/5OV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 908988
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a:LX/0wT;

    .line 908989
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 908979
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 908980
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    .line 908981
    invoke-direct {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g()V

    .line 908982
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 908990
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 908991
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    .line 908992
    invoke-direct {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g()V

    .line 908993
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 908994
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908995
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    .line 908996
    invoke-direct {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g()V

    .line 908997
    return-void
.end method

.method private a(LX/0wW;LX/0wc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 908998
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->d:LX/0wW;

    .line 908999
    iput-object p2, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->c:LX/0wc;

    .line 909000
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 909001
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    sget-object v1, LX/5OV;->NONE:LX/5OV;

    if-ne v0, v1, :cond_0

    .line 909002
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 909003
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 909004
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->requestLayout()V

    .line 909005
    :goto_0
    return-void

    .line 909006
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 909007
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f:LX/5OU;

    invoke-virtual {v0, p1, p2}, LX/5OU;->a(Landroid/view/View;Landroid/view/View;)V

    .line 909008
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {v1}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v1

    check-cast v1, LX/0wc;

    invoke-direct {p0, v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(LX/0wW;LX/0wc;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/fbui/popover/PopoverViewFlipper;I)V
    .locals 5

    .prologue
    .line 909048
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->NONE:LX/5OS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->BELOW:LX/5OS;

    if-ne v0, v1, :cond_1

    .line 909049
    :cond_0
    :goto_0
    return-void

    .line 909050
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 909051
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 909052
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v2

    .line 909053
    sub-int v2, p1, v2

    .line 909054
    iget-object v3, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v4, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method private final g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 909009
    const-class v0, Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v0, p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 909010
    sget-object v0, LX/5OV;->NONE:LX/5OV;

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    .line 909011
    new-instance v0, LX/5OT;

    invoke-direct {v0, p0}, LX/5OT;-><init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->e:LX/5OT;

    .line 909012
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    .line 909013
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->e:LX/5OT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 909014
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a()LX/5OU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f:LX/5OU;

    .line 909015
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 909016
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 909017
    move-object v0, v0

    .line 909018
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    .line 909019
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f:LX/5OU;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 909020
    sget-object v0, LX/5OS;->NONE:LX/5OS;

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    .line 909021
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 909022
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 909023
    const v2, 0x7f010245

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 909024
    iget v2, v1, Landroid/util/TypedValue;->resourceId:I

    if-lez v2, :cond_0

    .line 909025
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    .line 909026
    :goto_0
    const v2, 0x7f010246

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 909027
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-lez v0, :cond_1

    .line 909028
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    .line 909029
    :goto_1
    invoke-virtual {p0, v5}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setWillNotDraw(Z)V

    .line 909030
    return-void

    .line 909031
    :cond_0
    iput-object v6, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 909032
    :cond_1
    iput-object v6, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 909055
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 909047
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LX/5OU;
    .locals 1

    .prologue
    .line 909056
    new-instance v0, LX/5OU;

    invoke-direct {v0, p0}, LX/5OU;-><init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V

    return-object v0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 909044
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setPivotX(F)V

    .line 909045
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setPivotY(F)V

    .line 909046
    return-void
.end method

.method public final a(LX/0xh;)V
    .locals 4

    .prologue
    .line 909036
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 909037
    if-eqz p1, :cond_0

    .line 909038
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    invoke-virtual {p1, v0}, LX/0xh;->b(LX/0wd;)V

    .line 909039
    :cond_0
    :goto_0
    return-void

    .line 909040
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 909041
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 909042
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909043
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    invoke-virtual {v0, p1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 909033
    if-nez p1, :cond_0

    .line 909034
    :goto_0
    return-void

    .line 909035
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildCount()I

    move-result v0

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908983
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 908984
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 908985
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 908986
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 908987
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 908908
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 908909
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 908910
    invoke-direct {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 908911
    :goto_0
    return-void

    .line 908912
    :cond_0
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 908913
    iget v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 908914
    invoke-direct {p0, v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(Landroid/view/View;Landroid/view/View;)V

    .line 908915
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 908916
    invoke-direct {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 908917
    :goto_0
    return-void

    .line 908918
    :cond_0
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 908919
    iget v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 908920
    invoke-direct {p0, v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(Landroid/view/View;Landroid/view/View;)V

    .line 908921
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 908922
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f:LX/5OU;

    invoke-virtual {v0}, LX/5OU;->a()V

    .line 908923
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->f:LX/5OU;

    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    invoke-virtual {v0, v1}, LX/0xh;->a(LX/0wd;)V

    .line 908924
    return-void
.end method

.method public getCurrentChild()I
    .locals 1

    .prologue
    .line 908925
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    return v0
.end method

.method public getTransitionType()LX/5OV;
    .locals 1

    .prologue
    .line 908978
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x3c9f0064

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 908926
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 908927
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 908928
    const/16 v1, 0x2d

    const v2, -0x1545d83a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5575a9dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 908929
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 908930
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 908931
    const/16 v1, 0x2d

    const v2, 0x4e8ada36    # 1.16477824E9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 908932
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->NONE:LX/5OS;

    if-ne v0, v1, :cond_0

    .line 908933
    :goto_0
    return-void

    .line 908934
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 908935
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->ABOVE:LX/5OS;

    if-ne v0, v1, :cond_2

    .line 908936
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 908937
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 908938
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 908939
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->BELOW:LX/5OS;

    if-ne v0, v1, :cond_1

    .line 908940
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 908941
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 908942
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 908943
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->i:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 908944
    :cond_0
    :goto_0
    return-void

    .line 908945
    :cond_1
    iget v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->j:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 908946
    if-eqz v0, :cond_0

    .line 908947
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 908948
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 908904
    if-nez p1, :cond_0

    .line 908905
    :goto_0
    return-void

    .line 908906
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->removeAllViews()V

    .line 908907
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setDismissSpring(LX/0wd;)V
    .locals 0

    .prologue
    .line 908949
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->h:LX/0wd;

    .line 908950
    return-void
.end method

.method public setNubOffset(I)V
    .locals 5

    .prologue
    .line 908951
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->NONE:LX/5OS;

    if-ne v0, v1, :cond_1

    .line 908952
    :cond_0
    :goto_0
    return-void

    .line 908953
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    sget-object v1, LX/5OS;->ABOVE:LX/5OS;

    if-ne v0, v1, :cond_2

    .line 908954
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 908955
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 908956
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 908957
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v2

    .line 908958
    div-int/lit8 v3, v0, 0x2

    sub-int v3, p1, v3

    .line 908959
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredHeight()I

    move-result v4

    sub-int v2, v4, v2

    .line 908960
    iget-object v4, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->k:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    invoke-virtual {v4, v3, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 908961
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 908962
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 908963
    iget-object v1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 908964
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v2

    .line 908965
    div-int/lit8 v3, v0, 0x2

    sub-int v3, p1, v3

    .line 908966
    sub-int/2addr v2, v1

    .line 908967
    iget-object v4, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->l:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    invoke-virtual {v4, v3, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method public setNubShown(LX/5OS;)V
    .locals 1

    .prologue
    .line 908968
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    if-eq v0, p1, :cond_0

    .line 908969
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->m:LX/5OS;

    .line 908970
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->invalidate()V

    .line 908971
    invoke-virtual {p0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->requestLayout()V

    .line 908972
    :cond_0
    return-void
.end method

.method public setShowSpring(LX/0wd;)V
    .locals 0

    .prologue
    .line 908973
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->g:LX/0wd;

    .line 908974
    return-void
.end method

.method public setTransitionType(LX/5OV;)V
    .locals 1

    .prologue
    .line 908975
    iget-object v0, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    if-eq v0, p1, :cond_0

    .line 908976
    iput-object p1, p0, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    .line 908977
    :cond_0
    return-void
.end method
