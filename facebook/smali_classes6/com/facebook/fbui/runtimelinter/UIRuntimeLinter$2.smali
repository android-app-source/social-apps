.class public final Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6Uq;


# direct methods
.method public constructor <init>(LX/6Uq;)V
    .locals 0

    .prologue
    .line 1102938
    iput-object p1, p0, Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;->a:LX/6Uq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1102939
    iget-object v0, p0, Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;->a:LX/6Uq;

    invoke-static {v0}, LX/6Uq;->b(LX/6Uq;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Uo;

    .line 1102940
    iget-object v2, p0, Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;->a:LX/6Uq;

    .line 1102941
    :try_start_0
    iget-object v3, v2, LX/6Uq;->j:LX/0Sy;

    const-wide/16 v5, 0x1388

    invoke-virtual {v3, v5, v6}, LX/0Sy;->a(J)V

    .line 1102942
    iget-object v3, v2, LX/6Uq;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 1102943
    if-eqz v3, :cond_1

    .line 1102944
    const v4, 0x1020002

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 1102945
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 1102946
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 1102947
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "Running UI linter "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1102948
    invoke-interface {v0, v4, v5}, LX/6Uo;->a(Landroid/view/ViewGroup;Ljava/util/Map;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1102949
    const-string v6, "view_hierarchy"

    .line 1102950
    iget-object v9, v2, LX/6Uq;->g:LX/6V0;

    sget-object v10, LX/6Uz;->ALL:LX/6Uz;

    sget-object v11, LX/6Uy;->PRETTY:LX/6Uy;

    invoke-virtual {v9, v4, v10, v11}, LX/6V0;->a(Landroid/view/View;LX/6Uz;LX/6Uy;)Ljava/lang/String;

    move-result-object v9

    move-object v4, v9

    .line 1102951
    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1102952
    const-string v4, "current_activity"

    invoke-static {v3}, LX/0l0;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1102953
    iget-object v3, v2, LX/6Uq;->e:LX/03V;

    invoke-interface {v0}, LX/6Uo;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, LX/6Uo;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1102954
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v7

    .line 1102955
    const-wide/16 v5, 0x1f4

    cmp-long v5, v3, v5

    if-lez v5, :cond_1

    .line 1102956
    iget-object v5, v2, LX/6Uq;->e:LX/03V;

    const-string v6, "Slow Runtime Lint"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Testing rule "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " took "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " msec"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1102957
    :cond_1
    :goto_1
    goto/16 :goto_0

    .line 1102958
    :cond_2
    return-void

    .line 1102959
    :catch_0
    move-exception v3

    .line 1102960
    iget-object v4, v2, LX/6Uq;->e:LX/03V;

    sget-object v5, LX/6Uq;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed while testing rule "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1102961
    sget-object v4, LX/6Uq;->a:Ljava/lang/Class;

    const-string v5, "Throwable caught while linting UI."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
