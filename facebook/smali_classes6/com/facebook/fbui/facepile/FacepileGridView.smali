.class public Lcom/facebook/fbui/facepile/FacepileGridView;
.super Landroid/view/View;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/4Ac;

.field private final d:LX/1Uo;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:I

.field public h:I

.field private i:I

.field private j:I

.field private k:I

.field public l:I

.field private m:I

.field private n:LX/6UX;

.field private o:Z

.field private p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1101878
    const-class v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbui/facepile/FacepileGridView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1101879
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1101880
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1101881
    const v0, 0x7f010258

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1101882
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1101883
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1101884
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    .line 1101885
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    .line 1101886
    iput-boolean v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->o:Z

    .line 1101887
    iput-boolean v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    .line 1101888
    const-class v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-static {v0, p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1101889
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/fbui/facepile/FacepileGridView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1101890
    sget-object v0, LX/03r;->FacepileGridView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1101891
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    .line 1101892
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    .line 1101893
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    .line 1101894
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    .line 1101895
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    .line 1101896
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    .line 1101897
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1101898
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    .line 1101899
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    :goto_0
    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    .line 1101900
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->d:LX/1Uo;

    .line 1101901
    new-instance v0, LX/6UX;

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    invoke-direct {v0, v1}, LX/6UX;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    .line 1101902
    return-void

    .line 1101903
    :cond_0
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    goto :goto_0
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 1101904
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    .line 1101905
    if-nez v0, :cond_0

    .line 1101906
    invoke-direct {p0, p1}, Lcom/facebook/fbui/facepile/FacepileGridView;->b(I)I

    move-result v0

    .line 1101907
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 1101908
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    sub-int/2addr v0, v2

    .line 1101909
    add-int/2addr v0, v1

    return v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1101800
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->requestLayout()V

    .line 1101801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    .line 1101802
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->a:LX/1Ad;

    return-void
.end method

.method private b(I)I
    .locals 3

    .prologue
    .line 1101911
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    if-lez v0, :cond_0

    .line 1101912
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    .line 1101913
    :goto_0
    return v0

    .line 1101914
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1101915
    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    div-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private b()V
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1101973
    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->o:Z

    .line 1101974
    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    .line 1101975
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    if-lez v1, :cond_2

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    if-lez v1, :cond_2

    move v2, v3

    .line 1101976
    :goto_0
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v0

    move v1, v0

    :goto_1
    if-eq v5, v6, :cond_4

    .line 1101977
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1101978
    iget-object v4, v0, LX/6UY;->a:Landroid/net/Uri;

    if-eqz v4, :cond_0

    .line 1101979
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/fbui/facepile/FacepileGridView;->d(I)LX/1aX;

    move-result-object v7

    .line 1101980
    invoke-virtual {v7}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    .line 1101981
    iget-object v1, v7, LX/1aX;->f:LX/1aZ;

    move-object v8, v1

    .line 1101982
    if-eqz v2, :cond_3

    .line 1101983
    iget v1, v0, LX/6UY;->e:I

    iget v9, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    add-int/2addr v9, v10

    mul-int/2addr v1, v9

    iget v9, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    sub-int/2addr v1, v9

    .line 1101984
    iget v9, v0, LX/6UY;->d:I

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    iget v11, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    add-int/2addr v10, v11

    mul-int/2addr v9, v10

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    sub-int/2addr v9, v10

    .line 1101985
    iget-object v10, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->a:LX/1Ad;

    iget-object v11, v0, LX/6UY;->a:Landroid/net/Uri;

    invoke-static {v11}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v11

    new-instance v12, LX/1o9;

    invoke-direct {v12, v1, v9}, LX/1o9;-><init>(II)V

    .line 1101986
    iput-object v12, v11, LX/1bX;->c:LX/1o9;

    .line 1101987
    move-object v1, v11

    .line 1101988
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v10, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v8}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1101989
    :goto_2
    invoke-virtual {v7, v1}, LX/1aX;->a(LX/1aZ;)V

    move v1, v4

    .line 1101990
    :cond_0
    iget-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    iget v7, v0, LX/6UY;->f:I

    invoke-virtual {v4, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1101991
    iget-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1101992
    iget-object v4, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1

    .line 1101993
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1101994
    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_2
    move v2, v0

    .line 1101995
    goto :goto_0

    .line 1101996
    :cond_3
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->a:LX/1Ad;

    iget-object v9, v0, LX/6UY;->a:Landroid/net/Uri;

    invoke-virtual {v1, v9}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    goto :goto_2

    .line 1101997
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-le v0, v1, :cond_5

    .line 1101998
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    iget-object v4, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v4}, LX/4Ac;->d()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, LX/4Ac;->a(I)V

    goto :goto_3

    .line 1101999
    :cond_5
    if-eqz v2, :cond_6

    .line 1102000
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->d()V

    .line 1102001
    :goto_4
    return-void

    .line 1102002
    :cond_6
    iput-boolean v3, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    goto :goto_4
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 1101916
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    if-lez v0, :cond_0

    .line 1101917
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    .line 1101918
    :goto_0
    return v0

    .line 1101919
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingTop()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1101920
    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    div-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1101921
    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->o:Z

    .line 1101922
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-eq v3, v4, :cond_0

    .line 1101923
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1101924
    iget-object v1, v0, LX/6UY;->a:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1101925
    iget-object v5, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v5, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    .line 1101926
    iget v5, v0, LX/6UY;->e:I

    iget v6, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    iget v7, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    add-int/2addr v6, v7

    mul-int/2addr v5, v6

    iget v6, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    sub-int/2addr v5, v6

    .line 1101927
    iget v6, v0, LX/6UY;->d:I

    iget v7, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    iget v8, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    add-int/2addr v7, v8

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    sub-int/2addr v6, v7

    .line 1101928
    iget-object v7, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->a:LX/1Ad;

    iget-object v0, v0, LX/6UY;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v8, LX/1o9;

    invoke-direct {v8, v5, v6}, LX/1o9;-><init>(II)V

    .line 1101929
    iput-object v8, v0, LX/1bX;->c:LX/1o9;

    .line 1101930
    move-object v0, v0

    .line 1101931
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1101932
    iget-object v5, v2, LX/1aX;->f:LX/1aZ;

    move-object v5, v5

    .line 1101933
    invoke-virtual {v0, v5}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1101934
    invoke-virtual {v2, v0}, LX/1aX;->a(LX/1aZ;)V

    move v0, v1

    .line 1101935
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 1101936
    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private d(I)LX/1aX;
    .locals 3

    .prologue
    .line 1101937
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1101938
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->d:LX/1Uo;

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Ac;->a(LX/1aX;)V

    .line 1101939
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 17

    .prologue
    .line 1101940
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    .line 1101941
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingLeft()I

    move-result v4

    .line 1101942
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingTop()I

    move-result v5

    .line 1101943
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    add-int v6, v1, v2

    .line 1101944
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    add-int v7, v1, v2

    .line 1101945
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    invoke-virtual {v1}, LX/6UX;->b()V

    .line 1101946
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v3, v1

    :goto_0
    if-eq v3, v8, :cond_2

    .line 1101947
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    invoke-virtual {v1}, LX/6UX;->a()I

    move-result v2

    .line 1101948
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    invoke-virtual {v1, v2}, LX/6UX;->a(I)I

    move-result v9

    .line 1101949
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6UY;

    .line 1101950
    iget v10, v1, LX/6UY;->e:I

    .line 1101951
    iget v11, v1, LX/6UY;->d:I

    .line 1101952
    iget-object v12, v1, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v12, :cond_0

    .line 1101953
    mul-int v12, v6, v2

    add-int/2addr v12, v4

    .line 1101954
    mul-int v13, v7, v9

    add-int/2addr v13, v5

    .line 1101955
    mul-int v14, v10, v6

    add-int/2addr v14, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    sub-int/2addr v14, v15

    .line 1101956
    mul-int v15, v11, v7

    add-int/2addr v15, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    .line 1101957
    iget-object v0, v1, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1101958
    iget-object v1, v1, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    .line 1101959
    if-eqz v1, :cond_0

    .line 1101960
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    sub-int v12, v14, v12

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    sub-int v13, v15, v13

    invoke-virtual {v1, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1101961
    :cond_0
    add-int/2addr v9, v11

    move v1, v2

    .line 1101962
    :goto_1
    add-int v11, v2, v10

    if-ge v1, v11, :cond_1

    .line 1101963
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    invoke-virtual {v11, v1, v9}, LX/6UX;->a(II)V

    .line 1101964
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1101965
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1101966
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->invalidate()V

    .line 1101967
    return-void
.end method

.method private getPreferredWidth()I
    .locals 3

    .prologue
    .line 1101968
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1101969
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    sub-int/2addr v1, v2

    .line 1101970
    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getCellHeight()I
    .locals 1

    .prologue
    .line 1101971
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    return v0
.end method

.method public getCellWidth()I
    .locals 1

    .prologue
    .line 1101972
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    return v0
.end method

.method public getHorizontalPadding()I
    .locals 1

    .prologue
    .line 1101877
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    return v0
.end method

.method public getNumCols()I
    .locals 1

    .prologue
    .line 1101910
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    return v0
.end method

.method public getNumRows()I
    .locals 1

    .prologue
    .line 1101799
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    return v0
.end method

.method public getVerticalPadding()I
    .locals 1

    .prologue
    .line 1101803
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4d2c97c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1101804
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1101805
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1101806
    const/16 v1, 0x2d

    const v2, 0x4b09857d    # 9012605.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x604d3426

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1101807
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1101808
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1101809
    const/16 v1, 0x2d

    const v2, 0x5773526c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1101810
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1101811
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-eq v1, v2, :cond_1

    .line 1101812
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1101813
    iget-object v3, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1101814
    iget-object v3, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 1101815
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1101816
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1101817
    :cond_1
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1101818
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1101819
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1101820
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 1101821
    sub-int v0, p4, p2

    invoke-direct {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->b(I)I

    move-result v0

    .line 1101822
    sub-int v1, p5, p3

    invoke-direct {p0, v1}, Lcom/facebook/fbui/facepile/FacepileGridView;->c(I)I

    move-result v1

    .line 1101823
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    if-eq v2, v1, :cond_1

    .line 1101824
    :cond_0
    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    .line 1101825
    iput v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->m:I

    .line 1101826
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    .line 1101827
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->o:Z

    if-eqz v0, :cond_2

    .line 1101828
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->c()V

    .line 1101829
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->p:Z

    if-eqz v0, :cond_3

    .line 1101830
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->d()V

    .line 1101831
    :cond_3
    return-void
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 1101832
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->getPreferredWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/facebook/fbui/facepile/FacepileGridView;->resolveSize(II)I

    move-result v0

    .line 1101833
    invoke-direct {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a(I)I

    move-result v1

    invoke-static {v1, p2}, Lcom/facebook/fbui/facepile/FacepileGridView;->resolveSize(II)I

    move-result v1

    .line 1101834
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/facepile/FacepileGridView;->setMeasuredDimension(II)V

    .line 1101835
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1101836
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1101837
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->c:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1101838
    return-void
.end method

.method public setCellHeight(I)V
    .locals 1

    .prologue
    .line 1101839
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    if-eq v0, p1, :cond_0

    .line 1101840
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->k:I

    .line 1101841
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101842
    :cond_0
    return-void
.end method

.method public setCellWidth(I)V
    .locals 1

    .prologue
    .line 1101843
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    if-eq v0, p1, :cond_0

    .line 1101844
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->j:I

    .line 1101845
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101846
    :cond_0
    return-void
.end method

.method public setFaces(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1101847
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1101848
    if-eqz p1, :cond_0

    .line 1101849
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1101850
    :cond_0
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->b()V

    .line 1101851
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->invalidate()V

    .line 1101852
    return-void
.end method

.method public setHorizontalPadding(I)V
    .locals 1

    .prologue
    .line 1101853
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    if-eq v0, p1, :cond_0

    .line 1101854
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    .line 1101855
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101856
    :cond_0
    return-void
.end method

.method public setNumCols(I)V
    .locals 1

    .prologue
    .line 1101857
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    if-eq v0, p1, :cond_0

    if-lez p1, :cond_0

    .line 1101858
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->i:I

    .line 1101859
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101860
    new-instance v0, LX/6UX;

    invoke-direct {v0, p1}, LX/6UX;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->n:LX/6UX;

    .line 1101861
    :cond_0
    return-void
.end method

.method public setNumRows(I)V
    .locals 1

    .prologue
    .line 1101862
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    if-eq v0, p1, :cond_0

    if-lez p1, :cond_0

    .line 1101863
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    .line 1101864
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101865
    :cond_0
    return-void
.end method

.method public setVerticalPadding(I)V
    .locals 1

    .prologue
    .line 1101866
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    if-eq v0, p1, :cond_0

    .line 1101867
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->g:I

    .line 1101868
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileGridView;->a()V

    .line 1101869
    :cond_0
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 4

    .prologue
    .line 1101870
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-eq v1, v2, :cond_2

    .line 1101871
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1101872
    iget-object v3, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eq v3, p1, :cond_0

    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    .line 1101873
    :cond_0
    const/4 v0, 0x1

    .line 1101874
    :goto_1
    return v0

    .line 1101875
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1101876
    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_1
.end method
