.class public Lcom/facebook/fbui/facepile/FacepileView;
.super Landroid/view/View;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:I

.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/4Ac;

.field private final f:LX/1Uo;

.field private final g:LX/1nq;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field public m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Z

.field public s:I

.field private t:I

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Landroid/text/Layout;

.field private y:Landroid/graphics/Paint;

.field private z:Landroid/graphics/RectF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1102161
    const-class v0, Lcom/facebook/fbui/facepile/FacepileView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbui/facepile/FacepileView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1102162
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/facepile/FacepileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1102163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1102164
    const v0, 0x7f010257

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/facepile/FacepileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1102166
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1102167
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    .line 1102168
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    .line 1102169
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    .line 1102170
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    .line 1102171
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    .line 1102172
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    .line 1102173
    iput-boolean v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    .line 1102174
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    .line 1102175
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->t:I

    .line 1102176
    iput v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->u:I

    .line 1102177
    const-class v0, Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {v0, p0}, Lcom/facebook/fbui/facepile/FacepileView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1102178
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1102179
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    .line 1102180
    new-instance v0, LX/1Uo;

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->f:LX/1Uo;

    .line 1102181
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->a:LX/1Ad;

    sget-object v2, Lcom/facebook/fbui/facepile/FacepileView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1102182
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    .line 1102183
    sget-object v0, LX/03r;->FacepileView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1102184
    const/16 v0, 0x0

    const v3, 0x800033

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    .line 1102185
    const/16 v0, 0x6

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    .line 1102186
    const/16 v0, 0xc

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    .line 1102187
    const/16 v0, 0x4

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->k:I

    .line 1102188
    const/16 v0, 0xf

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->o:Z

    .line 1102189
    const/16 v0, 0x10

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    .line 1102190
    const/16 v0, 0x7

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    .line 1102191
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    if-eqz v0, :cond_0

    .line 1102192
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v0

    .line 1102193
    const/16 v3, 0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->t:I

    .line 1102194
    const/16 v3, 0x2

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 1102195
    const/16 v4, 0x3

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->u:I

    .line 1102196
    iget v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->t:I

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, LX/4Ab;->a(IF)LX/4Ab;

    .line 1102197
    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->u:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, LX/4Ab;->d(F)LX/4Ab;

    .line 1102198
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->f:LX/1Uo;

    .line 1102199
    iput-object v0, v3, LX/1Uo;->u:LX/4Ab;

    .line 1102200
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    .line 1102201
    const/16 v0, 0x9

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->v:Z

    .line 1102202
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->v:Z

    if-eqz v0, :cond_2

    .line 1102203
    const/16 v0, 0xe

    const v3, 0x7f0a00d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1102204
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    invoke-virtual {v3, v0}, LX/1nq;->c(I)LX/1nq;

    move-result-object v0

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v3}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 1102205
    const/16 v0, 0xb

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 1102206
    const v0, 0x7f0b0050

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1102207
    if-eqz v3, :cond_1

    .line 1102208
    sget-object v0, LX/03r;->TextStyle:[I

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1102209
    const/16 v0, 0x1

    const v4, 0x7f0b0050

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 1102210
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1102211
    :cond_1
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    invoke-virtual {v3, v0}, LX/1nq;->b(I)LX/1nq;

    .line 1102212
    new-instance v0, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->y:Landroid/graphics/Paint;

    .line 1102213
    const/16 v0, 0xd

    const v3, 0x7f0a00fb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1102214
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->y:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1102215
    const/16 v0, 0xa

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->w:Z

    .line 1102216
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    .line 1102217
    :cond_2
    const/16 v0, 0x11

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1102218
    if-eqz v0, :cond_3

    .line 1102219
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->f:LX/1Uo;

    .line 1102220
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1102221
    :cond_3
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1102222
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 1102223
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v0

    .line 1102224
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v1, v0

    .line 1102225
    if-nez v1, :cond_0

    .line 1102226
    const/4 v0, 0x0

    .line 1102227
    :goto_0
    return v0

    :cond_0
    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingStart()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1102228
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v2, v2

    .line 1102229
    sub-int/2addr v0, v2

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1102230
    if-eqz p0, :cond_1

    .line 1102231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1102232
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1102233
    new-instance v3, LX/6UY;

    invoke-direct {v3, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 1102234
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(LX/1nq;III)V
    .locals 3

    .prologue
    .line 1102235
    move v0, p2

    .line 1102236
    :cond_0
    add-int v1, v0, p3

    div-int/lit8 v1, v1, 0x2

    .line 1102237
    invoke-virtual {p0, v1}, LX/1nq;->b(I)LX/1nq;

    move-result-object v2

    invoke-virtual {v2}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v2

    .line 1102238
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    if-gt v2, p1, :cond_1

    .line 1102239
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 1102240
    add-int/lit8 v0, v1, 0x1

    .line 1102241
    :goto_0
    if-lt v0, p3, :cond_0

    .line 1102242
    invoke-virtual {p0, p2}, LX/1nq;->b(I)LX/1nq;

    .line 1102243
    return-void

    :cond_1
    move p3, v1

    .line 1102244
    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1102245
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->x:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 1102246
    :goto_0
    return-void

    .line 1102247
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->w:Z

    if-eqz v0, :cond_1

    .line 1102248
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1102249
    :goto_1
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->A:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1102250
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->x:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1102251
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->A:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 1102252
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    if-eqz v0, :cond_2

    .line 1102253
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1102254
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private static a(Lcom/facebook/fbui/facepile/FacepileView;LX/1Ad;LX/154;LX/0hL;)V
    .locals 0

    .prologue
    .line 1102255
    iput-object p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/fbui/facepile/FacepileView;->b:LX/154;

    iput-object p3, p0, Lcom/facebook/fbui/facepile/FacepileView;->c:LX/0hL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    invoke-static {v2}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v2

    check-cast v2, LX/0hL;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->a(Lcom/facebook/fbui/facepile/FacepileView;LX/1Ad;LX/154;LX/0hL;)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 1102384
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1102385
    iget-boolean v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->v:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 1102256
    if-nez p1, :cond_0

    .line 1102257
    const/4 v0, 0x0

    .line 1102258
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 1102383
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->o:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getTotalItemsToDraw()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 14

    .prologue
    .line 1102309
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1102310
    :cond_0
    :goto_0
    return-void

    .line 1102311
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->c:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v4

    .line 1102312
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingTop()I

    move-result v1

    .line 1102313
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingBottom()I

    move-result v3

    .line 1102314
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getNumFacesToDraw()I

    move-result v5

    .line 1102315
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getMeasuredHeight()I

    move-result v6

    .line 1102316
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getTotalItemsToDraw()I

    move-result v7

    .line 1102317
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    and-int/lit8 v8, v0, 0x70

    .line 1102318
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->w:Z

    if-eqz v0, :cond_4

    const/high16 v0, 0x3fc00000    # 1.5f

    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    :goto_1
    float-to-int v9, v0

    .line 1102319
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getStartX()I

    move-result v2

    .line 1102320
    const/16 v0, 0x50

    if-ne v8, v0, :cond_5

    .line 1102321
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    sub-int v0, v6, v0

    sub-int/2addr v0, v3

    move v1, v0

    .line 1102322
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1102323
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    if-eqz v4, :cond_13

    .line 1102324
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v0

    add-int/2addr v0, v9

    add-int/2addr v0, v2

    .line 1102325
    :goto_3
    const/4 v2, 0x0

    move v3, v0

    :goto_4
    if-ge v2, v5, :cond_7

    .line 1102326
    if-eqz v4, :cond_6

    add-int/lit8 v0, v5, -0x1

    sub-int/2addr v0, v2

    .line 1102327
    :goto_5
    iget-object v6, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102328
    if-eqz v0, :cond_3

    iget-object v6, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_3

    .line 1102329
    iget-object v6, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    iget v8, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v8, v3

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v10, v1

    invoke-virtual {v6, v3, v1, v8, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1102330
    iget-object v6, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_3

    .line 1102331
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    .line 1102332
    iget v6, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v6, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    sub-int/2addr v6, v8

    .line 1102333
    iget v8, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v8, v8

    .line 1102334
    add-int/2addr v6, v8

    iget v8, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v8, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    sub-int/2addr v8, v10

    .line 1102335
    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v10, v10

    .line 1102336
    add-int/2addr v8, v10

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v10, v3

    .line 1102337
    iget v11, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v11, v11

    .line 1102338
    add-int/2addr v10, v11

    iget v11, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v11, v1

    .line 1102339
    iget v12, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v12, v12

    .line 1102340
    add-int/2addr v11, v12

    invoke-virtual {v0, v6, v8, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1102341
    :cond_3
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v3, v0

    .line 1102342
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1102343
    :cond_4
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    int-to-float v0, v0

    goto/16 :goto_1

    .line 1102344
    :cond_5
    const/16 v0, 0x10

    if-ne v8, v0, :cond_2

    .line 1102345
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    sub-int v0, v6, v0

    sub-int/2addr v0, v1

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    move v1, v0

    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 1102346
    goto :goto_5

    :cond_7
    move v0, v3

    .line 1102347
    :goto_6
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1102348
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    if-gtz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v5

    .line 1102349
    :goto_7
    iget-boolean v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    if-eqz v3, :cond_f

    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->t:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v9, v3

    .line 1102350
    :goto_8
    iget-object v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    invoke-virtual {v5, v3}, LX/1nq;->a(I)LX/1nq;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0805e3

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/facebook/fbui/facepile/FacepileView;->b:LX/154;

    invoke-virtual {v11, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v10

    invoke-virtual {v5, v6, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1102351
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    invoke-virtual {v2}, LX/1nq;->b()F

    move-result v2

    float-to-int v2, v2

    .line 1102352
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->b()Z

    move-result v3

    if-nez v3, :cond_10

    .line 1102353
    if-eqz v4, :cond_8

    .line 1102354
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getStartX()I

    move-result v0

    .line 1102355
    :cond_8
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    iget v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    const/4 v5, 0x1

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/fbui/facepile/FacepileView;->a(LX/1nq;III)V

    .line 1102356
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    int-to-float v3, v0

    int-to-float v4, v1

    add-int/2addr v0, v9

    int-to-float v0, v0

    iget v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v5, v1

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1102357
    :goto_9
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->x:Landroid/text/Layout;

    .line 1102358
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->x:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->A:I

    goto/16 :goto_0

    .line 1102359
    :cond_9
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    if-eqz v4, :cond_a

    .line 1102360
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v0

    add-int/2addr v0, v9

    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v6, v7, 0x2

    sub-int/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int/2addr v3, v6

    sub-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1102361
    :cond_a
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    add-int/lit8 v3, v7, -0x2

    mul-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1102362
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    mul-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 1102363
    const/4 v0, 0x0

    move v3, v0

    move v13, v2

    move v2, v1

    move v1, v13

    :goto_a
    if-ge v3, v5, :cond_12

    .line 1102364
    if-eqz v4, :cond_c

    .line 1102365
    add-int/lit8 v0, v5, -0x1

    sub-int v6, v0, v3

    .line 1102366
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v8, v7, 0x2

    sub-int/2addr v8, v6

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    mul-int/2addr v8, v0

    .line 1102367
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    iget-object v0, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v10, v1

    sub-int/2addr v10, v8

    iget v11, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v11, v2

    sub-int/2addr v11, v8

    invoke-virtual {v0, v1, v2, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1102368
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    sub-int/2addr v0, v8

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v8

    add-int/2addr v0, v8

    add-int/2addr v1, v0

    .line 1102369
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v8, v0, 0x2

    add-int/lit8 v0, v6, 0x1

    div-int/lit8 v6, v7, 0x2

    if-le v0, v6, :cond_b

    const/4 v0, -0x1

    :goto_b
    mul-int/2addr v0, v8

    add-int/2addr v0, v2

    move v13, v1

    move v1, v0

    move v0, v13

    .line 1102370
    :goto_c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_a

    .line 1102371
    :cond_b
    const/4 v0, 0x1

    goto :goto_b

    .line 1102372
    :cond_c
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v6, v7, 0x2

    sub-int/2addr v6, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int/2addr v6, v0

    .line 1102373
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    iget-object v0, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    iget v8, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v8, v1

    sub-int/2addr v8, v6

    iget v10, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v10, v2

    sub-int/2addr v10, v6

    invoke-virtual {v0, v1, v2, v8, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1102374
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    sub-int/2addr v0, v6

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getHorizontalPadding()I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v1, v0

    .line 1102375
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v6, v0, 0x2

    add-int/lit8 v0, v3, 0x1

    div-int/lit8 v8, v7, 0x2

    if-le v0, v8, :cond_d

    const/4 v0, -0x1

    :goto_d
    mul-int/2addr v0, v6

    sub-int v0, v2, v0

    move v13, v1

    move v1, v0

    move v0, v13

    goto :goto_c

    :cond_d
    const/4 v0, 0x1

    goto :goto_d

    .line 1102376
    :cond_e
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int/2addr v2, v5

    goto/16 :goto_7

    .line 1102377
    :cond_f
    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    goto/16 :goto_8

    .line 1102378
    :cond_10
    if-eqz v4, :cond_11

    .line 1102379
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getStartX()I

    move-result v0

    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    add-int/lit8 v4, v7, -0x2

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1102380
    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    .line 1102381
    :cond_11
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->g:LX/1nq;

    iget v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    iget v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/fbui/facepile/FacepileView;->a(LX/1nq;III)V

    .line 1102382
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->z:Landroid/graphics/RectF;

    int-to-float v3, v0

    int-to-float v4, v1

    add-int/2addr v0, v9

    iget v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v6, v7, 0x2

    mul-int/2addr v5, v6

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iget v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    add-int/2addr v5, v1

    iget v6, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    div-int/lit8 v7, v7, 0x2

    mul-int/2addr v6, v7

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_9

    :cond_12
    move v0, v1

    move v1, v2

    goto/16 :goto_6

    :cond_13
    move v0, v2

    goto/16 :goto_3
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1102303
    :goto_0
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1102304
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/4Ac;->a(I)V

    goto :goto_0

    .line 1102305
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 1102306
    new-instance v0, LX/1aX;

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->f:LX/1Uo;

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1aX;-><init>(LX/1aY;)V

    .line 1102307
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v1, v0}, LX/4Ac;->a(LX/1aX;)V

    goto :goto_1

    .line 1102308
    :cond_1
    return-void
.end method

.method private getNumFacesToDraw()I
    .locals 2

    .prologue
    .line 1102297
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    .line 1102298
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getMeasuredWidth()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/fbui/facepile/FacepileView;->a(I)I

    move-result v0

    .line 1102299
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    if-gt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1102300
    :goto_0
    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    add-int/lit8 v0, v0, -0x1

    :goto_1
    return v0

    .line 1102301
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1102302
    :cond_1
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    goto :goto_1
.end method

.method private getStartX()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1102285
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingStart()I

    move-result v0

    .line 1102286
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingEnd()I

    move-result v2

    .line 1102287
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getMeasuredWidth()I

    move-result v3

    .line 1102288
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getWidthForFacesToDraw()I

    move-result v1

    sub-int/2addr v1, v0

    sub-int v4, v1, v2

    .line 1102289
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->c:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    invoke-static {v1, v6}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v1

    .line 1102290
    :goto_0
    and-int/lit8 v1, v1, 0x7

    .line 1102291
    const/4 v5, 0x5

    if-ne v1, v5, :cond_2

    .line 1102292
    sub-int v1, v3, v4

    sub-int v0, v1, v0

    .line 1102293
    :cond_0
    :goto_1
    return v0

    .line 1102294
    :cond_1
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    goto :goto_0

    .line 1102295
    :cond_2
    if-ne v1, v6, :cond_0

    .line 1102296
    sub-int v1, v3, v4

    sub-int/2addr v1, v0

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method private getTotalItemsToDraw()I
    .locals 2

    .prologue
    .line 1102284
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getNumFacesToDraw()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWidthForFacesToDraw()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1102278
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getTotalItemsToDraw()I

    move-result v2

    .line 1102279
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->w:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 1102280
    :goto_0
    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    mul-int/2addr v3, v2

    invoke-direct {p0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->b(I)I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingStart()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingEnd()I

    move-result v3

    add-int/2addr v2, v3

    iget-boolean v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->v:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    iget-object v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    :goto_1
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    .line 1102281
    goto :goto_0

    .line 1102282
    :cond_1
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v1, v1

    .line 1102283
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/4Ac;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;",
            "LX/4Ac;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1102260
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1102261
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    :goto_0
    iput-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    .line 1102262
    if-nez p1, :cond_1

    .line 1102263
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->c()V

    .line 1102264
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102265
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102266
    :goto_1
    return-void

    :cond_0
    move-object v0, p2

    .line 1102267
    goto :goto_0

    .line 1102268
    :cond_1
    if-nez p2, :cond_3

    .line 1102269
    const/4 v0, 0x0

    .line 1102270
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102271
    iget-object v0, v0, LX/6UY;->a:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 1102272
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 1102273
    goto :goto_2

    .line 1102274
    :cond_2
    invoke-direct {p0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->c(I)V

    .line 1102275
    :cond_3
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1102276
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102277
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public getBadgeOffset()I
    .locals 1

    .prologue
    .line 1102259
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    return v0
.end method

.method public getDrawFaceCount()I
    .locals 1

    .prologue
    .line 1102159
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFaceCountForOverflow()I
    .locals 1

    .prologue
    .line 1102160
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    return v0
.end method

.method public getFaceSize()I
    .locals 1

    .prologue
    .line 1102090
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    return v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 1102099
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    return v0
.end method

.method public getHorizontalPadding()I
    .locals 2

    .prologue
    .line 1102094
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v0, v0

    .line 1102095
    if-lez v0, :cond_0

    .line 1102096
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v0, v0

    .line 1102097
    :goto_0
    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->k:I

    add-int/2addr v0, v1

    return v0

    .line 1102098
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3a76f91b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1102091
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1102092
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1102093
    const/16 v1, 0x2d

    const v2, -0x3d4fb10c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2930afbd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1102156
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1102157
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1102158
    const/16 v1, 0x2d

    const v2, 0x12dec360

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1102057
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1102058
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1102059
    :cond_0
    :goto_0
    return-void

    .line 1102060
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getNumFacesToDraw()I

    move-result v2

    .line 1102061
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getTotalItemsToDraw()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    .line 1102062
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 1102063
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v0

    .line 1102064
    :goto_1
    div-int/lit8 v0, v2, 0x2

    if-gt v1, v0, :cond_0

    if-ge v1, v4, :cond_0

    .line 1102065
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102066
    iget-object v5, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1102067
    iget-object v5, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_2

    .line 1102068
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1102069
    :cond_2
    sub-int v0, v3, v1

    if-ne v0, v2, :cond_4

    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1102070
    invoke-direct {p0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->a(Landroid/graphics/Canvas;)V

    .line 1102071
    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1102072
    :cond_4
    div-int/lit8 v0, v3, 0x2

    if-ge v1, v0, :cond_3

    .line 1102073
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    sub-int v5, v3, v1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102074
    iget-object v5, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1102075
    iget-object v5, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_3

    .line 1102076
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 1102077
    :cond_5
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->p:Z

    if-eqz v1, :cond_6

    .line 1102078
    invoke-direct {p0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->a(Landroid/graphics/Canvas;)V

    :cond_6
    move v1, v0

    .line 1102079
    :goto_3
    if-ge v1, v2, :cond_9

    if-ge v1, v4, :cond_9

    .line 1102080
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->p:Z

    if-eqz v0, :cond_8

    add-int/lit8 v0, v2, -0x1

    sub-int/2addr v0, v1

    .line 1102081
    :goto_4
    iget-object v3, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102082
    if-eqz v0, :cond_7

    iget-object v3, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_7

    .line 1102083
    iget-object v3, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1102084
    iget-object v3, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_7

    .line 1102085
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1102086
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_8
    move v0, v1

    .line 1102087
    goto :goto_4

    .line 1102088
    :cond_9
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->p:Z

    if-nez v0, :cond_0

    .line 1102089
    invoke-direct {p0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->a(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1102054
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1102055
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1102056
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1102023
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1102024
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    if-nez v0, :cond_4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 1102025
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    .line 1102026
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1102027
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1102028
    if-eq v1, v4, :cond_0

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_5

    .line 1102029
    :cond_0
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0}, Lcom/facebook/fbui/facepile/FacepileView;->a(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    .line 1102030
    :goto_1
    if-ne v1, v4, :cond_6

    move v1, v0

    .line 1102031
    :goto_2
    const/4 v0, 0x0

    .line 1102032
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :cond_1
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102033
    if-eqz v0, :cond_1

    .line 1102034
    iget-object v4, v0, LX/6UY;->a:Landroid/net/Uri;

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    if-ge v2, v4, :cond_2

    .line 1102035
    iget-object v4, p0, Lcom/facebook/fbui/facepile/FacepileView;->a:LX/1Ad;

    iget-object v5, v0, LX/6UY;->a:Landroid/net/Uri;

    invoke-virtual {v4, v5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    .line 1102036
    iget-object v5, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v5, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v5

    .line 1102037
    invoke-virtual {v5, v4}, LX/1aX;->a(LX/1aZ;)V

    .line 1102038
    invoke-virtual {v5}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    .line 1102039
    add-int/lit8 v2, v2, 0x1

    .line 1102040
    :cond_2
    iget-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_3

    .line 1102041
    iget-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    iget v5, v0, LX/6UY;->f:I

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1102042
    iget-object v4, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1102043
    :cond_3
    iget-object v4, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1

    .line 1102044
    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_3

    .line 1102045
    :cond_4
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    iput v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    goto :goto_0

    .line 1102046
    :cond_5
    iget-object v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->q:I

    goto :goto_1

    .line 1102047
    :cond_6
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getWidthForFacesToDraw()I

    move-result v0

    move v1, v0

    goto :goto_2

    .line 1102048
    :cond_7
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v0, v0

    .line 1102049
    add-int/2addr v0, v1

    invoke-static {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->resolveSize(II)I

    move-result v0

    iget v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 1102050
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    move v2, v2

    .line 1102051
    add-int/2addr v1, v2

    invoke-static {v1, p2}, Lcom/facebook/fbui/facepile/FacepileView;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setMeasuredDimension(II)V

    .line 1102052
    invoke-direct {p0}, Lcom/facebook/fbui/facepile/FacepileView;->c()V

    .line 1102053
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1102020
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1102021
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->e:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1102022
    return-void
.end method

.method public setBadgeOffset(I)V
    .locals 1

    .prologue
    .line 1102015
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    if-eq v0, p1, :cond_0

    .line 1102016
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->s:I

    .line 1102017
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102018
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102019
    :cond_0
    return-void
.end method

.method public setFaceCountForOverflow(I)V
    .locals 1

    .prologue
    .line 1102010
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    if-eq v0, p1, :cond_0

    .line 1102011
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->j:I

    .line 1102012
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102013
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102014
    :cond_0
    return-void
.end method

.method public setFaceDrawables(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1102003
    if-eqz p1, :cond_1

    .line 1102004
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1102005
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1102006
    new-instance v3, LX/6UY;

    invoke-direct {v3, v0}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1102007
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1102008
    :goto_1
    return-void

    .line 1102009
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    goto :goto_1
.end method

.method public setFaceSize(I)V
    .locals 1

    .prologue
    .line 1102100
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    if-eq v0, p1, :cond_0

    .line 1102101
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->l:I

    .line 1102102
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102103
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102104
    :cond_0
    return-void
.end method

.method public setFaceSizeOffset(I)V
    .locals 1

    .prologue
    .line 1102105
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    if-eq v0, p1, :cond_0

    .line 1102106
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->n:I

    .line 1102107
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102108
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102109
    :cond_0
    return-void
.end method

.method public setFaceStrings(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1102110
    if-eqz p1, :cond_1

    .line 1102111
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1102112
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1102113
    new-instance v3, LX/6UY;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1102114
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1102115
    :goto_1
    return-void

    .line 1102116
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    goto :goto_1
.end method

.method public setFaceUrls(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1102117
    if-eqz p1, :cond_1

    .line 1102118
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1102119
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1102120
    new-instance v3, LX/6UY;

    invoke-direct {v3, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1102121
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1102122
    :goto_1
    return-void

    .line 1102123
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    goto :goto_1
.end method

.method public setFaces(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1102124
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->a(Ljava/util/List;LX/4Ac;)V

    .line 1102125
    return-void
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 1102126
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    if-eq v0, p1, :cond_0

    .line 1102127
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->i:I

    .line 1102128
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102129
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102130
    :cond_0
    return-void
.end method

.method public setHorizontalPadding(I)V
    .locals 1

    .prologue
    .line 1102131
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->k:I

    if-eq v0, p1, :cond_0

    .line 1102132
    iput p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->k:I

    .line 1102133
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102134
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102135
    :cond_0
    return-void
.end method

.method public setReverseFacesZIndex(Z)V
    .locals 1

    .prologue
    .line 1102136
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->p:Z

    if-eq v0, p1, :cond_0

    .line 1102137
    iput-boolean p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->p:Z

    .line 1102138
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102139
    :cond_0
    return-void
.end method

.method public setShowCentralizedSymmetricLayout(Z)V
    .locals 1

    .prologue
    .line 1102140
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->o:Z

    if-eq v0, p1, :cond_0

    .line 1102141
    iput-boolean p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->o:Z

    .line 1102142
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102143
    :cond_0
    return-void
.end method

.method public setShowRoundFaces(Z)V
    .locals 2

    .prologue
    .line 1102144
    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    if-eq v0, p1, :cond_0

    .line 1102145
    iput-boolean p1, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    .line 1102146
    iget-object v1, p0, Lcom/facebook/fbui/facepile/FacepileView;->f:LX/1Uo;

    iget-boolean v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->r:Z

    if-eqz v0, :cond_1

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v0

    .line 1102147
    :goto_0
    iput-object v0, v1, LX/1Uo;->u:LX/4Ab;

    .line 1102148
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->requestLayout()V

    .line 1102149
    invoke-virtual {p0}, Lcom/facebook/fbui/facepile/FacepileView;->invalidate()V

    .line 1102150
    :cond_0
    return-void

    .line 1102151
    :cond_1
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .prologue
    .line 1102152
    iget-object v0, p0, Lcom/facebook/fbui/facepile/FacepileView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 1102153
    if-eqz v0, :cond_0

    iget-object v2, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    if-eq v2, p1, :cond_1

    iget-object v0, v0, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 1102154
    :cond_1
    const/4 v0, 0x1

    .line 1102155
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
