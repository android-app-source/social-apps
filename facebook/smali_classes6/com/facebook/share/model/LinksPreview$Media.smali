.class public final Lcom/facebook/share/model/LinksPreview$Media;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/model/LinksPreview$Media;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final height:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field public final src:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final width:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1021509
    const-class v0, Lcom/facebook/share/model/LinksPreview_MediaDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021510
    new-instance v0, LX/5vG;

    invoke-direct {v0}, LX/5vG;-><init>()V

    sput-object v0, Lcom/facebook/share/model/LinksPreview$Media;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1021511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021512
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview$Media;->type:Ljava/lang/String;

    .line 1021513
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    .line 1021514
    iput v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    .line 1021515
    iput v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    .line 1021516
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1021517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021518
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->type:Ljava/lang/String;

    .line 1021519
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    .line 1021520
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    .line 1021521
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    .line 1021522
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1021523
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1021524
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1021525
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021526
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021527
    iget v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1021528
    iget v0, p0, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1021529
    return-void
.end method
