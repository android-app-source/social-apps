.class public final Lcom/facebook/share/model/LinksPreview$MisinformationAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/model/LinksPreview$MisinformationAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final actionType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "action_type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final subtitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtitle"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final uri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1021533
    const-class v0, Lcom/facebook/share/model/LinksPreview_MisinformationActionDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021534
    new-instance v0, LX/5vH;

    invoke-direct {v0}, LX/5vH;-><init>()V

    sput-object v0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1021535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021536
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->actionType:Ljava/lang/String;

    .line 1021537
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->uri:Ljava/lang/String;

    .line 1021538
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->title:Ljava/lang/String;

    .line 1021539
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->subtitle:Ljava/lang/String;

    .line 1021540
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1021541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021542
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->actionType:Ljava/lang/String;

    .line 1021543
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->uri:Ljava/lang/String;

    .line 1021544
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->title:Ljava/lang/String;

    .line 1021545
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->subtitle:Ljava/lang/String;

    .line 1021546
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1021547
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1021548
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->actionType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021549
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021550
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021551
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021552
    return-void
.end method
