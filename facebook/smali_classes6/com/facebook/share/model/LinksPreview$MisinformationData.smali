.class public final Lcom/facebook/share/model/LinksPreview$MisinformationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/model/LinksPreview$MisinformationData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final actions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "actions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/share/model/LinksPreview$MisinformationAction;",
            ">;"
        }
    .end annotation
.end field

.field public final alertDescription:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "alert_description"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ctaText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cta_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final disputeFormUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dispute_form_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final disputeText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dispute_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final linkType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final reshareAlertTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reshare_alert_title"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final subtitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtitle"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "url"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1021591
    const-class v0, Lcom/facebook/share/model/LinksPreview_MisinformationDataDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021590
    new-instance v0, LX/2rz;

    invoke-direct {v0}, LX/2rz;-><init>()V

    sput-object v0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1021577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021578
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->title:Ljava/lang/String;

    .line 1021579
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->url:Ljava/lang/String;

    .line 1021580
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->linkType:Ljava/lang/String;

    .line 1021581
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->subtitle:Ljava/lang/String;

    .line 1021582
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->ctaText:Ljava/lang/String;

    .line 1021583
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->alertDescription:Ljava/lang/String;

    .line 1021584
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->reshareAlertTitle:Ljava/lang/String;

    .line 1021585
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeText:Ljava/lang/String;

    .line 1021586
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeFormUri:Ljava/lang/String;

    .line 1021587
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1021588
    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->actions:LX/0Px;

    .line 1021589
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1021565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021566
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->title:Ljava/lang/String;

    .line 1021567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->url:Ljava/lang/String;

    .line 1021568
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->linkType:Ljava/lang/String;

    .line 1021569
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->subtitle:Ljava/lang/String;

    .line 1021570
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->ctaText:Ljava/lang/String;

    .line 1021571
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->alertDescription:Ljava/lang/String;

    .line 1021572
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->reshareAlertTitle:Ljava/lang/String;

    .line 1021573
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeText:Ljava/lang/String;

    .line 1021574
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeFormUri:Ljava/lang/String;

    .line 1021575
    sget-object v0, Lcom/facebook/share/model/LinksPreview$MisinformationAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->actions:LX/0Px;

    .line 1021576
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1021564
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1021553
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021554
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021555
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->linkType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021556
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021557
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->ctaText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021558
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->alertDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021559
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->reshareAlertTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021560
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021561
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->disputeFormUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021562
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview$MisinformationData;->actions:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1021563
    return-void
.end method
