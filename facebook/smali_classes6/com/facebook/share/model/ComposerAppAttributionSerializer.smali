.class public Lcom/facebook/share/model/ComposerAppAttributionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/share/model/ComposerAppAttribution;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1021488
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    new-instance v1, Lcom/facebook/share/model/ComposerAppAttributionSerializer;

    invoke-direct {v1}, Lcom/facebook/share/model/ComposerAppAttributionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1021489
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1021490
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/share/model/ComposerAppAttribution;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1021491
    if-nez p0, :cond_0

    .line 1021492
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1021493
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1021494
    invoke-static {p0, p1, p2}, Lcom/facebook/share/model/ComposerAppAttributionSerializer;->b(Lcom/facebook/share/model/ComposerAppAttribution;LX/0nX;LX/0my;)V

    .line 1021495
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1021496
    return-void
.end method

.method private static b(Lcom/facebook/share/model/ComposerAppAttribution;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1021497
    const-string v0, "app_id"

    iget-object v1, p0, Lcom/facebook/share/model/ComposerAppAttribution;->appId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021498
    const-string v0, "app_name"

    iget-object v1, p0, Lcom/facebook/share/model/ComposerAppAttribution;->appName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021499
    const-string v0, "app_key_hash"

    iget-object v1, p0, Lcom/facebook/share/model/ComposerAppAttribution;->appKeyHash:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021500
    const-string v0, "app_metadata"

    iget-object v1, p0, Lcom/facebook/share/model/ComposerAppAttribution;->appMetadata:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1021501
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1021502
    check-cast p1, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-static {p1, p2, p3}, Lcom/facebook/share/model/ComposerAppAttributionSerializer;->a(Lcom/facebook/share/model/ComposerAppAttribution;LX/0nX;LX/0my;)V

    return-void
.end method
