.class public Lcom/facebook/share/model/LinksPreview;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/model/LinksPreview;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final caption:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public final href:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "href"
    .end annotation
.end field

.field public final media:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/share/model/LinksPreview$Media;",
            ">;"
        }
    .end annotation
.end field

.field public final misinformationData:Lcom/facebook/share/model/LinksPreview$MisinformationData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "misinformation_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final properties:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "properties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1021592
    const-class v0, Lcom/facebook/share/model/LinksPreviewDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021593
    new-instance v0, LX/5vF;

    invoke-direct {v0}, LX/5vF;-><init>()V

    sput-object v0, Lcom/facebook/share/model/LinksPreview;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1021594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021595
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview;->href:Ljava/lang/String;

    .line 1021596
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    .line 1021597
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    .line 1021598
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 1021599
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->media:Ljava/util/List;

    .line 1021600
    iput-object v1, p0, Lcom/facebook/share/model/LinksPreview;->misinformationData:Lcom/facebook/share/model/LinksPreview$MisinformationData;

    .line 1021601
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->properties:Ljava/util/Map;

    .line 1021602
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1021603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->href:Ljava/lang/String;

    .line 1021605
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    .line 1021606
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    .line 1021607
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 1021608
    const-class v0, Lcom/facebook/share/model/LinksPreview$Media;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->media:Ljava/util/List;

    .line 1021609
    const-class v0, Lcom/facebook/share/model/LinksPreview$MisinformationData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/LinksPreview$MisinformationData;

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->misinformationData:Lcom/facebook/share/model/LinksPreview$MisinformationData;

    .line 1021610
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/LinksPreview;->properties:Ljava/util/Map;

    .line 1021611
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/share/model/LinksPreview$Media;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1021612
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->media:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1021613
    :goto_0
    return-object v0

    .line 1021614
    :cond_0
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->media:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/LinksPreview$Media;

    .line 1021615
    const-string v3, "image"

    iget-object v4, v0, Lcom/facebook/share/model/LinksPreview$Media;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1021616
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1021617
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1021618
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1021619
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->href:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021620
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021621
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021622
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021623
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->media:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1021624
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->misinformationData:Lcom/facebook/share/model/LinksPreview$MisinformationData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1021625
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->properties:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1021626
    return-void
.end method
