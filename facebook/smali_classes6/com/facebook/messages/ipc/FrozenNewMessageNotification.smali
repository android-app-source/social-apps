.class public Lcom/facebook/messages/ipc/FrozenNewMessageNotification;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5Qp;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/5Qp;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/facebook/messages/ipc/FrozenNewMessageNotification;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messages/ipc/FrozenNewMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field public final g:Landroid/app/PendingIntent;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field private final j:J
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field private final k:J
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field public final l:Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113610
    new-instance v0, LX/6bS;

    invoke-direct {v0}, LX/6bS;-><init>()V

    sput-object v0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1113638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113639
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->h:Ljava/lang/String;

    .line 1113640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    .line 1113641
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->a:Ljava/lang/String;

    .line 1113642
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->b:Ljava/lang/String;

    .line 1113643
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->c:Ljava/lang/String;

    .line 1113644
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->d:Ljava/lang/String;

    .line 1113645
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->e:Ljava/lang/String;

    .line 1113646
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->f:Ljava/lang/String;

    .line 1113647
    invoke-static {p1}, Landroid/app/PendingIntent;->readPendingIntentOrNullFromParcel(Landroid/os/Parcel;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->g:Landroid/app/PendingIntent;

    .line 1113648
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 1113649
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->i:Ljava/lang/String;

    .line 1113650
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->j:J

    .line 1113651
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->k:J

    .line 1113652
    const-class v0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->l:Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;

    .line 1113653
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1113624
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113625
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113626
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113627
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113628
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113629
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113630
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113631
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->g:Landroid/app/PendingIntent;

    invoke-static {v0, p1}, Landroid/app/PendingIntent;->writePendingIntentOrNullToParcel(Landroid/app/PendingIntent;Landroid/os/Parcel;)V

    .line 1113632
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113633
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113634
    iget-wide v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1113635
    iget-wide v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1113636
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->l:Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113637
    return-void
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1113620
    check-cast p1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;

    .line 1113621
    invoke-virtual {p0, p1}, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1113622
    const/4 v0, 0x0

    .line 1113623
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->j:J

    iget-wide v2, p1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113619
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1113614
    instance-of v0, p1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;

    if-nez v0, :cond_0

    .line 1113615
    const/4 v0, 0x0

    .line 1113616
    :goto_0
    return v0

    .line 1113617
    :cond_0
    check-cast p1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;

    .line 1113618
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1113613
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1113611
    invoke-static {p1, p2, p0}, LX/5Qq;->a(Landroid/os/Parcel;ILX/5Qp;)V

    .line 1113612
    return-void
.end method
