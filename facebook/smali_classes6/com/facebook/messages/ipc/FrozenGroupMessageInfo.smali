.class public Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5Qp;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messages/ipc/FrozenParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113590
    new-instance v0, LX/6bR;

    invoke-direct {v0}, LX/6bR;-><init>()V

    sput-object v0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1113591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113592
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    .line 1113593
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->a:I

    .line 1113594
    const-class v0, Lcom/facebook/messages/ipc/FrozenParticipant;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->b:LX/0Px;

    .line 1113595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->c:Ljava/lang/String;

    .line 1113596
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->d:Ljava/lang/String;

    .line 1113597
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1113598
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113599
    iget v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113600
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1113601
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113602
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113603
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113604
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1113605
    invoke-static {p1, p2, p0}, LX/5Qq;->a(Landroid/os/Parcel;ILX/5Qp;)V

    .line 1113606
    return-void
.end method
