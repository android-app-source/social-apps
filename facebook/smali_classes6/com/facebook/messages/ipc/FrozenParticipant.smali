.class public Lcom/facebook/messages/ipc/FrozenParticipant;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5Qp;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messages/ipc/FrozenParticipant;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/ipc/annotation/FrozenField;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113664
    new-instance v0, LX/6bT;

    invoke-direct {v0}, LX/6bT;-><init>()V

    sput-object v0, Lcom/facebook/messages/ipc/FrozenParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1113665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113666
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    .line 1113667
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    .line 1113668
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messages/ipc/FrozenParticipant;->b:Ljava/lang/String;

    .line 1113669
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1113660
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113661
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113662
    iget-object v0, p0, Lcom/facebook/messages/ipc/FrozenParticipant;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113663
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113659
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1113657
    invoke-static {p1, p2, p0}, LX/5Qq;->a(Landroid/os/Parcel;ILX/5Qp;)V

    .line 1113658
    return-void
.end method
