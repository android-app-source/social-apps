.class public final Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x70d22e13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 978767
    const-class v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 978766
    const-class v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 978764
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 978765
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 978762
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 978763
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 978753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 978754
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 978755
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 978756
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 978757
    iget v2, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 978758
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 978759
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 978760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 978761
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 978730
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->f:Ljava/util/List;

    .line 978731
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 978740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 978741
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 978742
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 978743
    if-eqz v1, :cond_2

    .line 978744
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    .line 978745
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 978746
    :goto_0
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 978747
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 978748
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 978749
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    .line 978750
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 978751
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 978752
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 978737
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 978738
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->e:I

    .line 978739
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 978734
    new-instance v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;-><init>()V

    .line 978735
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 978736
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 978733
    const v0, -0x5a232f55

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 978732
    const v0, -0x530b25c0

    return v0
.end method
