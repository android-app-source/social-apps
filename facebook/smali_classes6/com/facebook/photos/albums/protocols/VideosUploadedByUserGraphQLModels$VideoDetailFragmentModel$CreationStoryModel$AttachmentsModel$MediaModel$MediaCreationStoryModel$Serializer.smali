.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 979241
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 979242
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 979247
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 979244
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 979245
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/5hu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 979246
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 979243
    check-cast p1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;LX/0nX;LX/0my;)V

    return-void
.end method
