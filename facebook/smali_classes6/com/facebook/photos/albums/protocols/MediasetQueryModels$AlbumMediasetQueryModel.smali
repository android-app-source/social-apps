.class public final Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d5e06b6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 977586
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 977585
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 977556
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 977557
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 977582
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 977583
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 977584
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 977574
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 977575
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 977576
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 977577
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 977578
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 977579
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 977580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 977581
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 977566
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 977567
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 977568
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977569
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 977570
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;

    .line 977571
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->f:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977572
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 977573
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 977587
    new-instance v0, LX/5h8;

    invoke-direct {v0, p1}, LX/5h8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 977564
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->f:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->f:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977565
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;->f:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 977562
    invoke-virtual {p2}, LX/18L;->a()V

    .line 977563
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 977561
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 977558
    new-instance v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;-><init>()V

    .line 977559
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 977560
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 977555
    const v0, -0xa765bfd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 977554
    const v0, 0x252222

    return v0
.end method
