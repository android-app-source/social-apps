.class public final Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x530ea8b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 978114
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 978115
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 978116
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 978117
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 978118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 978119
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 978120
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 978121
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 978122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 978123
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 978124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 978125
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 978126
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 978127
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 978128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;

    .line 978129
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 978130
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 978131
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 978132
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 978133
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 978134
    new-instance v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$TaggedMediasetQueryModel$TaggedMediasetModel;-><init>()V

    .line 978135
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 978136
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 978137
    const v0, 0x4c62824a    # 5.937796E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 978138
    const v0, 0x28ddea18

    return v0
.end method
