.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 980796
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 980797
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 980798
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 980799
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 980800
    const/4 v2, 0x0

    .line 980801
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 980802
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 980803
    :goto_0
    move v1, v2

    .line 980804
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 980805
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 980806
    new-instance v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;-><init>()V

    .line 980807
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 980808
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 980809
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 980810
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 980811
    :cond_0
    return-object v1

    .line 980812
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 980813
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 980814
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 980815
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 980816
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 980817
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 980818
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 980819
    :cond_4
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 980820
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 980821
    :cond_5
    const-string v6, "uploaded_videos"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 980822
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 980823
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_e

    .line 980824
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 980825
    :goto_2
    move v1, v5

    .line 980826
    goto :goto_1

    .line 980827
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 980828
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 980829
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 980830
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 980831
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 980832
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 980833
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 980834
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 980835
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_8

    if-eqz v9, :cond_8

    .line 980836
    const-string v10, "count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 980837
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v8, v1

    move v1, v6

    goto :goto_3

    .line 980838
    :cond_9
    const-string v10, "nodes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 980839
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 980840
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_a

    .line 980841
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_a

    .line 980842
    const/4 v10, 0x0

    .line 980843
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v11, :cond_12

    .line 980844
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 980845
    :goto_5
    move v9, v10

    .line 980846
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 980847
    :cond_a
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 980848
    goto :goto_3

    .line 980849
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 980850
    :cond_c
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 980851
    if-eqz v1, :cond_d

    .line 980852
    invoke-virtual {v0, v5, v8, v5}, LX/186;->a(III)V

    .line 980853
    :cond_d
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 980854
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_e
    move v1, v5

    move v7, v5

    move v8, v5

    goto :goto_3

    .line 980855
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 980856
    :cond_10
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_11

    .line 980857
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 980858
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 980859
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_10

    if-eqz v11, :cond_10

    .line 980860
    const-string p0, "videoThumbnail"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 980861
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_6

    .line 980862
    :cond_11
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 980863
    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 980864
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    goto :goto_5

    :cond_12
    move v9, v10

    goto :goto_6
.end method
