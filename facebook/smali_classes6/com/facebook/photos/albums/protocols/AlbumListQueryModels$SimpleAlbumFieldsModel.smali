.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2d544b02
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:J

.field private k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 974305
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 974306
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 974307
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 974308
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAlbumCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 974309
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 974310
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974317
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 974318
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974311
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 974312
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974313
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l:Ljava/lang/String;

    .line 974314
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974315
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    .line 974316
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974301
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974302
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974303
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    .line 974304
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    return-object v0
.end method

.method private q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyScope"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974299
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 974300
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974297
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974298
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    .line 974270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 974271
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x1c013d1a

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 974272
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 974273
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 974274
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 974275
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 974276
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 974277
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 974278
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v11, -0x72d5d3c2

    invoke-static {v3, v2, v11}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 974279
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 974280
    const/16 v2, 0xe

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 974281
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 974282
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 974283
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 974284
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 974285
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 974286
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 974287
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 974288
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 974289
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 974290
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 974291
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 974292
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 974293
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 974294
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 974295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 974296
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 974229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 974230
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 974231
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1c013d1a

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 974232
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 974233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974234
    iput v3, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->e:I

    move-object v1, v0

    .line 974235
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 974236
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 974237
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 974238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974239
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 974240
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 974241
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    .line 974242
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 974243
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974244
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$MediaOwnerObjectModel;

    .line 974245
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 974246
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974247
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 974248
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974249
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974250
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 974251
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    .line 974252
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 974253
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974254
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->p:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel$OwnerModel;

    .line 974255
    :cond_4
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 974256
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x72d5d3c2

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 974257
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 974258
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974259
    iput v3, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q:I

    move-object v1, v0

    .line 974260
    :cond_5
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 974261
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974262
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 974263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974264
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->r:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 974265
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 974266
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 974267
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 974268
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_7
    move-object p0, v1

    .line 974269
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974228
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 974219
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 974220
    const/4 v0, 0x0

    const v1, -0x1c013d1a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->e:I

    .line 974221
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->g:Z

    .line 974222
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->h:Z

    .line 974223
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->i:Z

    .line 974224
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->j:J

    .line 974225
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->o:J

    .line 974226
    const/16 v0, 0xc

    const v1, -0x72d5d3c2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;->q:I

    .line 974227
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 974216
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;-><init>()V

    .line 974217
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 974218
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 974215
    const v0, 0x462425d8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 974214
    const v0, 0x3c68e4f

    return v0
.end method
