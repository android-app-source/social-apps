.class public final Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 978681
    const-class v0, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 978682
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 978684
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 978685
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 978686
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 978687
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 978688
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 978689
    if-eqz v2, :cond_0

    .line 978690
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 978691
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 978692
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 978693
    if-eqz v2, :cond_1

    .line 978694
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 978695
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 978696
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 978697
    if-eqz v2, :cond_2

    .line 978698
    const-string p0, "video_list_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 978699
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 978700
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 978701
    if-eqz v2, :cond_3

    .line 978702
    const-string p0, "video_list_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 978703
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 978704
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 978705
    if-eqz v2, :cond_4

    .line 978706
    const-string p0, "video_list_videos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 978707
    invoke-static {v1, v2, p1, p2}, LX/5hP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 978708
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 978709
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 978683
    check-cast p1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
