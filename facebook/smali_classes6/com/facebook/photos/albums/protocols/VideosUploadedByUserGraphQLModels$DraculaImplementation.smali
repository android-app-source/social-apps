.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 978975
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 978976
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 978973
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 978974
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 978947
    if-nez p1, :cond_0

    move v0, v1

    .line 978948
    :goto_0
    return v0

    .line 978949
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 978950
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 978951
    :sswitch_0
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 978952
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 978953
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 978954
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 978955
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 978956
    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 978957
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 978958
    const v3, 0x5bd5314d

    const/4 v6, 0x0

    .line 978959
    if-nez v2, :cond_1

    move v5, v6

    .line 978960
    :goto_1
    move v2, v5

    .line 978961
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 978962
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 978963
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 978964
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 978965
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    .line 978966
    if-nez p1, :cond_2

    const/4 v5, 0x0

    .line 978967
    :goto_2
    if-ge v6, p1, :cond_3

    .line 978968
    invoke-virtual {p0, v2, v6}, LX/15i;->q(II)I

    move-result p2

    .line 978969
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v5, v6

    .line 978970
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 978971
    :cond_2
    new-array v5, p1, [I

    goto :goto_2

    .line 978972
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p3, v5, v6}, LX/186;->a([IZ)I

    move-result v5

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x50ceccc1 -> :sswitch_1
        0x5bd5314d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 978946
    new-instance v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 978941
    if-eqz p0, :cond_0

    .line 978942
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 978943
    if-eq v0, p0, :cond_0

    .line 978944
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 978945
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 978927
    sparse-switch p2, :sswitch_data_0

    .line 978928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 978929
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 978930
    invoke-static {v0, p3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 978931
    :goto_0
    return-void

    .line 978932
    :sswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 978933
    const v1, 0x5bd5314d

    .line 978934
    if-eqz v0, :cond_0

    .line 978935
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 978936
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 978937
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 978938
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 978939
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 978940
    :cond_0
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x50ceccc1 -> :sswitch_1
        0x5bd5314d -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 978921
    if-eqz p1, :cond_0

    .line 978922
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 978923
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;

    .line 978924
    if-eq v0, v1, :cond_0

    .line 978925
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 978926
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 978920
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 978977
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 978978
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 978915
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 978916
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 978917
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 978918
    iput p2, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->b:I

    .line 978919
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 978914
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 978913
    new-instance v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 978910
    iget v0, p0, LX/1vt;->c:I

    .line 978911
    move v0, v0

    .line 978912
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 978907
    iget v0, p0, LX/1vt;->c:I

    .line 978908
    move v0, v0

    .line 978909
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 978904
    iget v0, p0, LX/1vt;->b:I

    .line 978905
    move v0, v0

    .line 978906
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 978901
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 978902
    move-object v0, v0

    .line 978903
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 978892
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 978893
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 978894
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 978895
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 978896
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 978897
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 978898
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 978899
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 978900
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 978889
    iget v0, p0, LX/1vt;->c:I

    .line 978890
    move v0, v0

    .line 978891
    return v0
.end method
