.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x22bc2191
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973061
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973062
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973063
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973064
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973065
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 973066
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 973067
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 973068
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973069
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973070
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 973071
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 973072
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 973073
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 973074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973075
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 973076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973077
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973078
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973079
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973080
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    .line 973081
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973082
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973083
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 973084
    new-instance v0, LX/5g8;

    invoke-direct {v0, p1}, LX/5g8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973085
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973086
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 973087
    invoke-virtual {p2}, LX/18L;->a()V

    .line 973088
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 973089
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973090
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;-><init>()V

    .line 973091
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973092
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973093
    const v0, -0x215dcef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973094
    const v0, 0x252222

    return v0
.end method
