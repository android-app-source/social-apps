.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 975449
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 975450
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 975533
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 975452
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 975453
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x1

    const-wide/16 v6, 0x0

    .line 975454
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 975455
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975456
    if-eqz v2, :cond_0

    .line 975457
    const-string v3, "album_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975458
    invoke-static {v1, v2, p1, p2}, LX/5go;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975459
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 975460
    if-eqz v2, :cond_1

    .line 975461
    const-string v2, "album_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975462
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975463
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975464
    if-eqz v2, :cond_2

    .line 975465
    const-string v3, "allow_contributors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975466
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975467
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975468
    if-eqz v2, :cond_3

    .line 975469
    const-string v3, "can_edit_caption"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975470
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975471
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975472
    if-eqz v2, :cond_4

    .line 975473
    const-string v3, "can_upload"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975474
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975475
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975476
    if-eqz v2, :cond_5

    .line 975477
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975478
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975479
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975480
    if-eqz v2, :cond_6

    .line 975481
    const-string v3, "contributors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975482
    invoke-static {v1, v2, p1, p2}, LX/5gk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975483
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 975484
    cmp-long v4, v2, v6

    if-eqz v4, :cond_7

    .line 975485
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975486
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 975487
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975488
    if-eqz v2, :cond_8

    .line 975489
    const-string v3, "explicit_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975490
    invoke-static {v1, v2, p1}, LX/5gp;->a(LX/15i;ILX/0nX;)V

    .line 975491
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 975492
    if-eqz v2, :cond_9

    .line 975493
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975494
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975495
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975496
    if-eqz v2, :cond_a

    .line 975497
    const-string v3, "media"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975498
    invoke-static {v1, v2, p1, p2}, LX/5hF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975499
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975500
    if-eqz v2, :cond_b

    .line 975501
    const-string v3, "media_owner_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975502
    invoke-static {v1, v2, p1}, LX/5gq;->a(LX/15i;ILX/0nX;)V

    .line 975503
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975504
    if-eqz v2, :cond_c

    .line 975505
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975506
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 975507
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 975508
    cmp-long v4, v2, v6

    if-eqz v4, :cond_d

    .line 975509
    const-string v4, "modified_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975510
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 975511
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975512
    if-eqz v2, :cond_e

    .line 975513
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975514
    invoke-static {v1, v2, p1}, LX/5gr;->a(LX/15i;ILX/0nX;)V

    .line 975515
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975516
    if-eqz v2, :cond_f

    .line 975517
    const-string v3, "photo_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975518
    invoke-static {v1, v2, p1}, LX/5gs;->a(LX/15i;ILX/0nX;)V

    .line 975519
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975520
    if-eqz v2, :cond_10

    .line 975521
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975522
    invoke-static {v1, v2, p1, p2}, LX/5gv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975523
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975524
    if-eqz v2, :cond_11

    .line 975525
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975526
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 975527
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 975528
    if-eqz v2, :cond_12

    .line 975529
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975530
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975531
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 975532
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 975451
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;LX/0nX;LX/0my;)V

    return-void
.end method
