.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf563637
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973590
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973591
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973592
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973593
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973604
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 973605
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 973594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973595
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973596
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 973597
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 973598
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 973599
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 973600
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 973601
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 973602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973603
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 973582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973583
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973584
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    .line 973585
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    .line 973587
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    .line 973588
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973589
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973572
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973579
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;-><init>()V

    .line 973580
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973581
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973578
    const v0, -0x5c1d73f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973577
    const v0, 0x64687ce

    return v0
.end method

.method public final j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFamilyTaggedMediaset"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973575
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    .line 973576
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973573
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 973574
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method
