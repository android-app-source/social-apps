.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 976629
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 976630
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 976627
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 976628
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 976610
    if-nez p1, :cond_0

    .line 976611
    :goto_0
    return v0

    .line 976612
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 976613
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 976614
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 976615
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 976616
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 976617
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 976618
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 976619
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 976620
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 976621
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 976622
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 976623
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 976624
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 976625
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 976626
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4222642f -> :sswitch_1
        0x7d4c82a2 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 976609
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 976606
    sparse-switch p0, :sswitch_data_0

    .line 976607
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 976608
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x4222642f -> :sswitch_0
        0x7d4c82a2 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 976605
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 976603
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->b(I)V

    .line 976604
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 976573
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 976574
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 976575
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a:LX/15i;

    .line 976576
    iput p2, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->b:I

    .line 976577
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 976631
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 976602
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 976599
    iget v0, p0, LX/1vt;->c:I

    .line 976600
    move v0, v0

    .line 976601
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 976596
    iget v0, p0, LX/1vt;->c:I

    .line 976597
    move v0, v0

    .line 976598
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 976593
    iget v0, p0, LX/1vt;->b:I

    .line 976594
    move v0, v0

    .line 976595
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976590
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 976591
    move-object v0, v0

    .line 976592
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 976581
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 976582
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 976583
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 976584
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 976585
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 976586
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 976587
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 976588
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 976589
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 976578
    iget v0, p0, LX/1vt;->c:I

    .line 976579
    move v0, v0

    .line 976580
    return v0
.end method
