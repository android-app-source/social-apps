.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 973059
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 973060
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 973058
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 973045
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 973046
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 973047
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 973048
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 973049
    if-eqz v2, :cond_0

    .line 973050
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973051
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 973052
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 973053
    if-eqz v2, :cond_1

    .line 973054
    const-string p0, "albums"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973055
    invoke-static {v1, v2, p1, p2}, LX/4Kp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 973056
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 973057
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 973044
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
