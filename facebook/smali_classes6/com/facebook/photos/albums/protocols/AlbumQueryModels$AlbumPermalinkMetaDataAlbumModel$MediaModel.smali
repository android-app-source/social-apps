.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 975738
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 975739
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 975723
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 975724
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 975743
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975744
    iget v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 975733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 975734
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 975735
    iget v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 975736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 975737
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 975740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 975741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 975742
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 975730
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 975731
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;->e:I

    .line 975732
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 975727
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;-><init>()V

    .line 975728
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 975729
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 975726
    const v0, 0x49060018    # 548865.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 975725
    const v0, 0x2c8ffc04

    return v0
.end method
