.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ea026b3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 976023
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 976022
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 976008
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 976009
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976020
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 976021
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 976014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 976015
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 976016
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 976017
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 976018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 976019
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 976024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 976025
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 976026
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 976027
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 976028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    .line 976029
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 976030
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 976031
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976013
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 976010
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;-><init>()V

    .line 976011
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 976012
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 976007
    const v0, -0x3a71dbb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 976006
    const v0, 0x4984e12

    return v0
.end method
