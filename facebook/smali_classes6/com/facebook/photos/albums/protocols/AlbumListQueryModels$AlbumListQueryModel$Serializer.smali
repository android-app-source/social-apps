.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 973126
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 973127
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 973128
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 973129
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 973130
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 973131
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 973132
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 973133
    if-eqz v2, :cond_0

    .line 973134
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973135
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 973136
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 973137
    if-eqz v2, :cond_1

    .line 973138
    const-string p0, "albums"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973139
    invoke-static {v1, v2, p1, p2}, LX/4Kp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 973140
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 973141
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 973142
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
