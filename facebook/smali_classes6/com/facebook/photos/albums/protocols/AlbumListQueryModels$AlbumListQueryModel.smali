.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x22bc2191
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973176
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973175
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973173
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973174
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 973171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 973172
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 973162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973163
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973164
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 973165
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 973166
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 973167
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 973168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973169
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 973154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973155
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973156
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973157
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973158
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    .line 973159
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973160
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973161
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 973153
    new-instance v0, LX/5g9;

    invoke-direct {v0, p1}, LX/5g9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973151
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973152
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->f:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 973143
    invoke-virtual {p2}, LX/18L;->a()V

    .line 973144
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 973150
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973147
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;-><init>()V

    .line 973148
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973149
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973146
    const v0, 0x5e023c37

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973145
    const v0, 0x252222

    return v0
.end method
