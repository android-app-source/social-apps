.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 975745
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 975746
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 975747
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 975748
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 975749
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x1

    const-wide/16 v6, 0x0

    .line 975750
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 975751
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975752
    if-eqz v2, :cond_0

    .line 975753
    const-string v3, "album_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975754
    invoke-static {v1, v2, p1, p2}, LX/5go;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975755
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 975756
    if-eqz v2, :cond_1

    .line 975757
    const-string v2, "album_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975758
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975759
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975760
    if-eqz v2, :cond_2

    .line 975761
    const-string v3, "allow_contributors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975762
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975763
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975764
    if-eqz v2, :cond_3

    .line 975765
    const-string v3, "can_edit_caption"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975766
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975767
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975768
    if-eqz v2, :cond_4

    .line 975769
    const-string v3, "can_upload"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975770
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975771
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 975772
    if-eqz v2, :cond_5

    .line 975773
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975774
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 975775
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975776
    if-eqz v2, :cond_6

    .line 975777
    const-string v3, "contributors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975778
    invoke-static {v1, v2, p1, p2}, LX/5gk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975779
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 975780
    cmp-long v4, v2, v6

    if-eqz v4, :cond_7

    .line 975781
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975782
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 975783
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975784
    if-eqz v2, :cond_8

    .line 975785
    const-string v3, "explicit_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975786
    invoke-static {v1, v2, p1}, LX/5gp;->a(LX/15i;ILX/0nX;)V

    .line 975787
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 975788
    if-eqz v2, :cond_9

    .line 975789
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975790
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975791
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975792
    if-eqz v2, :cond_a

    .line 975793
    const-string v3, "media"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975794
    invoke-static {v1, v2, p1}, LX/5gm;->a(LX/15i;ILX/0nX;)V

    .line 975795
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975796
    if-eqz v2, :cond_b

    .line 975797
    const-string v3, "media_owner_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975798
    invoke-static {v1, v2, p1}, LX/5gq;->a(LX/15i;ILX/0nX;)V

    .line 975799
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975800
    if-eqz v2, :cond_c

    .line 975801
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975802
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 975803
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 975804
    cmp-long v4, v2, v6

    if-eqz v4, :cond_d

    .line 975805
    const-string v4, "modified_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975806
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 975807
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975808
    if-eqz v2, :cond_e

    .line 975809
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975810
    invoke-static {v1, v2, p1}, LX/5gr;->a(LX/15i;ILX/0nX;)V

    .line 975811
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975812
    if-eqz v2, :cond_f

    .line 975813
    const-string v3, "photo_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975814
    invoke-static {v1, v2, p1}, LX/5gs;->a(LX/15i;ILX/0nX;)V

    .line 975815
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975816
    if-eqz v2, :cond_10

    .line 975817
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975818
    invoke-static {v1, v2, p1, p2}, LX/5gv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975819
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975820
    if-eqz v2, :cond_11

    .line 975821
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975822
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 975823
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 975824
    if-eqz v2, :cond_12

    .line 975825
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975826
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 975827
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 975828
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 975829
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;LX/0nX;LX/0my;)V

    return-void
.end method
