.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 975328
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 975329
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 975330
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 975331
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 975332
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 975333
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 975334
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 975335
    if-eqz v2, :cond_0

    .line 975336
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 975337
    invoke-static {v1, v2, p1, p2}, LX/59W;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 975338
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 975339
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 975340
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
