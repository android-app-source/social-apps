.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x145cdd32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 979081
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 979082
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 979083
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 979084
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 979085
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 979086
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 979087
    return-void
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;
    .locals 9

    .prologue
    .line 979088
    if-nez p0, :cond_0

    .line 979089
    const/4 p0, 0x0

    .line 979090
    :goto_0
    return-object p0

    .line 979091
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;

    if-eqz v0, :cond_1

    .line 979092
    check-cast p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;

    goto :goto_0

    .line 979093
    :cond_1
    new-instance v0, LX/5hW;

    invoke-direct {v0}, LX/5hW;-><init>()V

    .line 979094
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5hW;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 979095
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hW;->b:Ljava/lang/String;

    .line 979096
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 979097
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 979098
    iget-object v3, v0, LX/5hW;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 979099
    iget-object v5, v0, LX/5hW;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 979100
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 979101
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 979102
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 979103
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 979104
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 979105
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 979106
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 979107
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 979108
    new-instance v3, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;-><init>(LX/15i;)V

    .line 979109
    move-object p0, v3

    .line 979110
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 979111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 979112
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 979113
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 979114
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 979115
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 979116
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 979117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 979118
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 979077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 979078
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 979079
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 979080
    new-instance v0, LX/5hX;

    invoke-direct {v0, p1}, LX/5hX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979074
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 979075
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 979076
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 979072
    invoke-virtual {p2}, LX/18L;->a()V

    .line 979073
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 979071
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 979068
    new-instance v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;-><init>()V

    .line 979069
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 979070
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979066
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->f:Ljava/lang/String;

    .line 979067
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ActorsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 979065
    const v0, -0x61c2282

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 979064
    const v0, 0x3c2b9d5

    return v0
.end method
