.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 973212
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 973213
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 973180
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 973181
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 973214
    if-nez p1, :cond_0

    move v0, v1

    .line 973215
    :goto_0
    return v0

    .line 973216
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 973217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 973218
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 973219
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 973220
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 973221
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v3

    .line 973222
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 973223
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 973224
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 973225
    invoke-virtual {p3, v5, v3, v1}, LX/186;->a(III)V

    .line 973226
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 973227
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 973228
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 973229
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 973230
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 973231
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 973232
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 973233
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973234
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 973235
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 973236
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 973237
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 973238
    const v2, 0x5be02d8e

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 973239
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 973240
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 973241
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 973242
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 973243
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 973244
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 973245
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 973246
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 973247
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 973248
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 973249
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 973250
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 973251
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 973252
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 973253
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 973254
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 973255
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x72d5d3c2 -> :sswitch_3
        -0x1c013d1a -> :sswitch_2
        0x57fca506 -> :sswitch_0
        0x5be02d8e -> :sswitch_4
        0x653a24b0 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 973256
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 973271
    if-eqz p0, :cond_0

    .line 973272
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 973273
    if-eq v0, p0, :cond_0

    .line 973274
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 973275
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 973257
    sparse-switch p2, :sswitch_data_0

    .line 973258
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 973259
    :sswitch_0
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 973260
    invoke-static {v0, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 973261
    :goto_0
    :sswitch_1
    return-void

    .line 973262
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 973263
    const v1, 0x5be02d8e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x72d5d3c2 -> :sswitch_2
        -0x1c013d1a -> :sswitch_0
        0x57fca506 -> :sswitch_1
        0x5be02d8e -> :sswitch_1
        0x653a24b0 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 973264
    if-eqz p1, :cond_0

    .line 973265
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v1

    .line 973266
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    .line 973267
    if-eq v0, v1, :cond_0

    .line 973268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 973269
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 973270
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 973205
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 973206
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 973207
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 973208
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 973209
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a:LX/15i;

    .line 973210
    iput p2, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->b:I

    .line 973211
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 973204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 973203
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973200
    iget v0, p0, LX/1vt;->c:I

    .line 973201
    move v0, v0

    .line 973202
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973197
    iget v0, p0, LX/1vt;->c:I

    .line 973198
    move v0, v0

    .line 973199
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 973194
    iget v0, p0, LX/1vt;->b:I

    .line 973195
    move v0, v0

    .line 973196
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973191
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 973192
    move-object v0, v0

    .line 973193
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 973182
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 973183
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 973184
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 973185
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 973186
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 973187
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 973188
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 973189
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 973190
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 973177
    iget v0, p0, LX/1vt;->c:I

    .line 973178
    move v0, v0

    .line 973179
    return v0
.end method
