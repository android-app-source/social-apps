.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41423a80
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973722
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973721
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973719
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973720
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973717
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->f:Ljava/lang/String;

    .line 973718
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 973689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973690
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973691
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 973692
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 973693
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 973694
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 973695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973696
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 973709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973710
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973711
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    .line 973712
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    .line 973714
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    .line 973715
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973716
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 973708
    new-instance v0, LX/5gB;

    invoke-direct {v0, p1}, LX/5gB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973707
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 973705
    invoke-virtual {p2}, LX/18L;->a()V

    .line 973706
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 973704
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973701
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;-><init>()V

    .line 973702
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973703
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973700
    const v0, 0x40d8d06c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973699
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFamilyNonUserMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973697
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    .line 973698
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    return-object v0
.end method
