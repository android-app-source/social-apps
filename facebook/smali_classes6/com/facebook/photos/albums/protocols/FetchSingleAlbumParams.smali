.class public Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 977369
    new-instance v0, LX/5gz;

    invoke-direct {v0}, LX/5gz;-><init>()V

    sput-object v0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 977349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 977350
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->a:Ljava/lang/String;

    .line 977351
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->b:Ljava/lang/String;

    .line 977352
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->c:Ljava/lang/String;

    .line 977353
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->d:I

    .line 977354
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->e:I

    .line 977355
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->f:I

    .line 977356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->g:I

    .line 977357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->h:I

    .line 977358
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V
    .locals 0

    .prologue
    .line 977359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 977360
    iput-object p1, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->a:Ljava/lang/String;

    .line 977361
    iput-object p2, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->b:Ljava/lang/String;

    .line 977362
    iput-object p3, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->c:Ljava/lang/String;

    .line 977363
    iput p4, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->d:I

    .line 977364
    iput p5, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->e:I

    .line 977365
    iput p6, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->f:I

    .line 977366
    iput p7, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->g:I

    .line 977367
    iput p8, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->h:I

    .line 977368
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 977348
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 977339
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 977340
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 977341
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 977342
    iget v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 977343
    iget v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 977344
    iget v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 977345
    iget v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 977346
    iget v0, p0, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 977347
    return-void
.end method
