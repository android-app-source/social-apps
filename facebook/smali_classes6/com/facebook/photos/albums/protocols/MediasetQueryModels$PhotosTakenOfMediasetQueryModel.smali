.class public final Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x69dd3031
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 977958
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 977959
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 977960
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 977961
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 977970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 977971
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 977972
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 977973
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 977974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 977975
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 977962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 977963
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 977964
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977965
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 977966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;

    .line 977967
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977968
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 977969
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 977957
    new-instance v0, LX/5hB;

    invoke-direct {v0, p1}, LX/5hB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 977947
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 977948
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;->e:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 977955
    invoke-virtual {p2}, LX/18L;->a()V

    .line 977956
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 977954
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 977951
    new-instance v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;-><init>()V

    .line 977952
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 977953
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 977950
    const v0, -0x739a8f67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 977949
    const v0, 0x25d6af

    return v0
.end method
