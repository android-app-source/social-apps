.class public final Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 977506
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 977507
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 977508
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 977509
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 977510
    const/4 v2, 0x0

    .line 977511
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 977512
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 977513
    :goto_0
    move v1, v2

    .line 977514
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 977515
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 977516
    new-instance v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$AlbumMediasetQueryModel;-><init>()V

    .line 977517
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 977518
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 977519
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 977520
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 977521
    :cond_0
    return-object v1

    .line 977522
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 977523
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_5

    .line 977524
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 977525
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 977526
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 977527
    const-string p0, "__type__"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 977528
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 977529
    :cond_4
    const-string p0, "media"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 977530
    invoke-static {p1, v0}, LX/5hF;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 977531
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 977532
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 977533
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977534
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
