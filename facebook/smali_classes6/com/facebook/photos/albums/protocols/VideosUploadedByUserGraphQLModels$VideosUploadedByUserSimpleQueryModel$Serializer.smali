.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 980867
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 980868
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 980907
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 980870
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 980871
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 980872
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 980873
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 980874
    if-eqz v2, :cond_0

    .line 980875
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980876
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 980877
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 980878
    if-eqz v2, :cond_1

    .line 980879
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980880
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 980881
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 980882
    if-eqz v2, :cond_6

    .line 980883
    const-string v3, "uploaded_videos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980884
    const/4 v3, 0x0

    .line 980885
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 980886
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 980887
    if-eqz v3, :cond_2

    .line 980888
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980889
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 980890
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 980891
    if-eqz v3, :cond_5

    .line 980892
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980893
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 980894
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_4

    .line 980895
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 980896
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 980897
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 980898
    if-eqz v0, :cond_3

    .line 980899
    const-string v2, "videoThumbnail"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 980900
    invoke-static {v1, v0, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 980901
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 980902
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 980903
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 980904
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 980905
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 980906
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 980869
    check-cast p1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserSimpleQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
