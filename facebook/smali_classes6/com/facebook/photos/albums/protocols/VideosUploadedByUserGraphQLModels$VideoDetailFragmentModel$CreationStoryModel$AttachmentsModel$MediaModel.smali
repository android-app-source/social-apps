.class public final Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x70da508
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 979353
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 979354
    const-class v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 979355
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 979356
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 979357
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 979358
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 979359
    return-void
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;
    .locals 15

    .prologue
    .line 979360
    if-nez p0, :cond_0

    .line 979361
    const/4 p0, 0x0

    .line 979362
    :goto_0
    return-object p0

    .line 979363
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    if-eqz v0, :cond_1

    .line 979364
    check-cast p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    goto :goto_0

    .line 979365
    :cond_1
    new-instance v0, LX/5hZ;

    invoke-direct {v0}, LX/5hZ;-><init>()V

    .line 979366
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 979367
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->c()I

    move-result v1

    iput v1, v0, LX/5hZ;->b:I

    .line 979368
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->c:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    .line 979369
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->e()I

    move-result v1

    iput v1, v0, LX/5hZ;->d:I

    .line 979370
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->bZ_()I

    move-result v1

    iput v1, v0, LX/5hZ;->e:I

    .line 979371
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->ca_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->f:Ljava/lang/String;

    .line 979372
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->j()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 979373
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/5hZ;->h:Z

    .line 979374
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/5hZ;->i:Z

    .line 979375
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->j:Ljava/lang/String;

    .line 979376
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->n()I

    move-result v1

    iput v1, v0, LX/5hZ;->k:I

    .line 979377
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->l:Ljava/lang/String;

    .line 979378
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->m:Ljava/lang/String;

    .line 979379
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5hZ;->n:Ljava/lang/String;

    .line 979380
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/5hZ;->o:Z

    .line 979381
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->s()I

    move-result v1

    iput v1, v0, LX/5hZ;->p:I

    .line 979382
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v14, 0x0

    .line 979383
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 979384
    iget-object v3, v0, LX/5hZ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 979385
    iget-object v5, v0, LX/5hZ;->c:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 979386
    iget-object v7, v0, LX/5hZ;->f:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 979387
    iget-object v8, v0, LX/5hZ;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 979388
    iget-object v9, v0, LX/5hZ;->j:Ljava/lang/String;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 979389
    iget-object v10, v0, LX/5hZ;->l:Ljava/lang/String;

    invoke-virtual {v2, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 979390
    iget-object v11, v0, LX/5hZ;->m:Ljava/lang/String;

    invoke-virtual {v2, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 979391
    iget-object v12, v0, LX/5hZ;->n:Ljava/lang/String;

    invoke-virtual {v2, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 979392
    const/16 v13, 0x10

    invoke-virtual {v2, v13}, LX/186;->c(I)V

    .line 979393
    invoke-virtual {v2, v14, v3}, LX/186;->b(II)V

    .line 979394
    iget v3, v0, LX/5hZ;->b:I

    invoke-virtual {v2, v6, v3, v14}, LX/186;->a(III)V

    .line 979395
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 979396
    const/4 v3, 0x3

    iget v5, v0, LX/5hZ;->d:I

    invoke-virtual {v2, v3, v5, v14}, LX/186;->a(III)V

    .line 979397
    const/4 v3, 0x4

    iget v5, v0, LX/5hZ;->e:I

    invoke-virtual {v2, v3, v5, v14}, LX/186;->a(III)V

    .line 979398
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 979399
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 979400
    const/4 v3, 0x7

    iget-boolean v5, v0, LX/5hZ;->h:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 979401
    const/16 v3, 0x8

    iget-boolean v5, v0, LX/5hZ;->i:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 979402
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 979403
    const/16 v3, 0xa

    iget v5, v0, LX/5hZ;->k:I

    invoke-virtual {v2, v3, v5, v14}, LX/186;->a(III)V

    .line 979404
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 979405
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 979406
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 979407
    const/16 v3, 0xe

    iget-boolean v5, v0, LX/5hZ;->o:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 979408
    const/16 v3, 0xf

    iget v5, v0, LX/5hZ;->p:I

    invoke-virtual {v2, v3, v5, v14}, LX/186;->a(III)V

    .line 979409
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 979410
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 979411
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 979412
    invoke-virtual {v3, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 979413
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 979414
    new-instance v3, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;-><init>(LX/15i;)V

    .line 979415
    move-object p0, v3

    .line 979416
    goto/16 :goto_0
.end method

.method private t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979417
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->g:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->g:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    .line 979418
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->g:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979419
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 979420
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 979421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 979422
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 979423
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 979424
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->ca_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 979425
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 979426
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 979427
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 979428
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 979429
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 979430
    const/16 v8, 0x10

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 979431
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 979432
    const/4 v0, 0x1

    iget v8, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->f:I

    invoke-virtual {p1, v0, v8, v9}, LX/186;->a(III)V

    .line 979433
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 979434
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->h:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 979435
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->i:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 979436
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 979437
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 979438
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 979439
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 979440
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 979441
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 979442
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 979443
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 979444
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 979445
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 979446
    const/16 v0, 0xf

    iget v1, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 979447
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 979448
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 979449
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 979450
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 979451
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    .line 979452
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 979453
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    .line 979454
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->g:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    .line 979455
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 979456
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 979457
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 979458
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    .line 979459
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 979460
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 979461
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 979462
    new-instance v0, LX/5he;

    invoke-direct {v0, p1}, LX/5he;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979463
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->ca_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 979464
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 979465
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->f:I

    .line 979466
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->h:I

    .line 979467
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->i:I

    .line 979468
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->l:Z

    .line 979469
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m:Z

    .line 979470
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o:I

    .line 979471
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->s:Z

    .line 979472
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t:I

    .line 979473
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 979474
    invoke-virtual {p2}, LX/18L;->a()V

    .line 979475
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 979323
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979476
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 979477
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 979478
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 979350
    new-instance v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;-><init>()V

    .line 979351
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 979352
    return-object v0
.end method

.method public final bZ_()I
    .locals 2

    .prologue
    .line 979479
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979480
    iget v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->i:I

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 979321
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979322
    iget v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->f:I

    return v0
.end method

.method public final ca_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979324
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->j:Ljava/lang/String;

    .line 979325
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979326
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel$MediaCreationStoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 979327
    const v0, -0x6fe286aa

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 979328
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979329
    iget v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->h:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 979330
    const v0, 0x46c7fc4

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979331
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 979332
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979333
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->l:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 979334
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979335
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->m:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979336
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->n:Ljava/lang/String;

    .line 979337
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 979338
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979339
    iget v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->o:I

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979340
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p:Ljava/lang/String;

    .line 979341
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979342
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q:Ljava/lang/String;

    .line 979343
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979344
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->r:Ljava/lang/String;

    .line 979345
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 979346
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979347
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->s:Z

    return v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 979348
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 979349
    iget v0, p0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$AttachmentsModel$MediaModel;->t:I

    return v0
.end method
