.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x123f4f29
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 976356
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 976357
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 976358
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 976359
    return-void
.end method

.method private j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976376
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->f:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->f:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    .line 976377
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->f:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976360
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->i:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->i:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 976361
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->i:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 976362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 976363
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 976364
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 976365
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 976366
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 976367
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 976368
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 976369
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 976370
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 976371
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 976372
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 976373
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 976374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 976375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 976341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 976342
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 976343
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    .line 976344
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 976345
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    .line 976346
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->f:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    .line 976347
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 976348
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 976349
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 976350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    .line 976351
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->i:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 976352
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 976353
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976354
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->e:Ljava/lang/String;

    .line 976355
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 976338
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;-><init>()V

    .line 976339
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 976340
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976337
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976335
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->g:Ljava/lang/String;

    .line 976336
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976333
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->h:Ljava/lang/String;

    .line 976334
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 976332
    const v0, 0x5981e802

    return v0
.end method

.method public final synthetic e()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 976331
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 976330
    const v0, -0x1c648c34

    return v0
.end method
