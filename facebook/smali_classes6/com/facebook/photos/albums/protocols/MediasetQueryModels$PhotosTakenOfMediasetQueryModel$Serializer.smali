.class public final Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 977934
    const-class v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 977935
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 977946
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 977937
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 977938
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 977939
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 977940
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 977941
    if-eqz v2, :cond_0

    .line 977942
    const-string p0, "photos_taken_of"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 977943
    invoke-static {v1, v2, p1, p2}, LX/5hF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 977944
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 977945
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 977936
    check-cast p1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/MediasetQueryModels$PhotosTakenOfMediasetQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
